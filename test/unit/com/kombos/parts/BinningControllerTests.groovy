package com.kombos.parts



import org.junit.*
import grails.test.mixin.*

@TestFor(BinningController)
@Mock(Binning)
class BinningControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/binning/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.binningInstanceList.size() == 0
        assert model.binningInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.binningInstance != null
    }

    void testSave() {
        controller.save()

        assert model.binningInstance != null
        assert view == '/binning/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/binning/show/1'
        assert controller.flash.message != null
        assert Binning.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/binning/list'

        populateValidParams(params)
        def binning = new Binning(params)

        assert binning.save() != null

        params.id = binning.id

        def model = controller.show()

        assert model.binningInstance == binning
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/binning/list'

        populateValidParams(params)
        def binning = new Binning(params)

        assert binning.save() != null

        params.id = binning.id

        def model = controller.edit()

        assert model.binningInstance == binning
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/binning/list'

        response.reset()

        populateValidParams(params)
        def binning = new Binning(params)

        assert binning.save() != null

        // test invalid parameters in update
        params.id = binning.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/binning/edit"
        assert model.binningInstance != null

        binning.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/binning/show/$binning.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        binning.clearErrors()

        populateValidParams(params)
        params.id = binning.id
        params.version = -1
        controller.update()

        assert view == "/binning/edit"
        assert model.binningInstance != null
        assert model.binningInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/binning/list'

        response.reset()

        populateValidParams(params)
        def binning = new Binning(params)

        assert binning.save() != null
        assert Binning.count() == 1

        params.id = binning.id

        controller.delete()

        assert Binning.count() == 0
        assert Binning.get(binning.id) == null
        assert response.redirectedUrl == '/binning/list'
    }

    void testUpdateField() {
        fail "Implement me"
    }

    void testMassDelete() {
        fail "Implement me"
    }
}
