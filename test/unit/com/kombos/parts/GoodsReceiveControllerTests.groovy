package com.kombos.parts



import org.junit.*
import grails.test.mixin.*

@TestFor(GoodsReceiveController)
@Mock(GoodsReceive)
class GoodsReceiveControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/goodsReceive/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.goodsReceiveInstanceList.size() == 0
        assert model.goodsReceiveInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.goodsReceiveInstance != null
    }

    void testSave() {
        controller.save()

        assert model.goodsReceiveInstance != null
        assert view == '/goodsReceive/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/goodsReceive/show/1'
        assert controller.flash.message != null
        assert GoodsReceive.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/goodsReceive/list'

        populateValidParams(params)
        def goodsReceive = new GoodsReceive(params)

        assert goodsReceive.save() != null

        params.id = goodsReceive.id

        def model = controller.show()

        assert model.goodsReceiveInstance == goodsReceive
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/goodsReceive/list'

        populateValidParams(params)
        def goodsReceive = new GoodsReceive(params)

        assert goodsReceive.save() != null

        params.id = goodsReceive.id

        def model = controller.edit()

        assert model.goodsReceiveInstance == goodsReceive
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/goodsReceive/list'

        response.reset()

        populateValidParams(params)
        def goodsReceive = new GoodsReceive(params)

        assert goodsReceive.save() != null

        // test invalid parameters in update
        params.id = goodsReceive.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/goodsReceive/edit"
        assert model.goodsReceiveInstance != null

        goodsReceive.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/goodsReceive/show/$goodsReceive.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        goodsReceive.clearErrors()

        populateValidParams(params)
        params.id = goodsReceive.id
        params.version = -1
        controller.update()

        assert view == "/goodsReceive/edit"
        assert model.goodsReceiveInstance != null
        assert model.goodsReceiveInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/goodsReceive/list'

        response.reset()

        populateValidParams(params)
        def goodsReceive = new GoodsReceive(params)

        assert goodsReceive.save() != null
        assert GoodsReceive.count() == 1

        params.id = goodsReceive.id

        controller.delete()

        assert GoodsReceive.count() == 0
        assert GoodsReceive.get(goodsReceive.id) == null
        assert response.redirectedUrl == '/goodsReceive/list'
    }

    void testUpdateField() {
        fail "Implement me"
    }

    void testMassDelete() {
        fail "Implement me"
    }
}
