package com.kombos.parts



import org.junit.*
import grails.test.mixin.*

@TestFor(StockINController)
@Mock(StockIN)
class StockINControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/stockIN/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.stockINInstanceList.size() == 0
        assert model.stockINInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.stockINInstance != null
    }

    void testSave() {
        controller.save()

        assert model.stockINInstance != null
        assert view == '/stockIN/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/stockIN/show/1'
        assert controller.flash.message != null
        assert StockIN.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/stockIN/list'

        populateValidParams(params)
        def stockIN = new StockIN(params)

        assert stockIN.save() != null

        params.id = stockIN.id

        def model = controller.show()

        assert model.stockINInstance == stockIN
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/stockIN/list'

        populateValidParams(params)
        def stockIN = new StockIN(params)

        assert stockIN.save() != null

        params.id = stockIN.id

        def model = controller.edit()

        assert model.stockINInstance == stockIN
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/stockIN/list'

        response.reset()

        populateValidParams(params)
        def stockIN = new StockIN(params)

        assert stockIN.save() != null

        // test invalid parameters in update
        params.id = stockIN.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/stockIN/edit"
        assert model.stockINInstance != null

        stockIN.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/stockIN/show/$stockIN.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        stockIN.clearErrors()

        populateValidParams(params)
        params.id = stockIN.id
        params.version = -1
        controller.update()

        assert view == "/stockIN/edit"
        assert model.stockINInstance != null
        assert model.stockINInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/stockIN/list'

        response.reset()

        populateValidParams(params)
        def stockIN = new StockIN(params)

        assert stockIN.save() != null
        assert StockIN.count() == 1

        params.id = stockIN.id

        controller.delete()

        assert StockIN.count() == 0
        assert StockIN.get(stockIN.id) == null
        assert response.redirectedUrl == '/stockIN/list'
    }

    void testUpdateField() {
        fail "Implement me"
    }

    void testMassDelete() {
        fail "Implement me"
    }
}
