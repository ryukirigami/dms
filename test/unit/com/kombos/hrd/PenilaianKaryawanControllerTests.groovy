package com.kombos.hrd



import org.junit.*
import grails.test.mixin.*

@TestFor(PenilaianKaryawanController)
@Mock(PenilaianKaryawan)
class PenilaianKaryawanControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/penilaianKaryawan/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.penilaianKaryawanInstanceList.size() == 0
        assert model.penilaianKaryawanInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.penilaianKaryawanInstance != null
    }

    void testSave() {
        controller.save()

        assert model.penilaianKaryawanInstance != null
        assert view == '/penilaianKaryawan/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/penilaianKaryawan/show/1'
        assert controller.flash.message != null
        assert PenilaianKaryawan.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/penilaianKaryawan/list'

        populateValidParams(params)
        def penilaianKaryawan = new PenilaianKaryawan(params)

        assert penilaianKaryawan.save() != null

        params.id = penilaianKaryawan.id

        def model = controller.show()

        assert model.penilaianKaryawanInstance == penilaianKaryawan
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/penilaianKaryawan/list'

        populateValidParams(params)
        def penilaianKaryawan = new PenilaianKaryawan(params)

        assert penilaianKaryawan.save() != null

        params.id = penilaianKaryawan.id

        def model = controller.edit()

        assert model.penilaianKaryawanInstance == penilaianKaryawan
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/penilaianKaryawan/list'

        response.reset()

        populateValidParams(params)
        def penilaianKaryawan = new PenilaianKaryawan(params)

        assert penilaianKaryawan.save() != null

        // test invalid parameters in update
        params.id = penilaianKaryawan.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/penilaianKaryawan/edit"
        assert model.penilaianKaryawanInstance != null

        penilaianKaryawan.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/penilaianKaryawan/show/$penilaianKaryawan.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        penilaianKaryawan.clearErrors()

        populateValidParams(params)
        params.id = penilaianKaryawan.id
        params.version = -1
        controller.update()

        assert view == "/penilaianKaryawan/edit"
        assert model.penilaianKaryawanInstance != null
        assert model.penilaianKaryawanInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/penilaianKaryawan/list'

        response.reset()

        populateValidParams(params)
        def penilaianKaryawan = new PenilaianKaryawan(params)

        assert penilaianKaryawan.save() != null
        assert PenilaianKaryawan.count() == 1

        params.id = penilaianKaryawan.id

        controller.delete()

        assert PenilaianKaryawan.count() == 0
        assert PenilaianKaryawan.get(penilaianKaryawan.id) == null
        assert response.redirectedUrl == '/penilaianKaryawan/list'
    }
	
	void testUpdateField() {
		fail "Implement me"
	 }
	
	void testMassDelete() {
		fail "Implement me"
	 }
}
