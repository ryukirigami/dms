package com.kombos.example



import org.junit.*
import grails.test.mixin.*

@TestFor(ContohController)
@Mock(Contoh)
class ContohControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/contoh/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.contohInstanceList.size() == 0
        assert model.contohInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.contohInstance != null
    }

    void testSave() {
        controller.save()

        assert model.contohInstance != null
        assert view == '/contoh/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/contoh/show/1'
        assert controller.flash.message != null
        assert Contoh.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/contoh/list'

        populateValidParams(params)
        def contoh = new Contoh(params)

        assert contoh.save() != null

        params.id = contoh.id

        def model = controller.show()

        assert model.contohInstance == contoh
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/contoh/list'

        populateValidParams(params)
        def contoh = new Contoh(params)

        assert contoh.save() != null

        params.id = contoh.id

        def model = controller.edit()

        assert model.contohInstance == contoh
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/contoh/list'

        response.reset()

        populateValidParams(params)
        def contoh = new Contoh(params)

        assert contoh.save() != null

        // test invalid parameters in update
        params.id = contoh.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/contoh/edit"
        assert model.contohInstance != null

        contoh.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/contoh/show/$contoh.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        contoh.clearErrors()

        populateValidParams(params)
        params.id = contoh.id
        params.version = -1
        controller.update()

        assert view == "/contoh/edit"
        assert model.contohInstance != null
        assert model.contohInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/contoh/list'

        response.reset()

        populateValidParams(params)
        def contoh = new Contoh(params)

        assert contoh.save() != null
        assert Contoh.count() == 1

        params.id = contoh.id

        controller.delete()

        assert Contoh.count() == 0
        assert Contoh.get(contoh.id) == null
        assert response.redirectedUrl == '/contoh/list'
    }
	
	void testUpdateField() {
		fail "Implement me"
	 }
	
	void testMassDelete() {
		fail "Implement me"
	 }
}
