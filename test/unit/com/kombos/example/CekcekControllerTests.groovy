package com.kombos.example



import org.junit.*
import grails.test.mixin.*

@TestFor(CekcekController)
@Mock(Cekcek)
class CekcekControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/cekcek/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.cekcekInstanceList.size() == 0
        assert model.cekcekInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.cekcekInstance != null
    }

    void testSave() {
        controller.save()

        assert model.cekcekInstance != null
        assert view == '/cekcek/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/cekcek/show/1'
        assert controller.flash.message != null
        assert Cekcek.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/cekcek/list'

        populateValidParams(params)
        def cekcek = new Cekcek(params)

        assert cekcek.save() != null

        params.id = cekcek.id

        def model = controller.show()

        assert model.cekcekInstance == cekcek
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/cekcek/list'

        populateValidParams(params)
        def cekcek = new Cekcek(params)

        assert cekcek.save() != null

        params.id = cekcek.id

        def model = controller.edit()

        assert model.cekcekInstance == cekcek
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/cekcek/list'

        response.reset()

        populateValidParams(params)
        def cekcek = new Cekcek(params)

        assert cekcek.save() != null

        // test invalid parameters in update
        params.id = cekcek.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/cekcek/edit"
        assert model.cekcekInstance != null

        cekcek.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/cekcek/show/$cekcek.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        cekcek.clearErrors()

        populateValidParams(params)
        params.id = cekcek.id
        params.version = -1
        controller.update()

        assert view == "/cekcek/edit"
        assert model.cekcekInstance != null
        assert model.cekcekInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/cekcek/list'

        response.reset()

        populateValidParams(params)
        def cekcek = new Cekcek(params)

        assert cekcek.save() != null
        assert Cekcek.count() == 1

        params.id = cekcek.id

        controller.delete()

        assert Cekcek.count() == 0
        assert Cekcek.get(cekcek.id) == null
        assert response.redirectedUrl == '/cekcek/list'
    }
	
	void testUpdateField() {
		fail "Implement me"
	 }
	
	void testMassDelete() {
		fail "Implement me"
	 }
}
