package com.kombos.example



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Contoh)
class ContohTests {

    void testSomething() {
       fail "Implement me"
    }
}
