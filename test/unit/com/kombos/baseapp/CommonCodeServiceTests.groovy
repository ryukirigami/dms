package com.kombos.baseapp


import com.kombos.baseapp.utils.GridUtilService
//import com.kombos.baseapp.CommonCode;
//import com.kombos.baseapp.CommonCodeService;

import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(CommonCodeService)
@Mock([CommonCode, GridUtilService])
class CommonCodeServiceTests {

    void testGetList() {
        def rowList = [new CommonCode(key: 'value', value: 'value')]
        def serviceStub = mockFor(GridUtilService)
        def params = [page: 1]
        serviceStub.demand.getDomainList {Class c, Map map -> [rows: rowList, totalRecords: rowList.size(), page: 1, totalPage: 1]}
        service.gridUtilService = serviceStub.createMock()
        service.getList(params)
    }

    void testAdd() {
        def params = [key: 'value', value: 'value']
        service.add(params)
        assert CommonCode.count() == 1
    }

    void testEdit() {
        mockDomain(CommonCode, [[key: 'value', value: 'value']])
        def params = [key: 'value', value: 'value']
        service.edit(params)
        assert CommonCode.count() == 1
        assert CommonCode.findByKey('value').value == 'value'
    }

    void testDelete() {
        mockDomain(CommonCode, [[key: 'value', value: 'value']])
        def params = [key: 'value', value: 'value']
        service.delete(params)
        assert CommonCode.count() == 0
    }

}
