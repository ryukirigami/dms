package com.kombos.baseapp.sec.shiro

//import com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfo;
//import com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfoController;

import grails.test.mixin.Mock
import grails.test.mixin.TestFor


@TestFor(UserAdditionalColumnInfoController)
@Mock(UserAdditionalColumnInfo)
class UserAdditionalColumnInfoControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/userAdditionalColumnInfo/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.userAdditionalColumnInfoInstanceList.size() == 0
        assert model.userAdditionalColumnInfoInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.userAdditionalColumnInfoInstance != null
    }

    void testSave() {
        controller.save()

        assert model.userAdditionalColumnInfoInstance != null
        assert view == '/userAdditionalColumnInfo/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/userAdditionalColumnInfo/show/1'
        assert controller.flash.message != null
        assert UserAdditionalColumnInfo.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/userAdditionalColumnInfo/list'

        populateValidParams(params)
        def userAdditionalColumnInfo = new UserAdditionalColumnInfo(params)

        assert userAdditionalColumnInfo.save() != null

        params.id = userAdditionalColumnInfo.id

        def model = controller.show()

        assert model.userAdditionalColumnInfoInstance == userAdditionalColumnInfo
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/userAdditionalColumnInfo/list'

        populateValidParams(params)
        def userAdditionalColumnInfo = new UserAdditionalColumnInfo(params)

        assert userAdditionalColumnInfo.save() != null

        params.id = userAdditionalColumnInfo.id

        def model = controller.edit()

        assert model.userAdditionalColumnInfoInstance == userAdditionalColumnInfo
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/userAdditionalColumnInfo/list'

        response.reset()

        populateValidParams(params)
        def userAdditionalColumnInfo = new UserAdditionalColumnInfo(params)

        assert userAdditionalColumnInfo.save() != null

        // test invalid parameters in update
        params.id = userAdditionalColumnInfo.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/userAdditionalColumnInfo/edit"
        assert model.userAdditionalColumnInfoInstance != null

        userAdditionalColumnInfo.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/userAdditionalColumnInfo/show/$userAdditionalColumnInfo.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        userAdditionalColumnInfo.clearErrors()

        populateValidParams(params)
        params.id = userAdditionalColumnInfo.id
        params.version = -1
        controller.update()

        assert view == "/userAdditionalColumnInfo/edit"
        assert model.userAdditionalColumnInfoInstance != null
        assert model.userAdditionalColumnInfoInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/userAdditionalColumnInfo/list'

        response.reset()

        populateValidParams(params)
        def userAdditionalColumnInfo = new UserAdditionalColumnInfo(params)

        assert userAdditionalColumnInfo.save() != null
        assert UserAdditionalColumnInfo.count() == 1

        params.id = userAdditionalColumnInfo.id

        controller.delete()

        assert UserAdditionalColumnInfo.count() == 0
        assert UserAdditionalColumnInfo.get(userAdditionalColumnInfo.id) == null
        assert response.redirectedUrl == '/userAdditionalColumnInfo/list'
    }
	
	void testUpdateField() {
		fail "Implement me"
	 }
	
	void testMassDelete() {
		fail "Implement me"
	 }
}
