package com.kombos.baseapp.sec.shiro


import com.kombos.baseapp.utils.GridUtilService
//import com.kombos.baseapp.sec.shiro.Role;
//import com.kombos.baseapp.sec.shiro.RoleService;

import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(RoleService)
@Mock([Role,GridUtilService])
class RoleServiceTests {

    void testGetList() {
        def rowList = [new Role(name: 'value', permissions: 'value', )]
        def serviceStub = mockFor(GridUtilService)
        def params = [page: 1]
        serviceStub.demand.getDomainList {Class c, Map map -> [rows: rowList, totalRecords: rowList.size(), page:1, totalPage: 1]}
        service.gridUtilService = serviceStub.createMock()
        service.getList(params)
    }

    void testAdd() {
        def params = [name: 'value', permissions: ['*:*'], ]
        service.add(params)
        assert Role.count() == 1
    }

    void testEdit() {
        mockDomain(Role, [[name: 'value', permissions: ['*:*'], ]])
        def params = [name: 'value', permissions: ['*:*'], ]
        service.edit(params)
        assert Role.count() == 1
        assert Role.findByName('value').name == 'value'
    }

    void testDelete() {
        mockDomain(Role, [[name: 'value', permissions: ['*:*'], ]])
        def params = [name: 'value', permissions: ['*:*'], ]
        service.delete(params)
        assert Role.count() == 0
    }
}
