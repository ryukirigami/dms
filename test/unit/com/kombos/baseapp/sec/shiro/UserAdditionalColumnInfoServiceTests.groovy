package com.kombos.baseapp.sec.shiro


import com.kombos.baseapp.utils.GridUtilService
//import com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfo;
//import com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfoService;

import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(UserAdditionalColumnInfoService)
@Mock([UserAdditionalColumnInfo, GridUtilService])
class UserAdditionalColumnInfoServiceTests {

    void testGetList() {
        def rowList = [new UserAdditionalColumnInfo(columnName: 'value', label: 'value')]
        def serviceStub = mockFor(GridUtilService)
        def params = [page: 1]
        serviceStub.demand.getDomainList {Class c, Map map -> [rows: rowList, totalRecords: rowList.size(), page: 1, totalPage: 1]}
        service.gridUtilService = serviceStub.createMock()
        service.getList(params)
    }

    void testAdd() {
        def params = [columnName: 'value', label: 'value']
        service.add(params)
        assert UserAdditionalColumnInfo.count() == 1
    }

    void testEdit() {
        mockDomain(UserAdditionalColumnInfo, [[columnName: 'value', label: 'value']])
        def params = [columnName: 'value', label: 'value']
        service.edit(params)
        assert UserAdditionalColumnInfo.count() == 1
        assert UserAdditionalColumnInfo.findByColumnName('value').columnName == 'value'
    }

    void testDelete() {
        mockDomain(UserAdditionalColumnInfo, [[columnName: 'value', label: 'value']])
        def params = [columnName: 'value', label: 'value']
        service.delete(params)
        assert UserAdditionalColumnInfo.count() == 0
    }

}
