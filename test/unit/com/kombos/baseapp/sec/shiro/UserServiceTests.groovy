package com.kombos.baseapp.sec.shiro



import grails.test.mixin.*
import org.junit.*

import com.kombos.baseapp.sec.shiro.UserService;

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(UserService)
class UserServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
