package com.kombos.baseapp.sec.shiro

//import com.kombos.baseapp.sec.shiro.UserAdditionalColumnController;
//import com.kombos.baseapp.sec.shiro.UserAdditionalColumnService;

import grails.test.mixin.TestFor

@TestFor(UserAdditionalColumnController)
class UserAdditionalColumnControllerTests {

    void testIndex() {
        controller.index()
        assert view == null
    }

    void testList() {
        def serviceStub = mockFor(UserAdditionalColumnController)
        def rowList = [[id: 0, cell: ["mock_value"]], [id: 1, cell: ["mock_value"]]]
        serviceStub.demand.getList {Map map -> [rows: rowList, totalRecords: rowList.size(), totalPage: 1]}

        params.page = 1
        controller.userAdditionalColumnService = serviceStub.createMock()
        controller.getList()

        assert response.json.rows.collect().size() == rowList.size()
        assert response.json.records == rowList.size()
        assert response.json.page == 1
        assert response.json.total == 1
    }

    void testAdd() {
        params.oper = 'add'
        def serviceStub = mockFor(UserAdditionalColumnService)
        serviceStub.demand.add(params) {->}
        controller.userAdditionalColumnService = serviceStub.createMock()
        controller.edit()
        assert response.json.message == 'Add Success'
    }

    void testEdit() {
        params.oper = 'edit'
        def serviceStub = mockFor(UserAdditionalColumnService)
        serviceStub.demand.edit(params) {->}
        controller.userAdditionalColumnService = serviceStub.createMock()
        controller.edit()
        assert response.json.message == 'Edit Success'
    }

    void testDelete() {
        params.oper = 'del'
        def serviceStub = mockFor(UserAdditionalColumnService)
        serviceStub.demand.delete(params) {->}
        controller.userAdditionalColumnService = serviceStub.createMock()
        controller.edit()
        assert response.json.message == 'Delete Success'
    }

}
