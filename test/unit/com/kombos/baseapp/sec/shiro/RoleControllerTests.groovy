package com.kombos.baseapp.sec.shiro

//import com.kombos.baseapp.sec.shiro.RoleController;
//import com.kombos.baseapp.sec.shiro.RoleService;

import grails.test.mixin.TestFor

@TestFor(RoleController)
class RoleControllerTests {

    void testIndex () {
        controller.index ()
        assert view == null
    }

    void testList() {
        def serviceStub = mockFor(RoleController)
        def rowList = [[id:0,cell:["mock_value"]],[id:1,cell:["mock_value"]]]
        serviceStub.demand.getList {Map map -> [rows: rowList, totalRecords: rowList.size(), totalPage: 1]}

        params.page = 1
        controller.roleService = serviceStub.createMock()
        controller.getList()

        assert response.json.rows.collect().size() == rowList.size()
        assert response.json.records == rowList.size()
        assert response.json.page == 1
        assert response.json.total == 1
    }

    void testAdd() {
        params.oper = 'add'
        def serviceStub = mockFor(RoleService)
        serviceStub.demand.add(params) {->}
        controller.roleService = serviceStub.createMock()
        controller.edit()
        assert response.json.message == 'Add Success'
    }

    void testEdit() {
        params.oper = 'edit'
        def serviceStub = mockFor(RoleService)
        serviceStub.demand.edit(params) {->}
        controller.roleService = serviceStub.createMock()
        controller.edit()
        assert response.json.message == 'Edit Success'
    }

    void testDelete() {
        params.oper = 'del'
        def serviceStub = mockFor(RoleService)
        serviceStub.demand.delete(params) {->}
        controller.roleService = serviceStub.createMock()
        controller.edit()
        assert response.json.message == 'Delete Success'
    }

}
