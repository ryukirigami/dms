package com.kombos.baseapp


import com.kombos.baseapp.utils.GridUtilService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import com.kombos.baseapp.utils.SecurityChecklistUtilService
//import com.kombos.baseapp.SecurityChecklist;
//import com.kombos.baseapp.SecurityChecklistService;

@TestFor(SecurityChecklistService)
@Mock([SecurityChecklist, GridUtilService])
class SecurityChecklistServiceTests {
    void testGetList() {
        def rowList = [new SecurityChecklist(approvalStatus: 'value', category: 'value', code: 'value', defaultValue: 'value', enable: 'value', label: 'value', type: 'value', value: 'value')]
        def serviceStub = mockFor(GridUtilService)
        def params = [page: 1]
        serviceStub.demand.getDomainList {Class c, Map map -> [rows: rowList, totalRecords: rowList.size(), page: 1, totalPage: 1]}
        service.gridUtilService = serviceStub.createMock()
        service.getList(params)
    }

    void testAdd() {
        def params = [approvalStatus: 'value', category: 'value', code: 'value', defaultValue: 'value', enable: 'value', label: 'value', type: 'value', value: 'value']
        service.add(params)
        assert SecurityChecklist.count() == 1
    }

    void testEdit() {
        mockDomain(SecurityChecklist, [[approvalStatus: 'value', category: 'value', code: 'value', defaultValue: 'value', enable: 'value', label: 'value', type: 'value', value: 'value']])
        def params = [approvalStatus: 'value', category: 'value', code: 'value', defaultValue: 'value', enable: 'value', label: 'value', type: 'value', value: 'value']
        service.edit(params)
        assert SecurityChecklist.count() == 1
        assert SecurityChecklist.findByCode('value').value == 'value'
    }

    void testDelete() {
        mockDomain(SecurityChecklist, [[approvalStatus: 'value', category: 'value', code: 'value', defaultValue: 'value', enable: 'value', label: 'value', type: 'value', value: 'value']])
        def params = [approvalStatus: 'value', category: 'value', code: 'value', defaultValue: 'value', enable: 'value', label: 'value', type: 'value', value: 'value']
        service.delete(params)
        assert SecurityChecklist.count() == 0
    }

    void testCheckPassword(){
        String password = "P@ssword 468"

        List<SecurityChecklist> children = new ArrayList<SecurityChecklist>()
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_NO_WHITESPACE, label:'No Whitespace', defaultValue: 'Yes', type: SecurityChecklist.TYPE[3], enable: false).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_NO_ALPHABETICAL_SEQUENCE, label:'No Alphabetical Sequence', defaultValue: 'Yes', type: SecurityChecklist.TYPE[3], enable: true).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_NO_QWERTY_SEQUENCE, label:'No Qwert Sequence', defaultValue: 'Yes', type: SecurityChecklist.TYPE[3], enable: true).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_NUMERICAL_SEQUENCE_RULE, label:'Numerical Sequence Rule', defaultValue: '3', type: SecurityChecklist.TYPE[0], enable: true).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_REPEAT_CHARACTER_RULE, label:'Repeat Character Rule', defaultValue: '3', type: SecurityChecklist.TYPE[0], enable: true).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_DIGIT_CHARACTER_RULE, label:'Digit Character Rule', defaultValue: '3', type: SecurityChecklist.TYPE[0], enable: true).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_NON_ALPHANUMERIC_CHARACTER_RULE, label:'Non Alphanumeric Character Rule', defaultValue: '1', type: SecurityChecklist.TYPE[0], enable: true).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_UPPER_CASE_CHARACTER_RULE, label:'Upper Case Character Rule', defaultValue: '1', type: SecurityChecklist.TYPE[0], enable: true).save())
        children.add(new SecurityChecklist(category:'authentication', code:SecurityChecklistUtilService.CODE_LOWER_CASE_CHARACTER_RULE, label:'Lowe Case Character Rule', defaultValue: '1', type: SecurityChecklist.TYPE[0], enable: true).save())

        mockDomain(SecurityChecklist, [[category:'authentication', code:SecurityChecklistUtilService.CODE_PREVENTING_GUESSABLE_PASSWORD, label:'Preventing Guessable Password', defaultValue: 'Yes', type: SecurityChecklist.TYPE[3], enable: true, children : children]])

        SecurityChecklistUtilService securityChecklistUtilService = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PREVENTING_GUESSABLE_PASSWORD)
        HashMap mapResult = securityChecklistUtilService.validatePreventingGuessablePassword(password)
        if (mapResult.get("isValid")) {
            System.out.println("Valid password");
        } else {
            System.out.println("Invalid password:");
            for (String msg : mapResult.get("message")) {
                System.out.println(msg);
            }
        }
        assert false
    }
}
