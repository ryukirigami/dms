package com.kombos.baseapp


import com.kombos.baseapp.utils.GridUtilService
//import com.kombos.baseapp.AppSettingParam;
//import com.kombos.baseapp.AppSettingParamService;

import grails.test.mixin.Mock
import grails.test.mixin.TestFor

@TestFor(AppSettingParamService)
@Mock([AppSettingParam,GridUtilService])
class AppSettingParamServiceTests {

    void testGetList() {
        def rowList = [new AppSettingParam(approvalStatus: 'value', category: 'value', code: 'value', label: 'value', value: 'value')]
        def serviceStub = mockFor(GridUtilService)
        def params = [page: 1]
        serviceStub.demand.getDomainList {Class c, Map map -> [rows: rowList, totalRecords: rowList.size(), page:1, totalPage: 1]}
        service.gridUtilService = serviceStub.createMock()
        service.getList(params)
    }

    void testAdd() {
        def params = [approvalStatus: 'value', category: 'value', code: 'value', label: 'value', value: 'value']
        service.add(params)
        assert AppSettingParam.count() == 1
    }

    void testEdit() {
        mockDomain(AppSettingParam, [[approvalStatus: 'value', category: 'value', code: 'value', label: 'value', value: 'value']])
        def params = [approvalStatus: 'value', category: 'value', code: 'value', label: 'value', value: 'value']
        service.edit(params)
        assert AppSettingParam.count() == 1
        assert AppSettingParam.findByCode('value').value == 'value'
    }

    void testDelete() {
        mockDomain(AppSettingParam, [[approvalStatus: 'value', category: 'value', code: 'value', label: 'value', value: 'value']])
        def params = [approvalStatus: 'value', category: 'value', code: 'value', label: 'value', value: 'value']
        service.delete(params)
        assert AppSettingParam.count() == 0
    }

}
