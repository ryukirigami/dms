package com.kombos.customerprofile

import com.kombos.customerprofile.HistoryCustomerVehicleController
import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(HistoryCustomerVehicleController)
class HistoryCustomerVehicleControllerTests {

    void testSomething() {
       fail "Implement me"
    }
}
