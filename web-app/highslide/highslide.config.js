/**
*	Site-specific configuration settings for Highslide JS
*/
hs.graphicsDir = 'highslide/graphics/';
hs.creditsPosition = 'bottom left';
hs.outlineType = 'custom';
hs.dimmingOpacity = 0.5;
hs.align = 'center';
hs.captionEval = 'this.a.title';
hs.registerOverlay({
	html: '<div class="closebutton" onclick="return hs.close(this)" title="Close"></div>',
	position: 'top right',
	useOnHtml: true,
	fade: 2 // fading the semi-transparent overlay looks bad in IE
});
//hs.forceAjaxReload = true;
hs.cacheAjax = false;
