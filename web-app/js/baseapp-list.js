	var showInstance;
	var loadFormInstance;
	var editInstance;
	var shrinkTableLayout;
	var expandTableLayout;
	$(function(){ 
	
	$("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	
	showInstance = function(module, url) {
    	shrinkTableLayout(module);
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'GET', url:url,
   			success:function(data,textStatus){
   				loadFormInstance(module, data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    editInstance = function(module,url) {
    	shrinkTableLayout(module);
       	$('#spinner').fadeIn(1);
   		$.ajax({type:'GET', url: url,
   			success:function(data,textStatus){
   				loadFormInstance(module, data, textStatus);
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
    };
    
    loadFormInstance = function(module, data, textStatus){
    	$('#'+module+'-form').html(data);
   	}
    
    shrinkTableLayout = function(module){
		var table = $('#'+module+'-table');
		if(table.hasClass("span12")){
   			table.toggleClass("span12 span5");
        }
        $('#'+module+'-form').css("display","block"); 
   	}
   	
   	expandTableLayout = function(module){
		var table = $('#'+module+'-table');
   		if(table.hasClass("span5")){
   			table.toggleClass("span5 span12");
   		}
        $('#'+module+'-form').css("display","none").empty();
   	}
   	
   	massDelete = function(module, url, callback) {
   		var recordsToDelete = [];
		$('#'+module+'-table tbody .row-select').each(function() {
			if(this.checked){
    			var id = $(this).next("input:hidden").val();
    			recordsToDelete.push(id);
    		}
		});
		
		var json = JSON.stringify(recordsToDelete);
		
		$.ajax({
    		url:url,      
    		type: "POST", // Always use POST when deleting data
    		data: { ids: json },
    		complete: function(xhr, status) {
        		callback();
    		}
		});
		
   	}

});