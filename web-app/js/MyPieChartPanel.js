Ext.define('My.app.PieChartPanel', {
    extend: 'Ext.panel.Panel',
    title: 'Severity',
    layout : 'fit',
    initComponent: function (config) {
        this.items =[this.buildChart()];
        Ext.apply(this, config);
        this.callParent(arguments);

    },

    buildChart : function(){
        var store = Ext.create('Ext.data.JsonStore', {
            fields: ['name', 'data', 'symbol', 'matchValue'],
            /*data: [
                { 'name': 'metric one',   'data': 10 },
                { 'name': 'metric two',   'data':  7 },
                { 'name': 'metric three', 'data':  5 },
                { 'name': 'metric four',  'data':  2 },
                { 'name': 'metric five',  'data': 27 }
            ]*/
            proxy: {
                type: 'ajax',
                url: MyApp.config.pieChartDataURL,
                reader: {
                    type: 'json',
                    root: 'rows',
                    idProperty: 'name'
                }
            }
        });

        var chart = Ext.create('Ext.chart.Chart', {
            id: 'myPie',
//            width: 500,
//            height: 350,
            animate: true,
            store: store,
            theme: 'Base:gradients',
            legend: {
                position: 'right'
            },
            series: [{
                type: 'pie',
                angleField: 'data',
                showInLegend: true,
                tips: {
                    trackMouse: true,
                    width: 400,
                    height: 40,
                    renderer: function(storeItem, item) {
                        // calculate and display percentage on hover
                        var total = 0;
                        store.each(function(rec) {
                            total += rec.get('data');
                        });
                        var title = storeItem.get('name')+" "+storeItem.get('symbol')+" "+storeItem.get('matchValue')+" ("+Math.round(storeItem.get('data') / total * 100)+"%)";
                        this.setTitle(title);
                        //this.setTitle(storeItem.get('name') + ' : ' + storeItem.get('data') + ' matching data (' + Math.round(storeItem.get('data') / total * 100) + '%)');
                    }
                },
                highlight: {
                    segment: {
                        margin: 20
                    }
                },
                label: {
                    field: 'name',
                    display: 'rotate',
                    contrast: true,
                    font: '12px Arial'
                }
            }]
        });

        return chart;

    }

});