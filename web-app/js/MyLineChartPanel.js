Ext.define('My.app.LineChartPanel', {
    extend: 'Ext.panel.Panel',
    title : 'Execution Chart',
    layout : 'fit',
    initComponent: function (config) {
        this.items =[this.buildChart()];
        Ext.apply(this, config);
        this.callParent(arguments);

    },

    buildChart : function(){
        Ext.define('LineModel', {
            extend: 'Ext.data.Model',
            fields: MyApp.config.buildLineChartFields(/*xRuleLevels, yRuleLevels, */this)
        });

        var store = new Ext.data.JsonStore({
            // store configs
            storeId: 'lineStore',
            proxy: {
                type: 'ajax',
                url: MyApp.config.lineChartDataURL,
                reader: {
                    type: 'json',
                    root: 'rows',
                    idProperty: 'id'
                }
            },
            //alternatively, a Ext.data.Model name can be given (see Ext.data.Store for an example)
            model : 'LineModel',
            listeners: {
                exception: function(proxy, response, operation){
                    Ext.MessageBox.show({
                        title: 'ERROR',
                        msg: operation.getError(),
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }
            }
        });

        /*store.load({
            scope: this,
            callback: function(records, operation, success) {
            }
        });*/

        return Ext.create('Ext.chart.Chart', {
            theme: 'Red',
            animate: true,
            store: store,
            id:'myLine',
            legend: {
                position: 'right'
            },
            axes: [
                {
                    title: 'Match %',
                    type: 'Numeric',
                    position: 'left',
                    fields: MyApp.config.getRuleLevels('y') ,
                    minimum: 0,
                    maximum: 100
                },
                {
                    title: 'Execution',
                    type: 'Category',
                    position: 'bottom',
                    fields: ['execution'],//getRuleLevels('x'),
                    grid: true
                }
            ],
            series: MyApp.config.buildLineChartSeries(/*xRuleLevels, yRuleLevels, */this)
        });

    },

    itemMouseUp : function(obj){
        var pieStore = Ext.getCmp('myPie').getStore();

        pieStore.load({
            params: {
                jobSumID: obj.storeItem.data.id,
                //data:JSON.stringify(Object.keys(obj.series)),
                levelName:obj.series.yField,
                xdata: obj.value[0],
                ydata: obj.value[1]
            }
        });
    },

    tipsRenderrer : function(storeItem, item) {
        //this.setTitle(storeItem.get('name') + ': ' + storeItem.get('data1') + ' views');
        var text = item.series.yField+", x:"+item.value[0]+", y:"+item.value[1];
        this.setTitle(text);
    }

});