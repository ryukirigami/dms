/**
 * Created by Nurul Ifan Purba on 20/05/14.
 */


function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isDecimalKey(evt)  //isNumberkey + .
{
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode == 46) return true;
    else if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function setValLeadTimeSupply(val,valEdit)
{
    var option="<option value=''></option>";
    $('#m112JamLamaDatang2').html(option);
    if(val =='Realtime/Emergency'){
        for (var i=1;i<=7;i++)
        {
            if(valEdit==i){
                option = option+"<option value='"+i+"' selected='selected'>"+i+"</option>";
            }else{
                option = option+"<option value='"+i+"'>"+i+"</option>";
            }
        }
        $('#m112JamLamaDatang1').html(option);
    }else if(val=='Batch Routine/Reguler'){
        for (var i=8;i<=14;i++)
        {
            if(valEdit==i){
                option = option+"<option value='"+i+"' selected='selected'>"+i+"</option>";
            }else{
                option = option+"<option value='"+i+"'>"+i+"</option>";
            }
        }
        $('#m112JamLamaDatang1').html(option);
    }else if(val=='Booking'){
        for (var i=15;i<=99;i++)
        {
            if(valEdit==i){
                option = option+"<option value='"+i+"' selected='selected'>"+i+"</option>";
            }else{
                option = option+"<option value='"+i+"'>"+i+"</option>";
            }
        }
        $('#m112JamLamaDatang1').html(option);
    }
}

function setLeadTimeSupply2(val,valEdit)
{
    var option2="";
    if(val <= 7){
        val = parseInt(val) + 1;
        for (var i=val;i<=7;i++)
        {
            if(valEdit==i){
                option2 = option2+"<option value='"+i+"' selected='selected'>"+i+"</option>";
            }else{
                option2 = option2+"<option value='"+i+"'>"+i+"</option>";
            }
        }
        $('#m112JamLamaDatang2').html(option2);

    }else if(val<=14){
        val = parseInt(val) + 1;
        for (var i=val;i<=14;i++)
        {
            if(valEdit==i){
                option2 = option2+"<option value='"+i+"' selected='selected'>"+i+"</option>";
            }else{
                option2 = option2+"<option value='"+i+"'>"+i+"</option>";
            }
        }
        $('#m112JamLamaDatang2').html(option2);

    }else if(val<=99){
        val = parseInt(val) + 1;
        for (var i=val;i<=99;i++)
        {
            if(valEdit==i){
                option2 = option2+"<option value='"+i+"' selected='selected'>"+i+"</option>";
            }else{
                option2 = option2+"<option value='"+i+"'>"+i+"</option>";
            }
        }
        $('#m112JamLamaDatang2').html(option2);
    }
}

$(document).ready(function()
{
    $('#m112CutoffTime1_check').click(function(){
        if($('#m112CutoffTime1_check').is(':checked')){
            $('#m112CutoffTime2_check').prop('disabled', false);
            $("#m112CutoffTime1_hour").prop('disabled', false);
            $("#m112CutoffTime1_minute").prop('disabled', false);
        }else{
            $('#m112CutoffTime2_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime3_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime4_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime5_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime6_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime7_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime8_check').prop({checked:false, disabled:true});
            $("#m112CutoffTime1_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime1_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime2_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime2_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime3_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime3_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime4_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime4_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime5_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime5_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime6_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime6_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime7_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime7_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    $('#m112CutoffTime2_check').click(function(){
        if($('#m112CutoffTime2_check').is(':checked')){
            $('#m112CutoffTime3_check').prop('disabled', false);
            $("#m112CutoffTime2_hour").val(parseInt($("#m112CutoffTime1_hour").val())).prop('disabled', false);
            $("#m112CutoffTime2_minute").val(parseInt($('#m112CutoffTime1_minute').val())+1).prop('disabled', false);
            if(parseInt($('#m112CutoffTime2_hour').val()) == parseInt($('#m112CutoffTime1_hour').val()) && parseInt($('#m112CutoffTime1_minute').val()) == '59' ){
                $("#m112CutoffTime2_hour").val(parseInt($('#m112CutoffTime1_hour').val())+1);
                $('#m112CutoffTime2_minute').val('00');
            }
        }else{
            $('#m112CutoffTime3_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime4_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime5_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime6_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime7_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime8_check').prop({checked:false, disabled:true});
            $("#m112CutoffTime2_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime2_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime3_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime3_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime4_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime4_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime5_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime5_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime6_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime6_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime7_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime7_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    $('#m112CutoffTime3_check').click(function(){
        if($('#m112CutoffTime3_check').is(':checked')){
            $('#m112CutoffTime4_check').prop('disabled', false);
            $("#m112CutoffTime3_hour").val(parseInt($("#m112CutoffTime2_hour").val())).prop('disabled', false);
            $("#m112CutoffTime3_minute").val(parseInt($('#m112CutoffTime2_minute').val())+1).prop('disabled', false);
            if(parseInt($('#m112CutoffTime3_hour').val()) == parseInt($('#m112CutoffTime2_hour').val()) && parseInt($('#m112CutoffTime2_minute').val()) == '59' ){
                $("#m112CutoffTime3_hour").val(parseInt($('#m112CutoffTime2_hour').val())+1);
                $('#m112CutoffTime3_minute').val('00');
            }
        }else{
            $('#m112CutoffTime4_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime5_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime6_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime7_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime8_check').prop({checked:false, disabled:true});
            $("#m112CutoffTime3_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime3_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime4_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime4_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime5_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime5_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime6_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime6_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime7_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime7_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    $('#m112CutoffTime4_check').click(function(){
        if($('#m112CutoffTime4_check').is(':checked')){
            $('#m112CutoffTime5_check').prop('disabled', false);
            $("#m112CutoffTime4_hour").val(parseInt($("#m112CutoffTime3_hour").val())).prop('disabled', false);
            $("#m112CutoffTime4_minute").val(parseInt($('#m112CutoffTime3_minute').val())+1).prop('disabled', false);
            if(parseInt($('#m112CutoffTime4_hour').val()) == parseInt($('#m112CutoffTime3_hour').val()) && parseInt($('#m112CutoffTime3_minute').val()) == '59' ){
                $("#m112CutoffTime4_hour").val(parseInt($('#m112CutoffTime3_hour').val())+1);
                $('#m112CutoffTime4_minute').val('00');
            }
        }else{
            $('#m112CutoffTime5_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime6_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime7_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime8_check').prop({checked:false, disabled:true});
            $("#m112CutoffTime4_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime4_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime5_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime5_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime6_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime6_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime7_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime7_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    $('#m112CutoffTime5_check').click(function(){
        if($('#m112CutoffTime5_check').is(':checked')){
            $('#m112CutoffTime6_check').prop('disabled', false);
            $("#m112CutoffTime5_hour").val(parseInt($("#m112CutoffTime4_hour").val())).prop('disabled', false);
            $("#m112CutoffTime5_minute").val(parseInt($('#m112CutoffTime4_minute').val())+1).prop('disabled', false);
            if(parseInt($('#m112CutoffTime5_hour').val()) == parseInt($('#m112CutoffTime4_hour').val()) && parseInt($('#m112CutoffTime4_minute').val()) == '59' ){
                $("#m112CutoffTime5_hour").val(parseInt($('#m112CutoffTime4_hour').val())+1);
                $('#m112CutoffTime5_minute').val('00');
            }
        }else{
            $('#m112CutoffTime6_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime7_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime8_check').prop({checked:false, disabled:true});
            $("#m112CutoffTime5_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime5_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime6_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime6_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime7_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime7_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    $('#m112CutoffTime6_check').click(function(){
        if($('#m112CutoffTime6_check').is(':checked')){
            $('#m112CutoffTime7_check').prop('disabled', false);
            $("#m112CutoffTime6_hour").val(parseInt($("#m112CutoffTime5_hour").val())).prop('disabled', false);
            $("#m112CutoffTime6_minute").val(parseInt($('#m112CutoffTime5_minute').val())+1).prop('disabled', false);
            if(parseInt($('#m112CutoffTime6_hour').val()) == parseInt($('#m112CutoffTime5_hour').val()) && parseInt($('#m112CutoffTime5_minute').val()) == '59' ){
                $("#m112CutoffTime6_hour").val(parseInt($('#m112CutoffTime5_hour').val())+1);
                $('#m112CutoffTime6_minute').val('00');
            }
        }else{
            $('#m112CutoffTime7_check').prop({checked:false, disabled:true});
            $('#m112CutoffTime8_check').prop({checked:false, disabled:true});
            $("#m112CutoffTime6_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime6_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime7_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime7_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    $('#m112CutoffTime7_check').click(function(){
        if($('#m112CutoffTime7_check').is(':checked')){
            $('#m112CutoffTime8_check').prop('disabled', false);
            $("#m112CutoffTime7_hour").val(parseInt($("#m112CutoffTime6_hour").val())).prop('disabled', false);
            $("#m112CutoffTime7_minute").val(parseInt($('#m112CutoffTime6_minute').val())+1).prop('disabled', false);
            if(parseInt($('#m112CutoffTime7_hour').val()) == parseInt($('#m112CutoffTime6_hour').val()) && parseInt($('#m112CutoffTime6_minute').val()) == '59' ){
                $("#m112CutoffTime7_hour").val(parseInt($('#m112CutoffTime6_hour').val())+1);
                $('#m112CutoffTime7_minute').val('00');
            }
        }else{
            $('#m112CutoffTime8_check').prop({checked:false, disabled:true});
            $("#m112CutoffTime7_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime7_minute").val("00").prop('disabled', true);
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    $('#m112CutoffTime8_check').click(function(){
        if($('#m112CutoffTime8_check').is(':checked')){
            $("#m112CutoffTime8_hour").val(parseInt($("#m112CutoffTime7_hour").val())).prop('disabled', false);
            $("#m112CutoffTime8_minute").val(parseInt($('#m112CutoffTime7_minute').val())+1).prop('disabled', false);
            if(parseInt($('#m112CutoffTime8_hour').val()) == parseInt($('#m112CutoffTime7_hour').val()) && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
        }else{
            $("#m112CutoffTime8_hour").val("00").prop('disabled', true);
            $("#m112CutoffTime8_minute").val("00").prop('disabled', true);
        }
    });

    //1
    $("#m112CutoffTime1_hour").change(function(){
        if($('#m112CutoffTime2_check').is(':checked')){
            if(parseInt($('#m112CutoffTime2_hour').val()) <= parseInt($('#m112CutoffTime1_hour').val())
                && parseInt($('#m112CutoffTime1_minute').val()) == '59' ){
                $("#m112CutoffTime2_hour").val(parseInt($('#m112CutoffTime1_hour').val())+1);
//                $('#m112CutoffTime2_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime2_hour").val()) == parseInt($("#m112CutoffTime1_hour").val())
                && parseInt($("#m112CutoffTime2_minute").val()) < parseInt($("#m112CutoffTime1_minute").val())){
                $('#m112CutoffTime2_minute').val(parseInt($('#m112CutoffTime1_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime2_hour").val()) < parseInt($("#m112CutoffTime1_hour").val())){
                $("#m112CutoffTime2_hour").val($("#m112CutoffTime1_hour").val());
                $('#m112CutoffTime2_minute').val(parseInt($('#m112CutoffTime1_minute').val())+1);
            }
        }
        if($('#m112CutoffTime3_check').is(':checked')){
            if(parseInt($('#m112CutoffTime3_hour').val()) <= parseInt($('#m112CutoffTime2_hour').val())
                && parseInt($('#m112CutoffTime2_minute').val()) == '59' ){
                $("#m112CutoffTime3_hour").val(parseInt($('#m112CutoffTime2_hour').val())+1);
//                $('#m112CutoffTime3_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) == parseInt($("#m112CutoffTime2_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) < parseInt($("#m112CutoffTime2_minute").val())){
                $('#m112CutoffTime3_minute').val(parseInt($('#m112CutoffTime2_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) < parseInt($("#m112CutoffTime2_hour").val())){
                $("#m112CutoffTime3_hour").val($("#m112CutoffTime2_hour").val());
                $('#m112CutoffTime3_minute').val(parseInt($('#m112CutoffTime2_minute').val())+1);
            }
        }
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($('#m112CutoffTime4_hour').val()) <= parseInt($('#m112CutoffTime3_hour').val())
                && parseInt($('#m112CutoffTime3_minute').val()) == '59' ){
                $("#m112CutoffTime4_hour").val(parseInt($('#m112CutoffTime3_hour').val())+1);
//                $('#m112CutoffTime4_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) < parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val(parseInt($('#m112CutoffTime3_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val());
                $('#m112CutoffTime4_minute').val(parseInt($('#m112CutoffTime3_minute').val())+1);
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($('#m112CutoffTime5_hour').val()) <= parseInt($('#m112CutoffTime4_hour').val())
                && parseInt($('#m112CutoffTime4_minute').val()) == '59' ){
                $("#m112CutoffTime5_hour").val(parseInt($('#m112CutoffTime4_hour').val())+1);
//                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) < parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($('#m112CutoffTime6_hour').val()) <= parseInt($('#m112CutoffTime5_hour').val())
                && parseInt($('#m112CutoffTime5_minute').val()) == '59' ){
                $("#m112CutoffTime6_hour").val(parseInt($('#m112CutoffTime5_hour').val())+1);
//                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) < parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($('#m112CutoffTime7_hour').val()) <= parseInt($('#m112CutoffTime6_hour').val())
                && parseInt($('#m112CutoffTime6_minute').val()) == '59' ){
                $("#m112CutoffTime7_hour").val(parseInt($('#m112CutoffTime6_hour').val())+1);
//                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($('#m112CutoffTime8_hour').val()) <= parseInt($('#m112CutoffTime7_hour').val())
                && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
//                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
        }
    });

    $("#m112CutoffTime1_minute").change(function(){
        if($('#m112CutoffTime2_check').is(':checked')){
            if(parseInt($('#m112CutoffTime2_hour').val()) == parseInt($('#m112CutoffTime1_hour').val()) && parseInt($('#m112CutoffTime1_minute').val()) == '59' ){
                $("#m112CutoffTime2_hour").val(parseInt($('#m112CutoffTime1_hour').val())+1);
                $('#m112CutoffTime2_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime2_hour").val()) == parseInt($("#m112CutoffTime1_hour").val())
                && parseInt($("#m112CutoffTime2_minute").val()) <= parseInt($("#m112CutoffTime1_minute").val())){
                $('#m112CutoffTime2_minute').val(parseInt($('#m112CutoffTime1_minute').val())+1);
            }
        }
        if($('#m112CutoffTime3_check').is(':checked')){
            if(parseInt($("#m112CutoffTime3_hour").val()) < parseInt($("#m112CutoffTime2_hour").val())){
                $("#m112CutoffTime3_hour").val($("#m112CutoffTime2_hour").val());
                $("#m112CutoffTime3_minute").val(parseInt($("#m112CutoffTime2_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) == parseInt($("#m112CutoffTime2_hour").val())
                && parseInt($("#m112CutoffTime2_minute").val()) == '59'){
                $('#m112CutoffTime3_hour').val(parseInt($("#m112CutoffTime2_hour").val())+1);
                $('#m112CutoffTime3_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) == parseInt($("#m112CutoffTime2_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) <= parseInt($("#m112CutoffTime2_minute").val())){
                $('#m112CutoffTime3_minute').val(parseInt($("#m112CutoffTime2_minute").val())+1);
            }
        }
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val());
                $("#m112CutoffTime4_minute").val(parseInt($("#m112CutoffTime3_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) == '59'){
                $('#m112CutoffTime4_hour').val(parseInt($("#m112CutoffTime3_hour").val())+1);
                $('#m112CutoffTime4_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) <= parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val(parseInt($("#m112CutoffTime3_minute").val())+1);
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $("#m112CutoffTime5_minute").val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) == '59'){
                $('#m112CutoffTime5_hour').val(parseInt($("#m112CutoffTime4_hour").val())+1);
                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) <= parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $("#m112CutoffTime6_minute").val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) == '59'){
                $('#m112CutoffTime6_hour').val(parseInt($("#m112CutoffTime5_hour").val())+1);
                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) <= parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $("#m112CutoffTime7_minute").val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) == '59'){
                $('#m112CutoffTime7_hour').val(parseInt($("#m112CutoffTime6_hour").val())+1);
                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) <= parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $("#m112CutoffTime8_minute").val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) == '59'){
                $('#m112CutoffTime8_hour').val(parseInt($("#m112CutoffTime7_hour").val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) <= parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
        }
    });

    $("#m112CutoffTime2_hour").change(function(){
        if($('#m112CutoffTime2_check').is(':checked')){
            if(parseInt($('#m112CutoffTime2_hour').val()) <= parseInt($('#m112CutoffTime1_hour').val()) && parseInt($('#m112CutoffTime1_minute').val()) == '59' ){
                $("#m112CutoffTime2_hour").val(parseInt($('#m112CutoffTime1_hour').val())+1);
//                $('#m112CutoffTime2_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime2_hour").val()) <= parseInt($("#m112CutoffTime1_hour").val())){
                $("#m112CutoffTime2_hour").val($("#m112CutoffTime1_hour").val());
                $('#m112CutoffTime2_minute').val(parseInt($('#m112CutoffTime1_minute').val())+1);
            }
        }
        if($('#m112CutoffTime3_check').is(':checked')){
            if(parseInt($('#m112CutoffTime3_hour').val()) <= parseInt($('#m112CutoffTime2_hour').val())
                && parseInt($('#m112CutoffTime2_minute').val()) == '59' ){
                $("#m112CutoffTime3_hour").val(parseInt($('#m112CutoffTime2_hour').val())+1);
//                $('#m112CutoffTime3_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) == parseInt($("#m112CutoffTime2_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) < parseInt($("#m112CutoffTime2_minute").val())){
                $('#m112CutoffTime3_minute').val(parseInt($('#m112CutoffTime2_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) < parseInt($("#m112CutoffTime2_hour").val())){
                $("#m112CutoffTime3_hour").val($("#m112CutoffTime2_hour").val());
                $('#m112CutoffTime3_minute').val(parseInt($('#m112CutoffTime2_minute').val())+1);
            }
        }
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($('#m112CutoffTime4_hour').val()) <= parseInt($('#m112CutoffTime3_hour').val())
                && parseInt($('#m112CutoffTime3_minute').val()) == '59' ){
                $("#m112CutoffTime4_hour").val(parseInt($('#m112CutoffTime3_hour').val())+1);
//                $('#m112CutoffTime4_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) < parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val(parseInt($('#m112CutoffTime3_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val());
                $('#m112CutoffTime4_minute').val(parseInt($('#m112CutoffTime3_minute').val())+1);
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($('#m112CutoffTime5_hour').val()) <= parseInt($('#m112CutoffTime4_hour').val())
                && parseInt($('#m112CutoffTime4_minute').val()) == '59' ){
                $("#m112CutoffTime5_hour").val(parseInt($('#m112CutoffTime4_hour').val())+1);
//                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) < parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($('#m112CutoffTime6_hour').val()) <= parseInt($('#m112CutoffTime5_hour').val())
                && parseInt($('#m112CutoffTime5_minute').val()) == '59' ){
                $("#m112CutoffTime6_hour").val(parseInt($('#m112CutoffTime5_hour').val())+1);
//                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) < parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($('#m112CutoffTime7_hour').val()) <= parseInt($('#m112CutoffTime6_hour').val())
                && parseInt($('#m112CutoffTime6_minute').val()) == '59' ){
                $("#m112CutoffTime7_hour").val(parseInt($('#m112CutoffTime6_hour').val())+1);
//                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($('#m112CutoffTime8_hour').val()) <= parseInt($('#m112CutoffTime7_hour').val())
                && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
//                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
        }

    });

    $("#m112CutoffTime2_minute").change(function(){
        if($('#m112CutoffTime2_check').is(':checked')){
            if(parseInt($('#m112CutoffTime2_hour').val()) == parseInt($('#m112CutoffTime1_hour').val()) && parseInt($('#m112CutoffTime1_minute').val()) == '59' ){
                $("#m112CutoffTime2_hour").val(parseInt($('#m112CutoffTime1_hour').val())+1);
                $('#m112CutoffTime2_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime2_hour").val()) == parseInt($("#m112CutoffTime1_hour").val())
                && parseInt($("#m112CutoffTime2_minute").val()) <= parseInt($("#m112CutoffTime1_minute").val())){
                $('#m112CutoffTime2_minute').val(parseInt($('#m112CutoffTime1_minute').val())+1);
            }
        }
        if($('#m112CutoffTime3_check').is(':checked')){
            if(parseInt($("#m112CutoffTime3_hour").val()) < parseInt($("#m112CutoffTime2_hour").val())){
                $("#m112CutoffTime3_hour").val($("#m112CutoffTime2_hour").val());
                $("#m112CutoffTime3_minute").val(parseInt($("#m112CutoffTime2_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) == parseInt($("#m112CutoffTime2_hour").val())
                && parseInt($("#m112CutoffTime2_minute").val()) == '59'){
                $('#m112CutoffTime3_hour').val(parseInt($("#m112CutoffTime2_hour").val())+1);
                $('#m112CutoffTime3_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) == parseInt($("#m112CutoffTime2_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) <= parseInt($("#m112CutoffTime2_minute").val())){
                $('#m112CutoffTime3_minute').val(parseInt($("#m112CutoffTime2_minute").val())+1);
            }
        }
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val());
                $("#m112CutoffTime4_minute").val(parseInt($("#m112CutoffTime3_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) == '59'){
                $('#m112CutoffTime4_hour').val(parseInt($("#m112CutoffTime3_hour").val())+1);
                $('#m112CutoffTime4_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) <= parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val(parseInt($("#m112CutoffTime3_minute").val())+1);
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $("#m112CutoffTime5_minute").val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) == '59'){
                $('#m112CutoffTime5_hour').val(parseInt($("#m112CutoffTime4_hour").val())+1);
                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) <= parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $("#m112CutoffTime6_minute").val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) == '59'){
                $('#m112CutoffTime6_hour').val(parseInt($("#m112CutoffTime5_hour").val())+1);
                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) <= parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $("#m112CutoffTime7_minute").val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) == '59'){
                $('#m112CutoffTime7_hour').val(parseInt($("#m112CutoffTime6_hour").val())+1);
                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) <= parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $("#m112CutoffTime8_minute").val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) == '59'){
                $('#m112CutoffTime8_hour').val(parseInt($("#m112CutoffTime7_hour").val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) <= parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
        }
    });

    $("#m112CutoffTime3_hour").change(function(){
        if($('#m112CutoffTime3_check').is(':checked')){
            if(parseInt($('#m112CutoffTime3_hour').val()) <= parseInt($('#m112CutoffTime2_hour').val()) && parseInt($('#m112CutoffTime2_minute').val()) == '59' ){
                $("#m112CutoffTime3_hour").val(parseInt($('#m112CutoffTime2_hour').val())+1);
//                $('#m112CutoffTime3_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) <= parseInt($("#m112CutoffTime2_hour").val())){
                $("#m112CutoffTime3_hour").val($("#m112CutoffTime2_hour").val());
                $('#m112CutoffTime3_minute').val(parseInt($('#m112CutoffTime2_minute').val())+1);
            }
        }
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($('#m112CutoffTime4_hour').val()) <= parseInt($('#m112CutoffTime3_hour').val())
                && parseInt($('#m112CutoffTime3_minute').val()) == '59' ){
                $("#m112CutoffTime4_hour").val(parseInt($('#m112CutoffTime3_hour').val())+1);
//                $('#m112CutoffTime4_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) < parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val(parseInt($('#m112CutoffTime3_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val());
                $('#m112CutoffTime4_minute').val(parseInt($('#m112CutoffTime3_minute').val())+1);
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($('#m112CutoffTime5_hour').val()) <= parseInt($('#m112CutoffTime4_hour').val())
                && parseInt($('#m112CutoffTime4_minute').val()) == '59' ){
                $("#m112CutoffTime5_hour").val(parseInt($('#m112CutoffTime4_hour').val())+1);
//                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) < parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($('#m112CutoffTime6_hour').val()) <= parseInt($('#m112CutoffTime5_hour').val())
                && parseInt($('#m112CutoffTime5_minute').val()) == '59' ){
                $("#m112CutoffTime6_hour").val(parseInt($('#m112CutoffTime5_hour').val())+1);
//                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) < parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($('#m112CutoffTime7_hour').val()) <= parseInt($('#m112CutoffTime6_hour').val())
                && parseInt($('#m112CutoffTime6_minute').val()) == '59' ){
                $("#m112CutoffTime7_hour").val(parseInt($('#m112CutoffTime6_hour').val())+1);
//                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($('#m112CutoffTime8_hour').val()) <= parseInt($('#m112CutoffTime7_hour').val())
                && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
//                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
        }
    });

    $("#m112CutoffTime3_minute").change(function(){
        if($('#m112CutoffTime3_check').is(':checked')){
            if(parseInt($("#m112CutoffTime3_hour").val()) < parseInt($("#m112CutoffTime2_hour").val())){
                $("#m112CutoffTime3_hour").val($("#m112CutoffTime2_hour").val())
            }
            else if(parseInt($("#m112CutoffTime3_hour").val()) == parseInt($("#m112CutoffTime2_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) < parseInt($("#m112CutoffTime2_minute").val())){
                $('#m112CutoffTime3_minute').val($('#m112CutoffTime2_minute').val());
            }
        }
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val());
                $("#m112CutoffTime4_minute").val(parseInt($("#m112CutoffTime3_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime3_minute").val()) == '59'){
                $('#m112CutoffTime4_hour').val(parseInt($("#m112CutoffTime3_hour").val())+1);
                $('#m112CutoffTime4_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) <= parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val(parseInt($("#m112CutoffTime3_minute").val())+1);
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $("#m112CutoffTime5_minute").val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) == '59'){
                $('#m112CutoffTime5_hour').val(parseInt($("#m112CutoffTime4_hour").val())+1);
                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) <= parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $("#m112CutoffTime6_minute").val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) == '59'){
                $('#m112CutoffTime6_hour').val(parseInt($("#m112CutoffTime5_hour").val())+1);
                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) <= parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $("#m112CutoffTime7_minute").val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) == '59'){
                $('#m112CutoffTime7_hour').val(parseInt($("#m112CutoffTime6_hour").val())+1);
                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) <= parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $("#m112CutoffTime8_minute").val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) == '59'){
                $('#m112CutoffTime8_hour').val(parseInt($("#m112CutoffTime7_hour").val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) <= parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
        }
    });

    $("#m112CutoffTime4_hour").change(function(){
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val())
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) < parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val($('#m112CutoffTime3_minute').val());
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($('#m112CutoffTime5_hour').val()) <= parseInt($('#m112CutoffTime4_hour').val())
                && parseInt($('#m112CutoffTime4_minute').val()) == '59' ){
                $("#m112CutoffTime5_hour").val(parseInt($('#m112CutoffTime4_hour').val())+1);
//                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) < parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $('#m112CutoffTime5_minute').val(parseInt($('#m112CutoffTime4_minute').val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($('#m112CutoffTime6_hour').val()) <= parseInt($('#m112CutoffTime5_hour').val())
                && parseInt($('#m112CutoffTime5_minute').val()) == '59' ){
                $("#m112CutoffTime6_hour").val(parseInt($('#m112CutoffTime5_hour').val())+1);
//                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) < parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($('#m112CutoffTime7_hour').val()) <= parseInt($('#m112CutoffTime6_hour').val())
                && parseInt($('#m112CutoffTime6_minute').val()) == '59' ){
                $("#m112CutoffTime7_hour").val(parseInt($('#m112CutoffTime6_hour').val())+1);
//                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($('#m112CutoffTime8_hour').val()) <= parseInt($('#m112CutoffTime7_hour').val())
                && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
//                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
        }
    });

    $("#m112CutoffTime4_minute").change(function(){
        if($('#m112CutoffTime4_check').is(':checked')){
            if(parseInt($("#m112CutoffTime4_hour").val()) < parseInt($("#m112CutoffTime3_hour").val())){
                $("#m112CutoffTime4_hour").val($("#m112CutoffTime3_hour").val())
            }
            else if(parseInt($("#m112CutoffTime4_hour").val()) == parseInt($("#m112CutoffTime3_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) < parseInt($("#m112CutoffTime3_minute").val())){
                $('#m112CutoffTime4_minute').val($('#m112CutoffTime3_minute').val());
            }
        }
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val());
                $("#m112CutoffTime5_minute").val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime4_minute").val()) == '59'){
                $('#m112CutoffTime5_hour').val(parseInt($("#m112CutoffTime4_hour").val())+1);
                $('#m112CutoffTime5_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) <= parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val(parseInt($("#m112CutoffTime4_minute").val())+1);
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $("#m112CutoffTime6_minute").val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) == '59'){
                $('#m112CutoffTime6_hour').val(parseInt($("#m112CutoffTime5_hour").val())+1);
                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) <= parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $("#m112CutoffTime7_minute").val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) == '59'){
                $('#m112CutoffTime7_hour').val(parseInt($("#m112CutoffTime6_hour").val())+1);
                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) <= parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $("#m112CutoffTime8_minute").val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) == '59'){
                $('#m112CutoffTime8_hour').val(parseInt($("#m112CutoffTime7_hour").val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) <= parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
        }
    });

    $("#m112CutoffTime5_hour").change(function(){
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val())
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) < parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val($('#m112CutoffTime4_minute').val());
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($('#m112CutoffTime6_hour').val()) <= parseInt($('#m112CutoffTime5_hour').val())
                && parseInt($('#m112CutoffTime5_minute').val()) == '59' ){
                $("#m112CutoffTime6_hour").val(parseInt($('#m112CutoffTime5_hour').val())+1);
//                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) < parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $('#m112CutoffTime6_minute').val(parseInt($('#m112CutoffTime5_minute').val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($('#m112CutoffTime7_hour').val()) <= parseInt($('#m112CutoffTime6_hour').val())
                && parseInt($('#m112CutoffTime6_minute').val()) == '59' ){
                $("#m112CutoffTime7_hour").val(parseInt($('#m112CutoffTime6_hour').val())+1);
//                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($('#m112CutoffTime8_hour').val()) <= parseInt($('#m112CutoffTime7_hour').val())
                && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
//                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
        }
    });

    $("#m112CutoffTime5_minute").change(function(){
        if($('#m112CutoffTime5_check').is(':checked')){
            if(parseInt($("#m112CutoffTime5_hour").val()) < parseInt($("#m112CutoffTime4_hour").val())){
                $("#m112CutoffTime5_hour").val($("#m112CutoffTime4_hour").val())
            }
            else if(parseInt($("#m112CutoffTime5_hour").val()) == parseInt($("#m112CutoffTime4_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) < parseInt($("#m112CutoffTime4_minute").val())){
                $('#m112CutoffTime5_minute').val($('#m112CutoffTime4_minute').val());
            }
        }
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val());
                $("#m112CutoffTime6_minute").val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime5_minute").val()) == '59'){
                $('#m112CutoffTime6_hour').val(parseInt($("#m112CutoffTime5_hour").val())+1);
                $('#m112CutoffTime6_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) <= parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val(parseInt($("#m112CutoffTime5_minute").val())+1);
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $("#m112CutoffTime7_minute").val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) == '59'){
                $('#m112CutoffTime7_hour').val(parseInt($("#m112CutoffTime6_hour").val())+1);
                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) <= parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $("#m112CutoffTime8_minute").val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) == '59'){
                $('#m112CutoffTime8_hour').val(parseInt($("#m112CutoffTime7_hour").val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) <= parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
        }
    });

    $("#m112CutoffTime6_hour").change(function(){
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val())
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) < parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val($('#m112CutoffTime5_minute').val());
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($('#m112CutoffTime7_hour').val()) <= parseInt($('#m112CutoffTime6_hour').val())
                && parseInt($('#m112CutoffTime6_minute').val()) == '59' ){
                $("#m112CutoffTime7_hour").val(parseInt($('#m112CutoffTime6_hour').val())+1);
//                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $('#m112CutoffTime7_minute').val(parseInt($('#m112CutoffTime6_minute').val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($('#m112CutoffTime8_hour').val()) <= parseInt($('#m112CutoffTime7_hour').val())
                && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
//                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
        }
    });

    $("#m112CutoffTime6_minute").change(function(){
        if($('#m112CutoffTime6_check').is(':checked')){
            if(parseInt($("#m112CutoffTime6_hour").val()) < parseInt($("#m112CutoffTime5_hour").val())){
                $("#m112CutoffTime6_hour").val($("#m112CutoffTime5_hour").val())
            }
            else if(parseInt($("#m112CutoffTime6_hour").val()) == parseInt($("#m112CutoffTime5_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) < parseInt($("#m112CutoffTime5_minute").val())){
                $('#m112CutoffTime6_minute').val($('#m112CutoffTime5_minute').val());
            }
        }
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val());
                $("#m112CutoffTime7_minute").val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime6_minute").val()) == '59'){
                $('#m112CutoffTime7_hour').val(parseInt($("#m112CutoffTime6_hour").val())+1);
                $('#m112CutoffTime7_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) <= parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val(parseInt($("#m112CutoffTime6_minute").val())+1);
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $("#m112CutoffTime8_minute").val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) == '59'){
                $('#m112CutoffTime8_hour').val(parseInt($("#m112CutoffTime7_hour").val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) <= parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
        }
    });

    $("#m112CutoffTime7_hour").change(function(){
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val())
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val($('#m112CutoffTime6_minute').val());
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($('#m112CutoffTime8_hour').val()) <= parseInt($('#m112CutoffTime7_hour').val())
                && parseInt($('#m112CutoffTime7_minute').val()) == '59' ){
                $("#m112CutoffTime8_hour").val(parseInt($('#m112CutoffTime7_hour').val())+1);
//                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $('#m112CutoffTime8_minute').val(parseInt($('#m112CutoffTime7_minute').val())+1);
            }
        }
    });

    $("#m112CutoffTime7_minute").change(function(){
        if($('#m112CutoffTime7_check').is(':checked')){
            if(parseInt($("#m112CutoffTime7_hour").val()) < parseInt($("#m112CutoffTime6_hour").val())){
                $("#m112CutoffTime7_hour").val($("#m112CutoffTime6_hour").val())
            }
            else if(parseInt($("#m112CutoffTime7_hour").val()) == parseInt($("#m112CutoffTime6_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) < parseInt($("#m112CutoffTime6_minute").val())){
                $('#m112CutoffTime7_minute').val($('#m112CutoffTime6_minute').val());
            }
        }
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val());
                $("#m112CutoffTime8_minute").val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime7_minute").val()) == '59'){
                $('#m112CutoffTime8_hour').val(parseInt($("#m112CutoffTime7_hour").val())+1);
                $('#m112CutoffTime8_minute').val('00');
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) <= parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val(parseInt($("#m112CutoffTime7_minute").val())+1);
            }
        }
    });

    $("#m112CutoffTime8_hour").change(function(){
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val())
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val($('#m112CutoffTime7_minute').val());
            }
        }
    });

    $("#m112CutoffTime8_minute").change(function(){
        if($('#m112CutoffTime8_check').is(':checked')){
            if(parseInt($("#m112CutoffTime8_hour").val()) < parseInt($("#m112CutoffTime7_hour").val())){
                $("#m112CutoffTime8_hour").val($("#m112CutoffTime7_hour").val())
            }
            else if(parseInt($("#m112CutoffTime8_hour").val()) == parseInt($("#m112CutoffTime7_hour").val())
                && parseInt($("#m112CutoffTime8_minute").val()) < parseInt($("#m112CutoffTime7_minute").val())){
                $('#m112CutoffTime8_minute').val($('#m112CutoffTime7_minute').val());
            }
        }
    });
});