Ext.define('My.app.MainPanel', {
    extend: 'Ext.panel.Panel',
    layout: {
        type: 'hbox',
        align: 'left'
    },
    initComponent: function (config) {
        this.items =[this.buildGridPanel(), this.buildChartPanel()];
        Ext.apply(this, config);
        this.callParent(arguments);

    },

    buildGridPanel : function(){
        return Ext.create('My.app.GridPanel',{
            width  : '45%',
            height : '100%',
            frame:true,
            border:true
        });
    },

    buildChartPanel : function() {
        return {
            xtype:'panel',
            height : '100%',
            width  : '55%',
            border: true,
            frame:true,
            layout: 'anchor',
            items: [this.buildLineChartPanel(), this.buildPieChartPanel()]
        }
    },

    buildLineChartPanel : function(){
        return Ext.create('My.app.LineChartPanel',{
            border:true,
            anchor: '100% 50%'
        });
    },

    buildPieChartPanel : function(){
        return Ext.create('My.app.PieChartPanel',{
            border:true,
            anchor: '100% 50%'
        });
    }

});