$(document).ready(function() {
	$("form").each(	function() {
		var $that = $(this);
		$that.submit(function() {
			$that.find("input[type='image'],input[type='submit']")
					.attr("disabled", "true");
		});
	});
});
