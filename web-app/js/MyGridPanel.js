Ext.define('My.app.GridPanel', {
    extend: 'Ext.panel.Panel',
    layout : 'fit',
    initComponent: function (config) {
        this.items =[this.buildPanel()];
        Ext.apply(this, config);
        this.callParent(arguments);

    },

    buildPanel : function(){
        Ext.define('GridModel', {
            extend: 'Ext.data.Model',
            fields: [
                {name: 'code'},
                {name: 'job_name'},
                {name: 'table_name'},
                {name: 'db_connection'},
                {name: 'created_by'},
                {
                    name: 'created_date',
                    useNull: true,
                    type: 'date',
                    dateFormat: 'Y-m-d H:i:s'
                },
                {name: 'nof_execution', type: 'int'}
            ]
        });

        var store = new Ext.data.JsonStore({
            // store configs
            storeId: 'gridStore',
            id:'gridStore',
            remoteSort: true,
            proxy: {
                type: 'ajax',
                url: MyApp.config.gridDataURL,
                reader: {
                    type: 'json',
                    root: 'rows',
                    idProperty: 'id',
                    totalProperty: 'totalCount',
                    simpleSortMode: true
                }
            },
            //alternatively, a Ext.data.Model name can be given (see Ext.data.Store for an example)
            model : 'GridModel',
            listeners: {
                exception: function(proxy, response, operation){
                    Ext.MessageBox.show({
                        title: 'ERROR',
                        msg: operation.getError(),
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }
            }
        });

        var dateRenderer = Ext.util.Format.dateRenderer('m/d/Y h:i A');

        var grid = Ext.create('Ext.grid.Panel', {
            id   : 'jobsGrid',
            title: 'Jobs Summary',
            store: store,
            disableSelection: false,
            loadMask: true,
//            width: 500,
//            height: 700,
            columnLines: true,
            viewConfig: {
                id: 'gv',
                trackOver: true,
                stripeRows: true
            },
            listeners: {
                cellclick : this.cellClick
            },
            // grid columns
            columns:[{
                // id assigned so we can apply custom css (e.g. .x-grid-cell-topic b { color:#333 })
                // TODO: This poses an issue in subclasses of Grid now because Headers are now Components
                // therefore the id will be registered in the ComponentManager and conflict. Need a way to
                // add additional CSS classes to the rendered cells.
                id: 'id',
                text: "ID",
                dataIndex: 'id',
                hidden: true,
                sortable: false
            },{
                text: "Job Name",
                dataIndex: 'job_name',
                flex: 1,
                sortable: true
            },{
                text: "Table Name",
                dataIndex: 'table_name',
                flex: 1,
                sortable: true
            },{
                text: "DB Connection",
                dataIndex: 'db_connection',
                flex: 1,
                sortable: true
            },{
                text: "Created By",
                dataIndex: 'created_by',
                flex: 1,
                sortable: true
            },{
                text: "Created Date",
                dataIndex: 'created_date',
                flex: 1,
                sortable: true,
                renderer : dateRenderer
            },{
                text: "Number of Execution",
                dataIndex: 'nof_execution',
                flex: 1,
                sortable: true
            }],
            // paging bar on the bottom
            bbar: Ext.create('Ext.PagingToolbar', {
                store: store,
                displayInfo: true,
                displayMsg: 'Displaying Jobs {0} - {1} of {2}',
                emptyMsg: "No Jobs to display",
                items:[
                    '-'/*, {
                        text: 'Show Preview',
                        enableToggle: true,
                        toggleHandler: function(btn, pressed) {
                            var preview = Ext.getCmp('gv').getPlugin('preview');
                            preview.toggleExpanded(pressed);
                    }
                }*/]
            })
        });

        // trigger the data store load
        //store.loadPage(1);

        grid.getSelectionModel().on('selectionchange', function(sm, selectedRecord) {
            if (selectedRecord.length) {
                this.cellClick(grid, null, null, selectedRecord[0]);
            }
        }, this);

        return grid;
    },

    cellClick : function(self, td, cellIndex, record, tr, rowIndex){
        //get line chart store and reload
        var line = Ext.getCmp('myLine'), lineStore = line.getStore();
        line.findParentByType('panel').setTitle("Execution Chart: "+record.get('job_name'));

        lineStore.load({
            params: {
                jobCode: record.get('code')
            }
        });


    }

});