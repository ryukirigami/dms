(function() {
	var a = "assets/css/" + DEMO_ADMINFLARE_VERSION + "/";
	$(document)
			.ready(
					function() {
						var d = function(h) {
							var i = demoGetColor(DEMO_CURRENT_THEME), f = demoGetColor(h);
							$("#styleswitcher-buttons .btn-" + i).text(
									demoCapitaliseFirstLetter(i));
							var j = $("#styleswitcher-buttons .btn-code");
							j.next("br").remove();
							j.remove();
							$("#styleswitcher-buttons .btn-" + f)
									.text("Primary")
									.parent()
									.next()
									.prepend(
											'<code class="btn-code">btn btn-primary</code><br>');
							var g = $("#styleswitcher-progress-bars .progress-code");
							g.prev("br").remove();
							g.remove();
							$("#styleswitcher-progress-bars .progress-" + f)
									.parent()
									.next()
									.append(
											'<br><code class="progress-code">&lt;div class="progress"&gt;...&lt;/div&gt;</code>')
						};
						var c = function(f) {
							f = demoTestThemeName(f);
							$("#bootstrap-css").attr("href",
									a + f + "/bootstrap.min.css");
							$("#adminflare-css").attr("href",
									a + f + "/adminflare.min.css");
							d(f);
							DEMO_CURRENT_THEME = f;
							demoSetCookieThemeName(f)
						};
						var b = function() {
							var f = $("#theme_switcher");
							f.css({
								left : $(window).width()
										- (f.hasClass("open") ? $(
												"#theme_switcher").width() : 0)
							})
						};
						d(DEMO_CURRENT_THEME);
						
						$("body").append('<div id="theme_switcher" />');
						var e = $("#theme_switcher");
						e
								.append('<a href="javascript:void(0)" class="button"><i class="icon-cogs"></i></a>');
						e.append('<div class="themes" />');
						$(".themes", e)
								.append("<div>Select theme</div>")
								.append(
										'<a href="#" data-theme="default"><span style="background: #3690e6"></span>Blue (Default)</a>')
								.append(
										'<a href="#" data-theme="cyan"><span style="background: #43b7bc"></span>Cyan</a>')
								.append(
										'<a href="#" data-theme="green"><span style="background: #98ba09"></span>Green</a>')
								.append(
										'<a href="#" data-theme="orange"><span style="background: #e7912a"></span>Orange</a>')
								.append(
										'<a href="#" data-theme="red"><span style="background: #d85837"></span>Red</a>')
								.append(
										'<a href="#" data-theme="black-blue" class="two-colors"><span style="background: #2c2c2c"></span><span style="background: #3690e6"></span>Black/Blue</a>')
								.append(
										'<a href="#" data-theme="black-cyan" class="two-colors"><span style="background: #2c2c2c"></span><span style="background: #43b7bc"></span>Black/Cyan</a>')
								.append(
										'<a href="#" data-theme="black-green" class="two-colors"><span style="background: #2c2c2c"></span><span style="background: #98ba09"></span>Black/Green</a>')
								.append(
										'<a href="#" data-theme="black-orange" class="two-colors"><span style="background: #2c2c2c"></span><span style="background: #e7912a"></span>Black/Orange</a>')
								.append(
										'<a href="#" data-theme="black-red" class="two-colors"><span style="background: #2c2c2c"></span><span style="background: #d85837"></span>Black/Red</a>')
								.append(
										'<a href="#" data-theme="black-pink" class="two-colors"><span style="background: #2c2c2c"></span><span style="background: #e3649a"></span>Black/Pink</a>');
						$("#theme_switcher .button").click(
								function() {
									var f = $("#theme_switcher");
									f.animate({
										left : $(window).width()
												- (f.hasClass("open") ? 0 : $(
														"#theme_switcher")
														.width())
									});
									f.toggleClass("open");
									$(".button i", f).toggleClass("icon-cogs")
											.toggleClass("icon-remove")
								});
						$(".themes a").click(function() {
							c($(this).attr("data-theme"));
							return false
						});
						b();
						$(window).resize(b)
					})
})();