package com.kombos.baseapp

import java.text.SimpleDateFormat

import org.apache.shiro.SecurityUtils

import com.kombos.baseapp.sec.shiro.Permission
import com.kombos.baseapp.ActionType;
import com.kombos.baseapp.AppSettingParam;
import com.kombos.baseapp.menu.Menu;



class BaseappTagLib {
	static namespace = "ba"
	
	def editableValue = {
		attrs, body ->
		def bean = attrs.bean
		
		String url =  attrs.url?.toString()
		String field = attrs.field?.toString()
		String type = attrs.type?.toString()
		String title = attrs.title?.toString()
		String onsuccess = attrs.onsuccess?.toString()
		
		if (!bean || !field || !url) {
			return
		}
		out << '<a href="#"	id="value-'+field+'" data-type="'+type+'" data-pk="'+bean.id+'" data-name="'+field+'" data-original-title="'+title+'" class="editable editable-click">'
		out << fieldValue(bean:bean, field:field)
		out << '</a>'
		out << r.script() {
            out << '$(\'#value-'+field+'\').editable({'
			out << 'url: \''+url+'\','
			out << 'params: function(params) {'			
			out << 'params.id = '+bean.id+';params.version = '+bean.version+';return params;'
			out << '},'
			out << 'success: function(response, newValue) {if(!response.success) '+onsuccess+';return response.msg;}'
			out << '});'
        }
	}
	
	def confirm = { attrs, body ->
		String clazz = attrs.class?:"btn"
		String label = attrs.label?:"Confirm"
		String message = attrs.message?:"Are you sure?"
		if (attrs.id == null) {
			attrs.id = attrs.name
		}
		String id = attrs.id
		String onsuccess = attrs.onsuccess?.toString()
		
		out << '<a class=\''+clazz+'\' href=\'javascript:void(0);\' id=\'confirm-'+id+'\'>'+label+'</a>'
		out << r.script() {
			out << '$(\'#confirm-'+id+'\').click(function(){'
			out << 'bootbox.confirm(\''+message+'\', function(result) {'
			out << 'if(result){'+onsuccess
			out << ';}});'
			out << '});'
		}
	}
	
	def datePicker = { attrs, body ->
		def value = attrs.value
		//log.info("Value class: " + value)
		String name =  attrs.name?.toString()
        String format = attrs.format?.toString()
        String formatFull = attrs.format?.toString()
        String required = attrs.required?.toString()
        String stringRequired = ""
        if(required=='true'){
            stringRequired = 'required=""'
        }

		def datadateformat= format.replaceAll 'M', 'm'		
		
		String full = value?new SimpleDateFormat(formatFull.replaceAll('m','M')).format(value):"";
		String year = value?new SimpleDateFormat("yyyy").format(value):"";
		String month = value?new SimpleDateFormat("MM").format(value):"";
		String day = value?new SimpleDateFormat("dd").format(value):"";
		
		out << '<input type="hidden" value="date.struct" name="'+name+'">'
		out << '<input type="hidden" value="'+day+'" id="'+name+'_day" name="'+name+'_day">'
		out << '<input type="hidden" value="'+month+'" id="'+name+'_month" name="'+name+'_month">'
		out << '<input type="hidden" value="'+year+'" id="'+name+'_year" name="'+name+'_year">'
		out << '<input type="text" '+ stringRequired +' id="'+name+'" value="'+full+'" name="'+name+'_dp" data-date-format="'+datadateformat+'">'
		out << r.script() {
			out << 'var '+name+'_dp = $(\'#'+name+'\').datepicker().on(\'changeDate\', function(ev) {'
			out << 'var newDate = new Date(ev.date);'
			out << '$(\'#'+name+'_day\').val(newDate.getDate());'
			out << '$(\'#'+name+'_month\').val(newDate.getMonth()+1);'
			out << '$(\'#'+name+'_year\').val(newDate.getFullYear());'
			out << '$(this).datepicker(\'hide\');'
			out << '});'
		}
	}
	
	def setting = {attrs, body ->
		def code = attrs.valueFor ?: attrs.value
		if (code) {
			def val = "${AppSettingParam.valueFor(code) ?: attrs.default}"
			switch (attrs.encodeAs?.toLowerCase()) {
				case 'html':
					val = val.encodeAsHTML()
					break
				case 'xml':
					val = val.encodeAsXML()
					break
				case 'json':
					val = val.encodeAsJSON()
			}

			out << val
		}
	}
	
	def menuService
	
	def menu = {attrs, body ->
		def menu = attrs.menu
		
		//if(menu && childrenHasPermission(menu)){
		//if(menu && menuService.userHasMenu(menu)){
		if(true){
			renderMenu(menu, false)
			//out << '<li class="separator"></li>'
		}
	}
	
	private void renderMenu(Menu menu, boolean sub) {
		//if(menu.children){
			if(sub){
				out << '<li class="dropdown-submenu"><a class="usermenu" href="#">'
			} else {
				out << '<li class="dropdown"><a class="usermenu" href="#">'
			}
			out << '<span class="'+menu.iconClass+'" style="left: 0px"></span>'+menu.label+'</a>'
			out << '<ul class="dropdown-menu" id="'+menu.label+'-list">'
			menu.children.each() {Menu cmenu ->
				//if(cmenu && childrenHasPermission(cmenu)){
				//if(cmenu && menuService.userHasMenu(cmenu)){
				if(true){
					out << renderMenu(cmenu, true)
				}
			};
			out << '</ul></li>'
	//	} else {
	//	Permission p = Permission.findByMenuAndActionType(menu, ActionType.VIEW)
	//	if(p == null || SecurityUtils.subject.isPermitted(p.linkController + ":" + p.actionString)) {
//			if(menu && menuService.userHasMenu(menu)){
//			if(menu.linkAction){
//				out << '<li><a href="#/'+menu.linkController+'/'+menu.linkAction+'">'
//				out << r.script() {
//					out << 'Path.map("#/'+menu.linkController+'/'+menu.linkAction+'").to(function(){loadPath("'+menu.linkController+'/'+menu.linkAction+'", "'+menu.menuCode+'");});'
//				}
//			} else {
//			out << '<li><a href="#/'+menu.linkController+'">'
//			out << r.script() {
//				out << 'Path.map("#/'+menu.linkController+'").to(function(){loadPath("'+menu.linkController+'", "'+menu.menuCode+'");});'
//			}
//			}
//			out << '<span class="'+menu.iconClass+'"></span>'
//			out << g.message(code: "app.menu."+menu.linkController+".label", default: menu.label)
//			out << '</a></li>'
//			
//		}
//		}
	}
	
	private boolean childrenHasPermission(Menu menu) {
		boolean ret = false
		if(menu.children){
			menu.children.each() {
				ret |= childrenHasPermission(it)
			}
		} else {
			Permission p = Permission.findByMenuAndActionTypeAndLinkController(menu, ActionType.VIEW, menu.linkController)
			if(p)
				ret =  SecurityUtils.subject.isPermitted(p.linkController + ":" + p.actionString)
			else
				ret = true
		}
		return ret
	}
	
}
