//generate code by AFIRDAUSM
package com.kombos.generatecode

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.NomorUrutHarian
import com.kombos.baseapp.SequenceCode
import com.kombos.baseapp.UrutBelumTerpakai
import com.kombos.finance.Journal
import com.kombos.finance.Transaction
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat


class GenerateCodeService {

    def serviceMethod() {

    }

    def generateJournalCode(params) {
        String hasil = ""
        try {
            def session = RequestContextHolder.currentRequestAttributes().getSession()
            Date date = new Date()
            Calendar cal = Calendar.getInstance()
            cal.set(date .format("yyyy").toInteger(),(date.format("MM").toInteger()) - 1,1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            String bulanParam =  date.format("yyyy-MM-")+"1"
            Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
            String bulanParam2 =  date.format("yyyy-MM-") +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
            Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
            String monthYear = date.format("MMYYYY")
            def journal = Journal.createCriteria().list {
                eq("companyDealer",session.userCompanyDealer)
                ilike("journalCode", "%" + (params as String).toUpperCase() + "%")
                ge("dateCreated",tgl)
                lt("dateCreated",tgl2+1)
                order("dateCreated","desc")
            }
            String companyCode = session?.userCompanyDealer?.m011ID
            String prefix = params
            def m = journal.size()?:0
            String endNomor = String.format('%07d',m+1)
            hasil = prefix + companyCode + monthYear+ endNomor
        }catch (Exception e){
            Date date = new Date()
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMYYYY")
            String dateString = dateFormat.format(date)
            String prefix = params
            String rpadding = "000000"
            def lastJournal = Journal.last()
            Long id = 1
            if (lastJournal) {
                id = lastJournal.id +1
            }
            hasil = prefix + dateString + rpadding + String.valueOf(id)
        }
        return hasil
    }

    def generateTransactionCode() {
        String hasil = ""
        try {
            def session = RequestContextHolder.currentRequestAttributes().getSession()
            Date date = new Date()
            Calendar cal = Calendar.getInstance()
            cal.set(date .format("yyyy").toInteger(),(date.format("MM").toInteger()) - 1,1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            String bulanParam =  date.format("yyyy-MM-")+"1"
            Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
            String bulanParam2 =  date.format("yyyy-MM-") +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
            Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
            String monthYear = date.format("MMYYYY")
            def transaction = Transaction.createCriteria().list {
                eq("companyDealer",session.userCompanyDealer)
                ge("dateCreated",tgl)
                lt("dateCreated",tgl2+1)
                order("dateCreated","desc")
            }
            String companyCode = session?.userCompanyDealer?.m011ID
            String prefix = "TRX"
            def m = transaction.size()?:0
            String endNomor = String.format('%07d',m+1)
            hasil = prefix + companyCode + monthYear+ endNomor
        }catch (Exception e){
            Date date = new Date()
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMYYYY")
            String dateString = dateFormat.format(date)
            String prefix = "TRX"
            String rpadding = "000000"
            def lastJournal = Transaction.last()
            Long id = 1
            if (lastJournal) {
                id = lastJournal.id +1
            }
            hasil = prefix + dateString + rpadding + String.valueOf(id)
        }
        return hasil
    }

    def codeGenerateSequence(String keyCode, CompanyDealer cabang) {
        SequenceCode sc = null
		if(cabang) {
            sc = SequenceCode.findByKeyCodeAndCabang(keyCode,cabang)
            if(sc==null){
                def newsc = SequenceCode.findByKeyCodeAndCabangIsNull(keyCode)
                sc = new SequenceCode(cabang: cabang, formatDate: newsc?.formatDate, keterangan: newsc?.keterangan, keyCode: newsc?.keyCode, lastSequence: newsc?.lastSequence, sequenceFormat: newsc?.sequenceFormat, sequencepadder: newsc?.sequencepadder, sequnceLength: newsc?.sequnceLength)
                sc.nomorUrutHarians.each { nuh->
                    newsc.nomorUrutHarians << nuh
                }
                //, nomorUrutHarians: newsc?.nomorUrutHarians
                sc?.save(flush: true)
            }
        }else{
            sc = SequenceCode.findByKeyCodeAndCabangIsNull(keyCode)
        }
        if(sc){
            //cek apakah ada code yang belum terpakai
            //def ubt = UrutBelumTerpakai.createCriteria().list {
            //    eq("sequenceCode", sc)
            //    order("id", "asc")
            //}
            String newCode = ""
            //if(!ubt.empty){
                //newCode = ubt?.first()?.codeBlmTerpakai
            //}

            //jika tidak ada code yang belum terpakai maka buat code baru
            //if(newCode==""){
                String resultGenerateSequence = ""
                Integer sl = sc?.sequnceLength
                String sp = sc?.sequencepadder
                String sf = sc?.sequenceFormat
                Integer ls = sc?.lastSequence
                Integer newSequence = ls + 1
                int lengthChar = 0
                int diff = 0
                newCode = sf
                Date date = new Date();

                if(sf?.indexOf("{CAB}") > -1){
                    newCode = newCode?.replace("{CAB}",cabang?.m011ID)
                }

                if(sf?.indexOf("{TANGGAL}") > -1){
                    SimpleDateFormat dateFormat = new SimpleDateFormat(sc?.formatDate);
                    String dateToday = dateFormat.format(date);
                    newCode = newCode?.replace("{TANGGAL}",dateToday)
                }

                if(sf?.indexOf("{KEY}") > -1){
                    lengthChar = newSequence?.toString()?.length()
                    diff = sl - lengthChar
                    for(int i = 0;i<diff;i++){
                        resultGenerateSequence += sp
                    }
                    resultGenerateSequence += newSequence?.toString()
                    sc?.setLastSequence(newSequence)
                    try{
                        sc?.save()
                    } catch (e){
                        e.printStackTrace()
                    }
                    newCode = newCode?.replace("{KEY}",resultGenerateSequence)
                }

                if(sf?.indexOf("{YYYY}") > -1){
                    //{Tahun 4 digit}
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
                    String yyyy = dateFormat.format(date);
                    newCode = newCode?.replace("{YYYY}",yyyy)
                }
                if(sf?.indexOf("{BULANROMAWI}") > -1){
                    //{Bulan ROMAWI}
                    SimpleDateFormat dateFormat = new SimpleDateFormat("M");
                    String m = dateFormat.format(date);
                    def strRomawi = ["I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII"]
                    newCode = newCode?.replace("{BULANROMAWI}",strRomawi[(Integer.parseInt(m)-1)])
                }
                if(sf?.indexOf("{MM}") > -1){
                    //{Bulan 2 digit}
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
                    String mm = dateFormat.format(date);
                    newCode = newCode?.replace("{MM}",mm)
                }
                if(sf?.indexOf("{DD}") > -1){
                    //{Tanggal 2 digit}
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
                    String dd = dateFormat.format(date);
                    newCode = newCode?.replace("{DD}",dd)
                }
                if(sf?.indexOf("{URUTHARIAN}") > -1){
                    //{Nomor Urut Harian 4 digit}
                    String urutHarian = ""

                    def nomorUrutHarian = NomorUrutHarian.findBySequenceCodeAndHariAndBulanAndTahun(sc,Integer.parseInt(date.format("dd")),Integer.parseInt(date.format("MM")),Integer.parseInt(date.format("yyyy")))

                    if(nomorUrutHarian == null){
                        nomorUrutHarian = new NomorUrutHarian(sequenceCode: sc, dateUrut: date, hari: Integer.parseInt(date.format("dd")), bulan: Integer.parseInt(date.format("MM")), tahun: Integer.parseInt(date.format("yyyy")), urut: 0)//.save(flush: true)
                    }
                    Integer urutanBaru = nomorUrutHarian?.urut + 1
                    lengthChar = urutanBaru?.toString()?.length()
                    diff = sl - lengthChar
                    for(int i = 0;i<diff;i++){
                        urutHarian += sp
                    }
                    urutHarian += urutanBaru?.toString()
                    nomorUrutHarian?.setUrut(urutanBaru)
                    try{
                        nomorUrutHarian?.save(flush: true)
                    } catch (e){
                        e.printStackTrace()
                    }

                    newCode = newCode?.replace("{URUTHARIAN}",urutHarian)
                }

            return newCode
        } else {
            return null
        }
    }

    def hapusCodeBelumTerpakai(String keyCode, CompanyDealer cabang, String code){
        SequenceCode sc = null
        if(cabang) {
            sc = SequenceCode.findByKeyCodeAndCabang(keyCode,cabang)
        }else{
            sc = SequenceCode.findByKeyCodeAndCabangIsNull(keyCode)
        }
        def ubt = UrutBelumTerpakai.findBySequenceCodeAndCodeBlmTerpakai(sc,code)
        try{
            if(ubt){
                ubt.delete(flush: true)
            }
        }catch (e){
            e.printStackTrace()
        }
    }

    def bootstrap() {
        //===========ADMINISTRASI===========
        //kode user
        def codeT001IDUser = SequenceCode.findByKeyCode('T001_IDUser') ?: new SequenceCode(keyCode: 'T001_IDUser',lastSequence: 0,sequenceFormat: '{YYYY}{MM}{DD}{URUTHARIAN}',sequencepadder: '0',sequnceLength: 4,keterangan: "User Profile")
        codeT001IDUser.save()

        //kode no NPB format No. squence/Code Cabang/Kelompok/Bulan/Tahun
        def codeNoNPB = SequenceCode.findByKeyCode('codeNoNPB') ?: new SequenceCode(keyCode: 'codeNoNPB',lastSequence: 0,sequenceFormat: 'No. {KEY}/{CAB}/{KELOMPOK}/{BULANROMAWI}/{YYYY}',sequencepadder: '0',sequnceLength: 4,keterangan: "No NPB")
        codeNoNPB.save()

        //kode role
        def kodeRole = SequenceCode.findByKeyCode('kodeRole') ?: new SequenceCode(keyCode: 'kodeRole',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "User Role")
        kodeRole.save()

        //kode bank
        def codeM702ID = SequenceCode.findByKeyCode('M702_ID') ?: new SequenceCode(keyCode: 'M702_ID',lastSequence: 0,sequenceFormat: 'B{KEY}',sequencepadder: '0',sequnceLength: 3,keterangan: "Master Bank")
        codeM702ID.save()

        //kode panel
        def codeM094ID = SequenceCode.findByKeyCode('M094_ID') ?: new SequenceCode(keyCode: 'M094_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "Master Panel")
        codeM094ID.save()

        //kode mesin edc
        def codeM704ID = SequenceCode.findByKeyCode('M704_ID') ?: new SequenceCode(keyCode: 'M704_ID',lastSequence: 0,sequenceFormat: 'E{KEY}',sequencepadder: '0',sequnceLength: 3,keterangan: "Master Mesin EDC")
        codeM704ID.save()

        //kode section
        def codeM051ID = SequenceCode.findByKeyCode('M051_ID') ?: new SequenceCode(keyCode: 'M051_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 3,keterangan: "Master Job - Section")
        codeM051ID.save()

        //kode serial
        def codeM052ID = SequenceCode.findByKeyCode('M052_ID') ?: new SequenceCode(keyCode: 'M052_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 3,keterangan: "Master Job - Serial")
        codeM052ID.save()

        //kode temporary job
        def codeM053JobID = SequenceCode.findByKeyCode('M053_JobID') ?: new SequenceCode(keyCode: 'M053_JobID',lastSequence: 0,sequenceFormat: 'tmp{KEY}',sequencepadder: '0',sequnceLength: 7,keterangan: "Master Job - Temporary Job")
        codeM053JobID.save()

        //kode vendor cat
        def codeM191ID = SequenceCode.findByKeyCode('M191_ID') ?: new SequenceCode(keyCode: 'M191_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "Master Vendor Cat")
        codeM191ID.save()

        //kode kegiatan approval
        def codeM770IDApproval = SequenceCode.findByKeyCode('M770_IDApproval') ?: new SequenceCode(keyCode: 'M770_IDApproval',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "Kegiatan Approval")
        codeM770IDApproval.save()

        //===========PARTS===========
        //kode vendor
        def codeM121ID = SequenceCode.findByKeyCode('M121_ID') ?: new SequenceCode(keyCode: 'M121_ID',lastSequence: 0,sequenceFormat: 'S{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "Master Vendor")
        codeM121ID.save()

        //kode SCC
        def codeM113KodeSCC = SequenceCode.findByKeyCode('M113_KodeSCC') ?: new SequenceCode(keyCode: 'M113_KodeSCC',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master SCC")
        codeM113KodeSCC.save()

        //kode group parts
        def codeM181ID = SequenceCode.findByKeyCode('M181_ID') ?: new SequenceCode(keyCode: 'M181_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master - Group Parts")
        codeM181ID.save()

        //kode parts bining
        def codeT168ID = SequenceCode.findByKeyCode('T168_ID') ?: new SequenceCode(keyCode: 'T168_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-BIN{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Parts Binning")
        codeT168ID.save()

        //kode stock in
        def codeT169ID = SequenceCode.findByKeyCode('T169_ID') ?: new SequenceCode(keyCode: 'T169_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-SIN{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Stock IN")
        codeT169ID.save()

        //kode parts claim
        def codeT171ID = SequenceCode.findByKeyCode('T171_ID') ?: new SequenceCode(keyCode: 'T171_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-PCL{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Parts Claim")
        codeT171ID.save()

        //kode parts return
        def codeT172ID = SequenceCode.findByKeyCode('T172_ID') ?: new SequenceCode(keyCode: 'T172_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-PRT{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Parts Return")
        codeT172ID.save()

        //kode block stock
        def codeT157ID = SequenceCode.findByKeyCode('T157_ID') ?: new SequenceCode(keyCode: 'T157_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-BST{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Block Stock")
        codeT157ID.save()

        //kode on hand adjustment
        def codeT145ID = SequenceCode.findByKeyCode('T145_ID') ?: new SequenceCode(keyCode: 'T145_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-OHA{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input On Hand Adjustment")
        codeT145ID.save()

        //kode disposal
        def codeT147ID = SequenceCode.findByKeyCode('T147_ID') ?: new SequenceCode(keyCode: 'T147_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-DIS{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Displosal")
        codeT147ID.save()

        //kode stock opname
        def codeT132ID = SequenceCode.findByKeyCode('T132_ID') ?: new SequenceCode(keyCode: 'T132_ID',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-SOP{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Stock Opname")
        codeT132ID.save()

        //===========RECEPTION===========
        //kode customer company form
        def codeT401NoWO = SequenceCode.findByKeyCode('T401_NoWO') ?: new SequenceCode(keyCode: 'T401_NoWO',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-WO-{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Reception")
        codeT401NoWO.save()

        //===========APPOINTMENT===========
        //kode customer company form
        def codeT401NoAppointment = SequenceCode.findByKeyCode('T401_NoAppointment') ?: new SequenceCode(keyCode: 'T401_NoAppointment',lastSequence: 0,sequenceFormat: '{CAB}-{TANGGAL}-AP-{KEY}',sequencepadder: '0',sequnceLength: 6, formatDate: 'yyMMdd',keterangan: "Input Appointment")
        codeT401NoAppointment.save()

        //===========CUSTOMERPROFILE===========
        //kode customer company form
        def codeT101ID = SequenceCode.findByKeyCode('T101_ID') ?: new SequenceCode(keyCode: 'T101_ID',lastSequence: 0,sequenceFormat: '{YYYY}{MM}{DD}{URUTHARIAN}',sequencepadder: '0',sequnceLength: 4,keterangan: "Customer Company Form")
        codeT101ID.save()

        //kode SPK form
        def codeT191IDSPK = SequenceCode.findByKeyCode('T191_IDSPK') ?: new SequenceCode(keyCode: 'T191_IDSPK',lastSequence: 0,sequenceFormat: '{YYYY}{MM}{DD}{URUTHARIAN}',sequencepadder: '0',sequnceLength: 4,keterangan: "SPK Form")
        codeT191IDSPK.save()

        //kode customer form
        def codeT182T102ID = SequenceCode.findByKeyCode('T182_T102_ID') ?: new SequenceCode(keyCode: 'T182_T102_ID',lastSequence: 0,sequenceFormat: '{YYYY}{MM}{DD}{URUTHARIAN}',sequencepadder: '0',sequnceLength: 4,keterangan: "Customer Form")
        codeT182T102ID.save()

        //kode id card
        def codeM060ID = SequenceCode.findByKeyCode('M060_ID') ?: new SequenceCode(keyCode: 'M060_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master Customer - ID Card")
        codeM060ID.save()

        //kode agama
        def codeM061ID = SequenceCode.findByKeyCode('M061_ID') ?: new SequenceCode(keyCode: 'M061_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master Customer - Agama")
        codeM061ID.save()

        //kode nikah
        def codeM062ID = SequenceCode.findByKeyCode('M062_ID') ?: new SequenceCode(keyCode: 'M062_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master Customer - Nikah")
        codeM062ID.save()

        //kode hobby
        def codeM063ID = SequenceCode.findByKeyCode('M063_ID') ?: new SequenceCode(keyCode: 'M063_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master Customer - Hobby")
        codeM063ID.save()

        //kode survey
        def codeM131ID = SequenceCode.findByKeyCode('M131_ID') ?: new SequenceCode(keyCode: 'M131_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master Customer - Survey")
        codeM131ID.save()

        //kode merk
        def codeM064ID = SequenceCode.findByKeyCode('M064_ID') ?: new SequenceCode(keyCode: 'M064_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "Master Customer - Merk")
        codeM064ID.save()

        //kode model
        def codeM065ID = SequenceCode.findByKeyCode('M065_ID') ?: new SequenceCode(keyCode: 'M065_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "Master Customer - Model")
        codeM065ID.save()

        //kode warna
        def codeM092ID = SequenceCode.findByKeyCode('M092_ID') ?: new SequenceCode(keyCode: 'M092_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 4,keterangan: "Master Customer - Warna")
        codeM092ID.save()

        //kode status import
        def codeM100ID = SequenceCode.findByKeyCode('M100_ID') ?: new SequenceCode(keyCode: 'M100_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master Customer - Status Import")
        codeM100ID.save()

        //kode kwitansi
        def codeT722ID = SequenceCode.findByKeyCode('T722_ID') ?: new SequenceCode(keyCode: 'T722_ID',lastSequence: 0,sequenceFormat: '{CAB}{YYYY}{URUTHARIAN}',sequencepadder: '0',sequnceLength: 4,keterangan: "Kwitansi No")
        codeT722ID.save()

        //kode refund
        def codeT706ID = SequenceCode.findByKeyCode('T706_ID') ?: new SequenceCode(keyCode: 'T706_ID',lastSequence: 0,sequenceFormat: '{CAB}{YYYY}{URUTHARIAN}',sequencepadder: '0',sequnceLength: 4,keterangan: "Refund No")
        codeT706ID.save()

        //kode nama jabatan pic
        def codeM093ID = SequenceCode.findByKeyCode('M093_ID') ?: new SequenceCode(keyCode: 'M093_ID',lastSequence: 3,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 2,keterangan: "Master Nama Jabatan PIC")
        codeM093ID.save()

        //kode nama jabatan pic
        def codeT801ID = SequenceCode.findByKeyCode('T801_ID') ?: new SequenceCode(keyCode: 'T801_ID',lastSequence: 0,sequenceFormat: '{KEY}',sequencepadder: '0',sequnceLength: 6,keterangan: "Transaksi Follow Up")
        codeT801ID.save()
    }
}
