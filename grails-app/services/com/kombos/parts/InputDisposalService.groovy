package com.kombos.parts

import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User

class InputDisposalService {

    def requestTo(def username){
        def ret = []
        def user = User.findByUsername(username)

        def companyDealer = user.companyDealer

        def kepalaBengkel = Role.findByName("KEPALA_BENGKEL")

        kepalaBengkel.users.each {
            ret << it.username
            //println("Add: " + it.username)
        }

        ret
    }

    def assignTo(def username){
        def ret = []
        def user = User.findByUsername(username)

        def companyDealer = user.companyDealer

        def teknisi = Role.findByName("TEKNISI")

        teknisi.users.each {
            ret << it.username
            //println("Add: " + it.username)
        }

        ret
    }
}
