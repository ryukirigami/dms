package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartsStokService {
    boolean transactional = false
    def datatablesUtilService

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def cJum = PartsStok.createCriteria()
        def resultsJum = cJum.list {
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            if (params."sCriteria_kode_goods") {
                goods{
                    ilike("m111ID","%"+params."sCriteria_kode_goods"+"%")
                }
            }

            if (params."sCriteria_nama_goods") {
                goods{
                    ilike("m111Nama","%"+params."sCriteria_nama_goods"+"%")
                }
            }

            if (params."sCriteria_satuan_goods") {
                eq("goods", params."sCriteria_satuan_goods")
            }

            if (params."sCriteria_t131Qty1") {
                eq("t131Qty1", Double.parseDouble(params."sCriteria_t131Qty1"))
            }

            if (params."sCriteria_t131Qty2") {
                eq("t131Qty2", Double.parseDouble(params."sCriteria_t131Qty2"))
            }

            if (params."sCriteria_t131Qty1Free") {
                eq("t131Qty1Free", Double.parseDouble(params."sCriteria_t131Qty1Free"))
            }

            if (params."sCriteria_t131Qty2Free") {
                eq("t131Qty2Free", Double.parseDouble(params."sCriteria_t131Qty2Free"))
            }

            if (params."sCriteria_t131Qty1Reserved") {
                eq("t131Qty1Reserved", Double.parseDouble(params."sCriteria_t131Qty1Reserved"))
            }

            if (params."sCriteria_t131Qty2Reserved") {
                eq("t131Qty2Reserved", Double.parseDouble(params."sCriteria_t131Qty2Reserved"))
            }

            if (params."sCriteria_t131Qty1WIP") {
                eq("t131Qty1WIP", Double.parseDouble(params."sCriteria_t131Qty1WIP"))
            }

            if (params."sCriteria_t131Qty2WIP") {
                eq("t131Qty2WIP", Double.parseDouble(params."sCriteria_t131Qty2WIP"))
            }

            if (params."sCriteria_icc") {
//                eq("t131Qty2WIP", Double.parseDouble(params."sCriteria_t131Qty2WIP"))
                eq("goods",ICC.findByParameterICC(ParameterICC.findByM155KodeICCIlike("%"+params."sCriteria_icc"+"%"))?.goods)
            }

            if (params."sCriteria_t131Qty1BlockStok") {
                eq("t131Qty1BlockStock", Double.parseDouble(params."sCriteria_t131Qty1BlockStok"))
            }

            if (params."sCriteria_t131Qty2BlockStok") {
                eq("t131Qty2BlockStock", Double.parseDouble(params."sCriteria_t131Qty2BlockStok"))
            }

            if (params."sCriteria_t131LandedCost") {
                eq("t131LandedCost", Double.parseDouble(params."sCriteria_t131LandedCost"))
            }
             if (params."sCriteria_location") {
                eq("goods", ICC.findByParameterICC(Location.findByM120NamaLocationIlike("%"+params."sCriteria_location"+"%")?.parameterICC)?.goods)
            }
        }

        def tampilJUm = false
        int count = 0;
        resultsJum.each {
            tampilJUm = false
            def goods = GoodsReceiveDetail.findAllByGoodsAndCompanyDealer(it.goods,params?.companyDealer)
            goods.sort {
                it.dateCreated
            }

            def pembelian = new Date() - 1000
            if(goods){
                pembelian = goods.last()?.dateCreated
            }

            def selisih = (new Date() - pembelian)
            def data = selisih.intValue() / 30
            if(params.status=="1"){
                if(data.intValue() < 3 ){
                    tampilJUm = true
                }
            }else if(params.status=="2"){
                if(data.intValue() >= 3 && data.intValue() <= 6 ){
                    tampilJUm = true
                }
            }else if(params.status=="3"){
                if(data.intValue() > 6 ){
                    tampilJUm = true
                }
            }else{
                tampilJUm = true
            }
            if(tampilJUm== true){
                count++;
            }

        }
       // //println "kode goods : "+params.sCriteria_kode_goods

        def c = PartsStok.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            if (params."sCriteria_kode_goods") {
                goods{
                    ilike("m111ID","%"+params."sCriteria_kode_goods"+"%")
                }
            }

            if (params."sCriteria_nama_goods") {
                goods{
                    ilike("m111Nama","%"+params."sCriteria_nama_goods"+"%")
                }
            }

            if (params."sCriteria_satuan_goods") {
                eq("goods", params."sCriteria_satuan_goods")
            }

            if (params."sCriteria_t131Qty1") {
                eq("t131Qty1", Double.parseDouble(params."sCriteria_t131Qty1"))
            }

            if (params."sCriteria_t131Qty2") {
                eq("t131Qty2", Double.parseDouble(params."sCriteria_t131Qty2"))
            }

            if (params."sCriteria_t131Qty1Free") {
                eq("t131Qty1Free", Double.parseDouble(params."sCriteria_t131Qty1Free"))
            }

            if (params."sCriteria_t131Qty2Free") {
                eq("t131Qty2Free", Double.parseDouble(params."sCriteria_t131Qty2Free"))
            }

            if (params."sCriteria_t131Qty1Reserved") {
                eq("t131Qty1Reserved", Double.parseDouble(params."sCriteria_t131Qty1Reserved"))
            }

            if (params."sCriteria_t131Qty2Reserved") {
                eq("t131Qty2Reserved", Double.parseDouble(params."sCriteria_t131Qty2Reserved"))
            }

            if (params."sCriteria_t131Qty1WIP") {
                eq("t131Qty1WIP", Double.parseDouble(params."sCriteria_t131Qty1WIP"))
            }

            if (params."sCriteria_t131Qty2WIP") {
                eq("t131Qty2WIP", Double.parseDouble(params."sCriteria_t131Qty2WIP"))
            }

            if (params."sCriteria_icc") {
//                eq("t131Qty2WIP", Double.parseDouble(params."sCriteria_t131Qty2WIP"))
                  eq("goods",ICC.findByParameterICC(ParameterICC.findByM155KodeICCIlike("%"+params."sCriteria_icc"+"%"))?.goods)
            }

            if (params."sCriteria_t131Qty1BlockStok") {
                eq("t131Qty1BlockStock", Double.parseDouble(params."sCriteria_t131Qty1BlockStok"))
            }

            if (params."sCriteria_t131Qty2BlockStok") {
                eq("t131Qty2BlockStock", Double.parseDouble(params."sCriteria_t131Qty2BlockStok"))
            }

            if (params."sCriteria_t131LandedCost") {
                eq("t131LandedCost", Double.parseDouble(params."sCriteria_t131LandedCost"))
            }

             if (params."sCriteria_location") {

                eq("goods", ICC.findByParameterICC(Location.findByM120NamaLocationIlike("%"+params."sCriteria_location"+"%")?.parameterICC)?.goods)
            }

             switch (sortProperty) {
                case "kodeGoods":
                    goods{
                        order("m111ID", sortDir)
                    }
                    break;
                case "namaGoods":
                    goods{
                        order("m111Nama", sortDir)
                    }
                    break;
                case "qtyTotal":
                    order("t131Qty1", sortDir)
                    break;
                case "qtyFree":
                    order("t131Qty1Free", sortDir)
                    break;
                case "qtyReserved":
                    order("t131Qty1Reserved", sortDir)
                    break;
                case "qtyWIP":
                    order("t131Qty1WIP", sortDir)
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())

        def tampil = false
        results.each {
            tampil = false
            def goods = GoodsReceiveDetail.findAllByGoodsAndCompanyDealer(it.goods,params?.companyDealer)
            goods.sort {
                it.dateCreated
            }

            def pembelian = "-"
            if(goods){
                pembelian = goods.last()?.dateCreated
            }

            def selisih = (new Date() - (pembelian=="-"?(new Date()-1000): pembelian))
            def data = selisih.intValue() / 30
            if(params.status=="1"){
                if(data.intValue() < 3 ){
                    tampil = true
                }
            }else if(params.status=="2"){
                if(data.intValue() >= 3 && data.intValue() <= 6 ){
                    tampil = true
                }
            }else if(params.status=="3"){
                if(data.intValue() > 6 ){
                    tampil = true
                }
            }else{
                tampil = true
            }
            if(tampil == true){
                def harga = 0
                try {
                    harga = GoodsHargaJual.findByGoodsAndStaDel(it.goods,'0').t151HargaTanpaPPN
                }catch (Exception e){

                }
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer,

                    t131Tanggal: it.t131Tanggal ? it.t131Tanggal.format(dateFormat) : "",

                    kodeGoods: it.goods?.m111ID,

                    namaGoods: it.goods?.m111Nama,

                    harga : harga,

                    t131Qty1: it.t131Qty1 + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty2: it.t131Qty2 + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty1Free: it.t131Qty1Free + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty2Free: it.t131Qty2Free + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty1Reserved: it.t131Qty1Reserved + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty2Reserved: it.t131Qty2Reserved + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty1WIP: it.t131Qty1WIP + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty2WIP: it.t131Qty2WIP + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty1BlockStock: it.t131Qty1BlockStock + " " + it.goods?.satuan?.m118Satuan1,

                    t131Qty2BlockStock: it.t131Qty2BlockStock + " " + it.goods?.satuan?.m118Satuan1,

                    t131LandedCost: it.t131LandedCost + " " + it.goods?.satuan?.m118Satuan1,

                    icc : ICC.findByGoods(it.goods)?.parameterICC ? ICC.findByGoods(it.goods)?.parameterICC.m155KodeICC : "",

                    location : ICC.findByGoods(it.goods)?.parameterICC? Location.findByParameterICC(ICC.findByGoods(it.goods)?.parameterICC).m120NamaLocation : StockINDetail.findByBinningDetail(BinningDetail.findByGoodsReceiveDetail(GoodsReceiveDetail.findByGoods(it.goods)))?.location?.m120NamaLocation,

                    staDel: it?.staDel,

                    pembelianTerakhir : pembelian=="-" ?  "-" : pembelian.format("dd/MM/yyyy")

            ]
            }

        }

        [sEcho: params.sEcho, iTotalRecords: count, iTotalDisplayRecords: count, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PartsStok", params.id]]
            return result
        }

        result.partsStokInstance = PartsStok.get(params.id)

        if (!result.partsStokInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PartsStok", params.id]]
            return result
        }

        result.partsStokInstance = PartsStok.get(params.id)

        if (!result.partsStokInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PartsStok", params.id]]
            return result
        }

        result.partsStokInstance = PartsStok.get(params.id)

        if (!result.partsStokInstance)
            return fail(code: "default.not.found.message")

        try {
            result.partsStokInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        PartsStok.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.partsStokInstance && m.field)
                    result.partsStokInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["PartsStok", params.id]]
                return result
            }

            result.partsStokInstance = PartsStok.get(params.id)

            if (!result.partsStokInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.partsStokInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.partsStokInstance.properties = params

            if (result.partsStokInstance.hasErrors() || !result.partsStokInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PartsStok", params.id]]
            return result
        }

        result.partsStokInstance = new PartsStok()
        result.partsStokInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.partsStokInstance && m.field)
                result.partsStokInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["PartsStok", params.id]]
            return result
        }

        result.partsStokInstance = new PartsStok(params)

        if (result.partsStokInstance.hasErrors() || !result.partsStokInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def partsStok = PartsStok.findById(params.id)
        if (partsStok) {
            partsStok."${params.name}" = params.value
            partsStok.save()
            if (partsStok.hasErrors()) {
                throw new Exception("${partsStok.errors}")
            }
        } else {
            throw new Exception("PartsStok not found")
        }
    }

    def newStock(Goods goods, CompanyDealer companyDealer){
        def newStok =  new PartsStok()
        newStok?.companyDealer = companyDealer
        newStok?.t131Tanggal = new Date()
        newStok?.goods = goods
        newStok?.t131Qty1Free = 0
        newStok?.t131Qty2Free = 0
        newStok?.t131Qty1Reserved = 0
        newStok?.t131Qty2Reserved = 0
        newStok?.t131Qty1WIP = 0
        newStok?.t131Qty2WIP = 0
        newStok?.t131Qty1BlockStock = 0
        newStok?.t131Qty2BlockStock = 0
        newStok?.t131LandedCost = 0
        newStok?.t131Qty1 = 0
        newStok?.t131Qty2 = 0
        newStok.staDel = '0'
        newStok.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        newStok.lastUpdProcess = 'INSERT'
        newStok.dateCreated = new Date()
        newStok.lastUpdated = new Date()
        newStok?.save(flush: true)
        newStok.errors.each {
            //println "ini eror kurang stok : " + it
        }
    }
}