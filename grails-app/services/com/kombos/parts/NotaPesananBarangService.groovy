package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FullModelCode
import com.kombos.administrasi.KelompokNPB
import com.kombos.administrasi.PenegasanPengiriman
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class NotaPesananBarangService {
	boolean transactional = false

    def generateCodeService

    def datatablesUtilService

	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = NotaPesananBarang.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("cabangPengirim",params?.companyDealer)
			if(params."sCriteria_cabangPengirim"){
                cabangPengirim{
                    ilike("m011NamaWorkshop", "%"+params."sCriteria_cabangPengirim"+"%");
                }
				//eq("cabangPengirim",params."sCriteria_cabangPengirim")
			}

			if(params."sCriteria_cabangTujuan"){
                cabangTujuan{
                    ilike("m011NamaWorkshop", "%"+params."sCriteria_cabangTujuan"+"%");
                }
//                eq("cabangTujuan",params."sCriteria_cabangTujuan")
			}

			if(params."sCriteria_noNpb"){
				ilike("noNpb","%" + (params."sCriteria_noNpb" as String) + "%")
			}

			if(params."sCriteria_tglNpb"){
				ge("tglNpb",params."sCriteria_tglNpb")
				lt("tglNpb",params."sCriteria_tglNpb" + 1)
			}

			if(params."sCriteria_pemesan"){
				eq("pemesan",params."sCriteria_pemesan")
			}

			if(params."sCriteria_namaPemilik"){
				ilike("namaPemilik","%" + (params."sCriteria_namaPemilik" as String) + "%")
			}

			if(params."sCriteria_noPolisi"){
				ilike("noPolisi","%" + (params."sCriteria_noPolisi" as String) + "%")
			}

			if(params."sCriteria_noRangka"){
				ilike("noRangka","%" + (params."sCriteria_noRangka" as String) + "%")
			}

			if(params."sCriteria_noEngine"){
				ilike("noEngine","%" + (params."sCriteria_noEngine" as String) + "%")
			}

			if(params."sCriteria_tipeKendaraan"){
				tipeKendaraan{
                    ilike("t110FullModelCode","%"+ (params."sCriteria_tipeKendaraan" as String) + "%")
                }
			}

			if(params."sCriteria_tahunPembuatan"){
				eq("tahunPembuatan",Integer.parseInt(params."sCriteria_tahunPembuatan"))
			}

			if(params."sCriteria_noSpk"){
				ilike("noSpk","%" + (params."sCriteria_noSpk" as String) + "%")
			}

			if(params."sCriteria_tglSpk"){
				ge("tglSpk",params."sCriteria_tglSpk")
				lt("tglSpk",params."sCriteria_tglSpk" + 1)
			}

			if(params."sCriteria_keteranganTambahan"){
				ilike("keteranganTambahan","%" + (params."sCriteria_keteranganTambahan" as String) + "%")
			}

			if(params."sCriteria_penegasanPengiriman"){
                penegasanPengiriman{
                    ilike("jalur", "%"+params."sCriteria_penegasanPengiriman"+"%");
                }
			//	eq("penegasanPengiriman",params."sCriteria_penegasanPengiriman")
			}

			if(params."sCriteria_keteranganPenegasan"){
				ilike("keteranganPenegasan","%" + (params."sCriteria_keteranganPenegasan" as String) + "%")
			}

			if(params."sCriteria_staApproval"){
				eq("staApproval",params."sCriteria_staApproval")
			}

			if(params."sCriteria_tglApproveUnApprove"){
				ge("tglApproveUnApprove",params."sCriteria_tglApproveUnApprove")
				lt("tglApproveUnApprove",params."sCriteria_tglApproveUnApprove" + 1)
			}

			if(params."sCriteria_alasanUnApprove"){
				ilike("alasanUnApprove","%" + (params."sCriteria_alasanUnApprove" as String) + "%")
			}

			if(params."sCriteria_namaUserApproveUnApprove"){
				ilike("namaUserApproveUnApprove","%" + (params."sCriteria_namaUserApproveUnApprove" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
		results.each {
			rows << [
			
						id: it.id,
			
						cabangPengirim: it.cabangPengirim?.m011NamaWorkshop,
			
						cabangTujuan: it?.cabangTujuan?.m011NamaWorkshop,

                        cabangDiteruskan: it?.cabangDiteruskan?.id,

                        cd : user.companyDealer.id,

						noNpb: it.noNpb,
			
						tglNpb: it.tglNpb?it.tglNpb.format(dateFormat):"",
			
						pemesan: it.pemesan.fullname,
			
						namaPemilik: it.namaPemilik,
			
						noPolisi: it.noPolisi,
			
						noRangka: it.noRangka,
			
						noEngine: it.noEngine,
			
						tipeKendaraan: it.tipeKendaraan?.toString(),
			
						tahunPembuatan: it.tahunPembuatan,
			
						noSpk: it.noSpk,
			
						tglSpk: it.tglSpk?it.tglSpk.format(dateFormat):"",
			
						keteranganTambahan: it.keteranganTambahan,
			
						penegasanPengiriman: it?.penegasanPengiriman?.jalur,
			
						keteranganPenegasan: it.keteranganPenegasan,

						staApproval: it.kabengApprovalSta,

                        ptgsNPBJKTSta: it.ptgsNPBJKTSta,

						cabangDiteruskan: it.cabangDiteruskanSta,

                        cabangTglDiterima: it?.tglCabangDiterima?"0":"1",

						tglApproveUnApprove: it.tglApproveUnApprove?it.tglApproveUnApprove.format(dateFormat):"",
			
						alasanUnApprove: it.alasanUnApprove,
			
						namaUserApproveUnApprove: it.namaUserApproveUnApprove,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}

    def npbApprovalDatatables(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        def autorities = [:]

        roles.each {
            if(it.authority == "PETUGAS_NPB_HO")
                autorities.put("petugasNPBHO", true)
            else if(it.authority == "PETUGAS_DO")
                autorities.put("petugasDO",true)
            else if(it.authority == "PETUGAS_NPB_JKT")
                autorities.put("petugasNPBJKT",true)
            else if(it.authority == "PARTS_HA")
                autorities.put("partsHA", true)
            else if(it.authority == "KEPALA_BENGKEL")
                autorities.put("kepalaBengkel", true)
            else if(it.authority == "PARTSMAN")
                autorities.put("partsman", true)
            else if(it.authority == "ADMINISTRATION_HEAD")
                autorities.put("adminHead", true)
            else if(it.authority == "TEKNISI")
                autorities.put("teknisi", true)
            else if(it.authority == "SERVICE_ADVISOR_GENERAL_REPAIR")
                autorities.put("SAGR", true)

        }


        def c = NotaPesananBarang.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(autorities.get("kepalaBengkel"))
            {
                eq("kabengApprovalSta","0")
                eq("cabangPengirim", user.companyDealer)
            }

            else if(autorities.get("petugasNPBHO")){
                eq("cabangPengirim", user.companyDealer)
                eq("kabengApprovalSta", "1")
                eq("partsHOApproveSta", "0")
            }

            else if(autorities.get("petugasNPBJKT")){
                eq("cabangTujuan", user.companyDealer)
                eq("ptgsNPBJKTSta", "0")
                eq("kabengApprovalSta", "1")
                eq("partsHOApproveSta", "1")
            }else{
                eq("cabangDiteruskan", user.companyDealer)
            }

            if(params."sCriteria_cabangPengirim"){
                cabangPengirim{
                    ilike("m011NamaWorkshop", "%"+params."sCriteria_cabangPengirim"+"%");
                }
                //eq("cabangPengirim",params."sCriteria_cabangPengirim")
            }

            if(params."sCriteria_cabangTujuan"){
                cabangTujuan{
                    ilike("m011NamaWorkshop", "%"+params."sCriteria_cabangTujuan"+"%");
                }
//                eq("cabangTujuan",params."sCriteria_cabangTujuan")
            }

            if(params."sCriteria_noNpb"){
                ilike("noNpb","%" + (params."sCriteria_noNpb" as String) + "%")
            }

            if(params."sCriteria_tglNpb"){
                ge("tglNpb",params."sCriteria_tglNpb")
                lt("tglNpb",params."sCriteria_tglNpb" + 1)
            }

            if(params."sCriteria_pemesan"){
                eq("pemesan",params."sCriteria_pemesan")
            }

            if(params."sCriteria_namaPemilik"){
                ilike("namaPemilik","%" + (params."sCriteria_namaPemilik" as String) + "%")
            }

            if(params."sCriteria_noPolisi"){
                ilike("noPolisi","%" + (params."sCriteria_noPolisi" as String) + "%")
            }

            if(params."sCriteria_noRangka"){
                ilike("noRangka","%" + (params."sCriteria_noRangka" as String) + "%")
            }

            if(params."sCriteria_noEngine"){
                ilike("noEngine","%" + (params."sCriteria_noEngine" as String) + "%")
            }

            if(params."sCriteria_tipeKendaraan"){
                tipeKendaraan{
                    ilike("t110FullModelCode","%"+ (params."sCriteria_tipeKendaraan" as String) + "%")
                }
            }

            if(params."sCriteria_tahunPembuatan"){
                eq("tahunPembuatan",Integer.parseInt(params."sCriteria_tahunPembuatan"))
            }

            if(params."sCriteria_noSpk"){
                ilike("noSpk","%" + (params."sCriteria_noSpk" as String) + "%")
            }

            if(params."sCriteria_tglSpk"){
                ge("tglSpk",params."sCriteria_tglSpk")
                lt("tglSpk",params."sCriteria_tglSpk" + 1)
            }

            if(params."sCriteria_keteranganTambahan"){
                ilike("keteranganTambahan","%" + (params."sCriteria_keteranganTambahan" as String) + "%")
            }

            if(params."sCriteria_penegasanPengiriman"){
                penegasanPengiriman{
                    ilike("jalur", "%"+params."sCriteria_penegasanPengiriman"+"%");
                }
                //	eq("penegasanPengiriman",params."sCriteria_penegasanPengiriman")
            }

            if(params."sCriteria_keteranganPenegasan"){
                ilike("keteranganPenegasan","%" + (params."sCriteria_keteranganPenegasan" as String) + "%")
            }

            if(params."sCriteria_staApproval"){
                eq("staApproval",params."sCriteria_staApproval")
            }

            if(params."sCriteria_tglApproveUnApprove"){
                ge("tglApproveUnApprove",params."sCriteria_tglApproveUnApprove")
                lt("tglApproveUnApprove",params."sCriteria_tglApproveUnApprove" + 1)
            }

            if(params."sCriteria_alasanUnApprove"){
                ilike("alasanUnApprove","%" + (params."sCriteria_alasanUnApprove" as String) + "%")
            }

            if(params."sCriteria_namaUserApproveUnApprove"){
                ilike("namaUserApproveUnApprove","%" + (params."sCriteria_namaUserApproveUnApprove" as String) + "%")
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    cabangPengirim: it.cabangPengirim?.m011NamaWorkshop,

                    cabangTujuan: it?.cabangTujuan?.m011NamaWorkshop,

                    cabangDiteruskan: it?.cabangDiteruskan?.id,

                    cd: user?.companyDealer?.id,

                    noNpb: it.noNpb,

                    tglNpb: it.tglNpb?it.tglNpb.format(dateFormat):"",

                    pemesan: it.pemesan.fullname,

                    namaPemilik: it.namaPemilik,

                    noPolisi: it.noPolisi,

                    noRangka: it.noRangka,

                    noEngine: it.noEngine,

                    tipeKendaraan: it.tipeKendaraan?.toString(),

                    tahunPembuatan: it.tahunPembuatan,

                    noSpk: it.noSpk,

                    tglSpk: it.tglSpk?it.tglSpk.format(dateFormat):"",

                    keteranganTambahan: it.keteranganTambahan,

                    penegasanPengiriman: it?.penegasanPengiriman?.jalur,

                    keteranganPenegasan: it.keteranganPenegasan,

                    staApproval: it.kabengApprovalSta=="1"?"Approved":(it.kabengApprovalSta=="2"?"UnApproved":""),

                    tglApproveUnApprove: it.tglApproveUnApprove?it.tglApproveUnApprove.format(dateFormat):"",

                    alasanUnApprove: it.alasanUnApprove,

                    namaUserApproveUnApprove: it.namaUserApproveUnApprove,

                    foto : it.fotoImageMimeKabeng,

                    fotoKabeng : it?.fotoImageMimeKabeng?it.fotoImageMimeKabeng:"",
                    fotoHo : it?.fotoImageMimeHo?it.fotoImageMimeHo:"",

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["NotaPesananBarang", params.id] ]
			return result
		}

		result.notaPesananBarangInstance = NotaPesananBarang.get(params.id)

		if(!result.notaPesananBarangInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["NotaPesananBarang", params.id] ]
			return result
		}

		result.notaPesananBarangInstance = NotaPesananBarang.get(params.id)

		if(!result.notaPesananBarangInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["NotaPesananBarang", params.id] ]
			return result
		}

		result.notaPesananBarangInstance = NotaPesananBarang.get(params.id)

		if(!result.notaPesananBarangInstance)
			return fail(code:"default.not.found.message")

		try {
			result.notaPesananBarangInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}

    def update(params) {
        def result = [:]

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
      //  //println "ket penegasan : "+params

        NotaPesananBarang notaPesananBarang = NotaPesananBarang.get(params.id)
        notaPesananBarang.lastUpdated = datatablesUtilService?.syncTime()
        notaPesananBarang.updatedBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        notaPesananBarang.lastUpdProcess = "UPDATE"
        notaPesananBarang.alasanUnApprove = params.alasanUnApprove
        notaPesananBarang.cabangTujuan = CompanyDealer.get(params.cabangTujuan.id)
        notaPesananBarang.penegasanPengiriman = PenegasanPengiriman.get(params.penegasanPengiriman.id)
        notaPesananBarang.keteranganPenegasan = params.keteranganPenegasan
        notaPesananBarang.tipeKendaraan = FullModelCode.get(params.tipeKendaraan)
        notaPesananBarang.keteranganTambahan = params.keteranganTambahan
        notaPesananBarang.namaPemilik = params.namaPemilik
        notaPesananBarang.noEngine = params.noEngine
        notaPesananBarang.noNpb = params.noNpb
        notaPesananBarang.noPolisi = params.noPolisi
        notaPesananBarang.noRangka = params.noRangka
        notaPesananBarang.noSpk = params.noSpk
        notaPesananBarang.tahunPembuatan = Integer.parseInt(params.tahunPembuatan)
        notaPesananBarang.tglNpb = df.parse(params.tglNpb_dp)
        notaPesananBarang.tglSpk = df.parse(params.tglSpk_dp)
        notaPesananBarang.lastUpdated = datatablesUtilService?.syncTime()

        notaPesananBarang.save(flush:true)


        
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def goods = Goods.get(it)

           //println "Id goods : "+it
            if(goods){
              //  //println "qty "+params."qty${it}"
                def notaPesananBarangDetail = NotaPesananBarangDetail.findByGoodsAndNotaPesananBarang(goods,notaPesananBarang)

                if(notaPesananBarangDetail){
                    notaPesananBarangDetail.jumlah = Integer.parseInt(params."qty${it}")
                    notaPesananBarangDetail.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    notaPesananBarangDetail.lastUpdProcess = "UPDATE"
                    notaPesananBarangDetail.lastUpdated = datatablesUtilService?.syncTime()
                    notaPesananBarangDetail.save(flush: true)

                }else{
                    notaPesananBarangDetail = new NotaPesananBarangDetail()
                    notaPesananBarangDetail.goods = goods
                    notaPesananBarangDetail.jumlah = Integer.parseInt(params."qty${it}")
                    notaPesananBarangDetail.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    notaPesananBarangDetail.lastUpdProcess = "UPDATE"
                    notaPesananBarangDetail.notaPesananBarang = notaPesananBarang
                    notaPesananBarangDetail.dateCreated = datatablesUtilService?.syncTime()
                    notaPesananBarangDetail.lastUpdated = datatablesUtilService?.syncTime()
                    notaPesananBarangDetail.save(flush: true)

                }
            }
        }
        
    }

    def updateNPBApprove(params, authorities) {
        def result = [:]

        DateFormat df = new SimpleDateFormat("M/dd/yyyy")
        NotaPesananBarang notaPesananBarang = NotaPesananBarang.get(params.id)
        notaPesananBarang.lastUpdated = datatablesUtilService?.syncTime()
        notaPesananBarang.updatedBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        notaPesananBarang.lastUpdProcess = "UPDATE"

        notaPesananBarang.save(flush:true)

        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            //println "id dari goods : "+it
            def goods = Goods.get(it)

            if(goods){
                NotaPesananBarangDetail notaPesananBarangDetail = new NotaPesananBarangDetail()

                def cekGoods = NotaPesananBarangDetail.findByNotaPesananBarangAndGoods(notaPesananBarang,goods)

                if(!cekGoods){
                    notaPesananBarangDetail.goods = goods
                    notaPesananBarangDetail.notaPesananBarang = notaPesananBarang

                }else
                {
                    notaPesananBarangDetail = cekGoods
                }

                if(authorities.get("petugasNPBHO")){
                }
                if(authorities.get("petugasNPBJKT")){
                    //println "tanggal : "+params."ktrJKTTglDiproses${it}"

                    notaPesananBarangDetail.ktrJktTglDiproses = df.parse(params."ktrJKTTglDiproses${it}")
                    notaPesananBarang.ptgsNPBJKT = authorities.get("user")
                    notaPesananBarang.tglJKTDiterima = new SimpleDateFormat("yyyy-MM-dd").parse(params.tglJKTDiterima_dp)
                    notaPesananBarang.ptgsNPBJKTSta = "1"
                    notaPesananBarangDetail.lastUpdated  = new Date()
                    notaPesananBarangDetail.lastUpdProcess = "PETUGAS NPB JKT Update n Approve"
                }
                if(authorities.get("petugasDO")){
                    notaPesananBarangDetail.ktrJktTglBukaDO = df.parse(params."ktrJKTBukaDO${it}")
                    notaPesananBarangDetail.ktrJktTglKrmKeCabang = df.parse(params."ktrJktTglKrmKeCabang${it}")
                    notaPesananBarangDetail.lastUpdated = new Date()
                    notaPesananBarang.ptgsDOJKTSta = "1"
                    notaPesananBarangDetail.lastUpdProcess = "PETUGAS DO Approve n Update"
                    notaPesananBarang.ptgsDOJKT = authorities.get("user")
                }
                if(authorities.get("partsman")){
                    //println "CaKet : "+params."cabangKeterangan${it}"
                    notaPesananBarangDetail.cabangTglDiterima = df.parse(params."cabangTglDiterima${it}")
                    notaPesananBarangDetail.cabangKeterangan = params."cabangKeterangan${it}"
                    notaPesananBarang.cabangPenerima = authorities.get("user")
                    notaPesananBarangDetail.lastUpdated = new Date()
                    notaPesananBarangDetail.lastUpdProcess = "Partsman Update n Approve"
                }

                notaPesananBarangDetail.lastUpdated = datatablesUtilService?.syncTime()
                notaPesananBarangDetail.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                notaPesananBarangDetail.lastUpdProcess = "UPDATE"
                notaPesananBarangDetail.save(flush: true)

                //println "update notaPesananBarang"
            }


        }
    }

    def updateAsUser(params) {
        def result = [:]

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
        //println "ket penegasan : "+params

        NotaPesananBarang notaPesananBarang = NotaPesananBarang.get(params.id)
        notaPesananBarang.lastUpdated = new Date()
        notaPesananBarang.updatedBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        notaPesananBarang.lastUpdProcess = "UPDATE DATE AS "
        notaPesananBarang.keteranganTambahan = params.keteranganTambahan

        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        def autorities = [:]

        roles.each {
            if(it.authority == "PETUGAS_NPB_HO")
                autorities.put("petugasNPBHO", true)
            else if(it.authority == "PETUGAS_DO")
                autorities.put("petugasDO",true)
            else if(it.authority == "PETUGAS_NPB_JKT")
                autorities.put("petugasNPBJKT",true)
            else if(it.authority == "PARTS_HA")
                autorities.put("partsHA", true)
            else if(it.authority == "KEPALA_BENGKEL")
                autorities.put("kepalaBengkel", true)
            else if(it.authority == "PARTSMAN")
                autorities.put("partsman", true)
            else if(it.authority == "ADMINISTRATION_HEAD")
                autorities.put("adminHead", true)
            else if(it.authority == "TEKNISI")
                autorities.put("teknisi", true)
            else if(it.authority == "SERVICE_ADVISOR_GENERAL_REPAIR")
                autorities.put("SAGR", true)
        }

        if(autorities.get("petugasNPBHO")){
            notaPesananBarang.tglHODiterima = df.parse(params.tglHODiterima_dp)
            notaPesananBarang.tglHOKeJKT = df.parse(params.tglHOKeJKT_dp)
            notaPesananBarang.partsHOApproveSta = "1"
        }

        if(autorities.get("petugasDO")){
            notaPesananBarang.tglJKTBukaDO = df.parse(params.tglJKTBukaDO_dp)
            notaPesananBarang.tglJKTKirimCabang = df.parse(params.tglJKTKirimCabang_dp)
            notaPesananBarang.ptgsDOJKTSta = "1"

        }

        if(autorities.get("petugasNPBJKT")){
            notaPesananBarang.ptgsNPBJKTSta = "1"
        }

        if(autorities.get("partsman")){
            notaPesananBarang.tglCabangDiterima = df.parse(params.tglCabangDiterima_dp)

        }

            notaPesananBarang.lastUpdated = datatablesUtilService?.syncTime()
            notaPesananBarang.save(flush:true)

    }

    def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["NotaPesananBarang", params.id] ]
			return result
		}

		result.notaPesananBarangInstance = new NotaPesananBarang()
		result.notaPesananBarangInstance.properties = params

		// success
		return result
	}

	def save(params,CompanyDealer cabang) {
        def result = [:]

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")

        NotaPesananBarang notaPesananBarang = new NotaPesananBarang()
        notaPesananBarang.dateCreated = datatablesUtilService?.syncTime()
        notaPesananBarang.lastUpdated = datatablesUtilService?.syncTime()
        notaPesananBarang.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        notaPesananBarang.dateCreated = new Date()
        notaPesananBarang.lastUpdated = new Date()
        notaPesananBarang.pemesan =  User.findByUsernameAndCompanyDealer(org.apache.shiro.SecurityUtils.subject.principal.toString(),cabang)
        notaPesananBarang.lastUpdProcess = "INSERT"
        notaPesananBarang.alasanUnApprove = params.alasanUnApprove
        notaPesananBarang.cabangPengirim = cabang
        notaPesananBarang.cabangTujuan = CompanyDealer.get(params.cabangTujuan.id)
        notaPesananBarang.penegasanPengiriman = PenegasanPengiriman.get(params.penegasanPengiriman.id)
        notaPesananBarang.keteranganPenegasan = params.keteranganPenegasan
        notaPesananBarang.tipeKendaraan = FullModelCode.get(params.tipeKendaraan)
        notaPesananBarang.keteranganTambahan = params.keteranganTambahan
        notaPesananBarang.namaPemilik = params.namaPemilik
        notaPesananBarang.kelompokNPB = KelompokNPB.get(params.kelompokNPB.id)
        notaPesananBarang.noEngine = params.noEngine

        notaPesananBarang.noNpb = generateCodeService.codeGenerateSequence("codeNoNPB",cabang)
        notaPesananBarang.noNpb = notaPesananBarang.noNpb?.replace("{KELOMPOK}",notaPesananBarang.kelompokNPB?.kodeKelompok)

        notaPesananBarang.noPolisi = params.noPolisi
        notaPesananBarang.noRangka = params.noRangka
        notaPesananBarang.noSpk = params.noSpk
        notaPesananBarang.tahunPembuatan = Integer.parseInt(params.tahunPembuatan)
        notaPesananBarang.tglNpb = df.parse(params.tglNpb_dp)
        notaPesananBarang.tglSpk = df.parse(params.tglSpk_dp)
        ////println "cek : "+ notaPesananBarang.dump()
        notaPesananBarang.save(flush:true)

        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def goods = Goods.get(it)

            if(goods){
               //println "qty "+params."qty${it}"
                NotaPesananBarangDetail notaPesananBarangDetail = new NotaPesananBarangDetail()
                notaPesananBarangDetail.goods = goods
                notaPesananBarangDetail.dateCreated = datatablesUtilService?.syncTime()
                notaPesananBarangDetail.lastUpdated = datatablesUtilService?.syncTime()
                notaPesananBarangDetail.notaPesananBarang = notaPesananBarang
                notaPesananBarangDetail.jumlah = Integer.parseInt(params."qty${it}")
                notaPesananBarangDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                notaPesananBarangDetail.lastUpdProcess = "INSERT"
                notaPesananBarangDetail.save(flush: true)
          }


        }
	}
	
	def updateField(def params){
		def notaPesananBarang =  NotaPesananBarang.findById(params.id)
		if (notaPesananBarang) {
			notaPesananBarang."${params.name}" = params.value
			notaPesananBarang.save()
			if (notaPesananBarang.hasErrors()) {
				throw new Exception("${notaPesananBarang.errors}")
			}
		}else{
			throw new Exception("NotaPesananBarang not found")
		}
	}


    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
       def x = 0

        def c = NotaPesananBarangDetail.createCriteria()
        def results = c.list () {
            if(params.idNPB){
               eq("notaPesananBarang", NotaPesananBarang.get(params.idNPB))
            }
        }

        def rows = []

        results.sort {
            it.goods.id
        }


        results.each {

            rows << [

                    id: it?.goods?.id,

//                    notaPesananBarang: it.notaPesananBarang,

                    kodeParts : it?.goods?.m111ID,

                    namaParts : it?.goods?.m111Nama,

                    jumlah: it.jumlah,

                    hoTglDiterima:  it.notaPesananBarang?.tglHODiterima? it.notaPesananBarang?.tglHODiterima?.format(dateFormat) : "",

                    hoTglDiteruskan: it.notaPesananBarang.tglHOKeJKT? it.notaPesananBarang?.tglHOKeJKT?.format(dateFormat) : "",

                    ktrJktTglDiterima: it.notaPesananBarang.tglJKTDiterima ? it.notaPesananBarang.tglJKTDiterima?.format(dateFormat) : "",

                    ktrJktTglDiproses: it.ktrJktTglDiproses ? it.ktrJktTglDiproses.format(dateFormat) : "",

                    ktrJktTglBukaDO: it.ktrJktTglBukaDO ? it.ktrJktTglBukaDO.format(dateFormat) : "",

                    ktrJktTglKrmKeCabang: it.ktrJktTglKrmKeCabang ? it.ktrJktTglKrmKeCabang.format(dateFormat) : "",

                    cabangTglDiterima: it.cabangTglDiterima ? it.cabangTglDiterima.format(dateFormat) : "",

                    cabangKeterangan: it.cabangKeterangan,

                    hoKeterangan: it.hoKeterangan,
                    ktrJktKeterangan: it.ktrJktKeterangan,
                    nomorDO: it.nomorDO,

                    lastUpdProcess: it.lastUpdProcess


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def datatablesApprovalSubList(def params) {
     //   String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        String dateFormat = "M/dd/yyyy"
        def x = 0


        def c = NotaPesananBarangDetail.createCriteria()
        def results = c.list () {
            if(params.idNPB){
                eq("notaPesananBarang", NotaPesananBarang.get(params.idNPB))
            }
        }

        def rows = []

        results.sort {
            it.goods.m111ID
        }


        results.each {
            rows << [


                    id: it.id,

                    notaPesananBarang: it.notaPesananBarang,

                    kodeParts : it?.goods?.m111ID,

                    namaParts : it?.goods?.m111Nama,

                    jumlah: it.jumlah,

                    hoTglDiterima: it.hoTglDiterima ? it.hoTglDiterima.format(dateFormat) : "",

                    hoTglDiteruskan: it.hoTglDiteruskan ? it.hoTglDiteruskan.format(dateFormat) : "",

                    ktrJktTglDiterima: it.ktrJktTglDiterima ? it.ktrJktTglDiterima.format(dateFormat) : "",

                    ktrJktTglDiproses: it.ktrJktTglDiproses ? it.ktrJktTglDiproses.format(dateFormat) : "",

                    ktrJktTglBukaDO: it.ktrJktTglBukaDO ? it.ktrJktTglBukaDO.format(dateFormat) : "",

                    ktrJktTglKrmKeCabang: it.ktrJktTglKrmKeCabang ? it.ktrJktTglKrmKeCabang.format(dateFormat) : "",

                    cabangTglDiterima: it.cabangTglDiterima ? it.cabangTglDiterima.format(dateFormat) : "",

                    cabangKeterangan: it.cabangKeterangan?it.cabangKeterangan:"" ,

                    hoKeterangan: it.cabangKeterangan,
                    ktrJktKeterangan: it.cabangKeterangan,
                    nomorDO: it.cabangKeterangan,


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }


}