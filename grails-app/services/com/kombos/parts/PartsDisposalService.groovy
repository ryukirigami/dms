package com.kombos.parts

import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartsDisposalService implements AfterApprovalInterface {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsDisposal.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_t017Tanggal" && params."sCriteria_t017Tanggal2") {
                ge("t147TglDisp", params."sCriteria_t017Tanggal")
                lt("t147TglDisp", params."sCriteria_t017Tanggal2" + 1)
            }

        }
        def rows = []

        results.each {
            def pdd = ""
            if(params.statusApprove){
                pdd = PartsDisposalDetail.findByPartsDisposal(PartsDisposal.findById(it.id)).staApproval.toString()
                if(pdd==2){
                    pdd = 1
                }
            }
            if(pdd== params.statusApprove || !params.statusApprove){
                rows << [
                        id:it.id,
                        nomorDisposal: it.t147ID,
                        tanggalDisposal: it.t147TglDisp.format(dateFormat),
                        petugas: it.t147xNamaUser
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = PartsDisposalDetail.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            partsDisposal{
                eq("id",params.id as Long)
            }
        }
        def rows = []

        results.each {
            rows << [
                    id: it?.id,
                    kodeParts: it?.goods.m111ID,
                    namaParts: it?.goods.m111Nama,
                    qty: it?.t148Jumlah1,
                    location: KlasifikasiGoods.findByGoods(it?.goods)?.location?.m120NamaLocation
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            String data = it
            def partsDisposal = null
                partsDisposal = PartsDisposal.findByT147ID(data)
            if(partsDisposal){
                PartsDisposalDetail.findAllByPartsDisposal(PartsDisposal.findByT147ID(data)).each{
                    it?.delete(flush: true)
                }
                PartsDisposal.findByT147ID(data).each{
                    it?.delete(flush: true)
                }
            }else{
                try{
                    PartsDisposalDetail.findById(it as Long).delete()
                }catch(Exception a){

                }

            }
        }

    }
    def update(params) {

        Returns.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.partsDisposalInstance && m.field)
                    result.partsDisposalInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Returns", params.id]]
                return result
            }

            result.partsDisposalInstance = Returns.get(params.id)

            if (!result.partsDisposalInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.partsDisposalInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            //result.partsDisposalInstance.properties = params


            result.partsDisposalInstance?.t172Qty1Return  = Double.parseDouble(params.t172Qty1Return)
            result.partsDisposalInstance?.lastUpdated = datatablesUtilService?.syncTime()

            if (result.partsDisposalInstance.hasErrors() || !result.partsDisposalInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Returns", params.id]]
            return result
        }

        result.partsDisposalInstance = new Returns()
        result.partsDisposalInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.partsDisposalInstance && m.field)
                result.partsDisposalInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Returns", params.id]]
            return result
        }

        result.partsDisposalInstance = new Returns(params)

        if (result.partsDisposalInstance.hasErrors() || !result.partsDisposalInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def partsDisposal = Returns.findById(params.id)
        if (partsDisposal) {
            partsDisposal."${params.name}" = params.value
            partsDisposal.save()
            if (partsDisposal.hasErrors()) {
                throw new Exception("${partsDisposal.errors}")
            }
        } else {
            throw new Exception("Returns not found")
        }
    }

    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            def cf = PartsDisposal.get(it as Long)
            def rd = PartsDisposalDetail.findAllByPartsDisposal(cf)
            rd.each {
                def pdd = PartsDisposalDetail.get(it.id as Long)
                pdd.staApproval = staApproval
                if(staApproval==StatusApproval.APPROVED){
                    try{
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(Goods.findById(pdd.goods.id),'0',cf.companyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(Goods.findById(pdd.goods.id),cf.companyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(Goods.findById(pdd.goods.id),'0',cf.companyDealer)
                        }
                            def kurangStok2 =  PartsStok.get(goodsStok.id)
                            kurangStok2?.companyDealer = cf.companyDealer

                            kurangStok2?.goods = pdd.goods
                            kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - pdd.t148Jumlah1
                            kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - pdd.t148Jumlah1
                            kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - pdd.t148Jumlah1
                            kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - pdd.t148Jumlah1
                            kurangStok2.staDel = '0'
                            kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok2.lastUpdProcess = 'DISPOSAL'
                            kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                            kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                            kurangStok2?.save(flush: true)
                            kurangStok2.errors.each {
                                //println it
                            }
                    }catch(Exception ex){

                    }
                }
                pdd.lastUpdated = datatablesUtilService?.syncTime()
                pdd.save()
            }
        }
    }
}
