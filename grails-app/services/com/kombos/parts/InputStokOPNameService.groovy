package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import sun.print.resources.serviceui_sv

class InputStokOPNameService {

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService


//    def save(params,id) {
    def save(params,String nomorStockOpname) {
        String ada="tidak"
        def result = [:]


        def stokOPName = null
        result.ada = false

        if(StokOPName.findByT132ID(nomorStockOpname/*params.t132ID*/)!=null){
            ada="ada"
        }else{
            def jsonArray = JSON.parse(params.ids)
            jsonArray.each {
                def goods = KlasifikasiGoods.get(it)
                if(goods){
                    stokOPName = new StokOPName()

                    if(StokOPName.findByT132IDAndGoods(params.t132ID , goods)==null){
                        stokOPName?.t132Tanggal = params.t132Tanggal
                        stokOPName?.t132ID = nomorStockOpname//params.t132ID
                        stokOPName?.goods = goods
                        stokOPName?.location = goods.location
                        stokOPName?.t132StaUpdateStock = "0"
                        stokOPName?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        stokOPName?.lastUpdProcess = 'INSERT'
                        stokOPName?.companyDealer = params?.companyDealer
                        stokOPName?.dateCreated = datatablesUtilService?.syncTime()
                        stokOPName?.lastUpdated = datatablesUtilService?.syncTime()
                        if(PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods.goods,"0",params?.companyDealer)){
                            def partsok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods.goods,"0",params?.companyDealer)
                            stokOPName?.t132Jumlah1Stock = partsok?.t131Qty1
                            stokOPName?.t132Jumlah2Stock = partsok?.t131Qty2
                        }
                        stokOPName.save(flush:true)
                    }
                }
            }
        }

        return ada
    }

    def update(params) {
        String ada="tidak"
        def result = [:]
        String tanggalParams=params.t132Tanggal
        def strDate = new Date().parse("dd-MM-yyyy",tanggalParams)
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def stokOPName = null
            def goods = KlasifikasiGoods.get(it)
            def find = StokOPName.findByT132IDAndGoodsAndCompanyDealer(params.t132ID,goods,params?.companyDealer)
            if(find==null){
                stokOPName = new StokOPName()
                stokOPName?.t132Tanggal = strDate
                stokOPName?.t132ID = params.t132ID
                stokOPName?.goods = goods
                stokOPName?.location = goods.location
                stokOPName?.t132StaUpdateStock = "0"
                stokOPName?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                stokOPName?.lastUpdProcess = 'INSERT'
                stokOPName?.lastUpdated = datatablesUtilService?.syncTime()
                stokOPName?.dateCreated = datatablesUtilService?.syncTime()
                stokOPName?.companyDealer = params?.companyDealer
                if(PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods.goods,"0",params?.companyDealer)){
                    def partsok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods.goods,"0",params?.companyDealer)
                    stokOPName?.t132Jumlah1Stock = partsok?.t131Qty1
                    stokOPName?.t132Jumlah2Stock = partsok?.t131Qty2
                }
                stokOPName.save(flush:true)
            }else{
                find?.lastUpdated = datatablesUtilService?.syncTime()
                find?.t132Tanggal = strDate
                find?.lastUpdated = datatablesUtilService?.syncTime()
                find.save(flush: true)
            }
        }


        return ada
    }
}
