package com.kombos.parts

import com.kombos.administrasi.Divisi
import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class ValidasiOrderPartsService implements AfterApprovalInterface {
	boolean transactional = false
	
	def validasiOrderService
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def conversi = new Konversi()
	def datatablesUtilService

    def datatablesList(def params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
        def cTot = RequestDetail.createCriteria()
        def resultsTotal = cTot.list {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                request {
                    groupProperty("t162TglRequest", "tanggalRequest")
                    countDistinct("t162NoReff","jumlahRequest")
                }
            }
            request{
                if(params."sCriteria_tanggalTo"){
                    le("t162TglRequest",params."sCriteria_tanggalTo")
                }
                if(params."sCriteria_tanggalFrom"){
                    ge("t162TglRequest",params."sCriteria_tanggalFrom")
                }

            }
            if(params."sCriteria_status"){
                eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
            }
            request{
                eq("staDel",'0')
                eq("companyDealer",params?.companyDealer)
                order("t162TglRequest","desc")
            }
        }
		def c = RequestDetail.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
				request {
					groupProperty("t162TglRequest", "tanggalRequest")
                    countDistinct("t162NoReff","jumlahRequest")
				}
			}
			request{
				if(params."sCriteria_tanggalTo"){
					le("t162TglRequest",params."sCriteria_tanggalTo")
				}
				if(params."sCriteria_tanggalFrom"){
					ge("t162TglRequest",params."sCriteria_tanggalFrom")
				}

			}
			if(params."sCriteria_status"){
				eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
			}
            request{
                eq("companyDealer",params?.companyDealer)
                eq("staDel",'0')
                order("t162TglRequest","desc")
            }
		}
		def rows = []
		String temp = 0
        int jumData = 0
       DateFormat df = new SimpleDateFormat("dd/MM/yyyy")

	   results.each { data ->
           def c2 = RequestDetail.createCriteria()
           def resulttot = c2.list {
               request {
                   eq("staDel",'0')
                   eq("companyDealer",params?.companyDealer)
               }
               request{
                       ge("t162TglRequest",data.tanggalRequest)
                       le("t162TglRequest",data.tanggalRequest)
               }
               if(params."sCriteria_status"){
                   eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
               }
           }
           def total = 0
           resulttot.each {
               def hargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it.goods,params.companyDealer,'0')
               if(hargaBeli){
                   try {
                       total += (hargaBeli?.t150Harga * it.t162Qty1)
                   }catch(Exception e){

                   }

               }
           }
           String tgl = data.tanggalRequest?.format(dateFormat)
           Date tglParam = df.parse(data.tanggalRequest?.format(dateFormat) as String)
           if(tgl!=temp){
               jumData++
               rows << [
                       id: data.tanggalRequest?.format("yyyyMMdd"),
                       tanggalRequest: data.tanggalRequest?.format(dateFormat),
                       jumlahRequest: data.jumlahRequest,
                       totalHarga: total,
               ]
           }
           temp = tgl
		}
		[sEcho: params.sEcho, iTotalRecords:  resultsTotal.size(), iTotalDisplayRecords:  resultsTotal.size(), aaData: rows]
	}
   
    def datatablesSubList(def params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
	   String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
       DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
	   def propertiesToRender = params.sColumns.split(",")
	   def x = 0
	   
	   def c = RequestDetail.createCriteria()
	   def results = c.list () {
		   resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
		   projections {
			request {
               eq("companyDealer",params?.companyDealer)
               eq("staDel",'0')
			   groupProperty("t162NoReff", "noReferensi")
			   max("t162NamaPemohon","pemohon")
			  }
			}
			if(params.tanggalRequest){
				request{
                    Date date41 = df.parse(params.tanggalRequest)
                    if (params.tanggalRequest) {
                        ge("t162TglRequest", date41)
                        lt("t162TglRequest", date41 + 1)
                    }
				}
			}
		    if(params."sCriteria_status"){
				eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
			}
	   }
	   def rows = []
	   
	   def count = 1;
	   results.each { data ->
           def c2 = RequestDetail.createCriteria()
           def resulttot = c2.list {
               request {
                   eq("staDel",'0')
                   eq("companyDealer",params?.companyDealer)
                   eq("t162NoReff", data.noReferensi)
                   eq("t162NamaPemohon",data.pemohon)
               }
               if(params.tanggalRequest){
                   request{
                       Date date41 = df.parse(params.tanggalRequest)
                       if (params.tanggalRequest) {
                           ge("t162TglRequest", date41)
                           lt("t162TglRequest", date41 + 1)
                       }
                   }
               }
               if(params."sCriteria_status"){
                   eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
               }
           }
           def total = 0
           resulttot.each {
               def hargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it.goods,params.companyDealer,'0')
               if(hargaBeli){
                    total += (hargaBeli?.t150Harga * it.t162Qty1)
               }
           }
		   rows << [
			   id: count,
			   idTanggal: params.idTanggal,
			   noReferensi: data.noReferensi,
			   pemohon: data.pemohon,
			   totalHarga: total,
		   ]
		   count++
	   }
	   
	   [sEcho: params.sEcho, iTotalRecords:  count, iTotalDisplayRecords: count, aaData: rows]
			   
    }
   
    def datatablesSubSubList(def params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
	   String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
	   
	   def propertiesToRender = params.sColumns.split(",")
	   def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
	   def sortProperty = propertiesToRender[params.iSortCol_0 as int]
	   def x = 0
	   
	   def c = RequestDetail.createCriteria()
	   def results = c.list () {
		    if(params.noReferensi){
				request {
                    eq("staDel",'0')
                    eq("companyDealer",params?.companyDealer)
					eq("t162NoReff", params.noReferensi)
			   }
			}
			if(params."sCriteria_status"){
				eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
			}
	   }
	   
	   def rows = []

	   results.each {

           def kla = KlasifikasiGoods.findByGoods(it.goods)? KlasifikasiGoods.findByGoods(it.goods)?.franc?.m117NamaFranc :"";
           def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it.goods,session.userCompanyDealer,'0')
           def totalHarga = 0
           if(goodsHargaBeli){
            totalHarga = it.t162Qty1 * goodsHargaBeli?.t150Harga
           }
		   rows << [
			   id: it.id,
			   status: it.status.toString(),
			   kodePart: it.goods?.m111ID +"    / "+ kla,
			   namaPart: it.goods?.m111Nama,
			   idPart: it.goods?.id,
			   qty: it.t162Qty1,
			   satuan: it.goods?.satuan?.m118Satuan1,
			   vendor: goodsHargaBeli?.vendor?.m121Nama,
			   vendorId: goodsHargaBeli?.vendor?.id,
			   spld: goodsHargaBeli?.vendor?.m121staSPLD,
			   hargaSatuan: goodsHargaBeli?.t150Harga? conversi.toRupiah(goodsHargaBeli?.t150Harga):"",
			   totalHarga: conversi.toRupiah(totalHarga),
		   ]
	   }
	   
	   [sEcho: params.sEcho, iTotalRecords:  0, iTotalDisplayRecords: 0, aaData: rows]
			   
   }
   
   def saveHargaBeli(params) {
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		Request.withTransaction { status ->
			def result = [:]
			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.requestInstance && m.field)
					result.requestInstance.errors.rejectValue(m.field, m.code)
				result.status = "nok"	
				result.error = [ code: m.code, args: ["Request", params.requestId] ]
				return result
			}
			def rd = RequestDetail.get(params.requestId)
			if(!rd)
				return fail(code:"default.not.found.message")
			// Optimistic locking check.
			if(params.version) {
				if(rd.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}
			if(!rd.goods){
				return fail(code:"default.not.updated.message")
			}			
			def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(rd.goods,session.userCompanyDealer,'0')
            def hrgString = (params.hargaSatuan as String).replace(",","")
            params.hargaSatuan = hrgString as Double
			if(goodsHargaBeli){
			} else {
				goodsHargaBeli = new GoodsHargaBeli(
					companyDealer: session.userCompanyDealer,
					goods: rd.goods,
					t150TMT: new Date(),
					t150Harga: params.hargaSatuan as double,
                    dateCreated: params?.dateCreated,
                    lastUpdated: params?.lastUpdated,
                    staDel: "0"
				)

				goodsHargaBeli.save(flush:true)
			}

			// Success.
			result.status = "ok"
			def tanggalRequest = rd.request.t162TglRequest
			def noReferensi = rd.request.t162NoReff
			
			def cTanggal = RequestDetail.createCriteria()
			def resultTanggal = cTanggal.list {
				request{
					eq("t162TglRequest", tanggalRequest)	
				}
                if(params."sCriteria_status"){
                    eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
                }
			}
            def sumTotalPerTanggal = 0
            resultTanggal.each{
                def hargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it.goods,session.userCompanyDealer,'0')
                if(hargaBeli){
                    sumTotalPerTanggal += (it.t162Qty1 * hargaBeli?.t150Harga)
                }
            }
			result.sumTotalPerTanggal = sumTotalPerTanggal
			
			def cNoRef = RequestDetail.createCriteria()
			def resultNoRef = cNoRef.list {
				request{
					eq("t162NoReff", noReferensi)
				}
                if(params."sCriteria_status"){
                    eq("status",RequestStatus.getEnumFromId(params."sCriteria_status"))
                }
			}
            def sumTotalPerReferensi = 0
            resultNoRef.each{
                def hargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it.goods,session.userCompanyDealer,'0')
                if(hargaBeli){
                    sumTotalPerReferensi += (it.t162Qty1 * hargaBeli?.t150Harga)
                }
            }
			result.sumTotalPerReferensi = sumTotalPerReferensi
			return result

		} //end withTransaction
	}  // end update()
   
	def generateNomorDokumenApprovalOrderParts(){
	   def result = [:]
	   def c = ApprovalT770.createCriteria()
	   def results = c.get () {
		   resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
		   projections {
			   max("id","id")
		   }
	   }
	   
	   def m = results.id?:0
	   
	   result.value = String.format('%014d',m+1)
       result.tanggal = datatablesUtilService?.syncTime()?.format("dd-MM-yyyy HH:mm:ss")
	   return result
   }
   
   def requestApprovalOrderPartsApproval(def params) {
		def result = [:]
		def nonSpldreq = []
		def spldReq = [:]
	    def requestList = JSON.parse(params.requestIds)
	   
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		Request.withTransaction { status ->
			def fail = { Map m ->
				status.setRollbackOnly()
				result.status = "nok"
				result.error = [ code: m.code, args: ["Request", params.requestId] ]
				return result
			}
			requestList.each {
				def rd = RequestDetail.get(it)
				if(!rd)
					return fail(code:"default.not.found.message")
				
				rd.lastUpdated = datatablesUtilService?.syncTime()
				rd.status = RequestStatus.VALIDASI_BELUM_APPROVAL
				rd.save()
				
				if(!rd.goods){
				return fail(code:"default.not.updated.message")
				}		

				def vendor = Vendor.findById(params."vendor-${it}" as Long)
                def harga = (params."hargaSatuan-${it}" as String).replace(",","")
                params."hargaSatuan-${it}" = harga
                def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(rd.goods,session.userCompanyDealer,'0')
				if(goodsHargaBeli){
                    goodsHargaBeli?.lastUpdated = datatablesUtilService?.syncTime()
                    if(params."hargaSatuan-${it}") {
                        goodsHargaBeli.t150TMT = new Date()
                        goodsHargaBeli.t150Harga = params."hargaSatuan-${it}" as double
                        goodsHargaBeli.vendor = vendor
                        goodsHargaBeli.staDel = "0"
                        goodsHargaBeli.save(flush: true)
                    } else {
                        goodsHargaBeli.staDel = "1"
                        goodsHargaBeli.save(flush: true)
                    }
				} else {
					goodsHargaBeli = new GoodsHargaBeli(
						companyDealer: session.userCompanyDealer,
						goods: rd.goods,
						t150TMT: new Date(),
						vendor: vendor,
						t150Harga: params.hargaSatuan as double,
						dateCreated:  datatablesUtilService?.syncTime(),
						lastUpdated:  datatablesUtilService?.syncTime(),
						createdBy:  org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        staDel: "0"
					)
					//println("Saving new harga beli")
					goodsHargaBeli.save(flush:true)
				}
				
				def vo = rd.validasiOrder
				
				if(!vo){
					//println "create new validasi order"
					vo = validasiOrderService.createValidasiOrder(rd)
				}
				
				vo.t163HargaSatuan = params."hargaSatuan-${it}" as double
				vo.companyDealer = session?.userCompanyDealer
				vo.t163TotalHarga = rd.t162Qty1 * vo.t163HargaSatuan
				vo.t163NamaUserValidasi = org.apache.shiro.SecurityUtils.subject.principal.toString()
				vo.lastUpdated = datatablesUtilService?.syncTime()
				vo = vo.save(flush:true)
				vo.errors.each{ //println it
				}
				
				if(vendor.m121staSPLD == '0'){
					if(!spldReq."${vendor.m121Nama}"){
						spldReq."${vendor.m121Nama}" = []
					}
					spldReq."${vendor.m121Nama}" << vo 
				} else {
					nonSpldreq << vo.id
				}
			}
			
			
			
			spldReq.each() { key, value ->
				def noPo = generateNomorPO()
                Divisi divisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi
				def po = new PO(vendor: Vendor.findByM121Nama(key),
					t164TglPO: new Date(),
					t164NoPO: noPo,
					t164StaOpenCancelClose: POStatus.OPEN,
					t164xNamaUser: org.apache.shiro.SecurityUtils.subject.principal.toString(),
					t164xNamaDivisi: divisi?.m012NamaDivisi,
                    dateCreated:  datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime(),
                    companyDealer: session?.userCompanyDealer
				)
				po.save()
				
				value.each{
					def poDetail = new PODetail(
						validasiOrder: it,
						t164Qty1: it.requestDetail.t162Qty1,
						t164Qty2: it.requestDetail.t162Qty2,
						t164HargaSatuan: it.t163HargaSatuan,
						t164TotalHarga: it.t163TotalHarga,
						po: po,
						dateCreated:  datatablesUtilService?.syncTime(),
						lastUpdated:  datatablesUtilService?.syncTime()
					)
					poDetail.save()
				}
			}
 
		}
		
		
		
		result.needapproval = nonSpldreq.size()
		result.nonapproval = spldReq.size()
		
		if(nonSpldreq.size()>0){
			def requests = nonSpldreq.join('#')
			
			def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
			if(!kegiatanApproval){
				result.status = "nok"
				result.error = "Kegiatan Approval tidak diketemukan."
				return result
			}
			
			def approval = new ApprovalT770(
				t770FK:requests,
				kegiatanApproval: kegiatanApproval,
				t770NoDokumen: params.t770NoDokumen,
				t770TglJamSend: datatablesUtilService?.syncTime(),
				t770Pesan: params.pesan,
                    companyDealer: session?.userCompanyDealer,
                    dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
			)
			approval.save(flush:true)
			approval.errors.each{ //println it
			}
		}
		result.status = "ok"		
		   
		return result
    }

    def generateNomorPO(){
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def result = [:]
        DateFormat df = new SimpleDateFormat("yyyy")
        def currentDate = new Date().format("yyyy")
        def c = PO.createCriteria()
        def results = c.list {
            eq("companyDealer",session.userCompanyDealer)
            ge("t164TglPO", new Date().parse("dd-MM-yyyy","01-01-"+currentDate))
            lt("t164TglPO", new Date().parse("dd-MM-yyyy","31-12-"+currentDate))
            order("t164NoPO","desc")
        }

        def m = results.size()?:0
        if(m!=0){
            def endNomor =  results.first().t164NoPO.toString().substring(7)
            m = endNomor.toInteger()
        }
        def reg = session?.userCompanyDealer?.m011ID
        def data = reg +"." + new Date().format("yy").toString() + "." +  String.format('%05d',m+1)
//        result.value = String.format('%014d',m+1)
        result = data
        return result
    }
   
   def requestDirectPO(def params){
		def result = [:]
		def spldReq = [:]
	    def requestList = JSON.parse(params.requestIds)
	   
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		Request.withTransaction { status ->
			def fail = { Map m ->
				status.setRollbackOnly()
				result.status = "nok"
				result.error = [ code: m.code, args: ["Request", params.requestId] ]
				return result
			}
			requestList.each {
				def rd = RequestDetail.get(it)
				if(!rd)
					return fail(code:"default.not.found.message")
				
				rd.status = RequestStatus.VALIDASI_BELUM_APPROVAL
				rd.lastUpdated = datatablesUtilService?.syncTime()
				rd.save()
				
				if(!rd.goods){
				return fail(code:"default.not.updated.message")
				}

				def vendor = Vendor.findById(params."vendor-${it}" as Long)
                def harga = (params."hargaSatuan-${it}" as String).replace(",","")
                params."hargaSatuan-${it}" = harga
                def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(rd.goods,session.userCompanyDealer,'0')
				if(goodsHargaBeli){
                    goodsHargaBeli?.lastUpdated = datatablesUtilService?.syncTime()
                    if(params."hargaSatuan-${it}") {
                        goodsHargaBeli.t150TMT = datatablesUtilService?.syncTime()
                        goodsHargaBeli.t150Harga = params."hargaSatuan-${it}" as double
                        goodsHargaBeli.vendor = vendor
                        goodsHargaBeli.staDel = "0"
                        goodsHargaBeli.save(flush: true)
                    } else {
                        goodsHargaBeli.staDel = "1"
                        goodsHargaBeli.save(flush: true)
                    }
				} else {
					goodsHargaBeli = new GoodsHargaBeli(
						companyDealer: session.userCompanyDealer,
						goods: rd.goods,
						t150TMT: datatablesUtilService?.syncTime(),
						vendor: vendor,
						t150Harga: params.hargaSatuan as double,
						dateCreated:  datatablesUtilService?.syncTime(),
						lastUpdated:  datatablesUtilService?.syncTime(),
                        staDel: "0"
					)
					//println("Saving new harga beli")
					goodsHargaBeli.save(flush:true)
				}
				
				def vo = rd.validasiOrder
				
				if(!vo){
					//println "create new validasi order"
					vo = validasiOrderService.createValidasiOrder(rd)
				}
				
				vo.t163HargaSatuan = params."hargaSatuan-${it}" as double
				vo.companyDealer = session?.userCompanyDealer
				vo.t163TotalHarga = rd.t162Qty1 * vo.t163HargaSatuan
				vo.t163NamaUserValidasi = org.apache.shiro.SecurityUtils.subject.principal.toString()
				vo.lastUpdated = datatablesUtilService?.syncTime()
				vo.save(flush:true)
				vo.errors.each{ //println it
				}
				
				if(vendor.m121staSPLD == '0'){
					if(!spldReq."${vendor.m121Nama}"){
						spldReq."${vendor.m121Nama}" = []
					}
					spldReq."${vendor.m121Nama}" << vo
				} 
			}
			
			
			
			spldReq.each() { key, value ->
				def noPo = generateNomorPO()
				def vendor = Vendor.findByM121Nama(key)
                def divisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi
				def po = new PO(vendor: vendor,
					t164TglPO: new Date(),
					t164NoPO: noPo,
					t164StaOpenCancelClose: POStatus.OPEN,
					t164xNamaUser: org.apache.shiro.SecurityUtils.subject.principal.toString(),
					t164xNamaDivisi: divisi?.m012NamaDivisi,
					dateCreated:  datatablesUtilService?.syncTime(),
					lastUpdated:  datatablesUtilService?.syncTime(),
					companyDealer:  session?.userCompanyDealer
				)
				po.save(flush:true)
				po.errors.each{
					//println it
				}
				
				value.each{
					def poDetail = new PODetail(
						validasiOrder: it,
						t164Qty1: it.requestDetail.t162Qty1,
						t164Qty2: it.requestDetail.t162Qty2,
						t164HargaSatuan: it.t163HargaSatuan,
						t164TotalHarga: it.t163TotalHarga,
						po: po,
						dateCreated:  datatablesUtilService?.syncTime(),
						lastUpdated:  datatablesUtilService?.syncTime()
					)
					poDetail.save()
					poDetail.errors.each{
						//println it
					}
				}
			}
 
		}
		
		
		
		result.nonapproval = spldReq.size()
		
		result.status = "ok"		
		   
		return result
   }

	def afterApproval(String fks, 
		StatusApproval staApproval, 
		Date tglApproveUnApprove,	
		String alasanUnApprove,	
		String namaUserApproveUnApprove) {
        def session = RequestContextHolder?.currentRequestAttributes()?.getSession()
        def req = [:]
	fks.split(ApprovalT770.FKS_SEPARATOR).each {
        ValidasiOrder vd = ValidasiOrder.get(it as Long)
		vd.t163StaApproval = staApproval
		vd.t163TglApproveUnApprove = tglApproveUnApprove
		vd.t163AlasanUnApprove = alasanUnApprove
		vd.t163NamaUserApproveUnApprove =  namaUserApproveUnApprove
		vd.lastUpdated = datatablesUtilService?.syncTime()
		vd.save()
        if(staApproval == StatusApproval.APPROVED){
            def goodsHargaBeli=  GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(vd.requestDetail?.goods,session.userCompanyDealer,'0')
            def vendor = goodsHargaBeli?.vendor
            if(vendor){
                if(!req."${vendor?.m121Nama}"){
                    req."${vendor?.m121Nama}" = []
                }
                req."${vendor?.m121Nama}" << vd
            }

        }
	}

        req.each() { key, value ->
            def noPo = generateNomorPO()
            def vendor = Vendor.findByM121Nama(key)
            def divisi = com.kombos.baseapp.sec.shiro.User.findByUsername(namaUserApproveUnApprove).divisi
            def po = new PO(vendor: vendor,
                    t164TglPO: new Date(),
                    t164NoPO: noPo,
                    t164StaOpenCancelClose: POStatus.OPEN,
                    t164xNamaUser: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    t164xNamaDivisi: divisi?.m012NamaDivisi,
                    dateCreated:  datatablesUtilService?.syncTime(),
                    lastUpdated:  datatablesUtilService?.syncTime(),
                    companyDealer:  session?.userCompanyDealer
            )
            po.save(flush:true)
            po.errors.each{
                //println it
            }

            value.each{ValidasiOrder vo ->
                def poDetail = new PODetail(
                        validasiOrder: vo,
                        t164Qty1: vo.requestDetail.t162Qty1,
                        t164Qty2: vo.requestDetail.t162Qty2,
                        t164HargaSatuan: vo.t163HargaSatuan,
                        t164TotalHarga: vo.t163TotalHarga,
                        po: po,
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime()
                )
                poDetail.save()
                poDetail.errors.each{
                    //println it
                }
            }
        }
}

}
