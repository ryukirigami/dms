package com.kombos.parts

import org.springframework.web.context.request.RequestContextHolder

class ValidasiOrderService {

    def datatablesUtilService

    def createValidasiOrder(RequestDetail rd,def idTO) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        ValidasiOrder vo = new ValidasiOrder(
			requestDetail: rd,
            partTipeOrder: PartTipeOrder.get(idTO.toLong()),
            dateCreated: datatablesUtilService?.syncTime(),
            lastUpdated: datatablesUtilService?.syncTime(),
            companyDealer: session?.userCompanyDelaer
		)
		
		vo.save()
		vo.errors.each{
            //println it
        }
		vo
    }

    def createValidasiOrder(RequestDetail rd) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        ValidasiOrder vo = new ValidasiOrder(
                requestDetail: rd,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime(),
                companyDealer: session?.userCompanyDelaer
        )

        vo.save()
        vo.errors.each{
            //println it
        }
        vo
    }
}
