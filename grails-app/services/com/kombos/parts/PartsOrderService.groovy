package com.kombos.parts

import com.kombos.baseapp.sec.shiro.User

class PartsOrderService {
	private static final PARTS_ORDER_BPMN_FLOW = "partsOrder" 
	def grailsApplication
	def runtimeService
	
    def startActiviti(def username, def params) {
		def user = User.findByUsername(username)
		params."companyDealer" = user?.companyDealer?.m011NamaWorkshop
		String sessionUsernameKey = grailsApplication.config.activiti.sessionUsernameKey?:ActivitiConstants.DEFAULT_SESSION_USERNAME_KEY
		params[sessionUsernameKey] = username

		runtimeService.startProcessInstanceByKey(PARTS_ORDER_BPMN_FLOW, params)
    }
	
	def candidateForPartsRequest(def companyDealerNamaWorkshop, def roleName){
//		def ret = []
//				
//		def c = Role.createCriteria()
//		def results = c.get () {
//			companyDealer{
//				eq('m011NamaWorkshop', companyDealerNamaWorkshop)
//			}	
//			eq('name', roleName)
//		}
//		
//		
//		results.users.each {
//			ret << it.username
//		}
//
//		ret
		
		def c = User.createCriteria()
		def results = c.list () {
			companyDealer{
				eq('m011NamaWorkshop', companyDealerNamaWorkshop)
			}
			roles {
				eq('name', roleName)
			}
		}
		
		results*.username
	}
}
