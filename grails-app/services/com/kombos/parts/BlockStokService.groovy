package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.reception.Reception
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class BlockStokService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService
    def jasperService
    def generateCodeService
    def hapusTemp(){
        def del = BlockStok.findByStaData("0")
        if (BlockStok.findByStaData("0")!=null){
            def delList = BlockStok.createCriteria().list {
                eq("staData","0")
            }
            delList*.delete()
        }
    }

    def tambah(params){
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each {
            oList << BlokStokDetail.findById(Long.parseLong(it))
         }
        def add = null
        oList.each{
            add = new BlokStokDetail()
            add?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            add?.lastUpdProcess = "INSERT"
            add?.lastUpdated = datatablesUtilService?.syncTime()
            add?.dateCreated = datatablesUtilService?.syncTime()
            add?.setStaDel("0")
//            add.save()
        }
        return "ok"
    }

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")


        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0


        def c = BlockStok.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            eq("staDel","0")
            eq("companyDealer",params.userCompanyDealer)

            if (params."sCriteria_t0157TglBlockStok" && params."sCriteria_t0157TglBlockStokAkhir") {
                ge("t157TglBlockStok", df.parse(params."sCriteria_t0157TglBlockStok"))
                lt("t157TglBlockStok", df.parse(params."sCriteria_t0157TglBlockStokAkhir")+1)
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }
        def rows = []

    //    results.sort{a,b-> b.t157ID<=>a.t157ID}

        results.each {

            rows << [

                    id : it?.id,

                    idBlockStok: it?.t157ID,

                    tanggalBlockStok :  it.t157TglBlockStok ? it.t157TglBlockStok.format(dateFormat) : "" ,

                    nomorWO : it?.reception?.t401NoWO,

                    tanggalWO: it?.reception?.t401TanggalWO ? it?.reception?.t401TanggalWO.format(dateFormat) : "" ,

                    receptionTanggalWO: it.reception?.t401TanggalWO

            ]
        }


        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        def blockStoks = []

        jsonArray.each {
            blockStoks << BlockStok.get(it)
        }

        blockStoks.each {
            def detail = BlokStokDetail.findAllByBlockStokAndStaDel(it,'0')
            detail.each {
                def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',it.companyDealer)
                if(!goodsStok){
                    def partsStokService = new PartsStokService()
                    partsStokService.newStock(it.goods,it.companyDealer)
                    goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',it.companyDealer)
                }
                def kurangStok2=  PartsStok.get(goodsStok.id)
                kurangStok2?.companyDealer = it.companyDealer
                kurangStok2?.goods = it.goods
                kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - it.t158Qty2
                kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - it.t158Qty2
                kurangStok2?.t131Qty1Free = kurangStok2?.t131Qty1Free - it.t158Qty2
                kurangStok2?.t131Qty2Free = kurangStok2?.t131Qty2Free - it.t158Qty2
                kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                kurangStok2.lastUpdProcess = 'BLOCKSOTOK'
                kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                kurangStok2?.save(flush: true)
                kurangStok2.errors.each {
                    //println it
                }

                it.staDel = '1'
                it.lastUpdProcess = 'DELETE'
                it.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                it.lastUpdated = datatablesUtilService?.syncTime()
                it.save(flush: true)

            }
            it.staDel = '1'
            it.lastUpdProcess = 'DELETE'
            it.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            it.lastUpdated = datatablesUtilService?.syncTime()
            it.save(flush: true)
        }
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = BlokStokDetail.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params.idBlockStok){
               blockStok{
                    eq("id",Long.parseLong(params.idBlockStok))
                }

            }
            eq("staDel","0")
            eq("companyDealer",params.userCompanyDealer)
        }

        def rows = []

        results.sort {
            it.goods.m111ID
        }


        results.each {
            rows << [

                    id : it?.id,

                    kodeparts: it.goods?.m111ID,

                    namaparts: it.goods?.m111Nama,

                 //   qtypicking: PickingSlipDetail?.findByPickingSlip(PickingSlip.findByReception(it.blockStok?.reception))?.t142Qty1,
                    qtyPicking : PickingSlipDetail?.findByGoodsAndStaDel(it.goods,'0').last()?.t142Qty1,

                    satuan1: it.goods?.satuan?.m118Satuan1,

                    qtyblockstok: it.t158Qty1,

                    satuan2: it.goods?.satuan?.m118Satuan1,

                    lokasi: it.location?.m120NamaLocation,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }



    def saveStok(params,CompanyDealer cabang){


        def result = [:]

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def jsonDel = JSON.parse(params.del)
        def jsonChange = JSON.parse(params.change)

        Date dateBlockStok = df.parse(params.tanggal)

        def blockStokDetails = []
        def blockStokDetailsDelete = []

        Reception rec = Reception.get(params.noWO)
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        BlockStok blockStok = new BlockStok()
        blockStok.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        blockStok.lastUpdated = datatablesUtilService.syncTime()
        blockStok.dateCreated = datatablesUtilService.syncTime()
        blockStok.lastUpdProcess = "INSERT"
        blockStok.reception = rec
        blockStok.t157ID = generateCodeService.codeGenerateSequence("T157_ID",cabang)
        blockStok.t157TglBlockStok = dateBlockStok
        blockStok.companyDealer = session?.userCompanyDealer
        blockStok.save(flush:true)


        jsonDel.each {
            blockStokDetailsDelete << BlokStokDetail.get(it)
        }

        blockStokDetailsDelete.each {
            it.delete(flush:true)
        }

        jsonChange.each {
            blockStokDetails << BlokStokDetail.get(it)
        }
        blockStokDetails.each {
            it.blockStok = blockStok
            it.lastUpdated = datatablesUtilService.syncTime()
            it.statusPenyimpanan = "1"
            it.save(flush:true)
        }

        return result
    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BlockStok", params.id]]
            return result
        }

        result.blockStokInstance = BlockStok.get(params.id)

        if (!result.blockStokInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BlockStok", params.id]]
            return result
        }

        result.blockStokInstance = BlockStok.get(params.id)

        if (!result.blockStokInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BlockStok", params.id]]
            return result
        }

        result.blockStokInstance = BlockStok.get(params.id)

        if (!result.blockStokInstance)
            return fail(code: "default.not.found.message")

        try {
            result.blockStokInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }
    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BlockStok", params.id]]
            return result
        }

        result.blockStokInstance = new BlockStok()
        result.blockStokInstance.properties = params

        // success
        return result
    }

    def save(params,CompanyDealer cabang) {
        def result = [:]

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        Date dateBlockStok = df.parse(params.tanggal_dp)
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        BlockStok blockStok = new BlockStok()
        blockStok.t157ID = generateCodeService.codeGenerateSequence("T157_ID",cabang)
        blockStok.t157TglBlockStok = dateBlockStok
        blockStok.companyDealer = cabang
        blockStok.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        blockStok.dateCreated = datatablesUtilService.syncTime()
        blockStok.lastUpdated = datatablesUtilService.syncTime()
        blockStok.reception = Reception.get(params.reception.id)
        blockStok.staDel = "0"
        blockStok.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        blockStok.lastUpdProcess = "INSERT"
        blockStok.companyDealer = session?.userCompanyDealer
        blockStok.save(flush:true)

        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def goods = Goods.get(it)
            if(goods){
                BlokStokDetail blockStokDetail = new BlokStokDetail()
                blockStokDetail.goods = goods
                blockStokDetail.companyDealer = cabang
                blockStokDetail.t158Qty1 = Double.parseDouble(params."qtyBlockStok${it}")
                blockStokDetail.location = Location.get(params."location${it}")
                blockStokDetail.t158AlasanBlockStock = ""
                blockStokDetail.blockStok = blockStok
                blockStokDetail.t158StaDisposal = ""
                blockStokDetail.staDel = "0"
                blockStokDetail.t158Qty2 = Double.parseDouble(params."qtyBlockStok${it}")
                blockStokDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                blockStokDetail.lastUpdProcess = "INSERT"
                blockStokDetail.t158StaDisposal = ""
                blockStokDetail.dateCreated = datatablesUtilService?.syncTime()
                blockStokDetail.lastUpdated = datatablesUtilService?.syncTime()
                blockStokDetail.companyDealer = session?.userCompanyDealer

                blockStokDetail.save(flush: true)

                try {
                    def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',cabang)
                    if(!goodsStok){
                        def partsStokService = new PartsStokService()
                        partsStokService.newStock(goods,cabang)
                        goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',cabang)
                    }
                    def kurangStok2 =  PartsStok.get(goodsStok.id)
                    kurangStok2?.companyDealer = cabang
                    kurangStok2?.goods = goods
                    kurangStok2?.t131Qty1Free = kurangStok2?.t131Qty1Free + blockStokDetail.t158Qty2
                    kurangStok2?.t131Qty2Free = kurangStok2?.t131Qty2Free + blockStokDetail.t158Qty2
                    kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 + blockStokDetail.t158Qty2
                    kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 + blockStokDetail.t158Qty2
                    kurangStok2.staDel = '0'
                    kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    kurangStok2.lastUpdProcess = 'I_BLOCK'
                    kurangStok2.lastUpdated = datatablesUtilService.syncTime()
                    kurangStok2.dateCreated = datatablesUtilService.syncTime()
                    kurangStok2.companyDealer = session?.userCompanyDealer
                    kurangStok2?.save(flush: true)
                    kurangStok2.errors.each {
                        //println it
                    }
                }catch (Exception ss){
                    //println "gada data"
                }
            }
        }
    }


    def update(params) {
        def result = [:]

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        Date dateBlockStok = df.parse(params.tanggal_dp)

        BlockStok blockStok = BlockStok.get(params.idBlockStok)
        blockStok.t157TglBlockStok = dateBlockStok
        blockStok.lastUpdated = new Date()
        blockStok.reception = Reception.get(params.reception.id)
        blockStok.lastUpdProcess = "UPDATE"
        blockStok.lastUpdated = datatablesUtilService?.syncTime()
        blockStok.save(flush:true)
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def goods = Goods.get(it)
           if(goods){
                def blockStokDetail = BlokStokDetail.findByBlockStokAndGoods(blockStok,goods)

                if(blockStokDetail){
                    def awal = blockStokDetail.t158Qty1

                    blockStokDetail.goods = goods
                    blockStokDetail.t158Qty1 = Double.parseDouble(params."qtyBlockStok${it}")
                    blockStokDetail.location = Location.get(params."location${it}")
                    blockStokDetail.t158AlasanBlockStock = ""
                    blockStokDetail.blockStok = blockStok
                    blockStokDetail.t158StaDisposal = ""
                    blockStokDetail.t158Qty2 = Double.parseDouble(params."qtyBlockStok${it}")
                    blockStokDetail.lastUpdProcess = "UPDATE"
                    blockStokDetail.lastUpdated = datatablesUtilService?.syncTime()
                    blockStokDetail.save(flush: true)

                    def akhir = blockStokDetail.t158Qty1
                    def hasil = akhir - awal

                    try {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',params.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(goods,params.userCompanyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',params.userCompanyDealer)
                        }
                        def kurangStok2 =  PartsStok.get(goodsStok.id)
                        kurangStok2?.companyDealer = params.userCompanyDealer
                        kurangStok2?.goods = goods
                        kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free + hasil
                        kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free + hasil
                        kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 + hasil
                        kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 + hasil
                        kurangStok2.staDel = '0'
                        kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        kurangStok2.lastUpdProcess = 'INSERT'
                        kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                        kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                        kurangStok2?.save(flush: true)
                        kurangStok2.errors.each {
                            //println it
                        }
                    }catch (Exception ks){

                    }

                }else{
                    blockStokDetail = new BlokStokDetail()
                    blockStokDetail.goods = goods
                    blockStokDetail.companyDealer = params.userCompanyDealer
                    blockStokDetail.t158Qty1 = Double.parseDouble(params."qtyBlockStok${it}")
                    blockStokDetail.location = Location.get(params."location${it}")
                    blockStokDetail.t158AlasanBlockStock = ""
                    blockStokDetail.blockStok = blockStok
                    blockStokDetail.t158StaDisposal = ""
                    blockStokDetail.t158Qty2 = Double.parseDouble(params."qtyBlockStok${it}")
                    blockStokDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    blockStokDetail.lastUpdProcess = "INSERT"
                    blockStokDetail.t158StaDisposal = ""
                    blockStokDetail.staDel = "0"
                    blockStokDetail.lastUpdated = datatablesUtilService?.syncTime()
                    blockStokDetail.dateCreated = datatablesUtilService?.syncTime()
                    blockStokDetail.save(flush: true)

                    try {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',params.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(goods,params.userCompanyDealer)
                            goodsStok =  PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',params.userCompanyDealer)
                        }
                        def kurangStok2 =  PartsStok.get(goodsStok.id)
                        kurangStok2?.companyDealer = params.userCompanyDealer
                        kurangStok2?.goods = goods
                        kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free + blockStokDetail.t158Qty2
                        kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free + blockStokDetail.t158Qty2
                        kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 + blockStokDetail.t158Qty2
                        kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 + blockStokDetail.t158Qty2
                        kurangStok2.staDel = '0'
                        kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        kurangStok2.lastUpdProcess = 'up_blokctock'
                        kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                        kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                        kurangStok2.companyDealer = params?.userCompanyDealer
                        kurangStok2?.save(flush: true)
                        kurangStok2.errors.each {
                            //println it
                        }
                    }catch (Exception j){

                    }
                }
            }
        }
    }



    def doReport(def params)
    {

        def idBlockStok = params.idBlockStok
        def blockStok = BlockStok.get(idBlockStok)

        def data = [nomorSlip: blockStok?.id,tanggalBlockStok: blockStok?.t157TglBlockStok, nomorWO: blockStok?.reception?.t401NoWO, tanggalWO: blockStok?.reception?.t401TanggalWO]
        def reportData = new ArrayList();


        def results = BlokStokDetail.findAllByBlockStokAndStaDel(blockStok,'0')


        results.sort {
            it.goods.m111ID
        }

        int count = 0

        results.each {
            count = count + 1

            data.put("noUrut", count)
            data.put("kodeParts", it.goods?.m111ID)
            data.put("namaParts", it.goods?.m111Nama)
            data.put("satuan",it.goods?.satuan?.m118Satuan1)
            data.put("qtyDiterima",it.t158Qty1 )
            data.put("kodeLokasi", it.location?.m120ID + " | " + it.location?.m120NamaLocation)
        }

        data.put("tanggal", new Date().format("DD-MM-YYYY"))

        reportData.add(data)
        def file = File.createTempFile("blockStokSlip_", "." + "pdf");
        file.deleteOnExit()

        def reportDef = new JasperReportDef(name:'blockStok.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())
        return file
    }



    def editBlockStokDetail(def params){
        def blockStokDetail = BlokStokDetail.findById(params.idBlockStokDetail)
        def location = Location.findById(params.location)
        def qtyBlockStok = Double.parseDouble(params.qtyBlockStok)

        blockStokDetail.t158Qty1 = qtyBlockStok
        blockStokDetail.location = location
        blockStokDetail.lastUpdated = datatablesUtilService?.syncTime()
        blockStokDetail.save(flush: true)
    }


    def dTBlockStokDetailEdit(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def c = BlokStokDetail.createCriteria()
        def results = c.list () {
            if(params.idBlockStok){
                blockStok{
                    eq("id",Long.parseLong(params.idBlockStok))
                }

            }
        }

        def rows = []


        results.sort {
            it.goods.m111ID
        }


        results.each {
            rows << [

                    id : it?.goods?.id,

                    kodeParts: it.goods?.m111ID,

                    namaParts: it.goods?.m111Nama,

                    qtyPicking : PickingSlipDetail?.findByGoods(it.goods)?.t142Qty1,

                    satuan1: it.goods?.satuan?.m118Satuan1,

                    qtyBlockStock: it.t158Qty1?:0,

                    satuan2: it.goods?.satuan?.m118Satuan1,

                    location: it.location?.m120NamaLocation

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
    }



}