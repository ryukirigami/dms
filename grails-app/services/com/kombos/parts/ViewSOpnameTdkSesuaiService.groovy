package com.kombos.parts

import com.kombos.baseapp.AppSettingParam

class ViewSOpnameTdkSesuaiService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = StokOPName.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer);
            eq("t132ID",params."sCriteria_t132ID")
            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            String satuan1 = it.goods.goods.satuan.m118KodeSatuan
            String satuan2 = it.goods.goods.satuan.m118KodeSatuan
            boolean cek=false;
            if(it.t132Jumlah11==null){
                cek=true;
            } else if(it.t132Jumlah11!=null && it.t132Jumlah12==null){
                if(it.t132Jumlah11!=it.t132Jumlah1Stock || it.t132Jumlah21!=it.t132Jumlah2Stock){
                    cek=true;
                }
            } else if(it.t132Jumlah11!=null && it.t132Jumlah12!=null && it.t132Jumlah13==null){
                if(it.t132Jumlah12!=it.t132Jumlah1Stock || it.t132Jumlah22!=it.t132Jumlah2Stock){
                    cek=true;
                }
            }else{
                if(it.t132Jumlah13!=it.t132Jumlah1Stock || it.t132Jumlah23!=it.t132Jumlah2Stock){
                    cek=true;
                }
            }

            if(cek){
                rows << [


                        id: it.id,

                        t132ID: it.t132ID,

                        t132Tanggal: it.t132Tanggal?it.t132Tanggal.format(dateFormat):"",

                        lokasigoods: it.goods.location.m120NamaLocation,

                        namagoods : it.goods.goods.m111Nama,

                        kodegoods : it.goods.goods.m111ID,

                        satuangoods1 : satuan1 ,

                        satuangoods2 : satuan2 ,

                        icc : it.goods.location.parameterICC.m155NamaICC ,

                        qty1 :  it.t132Jumlah1Stock,

                        qty2 :  it.t132Jumlah2Stock,

                        t132Jumlah1Stock: it.t132Jumlah1Stock,

                        t132Jumlah2Stock: it.t132Jumlah2Stock,

                        location: it.location,

                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

}