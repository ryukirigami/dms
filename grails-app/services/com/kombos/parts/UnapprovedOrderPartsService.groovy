package com.kombos.parts

import com.kombos.baseapp.AppSettingParam

class UnapprovedOrderPartsService {

   boolean transactional = false
	
   def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
   def appSettingParamTimeFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.TIME_FORMAT)
   def conversi = new Konversi()

   def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def searchCol = params."sCriteria_column"
		def searchVal = params."sCriteria_value"
		
		def c = RequestDetail.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
					
			if(params."sCriteria_tanggalTo"){
				request{
					le("t162TglRequest",params."sCriteria_tanggalTo")
				}
			}
			if(params."sCriteria_tanggalFrom"){
				request{
					ge("t162TglRequest",params."sCriteria_tanggalFrom")
				}
			}

            request{
                eq("companyDealer",params?.companyDealer)
            }
			if(searchCol){
				switch(searchCol){
					case 'noReferensi':
						request {
							eq('t162NoReff', searchVal)
						}
						break
					case 'kodePart':
						goods{
							eq('m111ID', searchVal)
						}
						break
					case 'namaPart':
						goods{
							eq('m111Nama', searchVal)
						}
						break
					case 'qty':						
						eq('t162Qty1', searchVal as double)
						break
					case 'satuan':
						goods{
							satuan{
								eq('m118Satuan1', searchVal)
							}
						}
						break
					case 'vendor':
                        goods{
                            goodsHargaBeli {
                                vendor {
                                    eq('m121Nama', searchVal)
                                }
                            }
						}
						break
					case 'hargaSatuan':
						validasiOrder{
							eq('t163HargaSatuan', searchVal as double)
						}
						break
					case 'totalHarga':
						validasiOrder{
							eq('t163TotalHarga', searchVal as double)
						}
						break
				}
			}
			validasiOrder{
				eq('t163StaApproval', StatusApproval.WAIT_FOR_APPROVAL)
			}
		}
		def rows = []
		
	   results.each {
           def goodsHargaBeli=  GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.goods, params.companyDealer, '0')
			rows << [
				id: it.id,
				tanggalRequest: it.request?.t162TglRequest?.format(dateFormat),
				noReferensi: it.request?.t162NoReff?:"",
				kodePart: it.goods?.m111ID?:"",
				namaPart: it.goods?.m111Nama?:"",
				qty: it.t162Qty1,
				satuan: it.goods?.satuan?.m118Satuan1,
				vendor: goodsHargaBeli?.vendor?.m121Nama,
				hargaSatuan: it.validasiOrder?.t163HargaSatuan ? conversi.toRupiah(it.validasiOrder?.t163HargaSatuan) : 0,
				totalHarga: it.validasiOrder?.t163TotalHarga ? conversi.toRupiah(it.validasiOrder?.t163TotalHarga) : 0,
				
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	}

   def show(params) {
	   String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
	   String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
	   
	   def result = [:]
	   def fail = { Map m ->
		   result.error = [ code: m.code, args: ["Request", params.id] ]
		   return result
	   }

	   result.requestInstance = Request.get(params.id)
	   result.dateTimeFormat = dateFormat + " " + timeFormat

	   if(!result.requestInstance)
		   return fail(code:"default.not.found.message")

	   // Success.
	   return result
   }
}
