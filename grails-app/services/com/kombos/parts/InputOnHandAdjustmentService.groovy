package com.kombos.parts

import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User
import com.kombos.maintable.ApprovalT770

class InputOnHandAdjustmentService implements AfterApprovalInterface {
    def datatablesUtilService
    def requestTo(def username){
        def ret = []
        def user = User.findByUsername(username)

        def companyDealer = user.companyDealer

        def kepalaBengkel = Role.findByName("KEPALA_BENGKEL")

        kepalaBengkel.users.each {
            ret << it.username
            //println("Add: " + it.username)
        }

        ret
    }

    def assignTo(def username){
        def ret = []
        def user = User.findByUsername(username)

        def companyDealer = user.companyDealer

        def teknisi = Role.findByName("TEKNISI")

        teknisi.users.each {
            ret << it.username
            //println("Add: " + it.username)
        }

        ret
    }
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            //println "masuk after : "
            def cf = PartsAdjust.get(it as Long)
            def rd = PartsAdjDetail.findAllByPartsAdjust(cf)
            rd.each {
                def pdd = PartsAdjDetail.get(it.id as Long)
                pdd.statusApprove = staApproval
                if(staApproval==StatusApproval.APPROVED){
                    try{
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(pdd.goods,'0',pdd?.partsAdjust?.companyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(pdd.goods,pdd?.partsAdjust?.companyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(pdd.goods,'0',pdd?.partsAdjust?.companyDealer)
                        }
                        def kurangStok =  PartsStok.get(goodsStok.id)
                        kurangStok?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        kurangStok?.lastUpdProcess = 'ONHAND'
                        kurangStok?.t131Qty1 = pdd.t146JmlAkhir1
                        kurangStok?.t131Qty2 = pdd.t146JmlAkhir1
                        kurangStok?.t131Qty1Free = pdd.t146JmlAkhir1
                        kurangStok?.t131Qty2Free = pdd.t146JmlAkhir1
                        kurangStok?.t131Qty1Reserved = 0
                        kurangStok?.t131Qty2Reserved = 0
                        kurangStok?.t131Qty1WIP = 0
                        kurangStok?.t131Qty2WIP = 0
                        kurangStok?.t131Qty1BlockStock = 0
                        kurangStok?.t131Qty2BlockStock =0
                        kurangStok?.t131LandedCost = 0
                        kurangStok?.lastUpdated = datatablesUtilService?.syncTime()
                        kurangStok?.save(flush: true)
                        kurangStok.errors.each {
                            //println it
                        }
                    }catch(Exception ex){

                    }
                }
                pdd.save()
            }
        }
    }
}
