package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KalenderKerja
import com.kombos.baseapp.AppSettingParam
import org.apache.shiro.session.Session

import java.text.SimpleDateFormat

class ICCService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def hitungICC(def params,def companyDealerId){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy")

        CompanyDealer companyDealer = CompanyDealer.get(companyDealerId)
        Date tanggalAkhir = format.parse(params.tanggal)

        Calendar cal = Calendar.getInstance()
        cal.setTime(tanggalAkhir)
        cal.add(Calendar.YEAR, 1)
        Date tanggalDAD =  cal.getTime()

        int working =  tanggalDAD - tanggalAkhir
        def jmlhKalenderKerja = KalenderKerja.findAllByM031IDBetweenAndCompanyDealerAndM031StaLibur(tanggalAkhir,tanggalDAD,companyDealer,"N")?.size()?:0

        //example DAD = Quantity Requst(120 hari)/ 120 hari  * Targer Working Days/12
        //DAD
        //ic gudang it.goods?.partsStok?.t131Qty1?:"0" as int

        MIP mip = MIP.findByM160TglBerlakuLessThanAndCompanyDealer(tanggalAkhir,companyDealer)

        if(!mip)
            mip = MIP.findByCompanyDealer(companyDealer)
        def rangeDAD = mip?.m160RangeDAD?:0.0
        def tanggalAwal = tanggalAkhir - rangeDAD.toInteger()

        def requestFreq = Request.findAllByT162TglRequestBetween(tanggalAwal,tanggalAkhir)
        def requestFreq2 = Request.findAllByT162TglRequestBetween(tanggalAwal,tanggalAkhir)
        def res = []
        def listICC = []
        List listGoods = new ArrayList<Goods>()

        def iccBefore = ICC.findAllByCompanyDealer(companyDealer)
        iccBefore.each {
            it.staDel = "0"
            it.save(flush:true)
        }

        //Algorima perhitungan ICC
        requestFreq.each {
            def reqDetails = RequestDetail.findAllByRequest(it)


           reqDetails.each {

               def count = 0
               def goods = it.goods


            requestFreq2.each {
                def reqDetails2 = RequestDetail.findAllByRequest(it)
                reqDetails2.each {
                    if(goods.m111ID == it.goods.m111ID)
                        count = count + 1

                }
            }




               if(!listGoods.contains(goods)){
                //   def stokGudang = it.goods?.partsStok?.t131Qty1?:"0" as double
                   def dad = (count / rangeDAD) * (jmlhKalenderKerja / 12)
                   listICC << [
                           kodePart : it.goods.m111ID,
                           namaPart : it.goods.m111Nama,
                           iccGudang : KlasifikasiGoods.findByGoods(it?.goods)?.location?.parameterICC?:"",
                           iccSuggest : ParameterICC.findByM155NilaiDAD1LessThanAndM155NilaiDAD2GreaterThan(dad, dad)?.m155KodeICC?:""
                   ]

                   listGoods.add(goods)
                   def icc = new ICC()
                   icc.staDel = "0"
                   icc.companyDealer = companyDealer
                   icc.goods = it.goods
                   icc.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                   icc.lastUpdProcess = "INSERT"
                   icc.dateCreated = new Date()
                   icc.parameterICC = ParameterICC.findByM155NilaiDAD1LessThanAndM155NilaiDAD2GreaterThan(stokGudang, stokGudang)?: ParameterICC.first()
                   icc.t155DAD = dad.toInteger()
                   icc.t155Tanggal = tanggalAkhir
                   icc.save()

               }else{
          //        //println "goods sudah ada"
             }


           }
        }

        return listICC

    }

}