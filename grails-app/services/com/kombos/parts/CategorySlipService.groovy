package com.kombos.parts

import com.kombos.baseapp.AppSettingParam

class CategorySlipService  {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params,session) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = CategorySlip.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",session.userCompanyDealer)
            if (params."sCriteria_categoryCode") {
                ilike("categoryCode", "%"+params."sCriteria_categoryCode"+"%")
            }

            if (params."sCriteria_name") {
                ilike("name", "%" + (params."sCriteria_name" as String) + "%")
            }

            if (params."sCriteria_accountNumber") {
                ilike("accountNumber", "%" + (params."sCriteria_accountNumber" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    categoryCode: it.categoryCode,

                    name: it.name,

                    accountNumber: it.accountNumber.accountNumber,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CategorySlip", params.id]]
            return result
        }

        result.categorySlipInstance = CategorySlip.get(params.id)

        if (!result.categorySlipInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CategorySlip", params.id]]
            return result
        }

        result.categorySlipInstance = CategorySlip.get(params.id)

        if (!result.categorySlipInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CategorySlip", params.id]]
            return result
        }

        result.categorySlipInstance = CategorySlip.get(params.id)

        if (!result.categorySlipInstance)
            return fail(code: "default.not.found.message")

        try {
            result.categorySlipInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        CategorySlip.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.categorySlipInstance && m.field)
                    result.categorySlipInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["CategorySlip", params.id]]
                return result
            }

            result.categorySlipInstance = CategorySlip.get(params.id)

            if (!result.categorySlipInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.categorySlipInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }


            result.categorySlipInstance.properties = params

            if (result.categorySlipInstance.hasErrors() || !result.categorySlipInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CategorySlip", params.id]]
            return result
        }

        result.categorySlipInstance = new CategorySlip()
        result.categorySlipInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.categorySlipInstance && m.field)
                result.categorySlipInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["CategorySlip", params.id]]
            return result
        }

        result.categorySlipInstance = new CategorySlip(params)

        if (result.categorySlipInstance.hasErrors() || !result.categorySlipInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def categorySlip = CategorySlip.findById(params.id)
        if (categorySlip) {
            categorySlip."${params.name}" = params.value
            categorySlip.save()
            if (categorySlip.hasErrors()) {
                throw new Exception("${categorySlip.errors}")
            }
        } else {
            throw new Exception("CategorySlip not found")
        }
    }

}