package com.kombos.parts

import com.kombos.baseapp.AppSettingParam

class CustomerSalesService{
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = CustomerSales.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_alamat") {
                ilike("alamat", "%" + (params."sCriteria_alamat" as String) + "%")
            }
            if (params."sCriteria_alamatNpwp") {
                ilike("alamatNpwp", "%" + (params."sCriteria_alamatNpwp" as String) + "%")
            }
            if (params."sCriteria_customerCode") {
                ilike("customerCode", "%" + (params."sCriteria_customerCode" as String) + "%")
            }
            if (params."sCriteria_nama") {
                ilike("nama", "%" + (params."sCriteria_nama" as String) + "%")
            }
            if (params."sCriteria_phoneNumber") {
                ilike("phoneNumber", "%" + (params."sCriteria_phoneNumber" as String) + "%")
            }
            if (params."sCriteria_kodePos") {
                ilike("kodePos", "%" + (params."sCriteria_kodePos" as String) + "%")
            }
            if (params."sCriteria_npwp") {
                ilike("npwp", "%" + (params."sCriteria_npwp" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {

            rows << [
                id : it.id,
                customerCode : it.customerCode,
                nama : it.nama,
                phoneNumber : it.phoneNumber,
                alamat : it.alamat,
                kodePos : it.kodePos,
                npwp : it.npwp,
                alamatNpwp : it.alamatNpwp

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CustomerSales", params.id]]
            return result
        }

        result.customerSalesInstance = CustomerSales.get(params.id)

        if (!result.customerSalesInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CustomerSales", params.id]]
            return result
        }

        result.customerSalesInstance = CustomerSales.get(params.id)

        if (!result.customerSalesInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CustomerSales", params.id]]
            return result
        }

        result.customerSalesInstance = CustomerSales.get(params.id)

        if (!result.customerSalesInstance)
            return fail(code: "default.not.found.message")

        try {
            result.customerSalesInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        CustomerSales.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.customerSalesInstance && m.field)
                    result.customerSalesInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["CustomerSales", params.id]]
                return result
            }

            result.customerSalesInstance = CustomerSales.get(params.id)

            if (!result.customerSalesInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.customerSalesInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.customerSalesInstance.properties = params

            if (result.customerSalesInstance.hasErrors() || !result.customerSalesInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CustomerSales", params.id]]
            return result
        }

        result.customerSalesInstance = new CustomerSales()
        result.customerSalesInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.customerSalesInstance && m.field)
                result.customerSalesInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["CustomerSales", params.id]]
            return result
        }

        result.customerSalesInstance = new CustomerSales(params)
        result.customerSalesInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        result.customerSalesInstance.setLastUpdProcess("INSERT")
        result.customerSalesInstance.setStaDel("0")
        if (result.customerSalesInstance.hasErrors() || !result.customerSalesInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def customerSales = CustomerSales.findById(params.id)
        if (customerSales) {
            customerSales."${params.name}" = params.value
            customerSales.save()
            if (customerSales.hasErrors()) {
                throw new Exception("${customerSales.errors}")
            }
        } else {
            throw new Exception("CustomerSales not found")
        }
    }

}