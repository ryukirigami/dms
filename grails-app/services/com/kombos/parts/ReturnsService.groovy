package com.kombos.parts



import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class ReturnsService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Returns.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_t017Tanggal" && params."sCriteria_t017Tanggal2") {
                ge("t172TglJamReturn", params."sCriteria_t017Tanggal")
                lt("t172TglJamReturn", params."sCriteria_t017Tanggal2" + 1)
            }

            ilike("status","1")
            eq("staDel","0")

            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("vendor", "vendor")
                groupProperty("t172ID", "t172ID")
                groupProperty("t172TglJamReturn", "t172TglJamReturn")
                groupProperty("t172PetugasReturn", "t172PetugasReturn")
            }

        }
        def rows = []

        results.each {
            rows << [
                    t172TglJamReturn: it.t172TglJamReturn?.format(dateFormat),
                    vendor: it.vendor.m121Nama,
                    t172ID: it.t172ID,
                    t172PetugasReturn: it.t172PetugasReturn,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Returns.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            vendor{
                ilike("m121Nama","%" + params.vendor+ "%")
            }

            ilike("t172ID","%" + params.t172ID+ "%")

            Date date41 = df.parse(params.t172TglJamReturn)
            if (params.t172TglJamReturn) {
                ge("t172TglJamReturn", date41)
                lt("t172TglJamReturn", date41 + 1)
            }



            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("goodsReceive", "goodsReceive")
            }
        }
        def rows = []

        results.each {
            rows << [
                    t167NoReff: it.goodsReceive.t167NoReff,

                    t167TglReff: it.goodsReceive.t167TglReff?.format(dateFormat),

                    vendor : params.vendor,
                    t172ID : params.t172ID,
                    t172TglJamReturn: params.t172TglJamReturn,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Returns.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            goodsReceive{
                ilike("t167NoReff", params.t167NoReff )
            }
            eq("staDel","0")
            vendor{
                ilike("m121Nama","%" + params.vendor+ "%")
            }

            Date date41 = df.parse(params.t172TglJamReturn)
            if (params.t172TglJamReturn) {
                ge("t172TglJamReturn", date41)
                lt("t172TglJamReturn", date41 + 1)
            }
        }

        def rows = []

        results.each {
            //println "asief " +  GoodsReceiveDetail.findByGoodsAndInvoice(it.goods,it.invoice)?.poDetail.po?.t164NoPO
            //println it.invoice
            rows << [
                    id: it.id,
                    kodePart: it.goods?.m111ID,
                    namaPart: it.goods?.m111Nama,
                    noPO: GoodsReceiveDetail.findByGoodsAndInvoice(it.goods,it.invoice)?.poDetail.po?.t164NoPO,
                    tanggalPO: it.invoice?.po?.t164TglPO?.format(dateFormat),
                    tipeOrder: PODetail.findByPo(it.invoice?.po)?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                    t172Qty1Return: it.t172Qty1Return,
                    t167TglJamReceive: it.goodsReceive.t167TglJamReceive?.format(dateFormat),
                    vendor : params.vendor,
                    t172ID : params.t172ID,
                    t172TglJamReturn: params.t172TglJamReturn,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def show(params) {

        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Returns", params.id]]
            return result
        }

        result.returnsInstance = Returns.get(params.id)

        if (!result.returnsInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Returns", params.id]]
            return result
        }

        result.returnsInstance = Returns.get(params.id)

        if (!result.returnsInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Returns", params.id]]
            return result
        }

        result.returnsInstance = Returns.get(params.id)

        if (!result.returnsInstance)
            return fail(code: "default.not.found.message")

        try {
            result.returnsInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }
    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            String data = it
            def dt = null
            def gr = null
            def returns = null
            if(data.contains('.')){
                dt = data.split("\\.")
                gr = GoodsReceive.findByT167NoReff(dt[1].trim())
            }

            try{
                returns = Returns.findById(it)
            }catch(Exception a){

            }

            if(gr){
                Returns.findAllByGoodsReceiveAndT172IDAndStaDel(gr,dt[0].trim(),'0').each{
                    def ubh = Returns.get(it.id)
                    def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods,'0',params.companyDealer)
                    if(!goodsStok){
                        def partsStokService = new PartsStokService()
                        partsStokService.newStock(ubh?.goods,params.companyDealer)
                        goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods,'0',params.companyDealer)
                    }
                        def kurangStok2 =  PartsStok.get(goodsStok.id)
                        kurangStok2?.companyDealer = kurangStok2?.companyDealer
                        kurangStok2?.goods = ubh?.goods
                        kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free + ubh.t172Qty1Return
                        kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free + ubh.t172Qty1Return
                        kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 + ubh.t172Qty1Return
                        kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 + ubh.t172Qty1Return
                        kurangStok2.staDel = '0'
                        kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        kurangStok2.lastUpdProcess = 'RETURNSSMASS'
                        kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                        kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                        kurangStok2?.save(flush: true)
                        kurangStok2.errors.each {
                            //println it
                        }


                    ubh?.staDel = '1'
                    ubh?.lastUpdProcess = 'DELETE'
                    ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    ubh?.save(flush: true)
                }
            }else if(returns){
                def ubh = Returns.get(it)
                def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods,'0',params.companyDealer)
                if(!goodsStok){
                    def partsStokService = new PartsStokService()
                    partsStokService.newStock(ubh?.goods,params.companyDealer)
                    goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods,'0',params.companyDealer)
                }
                    def kurangStok2=  PartsStok.get(goodsStok.id)
                    kurangStok2?.companyDealer = kurangStok2?.companyDealer
                    kurangStok2?.goods = ubh?.goods
                    kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free + ubh.t172Qty1Return
                    kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free + ubh.t172Qty1Return
                    kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 + ubh.t172Qty1Return
                    kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 + ubh.t172Qty1Return
                    kurangStok2.staDel = '0'
                    kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    kurangStok2.lastUpdProcess = 'RETURNSSMASS'
                    kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                    kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                    kurangStok2?.save(flush: true)
                    kurangStok2.errors.each {
                        //println it
                    }


                ubh?.staDel = '1'
                ubh?.lastUpdProcess = 'DELETE'
                ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ubh?.save(flush: true)
            }else{
                try{
                    Returns.findAllByT172IDAndStaDel(it,'0').each{
                        def ubh = Returns.get(it.id)
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods,'0',params?.companyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(ubh?.goods,params.companyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods,'0',params.companyDealer)
                        }
                            def kurangStok =  PartsStok.get(goodsStok.id)

                            kurangStok?.staDel = '1'
                            kurangStok?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok?.lastUpdProcess = 'UPDATE'
                            kurangStok?.save(flush: true)

                            def kurangStok2 =  new PartsStok()
                            kurangStok2?.companyDealer = kurangStok.companyDealer
                            kurangStok2?.goods = ubh?.goods
                            kurangStok2?.t131Qty1Free = kurangStok.t131Qty1Free + ubh.t172Qty1Return
                            kurangStok2?.t131Qty2Free = kurangStok.t131Qty2Free + ubh.t172Qty1Return
                            kurangStok2?.t131Qty1Reserved = kurangStok.t131Qty1Reserved
                            kurangStok2?.t131Qty2Reserved = kurangStok.t131Qty2Reserved
                            kurangStok2?.t131Qty1WIP = kurangStok.t131Qty1WIP
                            kurangStok2?.t131Qty2WIP = kurangStok.t131Qty2WIP
                            kurangStok2?.t131Qty1BlockStock = kurangStok.t131Qty1BlockStock
                            kurangStok2?.t131Qty2BlockStock = kurangStok.t131Qty2BlockStock
                            kurangStok2?.t131LandedCost = kurangStok.t131LandedCost
                            kurangStok2?.t131Qty1 = kurangStok?.t131Qty1 + ubh.t172Qty1Return
                            kurangStok2?.t131Qty2 = kurangStok?.t131Qty2 + ubh.t172Qty1Return
                            kurangStok2.staDel = '0'
                            kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok2.lastUpdProcess = 'INSERT'
                            kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                            kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                            kurangStok2?.save(flush: true)
                            kurangStok2.errors.each {
                                //println it
                            }


                        ubh?.staDel = '1'
                        ubh?.lastUpdProcess = 'DELETE'
                        ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        ubh?.save(flush: true)
                    }
                }catch(Exception a){

                }

            }
        }

    }
    def update(params) {

        Returns.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.returnsInstance && m.field)
                    result.returnsInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Returns", params.id]]
                return result
            }

            result.returnsInstance = Returns.get(params.id)

            if (!result.returnsInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.returnsInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            //result.returnsInstance.properties = params


            result.returnsInstance?.t172Qty1Return  = Double.parseDouble(params.t172Qty1Return)
            result.returnsInstance?.lastUpdated  = datatablesUtilService?.syncTime()

            if (result.returnsInstance.hasErrors() || !result.returnsInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Returns", params.id]]
            return result
        }

        result.returnsInstance = new Returns()
        result.returnsInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.returnsInstance && m.field)
                result.returnsInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Returns", params.id]]
            return result
        }

        result.returnsInstance = new Returns(params)

        if (result.returnsInstance.hasErrors() || !result.returnsInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def returns = Returns.findById(params.id)
        if (returns) {
            returns."${params.name}" = params.value
            returns.save()
            if (returns.hasErrors()) {
                throw new Exception("${returns.errors}")
            }
        } else {
            throw new Exception("Returns not found")
        }
    }

}