package com.kombos.parts

import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.finance.PemakaianPartsSendiriJournalService
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class SupplySlipOwnService implements AfterApprovalInterface {

    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SupplySlipOwn.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer)
            order("ssDate", "desc")
        }

        def rows = []

        def isApprove = ["Waiting For Approval","Approved","UnApproved"]

        results.each {
            rows << [

                    id: it.id,

                    ssNumber: it.ssNumber,

                    ssDate: it.ssDate.format("dd-MM-yyyy"),

                    categorySlip: it.categorySlip.name,

                    staApprove: isApprove[it?.staApprove?.toInteger()]
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SalesOrderDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            sorNumber{
                eq("id",params.sorNumber.toLong())
            }

        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kodeGoods: it.materialCode.m111ID,

                    namaGoods: it.materialCode.m111Nama,

                    qty: it.quantity

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesPartList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsStok.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(exist.size()>0){
                goods{
                    not {
                        or {
                            exist.each {
                                eq('id', it as long)
                            }
                        }
                    }
                }
            }
            eq("staDel","0")
            not {
                eq("t131Qty1Free",0.toDouble())
            }
            goods{
                if (params."sCriteria_m111ID") {
                    ilike("m111ID", "%"+params."sCriteria_m111ID"+"%")
                }

                if (params."sCriteria_m111Nama") {
                    ilike("m111Nama", "%"+params."sCriteria_m111Nama"+"%")
                }

                satuan{
                    if (params."sCriteria_satuan") {
                        ilike("m118Satuan1", "%"+params."sCriteria_satuan"+"%")
                    }
                }

            }
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_stok") {
                eq("t131Qty1", params."sCriteria_stok".toDouble())
            }

        }

        def rows = []

        results.each {
            def harga = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it.goods,"0",new Date())
            rows << [

                    id: it.goods.id,

                    m111ID: it.goods.m111ID,

                    m111Nama: it.goods.m111Nama,

                    satuan: it.goods.satuan.m118Satuan1,

                    qty: it.t131Qty1Free,

                    harga : harga ? harga?.t151HargaDenganPPN : 0

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def massDelete(def params){
        def data = "oke"
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def hapus = null
            try {
                hapus = SupplySlipOwn.get(it)
            }catch (e){

            }
            if(hapus.staApprove=='1'){
                data = "not"
            }
            else {
                def so = SupplySlipOwn.findById(it.toLong())
                def details = SupplySlipOwnDetail.findAllBySupplySlipOwnAndStaDel(so, "0")
                details.each {
                    def ubh = SupplySlipOwnDetail.get(it.id)
                    ubh?.staDel = '1'
                    ubh?.lastUpdProcess = 'DELETE'
                    ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    ubh?.lastUpdated = datatablesUtilService?.syncTime()
                    ubh?.save(flush: true)

                    if (so.staApprove != "1") {
                        try {

                            def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods, '0', params?.companyDealer)
                            if (!goodsStok) {
                                def partsStokService = new PartsStokService()
                                partsStokService.newStock(ubh?.goods, params?.companyDealer)
                                goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh?.goods, '0', params?.companyDealer)
                            }
                            def kurangStok = PartsStok.get(goodsStok.id)
                            kurangStok?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok?.lastUpdProcess = 'U_SLIP'
                            kurangStok?.lastUpdated = datatablesUtilService?.syncTime()
                            kurangStok?.t131Qty1Free = kurangStok.t131Qty1Free + ubh?.qty
                            kurangStok?.t131Qty2Free = kurangStok.t131Qty2Free + ubh?.qty
                            kurangStok?.t131Qty1 = kurangStok.t131Qty1 + ubh?.qty
                            kurangStok?.t131Qty2 = kurangStok.t131Qty1 + ubh?.qty
                            kurangStok?.save(flush: true)
                        } catch (Exception a) {

                        }
                    }
                }
                def ubh2 = SupplySlipOwn.get(it)
                ubh2?.staDel = '1'
                ubh2?.lastUpdProcess = 'DELETE'
                ubh2?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ubh2?.lastUpdated = datatablesUtilService?.syncTime()
                ubh2?.save(flush: true)
            }
        }
        return data
    }

    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        if(staApproval==StatusApproval.APPROVED){
            def supplySlip = SupplySlipOwn.get(fks.toLong())
            supplySlip.staApprove = "1"
            supplySlip.save(flush: true)
            def details = SupplySlipOwnDetail.findAllBySupplySlipOwnAndStaDel(supplySlip,"0")
            def cd = supplySlip?.companyDealer
            details.each {
                try {
                    def kurangStok2 =  PartsStok.findByGoodsAndCompanyDealerAndStaDel(it.goods,cd,"0")
                    def spplyDetail = it

                    if(!kurangStok2){
                        def partsStokService = new PartsStokService()
                        partsStokService.newStock(it.goods,cd)
                        kurangStok2 = PartsStok.findByGoodsAndCompanyDealerAndStaDel(it.goods,cd,"0")
                    }
                        kurangStok2.properties = kurangStok2.properties
                        kurangStok2?.companyDealer = kurangStok2?.companyDealer
                        kurangStok2?.goods = spplyDetail?.goods
                        kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - spplyDetail?.qty
                        kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - spplyDetail?.qty
                        kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1- spplyDetail?.qty
                        kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2- spplyDetail?.qty
                        kurangStok2.staDel = '0'
                        kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        kurangStok2.lastUpdProcess = 'INSERT SUPP'
                        kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                        kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                        kurangStok2?.save(flush: true)
                }catch (Exception e){

                }
            }
            def params = [:]
            params.ssNumber = supplySlip?.ssNumber
            def service = new PemakaianPartsSendiriJournalService()
            service.createJournal(params)
        }else if(staApproval==StatusApproval.REJECTED){
            def supplySlip = SupplySlipOwn.get(fks.toLong())
            supplySlip.staApprove = "2"
            supplySlip.save(flush: true)
        }
    }
}