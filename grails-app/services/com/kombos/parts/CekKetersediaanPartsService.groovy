package com.kombos.parts

import com.kombos.administrasi.KegiatanApproval
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

class CekKetersediaanPartsService implements AfterApprovalInterface {
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)


	def validasiOrderService
    def datatablesUtilService
    def conversi = new Konversi()

   def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = RequestDetail.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {			
                request{
                    eq("companyDealer",params?.companyDealer)
                    eq("staDel",'0')
                }
                eq("status",RequestStatus.BELUM_REQUEST_BOOKING_FEE)
                order("dateCreated","desc")
		}
		
		def rows = []
		results.each {

			def hargaTanpaPPN = 0
            if(it.goods && it.goods.satuan){
                def ghj = GoodsHargaJual.findByGoodsAndStaDel(it.goods, "0")
                hargaTanpaPPN = ghj?.t151HargaTanpaPPN
            }
			def qty = it.t162Qty1?:"0" as int
			def satuan = it.goods?.satuan?.m118Satuan1
            def availabilityQty = 0
			availabilityQty = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',params?.companyDealer) ? PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',params?.companyDealer)?.t131Qty1:"0"
            def poDetail = ""
			poDetail= PODetail.findByValidasiOrder(it.validasiOrder)

			rows << [
				id: it.id,
				status: it.status?.toString(),
				validated: it.validasiOrder?.t163NamaUserValidasi?"1":"0",
				kodePart: it.goods?.m111ID?:"",
				namaPart: it.goods?.m111Nama?:"",
				requestQty: qty,
				requestSatuan: satuan,
				availabilityQty: availabilityQty,
				availabilitySatuan: satuan,
				orderQty: poDetail?poDetail.t164Qty1:0,
				orderSatuan: satuan,
				eta: ETA.findByPo(poDetail?.po)? ETA.findByPo(poDetail?.po).t165ETA.format(dateFormat):"",
				tipeOrder: it.validasiOrder?.partTipeOrder?.m112TipeOrder,
				rpp: it.validasiOrder?.rpp,
				dp: it.validasiOrder?.t163DP?:"",
				harga: conversi.toRupiah(hargaTanpaPPN),
				total: (hargaTanpaPPN)? conversi.toRupiah(hargaTanpaPPN * qty) : "0" as int
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
   
   def requestBookingFeeApproval(def params){
	   def result = [:]
	   def requestList = JSON.parse(params.requestIds)
	   
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		Request.withTransaction { status ->
			def fail = { Map m ->
				status.setRollbackOnly()
				result.status = "nok"
				result.error = [ code: m.code, args: ["Request", params.requestId] ]
				return result
			}
			requestList.each {
				def rd = RequestDetail.get(it)
				if(!rd)
					return fail(code:"default.not.found.message")
				
				rd.status = RequestStatus.BOOKING_FEE_BELUM_APPROVAL
				rd.lastUpdated = datatablesUtilService?.syncTime()
				rd.save()
				
				def vo = rd.validasiOrder
				
				if(!vo){
					vo = validasiOrderService.createValidasiOrder(rd)
				}
				
                vo.t163DP = params."dp-${it}" as Double
				vo.partTipeOrder = PartTipeOrder.get(params."tipeOrder-${it}")
				vo.lastUpdated = datatablesUtilService?.syncTime()
				vo.save(flush:true)
				vo.errors.each{ //println it
					}
			}
 
		}
		
		def requests = requestList.join('#')
		
		def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
		if(!kegiatanApproval){
			result.status = "nok"
			result.error = "Kegiatan Approval tidak diketemukan."
			return result
		}
		
		def approval = new ApprovalT770(
			t770FK:requests,
			kegiatanApproval: kegiatanApproval,
			t770NoDokumen: params.t770NoDokumen,
			t770TglJamSend: datatablesUtilService?.syncTime(),
			t770Pesan: params.pesan,
            companyDealer: session?.userCompanyDealer,    
            dateCreated: datatablesUtilService?.syncTime(),
            lastUpdated: datatablesUtilService?.syncTime()
		)
		approval.save(flush:true)
		approval.errors.each{ //println it
			}
		
		result.status = "ok"

		return result
   }
   
   def generateNomorDokumenRequestBookingFee(){
	   def result = [:]
	   def c = ApprovalT770.createCriteria()
	   def results = c.get () {
		   resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
		   projections {
			   max("id","id")
		   }
	   }
	   
	   def m = results.id?:0
	   
	   result.value = String.format('%014d',m+1)
	   return result
   }
   
   def orderParts(def params){
	   def result = [:]
	   def requestList = JSON.parse(params.requestIds)
       def tipeOrderList = JSON.parse(params.toIds)
       int ind = 0;
	   
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		Request.withTransaction { status ->
			def fail = { Map m ->
				status.setRollbackOnly()
				result.status = "nok"
				result.error = [ code: m.code, args: ["Request", params.requestId] ]
				return result
			}
			requestList.each {
				def rd = RequestDetail.get(it)
				if(!rd)
					return fail(code:"default.not.found.message")
				
				rd.status = RequestStatus.BELUM_VALIDASI
				rd.lastUpdated = datatablesUtilService?.syncTime()
				rd.save()
				
				def vo = rd.validasiOrder				
				if(!vo){
					vo = validasiOrderService.createValidasiOrder(rd, tipeOrderList[ind])
				}			
				ind++;
			}
 
		}
			
		result.status = "ok"
		   
		return result
   }

   def afterApproval(String fks,
	StatusApproval staApproval,
	Date tglApproveUnApprove,
	String alasanUnApprove,
	String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            def rd = RequestDetail.get(it as Long)
            rd.staApproval = staApproval
            rd.tglApproveUnApprove = tglApproveUnApprove
            rd.alasanUnApprove = alasanUnApprove
            rd.namaUserApproveUnApprove =  namaUserApproveUnApprove
            rd.lastUpdated = datatablesUtilService?.syncTime()
            rd.save()
        }
    }
   
}
