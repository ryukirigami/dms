package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartsTransferStokCekService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df =new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsTransferStok.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params.sCriteria_status=='1'){
                eq("cekBaca","1")
            }else if(params.sCriteria_status=='2'){
                eq("cekBaca","-")
            }else{
                eq("cekBaca","0")
            }
            eq("companyDealerTujuan",params?.userCompanyDealer)
            eq("staApprove","0");
            if (params.sCriteria_t017Tanggal && params.sCriteria_t017Tanggal2) {
                ge("tglTransferStok", params.sCriteria_t017Tanggal)
                lt("tglTransferStok", params.sCriteria_t017Tanggal2 +1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("companyDealer", "companyDealer")
                groupProperty("nomorPO", "nomorPO")

                max("tglTransferStok", "tglTransferStok")
                max("cekBaca", "cekBaca")
            }
        }
        def rows = []
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
        results.each {

            rows << [
                    companyDealer: it.companyDealer.m011NamaWorkshop,
                    tglTransferStok: sdf.format(it.tglTransferStok),
                    nomorPO: it.nomorPO,
                    status: it.cekBaca=='0'?'Belum Ditindak':it.cekBaca=='1'?'Sudah Ditindak':'-',

                    tglTransferStok1: params."sCriteria_t017Tanggal",
                    tglTransferStok2: params."sCriteria_t017Tanggal2",
                    sCriteria_status: params."sCriteria_status",
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsTransferStok.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params.sCriteria_status=='1'){
                eq("cekBaca","1")
            }else if(params.sCriteria_status=='2'){
                eq("cekBaca","-")
            }
            eq("companyDealerTujuan",params?.companyDealer)
            eq("nomorPO",params.nomorPO)
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    goods: it.goods?.m111ID,
                    goods2: it.goods?.m111Nama,
                    noPO: it.nomorPO,
                    cekSedia: it.cekKetersediaan=='1'?"Acc / " + it.noPengiriman:"",
                    hargaBeli : GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",params.companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",params?.companyDealer)?.t150Harga : 0,
                    qty : it.qtyParts
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            String data = it
            def transfer = null
            def cd = null
            def noPo = null
            try{
                transfer = PartsTransferStok.findById(it)
                if(!transfer){
                    noPo = PartsTransferStok.findByNomorPO(it)
                    if(!noPo){
                        cd = PartsTransferStok.findByCompanyDealerTujuan(CompanyDealer.findByM011NamaWorkshop(it))
                    }
                }
            }catch(Exception a){
            }

            if(transfer){
                def ubh = PartsTransferStok.get(it)
                ubh?.staDel = '1'
                ubh?.lastUpdProcess = 'DELETE'
                ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ubh?.lastUpdated = datatablesUtilService?.syncTime()
                ubh?.save(flush: true)
            }else if(noPo){
                PartsTransferStok.findAllByNomorPOAndStaDel(it,'0').each{
                    def ubh = PartsTransferStok.get(it)
                    ubh?.staDel = '1'
                    ubh?.lastUpdProcess = 'DELETE'
                    ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    ubh?.lastUpdated = datatablesUtilService?.syncTime()
                    ubh?.save(flush: true)
                }
            }else{
                try{
                    PartsTransferStok.findAllByCompanyDealerTujuanAndStaDel(CompanyDealer.findByM011NamaWorkshop(it),'0').each{
                        def ubh = PartsTransferStok.get(it.id)
                        ubh?.staDel = '1'
                        ubh?.lastUpdProcess = 'DELETE'
                        ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        ubh?.lastUpdated = datatablesUtilService?.syncTime()
                        ubh?.save(flush: true)
                    }
                }catch(Exception a){

                }

            }
        }
    }
}
