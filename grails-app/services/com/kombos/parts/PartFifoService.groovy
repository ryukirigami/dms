package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP

class PartFifoService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        String kodeGoods = params?.sCriteria_goods ? params?.sCriteria_goods?.toString()?.substring(0,params?.sCriteria_goods?.toString()?.indexOf("|")) : "--"
        def goods = Goods.findByM111IDIlikeAndStaDel("%"+kodeGoods.trim()+"%","0")

        def c2 = PartFifo.createCriteria()
        def total = c2.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq('companyDealer', params.companyDealer)

            if(params?."sCriteria_goods"){
                eq("goods", goods)
            }

            if(params?."sCriteria_tanggalAwal"){
                ge("tglBeli",new Date().parse("dd-MM-yyyy",params?.sCriteria_tanggalAwal))
            }

            if(params?."sCriteria_tanggalAkhir"){
                le("tglBeli",new Date().parse("dd-MM-yyyy",params?.sCriteria_tanggalAkhir) + 1)
            }

//            order("tglBeli","desc")

            projections{
                groupProperty("po")
                groupProperty("goods")
                groupProperty("qtyBeli")
                groupProperty("hargaBeli")
            }
        }

        def c = PartFifo.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq('companyDealer', params.companyDealer)

            if(params?."sCriteria_goods"){
                eq("goods", goods)
            }

            if(params?."sCriteria_tanggalAwal"){
                ge("tglBeli",new Date().parse("dd-MM-yyyy",params?.sCriteria_tanggalAwal))
            }

            if(params?."sCriteria_tanggalAkhir"){
                le("tglBeli",new Date().parse("dd-MM-yyyy",params?.sCriteria_tanggalAkhir) + 1)
            }

            projections{
                groupProperty("po")
                groupProperty("goods")
                groupProperty("qtyBeli")
                groupProperty("hargaBeli")
            }
        }

        def rows = []

        results.each {

            rows << [

                    po: it[0]?.t164NoPO ? it[0]?.t164NoPO : "-",

                    tglPO: it[0]?.t164TglPO ? it[0]?.t164TglPO.format("dd-MMM-yyyy") : "-",

                    kodeGoods: it[1]?.m111ID ? it[1]?.m111ID : "-",

                    goods: it[1]?.m111Nama ? it[1]?.m111Nama : "-",

                    qtyBeli: it[2],

                    hargaBeli: it[3],

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  total.size(), iTotalDisplayRecords: total.size(), aaData: rows]

    }

    def datatablesSubList(params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartFifo.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params.fifoId){
                po{
                    ilike("t164NoPO","%" + params."fifoId" + "%")
                }

                goods{
                    ilike("m111ID","%" + params."kodeGoods" + "%")
                }
            }
        }


        def rows = []

        results.each {
            def noWO = Reception.findByIdAndStaDel(it?.wo_do, "0")

            def noSO = "-"
            def tglSO
            def qty = "-"
            def hargaJual = "-"

            if(noWO){
                noSO = noWO.t401NoWO.toString()
                tglSO = noWO.t401TglJamCetakWO
                qty = PartsRCP.findByReceptionAndGoodsAndStaDel(noWO, it.goods, "0").t403Jumlah1
                hargaJual = PartsRCP.findByReceptionAndGoodsAndStaDel(noWO, it.goods, "0").t403HargaRp

            }else {

            }

            rows << [

                    id : it?.id ? it?.id : "-",

                    noSO: noSO? noSO : "-",

                    tglSO: tglSO ? tglSO.format("dd-MMM-yyyy") : "-",

                    qty: qty ? qty : "-",

                    hargaJual: hargaJual ? hargaJual : "-"

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def serviceMethod() {

    }
}
