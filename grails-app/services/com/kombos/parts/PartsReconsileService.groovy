package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartsReconsileService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)


    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PO.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t164NoPO", "t164NoPO")
                groupProperty("vendor", "vendor")
                groupProperty("t164TglPO", "t164TglPO")
            }
            order("t164TglPO","asc")
        }
        def rows = []

        results.each {
            rows << [
                    vendor: it.vendor?.m121Nama,
                    noPo: it.t164NoPO,
                    tglPo: it.t164TglPO.format(dateFormat)
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Invoice.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            po{
                    eq("t164NoPO",params.noPo)
            }
        }
        def rows = []
        if(results.size()!=0){
            results.each {
                 rows << [
                        noInvoice: it.t166NoInv?it.t166NoInv:"-",
                        tglInvoice: it.t166TglInv?it.t166TglInv?.format(dateFormat):"-",
                        noPo: params.noPo,
                ]
            }
        }else{
            rows << [
                    noInvoice: "-",
                    tglInvoice: "-",
                    noPo: params.noPo,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PODetail.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            po{
                eq("t164NoPO",params.noPo)
            }
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    kodePart: it.validasiOrder?.requestDetail?.goods?.m111ID,
                    namaPart: it.validasiOrder?.requestDetail?.goods?.m111Nama,
             ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
}
