package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class SendPOService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def jasperService
    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def listEta = ETA.findAllByCompanyDealerAndStaDel(params?.companyDealer,"0")
        def c = PO.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params."sCriteria_vendor"){
                eq("vendor",Vendor.findByM121NamaIlike("%" + (params."sCriteria_vendor" as String) + "%"))
            }

            if(params."sCriteria_nomorPO"){
                ilike("t164NoPO","%" + (params."sCriteria_nomorPO" as String) + "%")
            }

            if(params."sCriteria_tanggalPO"){
                log.info("masuk tanggal")
                ge("t164TglPO",params."sCriteria_tanggalPO")
                lt("t164TglPO",params."sCriteria_tanggalPO" + 1)
            }


            if(params."sCriteria_tipeOrder"){
                eq("vendor", PartTipeOrder.findByM112TipeOrder(params."sCriteria_tipeOrder")?.vendor)
            }



            if (params."search_tglStart" && params."search_tglEnd") {
                ge("t164TglPO",  df.parse(params."search_tglStart"))
                lt("t164TglPO", df.parse(params."search_tglEnd") + 1)
            }


            if((params."spld")!="1"&&(params."nonSpld")!="1"){
            }else if((params."spld")=="1"&&(params." nonSpld")!="1"){
                log.info("masuk spld saja")
                vendor{
                    eq("m121staSPLD","1")
                }


            }else {
                vendor{
                    eq("m121staSPLD","0")

                }

            }


            if((params."search_blmDikirim")!="1"&&(params."search_sdhDikirim")!="1"){
            }else if((params."search_blmDikirim")=="1"&&(params."search_sdhDikirim")!="1"){
               eq("staKirimPrint", "0")


            }else {
                    eq("staKirimPrint","1")
            }

            if((params."etaBlmLengkap")=="0" && (params."etaLengkap")=="0") {
            }else
            if(params."etaLengkap"=="1" && params."etaBlmLengkap"=="0"){
                if(listEta.size()>0){
                    inList("t164NoPO",listEta.po.t164NoPO)
                }
            }else if(params."etaLengkap"=="0" && params."etaBlmLengkap"=="1"){
                if(listEta.size()>0){
                    not {
                        inList("t164NoPO",listEta.po.t164NoPO)
                    }
                }
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("vendor", "vendor")
                groupProperty("t164NoPO","t164NoPO")
                groupProperty("t164TglPO","t164TglPO")
                //  groupProperty("validasiOrder","validasiOrder")

            }
            isNotNull("vendor")
            eq("companyDealer",params.companyDealer)
            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }


        def rows = []
        ETA searchPO = null
        ETA searchNullETA = null

        results.each {

                rows << [

                        t164NoPO: it.t164NoPO,

                        t164TglPO: it.t164TglPO?it.t164TglPO.format(dateFormat):"",

                        vendor: it.vendor?.m121Nama,

                        validasiOrder: it.t164NoPO,

                        PO: it.t164NoPO,

                        tipeOrder: PODetail.findByPo(PO.findByT164NoPO(it.t164NoPO))?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                ]


        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = PO.get(params.id)

        if(!result.POInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = PO.get(params.id)

        if(!result.POInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = PO.get(params.id)

        if(!result.POInstance)
            return fail(code:"default.not.found.message")

        try {
            result.POInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }

    def update(params) {
        PO.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.POInstance && m.field)
                    result.POInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["PO", params.id] ]
                return result
            }

            result.POInstance = PO.get(params.id)

            if(!result.POInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.POInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.POInstance.properties = params

            if(result.POInstance.hasErrors() || !result.POInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = new PO()
        result.POInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.POInstance && m.field)
                result.POInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = new PO(params)

        if(result.POInstance.hasErrors() || !result.POInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        // success
        return result
    }

    def updateField(def params){
        def PO =  PO.findById(params.id)
        if (PO) {
            PO."${params.name}" = params.value
            PO.save()
            if (PO.hasErrors()) {
                throw new Exception("${PO.errors}")
            }
        }else{
            throw new Exception("PO not found")
        }
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def rows = []

        def po = PO.findByT164NoPO(params.nomorPO)
        def poDetail = PODetail.findAllByPo(po)
        def goodness = []

        int count = 0
        def qtyIssue = 0

        poDetail.each {
            def etas = ETA.findAllByGoods(it.validasiOrder.requestDetail.goods)
            etas.each {
                qtyIssue = qtyIssue + it.t165Qty1
            }

            rows <<[
                    kodePart: it.validasiOrder.requestDetail.goods.m111ID,
                    namaPart: it.validasiOrder.requestDetail.goods.m111Nama,
                    qty: it.t164Qty1,
                    satuan : it.validasiOrder.requestDetail.goods.satuan.m118Satuan1,
                    naon : "",
                    nomorPO : params.t164NoPO

            ]

            count = count + 1
            qtyIssue = 0

        }

        [sEcho: params.sEcho, iTotalRecords:  count, iTotalDisplayRecords: count, aaData: rows]
        //	   [sEcho: params.sEcho, iTotalRecords:  1, iTotalDisplayRecords: 1, aaData: rows]

    }

    def sendEmailSPLD(def params){
       def poList = parsingPO(params)

    }

    def exportPO(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def result = [:]
        def idPO = params.idPO
        def po = PO.findByT164NoPO(idPO)

        if(params.idPO == '0'){
            return;
        }
        else{
            def reportData = new ArrayList();
            def grandTotal = 0.0
            def hargaTotal = 0.0
            def hargaSatuan = 0.0
            def qty = 0

            def results = PODetail.findAllByPo(po)
            results.sort {
                it.validasiOrder.requestDetail.goods.m111ID
            }

            int count = 0

            results.each {
                def data = [:]

                hargaSatuan = GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.validasiOrder.requestDetail.goods,"0",it?.po?.companyDealer)? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.validasiOrder.requestDetail.goods,"0",it?.po?.companyDealer).t150Harga : 0
                qty = it.t164Qty1
                hargaTotal = hargaSatuan * qty


                count = count + 1
            //    log.info("nama parts : "+it.goods?.m111Nama)
                data.put("noUrut", count)
                data.put("kodePart", it.validasiOrder.requestDetail.goods.m111ID)
                data.put("namaPart", it.validasiOrder.requestDetail.goods.m111Nama)
                data.put("satuan",it.validasiOrder.requestDetail.goods.satuan?.m118Satuan1)
                data.put("qty",qty)
                data.put("hargaSatuan", hargaSatuan )
                data.put("totalHarga", hargaTotal)

                grandTotal = grandTotal + hargaTotal

                reportData.add(data)
            }

            def parameters = [nomorPO: po?.t164NoPO,tanggalPO: po?.t164TglPO.format(dateFormat)]
            parameters.tipeOrder = PartTipeOrder.findByVendor(po.vendor)?.getM112TipeOrder()
            parameters.namaVendor = po.vendor.m121Nama
            parameters.alamatVendor = po.vendor.m121Alamat
            parameters.grandTotal = grandTotal
            parameters.tanggalBuat = new Date().format(dateFormat)


            def reportDef = new JasperReportDef(name:'sendPO.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData,
                    parameters: parameters
            )

            result.report = reportDef

            return result



        }
    }


    def printPO(def params){
        
    }

    def parsingPO(params){
        def jsonArray = JSON.parse(params.checkedPO)
        def poList = []
        jsonArray.each {
            poList << PO.findByT164NoPO(it)
        }

        return poList


    }





}
