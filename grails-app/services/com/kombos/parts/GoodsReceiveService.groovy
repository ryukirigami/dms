package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.finance.Collection167
import com.kombos.finance.Journal
import com.kombos.finance.JournalDetail
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class GoodsReceiveService {
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def serviceMethod() {

    }

    def vendorList(def params) {

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        /**/
        def c2 = GoodsReceive.createCriteria()
        def resultsCount = c2.list () {
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                def tanggalAwal = new Date().parse("dd/MM/yyyy",params.sCriteria_tglFrom)
                def tanggalAKhir = new Date().parse("dd/MM/yyyy",params.sCriteria_tglTo)
                ge("t167TglJamReceive",tanggalAwal)
                lt("t167TglJamReceive", tanggalAKhir + 1)
            }
            if (params."search_noReff" && params."search_noReff") {
                eq("t167NoReff",params."search_noReff" as String)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("vendor", "vendor")
                groupProperty("companyDealerSupply", "companyDealerSupply")
            }
        }
        int count = 0
        resultsCount.each {
            def tampil = false
            if(params.criteriaParts=='0'){
                def staBinning = BinningDetail.findByGoodsReceiveDetail(GoodsReceiveDetail.findByGoodsReceive(GoodsReceive.findByVendor(Vendor.findByM121Nama(it?.vendor?.m121Nama))))
                if(!staBinning){
                    tampil = true
                }
            }else if(params.criteriaParts=='1'){
                def staBinning = BinningDetail.findByGoodsReceiveDetail(GoodsReceiveDetail.findByGoodsReceive(GoodsReceive.findByVendor(Vendor.findByM121Nama(it?.vendor?.m121Nama))))
                if(staBinning){
                    tampil = true
                }
            }else{
                tampil = true
            }
            if(tampil == true){
                count+=1
            }
        }
        /**/
        def c = GoodsReceive.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if (params."search_noReff" && params."search_noReff") {
                eq("t167NoReff",params."search_noReff" as String)
            }
            if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                def tanggalAwal = new Date().parse("dd/MM/yyyy",params.sCriteria_tglFrom)
                def tanggalAKhir = new Date().parse("dd/MM/yyyy",params.sCriteria_tglTo)
                ge("t167TglJamReceive",tanggalAwal)
                lt("t167TglJamReceive", tanggalAKhir + 1)
            }

            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("vendor", "vendor")
                groupProperty("companyDealerSupply", "companyDealerSupply")
            }

        }
        def rows = []

        results.each {
            def tampil = false
            if(params.criteriaParts=='0'){
                def staBinning = BinningDetail.findByGoodsReceiveDetail(GoodsReceiveDetail.findByGoodsReceive(GoodsReceive.findByVendor(Vendor.findByM121Nama(it?.vendor?.m121Nama))))
                if(!staBinning){
                    tampil = true
                }
            }else if(params.criteriaParts=='1'){
                def staBinning = BinningDetail.findByGoodsReceiveDetail(GoodsReceiveDetail.findByGoodsReceive(GoodsReceive.findByVendor(Vendor.findByM121Nama(it?.vendor?.m121Nama))))
                if(staBinning){
                    tampil = true
                }
            }else{
                tampil = true
            }
            if(tampil == true){
                rows << [
                        namaVendor: it?.vendor?.m121Nama?it?.vendor?.m121Nama:it?.companyDealerSupply?.m011NamaWorkshop,
                        VendorId: it?.vendor?.m121Nama?it?.vendor?.id:it?.companyDealerSupply?.m011NamaWorkshop,
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  count, iTotalDisplayRecords: count, aaData: rows]
    }

    def datatablesSubList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = GoodsReceive.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            def staBinning = BinningDetail.findByGoodsReceiveDetail(GoodsReceiveDetail.findByGoodsReceive(it))
            if(Vendor.findByM121NamaLike("%"+params."namaVendor"+"%")){
                eq("vendor",Vendor.findById(params.VendorId as Long))
            }else{
                eq("companyDealerSupply",CompanyDealer.findByM011NamaWorkshopLike("%"+params."namaVendor"+"%"))
            }
            ilike("staDel","0")
            if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                def tanggalAwal = new Date().parse("dd/MM/yyyy",params.sCriteria_tglFrom)
                def tanggalAKhir = new Date().parse("dd/MM/yyyy",params.sCriteria_tglTo)
                ge("t167TglJamReceive",tanggalAwal)
                lt("t167TglJamReceive", tanggalAKhir + 1)
            }
            if (params."search_noReff" && params."search_noReff") {
                eq("t167NoReff",params."search_noReff" as String)
            }
            order("t167TglJamReceive","desc")
        }



        def rows = []

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

        def data=true
        results.each {
            data=false
            if (params."criteriaParts"=="1") {
                if(GoodsReceiveDetail.findByGoodsReceive(it)?.staBinning=="1"){
                    data=true
                }
            }else if (params."criteriaParts"=="0") {
                if(GoodsReceiveDetail.findByGoodsReceive(it)?.staBinning!="1"){
                    data=true
                }
            }else{
                data=true
            }
            if( data==true){
                rows << [
                    id: it.id,
                    namaVendor: it.vendor?.m121Nama,
                    idGoodsReceive: it.t167ID,
                    tipeBayar : it.t167TipeBayar,
                    noReff: it.t167NoReff,
                    tglReff: it?.t167TglReff ? it?.t167TglReff.format("dd/MM/yyyy"):"-",
                    tglJamReceive: sdf.format(it.t167TglJamReceive),
                    petugasReceive: it.t167PetugasReceive,
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def datatablesSubSubList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
        def c = GoodsReceiveDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            goodsReceive{
                eq("companyDealer",params?.companyDealer);
            }
            eq("goodsReceive",GoodsReceive.findByT167ID("" + params."idGoodsReceive"))   //T167NoReffLike("%"+params."noReff"+"%"))
            ilike("staDel","0")
        }

        def rows = []
        results.each {
            def tampil = false
            if (params."criteriaParts"=="1") {
                if(GoodsReceiveDetail.findByGoodsReceive(it)?.staBinning=="1"){
                    tampil=true
                }
            }else if (params."criteriaParts"=="0") {
                if(GoodsReceiveDetail.findByGoodsReceive(it)?.staBinning!="1"){
                    tampil=true
                }
            }else{
                tampil=true
            }
            if(tampil==true){
               def vo = ""
                try {
                    vo = ValidasiOrder.findByRequestDetail(RequestDetail.findByGoods(it.goods))
                }catch (Exception e){

                }
                rows << [
                    id: it.id,
                    partsCode: it.goods?.m111ID,   //it.goods?.m111ID,
                    partsName: it.goods?.m111Nama,  //it.goods?.m111Nama,
                    poNumber: it.poDetail?.po?.t164NoPO?it.poDetail?.po?.t164NoPO:(it?.updatedBy?it?.updatedBy:""),
                    poDate: it.poDetail?.po?.t164TglPO? sdf.format(it.poDetail?.po?.t164TglPO):(PO.findByT164NoPOLike("%"+it.updatedBy+"%")?.t164TglPO?sdf.format(PO.findByT164NoPOLike("%"+it.updatedBy+"%")?.t164TglPO):""),
                    orderType: vo?.partTipeOrder?.m112TipeOrder?vo?.partTipeOrder?.m112TipeOrder:'-',
                    qtyRef: it.t167Qty1Reff + " " + it.goods?.satuan?.m118Satuan1,
                    qtyRcv: it.t167Qty1Issued + " " + it.goods?.satuan?.m118Satuan1,
                    qtyRusak: it.t167Qty1Rusak + " " + it.goods?.satuan?.m118Satuan1,
                    qtySalah: it.t167Qty1Salah + " " + it.goods?.satuan?.m118Satuan1,
                ]
            }
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesListInput(def params) {
        def c = GoodsReceiveDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            goodsReceive{
                eq("companyDealer",params?.companyDealer);
            }
            eq("goodsReceive", GoodsReceive.findByT167NoReffIlike(params."noReff"))
            ilike("staDel","0")
        }
        def rows = []
        def nos = 0

        results.each {
            nos = nos + 1
            rows << [
                    id: it.id,
                    norut : nos,
                    pilih : false,
                    partsCode: it.goods?.m111ID,
                    partsName: it.goods?.m111Nama,
                    poNumber: it.poDetail?.po?.t164NoPO,
                    poDate: it.poDetail?.po?.t164TglPO,
                    orderType : it.poDetail?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                    qtyRef : it.poDetail?.t164Qty1,
                    qtyRcv : it.poDetail?.t164Qty2,
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesListInputAddParts(def params) {
        def ret
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        def c = PO.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
        }

        def rows = []

        results.each {
            def requestDetails = it?.validasiOrder?.requestDetail?.requests
            def nomorPO = it?.t164NoPO
            def tglPO = it?.t164TglPO
            requestDetails.each {
                rows << [
                        id: it.id,
                        kode: it?.goods?.m111ID,
                        nama: it?.goods?.m111Nama,
                        nomorPO: nomorPO,
                        tglPO: tglPO
                ]
            }
        }
        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def inputGoodsReceiveDatatablesList(def params) {

        def results = new ArrayList<BinningDetail>()
        def rows = []
        if (params."goodsReceiveId") {
            GoodsReceive goodsReceive = GoodsReceive.findById(params."goodsReceiveId")
            results = goodsReceive.goodsReceiveDetail
            results.each {
                rows << [
                        id: it.id,
                        partsCode: it.goods?.m111ID,
                        partsName: it.goods?.m111Nama,
                        poNumber : it.poDetail?.po?.t164NoPO,
                        poDate : it.poDetail?.po?.t164TglPO,
                        orderType : it.poDetail?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                        qtyRef : it.t167Qty1Reff,
                        qtyRcv : it.t167Qty1Issued,
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def massDeleteVendor(params){
        def jsonArray = JSON.parse(params.ids)
        def vendors = []

        jsonArray.each {
            vendors << Vendor.get(it)
        }

//        vendors.each {
//            def goodsReceives = GoodsReceive.findAllByVendor(it)
//            goodsReceives.each {
//                def goodsReceiveDetails = GoodsReceiveDetail.findAllByGoodsReceive(it)
//                goodsReceiveDetails.each {
//                    def binningDetails = BinningDetail.findByGoodsReceiveDetail(it)
//                    binningDetails.each {
//                        def stockINDetails = StockINDetail.findAllByBinningDetail(it)
//                        stockINDetails.each {
//                            it.delete()
//                        }
//                        it.delete()
//                    }
//                    it.delete()
//                }
//                it.delete();
//            }
//        }
    }
    def massDeleteGoodsReceive(params){
        def jsonArray = JSON.parse(params.ids)
        def goodsReceives = []

        jsonArray.each {
            goodsReceives << GoodsReceive.get(it)
        }

        goodsReceives.each {
            def goodsReceiveDetails = GoodsReceiveDetail.findAllByGoodsReceive(it)
            try {
                def journal = Journal.findByCompanyDealerAndDocNumberAndStaDel(it.companyDealer,it.t167NoReff,'0')
                def ubJ = Journal.get(journal.id)
                ubJ.staDel = '1'
                ubJ.save(flush: true)

                def journalDetail = JournalDetail.findAllByJournal(journal)
                journalDetail.each {
                    def uJD = JournalDetail.get(it.id)
                    uJD.staDel = '1'
                    uJD.save(flush: true)
                }

                def col = Collection167.findByGoodsReceive(it)
                def ubhCol = Collection167.get(col.id).delete()
            }catch (Exception e){
                println e
            }

            goodsReceiveDetails.each {
                def hapusG = GoodsReceiveDetail.get(it.id)
                hapusG.staDel='1'
                hapusG.save(flush: true)
                def hapusG2 = GoodsReceive.get(hapusG.goodsReceiveId)
                hapusG2.staDel='1'
                hapusG2.save(flush: true)

                def binningDetails = BinningDetail.findByGoodsReceiveDetail(it)
                binningDetails.each {
                    def hapusBinning = BinningDetail.get(it.id)
                    hapusBinning.staDel='1'
                    hapusBinning.save(flush: true)
                    def hapusBinning2 = Binning.get(hapusBinning.binningId)
                    hapusBinning2.staDel='1'
                    hapusBinning2.save(flush: true)

                    def stockINDetails = StockINDetail.findAllByBinningDetail(it)
                    stockINDetails.each {
                        try {
                            def goodsStok = PartsStok.findByGoodsAndCompanyDealerAndStaDel(it.binningDetail.goodsReceiveDetail.goods,it.stockIN.companyDealer,'0')
                            if(goodsStok){
                                def tambahStock =  PartsStok.get(goodsStok.id)
                                tambahStock?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                tambahStock?.t131Qty1Free = tambahStock.t131Qty1Free - it.t169Qty1StockIn
                                tambahStock?.t131Qty2Free = tambahStock.t131Qty2Free - it.t169Qty1StockIn
                                tambahStock?.t131Qty1 = tambahStock?.t131Qty1 - it.t169Qty1StockIn
                                tambahStock?.t131Qty2 = tambahStock?.t131Qty2 - it.t169Qty1StockIn
                                tambahStock?.lastUpdProcess = "KURANGI"
                                tambahStock?.save(flush: true)
                                tambahStock.errors.each {
                                    //println it
                                }
                            }
                        }catch(Exception e){
                        }
                        def hapusStockin = StockINDetail.get(it.id)
                        hapusStockin.staDel='1'
                        hapusStockin.save(flush: true)
                        def hapusStockin2 = StockIN.get(hapusStockin.stockINId)
                        hapusStockin2.staDel='1'
                        hapusStockin2.save(flush: true)
                    }
                }
            }
        }
    }
    def massDeleteGoodsReceiveDetail(params){
        def jsonArray = JSON.parse(params.ids)
        def goodsReceiveDetails = []

        jsonArray.each {
            goodsReceiveDetails << GoodsReceiveDetail.get(it)
        }
    }

}
