package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class ETAPOService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PO.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer);
            if(params."sCriteria_vendor"){
                eq("vendor",Vendor.findByM121NamaIlike("%" + (params."sCriteria_vendor" as String) + "%"))
            }

            if(params."sCriteria_nomorPO"){
                ilike("t164NoPO","%" + (params."sCriteria_nomorPO" as String) + "%")
            }

            if(params."sCriteria_tanggalPO"){
                log.info("masuk tanggal")
                ge("t164TglPO",params."sCriteria_tanggalPO")
                lt("t164TglPO",params."sCriteria_tanggalPO" + 1)
            }


            if(params."sCriteria_tipeOrder"){
                     eq("vendor", PartTipeOrder.findByM112TipeOrder(params."sCriteria_tipeOrder")?.vendor)
            }



            if (params."search_tglStart" && params."search_tglEnd") {
                ge("t164TglPO",  df.parse(params."search_tglStart"))
                lt("t164TglPO", df.parse(params."search_tglEnd") + 1)
            }


            if((params."spld")!="1"&&(params."nonSpld")!="1"){
            }else if((params."spld")=="1"&&(params." nonSpld")!="1"){
                   log.info("masuk spld saja")
                    vendor{
                        eq("m121staSPLD","1")
                    }


            }else {
                vendor{
                    eq("m121staSPLD","0")

                }

            }


            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("vendor", "vendor")
                groupProperty("t164NoPO","t164NoPO")
                groupProperty("t164TglPO","t164TglPO")
              //  groupProperty("validasiOrder","validasiOrder")

            }





            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }


        def rows = []
        ETA searchPO = null
        ETA searchNullETA = null

        results.each {
            if((params."etaBlmLengkap")=="0" && (params."etaLengkap")=="0") {
                if(it.vendor?.m121Nama){
                    rows << [

                            t164NoPO: it.t164NoPO,

                            t164TglPO: it.t164TglPO?it.t164TglPO.format(dateFormat):"",

                            vendor: it.vendor?.m121Nama,

                            validasiOrder: it.t164NoPO,

                            PO: it.t164NoPO,

                            request : it.t164NoPO,

                            tipeOrder: PODetail.findByPo(PO.findByT164NoPO(it.t164NoPO))?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                    ]
                }
            }else if(params."etaLengkap"=="1" && params."etaBlmLengkap"=="0"){

                searchPO = ETA.findByPo(PO.findByT164NoPO(it.t164NoPO))
                if(searchPO){
                    if(it.vendor?.m121Nama){
                        rows << [

                                t164NoPO: it.t164NoPO,

                                t164TglPO: it.t164TglPO?it.t164TglPO.format(dateFormat):"",

                                vendor: it.vendor?.m121Nama,

                                validasiOrder: it.t164NoPO,

                                PO: it.t164NoPO,

                                request : it.t164NoPO,

                                tipeOrder: PODetail.findByPo(PO.findByT164NoPO(it.t164NoPO))?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                        ]
                    }
                }else{
                    //println "search po lengkap kosong"
                }

            }else if(params."etaLengkap"=="0" && params."etaBlmLengkap"=="1"){

                searchNullETA = ETA.findByPo(PO.findByT164NoPO(it.t164NoPO))

                if(searchNullETA){
                    //println "searh nul eta ada"
                }else{
                    if(it.vendor?.m121Nama){
                        rows << [

                                t164NoPO: it.t164NoPO,

                                t164TglPO: it.t164TglPO?it.t164TglPO.format(dateFormat):"",

                                vendor: it.vendor?.m121Nama,

                                validasiOrder: it.t164NoPO,

                                PO: it.t164NoPO,

                                request : it.t164NoPO,

                                tipeOrder: PODetail.findByPo(PO.findByT164NoPO(it.t164NoPO))?.validasiOrder?.partTipeOrder?.m112TipeOrder,
                        ]
                    }
                }
            }
        }


        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = PO.get(params.id)

        if(!result.POInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = PO.get(params.id)

        if(!result.POInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = PO.get(params.id)

        if(!result.POInstance)
            return fail(code:"default.not.found.message")

        try {
            result.POInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }

    def update(params) {
        PO.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.POInstance && m.field)
                    result.POInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["PO", params.id] ]
                return result
            }

            result.POInstance = PO.get(params.id)

            if(!result.POInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.POInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.POInstance.properties = params

            if(result.POInstance.hasErrors() || !result.POInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = new PO()
        result.POInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.POInstance && m.field)
                result.POInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["PO", params.id] ]
            return result
        }

        result.POInstance = new PO(params)

        if(result.POInstance.hasErrors() || !result.POInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        // success
        return result
    }

    def updateField(def params){
        def PO =  PO.findById(params.id)
        if (PO) {
            PO."${params.name}" = params.value
            PO.save()
            if (PO.hasErrors()) {
                throw new Exception("${PO.errors}")
            }
        }else{
            throw new Exception("PO not found")
        }
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        //println("asief sub : " + params.nomorPO)
        def rows = []

        def po = PO.findByT164NoPO(params.nomorPO)
        def poDetail = PODetail.findAllByPo(po)
        def goodness = []

        int count = 0
        def qtyIssue = 0

        poDetail.each {
            def etas = ETA.findAllByGoodsAndPo(it.validasiOrder.requestDetail.goods, po)
            etas.each {
                qtyIssue = qtyIssue + it.t165Qty1
            }

            rows <<[
                    kodePart: it.validasiOrder.requestDetail.goods.m111ID,
                    namaPart: it.validasiOrder.requestDetail.goods.m111Nama,
                    qty: it.t164Qty1,
                    qtyIssue: qtyIssue,
                    qtySelisih : it.t164Qty1 - qtyIssue,
                    naon : "",
                    nomorPO : params.nomorPO
            ]

            count = count + 1
            qtyIssue = 0

        }

        //println "count : "+count
        [sEcho: params.sEcho, iTotalRecords:  count, iTotalDisplayRecords: count, aaData: rows]
        //	   [sEcho: params.sEcho, iTotalRecords:  1, iTotalDisplayRecords: 1, aaData: rows]

    }

    def datatablesSubSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue


        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        //println "asief subsub : " + (params.nomorPO)
        def goods = Goods.findByM111ID(params.goods)
        def po = PO.findByT164NoPO(params.nomorPO)
        def c = ETA.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(goods)
                eq("goods", goods)
            if(po)
                eq("po",po)

            eq("staDel","0")

        }

        def rows = []
        //println "result size : "+ results.size()
        results.each {

            rows << [
                    naon : "",
                    naon2 : "",
                    naon3 : "",
                    eta: it.t165ETA?it.t165ETA.format(dateFormat) + " " +  new SimpleDateFormat("HH:mm").format(new Date(it.t165ETA?.getTime())):"",
                    qtyIssue : it.t165Qty1

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

}
