package com.kombos.parts

import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON

class SalesOrderService implements AfterApprovalInterface{

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SalesOrder.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer);
            if (params."sCriteria_sorNumber") {
                ilike("sorNumber", "%"+params."sCriteria_sorNumber"+"%")
            }

            if(params."sCriteria_customerName"){
                ilike("customerName","%"+params."sCriteria_customerName"+"%")
            }

            if(params."sCriteria_deliveryLocation"){
                ilike("deliveryLocation","%"+params."sCriteria_deliveryLocation"+"%")
            }

            salesType{
                if (params."sCriteria_salesType"){
                    ilike("salesType","%"+params."sCriteria_salesType"+"%")
                }
            }

            if(params."sCriteria_paymentType"){
                ilike("paymentType","%"+params."sCriteria_paymentType"+"%")
            }

            if(params?."sCriteria_tggldeliveryDate"){
                ge("deliveryDate",params?."sCriteria_tggldeliveryDate")
                lt("deliveryDate",params?."sCriteria_tggldeliveryDate"+1)
            }
//            order('isApprove','desc')
            order('dateCreated','desc')
//            switch (sortProperty) {
//                default:
//                    order(sortProperty, sortDir)
//                    break;
//            }
        }

        def rows = []
        def staApproval = ["Approved","UnApproved","Wait For Approval"]
        results.each {
            rows << [

                    id: it?.id,

                    sorNumber: it?.sorNumber,

                    deliveryDate: it?.deliveryDate.format("dd-MM-yyyy"),

                    customerName: it?.customerName,

                    deliveryLocation: it?.deliveryLocation,

                    salesType: it?.salesType?.m117NamaFranc,

                    paymentType: it?.paymentType,

                    isApprove: staApproval[it?.isApprove],

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SalesOrderDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            sorNumber{
                eq("id",params.sorNumber.toLong())
            }
//            if (params."sCriteria_m803Id") {
//                eq("m803Id", params."sCriteria_m803Id")
//            }
//
//            switch (sortProperty) {
//                default:
//                    order(sortProperty, sortDir)
//                    break;
//            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kodeGoods: it.materialCode.m111ID,

                    namaGoods: it.materialCode.m111Nama,

                    qty: it.quantity

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesPartList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsStok.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(exist.size()>0){
                goods{
                not {
                    or {
                        exist.each {
                                eq('id', it as long)
                        }
                    }
                }
                }
            }
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer)
            not {
                eq("t131Qty1Free",0.toDouble())
            }
            goods{
                eq("staDel","0")
                if (params."sCriteria_m111ID") {
                    ilike("m111ID", "%"+params."sCriteria_m111ID"+"%")
                }

                if (params."sCriteria_m111Nama") {
                    ilike("m111Nama", "%"+params."sCriteria_m111Nama"+"%")
                }

                satuan{
                    if (params."sCriteria_satuan") {
                        ilike("m118Satuan1", "%"+params."sCriteria_satuan"+"%")
                    }
                }

            }

            if (params."sCriteria_stok") {
                eq("t131Qty1", params."sCriteria_stok".toDouble())
            }

        }

        def rows = []

        results.each {
            def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it?.goods,  "0",new Date(),[sort: "t151TMT",order: "desc"])
            def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                eq("staDel","0");
                eq("companyDealer",params?.companyDealer);
            }
            def mappingRegion = MappingPartRegion.createCriteria().get {
                if(mappingCompRegion?.size()>0){
                    inList("region",mappingCompRegion?.region)
                }else{
                    eq("id",-10000.toLong())
                }
                maxResults(1);
            }
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
            def hargaBeli = 0
            try {
                hargaBeli = (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",params?.companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",params?.companyDealer)?.t150Harga : 0 )
            }catch (Exception e){

            }
            if(mappingRegion){
                try {
                    if(KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("TOYOTA") ||
                            KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("OLI")){
                        hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                    }
                }catch (Exception e){

                }
            }
            def franc = ""
            try {
                if(KlasifikasiGoods.findByGoods(ghj.goods)){
                    franc = KlasifikasiGoods.findByGoods(ghj.goods)?.franc?.m117NamaFranc
                }
            }catch (Exception e){

            }
             rows << [

                    id: it?.goods?.id,

                    m111ID: it?.goods?.m111ID,

                    m111Nama: it?.goods?.m111Nama,

                    satuan: it?.goods?.satuan?.m118Satuan1,

                    franc: franc,

                    qty: it?.t131Qty1Free,

                    harga : new Konversi().toRupiah(hargaTanpaPPN),

                    hargaBeli : new Konversi().toRupiah(hargaBeli)

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def massDelete(def params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
                def so = SalesOrder.findById(it)
                def details = SalesOrderDetail.findAllBySorNumberAndStaDel(so,"0")
                def dO = DeliveryOrder.findBySorNumberAndStaDelAndCompanyDealer(so,"0",params?.companyDealer)
                if(!dO){
                    details.each{
                        def ubh = SalesOrderDetail.get(it.id)
                        ubh?.staDel = '1'
                        ubh?.lastUpdProcess = 'DELETE'
                        ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        ubh?.save(flush: true)
                    }
                    def ubh2 = SalesOrder.get(it)
                    ubh2?.staDel = '1'
                    ubh2?.lastUpdProcess = 'DELETE'
                    ubh2?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    ubh2?.save(flush: true)

                    def hapusApproval = ApprovalT770.findByT770NoDokumenAndStaDelAndCompanyDealer(ubh2.sorNumber,'0',ubh2.companyDealer)
                    hapusApproval?.staDel = '1'
                    hapusApproval?.lastUpdProcess = 'DELETE'
                    hapusApproval?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    hapusApproval?.save(flush: true)

                }
        }
    }

    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        if(staApproval==StatusApproval.APPROVED){
            def salesOrder = SalesOrder.get(fks.toLong())
                salesOrder.isApprove = 0
            salesOrder.save(flush: true)
        }else{
            def salesOrder = SalesOrder.get(fks.toLong())
            salesOrder.isApprove = 1
            salesOrder.save(flush: true)
        }
    }
}
