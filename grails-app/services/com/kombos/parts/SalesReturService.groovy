package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON

class SalesReturService {

    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SalesRetur.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer)
            order("dateCreated","desc")
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    sorNumber: it.sorNumber.sorNumber,

                    salesRetur: it.nomorRetur,

                    returDate: it.dateCreated.format(dateFormat),

                    deliveryDate: it.sorNumber.deliveryDate.format(dateFormat),

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SalesReturDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            salesRetur{
                eq("id",params.salesRetur as Long)
            }
        }

        def rows = []

        results.each {
            def sales = SalesOrderDetail.findBySorNumber(it.salesRetur.sorNumber).quantity
            rows << [

                    id: it.id,

                    kodeGoods: it.goods.m111ID,

                    namaGoods: it.goods.m111Nama,

                    qty: it.qty,

                    stockPesan: sales,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSOList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SalesOrderDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            sorNumber{
                eq("id",params.idSO.toLong())
            }

        }

        def rows = []

        def total = 0
        results.each {
            def jumlah = it?.discount && it?.discount > 0 ? it.quantity * it.unitPrice - (it.discount/100 * it.quantity * it.unitPrice) : it.quantity * it.unitPrice
            total+=jumlah
            rows << [

                    id: it.id,

                    m111IDAdd: it.materialCode.m111ID,

                    m111NamaAdd: it.materialCode.m111Nama,

                    stokAdd: it.quantity,

                    qtyAdd : 0,

                    hargaAdd: it.unitPrice,

                    satuanAdd : it.materialCode.satuan.m118Satuan1,

                    discountAdd : it?.discount,

                    jumlahAdd : jumlah,

                    total : total

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows,total:total]

    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SalesRetur", params.id]]
            return result
        }

        result.salesReturInstance = SalesRetur.get(params.id)

        if (!result.salesReturInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SalesRetur", params.id]]
            return result
        }

        result.salesReturInstance = SalesRetur.get(params.id)

        if (!result.salesReturInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SalesRetur", params.id]]
            return result
        }

        result.salesReturInstance = SalesRetur.get(params.id)

        if (!result.salesReturInstance)
            return fail(code: "default.not.found.message")

        try {
            result.salesReturInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        SalesRetur.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.salesReturInstance && m.field)
                    result.salesReturInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["SalesRetur", params.id]]
                return result
            }

            result.salesReturInstance = SalesRetur.get(params.id)

            if (!result.salesReturInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.salesReturInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            def cek = SalesRetur.createCriteria().list {
                eq("m803Pertanyaan",params.m803Pertanyaan.trim(),[ignoreCase : true])
            }

            if(cek){
                for(find in cek){
                    if(find?.id != result.salesReturInstance.id){
                        return [ada : "ada" , instance : result.salesReturInstance]
                    }
                }
            }


            result.salesReturInstance.properties = params

            if (result.salesReturInstance.hasErrors() || !result.salesReturInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SalesRetur", params.id]]
            return result
        }

        result.salesReturInstance = new SalesRetur()
        result.salesReturInstance.properties = params

        // success
        return result
    }


    def updateField(def params) {
        def salesRetur = SalesRetur.findById(params.id)
        if (salesRetur) {
            salesRetur."${params.name}" = params.value
            salesRetur.save()
            if (salesRetur.hasErrors()) {
                throw new Exception("${salesRetur.errors}")
            }
        } else {
            throw new Exception("SalesRetur not found")
        }
    }
    def massDelete(def params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            try{
                def ubh2 = SalesRetur.get(it)
                ubh2?.staDel = '1'
                ubh2?.lastUpdProcess = 'DELETE'
                ubh2?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ubh2?.lastUpdated = datatablesUtilService?.syncTime()
                ubh2?.save(flush: true)
                SalesReturDetail.findAllBySalesRetur(ubh2).each {
                    def ubh = SalesReturDetail.get(it.id)
                    ubh?.staDel = '1'
                    ubh?.lastUpdProcess = 'DELETE'
                    ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    ubh?.lastUpdated = datatablesUtilService?.syncTime()
                    ubh?.save(flush: true)

                    def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.goods,'0',params?.companyDealer)
                    if(!goodsStok){
                        def partsStokService = new PartsStokService()
                        partsStokService.newStock(it?.goods,params?.companyDealer)
                        goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.goods,'0',params?.companyDealer)
                    }
                        def kurangStok2=  PartsStok.get(goodsStok.id)
                        kurangStok2?.companyDealer = kurangStok2?.companyDealer
                        kurangStok2?.goods = it?.goods
                        kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - it?.qty
                        kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - it?.qty
                        kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - it?.qty
                        kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - it?.qty
                        kurangStok2.staDel = '0'
                        kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        kurangStok2.lastUpdProcess = 'RETUR'
                        kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                        kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                        kurangStok2?.save(flush: true)
                        kurangStok2.errors.each {
                            //println it
                        }

                }
            }catch(Exception a){

            }
        }
    }
}