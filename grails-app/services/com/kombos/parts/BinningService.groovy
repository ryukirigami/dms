package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class BinningService {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def serviceMethod() {

    }

    def datatablesList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Binning.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            ilike("staDel","0")
            if (params."sCriteria_noBinning") {
                ilike("t168ID","%"+ (params."sCriteria_noBinning" as String) +"%")
            }
            if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                if (params."sCriteria_tglFrom") {
                    def tanggalAwal = new Date().parse("dd/MM/yyyy",params.sCriteria_tglFrom)
                    def tanggalAKhir = new Date().parse("dd/MM/yyyy",params.sCriteria_tglTo)
                    ge("t168TglJamBinning",tanggalAwal)
                    lt("t168TglJamBinning", tanggalAKhir + 1)
                }
            }

            order("t168ID","desc")
        }




        def rows = []

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        def data=true
        results.each {
            data=false
            if (params."criteriaParts"=="1") {
                if(BinningDetail.findByBinning(it)?.staStockIn=="1"){
                    data=true
                }
            }else if (params."criteriaParts"=="0") {
                if(BinningDetail.findByBinning(it)?.staStockIn!="1"){
                    data=true
                }
            }else{
                data=true
            }
            if( data==true){
                rows << [
                    id: it.id,
                    nomorBinning: it.t168ID,
                    tanggalBinning: sdf.format(it.t168TglJamBinning),
                    petugasBinning: it.t168PetugasBinning,
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubList(def params) {
        def c = BinningDetail.createCriteria()
        def results = c.list {
            binning{
                eq("companyDealer",params?.companyDealer)
            }
            eq("binning", Binning.findByT168IDIlike(params."nomorBinning"))
            ilike("staDel","0")
        }

        def rows = []

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        DateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy")

        results.each {
            rows << [
                    id: it.id,
                    kodeParts: it.goodsReceiveDetail?.goods?.m111ID,
                    namaParts: it.goodsReceiveDetail?.goods?.m111Nama,
                    noReff : it.goodsReceiveDetail?.goodsReceive?.t167NoReff,
                    tglJamReceive: sdf.format(it.goodsReceiveDetail?.goodsReceive?.t167TglJamReceive),
                    nomorPO: it.goodsReceiveDetail?.poDetail?.po?.t164NoPO?it.goodsReceiveDetail?.poDetail?.po?.t164NoPO:it.goodsReceiveDetail?.updatedBy,
                    tanggalPO: it.goodsReceiveDetail?.poDetail?.po? sdf.format(it.goodsReceiveDetail?.poDetail?.po.t164TglPO):sdf.format(PO.findByT164NoPOLike("%"+it.goodsReceiveDetail?.updatedBy+"%").t164TglPO),
                    nomorInvoice: it.goodsReceiveDetail?.invoice?.t166NoInv,
                    tglInvoice: it.goodsReceiveDetail?.invoice? sdf2.format(it.goodsReceiveDetail?.invoice.t166TglInv):'-',
                    qtyInvoice: it.goodsReceiveDetail?.t167Qty1Reff + " " + it.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    qtyReceive1: it.goodsReceiveDetail?.t167Qty1Issued + " " + it.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    qtyRusak1: it.goodsReceiveDetail?.t167Qty1Rusak + " " + it.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    qtySalah1: it.goodsReceiveDetail?.t167Qty1Salah + " " + it.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }


    def inputBinningDatatablesList(def params) {
        def results = new ArrayList<BinningDetail>()
        def rows = []
        if (params."binningId") {
            Binning binning = Binning.findById(params."binningId")
            results = binning.binningDetail
            results.each {
                rows << [
                        id: it.id,
                        kodePart: it.goodsReceiveDetail?.goods?.m111ID,
                        namaPart: it.goodsReceiveDetail?.goods?.m111Nama,
                        tglJamReceive : it.goodsReceiveDetail?.goodsReceive?.t167TglJamReceive,
                        nomorPO : it.goodsReceiveDetail?.poDetail?.po?.t164NoPO,
                        tglPO : it.goodsReceiveDetail?.poDetail?.po?.t164TglPO,
                        nomorInvoice : it.goodsReceiveDetail?.invoice?.t166NoInv,
                        tglInvoice : it.goodsReceiveDetail?.invoice?.t166TglInv,
                        qInvoice: it.goodsReceiveDetail?.t167Qty1Reff,
                        qReceive: it.goodsReceiveDetail?.t167Qty1Issued,
                        qRusak: it.goodsReceiveDetail?.t167Qty1Rusak,
                        qSalah: it.goodsReceiveDetail?.t167Qty1Salah
                ]
            }

            //println "results = $results"

        }

        [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def datatablesListInputAddParts(def params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        def c = GoodsReceiveDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            binning{
                eq("companyDealer",session?.userCompanyDealer)
            }
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    kodeParts: it.goods.m111ID,
                    namaParts: it.goods.m111Nama,
                    nomorPO : it.poDetail?.po?.t164NoPO,
                    tglPO : it.poDetail?.po?.t164TglPO,
                    nomorInvoice : it.invoice?.t166NoInv,
                    tglInvoice : it.invoice?.t166TglInv,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def massDeleteBinning(params){
        def jsonArray = JSON.parse(params.ids)
        //println "jsonArray=" + jsonArray
        def binnings = []

        jsonArray.each {
            //println "id binning : "+it
            binnings << Binning.get(it)
        }

        binnings.each {
            def detail = BinningDetail.findAllByBinning(it)
            //println("detail=" + detail)
            detail.each {
                def stockINDetail = StockINDetail.findAllByBinningDetail(it)
                //println("stockINDetail=" + stockINDetail)
                stockINDetail.each {
                    it.delete()
                }
                it.delete()
            }
            it.delete();
        }
    }
    def massDeleteBinningDetail(params){
        def jsonArray = JSON.parse(params.ids)
        //println "jsonArray=" + jsonArray
        def binningDetails = []

        jsonArray.each {
            //println "id binning detail : "+it
            binningDetails << BinningDetail.get(it)
        }

        binningDetails.each {
            def stockINDetail = StockINDetail.findAllByBinningDetail(it)
            //println("stockINDetail=" + stockINDetail)
            stockINDetail.each {
                it.delete()
            }
            it.delete()
        }
    }

    def create(def params) {
    }

    def save(def params) {
    }

    def show(def params) {
    }

    def update(def params) {
    }

    def delete(def params) {
    }

}
