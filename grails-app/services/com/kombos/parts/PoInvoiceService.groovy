package com.kombos.parts

class PoInvoiceService {
    def conversi = new Konversi()

    def datatablesSubList(def params) {
        def c = PODetail.createCriteria()
        def results = c.list {
            po{
                eq("id",params.id.toLong())
            }

        }

        def rows = []

        results.each {
            def inv = Invoice?.findByPoAndGoodsAndStaDel(it.po,it?.validasiOrder?.requestDetail?.goods,"0")
            rows << [
                    id: it?.po?.id,

                    kodePart: it?.validasiOrder?.requestDetail?.goods?.m111ID,

                    namaPart: it?.validasiOrder?.requestDetail?.goods?.m111Nama,

                    qty: it?.t164Qty1,

                    qtyReceive: inv?.t166Qty1,

                    qtySelisih: it?.t164Qty1!=null && inv?.t166Qty1!=null ? it?.t164Qty1-inv?.t166Qty1 : "",

                    idPart : it?.validasiOrder?.requestDetail?.goods?.id

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

    }

    def datatablesSubSubList(def params) {
        def poId = PODetail?.get(params.id)
        Goods good =  Goods.get(params.idPart)
        def c = Invoice.createCriteria()
        def results = c.list {
            eq("companyDealer",params.companyDealer)
            po{
                eq("id", params.id as Long)
            }
                eq("goods", good)
        }

        def rows = []
        Invoice invoice
        PODetail po
        results.each {
            invoice = it
            po = PODetail?.findByPo(invoice.po)
            rows << [
                    id: invoice.id,

                    noInvoice: invoice.t166NoInv,

                    tglInvoice: invoice.t166TglInv.format("dd/MM/yyyy"),

                    retailPrice: invoice.t166RetailPrice ? conversi.toRupiah(invoice.t166RetailPrice) : 0,

                    discount: invoice.t166Disc,

                    netSalePrice: invoice.t166NetSalesPrice ? conversi.toRupiah(invoice.t166NetSalesPrice) : 0,

                    qtyReceive: invoice.t166Qty1
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }
}
