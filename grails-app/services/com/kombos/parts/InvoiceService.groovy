package com.kombos.parts

import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class InvoiceService {
    def conversi = new Konversi()
    def datatablesUtilService

    def addParts(User  user, params) {
        def jsonArray = JSON.parse(params.ids)
        PODetail podetil
        jsonArray.each {
            podetil = PODetail.get(it)
            def t166TglInv = new Date().parse("dd/MM/yyyy", params.t166TglInv as String)
            Calendar start = Calendar.getInstance();
            start.setTime(t166TglInv);
            params.remove("t166TglInv")

            params.t166TglInv = "date.struct"
            params.t166TglInv_day = ""+start.get(Calendar.DATE)
            params.t166TglInv_month = ""+(start.get(Calendar.MONTH)+1)
            params.t166TglInv_year = ""+start.get(Calendar.YEAR)

            Invoice invoice = new Invoice()

            invoice.t166NoInv = params.t166NoInv
            invoice.t166TglInv = t166TglInv
            invoice.po = podetil.po
            invoice.goods = podetil.validasiOrder?.requestDetail?.goods
            invoice.t166Qty1 = 0
            invoice.t166Qty2 = 0
            invoice.t166RetailPrice = 0
            invoice.t166Disc = 0
            invoice.t166NetSalesPrice = 0
            invoice.staDel = '0'
            invoice.t166xNamaUser = user.username
            invoice.t166xNamaDivisi = user.divisi==null?"no division":user.divisi.m012NamaDivisi
            invoice.createdBy = user.username
            invoice.updatedBy = user.username
            invoice.lastUpdProcess = "INSERT"
            invoice.dateCreated = datatablesUtilService?.syncTime()
            invoice.lastUpdated = datatablesUtilService?.syncTime()

            invoice.save()
            invoice.errors.each{
                //println it
            }
            return invoice
        }


    }

    def datatablesSubList(def params) {
        String criteria = " 1=1 "
        def param = []
        if(params."tanggalStart"){
            def start = params."tanggalStart"
            def end = params."tanggalEnd"
            criteria = criteria + " and a.t166TglInv between ? and ? "
            param = [start,end]
        }

        String staSPLD = " 1=1 "
        if(params."staSPLD1" && params."staSPLD2" == null){
            staSPLD = staSPLD + " and a.po.vendor.m121staSPLD = ${params.staSPLD1} "
        }

        if(params."staSPLD2" && params."staSPLD1" == null){
            staSPLD = staSPLD + " and a.po.vendor.m121staSPLD = ${params.staSPLD2} "
        }

        if(params."staSPLD1" && params."staSPLD2"){
            staSPLD = staSPLD + " and a.po.vendor.m121staSPLD in (${params.staSPLD1},${params.staSPLD1}) "
        }
        criteria = criteria + "and (" + staSPLD + ") "

        if(params.search_noInv){
            criteria = criteria + " and a.t166NoInv =  '" + params.search_noInv + "' "
        }

        def results = Invoice.executeQuery("SELECT a.t166NoInv, a.t166TglInv, SUM(a.t166NetSalesPrice) as totalNetSalesPrice FROM Invoice a where a.staDel != '1' and a.po.vendor.id = ${params.vendorId} and "+ criteria +" and a.companyDealer.id = ${params?.companyDealer?.id} group by a.t166NoInv, a.t166TglInv order by a.t166TglInv desc, a.t166NoInv ",param)

        def rows = []
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
        results.each {
            rows << [
                    id: it[0],

                    t166NoInv: it[0],

                    t166TglInv: sdf.format(it[1]),

                    t166NetSalesPrice: it[2] ? conversi.toRupiah(it[2]) : 0

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def datatablesSubSubList(def params) {
        String criteria = " 1=1 "
        def param = []
        if(params."tanggalStart"){
            def start = params."tanggalStart"
            def end = params."tanggalEnd"
            criteria = criteria + " and a.t166TglInv between ? and ? "
            param = [start,end]
        }

        String staSPLD = " 1=1 "
        if(params."staSPLD1" && params."staSPLD2" == null){
            staSPLD = staSPLD + " and a.po.vendor.m121staSPLD = ${params.staSPLD1} "
        }

        if(params."staSPLD2" && params."staSPLD1" == null){
            staSPLD = staSPLD + " and a.po.vendor.m121staSPLD = ${params.staSPLD2}"
        }

        if(params."staSPLD1" && params."staSPLD2"){
            staSPLD = staSPLD + " and a.po.vendor.m121staSPLD in (${params.staSPLD1},${params.staSPLD1}) "
        }

        criteria = criteria + " and (" + staSPLD + ") "

        def results = Invoice.executeQuery("SELECT a FROM Invoice a where a.staDel != '1' and a.t166NoInv = '${params.invoiceNo}' and a.companyDealer.id = ${params.companyDealer.id} and "+criteria,param)

        def rows = []
        Invoice invoice
        PO po
        results.each {
            invoice = it
            po = invoice.po
            rows << [
                    id: po.id,

                    kodePart: invoice.goods.m111ID,

                    namaPart: invoice.goods.m111Nama,

                    noPO: po.t164NoPO,

                    tanggalPO: po.t164TglPO.format("dd/MM/yyyy"),

                    tipeOrder: ValidasiOrder.findByRequestDetail(RequestDetail.findByGoods(invoice.goods))?.partTipeOrder?.m112TipeOrder,

                    qtyReceive: invoice.t166Qty2,

                    retailPrice: invoice.t166RetailPrice ? conversi.toRupiah(invoice.t166RetailPrice) : 0,

                    discount: invoice.t166Disc,

                    netSalePrice: invoice.t166NetSalesPrice ? conversi.toRupiah(invoice.t166NetSalesPrice) : 0
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }
}
