package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.InvoiceT701
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class PickingSlipService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PickingSlip.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_Tanggal" && params."sCriteria_Tanggal2") {
                ge("t141TglPicking",  params."sCriteria_Tanggal")
                lt("t141TglPicking", params."sCriteria_Tanggal2" + 1)
            }
            if (params."sCriteria_Wo") {
                reception{
                    ilike("t401NoWO","%"+ params."sCriteria_Wo" +"%")
                }
            }
            eq("staDel","0")
            order("t141TglPicking","desc")

        }

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
        def rows = []
        int juml = 0
        results.each {
            juml++
            def nopol = it?.reception?.historyCustomerVehicle.fullNoPol
            rows << [
                    id: it.id,
                    t141ID: it.t141ID,
                    t141Wo: it?.reception?.t401NoWO,
                    tanggal: sdf.format(it?.t141TglPicking ? it?.t141TglPicking : it?.dateCreated),
                    noPol: nopol,
                    sa: it?.reception?.t401NamaSA,
                    cv: it?.reception?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {
        //println "t141ID " + params.t141ID
        //println "tanggal " + params.tanggal
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = PickingSlipDetail.createCriteria()
        def results = c.list{
            pickingSlip{
                ilike("t141ID",  params.t141ID)
                eq("companyDealer",params?.companyDealer)
            }
            eq("staDel","0")

        }
        //println "list Subtable : " + results
        def rows = []

        results.each {
            rows << [
                    id: it?.id,
                    goods: it.goods?.m111ID,
                    goods2: it.goods?.m111Nama,
                    t142Qty1: it.t142Qty1,
                    satuan: it.goods?.satuan?.m118Satuan1,
                    t141ID: params.t141ID,
                    tanggal: params.tanggal,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PickingSlip", params.id]]
            return result
        }

        result.pickingSlipInstance = PickingSlipDetail.get(params.id)

        if (!result.pickingSlipInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PickingSlip", params.id]]
            return result
        }

        result.pickingSlipInstance = PickingSlipDetail.get(params.id)

        if (!result.pickingSlipInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PickingSlip", params.id]]
            return result
        }

        result.pickingSlipInstance = PickingSlipDetail.get(params.id)

        if (!result.pickingSlipInstance)
            return fail(code: "default.not.found.message")

        try {
            result.pickingSlipInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {

        PickingSlip.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.pickingSlipInstance && m.field)
                    result.pickingSlipInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["PickingSlip", params.id]]
                return result
            }

            result.pickingSlipInstance = PickingSlipDetail.get(params.id)

            if (!result.pickingSlipInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.pickingSlipInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            def pickingSlipInstance2 = PickingSlipDetail.get(params.id)
            pickingSlipInstance2.t142Qty1 = Double.parseDouble(params.t142Qty1)
            pickingSlipInstance2.t142Qty2 = Double.parseDouble(params.t142Qty1)
            pickingSlipInstance2.lastUpdProcess = 'UPDATE'
            pickingSlipInstance2.lastUpdated = datatablesUtilService?.syncTime()

            if (pickingSlipInstance2.hasErrors() || !pickingSlipInstance2.save())
                return fail(code: "default.not.updated.message")

            // Success.
            def awal = result.pickingSlipInstance.t142Qty1
            def akhir = pickingSlipInstance2.t142Qty1
            def hasil = akhir - awal

            def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(result.pickingSlipInstance?.goods,'0',params.userCompanyDealer)
            if(!goodsStok){
                def partsStokService = new PartsStokService()
                partsStokService.newStock(result.pickingSlipInstance?.goods,params.userCompanyDealer)
                goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(result.pickingSlipInstance?.goods,'0',params.userCompanyDealer)
            }
                def kurangStok2 =  PartsStok.get(goodsStok.id)
                kurangStok2?.companyDealer = params.userCompanyDealer
                kurangStok2?.goods = result.pickingSlipInstance?.goods
                kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - hasil
                kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - hasil
                kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - hasil
                kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - hasil
                kurangStok2.staDel = '0'
                kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                kurangStok2.lastUpdProcess = 'U_PICKING'
                kurangStok2.dateCreated =  datatablesUtilService?.syncTime()
                kurangStok2.lastUpdated =  datatablesUtilService?.syncTime()
                kurangStok2?.save(flush: true)
                kurangStok2.errors.each {
                    //println it
                }

            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PickingSlip", params.id]]
            return result
        }

        result.pickingSlipInstance = new PickingSlip()
        result.pickingSlipInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.pickingSlipInstance && m.field)
                result.pickingSlipInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["PickingSlip", params.id]]
            return result
        }

        result.pickingSlipInstance = new PickingSlip(params)

        if (result.pickingSlipInstance.hasErrors() || !result.pickingSlipInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def pickingSlip = PickingSlip.findById(params.id)
        if (pickingSlip) {
            pickingSlip."${params.name}" = params.value
            pickingSlip.save()
            if (pickingSlip.hasErrors()) {
                throw new Exception("${pickingSlip.errors}")
            }
        } else {
            throw new Exception("PickingSlip not found")
        }
    }
    def massDelete(params){
        def data = "sukses"
        //println "ini id hapus "+params.ids
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each {
            def hapus = null
            try {
                hapus = PickingSlipDetail.get(it)
            }catch (e){

            }
            if(hapus==null){
                def hapus3 = PickingSlip.findByT141IDAndStaDelAndCompanyDealer(it,'0',params.userCompanyDealer)
                def inv= InvoiceT701.findByReceptionAndT701StaDelAndT701StaApprovedReversalIsNull(hapus3.reception,'0')
                if(!inv){
                    hapus3.staDel = "1"
                    hapus3.lastUpdProcess = 'DELETE'
                    hapus3.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    hapus3.lastUpdated =  datatablesUtilService?.syncTime()
                    hapus3.save(flush: true)

                    def hapus2 = PickingSlipDetail.findAllByPickingSlipAndStaDel(PickingSlip.findByT141ID(it),'0').each {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',params.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(it.goods,params.userCompanyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',params.userCompanyDealer)
                        }
                            def kurangStok2 =  PartsStok.get(goodsStok.id)
                            kurangStok2?.companyDealer = params.userCompanyDealer
                            kurangStok2?.goods = it.goods
                            kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free + it.t142Qty1
                            kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free + it.t142Qty1
                            kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 + it.t142Qty1
                            kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 + it.t142Qty1
                            kurangStok2.staDel = '0'
                            kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok2.lastUpdProcess = 'PIKING DELETE'
                            kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                            kurangStok2.lastUpdated =  datatablesUtilService?.syncTime()
                            kurangStok2?.save(flush: true)
                            kurangStok2.errors.each {
                                //println it
                            }
                        it.staDel = '1'
                        it.lastUpdProcess = 'DELETE'
                        it.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        it.save(flush: true)
                    }
                }else{
                    data = "not"
                }
            }else{
                def inv= InvoiceT701.findByReceptionAndT701StaDelAndT701StaApprovedReversalIsNull(hapus.pickingSlip.reception,'0')
                if(!inv){
                    try {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(hapus.goods,'0',params.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(hapus.goods,params.userCompanyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(hapus.goods,'0',params.userCompanyDealer)
                        }
                            def kurangStok2 =  PartsStok.get(goodsStok.id)
                            kurangStok2?.companyDealer = params.userCompanyDealer
                            kurangStok2?.goods = hapus.goods
                            kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free + hapus.t142Qty1
                            kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free + hapus.t142Qty1
                            kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 + hapus.t142Qty1
                            kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 + hapus.t142Qty1
                            kurangStok2.staDel = '0'
                            kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok2.lastUpdProcess = 'uPICKING'
                            kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                            kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                            kurangStok2?.save(flush: true)
                            kurangStok2.errors.each {
                                //println it
                            }


                        hapus.staDel = '1'
                        hapus.lastUpdProcess = 'DELETE'
                        hapus.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        hapus.save(flush: true)
                    }catch (e){
                    }
                }else{
                    data = "not"
                }
            }
        }
        return data
    }

}