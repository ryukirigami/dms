package com.kombos.parts

import com.kombos.baseapp.AppSettingParam

class DpPartsService {
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = DpParts.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
               eq("staDel","0")

            if(params."sCriteria_m162TglBerlaku"){
                ge("m162TglBerlaku", params."sCriteria_m162TglBerlaku")
                lt("m162TglBerlaku", params."sCriteria_m162TglBerlaku" + 1)
            }

            if(params."sCriteria_m162BatasNilaiKenaDP1"){
                eq("m162BatasNilaiKenaDP1", params."sCriteria_m162BatasNilaiKenaDP1" as BigDecimal )
            }

            if(params."sCriteria_m162BatasNilaiKenaDP2"){
                eq("m162BatasNilaiKenaDP2", params."sCriteria_m162BatasNilaiKenaDP2" as BigDecimal )
            }

            if(params."sCriteria_m162PersenDP"){
                eq("m162PersenDP", params."sCriteria_m162PersenDP" as Double )
            }

            if(params."sCriteria_m162MaxHariBayarDP"){
                eq("m162MaxHariBayarDP", params."sCriteria_m162MaxHariBayarDP" as Integer )
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m162TglBerlaku: it.m162TglBerlaku ? it.m162TglBerlaku.format(dateFormat) : "",

                    m162BatasNilaiKenaDP1: it.m162BatasNilaiKenaDP1,

                    m162BatasNilaiKenaDP2: it.m162BatasNilaiKenaDP2,

                    m162PersenDP: it.m162PersenDP,

                    m162MaxHariBayarDP: it.m162MaxHariBayarDP,

            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }
}
