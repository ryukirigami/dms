package com.kombos.parts



import com.kombos.baseapp.AppSettingParam

class ETAService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = ETA.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_po"){
				eq("po",params."sCriteria_po")
			}

			if(params."sCriteria_goods"){
				eq("goods",params."sCriteria_goods")
			}

			if(params."sCriteria_t165Qty1"){
				eq("t165Qty1",params."sCriteria_t165Qty1")
			}

			if(params."sCriteria_t165ETA"){
				ge("t165ETA",params."sCriteria_t165ETA")
				lt("t165ETA",params."sCriteria_t165ETA" + 1)
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						po: it.po,
			
						goods: it.goods,
			
						t165Qty1: it.t165Qty1,
			
						t165ETA: it.t165ETA?it.t165ETA.format(dateFormat):"",
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ETA", params.id] ]
			return result
		}

		result.ETAInstance = ETA.get(params.id)

		if(!result.ETAInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ETA", params.id] ]
			return result
		}

		result.ETAInstance = ETA.get(params.id)

		if(!result.ETAInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ETA", params.id] ]
			return result
		}

		result.ETAInstance = ETA.get(params.id)

		if(!result.ETAInstance)
			return fail(code:"default.not.found.message")

		try {
			result.ETAInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		ETA.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.ETAInstance && m.field)
					result.ETAInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["ETA", params.id] ]
				return result
			}

			result.ETAInstance = ETA.get(params.id)

			if(!result.ETAInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.ETAInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.ETAInstance.properties = params

			if(result.ETAInstance.hasErrors() || !result.ETAInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ETA", params.id] ]
			return result
		}

		result.ETAInstance = new ETA()
		result.ETAInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.ETAInstance && m.field)
				result.ETAInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["ETA", params.id] ]
			return result
		}

		result.ETAInstance = new ETA(params)

		if(result.ETAInstance.hasErrors() || !result.ETAInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def ETA =  ETA.findById(params.id)
		if (ETA) {
			ETA."${params.name}" = params.value
			ETA.save()
			if (ETA.hasErrors()) {
				throw new Exception("${ETA.errors}")
			}
		}else{
			throw new Exception("ETA not found")
		}
	}

}