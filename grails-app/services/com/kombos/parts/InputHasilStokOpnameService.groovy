package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class InputHasilStokOpnameService {

    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def list(params){
        DateFormat df=new SimpleDateFormat("dd-MM-yyyy");
        def result = [:]
        def fail = { Map m ->
            result.error = "error"
            return result
        }

        def stok = StokOPName.createCriteria().list {
            eq("companyDealer",params?.companyDealer)
        }
        int a=0
        stok.each {
            boolean cek=false;
            if(it.t132Jumlah11==null){
                cek=true;
            } else if(it.t132Jumlah11!=null && it.t132Jumlah12==null){
                if(it.t132Jumlah11!=it.t132Jumlah1Stock){
                    cek=true;
                }
            } else if(it.t132Jumlah11!=null && it.t132Jumlah12!=null && it.t132Jumlah13==null){
                if(it.t132Jumlah12!=it.t132Jumlah1Stock){
                    cek=true;
                }
            }

            if(cek){
                if(a==0){
                    result.t132ID = it.t132ID
                    result.t132Tanggal = df.format(it.t132Tanggal)
                }else{
                    if(result.t132ID>=it.t132ID){
                        result.t132ID = it.t132ID
                        result.t132Tanggal = df.format(it.t132Tanggal)
                    }
                }
                a++;
            }
        }

        // Success.
        return result

    }

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = StokOPName.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            eq("t132StaUpdateStock","0")
            eq("t132ID",params."sCriteria_t132ID")


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            boolean cek=false;
            double jum1=0;
            if(it.t132Jumlah11==null){
                cek=true;
            } else if(it.t132Jumlah11!=null && it.t132Jumlah12==null){
                if(it.t132Jumlah11!=it.t132Jumlah1Stock){
                    cek=true;
                    jum1=it.t132Jumlah11;
                }
            } else if(it.t132Jumlah11!=null && it.t132Jumlah12!=null && it.t132Jumlah13==null){
                if(it.t132Jumlah12!=it.t132Jumlah1Stock){
                    cek=true;
                    jum1=it.t132Jumlah12;
                }
            }

            if(cek){
                rows << [


                        id: it.id,

                        t132ID: it.t132ID,

                        t132Tanggal: it.t132Tanggal?it.t132Tanggal.format(dateFormat):"",

                        lokasigoods: it.goods.location.m120NamaLocation,

                        namagoods : it.goods.goods.m111Nama,

                        kodegoods : it.goods.goods.m111ID,

                        satuangoods : it.goods.goods.satuan.m118KodeSatuan ,

                        icc : it.location.parameterICC.m155NamaICC ,

                        qty :  jum1,

                        t132Jumlah1Stock: it.t132Jumlah1Stock,

                        t132Jumlah2Stock: it.t132Jumlah2Stock,

                        location: it.location

                ]
            }

        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def editQty(params){
        String hasil="success"
        def jsonId = JSON.parse(params.ids)
        def jsonQty = JSON.parse(params.qtys)
        int a=0;
        jsonId.each {
            int sama=0;
            boolean cekKosong=true;
            def stok= StokOPName.get(it)
            double qty1=Double.parseDouble(jsonQty[a]);

            if(qty1==stok?.t132Jumlah1Stock){
                sama=1;
            }

            if(stok?.t132Jumlah11==null){
                stok?.t132Jumlah11=qty1
                stok?.t132Jumlah21=qty1
            }else if(stok?.t132Jumlah11!=null && stok?.t132Jumlah12==null){
                stok?.t132Jumlah12=qty1
                stok?.t132Jumlah22=qty1
            }else if(stok?.t132Jumlah11!=null && stok?.t132Jumlah12!=null && stok?.t132Jumlah13==null){
                stok?.t132Jumlah13=qty1
                stok?.t132Jumlah23=qty1
                hasil="full";
            }
            if(sama==1){
                stok?.t132StaUpdateStock="1"
            }
            stok?.lastUpdated = datatablesUtilService?.syncTime()
            stok?.save(flush: true)
            a++
        }
        return hasil

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.inputHasilStokOpnameInstance = StokOPName.get(params.id)

        if(!result.inputHasilStokOpnameInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.inputHasilStokOpnameInstance = StokOPName.get(params.id)

        if(!result.inputHasilStokOpnameInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.inputHasilStokOpnameInstance = StokOPName.get(params.id)

        if(!result.inputHasilStokOpnameInstance)
            return fail(code:"default.not.found.message")

        try {
            result.inputHasilStokOpnameInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }

    def update(params) {
        StokOPName.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.inputHasilStokOpnameInstance && m.field)
                    result.inputHasilStokOpnameInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["StokOPName", params.id] ]
                return result
            }

            result.inputHasilStokOpnameInstance = StokOPName.get(params.id)

            if(!result.inputHasilStokOpnameInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.inputHasilStokOpnameInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.inputHasilStokOpnameInstance.properties = params
            result.inputHasilStokOpnameInstance.t132StaUpdateStock = "1"

            if(result.inputHasilStokOpnameInstance.hasErrors() || !result.inputHasilStokOpnameInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.inputHasilStokOpnameInstance = new StokOPName()
        result.inputHasilStokOpnameInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.inputHasilStokOpnameInstance && m.field)
                result.inputHasilStokOpnameInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.inputHasilStokOpnameInstance = new StokOPName(params)

        if(result.inputHasilStokOpnameInstance.hasErrors() || !result.inputHasilStokOpnameInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        // success
        return result
    }

    def updateField(def params){
        def inputHasilStokOpname =  StokOPName.findById(params.id)
        if (inputHasilStokOpname) {
            inputHasilStokOpname."${params.name}" = params.value
            inputHasilStokOpname.save()
            if (inputHasilStokOpname.hasErrors()) {
                throw new Exception("${inputHasilStokOpname.errors}")
            }
        }else{
            throw new Exception("StokOPName not found")
        }
    }

}