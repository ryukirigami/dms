package com.kombos.parts

import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class StockINService {

    def serviceMethod() {

    }
    def datatablesList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = StockIN.createCriteria()
        def results = null
        if (params.search_noReff) {
            results = c.list{
                eq("companyDealer",params?.companyDealer)
                ilike("staDel","0")
                if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                    if (params."sCriteria_tglFrom") {
                        def tanggalAwal = new Date().parse("dd/MM/yyyy",params.sCriteria_tglFrom)
                        def tanggalAKhir = new Date().parse("dd/MM/yyyy",params.sCriteria_tglTo)
                        ge("t169TglJamStockIn",tanggalAwal)
                        lt("t169TglJamStockIn", tanggalAKhir + 1)
                    }
                }
                order("id","desc")
            }
        }else{
            results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("companyDealer",params?.companyDealer)
                ilike("staDel","0")
                if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                    if (params."sCriteria_tglFrom") {
                        def tanggalAwal = new Date().parse("dd/MM/yyyy",params.sCriteria_tglFrom)
                        def tanggalAKhir = new Date().parse("dd/MM/yyyy",params.sCriteria_tglTo)
                        ge("t169TglJamStockIn",tanggalAwal)
                        lt("t169TglJamStockIn", tanggalAKhir + 1)
                    }
                }
                order("id","desc")
            }
        }

        def rows = []

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        def noref = "";def tampil=false
        int jumlah = 0
        results.each {
            tampil=false
            try {
                noref = it?.stockINDetail?.last().binningDetail?.goodsReceiveDetail?.goodsReceive?.t167NoReff
            }catch(Exception e){}
            if (params.search_noReff) {
                if(noref.toUpperCase().contains((params."search_noReff" as String).toUpperCase())){
                    tampil=true
                    jumlah++
                }
            }else{
                tampil=true
            }
            if(tampil==true){
                rows << [
                    id: it.id,
                    nomorStockIN: it.t169ID + " / " + noref,
                    tanggalStockIN: sdf.format(it.t169TglJamStockIn),
                    petugasStockIN: it.t169PetugasStockIn,
                    nomorStockINs: it.t169ID,
                ]
            }
        }
        if (params.search_noReff) {
            [sEcho: params.sEcho, iTotalRecords:  jumlah, iTotalDisplayRecords: jumlah, aaData: rows]
        }else{
            [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }
    }

    def datatablesSubList(def params) {
        def c = StockINDetail.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            stockIN{
                eq("companyDealer",params?.companyDealer)
            }

            eq("stockIN", StockIN.findByT169ID(params."nomorStockIN"))
            ilike("staDel","0")
        }

        def rows = []

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

        results.each {
            BinningDetail binningDetail = it.binningDetail;
            Binning binning = binningDetail?.binning;
            GoodsReceiveDetail goodsReceiveDetail = binningDetail?.goodsReceiveDetail;
            Goods goods = goodsReceiveDetail?.goods;
            Satuan satuan = goods?.satuan;
            Location location = it.location;

            rows << [
                    id: it.binningDetail.id,
                    kodeParts: goods?.m111ID, // it.binningDetail?.goodsReceiveDetail?.goods?.m111ID,
                    namaParts: goods?.m111Nama, // it.binningDetail?.goodsReceiveDetail?.goods?.m111Nama,
                    noReff: it?.binningDetail?.goodsReceiveDetail?.goodsReceive?.t167NoReff,
                    tglJamBinning: sdf.format(binning?.t168TglJamBinning),  //it.binningDetail?.binning?.t168TglJamBinning),
                    nomorBinning: binning?.t168ID,  // it.binningDetail?.binning?.t168ID,
                    qtyReceive: goodsReceiveDetail?.t167Qty1Issued + " " + satuan?.m118Satuan1,        //it.binningDetail?.goodsReceiveDetail?.t167Qty1Issued + " " + it.binningDetail?.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    qtyBinning: binningDetail?.t168Qty1Binning + " " + satuan?.m118Satuan1, //it.binningDetail?.t168Qty1Binning + " " + it.binningDetail?.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    qtyRusak1: goodsReceiveDetail?.t167Qty1Rusak + " " + satuan?.m118Satuan1,    // it.binningDetail?.goodsReceiveDetail?.t167Qty1Rusak + " " + it.binningDetail?.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    qtySalah1: goodsReceiveDetail?.t167Qty1Salah + " " + satuan?.m118Satuan1,    // it.binningDetail?.goodsReceiveDetail?.t167Qty1Salah + " " + it.binningDetail?.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    qtyStockIn1: it.t169Qty1StockIn + " " + satuan?.m118Satuan1,       //it.t169Qty1StockIn + " " + it.binningDetail?.goodsReceiveDetail?.goods?.satuan?.m118Satuan1,
                    namaLocation: location?.m120NamaLocation, //it.location.m120NamaLocation,
                    namaLocationRusak:it.locationRusak?.m120NamaLocation,     //it.locationRusak.m120NamaLocation,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def inputStockINDatatablesList(def params) {
        def results = new ArrayList<StockINDetail>()
        def rows = []
        if (params."stockINId") {
            StockIN stokin = StockIN.findById(params."stockINId")
            results = stokin.stockINDetail
            results.each {
                rows << [
                    id: it.id,
                    pilih: false,
                    kodePart: it?.binningDetail.goodsReceiveDetail?.goods?.m111ID,
                    namaPart: it.binningDetail?.goodsReceiveDetail?.goods?.m111Nama,
                    tglJamBinning : it.binningDetail?.binning?.t168TglJamBinning,
                    nomorBinning : it.binningDetail?.binning?.t168ID,
                    qReceive : it.binningDetail?.goodsReceiveDetail?.t167Qty1Issued,
                    qBinning : it.binningDetail?.t168Qty1Binning,
                    qRusak : it.binningDetail?.goodsReceiveDetail?.t167Qty1Rusak,
                    qSalah : it.binningDetail?.goodsReceiveDetail?.t167Qty1Salah,
                    qStockIN : it.t169Qty1StockIn,
                    qLokasiRak : "0",   //it.binningDetail?.goodsReceiveDetail?.t167Qty2Salah,
                    qLokasiRakRusakSalah : "0", //it.binningDetail?.goodsReceiveDetail?.t167Qty1Reff,

                ]
            }

            //println "results = $results"

        }

        [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def datatablesListInputAddParts(def params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        def c = BinningDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            binning{
                eq("companyDealer",session?.userCompanyDealer)
            }
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    pilih: false,
                    kodeParts: it.goodsReceiveDetail?.goods?.m111ID,
                    namaParts: it.goodsReceiveDetail?.goods?.m111Nama,
                    nomorPO : it.goodsReceiveDetail?.poDetail?.po?.t164NoPO,
                    tglPO : it.goodsReceiveDetail?.poDetail?.po?.t164TglPO,
                    nomorInvoice : it.goodsReceiveDetail?.invoice?.t166NoInv,
                    tglInvoice : it.goodsReceiveDetail?.invoice?.t166TglInv,
                    nomorBinning : it.binning?.t168ID,
                    tglBinning : it.binning?.t168TglJamBinning,
            ]
        }

        //println "results = $results"

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def massDeleteStockIN(params){
        def jsonArray = JSON.parse(params.ids)
        //println "jsonArray=" + jsonArray
        def stockINs = []

        jsonArray.each {
            //println "id binning : "+it
            stockINs << StockIN.get(it)
        }

        stockINs.each {
            def detail = StockINDetail.findAllByStockIN(it)
            //println("detail=" + detail)
            detail.each {
                it.delete()
            }
            it.delete();
        }
    }
    def massDeleteStockINDetail(params){
        def jsonArray = JSON.parse(params.ids)
        //println "jsonArray=" + jsonArray
        def stockINDetails = []
        jsonArray.each {
            //println "id stock IN detail : "+it
            stockINDetails << StockINDetail.get( Long.parseLong(it))
        }

        stockINDetails.each {
            it.delete()
        }
    }

    def create(def params) {
    }

    def save(def params) {
    }

    def show(def params) {
    }

    def update(def params) {
    }

    def delete(def params) {
    }

}
