package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartsTransferStokService implements AfterApprovalInterface{
    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)


    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsTransferStok.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            if(params.sCriteria_status=='1'){
                eq("cekBaca",'-')
            }else{
                ne("cekBaca",'-')
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("companyDealerTujuan", "companyDealerTujuan")
                groupProperty("staApprove", "staApprove")
            }
        }
        def rows = []
        def isApprove = ["Approved","UnApproved","Waiting for Approval"]
        results.each {
            rows << [
                    companyDealerTujuan: it.companyDealerTujuan.m011NamaWorkshop,
                    staApprove: it?.staApprove ? isApprove[it?.staApprove?.toInteger()] : "",
                    tglTransferStok1: params."sCriteria_t017Tanggal",
                    tglTransferStok2: params."sCriteria_t017Tanggal2",
                    sCriteria_status: params.sCriteria_status
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = PartsTransferStok.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params.tglTransferStok1 && params.tglTransferStok2) {
                Date date41 = df.parse(params.tglTransferStok1)
                Date date42 = df.parse(params.tglTransferStok2)
                ge("tglTransferStok", date41)
                lt("tglTransferStok", date42+1)
            }
            eq("companyDealer",params?.companyDealer)
             companyDealerTujuan{
                 eq("m011NamaWorkshop",params.companyDealerTujuan)
             }
            if(params.sCriteria_status=='1'){
                eq('cekBaca','-')
            }else{
                ne('cekBaca','-')
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("nomorPO", "nomorPO")
                groupProperty("tglTransferStok", "tglTransferStok")
                groupProperty("cekBaca", "cekBaca")
                if(params.sCriteria_status=='1'){
                    groupProperty("noPengiriman", "noPengiriman")
                }
            }
        }
        def rows = []

        results.each {
            rows << [
                    nomorPO: it.nomorPO,

                    statusBaca: it?.cekBaca=='0'?'Belum Ditindak Lanjuti':it?.cekBaca=='1'?'Sudah Ditindak Lanjuti':'-',

                    tglTransferStok: it.tglTransferStok?.format(dateFormat),

                    companyDealerTujuan : params.companyDealerTujuan,

                    noPengiriman : it?.cekBaca=='-'? it.noPengiriman : ''
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def datatablesSubSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsTransferStok.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",params?.CD)
            companyDealerTujuan{
                eq("m011NamaWorkshop",params.companyDealerTujuan)
            }
            eq("nomorPO",params.nomorPO)
        }

        def rows = []

        results.each {
             rows << [
                    id: it.id,
                    goods: it.goods?.m111ID,
                    goods2: it.goods?.m111Nama,
                    noPO: it.nomorPO,
                    hargaBeli : GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it.goods,params.CD,"0")?.t150Harga,
                    qty : it.qtyParts
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            String data = it
            def transfer = null
            def cd = null
            def noPo = null
            try{
                transfer = PartsTransferStok.findById(it)
                if(!transfer){
                    noPo = PartsTransferStok.findByNomorPO(it)
                    if(!noPo){
                        cd = PartsTransferStok.findByCompanyDealerTujuan(CompanyDealer.findByM011NamaWorkshop(it))
                    }
                }
            }catch(Exception a){
            }

            if(transfer){
                def ubh = PartsTransferStok.get(it)
                ubh?.staDel = '1'
                ubh?.lastUpdProcess = 'DELETE'
                ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ubh?.lastUpdated = datatablesUtilService?.syncTime()
                ubh?.save(flush: true)
            }else if(noPo){
                PartsTransferStok.findAllByNomorPOAndStaDel(it,'0').each{
                    def ubh = PartsTransferStok.get(it)
                    ubh?.staDel = '1'
                    ubh?.lastUpdProcess = 'DELETE'
                    ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    ubh?.lastUpdated = datatablesUtilService?.syncTime()
                    ubh?.save(flush: true)
                }
            }else{
                try{
                    PartsTransferStok.findAllByCompanyDealerTujuanAndStaDel(CompanyDealer.findByM011NamaWorkshop(it),'0').each{
                        def ubh = PartsTransferStok.get(it.id)
                        ubh?.staDel = '1'
                        ubh?.lastUpdProcess = 'DELETE'
                        ubh?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        ubh?.save(flush: true)
                    }
                }catch(Exception a){

                }

            }
        }

    }

    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        if(staApproval==StatusApproval.APPROVED){
            def parameter = fks.split("#")
            def partTransfer = PartsTransferStok.findAllByNomorPOAndStaDel(parameter[0],"0")
            partTransfer.each {
                it.staApprove = "0"
                it.save(flush: true)
            }
            if(parameter.size()>1){
                def ids = parameter[1].split("-")
                ids.each {

                }
            }
        }else if(staApproval==StatusApproval.REJECTED){
            def parameter = fks.split("#")
            def partTransfer = PartsTransferStok.findAllByNomorPOAndStaDel(parameter[0],"0")
            partTransfer.each {
                it.staApprove = "1"
                it.save(flush: true)
            }
            if(parameter.size()>1){
                def ids = parameter[1].split("-")
                ids.each {

                }
            }
        }
    }
}
