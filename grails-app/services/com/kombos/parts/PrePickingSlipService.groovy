package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.board.JPB
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP
import org.hibernate.criterion.CriteriaSpecification

class PrePickingSlipService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def jum= 0
        def results
        def resultstOT
        def rows = []
        if(params.sCriteria_status=='0'){
            def ctOT = Reception.createCriteria()
            resultstOT = ctOT.list {
                eq("companyDealer",params?.companyDealer)
                if(params.sCriteria_noWo){
                    ilike("t401NoWO","%"+params.sCriteria_noWo+"%")
                }
                eq("staDel",'0')
                eq("staSave",'0')
                if (params."sCriteria_Tanggal" && params."sCriteria_Tanggal2") {
                    ge("t401TglJamRencana",  params."sCriteria_Tanggal")
                    lt("t401TglJamRencana", params."sCriteria_Tanggal2" + 1)
                }
                isNull("t401StaInvoice")
                isNotNull("t401TglJamRencana")
                order("t401TglJamRencana","desc")

            }
            def count = 0
            resultstOT.each {
                if(PartsRCP.findByReceptionAndStaDel(it,'0')){
                    count +=1
                }
            }
            def c = Reception.createCriteria()
            results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("companyDealer",params?.companyDealer)
                if(params.sCriteria_noWo){
                    ilike("t401NoWO","%"+params.sCriteria_noWo+"%")
                }
                eq("staDel",'0')
                eq("staSave",'0')
                if (params."sCriteria_Tanggal" && params."sCriteria_Tanggal2") {
                    ge("t401TglJamRencana",  params."sCriteria_Tanggal")
                    lt("t401TglJamRencana", params."sCriteria_Tanggal2" + 1)
                }
                isNull("t401StaInvoice")
                isNotNull("t401TglJamRencana")
                order("t401TglJamRencana","desc")

            }
            results.each {
                if(PartsRCP.findByReceptionAndStaDel(it,'0')){
                    rows << [
                            t141Wo: it?.t401NoWO,
                            tanggal: it?.t401TglJamRencana? it?.t401TglJamRencana.format("dd/MM/yyyy HH:mm") : "",
                            noPol: it.historyCustomerVehicle.fullNoPol,
                            sa: it?.t401NamaSA,
                            sCriteria_status : params.sCriteria_status
                    ]
                }
            }
            results.totalCount = count
        }else{
            def c = PickingSlip.createCriteria()
            results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("companyDealer",params?.companyDealer)
                eq("staDel",'0')
                reception{
                    if(params.sCriteria_noWo){
                        ilike("t401NoWO","%"+params.sCriteria_noWo+"%")
                    }
                    if (params."sCriteria_Tanggal" && params."sCriteria_Tanggal2") {
                        ge("t401TglJamRencana",  params."sCriteria_Tanggal")
                        lt("t401TglJamRencana", params."sCriteria_Tanggal2" + 1)
                    }
                    isNotNull("t401TglJamRencana")
                    order("t401TglJamRencana","desc")
                }
            }
            results.each {
                rows << [
                        t141Wo: it.reception?.t401NoWO,
                        tanggal: it.reception?.t401TglJamRencana? it.reception?.t401TglJamRencana.format("dd/MM/yyyy HH:mm") : "",
                        noPol: it.reception.historyCustomerVehicle.fullNoPol,
                        sa: it.reception?.t401NamaSA,
                        sCriteria_status : params.sCriteria_status
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = PartsRCP.createCriteria()
        def results = c.list{
            reception{
                eq("t401NoWO",params.t141Wo)
                eq("companyDealer",params?.companyDealer)
            }
            or{
                eq("t403StaTambahKurang","0")
                isNull("t403StaTambahKurang")
            }
            eq("staDel",'0')
        }
        //println "list: " + results
        def rows = []
        def tampil = false
        def qtySdhDiambil = 0, qtySisa = 0
        results.each {
            tampil = false
            def data = it
            qtySdhDiambil = 0
            def picking = PickingSlip.findAllByReceptionAndStaDel(Reception.findByT401NoWO(params.t141Wo),'0').each {
                def pickingSlipDetail = PickingSlipDetail.findByGoodsAndPickingSlipAndStaDel(data.goods,it,'0')
                qtySdhDiambil += PickingSlipDetail.findByGoodsAndPickingSlipAndStaDel(data.goods,it,'0')? PickingSlipDetail.findByGoodsAndPickingSlipAndStaDel(data.goods,it,'0')?.t142Qty1 : 0
                if(pickingSlipDetail){
                    tampil = true
                }
            }
            qtySdhDiambil = qtySdhDiambil? qtySdhDiambil:0
            qtySisa =  it?.t403Jumlah1 - qtySdhDiambil
            if(params.sCriteria_status=='1'){
                if(tampil==true){
                    rows << [
                            goods: it.goods?.m111ID,
                            goods2: it.goods?.m111Nama,
                            t142Qty1: qtySdhDiambil,
                            satuan: it.goods?.satuan.m118Satuan1,
                            location : KlasifikasiGoods.findByGoods(it.goods)?.getLocation()?.m120NamaLocation,
                    ]
                }
            }else{
                if(qtySisa!=0){
                    rows << [
                            goods: it.goods?.m111ID,
                            goods2: it.goods?.m111Nama,
                            t142Qty1:qtySisa,
                            satuan: it.goods?.satuan.m118Satuan1,
                            location : KlasifikasiGoods.findByGoods(it.goods)?.getLocation()?.m120NamaLocation,
                    ]
                }
            }

        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }


}