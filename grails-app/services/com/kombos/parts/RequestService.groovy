package com.kombos.parts

import grails.converters.JSON

class RequestService {

    def datatablesUtilService

    def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.requestInstance && m.field)
				result.requestInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Request", params.id] ]
			return result
		}
		def req = new Request()
		req.t162TglRequest = datatablesUtilService?.syncTime()
		req.dateCreated = datatablesUtilService?.syncTime()
		req.lastUpdated = datatablesUtilService?.syncTime()
		req.t162StaFA = params.t162StaFA
		req.t162NoReff = params.t162NoReff
		req.companyDealer = params.companyDealer
		req?.staDel = "0"
        req?.lastUpdProcess = "INSERT"
        req.t162NamaPemohon = org.apache.shiro.SecurityUtils.subject.principal.toString()
		req.t162xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
		req.t162xNamaDivisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi?.m012NamaDivisi
		req.save(flush:true)
		req.errors.each{
			//println it
		}
		def jsonArray = JSON.parse(params.ids)
		jsonArray.each {
			def goods = Goods.get(it)
			if(goods){
                def cQty = (params."qty-${it}" as String).replace(',','.')
				RequestDetail reqDetail = new RequestDetail()
				reqDetail.goods = goods
				reqDetail.t162Qty1 = cQty as Double
				reqDetail.t162Qty2 = cQty as Double
				reqDetail.t162Qty1Available = goods?.partsStok?.size()>0 ? goods?.partsStok?.last()?.t131Qty1 : 0
				reqDetail.t162Qty2Available = goods?.partsStok?.size()>0 ? goods?.partsStok?.last()?.t131Qty2 : 0
				reqDetail.request = req
				reqDetail.dateCreated = datatablesUtilService?.syncTime()
				reqDetail.lastUpdated =datatablesUtilService?.syncTime()
				reqDetail.save(flush: true)
				reqDetail.errors.each{
					//println it
				}
			}
		}

		result.requestInstance = req

		if(result.requestInstance.hasErrors() || !result.requestInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
}