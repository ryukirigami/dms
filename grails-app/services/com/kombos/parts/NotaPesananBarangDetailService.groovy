package com.kombos.parts



import com.kombos.baseapp.AppSettingParam

class NotaPesananBarangDetailService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NotaPesananBarangDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_notaPesananBarang") {
                eq("notaPesananBarang", params."sCriteria_notaPesananBarang")
            }

            if (params."sCriteria_goods") {
                eq("goods", params."sCriteria_goods")
            }

            if (params."sCriteria_jumlah") {
                eq("jumlah", params."sCriteria_jumlah")
            }

            if (params."sCriteria_hoTglDiterima") {
                ge("hoTglDiterima", params."sCriteria_hoTglDiterima")
                lt("hoTglDiterima", params."sCriteria_hoTglDiterima" + 1)
            }

            if (params."sCriteria_hoTglDiteruskan") {
                ge("hoTglDiteruskan", params."sCriteria_hoTglDiteruskan")
                lt("hoTglDiteruskan", params."sCriteria_hoTglDiteruskan" + 1)
            }

            if (params."sCriteria_ktrJktTglDiterima") {
                ge("ktrJktTglDiterima", params."sCriteria_ktrJktTglDiterima")
                lt("ktrJktTglDiterima", params."sCriteria_ktrJktTglDiterima" + 1)
            }

            if (params."sCriteria_ktrJktTglDiproses") {
                ge("ktrJktTglDiproses", params."sCriteria_ktrJktTglDiproses")
                lt("ktrJktTglDiproses", params."sCriteria_ktrJktTglDiproses" + 1)
            }

            if (params."sCriteria_ktrJktTglBukaDO") {
                ge("ktrJktTglBukaDO", params."sCriteria_ktrJktTglBukaDO")
                lt("ktrJktTglBukaDO", params."sCriteria_ktrJktTglBukaDO" + 1)
            }

            if (params."sCriteria_ktrJktTglKrmKeCabang") {
                ge("ktrJktTglKrmKeCabang", params."sCriteria_ktrJktTglKrmKeCabang")
                lt("ktrJktTglKrmKeCabang", params."sCriteria_ktrJktTglKrmKeCabang" + 1)
            }

            if (params."sCriteria_cabangTglDiterima") {
                ge("cabangTglDiterima", params."sCriteria_cabangTglDiterima")
                lt("cabangTglDiterima", params."sCriteria_cabangTglDiterima" + 1)
            }

            if (params."sCriteria_cabangKeterangan") {
                ilike("cabangKeterangan", "%" + (params."sCriteria_cabangKeterangan" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    notaPesananBarang: it.notaPesananBarang,

                    goods: it.goods,

                    jumlah: it.jumlah,

                    hoTglDiterima: it.hoTglDiterima ? it.hoTglDiterima.format(dateFormat) : "",

                    hoTglDiteruskan: it.hoTglDiteruskan ? it.hoTglDiteruskan.format(dateFormat) : "",

                    ktrJktTglDiterima: it.ktrJktTglDiterima ? it.ktrJktTglDiterima.format(dateFormat) : "",

                    ktrJktTglDiproses: it.ktrJktTglDiproses ? it.ktrJktTglDiproses.format(dateFormat) : "",

                    ktrJktTglBukaDO: it.ktrJktTglBukaDO ? it.ktrJktTglBukaDO.format(dateFormat) : "",

                    ktrJktTglKrmKeCabang: it.ktrJktTglKrmKeCabang ? it.ktrJktTglKrmKeCabang.format(dateFormat) : "",

                    cabangTglDiterima: it.cabangTglDiterima ? it.cabangTglDiterima.format(dateFormat) : "",

                    cabangKeterangan: it.cabangKeterangan,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["NotaPesananBarangDetail", params.id]]
            return result
        }

        result.notaPesananBarangDetailInstance = NotaPesananBarangDetail.get(params.id)

        if (!result.notaPesananBarangDetailInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["NotaPesananBarangDetail", params.id]]
            return result
        }

        result.notaPesananBarangDetailInstance = NotaPesananBarangDetail.get(params.id)

        if (!result.notaPesananBarangDetailInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["NotaPesananBarangDetail", params.id]]
            return result
        }

        result.notaPesananBarangDetailInstance = NotaPesananBarangDetail.get(params.id)

        if (!result.notaPesananBarangDetailInstance)
            return fail(code: "default.not.found.message")

        try {
            result.notaPesananBarangDetailInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        NotaPesananBarangDetail.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.notaPesananBarangDetailInstance && m.field)
                    result.notaPesananBarangDetailInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["NotaPesananBarangDetail", params.id]]
                return result
            }

            result.notaPesananBarangDetailInstance = NotaPesananBarangDetail.get(params.id)

            if (!result.notaPesananBarangDetailInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.notaPesananBarangDetailInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.notaPesananBarangDetailInstance.properties = params

            if (result.notaPesananBarangDetailInstance.hasErrors() || !result.notaPesananBarangDetailInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["NotaPesananBarangDetail", params.id]]
            return result
        }

        result.notaPesananBarangDetailInstance = new NotaPesananBarangDetail()
        result.notaPesananBarangDetailInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.notaPesananBarangDetailInstance && m.field)
                result.notaPesananBarangDetailInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["NotaPesananBarangDetail", params.id]]
            return result
        }

        result.notaPesananBarangDetailInstance = new NotaPesananBarangDetail(params)

        if (result.notaPesananBarangDetailInstance.hasErrors() || !result.notaPesananBarangDetailInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def notaPesananBarangDetail = NotaPesananBarangDetail.findById(params.id)
        if (notaPesananBarangDetail) {
            notaPesananBarangDetail."${params.name}" = params.value
            notaPesananBarangDetail.save()
            if (notaPesananBarangDetail.hasErrors()) {
                throw new Exception("${notaPesananBarangDetail.errors}")
            }
        } else {
            throw new Exception("NotaPesananBarangDetail not found")
        }
    }

}