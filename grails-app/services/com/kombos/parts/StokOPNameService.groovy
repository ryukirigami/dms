package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class StokOPNameService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = StokOPName.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_t132Tanggal" && params."sCriteria_t132Tanggalakhir") {
                ge("t132Tanggal", params."sCriteria_t132Tanggal")
                lt("t132Tanggal", params."sCriteria_t132Tanggalakhir" + 1)
            }

            if (params."sCriteria_t132ID") {
                ilike("t132ID", "%"+params."sCriteria_t132ID"+"%")
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t132ID", "t132ID")
                groupProperty("t132Tanggal", "t132Tanggal")
            }
        }
        def rows = []

        results.sort() {
            it.t132ID
        }

        results.each {
            rows << [

                    t132ID: it.t132ID,

                    t132Tanggal: it.t132Tanggal?it.t132Tanggal.format(dateFormat):"",

            ]
        }



        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = StokOPName.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            if(params.t132ID){
                eq("t132ID", params.t132ID)
            }
        }

        def rows = []

        results.sort {
            it.goods.goods.m111ID
        }

        results.each {
            rows << [

                    id: it.id,

                    lokasigoods: it.goods.location.m120NamaLocation,

                    namagoods : it.goods.goods.m111Nama,

                    kodegoods : it.goods.goods.m111ID,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.stokOPNameInstance = StokOPName.get(params.id)

        if(!result.stokOPNameInstance)
            return fail(code:"default.not.found.message")

        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.stokOPNameInstance = StokOPName.get(params.id)

        if(!result.stokOPNameInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.stokOPNameInstance = StokOPName.get(params.id)

        if(!result.stokOPNameInstance)
            return fail(code:"default.not.found.message")

        try {
            result.stokOPNameInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }

    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def hapus = new StokOPName()
            if(it.isNumber()==true){
                hapus = StokOPName.get(it)
            }else{
                hapus = StokOPName.findAllByT132ID(it)
            }
            hapus.each {
                it.delete()
            }
        }

    }

    def update(params) {
        StokOPName.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.stokOPNameInstance && m.field)
                    result.stokOPNameInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["StokOPName", params.id] ]
                return result
            }

            result.stokOPNameInstance = StokOPName.get(params.id)

            if(!result.stokOPNameInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.stokOPNameInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.stokOPNameInstance.properties = params

            if(result.stokOPNameInstance.hasErrors() || !result.stokOPNameInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["StokOPName", params.id] ]
            return result
        }

        result.stokOPNameInstance = new StokOPName()
        result.stokOPNameInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def jsonArray = JSON.parse(params.ids)

        jsonArray.each {
            def goods = KlasifikasiGoods.get(it)
            def partStok = new PartsStok()
            partStok=PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods.goods,"0",params?.companyDealer)
            def add = new StokOPName()
            add?.goods = goods
            add?.location = goods.location
            add?.t132Jumlah1Stock = partStok?.t131Qty1 ? partStok?.t131Qty1 : 0
            add?.t132Jumlah2Stock = partStok?.t131Qty2 ? partStok?.t131Qty2 : 0
            add?.setT132Tanggal(df.parse(params.tanggal))
            add?.setT132ID(params.kode)
            add?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            add?.lastUpdProcess = "BARU"
            add?.dateCreated = params?.dateCreated
            add?.lastUpdated = params?.lastUpdated
            add?.setT132StaUpdateStock("0")
        }

        return result
    }

    def updateField(def params){
        def stokOPName =  StokOPName.findById(params.id)
        if (stokOPName) {
            stokOPName."${params.name}" = params.value
            stokOPName.save()
            if (stokOPName.hasErrors()) {
                throw new Exception("${stokOPName.errors}")
            }
        }else{
            throw new Exception("StokOPName not found")
        }
    }

}