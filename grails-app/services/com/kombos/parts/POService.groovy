package com.kombos.parts



import com.kombos.baseapp.AppSettingParam

class POService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = PO.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_t164NoPO"){
				ilike("t164NoPO","%" + (params."sCriteria_t164NoPO" as String) + "%")
			}

			if(params."sCriteria_t164TglPO"){
				ge("t164TglPO",params."sCriteria_t164TglPO")
				lt("t164TglPO",params."sCriteria_t164TglPO" + 1)
			}

			if(params."sCriteria_vendor"){
				eq("vendor",params."sCriteria_vendor")
			}

			if(params."sCriteria_validasiOrder"){
				eq("validasiOrder",params."sCriteria_validasiOrder")
			}

			if(params."sCriteria_request"){
				eq("request",params."sCriteria_request")
			}

			if(params."sCriteria_goods"){
				eq("goods",params."sCriteria_goods")
			}

			if(params."sCriteria_t164Qty1"){
				eq("t164Qty1",params."sCriteria_t164Qty1")
			}

			if(params."sCriteria_t164Qty2"){
				eq("t164Qty2",params."sCriteria_t164Qty2")
			}

			if(params."sCriteria_t164HargaSatuan"){
				eq("t164HargaSatuan",params."sCriteria_t164HargaSatuan")
			}

			if(params."sCriteria_t164TotalHarga"){
				eq("t164TotalHarga",params."sCriteria_t164TotalHarga")
			}

			if(params."sCriteria_t164StaOpenCancelClose"){
				ilike("t164StaOpenCancelClose","%" + (params."sCriteria_t164StaOpenCancelClose" as String) + "%")
			}

			if(params."sCriteria_t164xNamaUser"){
				ilike("t164xNamaUser","%" + (params."sCriteria_t164xNamaUser" as String) + "%")
			}

			if(params."sCriteria_t164xNamaDivisi"){
				ilike("t164xNamaDivisi","%" + (params."sCriteria_t164xNamaDivisi" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						t164NoPO: it.t164NoPO,
			
						t164TglPO: it.t164TglPO?it.t164TglPO.format(dateFormat):"",
			
						vendor: it.vendor,
			
						validasiOrder: it.validasiOrder,
			
						request: it.request,
			
						goods: it.goods,
			
						t164Qty1: it.t164Qty1,
			
						t164Qty2: it.t164Qty2,
			
						t164HargaSatuan: it.t164HargaSatuan,
			
						t164TotalHarga: it.t164TotalHarga,
			
						t164StaOpenCancelClose: it.t164StaOpenCancelClose,
			
						t164xNamaUser: it.t164xNamaUser,
			
						t164xNamaDivisi: it.t164xNamaDivisi,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}

		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["PO", params.id] ]
			return result
		}

		result.POInstance = PO.get(params.id)

		if(!result.POInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["PO", params.id] ]
			return result
		}

		result.POInstance = PO.get(params.id)

		if(!result.POInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["PO", params.id] ]
			return result
		}

		result.POInstance = PO.get(params.id)

		if(!result.POInstance)
			return fail(code:"default.not.found.message")

		try {
			result.POInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		PO.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.POInstance && m.field)
					result.POInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["PO", params.id] ]
				return result
			}

			result.POInstance = PO.get(params.id)

			if(!result.POInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.POInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.POInstance.properties = params

			if(result.POInstance.hasErrors() || !result.POInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["PO", params.id] ]
			return result
		}

		result.POInstance = new PO()
		result.POInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.POInstance && m.field)
				result.POInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["PO", params.id] ]
			return result
		}

		result.POInstance = new PO(params)

		if(result.POInstance.hasErrors() || !result.POInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def PO =  PO.findById(params.id)
		if (PO) {
			PO."${params.name}" = params.value
			PO.save()
			if (PO.hasErrors()) {
				throw new Exception("${PO.errors}")
			}
		}else{
			throw new Exception("PO not found")
		}
	}

}