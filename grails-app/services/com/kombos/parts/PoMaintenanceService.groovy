package com.kombos.parts

import grails.converters.JSON

class PoMaintenanceService {
    def conversi = new Konversi()

    def datatablesSubList(def params) {
        String criteria = " 1=1 "
        def param = []
        criteria = criteria + " and a.po.staDel = '0' "
        if(params."tanggalStart"){
            def start = params."tanggalStart"
            def end = params."tanggalEnd"
            criteria = criteria + " and a.po.t164TglPO between ? and ? "
            param = [start,end]
        }
        if(params."sta"){
            if(params."sta" == 'X'){
                criteria = criteria + " and (a.po.t164StaOpenCancelClose = null or a.po.t164StaOpenCancelClose = ''"
            }else{
                criteria = criteria + " and a.po.t164StaOpenCancelClose = ${params.sta} "
            }
        }

        def c = PODetail.createCriteria()

        def results = c.list {
            po{
                eq("companyDealer",params?.companyDealer)
                eq("id",params.id as Long)
            }

        }

        def rows = []

        results.each {
            def invoice = Invoice.findByGoods(it.validasiOrder?.requestDetail?.goods)
            def qtyRef = GoodsReceiveDetail.findByGoodsAndInvoice(it.validasiOrder?.requestDetail?.goods, invoice)?.t167Qty1Reff
            if(qtyRef==null){
                qtyRef = 0
            }
            def qtyIssued = GoodsReceiveDetail.findByGoodsAndInvoice(it.validasiOrder?.requestDetail?.goods, invoice)?.t167Qty1Issued
            if(qtyIssued==null){
                qtyIssued = 0
            }
            rows << [
                    id: it.id,

                    kodePart: it.validasiOrder?.requestDetail?.goods.m111ID,

                    namaPart: it.validasiOrder?.requestDetail?.goods.m111Nama,

                    qty: GoodsReceiveDetail.findByGoodsAndInvoice(it.validasiOrder?.requestDetail?.goods, invoice)?.t167Qty1Reff?GoodsReceiveDetail.findByGoodsAndInvoice(it.validasiOrder?.requestDetail?.goods, invoice)?.t167Qty1Reff:'-',

                    qtyReceive: GoodsReceiveDetail.findByGoodsAndInvoice(it.validasiOrder?.requestDetail?.goods, invoice)?GoodsReceiveDetail.findByGoodsAndInvoice(it.validasiOrder?.requestDetail?.goods, invoice).t167Qty1Issued:'-',

                    qtySelisih: qtyRef - qtyIssued > 0 ? qtyRef - qtyIssued : "-",

                    idPart : it.validasiOrder?.requestDetail?.goods?.id
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def datatablesSubSubList(def params) {
        String criteria = " 1=1 "
        def param = []
        criteria = criteria + " and a.po.staDel = '0' "
        if(params."tanggalStart"){
            def start = params."tanggalStart"
            def end = params."tanggalEnd"
            criteria = criteria + " and a.po.t164TglPO between ? and ? "
            param = [start,end]
        }
        if(params."sta"){
            if(params."sta" == 'X'){
                criteria = criteria + " and (a.po.t164StaOpenCancelClose = null or a.po.t164StaOpenCancelClose = ''"
            }else{
                criteria = criteria + " and a.po.t164StaOpenCancelClose = ${params.sta} "
            }
        }
        def c = Invoice.createCriteria()

        def poId= PODetail.get(params.id).po?.id
        Goods good =  Goods.get(params.idPart)
        def results = c.list {
            eq("companyDealer",params?.companyDealer)
            po{
                eq("id",poId as Long)
            }
            eq("goods",good)
        }

        def rows = []
        Invoice invoice
        PO po
        results.each {
            invoice = it
            po = invoice.po
            rows << [
                    id: invoice.id,

                    noInvoice: invoice.t166NoInv,

                    tglInvoice: invoice.t166TglInv.format("dd/MM/yyyy"),

                    retailPrice: invoice.t166RetailPrice ? conversi.toRupiah(invoice.t166RetailPrice) : 0,

                    discount: invoice.t166Disc,

                    netSalePrice: invoice.t166NetSalesPrice ? conversi.toRupiah(invoice.t166NetSalesPrice) : 0,

                    qtyReceive: invoice.t166Qty2
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def massAction(def params){
        log.info(params.ids)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << PO.get(it)  }

        //oList*.discard() //detach all the objects from session
        oList.each{
            //println "Masuk "
            def status = ""
            if(params.sta=='0'){
                status = POStatus.OPEN
            }else if(params.sta=='1'){
                status = POStatus.CANCEL
            }else if(params.sta=='2'){
                status = POStatus.CLOSE
            }
            it?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            it?.lastUpdProcess = "UPDATE"
            it?.staDel = "0"
            it?.t164StaOpenCancelClose = status
            it.save(flush:true)
        }
    }
}
