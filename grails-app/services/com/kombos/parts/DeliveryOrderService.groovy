package com.kombos.parts

import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.finance.Journal
import com.kombos.finance.JournalDetail
import com.kombos.finance.JournalPendingMasukService
import com.kombos.finance.Transaction
import grails.converters.JSON

class DeliveryOrderService {

    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = DeliveryOrder.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            sorNumber{
                if (params."sCriteria_sorNumber") {
                    ilike("sorNumber", "%"+params."sCriteria_sorNumber"+"%")
                }
            }

            if (params."sCriteria_doNumber") {
                ilike("doNumber", "%"+params."sCriteria_doNumber"+"%")
            }

            if (params."sCriteria_status") {
                ilike("paymentStatus", "%"+params."sCriteria_status"+"%")
            }

            sorNumber{
                if (params."sCriteria_customerName") {
                    ilike("customerName", "%"+params."sCriteria_customerName"+"%")
                }
            }

            if(params?."sCriteria_tggllimitDate"){
                ge("limitDate",params?."sCriteria_tggllimitDate")
                lt("limitDate",params?."sCriteria_tggllimitDate"+1)
            }
            if (params."sCriteria_intext") {
                ilike("intext", "%"+params."sCriteria_intext"+"%")
            }
            eq("companyDealer",  params.companyDealer )
            order("dateCreated","desc")
//            switch (sortProperty) {
//                default:
//                    order(sortProperty, sortDir)
//                    break;
//            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it?.id,

                    sorNumber: it?.sorNumber?.sorNumber,

                    doNumber: it?.doNumber,

                    limitDate: it?.limitDate?.format("dd/MM/yyyy"),

                    customerName: it?.sorNumber?.customerName,

                    intext: it?.intext,

                    status: it?.paymentStatus
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSOList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SalesOrderDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            sorNumber{
                eq("id",params.idSO.toLong())
            }

        }

        def rows = []
        def konversi = new Konversi()
        def total = 0
        results.each {
            def jumlah = it?.discount && it?.discount > 0 ? it.quantity * it.unitPrice - (it.discount/100 * it.quantity * it.unitPrice) : it.quantity * it.unitPrice
            total+=jumlah
            rows << [

                    id: it.id,

                    m111IDAdd: it.materialCode.m111ID,

                    m111NamaAdd: it.materialCode.m111Nama,

                    qtyAdd: it.quantity,

                    hargaAdd: konversi.toRupiah(it.unitPrice),

                    satuanAdd : it.materialCode.satuan.m118Satuan1,

                    discountAdd : it?.discount,

                    jumlahAdd : konversi.toRupiah(jumlah),

                    total : total

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows,total:total]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DeliveryOrder", params.id]]
            return result
        }

        result.deliveryOrderInstance = DeliveryOrder.get(params.id)

        if (!result.deliveryOrderInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DeliveryOrder", params.id]]
            return result
        }

        result.deliveryOrderInstance = DeliveryOrder.get(params.id)

        if (!result.deliveryOrderInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DeliveryOrder", params.id]]
            return result
        }

        result.deliveryOrderInstance = DeliveryOrder.get(params.id)

        if (!result.deliveryOrderInstance)
            return fail(code: "default.not.found.message")

        try {
            result.deliveryOrderInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        DeliveryOrder.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.deliveryOrderInstance && m.field)
                    result.deliveryOrderInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["DeliveryOrder", params.id]]
                return result
            }

            result.deliveryOrderInstance = DeliveryOrder.get(params.id)

            if (!result.deliveryOrderInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.deliveryOrderInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            def cek = DeliveryOrder.createCriteria().list {
                eq("m803Pertanyaan",params.m803Pertanyaan.trim(),[ignoreCase : true])
            }

            if(cek){
                for(find in cek){
                    if(find?.id != result.deliveryOrderInstance.id){
                        return [ada : "ada" , instance : result.deliveryOrderInstance]
                    }
                }
            }


            result.deliveryOrderInstance.properties = params

            if (result.deliveryOrderInstance.hasErrors() || !result.deliveryOrderInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DeliveryOrder", params.id]]
            return result
        }

        result.deliveryOrderInstance = new DeliveryOrder()
        result.deliveryOrderInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.deliveryOrderInstance && m.field)
                result.deliveryOrderInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["DeliveryOrder", params.id]]
            return result
        }

        result.deliveryOrderInstance = new DeliveryOrder(params)

        def cek = DeliveryOrder.createCriteria().list {
            eq("m803Pertanyaan",params.m803Pertanyaan.trim(),[ignoreCase : true])
        }

        if(cek){
            return [ada : "ada" , instance : result.deliveryOrderInstance]
        }


        if (result.deliveryOrderInstance.hasErrors() || !result.deliveryOrderInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def deliveryOrder = DeliveryOrder.findById(params.id)
        if (deliveryOrder) {
            deliveryOrder."${params.name}" = params.value
            deliveryOrder.save()
            if (deliveryOrder.hasErrors()) {
                throw new Exception("${deliveryOrder.errors}")
            }
        } else {
            throw new Exception("DeliveryOrder not found")
        }
    }

    def massDelete(def params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            try{
                def ubh2 = DeliveryOrder.get(it)
                ubh2?.staDel = '1'
                ubh2?.lastUpdProcess = 'DELETE'
                ubh2?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ubh2?.save(flush: true)
                def sorNumber  = ubh2.sorNumber

                //BalikinStok
                SalesOrderDetail.findAllBySorNumberAndStaDel(sorNumber,"0").each {
                    def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.materialCode,'0',sorNumber?.companyDealer)
                    if(!goodsStok){
                        def partsStokService = new PartsStokService()
                        partsStokService.newStock(it.materialCode,sorNumber?.companyDealer)
                        goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.materialCode,'0',sorNumber?.companyDealer)
                    }
                    def kurangStok =  PartsStok.get(goodsStok.id)
                    kurangStok?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    kurangStok?.lastUpdProcess = 'TUNAI_DELETE'
                    kurangStok?.t131Qty1 = kurangStok?.t131Qty1 + it?.quantity
                    kurangStok?.t131Qty2 = kurangStok?.t131Qty2 + it?.quantity
                    kurangStok?.t131Qty1Free = kurangStok.t131Qty1Free + it?.quantity
                    kurangStok?.t131Qty2Free = kurangStok.t131Qty2Free + it?.quantity
                    kurangStok.lastUpdated = datatablesUtilService?.syncTime()
                    kurangStok?.save(flush: true)
                    kurangStok.errors.each {
                        println it
                    }
                }

                //MenghapusJurnal
                try {
                    def journalId = Journal.findAllByDescriptionIlikeAndCompanyDealerAndStaDel("%"+sorNumber.sorNumber+"%",sorNumber.companyDealer,"0")
                    journalId.each { j->
                        def journalDel = Journal.get(j.id as Long)
                        journalDel.staDel = '1'
                        journalDel.lastUpdProcess = 'DELETE'
                        journalDel.save(flush: true)
                        journalDel.each {
                            println it
                        }

                        def journalDetail = JournalDetail.findAllByJournal(journalDel)
                        journalDetail.each {
                            def jDel = JournalDetail.get(it.id as Long)
                            jDel.staDel = '1'
                            jDel.lastUpdProcess = 'DELETE'
                            jDel.save(flush: true)
                            jDel.each {
                                println it
                            }
                        }
                        try {
                            def transa = Transaction.findAllByJournal(journalDel)
                            transa.each {
                                def jDel = Transaction.get(it.id as Long)
                                jDel.staDel = '1'
                                jDel.lastUpdProcess = 'DELETE'
                                jDel.save(flush: true)
                                jDel.each {
                                    println it
                                }
                            }
                        }catch (Exception e){

                        }
                    }
                }catch (Exception e){
                    println e
                }
            }catch(Exception a){

            }
        }
    }

}