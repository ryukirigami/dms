package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.StockOpnameLog

class StokOPNameFreezeService {

    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def freeze(def params,def session){
        def companyDealerInstance = session.userCompanyDealer
        def stokDel = StockOpnameLog.findByCompanyDealerAndStaStockOpnameAndStaDel(companyDealerInstance,'1','0')
        if(stokDel){
            def ubahStokDel = StockOpnameLog.get(stokDel.id)
            ubahStokDel.staDel='1'
            ubahStokDel.updatedBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
            ubahStokDel.dateCreated =datatablesUtilService?.syncTime()
            ubahStokDel.lastUpdated = datatablesUtilService?.syncTime()
            ubahStokDel?.save(flush: true)
            ubahStokDel.errors.each {
                //println it
            }
        }

        def stokLog = new StockOpnameLog()
        stokLog?.companyDealer = companyDealerInstance
        stokLog?.staStockOpname = '0'
        stokLog?.staDel = '0'
        stokLog?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stokLog?.lastUpdProcess = 'INSERT'
        stokLog?.dateCreated = datatablesUtilService?.syncTime()
        stokLog?.lastUpdated = datatablesUtilService?.syncTime()
        stokLog?.save(flush: true)
        stokLog.errors.each {
            //println it
        }

        def hasil = []
        return hasil
    }

    def unfreeze(def params,def session){

        def companyDealerInstance = session.userCompanyDealer
        def stokDel = StockOpnameLog.findByCompanyDealerAndStaStockOpnameAndStaDel(companyDealerInstance,'0','0')
        if(stokDel){
            def ubahStokDel = StockOpnameLog.get(stokDel.id)
            ubahStokDel.staDel='1'
            ubahStokDel.updatedBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
            ubahStokDel.lastUpdated =datatablesUtilService?.syncTime()
            ubahStokDel?.save(flush: true)
            ubahStokDel.errors.each {
                //println it
            }
        }

        def stokLog = new StockOpnameLog()
        stokLog?.companyDealer = companyDealerInstance
        stokLog?.staStockOpname = '1'
        stokLog?.staDel = '0'
        stokLog?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stokLog?.lastUpdProcess = 'INSERT'
        stokLog?.dateCreated = datatablesUtilService?.syncTime()
        stokLog?.lastUpdated = datatablesUtilService?.syncTime()
        stokLog?.save(flush: true)
        stokLog.errors.each {
            //println it
        }
        def hasil = []
        return hasil

    }

}