package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaManPower
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.Refund

import java.text.DateFormat
import java.text.SimpleDateFormat

class DeliveryFinanceService {
	boolean transactional = false

	def sessionFactory

	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it?.id,
				name: it?.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}	
	
	def datatablesDeliveryFinanceSettlementReport1(def params){
//        params.workshop
//        params.customer
//        params.tanggal
//        params.tanggal2
        String dateFilter = " AND TO_CHAR(T701_INVOICE.T701_TglJamInvoice, 'DD-MON-YYYY') BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')"
        String dealerFilter = " AND T704_SETTLEMENT.COMPANY_DEALER_ID = " + params.workshop
        String customerFilter =""
        if(params.customer){
            customerFilter = " AND T701_INVOICE.T701_T182_ID IN ("+ params.customer+")"
        }
		final session = sessionFactory.currentSession
		String query =
					" SELECT * FROM ("+
					" SELECT"+
					" TO_CHAR(T701_INVOICE.T701_TglJamInvoice, 'DD-MON-YYYY') tgljaminvoice,"+
					" T701_INVOICE.T701_NOINV,"+
					" CASE "+
					" 	 WHEN T701_INVOICE.T701_IntExt = 1 THEN 'EXT'"+
					" 	 WHEN T701_INVOICE.T701_IntExt = 0 THEN 'INT'"+
					" 	 ELSE ''"+
					" END TIPE,"+
					" T701_INVOICE.T701_Nopol nopol,"+
					" T182_HISTORYCUSTOMER.T182_ID customerid,"+
					" UPPER(T701_INVOICE.T701_Customer) as customername,"+
					" NVL(T701_INVOICE.T701_TotalInv, 0) totalinv,"+
					" NVL(T701_INVOICE.T701_BookingFee, 0) bookingfee,"+
					" NVL(T701_INVOICE.T701_TotalBayarRp, 0) totalbayar"+					
					" FROM "+
					" T701_INVOICE"+
					" INNER JOIN T704_SETTLEMENT ON T704_SETTLEMENT.T704_T701_NOINV = T701_INVOICE.ID"+
					" LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID = T701_INVOICE.T701_T182_ID" +
                    " WHERE 1 =1 "+ dateFilter + dealerFilter + customerFilter +
					" ) DAT "+
					" GROUP BY DAT.TGLJAMINVOICE, DAT.T701_NOINV, DAT.TIPE, DAT.NOPOL, DAT.CUSTOMERID, DAT.CUSTOMERNAME, DAT.TOTALINV, DAT.BOOKINGFEE, DAT.TOTALBAYAR "+
					" ORDER BY DAT.TGLJAMINVOICE";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			//println ("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryFinanceSettlementReport2(def params, def noInvoice){
        String dateFilter = " AND TO_CHAR(T701_INVOICE.T701_TGLJAMINVOICE, 'DD-MON-YYYY') BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')"
        String dealerFilter = " AND T704_SETTLEMENT.COMPANY_DEALER_ID = " + params.workshop
        String customerFilter =""
        if(params.customer){
            customerFilter = " AND T701_INVOICE.T701_T182_ID IN ("+ params.customer+")"
        }
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" M701_METODEBAYAR.M701_METODEBAYAR, "+
					" M702_BANK.M702_NAMABANK, "+
					" TO_CHAR(T704_SETTLEMENT.T704_TGLJAMSETTLEMENT, 'DD-MON-YYYY') TGLSETTLEMENT, "+
					" T704_SETTLEMENT.T704_JMLBAYAR  "+
					" FROM "+
					" T704_SETTLEMENT "+
					(noInvoice == 'NOP' ? " INNER JOIN T701_INVOICE ON T701_INVOICE.ID =  T704_SETTLEMENT.T704_T701_NOINV " : " INNER JOIN T701_INVOICE ON T701_INVOICE.ID =  T704_SETTLEMENT.T704_T701_NOINV AND TRIM(T701_INVOICE.T701_NOINV) = '"+noInvoice.trim()+"' ") +
					" LEFT JOIN M701_METODEBAYAR ON M701_METODEBAYAR.ID =  T704_SETTLEMENT.T704_M701_ID "+
					" LEFT JOIN M702_BANK ON M702_BANK.ID =  T704_SETTLEMENT.T704_M702_ID "+					
					" WHERE 1 =1 "+ dateFilter + dealerFilter + customerFilter+
					" ";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results	
	}
	
	def datatablesDeliveryFinanceSettlementReport3(def params, def noInvoice){
        def BFPart = 0, BFService = 0, BFOnRisk = 0
		final session = sessionFactory.currentSession
        def inv = InvoiceT701.findByT701NoInv(noInvoice.trim() as String)
        def reffund = Refund.findAllByT706NomorWOAndT706StaDel(inv?.reception?.t401NoWO as String,"0")

        reffund.each {
            if(it.t706JenisRefund=="0"){
                BFPart += it.t706JmlRefund
            }
            if(it.t706JenisRefund=="1"){
                BFService += it.t706JmlRefund
            }
            if(it.t706JenisRefund=="2"){
                BFOnRisk += it.t706JmlRefund
            }
        }
        def totalRefund = BFPart + BFService + BFOnRisk
		String query =
					" SELECT "+
					" NVL(TO_NUMBER(T701_INVOICE.T701_REFUNDRP), 0) REFUNDRP, "+
					" NVL(TO_NUMBER(T704_SETTLEMENT.T704_JmlPPh23), 0) pph23, "+
					" NVL(TO_NUMBER(T704_SETTLEMENT.T704_JmlPPN), 0) PPN, "+
					" NVL(T704_SETTLEMENT.T704_JMLBAYAR, 0) TOTAL_BAYAR "+
					" FROM T701_INVOICE "+
					" INNER JOIN T704_SETTLEMENT ON T704_SETTLEMENT.T704_T701_NOINV = T701_INVOICE.ID   "+
					" WHERE TRIM(T701_INVOICE.T701_NOINV) = '"+noInvoice.trim()+"' "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: BFPart,
					column_1: BFService,
					column_2: BFOnRisk,
					column_3: resultRow[3] - totalRefund
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesDeliveryFinanceOutstandingBookingFeeReport(def params){
		final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        String query = "SELECT  DAT.BOOKINGFEE, DAT.NOWO, DAT.NOPOL, DAT.KODE_CUSTOMER, DAT.NAMA," +
                "        SUM(DAT.AGING1_30)," +
                "        SUM(DAT.AGING31_60)," +
                "        SUM(DAT.AGING61_90)," +
                "        SUM(DAT.AGING91_120)," +
                "        SUM(DAT.AGING121_999)," +
                "        SUM(DAT.TOTALBF)" +
                "FROM ( " +
                "  SELECT  TO_CHAR(T721_BOOKINGFEE.T721_TglJamBookingFee, 'DD-MON-YYYY')  BOOKINGFEE ," +
                "          T401_RECEPTION.T401_NOWO NOWO," +
                "          T722_KWITANSI.T722_NOPOL NOPOL," +
                "          T182_HISTORYCUSTOMER.T182_ID KODE_CUSTOMER," +
                "          T722_KWITANSI.T722_NAMA NAMA," +
                "          CASE " +
                "            WHEN T403_PARTSRCP.T403_JANJITGLJAMBAYARDP IS NOT NULL AND" +
                "              extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) > 0 AND" +
                "              extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) <= 30" +
                "            THEN T721_BOOKINGFEE.T721_JMLHBAYAR" +
                "            ELSE 0  END AS AGING1_30," +
                "          CASE" +
                "            WHEN T403_PARTSRCP.T403_JANJITGLJAMBAYARDP IS NOT NULL AND" +
                "              extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) > 30 AND" +
                "              extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) <= 60" +
                "            THEN T721_BOOKINGFEE.T721_JMLHBAYAR  " +
                "            ELSE 0  END AS AGING31_60," +
                "          CASE" +
                "            WHEN T403_PARTSRCP.T403_JANJITGLJAMBAYARDP IS NOT NULL AND" +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) > 61 AND" +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) <= 90" +
                "          THEN T721_BOOKINGFEE.T721_JMLHBAYAR  ELSE 0  END AS AGING61_90," +
                "          CASE" +
                "            WHEN T403_PARTSRCP.T403_JANJITGLJAMBAYARDP IS NOT NULL AND" +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) > 91 AND" +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) <= 120" +
                "          THEN T721_BOOKINGFEE.T721_JMLHBAYAR  ELSE 0  END AS AGING91_120,  " +
                "          CASE " +
                "          WHEN T403_PARTSRCP.T403_JANJITGLJAMBAYARDP IS NOT NULL AND " +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) > 121 AND" +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) <= 999" +
                "          THEN T721_BOOKINGFEE.T721_JMLHBAYAR  ELSE 0  END AS AGING121_999,  " +
                "          CASE " +
                "          WHEN T403_PARTSRCP.T403_JANJITGLJAMBAYARDP IS NOT NULL AND " +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) > 0 AND" +
                "            extract(DAY FROM SYSDATE - T403_PARTSRCP.T403_JANJITGLJAMBAYARDP) <= 1000" +
                "          THEN T721_BOOKINGFEE.T721_JMLHBAYAR  ELSE 0  END AS TOTALBF  " +
                "  FROM T721_BOOKINGFEE  " +
                "    INNER JOIN T722_KWITANSI ON T722_KWITANSI.ID =  T721_BOOKINGFEE.T721_T722_NOKWITANSI" +
                "    LEFT JOIN T401_RECEPTION ON T401_RECEPTION.ID =  T722_KWITANSI.T722_T401_NOWO" +
                "    LEFT JOIN T403_PARTSRCP ON T403_PARTSRCP.T403_T401_NOWO = T401_RECEPTION.ID" +
                "    LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID = T722_KWITANSI.T722_T182_ID  WHERE T721_BOOKINGFEE.T721_STADEL='0' AND" +
                "    T721_BOOKINGFEE.COMPANY_DEALER_ID='"+params.workshop+"' AND" +
                "    T721_BOOKINGFEE.T721_TGLJAMBOOKINGFEE BETWEEN TO_DATE('"+params.tanggal+"','dd-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','dd-MM-YYYY') AND T182_HISTORYCUSTOMER.ID='"+params.customer+"' AND T722_KWITANSI.T722_STABOOKINGONRISK='0')" +
                "    DAT  GROUP BY DAT.BOOKINGFEE, DAT.NOWO, DAT.NOPOL, DAT.KODE_CUSTOMER, DAT.NAMA ORDER BY DAT.BOOKINGFEE DESC  ";
		def results = null
        try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10]
				]
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryFinanceSalesReport(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+ 
					" DAT.TGLINVOICE, DAT.NOINVOICE, DAT.TIPE_INVOICE, DAT.TIPE_SERVICE, DAT.NOPOL, DAT.KODE_CUSTOMER, DAT.NAMA_CUSTOMER, "+
					" SUM(DAT.LABOUR_HARGA), SUM(DAT.LABOUR_DISCOUNT), DAT.LABOUR_SUBTOTAL, SUM(DAT.PARTS_HARGA), SUM(PARTS_DISCOUNT), DAT.PARTS_SUBTOTAL, "+
					" SUM(DAT.OIL_HARGA), SUM(DAT.OIL_DISCOUNT), DAT.OIL_SUBTOTAL, SUM(DAT.SUBLET_HARGA), SUM(DAT.SUBLET_DISCOUNT), DAT.SUBLET_SUBTOTAL, "+
					" SUM(DAT.MATERIAL_HARGA), SUM(DAT.MATERIAL_DISCOUNT), DAT.MATERIAL_SUBTOTAL, DAT.TOTAL_TURN_OVER, DAT.VAT, DAT.MATERAI, DAT.GRAND_TOTAL_INVOICE, DAT.STATUS_PEMBAYARAN "+
					" FROM ( "+
					" SELECT "+
					" TO_CHAR(T701_INVOICE.T701_TGLJAMINVOICE, 'DD-MON-YYYY') TGLINVOICE, "+
					" T701_INVOICE.T701_NoInv NOINVOICE, "+
					" CASE "+
					" 	WHEN T701_INVOICE.T701_IntExt = 1 THEN 'EXT' "+
					" 	WHEN T701_INVOICE.T701_IntExt = 0 THEN 'INT' "+
					" 	ELSE '' "+
					" END AS TIPE_INVOICE, "+
					" CASE "+
					" 	WHEN T701_INVOICE.T701_TIPE = 0 THEN 'GR' "+
					" 	WHEN T701_INVOICE.T701_TIPE = 1 THEN 'BP' "+
					" 	ELSE '' "+
					" END AS TIPE_SERVICE, "+
					" T701_INVOICE.T701_Nopol NOPOL, "+
					" T182_HISTORYCUSTOMER.T182_ID AS KODE_CUSTOMER, "+
					" T701_INVOICE.T701_Customer NAMA_CUSTOMER, "+
					" NVL((SELECT SUM(T702_JOBINV.T702_HargaRp) FROM T702_JOBINV WHERE T702_JOBINV.T702_T701_NOINV = T701_INVOICE.ID ), 0) LABOUR_HARGA, "+
					" NVL((SELECT SUM(T702_JOBINV.T702_DiscRp) FROM T702_JOBINV WHERE T702_JOBINV.T702_T701_NOINV = T701_INVOICE.ID ),  0) LABOUR_DISCOUNT, "+
					" NVL(T701_INVOICE.T701_JasaRp, 0) LABOUR_SUBTOTAL, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_HargaRp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%PART%'  WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) PARTS_HARGA, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_Discrp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%PART%' WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) PARTS_DISCOUNT, "+
					" NVL(T701_INVOICE.T701_PartsRp, 0) PARTS_SUBTOTAL, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_HargaRp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%OLI%'  WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) OIL_HARGA, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_Discrp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%OLI%' WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) OIL_DISCOUNT, "+
					" NVL(T701_INVOICE.T701_OliRp, 0) OIL_SUBTOTAL, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_HargaRp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%SUBLET%' WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) SUBLET_HARGA, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_Discrp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%SUBLET%' WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) SUBLET_DISCOUNT, "+
					" NVL(T701_INVOICE.T701_SubletRp, 0) AS SUBLET_SUBTOTAL, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_HargaRp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%MATERIAL%'  WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) MATERIAL_HARGA, "+
					" NVL((SELECT SUM(T703_PARTINV.T703_Discrp) FROM T703_PARTINV INNER JOIN M111_GOODS ON M111_GOODS.ID =  T703_PARTINV.T703_M111_ID INNER JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID INNER JOIN M117_FRANC ON  M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID AND UPPER(M117_FRANC.M117_NAMAFRANC) LIKE '%MATERIAL%' WHERE T703_PARTINV.T703_T701_NOINV = T701_INVOICE.ID ), 0) MATERIAL_DISCOUNT, "+
					" NVL(T701_INVOICE.T701_MaterialRp, 0) MATERIAL_SUBTOTAL, "+
					" NVL(T701_INVOICE.T701_SubTotalRp, 0) TOTAL_TURN_OVER, "+
					" NVL(T701_INVOICE.T701_PPnRp, 0) VAT, "+
					" NVL(T701_INVOICE.T701_MateraiRp, 0) MATERAI, "+
					" NVL(T701_INVOICE.T701_TotalInv, 0) GRAND_TOTAL_INVOICE, "+
					" CASE WHEN T701_INVOICE.T701_StaSettlement = '0' THEN 'LUNAS' "+
					"	   WHEN T701_INVOICE.T701_StaSettlement = '1' THEN 'AR' "+
					"	   WHEN T701_INVOICE.T701_StaSettlement = '2' THEN 'AP' "+
					"	   ELSE '' "+
					" END STATUS_PEMBAYARAN"+
					" FROM "+
					" T701_INVOICE "+
					" LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID =  T701_INVOICE.T701_T182_ID "+
					" WHERE TO_CHAR(T701_INVOICE.T701_TGLJAMINVOICE, 'DD-MM-YYYY') >= '"+params.tanggal+"' AND TO_CHAR(T701_INVOICE.T701_TGLJAMINVOICE, 'DD-MM-YYYY') <= '"+params.tanggal2+"'"+
					" ) DAT "+ 
					" GROUP BY "+
					" DAT.TGLINVOICE, DAT.NOINVOICE, DAT.TIPE_INVOICE, DAT.TIPE_SERVICE, DAT.NOPOL, DAT.KODE_CUSTOMER, DAT.NAMA_CUSTOMER, "+
					" DAT.LABOUR_SUBTOTAL, DAT.PARTS_SUBTOTAL, "+
					" DAT.OIL_SUBTOTAL, DAT.SUBLET_SUBTOTAL, "+
					" DAT.MATERIAL_SUBTOTAL, DAT.TOTAL_TURN_OVER, DAT.VAT, DAT.MATERAI, DAT.GRAND_TOTAL_INVOICE, DAT.STATUS_PEMBAYARAN "+
					" ORDER BY DAT.TGLINVOICE ASC "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],					
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25],
					column_26: resultRow[26]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryFinanceDailyCashierReport(def params){
		final session = sessionFactory.currentSession
		String query =
					"SELECT " +
                            "  SUM(DAT.BALANCE) BALANCE," +
                            "  DAT.DATEE, DAT.NOREFF, DAT.T701_Nopol, DAT.customerid, DAT.customername," +
                            "  SUM(DAT.CASH_IN) CASH_IN," +
                            "  SUM(DAT.CASH_OUT) CASH_OUT," +
                            "  SUM(DAT.debit) DEBIT, '' NAMABANK," +
                            "  SUM(DAT.CREDIT_AMOUNT) CREDIT_AMOUNT," +
                            "  SUM(DAT.CHARGE) CHARGE," +
                            "  SUM(DAT.TRANSFER) TRANSFER " +
                            "FROM ( " +
                            "        SELECT  0 BALANCE," +
                            "        TO_CHAR(T704_SETTLEMENT.T704_TglJamSettlement, 'DD-MON-YYYY' ) DATEE," +
                            "        T701_INVOICE.T701_NOINV NOREFF," +
                            "        T701_INVOICE.T701_Nopol," +
                            "        T182_HISTORYCUSTOMER.T182_ID customerid," +
                            "        T701_INVOICE.T701_Customer as customername," +
                            "        CASE" +
                            "          WHEN UPPER(M701_METODEBAYAR.M701_METODEBAYAR) LIKE '%CASH%' " +
                            "          THEN NVL(T704_SETTLEMENT.T704_JmlBayar, 0)" +
                            "        ELSE 0" +
                            "        END AS CASH_IN,  0 AS CASH_OUT," +
                            "        CASE" +
                            "          WHEN UPPER(M701_METODEBAYAR.M701_METODEBAYAR) LIKE '%DEBIT%' THEN NVL(T704_SETTLEMENT.T704_JmlBayar, 0)" +
                            "        ELSE 0 " +
                            "        END AS debit,  M702_BANK.M702_NAMABANK," +
                            "        CASE" +
                            "          WHEN UPPER(M701_METODEBAYAR.M701_METODEBAYAR) LIKE '%KREDIT%' THEN NVL(T704_SETTLEMENT.T704_JmlBayar, 0)" +
                            "        ELSE 0" +
                            "        END AS CREDIT_AMOUNT,   NVL(M703_RATEBANK.M703_JMLMIN, 0) CHARGE," +
                            "        CASE" +
                            "          WHEN LOWER(M701_METODEBAYAR.M701_METODEBAYAR) LIKE '%TRANSFER%' THEN NVL(T704_SETTLEMENT.T704_JmlBayar, 0) " +
                            "        ELSE 0" +
                            "        END AS transfer" +
                            "  FROM  T701_INVOICE" +
                            "    INNER JOIN T704_SETTLEMENT ON T704_SETTLEMENT.T704_T701_NOINV =  T701_INVOICE.ID" +
                            "    LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID = T701_INVOICE.T701_T182_ID" +
                            "    LEFT JOIN M701_METODEBAYAR ON M701_METODEBAYAR.ID =  T704_SETTLEMENT.T704_M701_ID" +
                            "    LEFT JOIN M702_BANK ON M702_BANK.ID =  T704_SETTLEMENT.T704_M702_ID  " +
                            "    LEFT JOIN M703_RATEBANK ON M703_RATEBANK.ID =  T704_SETTLEMENT.T704_M703_BANKCHARGE WHERE T701_INVOICE.COMPANY_DEALER_ID='"+params.workshop+"' AND T704_SETTLEMENT.COMPANY_DEALER_ID='"+params.workshop+"'" +
                            "    AND T704_SETTLEMENT.T704_TGLJAMSETTLEMENT BETWEEN TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','dd-MM-yyyy') AND T182_HISTORYCUSTOMER.ID='"+params.customer+"' " +
                            "    )" +
                            "    DAT  GROUP BY DAT.DATEE, DAT.NOREFF, DAT.T701_Nopol, DAT.customerid, DAT.customername ORDER BY DAT.DATEE ASC";
		def results = null
        try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryFinanceRefundReport(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" TO_CHAR(T706_REFUND.T706_TgJamlRefund, 'DD-MON-YYYY') REFUND_DATE, "+
					" T706_REFUND.T706_NoRefund NO_REFUND,  "+
					" CASE"+
					" 	WHEN T706_REFUND.T706_JenisRefund = '0' THEN 'BOOKING FEE PART'"+
					" 	WHEN T706_REFUND.T706_JenisRefund = '1' THEN 'BOOKING FEE SERVICE'"+
					" 	WHEN T706_REFUND.T706_JenisRefund = '2' THEN 'ON RISK'"+

					" 	ELSE ''"+
					" END AS JENIS_REFUND, "+
					" T706_REFUND.T706_NomorWO NOMOR_WO, "+
					" T706_REFUND.T706_NomorInvoice NOMOR_INVOICE, "+
					" T706_REFUND.T706_Nopol NOPOL, "+
					" T182_HISTORYCUSTOMER.T182_ID KODE_CUSTOMER, "+
					" T706_REFUND.T706_NAMACUSTOMER NAMA_CUSTOMER, "+
					" T706_REFUND.T706_JMLREFUND AMOUNT, "+
					" T706_REFUND.T706_ALASANREFUNDLAINNYA ALASAN_REFUND "+
					" FROM "+
					" T706_REFUND "+
					" LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID = T706_REFUND.T706_T182_ID  "+
                    " WHERE T706_REFUND.T706_TgJamlRefund BETWEEN TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')   "+
                    " AND T706_REFUND.COMPANY_DEALER_ID='"+params.workshop+"' "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			//println("Result size 0");
		}		
		return results	
	}	
	
	def datatablesDeliveryFinanceAgingAr(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" TO_CHAR(T701_INVOICE.T701_TglJamInvoice, 'DD-MON-YYYY') TGLJAMINVOICE, "+
					" T701_INVOICE.T701_NOINV NO_INVOICE, "+
					" T701_INVOICE.T701_IntExt TIPE_INVOICE, "+
					" T701_INVOICE.T701_Nopol NOPOL, "+
					" T182_HISTORYCUSTOMER.T182_ID KODE_CUSTOMER, "+
					" T701_INVOICE.T701_Customer, "+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) > 0 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 30  "+
					" 	 THEN T701_INVOICE.T701_BOOKINGFEE  "+
					" 	 ELSE 0  "+
					" END AS AGING1_30, "+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) > 30 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 60  "+
					" 	 THEN T701_INVOICE.T701_BOOKINGFEE "+
					" 	 ELSE 0 "+
					" END AS AGING31_60, "+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) > 60 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 90  "+
					" 	 THEN T701_INVOICE.T701_BOOKINGFEE  "+
					" 	 ELSE 0  "+
					" END AS AGING61_90, "+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) > 90 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 120  "+
					" 	 THEN T701_INVOICE.T701_BOOKINGFEE  "+
					" 	 ELSE 0  "+
					" END AS AGING91_120, "+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) > 120 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 999  "+
					" 	 THEN T701_INVOICE.T701_BOOKINGFEE  "+
					" 	 ELSE 0  "+
					" END AS AGING121_999  "+
					" FROM "+
					" T701_INVOICE "+
					" LEFT JOIN T704_SETTLEMENT ON T704_SETTLEMENT.T704_T701_NOINV =  T701_INVOICE.ID "+
					" LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID =  T701_INVOICE.T701_T182_ID "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],					
					column_10: resultRow[10]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryFinanceTandaTerimaInvoice(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" TO_CHAR(T701_INVOICE.T701_TglJamInvoice, 'DD-MON-YYYY') TGLINVOICE, "+
					" T701_INVOICE.T701_NOPOL, "+
					" T701_INVOICE.T701_NOINV, "+
					" T701_INVOICE.T701_TOTALINV "+
					" FROM "+
					" T701_INVOICE "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryFinanceBookingFee1(def params){
		final session = sessionFactory.currentSession
		String query =		
					" SELECT "+
					" DAT.ID, DAT.INVOICE_ID, DAT.T401_NOWO, DAT.T722_Nopol, DAT.T182_ID, DAT.T722_NAMA, NVL(SUM(DAT.BOOKING_FEE), 0) BOOKING_FEE,  "+
					" DAT.T701_NOINV, NVL(SUM(DAT.TOTAL_INV), 0) TOTAL_INV, DAT.TGLSETTLEMENT, NVL(SUM(DAT.JMLBAYAR), 0) JMLBAYAR, NVL(SUM(DAT.JML_REFUND), 0) JML_REFUND,"+
					" DAT.KWITANSIID, DAT.HISTORYCUSTOMERID"+
					" FROM ("+
					" SELECT "+
					" DAT.ID, DAT.INVOICE_ID, DAT.T401_NOWO,  T722_KWITANSI.T722_Nopol, T182_HISTORYCUSTOMER.T182_ID, T722_KWITANSI.T722_NAMA, NVL(T701_INVOICE.T701_BOOKINGFEE, 0) BOOKING_FEE,"+
					" T701_INVOICE.T701_NoInv, NVL(T701_INVOICE.T701_TotalInv, 0) TOTAL_INV, TO_CHAR(T704_SETTLEMENT.T704_TglJamSettlement, 'DD-MON-YYYY') TGLSETTLEMENT, NVL(T704_SETTLEMENT.T704_JMLBAYAR, 0) JMLBAYAR,  NVL(T706_REFUND.T706_JMLREFUND, 0) JML_REFUND, T722_KWITANSI.ID KWITANSIID, T182_HISTORYCUSTOMER.ID HISTORYCUSTOMERID "+
					" FROM ( "+
					" SELECT  "+
					" T401_RECEPTION.*,  "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID  "+
					" FROM   "+
					" T401_RECEPTION "+
					" ) DAT "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID"+
					" INNER JOIN T722_KWITANSI ON T722_KWITANSI.T722_T401_NOWO = DAT.ID" +
                    " LEFT JOIN T721_BOOKINGFEE ON T721_BOOKINGFEE.T721_T722_NOKWITANSI = T722_KWITANSI.ID"+
					" LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID = T722_KWITANSI.T722_T182_ID"+
					" LEFT JOIN T704_SETTLEMENT ON T704_SETTLEMENT.T704_T701_NOINV = DAT.INVOICE_ID"+
					" LEFT JOIN T706_REFUND ON T706_REFUND.T706_T182_ID = T182_HISTORYCUSTOMER.ID"+
                    " WHERE TO_CHAR(T721_BOOKINGFEE.T721_TGLJAMBOOKINGFEE, 'DD-MON-YYYY') BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')" +
                    " AND DAT.COMPANY_DEALER_ID = "+ params.workshop +
					" ) DAT "+
					" GROUP BY DAT.ID, DAT.INVOICE_ID, DAT.T401_NOWO, DAT.T722_Nopol, DAT.T182_ID, DAT.T722_NAMA, DAT.T701_NOINV, DAT.TGLSETTLEMENT, DAT.KWITANSIID, DAT.HISTORYCUSTOMERID"+
					" ORDER BY DAT.T701_NOINV ASC"+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryFinanceBookingFee2(def params, def kwitansiId){
		final session = sessionFactory.currentSession
		String query =		
					" SELECT "+
					" TO_CHAR(T721_BOOKINGFEE.T721_TglJamBookingFee, 'DD-MON-YYYY') TGLBOOKINGFEE, "+ 
					" T722_KWITANSI.T722_NOKWITANSI, "+
					" T721_BOOKINGFEE.T721_xNamaUser, "+
					" M701_METODEBAYAR.M701_METODEBAYAR, "+
					" M702_BANK.M702_NAMABANK, "+
					" T721_BOOKINGFEE.T721_NoBuktiTransfer, "+
					" M703_RATEBANK.M703_RATERP, "+
					" T721_BOOKINGFEE.T721_JMLHBAYAR "+
					" FROM "+
					" T721_BOOKINGFEE "+
					" INNER JOIN T722_KWITANSI ON T722_KWITANSI.ID = T721_BOOKINGFEE.T721_T722_NOKWITANSI AND T722_KWITANSI.ID = '"+kwitansiId+"' "+
					" LEFT JOIN M701_METODEBAYAR ON M701_METODEBAYAR.ID = T721_BOOKINGFEE.T721_M701_ID "+
					" LEFT JOIN M702_BANK ON M702_BANK.ID = T721_BOOKINGFEE.T721_M702_ID "+
					" LEFT JOIN M703_RATEBANK ON M703_RATEBANK.ID = T721_BOOKINGFEE.T721_M703_BANKCHARGE "+
					"";
		def results = null

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results	
	}
	
	def datatablesDeliveryFinanceBookingFee3(def params, def historyCustomerId){
		final session = sessionFactory.currentSession
		String query =		
					" SELECT "+
					" TO_CHAR(T706_refund.T706_TgJamlRefund, 'DD-MON-YYYY') TGLREFUND, "+
					" T706_REFUND.T706_NOREFUND, "+
					" T706_REFUND.T706_NomorKwitansi ||' / '|| T706_REFUND.T706_NomorInvoice NOKWITANSI, "+
					" T706_REFUND.T706_xNamaUser, "+
					" '' METODE_BAYAR, "+
					" M702_BANK.M702_NAMABANK, "+
					" T706_refund.T706_NoAccount, "+
					" NVL(T706_refund.T706_JmlRefund, 0) JMLREFUND "+
					" FROM "+
					" T706_refund "+
					" LEFT JOIN M702_BANK ON M702_BANK.ID =  T706_REFUND.T706_M702_ID "+
					" WHERE T706_REFUND.T706_T182_ID = '"+historyCustomerId+"' "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results	
	}
	
	def dataTablesNamaBank(def invoiceId){
		final session = sessionFactory.currentSession
		String query = 					
					" SELECT "+
					" M702_BANK.M702_NAMABANK NAMABANK, '' dummy "+
					" FROM"+
					" T701_INVOICE "+
					" INNER JOIN T704_SETTLEMENT ON T704_SETTLEMENT.T704_T701_NOINV =  T701_INVOICE.ID "+
					" INNER JOIN M702_BANK ON M702_BANK.ID =  T704_SETTLEMENT.T704_M702_ID "+
					" WHERE TRIM(T701_INVOICE.T701_NOINV) =  '"+invoiceId.trim()+"'   "+
					"";
        try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			final List<String> results = queryResults.collect { it[0] }
			if(!results.isEmpty()){
				return null != results.get(0) ? String.valueOf(results.get(0)) : "";
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;
	}

    def datatablesCustomerList(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = HistoryCustomer.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params.kodeCustomer){
                ilike("t182ID","%" + (params.kodeCustomer as String ) + "%")
            }
            if(params.namaCustomer){
                ilike("fullNama","%" + (params.namaCustomer as String ) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kode: it.t182ID,

                    nama: it.fullNama.toUpperCase()

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret
    }
}
 
