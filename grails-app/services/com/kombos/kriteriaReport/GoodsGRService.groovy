package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer

class GoodsGRService {
	boolean transactional = false

	def sessionFactory
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}
	
	def getJenisPekerjaanById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE ID IN ("+id+") ";
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaTipeKerusakan: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaTipeKerusakan")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}

	def datatablesSAList(def params){

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = NamaManPower.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			if (params."sa") {
				ilike("t015NamaBoard", "%" + (params."sa" as String) + "%")
			}

			userProfile{
				roles{
					ilike("name","%" + 'SA' + "%")
				}
			}

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				namaSa: it.t015NamaBoard+' - '+it?.userProfile?.t001Inisial

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}

	def datatablesJenisPayment(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = JobRCP.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if (params."payment") {
				ilike("t402StaCashInsurance", "%" + (params."payment" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPayment: it.t402StaCashInsurance == '1' ? 'Cash' : 'Insurance'

			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret
	}

	def datatablesJenisPekerjaan(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		
		String query = "SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE M055_KATEGORIJOB IN('GR','PDS','SBE','SBI','TWC')";
			
		final sqlQueryCount = session.createSQLQuery(query)
		final queryResultsCount = sqlQueryCount.with {			
			list()		
		}
		
		query = " SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE M055_KATEGORIJOB IN('GR','PDS','SBE','SBI','TWC')";
		if (params."pekerjaan") {
			query += " AND LOWER(M055_KATEGORIJOB) LIKE '%" + (params."pekerjaan" as String) + "%'  ";
		}		
		
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaPekerjaan: resultRow[1]
			]
			 
		}		
		ret = [sEcho: params.sEcho, iTotalRecords: queryResultsCount.size, iTotalDisplayRecords: queryResultsCount.size, aaData: results]
		return ret
	}	
	
	def datatablesGRGoodsWeekly (def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+ 
					" DAT.TGLMOS, DAT.NAMA, SUM(DAT.STOCK) AS STOCK, DAT.SATUAN, SUM(DAT.WEEK1) AS WEEK1, SUM(DAT.WEEK2) AS WEEK2, SUM(DAT.WEEK3) AS WEEK3, SUM(DAT.WEEK4) AS WEEK4, SUM(DAT.WEEK5) AS WEEK5  "+
					" 	FROM ( "+
					" 	SELECT "+
					" 	TO_CHAR(DAT.TGLMOS, 'MON-YYYY') AS TGLMOS, "+
					" 	DAT.NAMA, "+
					" 	DAT.STOCK, "+
					" 	DAT.SATUAN, "+
					" 	CASE "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'W') = '1' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS WEEK1, "+
					" 	CASE "+ 
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'W') = '2' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS WEEK2, "+
					" 	CASE "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'W') = '3' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS WEEK3, "+
					" 	CASE "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'W') = '4' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS WEEK4, "+
					" 	CASE "+ 
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'W') = '5' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS WEEK5 "+ 
					" 	FROM ( "+
					" 	SELECT M111_GOODS.M111_NAMA NAMA, NVL(SUM(T131_PARTSSTOK.T131_QTY1), 0) STOCK,  M118_SATUAN.M118_SATUAN1 SATUAN, "+
					" 	T417_MOS.T417_TGLMOS TGLMOS, NVL(SUM(T417_MOS.T417_QTY1), 0) QTY "+
					" 	FROM M111_GOODS "+
					" 	INNER JOIN T131_PARTSSTOK ON T131_PARTSSTOK.T131_M111_ID =  M111_GOODS.ID "+
					" 	INNER JOIN M118_SATUAN ON M118_SATUAN.ID =  M111_GOODS.M111_M118_ID "+
					" 	LEFT JOIN T417_MOS ON T417_MOS.T417_M111_ID =  M111_GOODS.ID "+
					" 	GROUP BY M111_GOODS.M111_NAMA, M118_SATUAN.M118_SATUAN1, T417_MOS.T417_TGLMOS, T417_MOS.T417_QTY1 "+
					" 	) DAT " +
					//" WHERE TO_CHAR(DAT.TGLMOS, 'DD-MM-YYYY') >= '"+params.tanggal+"' AND TO_CHAR(DAT.TGLMOS, 'DD-MM-YYYY') <= '"+params.tanggal2+"' "+
					" ) DAT "+
					" GROUP BY DAT.TGLMOS, DAT.NAMA, DAT.SATUAN "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesGRGoodsMonthly (def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" DAT.TGLMOS, DAT.NAMA, SUM(DAT.STOCK)  STOCK, DAT.SATUAN, SUM(DAT.JANUARI) JANUARI, "+
					" SUM(DAT.FEBRUARI) FEBRUARI, SUM(DAT.MARET) MARET, SUM(DAT.APRIL) APRIL, SUM(DAT.MEI) MEI, "+
					" SUM(DAT.JUNI) JUNI, SUM(DAT.JULI) JULI, SUM(DAT.AGUSTUS) AGUSTUS, SUM(DAT.SEPTEMBER) SEPTEMBER, "+
					" SUM(DAT.OKTOBER) OKTOBER, SUM(DAT.NOVEMBER) NOVEMBER, SUM(DAT.DESEMBER) DESEMBER "+
					" FROM ( "+
					" 	SELECT "+
					" 	TO_CHAR(DAT.TGLMOS, 'YYYY') AS TGLMOS, "+
					" 	DAT.NAMA, "+
					" 	DAT.STOCK, "+
					" 	DAT.SATUAN, "+
					" 	CASE "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '01' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS JANUARI, "+
					" 	CASE "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '02' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS FEBRUARI, "+
					" 	CASE "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '03' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS MARET, "+ 
					" 	CASE "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '04' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS APRIL, "+ 
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '05' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS MEI, "+
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '06' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS JUNI, "+
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '07' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS JULI, "+
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '08' THEN DAT.QTY  "+
					" 		ELSE 0 "+
					" 	END AS AGUSTUS, "+
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '09' THEN DAT.QTY  "+
					" 		ELSE 0 "+
					" 	END AS SEPTEMBER, "+
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '10' THEN DAT.QTY "+
					" 		ELSE 0 "+
					" 	END AS OKTOBER, "+
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '11' THEN DAT.QTY "+ 
					" 		ELSE 0 "+
					" 	END AS NOVEMBER, "+
					" 	CASE  "+
					" 		WHEN TO_CHAR(DAT.TGLMOS, 'MM') = '12' THEN DAT.QTY  "+
					" 		ELSE 0 "+
					" 	END AS DESEMBER "+
					" 	FROM ( "+
					" 	SELECT M111_GOODS.M111_NAMA NAMA, NVL(SUM(T131_PARTSSTOK.T131_QTY1), 0) STOCK,  M118_SATUAN.M118_SATUAN1 SATUAN, "+
					" 	T417_MOS.T417_TGLMOS TGLMOS, NVL(SUM(T417_MOS.T417_QTY1), 0) QTY "+
					" 	FROM M111_GOODS "+
					" 	INNER JOIN T131_PARTSSTOK ON T131_PARTSSTOK.T131_M111_ID =  M111_GOODS.ID "+
					" 	INNER JOIN M118_SATUAN ON M118_SATUAN.ID =  M111_GOODS.M111_M118_ID "+
					" 	LEFT JOIN T417_MOS ON T417_MOS.T417_M111_ID =  M111_GOODS.ID "+
					" 	GROUP BY M111_GOODS.M111_NAMA, M118_SATUAN.M118_SATUAN1, T417_MOS.T417_TGLMOS, T417_MOS.T417_QTY1 "+
					" 	) DAT"+
					" ) DAT "+
					" GROUP BY DAT.TGLMOS, DAT.NAMA, DAT.SATUAN "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
}
 
