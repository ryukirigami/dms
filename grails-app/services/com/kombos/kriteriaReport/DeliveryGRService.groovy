package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KategoriJob
import com.kombos.administrasi.NamaManPower
import com.kombos.woinformation.JobRCP

class DeliveryGRService {
	boolean transactional = false

	def sessionFactory

	def datatablesSAList(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NamaManPower.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                companyDealer{
                    eq("id",params.workshop as Long)
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if (params."sa") {
                ilike("t015NamaBoard", "%" + (params."sa" as String) + "%")
            }

            manPowerDetail{
                ilike("m015LevelManPower","%Service Advisor%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t015NamaLengkap: it.t015NamaLengkap.toUpperCase()

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret
	}

	def datatablesJenisPayment(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0


		def c = JobRCP.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if (params."payment") {
				ilike("t402StaCashInsurance", "%" + (params."payment" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPayment: it.t402StaCashInsurance == '1' ? 'Cash' : 'Insurance'

			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret
	}

	def datatablesJenisPekerjaan(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = KategoriJob.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            inList("m055KategoriJob",["GR","SBI","SBE","PDS","TWC"])
            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m055KategoriJob: it.m055KategoriJob

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret
	}
	
	def getServiceAdvisorById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, T015_NAMALENGKAP FROM T015_NAMAMANPOWER WHERE ID IN ("+id+") ";		
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaSa: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {

			ret += String.valueOf(result.get("namaSa")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}

    def getServiceAdvisorUser(def id){
        String ret = "";
        final session = sessionFactory.currentSession
        String query = "SELECT DOM_USER.USERNAME FROM DOM_USER  LEFT JOIN T015_NAMAMANPOWER ON T015_NAMAMANPOWER.T015_NAMALENGKAP = DOM_USER.FULLNAME WHERE T015_NAMAMANPOWER.ID IN ("+id+") ";
        final sqlQuery = session.createSQLQuery(query)

        final queryResults = sqlQuery.with {
            list()
        }

        final results = queryResults.collect { resultRow ->
            [namaSa: resultRow
            ]
        }

        for(Map<String, Object> result : results) {

            ret += "'" + String.valueOf(result.get("namaSa")) + "',";
        }

        if(ret != ""){
            ret = ret.substring(0, ret.length()-1);
        }

        return ret;
    }
	
	def getJenisPekerjaanById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE ID IN ("+id+") ";
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaTipeKerusakan: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaTipeKerusakan")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}
	
	def datatablesDeliveryGrDeliveryLeadTimeSummary(def params, def type){
		String strType = ""
		String dateFilter = "";
		String jenisPekerjaanFilter = "";
		String waitingStatusFilter = "";

		if(params.jenisPekerjaan == ""){
			jenisPekerjaanFilter = " AND 1=1 ";
		} else {
			jenisPekerjaanFilter = " AND DAT.KATEGORI_JOB_ID IN ("+params.jenisPekerjaan+")  ";
		}
		
		if(params.waitingStatus == "" || params.waitingStatus == "yes,no"){
			waitingStatusFilter = " AND 1=1 ";
		} else 
		if(params.waitingStatus == 'yes'){
			waitingStatusFilter = " AND DAT.T401_STACUSTOMERTUNGGU = '1' ";
		} else
		if(params.waitingStatus == 'no'){
			waitingStatusFilter = " AND DAT.T401_STACUSTOMERTUNGGU = '0' ";
		}

		if(type == 0) {
			strType = " '' AS YEAR, '' AS MONTH, ";			
			dateFilter = " AND DAT.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')";

		} else
		if(type == 1) {
			strType = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";			
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			strType = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";			
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
	
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" DAT.YEAR, DAT.MONTH, "+
					" DAT.KATEGORI_JOB, MAX(WF_JOC) AS MAX_WF_JOC, MIN(WF_JOC) AS MIN_WF_JOC, CEIL(AVG(WF_JOC)) AS AVG_WF_JOC, "+
					" MAX(LT_JOC) AS MAX_LT_JOC, MIN(LT_JOC)  AS MIN_LT_JOC, CEIL(AVG(LT_JOC)) AS AVG_LT_JOC, "+
					" MAX(WF_INVOICING) AS MAX_WF_INVOICING, MIN(WF_INVOICING)  AS MIN_WF_INVOICING, CEIL(AVG(WF_INVOICING)) AS AVG_WF_INVOICING, "+
					" MAX(LT_INVOICING) AS MAX_LT_INVOICING, MIN(LT_INVOICING)  AS MIN_LT_INVOICING, CEIL(AVG(LT_INVOICING)) AS AVG_LT_INVOICING, "+
					" MAX(WF_WASHING) AS MAX_WF_WASHING, MIN(WF_WASHING)  AS MIN_WF_WASHING, CEIL(AVG(WF_WASHING)) AS AVG_WF_WASHING, "+
					" MAX(LT_WASHING) AS MAX_LT_WASHING, MIN(LT_WASHING)  AS MIN_LT_WASHING, CEIL(AVG(LT_WASHING)) AS AVG_LT_WASHING, "+
					" MAX(WF_NOTIFICATION) AS MAX_WF_NOTIFICATION, MIN(WF_NOTIFICATION)  AS MIN_WF_NOTIFICATION, CEIL(AVG(WF_NOTIFICATION)) AS AVG_WF_NOTIFICATION, "+
					" MAX(LT_PREDELIVERY) AS MAX_LT_PREDELIVERY, MIN(LT_PREDELIVERY)  AS MIN_LT_PREDELIVERY, CEIL(AVG(LT_PREDELIVERY)) AS AVG_LT_PREDELIVERY, "+
					" MAX(WF_SETTLEMENT) AS MAX_WF_SETTLEMENT, MIN(WF_SETTLEMENT)  AS MIN_WF_SETTLEMENT, CEIL(AVG(WF_SETTLEMENT)) AS AVG_WF_SETTLEMENT, "+
					" MAX(WF_DELIVERY) AS MAX_WF_DELIVERY, MIN(WF_DELIVERY)  AS MIN_WF_DELIVERY, CEIL(AVG(WF_DELIVERY)) AS AVG_WF_DELIVERY, "+
					" MAX(LT_DELIVERY) AS MAX_LT_DELIVERY, MIN(LT_DELIVERY)  AS MIN_LT_DELIVERY, CEIL(AVG(LT_DELIVERY)) AS AVG_LT_DELIVERY "+ 
					" FROM ( "+
					" SELECT "+ 
					" "+strType+" "+
					" DAT.KATEGORI_JOB_ID, "+
					" DAT.KATEGORI_JOB, "+
					" NVL(CEIL((extract(DAY FROM WF_JOC_FINISH - WF_JOC_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_JOC_FINISH - WF_JOC_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_JOC_FINISH - WF_JOC_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_JOC_FINISH - WF_JOC_START))), 0) AS WF_JOC, "+
					" NVL(CEIL((extract(DAY FROM LT_JOC_FINISH - LT_JOC_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_JOC_FINISH - LT_JOC_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_JOC_FINISH - LT_JOC_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_JOC_FINISH - LT_JOC_START))), 0) AS LT_JOC, "+
					" NVL(CEIL((extract(DAY FROM WF_INVOICING_FINISH - WF_INVOICING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_INVOICING_FINISH - WF_INVOICING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_INVOICING_FINISH - WF_INVOICING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_INVOICING_FINISH - WF_INVOICING_START))), 0) AS WF_INVOICING, "+
					" NVL(CEIL((extract(DAY FROM LT_INVOICING_FINISH - LT_INVOICING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_INVOICING_FINISH - LT_INVOICING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_INVOICING_FINISH - LT_INVOICING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_INVOICING_FINISH - LT_INVOICING_START))), 0) AS LT_INVOICING, "+
					" NVL(CEIL((extract(DAY FROM WF_WASHING_FINISH - WF_WASHING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_WASHING_FINISH - WF_WASHING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_WASHING_FINISH - WF_WASHING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_WASHING_FINISH - WF_WASHING_START))), 0) AS WF_WASHING, "+
					" NVL(CEIL((extract(DAY FROM LT_WASHING_FINISH - LT_WASHING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_WASHING_FINISH - LT_WASHING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_WASHING_FINISH - LT_WASHING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_WASHING_FINISH - LT_WASHING_START))), 0) AS LT_WASHING, "+
					" NVL(CEIL((extract(DAY FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START))), 0) AS WF_NOTIFICATION, "+
					" NVL(CEIL((extract(DAY FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START))), 0) AS LT_PREDELIVERY, "+
					" NVL(CEIL((extract(DAY FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START))), 0) AS WF_SETTLEMENT, "+
					" NVL(CEIL((extract(DAY FROM WF_DELIVERY_FINISH - WF_DELIVERY_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_DELIVERY_FINISH - WF_DELIVERY_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_DELIVERY_FINISH - WF_DELIVERY_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_DELIVERY_FINISH - WF_DELIVERY_START))), 0) AS WF_DELIVERY, "+
					" NVL(CEIL((extract(DAY FROM LT_DELIVERY_FINISH - LT_DELIVERY_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_DELIVERY_FINISH - LT_DELIVERY_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_DELIVERY_FINISH - LT_DELIVERY_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_DELIVERY_FINISH - LT_DELIVERY_START))), 0) AS LT_DELIVERY "+
					" FROM ( "+
					" SELECT "+
					" DAT.T401_TANGGALWO, "+
					" (SELECT M055_KATEGORIJOB.ID FROM T402_JOBRCP INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID  "+
					" 					INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID  "+
					" 					WHERE T402_JOBRCP.T402_T401_NOWO = DAT.ID AND ROWNUM = 1)  KATEGORI_JOB_ID, "+
					" (SELECT M055_KATEGORIJOB.M055_KATEGORIJOB FROM T402_JOBRCP INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID  "+
					" 					INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID  "+
					" 					WHERE T402_JOBRCP.T402_T401_NOWO = DAT.ID AND ROWNUM = 1)  KATEGORI_JOB, "+
					" T601_JOCTECO.T601_TGLJAMJOC AS WF_JOC_START, "+
					" T508_FINALINSPECTION.T508_TGLJAMSELESAI AS WF_JOC_FINISH, "+
					" T601_JOCTECO.T601_TGLJAMJOC AS LT_JOC_START, "+
					" T601_JOCTECO.T601_TglJamJOCSelesai AS LT_JOC_FINISH, "+
					" T601_JOCTECO.T601_TglJamJOCSelesai AS WF_INVOICING_START, "+
					" T701_INVOICE.T701_TglJamMulai AS WF_INVOICING_FINISH, "+
					" T701_INVOICE.T701_TglJamMulai AS LT_INVOICING_START, "+
					" T701_INVOICE.T701_TglJamInvoice AS LT_INVOICING_FINISH, "+
					" T508_FINALINSPECTION.T508_TGLJAMSELESAI AS WF_WASHING_START, "+
					" CASE "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'start')  "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM "+
					" 	ELSE NULL "+
					" END AS WF_WASHING_FINISH, "+
					" CASE "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'start')  "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	ELSE NULL "+
					" END AS LT_WASHING_START, "+
					" CASE "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'finish')  "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	ELSE NULL "+
					" END AS LT_WASHING_FINISH, "+
					" CASE  "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'finish')  "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM "+ 
					" 	ELSE NULL "+
					" END AS WF_NOTIFICATION_START, "+
					" T701_INVOICE.T701_TGLJAMINVOICE AS WF_NOTIFICATION_FINISH, "+
					" DAT.T401_TGLJAMNOTIFIKASI AS LT_PREDELIVERY_START, "+
					" T508_FINALINSPECTION.T508_TGLJAMSELESAI AS LT_PREDELIVERY_FINISH, "+
					" DAT.T401_TGLJAMEXPLANATIONSELESAI AS WF_SETTLEMENT_START, "+
					" T704_SETTLEMENT.T704_TGLJAMSETTLEMENT AS WF_SETTLEMENT_FINISH, "+
					" DAT.T401_TGLJAMNOTIFIKASI AS WF_DELIVERY_START, "+
					" DAT.T401_TGLJAMEXPLANATION AS WF_DELIVERY_FINISH, "+
					" DAT.T401_TGLJAMEXPLANATION AS LT_DELIVERY_START, "+
					" T400_CUSTOMERIN.T400_TGLJAMOUT AS LT_DELIVERY_FINISH  "+
					" FROM ( "+
					" SELECT "+
					" T401_RECEPTION.*, "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID "+
					" FROM  "+
					" T401_RECEPTION "+
					" ) DAT "+
					" LEFT JOIN T601_JOCTECO ON T601_JOCTECO.T601_T401_NOWO = DAT.ID "+
					" LEFT JOIN T508_FINALINSPECTION ON T508_FINALINSPECTION.T508_T401_NOWO = DAT.ID "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID "+
					" LEFT JOIN T600_ANTRICUCI ON T600_ANTRICUCI.T600_T401_NOWO = DAT.ID "+
					" LEFT JOIN T602_ACTUALCUCI ON T602_ACTUALCUCI.T602_T600_ID = T600_ANTRICUCI.T600_ID  "+
					" LEFT JOIN T704_SETTLEMENT ON  T704_SETTLEMENT.RECEPTION_ID = DAT.ID  "+
					" LEFT JOIN T400_CUSTOMERIN ON T400_CUSTOMERIN.ID = DAT.T401_T400_ID "+
					" WHERE 1=1 "+
					" "+dateFilter+" "+
					" "+waitingStatusFilter+" "+
					" ) DAT "+
					" WHERE 1=1 "+
					" "+jenisPekerjaanFilter+" "+
					" ) DAT "+
					" GROUP BY DAT.YEAR, DAT.MONTH, DAT.KATEGORI_JOB "+
					"";
		def results = null
        //println("cetak "+query)
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25],
					column_26: resultRow[26],
					column_27: resultRow[27],
					column_28: resultRow[28],
					column_29: resultRow[29],
					column_30: resultRow[30],
					column_31: resultRow[31],
					column_32: resultRow[32],
					column_33: resultRow[33],
					column_34: resultRow[34],
					column_35: resultRow[35]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}

		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryGrDeliveryLeadTimeDetail(def params, def type){
		String dateFilter = "";
		if(type == 0) {
            dateFilter = " AND DAT.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
	
		final session = sessionFactory.currentSession

        //println("JK "+params.waitingStatus)
		String query =
					" SELECT "+
					" TO_CHAR(DAT.DELIVERY_DATE, 'DD-MON-YYYY') AS DELIVERY_DATE, DAT.NOPOL, DAT.NOWO, DAT.JENIS_KENDARAAN, DAT.JENIS_PEKERJAAN, DAT.WAITING, DAT.SA_RECEIVE, "+
					" DAT.SA_DELIVERY, DAT.PROMISED_DELIVERY_DATE, DAT.PROMISED_DELIVERY_TIME, DAT.FINAL_INSPECTION_DATE, "+
					" DAT.FINAL_INSPECTION_TIME, DAT.JOC_DATE, DAT.JOC_START, DAT.JOC_FINISH, DAT.INVOICE_DATE, DAT.INVOICE_START, "+
					" DAT.INVOICE_FINISH, DAT.WASHING_DATE, DAT.WASHING_START, DAT.WASHING_FINISH, DAT.NOTIFICATION_DATE, "+
					" DAT.NOTIFICATION_TIME, DAT.SETTLEMENT_DATE, DAT.SETTLEMENT_TIME, DAT.DELIVERY_START, DAT.DELIVERY_FINISH, "+
					" DAT.OTD, "+
					" NVL(CEIL((extract(DAY FROM WF_JOC_FINISH - WF_JOC_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_JOC_FINISH - WF_JOC_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_JOC_FINISH - WF_JOC_START) *  60 ) + "+ 
					" 					 (extract(SECOND FROM WF_JOC_FINISH - WF_JOC_START))), 0) AS WF_JOC, "+
					" NVL(CEIL((extract(DAY FROM LT_JOC_FINISH - LT_JOC_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_JOC_FINISH - LT_JOC_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_JOC_FINISH - LT_JOC_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_JOC_FINISH - LT_JOC_START))), 0) AS LT_JOC, "+
					" NVL(CEIL((extract(DAY FROM WF_INVOICING_FINISH - WF_INVOICING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_INVOICING_FINISH - WF_INVOICING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_INVOICING_FINISH - WF_INVOICING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_INVOICING_FINISH - WF_INVOICING_START))), 0) AS WF_INVOICING, "+
					" NVL(CEIL((extract(DAY FROM LT_INVOICING_FINISH - LT_INVOICING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_INVOICING_FINISH - LT_INVOICING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_INVOICING_FINISH - LT_INVOICING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_INVOICING_FINISH - LT_INVOICING_START))), 0) AS LT_INVOICING, "+
					" NVL(CEIL((extract(DAY FROM WF_WASHING_FINISH - WF_WASHING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_WASHING_FINISH - WF_WASHING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_WASHING_FINISH - WF_WASHING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_WASHING_FINISH - WF_WASHING_START))), 0) AS WF_WASHING, "+
					" NVL(CEIL((extract(DAY FROM LT_WASHING_FINISH - LT_WASHING_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_WASHING_FINISH - LT_WASHING_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_WASHING_FINISH - LT_WASHING_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_WASHING_FINISH - LT_WASHING_START))), 0) AS LT_WASHING, "+
					" NVL(CEIL((extract(DAY FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_NOTIFICATION_FINISH - WF_NOTIFICATION_START))), 0) AS WF_NOTIFICATION, "+
					" NVL(CEIL((extract(DAY FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START) *  60 ) + "+ 
					" 					 (extract(SECOND FROM LT_PREDELIVERY_FINISH - LT_PREDELIVERY_START))), 0) AS LT_PREDELIVERY, "+
					" NVL(CEIL((extract(DAY FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_SETTLEMENT_FINISH - WF_SETTLEMENT_START))), 0) AS WF_SETTLEMENT, "+
					" NVL(CEIL((extract(DAY FROM WF_DELIVERY_FINISH - WF_DELIVERY_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM WF_DELIVERY_FINISH - WF_DELIVERY_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM WF_DELIVERY_FINISH - WF_DELIVERY_START) *  60 ) + "+
					" 					 (extract(SECOND FROM WF_DELIVERY_FINISH - WF_DELIVERY_START))), 0) AS WF_DELIVERY, "+
					" NVL(CEIL((extract(DAY FROM LT_DELIVERY_FINISH - LT_DELIVERY_START) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM LT_DELIVERY_FINISH - LT_DELIVERY_START) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM LT_DELIVERY_FINISH - LT_DELIVERY_START) *  60 ) + "+
					" 					 (extract(SECOND FROM LT_DELIVERY_FINISH - LT_DELIVERY_START))), 0) AS LT_DELIVERY "+
					" FROM ( "+
					" SELECT  "+
					" DAT.T401_TANGGALWO AS DELIVERY_DATE, "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL, "+
					" DAT.T401_NOWO AS NOWO, "+
					" M102_BASEMODEL.M102_NAMABASEMODEL AS JENIS_KENDARAAN, "+
					" (SELECT M055_KATEGORIJOB.M055_KATEGORIJOB FROM T402_JOBRCP INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID  "+
					" 					INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID  "+
					" 					WHERE T402_JOBRCP.T402_T401_NOWO = DAT.ID AND ROWNUM =1  AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+"))  JENIS_PEKERJAAN, "+
					" DECODE(DAT.T401_STACUSTOMERTUNGGU, 1, 'Yes', 'No') AS WAITING,  "+
					" DAT.T401_NAMASA AS SA_RECEIVE,  "+
					" DAT.T401_NAMASAEXPLAINATION AS SA_DELIVERY, "+
					" TO_CHAR(DAT.T401_TGLJAMJANJIPENYERAHAN, 'DD-MON-YYYY') AS PROMISED_DELIVERY_DATE,  "+
					" TO_CHAR(DAT.T401_TGLJAMJANJIPENYERAHAN, 'HH24:MI:SS') AS PROMISED_DELIVERY_TIME, "+
					" TO_CHAR(T508_FINALINSPECTION.T508_TGLJAMSELESAI, 'DD-MON-YYYY') AS FINAL_INSPECTION_DATE,  "+
					" TO_CHAR(T508_FINALINSPECTION.T508_TGLJAMSELESAI, 'HH24:MI:SS') AS FINAL_INSPECTION_TIME, "+
					" TO_CHAR(T601_JOCTECO.T601_TGLJAMJOC, 'DD-MON-YYYY') AS JOC_DATE,  "+
					" TO_CHAR(T601_JOCTECO.T601_TGLJAMJOC, 'HH24:MI:SS') AS JOC_START,  "+
					" TO_CHAR(T601_JOCTECO.T601_TGLJAMJOCSELESAI, 'HH24:MI:SS') AS JOC_FINISH, "+
					" TO_CHAR(T701_INVOICE.T701_TGLJAMMULAI, 'DD-MON-YYYY') AS INVOICE_DATE,  "+
					" TO_CHAR(T701_INVOICE.T701_TGLJAMMULAI, 'HH24:MI:SS') AS INVOICE_START, "+ 
					" TO_CHAR(T701_INVOICE.T701_TGLJAMINVOICE, 'HH24:MI:SS') AS INVOICE_FINISH, "+
					" TO_CHAR(T602_ACTUALCUCI.T602_TGLJAM, 'DD-MON-YYYY') AS WASHING_DATE,  "+
					" CASE  "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID  =1 "+
					" 	THEN TO_CHAR(T602_ACTUALCUCI.T602_TGLJAM, 'HH24:MI:SS')  "+
					" 	ELSE NULL  "+
					" END AS WASHING_START,  "+
					" CASE  "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID =4 "+
					" 	THEN TO_CHAR(T602_ACTUALCUCI.T602_TGLJAM, 'HH24:MI:SS')  "+
					" 	ELSE NULL  "+
					" END AS WASHING_FINISH, "+
					" TO_CHAR(DAT.T401_TGLJAMNOTIFIKASI, 'DD-MON-YYYY') AS NOTIFICATION_DATE,  "+
					" TO_CHAR(DAT.T401_TGLJAMNOTIFIKASI, 'HH24:MI:SS') AS NOTIFICATION_TIME, "+
					" TO_CHAR(T704_SETTLEMENT.T704_TGLJAMSETTLEMENT, 'DD-MON-YYYY') AS SETTLEMENT_DATE,  "+
					" TO_CHAR(T704_SETTLEMENT.T704_TGLJAMSETTLEMENT, 'HH24:MI:SS') AS SETTLEMENT_TIME, "+
					" TO_CHAR(DAT.T401_TGLJAMEXPLANATION, 'HH24:MI:SS') AS DELIVERY_START, "+
					" TO_CHAR(T400_CUSTOMERIN.T400_TGLJAMOUT, 'HH24:MI:SS') AS DELIVERY_FINISH, "+
					" '' AS OTD, "+
					" T601_JOCTECO.T601_TGLJAMJOC AS WF_JOC_START, "+
					" T508_FINALINSPECTION.T508_TGLJAMSELESAI AS WF_JOC_FINISH, "+
					" T601_JOCTECO.T601_TGLJAMJOC AS LT_JOC_START, "+
					" T601_JOCTECO.T601_TglJamJOCSelesai AS LT_JOC_FINISH, "+
					" T601_JOCTECO.T601_TglJamJOCSelesai AS WF_INVOICING_START, "+
					" T701_INVOICE.T701_TglJamMulai AS WF_INVOICING_FINISH, "+
					" T701_INVOICE.T701_TglJamMulai AS LT_INVOICING_START, "+
					" T701_INVOICE.T701_TglJamInvoice AS LT_INVOICING_FINISH, "+
					" T508_FINALINSPECTION.T508_TGLJAMSELESAI AS WF_WASHING_START, "+
					" CASE "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID =1 "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	ELSE NULL "+
					" END AS WF_WASHING_FINISH, "+
					" CASE "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID =1  "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	ELSE NULL "+
					" END AS LT_WASHING_START, "+
					" CASE "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID =4  "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	ELSE NULL "+
					" END AS LT_WASHING_FINISH, "+
					" CASE  "+
					" 	WHEN T602_ACTUALCUCI.T602_M452_ID =4  "+
					" 	THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	ELSE NULL  "+
					" END AS WF_NOTIFICATION_START, "+
					" T701_INVOICE.T701_TGLJAMINVOICE AS WF_NOTIFICATION_FINISH, "+
					" DAT.T401_TGLJAMNOTIFIKASI AS LT_PREDELIVERY_START, "+
					" T508_FINALINSPECTION.T508_TGLJAMSELESAI AS LT_PREDELIVERY_FINISH, "+
					" DAT.T401_TGLJAMEXPLANATIONSELESAI AS WF_SETTLEMENT_START, "+
					" T704_SETTLEMENT.T704_TGLJAMSETTLEMENT AS WF_SETTLEMENT_FINISH, "+
					" DAT.T401_TGLJAMNOTIFIKASI AS WF_DELIVERY_START, "+
					" DAT.T401_TGLJAMEXPLANATION AS WF_DELIVERY_FINISH, "+
					" DAT.T401_TGLJAMEXPLANATION AS LT_DELIVERY_START, "+
					" T400_CUSTOMERIN.T400_TGLJAMOUT AS LT_DELIVERY_FINISH "+
					" FROM ( "+
					" SELECT "+
					" T401_RECEPTION.*, "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID "+
					" FROM  "+
					" T401_RECEPTION "+
					" ) DAT "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = DAT.T401_T183_ID LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID =  T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID  "+
					" LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE "+
					" LEFT JOIN M104_MODELNAME ON M104_MODELNAME.ID = T110_FULLMODELCODE.T110_M104_ID "+
					" LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID =   T110_FULLMODELCODE.T110_M102_ID "+
					" LEFT JOIN T508_FINALINSPECTION ON T508_FINALINSPECTION.T508_T401_NOWO = DAT.ID "+
					" LEFT JOIN T601_JOCTECO ON T601_JOCTECO.T601_T401_NOWO = DAT.ID "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.T701_T401_NOWO = DAT.ID "+
					" LEFT JOIN T600_ANTRICUCI ON T600_ANTRICUCI.T600_T401_NOWO = DAT.ID "+
					" LEFT JOIN T602_ACTUALCUCI ON T602_ACTUALCUCI.T602_T600_ID = T600_ANTRICUCI.T600_ID  "+
					" LEFT JOIN T704_SETTLEMENT ON  T704_SETTLEMENT.RECEPTION_ID = DAT.ID "+
					" LEFT JOIN T400_CUSTOMERIN ON T400_CUSTOMERIN.ID = DAT.T401_T400_ID "+
					" WHERE 1=1 "+
					" "+dateFilter+" "+
                    " AND DAT.COMPANY_DEALER_ID='"+params.workshop+"' "+
					" ) DAT "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25],
					column_26: resultRow[26],
					column_27: resultRow[27],
					column_28: resultRow[28],
					column_29: resultRow[29],
					column_30: resultRow[30],
					column_31: resultRow[31],
					column_32: resultRow[32],
					column_33: resultRow[33],
					column_34: resultRow[34],
					column_35: resultRow[35],
					column_36: resultRow[36],
					column_37: resultRow[37],
					column_38: resultRow[38]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryGrDeliverySADeliverySummary(def params, def type){
		String strType = "";
		String dateFilter = "";
		String group = "", orderby = "", jenisPekerjaanFilter = "";

        if(params.jenisPekerjaan){
            jenisPekerjaanFilter = " AND M053_OPERATION.M053_M055_ID IN("+params.jenisPekerjaan+")";
        }

		if(type == 0) {
			strType = " '' AS YEAR, '' AS MONTH, TO_CHAR(DAT.TANGGALWO, 'DD-MON-YYYY') AS DAY, ";	
			group = " TO_CHAR(DAT.TANGGALWO, 'DD-MON-YYYY') ";
            orderby = " ORDER BY TO_DATE(TO_CHAR(DAT.TANGGALWO,'DD-MON-YYYY'),'DD-MON-YYYY') "
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY') ";
		} else if(type == 1) {
			strType = " TO_CHAR(DAT.TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.TANGGALWO, 'MON') AS MONTH, '' AS DAY, ";
			group = " TO_CHAR(DAT.TANGGALWO, 'YYYY'), TO_CHAR(DAT.TANGGALWO, 'MON')";
            orderby = " ORDER BY TO_CHAR(DAT.TANGGALWO, 'YYYY'), TO_DATE(TO_CHAR(DAT.TANGGALWO,'MON'),'MON'), DAT.NAMASA "
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY') BETWEEN '"+params.bulan1+"-"+params.tahun+"' AND '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			strType = " TO_CHAR(DAT.TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, '' AS DAY, ";			
			group = " TO_CHAR(DAT.TANGGALWO, 'YYYY'), DAT.NAMASA ";
            orderby = " ORDER BY TO_DATE(TO_CHAR(DAT.TANGGALWO,'YYYY'),'YYYY'), DAT.NAMASA "
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
	
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" "+strType+" "+
					" DAT.NAMASA, "+
					" NVL(COUNT(DAT.NOWO), 0) AS SA_TARGET, "+
					" NVL(SUM(SADELIVERY), 0) AS SA_DELIVERY,"+
					" NVL(SUM(OTHERSADELIVERY), 0) AS SA_OTHERDELIVERY, "+
					" NVL(COUNT(T701_NOINV), 0) AS TOTAL_INVOICE, "+
					" NVL(SUM(T701_TOTALINV), 0) AS TOTAL_SALES, "+
					" DECODE(SUM(DAT.SADELIVERY), 0, 0, (SUM(DAT.SADELIVERY) / NVL(COUNT(DAT.NOWO), 0)) * 100) AS DELIVERY_RATE  "+
					" FROM ( "+
					" SELECT  "+
					" DAT.T401_TANGGALWO AS TANGGALWO, "+
					" DAT.T401_NAMASA AS NAMASA, "+
					" CASE "+
					" 	WHEN DAT.T401_NAMASA = DAT.T401_NAMASAEXPLAINATION AND DAT.T401_NAMASAEXPLAINATION IS NOT NULL THEN 1 "+
					" 	ELSE 0 "+
					" END AS SADELIVERY, "+
					" CASE "+
					" 	WHEN DAT.T401_NAMASA <> DAT.T401_NAMASAEXPLAINATION AND DAT.T401_NAMASAEXPLAINATION IS NOT NULL THEN 1 "+
					" 	ELSE 0 "+
					" END AS OTHERSADELIVERY, "+
					" DAT.T401_NOWO AS NOWO,  "+
					" T701_INVOICE.T701_NOINV, "+
					" T701_INVOICE.T701_TOTALINV "+
					" FROM ( "+
					" SELECT  "+
					" T401_RECEPTION.*, "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1 AND T701_INVOICE.T701_STADEL = '0') AS INVOICE_ID "+
					" FROM  "+
					" T401_RECEPTION WHERE T401_RECEPTION.T401_STADEL = '0'"+
					" ) DAT "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID " +
                    " LEFT JOIN T402_JOBRCP ON T402_JOBRCP.T402_T401_NOWO = DAT.ID " +
                    " LEFT JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID "+
					" WHERE T701_INVOICE.COMPANY_DEALER_ID = " + params.workshop +
                    " " +jenisPekerjaanFilter+
                    " AND T701_INVOICE.T701_STADEL = '0'"+
					" "+dateFilter+" "+
					" ) DAT "+
					" GROUP BY "+group+" , DAT.NAMASA" +
                    " "+ orderby +
					"";
		def results = null

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: Math.round(resultRow[9])
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryGrDeliverySADeliverySummaryResult(def params, def type){
		String dateFilter = "", orderby = " ORDER BY DAT.NAMASA ", jenisPekerjaanFilter = "";

        if(params.jenisPekerjaan){
            jenisPekerjaanFilter = " AND M053_OPERATION.M053_M055_ID IN("+params.jenisPekerjaan+")";
        }

		if(type == 0) {
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY') ";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY') BETWEEN '"+params.bulan1+"-"+params.tahun+"' AND '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" DAT.NAMASA, "+
					" NVL(COUNT(DAT.NOWO), 0) AS SA_TARGET, "+
					" NVL(SUM(SADELIVERY), 0) AS SA_DELIVERY, "+
					" NVL(SUM(OTHERSADELIVERY), 0) AS SA_OTHERDELIVERY, "+
					" NVL(COUNT(T701_NOINV), 0) AS TOTAL_INVOICE, "+
					" NVL(SUM(T701_TOTALINV), 0) AS TOTAL_SALES, "+
					" DECODE(SUM(DAT.SADELIVERY), 0, 0, (SUM(DAT.SADELIVERY) / NVL(COUNT(DAT.NOWO), 0)) * 100) AS DELIVERY_RATE "+
					" FROM ( "+
					" SELECT "+
					" DAT.T401_TANGGALWO AS TANGGALWO, "+
					" DAT.T401_NAMASA AS NAMASA, "+
					" CASE "+
					" 	WHEN DAT.T401_NAMASA = DAT.T401_NAMASAEXPLAINATION AND DAT.T401_NAMASAEXPLAINATION IS NOT NULL THEN 1 "+
					" 	ELSE 0 "+
					" END AS SADELIVERY, "+
					" CASE "+
					" 	WHEN DAT.T401_NAMASA <> DAT.T401_NAMASAEXPLAINATION AND DAT.T401_NAMASAEXPLAINATION IS NOT NULL THEN 1 "+
					" 	ELSE 0 "+
					" END AS OTHERSADELIVERY, "+
					" DAT.T401_NOWO AS NOWO, "+
					" T701_INVOICE.T701_NOINV, "+
					" T701_INVOICE.T701_TOTALINV "+
					" FROM ( "+
                       " SELECT  "+
                       " T401_RECEPTION.*, "+
                       " (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1 AND T701_INVOICE.T701_STADEL = '0') AS INVOICE_ID "+
                            " FROM  "+
                            " T401_RECEPTION WHERE T401_RECEPTION.T401_STADEL = '0'"+
                            " ) DAT "+
                       " LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID " +
                       " LEFT JOIN T402_JOBRCP ON T402_JOBRCP.T402_T401_NOWO = DAT.ID " +
                       " LEFT JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID "+
                       " WHERE T701_INVOICE.COMPANY_DEALER_ID = " + params.workshop +
                       " " +jenisPekerjaanFilter+
                       " AND T701_INVOICE.T701_STADEL = '0'"+
                       " "+dateFilter+") DAT "+
                       " GROUP BY DAT.NAMASA" +
                       " "+ orderby +
					"";
		def results = null

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: Math.round(resultRow[6])
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryGrDeliverySADeliveryDetail(def params, def type){
		String dateFilter = "";
        String group = "", orderby = "", jenisPekerjaanFilter = "";

        if(params.jenisPekerjaan){
            jenisPekerjaanFilter = " AND M053_OPERATION.M053_M055_ID IN("+params.jenisPekerjaan+")";
        }

		if(type == 0) {
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY') ";
            orderby = " ORDER BY TO_DATE(TO_CHAR(DAT.T401_TANGGALWO,'DD-MON-YYYY'),'DD-MON-YYYY'), DAT.T401_NAMASA "
		} else if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(DAT.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			dateFilter = " AND TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		final session = sessionFactory.currentSession
		String query =
					" SELECT TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') AS TANGGALWO," +
                        " DAT.T401_NAMASA AS NAMASA," +
                        "  M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL," +
                        "  DAT.T401_NOWO," +
                        "  M102_BASEMODEL.M102_NAMABASEMODEL," +
                        "  M055_KATEGORIJOB.M055_KATEGORIJOB AS KATEGORI_JOB," +
                        "  T701_INVOICE.T701_NOINV," +
                        "  TO_CHAR(T701_INVOICE.T701_TOTALINV) AS TOTALINV," +
                        "  DAT.T401_NAMASAEXPLAINATION," +
                        "  T017_MANPOWERABSENSI.T017_KET" +
                    " FROM (SELECT T401_RECEPTION.*," +
                            " (SELECT T701_INVOICE.ID" +
                            "  FROM T701_INVOICE" +
                            "  WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID" +
                            "  AND T701_INVOICE.T701_STADEL = '0'" +
                            "  AND ROWNUM = 1) AS INVOICE_ID" +
                          " FROM T401_RECEPTION WHERE T401_RECEPTION.T401_STADEL = '0')DAT" +
                          " LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID" +
                          " LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = DAT.T401_T183_ID" +
                          " LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID" +
                          " LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE" +
                          " LEFT JOIN M104_MODELNAME ON M104_MODELNAME.ID = T110_FULLMODELCODE.T110_M104_ID" +
                          " LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID = T110_FULLMODELCODE.T110_M102_ID" +
                          " LEFT JOIN T015_NAMAMANPOWER ON T015_NAMAMANPOWER.ID = DAT.T401_T015_ID" +
                          " LEFT JOIN T017_MANPOWERABSENSI ON T017_MANPOWERABSENSI.T017_T015_IDMANPOWER = T015_NAMAMANPOWER.ID" +
                          " LEFT JOIN T402_JOBRCP ON T402_JOBRCP.T402_T401_NOWO = DAT.ID" +
                          " LEFT JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID" +
                          " LEFT JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID" +
                          " WHERE T701_INVOICE.COMPANY_DEALER_ID = "+ params.workshop +
                          " AND T701_INVOICE.T701_STADEL = '0'" +
                          " "+jenisPekerjaanFilter+
					" "+dateFilter+" " +
                    " "+orderby+
                    " ";
		def results = null

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesDeliveryGrOTDRateSummaryServiceAdvisor(def params, def type){
        String strType = ""
        String dateFilter = ""
        String jenisPekerjaanFilter = "";
        String group = ""
        //println params.jenisPekerjaan
        String SAFilter = ""
        if(params.sa){
            SAFilter = "AND T401_RECEPTION.T401_NAMASA in ("+ getServiceAdvisorUser(params.sa) +")"
        }
        if(params.jenisPekerjaan == ""){
            jenisPekerjaanFilter = " ";
        } else {
            jenisPekerjaanFilter = " AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+") ";
        }
        if(type == 0) {
            strType = " '' AS YEAR, '' AS MONTH, ";
            group = "";
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY')  AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
        } else
        if(type == 1) {
            strType = " TO_CHAR(DAT.TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.TANGGALWO, 'MON') AS MONTH, ";
            group = " ,TO_CHAR(DAT.TANGGALWO, 'YYYY'), TO_CHAR(DAT.TANGGALWO, 'MON') ";
            if(params.bulan1.length() == 1){
                params.bulan1 = "0"+params.bulan1;
            }
            if(params.bulan2.length() == 1){
                params.bulan2 = "0"+params.bulan2;
            }
            dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
        }

        final session = sessionFactory.currentSession
        String query =
                " SELECT "+
                        " "+strType+" "+
                        " UPPER(DAT.NAMASA), "+
                        " NVL(COUNT(DAT.UNIT_TOBE_DELIVERED), 0) AS UNIT_TOBE_DELIVERED, "+
                        " NVL(SUM(DAT.UNIT_ONTIME_TOBE_DELIVERED), 0) AS UNIT_ONTIME_TOBE_DELIVERED, "+
                        " NVL(DECODE(COUNT(DAT.UNIT_TOBE_DELIVERED), 0, 0, ROUND(SUM(DAT.UNIT_ONTIME_TOBE_DELIVERED) / COUNT(UNIT_TOBE_DELIVERED),4) * 100), 0) AS OTD_RATE "+
                        " FROM ( "+
                        " SELECT DISTINCT(T401_RECEPTION.T401_TANGGALWO) AS TANGGALWO, T401_RECEPTION.T401_NAMASA AS NAMASA, M055_KATEGORIJOB.M055_KATEGORIJOB AS JENISPEKERJAAN,  "+
                        " T401_RECEPTION.T401_NOWO AS UNIT_TOBE_DELIVERED,  CASE WHEN T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN IS NOT NULL AND T401_RECEPTION.T401_TGLJAMNOTIFIKASI IS NOT NULL AND NVL(ABS( CEIL((extract(DAY FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) * 24 * 60 * 60)+ (extract(HOUR FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 * 60) + (extract(MINUTE FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 ) + (extract(SECOND FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) ))), 0) <= 900  THEN 1  ELSE 0  END   UNIT_ONTIME_TOBE_DELIVERED "+
                        " FROM T402_JOBRCP  "+
                        " LEFT JOIN T401_RECEPTION ON T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID  "+
                        " INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID  "+
                        "  WHERE 1=1 AND T402_JOBRCP.T402_STADEL = '0' AND (T402_JOBRCP.T402_STATAMBAHKURANG IS NULL OR T402_JOBRCP.T402_STATAMBAHKURANG = '0') "+SAFilter+"  "+ jenisPekerjaanFilter + " AND  "+
                        "  T401_RECEPTION.COMPANY_DEALER_ID = '"+params.workshop+"' AND T401_RECEPTION.T401_STASAVE = '0' AND T401_RECEPTION.T401_STADEL = '0'   "+ dateFilter +
                        "  ) DAT  "+
                        " GROUP BY DAT.NAMASA "+group+" "+
                        "";
        def results = null
        //println query
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            results = queryResults.collect { resultRow ->
                [
                        column_0: resultRow[0],
                        column_1: resultRow[1],
                        column_2: resultRow[2],
                        column_3: resultRow[3],
                        column_4: resultRow[4],
                        column_5: resultRow[5]
                ]
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        //println(results.size());
        if(results.size() <= 0) {
            throw new java.lang.Exception("Result size 0");
        }
        return results
	}
	
	def datatablesDeliveryGrOTDRateSummaryJenisPekerjaan(def params, def type){
		String strType = ""
		String dateFilter = "";
		String jenisPekerjaanFilter = "";
		String group = ""
        String SAFilter = ""
        if(params.sa){
            SAFilter = "AND T401_RECEPTION.T401_NAMASA in ("+ getServiceAdvisorUser(params.sa) +")"
        }
		if(params.jenisPekerjaan == ""){
			jenisPekerjaanFilter = " AND M055_KATEGORIJOB.M055_KATEGORIJOB not IN ('BP') ";
		} else {
			jenisPekerjaanFilter = " AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+") ";
		}
		
		if(type == 0) {
			strType = " '' AS YEAR, '' AS MONTH, ";			
			group = "";
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY')  AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		} else
		if(type == 1) {
			strType = " TO_CHAR(DAT.TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.TANGGALWO, 'MON') AS MONTH, ";			
			group = " ,TO_CHAR(DAT.TANGGALWO, 'YYYY'), TO_CHAR(DAT.TANGGALWO, 'MON') ORDER BY TO_CHAR(DAT.TANGGALWO, 'MON') DESC ";
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";						
		} 
		
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" "+strType+" "+
					" DAT.JENIS_PEKERJAAN, "+
					" NVL(COUNT(DAT.UNIT_TOBE_DELIVERED), 0) AS UNIT_TOBE_DELIVERED, "+
					" NVL(SUM(DAT.UNIT_ONTIME_TOBE_DELIVERED), 0) AS UNIT_ONTIME_TOBE_DELIVERED, "+
					" NVL(DECODE(COUNT(DAT.UNIT_TOBE_DELIVERED), 0, 0, ROUND(SUM(DAT.UNIT_ONTIME_TOBE_DELIVERED) / COUNT(UNIT_TOBE_DELIVERED),4) * 100), 0) AS OTD_RATE "+
					" FROM ( "+
                    " SELECT DISTINCT(T401_RECEPTION.T401_TANGGALWO) AS TANGGALWO, T401_RECEPTION.T401_NAMASA AS NAMASA, M055_KATEGORIJOB.M055_KATEGORIJOB AS JENIS_PEKERJAAN,  "+
                    " T401_RECEPTION.T401_NOWO AS UNIT_TOBE_DELIVERED,  CASE WHEN T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN IS NOT NULL AND T401_RECEPTION.T401_TGLJAMNOTIFIKASI IS NOT NULL AND NVL(ABS( CEIL((extract(DAY FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) * 24 * 60 * 60)+ (extract(HOUR FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 * 60) + (extract(MINUTE FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 ) + (extract(SECOND FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) ))), 0) <= 900  THEN 1  ELSE 0  END   UNIT_ONTIME_TOBE_DELIVERED "+
                    " FROM T402_JOBRCP  "+
                    " LEFT JOIN T401_RECEPTION ON T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID  "+
                    " INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID  "+
                    "  WHERE 1=1 AND T402_JOBRCP.T402_STADEL = '0' AND (T402_JOBRCP.T402_STATAMBAHKURANG IS NULL OR T402_JOBRCP.T402_STATAMBAHKURANG = '0') " +
                     " "+SAFilter+"  "+ jenisPekerjaanFilter + " AND  "+
                    "  T401_RECEPTION.COMPANY_DEALER_ID = '"+params.workshop+"' AND T401_RECEPTION.T401_STASAVE = '0' AND T401_RECEPTION.T401_STADEL = '0'   "+ dateFilter +
                    "  ) DAT  "+
					" GROUP BY DAT.JENIS_PEKERJAAN "+group+" "+
					"";
		def results = null
        def TOTAL = 0
        def TOTAL2 = 0
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			 
				list()		
			}
			results = queryResults.collect { resultRow ->
                TOTAL += resultRow[3]
                TOTAL2 += resultRow[4]
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: (TOTAL2 / TOTAL) * 100
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesDeliveryGrOTDRateSummaryTotalWorkshop(def params, def type){
		String dateFilter = "";
        String SAFilter = ""
        String jenisPekerjaanFilter = ""
        if(params.sa){
            SAFilter = "AND T401_RECEPTION.T401_NAMASA in ("+ getServiceAdvisorUser(params.sa) +")"
        }
        if(params.jenisPekerjaan == ""){
            jenisPekerjaanFilter = " AND M055_KATEGORIJOB.M055_KATEGORIJOB not IN ('BP') ";
        } else {
            jenisPekerjaanFilter = " AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+") ";
        }
		if(type == 0) {
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') >= '"+params.tanggal+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') <= '"+params.tanggal2+"' ";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";			
		} 
	
		final session = sessionFactory.currentSession
		String query =
					" SELECT  TO_CHAR(DAT.TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.TANGGALWO, 'MON') AS MONTH,  "+
					" NVL(COUNT(DAT.UNIT_TOBE_DELIVERED), 0) AS UNIT_TOBE_DELIVERED, "+
					" NVL(SUM(DAT.UNIT_ONTIME_TOBE_DELIVERED), 0) AS UNIT_ONTIME_TOBE_DELIVERED, "+
					" NVL(DECODE(COUNT(DAT.UNIT_TOBE_DELIVERED), 0, 0, ROUND(SUM(DAT.UNIT_ONTIME_TOBE_DELIVERED) / COUNT(UNIT_TOBE_DELIVERED),4) * 100), 0) AS OTD_RATE "+
					" FROM ( "+
                            " SELECT DISTINCT(T401_RECEPTION.T401_TANGGALWO) AS TANGGALWO, T401_RECEPTION.T401_NAMASA AS NAMASA, M055_KATEGORIJOB.M055_KATEGORIJOB AS JENIS_PEKERJAAN,  "+
                            " T401_RECEPTION.T401_NOWO AS UNIT_TOBE_DELIVERED,  CASE WHEN T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN IS NOT NULL AND T401_RECEPTION.T401_TGLJAMNOTIFIKASI IS NOT NULL AND NVL(ABS( CEIL((extract(DAY FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) * 24 * 60 * 60)+ (extract(HOUR FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 * 60) + (extract(MINUTE FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 ) + (extract(SECOND FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) ))), 0) <= 900  THEN 1  ELSE 0  END   UNIT_ONTIME_TOBE_DELIVERED "+
                            " FROM T402_JOBRCP  "+
                            " LEFT JOIN T401_RECEPTION ON T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID  "+
                            " INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID  "+
                            "  WHERE 1=1 AND T402_JOBRCP.T402_STADEL = '0' AND (T402_JOBRCP.T402_STATAMBAHKURANG IS NULL OR T402_JOBRCP.T402_STATAMBAHKURANG = '0') " +
                            " "+SAFilter+"  "+ jenisPekerjaanFilter + " AND  "+
                            "  T401_RECEPTION.COMPANY_DEALER_ID = '"+params.workshop+"' AND T401_RECEPTION.T401_STASAVE = '0' AND T401_RECEPTION.T401_STADEL = '0'   "+ dateFilter +
                            "  ) DAT  "+
                    " GROUP BY TO_CHAR(DAT.TANGGALWO, 'YYYY'), TO_CHAR(DAT.TANGGALWO, 'MON') ORDER BY TO_CHAR(DAT.TANGGALWO, 'MON') DESC"+
					"";
		def results = null
        //println "TOATL " + query

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],

				]
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}	
	
	def datatablesDeliveryGrOTDRateDetail(def params, def type){
		String dateFilter = "";
        String SAFilter = ""
        String jenisPekerjaanFilter = "";
        if(params.sa){
            SAFilter = "AND T401_RECEPTION.T401_NAMASA in ("+ getServiceAdvisorUser(params.sa) +")"
        }
        if(params.jenisPekerjaan == ""){
            jenisPekerjaanFilter = " AND M055_KATEGORIJOB.M055_KATEGORIJOB not IN ('BP') ";
        } else {
            jenisPekerjaanFilter = " AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+") ";
        }
		if(type == 0) {
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY')  AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";			
		}

		final session = sessionFactory.currentSession
		String query =
					"SELECT DAT.DELIVERY_DATE, DAT.NOPOL, DAT.KATEGORI_JOB, DAT.NOWO, DAT.M102_NAMABASEMODEL,DAT.NAMASA, " +
                            "  DAT.T401_NAMASAEXPLAINATION, DAT.PROMISED_DELIVERY_DATE, DAT.PROMISED_DELIVERY_TIME,DAT.NOTIFICATION_DATE, " +
                            "  DAT.NOTIFICATION_TIME,DAT.UNIT_ONTIME_TOBE_DELIVERED " +
                            "  FROM( " +
                            "  SELECT DISTINCT(T401_RECEPTION.T401_TANGGALWO),  M055_KATEGORIJOB.M055_KATEGORIJOB AS KATEGORI_JOB, TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MON-YYYY') AS DELIVERY_DATE,  T402_JOBRCP.T402_STADEL, T402_JOBRCP.T402_STATAMBAHKURANG, " +
                            " M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,   " +
                            " T401_RECEPTION.T401_NOWO AS NOWO,  M102_BASEMODEL.M102_NAMABASEMODEL,    UPPER(T401_RECEPTION.T401_NAMASA) AS NAMASA,  T401_RECEPTION.T401_NAMASAEXPLAINATION,   " +
                            "    TO_CHAR(T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN, 'DD-MON-YYYY') AS PROMISED_DELIVERY_DATE,  " +
                            "    TO_CHAR(T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN, 'HH24:MI:SS') AS PROMISED_DELIVERY_TIME,  " +
                            "    TO_CHAR(T401_RECEPTION.T401_TGLJAMNOTIFIKASI, 'DD-MON-YYYY') AS NOTIFICATION_DATE,  " +
                            "    TO_CHAR(T401_RECEPTION.T401_TGLJAMNOTIFIKASI, 'HH24:MI:SS') AS NOTIFICATION_TIME,  " +
                            "    CASE WHEN T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN IS NOT NULL AND T401_RECEPTION.T401_TGLJAMNOTIFIKASI IS NOT NULL AND NVL(ABS( CEIL((extract(DAY FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) * 24 * 60 * 60)+ (extract(HOUR FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 * 60) + (extract(MINUTE FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) *  60 ) + (extract(SECOND FROM T401_RECEPTION.T401_TGLJAMJANJIPENYERAHAN - T401_RECEPTION.T401_TGLJAMNOTIFIKASI) ))), 0) <= 900  THEN 'Yes'   ELSE 'No'   END   UNIT_ONTIME_TOBE_DELIVERED   " +
                            "  FROM   T402_JOBRCP   " +
                            "  LEFT JOIN T401_RECEPTION ON T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID  " +
                            "  INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID " +
                            "  LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = T401_T183_ID   LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID   LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE   LEFT JOIN M104_MODELNAME ON M104_MODELNAME.ID = T110_FULLMODELCODE.T110_M104_ID   LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID =   T110_FULLMODELCODE.T110_M102_ID   " +
                            "  WHERE 1=1  AND " +
                            "  T402_JOBRCP.T402_STADEL = '0' AND (T402_JOBRCP.T402_STATAMBAHKURANG IS NULL OR T402_JOBRCP.T402_STATAMBAHKURANG = '0')" +
                            "  AND T401_RECEPTION.COMPANY_DEALER_ID = '" +params.workshop +"' AND T401_RECEPTION.T401_STASAVE = '0' AND T401_RECEPTION.T401_STADEL = '0'   " +
                            "  " + dateFilter + " " + jenisPekerjaanFilter +
                            "  " + SAFilter +
                            " ORDER BY T401_RECEPTION.T401_TANGGALWO" +
                            "  ) DAT ";
		def results = null
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->

				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11]
				]

			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
}
 
