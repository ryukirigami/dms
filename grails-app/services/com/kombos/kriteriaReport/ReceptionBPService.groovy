package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaProsesBP
import com.kombos.administrasi.VendorAsuransi
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.maintable.MetodeBayar
import com.kombos.maintable.TipeKerusakan
import com.kombos.parts.Konversi
import com.kombos.woinformation.JobRCP

import java.text.DateFormat
import java.text.SimpleDateFormat

class ReceptionBPService {
	boolean transactional = false

	def sessionFactory
    def konversi = new Konversi()


	def datatablesSAList(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession

        String query = "SELECT T015_NAMAMANPOWER.ID, UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) FROM DOM_USER_ROLES " +
                "INNER JOIN DOM_USER ON DOM_USER.USERNAME = DOM_USER_ROLES.USER_ID " +
                "INNER JOIN T015_NAMAMANPOWER ON UPPER(DOM_USER.FULLNAME) = UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) " +
                "WHERE ROLE_ID LIKE '%LSA%' AND DOM_USER.COMPANY_DEALER_ID = "+ params.companyDealerId +" AND DOM_USER.T001_STADEL = '0'";
		final sqlCount = session.createSQLQuery(query)
		final queryResultsCount = sqlCount.with {			
			list()		
		}
        query = "SELECT T015_NAMAMANPOWER.ID, UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) FROM DOM_USER_ROLES " +
                "INNER JOIN DOM_USER ON DOM_USER.USERNAME = DOM_USER_ROLES.USER_ID " +
                "INNER JOIN T015_NAMAMANPOWER ON UPPER(DOM_USER.FULLNAME) = UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) " +
                "WHERE ROLE_ID LIKE '%LSA%' AND DOM_USER.COMPANY_DEALER_ID = "+ params.companyDealerId +" AND DOM_USER.T001_STADEL = '0'";
        if (params."sa") {
            query += " AND LOWER(T015_NAMALENGKAP) LIKE '%" + (params."sa" as String).toLowerCase() + "%'  ";
        }
		query += " ORDER BY T015_NAMALENGKAP " +sortDir+ " ";
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.dump();
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaSa: resultRow[1]
			]
			 
		}
		ret = [sEcho: params.sEcho, iTotalRecords: queryResultsCount.size, iTotalDisplayRecords: queryResultsCount.size, aaData: results]
		return ret
	}
    def dataTablesJenisInsurance(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = VendorAsuransi.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."m193Nama") {
                ilike("m193Nama", "%" + (params."m193Nama" as String) + "%")
            }
            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }
        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    m193Nama: it?.m193Nama

            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        return ret
    }
	def datatablesJenisPayment(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = MetodeBayar.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			if (params."payment") {
				ilike("m701MetodeBayar", "%" + (params."payment" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPayment: it?.m701MetodeBayar

			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret
	}

	def datatablesJenisPekerjaan(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

//        def c = TipeKerusakan.createCriteria()
        def c = NamaProsesBP.createCriteria()
        def results = c.list(

                max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."pekerjaan") {
                ilike("m190NamaProsesBP", "%" + (params."pekerjaan" as String) + "%")
            }
            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }
        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    namaPekerjaan: it?.m190NamaProsesBP
            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        return ret

//		def ret
//		List results = new ArrayList();
//		def result = [id: '1', namaPekerjaan: 'Body']
//		results.add(result);
//		result = [id: '2', namaPekerjaan: 'Preparation']
//		results.add(result);
//		result = [id: '3', namaPekerjaan: 'Painting']
//		results.add(result);
//		result = [id: '4', namaPekerjaan: 'Polishing']
//		results.add(result);
//		result = [id: '5', namaPekerjaan: 'Assembly']
//		results.add(result);
//		ret = [sEcho: params.sEcho, iTotalRecords: results.size, iTotalDisplayRecords: results.size, aaData: results]
//		return ret
	}
	
	def datatablesLeadTimeJenisPekerjaan(def params, def type){
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "";
		String dateFilter = "";
		
		if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
//			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') >= '"+params.tanggal+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') <= '"+params.tanggal2+"'";
			dateFilter = "AND T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')+1";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 					
					"select dat.yr yr, dat.mth mth, dat.jenis_pekerjaan jenis_pekerjaan," +
                            "      max(wfr), min(wfr), avg(wfr)," +
                            "      max(rp), min(rp), avg(rp)," +
                            "      max(wfe), min(wfe), avg(wfe)," +
                            "      max(estimasi), min(estimasi), avg(estimasi)," +
                            "      max(wfp), min(wfp), avg(wfp)," +
                            "      max(ip), min(ip), avg(ip), " +
                            "      max(wfj), min(wfj), avg(wfj)" +
                            "  from (" +
                            "    select to_char(dat.tanggalwo, 'yyyy') yr," +
                            "            to_char(dat.tanggalwo, 'mon') mth," +
                            "            to_char(dat.tanggalwo, 'DD-MM-YYYY') tgl," +
                            "            dat.jenis_pekerjaan jenis_pekerjaan,dat.dealer dealer,NVL(CEIL(" +
                            "            (extract(DAY FROM DAT.finish_wfr - DAT.start_wfr) * 24 * 60 * 60)+" +
                            "            (extract(HOUR FROM DAT.finish_wfr - DAT.start_wfr) *  60 * 60) +" +
                            "            (extract(MINUTE FROM DAT.finish_wfr - DAT.start_wfr) *  60 ) +" +
                            "            (extract(SECOND FROM DAT.finish_wfr - DAT.start_wfr))), 0) AS wfr," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_rp - DAT.start_rp) * 24 * 60 * 60)+" +
                            "            (extract(HOUR FROM DAT.finish_rp - DAT.start_rp) *  60 * 60) +" +
                            "            (extract(MINUTE FROM DAT.finish_rp - DAT.start_rp) *  60 ) +" +
                            "            (extract(SECOND FROM DAT.finish_rp - DAT.start_rp))), 0) AS rp," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_wfe - DAT.start_wfe) * 24 * 60 * 60)+" +
                            "            (extract(HOUR FROM DAT.finish_wfe - DAT.start_wfe) *  60 * 60) + " +
                            "            (extract(MINUTE FROM DAT.finish_wfe - DAT.start_wfe) *  60 ) +   " +
                            "            (extract(SECOND FROM DAT.finish_wfe - DAT.start_wfe))), 0) AS wfe," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_estimasi - DAT.start_estimasi) * 24 * 60 * 60)+" +
                            "            (extract(HOUR FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 * 60) + " +
                            "            (extract(MINUTE FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 ) + " +
                            "            (extract(SECOND FROM DAT.finish_estimasi - DAT.start_estimasi))), 0) AS estimasi,  " +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_wfp - DAT.start_wfp) * 24 * 60 * 60)+    " +
                            "            (extract(HOUR FROM DAT.finish_wfp - DAT.start_wfp) *  60 * 60) +    " +
                            "            (extract(MINUTE FROM DAT.finish_wfp - DAT.start_wfp) *  60 ) +   " +
                            "            (extract(SECOND FROM DAT.finish_wfp - DAT.start_wfp))), 0) AS wfp,  0 ip," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_wfj - DAT.start_wfj) * 24 * 60 * 60)+ " +
                            "            (extract(HOUR FROM DAT.finish_wfj - DAT.start_wfj) *  60 * 60) +    " +
                            "            (extract(MINUTE FROM DAT.finish_wfj - DAT.start_wfj) *  60 ) +      " +
                            "            (extract(SECOND FROM DAT.finish_wfj - DAT.start_wfj))), 0) AS wfj   " +
                            "    from (" +
                            "    select M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN jenis_pekerjaan," +
                            "            T401_RECEPTION.T401_TANGGALWO tanggalwo, T401_RECEPTION.COMPANY_DEALER_ID dealer," +
                            "            case" +
                            "              when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) " +
                            "              then  T400_CustomerIn.T400_TglJamReception" +
                            "            else null" +
                            "            end as finish_wfr," +
                            "                T400_CustomerIn.T400_TglCetakNoAntrian start_wfr," +
                            "            case" +
                            "              when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1)" +
                            "              then  T400_CustomerIn.T400_TglJamSelesaiReception " +
                            "           else null" +
                            "           end as finish_rp," +
                            "           case " +
                            "            when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) " +
                            "            then  T400_CustomerIn.T400_TglJamReception" +
                            "          else null" +
                            "          end as start_rp," +
                            "          case" +
                            "            when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) " +
                            "            then  T400_CustomerIn.T400_TglJamReception" +
                            "          else null" +
                            "          end as finish_wfe," +
                            "          case" +
                            "            when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) " +
                            "            then  T400_CustomerIn.T400_TglJamSelesaiReception " +
                            "          else null" +
                            "          end as start_wfe," +
                            "          case" +
                            "            when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1)" +
                            "            then  T400_CustomerIn.T400_TglJamSelesaiReception" +
                            "          else null " +
                            "          end as finish_estimasi, " +
                            "          case " +
                            "            when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1)" +
                            "            then  T400_CustomerIn.T400_TglJamReception" +
                            "          else null" +
                            "          end as start_estimasi, T193_SPKAsuransi.T193_TglKirimDok finish_wfp," +
                            "          case" +
                            "            when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1)" +
                            "            then  T400_CustomerIn.T400_TglJamSelesaiReception " +
                            "          else null" +
                            "          end as start_wfp,T193_SPKAsuransi.T193_TanggalSPK finish_ip,T193_SPKAsuransi.T193_TglKirimDok start_ip,T401_Reception.T401_TglJamAmbilWO finish_wfj," +
                            "          T193_SPKAsuransi.T193_TanggalSPK start_wfj" +
                            "          from" +
                            "            T401_RECEPTION" +
                            "            left join m401_tipekerusakan on m401_tipekerusakan.id = T401_RECEPTION.T401_M401_ID" +
                            "            left join t400_customerin on T400_CUSTOMERIN.ID = T401_RECEPTION.T401_T400_ID" +
                            "            left join t193_spkasuransi on t193_spkasuransi.id = T401_RECEPTION.T401_T193_IDASURANSI" +
                            "            WHERE T401_RECEPTION.COMPANY_DEALER_ID='"+params.workshop+"' AND T401_RECEPTION.T401_STADEL='0' " +
                            "            "+dateFilter+")dat)" +
                            "          dat group by dat.yr, dat.mth, jenis_pekerjaan, dat.dealer, dat.tgl" +
                            "          order by dat.yr, dat.mth, dat.jenis_pekerjaan,dat.tgl DESC";
		def results = null
        try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12],
					 column_13: resultRow[13],
					 column_14: resultRow[14],
					 column_15: resultRow[15],
					 column_16: resultRow[16],
					 column_17: resultRow[17],
					 column_18: resultRow[18],
					 column_19: resultRow[19],
					 column_20: resultRow[20],
					 column_21: resultRow[21],
					 column_22: resultRow[22],
					 column_23: resultRow[23]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}
		
		return results	
	}
	
	def datatablesLeadTimeJenisPayment(def params, def type){
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "";
		String dateFilter = "";
		
		if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
//			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') >= '"+params.tanggal+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') <= '"+params.tanggal2+"'";
            dateFilter = " AND T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')+1";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
//			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
            dateFilter = " AND T401_TANGGALWO BETWEEN TO_DATE('"+params.bulan1+"-"+params.tahun+"', 'MM-YYYY') AND TO_DATE('"+params.bulan2+"-"+params.tahun+"', 'MM-YYYY')+1";
		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 					
					" select                                                                                                                                 "+
					" dat.yr yr,                                                                                                     "+
					" dat.mth mth,                                                                                                     "+
					" dat.jenis_payment jenis_payment,                                                                                                   "+
					" max(wfr), min(wfr), avg(wfr),                                                                                                          "+
					" max(rp), min(rp), avg(rp),                                                                                                             "+
					" max(wfe), min(wfe), avg(wfe),                                                                                                          "+
					" max(estimasi), min(estimasi), avg(estimasi),                                                                                           "+
					" max(wfp), min(wfp), avg(wfp),                                                                                                          "+
					" max(ip), min(ip), avg(ip),                                                                                                             "+
					" max(wfj), min(wfj), avg(wfj)                                                                                                           "+
					" from (                                                                                                                                 "+
					" select                                                                                                                                 "+
					" to_char(dat.tanggalwo, 'yyyy') yr, "+
					" to_char(dat.tanggalwo, 'mon') mth, "+
					" dat.jenis_payment jenis_payment,                                                                                                                    "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfr - DAT.start_wfr) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfr - DAT.start_wfr) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfr - DAT.start_wfr) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfr - DAT.start_wfr))), 0) AS wfr,                                                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_rp - DAT.start_rp) * 24 * 60 * 60)+                                                              "+
					" 		(extract(HOUR FROM DAT.finish_rp - DAT.start_rp) *  60 * 60) +                                                                   "+
					" 			 (extract(MINUTE FROM DAT.finish_rp - DAT.start_rp) *  60 ) +                                                                "+
					" 					 (extract(SECOND FROM DAT.finish_rp - DAT.start_rp))), 0) AS rp,                                                     "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfe - DAT.start_wfe) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfe - DAT.start_wfe) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfe - DAT.start_wfe) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfe - DAT.start_wfe))), 0) AS wfe,                                                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_estimasi - DAT.start_estimasi) * 24 * 60 * 60)+                                                  "+
					" 		(extract(HOUR FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 * 60) +                                                       "+
					" 			 (extract(MINUTE FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 ) +                                                    "+
					" 					 (extract(SECOND FROM DAT.finish_estimasi - DAT.start_estimasi))), 0) AS estimasi,                                   "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfp - DAT.start_wfp) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfp - DAT.start_wfp) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfp - DAT.start_wfp) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfp - DAT.start_wfp))), 0) AS wfp,                                                  "+
					" 0 ip,                                                                                                                                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfj - DAT.start_wfj) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfj - DAT.start_wfj) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfj - DAT.start_wfj) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfj - DAT.start_wfj))), 0) AS wfj                                                   "+
					" from (                                                                                                                                 "+
					" select                                                                                                                                 "+
					" T402_JobRcp.T402_STACASHINSURANCE jenis_payment, " +
					" T401_RECEPTION.T401_TANGGALWO tanggalwo,                                                                                               "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamReception              "+
					" 	else null                                                                                                                            "+
					" end as finish_wfr,                                                                                                                     "+
					" T400_CustomerIn.T400_TglCetakNoAntrian start_wfr,                                                                                      "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception       "+
					" 	else null                                                                                                                            "+
					" end as finish_rp,                                                                                                                      "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamReception              "+
					" 	else null                                                                                                                            "+
					" end as start_rp,                                                                                                                       "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamReception               "+
					" 	else null                                                                                                                            "+
					" end as finish_wfe,                                                                                                                     "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception       "+
					" 	else null                                                                                                                            "+
					" end as start_wfe,                                                                                                                      "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception        "+
					" 	else null                                                                                                                            "+
					" end as finish_estimasi,                                                                                                                "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamReception               "+
					" 	else null                                                                                                                            "+
					" end as start_estimasi,                                                                                                                 "+
					" T193_SPKAsuransi.T193_TglKirimDok finish_wfp,                                                                                          "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception        "+
					" 	else null                                                                                                                            "+
					" end as start_wfp,                                                                                                                      "+
					" T193_SPKAsuransi.T193_TanggalSPK finish_ip,                                                                                            "+
					" T193_SPKAsuransi.T193_TglKirimDok start_ip,                                                                                            "+
					" T401_Reception.T401_TglJamAmbilWO finish_wfj,                                                                                          "+
					" T193_SPKAsuransi.T193_TanggalSPK start_wfj                                                                                             "+
					" from                                                                                                                                   "+
					" T401_RECEPTION                                                                                                                         "+					
					" left join T402_JobRcp on T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.id "+
					" left join t400_customerin on T400_CUSTOMERIN.ID = T401_RECEPTION.T401_T400_ID                                                          "+
					" left join t193_spkasuransi on t193_spkasuransi.id = T401_RECEPTION.T401_T193_IDASURANSI                                                "+
					" WHERE T401_RECEPTION.COMPANY_DEALER_ID='"+params.workshop+"' AND T401_RECEPTION.T401_STADEL='0' "+dateFilter+" " +
                    ") dat                                                                                                                                  "+
					" ) dat                                                                                                                                  "+
					" group by dat.yr, dat.mth, jenis_payment                                                                                              "+
					" order by dat.yr, dat.mth, dat.jenis_payment DESC                                                                                         "+
					"";
		def results = null
		//println("query payment "+query)
        try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12],
					 column_13: resultRow[13],
					 column_14: resultRow[14],
					 column_15: resultRow[15],
					 column_16: resultRow[16],
					 column_17: resultRow[17],
					 column_18: resultRow[18],
					 column_19: resultRow[19],
					 column_20: resultRow[20],
					 column_21: resultRow[21],
					 column_22: resultRow[22],
					 column_23: resultRow[23]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}
		
		return results	
	}
	
	def datatablesLeadTimeServiceAdvisor(def params, def type){
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "";
		String dateFilter = "";
		
		if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
            dateFilter = " AND T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')+1";
        } else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 					
					"select " +
                            "      dat.yr yr,dat.mth mth,dat.service_advisor service_advisor," +
                            "      max(wfr), min(wfr), avg(wfr),max(rp), min(rp), avg(rp)," +
                            "      max(wfe), min(wfe), avg(wfe),max(estimasi), min(estimasi), avg(estimasi)," +
                            "      max(wfp), min(wfp), avg(wfp),max(ip), min(ip), avg(ip),max(wfj), min(wfj), avg(wfj)" +
                            "from (select" +
                            "            to_char(dat.tanggalwo, 'yyyy') yr,  to_char(dat.tanggalwo, 'mon') mth,  dat.service_advisor service_advisor," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_wfr - DAT.start_wfr) * 24 * 60 * 60)+  " +
                            "              (extract(HOUR FROM DAT.finish_wfr - DAT.start_wfr) *  60 * 60) +" +
                            "              (extract(MINUTE FROM DAT.finish_wfr - DAT.start_wfr) *  60 ) +  " +
                            "              (extract(SECOND FROM DAT.finish_wfr - DAT.start_wfr))), 0) AS wfr," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_rp - DAT.start_rp) * 24 * 60 * 60)+ " +
                            "              (extract(HOUR FROM DAT.finish_rp - DAT.start_rp) *  60 * 60) + " +
                            "              (extract(MINUTE FROM DAT.finish_rp - DAT.start_rp) *  60 ) +  " +
                            "              (extract(SECOND FROM DAT.finish_rp - DAT.start_rp))), 0) AS rp, " +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_wfe - DAT.start_wfe) * 24 * 60 * 60)+ " +
                            "              (extract(HOUR FROM DAT.finish_wfe - DAT.start_wfe) *  60 * 60) + " +
                            "              (extract(MINUTE FROM DAT.finish_wfe - DAT.start_wfe) *  60 ) +   " +
                            "              (extract(SECOND FROM DAT.finish_wfe - DAT.start_wfe))), 0) AS wfe," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_estimasi - DAT.start_estimasi) * 24 * 60 * 60)+" +
                            "              (extract(HOUR FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 * 60) +   " +
                            "              (extract(MINUTE FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 ) +   " +
                            "              (extract(SECOND FROM DAT.finish_estimasi - DAT.start_estimasi))), 0) AS estimasi, " +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_wfp - DAT.start_wfp) * 24 * 60 * 60)+  " +
                            "              (extract(HOUR FROM DAT.finish_wfp - DAT.start_wfp) *  60 * 60) + " +
                            "              (extract(MINUTE FROM DAT.finish_wfp - DAT.start_wfp) *  60 ) +  " +
                            "              (extract(SECOND FROM DAT.finish_wfp - DAT.start_wfp))), 0) AS wfp,0 ip," +
                            "            NVL(CEIL((extract(DAY FROM DAT.finish_wfj - DAT.start_wfj) * 24 * 60 * 60)+ " +
                            "              (extract(HOUR FROM DAT.finish_wfj - DAT.start_wfj) *  60 * 60) +  " +
                            "              (extract(MINUTE FROM DAT.finish_wfj - DAT.start_wfj) *  60 ) +    " +
                            "              (extract(SECOND FROM DAT.finish_wfj - DAT.start_wfj))), 0) AS wfj  " +
                            "      from (" +
                            "        select T401_RECEPTION.t401_namasa service_advisor, T401_RECEPTION.T401_TANGGALWO tanggalwo, " +
                            "        case" +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) " +
                            "          then  T400_CustomerIn.T400_TglJamReception  " +
                            "        else null  " +
                            "        end as finish_wfr, " +
                            "        T400_CustomerIn.T400_TglCetakNoAntrian start_wfr," +
                            "        case " +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) " +
                            "          then  T400_CustomerIn.T400_TglJamSelesaiReception " +
                            "        else null " +
                            "        end as finish_rp," +
                            "        case" +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1)" +
                            "          then  T400_CustomerIn.T400_TglJamReception " +
                            "        else null" +
                            "        end as start_rp," +
                            "        case" +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) " +
                            "          then  T400_CustomerIn.T400_TglJamReception " +
                            "        else null" +
                            "        end as finish_wfe," +
                            "        case " +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) " +
                            "          then  T400_CustomerIn.T400_TglJamSelesaiReception " +
                            "        else null" +
                            "        end as start_wfe," +
                            "        case" +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) " +
                            "          then  T400_CustomerIn.T400_TglJamSelesaiReception " +
                            "        else null " +
                            "        end as finish_estimasi," +
                            "        case" +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1)" +
                            "          then  T400_CustomerIn.T400_TglJamReception" +
                            "        else null" +
                            "        end as start_estimasi," +
                            "        T193_SPKAsuransi.T193_TglKirimDok finish_wfp," +
                            "        case" +
                            "          when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) " +
                            "          then  T400_CustomerIn.T400_TglJamSelesaiReception  " +
                            "        else null" +
                            "        end as start_wfp," +
                            "        T193_SPKAsuransi.T193_TanggalSPK finish_ip,T193_SPKAsuransi.T193_TglKirimDok start_ip," +
                            "        T401_Reception.T401_TglJamAmbilWO finish_wfj,T193_SPKAsuransi.T193_TanggalSPK start_wfj " +
                            "        from T401_RECEPTION" +
                            "        left join t400_customerin on T400_CUSTOMERIN.ID = T401_RECEPTION.T401_T400_ID " +
                            "        left join t193_spkasuransi on t193_spkasuransi.id = T401_RECEPTION.T401_T193_IDASURANSI " +
                            "        WHERE T401_RECEPTION.COMPANY_DEALER_ID='"+params.workshop+"' AND T401_RECEPTION.T401_STADEL='0' " +
                            "        "+dateFilter+" " +
                            "        AND T401_RECEPTION.t401_namasa like '%atmojo%' OR T401_RECEPTION.COMPANY_DEALER_ID='"+params.workshop+"' AND T401_RECEPTION.T401_STADEL='0'" +
                            "        "+dateFilter+" AND T401_RECEPTION.t401_namasa like '%merry%'  ) dat)" +
                            "        dat group by dat.yr, dat.mth, dat.service_advisor " +
                            "        order by dat.yr, dat.mth, dat.service_advisor";
		def results = null
        //println("query SA "+query)
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12],
					 column_13: resultRow[13],
					 column_14: resultRow[14],
					 column_15: resultRow[15],
					 column_16: resultRow[16],
					 column_17: resultRow[17],
					 column_18: resultRow[18],
					 column_19: resultRow[19],
					 column_20: resultRow[20],
					 column_21: resultRow[21],
					 column_22: resultRow[22],
					 column_23: resultRow[23]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}
		
		return results	
	}
	
	def datatablesLeadTimeTotal(def params, def type){
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "";
		String dateFilter = "";
		
		if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
//			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') >= '"+params.tanggal+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY') <= '"+params.tanggal2+"'";
            dateFilter = " AND T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')+1";
        } else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
//			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
            dateFilter = " AND T401_TANGGALWO BETWEEN TO_DATE('"+params.bulan1+"-"+params.tahun+"', 'MM-YYYY') AND TO_DATE('"+params.bulan2+"-"+params.tahun+"', 'MM-YYYY')+1";		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 	
					" select dat.yr yr, dat.mth mth, sum(dat.max_wfr), sum(dat.min_wfr), sum(dat.avg_wfr), "+
					" sum(dat.max_rp), sum(dat.min_rp), sum(dat.avg_rp), "+
					" sum(dat.max_wfe), sum(dat.min_wfe), sum(dat.avg_wfe), "+
					" sum(dat.max_estimasi), sum(dat.min_estimasi), sum(dat.avg_estimasi), "+
					" sum(dat.max_wfp), sum(dat.min_wfp), sum(dat.avg_wfp), "+
					" sum(dat.max_ip), sum(dat.min_ip), sum(dat.avg_ip), "+
					" sum(dat.max_wfj), sum(dat.min_wfj), sum(dat.avg_wfj) "+
					" from ( "+
					" select                                                                                                                                 "+
					" dat.yr yr,                                                                                                     "+
					" dat.mth mth,                                                                                                     "+					
					" max(wfr) max_wfr, min(wfr) min_wfr, avg(wfr) avg_wfr,                                                                                                          "+
					" max(rp) max_rp, min(rp) min_rp, avg(rp) avg_rp,                                                                                                             "+
					" max(wfe) max_wfe, min(wfe) min_wfe, avg(wfe) avg_wfe,                                                                                                          "+
					" max(estimasi) max_estimasi, min(estimasi) min_estimasi, avg(estimasi) avg_estimasi,                                                                                           "+
					" max(wfp) max_wfp, min(wfp) min_wfp, avg(wfp) avg_wfp,                                                                                                          "+
					" max(ip) max_ip, min(ip) min_ip, avg(ip) avg_ip,                                                                                                             "+
					" max(wfj) max_wfj, min(wfj) min_wfj, avg(wfj) avg_wfj                                                                                                           "+
					" from (                                                                                                                                 "+
					" select                                                                                                                                 "+
					" to_char(dat.tanggalwo, 'yyyy') yr, "+
					" to_char(dat.tanggalwo, 'mon') mth, "+					
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfr - DAT.start_wfr) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfr - DAT.start_wfr) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfr - DAT.start_wfr) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfr - DAT.start_wfr))), 0) AS wfr,                                                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_rp - DAT.start_rp) * 24 * 60 * 60)+                                                              "+
					" 		(extract(HOUR FROM DAT.finish_rp - DAT.start_rp) *  60 * 60) +                                                                   "+
					" 			 (extract(MINUTE FROM DAT.finish_rp - DAT.start_rp) *  60 ) +                                                                "+
					" 					 (extract(SECOND FROM DAT.finish_rp - DAT.start_rp))), 0) AS rp,                                                     "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfe - DAT.start_wfe) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfe - DAT.start_wfe) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfe - DAT.start_wfe) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfe - DAT.start_wfe))), 0) AS wfe,                                                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_estimasi - DAT.start_estimasi) * 24 * 60 * 60)+                                                  "+
					" 		(extract(HOUR FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 * 60) +                                                       "+
					" 			 (extract(MINUTE FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 ) +                                                    "+
					" 					 (extract(SECOND FROM DAT.finish_estimasi - DAT.start_estimasi))), 0) AS estimasi,                                   "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfp - DAT.start_wfp) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfp - DAT.start_wfp) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfp - DAT.start_wfp) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfp - DAT.start_wfp))), 0) AS wfp,                                                  "+
					" 0 ip,                                                                                                                                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfj - DAT.start_wfj) * 24 * 60 * 60)+                                                            "+
					" 		(extract(HOUR FROM DAT.finish_wfj - DAT.start_wfj) *  60 * 60) +                                                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfj - DAT.start_wfj) *  60 ) +                                                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfj - DAT.start_wfj))), 0) AS wfj                                                   "+
					" from (                                                                                                                                 "+
					" select                                                                                                                                 "+					
					" T401_RECEPTION.T401_TANGGALWO tanggalwo,                                                                                               "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamReception              "+
					" 	else null                                                                                                                            "+
					" end as finish_wfr,                                                                                                                     "+
					" T400_CustomerIn.T400_TglCetakNoAntrian start_wfr,                                                                                      "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception       "+
					" 	else null                                                                                                                            "+
					" end as finish_rp,                                                                                                                      "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamReception              "+
					" 	else null                                                                                                                            "+
					" end as start_rp,                                                                                                                       "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamReception               "+
					" 	else null                                                                                                                            "+
					" end as finish_wfe,                                                                                                                     "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception       "+
					" 	else null                                                                                                                            "+
					" end as start_wfe,                                                                                                                      "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception        "+
					" 	else null                                                                                                                            "+
					" end as finish_estimasi,                                                                                                                "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamReception               "+
					" 	else null                                                                                                                            "+
					" end as start_estimasi,                                                                                                                 "+
					" T193_SPKAsuransi.T193_TglKirimDok finish_wfp,                                                                                          "+
					" case                                                                                                                                   "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception        "+
					" 	else null                                                                                                                            "+
					" end as start_wfp,                                                                                                                      "+
					" T193_SPKAsuransi.T193_TanggalSPK finish_ip,                                                                                            "+
					" T193_SPKAsuransi.T193_TglKirimDok start_ip,                                                                                            "+
					" T401_Reception.T401_TglJamAmbilWO finish_wfj,                                                                                          "+
					" T193_SPKAsuransi.T193_TanggalSPK start_wfj                                                                                             "+
					" from                                                                                                                                   "+
					" T401_RECEPTION                                                                                                                         "+										
					" left join t400_customerin on T400_CUSTOMERIN.ID = T401_RECEPTION.T401_T400_ID                                                          "+
					" left join t193_spkasuransi on t193_spkasuransi.id = T401_RECEPTION.T401_T193_IDASURANSI                                                "+
					" WHERE T401_RECEPTION.COMPANY_DEALER_ID='"+params.workshop+"' AND T401_RECEPTION.T401_STADEL='0' "+dateFilter+") dat                                                                                                                                  "+
					" ) dat                                                                                                                                  "+
					" group by dat.yr, dat.mth                                                                                              "+
					" order by dat.yr, dat.mth                                                                                          "+
					" ) dat group by dat.yr, dat.mth "+
					"";
		def results = null
        //println("query total "+query)
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12],
					 column_13: resultRow[13],
					 column_14: resultRow[14],
					 column_15: resultRow[15],
					 column_16: resultRow[16],
					 column_17: resultRow[17],
					 column_18: resultRow[18],
					 column_19: resultRow[19],
					 column_20: resultRow[20],
					 column_21: resultRow[21],
					 column_22: resultRow[22]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}
		
		return results		
	}
	
	def datatablesLeadTimeDetail(def params){
		final session = sessionFactory.currentSession

		String query =
					" select "+
					" dat.T400_TglCetakNoAntrian, dat.nopol, dat.M102_NAMABASEMODEL, dat.M401_NAMATIPEKERUSAKAN, T402_STACASHINSURANCE, dat.T401_NAMASA, dat.customer_in, "+
					" dat.reception_tracking_start, dat.reception_tracking_end, dat.estimasi_date, dat.estimasi_start, dat.estimasi_finish, dat.tgl_send_insurance, dat.time_send_insurance,  dat.tgl_upload_spk, dat.time_upload_spk,"+
					" dat.tgl_ambil_wo, dat.jam_ambil_wo, dat.status_wo, "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfr - DAT.start_wfr) * 24 * 60 * 60)+                              "+
					" 		(extract(HOUR FROM DAT.finish_wfr - DAT.start_wfr) *  60 * 60) +                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfr - DAT.start_wfr) *  60 ) +                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfr - DAT.start_wfr))), 0) AS wfr,                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_rp - DAT.start_rp) * 24 * 60 * 60)+                                "+
					" 		(extract(HOUR FROM DAT.finish_rp - DAT.start_rp) *  60 * 60) +                                   "+
					" 			 (extract(MINUTE FROM DAT.finish_rp - DAT.start_rp) *  60 ) +                                "+
					" 					 (extract(SECOND FROM DAT.finish_rp - DAT.start_rp))), 0) AS rp,                     "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfe - DAT.start_wfe) * 24 * 60 * 60)+                              "+
					" 		(extract(HOUR FROM DAT.finish_wfe - DAT.start_wfe) *  60 * 60) +                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfe - DAT.start_wfe) *  60 ) +                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfe - DAT.start_wfe))), 0) AS wfe,                  "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_estimasi - DAT.start_estimasi) * 24 * 60 * 60)+                    "+
					" 		(extract(HOUR FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 * 60) +                       "+
					" 			 (extract(MINUTE FROM DAT.finish_estimasi - DAT.start_estimasi) *  60 ) +                    "+
					" 					 (extract(SECOND FROM DAT.finish_estimasi - DAT.start_estimasi))), 0) AS estimasi,   "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfp - DAT.start_wfp) * 24 * 60 * 60)+                              "+
					" 		(extract(HOUR FROM DAT.finish_wfp - DAT.start_wfp) *  60 * 60) +                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfp - DAT.start_wfp) *  60 ) +                              "+
					" 					 (extract(SECOND FROM DAT.finish_wfp - DAT.start_wfp))), 0) AS wfp,                  "+
					" 0 ip,                                                                                                    "+
					" NVL(CEIL((extract(DAY FROM DAT.finish_wfj - DAT.start_wfj) * 24 * 60 * 60)+                              "+
					" 		(extract(HOUR FROM DAT.finish_wfj - DAT.start_wfj) *  60 * 60) +                                 "+
					" 			 (extract(MINUTE FROM DAT.finish_wfj - DAT.start_wfj) *  60 ) +                              "+
					" 					  (extract(SECOND FROM DAT.finish_wfj - DAT.start_wfj))), 0) AS wfj                   "+
					" from ("+
					" select"+
					" to_char(t400_customerin.T400_TglCetakNoAntrian, 'dd-Mon-yyyy') t400_tglcetaknoantrian,"+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,"+
					" M102_BASEMODEL.M102_NAMABASEMODEL,"+
					" M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN,"+
					" T402_JOBRCP.T402_STACASHINSURANCE,"+
					" T401_RECEPTION.T401_NAMASA,"+
					" to_char(t400_customerin.T400_TglCetakNoAntrian, 'dd-Mon-yyyy') customer_in,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' then to_char(T400_CustomerIn.T400_TglJamReception, 'hh24:mi')"+
					" 	else ''"+
					" end as reception_tracking_start,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' then to_char(T400_CustomerIn.T400_TglJamSelesaiReception, 'hh24:mi')"+
					" 	else ''"+
					" end as reception_tracking_end ,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then to_char(T400_CustomerIn.T400_TglJamReception, 'dd-Mon-yyyy')"+
					" 	else ''"+
					" end as estimasi_date,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then to_char(T400_CustomerIn.T400_TglJamReception, 'hh24:mi')"+
					" 	else ''"+
					" end as estimasi_start,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then to_char(T400_CustomerIn.T400_TglJamSelesaiReception, 'hh24:mi')"+
					" 	else ''"+
					" end as estimasi_finish,"+
					" to_char(T193_TglKirimDok, 'dd-Mon-yyyy') tgl_send_insurance,"+
					" to_char(T193_TglKirimDok, 'hh24:mi') time_send_insurance,"+
					" to_char(T193_TanggalSPK, 'dd-Mon-yyyy') tgl_upload_spk,"+
					" to_char(T193_TanggalSPK, 'hh24:mi') time_upload_spk,"+
					" to_char(T401_TglJamAmbilWO, 'dd-Mon-yyyy') tgl_ambil_wo,"+
					" to_char(T401_TglJamAmbilWO, 'hh24:mi') jam_ambil_wo,"+
					" '' status_wo,"+
					" case           "+                                                                                                                     
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamReception         "+
					" 	else null                                                                                                                       "+
					" end as finish_wfr,                                                                                                                  "+
					" T400_CustomerIn.T400_TglCetakNoAntrian start_wfr,                                                                                   "+
					" case                                                                                                                                "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception  "+
					" 	else null                                                                                                                       "+
					" end as finish_rp,                                                                                                                   "+
					" case                                                                                                                                "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamReception         "+
					" 	else null                                                                                                                       "+
					" end as start_rp,                                                                                                                    "+
					" case                                                                                                                                "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamReception          "+
					" 	else null                                                                                                                       "+
					" end as finish_wfe,                                                                                                                  "+
					" case                                                                                                                                "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception  "+
					" 	else null                                                                                                                       "+
					" end as start_wfe,                                                                                                                   "+
					" case                                                                                                                                "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception   "+
					" 	else null                                                                                                                       "+
					" end as finish_estimasi,                                                                                                             "+
					" case                                                                                                                                "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamReception          "+
					" 	else null                                                                                                                       "+
					" end as start_estimasi,                                                                                                              "+
					" T193_SPKAsuransi.T193_TglKirimDok finish_wfp,                                                                                       "+
					" case                                                                                                                                "+
					" 	when (lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' or 1=1) then  T400_CustomerIn.T400_TglJamSelesaiReception   "+
					" 	else null                                                                                                                       "+
					" end as start_wfp,                                                                                                                   "+
					" T193_SPKAsuransi.T193_TanggalSPK finish_ip,                                                                                         "+
					" T193_SPKAsuransi.T193_TglKirimDok start_ip,                                                                                         "+
					" T401_Reception.T401_TglJamAmbilWO finish_wfj,                                                                                       "+
					" T193_SPKAsuransi.T193_TanggalSPK start_wfj                                                                                          "+
					" from"+
					" t401_reception"+
					" left join t400_customerin on t400_customerin.id =  T401_RECEPTION.T401_T400_ID"+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = T401_T183_ID"+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID"+
					" LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE"+
					" LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID =   T110_FULLMODELCODE.T110_M102_ID"+
					" left join M401_TIPEKERUSAKAN on M401_TIPEKERUSAKAN.id = T401_RECEPTION.T401_M401_ID"+ 
					" left join t193_spkasuransi on t193_spkasuransi.id = T401_RECEPTION.T401_T193_IDASURANSI"+
					" left join T402_JobRcp on T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.id"+
					" WHERE T401_RECEPTION.COMPANY_DEALER_ID='"+params.workshop+"' AND T401_STADEL='0' " +
                    " " +
                    " ) dat ORDER BY T400_TGLCETAKNOANTRIAN ASC"+
					"";
		def results = null
        //println ("query detil "+query)
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}
	
	def getServiceAdvisorById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, T015_NAMALENGKAP FROM T015_NAMAMANPOWER WHERE ID IN ("+id+") ";		
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaSa: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaSa")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}

	def getJenisPekerjaanById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE ID IN ("+id+") ";
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaTipeKerusakan: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaTipeKerusakan")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}
	
	def datatablesCompositionOnJobType(def params){
		final session = sessionFactory.currentSession
        String dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		String query =
					" select "+
					" dat.tanggalwo, sum(dat.total_unit), sum(body_), sum(bp_), sum(twc_), sum(prt_), sum(rtj_)  "+ 
					" from ("+
					" select"+
					" to_char(T401_RECEPTION.T401_TANGGALWO, 'dd-Mon-yyyy') tanggalwo,"+
					" 1 as total_unit,"+
					" case"+
					" 	when lower(M055_KATEGORIJOB.M055_KATEGORIJOB) like '%bp%' and T401_RECEPTION.T401_PROSES3 = '0'  then 1"+
					" 	else 0"+
					" end body_,    "+
					" case"+
					" 	when lower(M055_KATEGORIJOB.M055_KATEGORIJOB) like '%bp%' and T401_RECEPTION.T401_PROSES3 = '1'  then 1"+
					" 	else 0"+
					" end bp_,"+
					" case"+
					" 	when T402_JOBRCP.T402_staWarranty = '1' then 1"+
					" 	else 0"+
					" end twc_,"+
					" nvl((   select case when T403_PARTSRCP.ID is not null then 1 else 0 end "+
					" 	from T403_PARTSRCP where T403_PARTSRCP.T403_T401_NOWO = t401_reception.id and rownum = 1), 0 ) prt_,"+
					" case"+
					" 	when lower(M055_KATEGORIJOB.M055_KATEGORIJOB) like '%return%job%'  then 1"+
					" 	else 0"+
					" end rtj_"+
					" from"+
					" t401_reception"+
					" inner join T402_JOBRCP on T402_JOBRCP.t402_t401_nowo = t401_reception.id"+
					" left join M053_OPERATION on M053_OPERATION.id = T402_JOBRCP.T402_M053_JOBID"+
					" left join M055_KATEGORIJOB on M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID where 1=1 "+ dateFilter +
					" ) dat"+
					" group by dat.tanggalwo order by dat.tanggalwo asc"+
					"";
		def results = null
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesCompositionOnPaymentMethod(def params){
		final session = sessionFactory.currentSession
        String dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		String query =
					" select "+
					" dat.settlementdate, sum(dat.nowo) totalunit, sum(dat.cash) cash, sum(dat.insurance) insurance, sum(dat.twc) twc, sum(dat.prt) prt, sum(dat.rtj) rtj, sum(dat.project) project "+
					" from ("+
					" select "+
					" to_char(T704_TglJamSettlement, 'dd-Mon-yyyy') settlementdate,"+
					" 1 nowo,"+
					" case"+
					" 	when lower(T402_JobRcp.T402_StaCashInsurance) like  '%cash%' then 1"+
					" 	else 0"+
					" end cash,"+
					" case"+
					" 	when lower(T402_JobRcp.T402_StaCashInsurance) like  '%insurance%' then 1"+
					" 	else 0"+
					" end insurance,"+
					" case"+
					" 	when T402_JOBRCP.T402_staWarranty = '1' then 1"+
					" 	else 0"+
					" end twc,"+
					" nvl((   select case when T403_PARTSRCP.ID is not null then 1 else 0 end "+
					" 	from T403_PARTSRCP where T403_PARTSRCP.T403_T401_NOWO = t401_reception.id and rownum = 1), 0 ) prt,"+
					" case"+
					" 	when lower(M055_KATEGORIJOB.M055_KATEGORIJOB) like '%return%job%'  then 1"+
					" 	else 0"+
					" end rtj,"+
					" case"+
					" 	when lower(T183_HistoryCustomerVehicle.T183_staProject) like '%return%job%' then 1"+
					" 	else 0"+
					" end project"+
					" from"+
					" t704_settlement"+
					" inner join T701_INVOICE on T701_INVOICE.ID = T704_SETTLEMENT.T704_T701_NOINV"+
					" inner join t401_reception on t401_reception.id = T701_INVOICE.T701_T401_NOWO"+
					" inner join T402_JOBRCP on T402_JOBRCP.t402_t401_nowo = t401_reception.id"+
					" left join M053_OPERATION on M053_OPERATION.id = T402_JOBRCP.T402_M053_JOBID"+
					" left join M055_KATEGORIJOB on M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID"+
					" left join T183_HISTORYCUSTOMERVEHICLE on T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID where 1= 1 "+ dateFilter +
					" ) dat"+
					" group by dat.settlementdate order by dat.settlementdate asc"+
					"";
		def results = null
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesCompositionOnDamaged(def params){
		final session = sessionFactory.currentSession
        String dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		String query =
					" select "+
					" dat.tglwo, sum(dat.nowo) nowo, sum(dat.tps_line) tps_line, sum(dat.light) light, sum(dat.medium) medium, sum(dat.heavy) heavy, sum(dat.minor) minor "+
					" from "+
					" ("+
					" select "+
					" to_char(T401_RECEPTION.T401_TANGGALWO, 'dd-Mon-yyyy') tglwo,"+
					" 1 nowo,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_StaTPSLine) like '%tps%line% ' then 1"+
					" 	else 0"+
					" end tps_line,"+
					" case"+
					" 	when lower(m401_tipekerusakan.M401_NAMATIPEKERUSAKAN) like '%light%' then 1"+
					" 	else 0"+
					" end light,"+
					" case"+
					" 	when lower(m401_tipekerusakan.M401_NAMATIPEKERUSAKAN) like '%medium%' then 1"+
					" 	else 0"+
					" end medium,"+
					" case"+
					" 	when lower(m401_tipekerusakan.M401_NAMATIPEKERUSAKAN) like '%heavy%' then 1"+
					" 	else 0"+
					" end heavy,"+
					" case"+
					" 	when lower(m401_tipekerusakan.M401_NAMATIPEKERUSAKAN) like '%minor%' then 1"+
					" 	else 0"+
					" end minor"+
					" from t401_reception"+
					" left join m401_tipekerusakan on M401_TIPEKERUSAKAN.ID =  T401_RECEPTION.T401_M401_ID where 1=1 "+ dateFilter +
					" ) dat"+
					" group by dat.tglwo order by dat.tglwo asc"+
					"";
		def results = null
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}

	def datatablesCompositionOnSaChart(def params){

		String dateFilter = "R.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')"
		String dealerFilter = "R.COMPANY_DEALER_ID = " + params.workshop

		final session = sessionFactory.currentSession

		String query = "SELECT D.FULLNAME SA, COUNT(*) JML FROM T401_RECEPTION R LEFT JOIN DOM_USER D ON R.T401_NAMASA = D.USERNAME " +
				"WHERE "+ dealerFilter +" AND "+ dateFilter +" GROUP BY D.FULLNAME";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {
				list()
			}
			results = queryResults.collect { resultRow ->
				[
						column_0: resultRow[0],
						column_1: resultRow[1]
				]
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesCompositionOnSaTable(def params){

		String dateFilter = "R.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')"
		String dealerFilter = "R.COMPANY_DEALER_ID = " + params.workshop

		final session = sessionFactory.currentSession
		String query = "SELECT TGL, SA, SUM(JML) JML_SA FROM (SELECT TO_CHAR(R.T401_TANGGALWO,'DD-MM-YYYY') TGL, D.FULLNAME SA, COUNT(*) JML FROM " +
				"T401_RECEPTION R LEFT JOIN DOM_USER D ON R.T401_NAMASA = D.USERNAME WHERE "+ dealerFilter +" AND " + dateFilter + " GROUP BY R.T401_TANGGALWO,D.FULLNAME " +
				"ORDER BY R.T401_TANGGALWO) GROUP BY  TGL, SA ORDER BY TO_DATE(TGL, 'DD-MM-YYYY')";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesJenisSPK(def params, def type){
        String dateFilter = ""
        String dealerFilter = ""
        if(type==1){
            dateFilter = " and t401_reception.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND t401_reception.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')+1 "
            dealerFilter = " and t401_reception.COMPANY_DEALER_ID = " + params.workshop
        }
		final session = sessionFactory.currentSession
		String query =
					" select "+
					" to_char(tanggalwo, 'yyyy') yr,"+
					" to_char(tanggalwo, 'Mon') mth,"+
					" idspk,"+
					" max(wf_estimasi), min(wf_estimasi), avg(wf_estimasi),"+
					" max(estimasi), min(estimasi), avg(estimasi),"+
					" max(wf_insurance_process), min(wf_insurance_process), avg(wf_insurance_process),"+
					" max(approval), min(approval), avg(approval),"+
					" max(job_dispatch), min(job_dispatch), avg(job_dispatch)"+
					" from"+
					" ("+
					" select "+
					" dat.tanggalwo tanggalwo,"+
					" dat.T191_IDSPK idspk,"+
					" NVL(CEIL((extract(DAY FROM finish_wf_estimasi - start_wf_estimasi) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_wf_estimasi - start_wf_estimasi) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_wf_estimasi - start_wf_estimasi) *  60 ) +"+ 
					" 					 (extract(SECOND FROM finish_wf_estimasi - start_wf_estimasi))), 0) AS wf_estimasi, "+
					" NVL(CEIL((extract(DAY FROM finish_estimasi - start_estimasi) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_estimasi - start_estimasi) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_estimasi - start_estimasi) *  60 ) +"+ 
					" 					 (extract(SECOND FROM finish_estimasi - start_estimasi))), 0) AS estimasi,"+
					" NVL(CEIL((extract(DAY FROM finish_wf_insurance_process - start_wf_insurance_process) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_wf_insurance_process - start_wf_insurance_process) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_wf_insurance_process - start_wf_insurance_process) *  60 ) +"+ 
					" 					 (extract(SECOND FROM finish_wf_insurance_process - start_wf_insurance_process))), 0) AS wf_insurance_process,"+
					" ((finish_approval - start_approval) * 24 * 60 * 60)  approval,                    "+
					" NVL(CEIL((extract(DAY FROM finish_job_dispatch - start_job_dispatch) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_job_dispatch - start_job_dispatch) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_job_dispatch - start_job_dispatch) *  60 ) +"+ 
					" 					 (extract(SECOND FROM finish_job_dispatch - start_job_dispatch))), 0) AS job_dispatch"+
					" from"+
					" ("+
					" select"+
					" T401_RECEPTION.T401_TANGGALWO tanggalwo,"+
					" t191_spk.T191_IDSPK,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_CustomerIn.T400_TglJamReception"+
					" 	else null"+
					" end finish_wf_estimasi,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%reception%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end start_wf_estimasi,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end finish_estimasi,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_CustomerIn.T400_TglJamReception"+
					" 	else null"+
					" end start_estimasi,"+
					" T193_SPKAsuransi.T193_TglKirimDok finish_wf_insurance_process,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%reception%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end start_wf_insurance_process,"+
					" T193_SPKAsuransi.T193_TanggalSPK finish_approval,"+
					" T193_SPKAsuransi.T193_TglKirimDok start_approval,"+
					" T401_Reception.T401_TglJamAmbilWO finish_job_dispatch,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%reception%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end start_job_dispatch"+
					" from t191_spk"+
					" inner join t401_reception on T401_RECEPTION.T401_T191_IDSPK =  T191_SPK.ID"+
					" inner join t400_customerin on T400_CUSTOMERIN.ID = T401_RECEPTION.T401_T400_ID"+
					" left join T193_SPKAsuransi on T193_SPKASURANSI.ID  =  T401_RECEPTION.T401_T193_IDASURANSI"+
                    " WHERE 1=1 and t400_customerin.T400_M400_ID = '2' " + dateFilter + dealerFilter +
					" ) dat"+
					" ) dat"+
					" group by idspk, tanggalwo"+
					"";
		def results = null
        //println "datatablesJenisSPK"
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17]
				]			 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesInsurance(def params, def type){
        String dateFilter = ""
        String dealerFilter = ""
        if(type==1){
            dateFilter = " and t401_reception.T401_TANGGALWO between TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')+1 "
            dealerFilter = " and t401_reception.COMPANY_DEALER_ID = " + params.workshop
        }
		final session = sessionFactory.currentSession
		String query =
					" select "+
					" to_char(tanggalwo, 'yyyy') yr, "+
					" to_char(tanggalwo, 'Mon') mth,"+
					" nama,"+
					" max(wf_estimasi), min(wf_estimasi), avg(wf_estimasi),"+
					" max(estimasi), min(estimasi), avg(estimasi),"+
					" max(wf_insurance_process), min(wf_insurance_process), avg(wf_insurance_process),"+
					" max(approval), min(approval), avg(approval),"+
					" max(job_dispatch), min(job_dispatch), avg(job_dispatch)"+
					" from"+
					" ("+
					" select "+
					" dat.tanggalwo tanggalwo,"+
					" dat.nama nama,"+
					" NVL(CEIL((extract(DAY FROM finish_wf_estimasi - start_wf_estimasi) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_wf_estimasi - start_wf_estimasi) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_wf_estimasi - start_wf_estimasi) *  60 ) +"+ 
					" 					 (extract(SECOND FROM finish_wf_estimasi - start_wf_estimasi))), 0) AS wf_estimasi, "+
					" NVL(CEIL((extract(DAY FROM finish_estimasi - start_estimasi) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_estimasi - start_estimasi) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_estimasi - start_estimasi) *  60 ) +"+ 
					" 					 (extract(SECOND FROM finish_estimasi - start_estimasi))), 0) AS estimasi,"+
					" NVL(CEIL((extract(DAY FROM finish_wf_insurance_process - start_wf_insurance_process) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_wf_insurance_process - start_wf_insurance_process) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_wf_insurance_process - start_wf_insurance_process) *  60 ) + "+
					" 					 (extract(SECOND FROM finish_wf_insurance_process - start_wf_insurance_process))), 0) AS wf_insurance_process,"+
					" ((finish_approval - start_approval) * 24 * 60 * 60)  approval,                    "+
					" NVL(CEIL((extract(DAY FROM finish_job_dispatch - start_job_dispatch) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM finish_job_dispatch - start_job_dispatch) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM finish_job_dispatch - start_job_dispatch) *  60 ) +"+ 
					" 					 (extract(SECOND FROM finish_job_dispatch - start_job_dispatch))), 0) AS job_dispatch"+
					" from"+
					" ("+
					" select"+
					" T401_RECEPTION.T401_TANGGALWO tanggalwo,"+
					" M193_VENDORASURANSI.M193_NAMA nama,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_CustomerIn.T400_TglJamReception"+
					" 	else null"+
					" end finish_wf_estimasi,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%reception%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end start_wf_estimasi,"+
					" case "+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end finish_estimasi,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_CustomerIn.T400_TglJamReception"+
					" 	else null"+
					" end start_estimasi,"+
					" T193_SPKAsuransi.T193_TglKirimDok finish_wf_insurance_process,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%reception%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end start_wf_insurance_process,"+
					" T193_SPKAsuransi.T193_TanggalSPK finish_approval,"+
					" T193_SPKAsuransi.T193_TglKirimDok start_approval,"+
					" T401_Reception.T401_TglJamAmbilWO finish_job_dispatch,"+
					" case"+
					" 	when lower(t401_reception.T401_STARECESTSLSQUOT) like '%reception%' then T400_CustomerIn.T400_TglJamSelesaiReception"+
					" 	else null"+
					" end start_job_dispatch"+
					" from"+
					" m193_vendorasuransi"+
					" inner join T193_SPKAsuransi on T193_SPKASURANSI.T193_M193_ID =   M193_VENDORASURANSI.ID"+
					" inner join t401_reception on T401_RECEPTION.T401_T193_IDASURANSI = T193_SPKASURANSI.ID"+
					" inner join t400_customerin on T400_CUSTOMERIN.ID = T401_RECEPTION.T401_T400_ID"+
                    " WHERE 1=1 and t400_customerin.T400_M400_ID = '2' " + dateFilter + dealerFilter +
					" ) dat"+
					" ) dat group by dat.tanggalwo, dat.nama"+
					"";
		def results = null
        //println "datatablesInsurance"
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17]
				]			 
			}

		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesInsuranceApprovalDetail(def params){
        String dateFilter = ""
        String dealerFilter = ""
        dateFilter = " and t401_reception.T401_TANGGALWO between TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY')+1 "
        dealerFilter = " and t401_reception.COMPANY_DEALER_ID = " + params.workshop

		final session = sessionFactory.currentSession
		String query =
					" select"+ 
					" dat.datee,"+
					" dat.nopol,"+
					" dat.jenis_kendaraan,"+
					" dat.insurance,"+
					" dat.id_spk,"+
					" dat.finish_reception,"+
					" dat.ehr_date,"+
					" dat.ehr_start,"+
					" dat.ehr_finish,"+
					" dat.si_date,"+
					" dat.si_time,"+
					" dat.us_date,"+
					" dat.us_time,"+
					" dat.jd_date,"+
					" dat.jd_time,"+
					" status,"+
					" NVL(CEIL((extract(DAY FROM wf_estimasi_finish - wf_estimasi_start) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM wf_estimasi_finish - wf_estimasi_start) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM wf_estimasi_finish - wf_estimasi_start) *  60 ) +"+ 
					" 					 (extract(SECOND FROM wf_estimasi_finish - wf_estimasi_start))), 0) AS wf_estimasi,"+
					" NVL(CEIL((extract(DAY FROM estimasi_finish - estimasi_start) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM estimasi_finish - estimasi_start) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM estimasi_finish - estimasi_start) *  60 ) +"+ 
					" 					 (extract(SECOND FROM estimasi_finish - estimasi_start))), 0) AS estimasi,"+
					" NVL(CEIL((extract(DAY FROM insurance_process_finish - insurance_process_start) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM insurance_process_finish - insurance_process_start) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM insurance_process_finish - insurance_process_start) *  60 ) +"+ 
					" 					 (extract(SECOND FROM insurance_process_finish - insurance_process_start))), 0) AS insurance_process,                    "+
					" nvl((insurance_approval_finish - insurance_approval_start), 0 ) AS insurance_approval,"+
					"  NVL(CEIL((extract(DAY FROM job_dispatch_finish - job_dispatch_start) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM job_dispatch_finish - insurance_approval_start) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM job_dispatch_finish - insurance_approval_start) *  60 ) +"+ 
					" 					 (extract(SECOND FROM job_dispatch_finish - insurance_approval_start))), 0) AS job_dispatch"+
					" from "+
					" ("+
					" select"+
					" to_char(T401_TanggalWO, 'dd-Mon-yyyy') datee,"+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,"+
					" M102_NamaBaseModel jenis_kendaraan,"+
					" M193_Nama insurance,"+
					" T191_IDSPK id_spk,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' then to_char(T400_TglJamSelesaiReception, 'dd-Mon-yyyy')"+
					" 	else null"+
					" end finish_reception,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then to_char(T400_TglJamReception, 'dd-Mon-yyyy')"+
					" 	else null"+
					" end ehr_date,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then to_char(T400_TglJamReception, 'hh24:mi')"+
					" 	else null"+
					" end ehr_start,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then to_char(T400_TglJamSelesaiReception, 'hh24:mi')"+
					" 	else null"+
					" end ehr_finish,"+
					" to_char(T193_TglKirimDok, 'dd-Mon-yyyy') si_date,"+
					" to_char(T193_TglKirimDok, 'hh24:mi') si_time,"+
					" to_char(T193_TanggalSPK, 'dd-Mon-yyyy') us_date,"+
					" to_char(T193_TanggalSPK, 'hh24:mi') us_time,"+
					" to_char(T401_TglJamAmbilWO, 'dd-Mon-yyyy') jd_date,"+
					" to_char(T401_TglJamAmbilWO, 'hh24:mi') jd_time,"+
					" '' status,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_TglJamReception"+
					" 	else null"+
					" end wf_estimasi_finish, "+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' then T400_TglJamSelesaiReception"+
					" 	else null"+
					" end wf_estimasi_start,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_TglJamSelesaiReception"+
					" 	else null"+
					" end estimasi_finish, "+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_TglJamReception"+
					" 	else null"+
					" end estimasi_start,"+
					" T193_TglKirimDok insurance_process_finish,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%estimasi%' then T400_TglJamSelesaiReception"+
					" 	else null"+
					" end insurance_process_start,"+
					" T193_TanggalSPK insurance_approval_finish,"+
					" T193_TglKirimDok insurance_approval_start,"+
					" T401_TglJamAmbilWO job_dispatch_finish,"+
					" case"+
					" 	when lower(T401_RECEPTION.T401_STARECESTSLSQUOT) like '%reception%' then T400_TglJamSelesaiReception"+
					" 	else null"+
					" end job_dispatch_start"+
					" from"+
					" t401_reception"+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = T401_T183_ID"+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID"+
					" LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE"+
					" LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID =   T110_FULLMODELCODE.T110_M102_ID"+
					" inner  join T193_SPKAsuransi on T193_SPKASURANSI.ID =  T401_RECEPTION.T401_T193_IDASURANSI"+
					" left join M193_VENDORASURANSI on M193_VENDORASURANSI.ID = T193_SPKASURANSI.T193_M193_ID"+
					" inner  join t191_spk on T191_SPK.ID = T401_RECEPTION.T401_T191_IDSPK"+
					" left join t400_customerin on T400_CUSTOMERIN.ID = T401_RECEPTION.T401_T400_ID"+
                    " WHERE 1=1 and t400_customerin.T400_M400_ID = '2'  " + dateFilter + dealerFilter +
					" ) dat    "+
					"";
		def results = null
        //println "datatablesInsuranceApprovalDetail"
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20]
				]			 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesPotentialLostSalesSummary(def params, def type){
		final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("M-yyyy");
        DateFormat df1 = new SimpleDateFormat("MM-yyyy");
        def bulan1 = params.bulan1 + "-" + params.tahun
        def bulan2 = params.bulan2 + "-" + params.tahun
        def startDate = df.parse(bulan1);
        def endDate = df.parse(bulan2);

        String dateFilter = "", strType = "", orderby = ""

        if(type == 1){
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')";
            strType = "(SELECT TO_CHAR(T401_RECEPTION.T401_TANGGALWO,'dd-Mon-yyyy') TGLJAMAPP,"
            orderby = " ORDER BY DAT.TGLJAMAPP"
        }else if(type == 2){
            dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY') BETWEEN '"+df1.format(startDate)+"' AND '"+df1.format(endDate)+"'";
            strType = "(SELECT TO_CHAR(T401_RECEPTION.T401_TANGGALWO,'MON-YYYY') TGLJAMAPP,"
            orderby = " ORDER BY TO_DATE(DAT.TGLJAMAPP, 'MON-YYYY')"
        }
		String query =
					" SELECT" +
                            " DAT.TGLJAMAPP," +
                            " SUM(DAT.ENTRY_BOOKING) ENTRY_BOOKING," +
                            " SUM(DAT.ENTRY_NONBOOKING) ENTRY_NON_BOOKING," +
                            " (SUM(DAT.ENTRY_BOOKING) + SUM(DAT.ENTRY_NONBOOKING)) ENTRY_TOTAL," +
                            " SUM(DAT.LOST_ENTRY_BOOKING) LOST_ENTRY_BOOKING," +
                            " SUM(DAT.LOST_ENTRY_NONBOOKING) LOST_ENTRY_NONBOOKING," +
                            " (SUM(DAT.LOST_ENTRY_BOOKING) + SUM(DAT.LOST_ENTRY_NONBOOKING)) LOST_TOTAL," +
                            " ((SUM(DAT.LOST_ENTRY_BOOKING) + SUM(DAT.LOST_ENTRY_NONBOOKING)) / (SUM(DAT.ENTRY_BOOKING) + SUM(DAT.ENTRY_NONBOOKING)) * 100) PERCENT_LOST" +
                    " FROM" +
                            " " + strType +
                            "  CASE WHEN T301_APPOINTMENT.T301_T401_NOWO IS NOT NULL AND T301_APPOINTMENT.T301_STADEL = 0 AND T301_APPOINTMENT.STA_SAVE = 0" +
                            "    THEN 1" +
                            "    ELSE 0" +
                            "  END ENTRY_BOOKING," +
                            "  CASE WHEN T400_CUSTOMERIN.T400_STAAPP <> 'B' AND T401_RECEPTION.T401_STADEL = 0 AND T401_RECEPTION.T401_STASAVE = 0" +
                            "    THEN 1" +
                            "    ELSE 0" +
                            "  END ENTRY_NONBOOKING," +
                            "  CASE WHEN T301_APPOINTMENT.T301_T401_NOWO IS NOT NULL AND T301_APPOINTMENT.T301_STAOKCANCELRESCHEDULE = 1" +
                            "    THEN 1" +
                            "    ELSE 0" +
                            "  END LOST_ENTRY_BOOKING," +
                            "  CASE WHEN T400_CUSTOMERIN.T400_STAAPP <> 'B' AND T401_RECEPTION.T401_STAOKCANCELRESCHEDULE = 1" +
                            "    THEN 1" +
                            "    ELSE 0" +
                            "  END LOST_ENTRY_NONBOOKING" +
                            "  FROM" +
                            "    T400_CUSTOMERIN" +
                            "    LEFT JOIN T401_RECEPTION ON T401_RECEPTION.T401_T400_ID = T400_CUSTOMERIN.ID" +
                            "    LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO = T401_RECEPTION.ID" +
                            "    WHERE T400_CUSTOMERIN.T400_M400_ID = 2" +
                            "    AND T400_CUSTOMERIN.T400_M011_ID = " + params.workshop +
                            " "+ dateFilter +
					" ) DAT"+
					" GROUP BY DAT.TGLJAMAPP" + orderby +
                    " ASC";
		def results = null

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7]
				]			 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}

    def datatablesPotentialLostSalesDetail(def params, def type){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("M-yyyy");
        DateFormat df1 = new SimpleDateFormat("MM-yyyy");
        def bulan1 = params.bulan1 + "-" + params.tahun
        def bulan2 = params.bulan2 + "-" + params.tahun
        def startDate = df.parse(bulan1);
        def endDate = df.parse(bulan2);

        String dateFilter = "", strType = "", orderby = ""

        if(type == 1){
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')";
            strType = " TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MON-YYYY') AS DATE_CREATED,"
            orderby = " ORDER BY DATE_CREATED ASC"
        }else if(type == 2){
            dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY') BETWEEN '"+df1.format(startDate)+"' AND '"+df1.format(endDate)+"'";
            strType = " TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MON-YYYY') AS DATE_CREATED,"
            orderby = " ORDER BY TO_DATE(DATE_CREATED, 'MON-YYYY') ASC"
        }

        String query =
                " SELECT "+strType+
                        " M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,"+
                        " T401_RECEPTION.T401_NOWO,"+
                        " M102_BASEMODEL.M102_NAMABASEMODEL,"+
                        " (SELECT T402_JOBRCP.T402_STACASHINSURANCE FROM T402_JOBRCP WHERE T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) CASH_INSURANCE,"+
                        " CASE WHEN T301_APPOINTMENT.T301_T401_NOWO IS NOT NULL AND T301_APPOINTMENT.T301_STADEL = 0 AND T301_APPOINTMENT.STA_SAVE = 0" +
                        "   THEN 'BOOKING'"+
                        "   ELSE 'NON BOOKING'"+
                        " END AS IS_BOOKING,"+
                        " M402_ALASANCANCEL.M402_NAMAALASANCANCEL"+
                        " FROM T400_CUSTOMERIN"+
                        " INNER JOIN T401_RECEPTION ON T401_RECEPTION.T401_T400_ID =  T400_CUSTOMERIN.ID"+
                        " LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO = T401_RECEPTION.ID"+
                        " LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = T401_T183_ID"+
                        " LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID"+
                        " LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE"+
                        " LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID =   T110_FULLMODELCODE.T110_M102_ID"+
                        " LEFT JOIN M402_ALASANCANCEL ON M402_ALASANCANCEL.ID = T401_RECEPTION.T401_M402_ID"+
                        " WHERE T400_CUSTOMERIN.T400_M011_ID = " + params.workshop +
                        " AND T401_RECEPTION.T401_STADEL = 0" +
                        " AND T401_RECEPTION.T401_STASAVE = 0" +
                        " AND T400_CUSTOMERIN.T400_M400_ID = 2" +
                        " "+dateFilter+" "+
                        " "+orderby;
        def results = null

        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            results = queryResults.collect { resultRow ->
                [
                        column_0: resultRow[0],
                        column_1: resultRow[1],
                        column_2: resultRow[2],
                        column_3: resultRow[3],
                        column_4: resultRow[4],
                        column_5: resultRow[5],
                        column_6: resultRow[6],
                ]
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return results
    }

    def datatablesAdditionalSPK(def params){
        final session = sessionFactory.currentSession

        String query =
                " SELECT " +
                    " TO_CHAR(R.T401_TANGGALWO, 'DD-MON-YYYY') AS RECDATE," +
                    " M.M116_ID || ' ' || HV.T183_NOPOLTENGAH  || ' ' || HV.T183_NOPOLBELAKANG AS NOPOL," +
                    " B.M102_NAMABASEMODEL AS NAMABASE," +
                    " V.M193_NAMA AS NAMAASU," +
                    " CASE WHEN S.T193_STAUTAMA = '2'" +
                    "  THEN 1" +
                    "  ELSE 0" +
                    " END AS TOTADDSPK," +
                    " CASE WHEN S.T193_STAUTAMA = '2' OR S.T193_STAUTAMA = '1'" +
                    "  THEN 1" +
                    "  ELSE 0" +
                    " END AS TOTSPK" +
                    " FROM T400_CUSTOMERIN C" +
                    " INNER JOIN T401_RECEPTION R ON R.T401_T400_ID = C.ID" +
                    " LEFT JOIN T193_SPKASURANSI S ON S.ID = R.T401_T193_IDASURANSI" +
                    " LEFT JOIN T183_HISTORYCUSTOMERVEHICLE HV ON HV.ID = R.T401_T183_ID" +
                    " LEFT JOIN M193_VENDORASURANSI V ON V.ID = HV.T183_M193_ID" +
                    " LEFT JOIN M116_KODEKOTANOPOL M ON M.ID = HV.T183_M116_ID" +
                    " LEFT JOIN T110_FULLMODELCODE F ON F.ID = HV.T183_T110_FULLMODELCODE" +
                    " LEFT JOIN M102_BASEMODEL B ON B.ID = F.T110_M102_ID" +
                    " WHERE C.T400_M011_ID = " + params.workshop +
                    " AND R.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')" +
                    " AND R.T401_STADEL = 0" +
                    " AND R.T401_STASAVE = 0" +
                    " AND C.T400_M400_ID = 2" +
                    " ORDER BY R.T401_TANGGALWO ASC";
        def results = null

        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            def totAddSpk = 0, totSpk = 0
            results = queryResults.collect { resultRow ->
                totAddSpk = totAddSpk + resultRow[4]
                totSpk = totSpk + resultRow[5]
                [
                        column_0: resultRow[0],
                        column_1: resultRow[1],
                        column_2: resultRow[2],
                        column_3: resultRow[3],
                        column_4: totAddSpk,
                        column_5: totSpk,
                        column_6: resultRow[5] == 0 ? 0 : Math.round(resultRow[4]/resultRow[5])*100
                ]
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return results
    }
	
}
 
