package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer

class ProductionBPService {
	boolean transactional = false

	def sessionFactory
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}

    def ambilJenisPekerjaanId(def id){
        String ret = "";
        final session = sessionFactory.currentSession
        String query = " SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE ID IN ("+id+") ";
        final sqlQuery = session.createSQLQuery(query)
        final queryResults = sqlQuery.with {
            list()
        }
        final results = queryResults.collect { resultRow ->
            [
                    id: resultRow[0],
                    m055KategoriJob: resultRow[1]
            ]
        }
        for(Map<String, Object> result : results) {
            ret += String.valueOf(result.get("m055KategoriJob")) + ", ";
        }
        if(ret != ""){
            ret = ret.substring(0, ret.length()-1);
        }
        return ret;
    }

	
	def getJenisPekerjaanById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M401_NAMATIPEKERUSAKAN FROM M401_TIPEKERUSAKAN WHERE IN ("+id+") ";		
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaTipeKerusakan: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaTipeKerusakan")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}

	def getJenisProsesById(def id){
		return "";
	}

	def datatablesJenisPekerjaan(def params){
		def ret
		/*
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M401_NAMATIPEKERUSAKAN FROM M401_TIPEKERUSAKAN WHERE M401_STADEL = '0' ";
		if (params."pekerjaan") {
			query += " AND LOWER(M401_NAMATIPEKERUSAKAN) LIKE '%" + (params."pekerjaan" as String) + "%'  ";
		}		
		query += " ORDER BY M401_NAMATIPEKERUSAKAN " +sortDir+ " ";
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaPekerjaan: resultRow[1]
			]
			 
		}
		*/
		List results = new ArrayList();		
		def result = [id: '1', namaPekerjaan: 'Light Repair']
		results.add(result);		
		result = [id: '2', namaPekerjaan: 'Medium']
		results.add(result);		
		result = [id: '3', namaPekerjaan: 'Heavy Repair']
		results.add(result);		
		result = [id: '4', namaPekerjaan: 'TPS Line']
		results.add(result);		
		result = [id: '5', namaPekerjaan: 'Project']
		results.add(result);		
		ret = [sEcho: params.sEcho, iTotalRecords: results.size, iTotalDisplayRecords: results.size, aaData: results]
		return ret	
	}
	
	def datatablesJenisProses(def params){		
		def ret
		/*
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		String query = " SELECT SYSDATE AS ID, SYSDATE AS NAMAPROSES FROM DUAL WHERE 1!=1  ";		
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaProses: resultRow[1]
			]
			 
		}
		*/
		List results = new ArrayList();		
		def result = [id: '1', namaProses: 'Body']
		results.add(result);		
		result = [id: '2', namaProses: 'Preparation']
		results.add(result);		
		result = [id: '3', namaProses: 'Painting']
		results.add(result);		
		result = [id: '4', namaProses: 'Polishing']
		results.add(result);		
		result = [id: '5', namaProses: 'Assembly']
		results.add(result);		
		ret = [sEcho: params.sEcho, iTotalRecords: results.size, iTotalDisplayRecords: results.size, aaData: results]
		return ret	
	}
	
	def datatablesProductionLeadTimeDetail (def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                                                                                                                 "+
					" dat.jenis_pekerjaan, dat.datee, dat.jumlahpanel, dat.nopol,  dat.m104_namamodelname,                                                                                   "+
					" NVL(CEIL((extract(DAY FROM dat.body_wfp_end - dat.body_wfp_start) * 24 * 60 * 60)+                                                                                     "+
					"                  (extract(HOUR FROM dat.body_wfp_end - dat.body_wfp_start) *  60 * 60) +                                                                               "+
					"                       (extract(MINUTE FROM dat.body_wfp_end - dat.body_wfp_start) *  60 ) +                                                                            "+
					"                               (extract(SECOND FROM dat.body_wfp_end - dat.body_wfp_start))), 0) body_wfp,                                                              "+
					" NVL(CEIL((extract(DAY FROM dat.body_stg_end - dat.body_stg_start) * 24 * 60 * 60)+                                                                                     "+
					"                  (extract(HOUR FROM dat.body_stg_end - dat.body_stg_start) *  60 * 60) +                                                                               "+
					"                       (extract(MINUTE FROM dat.body_stg_end - dat.body_stg_start) *  60 ) +                                                                            "+
					"                               (extract(SECOND FROM dat.body_stg_end - dat.body_stg_start))), 0) body_stg,                                                              "+
					" NVL(CEIL((extract(DAY FROM dat.body_lt_end - dat.body_lt_start) * 24 * 60 * 60)+                                                                                       "+
					"                  (extract(HOUR FROM dat.body_lt_end - dat.body_lt_start) *  60 * 60) +                                                                                 "+
					"                       (extract(MINUTE FROM dat.body_lt_end - dat.body_lt_start) *  60 ) +                                                                              "+
					"                               (extract(SECOND FROM dat.body_lt_end - dat.body_lt_start))), 0) body_lt,                                                                 "+
					" NVL(CEIL((extract(DAY FROM dat.body_idr_end - dat.body_idr_start) * 24 * 60 * 60)+                                                                                     "+
					"                  (extract(HOUR FROM dat.body_idr_end - dat.body_idr_start) *  60 * 60) +                                                                               "+
					"                       (extract(MINUTE FROM dat.body_idr_end - dat.body_idr_start) *  60 ) +                                                                            "+
					"                               (extract(SECOND FROM dat.body_idr_end - dat.body_idr_start))), 0) body_idr,                                                              "+
					" NVL(CEIL((extract(DAY FROM dat.body_wfq_end - dat.body_wfq_start) * 24 * 60 * 60)+                                                                                     "+
					"                  (extract(HOUR FROM dat.body_wfq_end - dat.body_wfq_start) *  60 * 60) +                                                                               "+
					"                       (extract(MINUTE FROM dat.body_wfq_end - dat.body_wfq_start) *  60 ) +                                                                            "+
					"                               (extract(SECOND FROM dat.body_wfq_end - dat.body_wfq_start))), 0) body_wfq,                                                              "+
					" NVL(CEIL((extract(DAY FROM dat.body_ltq_end - dat.body_ltq_start) * 24 * 60 * 60)+                                                                                     "+
					"                  (extract(HOUR FROM dat.body_ltq_end - dat.body_ltq_start) *  60 * 60) +                                                                               "+
					"                       (extract(MINUTE FROM dat.body_ltq_end - dat.body_ltq_start) *  60 ) +                                                                            "+
					"                               (extract(SECOND FROM dat.body_ltq_end - dat.body_ltq_start))), 0) body_ltq,                                                              "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_wfp_end - dat.preparation_wfp_start) * 24 * 60 * 60)+                                                                       "+
					"                  (extract(HOUR FROM dat.preparation_wfp_end - dat.preparation_wfp_start) *  60 * 60) +                                                                 "+
					"                       (extract(MINUTE FROM dat.preparation_wfp_end - dat.preparation_wfp_start) *  60 ) +                                                              "+
					"                               (extract(SECOND FROM dat.preparation_wfp_end - dat.preparation_wfp_start))), 0) preparation_wfp,                                         "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_stg_end - dat.preparation_stg_start) * 24 * 60 * 60)+                                                                       "+
					"                  (extract(HOUR FROM dat.preparation_stg_end - dat.preparation_stg_start) *  60 * 60) +                                                                 "+
					"                       (extract(MINUTE FROM dat.preparation_stg_end - dat.preparation_stg_start) *  60 ) +                                                              "+
					"                               (extract(SECOND FROM dat.preparation_stg_end - dat.preparation_stg_start))), 0) preparation_stg,                                         "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_lt_end - dat.preparation_lt_start) * 24 * 60 * 60)+                                                                         "+
					"                  (extract(HOUR FROM dat.preparation_lt_end - dat.preparation_lt_start) *  60 * 60) +                                                                   "+
					"                       (extract(MINUTE FROM dat.preparation_lt_end - dat.preparation_lt_start) *  60 ) +                                                                "+
					"                               (extract(SECOND FROM dat.preparation_lt_end - dat.preparation_lt_start))), 0) preparation_lt,                                            "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_idr_end - dat.preparation_idr_start) * 24 * 60 * 60)+                                                                       "+
					"                  (extract(HOUR FROM dat.preparation_idr_end - dat.preparation_idr_start) *  60 * 60) +                                                                 "+
					"                       (extract(MINUTE FROM dat.preparation_idr_end - dat.preparation_idr_start) *  60 ) +                                                              "+
					"                               (extract(SECOND FROM dat.preparation_idr_end - dat.preparation_idr_start))), 0) preparation_idr,                                         "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_wfq_end - dat.preparation_wfq_start) * 24 * 60 * 60)+                                                                       "+
					"                  (extract(HOUR FROM dat.preparation_wfq_end - dat.preparation_wfq_start) *  60 * 60) +                                                                 "+
					"                       (extract(MINUTE FROM dat.preparation_wfq_end - dat.preparation_wfq_start) *  60 ) +                                                              "+
					"                               (extract(SECOND FROM dat.preparation_wfq_end - dat.preparation_wfq_start))), 0) preparation_wfq,                                         "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_ltq_end - dat.preparation_ltq_start) * 24 * 60 * 60)+                                                                       "+
					"                  (extract(HOUR FROM dat.preparation_ltq_end - dat.preparation_ltq_start) *  60 * 60) +                                                                 "+
					"                       (extract(MINUTE FROM dat.preparation_ltq_end - dat.preparation_ltq_start) *  60 ) +                                                              "+
					"                               (extract(SECOND FROM dat.preparation_ltq_end - dat.preparation_ltq_start))), 0) preparation_ltq,                                         "+
					" NVL(CEIL((extract(DAY FROM dat.painting_wfp_end - dat.painting_wfp_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.painting_wfp_end - dat.painting_wfp_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.painting_wfp_end - dat.painting_wfp_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.painting_wfp_end - dat.painting_wfp_start))), 0) painting_wfp,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.painting_stg_end - dat.painting_stg_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.painting_stg_end - dat.painting_stg_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.painting_stg_end - dat.painting_stg_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.painting_stg_end - dat.painting_stg_start))), 0) painting_stg,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.painting_lt_end - dat.painting_lt_start) * 24 * 60 * 60)+                                                                               "+
					"                  (extract(HOUR FROM dat.painting_lt_end - dat.painting_lt_start) *  60 * 60) +                                                                         "+
					"                       (extract(MINUTE FROM dat.painting_lt_end - dat.painting_lt_start) *  60 ) +                                                                      "+
					"                               (extract(SECOND FROM dat.painting_lt_end - dat.painting_lt_start))), 0) painting_lt,                                                     "+
					" NVL(CEIL((extract(DAY FROM dat.painting_idr_end - dat.painting_idr_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.painting_idr_end - dat.painting_idr_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.painting_idr_end - dat.painting_idr_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.painting_idr_end - dat.painting_idr_start))), 0) painting_idr,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.painting_wfq_end - dat.painting_wfq_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.painting_wfq_end - dat.painting_wfq_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.painting_wfq_end - dat.painting_wfq_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.painting_wfq_end - dat.painting_wfq_start))), 0) painting_wfq,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.painting_ltq_end - dat.painting_ltq_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.painting_ltq_end - dat.painting_ltq_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.painting_ltq_end - dat.painting_ltq_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.painting_ltq_end - dat.painting_ltq_start))), 0) painting_ltq,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_wfp_end - dat.polishing_wfp_start) * 24 * 60 * 60)+                                                                           "+
					"                  (extract(HOUR FROM dat.polishing_wfp_end - dat.polishing_wfp_start) *  60 * 60) +                                                                     "+
					"                       (extract(MINUTE FROM dat.polishing_wfp_end - dat.polishing_wfp_start) *  60 ) +                                                                  "+
					"                               (extract(SECOND FROM dat.polishing_wfp_end - dat.polishing_wfp_start))), 0) polishing_wfp,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_stg_end - dat.polishing_stg_start) * 24 * 60 * 60)+                                                                           "+
					"                  (extract(HOUR FROM dat.polishing_stg_end - dat.polishing_stg_start) *  60 * 60) +                                                                     "+
					"                       (extract(MINUTE FROM dat.polishing_stg_end - dat.polishing_stg_start) *  60 ) +                                                                  "+
					"                               (extract(SECOND FROM dat.polishing_stg_end - dat.polishing_stg_start))), 0) polishing_stg,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_lt_end - dat.polishing_lt_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.polishing_lt_end - dat.polishing_lt_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.polishing_lt_end - dat.polishing_lt_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.polishing_lt_end - dat.polishing_lt_start))), 0) polishing_lt,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_idr_end - dat.polishing_idr_start) * 24 * 60 * 60)+                                                                           "+
					"                  (extract(HOUR FROM dat.polishing_idr_end - dat.polishing_idr_start) *  60 * 60) +                                                                     "+
					"                       (extract(MINUTE FROM dat.polishing_idr_end - dat.polishing_idr_start) *  60 ) +                                                                  "+
					"                               (extract(SECOND FROM dat.polishing_idr_end - dat.polishing_idr_start))), 0) polishing_idr,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_wfq_end - dat.polishing_wfq_start) * 24 * 60 * 60)+                                                                           "+
					"                  (extract(HOUR FROM dat.polishing_wfq_end - dat.polishing_wfq_start) *  60 * 60) +                                                                     "+
					"                       (extract(MINUTE FROM dat.polishing_wfq_end - dat.polishing_wfq_start) *  60 ) +                                                                  "+
					"                               (extract(SECOND FROM dat.polishing_wfq_end - dat.polishing_wfq_start))), 0) polishing_wfq,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_ltq_end - dat.polishing_ltq_start) * 24 * 60 * 60)+                                                                           "+
					"                  (extract(HOUR FROM dat.polishing_ltq_end - dat.polishing_ltq_start) *  60 * 60) +                                                                     "+
					"                       (extract(MINUTE FROM dat.polishing_ltq_end - dat.polishing_ltq_start) *  60 ) +                                                                  "+
					"                               (extract(SECOND FROM dat.polishing_ltq_end - dat.polishing_ltq_start))), 0) polishing_ltq,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_wfp_end - dat.assembly_wfp_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.assembly_wfp_end - dat.assembly_wfp_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.assembly_wfp_end - dat.assembly_wfp_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.assembly_wfp_end - dat.assembly_wfp_start))), 0) assembly_wfp,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_stg_end - dat.assembly_stg_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.assembly_stg_end - dat.assembly_stg_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.assembly_stg_end - dat.assembly_stg_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.assembly_stg_end - dat.assembly_stg_start))), 0) assembly_stg,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_lt_end - dat.assembly_lt_start) * 24 * 60 * 60)+                                                                               "+
					"                  (extract(HOUR FROM dat.assembly_lt_end - dat.assembly_lt_start) *  60 * 60) +                                                                         "+
					"                       (extract(MINUTE FROM dat.assembly_lt_end - dat.assembly_lt_start) *  60 ) +                                                                      "+
					"                               (extract(SECOND FROM dat.assembly_lt_end - dat.assembly_lt_start))), 0) assembly_lt,                                                     "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_idr_end - dat.assembly_idr_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.assembly_idr_end - dat.assembly_idr_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.assembly_idr_end - dat.assembly_idr_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.assembly_idr_end - dat.assembly_idr_start))), 0) assembly_idr,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_wfq_end - dat.assembly_wfq_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.assembly_wfq_end - dat.assembly_wfq_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.assembly_wfq_end - dat.assembly_wfq_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.assembly_wfq_end - dat.assembly_wfq_start))), 0) assembly_wfq,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_ltq_end - dat.assembly_ltq_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.assembly_ltq_end - dat.assembly_ltq_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.assembly_ltq_end - dat.assembly_ltq_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.assembly_ltq_end - dat.assembly_ltq_start))), 0) assembly_ltq                                                   "+
					" from (                                                                                                                                                                 "+
					" select                                                                                                                                                                 "+
					" to_char(T401_TanggalWO, 'dd-mon-yyyy') datee,                                                                                                                          "+
					" M046_COLORMATCHING.M046_JMLPANEL jumlahpanel,                                                                                                                          "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,                  "+
					" M104_MODELNAME.M104_NAMAMODELNAME,                                                                                                                                     "+
					" case                                                                                                                                                                   "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%light%repair%' then 'LIGHT REPAIR'                                                                    "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%medium%' then 'MEDIUM'                                                                                "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%heavy%repair%' then 'HEAVY REPAIR'                                                                    "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%minor%' then 'MINOR'                                                                                  "+
					"     when t401_reception.T401_StaTPSLine = '1' then 'TPS LINE'                                                                                                          "+
					"     when T183_HISTORYCUSTOMERVEHICLE.T183_staProject = '1' then 'PROJECT'                                                                                              "+
					"     else 'not defined yet'                                                                                                                                                     "+
					" end jenis_pekerjaan,                                                                                                                                                   "+
					" t452_actual.T452_TglJam body_wfp_end,                                                                                                                                  "+
					" t401_reception.T401_TglJamAmbilWO body_wfp_start,                                                                                                                      "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                       "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_stg_end,                                                                                                                                                      "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                        "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_stg_start,                                                                                                                                                    "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                     "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_lt_end,                                                                                                                                                       "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                     "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_lt_start,                                                                                                                                                     "+
					" T506_TglJamSelesai body_idr_end,                                                                                                                                       "+
					" T506_TglJamMulai body_idr_start,                                                                                                                                       "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%'                                                                                                      "+
					"     then  T507_TglJamMulai                                                                                                                                             "+
					"     else null                                                                                                                                                          "+
					" end body_wfq_end,                                                                                                                                                      "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                     "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_wfq_start,                                                                                                                                                    "+
					" T507_TglJamSelesai body_ltq_end,                                                                                                                                       "+
					" T507_TglJamMulai body_ltq_start,                                                                                                                                       "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end preparation_wfp_end,                                                                                                                                               "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                        "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end preparation_wfp_start,                                                                                                                                             "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end preparation_stg_end,                                                                                                                                               "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end preparation_stg_start,                                                                                                                                             "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                              "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end preparation_lt_end,                                                                                                                                                "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                              "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end preparation_lt_start,                                                                                                                                              "+
					" T506_TglJamSelesai preparation_idr_end,                                                                                                                                "+
					" T506_TglJamMulai preparation_idr_start,                                                                                                                                "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%'                                                                                               "+
					"     then  T507_TglJamMulai                                                                                                                                             "+
					"     else null                                                                                                                                                          "+
					" end preparation_wfq_end,                                                                                                                                               "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                              "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end preparation_wfq_start,                                                                                                                                             "+
					" T507_TglJamSelesai preparation_ltq_end,                                                                                                                                "+
					" T507_TglJamMulai preparation_ltq_start,                                                                                                                                "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                   "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end painting_wfp_end,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end painting_wfp_start,                                                                                                                                                "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                   "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end painting_stg_end,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                    "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end painting_stg_start,                                                                                                                                                "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end painting_lt_end,                                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end painting_lt_start,                                                                                                                                                 "+
					" T506_TglJamSelesai painting_idr_end,                                                                                                                                   "+
					" T506_TglJamMulai painting_idr_start,                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%'                                                                                                  "+
					"     then  T507_TglJamMulai                                                                                                                                             "+
					"     else null                                                                                                                                                          "+
					" end painting_wfq_end,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end painting_wfq_start,                                                                                                                                                "+
					" T507_TglJamSelesai painting_ltq_end,                                                                                                                                   "+
					" T507_TglJamMulai painting_ltq_start,                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                  "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end polishing_wfp_end,                                                                                                                                                 "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                    "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end polishing_wfp_start,                                                                                                                                               "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                  "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end polishing_stg_end,                                                                                                                                                 "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                   "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end polishing_stg_start,                                                                                                                                               "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end polishing_lt_end,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end polishing_lt_start,                                                                                                                                                "+
					" T506_TglJamSelesai polishing_idr_end,                                                                                                                                  "+
					" T506_TglJamMulai polishing_idr_start,                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%'                                                                                                 "+
					"     then  T507_TglJamMulai                                                                                                                                             "+
					"     else null                                                                                                                                                          "+
					" end polishing_wfq_end,                                                                                                                                                 "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end polishing_wfq_start,                                                                                                                                               "+
					" T507_TglJamSelesai polishing_ltq_end,                                                                                                                                  "+
					" T507_TglJamMulai polishing_ltq_start,                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                   "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end assembly_wfp_end,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                   "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end assembly_wfp_start,                                                                                                                                                "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                   "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end assembly_stg_end,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                    "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end assembly_stg_start,                                                                                                                                                "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end assembly_lt_end,                                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end assembly_lt_start,                                                                                                                                                 "+
					" T506_TglJamSelesai assembly_idr_end,                                                                                                                                   "+
					" T506_TglJamMulai assembly_idr_start,                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%'                                                                                                  "+
					"     then  T507_TglJamMulai                                                                                                                                             "+
					"     else null                                                                                                                                                          "+
					" end assembly_wfq_end,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                 "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end assembly_wfq_start,                                                                                                                                                "+
					" T507_TglJamSelesai assembly_ltq_end,                                                                                                                                   "+
					" T507_TglJamMulai assembly_ltq_start                                                                                                                                    "+
					" from                                                                                                                                                                   "+
					" t401_reception                                                                                                                                                         "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                                                                             "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID                                                                 "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID                                                                       "+
					" left join t110_fullmodelcode on T110_FULLMODELCODE.ID =  T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE                                                           "+
					" left join m104_modelname on M104_MODELNAME.ID =  T110_FULLMODELCODE.T110_M104_ID                                                                                       "+
					" left join m401_tipekerusakan on M401_TIPEKERUSAKAN.ID = T401_RECEPTION.T401_M401_ID                                                                                    "+
					" left join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                                                                              "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                                                                          "+
					" left join t452_actual on T452_ACTUAL.T452_T401_NOWO =  T401_RECEPTION.ID                                                                                               "+
					" left join m452_statusactual on m452_statusactual.id =  T452_ACTUAL.T452_M452_ID                                                                                        "+
					" left join t506_idr on T506_IDR.T506_T401_NOWO =  t401_reception.id                                                                                                     "+
					" left join t507_inspectionqc on T507_INSPECTIONQC.T507_T401_NOWO = t401_reception.id                                                                                    "+
					" ) dat                                                                                                                                                                  "+
					" order by dat.jenis_pekerjaan, dat.datee                                                                                                                                "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25],
					column_26: resultRow[26],
					column_27: resultRow[27],
					column_28: resultRow[28],
					column_29: resultRow[29],
					column_30: resultRow[30],
					column_31: resultRow[31],
					column_32: resultRow[32],
					column_33: resultRow[33],
					column_34: resultRow[34]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesProductionLeadTimeSummary (def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                                                                                                                       "+
					" dat.jenis_pekerjaan,                                                                                                                                                         "+
					" avg(wfp), avg(stg), avg(lt), avg(idr), avg(wfq), avg(ltq),                                                                                                                   "+
					" max(wfp), max(stg), max(lt), max(idr), max(wfq), max(ltq),                                                                                                                   "+
					" min(wfp), min(stg), min(lt), min(idr), min(wfq), min(ltq)                                                                                                                    "+
					" from (                                                                                                                                                                       "+
					" select                                                                                                                                                                       "+
					" dat.jenis_pekerjaan,                                                                                                                                                         "+
					" sum(body_wfp) + sum(preparation_wfp) +  sum(painting_wfp) + sum(polishing_wfp) + sum(assembly_wfp)  wfp,                                                                     "+
					" sum(body_stg) + sum(preparation_stg) +  sum(painting_stg) + sum(polishing_stg) + sum(assembly_stg)  stg,                                                                     "+
					" sum(body_lt) + sum(preparation_lt) +  sum(painting_lt) + sum(polishing_lt) + sum(assembly_lt)  lt,                                                                           "+
					" sum(body_idr) + sum(preparation_idr) +  sum(painting_idr) + sum(polishing_idr) + sum(assembly_idr)  idr,                                                                     "+
					" sum(body_wfq) + sum(preparation_wfq) +  sum(painting_wfq) + sum(polishing_wfq) + sum(assembly_wfq)  wfq,                                                                     "+
					" sum(body_ltq) + sum(preparation_ltq) +  sum(painting_ltq) + sum(polishing_ltq) + sum(assembly_ltq)  ltq                                                                      "+
					" from (                                                                                                                                                                       "+
					" select                                                                                                                                                                       "+
					" dat.jenis_pekerjaan, dat.datee, dat.jumlahpanel, dat.nopol,  dat.m104_namamodelname,                                                                                         "+
					" NVL(CEIL((extract(DAY FROM dat.body_wfp_end - dat.body_wfp_start) * 24 * 60 * 60)+                                                                                           "+
					"                  (extract(HOUR FROM dat.body_wfp_end - dat.body_wfp_start) *  60 * 60) +                                                                                     "+
					"                       (extract(MINUTE FROM dat.body_wfp_end - dat.body_wfp_start) *  60 ) +                                                                                  "+
					"                               (extract(SECOND FROM dat.body_wfp_end - dat.body_wfp_start))), 0) body_wfp,                                                                    "+
					" NVL(CEIL((extract(DAY FROM dat.body_stg_end - dat.body_stg_start) * 24 * 60 * 60)+                                                                                           "+
					"                  (extract(HOUR FROM dat.body_stg_end - dat.body_stg_start) *  60 * 60) +                                                                                     "+
					"                       (extract(MINUTE FROM dat.body_stg_end - dat.body_stg_start) *  60 ) +                                                                                  "+
					"                               (extract(SECOND FROM dat.body_stg_end - dat.body_stg_start))), 0) body_stg,                                                                    "+
					" NVL(CEIL((extract(DAY FROM dat.body_lt_end - dat.body_lt_start) * 24 * 60 * 60)+                                                                                             "+
					"                  (extract(HOUR FROM dat.body_lt_end - dat.body_lt_start) *  60 * 60) +                                                                                       "+
					"                       (extract(MINUTE FROM dat.body_lt_end - dat.body_lt_start) *  60 ) +                                                                                    "+
					"                               (extract(SECOND FROM dat.body_lt_end - dat.body_lt_start))), 0) body_lt,                                                                       "+
					" NVL(CEIL((extract(DAY FROM dat.body_idr_end - dat.body_idr_start) * 24 * 60 * 60)+                                                                                           "+
					"                  (extract(HOUR FROM dat.body_idr_end - dat.body_idr_start) *  60 * 60) +                                                                                     "+
					"                       (extract(MINUTE FROM dat.body_idr_end - dat.body_idr_start) *  60 ) +                                                                                  "+
					"                               (extract(SECOND FROM dat.body_idr_end - dat.body_idr_start))), 0) body_idr,                                                                    "+
					" NVL(CEIL((extract(DAY FROM dat.body_wfq_end - dat.body_wfq_start) * 24 * 60 * 60)+                                                                                           "+
					"                  (extract(HOUR FROM dat.body_wfq_end - dat.body_wfq_start) *  60 * 60) +                                                                                     "+
					"                       (extract(MINUTE FROM dat.body_wfq_end - dat.body_wfq_start) *  60 ) +                                                                                  "+
					"                               (extract(SECOND FROM dat.body_wfq_end - dat.body_wfq_start))), 0) body_wfq,                                                                    "+
					" NVL(CEIL((extract(DAY FROM dat.body_ltq_end - dat.body_ltq_start) * 24 * 60 * 60)+                                                                                           "+
					"                  (extract(HOUR FROM dat.body_ltq_end - dat.body_ltq_start) *  60 * 60) +                                                                                     "+
					"                       (extract(MINUTE FROM dat.body_ltq_end - dat.body_ltq_start) *  60 ) +                                                                                  "+
					"                               (extract(SECOND FROM dat.body_ltq_end - dat.body_ltq_start))), 0) body_ltq,                                                                    "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_wfp_end - dat.preparation_wfp_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.preparation_wfp_end - dat.preparation_wfp_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.preparation_wfp_end - dat.preparation_wfp_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.preparation_wfp_end - dat.preparation_wfp_start))), 0) preparation_wfp,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_stg_end - dat.preparation_stg_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.preparation_stg_end - dat.preparation_stg_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.preparation_stg_end - dat.preparation_stg_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.preparation_stg_end - dat.preparation_stg_start))), 0) preparation_stg,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_lt_end - dat.preparation_lt_start) * 24 * 60 * 60)+                                                                               "+
					"                  (extract(HOUR FROM dat.preparation_lt_end - dat.preparation_lt_start) *  60 * 60) +                                                                         "+
					"                       (extract(MINUTE FROM dat.preparation_lt_end - dat.preparation_lt_start) *  60 ) +                                                                      "+
					"                               (extract(SECOND FROM dat.preparation_lt_end - dat.preparation_lt_start))), 0) preparation_lt,                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_idr_end - dat.preparation_idr_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.preparation_idr_end - dat.preparation_idr_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.preparation_idr_end - dat.preparation_idr_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.preparation_idr_end - dat.preparation_idr_start))), 0) preparation_idr,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_wfq_end - dat.preparation_wfq_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.preparation_wfq_end - dat.preparation_wfq_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.preparation_wfq_end - dat.preparation_wfq_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.preparation_wfq_end - dat.preparation_wfq_start))), 0) preparation_wfq,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_ltq_end - dat.preparation_ltq_start) * 24 * 60 * 60)+                                                                             "+
					"                  (extract(HOUR FROM dat.preparation_ltq_end - dat.preparation_ltq_start) *  60 * 60) +                                                                       "+
					"                       (extract(MINUTE FROM dat.preparation_ltq_end - dat.preparation_ltq_start) *  60 ) +                                                                    "+
					"                               (extract(SECOND FROM dat.preparation_ltq_end - dat.preparation_ltq_start))), 0) preparation_ltq,                                               "+
					" NVL(CEIL((extract(DAY FROM dat.painting_wfp_end - dat.painting_wfp_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.painting_wfp_end - dat.painting_wfp_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.painting_wfp_end - dat.painting_wfp_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.painting_wfp_end - dat.painting_wfp_start))), 0) painting_wfp,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.painting_stg_end - dat.painting_stg_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.painting_stg_end - dat.painting_stg_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.painting_stg_end - dat.painting_stg_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.painting_stg_end - dat.painting_stg_start))), 0) painting_stg,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.painting_lt_end - dat.painting_lt_start) * 24 * 60 * 60)+                                                                                     "+
					"                  (extract(HOUR FROM dat.painting_lt_end - dat.painting_lt_start) *  60 * 60) +                                                                               "+
					"                       (extract(MINUTE FROM dat.painting_lt_end - dat.painting_lt_start) *  60 ) +                                                                            "+
					"                               (extract(SECOND FROM dat.painting_lt_end - dat.painting_lt_start))), 0) painting_lt,                                                           "+
					" NVL(CEIL((extract(DAY FROM dat.painting_idr_end - dat.painting_idr_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.painting_idr_end - dat.painting_idr_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.painting_idr_end - dat.painting_idr_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.painting_idr_end - dat.painting_idr_start))), 0) painting_idr,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.painting_wfq_end - dat.painting_wfq_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.painting_wfq_end - dat.painting_wfq_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.painting_wfq_end - dat.painting_wfq_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.painting_wfq_end - dat.painting_wfq_start))), 0) painting_wfq,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.painting_ltq_end - dat.painting_ltq_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.painting_ltq_end - dat.painting_ltq_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.painting_ltq_end - dat.painting_ltq_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.painting_ltq_end - dat.painting_ltq_start))), 0) painting_ltq,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_wfp_end - dat.polishing_wfp_start) * 24 * 60 * 60)+                                                                                 "+
					"                  (extract(HOUR FROM dat.polishing_wfp_end - dat.polishing_wfp_start) *  60 * 60) +                                                                           "+
					"                       (extract(MINUTE FROM dat.polishing_wfp_end - dat.polishing_wfp_start) *  60 ) +                                                                        "+
					"                               (extract(SECOND FROM dat.polishing_wfp_end - dat.polishing_wfp_start))), 0) polishing_wfp,                                                     "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_stg_end - dat.polishing_stg_start) * 24 * 60 * 60)+                                                                                 "+
					"                  (extract(HOUR FROM dat.polishing_stg_end - dat.polishing_stg_start) *  60 * 60) +                                                                           "+
					"                       (extract(MINUTE FROM dat.polishing_stg_end - dat.polishing_stg_start) *  60 ) +                                                                        "+
					"                               (extract(SECOND FROM dat.polishing_stg_end - dat.polishing_stg_start))), 0) polishing_stg,                                                     "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_lt_end - dat.polishing_lt_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.polishing_lt_end - dat.polishing_lt_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.polishing_lt_end - dat.polishing_lt_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.polishing_lt_end - dat.polishing_lt_start))), 0) polishing_lt,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_idr_end - dat.polishing_idr_start) * 24 * 60 * 60)+                                                                                 "+
					"                  (extract(HOUR FROM dat.polishing_idr_end - dat.polishing_idr_start) *  60 * 60) +                                                                           "+
					"                       (extract(MINUTE FROM dat.polishing_idr_end - dat.polishing_idr_start) *  60 ) +                                                                        "+
					"                               (extract(SECOND FROM dat.polishing_idr_end - dat.polishing_idr_start))), 0) polishing_idr,                                                     "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_wfq_end - dat.polishing_wfq_start) * 24 * 60 * 60)+                                                                                 "+
					"                  (extract(HOUR FROM dat.polishing_wfq_end - dat.polishing_wfq_start) *  60 * 60) +                                                                           "+
					"                       (extract(MINUTE FROM dat.polishing_wfq_end - dat.polishing_wfq_start) *  60 ) +                                                                        "+
					"                               (extract(SECOND FROM dat.polishing_wfq_end - dat.polishing_wfq_start))), 0) polishing_wfq,                                                     "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_ltq_end - dat.polishing_ltq_start) * 24 * 60 * 60)+                                                                                 "+
					"                  (extract(HOUR FROM dat.polishing_ltq_end - dat.polishing_ltq_start) *  60 * 60) +                                                                           "+
					"                       (extract(MINUTE FROM dat.polishing_ltq_end - dat.polishing_ltq_start) *  60 ) +                                                                        "+
					"                               (extract(SECOND FROM dat.polishing_ltq_end - dat.polishing_ltq_start))), 0) polishing_ltq,                                                     "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_wfp_end - dat.assembly_wfp_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.assembly_wfp_end - dat.assembly_wfp_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.assembly_wfp_end - dat.assembly_wfp_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.assembly_wfp_end - dat.assembly_wfp_start))), 0) assembly_wfp,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_stg_end - dat.assembly_stg_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.assembly_stg_end - dat.assembly_stg_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.assembly_stg_end - dat.assembly_stg_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.assembly_stg_end - dat.assembly_stg_start))), 0) assembly_stg,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_lt_end - dat.assembly_lt_start) * 24 * 60 * 60)+                                                                                     "+
					"                  (extract(HOUR FROM dat.assembly_lt_end - dat.assembly_lt_start) *  60 * 60) +                                                                               "+
					"                       (extract(MINUTE FROM dat.assembly_lt_end - dat.assembly_lt_start) *  60 ) +                                                                            "+
					"                               (extract(SECOND FROM dat.assembly_lt_end - dat.assembly_lt_start))), 0) assembly_lt,                                                           "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_idr_end - dat.assembly_idr_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.assembly_idr_end - dat.assembly_idr_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.assembly_idr_end - dat.assembly_idr_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.assembly_idr_end - dat.assembly_idr_start))), 0) assembly_idr,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_wfq_end - dat.assembly_wfq_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.assembly_wfq_end - dat.assembly_wfq_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.assembly_wfq_end - dat.assembly_wfq_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.assembly_wfq_end - dat.assembly_wfq_start))), 0) assembly_wfq,                                                        "+
					" NVL(CEIL((extract(DAY FROM dat.assembly_ltq_end - dat.assembly_ltq_start) * 24 * 60 * 60)+                                                                                   "+
					"                  (extract(HOUR FROM dat.assembly_ltq_end - dat.assembly_ltq_start) *  60 * 60) +                                                                             "+
					"                       (extract(MINUTE FROM dat.assembly_ltq_end - dat.assembly_ltq_start) *  60 ) +                                                                          "+
					"                               (extract(SECOND FROM dat.assembly_ltq_end - dat.assembly_ltq_start))), 0) assembly_ltq                                                         "+
					" from (                                                                                                                                                                       "+
					" select                                                                                                                                                                       "+
					" to_char(T401_TanggalWO, 'dd-mon-yyyy') datee,                                                                                                                                "+
					" M046_COLORMATCHING.M046_JMLPANEL jumlahpanel,                                                                                                                                "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,                        "+
					" M104_MODELNAME.M104_NAMAMODELNAME,                                                                                                                                           "+
					" case                                                                                                                                                                         "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%light%repair%' then 'LIGHT REPAIR'                                                                          "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%medium%' then 'MEDIUM'                                                                                      "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%heavy%repair%' then 'HEAVY REPAIR'                                                                          "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%minor%' then 'MINOR'                                                                                        "+
					"     when t401_reception.T401_StaTPSLine = '1' then 'TPS LINE'                                                                                                                "+
					"     when T183_HISTORYCUSTOMERVEHICLE.T183_staProject = '1' then 'PROJECT'                                                                                                    "+
					"     else 'not defined yet'                                                                                                                                                           "+
					" end jenis_pekerjaan,                                                                                                                                                         "+
					" t452_actual.T452_TglJam body_wfp_end,                                                                                                                                        "+
					" t401_reception.T401_TglJamAmbilWO body_wfp_start,                                                                                                                            "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                             "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end body_stg_end,                                                                                                                                                            "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                              "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end body_stg_start,                                                                                                                                                          "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                           "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end body_lt_end,                                                                                                                                                             "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                           "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end body_lt_start,                                                                                                                                                           "+
					" T506_TglJamSelesai body_idr_end,                                                                                                                                             "+
					" T506_TglJamMulai body_idr_start,                                                                                                                                             "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%'                                                                                                            "+
					"     then  T507_TglJamMulai                                                                                                                                                   "+
					"     else null                                                                                                                                                                "+
					" end body_wfq_end,                                                                                                                                                            "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                           "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end body_wfq_start,                                                                                                                                                          "+
					" T507_TglJamSelesai body_ltq_end,                                                                                                                                             "+
					" T507_TglJamMulai body_ltq_start,                                                                                                                                             "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                      "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end preparation_wfp_end,                                                                                                                                                     "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                              "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end preparation_wfp_start,                                                                                                                                                   "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                      "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end preparation_stg_end,                                                                                                                                                     "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end preparation_stg_start,                                                                                                                                                   "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                    "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end preparation_lt_end,                                                                                                                                                      "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                    "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end preparation_lt_start,                                                                                                                                                    "+
					" T506_TglJamSelesai preparation_idr_end,                                                                                                                                      "+
					" T506_TglJamMulai preparation_idr_start,                                                                                                                                      "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%'                                                                                                     "+
					"     then  T507_TglJamMulai                                                                                                                                                   "+
					"     else null                                                                                                                                                                "+
					" end preparation_wfq_end,                                                                                                                                                     "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                    "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end preparation_wfq_start,                                                                                                                                                   "+
					" T507_TglJamSelesai preparation_ltq_end,                                                                                                                                      "+
					" T507_TglJamMulai preparation_ltq_start,                                                                                                                                      "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                         "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end painting_wfp_end,                                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end painting_wfp_start,                                                                                                                                                      "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                         "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end painting_stg_end,                                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                          "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end painting_stg_start,                                                                                                                                                      "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end painting_lt_end,                                                                                                                                                         "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end painting_lt_start,                                                                                                                                                       "+
					" T506_TglJamSelesai painting_idr_end,                                                                                                                                         "+
					" T506_TglJamMulai painting_idr_start,                                                                                                                                         "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%'                                                                                                        "+
					"     then  T507_TglJamMulai                                                                                                                                                   "+
					"     else null                                                                                                                                                                "+
					" end painting_wfq_end,                                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end painting_wfq_start,                                                                                                                                                      "+
					" T507_TglJamSelesai painting_ltq_end,                                                                                                                                         "+
					" T507_TglJamMulai painting_ltq_start,                                                                                                                                         "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                        "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end polishing_wfp_end,                                                                                                                                                       "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                          "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end polishing_wfp_start,                                                                                                                                                     "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                        "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end polishing_stg_end,                                                                                                                                                       "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                         "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end polishing_stg_start,                                                                                                                                                     "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                      "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end polishing_lt_end,                                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                      "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end polishing_lt_start,                                                                                                                                                      "+
					" T506_TglJamSelesai polishing_idr_end,                                                                                                                                        "+
					" T506_TglJamMulai polishing_idr_start,                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%'                                                                                                       "+
					"     then  T507_TglJamMulai                                                                                                                                                   "+
					"     else null                                                                                                                                                                "+
					" end polishing_wfq_end,                                                                                                                                                       "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                      "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end polishing_wfq_start,                                                                                                                                                     "+
					" T507_TglJamSelesai polishing_ltq_end,                                                                                                                                        "+
					" T507_TglJamMulai polishing_ltq_start,                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                         "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end assembly_wfp_end,                                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                         "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end assembly_wfp_start,                                                                                                                                                      "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                         "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end assembly_stg_end,                                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                          "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end assembly_stg_start,                                                                                                                                                      "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end assembly_lt_end,                                                                                                                                                         "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end assembly_lt_start,                                                                                                                                                       "+
					" T506_TglJamSelesai assembly_idr_end,                                                                                                                                         "+
					" T506_TglJamMulai assembly_idr_start,                                                                                                                                         "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%'                                                                                                        "+
					"     then  T507_TglJamMulai                                                                                                                                                   "+
					"     else null                                                                                                                                                                "+
					" end assembly_wfq_end,                                                                                                                                                        "+
					" case                                                                                                                                                                         "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                       "+
					"     then T452_TglJam                                                                                                                                                         "+
					"     else null                                                                                                                                                                "+
					" end assembly_wfq_start,                                                                                                                                                      "+
					" T507_TglJamSelesai assembly_ltq_end,                                                                                                                                         "+
					" T507_TglJamMulai assembly_ltq_start                                                                                                                                          "+
					" from                                                                                                                                                                         "+
					" t401_reception                                                                                                                                                               "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                                                                                   "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID                                                                       "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID                                                                             "+
					" left join t110_fullmodelcode on T110_FULLMODELCODE.ID =  T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE                                                                 "+
					" left join m104_modelname on M104_MODELNAME.ID =  T110_FULLMODELCODE.T110_M104_ID                                                                                             "+
					" left join m401_tipekerusakan on M401_TIPEKERUSAKAN.ID = T401_RECEPTION.T401_M401_ID                                                                                          "+
					" left join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                                                                                    "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                                                                                "+
					" left join t452_actual on T452_ACTUAL.T452_T401_NOWO =  T401_RECEPTION.ID                                                                                                     "+
					" left join m452_statusactual on m452_statusactual.id =  T452_ACTUAL.T452_M452_ID                                                                                              "+
					" left join t506_idr on T506_IDR.T506_T401_NOWO =  t401_reception.id                                                                                                           "+
					" left join t507_inspectionqc on T507_INSPECTIONQC.T507_T401_NOWO = t401_reception.id                                                                                          "+
					" ) dat                                                                                                                                                                        "+
					" order by dat.jenis_pekerjaan, dat.datee                                                                                                                                      "+
					" ) dat                                                                                                                                                                        "+
					" group by dat.jenis_pekerjaan                                                                                                                                                 "+
					" ) dat                                                                                                                                                                        "+
					" group by dat.jenis_pekerjaan                                                                                                                                                 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesProductionRedoLeadTimeDetail (def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                                                                                                                  "+
					" dat.jenis_pekerjaan, dat.datee, dat.jumlahpanel, dat.nopol,  dat.m104_namamodelname,                                                                                    "+
					" NVL(CEIL((extract(DAY FROM dat.body_wfp_end - dat.body_wfp_start) * 24 * 60 * 60)+                                                                                      "+
					"                  (extract(HOUR FROM dat.body_wfp_end - dat.body_wfp_start) *  60 * 60) +                                                                                "+
					"                       (extract(MINUTE FROM dat.body_wfp_end - dat.body_wfp_start) *  60 ) +                                                                             "+
					"                               (extract(SECOND FROM dat.body_wfp_end - dat.body_wfp_start))), 0) body_wfp,                                                               "+
					" NVL(CEIL((extract(DAY FROM dat.body_stg_end - dat.body_stg_start) * 24 * 60 * 60)+                                                                                      "+
					"                  (extract(HOUR FROM dat.body_stg_end - dat.body_stg_start) *  60 * 60) +                                                                                "+
					"                       (extract(MINUTE FROM dat.body_stg_end - dat.body_stg_start) *  60 ) +                                                                             "+
					"                               (extract(SECOND FROM dat.body_stg_end - dat.body_stg_start))), 0) body_stg,                                                               "+
					" NVL(CEIL((extract(DAY FROM dat.body_lt_end - dat.body_lt_start) * 24 * 60 * 60)+                                                                                        "+
					"                  (extract(HOUR FROM dat.body_lt_end - dat.body_lt_start) *  60 * 60) +                                                                                  "+
					"                       (extract(MINUTE FROM dat.body_lt_end - dat.body_lt_start) *  60 ) +                                                                               "+
					"                               (extract(SECOND FROM dat.body_lt_end - dat.body_lt_start))), 0) body_lt,                                                                  "+
					" NVL(CEIL((extract(DAY FROM dat.body_idr_end - dat.body_idr_start) * 24 * 60 * 60)+                                                                                      "+
					"                  (extract(HOUR FROM dat.body_idr_end - dat.body_idr_start) *  60 * 60) +                                                                                "+
					"                       (extract(MINUTE FROM dat.body_idr_end - dat.body_idr_start) *  60 ) +                                                                             "+
					"                               (extract(SECOND FROM dat.body_idr_end - dat.body_idr_start))), 0) body_idr,                                                               "+
					" NVL(CEIL((extract(DAY FROM dat.body_wfq_end - dat.body_wfq_start) * 24 * 60 * 60)+                                                                                      "+
					"                  (extract(HOUR FROM dat.body_wfq_end - dat.body_wfq_start) *  60 * 60) +                                                                                "+
					"                       (extract(MINUTE FROM dat.body_wfq_end - dat.body_wfq_start) *  60 ) +                                                                             "+
					"                               (extract(SECOND FROM dat.body_wfq_end - dat.body_wfq_start))), 0) body_wfq,                                                               "+
					" NVL(CEIL((extract(DAY FROM dat.body_ltq_end - dat.body_ltq_start) * 24 * 60 * 60)+                                                                                      "+
					"                  (extract(HOUR FROM dat.body_ltq_end - dat.body_ltq_start) *  60 * 60) +                                                                                "+
					"                       (extract(MINUTE FROM dat.body_ltq_end - dat.body_ltq_start) *  60 ) +                                                                             "+
					"                               (extract(SECOND FROM dat.body_ltq_end - dat.body_ltq_start))), 0) body_ltq,                                                               "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_wfp_end - dat.preparation_wfp_start) * 24 * 60 * 60)+                                                                        "+
					"                  (extract(HOUR FROM dat.preparation_wfp_end - dat.preparation_wfp_start) *  60 * 60) +                                                                  "+
					"                       (extract(MINUTE FROM dat.preparation_wfp_end - dat.preparation_wfp_start) *  60 ) +                                                               "+
					"                               (extract(SECOND FROM dat.preparation_wfp_end - dat.preparation_wfp_start))), 0) preparation_wfp,                                          "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_stg_end - dat.preparation_stg_start) * 24 * 60 * 60)+                                                                        "+
					"                  (extract(HOUR FROM dat.preparation_stg_end - dat.preparation_stg_start) *  60 * 60) +                                                                  "+
					"                       (extract(MINUTE FROM dat.preparation_stg_end - dat.preparation_stg_start) *  60 ) +                                                               "+
					"                               (extract(SECOND FROM dat.preparation_stg_end - dat.preparation_stg_start))), 0) preparation_stg,                                          "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_lt_end - dat.preparation_lt_start) * 24 * 60 * 60)+                                                                          "+
					"                  (extract(HOUR FROM dat.preparation_lt_end - dat.preparation_lt_start) *  60 * 60) +                                                                    "+
					"                       (extract(MINUTE FROM dat.preparation_lt_end - dat.preparation_lt_start) *  60 ) +                                                                 "+
					"                               (extract(SECOND FROM dat.preparation_lt_end - dat.preparation_lt_start))), 0) preparation_lt,                                             "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_idr_end - dat.preparation_idr_start) * 24 * 60 * 60)+                                                                        "+
					"                  (extract(HOUR FROM dat.preparation_idr_end - dat.preparation_idr_start) *  60 * 60) +                                                                  "+
					"                       (extract(MINUTE FROM dat.preparation_idr_end - dat.preparation_idr_start) *  60 ) +                                                               "+
					"                               (extract(SECOND FROM dat.preparation_idr_end - dat.preparation_idr_start))), 0) preparation_idr,                                          "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_wfq_end - dat.preparation_wfq_start) * 24 * 60 * 60)+                                                                        "+
					"                  (extract(HOUR FROM dat.preparation_wfq_end - dat.preparation_wfq_start) *  60 * 60) +                                                                  "+
					"                       (extract(MINUTE FROM dat.preparation_wfq_end - dat.preparation_wfq_start) *  60 ) +                                                               "+
					"                               (extract(SECOND FROM dat.preparation_wfq_end - dat.preparation_wfq_start))), 0) preparation_wfq,                                          "+
					" NVL(CEIL((extract(DAY FROM dat.preparation_ltq_end - dat.preparation_ltq_start) * 24 * 60 * 60)+                                                                        "+
					"                  (extract(HOUR FROM dat.preparation_ltq_end - dat.preparation_ltq_start) *  60 * 60) +                                                                  "+
					"                       (extract(MINUTE FROM dat.preparation_ltq_end - dat.preparation_ltq_start) *  60 ) +                                                               "+
					"                               (extract(SECOND FROM dat.preparation_ltq_end - dat.preparation_ltq_start))), 0) preparation_ltq,                                          "+
					" NVL(CEIL((extract(DAY FROM dat.painting_wfp_end - dat.painting_wfp_start) * 24 * 60 * 60)+                                                                              "+
					"                  (extract(HOUR FROM dat.painting_wfp_end - dat.painting_wfp_start) *  60 * 60) +                                                                        "+
					"                       (extract(MINUTE FROM dat.painting_wfp_end - dat.painting_wfp_start) *  60 ) +                                                                     "+
					"                               (extract(SECOND FROM dat.painting_wfp_end - dat.painting_wfp_start))), 0) painting_wfp,                                                   "+
					" NVL(CEIL((extract(DAY FROM dat.painting_stg_end - dat.painting_stg_start) * 24 * 60 * 60)+                                                                              "+
					"                  (extract(HOUR FROM dat.painting_stg_end - dat.painting_stg_start) *  60 * 60) +                                                                        "+
					"                       (extract(MINUTE FROM dat.painting_stg_end - dat.painting_stg_start) *  60 ) +                                                                     "+
					"                               (extract(SECOND FROM dat.painting_stg_end - dat.painting_stg_start))), 0) painting_stg,                                                   "+
					" NVL(CEIL((extract(DAY FROM dat.painting_lt_end - dat.painting_lt_start) * 24 * 60 * 60)+                                                                                "+
					"                  (extract(HOUR FROM dat.painting_lt_end - dat.painting_lt_start) *  60 * 60) +                                                                          "+
					"                       (extract(MINUTE FROM dat.painting_lt_end - dat.painting_lt_start) *  60 ) +                                                                       "+
					"                               (extract(SECOND FROM dat.painting_lt_end - dat.painting_lt_start))), 0) painting_lt,                                                      "+
					" NVL(CEIL((extract(DAY FROM dat.painting_idr_end - dat.painting_idr_start) * 24 * 60 * 60)+                                                                              "+
					"                  (extract(HOUR FROM dat.painting_idr_end - dat.painting_idr_start) *  60 * 60) +                                                                        "+
					"                       (extract(MINUTE FROM dat.painting_idr_end - dat.painting_idr_start) *  60 ) +                                                                     "+
					"                               (extract(SECOND FROM dat.painting_idr_end - dat.painting_idr_start))), 0) painting_idr,                                                   "+
					" NVL(CEIL((extract(DAY FROM dat.painting_wfq_end - dat.painting_wfq_start) * 24 * 60 * 60)+                                                                              "+
					"                  (extract(HOUR FROM dat.painting_wfq_end - dat.painting_wfq_start) *  60 * 60) +                                                                        "+
					"                       (extract(MINUTE FROM dat.painting_wfq_end - dat.painting_wfq_start) *  60 ) +                                                                     "+
					"                               (extract(SECOND FROM dat.painting_wfq_end - dat.painting_wfq_start))), 0) painting_wfq,                                                   "+
					" NVL(CEIL((extract(DAY FROM dat.painting_ltq_end - dat.painting_ltq_start) * 24 * 60 * 60)+                                                                              "+
					"                  (extract(HOUR FROM dat.painting_ltq_end - dat.painting_ltq_start) *  60 * 60) +                                                                        "+
					"                       (extract(MINUTE FROM dat.painting_ltq_end - dat.painting_ltq_start) *  60 ) +                                                                     "+
					"                               (extract(SECOND FROM dat.painting_ltq_end - dat.painting_ltq_start))), 0) painting_ltq,                                                   "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_wfp_end - dat.polishing_wfp_start) * 24 * 60 * 60)+                                                                            "+
					"                  (extract(HOUR FROM dat.polishing_wfp_end - dat.polishing_wfp_start) *  60 * 60) +                                                                      "+
					"                       (extract(MINUTE FROM dat.polishing_wfp_end - dat.polishing_wfp_start) *  60 ) +                                                                   "+
					"                               (extract(SECOND FROM dat.polishing_wfp_end - dat.polishing_wfp_start))), 0) polishing_wfp,                                                "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_stg_end - dat.polishing_stg_start) * 24 * 60 * 60)+                                                                            "+
					"                  (extract(HOUR FROM dat.polishing_stg_end - dat.polishing_stg_start) *  60 * 60) +                                                                      "+
					"                       (extract(MINUTE FROM dat.polishing_stg_end - dat.polishing_stg_start) *  60 ) +                                                                   "+
					"                               (extract(SECOND FROM dat.polishing_stg_end - dat.polishing_stg_start))), 0) polishing_stg,                                                "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_lt_end - dat.polishing_lt_start) * 24 * 60 * 60)+                                                                              "+
					"                  (extract(HOUR FROM dat.polishing_lt_end - dat.polishing_lt_start) *  60 * 60) +                                                                        "+
					"                       (extract(MINUTE FROM dat.polishing_lt_end - dat.polishing_lt_start) *  60 ) +                                                                     "+
					"                               (extract(SECOND FROM dat.polishing_lt_end - dat.polishing_lt_start))), 0) polishing_lt,                                                   "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_idr_end - dat.polishing_idr_start) * 24 * 60 * 60)+                                                                            "+
					"                  (extract(HOUR FROM dat.polishing_idr_end - dat.polishing_idr_start) *  60 * 60) +                                                                      "+
					"                       (extract(MINUTE FROM dat.polishing_idr_end - dat.polishing_idr_start) *  60 ) +                                                                   "+
					"                               (extract(SECOND FROM dat.polishing_idr_end - dat.polishing_idr_start))), 0) polishing_idr,                                                "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_wfq_end - dat.polishing_wfq_start) * 24 * 60 * 60)+                                                                            "+
					"                  (extract(HOUR FROM dat.polishing_wfq_end - dat.polishing_wfq_start) *  60 * 60) +                                                                      "+
					"                       (extract(MINUTE FROM dat.polishing_wfq_end - dat.polishing_wfq_start) *  60 ) +                                                                   "+
					"                               (extract(SECOND FROM dat.polishing_wfq_end - dat.polishing_wfq_start))), 0) polishing_wfq,                                                "+
					" NVL(CEIL((extract(DAY FROM dat.polishing_ltq_end - dat.polishing_ltq_start) * 24 * 60 * 60)+                                                                            "+
					"                  (extract(HOUR FROM dat.polishing_ltq_end - dat.polishing_ltq_start) *  60 * 60) +                                                                      "+
					"                       (extract(MINUTE FROM dat.polishing_ltq_end - dat.polishing_ltq_start) *  60 ) +                                                                   "+
					"                               (extract(SECOND FROM dat.polishing_ltq_end - dat.polishing_ltq_start))), 0) polishing_ltq                                                 "+
					" from (                                                                                                                                                                  "+
					" select                                                                                                                                                                  "+
					" to_char(T401_TanggalWO, 'dd-mon-yyyy') datee,                                                                                                                           "+
					" M046_COLORMATCHING.M046_JMLPANEL jumlahpanel,                                                                                                                           "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,                   "+
					" M104_MODELNAME.M104_NAMAMODELNAME,                                                                                                                                      "+
					" case                                                                                                                                                                    "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%light%repair%' then 'LIGHT REPAIR'                                                                     "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%medium%' then 'MEDIUM'                                                                                 "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%heavy%repair%' then 'HEAVY REPAIR'                                                                     "+
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%minor%' then 'MINOR'                                                                                   "+
					"     when t401_reception.T401_StaTPSLine = '1' then 'TPS LINE'                                                                                                           "+
					"     when T183_HISTORYCUSTOMERVEHICLE.T183_staProject = '1' then 'PROJECT'                                                                                               "+
					"     else 'not defined yet'                                                                                                                                                      "+
					" end jenis_pekerjaan,                                                                                                                                                    "+
					" t452_actual.T452_TglJam body_wfp_end,                                                                                                                                   "+
					" t401_reception.T401_TglJamAmbilWO body_wfp_start,                                                                                                                       "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                        "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end body_stg_end,                                                                                                                                                       "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                         "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end body_stg_start,                                                                                                                                                     "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                      "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end body_lt_end,                                                                                                                                                        "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                      "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end body_lt_start,                                                                                                                                                      "+
					" T506_TglJamSelesai body_idr_end,                                                                                                                                        "+
					" T506_TglJamMulai body_idr_start,                                                                                                                                        "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%'                                                                                                       "+
					"     then  T507_TglJamMulai                                                                                                                                              "+
					"     else null                                                                                                                                                           "+
					" end body_wfq_end,                                                                                                                                                       "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                      "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end body_wfq_start,                                                                                                                                                     "+
					" T507_TglJamSelesai body_ltq_end,                                                                                                                                        "+
					" T507_TglJamMulai body_ltq_start,                                                                                                                                        "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                 "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end preparation_wfp_end,                                                                                                                                                "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                         "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end preparation_wfp_start,                                                                                                                                              "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                 "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end preparation_stg_end,                                                                                                                                                "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end preparation_stg_start,                                                                                                                                              "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                               "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end preparation_lt_end,                                                                                                                                                 "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                               "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end preparation_lt_start,                                                                                                                                               "+
					" T506_TglJamSelesai preparation_idr_end,                                                                                                                                 "+
					" T506_TglJamMulai preparation_idr_start,                                                                                                                                 "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%'                                                                                                "+
					"     then  T507_TglJamMulai                                                                                                                                              "+
					"     else null                                                                                                                                                           "+
					" end preparation_wfq_end,                                                                                                                                                "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                               "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end preparation_wfq_start,                                                                                                                                              "+
					" T507_TglJamSelesai preparation_ltq_end,                                                                                                                                 "+
					" T507_TglJamMulai preparation_ltq_start,                                                                                                                                 "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                    "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end painting_wfp_end,                                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%preparation%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end painting_wfp_start,                                                                                                                                                 "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                    "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end painting_stg_end,                                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                     "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end painting_stg_start,                                                                                                                                                 "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end painting_lt_end,                                                                                                                                                    "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end painting_lt_start,                                                                                                                                                  "+
					" T506_TglJamSelesai painting_idr_end,                                                                                                                                    "+
					" T506_TglJamMulai painting_idr_start,                                                                                                                                    "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%'                                                                                                   "+
					"     then  T507_TglJamMulai                                                                                                                                              "+
					"     else null                                                                                                                                                           "+
					" end painting_wfq_end,                                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end painting_wfq_start,                                                                                                                                                 "+
					" T507_TglJamSelesai painting_ltq_end,                                                                                                                                    "+
					" T507_TglJamMulai painting_ltq_start,                                                                                                                                    "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                   "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end polishing_wfp_end,                                                                                                                                                  "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%painting%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                     "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end polishing_wfp_start,                                                                                                                                                "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                   "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end polishing_stg_end,                                                                                                                                                  "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                    "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end polishing_stg_start,                                                                                                                                                "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                 "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end polishing_lt_end,                                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                 "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end polishing_lt_start,                                                                                                                                                 "+
					" T506_TglJamSelesai polishing_idr_end,                                                                                                                                   "+
					" T506_TglJamMulai polishing_idr_start,                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%'                                                                                                  "+
					"     then  T507_TglJamMulai                                                                                                                                              "+
					"     else null                                                                                                                                                           "+
					" end polishing_wfq_end,                                                                                                                                                  "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                 "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end polishing_wfq_start,                                                                                                                                                "+
					" T507_TglJamSelesai polishing_ltq_end,                                                                                                                                   "+
					" T507_TglJamMulai polishing_ltq_start,                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%o'                                    "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end assembly_wfp_end,                                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%polishing%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                    "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end assembly_wfp_start,                                                                                                                                                 "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                    "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end assembly_stg_end,                                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                     "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end assembly_stg_start,                                                                                                                                                 "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end assembly_lt_end,                                                                                                                                                    "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end assembly_lt_start,                                                                                                                                                  "+
					" T506_TglJamSelesai assembly_idr_end,                                                                                                                                    "+
					" T506_TglJamMulai assembly_idr_start,                                                                                                                                    "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%'                                                                                                   "+
					"     then  T507_TglJamMulai                                                                                                                                              "+
					"     else null                                                                                                                                                           "+
					" end assembly_wfq_end,                                                                                                                                                   "+
					" case                                                                                                                                                                    "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%assembly%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                  "+
					"     then T452_TglJam                                                                                                                                                    "+
					"     else null                                                                                                                                                           "+
					" end assembly_wfq_start,                                                                                                                                                 "+
					" T507_TglJamSelesai assembly_ltq_end,                                                                                                                                    "+
					" T507_TglJamMulai assembly_ltq_start                                                                                                                                     "+
					" from                                                                                                                                                                    "+
					" t401_reception                                                                                                                                                          "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                                                                              "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID                                                                  "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID                                                                        "+
					" left join t110_fullmodelcode on T110_FULLMODELCODE.ID =  T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE                                                            "+
					" left join m104_modelname on M104_MODELNAME.ID =  T110_FULLMODELCODE.T110_M104_ID                                                                                        "+
					" left join m401_tipekerusakan on M401_TIPEKERUSAKAN.ID = T401_RECEPTION.T401_M401_ID                                                                                     "+
					" left join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                                                                               "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                                                                           "+
					" left join t452_actual on T452_ACTUAL.T452_T401_NOWO =  T401_RECEPTION.ID                                                                                                "+
					" left join m452_statusactual on m452_statusactual.id =  T452_ACTUAL.T452_M452_ID                                                                                         "+
					" left join t506_idr on T506_IDR.T506_T401_NOWO =  t401_reception.id                                                                                                      "+
					" left join t507_inspectionqc on T507_INSPECTIONQC.T507_T401_NOWO = t401_reception.id                                                                                     "+
					" ) dat                                                                                                                                                                   "+
					" order by dat.jenis_pekerjaan, dat.datee                                                                                                                                 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25],
					column_26: resultRow[26],
					column_27: resultRow[27],
					column_28: resultRow[28]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesProductionLeadTimeDetail_2(def params, def type) {
		def namaProses = "";
		if(type == "03"){
			namaProses = "body";
		} else 
		if(type == "04"){
			namaProses = "preparation";
		} else
		if(type == "05"){
			namaProses = "painting";
		} else
		if(type == "06"){
			namaProses = "polishing";
		} else
		if(type == "07"){
			namaProses = "assembly";
		}
	
		final session = sessionFactory.currentSession
		String query =
					" select                                                                                                                                                                 "+
					" dat.datee, dat.nopol, dat.jenis_pekerjaan, dat.m104_namamodelname, dat.job_dispatch_date, dat.job_dispatch_time, dat.jumlahpanel,                                      "+
					" dat.body_start_date, dat.body_start_time, dat.body_pause_date, dat.body_pause_time, dat.body_resume_date, dat.body_resume_time,                                        "+
					" dat.body_finish_date, dat.body_finish_time, dat.idr_start_date, dat.idr_start_time, dat.idr_finish_date, dat.idr_finish_time,                                          "+
					" dat.qc_start_date, dat.qc_start_time, dat.qc_finish_date, dat.qc_finish_time,                                                                                          "+
					" NVL(CEIL((extract(DAY FROM body_wfp_end - body_wfp_start) * 24 * 60 * 60)+                                                                                             "+
					"                  (extract(HOUR FROM body_wfp_end - body_wfp_start) *  60 * 60) +                                                                                       "+
					"                       (extract(MINUTE FROM body_wfp_end - body_wfp_start) *  60 ) +                                                                                    "+
					"                               (extract(SECOND FROM body_wfp_end - body_wfp_start))), 0) body_wfp,                                                                      "+
					" NVL(CEIL((extract(DAY FROM body_lt_end - body_lt_start) * 24 * 60 * 60)+                                                                                               "+
					"                  (extract(HOUR FROM body_lt_end - body_lt_start) *  60 * 60) +                                                                                         "+
					"                       (extract(MINUTE FROM body_lt_end - body_lt_start) *  60 ) +                                                                                      "+
					"                               (extract(SECOND FROM body_lt_end - body_lt_start))), 0) body_lt,                                                                         "+
					" NVL(CEIL((extract(DAY FROM body_idr_end - body_idr_start) * 24 * 60 * 60)+                                                                                             "+
					"                  (extract(HOUR FROM body_idr_end - body_idr_start) *  60 * 60) +                                                                                       "+
					"                       (extract(MINUTE FROM body_idr_end - body_idr_start) *  60 ) +                                                                                    "+
					"                               (extract(SECOND FROM body_idr_end - body_idr_start))), 0) body_idr,                                                                      "+
					" NVL(CEIL((extract(DAY FROM body_stg_end - body_stg_start) * 24 * 60 * 60)+                                                                                             "+
					"                  (extract(HOUR FROM body_stg_end - body_stg_start) *  60 * 60) +                                                                                       "+
					"                       (extract(MINUTE FROM body_stg_end - body_stg_start) *  60 ) +                                                                                    "+
					"                               (extract(SECOND FROM body_stg_end - body_stg_start))), 0) body_stg,                                                                      "+
					" NVL(CEIL((extract(DAY FROM body_wfq_end - body_wfq_start) * 24 * 60 * 60)+                                                                                             "+
					"                  (extract(HOUR FROM body_wfq_end - body_wfq_start) *  60 * 60) +                                                                                       "+
					"                       (extract(MINUTE FROM body_wfq_end - body_wfq_start) *  60 ) +                                                                                    "+
					"                               (extract(SECOND FROM body_wfq_end - body_wfq_start))), 0) body_wfq,                                                                      "+
					" NVL(CEIL((extract(DAY FROM body_ltq_end - body_ltq_start) * 24 * 60 * 60)+                                                                                             "+
					"                  (extract(HOUR FROM body_ltq_end - body_ltq_start) *  60 * 60) +                                                                                       "+
					"                       (extract(MINUTE FROM body_ltq_end - body_ltq_start) *  60 ) +                                                                                    "+
					"                               (extract(SECOND FROM body_ltq_end - body_ltq_start))), 0) body_ltq                                                                       "+
					" from (                                                                                                                                                                 "+
					" select                                                                                                                                                                 "+
					" M190_NAMAPROSESBP.M190_NAMAPROSESBP namaproses, 																														 "+
					" to_char(T401_TanggalWO, 'dd-mon-yyyy') datee,                                                                                                                          "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL,                  "+
					" case                                                                                                                                                                   "+ 
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%light%repair%' then 'LIGHT REPAIR'                                                                    "+ 
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%medium%' then 'MEDIUM'                                                                                "+ 
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%heavy%repair%' then 'HEAVY REPAIR'                                                                    "+ 
					"     when lower(M401_TIPEKERUSAKAN.M401_NAMATIPEKERUSAKAN) like '%minor%' then 'MINOR'                                                                                  "+
					"     when t401_reception.T401_StaTPSLine = '1' then 'TPS LINE'                                                                                                          "+ 
					"     when T183_HISTORYCUSTOMERVEHICLE.T183_staProject = '1' then 'PROJECT'                                                                                              "+ 
					"     else 'not defined yet'                                                                                                                                                     "+ 
					" end jenis_pekerjaan,                                                                                                                                                   "+
					" M104_MODELNAME.M104_NAMAMODELNAME,                                                                                                                                     "+
					" to_char(T401_TglJamAmbilWO, 'dd-mon-yyyy') job_dispatch_date,                                                                                                          "+
					" to_char(T401_TglJamAmbilWO, 'hh24:mi') job_dispatch_time,                                                                                                              "+
					" M046_COLORMATCHING.M046_JMLPANEL jumlahpanel,                                                                                                                          "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%start%' then to_char(T452_TglJam, 'dd-mon-yyyy')                                                            "+
					"     else null                                                                                                                                                          "+
					" end body_start_date,                                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%start%' then to_char(T452_TglJam, 'hh24:mi')                                                                "+
					"     else null                                                                                                                                                          "+
					" end body_start_time,                                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%' then to_char(T452_TglJam, 'dd-mon-yyyy')                                                            "+
					"     else null                                                                                                                                                          "+
					" end body_pause_date,                                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%' then to_char(T452_TglJam, 'hh24:mi')                                                                "+
					"     else null                                                                                                                                                          "+
					" end body_pause_time,                                                                                                                                                   "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%' then to_char(T452_TglJam, 'dd-mon-yyyy')                                                           "+
					"     else null                                                                                                                                                          "+
					" end body_resume_date,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%' then to_char(T452_TglJam, 'hh24:mi')                                                               "+
					"     else null                                                                                                                                                          "+
					" end body_resume_time,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%finish%' then to_char(T452_TglJam, 'dd-mon-yyyy')                                                           "+
					"     else null                                                                                                                                                          "+
					" end body_finish_date,                                                                                                                                                  "+
					" case                                                                                                                                                                   "+
					"     when lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%finish%' then to_char(T452_TglJam, 'hh24:mi')                                                               "+
					"     else null                                                                                                                                                          "+
					" end body_finish_time,                                                                                                                                                  "+
					" to_char(T506_TglJamMulai, 'dd-mon-yyyy') idr_start_date,                                                                                                               "+
					" to_char(T506_TglJamMulai, 'hh24:mi') idr_start_time,                                                                                                                   "+
					" to_char(T506_TglJamSelesai, 'dd-mon-yyyy') idr_finish_date,                                                                                                            "+
					" to_char(T506_TglJamSelesai, 'hh24:mi') idr_finish_time,                                                                                                                "+
					" to_char(T507_TglJamMulai, 'dd-mon-yyyy') qc_start_date,                                                                                                                "+
					" to_char(T507_TglJamMulai, 'hh24:mi') qc_start_time,                                                                                                                    "+
					" to_char(T507_TglJamSelesai, 'dd-mon-yyyy') qc_finish_date,                                                                                                             "+
					" to_char(T507_TglJamSelesai, 'hh24:mi') qc_finish_time,                                                                                                                 "+
					" t452_actual.T452_TglJam body_wfp_end,                                                                                                                                  "+
					" t401_reception.T401_TglJamAmbilWO body_wfp_start,                                                                                                                      "+
					" case                                                                                                                                                                   "+ 
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                     "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_lt_end,                                                                                                                                                       "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%on%'                                     "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_lt_start,                                                                                                                                                     "+
					" T506_TglJamSelesai body_idr_end,                                                                                                                                       "+
					" T506_TglJamMulai body_idr_start,                                                                                                                                       "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%resume%'                                       "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_stg_end,                                                                                                                                                      "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%pause%'                                        "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null    datatablesRedoBodyDailyUnitDetail                                                                                                                                                      "+
					" end body_stg_start,                                                                                                                                                    "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%'                                                                                                      "+
					"     then  T507_TglJamMulai                                                                                                                                             "+
					"     else null                                                                                                                                                          "+
					" end body_wfq_end,                                                                                                                                                      "+
					" case                                                                                                                                                                   "+
					"     when lower(M190_NAMAPROSESBP.M190_NAMAPROSESBP) like '%body%' and lower(M452_STATUSACTUAL.M452_STATUSACTUAL) like '%clock%of%'                                     "+
					"     then T452_TglJam                                                                                                                                                   "+
					"     else null                                                                                                                                                          "+
					" end body_wfq_start,                                                                                                                                                    "+
					" T507_TglJamSelesai body_ltq_end,                                                                                                                                       "+
					" T507_TglJamMulai body_ltq_start                                                                                                                                        "+
					" from                                                                                                                                                                   "+
					" t401_reception                                                                                                                                                         "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID                                                                 "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID                                                                       "+
					" left join m401_tipekerusakan on M401_TIPEKERUSAKAN.ID = T401_RECEPTION.T401_M401_ID                                                                                    "+
					" left join t110_fullmodelcode on T110_FULLMODELCODE.ID =  T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE                                                           "+
					" left join m104_modelname on M104_MODELNAME.ID =  T110_FULLMODELCODE.T110_M104_ID                                                                                       "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                                                                             "+
					" left join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                                                                              "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                                                                          "+
					" left join t452_actual on T452_ACTUAL.T452_T401_NOWO =  T401_RECEPTION.ID                                                                                               "+
					" left join m452_statusactual on m452_statusactual.id =  T452_ACTUAL.T452_M452_ID                                                                                        "+
					" left join t506_idr on T506_IDR.T506_T401_NOWO =  t401_reception.id                                                                                                     "+
					" left join t507_inspectionqc on T507_INSPECTIONQC.T507_T401_NOWO = t401_reception.id                                                                                    "+
					" ) dat                                                                                                                                                                  "+
					/*" where dat.namaproses like '% "+namaProses+" %' 																														 "+*/
					" order by dat.datee                                                                                                                                                     "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25],
					column_26: resultRow[26],
					column_27: resultRow[27],
					column_28: resultRow[28]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoBodyDailyUnitPieChart(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select 					                                                                                     "+
					" sum(dat.redo) redo, sum(dat.no_redo) no_redo                                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t506_idr                                                                                                       "+
					" inner join t401_reception on t401_reception.id =  t506_idr.t506_t401_nowo                                      "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T506_IDR.T506_M053_JOBID_REDO                                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t507_inspectionqc                                                                                              "+
					" inner join t401_reception on t401_reception.id =  t507_inspectionqc.t507_t401_nowo                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T507_INSPECTIONQC.T507_M053_JOBID_REDO                        "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t508_finalinspection                                                                                           "+
					" inner join t401_reception on t401_reception.id =  t508_finalinspection.t508_t401_nowo                          "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T508_FINALINSPECTION.T508_M053_JOBID                          "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" ) dat                                                                                                          "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoBodyDailyUnitPieChartTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select 					                                                                                     "+
					" sum(dat.redo) redo, sum(dat.no_redo) no_redo                                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t506_idr                                                                                                       "+
					" inner join t401_reception on t401_reception.id =  t506_idr.t506_t401_nowo                                      "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T506_IDR.T506_M053_JOBID_REDO                                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t507_inspectionqc                                                                                              "+
					" inner join t401_reception on t401_reception.id =  t507_inspectionqc.t507_t401_nowo                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T507_INSPECTIONQC.T507_M053_JOBID_REDO                        "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t508_finalinspection                                                                                           "+
					" inner join t401_reception on t401_reception.id =  t508_finalinspection.t508_t401_nowo                          "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T508_FINALINSPECTION.T508_M053_JOBID                          "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" ) dat                                                                                                          "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]					
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoBodyDailyUnitGrafik(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%'					   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results		
	}
	
	def datatablesRedoBodyDailyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%'					   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoBodyWeeklyUnitGrafikTable(def params){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'W'), "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoBodyMonthlyUnitGrafikTable(def params){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'mon yyyy'), 									   "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoBodyYearlyUnitGrafikTable(def params){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'yyyy'), 									   "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoBodyDailyUnitDetail(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                                                           "+
					" to_char(T401_TanggalWO, 'dd-mon-yyyy') datee,                                                                    "+
					" M116_KODEKOTANOPOL.M116_ID || ' '||T183_NopolTengah|| ' ' ||T183_NopolBelakang nopol,                            "+
					" nvl(M046_COLORMATCHING.M046_JMLPANEL, 0) jumlahpanel,                                                            "+
					" case                                                                                                             "+
                    "     when   T506_IDR.t506_staredo = '1' or T508_FINALINSPECTION.T508_STAREDO = '1'                                "+
                    "     then   M046_COLORMATCHING.M046_JMLPANEL                                                                      "+
                    "     else 0                                                                                                       "+
					" end jumlahpanelredo,					                                                                           "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%welding%nd%' then 1                                           "+
					" 	else 0                                                                                                         "+
					" end weldingnd,                                                                                                   "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%panel%shrinkage%' then 1                                      "+
					" 	else 0                                                                                                         "+
					" end panelshrinkage,                                                                                              "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%body%dimension%' then 1                                       "+
					" 	else 0                                                                                                         "+
					" end bodydimension,                                                                                               "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%kaca%bocor%' then 1                                           "+
					" 	else 0                                                                                                         "+
					" end kacabocor,                                                                                                   "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%kaca%tergores%' then 1                                        "+
					" 	else 0                                                                                                         "+
					" end kacatergores,                                                                                                "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%feather%edging%' then 1                                       "+
					" 	else 0                                                                                                         "+
					" end featheredging,                                                                                               "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%clearance%ng%' then 1                                         "+
					" 	else 0                                                                                                         "+
					" end clearanceng,                                                                                                 "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%levelling%ng%' then 1                                         "+
					" 	else 0                                                                                                         "+
					" end levellingng                                                                                                  "+
					" from                                                                                                             "+
					" t401_reception                                                                                                   "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID           "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                       "+
					" left join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_T401_NOWO = T401_RECEPTION.ID                                  "+
					" left join m503_deffect on M503_DEFFECT.ID = T503_JOBDEFFECT.T503_M503_ID                                         "+
					" left join T506_IDR on T506_IDR.T506_T401_NOWO = t401_reception.id                                                "+
                    " left join T508_FinalInspection on T508_FINALINSPECTION.T508_T401_NOWO =  t401_reception.id                       "+
                    " left join T507_InspectionQC on T507_INSPECTIONQC.T507_T401_NOWO =  t401_reception.id                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%body%'													   "+
					" order by T401_TanggalWO asc 																					   "+
					"";                                                                                                                
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesCombibothDailyGrafik(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesCombibothDailyGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesCombibothDailyStall1Grafik(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesCombibothDailyStall1GrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesCombibothDailyStall2Grafik(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesCombibothDailyStall2GrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesCombibothDailyDetail(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesOnTimeInEachProcessGrafik(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesOnTimeInEachProcessSummary(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesOnTimeInEachProcessDetail(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesTechnicianProductivityDetail(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesTechnicianProductivitySummary(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesTechnicianProductivityGrafik(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}	
	
	def datatablesRedoPreparationDailyUnitPieChart(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select 					                                                                                     "+
					" sum(dat.redo) redo, sum(dat.no_redo) no_redo                                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t506_idr                                                                                                       "+
					" inner join t401_reception on t401_reception.id =  t506_idr.t506_t401_nowo                                      "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T506_IDR.T506_M053_JOBID_REDO                                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t507_inspectionqc                                                                                              "+
					" inner join t401_reception on t401_reception.id =  t507_inspectionqc.t507_t401_nowo                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T507_INSPECTIONQC.T507_M053_JOBID_REDO                        "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t508_finalinspection                                                                                           "+
					" inner join t401_reception on t401_reception.id =  t508_finalinspection.t508_t401_nowo                          "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T508_FINALINSPECTION.T508_M053_JOBID                          "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" ) dat                                                                                                          "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPreparationDailyUnitPieChartTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select 					                                                                                     "+
					" sum(dat.redo) redo, sum(dat.no_redo) no_redo                                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t506_idr                                                                                                       "+
					" inner join t401_reception on t401_reception.id =  t506_idr.t506_t401_nowo                                      "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T506_IDR.T506_M053_JOBID_REDO                                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t507_inspectionqc                                                                                              "+
					" inner join t401_reception on t401_reception.id =  t507_inspectionqc.t507_t401_nowo                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T507_INSPECTIONQC.T507_M053_JOBID_REDO                        "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t508_finalinspection                                                                                           "+
					" inner join t401_reception on t401_reception.id =  t508_finalinspection.t508_t401_nowo                          "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T508_FINALINSPECTION.T508_M053_JOBID                          "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" ) dat                                                                                                          "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]					
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPreparationDailyUnitGrafik(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results		
	}
	
	def datatablesRedoPreparationDailyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results			
	}
	
	def datatablesRedoPreparationWeeklyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'W'), 									   "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPreparationMonthlyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'mon-yyyy'), 							   "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPreparationYearlyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'yyyy'), 							   "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPreparationDailyUnitGrafikTableDetail(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                                                           "+
					" to_char(T401_TanggalWO, 'dd-mon-yyyy') datee,                                                                    "+
					" M116_KODEKOTANOPOL.M116_ID || ' '||T183_NopolTengah|| ' ' ||T183_NopolBelakang nopol,                            "+
					" nvl(M046_COLORMATCHING.M046_JMLPANEL, 0) jumlahpanel,                                                            "+
					" case                                                                                                             "+
                    "     when   T506_IDR.t506_staredo = '1' or T508_FINALINSPECTION.T508_STAREDO = '1'                                "+
                    "     then   M046_COLORMATCHING.M046_JMLPANEL                                                                      "+
                    "     else 0                                                                                                       "+
					" end jumlahpanelredo,					                                                                           "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%puty%mark%' then 1                                           "+
					" 	else 0                                                                                                         "+
					" end putymark,                                                                                                   "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%pin%hole%' then 1                                      	   "+
					" 	else 0                                                                                                         "+
					" end pinhole,                                                                                              "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%cracking%' then 1                                       	   "+
					" 	else 0                                                                                                         "+
					" end cracking,                                                                                               "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%sanding%mark%' then 1                                           "+
					" 	else 0                                                                                                         "+
					" end sandingmark,                                                                                                   "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%gelombang%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end gelombang                                                                                                "+					
					" from                                                                                                             "+
					" t401_reception                                                                                                   "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID           "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                       "+
					" left join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_T401_NOWO = T401_RECEPTION.ID                                  "+
					" left join m503_deffect on M503_DEFFECT.ID = T503_JOBDEFFECT.T503_M503_ID                                         "+
					" left join T506_IDR on T506_IDR.T506_T401_NOWO = t401_reception.id                                                "+
                    " left join T508_FinalInspection on T508_FINALINSPECTION.T508_T401_NOWO =  t401_reception.id                       "+
                    " left join T507_InspectionQC on T507_INSPECTIONQC.T507_T401_NOWO =  t401_reception.id                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%preparation%'											   "+
					" order by T401_TanggalWO asc 																					   "+
					"";                                                                                                                
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPaintingDailyUnitPieChart(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select 					                                                                                     "+
					" sum(dat.redo) redo, sum(dat.no_redo) no_redo                                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t506_idr                                                                                                       "+
					" inner join t401_reception on t401_reception.id =  t506_idr.t506_t401_nowo                                      "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T506_IDR.T506_M053_JOBID_REDO                                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t507_inspectionqc                                                                                              "+
					" inner join t401_reception on t401_reception.id =  t507_inspectionqc.t507_t401_nowo                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T507_INSPECTIONQC.T507_M053_JOBID_REDO                        "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t508_finalinspection                                                                                           "+
					" inner join t401_reception on t401_reception.id =  t508_finalinspection.t508_t401_nowo                          "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID 									 "+
					" left join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID 								 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%' 												 "+
					" left join M053_operation on M053_OPERATION.ID =  T508_FINALINSPECTION.T508_M053_JOBID                          "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" ) dat                                                                                                          "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPaintingDailyUnitPieChartTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select 					                                                                                     "+
					" sum(dat.redo) redo, sum(dat.no_redo) no_redo                                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t506_idr                                                                                                       "+
					" inner join t401_reception on t401_reception.id =  t506_idr.t506_t401_nowo                                      "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T506_IDR.T506_M053_JOBID_REDO                                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t507_inspectionqc                                                                                              "+
					" inner join t401_reception on t401_reception.id =  t507_inspectionqc.t507_t401_nowo                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T507_INSPECTIONQC.T507_M053_JOBID_REDO                        "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" union all                                                                                                      "+
					" select                                                                                                         "+
					" nvl(sum(dat.redo), 0) redo, nvl(sum(dat.no_redo), 0) no_redo                                                   "+
					" from (                                                                                                         "+
					" select                                                                                                         "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then M046_COLORMATCHING.M046_JMLPANEL                                "+
					" 	else 0                                                                                                       "+
					" end redo,                                                                                                      "+
					" case                                                                                                           "+
					" 	when M053_OPERATION.M053_ID is not null then 0                                                               "+
					" 	else M046_COLORMATCHING.M046_JMLPANEL                                                                        "+
					" end no_redo                                                                                                    "+
					" from                                                                                                           "+
					" t508_finalinspection                                                                                           "+
					" inner join t401_reception on t401_reception.id =  t508_finalinspection.t508_t401_nowo                          "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                     "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                 "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'													 "+
					" left join M053_operation on M053_OPERATION.ID =  T508_FINALINSPECTION.T508_M053_JOBID                          "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                     "+
					" ) dat                                                                                                          "+
					" ) dat                                                                                                          "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]					
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	
	def datatablesRedoPaintingDailyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesRedoPaintingDailyUnitGrafikTableDetail(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                                                           "+
					" to_char(T401_TanggalWO, 'dd-mon-yyyy') datee,                                                                    "+
					" M116_KODEKOTANOPOL.M116_ID || ' '||T183_NopolTengah|| ' ' ||T183_NopolBelakang nopol,                            "+
					" nvl(M046_COLORMATCHING.M046_JMLPANEL, 0) jumlahpanel,                                                            "+
					" case                                                                                                             "+
                    "     when   T506_IDR.t506_staredo = '1' or T508_FINALINSPECTION.T508_STAREDO = '1'                                "+
                    "     then   M046_COLORMATCHING.M046_JMLPANEL                                                                      "+
                    "     else 0                                                                                                       "+
					" end jumlahpanelredo,					                                                                           "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%beda%warna%' then 1                                           "+
					" 	else 0                                                                                                         "+
					" end bedawarna,                                                                                                   "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%poor%hidin%' then 1                                      	   "+
					" 	else 0                                                                                                         "+
					" end poorhidin,                                                                                              "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%runs%' then 1                                       	   "+
					" 	else 0                                                                                                         "+
					" end runs,                                                                                               "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%fish%eye%' then 1                                           "+
					" 	else 0                                                                                                         "+
					" end fisheye,                                                                                                   "+
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%basecoat%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end basecoat,                                                                                                "+					
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%clearcoat%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end clearcoat,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%blisters%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end blisters,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%lifting%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end litfting,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%orange%peel%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end orange_peel,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%mothling%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end mothling,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%low%glass%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end lowglass,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%shrinkage%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end shrinkage,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%seeds%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end seeds,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%water%spooting%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end water_spooting,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%clouding%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end clouding,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%popping%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end popping,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%over%spary%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end over_spray,                                                                                                "+	
					" case                                                                                                             "+
					" 	when lower(M503_DEFFECT.M503_NAMADEFFECT) like '%absord%paper%' then 1                                        	   "+
					" 	else 0                                                                                                         "+
					" end absorb_paper                                                                                                "+	
					" from                                                                                                             "+
					" t401_reception                                                                                                   "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID           "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID                 "+
					" left join m046_colormatching on M046_COLORMATCHING.ID =  T401_RECEPTION.T401_M046_JMLPANEL                       "+
					" left join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_T401_NOWO = T401_RECEPTION.ID                                  "+
					" left join m503_deffect on M503_DEFFECT.ID = T503_JOBDEFFECT.T503_M503_ID                                         "+
					" left join T506_IDR on T506_IDR.T506_T401_NOWO = t401_reception.id                                                "+
                    " left join T508_FinalInspection on T508_FINALINSPECTION.T508_T401_NOWO =  t401_reception.id                       "+
                    " left join T507_InspectionQC on T507_INSPECTIONQC.T507_T401_NOWO =  t401_reception.id                             "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID                                       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID                                   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'											   "+
					" order by T401_TanggalWO asc 																					   "+
					"";                                                                                                                
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesRedoPaintingWeeklyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'W') created_date,                         "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesRedoPaintingMonthlyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'mon-yyyy') created_date,                  "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesRedoPaintingYearlyUnitGrafikTable(def params, def type){
		final session = sessionFactory.currentSession
		String query =
					" select                                                                           "+
					" to_char(T503_JOBDEFFECT.date_created, 'yyyy') created_date,                  "+
					" M503_DEFFECT.M503_NAMADEFFECT namadeffect,                                       "+
					" count(T503_JOBDEFFECT.ID) total                                                  "+
					" from                                                                             "+
					" m503_deffect                                                                     "+
					" inner join T503_JOBDEFFECT on T503_JOBDEFFECT.T503_M503_ID = M503_DEFFECT.ID     "+
					" inner join t401_reception on t401_reception.id = T503_JOBDEFFECT.t503_t401_nowo  "+
					" inner join m022_stall on M022_STALL.ID =  T401_RECEPTION.T401_M022_STALLID       "+
					" inner join M190_NamaProsesBP on M190_NAMAPROSESBP.ID = M022_STALL.M022_M190_ID   "+
					" and lower(M190_NamaProsesBP.M190_NamaProsesBP) like '%painting%'			   "+
					" group by M503_DEFFECT.M503_NAMADEFFECT                                           "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
}
 
