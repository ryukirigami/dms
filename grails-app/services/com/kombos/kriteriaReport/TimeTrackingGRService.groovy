package com.kombos.kriteriaReport

import com.kombos.administrasi.BaseModel
import com.kombos.administrasi.KategoriJob
import com.kombos.administrasi.ModelName
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.maintable.ActualCuci
import com.kombos.maintable.AntriCuci
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.JOCTECO
import com.kombos.maintable.Settlement
import com.kombos.maintable.StatusActual
import com.kombos.maintable.StatusKendaraan
import com.kombos.parts.Invoice
import com.kombos.parts.StatusApproval
import com.kombos.production.FinalInspection
import com.kombos.production.IDR
import com.kombos.production.Masalah
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.Prediagnosis
import groovy.time.TimeCategory
import groovy.time.TimeDuration

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class TimeTrackingGRService {
    boolean transactional = false

    def sessionFactory

    def findJenisPekerjaan(def jenisPekerjaan){
        String hasil = ""
        def arr = ((jenisPekerjaan as String).split(","))
        arr.each {
            hasil  += KategoriJob.findById(it as Long).m055KategoriJob + ", "
        }
        hasil = hasil.substring(0, hasil.length()-2)
        return hasil

    }

    def findJenisKendaraan(def jenisKendaraan){
        String hasil = ""
        def arr = ((jenisKendaraan as String).split(","))
        arr.each {
            def bm = BaseModel.findById(it as Long)
            hasil  += bm.m102NamaBaseModel+"("+ bm.m102KodeBaseModel+")" + ", "
        }
        hasil = hasil.substring(0, hasil.length()-2)
        return hasil

    }

    def findJobStopped(def jobStopped){
        String hasil = ""
        def arr = ((jobStopped as String).split(","))
        arr.each {
            def bm = StatusKendaraan.findById(it as Long)
            hasil  += bm.m999NamaStatusKendaraan + ", "
        }
        hasil = hasil.substring(0, hasil.length()-2)
        return hasil

    }

    def findTeknisiorSa(def teknisi){
        String hasil = ""
        def arr = ((teknisi as String).split(","))
        arr.each {
            def bm = NamaManPower.findById(it as Long)
            hasil  += bm.t015NamaLengkap + ", "
        }
        hasil = hasil.substring(0, hasil.length()-2)
        return hasil

    }

    def datatablesKendaraanList(def params){

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = BaseModel.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")

            if (params."m102NamaBaseModel") {
                ilike("m102NamaBaseModel", "%" + (params."m102NamaBaseModel" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m102NamaBaseModel: it.m102NamaBaseModel+"-"+it.m102KodeBaseModel

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret

    }

    def datatablesTechnisianList(def params){

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NamaManPower.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                companyDealer{
                    eq("id",params.workshop as Long)
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if (params."teknisi") {
                ilike("t015NamaBoard", "%" + (params."teknisi" as String) + "%")
            }

            manPowerDetail{
                ilike("m015LevelManPower","%Technician%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    teknisi: it.t015NamaBoard+' - '+it?.userProfile?.t001Inisial

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret

    }

    def datatablesSAList(def params){

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NamaManPower.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                companyDealer{
                    eq("id",params.workshop as Long)
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if (params."sa") {
                ilike("t015NamaBoard", "%" + (params."sa" as String) + "%")
            }

            manPowerDetail{
                ilike("m015LevelManPower","%Service Advisor%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t015NamaLengkap: it.t015NamaLengkap.toUpperCase()

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret

    }

    def datatablesStoppedList(def params){

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = StatusKendaraan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m999NamaStatusKendaraan: it.m999NamaStatusKendaraan

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret

    }

    def datatablesJenisPekerjaan(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = KategoriJob.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            inList("m055KategoriJob",["GR","SBI","SBE","PDS"])
            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m055KategoriJob: it.m055KategoriJob

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        return ret
    }

    def datatablesTimeTrackingGR(def params){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        def jenisPekerjaan = ((params.jenisPekerjaan as String).split(","))
        def jenisKendaraan = ((params.jenisKendaraan as String).split(","))
        def teknisi = ((params.teknisi as String).split(","))
        def sa = ((params.sa as String).split(","))

        def arrSA = []
        sa.each {
            if(it!=""){
                arrSA.add(it as Long)
            }
        }
        def arrJenisKendaraan = []
        jenisKendaraan.each {
            if(it!=""){
                arrJenisKendaraan.add(it as Long)
            }
        }

        def arrTeknisi = []
        teknisi.each {
            if(it!=""){
                arrTeknisi.add(it as Long)
            }
        }

        def arrJenisPekerjaan = []
        jenisPekerjaan.each {
            if(it!=""){
                arrJenisPekerjaan.add(it as Long)
            }
        }
        def jobStopped = ((params.jobStopped as String).split(","))
        // sa
        // jobStopped
        def row = []
        def c = Reception.createCriteria()
        def dataReception = c.list {
            if(arrJenisKendaraan.size()>0){
                historyCustomerVehicle{
                    fullModelCode{
                        baseModel{
                            inList("id",arrJenisKendaraan)
                        }
                    }
                }
            }
            eq("staDel","0")
            eq("staSave","0")
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","GR")
                }
            }
            companyDealer{
                eq("id", params.workshop as Long)
            }
            if(params.tanggal && params.tanggal2){
                ge("t401TanggalWO", startDate)
                lt("t401TanggalWO", endDate + 1)
            }

            order("lastUpdated","asc")
        }
        dataReception.each { rec ->
            def tampilTeknisi = false,tampilSA = false,tampilJobStopped = false,tampilPekerjaan = false
            def kategoriJob = JobRCP.findByReception(rec)?.operation?.kategoriJob?.m055KategoriJob

            def jobRcp = JobRCP.createCriteria().list {
                reception{
                    eq("id",rec.id)
                }
                ge("lastUpdated", startDate)
                lt("lastUpdated", endDate + 1)
                eq("staDel","0")
                or{
                    eq("t402StaTambahKurang","0")
                    isNull("t402StaTambahKurang")
                }
            }

            def prediagnose = Prediagnosis.findByReceptionAndStaDel(rec,"0")
            def finalInspection = FinalInspection.findByReception(rec)
            def namaTeknisi = JPB.findAllByReceptionAndStaDel(rec,"0").namaManPower?.t015NamaBoard
            def inspect = IDR.findByReception(rec)
            def joc = JOCTECO.findByReceptionAndStaDel(rec,"0")
            def invoicing = InvoiceT701.findByReceptionAndT701StaDel(rec,"0")
            def settlement = Settlement.findByReceptionAndStaDel(rec,"0")
            def production = Actual.createCriteria().list {
                eq("staDel","0")
                reception{
                    eq("id",rec?.id)
                }
                or{
                    eq("statusActual",StatusActual.findById(1 as Long))
                    eq("statusActual",StatusActual.findById(4 as Long))
                }
                order("t452TglJam","asc")
            }

            def jobStop = Actual.createCriteria().list {
                eq("staDel","0")
                reception{
                    eq("id",rec?.id)
                }
                or{
                    eq("statusActual",StatusActual.findById(2 as Long))
                    eq("statusActual",StatusActual.findById(5 as Long))
                    eq("statusActual",StatusActual.findById(6 as Long))
                }
                order("t452TglJam","asc")
            }
            def masalah = Masalah.createCriteria().list {
                actual{
                    eq("staDel","0")
                    reception{
                        eq("id",rec?.id)
                    }
                    or{
                        eq("statusActual",StatusActual.findById(2 as Long))
                        eq("statusActual",StatusActual.findById(5 as Long))
                        eq("statusActual",StatusActual.findById(6 as Long))
                    }
                    order("t452TglJam","asc")
                }
            }
            def jobContinue = Actual.createCriteria().list {
                eq("staDel","0")
                reception{
                    eq("id",rec?.id)
                }
                eq("statusActual",StatusActual.findById(3 as Long))
                order("t452TglJam","asc")
            }

            def washing = ActualCuci.createCriteria().list {
                antriCuciID{
                    reception{
                        eq("id",rec?.id)
                    }
                }
                or{
                    eq("statusActual",StatusActual.findById(1 as Long))
                    eq("statusActual",StatusActual.findById(4 as Long))
                }

                statusActual{
                    order("id","desc")
                }
            }

            if(arrTeknisi.size()>0){
                def namaTeknisiID = JPB.findAllByReceptionAndStaDel(rec,"0").namaManPower?.id
                namaTeknisiID.each { idTeknisi ->
                    arrTeknisi.each {
                        if(idTeknisi == it){
                            tampilTeknisi = true
                        }
                    }
                }
            }else{
                tampilTeknisi = true
            }

            if(arrJenisPekerjaan.size()>0){
                def kategoriJobID = JobRCP.findByReception(rec)?.operation?.kategoriJob?.id
                arrJenisPekerjaan.each {
                    if(kategoriJobID == it){
                        tampilPekerjaan = true
                    }
                }
            }else{
                tampilPekerjaan = true
            }

            if(arrSA.size()>0){
                def domSA = User.findByUsername(rec?.t401NamaSA)?.t001NamaPegawai
                def saID = NamaManPower.findByT015NamaLengkapIlikeAndStaDel("%"+domSA+"%",'0')?.id
                arrSA.each {
                    print "SAAid " + saID
                    if(saID == it){
                        tampilSA = true
                    }
                }
            }else{
                tampilSA = true
            }

            def wfReception="",  wfJobDispatch = "", wfClockOn="", wfJoc = "", wfSettlement = "", wfInvoicing = ""
            def wfNotifikasi = "", wfWashing = "",wfFinalInspection = "", wfDelivery = ""
            def leadTimeReception = "", leadTimePrediagnose ="" , leadTimeFinalInspection = "", leadTimeIDR = ""
            def leadTimeJOC = "", leadTimeInvoicing = "", leadTimeDelivery = "", leadTimeWashing = ""
            def leadTimeProduction = "",leadTimeJobstopped = "",otd=""

            try{
                TimeDuration duration = TimeCategory.minus(rec.customerIn.t400TglJamReception, rec.customerIn.t400TglCetakNoAntrian)
                wfReception =  duration.getHours()+":"+duration.getMinutes()+":"+duration.getSeconds()
                if(duration.getHours()<0 || duration.getMinutes()<0 || duration.getSeconds()<0){
                    wfReception =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdLeadtime = TimeCategory.minus(rec.customerIn.t400TglJamSelesaiReception, rec.customerIn.t400TglJamReception)
                leadTimeReception =  tdLeadtime.getHours()+":"+tdLeadtime.getMinutes()+":"+tdLeadtime.getSeconds()
                if(tdLeadtime.getHours()<0 || tdLeadtime.getMinutes()<0 || tdLeadtime.getSeconds()<0){
                    leadTimeReception =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdJobDispatch = TimeCategory.minus(rec.t401TglJamCetakWO, rec.customerIn.t400TglJamSelesaiReception)
                wfJobDispatch =  tdJobDispatch.getHours()+":"+tdJobDispatch.getMinutes()+":"+tdJobDispatch.getSeconds()
                if(tdJobDispatch.getHours()<0 || tdJobDispatch.getMinutes()<0 || tdJobDispatch.getSeconds()<0){
                    wfJobDispatch =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdPrediagnose = TimeCategory.minus(prediagnose?.t406TglJamSelesai, prediagnose?.t406TglJamMulai)
                leadTimePrediagnose =  tdPrediagnose.getHours()+":"+tdPrediagnose.getMinutes()+":"+tdPrediagnose.getSeconds()
                if(tdPrediagnose.getHours()<0 || tdPrediagnose.getMinutes()<0 || tdPrediagnose.getSeconds()<0){
                    leadTimePrediagnose =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdIDR = TimeCategory.minus(inspect?.t506TglJamSelesai, inspect?.t506TglJamMulai)
                leadTimeIDR =  tdIDR?.getHours()+":"+tdIDR?.getMinutes()+":"+tdIDR?.getSeconds()
                if(tdIDR.getHours()<0 || tdIDR.getMinutes()<0 || tdIDR.getSeconds()<0){
                    leadTimeIDR =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdWfClockOn = TimeCategory.minus(rec.t401TglJamCetakWO, production.get(0)?.t452TglJam)
                wfClockOn =  tdWfClockOn.getHours()+":"+tdWfClockOn.getMinutes()+":"+tdWfClockOn.getSeconds()
                if(tdWfClockOn.getHours()<0 || tdWfClockOn.getMinutes()<0 || tdWfClockOn.getSeconds()<0){
                    wfClockOn =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdProduction = TimeCategory.minus(production.get(production.size()-1)?.t452TglJam, production.get(0)?.t452TglJam)
                leadTimeProduction =  tdProduction.getHours()+":"+tdProduction.getMinutes()+":"+tdProduction.getSeconds()
                if(tdProduction.getHours()<0 || tdProduction.getMinutes()<0 || tdProduction.getSeconds()<0){
                    leadTimeProduction =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                if(jobStop.size()>0 && jobContinue.size()>0){
                    TimeDuration tdjobStopped = TimeCategory.minus(jobContinue.get(0)?.t452TglJam, jobStop.get(0)?.t452TglJam)
                    leadTimeJobstopped =  tdjobStopped.getHours()+":"+tdjobStopped.getMinutes()+":"+tdjobStopped.getSeconds()
                    if(tdjobStopped.getHours()<0 || tdjobStopped.getMinutes()<0 || tdjobStopped.getSeconds()<0){
                        leadTimeJobstopped =  "00:00:00"
                    }
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdFinalInspection = TimeCategory.minus(finalInspection?.t508TglJamMulai, production.get(production.size()-1)?.t452TglJam)
                wfFinalInspection =  tdFinalInspection.getHours()+":"+tdFinalInspection.getMinutes()+":"+tdFinalInspection.getSeconds()
                if(tdFinalInspection.getHours()<0 || tdFinalInspection.getMinutes()<0 || tdFinalInspection.getSeconds()<0){
                    wfFinalInspection =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdFinalInspection = TimeCategory.minus(finalInspection?.t508TglJamSelesai, finalInspection?.t508TglJamMulai)
                leadTimeFinalInspection =  tdFinalInspection.getHours()+":"+tdFinalInspection.getMinutes()+":"+tdFinalInspection.getSeconds()
                if(tdFinalInspection.getHours()<0 || tdFinalInspection.getMinutes()<0 || tdFinalInspection.getSeconds()<0){
                    leadTimeFinalInspection =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdWfJOC = TimeCategory.minus(joc?.t601TglJamJOC, finalInspection?.t508TglJamSelesai)
                wfJoc =  tdWfJOC.getHours()+":"+tdWfJOC.getMinutes()+":"+tdWfJOC.getSeconds()
                if(tdWfJOC.getHours()<0 || tdWfJOC.getMinutes()<0 || tdWfJOC.getSeconds()<0){
                    wfJoc =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdJOC = TimeCategory.minus(joc?.t601TglJamJOCSelesai, joc?.t601TglJamJOC)
                leadTimeJOC =  tdJOC.getHours()+":"+tdJOC.getMinutes()+":"+tdJOC.getSeconds()
                if(tdJOC.getHours()<0 || tdJOC.getMinutes()<0 || tdJOC.getSeconds()<0){
                    leadTimeJOC =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdWfInvoicing = TimeCategory.minus(joc?.t601TglJamJOCSelesai, invoicing?.t701TglJamInvoice)
                wfInvoicing =  tdWfInvoicing.getHours()+":"+tdWfInvoicing.getMinutes()+":"+tdWfInvoicing.getSeconds()
                if(tdWfInvoicing.getHours()<0 || tdWfInvoicing.getMinutes()<0 || tdWfInvoicing.getSeconds()<0){
                    wfInvoicing =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdInvoicing = TimeCategory.minus(invoicing?.t701TglJamCetak, invoicing?.t701TglJamInvoice)
                leadTimeInvoicing =  tdInvoicing.getHours()+":"+tdInvoicing.getMinutes()+":"+tdInvoicing.getSeconds()
                if(tdInvoicing.getHours()<0 || tdInvoicing.getMinutes()<0 || tdInvoicing.getSeconds()<0){
                    leadTimeInvoicing =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                if(washing.size()>=1){
                    TimeDuration tdWfWashing = TimeCategory.minus(finalInspection?.t508TglJamSelesai, washing.get(0)?.t602TglJam)
                    wfWashing =  tdWfWashing.getHours()+":"+tdWfWashing.getMinutes()+":"+tdWfWashing.getSeconds()
                    if(tdWfWashing.getHours()<0 || tdWfWashing.getMinutes()<0 || tdWfWashing.getSeconds()<0){
                        wfWashing =  "00:00:00"
                    }
                }
            }catch (Exception e){
            }

            try{
                if(washing.size()>=2){
                    TimeDuration tdWashing = TimeCategory.minus(washing.get(1)?.t602TglJam, washing.get(0)?.t602TglJam)
                    leadTimeWashing =  tdWashing.getHours()+":"+tdWashing.getMinutes()+":"+tdWashing.getSeconds()
                    if(tdWashing.getHours()<0 || tdWashing.getMinutes()<0 || tdWashing.getSeconds()<0){
                        leadTimeWashing =  "00:00:00"
                    }
                }
            }catch (Exception e){
            }
            try{
                if(washing.size()>=2){
                    TimeDuration tdwfNotifikasi = TimeCategory.minus(invoicing?.t701TglJamInvoice, washing?.get(1)?.t602TglJam)
                    wfNotifikasi =  tdwfNotifikasi.getHours()+":"+tdwfNotifikasi.getMinutes()+":"+tdwfNotifikasi.getSeconds()
                    if(tdwfNotifikasi.getHours()<0 || tdwfNotifikasi.getMinutes()<0 || tdwfNotifikasi.getSeconds()<0){
                       wfNotifikasi =  "00:00:00"
                    }
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdWfSettlement = TimeCategory.minus(settlement?.t704TglJamSettlement, rec?.t401TglJamExplanation)
                wfSettlement =  tdWfSettlement.getHours()+":"+tdWfSettlement.getMinutes()+":"+tdWfSettlement.getSeconds()
                if(tdWfSettlement.getHours()<0 || tdWfSettlement.getMinutes()<0 || tdWfSettlement.getSeconds()<0){
                    wfSettlement =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdwfDelivery = TimeCategory.minus(rec?.t401TglJamExplanation, rec?.t401TglJamNotifikasi)
                wfDelivery =  tdwfDelivery.getHours()+":"+tdwfDelivery.getMinutes()+":"+tdwfDelivery.getSeconds()
                if(tdwfDelivery.getHours()<0 || tdwfDelivery.getMinutes()<0 || tdwfDelivery.getSeconds()<0){
                    wfDelivery =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdDelivery = TimeCategory.minus(rec?.customerIn?.t400TglJamOut, rec?.t401TglJamExplanation)
                leadTimeDelivery =  tdDelivery.getHours()+":"+tdDelivery.getMinutes()+":"+tdDelivery.getSeconds()
                if(tdDelivery.getHours()<0 || tdDelivery.getMinutes()<0 || tdDelivery.getSeconds()<0){
                    leadTimeDelivery =  "00:00:00"
                }
            }catch (Exception e){
            }

            try{
                TimeDuration tdOtd = TimeCategory.minus(rec?.t401TglJamJanjiPenyerahan, rec?.t401TglJamNotifikasi)
                if(Math.abs(tdOtd.getMinutes()) >=0 && Math.abs(tdOtd.getMinutes()) <=30){
                    otd = "1"
                }
            }catch (Exception e){
            }

            if(tampilTeknisi==true && tampilPekerjaan==true && tampilSA==true){
                row <<[
                        column_0 : rec?.historyCustomerVehicle?.fullNoPol,
                        column_1 : rec?.t401NoWO,
                        column_2 : kategoriJob,
                        column_3 : rec?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                        column_4 : rec?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102KodeBaseModel,
                        column_5 : jobRcp.size()>=1?jobRcp.get(0):"",
                        column_6 : jobRcp.size()>=2?jobRcp.get(1):"",
                        column_7 : jobRcp.size()>=3?jobRcp.get(2):"",
                        column_8 : jobRcp.size()>=4?jobRcp.get(3):"",
                        column_9 : jobRcp.size()>=5?jobRcp.get(4):"",
                        column_10 : rec?.t401NamaSA,
                        column_11 : namaTeknisi.toString().substring(1,namaTeknisi.toString().length()-1),
                        column_12 : rec?.t401StaCustomerTunggu?:"0",
                        column_13 : rec?.customerIn?.t400TglCetakNoAntrian?.format("dd-MMM-yyyy"),
                        column_14 : rec?.customerIn?.t400TglCetakNoAntrian?.format("HH:mm:ss"),
                        column_15 : wfReception,
                        column_16 : rec?.dateCreated?.format("dd-MMM-yyyy"),
                        column_17 : rec?.dateCreated?.format("HH:mm:ss"),
                        column_18 : rec?.customerIn?.t400TglJamSelesaiReception?.format("HH:mm:ss"),
                        column_19 : rec?.t401StaOkCancelReSchedule?:"0",
                        column_20 : leadTimeReception,
                        column_21 : prediagnose?.t406TglJamMulai?.format("HH:mm:ss"),
                        column_22 : prediagnose?.t406TglJamSelesai?.format("HH:mm:ss"),
                        column_23 : leadTimePrediagnose,
                        column_24 : wfJobDispatch,
                        column_25 : rec?.t401TglJamCetakWO?.format("HH:mm:ss"),
                        column_26 : wfClockOn,
                        column_27 : production.size()>=1?production.get(0)?.t452TglJam?.format("dd-MMM-yyyy"):"",
                        column_28 : production.size()>=1?production.get(0)?.t452TglJam?.format("HH:mm:ss"):"",
                        column_29 : inspect?.t506TglJamMulai?.format("HH:mm:ss"),
                        column_30 : inspect?.t506TglJamSelesai?.format("HH:mm:ss"),
                        column_31 : production.size()>=2?production.get(production.size()-1)?.t452TglJam?.format("dd-MMM-yyyy"):"",
                        column_32 : production.size()>=2?production.get(production.size()-1)?.t452TglJam?.format("HH:mm:ss"):"",
                        column_33 : leadTimeProduction,
                        column_34 : leadTimeIDR,
                        column_35 : jobStop.size()>=1?jobStop.get(0)?.t452TglJam?.format("dd-MMM-yyyy"):"",
                        column_36 : jobStop.size()>=1?jobStop.get(0)?.t452TglJam?.format("HH:mm:ss"):"",
                        column_37 : jobStop.size(),
                        column_38 : masalah.size()>=1?masalah.get(0)?.statusKendaraan?.m999NamaStatusKendaraan:"",
                        column_39 : jobContinue.size()>=1?jobContinue.get(0)?.t452TglJam?.format("dd-MMM-yyyy"):"",
                        column_40 : jobContinue.size()>=1?jobContinue.get(0)?.t452TglJam?.format("HH:mm:ss"):"",
                        column_41 : leadTimeJobstopped,
                        column_42 : wfFinalInspection,
                        column_43 : finalInspection?.t508TglJamMulai?.format("dd-MMM-yyyy"),
                        column_44 : finalInspection?.t508TglJamMulai?.format("HH:mm:ss"),
                        column_45 : finalInspection?.t508TglJamSelesai?.format("dd-MMM-yyyy"),
                        column_46 : finalInspection?.t508TglJamSelesai?.format("HH:mm:ss"),
                        column_47 : leadTimeFinalInspection,
                        column_48 : wfJoc,
                        column_49 : joc?.t601TglJamJOC?.format("dd-MMM-yyyy"),
                        column_50 : joc?.t601TglJamJOC?.format("HH:mm:ss"),
                        column_51 : joc?.t601TglJamJOCSelesai?.format("dd-MMM-yyyy"),
                        column_52 : joc?.t601TglJamJOCSelesai?.format("HH:mm:ss"),
                        column_53 : leadTimeJOC,
                        column_54 : wfInvoicing,
                        column_55 : invoicing?.t701TglJamInvoice?.format("dd-MMM-yyyy"),
                        column_56 : invoicing?.t701TglJamInvoice?.format("HH:mm:ss"),
                        column_57 : invoicing?.t701TglJamCetak?.format("HH:mm:ss"),
                        column_58 : leadTimeInvoicing,
                        column_59 : wfWashing,
                        column_60 : washing.size()>=1?washing?.get(0)?.antriCuciTanggal?.t600Tanggal?.format("dd-MMM-yyyy"):"",
                        column_61 : washing.size()>=1?washing?.get(0)?.t602TglJam?.format("HH:mm:ss"):"",
                        column_62 : washing.size()>=2?washing?.get(1)?.t602TglJam?.format("HH:mm:ss"):"",
                        column_63 : leadTimeWashing,
                        column_64 : wfNotifikasi,
                        column_65 : rec?.t401TglJamNotifikasi?.format("HH:mm:ss"),
                        column_66 : wfDelivery,
                        column_67 : rec?.t401TglJamExplanation?.format("dd-MMM-yyyy"),
                        column_68 : rec?.t401TglJamExplanation?.format("HH:mm:ss"),
                        column_69 : rec?.customerIn?.t400TglJamOut?.format("dd-MMM-yyyy"),
                        column_70 : rec?.customerIn?.t400TglJamOut?.format("HH:mm:ss"),
                        column_71 : wfSettlement,
                        column_72 : settlement?.t704TglJamSettlement?.format("HH:mm:ss"),
                        column_73 : leadTimeDelivery,
                        column_74 : rec?.t401TglJamJanjiPenyerahan?.format("dd-MMM-yyyy"),
                        column_75 : rec?.t401TglJamJanjiPenyerahan?.format("HH:mm:ss"),
                        column_76 : otd
                ]
            }
        }
        return row
    }
}
