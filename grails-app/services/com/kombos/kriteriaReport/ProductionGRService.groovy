package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KategoriJob
import com.kombos.administrasi.ModelName
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.maintable.StatusKendaraan
import com.kombos.parts.Konversi
import com.kombos.woinformation.JobRCP
import org.h2.util.New

import java.text.DateFormat
import java.text.SimpleDateFormat

class ProductionGRService {
	boolean transactional = false

	def sessionFactory
    def konversi = new Konversi()
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}

    def ambilJenisPekerjaanById(def id){
        String ret = "";
        final session = sessionFactory.currentSession
        String query = " SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE ID IN ("+id+") ";
        final sqlQuery = session.createSQLQuery(query)
        final queryResults = sqlQuery.with {
            list()
        }
        final results = queryResults.collect { resultRow ->
            [
                id: resultRow[0],
                m055KategoriJob: resultRow[1]
            ]
        }
        for(Map<String, Object> result : results) {
            ret += String.valueOf(result.get("m055KategoriJob")) + ", ";
        }
        if(ret != ""){
            ret = ret.substring(0, ret.length()-1);
        }
        return ret;
    }

    def getJenisPekerjaanById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M401_NAMATIPEKERUSAKAN FROM M401_TIPEKERUSAKAN WHERE IN ("+id+") ";		
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaTipeKerusakan: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaTipeKerusakan")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}

	def datatablesKendaraanList(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = ModelName.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			if (params."m104NamaModelName") {
				ilike("m104NamaModelName", "%" + (params."m104NamaModelName" as String) + "%")
			}

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				m104NamaModelName: it.m104NamaModelName

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}

	def datatablesTechnisianList(def params){

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = NamaManPower.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			if (params."teknisi") {
				ilike("t015NamaBoard", "%" + (params."teknisi" as String) + "%")
			}

			userProfile{
				roles{
					ilike("name","%" + 'Teknisi' + "%")
				}
			}

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				teknisi: it.t015NamaBoard+' - '+it?.userProfile?.t001Inisial

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}

	def datatablesSAList(def params){

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = NamaManPower.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			if (params."sa") {
				ilike("t015NamaBoard", "%" + (params."sa" as String) + "%")
			}

			userProfile{
				roles{
					ilike("name","%" + 'SA' + "%")
				}
			}

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				namaSa: it.t015NamaBoard+' - '+it?.userProfile?.t001Inisial

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}

	def datatablesJenisPayment(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = JobRCP.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if (params."payment") {
				ilike("t402StaCashInsurance", "%" + (params."payment" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPayment: it.t402StaCashInsurance == '1' ? 'Cash' : 'Insurance'

			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret
	}

	def datatablesJenisPekerjaan(def params){
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = KategoriJob.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params."pekerjaan"){
                ilike("m055KategoriJob","%" + (params."pekerjaan" as String) + "%")
            }
            inList("m055KategoriJob", ["GR","SBE","SBI","PDS","TWC"])

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    m055KategoriJob: it?.m055KategoriJob
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	}

	def datatablesStoppedList(def params){

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = StatusKendaraan.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				m999NamaStatusKendaraan: it.m999NamaStatusKendaraan

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}
	
	def datatablesGrTechnicianProductivityDetail(def params){
		final session = sessionFactory.currentSession
        //println("isi : "+params)
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yy");
        def tgl1 = df.parse(params.tanggal)
        def tgl2 = df.parse(params.tanggal2)
        String jenisPekerjaanFilter = ""
        if(params.jenisPekerjaan){
            jenisPekerjaanFilter = " AND JOB_ID IN("+params.jenisPekerjaan+")";
        }

		String query ="SELECT DAT.T021_GROUP, " +
                "       DAT.FOREMAN," +
                "       DAT.T015_NAMALENGKAP, " +
                "       DAT.HARI_KERJA_AKTUAL HARI_KERJA_AKTUAL,  " +
                "       DAT.HARI_KERJA_KALENDER HARI_KERJA_KALENDER," +
                "       DAT.JAM_KERJA," +
                "       SUM(DAT.OVERTIME) OVER_TIME," +
                "       SUM(DAT.MINUS_TIME) MINUS_TIME,  " +
                "       SUM(DAT.TOTAL_WO) TOTAL_UNIT, " +
                "       SUM(DAT.TOTAL_WO)/DAT.JAM_KERJA TOTAL_UNIT_PER_HARI," +
                "       SUM(DAT.RTJ) RTJ," +
                "       SUM(DAT.RTJ)/SUM(DAT.TOTAL_WO) PERSEN_RTJ," +
                "       NVL(DAT.JAM_KERJA,0) + NVL(DAT.OVERTIME,0) - NVL(DAT.MINUS_TIME,0) JT," +
                "       CEIL(NVL(SUM(ACTUALRATE) , 0)) JA," +
                "       CEIL(NVL(SUM(FLATRATE) , 0)) JF," +
                "       NVL(SUM(ACTUALRATE) , 0)/(NVL(DAT.JAM_KERJA,0) + NVL(DAT.OVERTIME,0) - NVL(DAT.MINUS_TIME,0)) LABOUR_UTIL," +
                "       NVL(SUM(FLATRATE) , 0)/(NVL(DAT.JAM_KERJA,0) + NVL(DAT.OVERTIME,0) - NVL(DAT.MINUS_TIME,0)) OVERALL_PROD," +
                "       NVL(SUM(FLATRATE) , 0)/NVL(SUM(ACTUALRATE) , 0) TECH_EFF FROM (" +
                "SELECT   DAT.T021_GROUP," +
                "         DAT.FOREMAN," +
                "         DAT.T015_NAMALENGKAP," +
                "         DAT.TH004_ATTEND AS HARI_KERJA_AKTUAL," +
                "         30 - (SELECT COUNT(1) FROM M031_KALENDERKERJA WHERE M031_KALENDERKERJA.M031_STALIBUR='Y') AS HARI_KERJA_KALENDER," +
                "         DAT.TH004_ATTEND * 8 AS JAM_KERJA," +
                "         0 AS MINUS_TIME," +
                "         0 AS OVERTIME, TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MON-YYYY') AS TANGGALWO," +
                "         CASE WHEN T401_RECEPTION.T401_NOWO IS NOT NULL THEN 1" +
                "             ELSE 0  " +
                "         END AS TOTAL_WO," +
                "         CASE WHEN LOWER(T402_JOBRCP.T402_STAWARRANTY) LIKE '%rtj%' THEN 1" +
                "            ELSE 0" +
                "         END AS RTJ," +
                "         (SELECT SUM(T113_FLATRATE.T113_PRODUCTIONFLATRATE) FROM T113_FLATRATE WHERE T113_FLATRATE.T113_M053_JOBID = T402_JOBRCP.T402_M053_JOBID ) ACTUALRATE," +
                "         (SELECT SUM(T113_FLATRATE.T113_FLATRATE) FROM T113_FLATRATE WHERE T113_FLATRATE.T113_M053_JOBID = T402_JOBRCP.T402_M053_JOBID ) FLATRATE" +
                "      FROM (SELECT DISTINCT T015_NAMAMANPOWER.ID,  " +
                "                   T021_GROUPMANPOWER.T021_GROUP," +
                "                   (SELECT T015_NAMALENGKAP FROM T015_NAMAMANPOWER WHERE  T015_NAMAMANPOWER.ID = T021_GROUPMANPOWER.NAMA_MAN_POWER_FOREMAN_ID AND T015_NAMAMANPOWER.T015_STADEL = 0) FOREMAN," +
                "                   T015_NAMAMANPOWER.T015_IDMANPOWER, " +
                "                   T015_NAMAMANPOWER.T015_NAMALENGKAP," +
                "                   MH009_KRYWAN.MH009_ID," +
                "                   TH008_ABSENSIKARYAWANDETAIL.TH008_FUNCTIONKEY," +
                "                   TH008_ABSENSIKARYAWANDETAIL.TH008_TRANSACTIONTIME," +
                "                   TH004_ABSENSIKARYAWAN.TH004_BULANTAHUN," +
                "                   TH004_ABSENSIKARYAWAN.TH004_ATTEND" +
                "            FROM T015_NAMAMANPOWER" +
                "            INNER JOIN T021_GROUPMANPOWER ON T021_GROUPMANPOWER.ID = T015_NAMAMANPOWER.T015_T021_GROUP" +
                "            LEFT JOIN MH009_KRYWAN ON MH009_KRYWAN.MH009_NOMORPOKOKKARYAWAN = T015_NAMAMANPOWER.T015_IDMANPOWER" +
                "            LEFT JOIN TH004_ABSENSIKARYAWAN ON TH004_ABSENSIKARYAWAN.TH004_MH009_ID_KARYAWAN = MH009_KRYWAN.MH009_ID" +
                "            LEFT JOIN TH008_ABSENSIKARYAWANDETAIL ON  TH008_ABSENSIKARYAWANDETAIL.TH008_TH004_ID = TH004_ABSENSIKARYAWAN.TH004_ID" +
                "            WHERE T015_NAMAMANPOWER.T015_M011_ID= "+params.workshop+"" +
                "            AND TH008_ABSENSIKARYAWANDETAIL.TH008_FUNCTIONKEY IN(1,2)          " +
                "            AND T021_GROUPMANPOWER.T021_GROUP <> 'GROUP BODY')DAT             " +
                "            LEFT JOIN T451_JPB ON T451_JPB.T451_T015_IDMANPOWER = DAT.ID  " +
                "            LEFT JOIN T401_RECEPTION ON  T451_JPB.T451_T401_NOWO = T401_RECEPTION.ID  " +
                "            LEFT JOIN T402_JOBRCP ON T402_JOBRCP.T402_T401_NOWO =  T401_RECEPTION.ID  " +
                "            LEFT JOIN T453_ACTUALRATETEKNISI ON T453_ACTUALRATETEKNISI.T453_T401_NOWO = T401_RECEPTION.ID  " +
                "            WHERE DAT.FOREMAN <> DAT.T015_NAMALENGKAP " +
                "            AND DAT.TH004_BULANTAHUN =TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MMYYYY')" +
                "            AND TO_CHAR(DAT.TH008_TRANSACTIONTIME,'DD-MM-YYYY')= TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY')" +
                "            AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MM-YYYY')='"+params.tanggal+"' " +
                "            AND T451_JPB.T451_M011_ID = T401_RECEPTION.COMPANY_DEALER_ID" +
                "            AND T401_RECEPTION.T401_STADEL='0') DAT GROUP BY DAT.T021_GROUP, DAT.FOREMAN, DAT.T015_NAMALENGKAP, DAT.TANGGALWO,DAT.HARI_KERJA_AKTUAL,DAT.HARI_KERJA_KALENDER,DAT.JAM_KERJA," +
                "        NVL(DAT.JAM_KERJA,0) + NVL(DAT.OVERTIME,0) - NVL(DAT.MINUS_TIME,0)" +
                "ORDER BY DAT.FOREMAN ASC";
        def results = null
        //println("o1 : "+query)

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],				 				 
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9:Math.round(resultRow[9]*1000)/1000,
				 column_10: resultRow[10],
				 column_11: resultRow[11],
				 column_12: resultRow[12],
				 column_13: resultRow[13],
				 column_14: resultRow[14],
				 column_15: Math.round(resultRow[15]*1000)/1000,
				 column_16: Math.round(resultRow[16]*1000)/1000,
				 column_17: Math.round(resultRow[17]*1000)/1000
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	def datatablesGrTechnicianProductivityDetailServiceBerkala(def params){
		final session = sessionFactory.currentSession
		String query ="SELECT  DAT.T021_GROUP, DAT.FOREMAN, DAT.T015_NAMALENGKAP," +
                "        SUM(DAT.HARI_KERJA_AKTUAL) HARI_KERJA_AKTUAL,  SUM(DAT.HARI_KERJA_KALENDER) HARI_KERJA_KALENDER, SUM(DAT.JAM_KERJA) JAM_KERJA," +
                "        SUM(DAT.OVERTIME) OVERTIME,  SUM(DAT.MINUS_TIME) MINUS_TIME,  SUM(DAT.TOTAL_WO) TOTAL_UNIT, SUM(DAT.TOTAL_WO) TOTAL_UNIT_PER_HARI, " +
                "        SUM(DAT.RTJ) RTJ, DECODE(SUM(DAT.TOTAL_WO), 0, 0, CEIL(SUM(DAT.RTJ) / SUM(DAT.TOTAL_WO)) * 100) AS PERSEN_RTJ, " +
                "        CEIL(NVL((SUM(DAT.JAM_KERJA) + SUM(DAT.OVERTIME)) -  SUM(DAT.MINUS_TIME), 0)) JT,  CEIL(NVL(SUM(T453_ACTUALRATE) , 0)) JA, " +
                "        CEIL(NVL(SUM(FLATRATE) , 0)) JF,  CEIL(DECODE(CEIL(NVL((SUM(DAT.JAM_KERJA) + SUM(DAT.OVERTIME)) -  SUM(DAT.MINUS_TIME), 0)), 0, 0, CEIL(NVL(SUM(T453_ACTUALRATE) , 0)) /  CEIL(NVL((SUM(DAT.JAM_KERJA) + SUM(DAT.OVERTIME)) -  SUM(DAT.MINUS_TIME), 0)))) LU,  CEIL(DECODE(CEIL(NVL((SUM(DAT.JAM_KERJA) + SUM(DAT.OVERTIME)) -  SUM(DAT.MINUS_TIME), 0)), 0, 0, CEIL(NVL(SUM(FLATRATE) , 0)) / CEIL(NVL((SUM(DAT.JAM_KERJA) + SUM(DAT.OVERTIME)) -  SUM(DAT.MINUS_TIME), 0)))) OP,  CEIL(DECODE(CEIL(NVL(SUM(T453_ACTUALRATE) , 0)), 0, 0, CEIL(NVL(SUM(FLATRATE) , 0)) / CEIL(NVL(SUM(T453_ACTUALRATE) , 0)) )) TE  FROM (  SELECT   DAT.T021_GROUP,  DAT.FOREMAN,  DAT.T015_NAMALENGKAP,  CASE      WHEN T017_MANPOWERABSENSI.T017_TANGGAL IS NOT NULL THEN 1      ELSE 0  END AS HARI_KERJA_AKTUAL, " +
                "        (SELECT COUNT(1) FROM M031_KALENDERKERJA) AS HARI_KERJA_KALENDER,  NVL(CEIL((extract(DAY FROM T022_MANPOWERJAMKERJA.T022_JAMSELESAI1 - T022_MANPOWERJAMKERJA.T022_JAMMULAI1) * 24 * 60 * 60)+           (extract(HOUR FROM T022_MANPOWERJAMKERJA.T022_JAMSELESAI1 - T022_MANPOWERJAMKERJA.T022_JAMMULAI1) *  60 * 60) +                (extract(MINUTE FROM T022_MANPOWERJAMKERJA.T022_JAMSELESAI1 - T022_MANPOWERJAMKERJA.T022_JAMMULAI1) *  60 ) +                         (extract(SECOND FROM T022_MANPOWERJAMKERJA.T022_JAMSELESAI1 - T022_MANPOWERJAMKERJA.T022_JAMMULAI1))) / 3600, 0) AS JAM_KERJA, " +
                "          NVL(CEIL((extract(DAY FROM T017_MANPOWERABSENSI.T017_JAMPULANG - T022_MANPOWERJAMKERJA.T022_JAMSELESAI1) * 24 * 60 * 60)+            (extract(HOUR FROM T017_MANPOWERABSENSI.T017_JAMPULANG - T022_MANPOWERJAMKERJA.T022_JAMSELESAI1) *  60 * 60) +                 (extract(MINUTE FROM T017_MANPOWERABSENSI.T017_JAMPULANG - T022_MANPOWERJAMKERJA.T022_JAMSELESAI1) *  60 ) +                         (extract(SECOND FROM T017_MANPOWERABSENSI.T017_JAMPULANG - T022_MANPOWERJAMKERJA.T022_JAMSELESAI1))) / 3600, 0) AS OVERTIME,  0 AS MINUS_TIME,  TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MON-YYYY') AS TANGGALWO,  CASE      WHEN T401_RECEPTION.T401_NOWO IS NOT NULL THEN 1      ELSE 0  END AS TOTAL_WO,  CASE      WHEN   LOWER(T402_JOBRCP.T402_STAWARRANTY) LIKE '%rtj%' THEN 1       ELSE 0  END AS RTJ,  T453_ACTUALRATETEKNISI.T453_ACTUALRATE, " +
                "          (SELECT SUM(T113_FLATRATE.T113_FLATRATE) FROM T113_FLATRATE WHERE T113_FLATRATE.T113_M053_JOBID = T402_JOBRCP.T402_M053_JOBID ) FLATRATE " +
                "FROM (  SELECT   T015_NAMAMANPOWER.ID,  T021_GROUPMANPOWER.T021_GROUP, " +
                "            (SELECT T015_NAMALENGKAP FROM T015_NAMAMANPOWER       WHERE  T015_NAMAMANPOWER.ID = T021_GROUPMANPOWER.NAMA_MAN_POWER_FOREMAN_ID AND T015_NAMAMANPOWER.T015_STADEL = 0) FOREMAN, " +
                "              T015_NAMAMANPOWER.T015_NAMALENGKAP  " +
                "         FROM T015_NAMAMANPOWER  INNER JOIN T021_GROUPMANPOWER ON T021_GROUPMANPOWER.ID = T015_NAMAMANPOWER.T015_T021_GROUP " +
                "         WHERE T015_NAMAMANPOWER.T015_M011_ID = "+params.workshop+"" +
                "    ) DAT " +
                "          LEFT JOIN T017_MANPOWERABSENSI ON T017_MANPOWERABSENSI.T017_T015_IDMANPOWER = DAT.ID " +
                "          LEFT JOIN T022_MANPOWERJAMKERJA ON  T022_MANPOWERJAMKERJA.T022_T015_IDMANPOWER = DAT.ID " +
                "          LEFT JOIN T451_JPB ON T451_JPB.T451_T015_IDMANPOWER = DAT.ID " +
                "          LEFT JOIN T401_RECEPTION ON  T451_JPB.T451_T401_NOWO = T401_RECEPTION.ID " +
                "          LEFT JOIN T402_JOBRCP ON T402_JOBRCP.T402_T401_NOWO =  T401_RECEPTION.ID " +
                "          LEFT JOIN T453_ACTUALRATETEKNISI ON T453_ACTUALRATETEKNISI.T453_T401_NOWO = T401_RECEPTION.ID  WHERE DAT.FOREMAN <> DAT.T015_NAMALENGKAP  ) DAT" +
                "        GROUP BY DAT.T021_GROUP, DAT.FOREMAN, DAT.T015_NAMALENGKAP "+" ORDER BY DAT.FOREMAN ASC" +
                "";
		def results = null

		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],				 				 
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9],
				 column_10: resultRow[10],
				 column_11: resultRow[11],
				 column_12: resultRow[12],
				 column_13: resultRow[13],
				 column_14: resultRow[14],
				 column_15: resultRow[15],
				 column_16: resultRow[16],
				 column_17: resultRow[17]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}

	def getKepalaBengkel(def params){
		final session = sessionFactory.currentSession
        String Query = "SELECT T015_NAMALENGKAP FROM T015_NAMAMANPOWER NP INNER JOIN M015_MANPOWERDETAIL MP ON(NP.T015_M015_ID = MP.ID)INNER JOIN M014_MANPOWER M ON(MP.M015_M014_ID = M.ID)WHERE M.M014_JABATANMANPOWER = 'KEPALA BENGKEL' AND NP.T015_M011_ID = "+params.workshop+"";
        def results = null
		try {
			final sqlQuery = session.createSQLQuery(Query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			final List<String> names = queryResults.collect { it }
			if(!names.isEmpty()){
				return null != names.get(0) ? names.get(0) : "";
                //println("nama =-= "+names.get(0))
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;			
	}
	
	def getJumlahSA(def params){
		final session = sessionFactory.currentSession
		String query = "SELECT COUNT(NP.T015_NAMALENGKAP) JUMLAH_SA,'' DUMMY FROM T015_NAMAMANPOWER NP INNER JOIN M015_MANPOWERDETAIL MP ON(NP.T015_M015_ID = MP.ID)" +
                "INNER JOIN M014_MANPOWER M ON(MP.M015_M014_ID = M.ID)" +
                "WHERE M.M014_JABATANMANPOWER = 'SERVICE ADVISOR GR'" +
                "AND NP.T015_M011_ID = "+params.workshop+"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			final List<String> jumlahSA = queryResults.collect { it[0] }
			if(!jumlahSA.isEmpty()){
				return null != jumlahSA.get(0) ? Integer.parseInt(String.valueOf(jumlahSA.get(0))) : "";
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;
	}
	
	def getJumlahForeman(def params){
		final session = sessionFactory.currentSession
		String query = "SELECT COUNT(1), '' DUMMY FROM " +
                "(  SELECT DAT.FOREMAN FROM " +
                "(  SELECT  " +
                "(SELECT T015_NAMALENGKAP FROM T015_NAMAMANPOWER   WHERE  T015_NAMAMANPOWER.ID = T021_GROUPMANPOWER.NAMA_MAN_POWER_FOREMAN_ID  AND T015_NAMAMANPOWER.T015_STADEL = 0 ) FOREMAN  " +
                "FROM  T015_NAMAMANPOWER  INNER JOIN T021_GROUPMANPOWER ON T021_GROUPMANPOWER.ID = T015_NAMAMANPOWER.T015_T021_GROUP AND T015_NAMAMANPOWER.T015_M011_ID="+params.workshop+") DAT  GROUP BY DAT.FOREMAN  )";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			final List<String> jumlahForeman = queryResults.collect { it[0] }
			if(!jumlahForeman.isEmpty()){
				return null != jumlahForeman.get(0) ? Integer.parseInt(String.valueOf(jumlahForeman.get(0))) : "";
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;
	}
	
	def getJumlahTeknisi(def params){
		final session = sessionFactory.currentSession
		String query = 					
					" SELECT COUNT(*), '' DUMMY FROM ( "+
					" SELECT "+ 
					" T015_NAMAMANPOWER.ID, "+ 
					" T021_GROUPMANPOWER.T021_GROUP, "+ 
					" (SELECT T015_NAMALENGKAP FROM T015_NAMAMANPOWER "+ 
					" 	WHERE  T015_NAMAMANPOWER.ID = T021_GROUPMANPOWER.NAMA_MAN_POWER_FOREMAN_ID AND T015_NAMAMANPOWER.T015_STADEL = 0) FOREMAN, "+
					" T015_NAMAMANPOWER.T015_NAMALENGKAP  "+
					" FROM T015_NAMAMANPOWER "+
					" INNER JOIN T021_GROUPMANPOWER ON T021_GROUPMANPOWER.ID = T015_NAMAMANPOWER.T015_T021_GROUP AND T015_NAMAMANPOWER.T015_M011_ID="+params.workshop+") DAT "+
					" WHERE DAT.FOREMAN <> DAT.T015_NAMALENGKAP "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			final List<String> jumlahTeknisi = queryResults.collect { it[0] }
			if(!jumlahTeknisi.isEmpty()){
				return null != jumlahTeknisi.get(0) ? Integer.parseInt(String.valueOf(jumlahTeknisi.get(0))) : "";
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;
	}
	
	def getJumlahStallSBNP(def params){
		final session = sessionFactory.currentSession
		String query = 					
					" SELECT COUNT(*) AS SBNP_STALL, '' DUMMY "+
					" FROM M022_Stall "+
					" INNER JOIN M023_SUBJENISSTALL ON M023_SUBJENISSTALL.ID = M022_STALL.M022_M023_ID "+
					" AND UPPER(M023_SUBJENISSTALL.M023_NAMASUBJENISSTALL) LIKE '%SBNP%' AND M022_Stall.M022_M011_ID="+params.workshop+" "+
					""; 
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			final List<String> jumlahStall = queryResults.collect { it[0] }
			if(!jumlahStall.isEmpty()){
				return null != jumlahStall.get(0) ? Integer.parseInt(String.valueOf(jumlahStall.get(0))) : "";
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;
	}
	
	def getJumlahStallGR(def params){
		final session = sessionFactory.currentSession
		String query = 					
					" SELECT COUNT(*) AS GR_STALL, '' DUMMY "+
					" FROM M022_Stall "+
					" INNER JOIN M023_SUBJENISSTALL ON M023_SUBJENISSTALL.ID = M022_STALL.M022_M023_ID "+
					" AND UPPER(M023_SUBJENISSTALL.M023_NAMASUBJENISSTALL) LIKE '%GR%' AND M022_Stall.M022_M011_ID="+params.workshop+" "+
					""; 
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			final List<String> jumlahStall = queryResults.collect { it[0] }
			if(!jumlahStall.isEmpty()){
				return null != jumlahStall.get(0) ? Integer.parseInt(String.valueOf(jumlahStall.get(0))) : "";
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;
	}
	
	def getTE(def params){
		def results = datatablesGrTechnicianProductivityDetail(params);
		def data = 0;
		for(Map<String, Object> result : results){
//			data += Integer.parseInt(String.valueOf(result.get("column_17")));
			data += Float.parseFloat(String.valueOf(result.get("column_17")));
		}
		return data;
	}
	
	def getLU(def params){
		def results = datatablesGrTechnicianProductivityDetail(params);
		def data = 0;
		for(Map<String, Object> result : results){
//			data += Integer.parseInt(String.valueOf(result.get("column_15")));
            data += Float.parseFloat(String.valueOf(result.get("column_15")));

		}
		return data;
	}
	
	def getOP(def params){
		def results = datatablesGrTechnicianProductivityDetail(params);
		def data = 87;
		for(Map<String, Object> result : results){
//			data += Integer.parseInt(String.valueOf(result.get("column_16")));
            data += Float.parseFloat(String.valueOf(result.get("column_16")));
		}
		return data;
	}
	
	def serviceBerkalaRUM(def params){
		def results = datatablesGrTechnicianProductivityDetail(params);
		def unit = 0;
		def totalHariKerjaAktual = 0;
		for(Map<String, Object> result : results){
			unit += Integer.parseInt(String.valueOf(result.get("column_8")));
			totalHariKerjaAktual += Integer.parseInt(String.valueOf(result.get("column_3")));
		}
		if(totalHariKerjaAktual == 0){
			return 0;
		}
		return unit / totalHariKerjaAktual;
	}
	
	def serviceBerkalaRPS(def params){
		def results = datatablesGrTechnicianProductivityDetail(params);
		def unit = 0;
		def totalHariKerjaAktual = 0;
		for(Map<String, Object> result : results){
			unit += Integer.parseInt(String.valueOf(result.get("column_8")));
			totalHariKerjaAktual += Integer.parseInt(String.valueOf(result.get("column_3")));
		}
		if(totalHariKerjaAktual == 0){
			return 0;
		}
		return unit / totalHariKerjaAktual;
	}
	
	def generalRepairRUM(def params){
		def results = datatablesGrTechnicianProductivityDetail(params);
		def unit = 0;
		def totalHariKerjaAktual = 0;
		for(Map<String, Object> result : results){
			unit += Integer.parseInt(String.valueOf(result.get("column_8")));
			totalHariKerjaAktual += Integer.parseInt(String.valueOf(result.get("column_3")));
		}
		if(totalHariKerjaAktual == 0){
			return 0;
		}
		return unit / totalHariKerjaAktual;
	}
	
	def generalRepairRPS(def params){
		def results = datatablesGrTechnicianProductivityDetail(params);
		def unit = 0;
		def totalHariKerjaAktual = 0;
		for(Map<String, Object> result : results){
			unit += Integer.parseInt(String.valueOf(result.get("column_8")));
			totalHariKerjaAktual += Integer.parseInt(String.valueOf(result.get("column_3")));
		}
		if(totalHariKerjaAktual == 0){
			return 0;
		}
		return unit / totalHariKerjaAktual;
	}
	
	def grandTotalRUM(def params){
		return serviceBerkalaRUM(params) + generalRepairRUM(params);
	}
	
	def grandTotalRPS(def params){
		return serviceBerkalaRPS(params) + generalRepairRPS(params);
	}
	
	def datatablesProcessLeadTimeGrafik(def params){
		final session = sessionFactory.currentSession
		String query = 
					" SELECT "+
					" DAT.TGL_WO, SUM(DAT.RECEPTION) RECEPTION, SUM(DAT.PREDIAGNOSE) PREDIAGNOSE, SUM(DAT.PRODUCTION) PRODUCTION, SUM(DAT.INSPECTION_DURING_REPAIR) INSPECTION_DURING_REPAIR, "+
					" SUM(DAT.FINAL_INSPECTION) FINAL_INSPECTION, SUM(DAT.PROSES_JOC) PROCESS_JOC, SUM(DAT.INVOICING) INVOICING, SUM(DAT.DELIVERY) DELIVERY, "+
					" NVL(CEIL((extract(DAY FROM DAT.FINISH_WASHING - DAT.START_WASHING) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM DAT.FINISH_WASHING - DAT.START_WASHING) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.FINISH_WASHING - DAT.START_WASHING) *  60 ) + "+
					" 					  (extract(SECOND FROM DAT.FINISH_WASHING- DAT.START_WASHING))), 0) AS Washing "+
					" FROM ( "+
					" SELECT "+ 
					" TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') AS TGL_WO, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception) * 24 * 60 * 60)+  "+
					" 		 (extract(HOUR FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception))), 0) AS Reception , "+
					" NVL(CEIL((extract(DAY FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai) *  60 ) + "+
					" 					  (extract(SECOND FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai))), 0) AS PreDiagnose, "+
					" NVL(CEIL((extract(DAY FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai))), 0) AS Production, "+
					" NVL(CEIL((extract(DAY FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai))), 0) AS Inspection_During_Repair, "+
					" NVL(CEIL((extract(DAY FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai))), 0) AS Final_Inspection, "+
					" NVL(CEIL((extract(DAY FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 ) + "+  
					" 					  (extract(SECOND FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC))), 0) AS Proses_Joc, "+
					" NVL(CEIL((extract(DAY FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI) * 24 * 60 * 60)+ "+ 
					" 		 (extract(HOUR FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI))), 0) AS Invoicing, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TglJamOut - DAT.T401_TglJamExplanation) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T400_CUSTOMERIN.T400_TglJamOut -DAT.T401_TglJamExplanation) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T400_CUSTOMERIN.T400_TglJamOut - DAT.T401_TglJamExplanation) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T400_CUSTOMERIN.T400_TglJamOut - DAT.T401_TglJamExplanation))), 0) AS Delivery, "+
					" CASE  "+
					" 	 WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'start')  "+ 
					" 	 THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	 ELSE NULL  "+
					" END AS START_WASHING, "+                                         
					" CASE  "+
					" 	 WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'finish') "+ 
					" 	 THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	 ELSE NULL  "+
					" END AS FINISH_WASHING "+
					" FROM ( "+
					" SELECT "+
					" T401_RECEPTION.*, "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID, "+
					" (SELECT T452_ACTUAL.ID FROM T452_ACTUAL WHERE T452_ACTUAL.T452_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS ACTUAL_ID "+
					" FROM  "+
					" T401_RECEPTION "+
					" ) DAT "+
					" INNER JOIN T400_CUSTOMERIN ON T400_CUSTOMERIN.ID = DAT.T401_T400_ID "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = DAT.ID "+
					" LEFT JOIN T452_ACTUAL ON T452_ACTUAL.T452_T401_NOWO = DAT.ACTUAL_ID "+
					" LEFT JOIN T506_IDR ON T506_IDR.T506_T401_NOWO = DAT.ID "+
					" LEFT JOIN T508_FINALINSPECTION ON T508_FINALINSPECTION.T508_T401_NOWO = DAT.ID "+
					" LEFT JOIN T601_JOCTECO ON T601_JOCTECO.T601_T401_NOWO = DAT.ID "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID "+
					" LEFT JOIN T600_ANTRICUCI ON T600_ANTRICUCI.T600_T401_NOWO = DAT.ID "+
					" LEFT JOIN T602_ACTUALCUCI ON T602_ACTUALCUCI.T602_T600_ID = T600_ANTRICUCI.T600_ID "+
					" ) DAT "+
					" GROUP BY DAT.TGL_WO, DAT.FINISH_WASHING, DAT.START_WASHING "+ 
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],				 				 
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9]				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
		def datatablesStagnationLeadTimeGrafik(def params){
		final session = sessionFactory.currentSession
		String query = 
					" SELECT DAT.TGL_WO, SUM(DAT.JOB_STOPPED) JOB_STOPPED, SUM(DAT.RECEPTION) RECEPTION, SUM(DAT.PRODUCTION_JOB_DISPATCH) PRODUCTION_JOB_DISPATCH, "+
					" SUM(DAT.PRODUCTION_CLOCK_ON) PRODUCTION_CLOCK_ON, SUM(DAT.FINAL_INSPECTION) FINAL_INSPECTION, SUM(DAT.JOC) JOC, SUM(DAT.INVOICING) INVOICING, "+
					" SUM(DAT.WASHING) WASHING, SUM(DAT.NOTIFIKASI) NOTIFIKASI, SUM(DAT.SETTLEMENT) SETTLEMENT, SUM(DAT.DELIVERY) DELIVERY FROM( "+
					" SELECT "+ 
					" TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') AS TGL_WO, "+
					" NVL(CEIL((extract(DAY FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) * 24 * 60 * 60)+ "+ 
					" 		 (extract(HOUR FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai))), 0) AS Job_Stopped, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian) *  60 ) +  "+
					" 					  (extract(SECOND FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian))), 0) AS Reception, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO) *  60 ) + "+ 
					" 					  (extract(SECOND FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO))), 0) AS Production_Job_Dispatch, "+ 
					" NVL(CEIL((extract(DAY FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO) *  60 ) +  "+
					" 					  (extract(SECOND FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO))), 0) AS Production_Clock_On, "+
					" NVL(CEIL((extract(DAY FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM))), 0) AS Final_Inspection, "+
					" NVL(CEIL((extract(DAY FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC))), 0) AS JOC , "+
					" NVL(CEIL((extract(DAY FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai))), 0) AS Invoicing, "+
					" NVL(CEIL((extract(DAY FROM T602_ACTUALCUCI.T602_TglJam - T508_FINALINSPECTION.T508_TglJamSelesai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T602_ACTUALCUCI.T602_TglJam - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T602_ACTUALCUCI.T602_TglJam - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T602_ACTUALCUCI.T602_TglJam  - T508_FINALINSPECTION.T508_TglJamSelesai))), 0) AS Washing, "+
					" NVL(CEIL((extract(DAY FROM T701_INVOICE.T701_TglJamInvoice - T602_ACTUALCUCI.T602_TglJam) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T701_INVOICE.T701_TglJamInvoice - T602_ACTUALCUCI.T602_TglJam) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T701_INVOICE.T701_TglJamInvoice - T602_ACTUALCUCI.T602_TglJam) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T701_INVOICE.T701_TglJamInvoice  - T602_ACTUALCUCI.T602_TglJam))), 0) AS Notifikasi , "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TglJamExplanationSelesai - T704_SETTLEMENT.T704_TglJamSettlement) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM DAT.T401_TglJamExplanationSelesai - T704_SETTLEMENT.T704_TglJamSettlement) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.T401_TglJamExplanationSelesai - T704_SETTLEMENT.T704_TglJamSettlement) *  60 ) + "+ 
					" 					  (extract(SECOND FROM DAT.T401_TglJamExplanationSelesai  - T704_SETTLEMENT.T704_TglJamSettlement))), 0) AS Settlement, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TglJamExplanation - DAT.T401_TglJamNotifikasi) * 24 * 60 * 60)+ "+ 
					" 		 (extract(HOUR FROM DAT.T401_TglJamExplanation - DAT.T401_TglJamNotifikasi) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.T401_TglJamExplanation - DAT.T401_TglJamNotifikasi) *  60 ) + "+ 
					" 					  (extract(SECOND FROM DAT.T401_TglJamExplanation  - DAT.T401_TglJamNotifikasi))), 0) AS Delivery  "+
					" FROM "+
					" ( "+
					" SELECT  "+
					" T401_RECEPTION.*, "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID, "+
					" (SELECT T452_ACTUAL.ID FROM T452_ACTUAL WHERE T452_ACTUAL.T452_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS ACTUAL_ID "+
					" FROM  "+
					" T401_RECEPTION "+
					" ) DAT "+
					" INNER JOIN T400_CUSTOMERIN ON T400_CUSTOMERIN.ID = DAT.T401_T400_ID "+
					" LEFT JOIN T452_ACTUAL ON T452_ACTUAL.T452_T401_NOWO = DAT.ACTUAL_ID "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = DAT.ID "+
					" LEFT JOIN T508_FINALINSPECTION ON T508_FINALINSPECTION.T508_T401_NOWO = DAT.ID "+
					" LEFT JOIN T601_JOCTECO ON T601_JOCTECO.T601_T401_NOWO = DAT.ID "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID "+
					" LEFT JOIN T600_ANTRICUCI ON T600_ANTRICUCI.T600_T401_NOWO = DAT.ID "+ 
					" LEFT JOIN T602_ACTUALCUCI ON T602_ACTUALCUCI.T602_T600_ID = T600_ANTRICUCI.T600_ID "+
					" LEFT JOIN T704_SETTLEMENT ON  T704_SETTLEMENT.RECEPTION_ID = DAT.ID "+
					" ) DAT "+
					" GROUP BY DAT.TGL_WO "+ 
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[	
					column_0: resultRow[0],			 
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesStagnationLeadTimeTable(def params){
		final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yy");
        def tgl1 = df.parse(params.tanggal)
        def tgl2 = df.parse(params.tanggal2)

        String jenisPekerjaanFilter = ""
        if(params.jenisPekerjaan){
            jenisPekerjaanFilter = " AND JOB_ID IN("+params.jenisPekerjaan+")";
        }

		String query = 					
					" SELECT TO_CHAR(TGL_WO, 'DD-MON-YYYY') AS TGL," +
                            " SUM(RECEPTION)," +
                            " SUM(PREDIAGNOSE)," +
                            " SUM(PRODUCTION)," +
                            " SUM(IDR)," +
                            " SUM(FINAL_INSPECTION)," +
                            " SUM(JOC)," +
                            " SUM(INVOICING)," +
                            " SUM(WASHING)," +
                            " SUM(DELIVERY)," +
                            " SUM(JOB_STOPPED)," +
                            " SUM(STAGNATION_RECEPTION)," +
                            " SUM(STAGNATION_JOB_DISPATCH)," +
                            " SUM(STAGNATION_PRODUCTION_CLOCK_ON)," +
                            " SUM(STAGNATION_FINAL_INSPECTION)," +
                            " SUM(STAGNATION_JOC)," +
                            " SUM(STAGNATION_INVOICING)," +
                            " SUM(STAGNATION_WASHING)," +
                            " SUM(STAGNATION_NOTIFIKASI)," +
                            " SUM(STAGNATION_SETTLEMENT)," +
                            " SUM(STAGNATION_DELIVERY)," +
                            " ROUND((SUM(RECEPTION) + SUM(PREDIAGNOSE) + SUM(PRODUCTION) + SUM(IDR) + SUM(FINAL_INSPECTION) + SUM(JOC) + SUM(INVOICING) + SUM(WASHING) + SUM(DELIVERY))/9,0) AS AVGPRO," +
                            " " +
                            " ROUND((SUM(JOB_STOPPED) + SUM(STAGNATION_RECEPTION) + SUM(STAGNATION_JOB_DISPATCH) + SUM(STAGNATION_PRODUCTION_CLOCK_ON) + SUM(STAGNATION_FINAL_INSPECTION) + SUM(STAGNATION_JOC) +" +
                            " SUM(STAGNATION_INVOICING) + SUM(STAGNATION_WASHING) + SUM(STAGNATION_NOTIFIKASI) + SUM(STAGNATION_SETTLEMENT) + SUM(STAGNATION_DELIVERY))/11,0) AS AVGSTG," +
                            " " +
                            " (SUM(RECEPTION) + SUM(PREDIAGNOSE) + SUM(PRODUCTION) + SUM(IDR) + SUM(FINAL_INSPECTION) + SUM(JOC) + SUM(INVOICING) + SUM(WASHING) + SUM(DELIVERY)) AS TOTALPRO," +
                            " " +
                            " (SUM(JOB_STOPPED) + SUM(STAGNATION_RECEPTION) + SUM(STAGNATION_JOB_DISPATCH) + SUM(STAGNATION_PRODUCTION_CLOCK_ON) + SUM(STAGNATION_FINAL_INSPECTION) + SUM(STAGNATION_JOC) +" +
                            " SUM(STAGNATION_INVOICING) + SUM(STAGNATION_WASHING) + SUM(STAGNATION_NOTIFIKASI) + SUM(STAGNATION_SETTLEMENT) + SUM(STAGNATION_DELIVERY)) - (CASE WHEN SUM(STAGNATION_WASHING) <= (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) THEN SUM(STAGNATION_WASHING) ELSE (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) END) AS TOTSTG," +
                            " " +
                            " (SUM(RECEPTION) + SUM(PREDIAGNOSE) + SUM(PRODUCTION) + SUM(IDR) + SUM(FINAL_INSPECTION) + SUM(JOC) + SUM(INVOICING) + SUM(WASHING) + SUM(DELIVERY)) +" +
                            " (SUM(JOB_STOPPED) + SUM(STAGNATION_RECEPTION) + SUM(STAGNATION_JOB_DISPATCH) + SUM(STAGNATION_PRODUCTION_CLOCK_ON) + SUM(STAGNATION_FINAL_INSPECTION) + SUM(STAGNATION_JOC) +" +
                            " SUM(STAGNATION_INVOICING) + SUM(STAGNATION_WASHING) + SUM(STAGNATION_NOTIFIKASI) + SUM(STAGNATION_SETTLEMENT) + SUM(STAGNATION_DELIVERY)) - (CASE WHEN SUM(STAGNATION_WASHING) <= (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) THEN SUM(STAGNATION_WASHING) ELSE (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) END) AS TOTLEADTIME" +
                    " FROM R004_LEAD_TIME" +
                    " WHERE TO_DATE(TGL_WO,'DD-MM-YY') BETWEEN TO_DATE('"+df1.format(tgl1)+"', 'DD-MM-YY') AND TO_DATE('"+df1.format(tgl2)+"', 'DD-MM-YY') " +
                    " AND COMPANY_DEALER_ID = "+params.workshop+" " +
                    " "+jenisPekerjaanFilter+" " +
                    " GROUP BY TO_CHAR(TGL_WO, 'DD-MON-YYYY')" +
                    " ORDER BY TO_DATE(TO_CHAR(TGL_WO,'DD-MON-YYYY'),'DD-MON-YYYY') ASC";
		def results = null
        
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
                def hmax, maxSt = 0
                def n = 1
                def m = 10
                // Mencari MAX
                (0..7).each {
                    if (resultRow[n] >= hmax){
                        hmax = resultRow[n]
                    }
                    n++
                }

                (0..9).each{
                    if (resultRow[m] >= maxSt){
                        maxSt = resultRow[m]
                    }
                    m++
                }
				[	
					column_0: resultRow[0],
					column_1: konversi.secondToHMS(resultRow[1]),
					column_2: konversi.secondToHMS(resultRow[2]),
					column_3: konversi.secondToHMS(resultRow[3]),
					column_4: konversi.secondToHMS(resultRow[4]),
					column_5: konversi.secondToHMS(resultRow[5]),
					column_6: konversi.secondToHMS(resultRow[6]),
					column_7: konversi.secondToHMS(resultRow[7]),
					column_8: konversi.secondToHMS(resultRow[8]),
					column_9: konversi.secondToHMS(resultRow[9]),
					column_10: konversi.secondToHMS(resultRow[10]),
					column_11: konversi.secondToHMS(resultRow[11]),
					column_12: konversi.secondToHMS(resultRow[12]),
					column_13: konversi.secondToHMS(resultRow[13]),
					column_14: konversi.secondToHMS(resultRow[14]),
					column_15: konversi.secondToHMS(resultRow[15]),
					column_16: konversi.secondToHMS(resultRow[16]),
					column_17: konversi.secondToHMS(resultRow[17]),
					column_18: konversi.secondToHMS(resultRow[18]),
					column_19: konversi.secondToHMS(resultRow[19]),
					column_20: konversi.secondToHMS(resultRow[20]),
					column_21: konversi.secondToHMS(resultRow[21]),
					column_22: konversi.secondToHMS(resultRow[22]),
					column_23: konversi.secondToHMS(hmax),
					column_24: konversi.secondToHMS(maxSt),
					column_25: konversi.secondToHMS(resultRow[23]),
					column_26: konversi.secondToHMS(resultRow[24]),
					column_27: konversi.secondToHMS(resultRow[25])
				]
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesLeadTimeTotalGrafik(def params){
		final session = sessionFactory.currentSession
		String query = 
					" SELECT "+
					" DAT.TGL_WO, CEIL(AVG(DAT.RECEPTION)) RECEPTION, CEIL(AVG(DAT.PREDIAGNOSE)) PREDIAGNOSE, CEIL(AVG(DAT.PRODUCTION)) PRODUCTION, CEIL(AVG(DAT.INSPECTION_DURING_REPAIR)) INSPECTION_DURING_REPAIR, "+
					" CEIL(AVG(DAT.FINAL_INSPECTION)) FINAL_INSPECTION, CEIL(AVG(DAT.PROSES_JOC)) PROCESS_JOC, CEIL(AVG(DAT.INVOICING)) INVOICING, CEIL(AVG(DAT.DELIVERY)) DELIVERY, "+
					" CEIL(NVL(CEIL((extract(DAY FROM DAT.FINISH_WASHING - DAT.START_WASHING) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM DAT.FINISH_WASHING - DAT.START_WASHING) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.FINISH_WASHING - DAT.START_WASHING) *  60 ) + "+
					" 					  (extract(SECOND FROM DAT.FINISH_WASHING- DAT.START_WASHING))), 0)) AS Washing "+
					" FROM ( "+
					" SELECT "+ 
					" TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') AS TGL_WO, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception) * 24 * 60 * 60)+  "+
					" 		 (extract(HOUR FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T400_CUSTOMERIN.T400_TglJamSelesaiReception - T400_CUSTOMERIN.T400_TglJamReception))), 0) AS Reception , "+
					" NVL(CEIL((extract(DAY FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai) *  60 ) + "+
					" 					  (extract(SECOND FROM T406_PREDIAGNOSIS.T406_TglJamSelesai - T406_PREDIAGNOSIS.T406_TglJamMulai))), 0) AS PreDiagnose, "+
					" NVL(CEIL((extract(DAY FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai))), 0) AS Production, "+
					" NVL(CEIL((extract(DAY FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T506_IDR.T506_TglJamSelesai - T506_IDR.T506_TglJamMulai))), 0) AS Inspection_During_Repair, "+
					" NVL(CEIL((extract(DAY FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T508_FINALINSPECTION.T508_TglJamSelesai - T508_FINALINSPECTION.T508_TglJamMulai))), 0) AS Final_Inspection, "+
					" NVL(CEIL((extract(DAY FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 ) + "+  
					" 					  (extract(SECOND FROM T601_JOCTECO.T601_TglJamJOCSelesai - T601_JOCTECO.T601_TglJamJOC))), 0) AS Proses_Joc, "+
					" NVL(CEIL((extract(DAY FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI) * 24 * 60 * 60)+ "+ 
					" 		 (extract(HOUR FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T701_INVOICE.T701_TGLJAMINVOICE - T701_INVOICE.T701_TGLJAMMULAI))), 0) AS Invoicing, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TglJamOut - DAT.T401_TglJamExplanation) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T400_CUSTOMERIN.T400_TglJamOut -DAT.T401_TglJamExplanation) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T400_CUSTOMERIN.T400_TglJamOut - DAT.T401_TglJamExplanation) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T400_CUSTOMERIN.T400_TglJamOut - DAT.T401_TglJamExplanation))), 0) AS Delivery, "+
					" CASE  "+
					" 	 WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'start')  "+ 
					" 	 THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	 ELSE NULL  "+
					" END AS START_WASHING, "+                                         
					" CASE  "+
					" 	 WHEN T602_ACTUALCUCI.T602_M452_ID IN (SELECT M452_STATUSACTUAL.ID FROM M452_STATUSACTUAL WHERE LOWER(M452_STATUSACTUAL.M452_STATUSACTUAL) = 'finish') "+ 
					" 	 THEN T602_ACTUALCUCI.T602_TGLJAM  "+
					" 	 ELSE NULL  "+
					" END AS FINISH_WASHING "+
					" FROM ( "+
					" SELECT "+
					" T401_RECEPTION.*, "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID, "+
					" (SELECT T452_ACTUAL.ID FROM T452_ACTUAL WHERE T452_ACTUAL.T452_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS ACTUAL_ID "+
					" FROM  "+
					" T401_RECEPTION "+
					" ) DAT "+
					" INNER JOIN T400_CUSTOMERIN ON T400_CUSTOMERIN.ID = DAT.T401_T400_ID "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = DAT.ID "+
					" LEFT JOIN T452_ACTUAL ON T452_ACTUAL.T452_T401_NOWO = DAT.ACTUAL_ID "+
					" LEFT JOIN T506_IDR ON T506_IDR.T506_T401_NOWO = DAT.ID "+
					" LEFT JOIN T508_FINALINSPECTION ON T508_FINALINSPECTION.T508_T401_NOWO = DAT.ID "+
					" LEFT JOIN T601_JOCTECO ON T601_JOCTECO.T601_T401_NOWO = DAT.ID "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID "+
					" LEFT JOIN T600_ANTRICUCI ON T600_ANTRICUCI.T600_T401_NOWO = DAT.ID "+
					" LEFT JOIN T602_ACTUALCUCI ON T602_ACTUALCUCI.T602_T600_ID = T600_ANTRICUCI.T600_ID "+
					" ) DAT "+
					" GROUP BY DAT.TGL_WO, DAT.FINISH_WASHING, DAT.START_WASHING "+ 
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],				 				 
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9]				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesStagnationLeadTimeTotalGrafik(def params){
		final session = sessionFactory.currentSession
		String query = 
					" SELECT DAT.TGL_WO, CEIL(AVG(DAT.JOB_STOPPED)) JOB_STOPPED, CEIL(AVG(DAT.RECEPTION)) RECEPTION, CEIL(AVG(DAT.PRODUCTION_JOB_DISPATCH)) PRODUCTION_JOB_DISPATCH, "+
					" CEIL(AVG(DAT.PRODUCTION_CLOCK_ON)) PRODUCTION_CLOCK_ON, CEIL(AVG(DAT.FINAL_INSPECTION)) FINAL_INSPECTION, CEIL(AVG(DAT.JOC)) JOC, CEIL(AVG(DAT.INVOICING)) INVOICING, "+
					" CEIL(AVG(DAT.WASHING)) WASHING, CEIL(AVG(DAT.NOTIFIKASI)) NOTIFIKASI, CEIL(AVG(DAT.SETTLEMENT)) SETTLEMENT, CEIL(AVG(DAT.DELIVERY)) DELIVERY FROM( "+
					" SELECT "+ 
					" TO_CHAR(DAT.T401_TANGGALWO, 'DD-MON-YYYY') AS TGL_WO, "+
					" NVL(CEIL((extract(DAY FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) * 24 * 60 * 60)+ "+ 
					" 		 (extract(HOUR FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T452_ACTUAL.T452_TGLJAM - T406_PREDIAGNOSIS.T406_TglJamSelesai))), 0) AS Job_Stopped, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian) *  60 ) +  "+
					" 					  (extract(SECOND FROM T400_CUSTOMERIN.T400_TglJamReception - T400_CUSTOMERIN.T400_TglCetakNoAntrian))), 0) AS Reception, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO) *  60 ) + "+ 
					" 					  (extract(SECOND FROM DAT.T401_TglJamAmbilWO - DAT.T401_TanggalWO))), 0) AS Production_Job_Dispatch, "+ 
					" NVL(CEIL((extract(DAY FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO) *  60 ) +  "+
					" 					  (extract(SECOND FROM T452_ACTUAL.T452_TGLJAM - DAT.T401_TglJamAmbilWO))), 0) AS Production_Clock_On, "+
					" NVL(CEIL((extract(DAY FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T508_FINALINSPECTION.T508_TglJamMulai - T452_ACTUAL.T452_TGLJAM))), 0) AS Final_Inspection, "+
					" NVL(CEIL((extract(DAY FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T508_FINALINSPECTION.T508_TglJamSelesai - T601_JOCTECO.T601_TglJamJOC))), 0) AS JOC , "+
					" NVL(CEIL((extract(DAY FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T701_INVOICE.T701_TglJamMulai - T508_FINALINSPECTION.T508_TglJamSelesai))), 0) AS Invoicing, "+
					" NVL(CEIL((extract(DAY FROM T602_ACTUALCUCI.T602_TglJam - T508_FINALINSPECTION.T508_TglJamSelesai) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T602_ACTUALCUCI.T602_TglJam - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T602_ACTUALCUCI.T602_TglJam - T508_FINALINSPECTION.T508_TglJamSelesai) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T602_ACTUALCUCI.T602_TglJam  - T508_FINALINSPECTION.T508_TglJamSelesai))), 0) AS Washing, "+
					" NVL(CEIL((extract(DAY FROM T701_INVOICE.T701_TglJamInvoice - T602_ACTUALCUCI.T602_TglJam) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM T701_INVOICE.T701_TglJamInvoice - T602_ACTUALCUCI.T602_TglJam) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM T701_INVOICE.T701_TglJamInvoice - T602_ACTUALCUCI.T602_TglJam) *  60 ) + "+ 
					" 					  (extract(SECOND FROM T701_INVOICE.T701_TglJamInvoice  - T602_ACTUALCUCI.T602_TglJam))), 0) AS Notifikasi , "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TglJamExplanationSelesai - T704_SETTLEMENT.T704_TglJamSettlement) * 24 * 60 * 60)+ "+
					" 		 (extract(HOUR FROM DAT.T401_TglJamExplanationSelesai - T704_SETTLEMENT.T704_TglJamSettlement) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.T401_TglJamExplanationSelesai - T704_SETTLEMENT.T704_TglJamSettlement) *  60 ) + "+ 
					" 					  (extract(SECOND FROM DAT.T401_TglJamExplanationSelesai  - T704_SETTLEMENT.T704_TglJamSettlement))), 0) AS Settlement, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TglJamExplanation - DAT.T401_TglJamNotifikasi) * 24 * 60 * 60)+ "+ 
					" 		 (extract(HOUR FROM DAT.T401_TglJamExplanation - DAT.T401_TglJamNotifikasi) *  60 * 60) + "+
					" 			  (extract(MINUTE FROM DAT.T401_TglJamExplanation - DAT.T401_TglJamNotifikasi) *  60 ) + "+ 
					" 					  (extract(SECOND FROM DAT.T401_TglJamExplanation  - DAT.T401_TglJamNotifikasi))), 0) AS Delivery  "+
					" FROM "+
					" ( "+
					" SELECT  "+
					" T401_RECEPTION.*, "+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID, "+
					" (SELECT T452_ACTUAL.ID FROM T452_ACTUAL WHERE T452_ACTUAL.T452_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS ACTUAL_ID "+
					" FROM  "+
					" T401_RECEPTION "+
					" ) DAT "+
					" INNER JOIN T400_CUSTOMERIN ON T400_CUSTOMERIN.ID = DAT.T401_T400_ID "+
					" LEFT JOIN T452_ACTUAL ON T452_ACTUAL.T452_T401_NOWO = DAT.ACTUAL_ID "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = DAT.ID "+
					" LEFT JOIN T508_FINALINSPECTION ON T508_FINALINSPECTION.T508_T401_NOWO = DAT.ID "+
					" LEFT JOIN T601_JOCTECO ON T601_JOCTECO.T601_T401_NOWO = DAT.ID "+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID = DAT.INVOICE_ID "+
					" LEFT JOIN T600_ANTRICUCI ON T600_ANTRICUCI.T600_T401_NOWO = DAT.ID "+ 
					" LEFT JOIN T602_ACTUALCUCI ON T602_ACTUALCUCI.T602_T600_ID = T600_ANTRICUCI.T600_ID "+
					" LEFT JOIN T704_SETTLEMENT ON  T704_SETTLEMENT.RECEPTION_ID = DAT.ID "+
					" ) DAT "+
					" GROUP BY DAT.TGL_WO "+ 
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[	
					column_0: resultRow[0],			 
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}

    def datatablesStagnationLeadTimeTableMontly(def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("M-yyyy");
        DateFormat df1 = new SimpleDateFormat("MM-yyyy");
        def bulan1 = params.bulan1 + "-" + params.tahun
        def bulan2 = params.bulan2 + "-" + params.tahun
        def startDate = df.parse(bulan1);
        def endDate = df.parse(bulan2);

        String jenisPekerjaanFilter = ""
        if(params.jenisPekerjaan){
            jenisPekerjaanFilter = "AND JOB_ID IN("+params.jenisPekerjaan+")";
        }

        String query =
                " SELECT TO_CHAR(TGL_WO, 'YYYY')," +
                        " TO_CHAR(TGL_WO, 'MONTH')," +
                        " SUM(RECEPTION)," +
                        " SUM(PREDIAGNOSE)," +
                        " SUM(PRODUCTION)," +
                        " SUM(IDR)," +
                        " SUM(FINAL_INSPECTION)," +
                        " SUM(JOC)," +
                        " SUM(INVOICING)," +
                        " SUM(WASHING)," +
                        " SUM(DELIVERY)," +
                        " SUM(JOB_STOPPED)," +
                        " SUM(STAGNATION_RECEPTION)," +
                        " SUM(STAGNATION_JOB_DISPATCH)," +
                        " SUM(STAGNATION_PRODUCTION_CLOCK_ON)," +
                        " SUM(STAGNATION_FINAL_INSPECTION)," +
                        " SUM(STAGNATION_JOC)," +
                        " SUM(STAGNATION_INVOICING)," +
                        " SUM(STAGNATION_WASHING)," +
                        " SUM(STAGNATION_NOTIFIKASI)," +
                        " SUM(STAGNATION_SETTLEMENT)," +
                        " SUM(STAGNATION_DELIVERY)," +
                        " ROUND((SUM(RECEPTION) + SUM(PREDIAGNOSE) + SUM(PRODUCTION) + SUM(IDR) + SUM(FINAL_INSPECTION) + SUM(JOC) + SUM(INVOICING) + SUM(WASHING) + SUM(DELIVERY))/9,0) AS AVGPRO," +
                        " ROUND((SUM(JOB_STOPPED) + SUM(STAGNATION_RECEPTION) + SUM(STAGNATION_JOB_DISPATCH) + SUM(STAGNATION_PRODUCTION_CLOCK_ON) + SUM(STAGNATION_FINAL_INSPECTION) + SUM(STAGNATION_JOC) +" +
                        "      SUM(STAGNATION_INVOICING) + SUM(STAGNATION_WASHING) + SUM(STAGNATION_NOTIFIKASI) + SUM(STAGNATION_SETTLEMENT) + SUM(STAGNATION_DELIVERY))/11,0) AS AVGSTG," +
                        " (SUM(RECEPTION) + SUM(PREDIAGNOSE) + SUM(PRODUCTION) + SUM(IDR) + SUM(FINAL_INSPECTION) + SUM(JOC) + SUM(INVOICING) + SUM(WASHING) + SUM(DELIVERY)) AS TOTALPRO," +
                        " (SUM(JOB_STOPPED) + SUM(STAGNATION_RECEPTION) + SUM(STAGNATION_JOB_DISPATCH) + SUM(STAGNATION_PRODUCTION_CLOCK_ON) + SUM(STAGNATION_FINAL_INSPECTION) + SUM(STAGNATION_JOC) + " +
                        "  SUM(STAGNATION_INVOICING) + SUM(STAGNATION_WASHING) + SUM(STAGNATION_NOTIFIKASI) + SUM(STAGNATION_SETTLEMENT) + SUM(STAGNATION_DELIVERY)) - (CASE WHEN SUM(STAGNATION_WASHING) <= (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) THEN SUM(STAGNATION_WASHING) ELSE (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) END) AS TOTSTG," +
                        " (SUM(RECEPTION) + SUM(PREDIAGNOSE) + SUM(PRODUCTION) + SUM(IDR) + SUM(FINAL_INSPECTION) + SUM(JOC) + SUM(INVOICING) + SUM(WASHING) + SUM(DELIVERY)) +" +
                        "      (SUM(JOB_STOPPED) + SUM(STAGNATION_RECEPTION) + SUM(STAGNATION_JOB_DISPATCH) + SUM(STAGNATION_PRODUCTION_CLOCK_ON) + SUM(STAGNATION_FINAL_INSPECTION) + SUM(STAGNATION_JOC) +" +
                        "      SUM(STAGNATION_INVOICING) + SUM(STAGNATION_WASHING) + SUM(STAGNATION_NOTIFIKASI) + SUM(STAGNATION_SETTLEMENT) + SUM(STAGNATION_DELIVERY)) - (CASE WHEN SUM(STAGNATION_WASHING) <= (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) THEN SUM(STAGNATION_WASHING) ELSE (SUM(STAGNATION_JOC) + SUM(STAGNATION_INVOICING)) END) AS TOTLEADTIME" +
                        " FROM R004_LEAD_TIME" +
                        " WHERE TO_CHAR(TGL_WO,'MM-YYYY') BETWEEN '"+df1.format(startDate)+"' AND '"+df1.format(endDate)+"' " +
                        " AND COMPANY_DEALER_ID = "+params.workshop+" " +
                        " "+jenisPekerjaanFilter+" " +
                        " GROUP BY TO_CHAR(TGL_WO, 'YYYY'), TO_CHAR(TGL_WO, 'MONTH')" +
                        " ORDER BY 1, 2 ";
        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            results = queryResults.collect { resultRow ->
                def hmax, maxSt = 0
                def n = 2
                def m = 11
                // Mencari MAX
                (0..8).each {
                    if (resultRow[n] >= hmax){
                        hmax = resultRow[n]
                    }
                    n++
                }

                (0..10).each{
                    if (resultRow[m] >= maxSt){
                        maxSt = resultRow[m]
                    }
                    m++
                }
                // Mencari MAX
                [
                        column_0: resultRow[0],
                        column_1: resultRow[1],
                        column_2: konversi.secondToHMS(resultRow[2]),
                        column_3: konversi.secondToHMS(resultRow[3]),
                        column_4: konversi.secondToHMS(resultRow[4]),
                        column_5: konversi.secondToHMS(resultRow[5]),
                        column_6: konversi.secondToHMS(resultRow[6]),
                        column_7: konversi.secondToHMS(resultRow[7]),
                        column_8: konversi.secondToHMS(resultRow[8]),
                        column_9: konversi.secondToHMS(resultRow[9]),
                        column_10: konversi.secondToHMS(resultRow[10]),
                        column_11: konversi.secondToHMS(resultRow[11]),
                        column_12: konversi.secondToHMS(resultRow[12]),
                        column_13: konversi.secondToHMS(resultRow[13]),
                        column_14: konversi.secondToHMS(resultRow[14]),
                        column_15: konversi.secondToHMS(resultRow[15]),
                        column_16: konversi.secondToHMS(resultRow[16]),
                        column_17: konversi.secondToHMS(resultRow[17]),
                        column_18: konversi.secondToHMS(resultRow[18]),
                        column_19: konversi.secondToHMS(resultRow[19]),
                        column_20: konversi.secondToHMS(resultRow[20]),
                        column_21: konversi.secondToHMS(resultRow[21]),
                        column_22: konversi.secondToHMS(resultRow[22]),
                        column_23: konversi.secondToHMS(resultRow[23]),
                        column_24: konversi.secondToHMS(hmax),
                        column_25: konversi.secondToHMS(maxSt),
                        column_26: konversi.secondToHMS(resultRow[24]),
                        column_27: konversi.secondToHMS(resultRow[25]),
                        column_28: konversi.secondToHMS(resultRow[26])
                ]
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        //System.out.//println(results.size());
        if(results.size() <= 0) {
            throw new java.lang.Exception("Result size 0");
        }
        return results
    }
}
 
