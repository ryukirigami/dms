package com.kombos.kriteriaReport

import com.kombos.woinformation.JobRCP

class TimeTrackingService {
	boolean transactional = false

	def sessionFactory

	def datatablesSAList(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		String query = " SELECT ID, T015_NAMALENGKAP FROM T015_NAMAMANPOWER WHERE T015_STADEL = '0' ";
		if (params."sa") {
			query += " AND LOWER(T015_NAMALENGKAP) LIKE '%" + (params."sa" as String) + "%'  ";
		}		
		query += " ORDER BY T015_NAMALENGKAP " +sortDir+ " ";
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.dump();
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaSa: resultRow[1]
			]
			 
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: results]
		return ret
	}
	
	def datatablesTechnician(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		String query = " SELECT ID, T015_NAMALENGKAP FROM T015_NAMAMANPOWER WHERE T015_STADEL = '0' ";
		if (params."sa") {
			query += " AND LOWER(T015_NAMALENGKAP) LIKE '%" + (params."sa" as String) + "%'  ";
		}		
		query += " ORDER BY T015_NAMALENGKAP " +sortDir+ " ";
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.dump();
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaSa: resultRow[1]
			]
			 
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: results]
		return ret
	}

	def datatablesJenisPekerjaan(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M401_NAMATIPEKERUSAKAN FROM M401_TIPEKERUSAKAN WHERE M401_STADEL = '0' ";
		if (params."pekerjaan") {
			query += " AND LOWER(M401_NAMATIPEKERUSAKAN) LIKE '%" + (params."pekerjaan" as String) + "%'  ";
		}		
		query += " ORDER BY M401_NAMATIPEKERUSAKAN " +sortDir+ " ";
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaPekerjaan: resultRow[1]
			]
			 
		}		
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: results]
		return ret
	}
	
	def datatablesJenisKendaraan(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		String query = " SELECT M102_BASEMODEL.ID, M102_BASEMODEL.M102_NAMABASEMODEL  FROM M102_BASEMODEL WHERE M102_BASEMODEL.M102_STADEL = '0' ";
		if (params."kendaraan") {
			query += " AND LOWER(M102_BASEMODEL.M102_NAMABASEMODEL) LIKE '%" + (params."kendaraan" as String) + "%'  ";
		}		
		query += " ORDER BY M102_BASEMODEL.M102_NAMABASEMODEL " +sortDir+ " ";
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaKendaraan: resultRow[1]
			]
			 
		}		
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: results]
		return ret
	}
	
	def datatablesJenisPayment(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = JobRCP.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if (params."payment") {
				ilike("t402StaCashInsurance", "%" + (params."payment" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPayment: it.t402StaCashInsurance == '1' ? 'Cash' : 'Insurance'

			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret
	}

	
	def datatablesTimeTrackingGR(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results	
	}
	
	def datatablesTimeTrackingBPDetail(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesTimeTrackingBPDetailProduction(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesTimeTrackingBPCenterDetail(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesTimeTrackingBPCenterDetailReception(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesTimeTrackingBPCenterDetailProduction(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
	
	def datatablesTimeTrackingBPCenterDetailDelivery(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT SYSDATE FROM DUAL WHERE 1!=1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println(results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results
	}
}
 
