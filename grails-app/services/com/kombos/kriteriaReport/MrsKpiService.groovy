package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import java.util.Map
import java.util.HashMap

class MrsKpiService {
	boolean transactional = false;
	def sessionFactory;
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}
	
	def datatablesCustomerAppointmentSaatDitelp(def params){
		final session = sessionFactory.currentSession;
		String query = "" +		
			" SELECT "+
			" A.DESCRIPTION, "+
			" SUM(A.JANUARI) JANUARI, "+
			" SUM(A.FEBRUARI) FEBRUARI, "+
			" SUM(A.MARET) MARET, "+
			" SUM(A.APRIL) APRIL, "+
			" SUM(A.MEI) MEI, "+
			" SUM(A.JUNI) JUNI, "+
			" SUM(A.JULI) JULI, "+
			" SUM(A.AGUSTUS) AGUSTUS, "+
			" SUM(A.SEPTEMBER) SEPTEMBER, "+
			" SUM(A.OKTOBER) OKTOBER, "+
			" SUM(A.NOVEMBER) NOVEMBER, "+
			" SUM(A.DESEMBER) DESEMBER "+
			" FROM ( "+
			" SELECT "+
			" ' - Jumlah Customer Yang Appointment Saat Ditelp (Actual)' DESCRIPTION, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '01-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS JANUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '02-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS FEBRUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '03-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS MARET, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '04-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS APRIL, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '05-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS MEI, "+
			" CASE  "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '06-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS JUNI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '07-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS JULI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '08-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS AGUSTUS, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '09-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS SEPTEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '10-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS OKTOBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '11-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS NOVEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '12-2015' AND A.T202_STALANGSUNGAPPOINTMENT > 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS DESEMBER "+
			" FROM ( "+
			" SELECT * FROM ( "+
			" SELECT TO_CHAR(A.DATE_CREATED, 'DD-MM-YYYY') DATE_CREATED, A.T202_T201_ID REMINDER, A.T202_STALANGSUNGAPPOINTMENT FROM T202_INVITATION A "+
			" INNER JOIN T201_REMINDER B ON B.T201ID = A.T202_T201_ID AND TO_CHAR(B.T201_TGLCALL, 'YYYY') = '2015' "+
			" ) A "+
			" GROUP BY A.DATE_CREATED, A.REMINDER, A.T202_STALANGSUNGAPPOINTMENT "+
			" ) A "+
			" ) A  "+
			" GROUP BY A.DESCRIPTION "+
			"";
			
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12]					 
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		////System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def datatablesCustomerAppointmentSetelahDitelp(def params){
		final session = sessionFactory.currentSession;
		String query = "" +		
			" SELECT "+
			" A.DESCRIPTION, "+
			" SUM(A.JANUARI) JANUARI, "+
			" SUM(A.FEBRUARI) FEBRUARI, "+
			" SUM(A.MARET) MARET, "+
			" SUM(A.APRIL) APRIL, "+
			" SUM(A.MEI) MEI, "+
			" SUM(A.JUNI) JUNI, "+
			" SUM(A.JULI) JULI, "+
			" SUM(A.AGUSTUS) AGUSTUS, "+
			" SUM(A.SEPTEMBER) SEPTEMBER, "+
			" SUM(A.OKTOBER) OKTOBER, "+
			" SUM(A.NOVEMBER) NOVEMBER, "+
			" SUM(A.DESEMBER) DESEMBER "+
			" FROM ( "+
			" SELECT "+
			" ' - Jumlah Customer Yang Appointment Saat Ditelp (Actual)' DESCRIPTION, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '01-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS JANUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '02-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS FEBRUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '03-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS MARET, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '04-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS APRIL, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '05-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS MEI, "+
			" CASE  "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '06-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS JUNI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '07-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS JULI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '08-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS AGUSTUS, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '09-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS SEPTEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '10-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS OKTOBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '11-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS NOVEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '12-2015' AND A.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			" 	ELSE 0 "+
			" END AS DESEMBER "+
			" FROM ( "+
			" SELECT * FROM ( "+
			" SELECT TO_CHAR(A.DATE_CREATED, 'DD-MM-YYYY') DATE_CREATED, A.T202_T201_ID REMINDER, A.T202_STALANGSUNGAPPOINTMENT FROM T202_INVITATION A "+
			" INNER JOIN T201_REMINDER B ON B.T201ID = A.T202_T201_ID AND TO_CHAR(B.T201_TGLCALL, 'YYYY') = '2015' "+
			" ) A "+
			" GROUP BY A.DATE_CREATED, A.REMINDER, A.T202_STALANGSUNGAPPOINTMENT "+
			" ) A "+
			" ) A  "+
			" GROUP BY A.DESCRIPTION "+
			"";
			
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12]					 
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def datatablesPlans(def params, def type){
		def query = ""+
			" SELECT "+
			" A.DESCRIPTION, "+
			" SUM(A.JANUARI) JANUARI, "+
			" SUM(A.FEBRUARI) FEBRUARI, "+
			" SUM(A.MARET) MARET, "+
			" SUM(A.APRIL) APRIL, "+
			" SUM(A.MEI) MEI, "+
			" SUM(A.JUNI) JUNI, "+
			" SUM(A.JULI) JULI, "+
			" SUM(A.AGUSTUS) AGUSTUS, "+
			" SUM(A.SEPTEMBER) SEPTEMBER, "+
			" SUM(A.OKTOBER) OKTOBER, "+
			" SUM(A.NOVEMBER) NOVEMBER, "+
			" SUM(A.DESEMBER) DESEMBER "+
			" FROM ( "+
			" SELECT "+
			" '' DESCRIPTION, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '01-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS JANUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '02-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS FEBRUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '03-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS MARET, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '04-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS APRIL, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '05-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS MEI, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '06-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS JUNI, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '07-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS JULI, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '08-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS AGUSTUS, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '09-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS SEPTEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '10-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS OKTOBER, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '11-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS NOVEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(A.DATE_CREATED, 'MM-YYYY') = '12-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS DESEMBER "+
			" FROM T201_REMINDER A "+
			"";
		if(type == 0){
			query += ""+				
				" WHERE TO_CHAR(A.DATE_CREATED, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DESCRIPTION "+
				"";
		} else
		if(type == 1 || type == 2){
			query += ""+
				" WHERE TO_CHAR(A.T201_TGLDM, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DESCRIPTION "+ 
				"";
		} else
		if(type == 3 || type == 4){
			query += ""+
				" WHERE TO_CHAR(A.T201_TGLSMS, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DESCRIPTION "+
				"";
		} else
		if(type == 5 || type == 7 || type == 8){
			query += ""+
				" WHERE TO_CHAR(A.T201_TGLCALL, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DESCRIPTION "+
				"";
		}
		final session = sessionFactory.currentSession;
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12]					 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def datatablesActuals(def params, def type){
		def query = ""+
			" SELECT A.DESCRIPTION, "+ 
			" SUM(A.JANUARI) JANUARI, "+
			" SUM(A.FEBRUARI) FEBRUARI, "+
			" SUM(A.MARET) MARET, "+
			" SUM(A.APRIL) APRIL, "+
			" SUM(A.MEI) MEI, "+
			" SUM(A.JUNI) JUNI, "+
			" SUM(A.JULI) JULI, "+
			" SUM(A.AGUSTUS) AGUSTUS, "+
			" SUM(A.SEPTEMBER) SEPTEMBER, "+
			" SUM(A.OKTOBER) OKTOBER, "+
			" SUM(A.NOVEMBER) NOVEMBER, "+
			" SUM(A.DESEMBER) DESEMBER "+
			" FROM ( "+
			" SELECT "+
			" '' DESCRIPTION, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '01-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS JANUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '02-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS FEBRUARI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '03-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS MARET, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '04-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS APRIL, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '05-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS MEI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '06-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS JUNI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '07-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS JULI, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '08-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS AGUSTUS, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '09-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS SEPTEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '10-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS OKTOBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '11-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS NOVEMBER, "+
			" CASE "+
			"   WHEN TO_CHAR(TO_DATE(A.DATE_CREATED, 'DD-MM-YYYY'), 'MM-YYYY') = '12-2015' THEN 1 "+
			"   ELSE 0 "+
			" END AS DESEMBER "+
			" FROM ( "+
			"";
		if(type == 0){
			query += ""+	
				" SELECT * FROM ( "+
				" SELECT TO_CHAR(A.DATE_CREATED, 'DD-MM-YYYY') DATE_CREATED, A.T202_T201_ID REMINDER FROM T202_INVITATION A "+
				" WHERE TO_CHAR(A.DATE_CREATED, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DATE_CREATED, A.REMINDER "+
				" ) A "+
				" ) A "+
				" GROUP BY A.DESCRIPTION "+
				"";
		} else 
		if(type == 1 || type == 2){
			query += ""+
				" SELECT * FROM ( "+
				" SELECT TO_CHAR(A.DATE_CREATED, 'DD-MM-YYYY') DATE_CREATED, A.T202_T201_ID REMINDER FROM T202_INVITATION A "+
				" INNER JOIN T201_REMINDER B ON B.T201ID = A.T202_T201_ID AND TO_CHAR(B.T201_TGLDM, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DATE_CREATED, A.REMINDER "+
				" ) A "+
				" ) A "+ 
				" GROUP BY A.DESCRIPTION "+
				"";
		} else 
		if(type == 3 || type == 4){
			query += ""+
				" SELECT * FROM ( "+
				" SELECT TO_CHAR(A.DATE_CREATED, 'DD-MM-YYYY') DATE_CREATED, A.T202_T201_ID REMINDER FROM T202_INVITATION A  "+
				" INNER JOIN T201_REMINDER B ON B.T201ID = A.T202_T201_ID AND TO_CHAR(B.T201_TGLSMS, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DATE_CREATED, A.REMINDER "+
				" ) A "+
				" ) A GROUP BY A.DESCRIPTION "+
				"";
		} else
		if(type == 5){
			query += ""+
				" SELECT * FROM ( "+
				" SELECT TO_CHAR(A.DATE_CREATED, 'DD-MM-YYYY') DATE_CREATED, A.T202_T201_ID REMINDER FROM T202_INVITATION A "+
				" INNER JOIN T201_REMINDER B ON B.T201ID = A.T202_T201_ID AND TO_CHAR(B.T201_TGLCALL, 'YYYY') = '2015' "+
				" ) A "+
				" GROUP BY A.DATE_CREATED, A.REMINDER "+
				" ) A "+
				" ) A  "+
				" GROUP BY A.DESCRIPTION "+
				"";
		}
		final session = sessionFactory.currentSession;
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],
					 column_6: resultRow[6],
					 column_7: resultRow[7],
					 column_8: resultRow[8],
					 column_9: resultRow[9],
					 column_10: resultRow[10],
					 column_11: resultRow[11],
					 column_12: resultRow[12]					 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def summaryDirectEmail(def params){
		def query = ""+
			" SELECT "+
			" A.TGLDM, SUM(A.PLAN) PLAN, SUM(A.SENT) SENT, SUM(A.RECEIVE) RECEIVE,  (SUM(A.SENT) / SUM(A.PLAN) * 100) PERCENTSENT, (SUM(A.RECEIVE) / SUM(A.PLAN) * 100) PERCENTRECEIVE "+
			" FROM ( "+
			" SELECT "+
			" TO_CHAR(A.TGLDM, 'DD-MON-YYYY') TGLDM, "+
			" CASE"+
			"   WHEN A.TGLDM IS NOT NULL THEN 1 "+
			"   ELSE 0"+
			" END PLAN,"+
			" CASE"+
			"   WHEN A.TGLJAMKIRIM IS NOT NULL THEN 1"+
			"   ELSE 0"+
			" END SENT,"+
			" CASE"+
			"   WHEN A.RECEIVE = '1' THEN 1"+
			"   ELSE 0"+
			" END RECEIVE"+
			" FROM ("+
			" SELECT A.T201_TGLDM TGLDM, B.T202_TGLJAMKIRIM TGLJAMKIRIM, SUBSTR(B.T202_STAINVITATION, 6, 1) RECEIVE FROM T201_REMINDER A "+
			" LEFT JOIN T202_INVITATION B ON B.T202_T201_ID = A.T201ID"+
			" WHERE A.T201_TGLDM IS NOT NULL"+
			" ) A"+
			" ) A"+
			" GROUP BY A.TGLDM"+
			" ORDER BY A.TGLDM"+
			"";		
		final session = sessionFactory.currentSession;
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5]					 				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def detailDirectEmail(def params){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "Imam Akbar Arifaldi");
		map.put("column_2", "Bogor");
		map.put("column_3", "085695113565");
		map.put("column_4", "79886545");
		map.put("column_5", "F1234A");
		map.put("column_6", "Avanza");
		map.put("column_7", "2003");
		map.put("column_8", "20 K");
		map.put("column_9", "01-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "B");
		map.put("column_2", "Jakarta");
		map.put("column_3", "8713");
		map.put("column_4", "4546");
		map.put("column_5", "B2234B");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2005");
		map.put("column_8", "50 K");
		map.put("column_9", "01-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "C");
		map.put("column_2", "Depok");
		map.put("column_3", "8714");
		map.put("column_4", "4547");
		map.put("column_5", "B460ES");
		map.put("column_6", "Camry");
		map.put("column_7", "2007");
		map.put("column_8", "10 K");
		map.put("column_9", "-");
		map.put("column_10", "No");
		map.put("column_11", "-");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "D");
		map.put("column_2", "Depok");
		map.put("column_3", "8715");
		map.put("column_4", "4548");
		map.put("column_5", "B3L472");
		map.put("column_6", "Fortuner");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "E");
		map.put("column_2", "Depok");
		map.put("column_3", "8716");
		map.put("column_4", "4549");
		map.put("column_5", "B3L473");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "F");
		map.put("column_2", "Depok");
		map.put("column_3", "8717");
		map.put("column_4", "4550");
		map.put("column_5", "B3L474");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "03-Feb-2015");
		map.put("column_1", "G");
		map.put("column_2", "Depok");
		map.put("column_3", "8718");
		map.put("column_4", "4551");
		map.put("column_5", "B3L475");
		map.put("column_6", "Toyota Avanza");
		map.put("column_7", "2005");
		map.put("column_8", "10 K");
		map.put("column_9", "03-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		return list;
	}
	
	def summaryDirectSms(def params){
		def query = ""+
			" SELECT "+
			" A.TGLSMS, SUM(A.PLAN) PLAN, SUM(A.SENT) SENT, SUM(A.RECEIVE) RECEIVE,  TO_CHAR((SUM(A.SENT) / SUM(A.PLAN) * 100), '999,999,999.99') PERCENTSENT, (SUM(A.RECEIVE) / SUM(A.PLAN) * 100) PERCENTRECEIVE "+
			" FROM ( "+
			" SELECT "+
			" TO_CHAR(A.TGLSMS, 'DD-MON-YYYY') TGLSMS, "+
			" CASE"+
			"   WHEN A.TGLSMS IS NOT NULL THEN 1 "+
			"   ELSE 0"+
			" END PLAN,"+
			" CASE"+
			"   WHEN A.TGLJAMKIRIM IS NOT NULL THEN 1"+
			"   ELSE 0"+
			" END SENT,"+
			" CASE"+
			"   WHEN A.RECEIVE = '1' THEN 1"+
			"   ELSE 0"+
			" END RECEIVE"+
			" FROM ("+
			" SELECT A.T201_TGLSMS TGLSMS, B.T202_TGLJAMKIRIM TGLJAMKIRIM, SUBSTR(B.T202_STAINVITATION, 6, 1) RECEIVE FROM T201_REMINDER A "+
			" LEFT JOIN T202_INVITATION B ON B.T202_T201_ID = A.T201ID"+
			" WHERE A.T201_TGLSMS IS NOT NULL"+
			" ) A"+
			" ) A"+
			" GROUP BY A.TGLSMS"+
			" ORDER BY A.TGLSMS"+
			"";		
		final session = sessionFactory.currentSession;
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5]					 				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def detailDirectSms(def params){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "Imam Akbar Arifaldi");
		map.put("column_2", "Bogor");
		map.put("column_3", "085695113565");
		map.put("column_4", "79886545");
		map.put("column_5", "F1234A");
		map.put("column_6", "Avanza");
		map.put("column_7", "2003");
		map.put("column_8", "20 K");
		map.put("column_9", "01-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "B");
		map.put("column_2", "Jakarta");
		map.put("column_3", "8713");
		map.put("column_4", "4546");
		map.put("column_5", "B2234B");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2005");
		map.put("column_8", "50 K");
		map.put("column_9", "01-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "C");
		map.put("column_2", "Depok");
		map.put("column_3", "8714");
		map.put("column_4", "4547");
		map.put("column_5", "B460ES");
		map.put("column_6", "Camry");
		map.put("column_7", "2007");
		map.put("column_8", "10 K");
		map.put("column_9", "-");
		map.put("column_10", "No");
		map.put("column_11", "-");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "D");
		map.put("column_2", "Depok");
		map.put("column_3", "8715");
		map.put("column_4", "4548");
		map.put("column_5", "B3L472");
		map.put("column_6", "Fortuner");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "E");
		map.put("column_2", "Depok");
		map.put("column_3", "8716");
		map.put("column_4", "4549");
		map.put("column_5", "B3L473");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "F");
		map.put("column_2", "Depok");
		map.put("column_3", "8717");
		map.put("column_4", "4550");
		map.put("column_5", "B3L474");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "03-Feb-2015");
		map.put("column_1", "G");
		map.put("column_2", "Depok");
		map.put("column_3", "8718");
		map.put("column_4", "4551");
		map.put("column_5", "B3L475");
		map.put("column_6", "Toyota Avanza");
		map.put("column_7", "2005");
		map.put("column_8", "10 K");
		map.put("column_9", "03-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		return list;
	}
	
	def summaryDirectCall(def params){
		def query = ""+
			" SELECT "+
			" A.TGLCALL, SUM(A.PLAN) PLAN, SUM(A.SENT) SENT, SUM(A.RECEIVE) RECEIVE,  TO_CHAR((SUM(A.SENT) / SUM(A.PLAN) * 100), '999,999,999.99') PERCENTSENT, (SUM(A.RECEIVE) / SUM(A.PLAN) * 100) PERCENTRECEIVE "+
			" FROM ( "+
			" SELECT "+
			" TO_CHAR(A.TGLCALL, 'DD-MON-YYYY') TGLCALL, "+
			" CASE"+
			"   WHEN A.TGLCALL IS NOT NULL THEN 1 "+
			"   ELSE 0"+
			" END PLAN,"+
			" CASE"+
			"   WHEN A.TGLJAMKIRIM IS NOT NULL THEN 1"+
			"   ELSE 0"+
			" END SENT,"+
			" CASE"+
			"   WHEN A.RECEIVE = '1' THEN 1"+
			"   ELSE 0"+
			" END RECEIVE"+
			" FROM ("+
			" SELECT A.T201_TGLCALL TGLCALL, B.T202_TGLJAMKIRIM TGLJAMKIRIM, SUBSTR(B.T202_STAINVITATION, 6, 1) RECEIVE FROM T201_REMINDER A "+
			" LEFT JOIN T202_INVITATION B ON B.T202_T201_ID = A.T201ID"+
			" WHERE A.T201_TGLCALL IS NOT NULL"+
			" ) A"+
			" ) A"+
			" GROUP BY A.TGLCALL"+
			" ORDER BY A.TGLCALL	"+	
			"";		
		final session = sessionFactory.currentSession;
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5]					 				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def detailDirectCall(def params){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "Imam Akbar Arifaldi");
		map.put("column_2", "Bogor");
		map.put("column_3", "085695113565");
		map.put("column_4", "79886545");
		map.put("column_5", "F1234A");
		map.put("column_6", "Avanza");
		map.put("column_7", "2003");
		map.put("column_8", "20 K");
		map.put("column_9", "01-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "B");
		map.put("column_2", "Jakarta");
		map.put("column_3", "8713");
		map.put("column_4", "4546");
		map.put("column_5", "B2234B");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2005");
		map.put("column_8", "50 K");
		map.put("column_9", "01-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "C");
		map.put("column_2", "Depok");
		map.put("column_3", "8714");
		map.put("column_4", "4547");
		map.put("column_5", "B460ES");
		map.put("column_6", "Camry");
		map.put("column_7", "2007");
		map.put("column_8", "10 K");
		map.put("column_9", "-");
		map.put("column_10", "No");
		map.put("column_11", "-");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "D");
		map.put("column_2", "Depok");
		map.put("column_3", "8715");
		map.put("column_4", "4548");
		map.put("column_5", "B3L472");
		map.put("column_6", "Fortuner");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "E");
		map.put("column_2", "Depok");
		map.put("column_3", "8716");
		map.put("column_4", "4549");
		map.put("column_5", "B3L473");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "F");
		map.put("column_2", "Depok");
		map.put("column_3", "8717");
		map.put("column_4", "4550");
		map.put("column_5", "B3L474");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "02-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "03-Feb-2015");
		map.put("column_1", "G");
		map.put("column_2", "Depok");
		map.put("column_3", "8718");
		map.put("column_4", "4551");
		map.put("column_5", "B3L475");
		map.put("column_6", "Toyota Avanza");
		map.put("column_7", "2005");
		map.put("column_8", "10 K");
		map.put("column_9", "03-Feb-2015");
		map.put("column_10", "Yes");
		map.put("column_11", "Delivered");
		list.add(map);
		return list;
	}
	
	def summaryAppointment(def params){
		def query = ""+			
			" SELECT "+
			" TO_CHAR(A.TGLCALL, 'DD-MON-YYYY') TGLCALL,"+
			" SUM(A.CONNECTEDCALL) CONNECTEDCALL,"+
			" SUM(A.APPOINTMENTATCALL) APPOINTMENTATCALL,"+
			" SUM(A.APPOINTMENTAFTERCALL) APPOINTMENTAFTERCALL,"+
			" (SUM(A.APPOINTMENTATCALL) + SUM(A.APPOINTMENTAFTERCALL)) APPOINTMENTTOTAL,"+
			" TO_CHAR((SUM(A.APPOINTMENTATCALL) / (SUM(A.APPOINTMENTATCALL) + SUM(A.APPOINTMENTAFTERCALL)) * 100), '999,999,999.99') PERCENTATCALL,"+
			" TO_CHAR((SUM(A.APPOINTMENTAFTERCALL) / (SUM(A.APPOINTMENTATCALL) + SUM(A.APPOINTMENTAFTERCALL)) * 100), '999,999,999.99') PERCENTAFTERCALL,"+
			" TO_CHAR((SUM(A.APPOINTMENTATCALL) / (SUM(A.APPOINTMENTATCALL) + SUM(A.APPOINTMENTAFTERCALL)) * 100) + (SUM(A.APPOINTMENTAFTERCALL) / (SUM(A.APPOINTMENTATCALL) + SUM(A.APPOINTMENTAFTERCALL)) * 100), '999,999,999.99') PERCENTTOTAL"+
			" FROM ("+
			" SELECT"+
			" A.TGLCALL,"+
			" CASE"+
			"   WHEN A.CONNECTEDCALL IS NOT NULL THEN 1"+
			"   ELSE 0"+
			" END CONNECTEDCALL,"+
			" CASE"+
			"   WHEN B.T202_STALANGSUNGAPPOINTMENT = 1 THEN 1 "+
			"   ELSE 0"+
			" END APPOINTMENTATCALL,"+
			" CASE"+
			"   WHEN B.T202_STALANGSUNGAPPOINTMENT = 0 THEN 1 "+
			"   ELSE 0"+
			" END APPOINTMENTAFTERCALL"+
			" FROM ("+
			" SELECT A.T201_TGLCALL TGLCALL, B.T202_TGLJAMKIRIM CONNECTEDCALL, B.T202_ID FROM T201_REMINDER A "+
			" LEFT JOIN T202_INVITATION B ON B.T202_T201_ID = A.T201ID"+
			" WHERE A.T201_TGLCALL IS NOT NULL"+
			" ) A"+
			" INNER JOIN T202_INVITATION B ON B.T202_ID = A.T202_ID"+
			" ) A "+
			" GROUP BY A.TGLCALL"+
			" ORDER BY A.TGLCALL"+
			"";		
		final session = sessionFactory.currentSession;
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					 column_0: resultRow[0],
					 column_1: resultRow[1],
					 column_2: resultRow[2],
					 column_3: resultRow[3],
					 column_4: resultRow[4],
					 column_5: resultRow[5],					 				 
					 column_6: resultRow[6],				 				 
					 column_7: resultRow[7]					 				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		//System.out.//println("Result size: " + results.size());
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results;	
	}
	
	def detailAppointment(def params){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "Imam Akbar Arifaldi");
		map.put("column_2", "Bogor");
		map.put("column_3", "085695113565");
		map.put("column_4", "79886545");
		map.put("column_5", "F1234A");
		map.put("column_6", "Avanza");
		map.put("column_7", "2003");
		map.put("column_8", "20 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "B");
		map.put("column_2", "Jakarta");
		map.put("column_3", "8713");
		map.put("column_4", "4546");
		map.put("column_5", "B2234B");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2005");
		map.put("column_8", "50 K");		
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "C");
		map.put("column_2", "Depok");
		map.put("column_3", "8714");
		map.put("column_4", "4547");
		map.put("column_5", "B460ES");
		map.put("column_6", "Camry");
		map.put("column_7", "2007");
		map.put("column_8", "10 K");
		map.put("column_9", "No");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "D");
		map.put("column_2", "Depok");
		map.put("column_3", "8715");
		map.put("column_4", "4548");
		map.put("column_5", "B3L472");
		map.put("column_6", "Fortuner");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "E");
		map.put("column_2", "Depok");
		map.put("column_3", "8716");
		map.put("column_4", "4549");
		map.put("column_5", "B3L473");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "F");
		map.put("column_2", "Depok");
		map.put("column_3", "8717");
		map.put("column_4", "4550");
		map.put("column_5", "B3L474");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "03-Feb-2015");
		map.put("column_1", "G");
		map.put("column_2", "Depok");
		map.put("column_3", "8718");
		map.put("column_4", "4551");
		map.put("column_5", "B3L475");
		map.put("column_6", "Toyota Avanza");
		map.put("column_7", "2005");
		map.put("column_8", "10 K");		
		map.put("column_9", "Yes");
		map.put("column_10", "No");
		list.add(map);
		return list;
	}
	
	def kpiResults(def params){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("column_0", "Customer Yang Datang Setelah Diundang");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Customer Yang Datang Setelah Diundang");		
		map.put("column_14", "20");
		map.put("column_15", "21");
		map.put("column_16", "22");
		map.put("column_17", "23");
		map.put("column_18", "24");
		map.put("column_19", "25");
		map.put("column_20", "26");
		map.put("column_21", "27");
		map.put("column_22", "28");
		map.put("column_23", "29");
		map.put("column_24", "30");
		map.put("column_25", "31");
		list.add(map);
		
		map = new HashMap<String, Object>();
		map.put("column_0", "Customer Yang Datang Setelah Diundang");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Customer Yang Diundang");		
		map.put("column_14", "32");
		map.put("column_15", "33");
		map.put("column_16", "34");
		map.put("column_17", "35");
		map.put("column_18", "36");
		map.put("column_19", "37");
		map.put("column_20", "38");
		map.put("column_21", "39");
		map.put("column_22", "40");
		map.put("column_23", "41");
		map.put("column_24", "42");
		map.put("column_25", "43");
		list.add(map);
		//==============================================================
		
		map = new HashMap<String, Object>();
		map.put("column_0", "Kontribusi MRS Terhadap Appointment");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Appointment Karena Aktivitas MRS");		
		map.put("column_14", "20");
		map.put("column_15", "21");
		map.put("column_16", "22");
		map.put("column_17", "23");
		map.put("column_18", "24");
		map.put("column_19", "25");
		map.put("column_20", "26");
		map.put("column_21", "27");
		map.put("column_22", "28");
		map.put("column_23", "29");
		map.put("column_24", "30");
		map.put("column_25", "31");
		list.add(map);
		
		map = new HashMap<String, Object>();
		map.put("column_0", "Kontribusi MRS Terhadap Appointment");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Total Appointment");		
		map.put("column_14", "32");
		map.put("column_15", "33");
		map.put("column_16", "34");
		map.put("column_17", "35");
		map.put("column_18", "36");
		map.put("column_19", "37");
		map.put("column_20", "38");
		map.put("column_21", "39");
		map.put("column_22", "40");
		map.put("column_23", "41");
		map.put("column_24", "42");
		map.put("column_25", "43");
		list.add(map);
		
		//-----------------------------------------------------
		
		map = new HashMap<String, Object>();
		map.put("column_0", "Kontribusi MRS Terhadap SBE");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Unit SBE Karena Aktivitas MRS");		
		map.put("column_14", "20");
		map.put("column_15", "21");
		map.put("column_16", "22");
		map.put("column_17", "23");
		map.put("column_18", "24");
		map.put("column_19", "25");
		map.put("column_20", "26");
		map.put("column_21", "27");
		map.put("column_22", "28");
		map.put("column_23", "29");
		map.put("column_24", "30");
		map.put("column_25", "31");
		list.add(map);
		
		map = new HashMap<String, Object>();
		map.put("column_0", "Kontribusi MRS Terhadap SBE");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Total Unit SBE");		
		map.put("column_14", "32");
		map.put("column_15", "33");
		map.put("column_16", "34");
		map.put("column_17", "35");
		map.put("column_18", "36");
		map.put("column_19", "37");
		map.put("column_20", "38");
		map.put("column_21", "39");
		map.put("column_22", "40");
		map.put("column_23", "41");
		map.put("column_24", "42");
		map.put("column_25", "43");
		list.add(map);
		
		//----------------------------------------------
		
		map = new HashMap<String, Object>();
		map.put("column_0", "Kontribusi MRS Terhadap Unit Entry");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Unit SBE dan General Repair Karena Aktivitas MRS");		
		map.put("column_14", "20");
		map.put("column_15", "21");
		map.put("column_16", "22");
		map.put("column_17", "23");
		map.put("column_18", "24");
		map.put("column_19", "25");
		map.put("column_20", "26");
		map.put("column_21", "27");
		map.put("column_22", "28");
		map.put("column_23", "29");
		map.put("column_24", "30");
		map.put("column_25", "31");
		list.add(map);
		
		map = new HashMap<String, Object>();
		map.put("column_0", "Kontribusi Total Unit Entry");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Total Unit SBE");		
		map.put("column_14", "32");
		map.put("column_15", "33");
		map.put("column_16", "34");
		map.put("column_17", "35");
		map.put("column_18", "36");
		map.put("column_19", "37");
		map.put("column_20", "38");
		map.put("column_21", "39");
		map.put("column_22", "40");
		map.put("column_23", "41");
		map.put("column_24", "42");
		map.put("column_25", "43");
		list.add(map);
		
		//-------------------------------------------------
		
		map = new HashMap<String, Object>();
		map.put("column_0", "SBE Retention");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Customer Yang Service 1.000 dari penjualan unit");		
		map.put("column_14", "20");
		map.put("column_15", "21");
		map.put("column_16", "22");
		map.put("column_17", "23");
		map.put("column_18", "24");
		map.put("column_19", "25");
		map.put("column_20", "26");
		map.put("column_21", "27");
		map.put("column_22", "28");
		map.put("column_23", "29");
		map.put("column_24", "30");
		map.put("column_25", "31");
		list.add(map);
		
		map = new HashMap<String, Object>();
		map.put("column_0", "SBE Retention");
		map.put("column_1", "63%");
		map.put("column_2", "62%");
		map.put("column_3", "61%");
		map.put("column_4", "61%");
		map.put("column_5", "60%");
		map.put("column_6", "60%");
		map.put("column_7", "59%");
		map.put("column_8", "59%");
		map.put("column_9", "58%");
		map.put("column_10", "58%");
		map.put("column_11", "58%");
		map.put("column_12", "57%");
		
		map.put("column_13", " - Jumlah Unit Yang Telah Dikirimkan Kepada Customer");		
		map.put("column_14", "32");
		map.put("column_15", "33");
		map.put("column_16", "34");
		map.put("column_17", "35");
		map.put("column_18", "36");
		map.put("column_19", "37");
		map.put("column_20", "38");
		map.put("column_21", "39");
		map.put("column_22", "40");
		map.put("column_23", "41");
		map.put("column_24", "42");
		map.put("column_25", "43");
		list.add(map);
		
		return list;
	}
	
	def summaryAppointmentContribution(def params){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "5");
		map.put("column_2", "2");
		map.put("column_3", "2");
		map.put("column_4", "4");
		map.put("column_5", "80%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "7");
		map.put("column_2", "5");
		map.put("column_3", "0");
		map.put("column_4", "5");
		map.put("column_5", "71%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "03-Feb-2015");
		map.put("column_1", "8");
		map.put("column_2", "4");
		map.put("column_3", "2");
		map.put("column_4", "6");
		map.put("column_5", "75%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "04-Feb-2015");
		map.put("column_1", "9");
		map.put("column_2", "5");
		map.put("column_3", "1");
		map.put("column_4", "6");
		map.put("column_5", "67%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "05-Feb-2015");
		map.put("column_1", "10");
		map.put("column_2", "5");
		map.put("column_3", "2");
		map.put("column_4", "7");
		map.put("column_5", "70%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "06-Feb-2015");
		map.put("column_1", "10");
		map.put("column_2", "5");
		map.put("column_3", "2");
		map.put("column_4", "7");
		map.put("column_5", "70%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "07-Feb-2015");
		map.put("column_1", "10");
		map.put("column_2", "5");
		map.put("column_3", "2");
		map.put("column_4", "7");
		map.put("column_5", "70%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "08-Feb-2015");
		map.put("column_1", "10");
		map.put("column_2", "5");
		map.put("column_3", "2");
		map.put("column_4", "7");
		map.put("column_5", "70%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "09-Feb-2015");
		map.put("column_1", "10");
		map.put("column_2", "5");
		map.put("column_3", "2");
		map.put("column_4", "7");
		map.put("column_5", "70%");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "10-Feb-2015");
		map.put("column_1", "10");
		map.put("column_2", "5");
		map.put("column_3", "2");
		map.put("column_4", "7");
		map.put("column_5", "70%");
		list.add(map);
	}
	
	def detailAppointmentContribution(def params){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "Imam Akbar Arifaldi");
		map.put("column_2", "Bogor");
		map.put("column_3", "085695113565");
		map.put("column_4", "79886545");
		map.put("column_5", "F1234A");
		map.put("column_6", "Avanza");
		map.put("column_7", "2003");
		map.put("column_8", "20 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "B");
		map.put("column_2", "Jakarta");
		map.put("column_3", "8713");
		map.put("column_4", "4546");
		map.put("column_5", "B2234B");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2005");
		map.put("column_8", "50 K");		
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "01-Feb-2015");
		map.put("column_1", "C");
		map.put("column_2", "Depok");
		map.put("column_3", "8714");
		map.put("column_4", "4547");
		map.put("column_5", "B460ES");
		map.put("column_6", "Camry");
		map.put("column_7", "2007");
		map.put("column_8", "10 K");
		map.put("column_9", "No");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "D");
		map.put("column_2", "Depok");
		map.put("column_3", "8715");
		map.put("column_4", "4548");
		map.put("column_5", "B3L472");
		map.put("column_6", "Fortuner");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "E");
		map.put("column_2", "Depok");
		map.put("column_3", "8716");
		map.put("column_4", "4549");
		map.put("column_5", "B3L473");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "02-Feb-2015");
		map.put("column_1", "F");
		map.put("column_2", "Depok");
		map.put("column_3", "8717");
		map.put("column_4", "4550");
		map.put("column_5", "B3L474");
		map.put("column_6", "Kijang Innova");
		map.put("column_7", "2002");
		map.put("column_8", "80 K");
		map.put("column_9", "Yes");
		map.put("column_10", "Yes");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("column_0", "03-Feb-2015");
		map.put("column_1", "G");
		map.put("column_2", "Depok");
		map.put("column_3", "8718");
		map.put("column_4", "4551");
		map.put("column_5", "B3L475");
		map.put("column_6", "Toyota Avanza");
		map.put("column_7", "2005");
		map.put("column_8", "10 K");		
		map.put("column_9", "Yes");
		map.put("column_10", "No");
		list.add(map);
		return list;
	}
}
 
