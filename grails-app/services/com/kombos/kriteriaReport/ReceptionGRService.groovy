package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.woinformation.JobRCP

class ReceptionGRService {
	boolean transactional = false

	def sessionFactory

	def datatablesSAList(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		
		String query = "SELECT T015_NAMAMANPOWER.ID, UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) FROM DOM_USER_ROLES " +
                "INNER JOIN DOM_USER ON DOM_USER.USERNAME = DOM_USER_ROLES.USER_ID " +
                "INNER JOIN T015_NAMAMANPOWER ON UPPER(DOM_USER.FULLNAME) = UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) " +
                "WHERE ROLE_ID LIKE '%LSA%' AND DOM_USER.COMPANY_DEALER_ID = "+ params.companyDealerId +" AND DOM_USER.T001_STADEL = '0'";
		final sqlCount = session.createSQLQuery(query)
		final queryResultsCount = sqlCount.with {
			list()		
		}

        query = "SELECT T015_NAMAMANPOWER.ID, UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) FROM DOM_USER_ROLES " +
                "INNER JOIN DOM_USER ON DOM_USER.USERNAME = DOM_USER_ROLES.USER_ID " +
                "INNER JOIN T015_NAMAMANPOWER ON UPPER(DOM_USER.FULLNAME) = UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) " +
                "WHERE ROLE_ID LIKE '%LSA%' AND DOM_USER.COMPANY_DEALER_ID = "+ params.companyDealerId +" AND DOM_USER.T001_STADEL = '0'";
		if (params."sa") {
			query += " AND LOWER(T015_NAMALENGKAP) LIKE '%" + (params."sa" as String).toLowerCase() + "%'  ";
		}		
		query += " ORDER BY T015_NAMALENGKAP " +sortDir+ " ";
		final sqlQuery = session.createSQLQuery(query)
		sqlQuery.dump();
		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaSa: resultRow[1]
			]
			 
		}
		ret = [sEcho: params.sEcho, iTotalRecords: queryResultsCount.size, iTotalDisplayRecords: queryResultsCount.size, aaData: results]
		return ret
	}
	
	def datatablesJenisPayment(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = JobRCP.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if (params."payment") {
				ilike("t402StaCashInsurance", "%" + (params."payment" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPayment: it.t402StaCashInsurance == '1' ? 'Cash' : 'Insurance'

			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret
	}
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}
	
	def getServiceAdvisorById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, T015_NAMALENGKAP FROM T015_NAMAMANPOWER WHERE ID IN ("+id+") ";		
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaSa: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaSa")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}
	
	def getJenisPekerjaanById(def id){
		String ret = "";
		final session = sessionFactory.currentSession
		String query = " SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE ID IN ("+id+") ";
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {			
			list()		
		}		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaTipeKerusakan: resultRow[1]
			]			 
		}
		for(Map<String, Object> result : results) {
			ret += String.valueOf(result.get("namaTipeKerusakan")) + ",";
		}
		if(ret != ""){
			ret = ret.substring(0, ret.length()-1);
		}
		return ret;
	}

	def datatablesJenisPekerjaan(def params){
		def ret
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		final session = sessionFactory.currentSession
		
		String query = "SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE M055_KATEGORIJOB IN('GR','PDS','SBE','SBI','TWC') ORDER BY M055_KATEGORIJOB ASC";
			
		final sqlQueryCount = session.createSQLQuery(query)
		final queryResultsCount = sqlQueryCount.with {			
			list()		
		}
		
		query = "SELECT ID, M055_KATEGORIJOB FROM M055_KATEGORIJOB WHERE  M055_KATEGORIJOB IN('GR','PDS','SBE','SBI','TWC') ";
		if (params."pekerjaan") {
			query += " AND LOWER(M055_KATEGORIJOB) LIKE '%" + (params."pekerjaan" as String).toLowerCase() + "%'  ";
		}
        query += " ORDER BY M055_KATEGORIJOB " +sortDir+ " ";

		final sqlQuery = session.createSQLQuery(query)

		sqlQuery.setFirstResult(params.iDisplayStart as int);
		sqlQuery.setMaxResults(params.iDisplayLength as int);
		final queryResults = sqlQuery.with {			
			list()		
		}
		
		final results = queryResults.collect { resultRow ->
			[id: resultRow[0],
			 namaPekerjaan: resultRow[1]
			]
			 
		}		
		ret = [sEcho: params.sEcho, iTotalRecords: queryResultsCount.size, iTotalDisplayRecords: queryResultsCount.size, aaData: results]
		return ret
	}

	def datatablesGrReceptionLeadTimeAppointmentStatus(def params, def type){	
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "";
		String appointmentStatus = "";
		String dateFilter = "";
		
		if(params.appointmentStatus == "")
			appointmentStatus = "'BOOKING','NON BOOKING'";
		else if(params.appointmentStatus == "BOOKING")
			appointmentStatus = "'BOOKING'";
		else if(params.appointmentStatus == "NON BOOKING")
			appointmentStatus = "'NON BOOKING'";
		else if(params.appointmentStatus == "BOOKING,NON BOOKING")
			appointmentStatus = "'BOOKING','NON BOOKING'";

        if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY')  AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"'";
		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 
					" SELECT DAT.APPOINTMENT_STATUS, " +strType1+ "  "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION)) AS MAX_WAITING_FOR_RECEPTION, "+ 
					" TO_CHAR(MIN (DAT.WAITING_FOR_RECEPTION)) AS MIN_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION) + MIN (DAT.WAITING_FOR_RECEPTION) / 2) AS AVG_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS)) AS MAX_RECEPTION_PROCESS, "+
					" TO_CHAR(MIN (DAT.RECEPTION_PROCESS)) AS MIN_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS) + MIN (DAT.RECEPTION_PROCESS) / 2) AS AVG_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE)) AS MAX_PRE_DIAGNOSE, "+
					" TO_CHAR(MIN (DAT.PRE_DIAGNOSE)) AS MIN_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE) + MIN (DAT.PRE_DIAGNOSE) / 2) AS AVG_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH)) AS MAX_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MIN (DAT.WAITING_FOR_JOB_DISPATCH)) AS MIN_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH) + MIN (DAT.WAITING_FOR_JOB_DISPATCH) / 2) AS AVG_WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+strType2+" "+
					" DAT.APPOINTMENT_STATUS, "+					
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN))), 0) AS WAITING_FOR_RECEPTION, "+
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION))), 0) AS RECEPTION_PROCESS, "+
					" NVL(CEIL((extract(DAY FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI))), 0) AS PRE_DIAGNOSE, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION))), 0) AS WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+
					" CASE "+
					"     WHEN T301_APPOINTMENT.ID IS NOT NULL THEN 'BOOKING' "+
					"      ELSE 'NON BOOKING' "+
					" END "+
					" AS APPOINTMENT_STATUS, "+
					" T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, "+
					" T400_CUSTOMERIN.T400_TGLJAMRECEPTION, "+
					" T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION, "+
					" T406_PREDIAGNOSIS.T406_JAMMULAI, "+
					" T406_PREDIAGNOSIS.T406_JAMSELESAI, "+
					" T401_RECEPTION.T401_TGLJAMAMBILWO, "+
					" T401_RECEPTION.T401_TANGGALWO "+
					" FROM T400_CUSTOMERIN "+
					" INNER JOIN T401_RECEPTION  ON T401_RECEPTION.T401_T400_ID =  T400_CUSTOMERIN.ID "+ dateFilter + " "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = "+
					"                 T401_RECEPTION.ID "+
					" LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO =  T401_RECEPTION.ID " +
                    " LEFT JOIN M011_COMPANYTAM ON M011_COMPANYTAM.ID = T400_CUSTOMERIN.T400_M011_ID WHERE  T400_CUSTOMERIN.T400_M011_ID = '"+params.workshop+"'"+
					" ) DAT "+
					" ) DAT "+
					" WHERE DAT.APPOINTMENT_STATUS IN ("+appointmentStatus+") "+
					" GROUP BY "+strType1+" DAT.APPOINTMENT_STATUS "+				
					"";

		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9],
				 column_10: resultRow[10],
				 column_11: resultRow[11],
				 column_12: resultRow[12],
				 column_13: resultRow[13],
				 column_14: resultRow[14]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}
		
		return results	
					
	}

	def datatablesGrReceptionLeadTimeJenisPekerjaan(def params, def type){	
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "";
		String appointmentStatus = "";
		String dateFilter = "";
		String jenisPekerjaanFilter = "";
		
		if(params.jenisPekerjaan == ""){
			jenisPekerjaanFilter = " AND 1=1 ";
		} else {
			jenisPekerjaanFilter = " AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+") ";
		}
		
		if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
			dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"' ";			
		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 
					" SELECT DAT.JENIS_PEKERJAAN, "+strType1+" "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION)) AS MAX_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MIN (DAT.WAITING_FOR_RECEPTION)) AS MIN_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION) + MIN (DAT.WAITING_FOR_RECEPTION) / 2) AS AVG_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS)) AS MAX_RECEPTION_PROCESS, "+
					" TO_CHAR(MIN (DAT.RECEPTION_PROCESS)) AS MIN_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS) + MIN (DAT.RECEPTION_PROCESS) / 2) AS AVG_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE)) AS MAX_PRE_DIAGNOSE, "+
					" TO_CHAR(MIN (DAT.PRE_DIAGNOSE)) AS MIN_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE) + MIN (DAT.PRE_DIAGNOSE) / 2) AS AVG_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH)) AS MAX_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MIN (DAT.WAITING_FOR_JOB_DISPATCH)) AS MIN_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH) + MIN (DAT.WAITING_FOR_JOB_DISPATCH) / 2) AS AVG_WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+strType2+" "+
					" DAT.JENIS_PEKERJAAN, "+
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN))), 0) AS WAITING_FOR_RECEPTION, "+
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION))), 0) AS RECEPTION_PROCESS, "+
					" NVL(CEIL((extract(DAY FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI))), 0) AS PRE_DIAGNOSE, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION))), 0) AS WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+
					" M055_KATEGORIJOB.M055_KATEGORIJOB AS JENIS_PEKERJAAN, "+
					" T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, "+
					" T400_CUSTOMERIN.T400_TGLJAMRECEPTION, "+
					" T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION, "+
					" T406_PREDIAGNOSIS.T406_JAMMULAI, "+
					" T406_PREDIAGNOSIS.T406_JAMSELESAI, "+
					" T401_RECEPTION.T401_TGLJAMAMBILWO, "+
					" T401_RECEPTION.T401_TANGGALWO "+
					" FROM T400_CUSTOMERIN "+
					" INNER JOIN T401_RECEPTION  ON T401_RECEPTION.T401_T400_ID =   T400_CUSTOMERIN.ID "+ dateFilter + " "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO =  T401_RECEPTION.ID "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID =  T401_RECEPTION.T401_T183_ID "+
					" INNER JOIN T402_JOBRCP ON T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID" +
					" INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID "+
					" INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID "+jenisPekerjaanFilter+" "+
                    " WHERE T400_CUSTOMERIN.T400_M011_ID = '"+params.workshop+"' "+
					" ) DAT "+
					" ) DAT "+
					" GROUP BY "+strType1+" DAT.JENIS_PEKERJAAN "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9],
				 column_10: resultRow[10],
				 column_11: resultRow[11],
				 column_12: resultRow[12],
				 column_13: resultRow[13],
				 column_14: resultRow[14]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}
		
		return results	
					
	}
	
	def datatablesGrReceptionLeadTimeServiceAdvisor(def params, def type){	
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "";
		String dateFilter = "";
		String serviceAdvisorFilter = "";
		
		if(params.sa == ""){
			serviceAdvisorFilter = "  ";
		} else {
			serviceAdvisorFilter = "HAVING DAT.T015_NAMALENGKAP IN  " +
                    "(SELECT DOM_USER.USERNAME  FROM DOM_USER_ROLES  " +
                    "INNER JOIN DOM_USER ON DOM_USER.USERNAME = DOM_USER_ROLES.USER_ID " +
                    "INNER JOIN T015_NAMAMANPOWER ON UPPER(DOM_USER.FULLNAME) = UPPER( T015_NAMAMANPOWER.T015_NAMALENGKAP) " +
                    "WHERE T015_NAMAMANPOWER.ID IN ("+params.sa+"))";
		}		
		if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND  TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"' ";			
		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 
					" SELECT DAT.T015_NAMALENGKAP, "+strType1+" "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION)) AS MAX_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MIN (DAT.WAITING_FOR_RECEPTION)) AS MIN_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION) + MIN (DAT.WAITING_FOR_RECEPTION) / 2) AS AVG_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS)) AS MAX_RECEPTION_PROCESS, "+
					" TO_CHAR(MIN (DAT.RECEPTION_PROCESS)) AS MIN_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS) + MIN (DAT.RECEPTION_PROCESS) / 2) AS AVG_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE)) AS MAX_PRE_DIAGNOSE, "+
					" TO_CHAR(MIN (DAT.PRE_DIAGNOSE)) AS MIN_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE) + MIN (DAT.PRE_DIAGNOSE) / 2) AS AVG_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH)) AS MAX_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MIN (DAT.WAITING_FOR_JOB_DISPATCH)) AS MIN_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH) + MIN (DAT.WAITING_FOR_JOB_DISPATCH) / 2) AS AVG_WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+strType2+" "+
					" DAT.T015_NAMALENGKAP, "+
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN))), 0) AS WAITING_FOR_RECEPTION, "+
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION))), 0) AS RECEPTION_PROCESS, "+
					" NVL(CEIL((extract(DAY FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI))), 0) AS PRE_DIAGNOSE, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION))), 0) AS WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+					
					" CASE  "+					
					"	WHEN T015_NAMAMANPOWER.ID IS NOT NULL THEN T015_NAMAMANPOWER.T015_NAMALENGKAP "+
					"	ELSE T401_RECEPTION.T401_NAMASA "+
					" END AS T015_NAMALENGKAP, "+
					" T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, "+
					" T400_CUSTOMERIN.T400_TGLJAMRECEPTION, "+
					" T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION, "+
					" T406_PREDIAGNOSIS.T406_JAMMULAI, "+
					" T406_PREDIAGNOSIS.T406_JAMSELESAI, "+
					" T401_RECEPTION.T401_TGLJAMAMBILWO, "+
					" T401_RECEPTION.T401_TANGGALWO "+
					" FROM T400_CUSTOMERIN "+
					" INNER JOIN T401_RECEPTION  ON T401_RECEPTION.T401_T400_ID =   T400_CUSTOMERIN.ID "+ dateFilter + " "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = "+
					"                 T401_RECEPTION.ID "+
					" LEFT JOIN T015_NAMAMANPOWER ON T015_NAMAMANPOWER.ID  = T401_RECEPTION.T401_T015_ID "+
                    " WHERE T400_CUSTOMERIN.T400_M011_ID = '"+params.workshop+"' "+
					" ) DAT "+
					" ) DAT "+
					" GROUP BY "+strType1+" DAT.T015_NAMALENGKAP "+ serviceAdvisorFilter
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9],
				 column_10: resultRow[10],
				 column_11: resultRow[11],
				 column_12: resultRow[12],
				 column_13: resultRow[13],
				 column_14: resultRow[14]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}
		
		return results	
					
	}
	
	def datatablesGrReceptionLeadTimeTotal(def params, def type){	
		final session = sessionFactory.currentSession
		String strType1 = "", strType2 = "", strType3 = "";
		String dateFilter = "";
		
		if(type == 0) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " '' AS YEAR, '' AS MONTH, ";
			strType3 = " DAT.YEAR, DAT.MONTH ";
            dateFilter = " AND T401_RECEPTION.T401_TANGGALWO >= TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY')  AND T401_RECEPTION.T401_TANGGALWO <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  ";
		} else
		if(type == 1) {
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, TO_CHAR(DAT.T401_TANGGALWO, 'MON') AS MONTH, ";
			strType3 = " DAT.YEAR, DAT.MONTH ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"' ";			
		} else
		if(type == 2) {
			strType1 = " DAT.YEAR, DAT.MONTH, ";
			strType2 = " TO_CHAR(DAT.T401_TANGGALWO, 'YYYY') AS YEAR, '' AS MONTH, ";
			strType3 = " DAT.YEAR, DAT.MONTH ";
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}
		
		String query = 
					" SELECT  "+strType1+" "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION)) AS MAX_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MIN (DAT.WAITING_FOR_RECEPTION)) AS MIN_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_RECEPTION) + MIN (DAT.WAITING_FOR_RECEPTION) / 2) AS AVG_WAITING_FOR_RECEPTION, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS)) AS MAX_RECEPTION_PROCESS, "+
					" TO_CHAR(MIN (DAT.RECEPTION_PROCESS)) AS MIN_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.RECEPTION_PROCESS) + MIN (DAT.RECEPTION_PROCESS) / 2) AS AVG_RECEPTION_PROCESS, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE)) AS MAX_PRE_DIAGNOSE, "+
					" TO_CHAR(MIN (DAT.PRE_DIAGNOSE)) AS MIN_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.PRE_DIAGNOSE) + MIN (DAT.PRE_DIAGNOSE) / 2) AS AVG_PRE_DIAGNOSE, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH)) AS MAX_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MIN (DAT.WAITING_FOR_JOB_DISPATCH)) AS MIN_WAITING_FOR_JOB_DISPATCH, "+
					" TO_CHAR(MAX (DAT.WAITING_FOR_JOB_DISPATCH) + MIN (DAT.WAITING_FOR_JOB_DISPATCH) / 2) AS AVG_WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+strType2+" "+	
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMRECEPTION - DAT.T400_TGLCETAKNOANTRIAN))), 0) AS WAITING_FOR_RECEPTION, "+
					" NVL(CEIL((extract(DAY FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T400_TGLJAMSELESAIRECEPTION - DAT.T400_TGLJAMRECEPTION))), 0) AS RECEPTION_PROCESS, "+
					" NVL(CEIL((extract(DAY FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T406_JAMSELESAI - DAT.T406_JAMMULAI))), 0) AS PRE_DIAGNOSE, "+
					" NVL(CEIL((extract(DAY FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM DAT.T401_TGLJAMAMBILWO - DAT.T400_TGLJAMSELESAIRECEPTION))), 0) AS WAITING_FOR_JOB_DISPATCH "+
					" FROM ( "+
					" SELECT "+					
					" T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, "+
					" T400_CUSTOMERIN.T400_TGLJAMRECEPTION, "+
					" T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION, "+
					" T406_PREDIAGNOSIS.T406_JAMMULAI, "+
					" T406_PREDIAGNOSIS.T406_JAMSELESAI, "+
					" T401_RECEPTION.T401_TGLJAMAMBILWO, "+
					" T401_RECEPTION.T401_TANGGALWO "+
					" FROM T400_CUSTOMERIN "+
					" INNER JOIN T401_RECEPTION  ON T401_RECEPTION.T401_T400_ID =   T400_CUSTOMERIN.ID "+ dateFilter + " "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = "+
					"                 T401_RECEPTION.ID "+
                    " WHERE T400_CUSTOMERIN.T400_M011_ID = '"+params.workshop+"' "+
					" ) DAT "+
					" ) DAT "+
					" GROUP BY "+strType3+"  "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9],
				 column_10: resultRow[10],
				 column_11: resultRow[11],
				 column_12: resultRow[12],
				 column_13: resultRow[13]
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results						
	}
	
	def datatablesGrReceptionLeadTimeDetail(def params, def type){	
		String dateFilter = "";
        String saFilter = "";
        String jenisPekerjaanFilter = "";
        String jenisPekerjaanFilter2 = "";
        if(params.jenisPekerjaan == ""){
            jenisPekerjaanFilter = " ";
            jenisPekerjaanFilter2 = " ";
        } else {
            jenisPekerjaanFilter2 = " AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+")";
            jenisPekerjaanFilter = " AND T400_CUSTOMERIN.ID IN (SELECT T401_RECEPTION.T401_T400_ID FROM T401_RECEPTION INNER JOIN T402_JOBRCP ON T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID AND M055_KATEGORIJOB.ID IN ("+params.jenisPekerjaan+"))";
        }
        if(params.sa) {
            saFilter = " LEFT JOIN DOM_USER ON DOM_USER.USERNAME = T401_RECEPTION.T401_NAMASA INNER JOIN T015_NAMAMANPOWER ON UPPER(DOM_USER.FULLNAME) = UPPER(T015_NAMAMANPOWER.T015_NAMALENGKAP) AND T015_NAMAMANPOWER.ID IN ("+params.sa+")"
        }

		if(type == 0) {
			dateFilter = " AND T401_RECEPTION.T401_TANGGALWO BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY') ";			
		} else
		if(type == 1) {
			dateFilter = " AND T401_RECEPTION.T401_TANGGALWO BETWEEN TO_DATE('01-"+params.bulan1+"-"+params.tahun+"', 'DD-MM-YYYY') AND TO_DATE('28-"+params.bulan2+"-"+params.tahun+"', 'DD-MM-YYYY') ";			
		} else
		if(type == 2) {
			dateFilter = " AND TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'YYYY') = '"+params.tahun+"' ";			
		}	
		final session = sessionFactory.currentSession
		String query = 
					" SELECT "+
					" TO_CHAR(T401_RECEPTION.T401_TANGGALWO, 'DD-MON-YYYY') AS TANGGAL_WO, "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL, "+
					" M102_BASEMODEL.M102_NAMABASEMODEL, "+
					" CASE "+
					"     WHEN T301_APPOINTMENT.ID IS NOT NULL THEN 'BOOKING' "+
					"     ELSE 'NON BOOKING' "+
					" END AS APPOINTMENT_STATUS , "+
					" T401_RECEPTION.T401_NAMASA, "+
					" (SELECT M055_KATEGORIJOB.M055_KATEGORIJOB FROM T402_JOBRCP INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID "+
					"     INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID "+
					"     WHERE T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1 "+jenisPekerjaanFilter2+")  KATEGORI_JOB, "+
					" TO_CHAR(T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, 'HH24:MI:SS') AS CUSTOMERIN, "+
					" TO_CHAR(T400_CUSTOMERIN.T400_TGLJAMRECEPTION, 'HH24:MI:SS') AS STRT, "+
					" TO_CHAR(T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION, 'HH24:MI:SS') AS FNISH, "+
					" TO_CHAR(T406_PREDIAGNOSIS.T406_JAMMULAI , 'HH24:MI:SS') AS PRE_DIAGNOSE_START, "+
					" TO_CHAR(T406_PREDIAGNOSIS.T406_JAMSELESAI , 'HH24:MI:SS') AS PRE_DIAGNOSE_FINISH, "+
					" TO_CHAR(T401_RECEPTION.T401_TGLJAMAMBILWO, 'DD-MON-YYYY') AS JOB_DISPATCH_DATE, "+
					" TO_CHAR(T401_RECEPTION.T401_TGLJAMAMBILWO, 'HH24:MI:SS') AS JOB_DISPATCH_TIME, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TGLJAMRECEPTION - T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM T400_CUSTOMERIN.T400_TGLJAMRECEPTION - T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM T400_CUSTOMERIN.T400_TGLJAMRECEPTION - T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN) *  60 ) + "+ 
					" 					 (extract(SECOND FROM T400_CUSTOMERIN.T400_TGLJAMRECEPTION - T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN))), 0) AS WF_RECEPTION, "+
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION - T400_CUSTOMERIN.T400_TGLJAMRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION - T400_CUSTOMERIN.T400_TGLJAMRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION - T400_CUSTOMERIN.T400_TGLJAMRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION - T400_CUSTOMERIN.T400_TGLJAMRECEPTION))), 0) AS RECEPTION_PROCESS, "+
					" NVL(CEIL((extract(DAY FROM T406_PREDIAGNOSIS.T406_JAMSELESAI - T406_PREDIAGNOSIS.T406_JAMMULAI) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM T406_PREDIAGNOSIS.T406_JAMSELESAI - T406_PREDIAGNOSIS.T406_JAMMULAI) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM T406_PREDIAGNOSIS.T406_JAMSELESAI - T406_PREDIAGNOSIS.T406_JAMMULAI) *  60 ) + "+ 
					" 					 (extract(SECOND FROM T406_PREDIAGNOSIS.T406_JAMSELESAI - T406_PREDIAGNOSIS.T406_JAMMULAI))), 0) AS PRE_DIAGNOSE, "+
					" NVL(CEIL((extract(DAY FROM T401_RECEPTION.T401_TGLJAMAMBILWO - T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION) * 24 * 60 * 60)+ "+
					" 		(extract(HOUR FROM T401_RECEPTION.T401_TGLJAMAMBILWO - T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION) *  60 * 60) + "+
					" 			 (extract(MINUTE FROM T401_RECEPTION.T401_TGLJAMAMBILWO - T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION) *  60 ) + "+ 
					" 					 (extract(SECOND FROM T401_RECEPTION.T401_TGLJAMAMBILWO - T400_CUSTOMERIN.T400_TGLJAMSELESAIRECEPTION))), 0) AS WF_JOB_DISPATCH "+
					" FROM T400_CUSTOMERIN "+
					" INNER JOIN T401_RECEPTION  ON T401_RECEPTION.T401_T400_ID = T400_CUSTOMERIN.ID "+ dateFilter + " "+
					" LEFT JOIN T406_PREDIAGNOSIS ON T406_PREDIAGNOSIS.T406_T401_NOWO = "+
					"                 T401_RECEPTION.ID "+
					" LEFT JOIN M401_TIPEKERUSAKAN ON M401_TIPEKERUSAKAN.ID =  T401_RECEPTION.T401_M401_ID "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = T401_T183_ID "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID "+
					" LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE "+
					" LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID =   T110_FULLMODELCODE.T110_M102_ID "+ saFilter + " " +
					" LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO = T401_RECEPTION.ID  "+
                    " WHERE T400_CUSTOMERIN.T400_M011_ID = '"+params.workshop+"' "+ jenisPekerjaanFilter +
					"";


        if(params.appointmentStatus=="BOOKING"){
            query += " AND T301_APPOINTMENT.T301_M301_ID = '1' "
        }else if(params.appointmentStatus=="NON BOOKING"){
            query += " AND T301_APPOINTMENT.T301_M301_ID is null"
        }
            query += " ORDER BY TANGGAL_WO ASC "
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9],
				 column_10: resultRow[10],
				 column_11: resultRow[11],
				 column_12: resultRow[12],
				 column_13: resultRow[13],
				 column_14: resultRow[14],
				 column_15: resultRow[15],
				 column_16: resultRow[16]
				]

			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results						
	}
	
	def datatablesGrReceptionCustomerInSummary(def params){

		final session = sessionFactory.currentSession
		String query = 
					" SELECT "+ 
					" DAT.T400_TGLCETAKNOANTRIAN, "+
					" TO_CHAR(DAT.BOOKING) AS BOOKING, "+
					" TO_CHAR(DAT.NON_BOOKING) AS NON_BOOKING, "+ 
					" TO_CHAR(DAT.BOOKING + DAT.NON_BOOKING) AS TOTAL, "+ 
					" TO_CHAR(DAT.OK_RECEPTION) AS OK_RECEPTION, "+ 
					" DECODE(DAT.CUSTOMER_IN, 0, 0, TO_CHAR((DAT.OK_RECEPTION / DAT.CUSTOMER_IN) * 100)) AS PERSEN_OK_RECEPTION, "+ 
					" TO_CHAR(DAT.CANCEL_RECEPTION) AS CANCEL_RECEPTION, "+ 
					" DECODE(DAT.CUSTOMER_IN,0,0, TO_CHAR((DAT.CANCEL_RECEPTION / DAT.CUSTOMER_IN) * 100)) AS PERSEN_CANCEL_RECEPTION, "+ 
					" TO_CHAR(DAT.ON_TIME) AS ON_TIME, "+
					" DECODE(DAT.CUSTOMER_IN,0,0, TO_CHAR((DAT.ON_TIME / DAT.CUSTOMER_IN) * 100)) AS PERSEN_ON_TIME "+ 
					" FROM ( "+ 
					" SELECT TO_CHAR(DAT.T400_TGLCETAKNOANTRIAN, 'DD-MON-YYYY') AS T400_TGLCETAKNOANTRIAN, "+
					"             COUNT(DAT.T400_TGLCETAKNOANTRIAN) AS CUSTOMER_IN, "+ 
					"             SUM(DAT.BOOKING) AS BOOKING , "+ 
					"             SUM(DAT.NON_BOOKING) AS NON_BOOKING, "+ 
					"             SUM(DAT.OK_RECEPTION) AS OK_RECEPTION, "+ 
					"             SUM(DAT.CANCEL_RECEPTION) AS CANCEL_RECEPTION, "+ 
					"             SUM(DAT.ON_TIME) AS ON_TIME "+ 
					" FROM ( "+ 
					" SELECT  T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, "+ 
					" CASE "+ 
					"     WHEN T400_CUSTOMERIN.T400_STAAPP = 'B' THEN 1 "+
					"     ELSE 0 "+ 
					" END AS BOOKING, "+ 
					" CASE "+ 
					"     WHEN T400_CUSTOMERIN.T400_STAAPP = 'W' THEN 0 "+
					"     ELSE 1 "+ 
					" END AS NON_BOOKING, "+ 
					" CASE "+
					"     WHEN T400_CUSTOMERIN.T400_TGLJAMRECEPTION IS NOT NULL AND T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN IS NOT NULL THEN 1 "+
					"     ELSE 0 "+ 
					" END AS OK_RECEPTION, "+ 
					" CASE "+
					"     WHEN T400_CUSTOMERIN.T400_TGLJAMRECEPTION IS NULL AND T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN IS NOT NULL THEN 1 "+
					"     ELSE 0 "+
					" END AS CANCEL_RECEPTION, "+
					" CASE  "+
					" 	WHEN  "+
					" 		NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN - T301_APPOINTMENT.T301_TGLJAMRENCANA) * 24 * 60 * 60)+  "+
					" 		(extract(HOUR FROM T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN - T301_APPOINTMENT.T301_TGLJAMRENCANA) *  60 * 60)), 0) <= 0 then 1  "+
					" 	ELSE 0  "+
					" END AS ON_TIME "+
					" FROM T400_CUSTOMERIN "+					
					" LEFT JOIN T401_RECEPTION ON T401_RECEPTION.T401_T400_ID =   T400_CUSTOMERIN.ID "+
					" LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO =  T401_RECEPTION.ID "+
					" WHERE T400_CUSTOMERIN.T400_M011_ID = '"+params.workshop+"' AND T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN >= TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND  T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN <= TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY')  " +
					" ) DAT "+
					" GROUP BY TO_CHAR(DAT.T400_TGLCETAKNOANTRIAN, 'DD-MON-YYYY') ORDER BY TO_CHAR(DAT.T400_TGLCETAKNOANTRIAN, 'DD-MON-YYYY')  ASC "+
					" ) DAT "+
					"";
		def results = null
        //println query
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7],
				 column_8: resultRow[8],
				 column_9: resultRow[9]				 
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results						
	}
	
	def datatablesGrReceptionCustomerInDetail(def params){	
		final session = sessionFactory.currentSession
		String query = 					
					" SELECT TO_CHAR(T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, 'DD-MON-YYYY'), "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL, "+
					" CASE "+
					"     WHEN T400_CUSTOMERIN.T400_STAAPP = 'B' THEN 'BOOKING' "+
					"     ELSE 'NON BOOKING' "+
					" END AS APPOINTMENT, "+
					" TO_CHAR(T301_APPOINTMENT.T301_TGLJAMRENCANA, 'DD-MM-YYYY HH24:MI:SS') AS RENCANA, "+
					" TO_CHAR(T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN, 'DD-MM-YYYY HH24:MI:SS') AS KEDATANGAN, "+						
					" NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN - T301_APPOINTMENT.T301_TGLJAMRENCANA) * 24 * 60 * 60)+  " +
					" 	(extract(HOUR FROM T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN - T301_APPOINTMENT.T301_TGLJAMRENCANA) *  60 * 60) + " +
					"		 (extract(MINUTE FROM T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN - T301_APPOINTMENT.T301_TGLJAMRENCANA) *  60)), 0) AS DELAY_TIME, " +										
					" CASE  "+
					" 	WHEN  "+
					" 		NVL(CEIL((extract(DAY FROM T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN - T301_APPOINTMENT.T301_TGLJAMRENCANA) * 24 * 60 * 60)+  "+
					" 		(extract(HOUR FROM T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN - T301_APPOINTMENT.T301_TGLJAMRENCANA) *  60 * 60)), 0) <= 0 then 'YES'  "+
					" 	ELSE 'NO'  "+
					" END AS ON_TIME, "+					
					" CASE "+
					" 	  WHEN T401_RECEPTION.T401_STAOKCANCELRESCHEDULE = 0 THEN 'OK' "+
					"	  WHEN T401_RECEPTION.T401_STAOKCANCELRESCHEDULE = 1 THEN 'CANCEL' "+
					"	  WHEN T401_RECEPTION.T401_STAOKCANCELRESCHEDULE = 2 THEN 'RESCHEDULE' "+
					"	  ELSE 'OK' "+
					" END AS STATUS, "+
					" M402_ALASANCANCEL.M402_NAMAALASANCANCEL "+
					" FROM T400_CUSTOMERIN "+					
					" LEFT JOIN T401_RECEPTION ON T401_RECEPTION.T401_T400_ID =   T400_CUSTOMERIN.ID "+
					" LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO  = T401_RECEPTION.ID "+
					" LEFT JOIN M402_ALASANCANCEL ON M402_ALASANCANCEL.ID = T401_RECEPTION.T401_M402_ID "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = T401_T183_ID "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID "+
					" WHERE T400_CUSTOMERIN.T400_M011_ID = '"+params.workshop+"' AND T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY') " +
					" ORDER BY T400_CUSTOMERIN.T400_TGLCETAKNOANTRIAN ASC";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7],
				 column_8: resultRow[8]				
				]
				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results						
	}
	
	def datatablesGrReceptionPotentialLostSalesSummary(def params, def type){	
		String dateFilter = "";
	
		final session = sessionFactory.currentSession		
		String sqlQuery1 = "";
		if(type == 0){
			sqlQuery1 = " TO_CHAR(DAT.DATE_CREATED,'DD-MON-YYYY') ";
			dateFilter = " AND T400_CUSTOMERIN.DATE_CREATED BETWEEN TO_DATE('"+params.tanggal+"', 'DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"', 'DD-MM-YYYY') ";
		} else
		if(type == 1){
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			sqlQuery1 = " TO_CHAR(DAT.DATE_CREATED,'MON-YYYY') ";
			dateFilter = " AND TO_CHAR(T400_CUSTOMERIN.DATE_CREATED, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND TO_CHAR(T400_CUSTOMERIN.DATE_CREATED, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"' ";			
		}
		
		String query = 					
					" SELECT "+
					" "+sqlQuery1+", SUM(DAT.POTENTIAL_BOOKING) AS POTENTIAL_BOOKING, "+
					" SUM(DAT.POTENTIAL_NON_BOOKING) AS POTENTIAL_NON_BOOKING, "+
					" (SUM(DAT.POTENTIAL_BOOKING) + SUM(DAT.POTENTIAL_NON_BOOKING)) AS TOTAL, "+
					" SUM(DAT.LOST_BOOKING) AS  LOST_BOOKING, "+
					" SUM(DAT.LOST_NON_BOOKING) AS LOST_NON_BOOKING, "+
					" (SUM(DAT.LOST_BOOKING) + SUM(DAT.LOST_NON_BOOKING)) AS TOTAL_LOST, "+
					" round(DECODE((SUM(DAT.POTENTIAL_BOOKING) + SUM(DAT.POTENTIAL_NON_BOOKING)),0,0,((SUM(DAT.LOST_BOOKING) + SUM(DAT.LOST_NON_BOOKING))/(SUM(DAT.POTENTIAL_BOOKING) + SUM(DAT.POTENTIAL_NON_BOOKING)))*100)) AS PERCENT_LOST "+
					" FROM ( "+
					" SELECT T400_CUSTOMERIN.DATE_CREATED, "+
					" CASE "+
					"     WHEN  T301_APPOINTMENT.T301_T401_NOWO IS NOT NULL THEN 1 "+
					"     ELSE 0 "+
					" END AS POTENTIAL_BOOKING, "+
					" CASE "+
					"     WHEN  T301_APPOINTMENT.T301_T401_NOWO IS NULL THEN 1 "+
					"     ELSE 0 "+
					" END AS POTENTIAL_NON_BOOKING, "+
					" CASE "+
					"     WHEN T301_APPOINTMENT.ID IS NOT NULL AND T401_RECEPTION.T401_STAOKCANCELRESCHEDULE = '1'  THEN 1 "+
					"     ELSE 0 "+
					" END AS LOST_BOOKING, "+
					" CASE "+
					"     WHEN T301_APPOINTMENT.ID IS NULL AND T401_RECEPTION.T401_STAOKCANCELRESCHEDULE = '1' THEN 1 "+
					"     ELSE 0 "+
					" END AS LOST_NON_BOOKING "+					
					" FROM T400_CUSTOMERIN "+
					" INNER JOIN  T401_RECEPTION ON T401_RECEPTION.T401_T400_ID =  T400_CUSTOMERIN.ID "+
					" LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO = T401_RECEPTION.ID "+
					" WHERE 1=1 "+
					" "+dateFilter+" "+
					" ORDER BY T400_CUSTOMERIN.DATE_CREATED "+
					" ) DAT "+
					" GROUP BY "+sqlQuery1+" ORDER BY "+ sqlQuery1 +" ASC"
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6],
				 column_7: resultRow[7]				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results						
	}
	
	def datatablesGrReceptionPotentialLostSalesDetail(def params, def type){	
		String dateFilter = "";
	
		final session = sessionFactory.currentSession		
		if(type == 0){
			dateFilter = " AND TO_CHAR(T400_CUSTOMERIN.DATE_CREATED, 'DD-MM-YYYY') >= '"+params.tanggal+"' AND TO_CHAR(T400_CUSTOMERIN.DATE_CREATED, 'DD-MM-YYYY') <= '"+params.tanggal2+"' ";			
		} else
		if(type == 1){
			if(params.bulan1.length() == 1){
				params.bulan1 = "0"+params.bulan1;
			}
			if(params.bulan2.length() == 1){
				params.bulan2 = "0"+params.bulan2;
			}
			dateFilter = " AND TO_CHAR(T400_CUSTOMERIN.DATE_CREATED, 'MM-YYYY')  >= '"+params.bulan1+"-"+params.tahun+"' AND  TO_CHAR(T400_CUSTOMERIN.DATE_CREATED, 'MM-YYYY')  <= '"+params.bulan2+"-"+params.tahun+"' ";			
		}
		String query =
					" SELECT "+
					" TO_CHAR(T400_CUSTOMERIN.DATE_CREATED, 'DD-MM-YYYY') AS DATE_CREATED, "+
					" M116_KODEKOTANOPOL.M116_ID || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLTENGAH  || '  ' ||T183_HISTORYCUSTOMERVEHICLE.T183_NOPOLBELAKANG AS NOPOL, "+
					" T401_RECEPTION.T401_NOWO, "+
					" M102_BASEMODEL.M102_NAMABASEMODEL, "+
					" (SELECT T402_JOBRCP.T402_STACASHINSURANCE FROM T402_JOBRCP WHERE T402_JOBRCP.T402_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) "+
					" CASH_INSURANCE, "+
					" CASE "+
					"     WHEN T301_APPOINTMENT.ID IS NOT NULL THEN 'BOOKING' "+
					"     ELSE 'NON BOOKING' "+
					" END AS IS_BOOKING, "+
					" M402_ALASANCANCEL.M402_NAMAALASANCANCEL "+
					" FROM T400_CUSTOMERIN "+
					" INNER JOIN  T401_RECEPTION ON T401_RECEPTION.T401_T400_ID =  T400_CUSTOMERIN.ID AND LOWER(T401_RECEPTION.T401_STAOKCANCELRESCHEDULE) = '1' "+
					" LEFT JOIN T301_APPOINTMENT ON T301_APPOINTMENT.T301_T401_NOWO = T401_RECEPTION.ID "+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = T401_T183_ID "+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID = T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID "+
					" LEFT JOIN T110_FULLMODELCODE ON T110_FULLMODELCODE.ID = T183_HISTORYCUSTOMERVEHICLE.T183_T110_FULLMODELCODE "+
					" LEFT JOIN M102_BASEMODEL ON M102_BASEMODEL.ID =   T110_FULLMODELCODE.T110_M102_ID "+					
					" LEFT JOIN M402_ALASANCANCEL ON M402_ALASANCANCEL.ID = T401_RECEPTION.T401_M402_ID "+
					" WHERE 1=1 "+
					" "+dateFilter+" "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[column_0: resultRow[0],
				 column_1: resultRow[1],
				 column_2: resultRow[2],
				 column_3: resultRow[3],
				 column_4: resultRow[4],
				 column_5: resultRow[5],
				 column_6: resultRow[6]				 				 
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		if(results.size() <= 0) {
			throw new java.lang.Exception("Result size 0");
		}		
		return results						
	}
	
}
 
