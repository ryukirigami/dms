package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.Reception
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class KpiService {
	boolean transactional = false

	def sessionFactory

	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {
			eq("id", Long.parseLong(code))
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				name: it.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}
		return row
	}

	def printEfaktur(def params){
		List<JasperReportDef> reportDefList = []
		def reportData = printEfakturDetail(params)

		def reportDef = new JasperReportDef(name:'reportEfaktur.jasper',
				fileFormat:JasperExportFormat.CSV_FORMAT,
				reportData: reportData

		)
		reportDefList.add(reportDef)
		return reportDefList
	}

	/* GR */
	def datatablesTotalKPIGeneralRepair(def params){
		def Object[] object = new Object[36]

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

//		def col0 = Reception.findByCompanyDealerAndStaDel(session?.userCompanyDealer,'0')
//		def c = Reception.createCriteria()
//		def results = c.list() {
//			eq("companyDealer",session?.userCompanyDealer)
//			eq("staDel","0")
//
//			ge("tanggal",params.tanggal)
//			lt("tanggal",params.tanggal2 + 1)

//			order("NO_FAKTUR","asc")
//			if(params."sCriteria_m014ID"){
//				ilike("m014ID","%" + (params."sCriteria_m014ID" as String) + "%")
//			}
//
//
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
//		}


		def leadTimeWalkIn = leadTimeWalkInGR(params)
		def selectedWalkIn = selectedWalkInGR(params)



		object[0] = leadTimeWalkIn
		object[1] = 5900;
		object[2] = 34;
		object[3] = 3;
		object[4] = 7;
		object[5] = 12;
		object[6] = 13;
		object[7] = 15;
		object[8] = 17;
		object[9] = 19;
		object[10] = 23;
		object[11] = 21;
		object[12] = 14;
		object[13] = 16;
		object[14] = 17;
		object[15] = 11;
		object[16] = 10;
		object[17] = 9;
		object[18] = 8;
		object[19] = 0;
		object[20] = 6;
		object[21] = 5;
		object[22] = 2;
		object[23] = 1;
		object[24] = 7;
		object[25] = 12;
		object[26] = 27;
		object[27] = 28;
		object[28] = 31;
		object[29] = 33;
		object[30] = 35;
		object[31] = 23;
		object[32] = 21;
		object[33] = 26;
		object[34] = 27;
		object[35] = 13;

		return object;
	}

	def leadTimeWalkInGR(def params){

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		def startDate = df.parse(params.tanggal); //String to Date
		def endDate = df.parse(params.tanggal2);
		String from = startDate.format("dd-MMM-yy") //Date to String
		String to = endDate.format("dd-MMM-yy")

		final session = sessionFactory.currentSession
		String query =
				"SELECT NVL(CEIL((SUM(NVL(RECEPTION,0)+NVL(PREDIAGNOSE,0)+NVL(PRODUCTION,0)+NVL(IDR,0) " +
				"+NVL(FINAL_INSPECTION,0)+NVL(JOC,0)+NVL(INVOICING,0)+NVL(WASHING,0)+NVL(DELIVERY,0) " +
				"+NVL(JOB_STOPPED,0)))/COUNT(*)),0) LEAD_TIME_WALK_IN, '' DUMMY FROM R004_LEAD_TIME WHERE " +
				"COMPANY_DEALER_ID = "+params.workshop+" AND TGL_WO BETWEEN '"+from+"' AND '"+to+"'";


		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {
				list()
			}
			final List<String> results = queryResults.collect { it[0] }
			if(!results.isEmpty()){
				return null != results.get(0) ? Integer.parseInt(String.valueOf(results.get(0))) : "";
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return null;
	}

	def selectedWalkInGR(def params){

	}

	/* BP */
	def datatablesTotalKPIBodyPaint(def params){
		def Object[] object = new Object[27];
			object[0] = 0;
			object[1] = 0;
			object[2] = 0;
			object[3] = 0;
			object[4] = 0;
		return object;
	}
}
 
