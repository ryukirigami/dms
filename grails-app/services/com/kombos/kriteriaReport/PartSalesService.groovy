package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.PartInv
import com.kombos.parts.*
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartSalesService {
    def convert = new Konversi()
	boolean transactional = false

	def sessionFactory
	
	def getWorkshopByCode(def code){
		def row = null
		def c = CompanyDealer.createCriteria()
		def results = c.list() {			
			eq("id", Long.parseLong(code))						
		}
		def rows = []

		results.each {
			rows << [
				id: it?.id,
				name: it?.m011NamaWorkshop

			]
		}
		if(!rows.isEmpty()){
			row = rows.get(0)
		}		
		return row
	}
	
	def datatablesTransaksiPartsPerSupplier(def params, def vendorId){
        //println "datatablesTransaksiPartsPerSupplier Vendor ID : " + vendorId
        def rows = []
        Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
        def company = CompanyDealer.get(params?.workshop?.toLong());
        def claims = Claim.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",company);
            ge("t171TglJamClaim",tgl);
            lt("t171TglJamClaim",tgl2+1);
            if(vendorId!=""){
                eq("vendor",Vendor.get(vendorId));
            }
            order("t171TglJamClaim");
        }

        claims.each {
            def klas = KlasifikasiGoods.findByGoods(it?.goods)
            def grDetail = 0,nilaiInv = 0,hargaBeli = 0;
            def gr = GoodsReceiveDetail.findByGoodsAndStaDelAndGoodsReceive(it?.goods,"0",it?.goodsReceive)
            if(gr){
                grDetail = gr?.t167LandedCost;
            }
            def guds = it?.goods
            Date cari = it?.t171TglJamClaim
            def partInv = PartInv.createCriteria().get {
                eq("goods",guds)
                ge("dateCreated",cari?.clearTime())
                invoice{
                    eq("companyDealer",company);
                }
                order("dateCreated");
                maxResults(1);
            }
            if(partInv){
                nilaiInv = partInv?.t703HargaRp
            }
            def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.goods,company,"0");
            if(goodsHargaBeli){
                hargaBeli = goodsHargaBeli?.t150Harga
            }
            def selisih = hargaBeli - nilaiInv
            selisih = selisih<0 ? (selisih * -1) : selisih
            rows << [
                column_0: it?.t171TglJamClaim?.format("dd-MMM-yyyy"),
                column_1: it?.t171ID,
                column_2: "CLAIM",
                column_3: it?.goods?.m111ID,
                column_4: it?.goods?.m111Nama,
                column_5: klas?.franc ? klas?.franc?.m117NamaFranc : "",
                column_6: Math.round(it?.t171Qty1Reff),
                column_7: it?.goods?.satuan?.m118Satuan1,
                column_8: grDetail,
                column_9: (it?.t171Qty1Reff * grDetail),
                column_10: hargaBeli,
                column_11: nilaiInv,
                column_12: selisih
            ]
        }

            def receives = GoodsReceiveDetail.createCriteria().list {
                eq("staDel","0");
                goodsReceive{
                    eq("staDel","0");
                    eq("companyDealer",company);
                    ge("t167TglJamReceive",tgl);
                    lt("t167TglJamReceive",tgl2+1);
                    if(vendorId!=""){
                        eq("vendor",Vendor.get(vendorId));
                    }
                    order("t167TglJamReceive");
                }
            }

        receives.each{
            def klas = KlasifikasiGoods.findByGoods(it?.goods)
            def grDetail = 0,nilaiInv = 0,hargaBeli = 0;
            grDetail = it?.t167LandedCost ? it?.t167LandedCost : 0;
            def guds = it?.goods
            Date cari = it?.goodsReceive?.t167TglJamReceive
            def partInv = PartInv.createCriteria().get {
                eq("goods",guds)
                ge("dateCreated",cari?.clearTime())
                invoice{
                    eq("companyDealer",company);
                }
                order("dateCreated");
                maxResults(1);
            }
            if(partInv){
                nilaiInv = partInv?.t703HargaRp
            }
            def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.goods,company,"0");
            if(goodsHargaBeli){
                hargaBeli = goodsHargaBeli?.t150Harga
            }
            def selisih = hargaBeli - nilaiInv
            selisih = selisih<0 ? (selisih * -1) : selisih
            rows << [
                    column_0: it?.goodsReceive?.t167TglJamReceive?.format("dd-MMM-yyyy"),
                    column_1: it?.goodsReceive?.t167NoReff,
                    column_2: "RECEIVE",
                    column_3: it?.goods?.m111ID,
                    column_4: it?.goods?.m111Nama,
                    column_5: klas?.franc ? klas?.franc?.m117NamaFranc : "",
                    column_6: Math.round(it?.t167Qty1Reff),
                    column_7: it?.goods?.satuan?.m118Satuan1,
                    column_8: grDetail,
                    column_9: (it?.t167Qty1Reff * grDetail),
                    column_10: hargaBeli,
                    column_11: nilaiInv,
                    column_12: selisih
            ]
        }

        def returns = Returns.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",company);
            ge("t172TglJamReturn",tgl);
            lt("t172TglJamReturn",tgl2+1);
            if(vendorId!=""){
                eq("vendor",Vendor.get(vendorId));
            }
            order("t172TglJamReturn");
        }

        returns.each {
            def klas = KlasifikasiGoods.findByGoods(it?.goods)
            def grDetail = 0,nilaiInv = 0,hargaBeli = 0;
            def gr = GoodsReceiveDetail.findByGoodsAndStaDelAndGoodsReceive(it?.goods,"0",it?.goodsReceive)
            if(gr){
                grDetail = gr?.t167LandedCost;
            }
            def guds = it?.goods
            Date cari = it?.t172TglJamReturn
            def partInv = PartInv.createCriteria().get {
                eq("goods",guds)
                ge("dateCreated",cari?.clearTime())
                invoice{
                    eq("companyDealer",company);
                }
                order("dateCreated");
                maxResults(1);
            }
            if(partInv){
                nilaiInv = partInv?.t703HargaRp
            }
            def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.goods,company,"0");
            if(goodsHargaBeli){
                hargaBeli = goodsHargaBeli?.t150Harga
            }
            def selisih = hargaBeli - nilaiInv
            selisih = selisih<0 ? (selisih * -1) : selisih
            rows << [
                    column_0: it?.t172TglJamReturn?.format("dd-MMM-yyyy"),
                    column_1: it?.t172ID,
                    column_2: "RETURN",
                    column_3: it?.goods?.m111ID,
                    column_4: it?.goods?.m111Nama,
                    column_5: klas?.franc ? klas?.franc?.m117NamaFranc : "",
                    column_6: Math.round(it?.t172Qty1Return),
                    column_7: it?.goods?.satuan?.m118Satuan1,
                    column_8: grDetail,
                    column_9: (it?.t172Qty1Return * grDetail),
                    column_10: hargaBeli,
                    column_11: nilaiInv,
                    column_12: selisih
            ]
        }

        return rows
	}
	
	def datatablesDetail(def params){
        def rows = []
        Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
        def company = CompanyDealer.get(params?.workshop?.toLong());
        //receives
        def receives = GoodsReceiveDetail.createCriteria().list {
            eq("staDel","0");
            goodsReceive{
                eq("staDel","0");
                eq("companyDealer",company);
                ge("t167TglJamReceive",tgl);
                lt("t167TglJamReceive",tgl2+1);
                order("t167TglJamReceive","asc");
            }
        }
        receives.each{
            rows << [
                    column_0: it?.goodsReceive?.t167TglJamReceive?.format("dd-MMM-yyyy"),
                    column_1: it?.goodsReceive?.t167TglJamReceive?.format("HH:mm"),
                    column_2: "RECEIVING",
                    column_3: it?.invoice?.po?.t164NoPO,
                    column_4: "",
                    column_5: "",
                    column_6: it?.goodsReceive?.vendor?.m121Nama,
                    column_7: "",
                    column_8: it?.goods?.m111ID,
                    column_9: it?.goods?.m111Nama,
                    column_10: it?.t167Qty1Issued,
                    column_11: it?.goods.satuan,
                    column_12: it?.t167LandedCost,
                    column_13: it?.t167LandedCost>0 ? (it?.t167Qty1Issued * it?.t167LandedCost) :"",
                    column_14: "",
                    column_15: ""
            ]
        }
        //STOCKIN
        def stockin = StockINDetail.createCriteria().list {
            eq("staDel","0");
            stockIN{
                eq("staDel","0");
                eq("companyDealer",company);
                ge("t169TglJamStockIn",tgl);
                lt("t169TglJamStockIn",tgl2+1);
                order("t169TglJamStockIn","asc");
            }
        }
        stockin.each{
            rows << [
                    column_0: it?.stockIN?.t169TglJamStockIn?.format("dd-MMM-yyyy"),
                    column_1: it?.stockIN?.t169TglJamStockIn?.format("HH:mm"),
                    column_2: "STOCKIN",
                    column_3: it?.stockIN?.t169ID,
                    column_4: "",
                    column_5: "",
                    column_6: "",
                    column_7: "",
                    column_8: it?.binningDetail?.goodsReceiveDetail?.goods?.m111ID,
                    column_9: it?.binningDetail?.goodsReceiveDetail?.goods?.m111Nama,
                    column_10: it?.t169Qty1StockIn,
                    column_11: it?.binningDetail?.goodsReceiveDetail?.goods?.satuan,
                    column_12: it?.binningDetail?.goodsReceiveDetail?.t167LandedCost,
                    column_13: it?.binningDetail?.goodsReceiveDetail?.t167LandedCost > 0 ? (it?.t169Qty1StockIn * it?.binningDetail?.goodsReceiveDetail?.t167LandedCost):"",
                    column_14: "",
                    column_15: ""
            ]
        }
        //CLAIM
        def claims = Claim.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",company);
            ge("t171TglJamClaim",tgl);
            lt("t171TglJamClaim",tgl2+1);
            order("t171TglJamClaim","asc");
        }
        claims.each{
            rows << [
                    column_0: it?.t171TglJamClaim?.format("dd-MMM-yyyy"),
                    column_1: it?.t171TglJamClaim?.format("HH:mm"),
                    column_2: "CLAIM",
                    column_3: it?.t171ID,
                    column_4: "",
                    column_5: "",
                    column_6: it?.vendor?.m121Nama,
                    column_7: "",
                    column_8: it?.goods?.m111ID,
                    column_9: it?.goods?.m111Nama,
                    column_10: it?.t171Qty1Issued,
                    column_11: it?.goods?.satuan,
                    column_12: "",
                    column_13: "",
                    column_14: "",
                    column_15: ""
            ]
        }

        //RETURN
        def returns = Returns.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",company);
            ge("t172TglJamReturn",tgl);
            lt("t172TglJamReturn",tgl2+1);
            order("t172TglJamReturn","asc");
        }
        returns.each{
            rows << [
                    column_0: it?.t172TglJamReturn?.format("dd-MMM-yyyy"),
                    column_1: it?.t172TglJamReturn?.format("HH:mm"),
                    column_2: "RETURN",
                    column_3: it?.t172ID,
                    column_4: "",
                    column_5: "",
                    column_6: it?.vendor?.m121Nama,
                    column_7: "",
                    column_8: it?.goods?.m111ID,
                    column_9: it?.goods?.m111Nama,
                    column_10: it?.t172Qty1Return,
                    column_11: it?.goods?.satuan,
                    column_12: "",
                    column_13: "",
                    column_14: "",
                    column_15: ""
            ]
        }

        //ADJUSMENT
        def adjustments = PartsAdjDetail.createCriteria().list {
            partsAdjust{
                eq("t145StaDel","0");
                eq("companyDealer",company);
                ge("t145TglAdj",tgl);
                lt("t145TglAdj",tgl2+1);
                order("t145TglAdj","asc");
            }
        }
        adjustments.each{
            rows << [
                    column_0: it?.partsAdjust?.t145TglAdj?.format("dd-MMM-yyyy"),
                    column_1: it?.partsAdjust?.t145TglAdj?.format("HH:mm"),
                    column_2: "ADJUSMENT",
                    column_3: it?.partsAdjust?.t145ID,
                    column_4: "",
                    column_5: "",
                    column_6: "",
                    column_7: "",
                    column_8: it?.goods?.m111ID,
                    column_9: it?.goods?.m111Nama,
                    column_10: it?.t146JmlAkhir1,
                    column_11: it?.goods?.satuan,
                    column_12: "",
                    column_13: "",
                    column_14: "",
                    column_15: ""
            ]
        }

        //GOODS ISSUE
        def goodsIssues = PartInv.createCriteria().list {
            invoice{
                eq("t701StaDel","0");
                eq("companyDealer",company);
                ge("t701TglJamInvoice",tgl);
                lt("t701TglJamInvoice",tgl2+1);
                order("t701TglJamInvoice","asc");
            }
        }
        goodsIssues.each{
            rows << [
                    column_0: it?.invoice?.t701TglJamInvoice?.format("dd-MMM-yyyy"),
                    column_1: it?.invoice?.t701TglJamInvoice?.format("HH:mm"),
                    column_2: "GOODS ISSUE",
                    column_3: it?.invoice?.reception?.t401NoWO,
                    column_4: it?.invoice?.t701NoInv,
                    column_5: it?.invoice?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan,
                    column_6: "",
                    column_7: it?.t703NoPartInv,
                    column_8: it?.goods?.m111ID,
                    column_9: it?.goods?.m111Nama,
                    column_10: it?.t703Jumlah1,
                    column_11: it?.goods?.satuan,
                    column_12: "",
                    column_13: "",
                    column_14: it?.t703HargaRp > 0 ? convert.toRupiah(it?.t703HargaRp):"",
                    column_15: it?.t703DiscRp > 0 ? convert.toRupiah(it?.t703DiscRp):""
            ]
        }
		return rows
	}
	

	def datatablesPartStockMovementReport(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT                                                                                                                   "+
					" DATEE, KODE_PART, NAMA_PART, SUM(LC), SUM(ON_HAND_QTY), M118_SATUAN1, SUM(ON_HAND_TOTAL_LC) ,                            "+
					" SUM(RESERVED_QTY), RESERVED_SATUAN, SUM(RESERVED_TOTAL_LC), SUM(wip_qty), WIP_SATUAN,                                    "+
					" SUM(WIP_TOTAL_LC), SUM(BLOCKSTOCK_QTY), BLOCKSTOCK_SATUAN, SUM(BLOCKSTOCK_TOTAL_LC),                                     "+
					" SUM(GOODS_ISSUE_QTY), GOODS_ISSUE_SATUAN, SUM(GOODS_ISSUE_TOTAL_LC), SUM(RETURN_PARTS),                                  "+
					" RETURN_PARTS_SATUAN, SUM(RETURN_PARTS_TOTAL_LC), SUM(ONHAND_ADJUSTMENT_QTY),  ONHAND_ADJUSTMENT_SATUAN,                  "+
					" SUM(onhand_adjustement_total_lc), M117_NAMAFRANC                                                                         "+
					" FROM (                                                                                                                   "+
					" SELECT                                                                                                                   "+
					" TO_CHAR(T131_Tanggal, 'DD-MON-YYYY') DATEE,                                                                              "+
					" M111_GOODS.M111_ID kode_part,                                                                                            "+
					" M111_GOODS.M111_NAMA nama_part,                                                                                          "+
					" NVL(T131_LandedCost, 0) lc,                                                                                              "+
					" '0' on_hand_qty,                                                                                                         "+
					" M118_SATUAN.M118_Satuan1,                                                                                                "+
					" '0' on_hand_total_lc,                                                                                                    "+
					" nvl(T131_Qty1Reserved, 0) reserved_qty,                                                                                  "+
					" M118_Satuan1 reserved_satuan,                                                                                            "+
					" '0' reserved_total_lc,                                                                                                   "+
					" nvl(T131_Qty1WIP, 0) wip_qty,                                                                                            "+
					" M118_Satuan1 wip_satuan,                                                                                                 "+
					" '0' wip_total_lc,                                                                                                        "+
					" nvl(T131_Qty1BlockStock, 0) blockstock_qty,                                                                              "+
					" M118_Satuan1 blockstock_satuan,                                                                                          "+
					" '0' blockstock_total_lc,                                                                                                 "+
					" nvl(T703_Jumlah1, 0) goods_issue_qty,                                                                                    "+
					" M118_Satuan1 goods_issue_satuan,                                                                                         "+
					" '0' goods_issue_total_lc,                                                                                                "+
					" nvl(T172_Qty1Return, 0) return_parts,                                                                                    "+
					" M118_Satuan1 return_parts_satuan,                                                                                        "+
					" '0' return_parts_total_lc,                                                                                               "+
					" '0' onhand_adjustment_qty,                                                                                               "+
					" M118_Satuan1 onhand_adjustment_satuan,                                                                                   "+
					" '0' onhand_adjustement_total_lc,                                                                                         "+
					" M117_FRANC.M117_NAMAFRANC                                                                                                "+
					" FROM                                                                                                                     "+
					" T131_PARTSSTOK                                                                                                           "+
					" inner join M111_GOODS on M111_GOODS.id =  T131_PARTSSTOK.T131_M111_ID                                                    "+
					" LEFT JOIN M118_SATUAN ON M118_SATUAN.ID = M111_GOODS.M111_M118_ID                                                        "+
					" LEFT JOIN T703_PARTINV ON T703_PARTINV.T703_M111_ID = M111_GOODS.ID                                                      "+
					" left join T172_RETURN on T172_RETURN.T162_M111_ID = M111_GOODS.ID                                                        "+
					" LEFT JOIN T161_KLASIFIKASIGOODS ON T161_KLASIFIKASIGOODS.T161_M111_ID = M111_GOODS.ID                                    "+
					" LEFT JOIN M117_FRANC ON M117_FRANC.ID = T161_KLASIFIKASIGOODS.T161_M117_ID                                               "+
					" ORDER BY M117_FRANC.M117_NAMAFRANC ASC                                                                                   "+
					" ) DAT                                                                                                                    "+
					" GROUP BY                                                                                                                 "+
					" DATEE, KODE_PART, NAMA_PART, M118_SATUAN1, RESERVED_SATUAN, WIP_SATUAN, BLOCKSTOCK_SATUAN, GOODS_ISSUE_SATUAN,           "+
					" RETURN_PARTS_SATUAN, ONHAND_ADJUSTMENT_SATUAN, M117_NAMAFRANC                                                            "+
					" ORDER BY M117_NAMAFRANC, DATEE, KODE_PART                                                                                "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21],
					column_22: resultRow[22],
					column_23: resultRow[23],
					column_24: resultRow[24],
					column_25: resultRow[25]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results	
	}
	
	def datatablesPartStockMovementPerCodeReport(def params){
		final session = sessionFactory.currentSession
		String query =
					" SELECT"+
					" TO_CHAR(T131_Tanggal, 'DD-MON-YYYY') DATEE, "+
					" '0' AS ON_HAND_QTY,"+
					" NVL(M118_Satuan1, 0) ON_HAND_SATUAN,"+
					" '0'  ON_HAND_TOTAL_LC,"+
					" NVL(T131_Qty1Reserved, 0) RESERVED_QTY,"+
					" M118_Satuan1 RESERVED_SATUAN,"+
					" '0' RESERVED_TOTAL_LC,"+
					" NVL(T131_Qty1WIP, 0) WIP_QTY,"+
					" M118_Satuan1 WIP_SATUAN,"+
					" '0' WIP_TOTAL_LC,"+
					" NVL(T131_Qty1BlockStock, 0) BLOCK_STOCK_QTY,"+
					" M118_Satuan1 BLOCK_STOCK_SATUAN,"+
					" '0' BLOCK_STOCK_LC,"+
					" NVL(T703_Jumlah1, 0) GOOD_ISSUE_QTY,"+
					" M118_Satuan1 GOOD_ISSUE_SATUAN,"+
					" '0' GOOD_ISSUE_LC,"+
					" NVL(T172_Qty1Return, 0) RETURN_PARTS_QTY,"+
					" M118_Satuan1 RETURN_PARTS_SATUAN,"+
					" '0' RETURN_PARTS_LC,"+
					" '0' ON_HAND_ADJUSTMENT_QTY,"+
					" M118_Satuan1 ON_HAND_ADJUSTMENT_SATUAN,"+
					" '0' ON_HAND_ADJUSTMENT_LC"+
					" FROM"+
					" T131_PARTSSTOK"+
					" inner join M111_GOODS on M111_GOODS.id =  T131_PARTSSTOK.T131_M111_ID"+
					" LEFT JOIN M118_SATUAN ON M118_SATUAN.ID = M111_GOODS.M111_M118_ID"+
					" LEFT JOIN T703_PARTINV ON T703_PARTINV.T703_M111_ID = M111_GOODS.ID"+
					" left join T172_RETURN on T172_RETURN.T162_M111_ID = M111_GOODS.ID"+
					" ORDER BY T131_Tanggal ASC"+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15],
					column_16: resultRow[16],
					column_17: resultRow[17],
					column_18: resultRow[18],
					column_19: resultRow[19],
					column_20: resultRow[20],
					column_21: resultRow[21]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def datatablesWipUnitReport(def params){
        def cd =CompanyDealer.findById(params.workshop)
        //println(" cd "+cd+" tanggal 1 "+params.tanggal+"tanggal 2 "+params.tanggal2)
		final session = sessionFactory.currentSession
		String query =
					" SELECT "+
					" TO_CHAR(DAT.T401_TanggalWO, 'DD-MM-YYYY') WO_DATE,"+
					" DAT.T401_NoWO NOMOR_WO, M116_KODEKOTANOPOL.M116_ID ||  ' '  ||  T183_NopolTengah || ' ' || T183_NopolBelakang NOPOL,"+
					" T102_CUSTOMER.T102_ID KODE_CUSTOMER,"+
					" T182_NamaDepan NAMA_CUSTOMER,"+
					" 'Reception' STATUS_WO,"+
					" (SELECT M055_KATEGORIJOB.M055_KATEGORIJOB FROM T402_JOBRCP INNER JOIN M053_OPERATION ON M053_OPERATION.ID = T402_JOBRCP.T402_M053_JOBID  "+
					" 					 INNER JOIN M055_KATEGORIJOB ON M055_KATEGORIJOB.ID = M053_OPERATION.M053_M055_ID "+
					" 					 WHERE T402_JOBRCP.T402_T401_NOWO = DAT.ID AND ROWNUM = 1)  KATEGORI_JOB,"+
                    " NVL(( SELECT SUM(T131_PARTSSTOK.T131_LANDEDCOST)  FROM T131_PARTSSTOK,M111_GOODS,T403_PARTSRCP,T401_RECEPTION "+
                    " WHERE T131_PARTSSTOK.T131_M111_ID=M111_GOODS.ID "+
                    " AND T403_PARTSRCP.T403_M111_ID=M111_GOODS.ID "+
                    " AND T403_PARTSRCP.T403_T401_NOWO=DAT.ID ),0) LANDED_COST,"+
					" NVL((SELECT sum(T402_JOBRCP.T402_HARGARP) FROM T402_JOBRCP WHERE T402_JOBRCP.T402_STADEL='0' AND T402_JOBRCP.T402_T401_NOWO = DAT.ID),0)  labour_cost,"+
					" T401_CatatanWAC CATATAN_WAC,"+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) > 0 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 30  "+
					" 	 THEN T701_INVOICE.T701_TOTALBAYARRP  "+
					" 	 ELSE 0  "+
					" END AS AGING1,    "+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) >31 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 60  "+
					" 	 THEN T701_INVOICE.T701_TOTALBAYARRP  "+
					" 	 ELSE 0  "+
					" END AS AGING2,"+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) >60 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 90  "+
					" 	 THEN T701_INVOICE.T701_TOTALBAYARRP  "+
					" 	 ELSE 0  "+
					" END AS AGING3,"+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) >90 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 120  "+
					" 	 THEN T701_INVOICE.T701_TOTALBAYARRP  "+
					" 	 ELSE 0  "+
					" END AS AGING4,"+
					" CASE  "+
					" 	 WHEN T701_INVOICE.T701_DueDate IS NOT NULL AND  "+
					" 		extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) >120 AND  extract(DAY FROM SYSDATE - T701_INVOICE.T701_DueDate) <= 999  "+
					" 	 THEN T701_INVOICE.T701_TOTALBAYARRP  "+
					" 	 ELSE 0  "+
					" END AS AGING5,"+
					" '0' TOTAL"+
					" FROM ("+
					" SELECT  "+
					" T401_RECEPTION.*,"+
					" (SELECT T701_INVOICE.ID FROM T701_INVOICE WHERE T701_INVOICE.T701_T401_NOWO = T401_RECEPTION.ID AND ROWNUM = 1) AS INVOICE_ID  "+
					" FROM   "+
					" T401_RECEPTION WHERE T401_TANGGALWO BETWEEN  TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY') "+
                    " AND COMPANY_DEALER_ID= '"+params.workshop+"'" +
					" ) DAT"+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.T701_T401_NOWO = DAT.ID"+
					" LEFT JOIN T182_HISTORYCUSTOMER ON T182_HISTORYCUSTOMER.ID = T701_INVOICE.T701_T182_ID"+
					" LEFT JOIN T183_HISTORYCUSTOMERVEHICLE ON T183_HISTORYCUSTOMERVEHICLE.ID = DAT.T401_T183_ID"+
					" LEFT JOIN M116_KODEKOTANOPOL ON M116_KODEKOTANOPOL.ID =  T183_HISTORYCUSTOMERVEHICLE.T183_M116_ID"+
					" LEFT JOIN T102_CUSTOMER ON T102_CUSTOMER.ID = T182_HISTORYCUSTOMER.T182_T102_ID"+
					" LEFT JOIN T701_INVOICE ON T701_INVOICE.ID =  DAT.INVOICE_ID "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4],
					column_5: resultRow[5],
					column_6: resultRow[6],
					column_7: resultRow[7],
					column_8: resultRow[8],
					column_9: resultRow[9],
					column_10: resultRow[10],
					column_11: resultRow[11],
					column_12: resultRow[12],
					column_13: resultRow[13],
					column_14: resultRow[14],
					column_15: resultRow[15]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results
	}
	
	def getNamaVendor(def vendorId){
		String nama = "";
        def vendor = Vendor.get(vendorId);
        nama = vendor ? vendor?.m121Nama : ""
        return nama
	}
	
	def getKodeVendor(def vendorId){
        String kode = "";
        def vendor = Vendor.get(vendorId);
        kode = vendor ? vendor?.m121ID : ""
        return kode
	}

	def getVendorId(def vendorId){
		def vendorID = -1.toLong()
        def vendor = Vendor.createCriteria().get {
            or{
                eq("m121ID",vendorId,[ignoreCase : true])
                ilike("m121ID","%"+vendorId+"%")
                eq("m121Nama",vendorId,[ignoreCase : true])
                ilike("m121Nama","%"+vendorId+"%");
            }
            maxResults(1);
        }
        if(vendorId==""){
            vendorID = ""
        }else if(vendor){
            vendorID = vendor?.id
        }
        return vendorID
	}

	
	def headerForReportStockMovement(def params){
		def kodePart = params.kodePart;
		def namaPart = params.namaPart;
		
		final session = sessionFactory.currentSession
		String query = 					
					" SELECT "+
					" M111_GOODS.M111_ID kodePart, "+
					" M111_GOODS.M111_NAMA namaPart, "+
					" T131_PARTSSTOK.T131_LANDEDCOST lc, "+
					" T131_PARTSSTOK.T131_QTY1 stokAwal, "+
					" T131_PARTSSTOK.T131_QTY2 stokAkhir "+
					" FROM  "+
					" T131_PARTSSTOK "+
					" inner join M111_GOODS on M111_GOODS.ID =  T131_PARTSSTOK.T131_M111_ID "+
					" and (lower(M111_GOODS.M111_ID) like '%"+kodePart.toLowerCase()+"%' or lower(M111_GOODS.M111_NAMA) like '%"+namaPart.toLowerCase()+"%') "+
					" where rownum = 1 "+
					"";
		def results = null
		try {
			final sqlQuery = session.createSQLQuery(query)
			final queryResults = sqlQuery.with {			
				list()		
			}
			results = queryResults.collect { resultRow ->
				[
					column_0: resultRow[0],
					column_1: resultRow[1],
					column_2: resultRow[2],
					column_3: resultRow[3],
					column_4: resultRow[4]
				]				 
			}
		} catch(Exception e){
			throw new java.lang.Exception(e.getMessage());
		}
		return results;
	}

    def dataBaStockTackingList(def params){
        DateFormat df = new SimpleDateFormat("d-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        Date tgl = startDate - 1

        int selisih = endDate - startDate

        def row = []
        (0..selisih).each {
            tgl+=1

            def c = StokOPName.createCriteria()
            def dataSudahStockTacking = c.list {
                ge("t132Tanggal", tgl)
                lt("t132Tanggal", tgl + 1)
                companyDealer{
                    eq("id", params.workshop as Long)
                }
            }

            def jSstPart = 0, jSstOil = 0, jAmSstPart = 0, jAmSstOil = 0, jBstPart = 0, jBstOil = 0, jAmBstPart = 0, jAmBstOil = 0, binPart = 0, binAPart = 0,
            binOil = 0, binAOil = 0, binPartG = 0, binAPartG = 0, binOilG = 0, binAOilG = 0, hQpart = 0, hApart = 0, hQoil = 0, hAoil = 0, hQtotal = 0, hAtotal = 0,
            jQpart = 0, jApart = 0, jQoil = 0, jAoil = 0, jQtotal = 0, jAtotal = 0
            double iQpart = 0, iApart = 0, iQoil = 0, iAoil = 0, iQtotal = 0, iAtotal = 0, kQpart = 0, kApart = 0, kQoil = 0, kAoil = 0, kQtotal = 0, kAtotal = 0
            def tglPart = ""

            dataSudahStockTacking.each {
                def klasPart = KlasifikasiGoods.findByIdAndFrancInList(it?.goods?.id, Franc.findAllByM117NamaFrancInList(["Parts Toyota", "Parts Campuran"]))
                if (klasPart){
                    jSstPart = it?.t132Jumlah13 ? jSstPart + it?.t132Jumlah13 : jSstPart
                    jBstPart = it?.t132Jumlah1Stock ? jBstPart + it?.t132Jumlah1Stock : jBstPart
                    def amountPart = PartsStok.findByGoodsAndStaDel(Goods.findById(klasPart?.goods?.id), "0")

                    if (amountPart){
                        jAmSstPart = amountPart?.t131LandedCost ? jAmSstPart + amountPart?.t131LandedCost : jAmSstPart
                        jAmBstPart = amountPart?.t131LandedCost ? jAmBstPart + amountPart?.t131LandedCost : jAmBstPart
                    }
                }
                //Bin Tag Part
                if(klasPart?.id == it?.goods?.id && it?.t132Jumlah13 == 0){
                    binPart = it?.t132Jumlah1Stock ? binPart + it?.t132Jumlah1Stock : binPart

                    def binAmountPart = PartsStok.findByGoodsAndStaDel(Goods.findById(klasPart?.goods?.id), "0")

                    if(binAmountPart){
                        binAPart = binAmountPart?.t131LandedCost ? binAPart + binAmountPart?.t131LandedCost : binAPart
                    }
                }

                if(klasPart?.id == it?.goods?.id && it?.t132Jumlah1Stock == 0){
                    binPartG = it?.t132Jumlah13 ? binPartG + it?.t132Jumlah13 : binPartG

                    def binAmountPartG = PartsStok.findByGoodsAndStaDel(Goods.findById(klasPart?.goods?.id), "0")

                    if(binAmountPartG){
                        binAPartG = binAmountPartG?.t131LandedCost ? binAPartG + binAmountPartG?.t131LandedCost : binAPartG
                    }
                }

                def klasOil = KlasifikasiGoods.findByIdAndFrancInList(it?.goods?.id, Franc.findAllByM117NamaFrancInList(["Bahan", "Material"]))
                if(klasOil){
                    jSstOil = it?.t132Jumlah13 ? jSstOil + it?.t132Jumlah13 : jSstOil
                    jBstOil = it?.t132Jumlah1Stock ? jBstOil + it?.t132Jumlah1Stock : jBstOil
                    def amountOil = PartsStok.findByGoodsAndStaDel(Goods.findById(klasOil?.goods?.id), "0")

                    if(amountOil){
                        jAmSstOil = amountOil?.t131LandedCost ? jAmSstOil + amountOil?.t131LandedCost : jAmSstOil
                        jAmBstOil = amountOil?.t131LandedCost ? jAmBstOil + amountOil?.t131LandedCost : jAmBstOil
                    }
                }
                //Bin Tag Oil
                if(klasOil?.id == it?.goods?.id && it?.t132Jumlah13 == 0){
                    binOil = it?.t132Jumlah1Stock ? binOil + it?.t132Jumlah1Stock : binOil

                    def binAmountOil = PartsStok.findByGoodsAndStaDel(Goods.findById(klasOil?.goods?.id), "0")

                    if(binAmountOil){
                        binAOil = binAmountOil?.t131LandedCost ? binAOil + binAmountOil?.t131LandedCost : binAOil
                    }
                }

                if(klasOil?.id == it?.goods?.id && it?.t132Jumlah1Stock == 0){
                    binOilG = it?.t132Jumlah13 ? binOilG + it?.t132Jumlah13 : binOilG

                    def binAmountOilG = PartsStok.findByGoodsAndStaDel(Goods.findById(klasOil?.goods?.id), "0")

                    if(binAmountOilG){
                        binAOilG = binAmountOilG?.t131LandedCost ? binAOilG + binAmountOilG?.t131LandedCost : binAOilG
                    }
                }
            }
            tglPart = tgl?.format("dd MMMM yyyy")
            hQpart = (((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) - ((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) - binPart) + binPartG

            hApart = ((((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) * jAmSstPart) - (((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) * jAmSstPart) -
                    (binAPart * binPart)) + (binAPartG * binPartG)

            hQoil = (((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) - ((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) - binOil) + binOilG

            hAoil = ((((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) * jAmSstOil) - (((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) * jAmSstOil) -
                    (binAOil * binOil)) + (binAOilG * binOilG)

            hQtotal = ((((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) + ((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0)) -
                    (((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) + ((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0)) -
                    (binPart + binOil)) + (binPartG + binOilG)

            hAtotal = (((((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) * jAmSstPart) + (((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) * jAmSstOil)) -
                    ((((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) * jAmSstPart) + (((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) * jAmSstOil)) -
                    ((binAPart * binPart) + (binAOil * binOil))) + ((binAPartG * binPartG) + (binAOilG * binOilG))

            iQpart = Math.round(new Double(hQpart) / new Double(jBstPart))
            iApart = Math.round(new Double(hApart) / (new Double(jAmBstPart) * new Double(jBstPart)))
            iQoil = Math.round(new Double(hQoil) / new Double(jBstOil))
            iAoil = Math.round(new Double(hAoil) / (new Double(jAmBstOil) * new Double(jBstOil)))
            iQtotal = Math.round(new Double(hQtotal) / (new Double(jBstPart) + new Double(jBstOil)))
            iAtotal = Math.round(new Double(hAtotal) / (new Double(jAmBstPart) * new Double(jBstPart)) + (new Double(jAmBstOil) * new Double(jBstOil)))

            jQpart = (((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) + ((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) + binPart) + binPartG
            jApart = ((((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) * jAmSstPart) + (((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) * jAmSstPart) +
                    (binAPart * binPart)) + (binAPartG * binPartG)

            jQoil = (((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) + ((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) + binOil) + binOilG
            jAoil = ((((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) * jAmSstOil) + (((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) * jAmSstOil) +
                    (binAOil * binOil)) + (binAOilG * binOilG)

            jQtotal = ((((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) + ((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0)) +
                    (((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) + ((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0)) +
                    (binPart + binOil)) + (binPartG + binOilG)

            jAtotal = (((((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) * jAmSstPart) + (((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) * jAmSstOil)) +
                    ((((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) * jAmSstPart) + (((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) * jAmSstOil)) +
                    ((binAPart * binPart) + (binAOil * binOil))) + ((binAPartG * binPartG) + (binAOilG * binOilG))

            kQpart = Math.round(jQpart / new Double(jBstPart))
            kApart = Math.round(jApart / (new Double(jAmBstPart) * new Double(jBstPart)))
            kQoil = Math.round(jQoil / new Double(jBstOil))
            kAoil = Math.round(jAoil / (new Double(jAmBstOil) * new Double(jBstOil)))
            kQtotal = Math.round(jQtotal / (new Double(jBstPart) + new Double(jBstOil)))
            kQtotal = Math.round(jAtotal / (new Double(jAmBstPart) * new Double(jBstPart)) + (new Double(jAmBstOil) * new Double(jBstOil)))

            row <<[
                    //PART1
                    column_0  : jSstPart,
                    column_1  : jAmSstPart * jSstPart,
                    column_2  : jBstPart,
                    column_3  : jAmBstPart * jBstPart,
                    column_4  : jSstPart - jBstPart,
                    column_5  : (jAmSstPart * jSstPart) - (jAmBstPart * jBstPart),
                    //OIL1
                    column_6  : jSstOil,
                    column_7  : jAmSstOil * jSstOil,
                    column_8  : jBstOil,
                    column_9  : jAmBstOil * jBstOil,
                    column_10 : jSstOil - jBstOil,
                    column_11 : (jAmSstOil * jBstOil) - (jAmBstOil * jBstOil),
                    //TOTAL1
                    column_12 : jSstPart + jSstOil,
                    column_13 : jAmSstPart + jAmSstOil,
                    column_14 : jBstPart + jBstOil,
                    column_15 : (jAmBstPart * jBstPart) + (jAmBstOil * jBstOil),
                    column_16 : (jSstPart - jBstPart) + (jSstOil - jBstOil),
                    column_17 : ((jAmSstPart * jSstPart) - (jAmBstPart * jBstPart)) + ((jAmSstOil * jBstOil) - (jAmBstOil * jBstOil)),
                    //PART2
                    column_18 : (jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0,
                    column_19 : ((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) * jAmSstPart,
                    column_20 : (jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0,
                    column_21 : ((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) * jAmSstPart,
                    column_22 : binPart,
                    column_23 : binAPart * binPart,
                    //OIL2
                    column_24 : (jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0,
                    column_25 : ((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) * jAmSstOil,
                    column_26 : (jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0,
                    column_27 : ((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) * jAmSstOil,
                    column_28 : binOil,
                    column_29 : binAOil * binOil,
                    //TOTAL2
                    column_30 : ((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) + ((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0),
                    column_31 : (((jSstPart - jBstPart) > 0 ? (jSstPart - jBstPart): 0) * jAmSstPart) + (((jSstOil - jBstOil) > 0 ? (jSstOil - jBstOil) : 0) * jAmSstOil),
                    column_32 : ((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) + ((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0),
                    column_33 : (((jBstPart -jSstPart) > 0 ? (jBstPart -jSstPart): 0) * jAmSstPart) + (((jBstOil - jSstOil) > 0 ? (jBstOil - jSstOil) : 0) * jAmSstOil),
                    column_34 : binPart + binOil,
                    column_35 : (binAPart * binPart) + (binAOil * binOil),
                    //BIN TAG
                    column_36 : binPartG,
                    column_37 : binAPartG * binPartG,
                    column_38 : binOilG,
                    column_39 : binAOilG * binOilG,
                    column_40 : binPartG + binOilG,
                    column_41 : (binAPartG * binPartG) + (binAOilG * binOilG),
                    //PERFORMANCE PART
                    column_42 : hQpart,
                    column_43 : hApart,
                    column_44 : iQpart,
                    column_45 : iApart,
                    column_46 : jQpart,
                    column_47 : jApart,
                    column_48 : kQpart,
                    column_49 : kApart,
                    //PERFORMANCE OIL
                    column_50 : hQoil,
                    column_51 : hAoil,
                    column_52 : iQoil,
                    column_53 : iAoil,
                    column_54 : jQoil,
                    column_55 : jAoil,
                    column_56 : kQoil,
                    column_57 : kAoil,
                    //PERFORMANCE TOTAL
                    column_58 : hQtotal,
                    column_59 : hAtotal,
                    column_60 : iQtotal,
                    column_61 : iAtotal,
                    column_62 : jQtotal,
                    column_63 : jAtotal,
                    column_64 : kQtotal,
                    column_65 : kAtotal,
                    column_tgl: tglPart
            ]
        }
        return row

    }

//jotun
    def getBeforeJumlahPart(def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT SUM (S.T132_JUMLAH1STOCK) FROM T132_STOKOPNAME S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T132_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M117_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC like '%Parts%' AND S.COMPANY_DEALER_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"','dd-MM-yyyy')";
        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0 }
            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0): "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }

    def getAfterJumlahPart(def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT SUM (S.T132_JUMLAH13) FROM T132_STOKOPNAME S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T132_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M117_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC like '%Parts%' AND S.COMPANY_DEALER_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"','dd-MM-yyyy')";

        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0 }
            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0): "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }

    def getBeforeJumlahOil(def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT SUM (S.T132_JUMLAH1STOCK) FROM T132_STOKOPNAME S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T132_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M117_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC like '%Parts%' OR F.M117_NAMAFRANC like'%Material%' AND S.COMPANY_DEALER_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"','dd-MM-yyyy')";

        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0
            }

            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0) : "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }

    def getAfterJumlahOil (def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT SUM (S.T132_JUMLAH13) FROM T132_STOKOPNAME S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T132_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M117_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC like '%Parts%' OR F.M117_NAMAFRANC like'%Material%' AND S.COMPANY_DEALER_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"','dd-MM-yyyy')";
        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0}
            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0): "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }

    //Amount
    def getAmountPartBefore (def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT S.T131_LANDEDCOST FROM T131_PARTSSTOK S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T131_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M111_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC LIKE'%Parts%' AND S.T131_M011_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"', 'dd-MM-yyyy')";
        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0 }
            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0): "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }

    def getAmountPartAfter (def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT S.T131_LANDEDCOST FROM T131_PARTSSTOK S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T131_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M111_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC LIKE'%Parts%' AND S.T131_M011_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"', 'dd-MM-yyyy')";
        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0 }
            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0): "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }


    def getAmountOilBefore (def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT S.T131_LANDEDCOST FROM T131_PARTSSTOK S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T131_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M111_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC LIKE'%Bahan%' AND F.M117_NAMAFRANC LIKE'%Material%' AND S.T131_M011_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"', 'dd-MM-yyyy')";
        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0 }
            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0): "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }

    def getAmountOilAfter (def params){
        final session = sessionFactory.currentSession
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);
        String query = "SELECT S.T131_LANDEDCOST FROM T131_PARTSSTOK S LEFT JOIN T161_KLASIFIKASIGOODS K ON(S.T131_M111_ID = K.ID)" +
                "LEFT JOIN M111_GOODS G ON(G.ID = K.T161_M111_ID) LEFT JOIN M117_FRANC F ON(K.T161_M111_ID = F.ID)" +
                "WHERE F.M117_NAMAFRANC LIKE'%Bahan%' AND F.M117_NAMAFRANC LIKE'%Material%' AND S.T131_M011_ID='"+params.workshop+"' AND S.DATE_CREATED BETWEEN TO_DATE('"+df1.format(startDate)+"', 'dd-MM-yyyy') AND TO_DATE('"+df1.format(endDate)+"', 'dd-MM-yyyy')";
        def results = null
        try {
            final sqlQuery = session.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            final List<String> jumlah = queryResults.collect { it ? it:0 }
            if(!jumlah.isEmpty()){
                return null != jumlah.get(0) ? jumlah.get(0): "";
            }
        } catch(Exception e){
            throw new java.lang.Exception(e.getMessage());
        }
        return null;

    }

//jotun
}
 
