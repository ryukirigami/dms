package com.kombos.kriteriaReport

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerFollowUp.FollowUp
import com.kombos.customerFollowUp.RealisasiFU
import com.kombos.finance.MonthlyBalanceController
import com.kombos.maintable.Pertanyaan
import org.hibernate.criterion.CriteriaSpecification

class CustomerFollowupBPService {
    boolean transactional = false

    def sessionFactory

    def getWorkshopByCode(def code){
        def row = null
        def c = CompanyDealer.createCriteria()
        def results = c.list() {
            eq("id", Long.parseLong(code))
        }
        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    name: it.m011NamaWorkshop

            ]
        }
        if(!rows.isEmpty()){
            row = rows.get(0)
        }
        return row
    }

    def getServiceAdvisorById(def id){
        String ret = "";
        final session = sessionFactory.currentSession
        String query = " SELECT username, T001_NamaPegawai FROM DOM_USER WHERE username IN ("+id+") ";
        final sqlQuery = session.createSQLQuery(query)
        final queryResults = sqlQuery.with {
            list()
        }
        final results = queryResults.collect { resultRow ->
            [username: resultRow[0],
                    namaSa: resultRow[1]
            ]
        }
        for(Map<String, Object> result : results) {
            ret += String.valueOf(result.get("username")) + ",";
        }
        if(ret != ""){
            ret = ret.substring(0, ret.length()-1);
        }
        return ret;
    }

    def datatablesSAList(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        def c = User.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            companyDealer{
                eq("id",params?.company?.toLong())
            }

            if(params."sCriteria_namaPegawai"){
                ilike("t001NamaPegawai","%" + (params."sCriteria_namaPegawai" as String) + "%")
            }

            roles{
                ilike("name","%SA & LSA%");
            }

            switch (sortProperty) {
                case "t001NamaPegawai":
                    order("t001NamaPegawai",sortDir)
                    break;
                default:
                    order(sortProperty, sortDir)

                    break;
            }

        }

        def rows = []

        results.each {
            rows << [
                    username : it.username,
                    t001NamaPegawai : it.t001NamaPegawai
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        return ret
    }

    def datatablesJenisPayment(def params){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = JobRCP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."payment") {
                ilike("t402StaCashInsurance", "%" + (params."payment" as String) + "%")
            }
            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }
        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    namaPayment: it.t402StaCashInsurance == '1' ? 'Cash' : 'Insurance'

            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        return ret
    }

    def datatablesJenisPekerjaan(def params){
        def ret
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        final session = sessionFactory.currentSession
        String query = " SELECT ID, M401_NAMATIPEKERUSAKAN FROM M401_TIPEKERUSAKAN WHERE M401_STADEL = '0' ";
        if (params."pekerjaan") {
            query += " AND LOWER(M401_NAMATIPEKERUSAKAN) LIKE '%" + (params."pekerjaan" as String) + "%'  ";
        }
        query += " ORDER BY M401_NAMATIPEKERUSAKAN " +sortDir+ " ";
        final sqlQuery = session.createSQLQuery(query)
        sqlQuery.setFirstResult(params.iDisplayStart as int);
        sqlQuery.setMaxResults(params.iDisplayLength as int);
        final queryResults = sqlQuery.with {
            list()
        }

        final results = queryResults.collect { resultRow ->
            [id: resultRow[0],
                    namaPekerjaan: resultRow[1]
            ]

        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: results]
        return ret
    }

    def datatablesBPCustomerFollowupSummary(def params, def type){
        def awal = new Date().parse("dd-MM-yyyy",params.tanggal);
        def akhir = new Date().parse("dd-MM-yyyy",params.tanggal2);
        def namaSA = params.sa;
        def listSA = []
        if(namaSA!=""){
            namaSA = namaSA?.toString()?.replaceAll("\'","");
            listSA = namaSA?.toString()?.split(",");
        }else {
            def c = User.createCriteria().list{
                eq("companyDealer",params?.companyDealer)
                roles{
                    ilike("name","%SA & LSA%");
                }
            }
            listSA = c?.username
        }

        def listFollowUp = FollowUp.createCriteria().list {
            eq("companyDealer",params.companyDealer);
            reception{
                customerIn{
                    tujuanKedatangan{
                        eq("m400Tujuan","BP")
                    }
                }
                if(listSA?.size()>0){
                    inList("t401NamaSA",listSA);
                }
            }
            ge("t801TglJamFU",awal);
            lt("t801TglJamFU",akhir+1)
            order("t801TglJamFU")
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t801TglJamFU", "tanggal");
            }
        }

        def rows = []
        listFollowUp.each {
            Date tgglFU = it.tanggal
            def planByTelp = 0, planBySMS = 0,realSamePhone = 0, realOthersPhone = 0,realSameSMS = 0,realOthersSMS = 0,responseSameSMS = 0,responseOtherSMS = 0
            def connectSamePhone = 0,connectOthersPhone = 0,connectSameSMS = 0,connectOthersSMS = 0, suksesCallRate = 0, fuPhoneRate = 0, suksesSMSRate = 0,fuSMSRate = 0
            def q1Phone = 0,q2Phone=0,q3Phone=0,q1sms=0,q2sms=0,q3sms=0,q1OtherPhone = 0,q2OtherPhone = 0,q3OtherPhone = 0, q1OtherSms = 0, q2OtherSms = 0, q3OtherSms = 0
            def listFU = FollowUp.createCriteria().list {
                eq("companyDealer",params.companyDealer);
                reception{
                    customerIn{
                        tujuanKedatangan{
                            eq("m400Tujuan","BP")
                        }
                    }
                    if(listSA?.size()>0){
                        inList("t401NamaSA",listSA);
                    }
                }
                ge("t801TglJamFU",tgglFU);
                lt("t801TglJamFU",tgglFU+1)
                order("t801TglJamFU");
            }
            listFU.each {
                if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                    planBySMS++;
                }else {
                    planByTelp++;
                }
            }
            def realFU = RealisasiFU.createCriteria().list {
                followUp{
                    eq("companyDealer",params.companyDealer);
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","BP")
                            }
                        }
                        if(listSA?.size()>0){
                            inList("t401NamaSA",listSA);
                        }
                    }
                    ge("t801TglJamFU",tgglFU);
                    lt("t801TglJamFU",tgglFU+1)
                }
            }
            realFU.each {
                if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                    if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                        realSameSMS++;
                        if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                            connectSameSMS++;
                        }
                        if(it?.t802StaBalasanSMS){
                            responseSameSMS++;
                        }
                    }else{
                        realOthersSMS++;
                        if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                            connectOthersSMS++;
                        }
                        if(it?.t802StaBalasanSMS){
                            responseOtherSMS++;
                        }
                    }
                }else {
                    if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                        realSamePhone++;
                        if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                            connectSamePhone++;
                        }
                    }else{
                        realOthersPhone++;
                        if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                            connectOthersPhone++;
                        }
                    }
                }
            }
            suksesCallRate = (connectSamePhone==0 && connectOthersPhone == 0) || (realSamePhone==0 && realOthersPhone == 0) ? 0 : (connectSamePhone + connectOthersPhone)/(realSamePhone+realOthersPhone)*100
            fuPhoneRate = connectSamePhone == 0 || planByTelp ? 0 : connectSamePhone / planByTelp * 100
            suksesSMSRate = (connectSameSMS==0 && connectOthersSMS==0) || (realSameSMS==0 && realOthersSMS==0) ? 0 :(connectSameSMS + connectOthersSMS)/(realSameSMS+realOthersSMS)*100
            fuSMSRate = (responseSameSMS==0 || planBySMS==0) ? 0 : responseSameSMS / planBySMS * 100
            rows << [
                    column_0: tgglFU?.format("dd-MM-yyyy"),
                    column_1: planByTelp,
                    column_2: realSamePhone,
                    column_3: realOthersPhone,
                    column_4: connectSamePhone,
                    column_5: connectOthersPhone,
                    column_6: connectSamePhone,
                    column_7: connectSamePhone,
                    column_8: planBySMS,
                    column_9: realSameSMS,
                    column_10: realOthersSMS,
                    column_11: connectSameSMS,
                    column_12: connectOthersSMS,
                    column_13: responseSameSMS,
                    column_14: responseOtherSMS,
                    column_15: Math.round(suksesCallRate),
                    column_16: Math.round(fuPhoneRate),
                    column_17: Math.round(suksesSMSRate),
                    column_18: Math.round(fuSMSRate),
            ]
        }
        return rows
    }

    def datatablesBPCustomerFollowupSummaryMonth(def params, def type){
        def awal = new Date().parse("dd-MM-yyyy",params.tanggal);
        def akhir = new Date().parse("dd-MM-yyyy",params.tanggal2);
        def namaSA = params.sa;
        def listSA = []
        if(namaSA!=""){
            namaSA = namaSA?.toString()?.replaceAll("\'","");
            listSA = namaSA?.toString()?.split(",");
        }else {
            def c = User.createCriteria().list{
                eq("companyDealer",params?.companyDealer)
                roles{
                    ilike("name","%SA & LSA%");
                }
            }
            listSA = c?.username
        }

        int blnAwal = params.bulan1.toInteger()
        int blnAkhir = params.bulan2.toInteger()

        def rows = []
        for(int a=blnAwal;a<=blnAkhir;a++){
            def dateAwal = new Date()?.parse("dd/M/yyyy","01/"+a+"/"+params?.tahun);
            def tanggalAkhir = new MonthlyBalanceController()?.tanggalAkhir(a,params?.tahun?.toInteger());
            def dateAkhir = new Date()?.parse("dd/M/yyyy",tanggalAkhir+"/"+a+"/"+params?.tahun);

            def listFU = FollowUp.createCriteria().list {
                eq("companyDealer",params.companyDealer);
                reception{
                    customerIn{
                        tujuanKedatangan{
                            eq("m400Tujuan","BP")
                        }
                    }
                    if(listSA?.size()>0){
                        inList("t401NamaSA",listSA);
                    }
                }
                ge("t801TglJamFU",dateAwal);
                lt("t801TglJamFU",dateAkhir+1);
                order("t801TglJamFU");
            }

            def planByTelp = 0, planBySMS = 0,realSamePhone = 0, realOthersPhone = 0,realSameSMS = 0,realOthersSMS = 0,responseSameSMS = 0,responseOtherSMS = 0
            def connectSamePhone = 0,connectOthersPhone = 0,connectSameSMS = 0,connectOthersSMS = 0, suksesCallRate = 0, fuPhoneRate = 0, suksesSMSRate = 0,fuSMSRate = 0
            def q1Phone = 0,q2Phone=0,q3Phone=0,q1sms=0,q2sms=0,q3sms=0,q1OtherPhone = 0,q2OtherPhone = 0,q3OtherPhone = 0, q1OtherSms = 0, q2OtherSms = 0, q3OtherSms = 0
            def firPhone = 0,firSMS = 0;

            listFU.each {
                Date tgglFU = it?.t801TglJamFU?.clearTime()
                if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                    planBySMS++;
                }else {
                    planByTelp++;
                }
                def follUp = it
                def realFU = RealisasiFU.createCriteria().list {
                    eq("followUp",follUp);
                }
                realFU.each {
                    if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                        if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                            realSameSMS++;
                            if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                connectSameSMS++;
                            }
                            if(it?.t802StaBalasanSMS){
                                responseSameSMS++;
                            }
                        }else{
                            realOthersSMS++;
                            if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                connectOthersSMS++;
                            }
                            if(it?.t802StaBalasanSMS){
                                responseOtherSMS++;
                            }
                        }
                    }else {
                        if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                            realSamePhone++;
                            if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                connectSamePhone++;
                            }
                        }else{
                            realOthersPhone++;
                            if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                connectOthersPhone++;
                            }
                        }
                    }
                    def reFU = it
                }
                suksesCallRate = (connectSamePhone==0 && connectOthersPhone == 0) || (realSamePhone==0 && realOthersPhone == 0) ? 0 : (connectSamePhone + connectOthersPhone)/(realSamePhone+realOthersPhone)*100
                fuPhoneRate = connectSamePhone == 0 || planByTelp ? 0 : connectSamePhone / planByTelp * 100
                suksesSMSRate = (connectSameSMS==0 && connectOthersSMS==0) || (realSameSMS==0 && realOthersSMS==0) ? 0 :(connectSameSMS + connectOthersSMS)/(realSameSMS+realOthersSMS)*100
            }
            rows << [
                    column_0: dateAwal?.format("yyyy"),
                    column_1: dateAwal?.format("MMMM"),
                    column_2: planByTelp,
                    column_3: realSamePhone,
                    column_4: realOthersPhone,
                    column_5: connectSamePhone,
                    column_6: connectOthersPhone,
                    column_7: connectSamePhone,
                    column_8: connectSamePhone,
                    column_9: planBySMS,
                    column_10: realSameSMS,
                    column_11: realOthersSMS,
                    column_12: connectSameSMS,
                    column_13: connectOthersSMS,
                    column_14: responseSameSMS,
                    column_15: responseOtherSMS,
                    column_16: Math.round(suksesCallRate),
                    column_17: Math.round(fuPhoneRate),
                    column_18: Math.round(suksesSMSRate),
                    column_19: Math.round(fuSMSRate)
            ]
        }
        return rows
    }

    def datatablesBPCustomerFollowupDetailMonthly(def params, def type){
        def awal = new Date().parse("dd-MM-yyyy",params.tanggal);
        def akhir = new Date().parse("dd-MM-yyyy",params.tanggal2);
        def namaSA = params.sa;
        def listSA = []
        int blnAwal = params.bulan1.toInteger()
        int blnAkhir = params.bulan2.toInteger()

        def rows = []
        if(namaSA!=""){
            namaSA = namaSA?.toString()?.replaceAll("\'","");
            listSA = namaSA?.toString()?.split(",");
        }else {
            def c = User.createCriteria().list{
                eq("companyDealer",params?.companyDealer)
                roles{
                    ilike("name","%SA & LSA%");
                }
            }
            listSA = c?.username
        }

        for(int a=blnAwal;a<=blnAkhir;a++){
            def dateAwal = new Date()?.parse("dd/M/yyyy","01/"+a+"/"+params?.tahun);
            def tanggalAkhir = new MonthlyBalanceController()?.tanggalAkhir(a,params?.tahun?.toInteger());
            def dateAkhir = new Date()?.parse("dd/M/yyyy",tanggalAkhir+"/"+a+"/"+params?.tahun);
            listSA.each {
                def planByTelp = 0, planBySMS = 0,realSamePhone = 0, realOthersPhone = 0,realSameSMS = 0,realOthersSMS = 0,responseSameSMS = 0,responseOtherSMS = 0
                def connectSamePhone = 0,connectOthersPhone = 0,connectSameSMS = 0,connectOthersSMS = 0, suksesCallRate = 0, fuPhoneRate = 0, suksesSMSRate = 0,fuSMSRate = 0
                def q1Phone = 0,q2Phone=0,q3Phone=0,q1sms=0,q2sms=0,q3sms=0,q1OtherPhone = 0,q2OtherPhone = 0,q3OtherPhone = 0, q1OtherSms = 0, q2OtherSms = 0, q3OtherSms = 0
                def firPhone = 0,firSMS = 0;
                def nama = it
                def listFollowUp = FollowUp.createCriteria().list {
                    eq("companyDealer",params.companyDealer);
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","BP")
                            }
                        }
                        eq("t401NamaSA",nama);
                    }
                    ge("t801TglJamFU",dateAwal);
                    lt("t801TglJamFU",dateAkhir+1);
                    order("t801TglJamFU");
                }
                listFollowUp.each {
                    Date tgglFU = it?.t801TglJamFU?.clearTime()
                    Date tgglDelivery = (tgglFU-3).clearTime()
                    if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                        planBySMS++;
                    }else {
                        planByTelp++;
                    }
                    def follUp = it
                    def realFU = RealisasiFU.createCriteria().list {
                        eq("followUp",follUp)
                    }
                    realFU.each {
                        if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                            if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                                realSameSMS++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectSameSMS++;
                                }
                                if(it?.t802StaBalasanSMS){
                                    responseSameSMS++;
                                }
                            }else{
                                realOthersSMS++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectOthersSMS++;
                                }
                                if(it?.t802StaBalasanSMS){
                                    responseOtherSMS++;
                                }
                            }
                        }else {
                            if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                                realSamePhone++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectSamePhone++;
                                }
                            }else{
                                realOthersPhone++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectOthersPhone++;
                                }
                            }
                        }
                        def reFU = it
                    }
                    suksesCallRate = (connectSamePhone==0 && connectOthersPhone == 0) || (realSamePhone==0 && realOthersPhone == 0) ? 0 : (connectSamePhone + connectOthersPhone)/(realSamePhone+realOthersPhone)*100
                    fuPhoneRate = connectSamePhone == 0 || planByTelp ? 0 : connectSamePhone / planByTelp * 100
                    suksesSMSRate = (connectSameSMS==0 && connectOthersSMS==0) || (realSameSMS==0 && realOthersSMS==0) ? 0 :(connectSameSMS + connectOthersSMS)/(realSameSMS+realOthersSMS)*100
                    fuSMSRate = (responseSameSMS==0 || planBySMS==0) ? 0 : responseSameSMS / planBySMS * 100
                }
                if(listFollowUp?.size()>0){
                    rows << [
                            column_0: dateAwal?.format("MMMM yyyy"),
                            column_1: dateAwal?.format("MMMM"),
                            column_2: nama,
                            column_3: planByTelp,
                            column_4: realSamePhone,
                            column_5: realOthersPhone,
                            column_6: connectSamePhone,
                            column_7: connectOthersPhone,
                            column_8: connectSamePhone,
                            column_9: connectOthersPhone,
                            column_10: planBySMS,
                            column_11: realSameSMS,
                            column_12: realOthersSMS,
                            column_13: connectSameSMS,
                            column_14: connectOthersSMS,
                            column_15: responseSameSMS,
                            column_16: responseOtherSMS,
                            column_17: Math.round(suksesCallRate),
                            column_18: Math.round(fuPhoneRate),
                            column_19: Math.round(suksesSMSRate),
                            column_20: Math.round(fuSMSRate)
                    ]
                }
            }
        }
        return rows
    }

    def datatablesBPCustomerFollowupDetail(def params, def type){
        def awal = new Date().parse("dd-MM-yyyy",params.tanggal);
        def akhir = new Date().parse("dd-MM-yyyy",params.tanggal2);
        def namaSA = params.sa;
        def listSA = []
        def rows = []
        if(namaSA!=""){
            namaSA = namaSA?.toString()?.replaceAll("\'","");
            listSA = namaSA?.toString()?.split(",");
        }else {
            def c = User.createCriteria().list{
                eq("companyDealer",params?.companyDealer)
                roles{
                    ilike("name","%SA & LSA%");
                }
            }
            listSA = c?.username
        }

        def listFollowUp = FollowUp.createCriteria().list {
            eq("companyDealer",params.companyDealer);
            reception{
                customerIn{
                    tujuanKedatangan{
                        eq("m400Tujuan","BP")
                    }
                }
                if(listSA?.size()>0){
                    inList("t401NamaSA",listSA);
                }
            }
            ge("t801TglJamFU",awal);
            lt("t801TglJamFU",akhir+1)
            order("t801TglJamFU")
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t801TglJamFU", "tanggal");
            }
        }

        listFollowUp.each {
            Date tgglFU = it.tanggal
            Date tgglDelivery = (tgglFU-3).clearTime()
            listSA.each {
                def planByTelp = 0, planBySMS = 0,realSamePhone = 0, realOthersPhone = 0,realSameSMS = 0,realOthersSMS = 0,responseSameSMS = 0,responseOtherSMS = 0
                def connectSamePhone = 0,connectOthersPhone = 0,connectSameSMS = 0,connectOthersSMS = 0, suksesCallRate = 0, fuPhoneRate = 0, suksesSMSRate = 0,fuSMSRate = 0
                def q1Phone = 0,q2Phone=0,q3Phone=0,q1sms=0,q2sms=0,q3sms=0,q1OtherPhone = 0,q2OtherPhone = 0,q3OtherPhone = 0, q1OtherSms = 0, q2OtherSms = 0, q3OtherSms = 0
                def nama = it
                def listFU = FollowUp.createCriteria().list {
                    eq("companyDealer",params.companyDealer);
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","BP")
                            }
                        }
                        eq("t401NamaSA",nama);
                    }
                    ge("t801TglJamFU",tgglFU);
                    lt("t801TglJamFU",tgglFU+1)
                    order("t801TglJamFU");
                }
                if(listFU?.size()>0){
                    listFU.each {
                        if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                            planBySMS++;
                        }else {
                            planByTelp++;
                        }
                    }
                    def realFU = RealisasiFU.createCriteria().list {
                        followUp{
                            eq("companyDealer",params.companyDealer);
                            reception{
                                customerIn{
                                    tujuanKedatangan{
                                        eq("m400Tujuan","BP")
                                    }
                                }
                                eq("t401NamaSA",nama);
                            }
                            ge("t801TglJamFU",tgglFU);
                            lt("t801TglJamFU",tgglFU+1)
                        }
                    }
                    realFU.each {
                        if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                            if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                                realSameSMS++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectSameSMS++;
                                }
                                if(it?.t802StaBalasanSMS){
                                    responseSameSMS++;
                                }
                            }else{
                                realOthersSMS++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectOthersSMS++;
                                }
                                if(it?.t802StaBalasanSMS){
                                    responseOtherSMS++;
                                }
                            }
                        }else {
                            if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                                realSamePhone++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectSamePhone++;
                                }
                            }else{
                                realOthersPhone++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectOthersPhone++;
                                }
                            }
                        }

                    }

                    suksesCallRate = (connectSamePhone==0 && connectOthersPhone == 0) || (realSamePhone==0 && realOthersPhone == 0) ? 0 : (connectSamePhone + connectOthersPhone)/(realSamePhone+realOthersPhone)*100
                    fuPhoneRate = connectSamePhone == 0 || planByTelp ? 0 : connectSamePhone / planByTelp * 100
                    suksesSMSRate = (connectSameSMS==0 && connectOthersSMS==0) || (realSameSMS==0 && realOthersSMS==0) ? 0 :(connectSameSMS + connectOthersSMS)/(realSameSMS+realOthersSMS)*100
                    fuSMSRate = (responseSameSMS==0 || planBySMS==0) ? 0 : responseSameSMS / planBySMS * 100

                    rows << [
                            column_0: tgglFU?.format("dd-MM-yyyy"),
                            column_1: tgglDelivery?.format("dd-MM-yyyy"),
                            column_2: nama,
                            column_3: planByTelp,
                            column_4: realSamePhone,
                            column_5: realOthersPhone,
                            column_6: connectSamePhone,
                            column_7: connectOthersPhone,
                            column_8: connectSamePhone,
                            column_9: connectOthersPhone,
                            column_10: planBySMS,
                            column_11: realSameSMS,
                            column_12: realOthersSMS,
                            column_13: connectSameSMS,
                            column_14: connectOthersSMS,
                            column_15: responseSameSMS,
                            column_16: responseOtherSMS,
                            column_17: Math.round(suksesCallRate),
                            column_18: Math.round(fuPhoneRate),
                            column_19: Math.round(suksesSMSRate),
                            column_20: Math.round(fuSMSRate)
                    ]
                }
            }
        }

        return rows
    }

    def datatablesBPCustomerFollowupGraph(def params){
        def awal = new Date().parse("dd-MM-yyyy",params.tanggal);
        def akhir = new Date().parse("dd-MM-yyyy",params.tanggal2);
        def namaSA = params.sa;
        def listSA = []
        int blnAwal = params.bulan1.toInteger()
        int blnAkhir = params.bulan2.toInteger()

        def rows = []
        if(namaSA!=""){
            namaSA = namaSA?.toString()?.replaceAll("\'","");
            listSA = namaSA?.toString()?.split(",");
        }else {
            def c = User.createCriteria().list{
                eq("companyDealer",params?.companyDealer)
                roles{
                    ilike("name","%SA & LSA%");
                }
            }
            listSA = c?.username
        }

        for(int a=blnAwal;a<=blnAkhir;a++){
            def dateAwal = new Date()?.parse("dd/M/yyyy","01/"+a+"/"+params?.tahun);
            def tanggalAkhir = new MonthlyBalanceController()?.tanggalAkhir(a,params?.tahun?.toInteger());
            def dateAkhir = new Date()?.parse("dd/M/yyyy",tanggalAkhir+"/"+a+"/"+params?.tahun);
            listSA.each {
                def planByTelp = 0, planBySMS = 0,realSamePhone = 0, realOthersPhone = 0,realSameSMS = 0,realOthersSMS = 0,responseSameSMS = 0,responseOtherSMS = 0
                def connectSamePhone = 0,connectOthersPhone = 0,connectSameSMS = 0,connectOthersSMS = 0, suksesCallRate = 0, fuPhoneRate = 0, suksesSMSRate = 0,fuSMSRate = 0
                def q1Phone = 0,q2Phone=0,q3Phone=0,q1sms=0,q2sms=0,q3sms=0,q1OtherPhone = 0,q2OtherPhone = 0,q3OtherPhone = 0, q1OtherSms = 0, q2OtherSms = 0, q3OtherSms = 0
                def firPhone = 0,firSMS = 0;
                def nama = it
                def listFollowUp = FollowUp.createCriteria().list {
                    eq("companyDealer",params.companyDealer);
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","BP")
                            }
                        }
                        eq("t401NamaSA",nama);
                    }
                    ge("t801TglJamFU",dateAwal);
                    lt("t801TglJamFU",dateAkhir+1);
                    order("t801TglJamFU");
                }
                listFollowUp.each {
                    Date tgglFU = it?.t801TglJamFU?.clearTime()
                    Date tgglDelivery = (tgglFU-3).clearTime()
                    if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                        planBySMS++;
                    }else {
                        planByTelp++;
                    }
                    def follUp = it
                    def realFU = RealisasiFU.createCriteria().list {
                        eq("followUp",follUp)
                    }
                    realFU.each {
                        if(it?.metodeFu?.m801NamaMetodeFu?.toUpperCase()?.contains("SMS")){
                            if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                                realSameSMS++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectSameSMS++;
                                }
                                if(it?.t802StaBalasanSMS){
                                    responseSameSMS++;
                                }
                            }else{
                                realOthersSMS++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectOthersSMS++;
                                }
                                if(it?.t802StaBalasanSMS){
                                    responseOtherSMS++;
                                }
                            }
                        }else {
                            if((it?.t802TglJamFU?.compareTo(tgglFU)==0 || it?.t802TglJamFU?.after(tgglFU)) && it?.t802TglJamFU?.before(tgglFU+1)){
                                realSamePhone++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectSamePhone++;
                                }
                            }else{
                                realOthersPhone++;
                                if(it?.t802StaTerhubung && it?.t802StaTerhubung=="1"){
                                    connectOthersPhone++;
                                }
                            }
                        }
                        def reFU = it
                    }
                    suksesCallRate = (connectSamePhone==0 && connectOthersPhone == 0) || (realSamePhone==0 && realOthersPhone == 0) ? 0 : (connectSamePhone + connectOthersPhone)/(realSamePhone+realOthersPhone)*100
                    fuPhoneRate = connectSamePhone == 0 || planByTelp ? 0 : connectSamePhone / planByTelp * 100
                    suksesSMSRate = (connectSameSMS==0 && connectOthersSMS==0) || (realSameSMS==0 && realOthersSMS==0) ? 0 :(connectSameSMS + connectOthersSMS)/(realSameSMS+realOthersSMS)*100
                    fuSMSRate = (responseSameSMS==0 || planBySMS==0) ? 0 : responseSameSMS / planBySMS * 100
                }
                if(listFollowUp?.size()>0){
                    rows << [
                            column_0: dateAwal?.format("MMMM"),
                            column_1: suksesCallRate,
                            column_2: fuPhoneRate,
                            column_3: suksesSMSRate,
                            column_4: fuSMSRate
                    ]
                }
            }
        }

        return rows
    }

    def datatablesBPCustomerFollowupPhone(def params, def type){
        String metodFU = "Telpon"
        if(type==1){
            metodFU="SMS";
        }
        def awal = new Date().parse("dd-MM-yyyy",params.tanggal);
        def akhir = new Date().parse("dd-MM-yyyy",params.tanggal2);
        def namaSA = params.sa;
        def listSA = []
        if(namaSA!=""){
            namaSA = namaSA?.toString()?.replaceAll("\'","");
            listSA = namaSA?.toString()?.split(",");
        }else {
            def c = User.createCriteria().list{
                eq("companyDealer",params?.companyDealer)
                roles{
                    ilike("name","%SA & LSA%");
                }
            }
            listSA = c?.username
        }

        def rows = []

        def listFollowUp = RealisasiFU.createCriteria().list {
            followUp{
                eq("companyDealer",params.companyDealer);
                reception{
                    customerIn{
                        tujuanKedatangan{
                            eq("m400Tujuan","BP")
                        }
                    }
                    if(listSA?.size()>0){
                        inList("t401NamaSA",listSA);
                    }
                }
                metodeFu{
                    ilike("m801NamaMetodeFu","%"+metodFU+"%")
                }
            }
            ge("t802TglJamFU",awal);
            lt("t802TglJamFU",akhir+1)
            order("t802TglJamFU")
        }

        listFollowUp.each {

            rows << [
                    column_0: it?.t802TglJamFU?.format("dd-MM-yyyy"),
                    column_1: it?.followUp?.reception?.historyCustomerVehicle?.fullNoPol,
                    column_2: it?.followUp?.reception?.t401NoWO,
                    column_3: it?.followUp?.reception?.historyCustomer?.fullNama,
                    column_4: it?.followUp?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    column_5: it?.t802NamaSA_FU,
                    column_6: it?.t802StaTerhubung && it?.t802StaTerhubung=="1" ? "Terhubung" : "Tidak Terhubung"
            ]
        }
        return rows
    }

}
 
