package com.kombos.kriteriaReport

import com.kombos.administrasi.ModelName
import com.kombos.administrasi.NamaManPower
import com.kombos.maintable.StatusKendaraan
import com.kombos.maintable.TipeKerusakan
import com.kombos.woinformation.JobRCP

class GatePassService {
	boolean transactional = false

	def sessionFactory

	def datatablesKendaraanList(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = ModelName.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			if (params."m104NamaModelName") {
				ilike("m104NamaModelName", "%" + (params."m104NamaModelName" as String) + "%")
			}

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				m104NamaModelName: it.m104NamaModelName

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}

	def datatablesTechnisianList(def params){

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = NamaManPower.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			if (params."teknisi") {
				ilike("t015NamaBoard", "%" + (params."teknisi" as String) + "%")
			}

			userProfile{
				roles{
					ilike("name","%" + 'Teknisi' + "%")
				}
			}

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				teknisi: it.t015NamaBoard+' - '+it?.userProfile?.t001Inisial

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}

	def datatablesSAList(def params){

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = NamaManPower.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			if (params."sa") {
				ilike("t015NamaBoard", "%" + (params."sa" as String) + "%")
			}

			userProfile{
				roles{
					ilike("name","%" + 'SA' + "%")
				}
			}

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				namaSa: it.t015NamaBoard+' - '+it?.userProfile?.t001Inisial

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}

	def datatablesJenisPayment(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = JobRCP.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if (params."payment") {
				ilike("t402StaCashInsurance", "%" + (params."payment" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPayment: it.t402StaCashInsurance == '1' ? 'Cash' : 'Insurance'

			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret
	}

	def datatablesJenisPekerjaan(def params){
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = TipeKerusakan.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			if (params."pekerjaan") {
				ilike("m401NamaTipeKerusakan", "%" + (params."pekerjaan" as String) + "%")
			}
			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}
		def rows = []

		results.each {
			rows << [
				id: it.id,
				namaPekerjaan: it.m401NamaTipeKerusakan
			]
		}
		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		return ret	
	}

	def datatablesStoppedList(def params){

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = StatusKendaraan.createCriteria()
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

			switch (sortProperty) {
				default:
					order(sortProperty, sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				m999NamaStatusKendaraan: it.m999NamaStatusKendaraan

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		return ret
	}


}
 
