package com.kombos.example



import com.kombos.baseapp.AppSettingParam

class CekcekService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Cekcek.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_agama"){
				eq("agama",params."sCriteria_agama")
			}

			if(params."sCriteria_alamat"){
				ilike("alamat","%" + (params."sCriteria_alamat" as String) + "%")
			}

			if(params."sCriteria_jumlahKendaraan"){
				eq("jumlahKendaraan",params."sCriteria_jumlahKendaraan")
			}

			if(params."sCriteria_kota"){
				ilike("kota","%" + (params."sCriteria_kota" as String) + "%")
			}

			if(params."sCriteria_nama"){
				ilike("nama","%" + (params."sCriteria_nama" as String) + "%")
			}

			if(params."sCriteria_tanggalLahir"){
				ge("tanggalLahir",params."sCriteria_tanggalLahir")
				lt("tanggalLahir",params."sCriteria_tanggalLahir" + 1)
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						agama: it.agama,
			
						alamat: it.alamat,
			
						jumlahKendaraan: it.jumlahKendaraan,
			
						kota: it.kota,
			
						nama: it.nama,
			
						tanggalLahir: it.tanggalLahir?it.tanggalLahir.format(dateFormat):"",
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Cekcek", params.id] ]
			return result
		}

		result.cekcekInstance = Cekcek.get(params.id)

		if(!result.cekcekInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Cekcek", params.id] ]
			return result
		}

		result.cekcekInstance = Cekcek.get(params.id)

		if(!result.cekcekInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Cekcek", params.id] ]
			return result
		}

		result.cekcekInstance = Cekcek.get(params.id)

		if(!result.cekcekInstance)
			return fail(code:"default.not.found.message")

		try {
			result.cekcekInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		Cekcek.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.cekcekInstance && m.field)
					result.cekcekInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Cekcek", params.id] ]
				return result
			}

			result.cekcekInstance = Cekcek.get(params.id)

			if(!result.cekcekInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.cekcekInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.cekcekInstance.properties = params

			if(result.cekcekInstance.hasErrors() || !result.cekcekInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Cekcek", params.id] ]
			return result
		}

		result.cekcekInstance = new Cekcek()
		result.cekcekInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.cekcekInstance && m.field)
				result.cekcekInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Cekcek", params.id] ]
			return result
		}

		result.cekcekInstance = new Cekcek(params)

		if(result.cekcekInstance.hasErrors() || !result.cekcekInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def cekcek =  Cekcek.findById(params.id)
		if (cekcek) {
			cekcek."${params.name}" = params.value
			cekcek.save()
			if (cekcek.hasErrors()) {
				throw new Exception("${cekcek.errors}")
			}
		}else{
			throw new Exception("Cekcek not found")
		}
	}

}