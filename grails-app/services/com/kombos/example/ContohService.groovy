package com.kombos.example



import org.activiti.engine.impl.pvm.delegate.ActivityExecution

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User

class ContohService {	
	boolean transactional = false
	
	
	def datatablesList(def params) {
		def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Contoh.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_alamat"){
				ilike("alamat","%" + (params."sCriteria_alamat" as String) + "%")
			}

			if(params."sCriteria_jumlahKendaraan"){
				eq("jumlahKendaraan",params."sCriteria_jumlahKendaraan")
			}

			if(params."sCriteria_kota"){
				ilike("kota","%" + (params."sCriteria_kota" as String) + "%")
			}

			if(params."sCriteria_nama"){
				ilike("nama","%" + (params."sCriteria_nama" as String) + "%")
			}

			if(params."sCriteria_tanggalLahir"){
				ge("tanggalLahir",params."sCriteria_tanggalLahir")
				lt("tanggalLahir",params."sCriteria_tanggalLahir" + 1)
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						alamat: it.alamat,
			
						jumlahKendaraan: it.jumlahKendaraan,
			
						kota: it.kota,
			
						nama: it.nama,
			
						tanggalLahir: it.tanggalLahir?it.tanggalLahir.format(dateFormat):"",
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(id) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Contoh", id as long] ]
			return result
		}

		result.contohInstance = Contoh.get(id)

		if(!result.contohInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Contoh", params.id] ]
			return result
		}

		result.contohInstance = Contoh.get(params.id)

		if(!result.contohInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Contoh", params.id] ]
			return result
		}

		result.contohInstance = Contoh.get(params.id)

		if(!result.contohInstance)
			return fail(code:"default.not.found.message")

		try {
			result.contohInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		Contoh.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.contohInstance && m.field)
					result.contohInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Contoh", params.id] ]
				return result
			}

			result.contohInstance = Contoh.get(params.id)

			if(!result.contohInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.contohInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.contohInstance.properties = params

			if(result.contohInstance.hasErrors() || !result.contohInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Contoh", params.id] ]
			return result
		}

		result.contohInstance = new Contoh()
		result.contohInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.contohInstance && m.field)
				result.contohInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Contoh", params.id] ]
			return result
		}

		result.contohInstance = new Contoh(params)

		if(result.contohInstance.hasErrors() || !result.contohInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def contoh =  Contoh.findById(params.id)
		if (contoh) {
			contoh."${params.name}" = params.value
			contoh.save()
			if (contoh.hasErrors()) {
				throw new Exception("${contoh.errors}")
			}
		}else{
			throw new Exception("Contoh not found")
		}
	}
	
	def requestTo(def username){
		def ret = []
		def user = User.findByUsername(username)

		def companyDealer = user.companyDealer
		
		def kepalaBengkel = Role.findByName("KEPALA_BENGKEL")
		
		kepalaBengkel.users.each {
			ret << it.username
		}

		ret
	}
	
	def assignTo(def username){
		def ret = []
		def user = User.findByUsername(username)

		def companyDealer = user.companyDealer
		
		def teknisi = Role.findByName("TEKNISI")
		
		teknisi.users.each {
			ret << it.username
		}

		ret
	}
	
	def sayHello(ActivityExecution execution) throws Exception {
        def variables = execution.getVariables()
        log.info("invitationScheduler " + execution.getVariables())
	}

}