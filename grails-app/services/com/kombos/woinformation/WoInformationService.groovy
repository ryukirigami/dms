package com.kombos.woinformation

import com.kombos.administrasi.FlatRate
import com.kombos.administrasi.GroupManPower
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.PartJob
import com.kombos.administrasi.Stall
import com.kombos.administrasi.TarifPerJam
import com.kombos.baseapp.AppSettingParam
import com.kombos.board.JPB
import com.kombos.customercomplaint.Complaint
import com.kombos.maintable.BookingFee
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.Kwitansi
import com.kombos.parts.GoodsHargaJual
import com.kombos.parts.Invoice
import com.kombos.parts.Konversi
import com.kombos.reception.KeluhanRcp
import com.kombos.reception.Reception

class WoInformationService {

    boolean transactional = false
    def konversi = new Konversi()
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Complaint.createCriteria()
        def results = c.list () {
            eq("staDel","0")


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def findData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def receptionInstance = Reception.createCriteria()
        def results = receptionInstance.list {
            eq("staSave","0");
            eq("staDel","0");
            eq("companyDealer",params?.companyDealer)
            if(params."kategori"=="Nomor WO"){
                like("t401NoWO","%" + (params."key" as String) + "%")
                eq("staDel","0")
            }else{
                historyCustomerVehicle{
                    kodeKotaNoPol{
                        eq("m116ID",params.noPolAwal.replace(" ",""),[ignoreCase: true])
                    }
                    eq("t183NoPolTengah",params.noPolTengah.replace(" ",""),[ignoreCase: true])
                    eq("t183NoPolBelakang",params.noPolAkhir.replace(" ",""),[ignoreCase: true])
                }
            }
            order("dateCreated","desc")
            maxResults(1)
        }

        def rows = []

        results.each {
            //
            def jpb=JPB.findByReceptionAndStaDel(it, "0")

            def keluhanItemsDb = KeluhanRcp.findAllByReceptionAndStaDel(it,"0")*.t411NamaKeluhan

            if(keluhanItemsDb){
                keluhanItemsDb.each{
                    keluhanItemsDb
                }
            }else{
                keluhanItemsDb="-"
            }

            def instance=Prediagnosis.findByReceptionAndStaDel(it,"0")
            String getKeluhan = "";
            String getStatusKend = "";
            String getPermintaan="";
            if(instance!=null){
                getKeluhan=instance.t406KeluhanCust;
            }
            def recept = it
            def instance2 = Progress.createCriteria().get {
                eq("staDel","0")
                eq("reception",recept)
                order("dateCreated","desc")
                maxResults(1);
            }


            if(instance2){
                getStatusKend=instance2?.statusKendaraan?.m999NamaStatusKendaraan?.toUpperCase();
            }

            for(int a=0;a<it.t401PermintaanCusts.size();a++){
                getPermintaan+=it.t401PermintaanCusts.permintaan
                if(a<it.t401PermintaanCusts.size()-1){
                    getPermintaan+=" , "
                }
            }

            def si = ""
            try {
                InvoiceT701.findAllByReceptionAndT701StaDel(it,'0').each {
                    if(!it.t701NoInv_Reff && (!it.t701StaApprovedReversal)){
                        si += it.t701NoInv +" - ($it.t701Customer)($it.t701JenisInv) ("+ it.t701TglJamInvoice.format('dd/MM/yyyy') + ") \n"
                    }
                }
                si = si.substring(0,si.length()-1)
            }catch (Exception e){

            }

            rows << [
                    id : it.id,

                    t401NoWo : it.t401NoWO ,

                    noPolisi : it.historyCustomerVehicle.fullNoPol,

                    modelKendaraan : it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,

                    vehicle : it?.historyCustomerVehicle?.customerVehicle?.t103VinCode,

                    tanggalWO : it.t401TanggalWO ? it.t401TanggalWO.format(dateFormat) : "",

                    waktuPenyerahan : it.t401TglJamJanjiPenyerahan ? it.t401TglJamJanjiPenyerahan.format(dateFormat) : "",

                    namaCustomer : it?.historyCustomer?.fullNama,

                    alamatCustomer : it?.historyCustomer?.t182Alamat,

                    teleponCustomer : it?.historyCustomer?.t182NoTelpRumah ? it?.historyCustomer?.t182NoTelpRumah : it?.historyCustomer?.t182NoHp,

                    keluhan : keluhanItemsDb,

                    permintaan : getPermintaan,

                    si  : si,

                    stall : jpb?.stall?.m022NamaStall ? jpb?.stall?.m022NamaStall:"-",

                    fir : instance ? instance?.t406T305_M302_ID : '',

                    staKendaraan :getStatusKend
            ]
        }

        return rows
    }

    def getTableBiayaData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def subtotal = 0,total=0
        def rows = []
        def c = JobRCP.createCriteria()
        def results = c.list() {
            reception{
                eq("t401NoWO",params.noWO)
            }
            eq("staDel","0")
            or{
                ilike("t402StaTambahKurang","0")
                and{
                    not{
                        ilike("t402StaTambahKurang","%1%")
                        ilike("t402StaApproveTambahKurang","%1%")
                    }
                }
                and {
                    isNull("t402StaTambahKurang")
                    isNull("t402StaApproveTambahKurang")
                }
            }
        }

        def hargaJob = 0
        def discJasa = 0;
        def discPart = 0;
        results.each {
            total = 0
            hargaJob = (it?.t402JmlSublet != null ? it?.t402JmlSublet?.toDouble() : it?.t402HargaRp)
            discJasa+=(it?.t402DiscRp ?it?.t402DiscRp: 0)

            total += hargaJob
            subtotal+=total
            String billTo = "";
            if(it?.t402namaBillTo){
                billTo = " - BillTo ("+it?.t402namaBillTo+")"
            }

            if(it.t402HargaBeliSublet!=null){
                String app = ""
                if(it.t402StaApprovalSublet=="0"){
                    app = " (Waiting Approval)"
                }

                if(it.t402StaApprovalSublet!="2"){
                    billTo += " -> Sublet ( "+ it?.vendor?.m121Nama +") " + app
                }
            }

            rows << [
                    namaJobParts : it.operation.m053NamaOperation+billTo,

                    hargaJob : konversi.toRupiah(Math.round(hargaJob)),

                    hargaParts : 0,

                    total : konversi.toRupiah(Math.round(total))

            ]

        }
        def hargaParts=0
        def partrcp = PartsRCP.createCriteria().list {
            eq("staDel","0");
            eq("reception",Reception.findByT401NoWO(params.noWO));
            or{
                ilike("t403StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t403StaTambahKurang","%1%")
                        ilike("t403StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t403StaTambahKurang")
                    isNull("t403StaApproveTambahKurang")
                }
            }
        }

        partrcp.each {
            discPart+= (it?.t403DiscRp?it?.t403DiscRp:0)
            hargaParts = it?.t403HargaRp
            total = (hargaParts * it?.t403Jumlah1)
            subtotal+=total
            String billTo = "";
            if(it?.t403namaBillTo){
                billTo = " - BillTo ("+it?.t403namaBillTo+")"
            }
            rows << [

                    namaJobParts : "Parts "+it.goods.m111ID+"||"+it.goods.m111Nama+billTo ,

                    hargaJob : it?.t403Jumlah1,

                    hargaParts : konversi.toRupiah(Math.round(hargaParts)),

                    total : konversi.toRupiah(Math.round(total))

            ]
        }

        if(results){
            rows << [
                    namaJobParts : 'DISCOUNT JASA',

                    hargaJob : '',

                    hargaParts : '',

                    total: konversi.toRupiah(Math.round(discJasa))

            ]

            rows << [
                    namaJobParts : 'DISCOUNT PARTS & BAHAN',

                    hargaJob : '',

                    hargaParts : '',

                    total: konversi.toRupiah(Math.round(discPart))

            ]
            subtotal = Math.round(subtotal) - Math.round(discJasa) - Math.round(discPart)
            rows << [
                    namaJobParts : 'SUB TOTAL',

                    hargaJob : '',

                    hargaParts : '',

                    total: konversi.toRupiah(Math.round(subtotal))

            ]

            def ppn = Math.round((10*subtotal)/100)
            rows << [
                    namaJobParts : 'PPN (10%)',

                    hargaJob : ' ',

                    hargaParts : ' ',

                    total: ppn?konversi.toRupiah(Math.round(ppn)):0

            ]

            rows << [
                    namaJobParts : 'GRAND TOTAL',

                    hargaJob : " ",

                    hargaParts : ' ',

                    total: konversi.toRupiah(Math.round(subtotal+ppn))

            ]

            def rpBF = 0
            def rpOR = 0
            def kwitansi = Kwitansi.findByReceptionAndT722StaDel(Reception.findByT401NoWOIlike("%"+params.noWO+"%"),"0")
            if(kwitansi){
                def bookingFee = BookingFee.findByKwitansiAndT721StaDel(kwitansi,"0")
                if(bookingFee){
                    if(kwitansi?.t722StaBookingOnRisk?.equalsIgnoreCase("0")){
                        rpBF = bookingFee?.t721JmlhBayar
                    }else{
                        rpOR = bookingFee?.t721JmlhBayar
                    }
                }
            }

            def totalBFOR = rpBF+rpOR
            rows << [
                    namaJobParts : 'BOOKING FEE/ON RISK',

                    hargaJob : '',

                    hargaParts : '',

                    total: konversi?.toRupiah(Math.round(totalBFOR))

            ]

        }


        return rows
    }


    def getTableRateData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def rows2 = []

        def e = JobRCP.createCriteria()
        def resultsRate = e.list() {
            eq("staDel","0")
            or{
                ilike("t402StaTambahKurang","0")
                and{
                    not{
                        ilike("t402StaTambahKurang","%1%")
                        ilike("t402StaApproveTambahKurang","%1%")
                    }
                }
                and {
                    isNull("t402StaTambahKurang")
                    isNull("t402StaApproveTambahKurang")
                }
            }
            reception{
                eq("t401NoWO",params.noWO)
            }
        }

        String tek = ""

        resultsRate.each {
            def jpb = JPB.findAllByReceptionAndStaDel(it?.reception, "0")*.namaManPower?.t015NamaBoard?.unique()
            def cFr = JPB.findByReceptionAndStaDel(it?.reception, "0")
            def fr = GroupManPower.findById(cFr?.namaManPower?.groupManPower?.id)
            def actualRate = ActualRateTeknisi.findByReception(it.reception)
            Double findRate = null

            jpb.each {
                tek = jpb?.toString()?.substring(1,jpb?.toString()?.length() - 1)
            }

            if(actualRate){
                findRate = actualRate?.t453ActualRate
            }else{
                findRate = it?.t402Rate
            }

            rows2 << [

                    namaJob : it.operation.m053NamaOperation ,
                    teknisi : tek,
                    rate : findRate ? findRate : "",
                    namaSa : cFr?.reception?.t401NamaSA ? cFr?.reception?.t401NamaSA?.toUpperCase() : "-",
                    namaForeman : fr?.namaManPowerForeman?.t015NamaLengkap ? fr?.namaManPowerForeman?.t015NamaLengkap : "-"

            ]
        }
        return rows2
    }

    def save(params) {
        def instance = Reception.findByT401NoWOAndStaDelAndStaSave(params.noWO,"0","0")
        def instanceStall = Stall.get(Long.parseLong(params.stall))
        if (instance!=null){
            instance.stall = instanceStall
            instance.lastUpdated = datatablesUtilService?.syncTime()
            instance.save()
        }
        return "OK"
    }
}