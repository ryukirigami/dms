package com.kombos.delivery

/**
 * Created by IntelliJ IDEA.
 * User: IBaihaqi
 * Date: 6/17/14
 */
class ExportFileService {

    def salesDataTablesList(def params) {
        def rows = []
        def output = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
        return output
    }
    def spkInvoiceSettlementDataTablesList(def params) {
        def rows = []
        def output = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
        return output
    }
    def bookingFeeDataTablesList(def params) {
        def rows = []
        def output = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
        return output
    }
    def refundBookingFeeDataTablesList(def params) {
        def rows = []
        def output = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
        return output
    }

}
