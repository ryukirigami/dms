package com.kombos.delivery

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.KasKasir
import com.kombos.parts.Konversi
import grails.util.Environment

/**
 * Created by IntelliJ IDEA.
 * User: IBaihaqi
 * Date: 6/17/14
 */
class SetoranBankService {
    def conversi = new Konversi()

    def setoranBankDataTablesList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = KasKasir.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                if (params."sCriteria_tglFrom") {
                    ge("t707TanggalSetorBank", params."sCriteria_tglFrom")
                    lt("t707TanggalSetorBank", params."sCriteria_tglTo" + 1)
                }
            }

            //eq("t701StaDel","0")

//            switch(sortProperty){
//                default:
//                    order(sortProperty,sortDir)
//                    break;
//            }
        }
        def rows = []
        results.each {
            rows << [
                id: it.id,
                    tglSetor: it.t707TanggalSetorBank,
                    setorKeBank: it.t707NamaBank,
                    namaAccount: it.t707AtasNama,
                    nomorAccount: it.t707NoRekening,
                    jumlahSetor: conversi.toRupiah(it.t707JmlSetor),
                    kasir: it.t707xNamaUser,
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }
                                          /* bootstrap */
    def bootstrap() {
        def currentEnv = Environment.current

        //initial data Invoice List
        if (currentEnv == Environment.DEVELOPMENT) {
            initKasKasir()
        }

    }

    private def initKasKasir() {
        def kasKasirA = new KasKasir(
            companyDealer: CompanyDealer.first(),
            t707Tanggal: new Date(),
            t707SaldoAwal: 1000000,
            t707SaldoAkhir: 1100000,
            t707TanggalSetorBank: new Date(),
            t707NamaBank: "BCA",
            t707AtasNama: "Atas Nama",
            t707NoRekening: "1112223330",
            t707JmlSetor: 100000,
            t707SaldoAkhirMinSetor: 500000,
            t707xNamaUser: "namaUser",
            t707xDivisi: "Divisi",
            t707StaDel: "0",
            createdBy: "SYSTEM", updatedBy: "SYSTEM", lastUpdProcess: "INSERT"
        )
        kasKasirA.save(flush: true)
        def kasKasirB = new KasKasir(
            companyDealer: CompanyDealer.findByM011ID("002"),
            t707Tanggal: new Date(),
            t707SaldoAwal: 2000000,
            t707SaldoAkhir: 1100000,
            t707TanggalSetorBank: new Date(),
            t707NamaBank: "Mandiri",
            t707AtasNama: "Nama 2",
            t707NoRekening: "2222223330",
            t707JmlSetor: 200000,
            t707SaldoAkhirMinSetor: 600000,
            t707xNamaUser: "namaUser",
            t707xDivisi: "Divisi",
            t707StaDel: "0",
            createdBy: "SYSTEM", updatedBy: "SYSTEM", lastUpdProcess: "INSERT"
        )
        kasKasirB.save(flush: true)
        //println "setelah save kasKasir B"
    }

}
