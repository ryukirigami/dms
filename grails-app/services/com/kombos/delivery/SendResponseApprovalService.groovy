package com.kombos.delivery

import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.SPK
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.MetodeBayar
import com.kombos.parts.Konversi
import com.kombos.reception.Reception
import grails.util.Environment

/**
 * Created by IntelliJ IDEA.
 * User: IBaihaqi
 * Date: 6/17/14
 */
class SendResponseApprovalService {
    def conversi = new Konversi()

    def sendApprovalDataTablesList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = InvoiceT701.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_tglFrom" && params."sCriteria_tglTo") {
                if (params."sCriteria_tglFrom") {
                    ge("t701TglJamInvoice", params."sCriteria_tglFrom")
                    lt("t701TglJamInvoice", params."sCriteria_tglTo" + 1)
                }
            }

            String statusApproval = params."sCriteria_statusApproval"
            //println("statusApproval=" + statusApproval)
//            if(statusApproval && !statusApproval.equals("")){
//                ilike("t701StaApprovalGoodWill","%" + (params."sCriteria_statusApproval" as String) + "%")
//            }
            //println("sCriteria_nomorInvoice=" + params."sCriteria_nomorInvoice")
            if(params."sCriteria_nomorInvoice"){
                ilike("t701NoInv_Reff","%" + (params."sCriteria_nomorInvoice" as String) + "%")
            }
//            if(params."sCriteria_nomorPolisi"){
//                ilike("","%" + (params."sCriteria_nomorPolisi" as String) + "%")
//            }
            //eq("t701StaDel","0")

//            switch(sortProperty){
//                default:
//                    order(sortProperty,sortDir)
//                    break;
//            }
        }
        def rows = []
        results.each {
            rows << [
                id: it?.id,
                tglRequest: it?.t701TglRequestApprovalGoodWill ? it?.t701TglRequestApprovalGoodWill : "-",
                noInvoice: it?.t701NoInv ? it?.t701NoInv : "-",
                customer: it?.historyCustomer?.fullNama ? it?.historyCustomer?.fullNama : "-",
                alamat: it?.t701Alamat ? it?.t701Alamat : "-",
                nopol: it?.t701Nopol ? it?.t701Nopol : "-",
                totalInvoice: it.t701TotalInv ? conversi.toRupiah(it.t701TotalInv) : "0",
                rpDiscReq: it.t701RequestGoodWillRp ? conversi.toRupiah(it.t701RequestGoodWillRp) : "0",
                persenDiscReq: it.t701RequestGoodWillPersen ? it.t701RequestGoodWillPersen : "0",
                status: it.t701StaApprovalGoodWill.equals("0") ? "WAIT_FOR_APPROVAL" : it.t701StaApprovalGoodWill.equals("1") ? "APPROVED" :
                        it.t701StaApprovalGoodWill.equals("2") ? "REJECTED" : "WAIT_TO_SEND",    //WAIT_FOR_APPROVAL("0"), APPROVED("1"), REJECTED("2"), WAIT_TO_SEND("3")
                tglRespon: it.t701TglResponApprovalGoodWill ? it.t701TglResponApprovalGoodWill : "-",
                rpDiscRespon: it.t701GoodWillRp ? conversi.toRupiah(it.t701GoodWillRp) : "0",
                persenDiscRespon: it.t701GoodWillPersen ? it.t701GoodWillPersen : "0",
                keterangan: it.t701xKet ? it.t701xKet : "-",    //AlasanUnApprovedGoodWill
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

}
