package com.kombos.delivery

import com.kombos.approval.AfterApprovalInterface
import com.kombos.maintable.JOCTECO
import com.kombos.parts.StatusApproval

/**
 * Created by Ahmad Fawaz on 25/11/14.
 */
class JobOrderCompletionService implements AfterApprovalInterface{
    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        def joc = JOCTECO.get(fks.toLong())
        joc?.t601TglJamJOCSelesai = new Date()
        joc?.t601xKetExp = alasanUnApprove
        if (staApproval == StatusApproval.APPROVED) {
            joc?.t601staOkCancelJOC="0"
        }else if (staApproval == StatusApproval.REJECTED) {
            joc?.t601staOkCancelJOC="1"
        }
        joc?.save(flush: true)
    }
}
