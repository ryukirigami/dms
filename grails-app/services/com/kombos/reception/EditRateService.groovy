package com.kombos.reception

import com.kombos.administrasi.TarifPerJam
import com.kombos.administrasi.TarifTWC
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.parts.StatusApproval
import com.kombos.woinformation.JobRCP

/**
 * Created by HP_Probook on 10/21/14.
 */
class EditRateService implements AfterApprovalInterface {
    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        if (staApproval == StatusApproval.APPROVED) {
            def datatableListUtilService = new DatatablesUtilService()
            def param = fks.split('#')
            def job = JobRCP?.get(param[0].toLong())
            def rateBefore = job?.t402Rate
            if(rateBefore<=0){
                def receptionInstance = job?.reception
                def companyDealer = receptionInstance?.companyDealer
                def operationInstance = job?.operation
                def nominalValue = 0 , rate = Double.valueOf(param[1].replace(",",".").toString())

                def harga = TarifPerJam.findByKategoriAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(receptionInstance?.historyCustomerVehicle?.fullModelCode?.baseModel?.kategoriKendaraan,"0",receptionInstance?.companyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
                def nominal1 = harga && rate ? harga?.t152TarifPerjamBP * rate : 0
                def nominal2 = harga && rate ? harga?.t152TarifPerjamSB * rate : 0
                def nominal3 = harga && rate ? harga?.t152TarifPerjamGR * rate : 0
                def nominalTam = harga && rate ? harga?.t152TarifPerjamDealer * rate : 0
                def nominal4 = harga?.t152TarifPerjamPDS && rate ? harga?.t152TarifPerjamPDS * rate : 0
                def nominal5 = harga?.t152TarifPerjamSBI && rate ? harga?.t152TarifPerjamSBI * rate : 0
                def nominal6 = harga?.t152TarifPerjamSPO && rate ? harga?.t152TarifPerjamSPO * rate : 0
                if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("TWC")
                        || operationInstance?.kategoriJob?.m055KategoriJob?.toString().toUpperCase().contains("WARRANTY")){
                    def hargaTwc = TarifTWC.createCriteria().get {
                        eq("staDel","0")
                        eq("companyDealer",companyDealer)
                        eq("kategoriKendaraan",receptionInstance?.historyCustomerVehicle?.fullModelCode?.kategoriKendaraan)
                        order("t113aTMT","desc")
                        maxResults(1);
                    }
                    if(hargaTwc){
                        def nominalTemp = hargaTwc?.t113aTarifPerjamTWC
                        nominalValue = rate ? (nominalTemp * rate) : 0
                    }
                } else if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("BP")){
                    nominalValue = nominal1
                }else if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("SBE")){
                    nominalValue = nominal2
                    String jobTam = "SB10K,SB20K,SB30K,SB40K,SB50K"
                    if(jobTam.toString().contains(operationInstance.m053Id)){
                        nominalValue = nominalTam
                    }
                }else if(operationInstance?.kategoriJob?.m055KategoriJob?.toString().toUpperCase().contains("PDS")
                        && operationInstance?.m053Id?.toUpperCase().contains("CUCI")){
                    nominalValue = nominal4
                }else if(operationInstance?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SBI")){
                    nominalValue = nominal5
                }
                else{
                    if(operationInstance?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")){
                        if(harga?.t152TarifPerjamSPO != null && harga?.t152TarifPerjamSPO != 0){
                            nominalValue = nominal6
                        }else{
                            nominalValue = nominal3
                        }
                    }else{
                        nominalValue = nominal3
                    }
                }

                job?.t402Rate = rate
                job?.t402HargaRp = nominalValue
                job?.t402TotalRp = job?.t402DiscRp ? nominalValue-job?.t402DiscRp : nominalValue
            }else{
                def rate = Double.valueOf(param[1].replace(",",".").toString())
                def nilaiSekarang = (job?.t402HargaRp/rateBefore)* rate
                job?.t402Rate = rate
                job?.t402HargaRp = nilaiSekarang
                job?.t402TotalRp = job?.t402DiscRp ? nilaiSekarang-job?.t402DiscRp : nilaiSekarang
            }
            job?.save(flush: true)
        }
    }
}
