package com.kombos.reception

import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.maintable.ApprovalT770

//import com.kombos.approval.ApprovalPOSublet
import com.kombos.parts.StatusApproval
import com.kombos.woinformation.JobRCP

import java.text.DateFormat
import java.text.SimpleDateFormat

class POSubletService implements AfterApprovalInterface{
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService = new DatatablesUtilService()

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")

        def approveStatus = 0

        if(params.menungguApp == "1"){
            approveStatus = approveStatus + 1;
        }

        if(params.unapproved == "1"){
            approveStatus = approveStatus + 2;
        }

        if(params.approved == "1"){
            approveStatus = approveStatus + 4
        }

        def c = Reception.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params?.staCari){
                if(params?.staCari?.toString()?.toLowerCase()?.contains("no")){
                    eq("id",-1000.toLong())
                }
            }else{
                eq("id",-1000.toLong())
            }
            if(params?.search_noWo){
                ilike("t401NoWO", "%"+params?.search_noWo+"%")
            }
            if(params?.tglStart){
                ge("t401TanggalWO",new Date().parse("dd/MM/yyyy",params?.tglStart))
            }
            if(params?.tglEnd){
                lt("t401TanggalWO",new Date().parse("dd/MM/yyyy",params?.tglEnd)+1)
            }
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            eq("staSave","0")
            order("dateCreated","desc")
        }

        def rows = []

        results.each {

                rows << [

                        id: it?.id,

                        nomorWO: it?.t401NoWO,

                        tanggalWO: it?.t401TanggalWO ? it?.t401TanggalWO?.format(dateFormat) : "",

                        nomorPolisi : it?.historyCustomerVehicle?.fullNoPol ,

                        baseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,

                        SA: it?.t401NamaSA ? User.findByUsername(it?.t401NamaSA)?.fullname : "",


                ]

        }
//        def hasil = []
//        int mulai = params.iDisplayStart.toLong()
//        for(int a = mulai;a< (mulai + 10 <= rows.size() ? mulai+10 : rows.size()) ;a++){
//            hasil << rows[a]
//        }
        [sEcho: params.sEcho, iTotalRecords: results?.totalCount, iTotalDisplayRecords: results?.totalCount, aaData: rows]

    }

    def datatablesSubList(params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params.idReception){
                reception{
                    eq("id",Long.parseLong(params.idReception))
                }
            }

            or{
                ilike("t402StaTambahKurang","0")
                and{
                    not{
                        ilike("t402StaTambahKurang","%1%")
                        ilike("t402StaApproveTambahKurang","%1%")
                    }
                }
                and {
                    isNull("t402StaTambahKurang")
                    isNull("t402StaApproveTambahKurang")
                }
            }

            eq("staDel","0")
        }

        def rows = []

        results.sort {
        //    it.t412NoPOSublet
        }

        def status = ["Waiting for approval","Approved","UnApproved"]
        results.each {
            rows << [

                    id : it?.id,

                    kodeJob: it.operation?.m053Id,

                    namaJob: it.operation?.m053NamaOperation,

                    vendor : it.vendor?.m121Nama,

                    hargaBeli : it?.t402HargaBeliSublet,

                    hargaJual : it?.t402HargaBeliSublet? it?.t402HargaRp:'',

                    statusApproval : it?.t402StaApprovalSublet && it?.t402StaApprovalSublet?.isNumber() ? status[it?.t402StaApprovalSublet.toInteger()] : "",

                    alasanUnApproved : it?.t402AlasanUnAproved,

                    tanggalKirimSublet : POSublet.findByReceptionAndOperation(it?.reception, it?.operation)?.t412TanggalPO?.format("dd/MM/YYYY"),

                    tanggalSelesaiSublet : POSublet.findByReceptionAndOperation(it?.reception, it?.operation)?.t412TanggalPO?.format("dd/MM/YYYY")

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["POSublet", params.id]]
            return result
        }

        result.POSubletInstance = POSublet.get(params.id)

        if (!result.POSubletInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["POSublet", params.id]]
            return result
        }

        result.POSubletInstance = POSublet.get(params.id)

        if (!result.POSubletInstance)
            return fail(code: "default.not.found.message")

        try {
            result.POSubletInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        POSublet.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.POSubletInstance && m.field)
                    result.POSubletInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["POSublet", params.id]]
                return result
            }

            result.POSubletInstance = POSublet.get(params.id)

            if (!result.POSubletInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.POSubletInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.POSubletInstance.properties = params

            if (result.POSubletInstance.hasErrors() || !result.POSubletInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["POSublet", params.id]]
            return result
        }

        result.POSubletInstance = new POSublet()
        result.POSubletInstance.properties = params
        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.POSubletInstance && m.field)
                result.POSubletInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["POSublet", params.id]]
            return result
        }

        result.POSubletInstance = new POSublet(params)
        result.createdBy = "SYSTEM"
        result.lastUpdProcess = "INSERT"


        if (result.POSubletInstance.hasErrors() || !result.POSubletInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def POSublet = POSublet.findById(params.id)
        if (POSublet) {
            POSublet."${params.name}" = params.value
            POSublet.save()
            if (POSublet.hasErrors()) {
                throw new Exception("${POSublet.errors}")
            }
        } else {
            throw new Exception("POSublet not found")
        }
    }

    def inputPOSublet(params){
        def jobRCP = JobRCP.get(params.idJobRCP)
        def nomorWO = jobRCP.reception?.t401NoWO
        def namaJob = jobRCP.operation?.m053NamaOperation
        def vendor = jobRCP.vendor
        def hargaBeli = jobRCP.t402HargaBeliSublet

        [nomorWO : nomorWO, namaJob : namaJob, vendor : vendor, hargaBeli: hargaBeli]
    }

    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            def jobrcp = JobRCP.findById(it as Long)
            def ubhData = JobRCP.get(jobrcp.id)
            ubhData?.t402StaApprovalSublet = staApproval
            if(staApproval == StatusApproval.REJECTED){
                ubhData?.t402AlasanUnAproved = alasanUnApprove
                ubhData.t402HargaBeliSublet = null
                ubhData.t402JmlSublet = null
                ubhData.vendor = null
            }else if(staApproval == StatusApproval.APPROVED){
                def poSublet = POSublet.findByReceptionAndOperationAndStaDel(jobrcp.reception,jobrcp.operation,'0')
                if(poSublet){
                    if(!InvoiceSublet.findByPoSublet(poSublet)){
                        def invS= new InvoiceSublet()
                        invS.companyDealer = jobrcp.companyDealer
                        invS.t413NoInv = poSublet.t412NoPOSublet
                        invS.t413TanggalInv = poSublet.dateCreated
                        invS.poSublet  = poSublet
                        invS.reception = poSublet.reception
                        invS.operation = poSublet.operation
                        invS.t413JmlSublet = ubhData.t402HargaBeliSublet
                        invS.t413JmlBayar = 0
                        invS.t413xKet = ubhData.t402KeteranganSublet + " / " + ubhData.vendor.m121Nama
                        invS.t413xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        invS.t413xDivisi = 'DIVISI'
                        invS.staDel = '0'
                        invS.dateCreated  = poSublet.dateCreated
                        invS.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        invS?.lastUpdated = datatablesUtilService?.syncTime()
                        invS.lastUpdProcess  = 'INSERT'
                        invS?.save(flush: true)
                        invS?.errors.each{
                            println it
                        }
                    }
                }else{
                    poSublet = new POSublet()
                    poSublet?.companyDealer = jobrcp.companyDealer
                    poSublet?.t412NoPOSublet = new Date().format("ddMMyyHHmmss").toString()
                    poSublet?.t412TanggalPO = datatablesUtilService?.syncTime()
                    poSublet?.t412Perihal = '-'
                    poSublet?.staSend = "0"
                    poSublet?.reception = jobrcp.reception
                    poSublet?.operation = jobrcp.operation
                    poSublet?.t412xKet = jobrcp.t402KeteranganSublet
                    poSublet?.t412xNamaUser = '-'
                    poSublet?.t412xDivisi = '-'
                    poSublet?.staDel = '0'
                    poSublet?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    poSublet?.lastUpdProcess = 'Insert'
                    poSublet?.dateCreated = datatablesUtilService?.syncTime()
                    poSublet?.lastUpdated = datatablesUtilService?.syncTime()
                    poSublet?.save(flush: true)
                    poSublet?.errors.each{
                        //println it
                    }

                    def invS= new InvoiceSublet()
                    invS.companyDealer = jobrcp.companyDealer
                    invS.t413NoInv = poSublet.t412NoPOSublet
                    invS.t413TanggalInv = poSublet.dateCreated
                    invS.poSublet  = poSublet
                    invS.reception = poSublet.reception
                    invS.operation = poSublet.operation
                    invS.t413JmlSublet = ubhData.t402HargaBeliSublet
                    invS.t413JmlBayar = 0
                    invS.t413xKet = ubhData.t402KeteranganSublet + " / " + ubhData.vendor.m121Nama
                    invS.t413xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    invS.t413xDivisi = 'DIVISI'
                    invS.staDel = '0'
                    invS.dateCreated  = poSublet.dateCreated
                    invS.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    invS?.lastUpdated = datatablesUtilService?.syncTime()
                    invS.lastUpdProcess  = 'INSERT'
                    invS?.save(flush: true)
                    invS?.errors.each{
                        println it
                    }
                }

            }
            ubhData?.save(flush: true)

        }
    }
}