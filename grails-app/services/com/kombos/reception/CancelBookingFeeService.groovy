package com.kombos.reception

import com.kombos.approval.AfterApprovalInterface
import com.kombos.maintable.ApprovalT770
import com.kombos.maintable.PartsApp
import com.kombos.parts.StatusApproval

class CancelBookingFeeService implements AfterApprovalInterface{

    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        if (staApproval == StatusApproval.APPROVED) {
            PartsApp.withTransaction { status ->
                def fail = { Map m ->
                    status.setRollbackOnly()
                }

                fks.split(ApprovalT770.FKS_SEPARATOR).each {
                    def pa = PartsApp.get(it as Long)
                    pa.t303StaOkcancel = "1"
                    pa.save()
                    pa.errors.each {
                        //println it
                    }
                }

            }
        }


    }
}
