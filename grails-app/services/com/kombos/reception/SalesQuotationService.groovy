package com.kombos.reception

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customercomplaint.Complaint
import com.kombos.customerprofile.*
import com.kombos.maintable.*
import com.kombos.parts.*
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import com.kombos.woinformation.Prediagnosis
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class SalesQuotationService {

    boolean transactional = false
    def conversi=new Konversi()
    def datatablesUtilService
    def validasiOrderService

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def generateCodeService
    def staApproveJob = ["Approved","UnApproved","Waiting For Approval"]

    def datatablesList(def params){

        def c = Reception.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_tanggalReception") {
                ge("t401TanggalWO", params."sCriteria_tanggalReception")
            }

            if(params."sCriteria_tanggalReceptionTo"){
                lt("t401TanggalWO", params."sCriteria_tanggalReceptionTo" + 1)
            }

            if(params."keyword".toString()){
                historyCustomer{

                    if(params."cat"?.toString()?.equalsIgnoreCase("sCriteria_namaCustomer")){
                        ilike("fullNama", "%"+params."keyword"?.toString()+"%")
                    }

                    if(params."cat"?.toString()?.equalsIgnoreCase("sCriteria_alamat")){
                        ilike("t182Alamat", "%"+params."keyword"?.toString()+"%")
                    }

                    if(params."cat"?.toString()?.equalsIgnoreCase("sCriteria_noHp")){
                        ilike("t182NoHp", "%"+params."keyword"?.toString()+"%")
                    }
                }

                historyCustomerVehicle{
                    if(params."cat"?.toString()?.equalsIgnoreCase("sCriteria_noPol")){
                        ilike("fullNoPol", "%"+params."keyword"?.toString()+"%")
                    }

                    fullModelCode{
                        baseModel{
                            if(params."cat"?.toString()?.equalsIgnoreCase("sCriteria_kendaraan")){
                                ilike("m102NamaBaseModel", "%"+params."keyword"?.toString()+"%")
                            }
                        }
                    }

                }

                if(params."cat"?.toString()?.equalsIgnoreCase("sCriteria_noWO")){
                    ilike("t401NoWO", "%"+params."keyword"?.toString()+"%")
                }

            }

            ilike("staDel", "0")
            order("id","desc")
        }

        def rows = []
        def nos = 0
        results.each {
            nos = nos + 1

            rows << [
                    id: it.id,
                    norut : nos,
                    namaCustomer : it.historyCustomer?.fullNama,
                    alamatCustomer : it.historyCustomer?.t182Alamat,
                    nomorHp : it.historyCustomer?.t182NoHp,
                    kendaraan : it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    noPolKendaraan : it.historyCustomerVehicle?.fullNoPol,
                    NoWO : it.t401NoWO,
                    tanggalWO : it.t401TglJamCetakWO ? it.t401TglJamCetakWO?.format("dd-MMMM-yyyy") : "-"
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def jobnPartsDatatablesList(def params,def session){
        def rows = []
        def diskonPart = null
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def cv = []
        if(params?.sCriteria_nomorBelakang && params?.sCriteria_nomorTengah && params?.sCriteria_kodeKota){
            cv = HistoryCustomerVehicle.createCriteria().list {
                eq("staDel","0")
                eq("t183NoPolBelakang",params?.sCriteria_nomorBelakang?.trim(),[ignoreCase: true])
                eq("t183NoPolTengah",params?.sCriteria_nomorTengah?.trim(),[ignoreCase: true])
                kodeKotaNoPol{
                    eq("id",params?.sCriteria_kodeKota?.toLong())
                }
            }
        }
        if(params.sReceptionId){
            Reception reception = Reception.get(params.sReceptionId)
            def app = Appointment.createCriteria().get {
                eq("staDel","0")
                if(cv?.size()>0){
                    eq("historyCustomerVehicle",cv?.last())
                }else{
                    eq("id",-10000.toLong())
                }
                ge("t301TglJamRencana",dateAwal)
                le("t301TglJamRencana",dateAkhir)
                maxResults(1);
            }

            if(app && reception?.staSave!="0"){
                def jobA = JobApp.findAllByReceptionAndT302StaDel(app.reception,"0")
                if(jobA){
                    jobA.each {
                        def jobRInput = JobRCP.findByOperationAndReceptionAndStaDel(it.operation,reception,"0");
                        if(jobRInput){
                            jobRInput.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            jobRInput.lastUpdProcess = "UPDATE"
                        }else {
                            jobRInput = new JobRCP()
                            jobRInput.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            jobRInput.lastUpdProcess = "INSERT"
                            jobRInput.dateCreated = datatablesUtilService?.syncTime()
                        }
                        jobRInput.lastUpdated = datatablesUtilService?.syncTime()
                        jobRInput.companyDealer = session.userCompanyDealer
                        jobRInput.reception = reception
                        jobRInput.operation = it.operation
                        jobRInput.statusWarranty = it.statusWarranty
                        jobRInput.staDel = 0
                        jobRInput.t402StaIntExt = "i"
                        jobRInput.save(flush:true)
                        jobRInput.errors.each {//println it
                        }

                        def cekP = PartsApp.findAllByReceptionAndOperationAndT303StaDel(app?.reception,it.operation,"0")
                        if(cekP){
                            cekP.each {
                                def partIn = PartsRCP.findByReceptionAndOperationAndGoodsAndStaDel(reception,it.operation,it.goods,"0");
                                if(!partIn){
                                    partIn = new PartsRCP()
                                    partIn.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    partIn.lastUpdProcess = "INSERT"
                                    partIn.dateCreated = datatablesUtilService?.syncTime()
                                }else{
                                    partIn.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    partIn.lastUpdProcess = "UPDATE"
                                }
                                def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it.goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                                def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                                    eq("staDel","0");
                                    eq("companyDealer",session?.userCompanyDealer);
                                }
                                def mappingRegion = MappingPartRegion.createCriteria().get {
                                    if(mappingCompRegion?.size()>0){
                                        inList("region",mappingCompRegion?.region)
                                    }else{
                                        eq("id",-10000.toLong())
                                    }
                                    maxResults(1);
                                }
                                def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                                if(mappingRegion){
                                    hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent );
                                }
                                partIn.lastUpdated = datatablesUtilService?.syncTime()
                                partIn.companyDealer = session.userCompanyDealer
                                partIn.reception = reception
                                partIn.operation = it.operation
                                partIn.goods = it.goods
                                partIn.t403HargaRp = hargaTanpaPPN
                                partIn.t403Jumlah1 = it.t303Jumlah1
                                partIn.t403Jumlah2 = it.t303Jumlah2
                                partIn.t403TotalRp = it.t303Jumlah1 * hargaTanpaPPN
                                partIn.t403DPRp = it.t303DPRpP
                                partIn.t403StaIntExt = "i"
                                partIn.staDel = 0


                                partIn.save(flush: true)
                                partIn.errors.each {//println it
                                }

                            }
                        }
                    }
                }
                app.staUsed = "1";
                app.lastUpdated = datatablesUtilService?.syncTime()
                app.save(flush : true);
                app.errors.each {//println it
                }
            }
            def jobRcp = JobRCP.findAllByReceptionAndStaDel(reception,"0")
            def totalJasa = 0
            def totalPart = 0
            def discJasa = 0
            def disclPart = 0
            def totalNominal = 0
            def totalRate = 0
            def baseModel = cv?.size()>0 ? cv?.last()?.fullModelCode?.baseModel : null
            jobRcp.each { JobRCP job ->
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(reception,job?.operation,"0")
                totalJasa+=job?.t402TotalRp
                discJasa+= (job?.t402DiscRp?job?.t402DiscRp:0)
                parts.each {
                    totalPart+= it?.t403TotalRp
                    disclPart+= (it.t403DiscRp?it.t403DiscRp:0)
                }
                totalRate+=job?.t402Rate
                rows << [

                        idJob         : job?.id,

                        namaJob       : job?.operation?.m053NamaOperation,

                        kategoriJob   : job?.operation?.kategoriJob?.m055KategoriJob,

                        rate          : job?.t402Rate,

                        statusWarranty: job?.statusWarranty?.m058NamaStatusWarranty,

                        nominal       : conversi.toRupiah(job?.t402TotalRp),

                        staIntExt       : job?.t402StaIntExt=="e" ? "e" : "i",

                        staBaru       : job?.t402StaTambahKurang ? (job?.t402StaTambahKurang=="0" ? "Tambah" : "Kurang") : "",

                        staApproveJob       : job?.t402StaApproveTambahKurang ? staApproveJob[job?.t402StaApproveTambahKurang?.toInteger()] : "",

                        staDiskon       : job?.reception?.staApprovalDisc ? staApproveJob[job?.reception?.staApprovalDisc?.toInteger()] : "",

                        staPart       : parts.size()>0 ? "ada" : "tidak",

                        idReception       : job?.receptionId,

                        jasa : job?.t402DiscPersen ? Double.valueOf(job?.t402DiscPersen).longValue() : "",

                        part : diskonPart ? conversi.toRupiah(Double.valueOf(diskonPart).longValue()) : "",

                        totalRate : conversi.toRupiah(totalRate),

                        totalJasa : conversi.toRupiah(Double.valueOf(totalJasa).longValue()),

                        totalPart : conversi.toRupiah(Double.valueOf(totalPart).longValue()),

                        discJasa : conversi.toRupiah(Double.valueOf(discJasa).longValue()),

                        discPart : conversi.toRupiah(Double.valueOf(disclPart).longValue()),

                        totalNominal : conversi.toRupiah(Double.valueOf(totalPart+totalJasa).longValue()),

                        total : conversi.toRupiah(Double.valueOf((totalPart+totalJasa)-(disclPart+discJasa)).longValue())

                ]

            }
            [sEcho: params.sEcho, iTotalRecords: jobRcp.size(), iTotalDisplayRecords: jobRcp.size(), aaData: rows]
        } else {
            [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]
        }
    }

    def jobnPartsOrderDatatablesList(def params,def session){
        def rows = []

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        if(params.sReceptionId){
            Reception reception = Reception.get(params.sReceptionId)
            def jobRcp = JobRCP.findAllByReceptionAndStaDel(reception,"0")
            def totalNominal = 0
            def cv = []
            if(params?.sCriteria_nomorBelakang && params?.sCriteria_nomorTengah && params?.sCriteria_kodeKota){
                cv = HistoryCustomerVehicle.createCriteria().list {
                    eq("staDel","0")
                    eq("t183NoPolBelakang",params?.sCriteria_nomorBelakang?.trim(),[ignoreCase: true])
                    eq("t183NoPolTengah",params?.sCriteria_nomorTengah?.trim(),[ignoreCase: true])
                    kodeKotaNoPol{
                        eq("id",params?.sCriteria_kodeKota?.toLong())
                    }
                }
            }
            def baseModel = cv?.size()>0 ? cv?.last()?.fullModelCode?.baseModel : null
            jobRcp.each { JobRCP job ->
                totalNominal = job?.t402TotalRp
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(reception,job?.operation,"0")

                parts.each {
                    totalNominal+=(it?.t403TotalRp?it?.t403TotalRp:0)

                }
                rows << [

                        idJob         : job?.id,

                        namaJob       : job?.operation?.m053NamaOperation,

                        kategoriJob   : job?.operation?.kategoriJob?.m055KategoriJob,

                        rate          : job?.t402Rate ? job?.t402Rate : 0,

                        statusWarranty: job?.statusWarranty?.m058NamaStatusWarranty,

                        nominal       : conversi.toRupiah(job?.t402TotalRp),

                        staBaru       : job?.t402StaTambahKurang ? (job?.t402StaTambahKurang=="0" ? "Tambah" : "Kurang") : "",

                        staApproveJob       : job?.t402StaApproveTambahKurang ? staApproveJob[job?.t402StaApproveTambahKurang?.toInteger()] : "",

                        staPart       : parts.size()>0 ? "ada" : "tidak",

                        idReception       : job?.receptionId,

                        totalNominal : conversi.toRupiah(Double.valueOf(totalNominal).longValue())

                ]
            }
            [sEcho: params.sEcho, iTotalRecords: jobRcp.size(), iTotalDisplayRecords: jobRcp.size(), aaData: rows]
        } else {
            [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]
        }
    }

    def workDatatablesList(def params){
        def rows = []
        def size = 0
        if (params."idJob") {
            def mappingWork = MappingWorkItems.findAllByOperation(Operation.findById(params."idJob" as Long))

            mappingWork.each {
                rows << [
                        namaWork : it.workItems.m039WorkItems,
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: rows]
    }

    def partDatatablesList(def params){
        def rows = []
        def size = 0
        if (params."idJob") {
            Operation job = JobRCP.get(params."idJob" as Long).operation
            if (job) {
                def partsRCP = PartsRCP.findAllByReceptionAndOperationAndStaDel(Reception.get(params.idReception.toLong()), job,"0")

                partsRCP.each {
                    def session = RequestContextHolder.currentRequestAttributes().getSession()
                    def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it?.goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                    def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                        eq("staDel","0");
                        eq("companyDealer",session?.userCompanyDealer);
                    }
                    def mappingRegion = MappingPartRegion.createCriteria().get {
                        if(mappingCompRegion?.size()>0){
                            inList("region",mappingCompRegion?.region)
                        }else{
                            eq("id",-10000.toLong())
                        }
                        maxResults(1);
                    }
                    def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                    if(mappingRegion){
                        hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent );
                    }
                    rows << [
                            idPart        : it.id,
                            namaPart      : it.goods?.m111Nama,
                            qty           : it.t403Jumlah1,
                            satuan        : it.goods.satuan.m118Satuan1,
                            staBaru       : it?.t403StaTambahKurang ? (it?.t403StaTambahKurang=="0" ? "Tambah" : "Kurang") : "",
                            staApprove    : it?.t403StaApproveTambahKurang ? staApproveJob[it?.t403StaApproveTambahKurang?.toInteger()] : "",
                            harga         : conversi.toRupiah(hargaTanpaPPN),
                            rate          : "",
                            staIntExt     : it?.t403StaIntExt=="e" ? "e" : "i",
                            statusWarranty: "",
                            nominal       : hargaTanpaPPN ? conversi.toRupiah(hargaTanpaPPN*it?.t403Jumlah1) : 0
                    ]
                }
            }
        }

        [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: rows]
    }

    def datatablesWAC(def params){
        def rows = []
        def size = 0
        if (params.idReception) {
            def reception = Reception.get(params.idReception.toLong())
            def c = WAC.createCriteria()
            def no = 1;
            def allWac = c.list {
                eq("reception",reception)
            }
            allWac.each {
                rows << [
                        no : no,
                        itemName : it?.masterWACID?.m403NamaPerlengkapan,
                        countOk : it?.t410StaItem=="0" ? "V" : "",
                        countNotOk : it?.t410StaItem=="1" ? "V" : "",
                        statusNone : it?.t410StaItem=="2" ? "V" : "",
                        note : it?.t410Keterangan
                ]
                no++
            }
            if(reception?.wacItem){
                def wac = reception?.wacItem
                wac.each {
                    rows << [
                            no : no,
                            itemName : it?.itemName,
                            countOk : it?.statusItem=="0" ? "V" : "",
                            countNotOk : it?.statusItem=="1" ? "V" : "",
                            statusNone : it?.statusItem=="2" ? "V" : "",
                            note : it?.note
                    ]
                    no++;
                }
            }
        }

        [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
    }

    def historyServiceDatatablesList(def params){

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def c = Reception.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0");
            historyCustomerVehicle{
                customerVehicle{
                    eq("t103VinCode",params.idVincode,[ignoreCase: true])
                }
            }
            order("dateCreated","desc");
        }

        def rows = []
        results.each {
            def arrJob = []
            def arrPart = []
            def jobRCP = JobRCP.findAllByReceptionAndStaDel(it,"0");
            String jobs = "";
            String parts = "";
            int a = 0;
            jobRCP.each {
                a++;
                jobs += "* "+ it?.operation?.m053NamaOperation
                if(a!=jobRCP?.size()){
                    jobs+="<br/> "
                }
            }
            a=0;
            def partRCP = PartsRCP.findAllByReceptionAndStaDel(it,"0");
            partRCP.each {
                a++;
                parts += "* "+it?.goods?.m111Nama
                if(a!=partRCP?.size()){
                    parts +="<br/> "
                }
            }
            def suggest = JobSuggestion.findByCustomerVehicleAndStaDel(it?.historyCustomerVehicle?.customerVehicle,"0")
            rows << [

                    tanggal: it?.t401TglJamCetakWO?.format("dd MMMM yyyy"),

                    job: jobs,

                    parts: parts,

                    jobSuggest: ''
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesComplaintList(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Complaint.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            historyCustomerVehicle{
                customerVehicle{
                    eq("t103VinCode",params.idVincode,[ignoreCase: true])
                }
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it?.id,

                    tanggal: it?.t921TglComplain,

                    keluhan: it?.t921Keluhan
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def create(params) {

//        def reception = Reception.findAllByStaDelAndStaSaveAndCompanyDealerAndDateCreatedLessThanEquals("0","1",params.companyDealer,new Date().clearTime())
//        if(reception.size()>0){
//            reception.each {
//                def job = JobRCP.findAllByReception(it)
//                job*.delete()
//                def part = PartsRCP.findAllByReception(it)
//                part*.delete()
//                def custIn = CustomerIn.findAllByReception(it)
//                custIn.each {
//                    it.reception = null
//                    it.save()
//                }
//                it.delete()
//            }
//        }
    }

    def savePreDiagnose(def params){
        Reception receptionInstance = Reception.get(params.receptionId)

        Prediagnosis prediagnosis = new Prediagnosis()
        prediagnosis.reception = receptionInstance
        prediagnosis.t406KeluhanCust = params.t406KeluhanCust
        prediagnosis.t406StaGejalaHariIni = params.t406StaGejalaHariIni
        prediagnosis.t406StaGejalaMingguLalu = params.t406StaGejalaMingguLalu
        prediagnosis.t406StaGejalaLainnya = params.t406StaGejalaLainnya
        prediagnosis.t406GejalaLainnya = params.t406GejalaLainnya
        if(params.t406Km){
            prediagnosis.t406Km = params.t406Km as int
        }
        prediagnosis.t406TglDiagnosis = datatablesUtilService?.syncTime()
        prediagnosis.t406StaFrekSekali = params.t406StaFrekSekali
        prediagnosis.t406StaFrekKadang = params.t406StaFrekKadang
        prediagnosis.t406StaFrekSelalu = params.t406StaFrekSelalu
        prediagnosis.t406StaFrekLainnya = params.t406StaFrekLainnya
        prediagnosis.t406FrekLainnya = params.t406FrekLainnya
        prediagnosis.t406StaMILOn = params.t406StaMILOn
        prediagnosis.t406StaMILKedip = params.t406StaMILKedip
        prediagnosis.t406StaMILOff = params.t406StaMILOff
        prediagnosis.t406MILLainnya = params.t406MILLainnya
        prediagnosis.t406StaMILIdling = params.t406StaMILIdling
        prediagnosis.t406StaMILStarting = params.t406StaMILStarting
        prediagnosis.t406StaMILKonstan = params.t406StaMILKonstan
        prediagnosis.t406MILKecepatanLainnya = params.t406MILKecepatanLainnya
        prediagnosis.t406StaMesinPanas = params.t406StaMesinPanas
        prediagnosis.t406StaMesinDingin = params.t406StaMesinDingin
        prediagnosis.t406StaPanasMesinLainnya = params.t406StaPanasMesinLainnya
        prediagnosis.t406PanasMesinLainnya = params.t406PanasMesinLainnya
        prediagnosis.t406Suhu = params.t406PanasMesinLainnya
        prediagnosis.t406StaGigi1 = params.t406StaGigi1
        prediagnosis.t406StaGigi2 = params.t406StaGigi2
        prediagnosis.t406StaGigi3 = params.t406StaGigi3
        prediagnosis.t406StaGigi4 = params.t406StaGigi4
        prediagnosis.t406StaGigi5 = params.t406StaGigi5
        prediagnosis.t406StaGigiP = params.t406StaGigiP
        prediagnosis.t406StaGigiR = params.t406StaGigiR
        prediagnosis.t406StaGigiN = params.t406StaGigiN
        prediagnosis.t406StaGigiD = params.t406StaGigiD
        prediagnosis.t406StaGigiS = params.t406StaGigiS
        prediagnosis.t406StaGigiLainnya = params.t406StaGigiLainnya
        prediagnosis.t406GigiLainnya = params.t406GigiLainnya
        prediagnosis.t406Kecepatan = params.t406Kecepatan as int
        prediagnosis.t406RPM = params.t406RPM as int
        prediagnosis.t406Beban = params.t406Beban as int
        prediagnosis.t406JmlPenumpang = Integer.parseInt(params.t406JmlPenumpang)
        prediagnosis.t406StaDalamKota = params.t406StaDalamKota
        prediagnosis.t406StaJalanLurus = params.t406StaJalanLurus
        prediagnosis.t406StaAkselerasi = params.t406StaAkselerasi
        prediagnosis.t406StaLuarKota = params.t406StaLuarKota
        prediagnosis.t406StaDatar = params.t406StaDatar
        prediagnosis.t406StaRem = params.t406StaRem
        prediagnosis.t406StaTol = params.t406StaTol
        prediagnosis.t406StaTanjakan = params.t406StaTanjakan
        prediagnosis.t406StaBelok = params.t406StaBelok
        prediagnosis.t406StaTurunan = params.t406StaTurunan
        prediagnosis.t406StaKondisiLainnya = params.t406StaKondisiLainnya
        prediagnosis.t406KondisiLainnya = params.t406KondisiLainnya
        prediagnosis.t406StaMacet = params.t406StaMacet
        prediagnosis.t406StaLancar = params.t406StaLancar
        prediagnosis.t406StaLalinLainnya = params.t406StaLalinLainnya
        prediagnosis.t406LalinLainnya = params.t406LalinLainnya
        prediagnosis.t406StaCerah = params.t406StaCerah
        prediagnosis.t406StaBerawan = params.t406StaBerawan
        prediagnosis.t406StaHujan = params.t406StaHujan
        prediagnosis.t406StaPanas = params.t406StaPanas
        prediagnosis.t406StaLembab = params.t406StaLembab
        if(params.t406Temperatur){
            prediagnosis.t406Temperatur = params.t406Temperatur as int
        }
        prediagnosis.t406StaEG = params.t406StaEG
        prediagnosis.t406StaSuspensi = params.t406StaSuspensi
        prediagnosis.t406StaKlasifikasiRem = params.t406StaKlasifikasiRem
        prediagnosis.t406StaLainnya = params.t406StaLainnya
        prediagnosis.t406KlasifikasiLainnya = params.t406KlasifikasiLainnya
        if(params.t406BlowerSpeed){
            prediagnosis.t406BlowerSpeed = params.t406BlowerSpeed as int
        }
        if(params.t406TempSetting){
            prediagnosis.t406TempSetting = params.t406TempSetting as int
        }
        prediagnosis.t406StaReci1 = params.t406StaReci1
        prediagnosis.t406StaReci2 = params.t406StaReci2
        prediagnosis.t406StaReci3 = params.t406StaReci3
        prediagnosis.t406StaReci4 = params.t406StaReci4
        prediagnosis.t406StaReci5 = params.t406StaReci5
        prediagnosis.t406StaReci6 = params.t406StaReci6
        prediagnosis.t406StaPerluDTR = params.t406StaPerluDTR
        prediagnosis.t406StaTidaKPerluDTR = params.t406StaTidaKPerluDTR
        prediagnosis.t406DetailPekerjaan = params.t406DetailPekerjaan
        prediagnosis.t406KonfirmasiAkhir = params.t406KonfirmasiAkhir
        prediagnosis.t406StaOK = params.t406StaOK
        prediagnosis.t406StaNG = params.t406StaNG

        prediagnosis.dateCreated = datatablesUtilService?.syncTime()
        prediagnosis.lastUpdated = datatablesUtilService?.syncTime()
        prediagnosis.t406TglJamMulai = params.t406TglJamMulai
        prediagnosis.t406TglJamSelesai = params.t406TglJamSelesai
        prediagnosis.staDel = '0'
        prediagnosis.createdBy = 'admin'
        prediagnosis.updatedBy = 'admin'
        prediagnosis.lastUpdProcess = 'INSERT'

        prediagnosis.save(flush: true)
        prediagnosis.errors.each{ //println "err " + it
            }
        def result = [success: "1"]

        def countPemeriksaanAwal = new Integer(params.countPemeriksaanAwal)
        def indexRow = 1
        def diagnosisDetail


        while(indexRow < countPemeriksaanAwal){
            diagnosisDetail = new DiagnosisDetail()
            diagnosisDetail.prediagnosis = prediagnosis
            diagnosisDetail.reception = receptionInstance
            diagnosisDetail.t416staAwalUlang = 'A'
            diagnosisDetail.t416System = params.get("inputSystemPA" + indexRow)
            diagnosisDetail.t416DTC = params.get("inputDTCPA" + indexRow)
            diagnosisDetail.t416StatusPCH = params.get("inputStatusPA" + indexRow)
            diagnosisDetail.t416Desc = params.get("inputDescPA" + indexRow)
            diagnosisDetail.t416Freeze = params.get("inputFreezePA" + indexRow)

            diagnosisDetail.staDel = 0
            diagnosisDetail.dateCreated = datatablesUtilService?.syncTime()
            diagnosisDetail.lastUpdated = datatablesUtilService?.syncTime()
            diagnosisDetail.createdBy = "system"
            diagnosisDetail.lastUpdProcess = "INSERT"

            diagnosisDetail.save(flush:true)
            indexRow++
        }

        def countCekUlang = new Integer(params.countCekUlang)
        indexRow = 1

        while(indexRow < countCekUlang){
            diagnosisDetail = new DiagnosisDetail()
            diagnosisDetail.prediagnosis = prediagnosis
            diagnosisDetail.reception = receptionInstance
            diagnosisDetail.t416staAwalUlang = 'U'
            diagnosisDetail.t416System = params.get("inputSystemCU" + indexRow)
            diagnosisDetail.t416DTC = params.get("inputDTCCU" + indexRow)
            diagnosisDetail.t416StatusPCH = params.get("inputStatusCU" + indexRow)
            diagnosisDetail.t416Desc = params.get("inputDescCU" + indexRow)
            diagnosisDetail.t416Freeze = params.get("inputFreezeCU" + indexRow)

            diagnosisDetail.staDel = 0
            diagnosisDetail.dateCreated = datatablesUtilService?.syncTime()
            diagnosisDetail.lastUpdated = datatablesUtilService?.syncTime()
            diagnosisDetail.createdBy = "system"
            diagnosisDetail.lastUpdProcess = "INSERT"

            diagnosisDetail.save(flush:true)
            indexRow++
        }

        return prediagnosis;
    }

    def addPartDatatablesList(def params){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_goods") {
                ilike("m111ID", "%" + (params."sCriteria_goods" as String) + "%")
            }

            if (params."sCriteria_goods2") {
                ilike("m111Nama", "%" + (params."sCriteria_goods2" as String) + "%")
            }

        }

        def rows = []
        def nos = 0
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        results.each {
            nos = nos + 1
            def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it, "0",new Date(),[sort: "t151TMT",order: "desc"])
            def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                eq("staDel","0");
                eq("companyDealer",session?.userCompanyDealer);
            }
            def mappingRegion = MappingPartRegion.createCriteria().get {
                if(mappingCompRegion?.size()>0){
                    inList("region",mappingCompRegion?.region)
                }else{
                    eq("id",-10000.toLong())
                }
                maxResults(1);
            }
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
            if(mappingRegion){
                hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent );
            }

            rows << [

                    id: it.id,

                    goods: it.m111ID,

                    goods2: it.m111Nama,
                    satuan: it.satuan?.m118Satuan1,
                    totalHarga: conversi.toRupiah(hargaTanpaPPN)

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]


    }

    def savePart(params){
        def result = [:]
        def fail = { Map m ->
            if(result.receptionInstance && m.field)
                result.receptionInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["Reception", params.id] ]
            return result
        }

        Reception receptionInstance = Reception.get(params.receptionId as Long)

        result.receptionInstance =  receptionInstance

        Reception reception = receptionInstance

        def jsonArray = JSON.parse(params.part_ids)
        jsonArray.each {


            Goods goods = Goods.get(params."part_${it}" as Long)
            NamaProsesBP prosesBP = NamaProsesBP.get(params."prosesBP_${it}" as Long)
            PartJob pj = PartJob.findByGoodsAndNamaProsesBP(goods, prosesBP)
            if(pj){
                PartsApp pa = new PartsApp()
                pa.setGoods(goods)
                pa.setOperation(pj.operation)
                pa.reception = reception
                pa.namaProsesBP = prosesBP
                pa.t303Jumlah1 = params."qty_${it}" as Long
                pa.t303StaDel = "0"
                pa.dateCreated = datatablesUtilService?.syncTime()
                pa.lastUpdated = datatablesUtilService?.syncTime()
                pa.save(flush: true)
                pa.errors.each { //println it
                }

                JobApp ja = new JobApp()
                ja.reception = reception
                ja.operation = pj.operation
                ja.t302StaDel = "0"
                ja.dateCreated = datatablesUtilService?.syncTime()
                ja.lastUpdated = datatablesUtilService?.syncTime()
                ja.save(flush: true)
                ja.errors.each { //println it
                }

            }

        }


        if(result.receptionInstance.hasErrors() || !result.receptionInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        result."status" = "ok"
        // success
        return result
    }

    def saveSummary(params){
        def result = [success: "1"]

        def id = params.id
        def countRow = new Integer(params.countRow)
        def indexRow = 1
        def settlement

        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm")

        Reception receptionInstance = Reception.get(id)
        receptionInstance.t401TglJamPenyerahan =  sdf.parse(params.get("tanggalJamJanjiPenyerahan"))


        while(indexRow < countRow){
            settlement = new Settlement()
            settlement.reception = receptionInstance
            settlement.metodeBayar = com.kombos.maintable.MetodeBayar.get(params.get("metodePembayaran" + indexRow))
            settlement.t704JmlBayar = new Double(params.get("jumlahPembayaran" + indexRow))
            settlement.t704keterangan = params.get("keteranganPembayaran" + indexRow)
            settlement.staDel = 0
            settlement.dateCreated = datatablesUtilService?.syncTime()
            settlement.lastUpdated = datatablesUtilService?.syncTime()
            settlement.createdBy = "system"
            settlement.lastUpdProcess = "INSERT"

            settlement.save(flush:true)
            indexRow++
        }

        receptionInstance.save(flush:true)

        return result
    }

    def orderPart(def params, CompanyDealer companyDealer) {

        Reception reception = Reception.get(params.idReception.toLong())
        RPP rpp=null;
        def rppc = RPP.createCriteria()
        def resRPP = rppc.list {
            eq("companyDealer", companyDealer)
            order("m161TglBerlaku", "desc")
        }

        if (resRPP.size() > 0) {
            rpp = resRPP.get(0)
        }

        DpParts dpParts=null;
        def dpc = DpParts.createCriteria()
        def resDpParts = dpc.list {
            eq("companyDealer", companyDealer)
            order("m162TglBerlaku", "desc")
        }

        if (resDpParts.size() > 0) {
            dpParts = resDpParts.get(0)
        }

        def paList = JSON.parse(params.paIds)
        if (paList) {
            def req = Request.findByT162NoReffAndStaDel(reception?.t401NoWO,"0");
            if(!req){
                req = new Request()
            }
            req.t162TglRequest = datatablesUtilService?.syncTime()
            req.t162NoReff = reception.t401NoWO
            req.t162NamaPemohon = org.apache.shiro.SecurityUtils.subject.principal.toString()
            req.t162xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
            req.t162xNamaDivisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi?.m012NamaDivisi
            req.dateCreated = datatablesUtilService?.syncTime()
            req.lastUpdated = datatablesUtilService?.syncTime()
            req.save(flush: true)
            req.errors.each {
                //println it

            }
            paList.each{
                PartsRCP pa = PartsRCP.get(it.toString().toLong());
                RequestDetail reqDetail = RequestDetail.findByRequestAndGoods(req,pa.goods);
                if(!reqDetail){
                    reqDetail = new RequestDetail()
                }
                reqDetail.status = RequestStatus.BELUM_VALIDASI
                reqDetail.goods = pa.goods
                reqDetail.t162Qty1 = pa.t403Jumlah1
                reqDetail.t162Qty2 = pa.t403Jumlah2
                reqDetail.t162Qty1Available = pa?.goods?.partsStok ? pa?.goods?.partsStok?.last()?.t131Qty1 : 0
                reqDetail.t162Qty2Available = pa?.goods?.partsStok ? pa?.goods?.partsStok?.last()?.t131Qty2 : 0
                reqDetail.request = req
                reqDetail.dateCreated = datatablesUtilService?.syncTime()
                reqDetail.lastUpdated = datatablesUtilService?.syncTime()
                reqDetail.save(flush: true)
                reqDetail.errors.each {
                    //println it

                }
                def session = RequestContextHolder.currentRequestAttributes().getSession()
                def hargaTanpaPPN = 0
                if (pa.goods && pa.goods.satuan) {
                    def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(pa.goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                    def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                        eq("staDel","0");
                        eq("companyDealer",session?.userCompanyDealer);
                    }
                    def mappingRegion = MappingPartRegion.createCriteria().get {
                        if(mappingCompRegion?.size()>0){
                            inList("region",mappingCompRegion?.region)
                        }else{
                            eq("id",-10000.toLong())
                        }
                        maxResults(1);
                    }
                    hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                    if(mappingRegion){
                        hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent );
                    }
                }
                ValidasiOrder vo = reqDetail.validasiOrder
                if (!vo) {
                    vo = validasiOrderService.createValidasiOrder(reqDetail)
                    def dp = 0
                    if (dpParts) {
                        vo.t163DP = (dpParts?.m162PersenDP / 100) * hargaTanpaPPN

                    }
                    List ptos = PartTipeOrder.findAllById(params."${'tipeOrder_' + it}" as Long)
                    if (ptos.size() > 0) {
                        PartTipeOrder pto = ptos.get(0)
                        vo.partTipeOrder = pto
                        Date date = datatablesUtilService?.syncTime()
//                        date.setHours(date.getHours() + pto.m112JamLamaDatang1)
                        vo.t163ETA = datatablesUtilService?.syncTime()

                    }
                    vo?.t163HargaSatuan = hargaTanpaPPN
                    vo?.t163MasaPengajuanRPP = rpp?.m161MasaPengajuanRPP
                    vo?.save(flush: true)
                    vo?.errors.each{
                        //println it

                    }
                }
            }
            if (req.hasErrors() || !req.save(flush: true)) {
                //println("Error creating request")
            }
        }
        def res = [:]
        res.status = "ok"
        res
    }

    def requestPartDatatablesList(def params, CompanyDealer companyDealer) {
        def kodeKota = params.kodeKota
        def nomorTengah = params.nomorTengah
        def nomorBelakang = params.nomorBelakang

        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def reception = Reception.get(params.receptionId.toLong())
        RPP rpp;
        def rppc = RPP.createCriteria()
        def resRPP = rppc.list {
            eq("companyDealer", companyDealer)
            order("m161TglBerlaku", "desc")
        }

        if (resRPP.size() > 0) {
            rpp = resRPP.get(0)
        }

        DpParts dpParts;
        def dpc = DpParts.createCriteria()
        def resDpParts = dpc.list {
            eq("companyDealer", companyDealer)
            order("m162TglBerlaku", "desc")
        }

        if (resDpParts.size() > 0) {
            dpParts = resDpParts.get(0)
        }


        def c = PartsRCP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0");
            eq("reception",reception);
            goods {
                order("m111ID", "asc")
            }
        }

        def rows = []
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        for(cari in results){
            def goods = cari.goods
            if (goods) {
                def hargaTanpaPPN = 0
                if (goods && goods.satuan) {
                    def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                    def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                        eq("staDel","0");
                        eq("companyDealer",session?.userCompanyDealer);
                    }
                    def mappingRegion = MappingPartRegion.createCriteria().get {
                        if(mappingCompRegion?.size()>0){
                            inList("region",mappingCompRegion?.region)
                        }else{
                            eq("id",-10000.toLong())
                        }
                        maxResults(1);
                    }
                    hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                    if(mappingRegion){
                        hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent );
                    }
                }
                def qty = cari.t403Jumlah1
                def satuan = goods?.satuan?.m118Satuan1
//                def availabilityQty = goods?.partsStok?.t131Qty1Free ?: "0" as int
                def availabilityQty = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',companyDealer) ? PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',companyDealer)?.t131Qty1:"0"
                def orderQty = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',companyDealer) ? PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',companyDealer)?.t131Qty1Reserved:"0"
                def dp = 0
                if (dpParts) {
//                    dp = (dpParts.m162PersenDP / 100) * hargaTanpaPPN
                }
                rows << [
                        id                : cari.id,
                        status            : "",//reqDetail.status?.toString(),
                        validated         : "0",
                        kodePart          : goods?.m111ID ?: "",
                        namaPart          : goods?.m111Nama ?: "",
                        requestQty        : qty,
                        requestSatuan     : satuan,
                        availabilityQty   : availabilityQty,
                        availabilitySatuan: satuan,
                        orderQty          : orderQty,
                        orderSatuan       : satuan,
                        eta               : "",
                        tipeOrder         : "",
                        rpp               : rpp?.m161MaxDapatRPP,
                        dp                : conversi.toRupiah(dp),
                        harga             : conversi.toRupiah(hargaTanpaPPN),
                        total             : (hargaTanpaPPN) ? conversi.toRupiah(hargaTanpaPPN * qty) : "0" as int
                ]
            }
        }


        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def cancelEditBookingFeeDatatablesList(def params, CompanyDealer companyDealer) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        Reception reception = Reception.get(params.receptionId as Long)

        def partsapps = [:]
        def reqs = [:]
        DpParts dpParts = null;
        def dpc = DpParts.createCriteria()
        def resDpParts = dpc.list {
            eq("companyDealer", companyDealer)
            order("m162TglBerlaku", "desc")
        }

        if (resDpParts.size() > 0) {
            dpParts = resDpParts.get(0)
        }


        def c = RequestDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            request {
                eq("t162NoReff", reception.t401NoWO)
            }
            goods {
                order("m111ID", "asc")
            }
        }

        results.each { RequestDetail rd ->
            def goods = rd.goods
            if (goods) {
                reqs."${goods.id}" = rd
            }
        }

        c = PartsRCP.createCriteria()
        results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("reception", reception)
            goods {
                order("m111ID", "asc")
            }
        }

        def rows = []
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        results.each { PartsRCP pa ->
            def goods = pa.goods
            if (goods) {
//                partsapps."${goods.id}" = pa
                RequestDetail rd = reqs."${goods.id}"
                def hargaTanpaPPN = 0
                if (goods && goods.satuan) {
                    def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                    def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                        eq("staDel","0");
                        eq("companyDealer",session?.userCompanyDealer);
                    }
                    def mappingRegion = MappingPartRegion.createCriteria().get {
                        if(mappingCompRegion?.size()>0){
                            inList("region",mappingCompRegion?.region)
                        }else{
                            eq("id",-10000.toLong())
                        }
                        maxResults(1);
                    }
                    hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                    if(mappingRegion){
                        hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent );
                    }
                    //it.goods?.goodsHargaJual?.t151HargaTanpaPPN
                }
                def requestQty = pa?.t403Jumlah1 ?: 0
                def requestQty2 = pa.t403Jumlah1 ?: requestQty
                def qty = pa.t403Jumlah1
                def satuan = goods?.satuan?.m118Satuan1
                def availabilityQty = goods?.partsStok?.t131Qty1Free ?: "0" as int
                def orderQty = goods?.partsStok?.t131Qty1Reserved ?: "0" as int
                def dp = 0
                if (dpParts) {
                    dp = (dpParts.m162PersenDP / 100) * hargaTanpaPPN
                }

                def dp2 = pa.t403DPRp ?: 0
                def total2 = hargaTanpaPPN * requestQty2

                rows << [
                        id            : pa.id,
                        kodePart      : goods?.m111ID ?: "",
                        namaPart      : goods?.m111Nama ?: "",
                        requestQty    : requestQty,
                        requestSatuan : satuan,
                        dp            : conversi.toRupiah(dp),
                        harga         : conversi.toRupiah(hargaTanpaPPN),
                        total         : conversi.toRupiah(hargaTanpaPPN * requestQty),
                        requestQty2   : requestQty2,
                        requestSatuan2: satuan,
                        dp2           : conversi.toRupiah(dp2),
                        harga2        : conversi.toRupiah(hargaTanpaPPN),
                        total2        : conversi.toRupiah(total2),
                        keterangan    : pa.t403xKet ?: "",
                        req           : (dpParts == null) ? "0" : 1

                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def getNewReception(def params){
        def hasil = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def kodeKotaNoPol = KodeKotaNoPol.get(params.kodeKotaNoPol.toLong())
        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0");
            eq("fullNoPol",kodeKotaNoPol?.m116ID+" "+params.noPolTengah+" "+params.noPolBelakang,[ignoreCase : true]);
        }

        if(histCV){
            if(histCV.last().customerVehicle.getCurrentCondition()==histCV.last()){

                def custTemp = new HistoryCustomer(),driverTemp = new HistoryCustomer()
                def cariPakai = false,cariDriver=false;
                def custVehicle = histCV.last().customerVehicle
                def fa = VehicleFA.findByCustomerVehicleAndHistoryCustomerVehicle(custVehicle,custVehicle?.getCurrentCondition())
                def mappingCV = MappingCustVehicle.findAllByCustomerVehicle(custVehicle,[sort : 'dateCreated',order : 'desc'])
                for(cari in mappingCV){
                    if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik dan Penanggung Jawab') && !cariPakai){
                        custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                        cariPakai=true;
                    }
                    if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Driver') && !cariDriver){
                        driverTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                        cariDriver=true;
                    }
                    if(cariPakai && cariDriver){
                        break
                    }else{
                        if(!cariPakai){
                            custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                        }
                    }
                }
                def tempRecept = new Reception()
                tempRecept.t401NoWO = generateCodeService.codeGenerateSequence("T401_NoWO", params.companyDealer)
                tempRecept.staSave = "1"
                tempRecept.t401StaReceptionEstimasiSalesQuotation = "estimasi"
                tempRecept.companyDealer = params.companyDealer
                tempRecept.historyCustomerVehicle = custVehicle.getCurrentCondition()
                tempRecept.historyCustomer = custTemp
                tempRecept.staDel = "0"
                tempRecept.isProgress = "1"
                tempRecept.t401StaAmbilWO = "0"
                tempRecept.dateCreated = datatablesUtilService?.syncTime()
                tempRecept.lastUpdated = datatablesUtilService?.syncTime()
                tempRecept.save(flush: true)
                tempRecept.errors.each { //println it
                }
                def spk = new SPK()
                if(custTemp?.company){
                    def getSPK = SPK.findAllByCompanyAndStaDelAndT191TglAwalLessThanEqualsAndT191TglAkhirGreaterThanEquals(custTemp?.company,"0",new Date(),new Date())
                    if(getSPK.size()>0){
                        tempRecept?.sPK = getSPK.last()
                        spk = getSPK.last()
                    }
                }
                def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(custVehicle,"0");
                def staJDPower = CustomerSurveyDetail.createCriteria().list {
                    eq("staDel","0")
                    eq("customerVehicle",custVehicle)
                    customerSurvey{
                        le("t104TglAwal",new Date())
                        ge("t104TglAkhir",new Date())
                    }
                }
                def spkAsuransi = SPKAsuransi.findAllByCustomerVehicleAndT193TglAwalLessThanEqualsAndT193TglAkhirGreaterThanEqualsAndStaDel(custVehicle,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00") ,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00"),"0");
                if(spkAsuransi.size()>0){
                    if(!tempRecept?.sPkAsuransi){
                        tempRecept?.sPkAsuransi = spkAsuransi.last()
                    }
                    tempRecept?.t401StaButuhSPKSebelumProd = "1"
                    tempRecept?.lastUpdated = datatablesUtilService?.syncTime()
                    tempRecept?.save(flush: true);
                }
                def skrg = (datatablesUtilService?.syncTime()-60).clearTime()
                def piutang = com.kombos.finance.Collection.createCriteria().get {
                    eq("staDel","0")
                    eq("paymentStatus","BELUM",[ignoreCase: true])
                    le("dueDate",skrg)
                    invoiceT071{
                        reception{
                            historyCustomerVehicle{
                                eq("fullNoPol",kodeKotaNoPol?.m116ID+" "+params.noPolTengah+" "+params.noPolBelakang,[ignoreCase : true]);
                            }
                        }
                    }
                    maxResults(1)
                }

                def approval = ApprovalT770.findByKegiatanApprovalAndT770FKAndStaDelAndDateCreatedBetween(KegiatanApproval.findByM770KegiatanApprovalAndStaDel(KegiatanApproval.PELANGGAN_MENUNGGAK,"0"),histCV?.last()?.id,"0",dateAwal,dateAkhir)
                def staApp = ""
                if(approval){
                    staApp = approval?.t770Status
                }
                hasil << [
                        fa : fa?.fa?.m185NamaFA,
                        tpss : "-",
                        fu : "-",
                        pks : "-",
                        fullnopol : tempRecept?.historyCustomerVehicle?.fullNoPol,
                        pemakai : custTemp?.t182NamaBelakang ? custTemp?.fullNama : custTemp?.t182NamaDepan,
                        driver : driverTemp?.t182NamaBelakang ? driverTemp?.fullNama : driverTemp?.t182NamaDepan,
                        mobil : custVehicle?.getCurrentCondition()?.fullModelCode?.baseModel?.m102NamaBaseModel,
                        telp : custTemp?.t182NoHp,
                        alamat : custTemp?.t182Alamat,
                        tanggalMulai : "-",
                        tanggalDelivery : "-",
                        idCV : custVehicle?.t103VinCode,
                        jobSuggest: jobSuggest?.t503KetJobSuggest,
                        idReception : tempRecept?.id ? tempRecept?.id : "",
                        noWO : tempRecept?.t401NoWO ? tempRecept?.t401NoWO : "",
                        tgglReception : tempRecept?.t401TglJamCetakWO ? tempRecept?.t401TglJamCetakWO?.format("dd MMMM yyyy") : new Date().format("dd MMMM yyyy"),
                        hasil: "ada",
                        spk : spk?.t191JmlSPK ? spk?.t191JmlSPK : "-1",
                        staJDPower : staJDPower?.size()>0 ? "0" : "1",
                        staAsuransi : spkAsuransi.size()>0 ? "Y-"+tempRecept?.t401StaButuhSPKSebelumProd : "N",
                        staPiutang : piutang ? "PY" : "PN",
                        staApp : staApp
                ]
            }else{
                hasil << [hasil : "replaced"];
            }
        }else{
            hasil << [hasil : "nothing"]
        }

        return hasil
    }

    def getReception(def params){
        def hasil = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def kodeKotaNoPol = KodeKotaNoPol.get(params.kodeKotaNoPol.toLong())
        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0");
            eq("fullNoPol",kodeKotaNoPol?.m116ID+" "+params.noPolTengah+" "+params.noPolBelakang,[ignoreCase : true]);
        }

        if(histCV){
            if(histCV.last().customerVehicle.getCurrentCondition()==histCV.last()){
                def custTemp = new HistoryCustomer(),driverTemp = new HistoryCustomer()
                def cariPakai = false,cariDriver=false;
                def custVehicle = histCV.last().customerVehicle
                def fa = VehicleFA.findByCustomerVehicleAndHistoryCustomerVehicle(custVehicle,custVehicle?.getCurrentCondition())
                def mappingCV = MappingCustVehicle.findAllByCustomerVehicle(custVehicle,[sort : 'dateCreated',order : 'desc'])
                def findRecep = Reception.findAllByHistoryCustomerVehicleAndDateCreatedBetweenAndStaDelAndStaSaveAndT401StaReceptionEstimasiSalesQuotation(histCV?.last(),dateAwal,dateAkhir,"0","2","estimasi")
                if(findRecep.size()>0){
                    for(cari in mappingCV){
                        if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik dan Penanggung Jawab') && !cariPakai){
                            custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                            cariPakai=true;
                        }
                        if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Driver') && !cariDriver){
                            driverTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                            cariDriver=true;
                        }
                        if(cariPakai && cariDriver){
                            break
                        }else{
                            if(!cariPakai){
                                custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                            }
                        }
                    }
                    def reception = findRecep?.last()
                    def appointment = Appointment.findByReceptionAndStaDel(reception,"0")
                    def spk = new SPK()
                    if(custTemp?.company){
                        def getspk = SPK.findAllByCompanyAndStaDelAndT191TglAwalLessThanEqualsAndT191TglAkhirGreaterThanEquals(custTemp?.company,"0",new Date(),new Date())
                        if(getspk.size()>0 && !reception?.sPK){
                            reception?.sPK = getspk.last()
                            spk = getspk.last()
                        }
                    }
                    def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(custVehicle,"0");
                    def staJDPower = CustomerSurveyDetail.createCriteria().list {
                        eq("staDel","0")
                        eq("customerVehicle",custVehicle)
                        customerSurvey{
                            le("t104TglAwal",new Date())
                            ge("t104TglAkhir",new Date())
                        }
                    }
                    def spkAsuransi = SPKAsuransi.findAllByCustomerVehicleAndT193TglAwalLessThanEqualsAndT193TglAkhirGreaterThanEqualsAndStaDel(custVehicle,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00") ,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00"),"0");
                    if(spkAsuransi.size()>0){

                        if(!reception?.sPkAsuransi){
                            reception?.sPkAsuransi = spkAsuransi.last()
                        }

                        if(!reception?.t401StaButuhSPKSebelumProd){
                            reception?.t401StaButuhSPKSebelumProd = "1"
                            reception?.lastUpdated = datatablesUtilService?.syncTime()
                            reception?.save(flush: true);
                        }
                    }
                    hasil << [
                            fa : fa?.fa?.m185NamaFA,
                            tpss : "-",
                            fu : "-",
                            pks : "-",
                            fullnopol : reception?.historyCustomerVehicle?.fullNoPol,
                            pemakai : custTemp?.t182NamaBelakang ? custTemp?.fullNama : custTemp?.t182NamaDepan,
                            driver : driverTemp?.t182NamaBelakang ? driverTemp?.fullNama : driverTemp?.t182NamaDepan,
                            mobil : custVehicle?.getCurrentCondition()?.fullModelCode?.baseModel?.m102NamaBaseModel,
                            staBooking : appointment ? "Booking" : "Walk In",
                            tanggalBayar : reception?.t401TglJamJanjiBayarDP ? reception?.t401TglJamJanjiBayarDP?.format("dd MMMM yyyy / HH:mm") : "-",
                            tanggalDatang : reception?.t401TglJamRencana ? reception?.t401TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                            tanggalMulai : reception?.t401TglJamRencana ? reception?.t401TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                            tanggalDelivery : reception?.t401TglJamJanjiPenyerahan ? reception?.t401TglJamJanjiPenyerahan?.format("dd MMMM yyyy / HH:mm") : "-",
                            telp : custTemp?.t182NoHp,
                            alamat : custTemp?.t182Alamat,
                            idCV : custVehicle?.t103VinCode,
                            jobSuggest: jobSuggest?.t503KetJobSuggest,
                            idReception : reception?.id ? reception?.id : "",
                            noWO : reception?.t401NoWO ? reception?.t401NoWO : "",
                            tgglReception : reception?.t401TglJamCetakWO ? reception?.t401TglJamCetakWO?.format("dd MMMM yyyy") : datatablesUtilService?.syncTime().format("dd MMMM yyyy"),
                            hasil: "ada",
                            spk : spk?.t191JmlSPK ? spk?.t191JmlSPK : "-1",
                            staJDPower : staJDPower?.size()>0 ? "0" : "1",
                            staAsuransi : spkAsuransi.size()>0 ? "Y" : "N"
                    ]
                }else{
                    hasil << [hasil : "noreception"];
                }
            }
            else{
                hasil << [hasil : "replaced"];
            }
        }
        else{
            hasil << [hasil : "nothing"]
        }
        return hasil
    }

    def jobKeluhan(def params){
        def result = [:]
        def user = User.findByUsernameAndStaDel(org.apache.shiro.SecurityUtils.subject.principal.toString(),"0");
        result.petugas = user.fullname
        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0")
            kodeKotaNoPol{
                eq("id",params.kode.toLong())
            }
            eq("t183NoPolTengah",params.tengah.trim(),[ignoreCase: true])
            eq("t183NoPolBelakang",params.belakang.trim(),[ignoreCase: true])
        }
        def idCV = histCV?.size()>0 ? histCV?.last()?.customerVehicle?.id : -1
        def c = Reception.createCriteria()
        def results = c.list() {
            eq("staDel","0")
            historyCustomerVehicle{
                customerVehicle{
                    eq("id",idCV.toLong());
                }
            }
        }
        def km = results?.size()>1 ? results?.last()?.t401KmSaatIni : 0
        result.minKM = km
        return result
    }

    def estimasiDatatablesList(def params, CompanyDealer companyDealer){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def x = 0

        def rec = Reception.findByT401NoWO(params.nowo)
        def jRcp = JobRCP.findAllByReception(rec)
        def mwi = MappingWorkItems.createCriteria().list {
            operation{
                eq("id", jRcp.operation)
            }
            eq("companyDealer", companyDealer)
        }

        def c = WorkItems.createCriteria()
//        def results = c.list() {
//            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
//            projections {
//                groupProperty("operation", "operation")
//            }
//        }
        def results = c.list {
            eq("id", mwi?.workItemsId)
            eq("companyDealer", mwi?.companyDealer)
        }

        def rows = []
        def hargaParts = 0
        def hargaJasa = 0
        def hargaTotal = 0
        results.each {
            def hrgaJ = JobRCP.findByOperationAndReceptionAndStaDel(jRcp?.operation?.last(), rec, "0")
            def hrgaP = PartsRCP.findByOperationAndReceptionAndStaDel(jRcp?.operation?.last(), rec, "0")

            hargaParts = hrgaP?.t403HargaRp
            hargaJasa = hrgaJ?.t402HargaRp
            hargaTotal = hargaParts + hargaJasa

            rows << [
//                        idJob                  : it.operation.id,
                    workItem            : it?.m039WorkItems,
                    area : '',
                    repairDifficulty:'',
                    plasticBumper:'',
                    hargaParts: conversi.toRupiah(hargaParts),
                    hargaJasa:conversi.toRupiah(hargaJasa),
                    hargaTotal: conversi.toRupiah(hargaTotal),

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }
}