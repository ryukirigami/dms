package com.kombos.reception

import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.example.Contoh
import com.kombos.maintable.MappingCustVehicle

class TowingMalamService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = TowingMalam.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer);
            if (params."sCriteria_t421ID") {
                ilike("t421ID", "%" + (params."sCriteria_t421ID" as String) + "%")
            }

            if (params."sCriteria_t421TglJamDatang") {
                ge("t421TglJamDatang", params."sCriteria_t421TglJamDatang")
                lt("t421TglJamDatang", params."sCriteria_t421TglJamDatang" + 1)
            }

            if (params."sCriteria_customerVehicle") {
                eq("customerVehicle", params."sCriteria_customerVehicle")
            }

            if (params."sCriteria_t421Mobil") {
                ilike("t421Mobil", "%" + (params."sCriteria_t421Mobil" as String) + "%")
            }

            if (params."sCriteria_t421Tahun") {
                ilike("t421Tahun", "%" + (params."sCriteria_t421Tahun" as String) + "%")
            }

            if (params."sCriteria_t421Warna") {
                ilike("t421Warna", "%" + (params."sCriteria_t421Warna" as String) + "%")
            }

            if (params."sCriteria_t421NamaPemilik") {
                ilike("t421NamaPemilik", "%" + (params."sCriteria_t421NamaPemilik" as String) + "%")
            }

            if (params."sCriteria_t421NoTelp") {
                ilike("t421NoTelp", "%" + (params."sCriteria_t421NoTelp" as String) + "%")
            }

            if (params."sCriteria_t421NamaPenerima") {
                ilike("t421NamaPenerima", "%" + (params."sCriteria_t421NamaPenerima" as String) + "%")
            }

            if (params."sCriteria_t421StaMetodeFU") {
                ilike("t421StaMetodeFU", "%" + (params."sCriteria_t421StaMetodeFU" as String) + "%")
            }

            if (params."sCriteria_t421StaFU") {
                ilike("t421StaFU", "%" + (params."sCriteria_t421StaFU" as String) + "%")
            }

            if (params."sCriteria_t421TglJamFU") {
                ge("t421TglJamFU", params."sCriteria_t421TglJamFU")
                lt("t421TglJamFU", params."sCriteria_t421TglJamFU" + 1)
            }

            if (params."sCriteria_t421NamaFU") {
                ilike("t421NamaFU", "%" + (params."sCriteria_t421NamaFU" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t421ID: it.t421ID,

                    t421TglJamDatang: it.t421TglJamDatang ? it.t421TglJamDatang.format("dd/MM/yyyy HH:mm") : "",

                    customerVehicle: (
                            it.customerVehicle.currentCondition==null?it.customerVehicle.t103VinCode:
                            (
                                it.customerVehicle.currentCondition.kodeKotaNoPol.m116ID
                                +" "+it.customerVehicle.currentCondition.t183NoPolTengah
                                +" "+it.customerVehicle.currentCondition.t183NoPolBelakang
                            )
                    ),

                    t421Mobil: it.t421Mobil,

                    t421Tahun: it.t421Tahun,

                    t421Warna: it.t421Warna,

                    t421NamaPemilik: it.t421NamaPemilik,

                    t421NoTelp: it.t421NoTelp,

                    t421NamaPenerima: it.t421NamaPenerima,

                    t421StaMetodeFU: it.t421StaMetodeFU==null?"":(it.t421StaMetodeFU=='T'?"Telp":"E-mail"),

                    t421StaFU: it.t421StaFU==null?"":(it.t421StaFU=='1'?"Berhasil":"Tidak Berhasil"),

                    t421TglJamFU: it.t421TglJamFU ? it.t421TglJamFU.format(dateFormat) : "",

                    t421NamaFU: it.t421NamaFU,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(id) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Contoh", id as long] ]
            return result
        }

        result.contohInstance = Contoh.get(id)

        if(!result.contohInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Contoh", params.id] ]
            return result
        }

        result.contohInstance = Contoh.get(params.id)

        if(!result.contohInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Contoh", params.id] ]
            return result
        }

        result.contohInstance = Contoh.get(params.id)

        if(!result.contohInstance)
            return fail(code:"default.not.found.message")

        try {
            result.contohInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Contoh", params.id] ]
            return result
        }

        result.contohInstance = new Contoh()
        result.contohInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.contohInstance && m.field)
                result.contohInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["Contoh", params.id] ]
            return result
        }

        result.contohInstance = new Contoh(params)

        if(result.contohInstance.hasErrors() || !result.contohInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        // success
        return result
    }

    def updateField(def params){
        def contoh =  Contoh.findById(params.id)
        if (contoh) {
            contoh."${params.name}" = params.value
            contoh.save()
            if (contoh.hasErrors()) {
                throw new Exception("${contoh.errors}")
            }
        }else{
            throw new Exception("Contoh not found")
        }
    }

    def requestTo(def username){
        def ret = []
        def user = User.findByUsername(username)

        def companyDealer = user.companyDealer

        def kepalaBengkel = Role.findByName("KEPALA_BENGKEL")

        kepalaBengkel.users.each {
            ret << it.username
            //println("Add: " + it.username)
        }

        ret
    }

    def assignTo(def username){
        def ret = []
        def user = User.findByUsername(username)

        def companyDealer = user.companyDealer

        def teknisi = Role.findByName("TEKNISI")

        teknisi.users.each {
            ret << it.username
            //println("Add: " + it.username)
        }

        ret
    }

    def findNoPol(params){
        HistoryCustomer historyCustomer
        def noTelp = ""
        def namaDepan = ""
        def historyCVInstance = HistoryCustomerVehicle?.createCriteria()?.get {
            eq("staDel","0");
            kodeKotaNoPol{
                eq("id",Long.parseLong(params.kode))
            }
            ilike("t183NoPolTengah","%"+params?.tengah+"%")
            ilike("t183NoPolBelakang","%"+params?.belakang+"%")
            maxResults(1);
        }
        if(historyCVInstance){
			def mappingCustVehicles = MappingCustVehicle.findAllByCustomerVehicle(historyCVInstance?.customerVehicle,[sort : 'dateCreated',order : 'desc'])
			if(mappingCustVehicles?.size()>0){
                for(cari in mappingCustVehicles){
                    if(cari?.customer?.histories?.first()?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains("PEMILIK")){
                        def historyCustomers = HistoryCustomer.findByCustomerAndStaDel(cari?.customer,"0")
                        if(historyCustomers){
                            historyCustomer = historyCustomers
                            noTelp = historyCustomer?.t182NoHp
                            namaDepan = historyCustomer?.fullNama
                            break
                        }
                    }
                }
			}
        }

        def rows = []

        rows = [

                id : historyCVInstance?.id,

                t183NamaSTNK : historyCVInstance?.t183NamaSTNK,

                t183AlamatSTNK : historyCVInstance?.t183AlamatSTNK,

                customerVehicle : historyCVInstance?.customerVehicle?.t103VinCode,

                fullModelCode : historyCVInstance?.fullModelCode?.t110FullModelCode,

                fullModelCodeBaseModel : historyCVInstance?.fullModelCode?.baseModel?.m102NamaBaseModel,

                fullModelCodeGear : historyCVInstance?.fullModelCode?.gear?.m106NamaGear,

                warna : historyCVInstance?.warna?.m092NamaWarna,

                t183ThnBlnRakit : historyCVInstance?.t183ThnBlnRakit,

                t183TglDEC : historyCVInstance?.t183TglDEC,

                noHp : noTelp,

                namaDepan : namaDepan
        ]

        return rows
    }
}