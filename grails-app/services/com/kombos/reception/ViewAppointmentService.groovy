package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.maintable.CustBaruWeb
import com.kombos.maintable.CustLamaWeb
import com.kombos.maintable.PartsApp
import com.kombos.parts.*
import com.kombos.woinformation.PartsRCP
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class ViewAppointmentService {
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def appSettingParamTimeFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.TIME_FORMAT)
    def conversi = new Konversi()


	def searchCustBaruWebCriteria = { params ->
		if(params."sCriteria_namaCustomer"){
				ilike("fullNama","%" + (params."sCriteria_namaCustomer" as String) + "%")
		}		
		if(params."sCriteria_alamatCustomer"){
				ilike("t187AlamatRumah","%" + (params."sCriteria_alamatCustomer" as String) + "%")
		}
		if(params."sCriteria_noHP"){
				ilike("t187TelpHP","%" + (params."sCriteria_noHP" as String) + "%")
		}
		if(params."sCriteria_noTelpRumah"){
				ilike("t187TelpRumah","%" + (params."sCriteria_noTelpRumah" as String) + "%")
		}
		if(params."sCriteria_jenisKelamin"){
				ilike("t187JKel","%" + (params."sCriteria_jenisKelamin" as String) + "%")
		}
		if(params."sCriteria_from_tglLahir"){
			ge("t187TanggalLahir",params."sCriteria_from_tglLahir")
		}
		if(params."sCriteria_to_tglLahir"){
			lt("t187TanggalLahir",params."sCriteria_to_tglLahir" + 1)
		}
	}

	def searchCustLamaWebCriteria = { params ->
		if(params."sCriteria_noPolisi"){
			customerVehicle{
				histories{
					ilike("fullNoPol","%" + (params."sCriteria_noPolisi" as String) + "%")
				}
			}
		}
		if(params."sCriteria_namaCustomerCustLama"){
			historyCustomer{
				ilike("fullNama","%" + (params."sCriteria_namaCustomerCustLama" as String) + "%")
			}
		}
		if(params."sCriteria_alamatCustomer"){
			historyCustomer{
				ilike("t182Alamat","%" + (params."sCriteria_alamatCustomer" as String) + "%")
			}
		}
		if(params."sCriteria_noHP"){
			historyCustomer{
				ilike("t182NoHp","%" + (params."sCriteria_noHP" as String) + "%")
			}
		}
		if(params."sCriteria_pekerjaan"){
			operation{
				ilike("m053NamaOperation","%" + (params."sCriteria_pekerjaan" as String) + "%")
			}
		}
		if(params."sCriteria_from_tglDaftar"){
			ge("t188TglDaftar",params."sCriteria_from_tglDaftar")
		}
		if(params."sCriteria_to_tglDaftar"){
			lt("t188TglDaftar",params."sCriteria_to_tglDaftar" + 1)
		}
		if(params."sCriteria_from_tglService"){
			ge("t188TanggalJamService",params."sCriteria_from_tglService")
		}
		if(params."sCriteria_to_tglService"){
			lt("t188TanggalJamService",params."sCriteria_to_tglService" + 1)
		}
	}
	
	def searchAppointmentCriteria = { params ->
		if(params."sCriteria_from_tglAppointment"){
				ge("t301TglJamApp",params."sCriteria_from_tglAppointment")
		}
		if(params."sCriteria_to_tglAppointment"){
				lt("t301TglJamApp",params."sCriteria_to_tglAppointment" + 1)
		}
		if(params."sCriteria_noAppointment"){
			reception{
				ilike("t401NoWO","%" + (params."sCriteria_noAppointment" as String) + "%")
			}
		}
		if(params."sCriteria_noPolisi"){
			historyCustomerVehicle{
				ilike("fullNoPol","%" + (params."sCriteria_noPolisi" as String) + "%")
			}
		}
		if(params."sCriteria_namaBaseModel"){
			historyCustomerVehicle{
				fullModelCode{
					baseModel{
						ilike("m102NamaBaseModel","%" + (params."sCriteria_namaBaseModel" as String) + "%")
					}
				}
			}
		}
		if(params."sCriteria_namaCustomer"){
			historyCustomer{
				ilike("fullNama","%" + (params."sCriteria_namaCustomer" as String) + "%")
			}
		}
		if(params."sCriteria_telpCustomer"){
			historyCustomer{
				ilike("t182NoHp","%" + (params."sCriteria_telpCustomer" as String) + "%")
			}
		}
		if(params."sCriteria_noHP"){
			historyCustomer{
				ilike("t182NoHp","%" + (params."sCriteria_noHP" as String) + "%")
			}
		}
		if(params."sCriteria_noTelpRumah"){
			historyCustomer{
				ilike("t182NoTelpRumah","%" + (params."sCriteria_noTelpRumah" as String) + "%")
			}
		}
		if(params."sCriteria_alamatCustomer"){
			historyCustomer{
				ilike("t182Alamat","%" + (params."sCriteria_alamatCustomer" as String) + "%")
			}
		}
		if(params."sCriteria_janjiBayar"){
			historyCustomer{
				ilike("t182Alamat","%" + (params."sCriteria_janjiBayar" as String) + "%")
			}
		}
		if(params."sCriteria_jenisKelamin"){
			historyCustomer{
				eq("t182JenisKelamin",(params."sCriteria_jenisKelamin" as String))
			}
		}
		if(params."sCriteria_from_tglLahir"){
			historyCustomer{
				ge("t182TglLahir",params."sCriteria_from_tglLahir")
			}
		}
		if(params."sCriteria_to_janjiBayar"){
			historyCustomer{
				lt("t182TglLahir",params."sCriteria_to_tglLahir" + 1)
			}
		}
		if(params."sCriteria_from_janjiBayar"){
				ge("t301TglJamJanjiBayarDP",params."sCriteria_from_janjiBayar")
		}
		if(params."sCriteria_to_janjiBayar"){
				lt("t301TglJamJanjiBayarDP",params."sCriteria_to_janjiBayar" + 1)
		}
		if(params."sCriteria_jmlBookingFee"){
			reception{
				eq("totalRencanaDP",params."sCriteria_jmlBookingFee" as Double)
			}
		}
		if(params."sCriteria_bookingFee"){
			reception{
				eq("totalRencanaDP",params."sCriteria_jmlBookingFee" as Double)
			}
		}
		if(params."sCriteria_tipeCustomer"){
			eq("t301KategoriCust",params."sCriteria_tipeCustomer" as String)
		}
        if (params."sCriteria_jamApp") {
            SimpleDateFormat sdf = new SimpleDateFormat("H")
            String plusMinus = params."sCriteria_plusMinus"
            int jamSekarang = sdf.format(new Date()) as int
            int criteriaJam = params."sCriteria_jamApp" as int
            int jamTambahan
            Date tglSekarang = new Date()

            if (plusMinus.equalsIgnoreCase("plus")) {
                jamTambahan = criteriaJam + jamSekarang

            } else {
                jamTambahan = jamSekarang - criteriaJam

            }

            /*le("t301TglJamApp", tglSekarang)*/
            eq("jamApp", jamTambahan)
        }
        if (params."sCriteria_menitApp") {
            SimpleDateFormat sdf = new SimpleDateFormat("H:m")
            String menitJam = sdf.format(new Date())
            String plusMinus = params."sCriteria_plusMinus"
            String[] splitMenitJam = menitJam.split(":")
            int menit = Integer.parseInt(splitMenitJam[1])
            int jam = Integer.parseInt(splitMenitJam[0])
            int criteriaMenit = params."sCriteria_menitApp" as int
            int menitTambahan

            if (plusMinus.equalsIgnoreCase("plus")) {
                menitTambahan = menit + criteriaMenit

                if (menitTambahan > 60) {
                    jam++
                    menitTambahan -= 60
                }
            } else {
                menitTambahan = menit - criteriaMenit

                if (menitTambahan <= 0) {
                    jam--
                    menitTambahan += 60
                }
            }

            /*le("t301TglJamApp", new Date())*/
            eq("jamApp", jam)
            le("menitApp", menitTambahan)
        }
	}
	
    def belumBayarBookingFeeDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq('companyDealer', companyDealer)
			reception{
				ne('totalRencanaDP', 0 as Double)
				eq('totalDPRp', 0 as Double)
			}
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
           rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoWO,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						telpCustomer: it.historyCustomer?.t182NoHp,
						janjiBayar: it.t301TglJamJanjiBayarDP?it.t301TglJamJanjiBayarDP.format(dateFormat + ' ' + timeFormat):'',
						jmlBookingFee:conversi.toRupiah( it.reception?.totalRencanaDP)
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	}
	
	def sudahBayarBookingFeeBelumOrderPartsDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq('companyDealer', companyDealer)
			reception{
				ne('totalRencanaDP', 0 as Double)
				ne('totalDPRp', 0 as Double)
				eq('staRequestParts', '0')
			}
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoWO,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						alamatCustomer: it.historyCustomer?.t182Alamat,
						telpCustomer: it.historyCustomer?.t182NoHp,
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def tungguETAPartsDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) { //iki cah cah cah//
	        searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq('companyDealer', companyDealer)
            ge("t301TglJamRencana",dateAwal)
		}
		println results
		def rows = []
		
		def noUrut = params.iDisplayStart as int
        noUrut++

		results.each {
            def staAmbil = "Belum"
            def namasA = it.t301NamaSA
            def cekParts = PartsApp.findAllByReceptionAndT303StaDel(it?.reception,"0");
            def appTemp = it
            def cekRequest = Request.findByT162NoReffIlikeAndStaDel("%"+appTemp?.reception?.t401NoAppointment+"%","0")
            if(appTemp?.t301TglJamRencana && cekRequest){
                cekParts.each {
                    def paTemp = it
                    def waktuApp = df.format(appTemp?.t301TglJamRencana)
                    Date dateCari = new Date().parse("dd/MM/yyyy HH:mm",waktuApp+" 00:00")
                    def parts = PartsRCP.createCriteria().list {
                        eq("staDel","0")
                        eq("goods",paTemp?.goods)
                        reception{
                            ge("t401TglJamRencana",dateCari)
                            eq("historyCustomerVehicle",appTemp?.historyCustomerVehicle)
                        }
                    }
                    if(parts?.size()>0){
                        staAmbil = "Sudah"
                    }
                }
                rows << [
                        id: it.id,
                        noUrut: noUrut,
                        noAppointment: it.reception?.t401NoAppointment,
                        noWo: it.reception?.t401NoWO,
                        tglAppointment:  it.t301TglJamRencana?it.t301TglJamRencana.format("dd/MM/yyyy HH:mm"):'',
                        noPolisi: it.historyCustomerVehicle?.fullNoPol,
                        namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                        namaCustomer: it.historyCustomer?.fullNama,
                        namaSA: namasA,
                        statusAmbil: staAmbil
                ]
                noUrut++
            }
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}

    def tungguETAPartsDatatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer

        def c = ValidasiOrder.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq('companyDealer', companyDealer)
            requestDetail{
                request{
                    eq("t162NoReff", params."noWO")
                }
            }
        }
        def rows = []
       results.each {
           def pod = PODetail.findByValidasiOrder(it)
           def tglEta = ETA.findByPo(pod?.po)?.t165ETA
            rows << [
                    spacer: "",
                    eta: tglEta?tglEta.format("dd/MM/yyyy HH:mm"):'-',
                    parts: it.requestDetail?.goods?.m111Nama
            ]

        }

        [sEcho: params.sEcho, iTotalRecords:   results.totalCount, iTotalDisplayRecords:  results.totalCount, aaData: rows]

    }
	
	def registrasiAccountWebDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = CustBaruWeb.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchCustBaruWebCriteria.delegate = delegate
			searchCustBaruWebCriteria(params)
			eq('companyDealer', companyDealer)
			eq('t187StaKonfirmasi', '0')
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						namaCustomer: it.fullNama,
						alamatCustomer: it.t187AlamatRumah,
						noHP: it.t187TelpHP,
						noTelpRumah: it.t187TelpRumah,
						jenisKelamin: it.t187JKel = 1 ? "Pria" : "Wanita" ,
						tglLahir: it.t187TanggalLahir?it.t187TanggalLahir.format(dateFormat):'',
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def viaWebBelumPlotASBJPBDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		def c = CustLamaWeb.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchCustLamaWebCriteria.delegate = delegate
			searchCustLamaWebCriteria(params)
				eq("companyDealer", companyDealer)
			    eq('t188StaKonfirmasi', '0')
//				switch(sortProperty){
//					default:
//						order(sortProperty,sortDir)
//						break;
//				}
		}
		
		def rows = []
		
		def noUrut = 1
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noPolisi: it?.customerVehicle ? it?.customerVehicle?.histories?.fullNoPol : "-",
						namaCustomerCustLama: it?.historyCustomer?.fullNama,
						alamatCustomer: it?.historyCustomer?.t182Alamat,
						noHP: it?.historyCustomer?.t182NoHp,
						tglDaftar: it?.t188TglDaftar ? it?.t188TglDaftar?.format("dd/MM/yyyy") : "-",
						pekerjaan: it?.operation?.m053NamaOperation,
						tglService: it?.t188TanggalJamService ? it?.t188TanggalJamService?.format("dd/MM/yyyy | HH:mm") : "-",
						staConfirm: "Y"
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def sudahBayarBookingFeeBelumPlotJPBDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq('companyDealer', companyDealer)
			reception{
				ne('totalRencanaDP', 0 as Double)
				ne('totalDPRp', 0 as Double)
				eq ('countJPB', 0)
			}
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoWO,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						alamatCustomer: it.historyCustomer?.t182Alamat,
						telpCustomer: it.historyCustomer?.t182NoHp,
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def menungguKelengkapanDokumenAsuransiDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq("companyDealer", companyDealer)
			customerVehicle{
				spkAsuransi{
					eq('t193StaJanjiKirimDok', '1')
					eq('t193StaKirimDok', '0')
				}
			}
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoWO,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						alamatCustomer: it.historyCustomer?.t182Alamat,
						telpCustomer: it.historyCustomer?.t182NoHp,
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def menungguSurveyDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq("companyDealer", companyDealer)
			customerVehicle{
				spkAsuransi{
					eq('t193StaReminderSurvey', '1')
					eq('t193StaActualSurvey', '0')
				}
			}
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoWO,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						alamatCustomer: it.historyCustomer?.t182Alamat,
						telpCustomer: it.historyCustomer?.t182NoHp,
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def menungguSPKDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq("companyDealer", companyDealer)
			customerVehicle{
				spkAsuransi{
					eq('t193StaActualSurvey', '1')
				}
			}
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = params.iDisplayStart as int
        noUrut++
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoWO,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						alamatCustomer: it.historyCustomer?.t182Alamat,
						telpCustomer: it.historyCustomer?.t182NoHp,
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def followUpAppointmentDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", session?.userCompanyDealer)
            eq("saveFlag", "0")
            eq("t301StaOkCancelReSchedule", "0")
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
            ge("t301TglJamApp",df.parse(currentDate))
            le("t301TglJamApp",df.parse(currentDate)+1)
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []

        def noUrut = params.iDisplayStart as int
        noUrut++
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoAppointment,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
                        jamAppointment : it.jamApp,
                        menitApp : it.menitApp,
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						telpCustomer: it.historyCustomer?.t182NoHp,
						tipeCustomer: it.t301KategoriCust,
						bookingFee: it.reception?.totalRencanaDP,
						parts: it.reception?.partsTersedia
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def perluRescheduleDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq('t301StaOkCancelReSchedule', '2')
			eq('companyDealer', companyDealer)
			not {
				reception{
					isNull('t401NoAppointment')
				}
			}
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []

        def noUrut = params.iDisplayStart as int
        noUrut++
		
		results.each {
			def bf = PartsApp.findByReceptionAndT303StaDel(it?.reception, '0')
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoAppointment ? it.reception?.t401NoAppointment : "-",
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						telpCustomer: it.historyCustomer?.t182NoHp,
						tipeCustomer: it.t301KategoriCust ? it.t301KategoriCust : "-",
						bookingFee : bf?.t303RencanaDP ? bf?.t303RencanaDP : '-',
						parts: it.reception?.partsTersedia ? it.reception?.partsTersedia : "-"
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
	
	def revisiJanjiPenyerahanDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			searchAppointmentCriteria.delegate = delegate
			searchAppointmentCriteria(params)
			eq("companyDealer", companyDealer)
			isNotNull('t301AlasanReSchedule')
//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []

        def noUrut = params.iDisplayStart as int
        noUrut++
		
		results.each {
			rows << [
						id: it.id,
						noUrut: noUrut,
						noAppointment: it.reception?.t401NoWO,
						tglAppointment:  it.t301TglJamApp?it.t301TglJamApp.format(dateFormat + ' ' + timeFormat):'',
						noPolisi: it.historyCustomerVehicle?.fullNoPol,
						namaBaseModel: it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it.historyCustomer?.fullNama,
						telpCustomer: it.historyCustomer?.t182NoHp,
						tipeCustomer: it.t301KategoriCust,
						bookingFee: it.reception?.totalRencanaDP,
						parts: it.reception?.partsTersedia
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	
	}
}
