package com.kombos.reception

import com.kombos.woinformation.JobRCP

class ViewPoInvoiceService {

    def datatablesList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = POSublet.createCriteria()
        def results = c.list(max: 10 as int, offset: params.iDisplayStart as int) {
            if (params."tanggalStart" && params."tanggalEnd") {
                ge("t412TanggalPO", params."tanggalStart")
                lt("t412TanggalPO", params."tanggalEnd" + 1)
            }
            eq("staDel","0")
            order("t412NoPOSublet")
        }

        def rows = []

        results.each {
            def tampil = true
            def jobRCP = JobRCP.findByReceptionAndOperationAndStaDel(it.reception,it.operation,"0")
            def invSublet = InvoiceSublet.findByPoSubletAndStaDel(it,"0")
            if(params.staBelum){
                if(invSublet){
                    if(invSublet.t413JmlSublet!=null)
                        tampil=false
                }
            }
            if(params.staSudah){
                if(invSublet){
                    if(invSublet.t413JmlSublet==null)
                        tampil=false
                }
            }
            if(params.staSudah && params.staBelum){
                tampil = true;
            }

            if(jobRCP && tampil){
                rows << [
                        idPo: it.id,
                        namaVendor: jobRCP?.vendor?.m121Nama,
                        nomorPo: it.t412NoPOSublet,
                        tanggalPo: it.t412TanggalPO.format("dd/MM/yyyy"),
                        perihal: it.t412Perihal
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
    }

    def datatablesSubList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def po = POSublet.get(params.idPo.toLong())
        def results = InvoiceSublet.createCriteria().list {
            eq("staDel","0")
            eq("poSublet",po)
            reception{
                eq("id",po.reception.id)
                order("t401NoWO")
            }
        }

        def rows = []
        def idAda = []
        results.each {
            if(!idAda.contains(it.reception.id)){
                def jobRCP = JobRCP.findByReceptionAndOperationAndStaDel(it.reception,it.operation,"0")
                if(jobRCP){
                    def jobInv = JobInv.findByReceptionAndOperationAndStaDel(it.reception,it.operation,"0")
                    if(!jobInv){
                        jobInv = JobInv.findByOperationAndStaDel(it.operation,'0')
                    }
                    rows << [
                            noWO: it.reception.t401NoWO ? it.reception.t401NoWO : "-",
                            noPolisi: it.reception.historyCustomerVehicle.fullNoPol ? it.reception.historyCustomerVehicle.fullNoPol : "-",
                            kodeJob: jobRCP.operation.m053Id ? jobRCP.operation.m053JobsId : "-",
                            namaJob: jobRCP.operation.m053NamaOperation? jobRCP.operation.m053NamaOperation : "",
                            noInvoice: it.t413NoInv ? it.t413NoInv : "",
                            tglInvoice: it.t413TanggalInv ? it.t413TanggalInv : "",
                            harga: jobInv.t702HargaRp ? jobInv.t702HargaRp : 0
                    ]
                }
                idAda << it.reception.id
            }
        }
        [sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
    }
}
