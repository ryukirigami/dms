package com.kombos.reception

class ViewStatusKendaraanService {

    def datatablesList(def params) {
        def c = Reception.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_tglFrom") {
                ge("t401TglJamCetakWO", params."sCriteria_tglFrom")
                lt("t401TglJamCetakWO", params."sCriteria_tglTo" + 1)
            }
            ilike("staDel", "0")
            ilike("staSave", "0")
            order("id","desc")
        }

        def rows = []
        def nos = 0
        results.each {
            nos = nos + 1
            //println("it.t401StaCustomerTunggu=" +it.t401StaCustomerTunggu)
            rows << [
                    id: it.id,
                    norut : nos,
                    namaCustomer : it.historyCustomer?.t182NamaBelakang,
                    nomorHp : it.historyCustomer?.t182NoHp,
                    kendaraan : it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    nopol : it.historyCustomerVehicle?.kodeKotaNoPol?.m116NamaKota + " " + it.historyCustomerVehicle?.t183NoPolTengah + " " + it.historyCustomerVehicle?.t183NoPolBelakang,
                    tanggalWO : it.t401TglJamCetakWO,
                    nomorWO : it.t401NoWO,
                    namaSA : it.t401NamaSA,
                    status : it.t401StaApp, //dbo.M999_StatusKendaraan.M999_NamaStatusKendaraan AS Status,
                    deliveryTime : it.t401TglJamPenyerahan,
                    statusDelivery : "-" //it.t401StaCustomerTunggu,
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

}
