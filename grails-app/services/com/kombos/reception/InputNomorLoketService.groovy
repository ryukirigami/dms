package com.kombos.reception

import com.kombos.baseapp.AppSettingParam

class InputNomorLoketService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Appointment.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params."sCriteria_reception"){
                eq("reception",params."sCriteria_reception")
            }

            if(params."sCriteria_asb"){
                eq("asb",params."sCriteria_asb")
            }

            if(params."sCriteria_jpb"){
                eq("jpb",params."sCriteria_jpb")
            }

            if(params."sCriteria_customerVehicle"){
                eq("customerVehicle",params."sCriteria_customerVehicle")
            }

            if(params."sCriteria_historyCustomerVehicle"){
                eq("historyCustomerVehicle",params."sCriteria_historyCustomerVehicle")
            }

            if(params."sCriteria_customer"){
                eq("customer",params."sCriteria_customer")
            }

            if(params."sCriteria_historyCustomer"){
                eq("historyCustomer",params."sCriteria_historyCustomer")
            }

            if(params."sCriteria_t301StaKategoriCustUmumSPKAsuransi"){
                ilike("t301StaKategoriCustUmumSPKAsuransi","%" + (params."sCriteria_t301StaKategoriCustUmumSPKAsuransi" as String) + "%")
            }

            if(params."sCriteria_sPK"){
                eq("sPK",params."sCriteria_sPK")
            }

            if(params."sCriteria_sPKAsuransi"){
                eq("sPKAsuransi",params."sCriteria_sPKAsuransi")
            }

            if(params."sCriteria_t301TglJamApp"){
                ge("t301TglJamApp",params."sCriteria_t301TglJamApp")
                lt("t301TglJamApp",params."sCriteria_t301TglJamApp" + 1)
            }

            if(params."sCriteria_jenisApp"){
                eq("jenisApp",params."sCriteria_jenisApp")
            }

            if(params."sCriteria_t301KategoriCust"){
                ilike("t301KategoriCust","%" + (params."sCriteria_t301KategoriCust" as String) + "%")
            }

            if(params."sCriteria_t301StaFir"){
                ilike("t301StaFir","%" + (params."sCriteria_t301StaFir" as String) + "%")
            }

            if(params."sCriteria_t301KmSaatIni"){
                eq("t301KmSaatIni",params."sCriteria_t301KmSaatIni")
            }

            if(params."sCriteria_t301NamaSA"){
                ilike("t301NamaSA","%" + (params."sCriteria_t301NamaSA" as String) + "%")
            }

            if(params."sCriteria_t301Permintaan"){
                ilike("t301Permintaan","%" + (params."sCriteria_t301Permintaan" as String) + "%")
            }

            if(params."sCriteria_companyDealerIDSumber"){
                eq("companyDealerIDSumber",params."sCriteria_companyDealerIDSumber")
            }

            if(params."sCriteria_companyDealerIDTujuan"){
                eq("companyDealerIDTujuan",params."sCriteria_companyDealerIDTujuan")
            }

            if(params."sCriteria_t301TglJamRencana"){
                ge("t301TglJamRencana",params."sCriteria_t301TglJamRencana")
                lt("t301TglJamRencana",params."sCriteria_t301TglJamRencana" + 1)
            }

            if(params."sCriteria_t301TglJamJanjiBayarDP"){
                ge("t301TglJamJanjiBayarDP",params."sCriteria_t301TglJamJanjiBayarDP")
                lt("t301TglJamJanjiBayarDP",params."sCriteria_t301TglJamJanjiBayarDP" + 1)
            }

            if(params."sCriteria_t301TglJamPenyerahann"){
                ge("t301TglJamPenyerahann",params."sCriteria_t301TglJamPenyerahann")
                lt("t301TglJamPenyerahann",params."sCriteria_t301TglJamPenyerahann" + 1)
            }

            if(params."sCriteria_t301StaOkCancelReSchedule"){
                ilike("t301StaOkCancelReSchedule","%" + (params."sCriteria_t301StaOkCancelReSchedule" as String) + "%")
            }

            if(params."sCriteria_t301TglJamPenyerahanSchedule"){
                ge("t301TglJamPenyerahanSchedule",params."sCriteria_t301TglJamPenyerahanSchedule")
                lt("t301TglJamPenyerahanSchedule",params."sCriteria_t301TglJamPenyerahanSchedule" + 1)
            }

            if(params."sCriteria_t301AlasanReSchedule"){
                ilike("t301AlasanReSchedule","%" + (params."sCriteria_t301AlasanReSchedule" as String) + "%")
            }

            if(params."sCriteria_t301SubTotalRp"){
                eq("t301SubTotalRp",params."sCriteria_t301SubTotalRp")
            }

            if(params."sCriteria_t301PersenPPN"){
                eq("t301PersenPPN",params."sCriteria_t301PersenPPN")
            }

            if(params."sCriteria_t301PPNRp"){
                eq("t301PPNRp",params."sCriteria_t301PPNRp")
            }

            if(params."sCriteria_t301GrandTotalRp"){
                eq("t301GrandTotalRp",params."sCriteria_t301GrandTotalRp")
            }

            if(params."sCriteria_t301DPRp"){
                eq("t301DPRp",params."sCriteria_t301DPRp")
            }

            if(params."sCriteria_t301SisaRp"){
                eq("t301SisaRp",params."sCriteria_t301SisaRp")
            }

            if(params."sCriteria_alasanCancel"){
                eq("alasanCancel",params."sCriteria_alasanCancel")
            }

            if(params."sCriteria_t301staFU1"){
                ilike("t301staFU1","%" + (params."sCriteria_t301staFU1" as String) + "%")
            }

            if(params."sCriteria_t301TglFU1"){
                ge("t301TglFU1",params."sCriteria_t301TglFU1")
                lt("t301TglFU1",params."sCriteria_t301TglFU1" + 1)
            }

            if(params."sCriteria_t301xNamaUserFU1"){
                ilike("t301xNamaUserFU1","%" + (params."sCriteria_t301xNamaUserFU1" as String) + "%")
            }

            if(params."sCriteria_t301staFU2"){
                ilike("t301staFU2","%" + (params."sCriteria_t301staFU2" as String) + "%")
            }

            if(params."sCriteria_t301TglFU2"){
                ge("t301TglFU2",params."sCriteria_t301TglFU2")
                lt("t301TglFU2",params."sCriteria_t301TglFU2" + 1)
            }

            if(params."sCriteria_t301xNamaUserFU2"){
                ilike("t301xNamaUserFU2","%" + (params."sCriteria_t301xNamaUserFU2" as String) + "%")
            }

            if(params."sCriteria_t301Ket"){
                ilike("t301Ket","%" + (params."sCriteria_t301Ket" as String) + "%")
            }

            if(params."sCriteria_t301staDariMRS"){
                ilike("t301staDariMRS","%" + (params."sCriteria_t301staDariMRS" as String) + "%")
            }

            if(params."sCriteria_t301staBookingWalkIn"){
                ilike("t301staBookingWalkIn","%" + (params."sCriteria_t301staBookingWalkIn" as String) + "%")
            }

            if(params."sCriteria_t301staBookingTelp"){
                ilike("t301staBookingTelp","%" + (params."sCriteria_t301staBookingTelp" as String) + "%")
            }

            if(params."sCriteria_t301staBookingWeb"){
                ilike("t301staBookingWeb","%" + (params."sCriteria_t301staBookingWeb" as String) + "%")
            }

            if(params."sCriteria_t301NoDokumenEstimasi"){
                ilike("t301NoDokumenEstimasi","%" + (params."sCriteria_t301NoDokumenEstimasi" as String) + "%")
            }

            if(params."sCriteria_t301TglDokumenEstimasi"){
                ge("t301TglDokumenEstimasi",params."sCriteria_t301TglDokumenEstimasi")
                lt("t301TglDokumenEstimasi",params."sCriteria_t301TglDokumenEstimasi" + 1)
            }

            if(params."sCriteria_t301TkKerusakan"){
                ilike("t301TkKerusakan","%" + (params."sCriteria_t301TkKerusakan" as String) + "%")
            }

            if(params."sCriteria_tipeKerusakan"){
                eq("tipeKerusakan",params."sCriteria_tipeKerusakan")
            }

            if(params."sCriteria_t301StaTPSLine"){
                ilike("t301StaTPSLine","%" + (params."sCriteria_t301StaTPSLine" as String) + "%")
            }

            if(params."sCriteria_colorMatchingKlasifikasi"){
                eq("colorMatchingKlasifikasi",params."sCriteria_colorMatchingKlasifikasi")
            }

            if(params."sCriteria_colorMatchingJmlPanel"){
                eq("colorMatchingJmlPanel",params."sCriteria_colorMatchingJmlPanel")
            }

            if(params."sCriteria_t301Proses1"){
                eq("t301Proses1",params."sCriteria_t301Proses1")
            }

            if(params."sCriteria_t301Proses2"){
                eq("t301Proses2",params."sCriteria_t301Proses2")
            }

            if(params."sCriteria_t301Proses3"){
                eq("t301Proses3",params."sCriteria_t301Proses3")
            }

            if(params."sCriteria_t301Proses4"){
                eq("t301Proses4",params."sCriteria_t301Proses4")
            }

            if(params."sCriteria_t301Proses5"){
                eq("t301Proses5",params."sCriteria_t301Proses5")
            }

            if(params."sCriteria_t301TotalProses"){
                eq("t301TotalProses",params."sCriteria_t301TotalProses")
            }

            if(params."sCriteria_t301staCustomerTunggu"){
                ilike("t301staCustomerTunggu","%" + (params."sCriteria_t301staCustomerTunggu" as String) + "%")
            }

            if(params."sCriteria_t301xNamaUser"){
                ilike("t301xNamaUser","%" + (params."sCriteria_t301xNamaUser" as String) + "%")
            }

            if(params."sCriteria_t301xNamaDivisi"){
                ilike("t301xNamaDivisi","%" + (params."sCriteria_t301xNamaDivisi" as String) + "%")
            }

            if(params."sCriteria_lastUpdProcess"){
                ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    reception: it.reception,

                    asb: it.asb,

                    jpb: it.jpb,

                    customerVehicle: it.customerVehicle,

                    historyCustomerVehicle: it.historyCustomerVehicle,

                    customer: it.customer,

                    historyCustomer: it.historyCustomer,

                    t301StaKategoriCustUmumSPKAsuransi: it.t301StaKategoriCustUmumSPKAsuransi,

                    sPK: it.sPK,

                    sPKAsuransi: it.sPKAsuransi,

                    t301TglJamApp: it.t301TglJamApp?it.t301TglJamApp.format(dateFormat):"",

                    jenisApp: it.jenisApp,

                    t301KategoriCust: it.t301KategoriCust,

                    t301StaFir: it.t301StaFir,

                    t301KmSaatIni: it.t301KmSaatIni,

                    t301NamaSA: it.t301NamaSA,

                    t301Permintaan: it.t301Permintaan,

                    companyDealerIDSumber: it.companyDealerIDSumber,

                    companyDealerIDTujuan: it.companyDealerIDTujuan,

                    t301TglJamRencana: it.t301TglJamRencana?it.t301TglJamRencana.format(dateFormat):"",

                    t301TglJamJanjiBayarDP: it.t301TglJamJanjiBayarDP?it.t301TglJamJanjiBayarDP.format(dateFormat):"",

                    t301TglJamPenyerahann: it.t301TglJamPenyerahann?it.t301TglJamPenyerahann.format(dateFormat):"",

                    t301StaOkCancelReSchedule: it.t301StaOkCancelReSchedule,

                    t301TglJamPenyerahanSchedule: it.t301TglJamPenyerahanSchedule?it.t301TglJamPenyerahanSchedule.format(dateFormat):"",

                    t301AlasanReSchedule: it.t301AlasanReSchedule,

                    t301SubTotalRp: it.t301SubTotalRp,

                    t301PersenPPN: it.t301PersenPPN,

                    t301PPNRp: it.t301PPNRp,

                    t301GrandTotalRp: it.t301GrandTotalRp,

                    t301DPRp: it.t301DPRp,

                    t301SisaRp: it.t301SisaRp,

                    alasanCancel: it.alasanCancel,

                    t301staFU1: it.t301staFU1,

                    t301TglFU1: it.t301TglFU1?it.t301TglFU1.format(dateFormat):"",

                    t301xNamaUserFU1: it.t301xNamaUserFU1,

                    t301staFU2: it.t301staFU2,

                    t301TglFU2: it.t301TglFU2?it.t301TglFU2.format(dateFormat):"",

                    t301xNamaUserFU2: it.t301xNamaUserFU2,

                    t301Ket: it.t301Ket,

                    t301staDariMRS: it.t301staDariMRS,

                    t301staBookingWalkIn: it.t301staBookingWalkIn,

                    t301staBookingTelp: it.t301staBookingTelp,

                    t301staBookingWeb: it.t301staBookingWeb,

                    t301NoDokumenEstimasi: it.t301NoDokumenEstimasi,

                    t301TglDokumenEstimasi: it.t301TglDokumenEstimasi?it.t301TglDokumenEstimasi.format(dateFormat):"",

                    t301TkKerusakan: it.t301TkKerusakan,

                    tipeKerusakan: it.tipeKerusakan,

                    t301StaTPSLine: it.t301StaTPSLine,

                    colorMatchingKlasifikasi: it.colorMatchingKlasifikasi,

                    colorMatchingJmlPanel: it.colorMatchingJmlPanel,

                    t301Proses1: it.t301Proses1,

                    t301Proses2: it.t301Proses2,

                    t301Proses3: it.t301Proses3,

                    t301Proses4: it.t301Proses4,

                    t301Proses5: it.t301Proses5,

                    t301TotalProses: it.t301TotalProses,

                    t301staCustomerTunggu: it.t301staCustomerTunggu,

                    t301xNamaUser: it.t301xNamaUser,

                    t301xNamaDivisi: it.t301xNamaDivisi,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def jobnPartsDatatablesList(def params){
        def rows = []

        //results.each {
        rows << [

                idJob: 1,

                namaJob: 'PENGGANTIAN PINTU DEPAN KIRI',

                rate: '',

                statusWarranty: '-',

                nominal: '1250000'

        ]
        //}

        [sEcho: params.sEcho, iTotalRecords:  1, iTotalDisplayRecords: 1, aaData: rows]
    }

    def partDatatablesList(def params){
        def rows = []

        //results.each {
        rows << [

                idPart: 1,

                namaPart: 'PINTU DEPAN KIRI',

                qty: '1',

                satuan: 'pcs',

                harga: 500000,

                rate: '',

                statusWarranty: '-',

                nominal: '1250000'

        ]
        //}

        [sEcho: params.sEcho, iTotalRecords:  1, iTotalDisplayRecords: 1, aaData: rows]
    }

    def historyServiceDatatablesList(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def rows = []

        //results.each {
        rows << [

                tanggal: new Date().format(dateFormat),

                job: ['Ganti kopling', 'TuneUp'],

                parts: ['Kopling'],

                jobSuggest: 'Ganti Rem'
        ]
        //}

        [sEcho: params.sEcho, iTotalRecords:  1, iTotalDisplayRecords: 1, aaData: rows]
    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Appointment", params.id] ]
            return result
        }

        result.appointmentInstance = Appointment.get(params.id)

        if(!result.appointmentInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Appointment", params.id] ]
            return result
        }

        result.appointmentInstance = Appointment.get(params.id)

        if(!result.appointmentInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Appointment", params.id] ]
            return result
        }

        result.appointmentInstance = Appointment.get(params.id)

        if(!result.appointmentInstance)
            return fail(code:"default.not.found.message")

        try {
            result.appointmentInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }

    def update(params) {
        Appointment.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.appointmentInstance && m.field)
                    result.appointmentInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["Appointment", params.id] ]
                return result
            }

            result.appointmentInstance = Appointment.get(params.id)

            if(!result.appointmentInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.appointmentInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.appointmentInstance.properties = params

            if(result.appointmentInstance.hasErrors() || !result.appointmentInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Appointment", params.id] ]
            return result
        }

        result.appointmentInstance = new Appointment()
        result.appointmentInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.appointmentInstance && m.field)
                result.appointmentInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["Appointment", params.id] ]
            return result
        }

        result.appointmentInstance = new Appointment(params)

        if(result.appointmentInstance.hasErrors() || !result.appointmentInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        // success
        return result
    }

    def updateField(def params){
        def appointment =  Appointment.findById(params.id)
        if (appointment) {
            appointment."${params.name}" = params.value
            appointment.save()
            if (appointment.hasErrors()) {
                throw new Exception("${appointment.errors}")
            }
        }else{
            throw new Exception("Appointment not found")
        }
    }
}
