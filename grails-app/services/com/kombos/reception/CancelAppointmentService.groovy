package com.kombos.reception

import com.kombos.approval.AfterApprovalInterface
import com.kombos.board.ASB
import com.kombos.maintable.ApprovalT770
import com.kombos.maintable.JobApp
import com.kombos.maintable.PartsApp
import com.kombos.parts.StatusApproval
import org.springframework.web.context.request.RequestContextHolder

class CancelAppointmentService implements AfterApprovalInterface{

    def datatablesUtilService

    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        if (staApproval == StatusApproval.APPROVED) {
            //println "Canceling Appointment.."
            PartsApp.withTransaction { status ->
                def fail = { Map m ->
                    status.setRollbackOnly()
                }
                def session = RequestContextHolder.currentRequestAttributes().getSession()
                fks.split(ApprovalT770.FKS_SEPARATOR).each {
                    def app = Appointment.get(it as Long)
                    def rcp = app.reception

                    if (rcp) {
                        def jobApp = JobApp.findAllByReception(rcp)
                        jobApp.each {
                            def parts = PartsApp.findByReceptionAndOperationAndT303StaDel(rcp, it.operation, "0")
                            parts.each {

                            }
                            it?.t302StaDel = "1"
                            it?.lastUpdProcess = "CANCEL"
                            it?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            it.save(flush: true)
                        }

                    }

                    def asb = ASB.findAllByAppointmentAndStaDel(app,"0");
                    asb.each {
                        it?.staDel = "1"
                        it?.lastUpdated = datatablesUtilService.syncTime()
                        it.save(flush: true)
                    }
                    app?.t301StaOkCancelReSchedule = "1"
                    app?.staDel ="1"
                    app?.save()
                    app.errors.each { //println it
                    }
                }
            }
        }
    }
}
