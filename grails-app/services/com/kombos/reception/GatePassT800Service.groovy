package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class GatePassT800Service {
	boolean transactional = false

    def datatablesUtilService

	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")

        def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = GatePassT800.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
             eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_Tanggal" && params."sCriteria_Tanggal2") {
                def tanggalAwal = new Date().parse("dd/MM/yyyy",params.sCriteria_Tanggal)
                def tanggalAKhir = new Date().parse("dd/MM/yyyy",params.sCriteria_Tanggal2)
                ge("t800TglJamGatePass",tanggalAwal)
                lt("t800TglJamGatePass", tanggalAKhir + 1)
            }

			if(params."sCriteria_wo"){
				reception{
                   ilike("t401NoWO","%" + params."sCriteria_wo" + "%");
                }
			}
            if(params."sCriteria_tglWO"){
                reception{
                    ge("t401TanggalWO",params."sCriteria_tglWO")
                    lt("t401TanggalWO",params."sCriteria_tglWO" + 1)
                }
            }

            if (params."sCriteria_tglgatepass") {
                ge("t800TglJamGatePass", params."sCriteria_tglgatepass")
                lt("t800TglJamGatePass", params."sCriteria_tglgatepass" + 1)
            }



            if(params."sCriteria_nopol"){
                reception{
                    historyCustomerVehicle{
                        String kode="",tengah="",akhir=""
                        String parameter = params.sCriteria_nopol.replace(" ","")
                        int kond=0
                        for(int a=0;a<parameter.length();a++){
                            if(parameter.charAt(a).toString().isNumber()){
                                kond=1
                                tengah+=parameter.charAt(a).toString()
                            }else{
                                if(kond==0){
                                    kode+=parameter.charAt(a).toString()
                                }else{
                                    akhir+=parameter.charAt(a).toString()
                                }
                            }
                        }
                        kodeKotaNoPol{
                            eq("m116ID",kode,[ignoreCase : true])
                        }
                        eq("t183NoPolTengah",tengah,[ignoreCase : true])
                        eq("t183NoPolBelakang",akhir,[ignoreCase : true])
                    }
                }
            }
            if(params."sCriteria_model"){
                reception{
                    historyCustomerVehicle{
                        fullModelCode{
                            baseModel{
                                ilike("m102NamaBaseModel","%" + params."sCriteria_model" + "%");
                            }
                        }
                    }

                }
            }

            if(params."sCriteria_namaCustomer"){
                reception{
                    eq("historyCustomer",HistoryCustomer.findByT182NamaDepanIlikeOrT182NamaBelakangIlike("%" + params."sCriteria_namaCustomer" + "%","%" + params."sCriteria_namaCustomer" + "%"));
                }
            }

			if(params."sCriteria_gatePass"){
                    eq("gatePass",GatePass.findByM800NamaGatePassIlike("%" + params."sCriteria_gatePass" + "%"))
			}

            if(params."sCriteria_noGatepass"){
                    ilike("t800ID","%" + params."sCriteria_noGatepass" + "%");
            }



			if(params."sCriteria_t800xKet"){
				ilike("t800xKet","%" + (params."sCriteria_t800xKet" as String) + "%")
			}

			if(params."sCriteria_t800xNamaUser"){
				ilike("t800xNamaUser","%" + (params."sCriteria_t800xNamaUser" as String) + "%")
			}

				ilike("staDel","0")




			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}

		def rows = []
		
		results.each {

			rows << [
			
						id: it.id,

                        t800ID: it?.t800ID,
			
						wo: it?.reception.t401NoWO,

                        tglWo: it?.reception.t401TanggalWO ? it?.reception.t401TanggalWO.format(dateFormat):"",

                        model : it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel:"-",

                        namaCustomer: it.reception?.historyCustomer?.fullNama ? it.reception?.historyCustomer?.fullNama:"-",

                        noPol: it.reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID+ " " + it.reception?.historyCustomerVehicle?.t183NoPolTengah+ " " + it.reception?.historyCustomerVehicle?.t183NoPolBelakang,

						t800TglJamGatePass: it?.t800TglJamGatePass ? it?.t800TglJamGatePass?.format("dd/MM/yyyy HH:mm"):"-",
			
						t800xKet: it.t800xKet ? it.t800xKet:"-",
			
						t800xNamaUser: it.t800xNamaUser ? it.t800xNamaUser:"-",
			
						t800xDivisi: it.t800xDivisi ? it.t800xDivisi:"-",

                        staDel: it.staDel,
			
						lastUpdProcess: it.lastUpdProcess ? it.lastUpdProcess:"-",

                        gatePass: it?.gatePass?.m800NamaGatePass ? it?.gatePass?.m800NamaGatePass:"-",
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["GatePassT800", params.id] ]
			return result
		}

		result.gatePassT800Instance = GatePassT800.get(params.id)

		if(!result.gatePassT800Instance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["GatePassT800", params.id] ]
			return result
		}

		result.gatePassT800Instance = GatePassT800.get(params.id)

		if(!result.gatePassT800Instance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["GatePassT800", params.id] ]
			return result
		}

		result.gatePassT800Instance = GatePassT800.get(params.id)

		if(!result.gatePassT800Instance)
			return fail(code:"default.not.found.message")

		try {
			result.gatePassT800Instance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		GatePassT800.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.gatePassT800Instance && m.field)
					result.gatePassT800Instance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["GatePassT800", params.id] ]
				return result
			}

			result.gatePassT800Instance = GatePassT800.get(params.id)

			if(!result.gatePassT800Instance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.gatePassT800Instance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			//result.gatePassT800Instance.properties = params
            def gate = GatePassT800.createCriteria().list {
                eq("staDel","0")
                reception{
                    historyCustomerVehicle{
                        eq("id",params.reception.id.toLong())
                    }
                }
            }
            if(gate){
                for(find in gate){
                    if(find?.id!=params.id){
                        return [ada : "ada",instance : result.gatePassT800Instance]
                        break
                    }
                }
            }
            result.gatePassT800Instance?.reception = Reception.findByHistoryCustomerVehicle(HistoryCustomerVehicle.findById(params.reception.id))
            result.gatePassT800Instance?.gatePass = GatePass.findById(params.gatePass)
            result.gatePassT800Instance?.t800TglJamGatePass = new Date()
            result.gatePassT800Instance?.t800xKet = params.t800xKet

            result.gatePassT800Instance?.t800xNamaUser = params.t800xNamaUser
            result.gatePassT800Instance?.t800xDivisi = 'DIVISI'
            result.gatePassT800Instance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.gatePassT800Instance?.lastUpdProcess = "UPDATE"
            result.gatePassT800Instance?.lastUpdate = params?.lastUpdate

			if(result.gatePassT800Instance.hasErrors() || !result.gatePassT800Instance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["GatePassT800", params.id] ]
			return result
		}

		result.gatePassT800Instance = new GatePassT800()
		result.gatePassT800Instance.properties = params

		// success
		return result
	}

	def save(params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def ket = "-"
        if(params.t800xKet!=""){
            ket = params.t800xKet
        }
        def result = [:]
		def fail = { Map m ->
			if(result.gatePassT800Instance && m.field)
				result.gatePassT800Instance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["GatePassT800", params.id] ]
			return result
		}

        result.gatePassT800Instance = new GatePassT800()
        result.gatePassT800Instance?.companyDealer= (session?.userCompanyDealer)
        result.gatePassT800Instance?.t800ID = new Date().format("yyMMddhhmmss").toString()
        result.gatePassT800Instance?.reception = Reception.findById(params.reception.id as Long)
        result.gatePassT800Instance?.gatePass = GatePass.findById(params.gatePass)
        result.gatePassT800Instance?.t800TglJamGatePass = datatablesUtilService?.syncTime()
        result.gatePassT800Instance?.t800xKet = ket
        result.gatePassT800Instance?.t800xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.gatePassT800Instance?.t800xDivisi = 'DIVISI'
        result.gatePassT800Instance?.staDel = '0'
        result.gatePassT800Instance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.gatePassT800Instance?.lastUpdProcess = "INSERT"
        result.gatePassT800Instance?.dateCreated  = datatablesUtilService?.syncTime()
        result.gatePassT800Instance?.lastUpdated = datatablesUtilService?.syncTime()

		if(result.gatePassT800Instance.hasErrors() || !result.gatePassT800Instance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def gatePassT800 =  GatePassT800.findById(params.id)
		if (gatePassT800) {
			gatePassT800."${params.name}" = params.value
			gatePassT800.save()
			if (gatePassT800.hasErrors()) {
				throw new Exception("${gatePassT800.errors}")
			}
		}else{
			throw new Exception("GatePassT800 not found")
		}
	}
    def findNoPol(params){
        def nopol =""; def depan = ""; def tengah = ""; def belakang = "";
        try {
            nopol = (params.nopol as String).trim()
            def res = nopol.split(" ");
            depan = res[0];
            tengah = res[1];
            belakang = res[2];
        }catch (Exception e){

        }
        def reception = Reception.createCriteria()
        def results = reception.list() {
            eq("staDel","0")
            eq("staSave","0")
            historyCustomerVehicle{
                kodeKotaNoPol{
                    eq("m116ID",depan)
                }
                eq("t183NoPolTengah",tengah)
                eq("t183NoPolBelakang",belakang)
            }
            order("dateCreated","desc")
            maxResults(1)
        }
        def rows = []

        results.each {
            rows << [

                    id : it?.id,

                    namaCustomer : it?.historyCustomer?.t182NamaDepan + " " + it?.historyCustomer?.t182NamaBelakang,

                    models : it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,

                    warna : it?.historyCustomerVehicle?.warna?.m092NamaWarna,
          ]
        }

        return rows
    }
}
