package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.parts.Claim
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class ViewMaterialPerWoService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)


    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = MOS.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params.kriteria=="s_noWo"){
                reception{
                    ilike("t401NoWO","%" + params.kataKunci + "%")
                }
            }else if(params.kriteria=="s_SA"){
                reception{
                    ilike("t401NamaSA","%" + params.kataKunci + "%")
                }
            }else if(params.kriteria=="s_noPol"){
                String nopol = params.kataKunci
                if(params.kataKunci){
                    reception{
                        historyCustomerVehicle{
                                ilike("fullNoPol","%" +params.kataKunci+ "%")
                        }
                    }
                }
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("reception", "reception")
            }

        }
        def rows = []

        results.each {
                def nopol = it.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.reception.historyCustomerVehicle.t183NoPolBelakang
                rows << [
                        t401NoWO: it.reception.t401NoWO,
                        t401TanggalWO: it.reception.t401TanggalWO?.format(dateFormat),
                        noPol: nopol,
                        t401NamaSA: it?.reception.t401NamaSA,
                ]
        }

        [sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def x = 0
        def c = MOS.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO", params.noWo)
            }
            eq("staDel","0")
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t417NoMOS", "t417NoMOS")
                groupProperty("t417xNamaUser", "t417xNamaUser")
                max("t417TglMOS", "t417TglMOS")
            }
        }

        //println "list: " + results
        def rows = []

        results.each {
            rows << [
                    t417NoMOS: it.t417NoMOS,
                    t417TglMOS: it.t417TglMOS,
                    t417xNamaUser: it.t417xNamaUser
            ]

        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = MOS.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            ilike("t417NoMOS", params.noMos)
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    kodeMaterial: it.goods?.m111ID,
                    namaMaterial: it.goods?.m111Nama,
                    t417Qty1: it.t417Qty1,
                    satuan: it.goods.satuan.m118Satuan1
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Claim", params.id]]
            return result
        }

        result.claimInstance = Claim.get(params.id)

        if (!result.claimInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def id = Claim.findByT171ID(params.id)
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Claim", id.id]]
            return result
        }

        result.claimInstance = Claim.get(id.id)

        if (!result.claimInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Claim", params.id]]
            return result
        }

        result.claimInstance = Claim.get(params.id)

        if (!result.claimInstance)
            return fail(code: "default.not.found.message")

        try {
            result.claimInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each {
            def hapus= null
            try{
                hapus= MOS.get(it)
            }catch (Exception ex){
                def  hapus2 = MOS.findAllByReception(Reception.findByT401NoWO(it))
                hapus2*.delete()
            }
            //println "it : " + it
            if(hapus==null){
                //println "masuk sini gan"
                def  hapus2 =  MOS.findAllByReception(Reception.findByT401NoWO(it))
                hapus2*.delete()
            }
            hapus*.delete()
        }

    }

}
