package com.kombos.reception

import com.kombos.approval.AfterApprovalInterface
import com.kombos.parts.StatusApproval

class SpkOverLimitService implements AfterApprovalInterface{
    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        Reception reception = Reception.get(fks.toLong())
        Reception nReception = new Reception()
        nReception.properties = reception.properties
        reception.staDel = "1"
        reception.save(flush: true)
        nReception.t401StaButuhSPKSebelumProd="0"
        nReception.save(flush: true)
    }
}
