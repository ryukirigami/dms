package com.kombos.reception

import com.kombos.administrasi.MappingJobPanel
import com.kombos.baseapp.AppSettingParam
import com.kombos.board.JPB
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class ViewOrderCatService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            customerIn{
                tujuanKedatangan{
                    ilike("m400Tujuan","BP")
                }
            }
            if(params.sCriteria_kriteria=="s_noWo"){
                eq("t401NoWO",params.sCriteria_kunci)
            }else if(params.sCriteria_kriteria=="s_model"){
                historyCustomerVehicle{
                    fullModelCode{
                        baseModel{
                            ilike("m102NamaBaseModel",params.sCriteria_kunci)
                        }
                    }
                }
            }else if(params.sCriteria_kriteria=="s_SA"){
                eq("t401NamaSA",params.sCriteria_kunci)
            }else if(params.sCriteria_kriteria=="s_noPol"){
                String nopol = params.sCriteria_kunci
                def data = nopol.split("\\s+")
                if(data.size()==3){
                    historyCustomerVehicle{
                        kodeKotaNoPol{
                            eq("m116ID",data[0].trim())
                        }
                    }
                    historyCustomerVehicle{
                        eq("t183NoPolTengah",data[1].trim())
                    }
                    historyCustomerVehicle{
                        eq("t183NoPolBelakang",data[2].trim())
                    }
                }else{
                    historyCustomerVehicle{
                        eq("t183NoPolTengah",params.sCriteria_kunci+"asadas")
                    }
                }
            }
            order("t401NoWO","asc")
        }
        def rows = []
        int jml = 0
        results.each {
            def pos = POS.findByReception(it)

            if(pos){
                jml++
                def nopol = it.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.historyCustomerVehicle.t183NoPolTengah + " " + it.historyCustomerVehicle.t183NoPolBelakang
                def jpb = JPB.findByReception(it)?.t451TglJamPlan
                if(jpb){
                    jpb += it.t401Proses1 as int
                    jpb += it.t401Proses2 as int
                }

                rows << [

                        t401NoWO: it.t401NoWO,

                        nopol: nopol,

                        t401TanggalWO : it.t401TanggalWO?it.t401TanggalWO.format("dd/MM/yyyy"):"-",

                        model : it.historyCustomerVehicle.fullModelCode?.baseModel?.m102NamaBaseModel,

                        paintingTime : jpb,

                        SA : it.t401NamaSA,

                        sCriteria_vendorCat : params.sCriteria_vendorCat,
                        sCriteria_Tanggal : params.sCriteria_Tanggal,
                        sCriteria_Tanggalakhir : params.sCriteria_Tanggalakhir

                ]
            }
        }



        [sEcho: params.sEcho, iTotalRecords:  jml, iTotalDisplayRecords: jml, aaData: rows]

    }

    def datatablesSubList(def params) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def c = POS.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO",params.t401NoWO)
            }

            if(params.sCriteria_vendorCat){
                vendorCat{
                    eq("id",params.sCriteria_vendorCat as Long)
                }
            }

            if (params.sCriteria_Tanggal!=""  && params.sCriteria_Tanggalakhir!="") {
                Date date41 = df.parse(params.sCriteria_Tanggal)
                Date date42 = df.parse(params.sCriteria_Tanggalakhir)
                ge("t418TglSupply", date41)
                lt("t418TglSupply", date42 + 1)
            }

            if(params.sCriteria_belumSupply){
                eq("t418NoSupply","")
            }else if(params.sCriteria_sudahSupply){
                ne("t418NoSupply","")
            }

        }

        def rows = []

        results.each {



            rows << [

                    id: it.id,

                    t401NoWO: params.t401NoWO,

                    noPos: it?.t418NoPOS,

                    tglPos : it?.t418TglPOS?it?.t418TglPOS.format("dd/MM/yyyy"):"-",

                    vendor : it?.vendorCat?.m191Nama,

                    noSupply : it?.t418NoSupply,

                    tglSupply : it?.t418TglSupply?it?.t418TglSupply.format("dd/MM/yyyy"):"-",

                    ket : it?.t418KetSupply,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount,  aaData: rows]

    }

    def datatablesSubSubList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def c = PosJob.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO",params.t401NoWO)
            }
            pos{
                eq("t418NoPOS",params.noPos)
            }
        }

        def rows = []

        def dataMjp = ""
        def daKet = ""
        results.each {
            def mjp = MappingJobPanel.findAllByOperation(it.operation)
            mjp.each {
                dataMjp += it.masterpanel?.m094NamaPanel
                dataMjp +=", "
            }
            daKet += it.operation.m053Ket + ", "
        }


        def posJ = results.first()
        rows << [

                id: posJ.id,

                kodeWarna  : posJ.pos?.warna?.m092ID,

                namaWarna : posJ.pos?.warna?.m092NamaWarna,

                panel : dataMjp,

                ket : daKet,

                t401NoWO : params.t401NoWO,

                noPos : params.noPos

        ]

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def datatablesSubSubSubList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def x = 0
        def c = SupplyWarna.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            pos{
                eq("t418NoPOS",params.noPos)
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kodeWarnaVendor: it?.warna?.m192IDWarna,

                    namaWarnaVendor: it?.warna?.m192NamaWarna,

                    qty : it?.t420Qty,

                    satuan : 'cc',


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def sup = null
            def pos = POS.findByT418NoPOS(it)
            def rec = Reception.findByT401NoWO(it)
            try{
                sup = SupplyWarna.findById(it)
            }catch(Exception a){

            }

            if(pos){
                SupplyWarna.findAllByPos(pos).each{
                    it?.delete(flush: true)
                }
                PosJob.findAllByPos(pos).each{
                    it?.delete(flush: true)
                }
                POS.findAllByT418NoPOS(it).each{
                    it?.delete(flush: true)
                }
            }else if(rec){
                SupplyWarna.findAllByReception(rec).each{
                    it?.delete(flush: true)
                }
                PosJob.findAllByReception(rec).each{
                    it?.delete(flush: true)
                }
                POS.findAllByReception(rec).each{
                    it?.delete(flush: true)
                }
            }else if(sup){
                SupplyWarna?.get(it)?.delete(flush: true)
            }
        }
    }
}
