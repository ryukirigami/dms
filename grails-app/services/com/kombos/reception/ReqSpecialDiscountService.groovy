package com.kombos.reception

import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.administrasi.TarifPerJam
import com.kombos.approval.AfterApprovalInterface
import com.kombos.parts.GoodsHargaJual
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.MappingPartRegion
import com.kombos.parts.StatusApproval
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP

/**
 * Created by HP_Probook on 10/21/14.
 */
class ReqSpecialDiscountService implements AfterApprovalInterface {
    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        if (staApproval == StatusApproval.APPROVED) {
            def arrParam = fks.split('#');
            def reception = Reception.get(arrParam[0].toLong())
            reception.staApprovalDisc = "0"
            reception.save(flush: true)
            def billTo = BillToReception.get(arrParam[1].toLong())
            def jobs = JobRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",reception)
            }
            def trfPerjam = TarifPerJam.findByKategoriAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.kategoriKendaraan,"0",reception?.companyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
            def tarif = trfPerjam?.t152TarifPerjamSB
            jobs.each {
                String kategori = it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()
                if(it?.t402billTo?.toUpperCase()?.contains("DEALER") && tarif && kategori?.replaceAll(" ","")!="SBI" && kategori?.contains("SB")){
                    String jobTam = "SB10K,SB20K,SB30K,SB40K,SB50K,ESB10K,ESB20K,ESB30K,ESB40K,ESB50K"
                    if(jobTam.toString().contains(it?.operation.m053Id)){
                        tarif = trfPerjam?.t152TarifPerjamDealer
                    }
                    def harga = it?.t402Rate * tarif
                    it?.t402HargaRp = harga
                    it?.t402TotalRp = harga
                }
                it?.t402TotalRp = it.t402HargaRp - (it?.t402DiscRp?it?.t402DiscRp:0)
                it?.save(flush: true)
            }
            def parts = PartsRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",reception)
            }
            parts.each {
                it?.t403TotalRp = ((it?.t403Jumlah1 * it?.t403HargaRp) - (it?.t403DiscRp?it?.t403DiscRp:0))
                if(it.t403DiscPersen <= 0 && it.t403DiscPersen && it.t403DiscPersen!=null){
                    def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it?.goods,  "0",new Date(),[sort: "t151TMT", order: "desc"])
                    def mappingCompRegion = MappingCompanyRegion.createCriteria().list { data ->
                        eq("staDel","0");
                        eq("companyDealer",it.reception.companyDealer);
                    }
                    def mappingRegion = MappingPartRegion.createCriteria().get {
                        if(mappingCompRegion?.size()>0){
                            inList("region",mappingCompRegion?.region)
                        }else{
                            eq("id",-10000.toLong())
                        }
                        maxResults(1);
                    }
                    def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                    if(mappingRegion){
                        try {
                            if(KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("TOYOTA") ||
                                    KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("OLI")){
                                hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                            }
                        }catch (Exception e){

                        }

                    }
                    def hargaAdd = hargaTanpaPPN - (hargaTanpaPPN * it.t403DiscPersen / 100)
                    it?.t403HargaRp = hargaAdd
                    it?.t403TotalRp = hargaAdd * it.t403Jumlah1
                    it.t403DiscPersen = 0
                    it.t403DiscRp = 0
                }
                it?.save(flush: true)
            }
        }
        else {
            def param = fks.split("#");
            def reception = Reception.get(param[0].toLong())
            reception.staApprovalDisc = "1"
            reception.save(flush: true)
            def billTo = BillToReception.get(param[1].toLong())
            def jobs = JobRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",reception)
                if(billTo){
                    eq("t402billTo",billTo?.billTo,[ignoreCase:true])
                    eq("t402namaBillTo",billTo?.namaBillTo,[ignoreCase: true])
                }else{
                    isNull("t402billTo")
                    isNull("t402namaBillTo")
                }
            }


            jobs.each {
                it?.t402billTo = ""
                it?.t402namaBillTo = ""
                it?.t402DiscPersen = 0
                it?.t402DiscRp = 0
                it?.t402StaTambahKurang = null
                it?.t402StaApproveTambahKurang = ""
                it?.save(flush: true)
            }
            def parts = PartsRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",reception)
                if(billTo){
                    eq("t403billTo",billTo?.billTo,[ignoreCase:true])
                    eq("t403namaBillTo",billTo?.namaBillTo,[ignoreCase: true])
                }else{
                    isNull("t403billTo")
                    isNull("t403namaBillTo")
                }
            }
            parts.each {
                it?.t403billTo = ""
                it?.t403namaBillTo = ""
                it?.t403DiscPersen = 0
                it?.t403DiscRp = 0
                it?.t403StaApproveTambahKurang = ""
                it?.staDel = '0'
                it.t403StaTambahKurang = ""
                it?.save(flush: true)
            }
            if(billTo){
                billTo.jobs.clear()
                billTo.parts.clear()
                billTo.save(flush: true)
            }
        }
    }
}
