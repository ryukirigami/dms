package com.kombos.reception

import com.kombos.administrasi.FullModelVinCode
import com.kombos.administrasi.GeneralParameter
import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.board.JPB
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.MappingCustVehicle
import org.activiti.engine.impl.pvm.delegate.ActivityExecution

import java.text.DateFormat
import java.text.SimpleDateFormat

class CustomerInService {

    boolean gormReady = false
    boolean locked = false

    def datatablesUtilService

    def findStatusBooking(params){
        def customerVehicles
        CustomerVehicle customerVehicle
        def rows = []
        def appointments
		def appointment
		
		def statusBooking = "W"
		def statusBookingDesc = "Walk In"
		def t301TglJamApp = null
		
		def historyCVInstance = HistoryCustomerVehicle.findByKodeKotaNoPolAndT183NoPolTengahAndT183NoPolBelakang(KodeKotaNoPol.findById(Long.parseLong(params.kode)),params.tengah,params.belakang)
        if(historyCVInstance){
            customerVehicles = CustomerVehicle.findAllByCompanyDealerAndStaDel(params.companyDealer,'0')
            customerVehicles.each {
                if(it.getCurrentCondition()==historyCVInstance){
                    customerVehicle = it.getCurrentCondition().getCustomerVehicle()
                }
            }
			
			if(customerVehicle){
				appointments = Appointment.findAllByCustomerVehicleAndStaDel(customerVehicle, '0')
				if(appointments){
					appointment = appointments.first()
				}
				
				if(appointment){
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy h:mm")
					def date = datatablesUtilService?.syncTime()
					def sqlTimestamp = date.toTimestamp()
					
					t301TglJamApp = appointment.t301TglJamApp
					//book valid
					if(appointment.t301TglJamApp >= sqlTimestamp){
						statusBooking = "B" 
						statusBookingDesc = "Booking"	
					}
					
					rows << [
						statusBooking: statusBooking,
						
						statusBookingDesc : statusBookingDesc,
						
						idCustomerVehicle : customerVehicle.id,
						
						t301TglJamApp: sdf.format(new Date(t301TglJamApp.getTime())),
						
						t183NamaSTNK : historyCVInstance?.t183NamaSTNK,

						t183AlamatSTNK : historyCVInstance?.t183AlamatSTNK,

						customerVehicle : historyCVInstance?.customerVehicle?.t103VinCode,

						fullModelCode : historyCVInstance?.fullModelCode?.t110FullModelCode,

						fullModelCodeBaseModel : historyCVInstance.fullModelCode.baseModel.m102KodeBaseModel,

						fullModelCodeGear : historyCVInstance.fullModelCode.gear.m106NamaGear,

						warna : historyCVInstance?.warna?.m092NamaWarna,

						t183ThnBlnRakit : historyCVInstance?.t183ThnBlnRakit,

						t183TglDEC : historyCVInstance?.t183TglDEC
					]	
				}
			}
        }
		
		return rows
    }

    def checkAppointmentBooked(params){
        def historyCustomerVehicle = HistoryCustomerVehicle.findByKodeKotaNoPolAndT183NoPolTengahAndT183NoPolBelakang(KodeKotaNoPol.findById(Long.parseLong(params.kode)),params.tengah,params.belakang)

    }

    def checkAppointmentBoookedComplaint(params){
        def historyCustomerVehicle = HistoryCustomerVehicle.findByKodeKotaNoPolAndT183NoPolTengahAndT183NoPolBelakang(KodeKotaNoPol.findById(Long.parseLong(params.kode)),params.tengah,params.belakang)

    }

    def checkGatePass(params){
        def historyCustomerVehicle = HistoryCustomerVehicle.findByKodeKotaNoPolAndT183NoPolTengahAndT183NoPolBelakang(KodeKotaNoPol.findById(Long.parseLong(params.kode)),params.tengah,params.belakang)
    }

    def boardAntrianGRList(def params) {
        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0


        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)


        def c = CustomerIn.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
            tujuanKedatangan{
                ilike("m400Tujuan","%GR%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []
        results.each {
            rows << [
                id: it.id,
                t400NomorAntrian: it?.t400NomorAntrian,
                nopol: it?.customerVehicle ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol : it?.t400noPol,
                t400TglJamReception:(it?.t400TglCetakNoAntrian==null)?"":hhmm.format(it?.t400TglCetakNoAntrian),
                t400TglJamSelesaiReception:(it?.t400TglJamSelesaiReception==null)?"":hhmm.format(it?.t400TglJamSelesaiReception),
                m404NoLoket:it?.loket?.m404NoLoket,
                t400TglJamOut:(it?.t400TglJamOut==null)?"" : hhmm.format(it?.t400TglJamOut),

            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def boardAntrianBPList(def params) {
        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0


        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)


        def c = CustomerIn.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
            tujuanKedatangan{
                ilike("m400Tujuan","%BP%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    t400NomorAntrian: it.t400NomorAntrian,
                    nopol: it?.customerVehicle ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol : it?.t400noPol,
                    t400TglJamReception:(it.t400TglCetakNoAntrian==null)?"":hhmm.format(it.t400TglCetakNoAntrian),
                    t400TglJamSelesaiReception:(it.t400TglJamSelesaiReception==null)?"":hhmm.format(it.t400TglJamSelesaiReception),
                    m404NoLoket:it.loket?.m404NoLoket,
                    t400TglJamOut:(it.t400TglJamOut==null)?"" : hhmm.format(it.t400TglJamOut),

            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def findNoPol(def params,def session){
        def hasil = []
        def gp = GeneralParameter.findByM000StaDelAndCompanyDealer("0",session.userCompanyDealer)
        String noPol = params.kode+" "+params.tengah+" "+params.belakang
        def results = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("fullNoPol",noPol, [ignoreCase : true])
            order("dateCreated","asc")
        }
        if(results.size()>0){
            def mapTemp = new HistoryCustomer()
            def staBooking = "",driverTemp="",pemilikTemp=""
            def tglJam = null
            def maping = MappingCustVehicle.findAllByCustomerVehicle(results?.last()?.customerVehicle,[sort : 'dateCreated',order : 'asc'])
            if(maping){
                for(cari in maping) {
                    mapTemp = HistoryCustomer.findByCustomerAndStaDel(cari?.customer,"0")
                    def pc = "PEMILIK"
                    if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer){
                        pc = cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()
                    }

                    if(!mapTemp?.fullNama==false){
                        if(pc.contains("PEMILIK")){
                            pemilikTemp =  mapTemp?.fullNama
                        }
                    }
                    if(pc.equalsIgnoreCase("driver") || pc?.equalsIgnoreCase("pengguna")){
                        driverTemp = HistoryCustomer.findByCustomerAndStaDel(cari?.customer,"0")?.fullNama
                    }
                }
            }
            def hari = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            def waktuSekarang = df.format(new Date())
            String awal = waktuSekarang+" 00:00"
            String akhir = waktuSekarang+" 24:00"
            Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
            Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)
            Date hariIni  = datatablesUtilService?.syncTime()
            def appointment = Appointment.createCriteria().get {
                eq("companyDealer",params.companyDealer);
                eq("staDel","0")
                eq("staSave","0")
                ge("t301TglJamRencana",dateAwal)
                lt("t301TglJamRencana",dateAkhir)
                historyCustomerVehicle{
                    eq("fullNoPol",noPol, [ignoreCase : true])
                }
                maxResults(1);
            }

            if(appointment){
                staBooking = "Booking"
                tglJam = hari[appointment?.t301TglJamRencana?.getDay()]+" "+appointment?.t301TglJamRencana?.format("dd MMMM yyyy | HH:mm");
                if(datatablesUtilService?.syncTime()?.after(appointment?.t301TglJamRencana)){
                    if(gp?.m000KonfirmasiTerlambat!=null){
                        def jamTemp = appointment?.t301TglJamRencana?.getHours() + gp?.m000KonfirmasiTerlambat?.getHours()
                        def minTemp = appointment?.t301TglJamRencana?.getMinutes() + gp?.m000KonfirmasiTerlambat?.getMinutes()
                        String jam = jamTemp.toString().length()==1 ? "0"+jamTemp.toString() : jamTemp
                        String menit = minTemp?.toString()?.length()==1 ? "0"+minTemp.toString() : minTemp.toString()
                        String waktu = waktuSekarang+" "+jam+":"+menit
                        if(jam.toInteger()>=24){
                            waktu = waktuSekarang+" 24:00"
                        }
                        hariIni  = datatablesUtilService?.syncTime()?.parse("dd/MM/yyyy HH:mm",waktu)
                    }
                    if(datatablesUtilService?.syncTime()?.after(hariIni)){
                        hariIni = datatablesUtilService?.syncTime()
                        staBooking = "Walk In"
                        tglJam = hari[hariIni?.getDay()]+" "+hariIni?.format("dd MMMM yyyy | HH:mm");
                    }
                }
            }else{
                def reception = Reception.findAllByHistoryCustomerVehicleAndStaDelAndStaSave(results?.last(),"0","0");
                if(reception?.size()>0){
                    def gatepass = GatePassT800.findByReceptionAndStaDelAndT800TglJamGatePassBetween(reception?.last(),"0",dateAwal,dateAkhir)
                    if(gatepass){
                        staBooking = "Gate Pass"
                        tglJam = hari[gatepass?.t800TglJamGatePass?.getDay()]+" "+gatepass?.t800TglJamGatePass?.format("dd MMMM yyyy | HH:mm");
                    }else{
                        staBooking = "Walk In"
                        tglJam = hari[hariIni?.getDay()]+" "+hariIni.format("dd MMMM yyyy | HH:mm");
                    }
                }else{
                    staBooking = "Walk In"
                    tglJam = hari[hariIni?.getDay()]+" "+hariIni.format("dd MMMM yyyy | HH:mm");
                }
            }
            def fullMVCTemp = FullModelVinCode.findByFullModelCodeAndStaDel(results?.last()?.fullModelCode,"0")
            hasil << [
                    idCV : results?.last()?.customerVehicle?.id,
                    mobil : results?.last()?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    tahun : fullMVCTemp?.t109ThnBlnPembuatan?.substring(0,4),
                    warna : results?.last()?.warna?.m092NamaWarna,
                    namaCustomer : pemilikTemp,
                    statusBooking : staBooking,
                    tglJam : tglJam,
                    driver : driverTemp
            ]
        }
        else
        {
            def hari = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            def waktuSekarang = df.format(new Date())
            String awal = waktuSekarang+" 00:00"
            String akhir = waktuSekarang+" 24:00"
            Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
            Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)
            Date hariIni = datatablesUtilService?.syncTime()
            def  staBooking = "Walk In"
            def tglJam = hari[hariIni?.getDay()]+" "+hariIni.format("dd MMMM yyyy | HH:mm");
            def no=0
            def findcustomerin =CustomerIn.findAllByDateCreatedBetweenAndStaDel(dateAwal,dateAkhir,"0")

            hasil << [
                    idCV : "" ,
                    mobil : "-",
                    tahun : "-",
                    warna : "-",
                    namaCustomer : "-",
                    statusBooking :staBooking,
                    tglJam :tglJam,
                    driver : "-"
            ]

        }
        return hasil
    }

    def clearLate(ActivityExecution execution) throws Exception {
        if(gormReady && !locked) {
            locked = true
            Date date = new Date()
            def c = JPB.createCriteria()
            def results = c.list () {
                eq("staDel","0")
                ge("t451TglJamPlan",new Date())
                lt("t451TglJamPlan",(new Date().clearTime()+1))
                reception{
                    eq("staDel","0")
                    isNotNull("t401NoAppointment")
                }
            }

            results.each {
                def app = Appointment.findByReceptionAndStaDel(it.reception,"0");
                def gp = GeneralParameter.findByCompanyDealerAndM000StaDel(app?.companyDealer,"0");
                def janji = app?.t301TglJamApp
                if(gp?.m000KonfirmasiTerlambat!=null){
                    def jamTemp = app?.t301TglJamRencana?.getHours() + gp?.m000KonfirmasiTerlambat?.getHours()
                    def minTemp = app?.t301TglJamRencana?.getMinutes() + gp?.m000KonfirmasiTerlambat?.getMinutes()
                    String jam = jamTemp.toString().length()==1 ? "0"+jamTemp.toString() : jamTemp
                    String menit = minTemp?.toString()?.length()==1 ? "0"+minTemp.toString() : minTemp.toString()
                    String waktu = new Date().format("dd/MM/yyyy")+" "+jam+":"+menit
                    if(jam.toInteger()>=24){
                        waktu = new Date().format("dd/MM/yyyy")+" 24:00"
                    }
                    janji = new Date().parse("dd/MM/yyyy HH:mm",waktu)
                }
                if(date.after(janji)){
                    it.staDel = "1"
                    it.save(flush: true)
                }
            }

            locked = false
        }
    }
}


