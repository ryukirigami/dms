package com.kombos.reception

import com.kombos.approval.AfterApprovalInterface
import com.kombos.parts.StatusApproval
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
/**
 * Created by HP_Probook on 10/21/14.
 */
class KurangiJobPartService implements AfterApprovalInterface {
    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        if (staApproval == StatusApproval.APPROVED) {
            //println fks
            try {
                def param = fks.split("#");
                def jobs = param[1].split("-")
                def reception = Reception.get(param[0].toLong())
                jobs.each {
                    if(it.toString().contains("*")){
                        def idP = it.toString().substring(0,it.toString().indexOf("*"))
                        def parts = PartsRCP.get(idP.toLong());
                        parts.staDel = "1"
                        parts.t403TglJamTambahKurang = null
                        parts.t403StaApproveTambahKurang = null
                        parts.t403StaTambahKurang = null
                        parts.setLastUpdProcess("DELETE")
                        parts.save(flush: true)
                    }else{
                        def job = JobRCP.get(it.toLong());
                        job.properties = job.properties
                        job.t402StaTambahKurang = null
                        job.t402StaApproveTambahKurang = null
                        job.t402TglTambahKurang = new Date()
                        job.staDel = "1"
                        job.setLastUpdProcess("DELETE")
                        try {
                            def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(job.reception,job.operation,"0")
                            parts.each {
                                def part = PartsRCP.get(it.id);
                                part.staDel = "1"
                                part.t403TglJamTambahKurang = null
                                part.t403StaApproveTambahKurang = null
                                part.t403StaTambahKurang = null
                                part.save(flush: true)
                                part.setLastUpdProcess("DELETE")
                            }
                        }catch (Exception e){}
                        job.save(flush: true)
                    }
                }
            }catch (Exception e1){

            }

        }else if(staApproval==StatusApproval.REJECTED){
            try {
                def param = fks.split("#");
                def jobs = param[1].split("-")
                def reception = Reception.get(param[0].toLong())
                jobs.each {
                    if(it.toString().contains("*")){
                        def idP = it.toString().substring(0,it.toString().indexOf("*"))
                        def nParts = PartsRCP.get(idP.toLong());
                        nParts.lastUpdProcess = "BATAL KURANGI"
                        nParts.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        nParts.t403TglJamTambahKurang = null
                        nParts.t403StaApproveTambahKurang = null
                        nParts.t403StaTambahKurang = null
                        nParts.save(flush: true)
                    }else{
                        def job = JobRCP.get(it.toLong());
                        job.t402TglTambahKurang = null
                        job.lastUpdProcess = "BATAL KURANGI"
                        job.t402StaApproveTambahKurang = null
                        job.t402StaTambahKurang = null
                        job.save(flush: true)
                    }
                }
            }catch (Exception e1){

            }

        }
    }
}
