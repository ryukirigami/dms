package com.kombos.reception

import com.kombos.approval.AfterApprovalInterface
import com.kombos.board.JPB

/**
 * Created by Ahmad Fawaz on 21/01/15.
 */
import com.kombos.maintable.StatusActual
import com.kombos.parts.StatusApproval
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP

class CancelReceptionService implements AfterApprovalInterface {


    @Override
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        if (staApproval == StatusApproval.APPROVED) {
            def recEdit = Reception.get(fks.toLong())
            def jobs = JobRCP.findAllByReceptionAndStaDel(recEdit,"0");
            jobs.each {
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(recEdit,it.operation,"0");
                parts.each {
                    def actual = Actual.findByReceptionAndOperationAndStaDel(recEdit,it.operation,"0");

                    it.staDel = "1"
                    it.lastUpdProcess = "CANCEL"
                    it.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    it.save(flush: true)
                }
                it.staDel = "1"
                it.lastUpdProcess = "CANCEL"
                it.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                it.save(flush: true)
            }
            def jpbs = JPB.findAllByReceptionAndStaDel(recEdit,"0");
            jpbs.each {
                def actual = Actual.findByNamaManPowerAndStaDel(it.namaManPower,"0");
                if(actual){
                    if(!actual.statusActual.m452StatusActual.equalsIgnoreCase("Clock Off")){
                        actual.lastUpdProcess = "CANCEL"
                        actual.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        def nActual = new Actual()
                        nActual.properties = actual.properties
                        nActual.statusActual = StatusActual.findByM452StatusActualIlikeAndM452StaDel("Clock Off","0");
                        nActual.save(flush: true)
                        actual.save(flush: true)
                    }
                }
                it?.staDel = "1"
                it?.save(flush: true)
                it?.errors?.each {
                    //println "JPB "+it
                }
            }
            recEdit.lastUpdProcess = "CANCEL"
            recEdit.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            recEdit.staDel = '1'
            recEdit.t401StaOkCancelReSchedule = '1'
            recEdit.save(flush: true)
        }

    }

}
