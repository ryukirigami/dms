package com.kombos.reception

import com.google.visualization.datasource.datatable.value.Value
import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.CustomerSurveyDetail
import com.kombos.customerprofile.FA
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.customerprofile.SPK
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.maintable.*
import com.kombos.parts.*
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import com.kombos.woinformation.Prediagnosis
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class AppointmentService {
    boolean transactional = false

    def validasiOrderService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def generateCodeService
    def conversi = new Konversi()
    def datatablesUtilService


    def partDatatablesList(def params,def session) {
        def rows = []
        def size = 0
        if (params."idJobApp") {
            def operation = null
            JobApp jobApp = JobApp.get(params."idJobApp" as Long)
            if (jobApp) {
                def partsApp = PartsApp.findAllByReceptionAndOperationAndT303StaDel(jobApp?.reception, jobApp?.operation,'0')

                size = partsApp.size()

                partsApp.each { PartsApp p ->
                    String staNeedDP = "no"
                    def dpParts = DpParts.createCriteria().get {
                        eq("staDel","0");
                        eq("companyDealer",session.userCompanyDealer);
                        order("m162TglBerlaku","desc");
                        maxResults(1);
                    }

                    if(p.t303HargaRp && dpParts){
                        if(dpParts.m162BatasNilaiKenaDP1.toDouble()<=p.t303HargaRp && dpParts.m162BatasNilaiKenaDP2.toDouble()>=p.t303HargaRp){
                            if(!p.t303DPRpP || p.t303DPRpP==0){
                                staNeedDP="yes"
                            }
                        }
                    }
                    rows << [
                            idPart: p?.id,
                            kodePart: p?.goods?.m111ID,
                            namaPart: p?.goods?.m111Nama,
                            qty: p?.t303Jumlah1,
                            satuan: p?.goods?.satuan?.m118Satuan1,
                            harga:conversi.toRupiah( p?.t303HargaRp ? p?.t303HargaRp : 0),
                            rate: "",
                            statusWarranty: "",
                            staNeedDP: staNeedDP,
//                            nominal: p?.t303Jumlah1 && p?.t303HargaRp ? (p?.t303Jumlah1 * p?.t303HargaRp) : 0,
                            nominal:conversi.toRupiah( p?.t303Jumlah1 && p?.t303HargaRp ? (p?.t303Jumlah1 * p?.t303HargaRp) : 0),
                    ]
                }
            }
        }

        [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: rows]
    }

//ian
    def jobnPartsDatatablesList(def params, def session) {
        def rows = []
        if (params."sAppointmentId") {
            Appointment appointment = Appointment.get(params."sAppointmentId" as Long)
            def totalNominal = 0
            if (appointment) {
                if (appointment.historyCustomerVehicle == null) {
                    if (params."sCriteria_kodeKota" && params."sCriteria_nomorTengah" && params."sCriteria_nomorBelakang") {
                        def chcv = HistoryCustomerVehicle.createCriteria()
                        HistoryCustomerVehicle hcv = chcv.get {
                            kodeKotaNoPol {
                                eq('m116ID', params."sCriteria_kodeKota",[ignoreCase: true])
                            }
                            eq('t183NoPolTengah', params."sCriteria_nomorTengah",[ignoreCase: true])
                            eq('t183NoPolBelakang', params."sCriteria_nomorBelakang", [ignoreCase: true])
                            maxResults(1);
                        }
                        appointment.historyCustomerVehicle = hcv
                        appointment.errors.each {
                            //println "Appointment . . . " + it
                        }
                    }
                }

//
                def jobRInput = JobApp.findByOperationAndT302StaDel(params.operation,"0");
                if(jobRInput){
                    jobRInput.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    jobRInput.lastUpdProcess = "UPDATE"
                    jobRInput?.dateCreated  = datatablesUtilService?.syncTime()
                    jobRInput?.lastUpdated  = datatablesUtilService?.syncTime()
                }else {
                    jobRInput = new JobApp()
                    jobRInput.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    jobRInput.lastUpdProcess = "INSERT"
                    jobRInput?.dateCreated  = datatablesUtilService?.syncTime()
                    jobRInput?.lastUpdated  = datatablesUtilService?.syncTime()
                }

//


                def jobApps = JobApp.findAllByReceptionAndT302StaDel(appointment.reception,"0")
                def totRate = 0
                def totTarif = 0
                def discJasa = 0
                jobApps.each { JobApp job ->
                    def rate = 0
                    def tarif = 0
                    def nominalValue = 0

                    def staDiskon = false
                    def partsApp = PartsApp.findAllByReceptionAndOperationAndT303StaDel(job.reception, job.operation, "0")
                    if (job.operation && appointment.historyCustomerVehicle?.fullModelCode?.baseModel) {
                        FlatRate r = FlatRate.findByOperationAndStaDelAndBaseModelAndT113TMTLessThan(job?.operation,'0',appointment?.historyCustomerVehicle?.fullModelCode?.baseModel, new Date())
                        if (r){
                            rate = r.t113FlatRate
                            totRate += r.t113FlatRate
                        }
                        def hrga = TarifPerJam.findByKategoriAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(appointment?.historyCustomerVehicle?.fullModelCode?.baseModel?.kategoriKendaraan,"0",params?.companyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
                        def nominal1 = hrga && r ? hrga?.t152TarifPerjamBP * r?.t113FlatRate : 0
                        def nominal2 = hrga && r ? hrga?.t152TarifPerjamSB * r?.t113FlatRate : 0
                        def nominal3 = hrga && r ? hrga?.t152TarifPerjamGR * r?.t113FlatRate : 0
                        def nominalTam = hrga && rate ? hrga?.t152TarifPerjamDealer * r?.t113FlatRate : 0
                        def nominal5 = hrga?.t152TarifPerjamSBI && r ? hrga?.t152TarifPerjamSBI * r?.t113FlatRate : 0
                        if(job?.operation?.kategoriJob?.m055KategoriJob?.contains("BP")){
                            nominalValue = nominal1
                            totalNominal+=nominal1
                        }else if(job?.operation?.kategoriJob?.m055KategoriJob?.contains("SBE")){
                            nominalValue = nominal2
                            String jobTam = "SB10K,SB20K,SB30K,SB40K,SB50K"
                            if(jobTam.toString().contains(job?.operation.m053Id)){
                                nominalValue = nominalTam
                                nominal2 = nominalTam
                            }
                            totalNominal+=nominal2
                        }else if(job?.operation?.kategoriJob?.m055KategoriJob?.contains("SBI")){
                            nominalValue = nominal5
                            totalNominal+=nominal5
                        }else{job?.t302HargaRp = nominalValue
                            if(job?.t302DiscPersen){
                                if(job?.t302DiscRp){
                                    if(job?.t302DiscRp!=(nominalValue * job.t302DiscPersen / 100)){
                                        staDiskon=true
                                        job.t302DiscRp = nominalValue * job.t302DiscPersen / 100
                                        discJasa+=job.t302DiscRp
                                    }
                                }else{
                                    staDiskon=true
                                    job.t302DiscRp = nominalValue * job.t302DiscPersen / 100
                                    discJasa+=job.t302DiscRp
                                }
                                job?.t302TotalRp = nominalValue - job.t302DiscRp
                            }else{
                                job?.t302TotalRp = nominalValue
                            }
                            nominalValue = nominal3
                            totalNominal+=nominal3
                        }

//
                        //println('inilah '+nominalValue)


                        if(job?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("BP")){
                            tarif = hrga ? hrga?.t152TarifPerjamBP * rate : 0
                        }else if(job?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SB")){
                            tarif = hrga ? hrga?.t152TarifPerjamSB * rate : 0
                        }else if(job?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SBI")){
                            tarif = hrga ? hrga?.t152TarifPerjamSBI * rate : 0
                        }else if(job?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("GR")){
                            tarif = hrga ? hrga?.t152TarifPerjamGR * rate : 0
                        }

                        totTarif += tarif
                        partsApp.each {
                            GoodsHargaJual ghj
                            if (it?.goods && it?.goods?.satuan)
                                ghj = GoodsHargaJual.findByGoodsAndStaDel(it?.goods, "0")
                            def nom = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                            def tot = it?.t303Jumlah1 ? it?.t303Jumlah1 * nom : 0
                            totTarif += tot
                        }

                    }

                    rows << [

                            idJob: job.operation.id,

                            idJobApp: job.id,

                            namaJob: job.operation?.m053NamaOperation,//'PENGGANTIAN PINTU DEPAN KIRI',

                            rate: rate,

                            staPart: partsApp.size() > 0 ? "ada" : "kosong",

                            statusWarranty: job.statusWarranty?.m058NamaStatusWarranty,

                            nominal: conversi.toRupiah(nominalValue),

                            totalRate : totRate,

                            totalNominal : conversi.toRupiah(totTarif)


                    ]

                }
                [sEcho: params.sEcho, iTotalRecords: jobApps.size(), iTotalDisplayRecords: jobApps.size(), aaData: rows]
            } else {
                [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]
            }
        } else {
            [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]
        }
    }

//ian



    def addPartDatatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_goods") {
                ilike("m111ID", "%" + (params."sCriteria_goods" as String) + "%")
            }

            if (params."sCriteria_goods2") {
                ilike("m111Nama", "%" + (params."sCriteria_goods2" as String) + "%")
            }

            // ilike("statusParts", "1")
//            switch (sortProperty) {
//                default:
//                    order(sortProperty, sortDir)
//                    break;
//            }
        }

        def rows = []
        def nos = 0
        results.each {
            nos = nos + 1
            def ghj = GoodsHargaJual.findByGoodsAndStaDel(it, "0")
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN
            //it.goods?.goodsHargaJual?.t151HargaTanpaPPN
            rows << [

                    id: it.id,

                    goods: it.m111ID,

                    goods2: it.m111Nama,
                    satuan: it.satuan?.m118Satuan1,
                    totalHarga:conversi.toRupiah(hargaTanpaPPN)

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]


    }


    def historyServiceDatatablesList(def params) {
        if (params.idVincode) {
            def c = Reception.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("staDel","0")
                eq("staSave","0")
                eq("t401StaInvoice","0")
                historyCustomerVehicle{
                    customerVehicle{
                        eq("t103VinCode",params.idVincode,[ignoreCase: true])
                    }
                }
                order("dateCreated","desc")
            }

            def rows = []
            results.each {
                def a = 0
                def jobRCP = JobRCP.findAllByReceptionAndStaDel(it,"0")
                String jobs = ""
                String parts = ""
                jobRCP.each {
                    a++
                    jobs += "* "+ it?.operation?.m053NamaOperation
                    if(a!=jobRCP?.size()){
                        jobs+="<br/> "
                    }
                }

                a = 0
                def partRCP = PartsRCP.createCriteria().list {
                    eq("staDel","0")
                    eq("reception", it)
                    or{
                        ilike("t403StaTambahKurang","%0%")
                        and{
                            not{
                                ilike("t403StaTambahKurang","%1%")
                                ilike("t403StaApproveTambahKurang","%1%")
                            }
                        }
                        and{
                            isNull("t403StaTambahKurang")
                            isNull("t403StaApproveTambahKurang")
                        }
                    }
                }

                partRCP.each {
                    a++
                    parts += "* "+it?.goods?.m111Nama
                    if(a!=partRCP?.size()){
                        parts +="<br/> "
                    }
                }

                def tglReception = it?.dateCreated
                def suggest = JobSuggestion.findByCustomerVehicleAndDateCreatedBetweenAndStaDel(it?.historyCustomerVehicle?.customerVehicle,tglReception?.clearTime(),tglReception+1,"0")
                rows << [
                        tanggal: it?.t401TglJamCetakWO ? it?.t401TglJamCetakWO?.format("dd MMMM yyyy") : it?.dateCreated?.format("dd MMMM yyyy"),

                        job: jobs,

                        parts: parts ? parts:"-",

                        jobSuggest: suggest?.t503KetJobSuggest ? suggest?.t503KetJobSuggest:"-"
                ]
            }

            [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }
    }

    def keluhanDatatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def rows = []

        def c = KeluhanRcp.createCriteria()
        def results = c.list() {


        }
        def nos = 0
        results.each { KeluhanRcp keluhanRcp ->
            nos = nos + 1
            rows << [
                    noUrut: nos,
                    id: keluhanRcp.id,
                    keluhan: keluhanRcp.t411NamaKeluhan,
                    butuhDiagnose: keluhanRcp.t411StaButuhDiagnose
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def addJobDatatablesList(def params) {

        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def rows = []
        def c = Operation.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
//            if(params."sCriteria_kategoriJob"){
//                kategoriJob{
//                    eq('m055KategoriJob', params."sCriteria_kategoriJob")
//                }
//            }
            if (params."sCriteria_kodeJob") {
                ilike('m053JobsId', '%' + params."sCriteria_kodeJob" + '%')
            }
            if (params."sCriteria_namaJob") {
                ilike('m053NamaOperation', '%' + params."sCriteria_namaJob" + '%')
            }
        }
        results.each { Operation op ->

            FlatRate r = FlatRate.findByOperationAndStaDel(op,"")
            def rate = r.t113FlatRate

            rows << [
                    id: op.id,
//                    i_km : op.m053Km,
                    kodeJob: op.m053JobsId,
                    namaJob: op.m053NamaOperation,
                    rate: rate,
                    noKeluhan: ""
            ]
        }
//		def tot =10
//		for(i in 1..tot){
//		rows << [
//
//						kodeJob: "OBGHJY",
//						namaJob: "CLUTCH DISC REPLACE",
//						rate: "1.5",
//						noKeluhan: ""
//			]
//		}

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

//    def show(params) {
//        def result = [:]
//        def fail = { Map m ->
//            result.error = [code: m.code, args: ["Appointment", params.id]]
//            return result
//        }
//
//        result.appointmentInstance = Appointment.get(params.id)
//
//        if (!result.appointmentInstance)
//            return fail(code: "default.not.found.message")
//
//        // Success.
//        return result
//    }

    def showAppointment(params) {
        def kodeKota = params.kodeKota
        def nomorTengah = params.nomorTengah
        def nomorBelakang = params.nomorBelakang

        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Appointment", params.id]]
            return result
        }

        if (!kodeKota && !nomorTengah && !nomorBelakang)
            return fail(code: "default.not.found.message")

        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("fullNoPol",kodeKota.trim()+" "+nomorTengah.trim()+" "+nomorBelakang.trim(),[ignoreCase: true])
        }

        if(histCV.size()>0){
            def date = new Date()
            def c = CustomerSurveyDetail.createCriteria()
            CustomerSurveyDetail csDetail = c.get() {
                customerSurvey {
                    le("t104TglAwal", date)
                    ge("t104TglAkhir", date - 1)
                }
                customerVehicle {
                    histories {
                        eq("fullNoPol",kodeKota.trim()+" "+nomorTengah.trim()+" "+nomorBelakang.trim(),[ignoreCase: true])
                    }
                }
            }
            def jenisSurvey = csDetail?.customerSurvey?.jenisSurvey?.m131JenisSurvey
            if (jenisSurvey) {
                result."${jenisSurvey}" = csDetail?.customerSurvey?.t104Text
            }

            // TODO cari nama FA dari VehicleFA

            def custs = []

            def cariPakai = false,cariDriver = false;
            def mapping = MappingCustVehicle.createCriteria().list {
                eq("customerVehicle",histCV?.last()?.customerVehicle);
                order("dateCreated","desc");
            }
            for(cari in mapping){
                if((cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK DAN PENANGGUNG JAWAB')) && !cariPakai){
                    HistoryCustomer hc = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                    cariPakai=true;
                    result."namaPemilik" = hc?.fullNama
                    if (result."alamat" == null)
                        result."alamat" = hc?.t182Alamat
                    if (result."telp" == null)
                        result."telp" = hc?.t182NoHp
                    result."hc" = hc?.id
                }
                if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('DRIVER') && !cariDriver){
                    HistoryCustomer hc = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                    result."namaDriver" = hc?.fullNama
                    cariDriver=true;
                }
                if(cariPakai && cariDriver){
                    break
                }
            }

            def z = Appointment.createCriteria()
            def results = z.list {
                historyCustomerVehicle {
                    kodeKotaNoPol {
                        ilike('m116ID', kodeKota)
                    }
                    eq('t183NoPolTengah', nomorTengah)
                    ilike('t183NoPolBelakang', nomorBelakang)
                }
                eq("t301StaOkCancelReSchedule", "0")
                order("t301TglJamRencana", "desc")
            }
            if (results.size() > 0) {
                Appointment app = results.first();
                result."appointmentId" = app.id
                result."tanggalAppointment" = app.t301TglJamApp
                result."noAppointment" = app.reception?.t401NoAppointment
                result."idCV" = csDetail?.customerVehicle?.t103VinCode
                def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(histCV?.last()?.customerVehicle,"0");
                result."jobSuggest" = jobSuggest?.t503KetJobSuggest
            }
            result."status" = "ok"
        }else{
            result."status" = "notfound"
        }
        return result
    }

    def saveApp(params) {
        def kodeKota = params.kodeKota
        def nomorTengah = params.nomorTengah
        def nomorBelakang = params.nomorBelakang
        def appointmentId = params.appointmentId

        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Appointment", params.id]]
            return result
        }

        if (!kodeKota && !nomorTengah)
//        && !nomorBelakang
            return fail(code: "default.not.found.message")
        def date = new Date()
        def kendaraan = HistoryCustomerVehicle.createCriteria().get {
            kodeKotaNoPol {
                eq('m116ID', kodeKota , [ignoreCase: true])
            }
            eq('t183NoPolTengah', nomorTengah,[ignoreCase: true])
            eq('t183NoPolBelakang', nomorBelakang,[ignoreCase: true])
            maxResults(1)
        }

        HistoryCustomerVehicle hcv = Appointment?.get(appointmentId)?.historyCustomerVehicle

        def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(hcv?.customerVehicle,"0")
        if(jobSuggest){
            def arrJob = jobSuggest?.t503KetJobSuggest?.split(",");
            def jobsTaken = params.jobTaken?.split(",");
            if(arrJob?.size()>0 && jobsTaken?.size()>0){
                for(int a=0;a<arrJob?.size();a++){
                    for(int b=0;b<jobsTaken?.size();b++){
                        if(arrJob[a].trim().replace(" ","")?.equalsIgnoreCase(jobsTaken[b].trim().replace(" ",""))){
                            String t503KetJobSuggest = jobSuggest?.t503KetJobSuggest?.replace(arrJob[a],"")
                            if(t503KetJobSuggest=="" || t503KetJobSuggest?.replace(',','')==""){
                                jobSuggest.staDel = "1"
                                jobSuggest.save(flush: true);
                            }else{
                                def jobSNew = new JobSuggestion()
                                jobSNew.properties = jobSuggest.properties
                                if(t503KetJobSuggest.indexOf(",")==0){
                                    jobSNew.t503KetJobSuggest = t503KetJobSuggest.substring(1)
                                }else{
                                    jobSNew.t503KetJobSuggest = t503KetJobSuggest
                                }
                                jobSNew.dateCreated = datatablesUtilService.syncTime()
                                jobSNew.lastUpdated = datatablesUtilService.syncTime()
                                jobSNew.save(flush: true)
                                jobSuggest.staDel = "1"
                                jobSuggest.save(flush: true);
                            }
                        }
                    }
                }
            }
        }

        def mapCV = MappingCustVehicle.createCriteria().list {
            eq("customerVehicle",kendaraan?.customerVehicle);
            order("dateCreated","desc")
        }
        def mcv = null
        if(mapCV){
            for(cari in mapCV){
                if (cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK DAN PENANGGUNG JAWAB') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK')) {
                    if(cari?.customer?.histories){
                        mcv = cari?.customer?.histories?.last()
                        break;
                    }
                }
            }
        }

        def app = Appointment.get(appointmentId)
        if(params.idCustLamaWeb){
            def custLamaWeb = CustLamaWeb.get(params.idCustLamaWeb.toLong())
            custLamaWeb?.t188EstimasiBiataService = params?.estTotal?.toDouble()
        }

        if (app) {
            if (mcv) {
                app.customer = mcv.customer;
                app.historyCustomer = mcv;
            }

            app.customerVehicle = hcv.customerVehicle.last();
            app.historyCustomerVehicle = hcv.customerVehicle.getCurrentCondition()
            app.t301TglJamApp = date
            app.lastUpdated = datatablesUtilService.syncTime()
            app.saveFlag = "0"
            app.staSave = "0"
            app.jamApp = new Date().format("HH").toInteger()
            app.menitApp = new Date().format("mm").toInteger()
            app.t301StaTHS = params.ths
            app.t301NamaSA = org.apache.shiro.SecurityUtils.subject.principal.toString()
            def jenisApp = JenisApp.findById(params.jenisApp)
            if(!jenisApp){
                jenisApp = new JenisApp()
                jenisApp.id = params.jenisApp.toLong()
                jenisApp.m301Id = params.jenisApp
                jenisApp.m301NamaJenisApp = params.jenisApp && params.jenisApp.toString()=="1" ? "GR" : "BP"
                jenisApp.m301StaDel = "0"
                jenisApp.createdBy = "SYSTEM"
                jenisApp.lastUpdProcess = "INSERT"
                jenisApp.dateCreated  = datatablesUtilService.syncTime()
                jenisApp.lastUpdated  = datatablesUtilService.syncTime()
                jenisApp.save(flush: true)
            }
            app.jenisApp = jenisApp
            app.lastUpdated = datatablesUtilService.syncTime()
            if(params.alamatTHS){
                app.t301AlamatTHS = params.alamatTHS
            }
            app.save(flush: true)
            app.errors.each {
                //println "Appointment Edit . . ." + it
            }
            result."status" = "ok"
        } else {
            result."status" = "nok"
        }

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Appointment", params.id]]
            return result
        }

        result.appointmentInstance = Appointment.get(params.id)

        if (!result.appointmentInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Appointment", params.id]]
            return result
        }

        result.appointmentInstance = Appointment.get(params.id)

        if (!result.appointmentInstance)
            return fail(code: "default.not.found.message")

        try {
            result.appointmentInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def create(params) {
//        def apps = Appointment.createCriteria().list {
//            eq("staSave","1")
//            eq("companyDealer",params.companyDealer)
//            ge("dateCreated",new Date().clearTime()-1)
//            lt("dateCreated",new Date().clearTime())
//        }
//
//        apps.each {
//            def recepTemp = it.reception
//            def jobApps = JobApp.findAllByReception(it.reception)
////            try {
//
//            jobApps.each {
//                def partApp = PartsApp.findAllByReceptionAndOperation(recepTemp,it.operation)
//                if(partApp)
//                    partApp*.delete()
//
//                it.delete()
//            }
//            it.delete()
//            recepTemp.delete()
////            }
////            catch (org.springframework.dao.DataIntegrityViolationException e) {
//////                return fail(code: "default.not.deleted.message")
////            }
//        }
    }
    def savePart(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.appointmentInstance && m.field)
                result.appointmentInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Appointment", params.id]]
            return result
        }

        Appointment appointmentInstance = Appointment.get(params.appointmentId as Long)
        result.appointmentInstance = appointmentInstance

        Reception reception = appointmentInstance.reception
        def maks = params.countPart.toLong()
        for (int it = 1; it < maks; it++) {
            Goods goods = Goods.get(params."part_${it}" as Long)
            NamaProsesBP prosesBP = NamaProsesBP.get(params."prosesBP_${it}" as Long)
            def job = Operation.get(params.idJob as Long)
            if(!job){
                job = JobApp.get(params.idJob as Long)?.operation
            }
            PartsApp pa = PartsApp.findByReceptionAndOperationAndGoodsAndT303StaDel(reception, job, goods, "0");
            if (pa == null) {
                pa = new PartsApp()
                pa.dateCreated = datatablesUtilService.syncTime()
            }
            pa.lastUpdated = datatablesUtilService.syncTime()
            pa.setGoods(goods)
            pa.setOperation(job)
            pa.reception = reception
            pa.namaProsesBP = prosesBP
            pa.t303Jumlah1 = params."qty_${it}" as Long
            pa.t303StaDel = "0"

            def totalhargaPart = 0
            def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
            def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                eq("staDel","0");
                eq("companyDealer",appointmentInstance?.companyDealer);
            }
            def mappingRegion = MappingPartRegion.createCriteria().get {
                if(mappingCompRegion?.size()>0){
                    inList("region",mappingCompRegion?.region)
                }else{
                    eq("id",-10000.toLong())
                }
                maxResults(1);
            }
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
            if(mappingRegion){
                try {
                    if(KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("TOYOTA") ||
                            KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("OLI")){
                            totalhargaPart = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                    }
                }catch (Exception e){

                }
            }
            pa.t303HargaRp = totalhargaPart
            pa.t303TotalRp = (totalhargaPart * pa.t303Jumlah1)
            pa.save(flush: true)
            pa.errors.each {
                //println "Parts App "+it
            }
            JobApp ja = JobApp.findByReceptionAndOperationAndT302StaDel(reception, job, "0")

            def tarif = 0
            def rate = 0
            FlatRate r = FlatRate.findByOperationAndStaDelAndBaseModelAndT113TMTLessThan(job,'0',appointmentInstance?.historyCustomerVehicle?.fullModelCode?.baseModel,new Date(),[sort: 'id',order: 'desc'])
            if (r){
                rate = r.t113FlatRate
            }

            def hrga = TarifPerJam.findByBaseModelAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(appointmentInstance?.historyCustomerVehicle?.fullModelCode?.baseModel,"0",params?.userCompanyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
            if(job?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("BP")){
                tarif = hrga ? hrga?.t152TarifPerjamBP * rate : 0
            }else if(job?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SB")){
                tarif = hrga ? hrga?.t152TarifPerjamSB * rate : 0
            }else if(job?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SBI")){
                tarif = hrga ? hrga?.t152TarifPerjamSBI * rate : 0
            }else if(job?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("GR")){
                tarif = hrga ? hrga?.t152TarifPerjamGR * rate : 0
            }
            ja.t302HargaRp = tarif
            ja.t302TotalRp = tarif
            ja.lastUpdated = datatablesUtilService.syncTime()
            ja.save(flush: true)
            ja.errors.each {
                //println it
            }


        }

        if (result.appointmentInstance.hasErrors() || !result.appointmentInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        result."status" = "ok"
        // success
        return result
    }

    def saveJobKeluhan(params) {
        def result = [:]
        def user = User.findByUsernameAndStaDel(org.apache.shiro.SecurityUtils.subject.principal.toString(),"0");
        result.petugas = user.fullname
        def fail = { Map m ->
            if (result.appointmentInstance && m.field)
                result.appointmentInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Appointment", params.id]]
            return result
        }

        Appointment appointmentInstance = Appointment.get(params.appointmentId as Long)
        appointmentInstance.staSave = "0"
        appointmentInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        appointmentInstance.save(flush: true)
        result.appointmentInstance = appointmentInstance

        Reception reception = appointmentInstance.reception

        def jsonArray = JSON.parse(params.keluhan_ids)

        jsonArray.each {
            KeluhanRcp keluhanRcp = new KeluhanRcp()
            keluhanRcp.reception = reception
            keluhanRcp.t411NoUrut = it as int
            keluhanRcp.t411NamaKeluhan = params."keluhan_${it}"
            keluhanRcp.t411StaButuhDiagnose = params."butuhDiagnose_${it}"
            keluhanRcp.dateCreated = datatablesUtilService?.syncTime()
            keluhanRcp.lastUpdated = datatablesUtilService?.syncTime()
            keluhanRcp.save(flush: true)
            keluhanRcp.errors.each {
                //println it
            }
        }

        for (int it = 1; it < params.countRowJob.toLong(); it++) {
            JobApp ja = JobApp.findByReceptionAndOperationAndT302StaDel(reception, Operation.get(params."operation${it}" as Long), "0")
            if (ja == null) {
                ja = new JobApp()
                ja.dateCreated = datatablesUtilService?.syncTime()
            }
            ja.reception = reception
            Operation operation = Operation.get(params."operation_${it}" as Long)
            ja.operation = operation
            ja.statusWarranty = StatusWarranty.get(params."statusWarranty_${it}" as Long)
            ja.t302NoKeluhan = params."noKeluhan_${it}"
            ja.t302StaDel = "0"
            ja.lastUpdated = datatablesUtilService?.syncTime()


            def totalhargaPart = 0
            def pjs = PartJob.findAllByOperation(operation)
            pjs.each { PartJob pj ->
                PartsApp pa = new PartsApp()
                pa.setGoods(pj.goods)
                pa.setOperation(pj.operation)
                pa.reception = reception
                pa.namaProsesBP = pj.namaProsesBP
                pa.t303Jumlah1 = pj.t112Jumlah
                pa.t303StaDel = "0"
                pa.dateCreated = datatablesUtilService.syncTime()
                pa.lastUpdated = datatablesUtilService.syncTime()
                pa.save(flush: true)
                pa.errors.each {
                    //println it
                }

                def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(pa.goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                    eq("staDel","0");
                    eq("companyDealer",appointmentInstance?.companyDealer);
                }
                def mappingRegion = MappingPartRegion.createCriteria().get {
                    if(mappingCompRegion?.size()>0){
                        inList("region",mappingCompRegion?.region)
                    }else{
                        eq("id",-10000.toLong())
                    }
                    maxResults(1);
                }
                def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                if(mappingRegion){
                    try {
                        if(KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("TOYOTA") ||
                                KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("OLI")){
                            totalhargaPart = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                        }
                    }catch (Exception e){

                    }
                }
            }
            FlatRate r = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(operation, appointmentInstance.historyCustomerVehicle.fullModelCode.baseModel,"0",new Date(),[sort: 'id',order: 'desc'])
            def rate = r.t113FlatRate
            def hargaJob = rate // TODO cari harganya gimana?
            ja.t302HargaRp = hargaJob
            ja.t302TotalRp = hargaJob + totalhargaPart
            ja.save(flush: true)
            ja.errors.each {
                //println it
            }
        }


        if (result.appointmentInstance.hasErrors() || !result.appointmentInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        result."status" = "ok"
        // success
        return result
    }

    def orderPart(def params, CompanyDealer companyDealer) {
        Appointment appointmentInstance = Appointment.get(params.appointmentId as Long)

        Reception reception = appointmentInstance.reception

        RPP rpp=null;
        def rppc = RPP.createCriteria()
        def resRPP = rppc.list {
            eq("companyDealer", companyDealer)
            order("m161TglBerlaku", "desc")
        }

        if (resRPP.size() > 0) {
            rpp = resRPP.get(0)
        }

        DpParts dpParts = null;
        def dpc = DpParts.createCriteria()
        def resDpParts = dpc.list {
            eq("companyDealer", companyDealer)
            order("m162TglBerlaku", "desc")
        }

        if (resDpParts.size() > 0) {
            dpParts = resDpParts.get(0)
        }

        def paList = JSON.parse(params.paIds)
        if (paList && appointmentInstance) {
            def req = Request.findByT162NoReffAndStaDel(reception?.t401NoAppointment,"0");
            if(!req){
                req = new Request()
                req.dateCreated = datatablesUtilService.syncTime()
                req.companyDealer = appointmentInstance?.companyDealer
            }
            req.t162TglRequest = new Date()
            req.t162NoReff = reception.t401NoAppointment
            req.t162NamaPemohon = org.apache.shiro.SecurityUtils.subject.principal.toString()
            req.t162xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
            req.t162xNamaDivisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())?.divisi?.m012NamaDivisi
            req.dateCreated = datatablesUtilService.syncTime()
            req.lastUpdated = datatablesUtilService.syncTime()
            req.save(flush: true)
            req.errors.each {
                //println it
            }
            paList.each {
                PartsApp pa = PartsApp.get(it as Long)
                RequestDetail reqDetail = new RequestDetail()
                reqDetail.status = RequestStatus.BELUM_VALIDASI
                reqDetail.goods = pa.goods
                reqDetail.t162Qty1 = pa.t303Jumlah1
                reqDetail.t162Qty2 = pa.t303Jumlah1
                reqDetail.t162Qty1Available = pa.goods?.partsStok?.size()>0 ? pa.goods?.partsStok?.last()?.t131Qty1 : 0
                reqDetail.t162Qty2Available = pa.goods?.partsStok?.size()>0 ? pa.goods?.partsStok?.last()?.t131Qty2 : 0
                reqDetail.request = req
                reqDetail.dateCreated = datatablesUtilService.syncTime()
                reqDetail.lastUpdated = datatablesUtilService.syncTime()
                reqDetail.save(flush: true)
                reqDetail.errors.each {
                    //println it
                }
                def hargaTanpaPPN = 0
                if (pa.goods && pa.goods.satuan) {
                    def ghj = GoodsHargaJual.findByGoodsAndStaDel(pa.goods,"0")
                    hargaTanpaPPN = ghj?.t151HargaTanpaPPN
                    //it.goods?.goodsHargaJual?.t151HargaTanpaPPN
                }
                ValidasiOrder vo = reqDetail.validasiOrder
                if (!vo) {
                    vo = validasiOrderService.createValidasiOrder(reqDetail)
                    def dp = 0
                    if (dpParts) {
                        vo.t163DP = (dpParts.m162PersenDP / 100) * hargaTanpaPPN

                    }
                    List ptos = PartTipeOrder.findAllByM112TipeOrder(params."${'tipeOrder_' + it}")
                    if (ptos.size() > 0) {
                        PartTipeOrder pto = ptos.get(0)
                        vo.partTipeOrder = pto
                        Date date = new Date()
                        date.setHours(date.getHours() + pto.m112JamLamaDatang1)
                        vo.t163ETA = date

                    }
                    vo.t163HargaSatuan = hargaTanpaPPN
                    vo.t163MasaPengajuanRPP = rpp?.m161MasaPengajuanRPP
//                    vo.rpp = (int)Math.round(rpp.m161MaxDapatRPP)
                    vo.save(flush: true)
                }
            }
            if (req.hasErrors() || !req.save(flush: true)) {
                //println("Error creating request")
            }
        }
        def res = [:]
        res.status = "ok"
        res
    }

    //test
    def requestPartDatatablesList(def params, CompanyDealer companyDealer) {
        def kodeKota = params.kodeKota
        def nomorTengah = params.nomorTengah
        def nomorBelakang = params.nomorBelakang

        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        Appointment appointmentInstance = Appointment.get(params.appointmentId as Long)

        Reception reception = appointmentInstance.reception

        RPP rpp;
        def rppc = RPP.createCriteria()
        def resRPP = rppc.list {
            eq("companyDealer", companyDealer)
            order("m161TglBerlaku", "desc")
        }

        if (resRPP.size() > 0) {
            rpp = resRPP.get(0)
        }


        def c = PartsApp.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t303StaDel","0")
            eq("reception", reception)
            goods {
                order("m111ID", "asc")
            }
        }


        def rows = []
        results.each { PartsApp pa ->
            def goods = pa.goods
            if (goods) {
                def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                    eq("staDel","0");
                    eq("companyDealer",pa?.companyDealer);
                }
                def mappingRegion = MappingPartRegion.createCriteria().get {
                    if(mappingCompRegion?.size()>0){
                        inList("region",mappingCompRegion?.region)
                    }else{
                        eq("id",-10000.toLong())
                    }
                    maxResults(1);
                }
                def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                if(mappingRegion){
                    try {
                        if(KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("TOYOTA") ||
                                KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("OLI")){
                            hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                        }
                    }catch (Exception e){

                    }
                }

                def dpParts = DpParts.findByCompanyDealerAndM162BatasNilaiKenaDP1GreaterThanEqualsAndM162BatasNilaiKenaDP2LessThanEqualsAndM162TglBerlakuLessThanEquals(companyDealer, hargaTanpaPPN, hargaTanpaPPN, new Date())
                def qty = pa.t303Jumlah1
                def satuan = goods?.satuan?.m118Satuan1
                def availabilityQty = goods?.partsStok.size()>0 ?goods?.partsStok?.last()?.t131Qty1Free : "0" as int
                def orderQty = goods?.partsStok?.size()>0 ? goods?.partsStok?.last()?.t131Qty1Reserved : "0" as int
                def dp = 0
                if (dpParts) {
                    dp = dpParts && hargaTanpaPPN ? (dpParts.m162PersenDP / 100) * hargaTanpaPPN : 0
                }
                rows << [
                        id: pa.id,
                        status: "",//reqDetail.status?.toString(),
                        validated: "0",
                        kodePart: goods?.m111ID ?: "",
                        namaPart: goods?.m111Nama ?: "",
                        requestQty: qty,
                        requestSatuan: satuan,
                        availabilityQty: availabilityQty,
                        availabilitySatuan: satuan,
                        orderQty: orderQty,
                        orderSatuan: satuan,
                        eta: "",
                        tipeOrder: "",
                        rpp: rpp?.m161MaxDapatRPP,
                        dp: conversi.toRupiah(dp),
                        staDp: dpParts ? "Y" : "N",
                        harga:conversi.toRupiah( hargaTanpaPPN),
                        total:conversi.toRupiah ((hargaTanpaPPN) ? hargaTanpaPPN * qty : "0" as int)
                ]
            }
        }



        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def cancelEditBookingFeeDatatablesList(def params, CompanyDealer companyDealer) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        Appointment appointmentInstance = Appointment.get(params.appointmentId as Long)

        Reception reception = appointmentInstance.reception

        def partsapps = [:]
        def reqs = [:]
        def dpc = DpParts.createCriteria()
        def dpParts = dpc.get {
            eq("companyDealer", companyDealer)
            eq("staDel","0")
            order("m162TglBerlaku", "desc")
            maxResults(1);
        }

        def partApp = PartsApp.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t303StaDel","0")
            eq("reception",reception)
        }


        def rows = []
        partApp.each { PartsApp pa ->
            boolean wajibDp = false;
            def goods = pa.goods
            if (goods) {
                def hargaTanpaPPN = 0
                if (pa?.t303HargaRp) {
                    hargaTanpaPPN = pa?.t303HargaRp
                }
                def requestQty = pa.t303Jumlah1 ?pa.t303Jumlah1: 0
                def requestQty2 = pa.t303Jumlah1P ?pa.t303Jumlah1P: requestQty
                def qty = pa.t303Jumlah1
                def satuan = goods?.satuan?.m118Satuan1
                def availabilityQty = goods?.partsStok?.size()>0 ?goods?.partsStok?.last()?.t131Qty1Free: "0" as int
                def orderQty = goods?.partsStok?.size()>0 ?goods?.partsStok?.last()?.t131Qty1Reserved: "0" as int
                def dp = 0
                if (dpParts) {
                    dp = (dpParts.m162PersenDP / 100) * hargaTanpaPPN
                }

                def dp2 = pa.t303DPRpP ?pa.t303DPRpP: 0
                def total2 = hargaTanpaPPN * requestQty2
                if(dpParts){
                    if(dpParts.m162BatasNilaiKenaDP1 <= pa?.t303HargaRp && pa?.t303HargaRp <= dpParts?.m162BatasNilaiKenaDP2){
                        wajibDp = true;
                    }
                }
//                if(wajibDp){
                rows << [
                        id: pa.id,
                        kodePart: goods?.m111ID ?: "",
                        namaPart: goods?.m111Nama ?: "",
                        requestQty: requestQty,
                        requestSatuan: satuan,
                        dp: conversi.toRupiah(dp),
                        harga: conversi.toRupiah(hargaTanpaPPN),
                        total: conversi.toRupiah(hargaTanpaPPN * requestQty),
                        requestQty2: requestQty2,
                        requestSatuan2: satuan,
                        dp2: conversi.toRupiah(dp2),
                        harga2: conversi.toRupiah(hargaTanpaPPN),
                        total2: conversi.toRupiah(total2),
                        keterangan: pa.t303xKet ?: "",
                        req: (dpParts == null) ? "0" : 1

                ]
//                }
            }
        }

        [sEcho: params.sEcho, iTotalRecords: partApp.totalCount, iTotalDisplayRecords: partApp.totalCount, aaData: rows]

    }

    def generateNomorDokumenApprovalEditBookingFee() {
        def result = [:]
        def c = ApprovalT770.createCriteria()
        def results = c.get() {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                max("id", "id")
            }
        }

        def m = results.id ?: 0

        result.value = String.format('%014d', m + 1)
        return result
    }

    def generateNomorDokumenApprovalCancelBookingFee() {
        def result = [:]
        def c = ApprovalT770.createCriteria()
        def results = c.get() {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                max("id", "id")
            }
        }

        def m = results.id ?: 0

        result.value = String.format('%014d', m + 1)
        return result
    }

    def generateNomorDokumenApprovalCancelApp() {
        def result = [:]
        def c = ApprovalT770.createCriteria()
        def results = c.get() {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                max("id", "id")
            }
        }

        def m = results.id ?: 0

        result.value = String.format('%014d', m + 1)
        return result
    }

    def requestApprovalEditBookingFee(def params) {
        def result = [:]
        def requestList = []
        def requestIds = JSON.parse(params.requestIds)
        requestIds.each {
            requestList << it
        }

        PartsApp.withTransaction { status ->
            def fail = { Map m ->
                status.setRollbackOnly()
                result.status = "nok"
                result.error = [code: m.code, args: ["Request", params.requestId]]
                return result
            }

            requestList.each {
                PartsApp pa = PartsApp.get(it as long)
                if (!pa)
                    return fail(code: "default.not.found.message")

                pa.t303Jumlah1P = params."qty-${it}" as Double
                pa.t303xKet = params."keterangan-${it}" as String
                pa.t303DPRpP = params."dp-${it}" as Double
                pa.t303RencanaDPP = params."dp-${it}" as Double
                pa.lastUpdated = datatablesUtilService.syncTime()

                pa.save(flush: true)
                pa.errors.each {
                    //println it
                }
            }
        }

        def requests = requestList.join('#')

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        if (!kegiatanApproval) {
            result.status = "nok"
            result.error = "Kegiatan Approval tidak diketemukan."
            return result
        }

        def approval = new ApprovalT770(
                companyDealer: params?.companyDealer,
                t770FK: requests,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: params.t770NoDokumen,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush: true)
        approval.errors.each {
            //println it
        }

        result.status = "ok"

        return result
    }

    def requestApprovalCancelBookingFee(def params) {
        def result = [:]
        def requestList = []
        def requestIds = JSON.parse(params.requestIds)
        requestIds.each {
            requestList << it
        }
        def requests = requestList.join('#')

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        if (!kegiatanApproval) {
            result.status = "nok"
            result.error = "Kegiatan Approval tidak diketemukan."
            return result
        }

        def approval = new ApprovalT770(
                companyDealer: params?.companyDealer,
                t770FK: requests,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: params.t770NoDokumen,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush: true)
        approval.errors.each {
            //println it
        }

        result.status = "ok"

        return result
    }

    def requestApprovalCancelAppointment(def params) {
        def result = [:]
        def requestList = []
        def requestIds = JSON.parse(params.requestIds)
        requestIds.each {
            requestList << it
        }
        def requests = requestList.join('#')

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        if (!kegiatanApproval) {
            result.status = "nok"
            result.error = "Kegiatan Approval tidak diketemukan."
            return result
        }

        def approval = new ApprovalT770(
                t770FK: requests,
                companyDealer: params?.companyDealer,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: params.t770NoDokumen,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush: true)
        approval.errors.each {
            //println it
        }

        result.status = "ok"

        return result
    }

    def getNewAppointment(def params){
        def hasil = []
        Date dateAwal = new Date().clearTime()
        Date dateAkhir = dateAwal+1
        def kodeKotaNoPol = KodeKotaNoPol.findByM116ID(params.kodeKotaNoPol.toString())
        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("fullNoPol", kodeKotaNoPol?.m116ID+" "+params.noPolTengah+" "+params.noPolBelakang,[ignoreCase : true])
        }

        if(histCV){
            if(histCV.last().customerVehicle.getCurrentCondition()==histCV.last()){
                def custTemp = new HistoryCustomer(), driverTemp = new HistoryCustomer()
                def cariPakai = false, cariDriver = false
                def custVehicle = histCV.last().customerVehicle
                def fa = VehicleFA.findByCustomerVehicleAndHistoryCustomerVehicle(custVehicle,custVehicle?.getCurrentCondition())
                def mappingCV = MappingCustVehicle.createCriteria().list {
                    eq("customerVehicle",custVehicle)
                    order("dateCreated","desc")
                }
                for(cari in mappingCV){
                    if((cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK DAN PENANGGUNG JAWAB')) && !cariPakai){
                        custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0")
                        cariPakai=true
                    }
                    if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('DRIVER') && !cariDriver){
                        driverTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0")
                        cariDriver=true
                    }
                    if(cariPakai && cariDriver){
                        break
                    }else{
                        if(!cariPakai){
                            custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0")
                        }
                    }
                }

                def reception = new Reception()
                reception.companyDealer = params.companyDealer
                reception.staSave = "1"
                reception.isProgress = "0"
                reception?.dateCreated = datatablesUtilService?.syncTime()
                reception?.lastUpdated = datatablesUtilService?.syncTime()
                reception.t401NoAppointment = generateCodeService.codeGenerateSequence("T401_NoAppointment", params.companyDealer)
                reception.save(flush: true)
                reception.errors.each {
                    println it
                }

                def appointment = new Appointment()
                appointment.companyDealer = params.companyDealer
                appointment.t301TglJamApp = new Date()
                appointment.historyCustomerVehicle = custVehicle.getCurrentCondition()
                appointment.historyCustomer = custTemp
                appointment.t301StaOkCancelReSchedule = "0"
                appointment.staDel = "0"
                appointment.saveFlag = "1"
                appointment.reception = reception
                appointment.staSave = "1"
                appointment?.dateCreated = datatablesUtilService?.syncTime()
                appointment?.lastUpdated = datatablesUtilService?.syncTime()
                appointment.save()
                appointment.errors.each {
                    println it
                }
//                def appointmentt = Appointment.get(params.appointmentId.toLong())

                def spk = new SPK()
                if(custTemp?.company){
                    def getSPK = SPK.findAllByCompanyAndStaDelAndT191TglAwalLessThanEqualsAndT191TglAkhirGreaterThanEquals(custTemp?.company,"0",new Date(),new Date())
                    if(getSPK.size()>0){
                        appointment?.sPK = getSPK.last()
                        spk = getSPK.last()
                    }
                }
                def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(custVehicle,"0")
                def staJDPower = CustomerSurveyDetail.createCriteria().list {
                    eq("staDel","0")
                    eq("customerVehicle",custVehicle)
                    customerSurvey{
                        le("t104TglAwal",new Date())
                        ge("t104TglAkhir",new Date())
                    }
                }

                hasil << [
                        fa : fa?.fa?.m185NamaFA,
                        tpss : "-",
                        fu : "-",
                        pks : "-",
                        fullnopol : appointment?.historyCustomerVehicle?.fullNoPol,
                        pemakai : custTemp?.t182NamaBelakang ? custTemp?.fullNama : custTemp?.t182NamaDepan,
                        driver : driverTemp?.t182NamaBelakang ? driverTemp?.fullNama : driverTemp?.t182NamaDepan,
                        mobil : custVehicle?.getCurrentCondition()?.fullModelCode?.baseModel?.m102NamaBaseModel,
                        tanggalBayar : appointment?.t301TglJamJanjiBayarDP ? appointment?.t301TglJamJanjiBayarDP?.format("dd MMMM yyyy / HH:mm") : "-",
                        tanggalDatang : appointment?.t301TglJamRencana ? appointment?.t301TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                        telp : custTemp?.t182NoHp,
                        alamat : custTemp?.t182Alamat,
                        tanggalMulai : "-",
                        tanggalDelivery : "-",
                        idCV : custVehicle?.t103VinCode,
                        jobSuggest: jobSuggest?.t503KetJobSuggest,
                        idAppointment : appointment?.id ? appointment?.id : "",
                        tanggalAppointment : appointment?.t301TglJamApp ? appointment?.t301TglJamApp?.format("dd MMMM yyyy") : new Date().format("dd MMMM yyyy"),
                        t770NoDokumen:  appointment?.reception?.t401NoAppointment ? appointment?.reception?.t401NoAppointment : "",
                        tgglReception : appointment?.reception?.t401TglJamCetakWO ? appointment?.reception?.t401TglJamCetakWO?.format("dd MMMM yyyy") : new Date().format("dd MMMM yyyy"),
                        hasil: "ada",
                        spk : spk?.t191JmlSPK ? spk?.t191JmlSPK : "-1",
                        staJDPower : staJDPower?.size()>0 ? "0" : "1",
                ]
            }else{
                hasil << [hasil : "replaced"]
            }
        }else{
            hasil << [hasil : "nothing"]
        }
        return hasil
    }

//    Coba New Appointment Atas
    def getAppointment(def params){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def waktuSekarang = df.format(new Date())

        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")

        def hasil = []
        def kodeKotaNoPol = KodeKotaNoPol.findByM116ID(params.kodeKotaNoPol.toString())

        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()

        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("fullNoPol",kodeKotaNoPol?.m116ID+" "+params.noPolTengah+" "+params.noPolBelakang,[ignoreCase : true])
        }

        if(histCV){
            if(histCV.last().customerVehicle.getCurrentCondition()==histCV.last()){
                def custTemp = new HistoryCustomer(),driverTemp = new HistoryCustomer()
                def cariPakai = false,cariDriver=false;
                def custVehicle = histCV.last().customerVehicle
                def fa = VehicleFA.findByCustomerVehicleAndHistoryCustomerVehicle(custVehicle,custVehicle?.getCurrentCondition())

                def mappingCV = MappingCustVehicle.createCriteria().list {
                    eq("customerVehicle",custVehicle)
                    order("dateCreated","desc")
                }

                def findAppoint = Appointment.createCriteria().list {
                    eq("staDel","0")
                    eq("staSave","0")
                    eq("historyCustomerVehicle",histCV.last())
                    or{
                        and {
                            ge("dateCreated",dateAwal)
                            lt("dateCreated",dateAkhir)
                        }
                        ge("t301TglJamRencana",dateAwal)
                    }
                    order("dateCreated")
                }

                if(findAppoint.size()>0){
                    for(cari in mappingCV){
                        if((cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toUpperCase()?.contains('PEMILIK DAN PENANGGUNG JAWAB')) && !cariPakai){
                            custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                            cariPakai=true;
                        }
                        if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Driver')&& !cariDriver){
                            driverTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                            cariDriver=true;
                        }
                        if(cariPakai && cariDriver){
                            break
                        }else{
                            if(!cariPakai){
                                custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                            }
                        }
                    }
                    def appointment = findAppoint?.last()
                    def spk = new SPK()
                    if(custTemp?.company){
                        def getspk = SPK.findAllByCompanyAndStaDelAndT191TglAwalLessThanEqualsAndT191TglAkhirGreaterThanEquals(custTemp?.company,"0",new Date(),new Date())
                        if(getspk.size()>0 && !appointment?.sPK){
                            appointment?.sPK = getspk.last()
                            spk = getspk.last()
                        }
                    }
                    def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(custVehicle,"0")
                    def staJDPower = CustomerSurveyDetail.createCriteria().list {
                        eq("staDel","0")
                        eq("customerVehicle",custVehicle)
                        customerSurvey{
                            le("t104TglAwal",new Date())
                            ge("t104TglAkhir",new Date())
                        }
                    }
                    def spkAsuransi = SPKAsuransi.findAllByCustomerVehicleAndT193TglAwalLessThanEqualsAndT193TglAkhirGreaterThanEqualsAndStaDel(custVehicle,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00") ,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00"),"0");
                    if(spkAsuransi.size()>0){

                        if(!appointment?.sPKAsuransi){
                            appointment?.sPKAsuransi = spkAsuransi.last()
                        }

                        if(!appointment?.reception?.t401StaButuhSPKSebelumProd){
                            appointment?.reception?.t401StaButuhSPKSebelumProd = "1"
                            appointment?.lastUpdated = datatablesUtilService.syncTime()
                            appointment?.save(flush: true);
                        }
                    }

                    def arrKeluhan = []
                    def keluhans = KeluhanRcp.findAllByReceptionAndStaDel(appointment.reception,"0")
                    keluhans.each {
                        arrKeluhan << [it?.t411NamaKeluhan,it?.t411StaButuhDiagnose,"K"]
                    }
                    def permintaans = PermintaanCustomer.findAllByReceptionAndStaDel(appointment.reception,"0")
                    permintaans.each {
                        arrKeluhan << [it?.permintaan,it?.staDiagnose,"P"]
                    }

                    def kategori = JobApp.findByReceptionAndT302StaDel(appointment.reception,"0",[sort : 'dateCreated',order: 'desc'])
                    hasil << [
                            fa : fa?.fa?.m185NamaFA,
                            tpss : "-",
                            fu : "-",
                            pks : "-",
                            fullnopol : appointment?.historyCustomerVehicle?.fullNoPol,
                            pemakai : custTemp?.t182NamaBelakang ? custTemp?.fullNama : custTemp?.t182NamaDepan,
                            driver : driverTemp?.t182NamaBelakang ? driverTemp?.fullNama : driverTemp?.t182NamaDepan,
                            mobil : custVehicle?.getCurrentCondition()?.fullModelCode?.baseModel?.m102NamaBaseModel,
                            tanggalBayar : appointment?.t301TglJamJanjiBayarDP ? appointment?.t301TglJamJanjiBayarDP?.format("dd MMMM yyyy / HH:mm") : "-",
                            tanggalDatang : appointment?.t301TglJamRencana ? appointment?.t301TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                            tanggalMulai : appointment?.t301TglJamRencana ? appointment?.t301TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                            tanggalDelivery : appointment?.reception?.t401TglJamJanjiPenyerahan? appointment?.reception?.t401TglJamJanjiPenyerahan?.format("dd MMMM yyyy / HH:mm") : "-",
                            telp : custTemp?.t182NoHp,
                            alamat : custTemp?.t182Alamat,
                            idCV : custVehicle?.t103VinCode,
                            jobSuggest: jobSuggest?.t503KetJobSuggest,
                            idAppointment : appointment?.id ? appointment?.id : "",
                            t770NoDokumen : appointment?.reception?.t401NoAppointment ? appointment?.reception?.t401NoAppointment : "",
                            tgglReception : appointment?.reception?.t401TglJamCetakWO ? appointment?.reception?.t401TglJamCetakWO?.format("dd MMMM yyyy") : new Date().format("dd MMMM yyyy"),
                            hasil: "ada",
                            spk : spk?.t191JmlSPK ? spk?.t191JmlSPK : "-1",
                            staJDPower : staJDPower?.size()>0 ? "0" : "1",
                            jenisApp : appointment?.jenisApp?.id,
                            staThs : appointment?.t301StaTHS,
                            alamatThs : appointment?.t301AlamatTHS,
                            kmSekarang : appointment?.reception?.t401KmSaatIni,
                            keluhan : arrKeluhan,
                            kategori : kategori?.operation?.kategoriJob?.id,
                            statusWarranty : kategori?.statusWarranty?.id
                    ]
                }else{
                    hasil << [hasil : "noappointment"]
                }
            }
            else{
                hasil << [hasil : "replaced"]
            }
        }
        else{
            hasil << [hasil : "nothing"]
        }
        return hasil

    }


    def savePreDiagnose(def params){
        Appointment appointmentInstance = Appointment.get(params.appointmentId)

        Prediagnosis prediagnosis = new Prediagnosis()
        prediagnosis.dateCreated = datatablesUtilService?.syncTime()
        prediagnosis.lastUpdated = datatablesUtilService?.syncTime()
        prediagnosis.appointment = appointmentInstance
        prediagnosis.t406KeluhanCust = params.t406KeluhanCust
        prediagnosis.t406StaGejalaHariIni = params.t406StaGejalaHariIni
        prediagnosis.t406StaGejalaMingguLalu = params.t406StaGejalaMingguLalu
        prediagnosis.t406StaGejalaLainnya = params.t406StaGejalaLainnya
        prediagnosis.t406GejalaLainnya = params.t406GejalaLainnya
        if(params.t406Km){
            prediagnosis.t406Km = params.t406Km as int
        }
        prediagnosis.t406TglDiagnosis = new Date()
        prediagnosis.t406StaFrekSekali = params.t406StaFrekSekali
        prediagnosis.t406StaFrekKadang = params.t406StaFrekKadang
        prediagnosis.t406StaFrekSelalu = params.t406StaFrekSelalu
        prediagnosis.t406StaFrekLainnya = params.t406StaFrekLainnya
        prediagnosis.t406FrekLainnya = params.t406FrekLainnya
        prediagnosis.t406StaMILOn = params.t406StaMILOn
        prediagnosis.t406StaMILKedip = params.t406StaMILKedip
        prediagnosis.t406StaMILOff = params.t406StaMILOff
        prediagnosis.t406MILLainnya = params.t406MILLainnya
        prediagnosis.t406StaMILIdling = params.t406StaMILIdling
        prediagnosis.t406StaMILStarting = params.t406StaMILStarting
        prediagnosis.t406StaMILKonstan = params.t406StaMILKonstan
        prediagnosis.t406MILKecepatanLainnya = params.t406MILKecepatanLainnya
        prediagnosis.t406StaMesinPanas = params.t406StaMesinPanas
        prediagnosis.t406StaMesinDingin = params.t406StaMesinDingin
        prediagnosis.t406StaPanasMesinLainnya = params.t406StaPanasMesinLainnya
        prediagnosis.t406PanasMesinLainnya = params.t406PanasMesinLainnya
        prediagnosis.t406Suhu = params.t406PanasMesinLainnya
        prediagnosis.t406StaGigi1 = params.t406StaGigi1
        prediagnosis.t406StaGigi2 = params.t406StaGigi2
        prediagnosis.t406StaGigi3 = params.t406StaGigi3
        prediagnosis.t406StaGigi4 = params.t406StaGigi4
        prediagnosis.t406StaGigi5 = params.t406StaGigi5
        prediagnosis.t406StaGigiP = params.t406StaGigiP
        prediagnosis.t406StaGigiR = params.t406StaGigiR
        prediagnosis.t406StaGigiN = params.t406StaGigiN
        prediagnosis.t406StaGigiD = params.t406StaGigiD
        prediagnosis.t406StaGigiS = params.t406StaGigiS
        prediagnosis.t406StaGigiLainnya = params.t406StaGigiLainnya
        prediagnosis.t406GigiLainnya = params.t406GigiLainnya
        prediagnosis.t406Kecepatan = params.t406Kecepatan as int
        prediagnosis.t406RPM = params.t406RPM as int
        prediagnosis.t406Beban = params.t406Beban as int
        prediagnosis.t406JmlPenumpang = Integer.parseInt(params.t406JmlPenumpang)
        prediagnosis.t406StaDalamKota = params.t406StaDalamKota
        prediagnosis.t406StaJalanLurus = params.t406StaJalanLurus
        prediagnosis.t406StaAkselerasi = params.t406StaAkselerasi
        prediagnosis.t406StaLuarKota = params.t406StaLuarKota
        prediagnosis.t406StaDatar = params.t406StaDatar
        prediagnosis.t406StaRem = params.t406StaRem
        prediagnosis.t406StaTol = params.t406StaTol
        prediagnosis.t406StaTanjakan = params.t406StaTanjakan
        prediagnosis.t406StaBelok = params.t406StaBelok
        prediagnosis.t406StaTurunan = params.t406StaTurunan
        prediagnosis.t406StaKondisiLainnya = params.t406StaKondisiLainnya
        prediagnosis.t406KondisiLainnya = params.t406KondisiLainnya
        prediagnosis.t406StaMacet = params.t406StaMacet
        prediagnosis.t406StaLancar = params.t406StaLancar
        prediagnosis.t406StaLalinLainnya = params.t406StaLalinLainnya
        prediagnosis.t406LalinLainnya = params.t406LalinLainnya
        prediagnosis.t406StaCerah = params.t406StaCerah
        prediagnosis.t406StaBerawan = params.t406StaBerawan
        prediagnosis.t406StaHujan = params.t406StaHujan
        prediagnosis.t406StaPanas = params.t406StaPanas
        prediagnosis.t406StaLembab = params.t406StaLembab
        if(params.t406Temperatur){
            prediagnosis.t406Temperatur = params.t406Temperatur as int
        }
        prediagnosis.t406StaEG = params.t406StaEG
        prediagnosis.t406StaSuspensi = params.t406StaSuspensi
        prediagnosis.t406StaKlasifikasiRem = params.t406StaKlasifikasiRem
        prediagnosis.t406StaLainnya = params.t406StaLainnya
        prediagnosis.t406KlasifikasiLainnya = params.t406KlasifikasiLainnya
        if(params.t406BlowerSpeed){
            prediagnosis.t406BlowerSpeed = params.t406BlowerSpeed as int
        }
        if(params.t406TempSetting){
            prediagnosis.t406TempSetting = params.t406TempSetting as int
        }
        prediagnosis.t406StaReci1 = params.t406StaReci1
        prediagnosis.t406StaReci2 = params.t406StaReci2
        prediagnosis.t406StaReci3 = params.t406StaReci3
        prediagnosis.t406StaReci4 = params.t406StaReci4
        prediagnosis.t406StaReci5 = params.t406StaReci5
        prediagnosis.t406StaReci6 = params.t406StaReci6
        prediagnosis.t406StaPerluDTR = params.t406StaPerluDTR
        prediagnosis.t406StaTidaKPerluDTR = params.t406StaTidaKPerluDTR
        prediagnosis.t406DetailPekerjaan = params.t406DetailPekerjaan
        prediagnosis.t406KonfirmasiAkhir = params.t406KonfirmasiAkhir
        prediagnosis.t406StaOK = params.t406StaOK
        prediagnosis.t406StaNG = params.t406StaNG
        prediagnosis.t406TglJamMulai = params.t406TglJamMulai
        prediagnosis.t406TglJamSelesai = params.t406TglJamSelesai
        prediagnosis.staDel = '0'
        prediagnosis.createdBy = 'admin'
        prediagnosis.lastUpdated = new Date()
        prediagnosis.updatedBy = 'admin'
        prediagnosis.lastUpdProcess = 'INSERT'

        prediagnosis.save(flush: true)
        prediagnosis.errors.each{
            //println "err " + it
        }
        def result = [success: "1"]

        def countPemeriksaanAwal = new Integer(params.countPemeriksaanAwal)
        def indexRow = 1
        def diagnosisDetail


        while(indexRow < countPemeriksaanAwal){
            diagnosisDetail = new DiagnosisDetail()
            diagnosisDetail.prediagnosis = prediagnosis
            diagnosisDetail.reception = appointmentInstance?.reception
            diagnosisDetail.t416staAwalUlang = 'A'
            diagnosisDetail.t416System = params.get("inputSystemPA" + indexRow)
            diagnosisDetail.t416DTC = params.get("inputDTCPA" + indexRow)
            diagnosisDetail.t416StatusPCH = params.get("inputStatusPA" + indexRow)
            diagnosisDetail.t416Desc = params.get("inputDescPA" + indexRow)
            diagnosisDetail.t416Freeze = params.get("inputFreezePA" + indexRow)

            diagnosisDetail.staDel = 0
            diagnosisDetail.dateCreated = datatablesUtilService.syncTime()
            diagnosisDetail.lastUpdated = datatablesUtilService.syncTime()
            diagnosisDetail.createdBy = "system"
            diagnosisDetail.lastUpdProcess = "INSERT"

            diagnosisDetail.save(flush:true)
            indexRow++
        }

        def countCekUlang = new Integer(params.countCekUlang)
        indexRow = 1

        while(indexRow < countCekUlang){
            diagnosisDetail = new DiagnosisDetail()
            diagnosisDetail.prediagnosis = prediagnosis
            diagnosisDetail.reception = appointmentInstance?.reception
            diagnosisDetail.t416staAwalUlang = 'U'
            diagnosisDetail.t416System = params.get("inputSystemCU" + indexRow)
            diagnosisDetail.t416DTC = params.get("inputDTCCU" + indexRow)
            diagnosisDetail.t416StatusPCH = params.get("inputStatusCU" + indexRow)
            diagnosisDetail.t416Desc = params.get("inputDescCU" + indexRow)
            diagnosisDetail.t416Freeze = params.get("inputFreezeCU" + indexRow)

            diagnosisDetail.staDel = 0
            diagnosisDetail.dateCreated = datatablesUtilService.syncTime()
            diagnosisDetail.lastUpdated = datatablesUtilService.syncTime()
            diagnosisDetail.createdBy = "system"
            diagnosisDetail.lastUpdProcess = "INSERT"

            diagnosisDetail.save(flush:true)
            indexRow++
        }
        return prediagnosis;
    }

    def JobKeluhan(def params){
        def result = [:]
        def user = User.findByUsernameAndStaDel(org.apache.shiro.SecurityUtils.subject.principal.toString(),"0");
        result.petugas = user.fullname
        return result
    }
}