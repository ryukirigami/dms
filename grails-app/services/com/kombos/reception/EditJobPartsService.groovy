package com.kombos.reception

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.customerprofile.*
import com.kombos.maintable.*
import com.kombos.parts.*
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import com.kombos.woinformation.Prediagnosis
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class EditJobPartsService {
    def conversi = new Konversi()
    boolean transactional = false

    def validasiOrderService
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def generateCodeService
    def staApproveJob = ["Approved","UnApproved","Waiting For Approval"]

    def datatablesGoodsList(def params,def session){
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartsRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                eq("t401NoWO",params.noWo.trim(),[ignoreCase: true])
            }
            or{
                ilike("t403StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t403StaTambahKurang","%1%")
                        ilike("t403StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t403StaTambahKurang")
                    isNull("t403StaApproveTambahKurang")
                }
            }
            if(params?.dari){
                if(params?.statusBillTo && params?.statusBillTo=="Bill To"){
                    if(params.billTo){
                        eq("t403billTo",params?.billTo,[ignoreCase: true])
                    }
                    if(params.namaBillTo){
                        eq("t403namaBillTo",params.namaBillTo,[ignoreCase: true])
                    }
                }else{
                    isNotNull("t403DiscPersen")
                    gt("t403DiscPersen",0.toDouble())
                }
            }
        }

        def rows = []
        def kon = new Konversi()
        results.each {
            def hargaParts = it?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.toUpperCase()?.contains("GR") ? (it?.t403HargaRp) : it?.t403TotalRp
            def total = (it?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.toUpperCase()?.contains("GR") ? (it?.t403HargaRp*it?.t403Jumlah1) : it?.t403HargaRp)
            rows << [
                    id: it.id,
                    m111ID: it.goods?.m111ID,
                    m111Nama: it.goods.m111Nama,
                    namaJob: it?.operation?.m053Id+" | "+it.operation.m053NamaOperation,
                    harga: kon.toRupiah(hargaParts),
                    qty: it.t403Jumlah1,
                    total: kon.toRupiah(total),
                    discount: it.t403DiscPersen ? it.t403DiscPersen : 0,
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def datatablesJobsList(def params,def session){
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer", session?.userCompanyDealer)
            reception{
                eq("t401NoWO",params.noWo.trim(),[ignoreCase: true])
            }
            if(params.dari){
                if(params?.statusBillTo && params?.statusBillTo=="Bill To"){
                    if(params.billTo){
                        eq("t402billTo",params?.billTo,[ignoreCase: true])
                    }
                    if(params.namaBillTo){
                        eq("t402namaBillTo",params.namaBillTo,[ignoreCase: true])
                    }
                }else{
                    isNotNull("t402DiscPersen")
                    gt("t402DiscPersen",0.toDouble())
                }
            }

            or{
                ilike("t402StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t402StaTambahKurang","%1%")
                        ilike("t402StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t402StaTambahKurang")
                    isNull("t402StaApproveTambahKurang")
                }
            }
        }

        def rows = []
        def konversi = new Konversi()
        results.each {
            rows << [
                    id: it?.id,
                    m053Id: it?.operation?.m053Id,
                    m053NamaOperation: it?.operation?.m053NamaOperation,
                    harga: it?.t402JmlSublet ? konversi.toRupiah(it?.t402JmlSublet?.toDouble()) : konversi.toRupiah(it?.t402HargaRp),
                    discount: it?.t402DiscPersen ? it?.t402DiscPersen : 0,
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def jobnPartsDatatablesList(def params,def session){
        def rows = []
        def diskonPart = null
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def cv = []
        if(params.sReceptionId){
            Reception reception = Reception.get(params.sReceptionId)
            def panjar = 0
            def kwitansi = Kwitansi.findByReceptionAndT722StaDel(reception,"0")
            if(kwitansi){
                def bookingFee = BookingFee.findByKwitansiAndT721StaDel(kwitansi,"0")
                if(bookingFee){
                    if(kwitansi?.t722StaBookingOnRisk?.equalsIgnoreCase("0")){
                        panjar = bookingFee?.t721JmlhBayar
                    }else{
                        panjar = bookingFee?.t721JmlhBayar
                    }
                }
            }

            def jobRcp = JobRCP.findAllByReceptionAndStaDel(reception,"0")

            if(reception){
                def totalJasa = 0
                def totalPart = 0
                def discJasa = 0
                def disclPart = 0
                def totalRate = 0
                jobRcp.each { JobRCP job ->
                    def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(reception,job?.operation,"0")
                    def isTrue = true;
                    if(job?.t402StaTambahKurang=="1" && job?.t402StaApproveTambahKurang=="0"){
                        isTrue = false;
                    }
                    if(isTrue){
                        totalJasa+=(job?.t402JmlSublet?job?.t402JmlSublet?.toDouble():job?.t402HargaRp)
                        discJasa+=(job?.t402DiscRp?job?.t402DiscRp:0)
                    }
                    parts.each {
                        isTrue = true;
                        if(it?.t403StaTambahKurang=="1" && it?.t403StaApproveTambahKurang=="0"){
                            isTrue = false;
                        }
                        if(isTrue){
                            totalPart+=(it?.t403Jumlah1 * it?.t403HargaRp)
                            disclPart+= (it?.t403DiscRp?it?.t403DiscRp:0)
                        }
                    }
                    totalRate+=job?.t402Rate
                    String isBP = job?.operation?.kategoriJob?.m055KategoriJob
                    String billToJenis = "";
                    try {
                        billToJenis = BillToReception.findByReceptionAndNamaBillTo(job.reception,job.t402namaBillTo).jenisBayar
                    }catch(Exception e){}

                    String billTo = "";
                    String billToApprove = "";
                    if(job?.t402namaBillTo){
                        if(job?.reception?.staApprovalDisc?.toInteger()!=0){
                            billToApprove = staApproveJob[job?.reception?.staApprovalDisc?.toInteger()]
                        }
                        billTo = " - BillTo ("+job?.t402namaBillTo+") - ("+billToJenis+") " + billToApprove
                    }
                    if(job.t402HargaBeliSublet!=null){
                        String app = ""
                        if(job.t402StaApprovalSublet=="0"){
                            app = " (Waiting Approval)"
                        }

                        if(job.t402StaApprovalSublet!="2"){
                            billTo += " -> Sublet ( "+ job?.vendor?.m121Nama +") " + app
                        }

                    }
                    //println("BP LAIN == "+isBP)
                    rows << [

                            idJob         : job?.id,

                            namaJob       : job?.operation?.m053NamaOperation+billTo,

                            kategoriJob   : job?.operation?.kategoriJob?.m055KategoriJob,

                            rate          : job?.t402StaTambahKurang=="1" && job?.t402StaApproveTambahKurang=="0" ? 0 : job?.t402Rate,

                            statusWarranty: job?.statusWarranty?.m058NamaStatusWarranty,

                            nominalJasa       : job?.t402StaTambahKurang=="1" && job?.t402StaApproveTambahKurang=="0" ? 0:conversi.toRupiah((job?.t402JmlSublet?job?.t402JmlSublet?.toDouble():job?.t402HargaRp)),

                            isBP          : ((job?.t402StaTambahKurang=="1" && job?.t402StaApproveTambahKurang=="0") || job?.reception?.t401StaInvoice)?"NO":isBP,

                            staIntExt       : job?.t402StaIntExt=="e" ? "e" : "i",

                            staBaru       : job?.t402StaTambahKurang ? (job?.t402StaTambahKurang=="0" ? "Tambah" : "Kurang") : "",

                            staApproveJob       : job?.t402StaApproveTambahKurang ? staApproveJob[job?.t402StaApproveTambahKurang?.toInteger()] : "",

                            staDiskon       : job?.reception?.staApprovalDisc ? staApproveJob[job?.reception?.staApprovalDisc?.toInteger()] : "",

                            staPart       : parts.size()>0 ? "ada" : "tidak",

                            idReception       : job?.receptionId,

                            panjar       : panjar ? conversi.toRupiah(Double.valueOf(panjar).longValue()) : "0",

                            jasa : job?.t402DiscPersen ? Double.valueOf(job?.t402DiscPersen).longValue() : "",

                            part : diskonPart ? conversi.toRupiah(Double.valueOf(diskonPart).longValue()) : "",

                            totalRate : conversi.toRupiah(totalRate),

                            totalJasa : conversi.toRupiah(Double.valueOf(totalJasa).longValue()),

                            totalPart : conversi.toRupiah(Double.valueOf(totalPart).longValue()),

                            discJasa : conversi.toRupiah(Double.valueOf(discJasa).longValue()),

                            discPart : conversi.toRupiah(Double.valueOf(disclPart).longValue()),

                            totalNominal : conversi.toRupiah(Double.valueOf(totalPart+totalJasa).longValue()),

                            total : conversi.toRupiah(Double.valueOf((totalPart+totalJasa)-(disclPart+discJasa)).longValue()),

                            customJob : job.operation.serial.section.m051NamaSection

                    ]

                }
            }
            [sEcho: params.sEcho, iTotalRecords: jobRcp.size(), iTotalDisplayRecords: jobRcp.size(), aaData: rows]
        } else {
            [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]
        }
    }

    def jobnPartsOrderDatatablesList(def params,def session){
        def rows = []

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        if(params.sReceptionId){
            Reception reception = Reception.get(params.sReceptionId)
            def jobRcp = JobRCP.findAllByReceptionAndStaDel(reception,"0")
            def totalNominal = 0
            def cv = []
            if(params?.sCriteria_nomorBelakang && params?.sCriteria_nomorTengah && params?.sCriteria_kodeKota){
                cv = HistoryCustomerVehicle.createCriteria().list {
                    eq("staDel","0")
                    eq("t183NoPolBelakang",params?.sCriteria_nomorBelakang?.trim(),[ignoreCase: true])
                    eq("t183NoPolTengah",params?.sCriteria_nomorTengah?.trim(),[ignoreCase: true])
                    kodeKotaNoPol{
                        eq("id",params?.sCriteria_kodeKota?.toLong())
                    }
                }
            }
            def baseModel = reception?.historyCustomerVehicle?.fullModelCode?.baseModel
            jobRcp.each { JobRCP job ->
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(reception,job?.operation,"0")
                parts.each {
                    GoodsHargaJual ghj
                    ghj = GoodsHargaJual.findByGoodsAndStaDel(it.goods, "0")
                    def nominalPart = ghj && it.t403Jumlah1 ? ghj.t151HargaTanpaPPN * it.t403Jumlah1 : 0
                    totalNominal+= nominalPart

                }
                rows << [

                        idJob         : job?.id,

                        namaJob       : job?.operation?.m053NamaOperation,

                        kategoriJob   : job?.operation?.kategoriJob?.m055KategoriJob,

                        rate          : job?.t402Rate ? job?.t402Rate : 0,

                        statusWarranty: job?.statusWarranty?.m058NamaStatusWarranty,

                        nominal       : conversi.toRupiah(job?.t402HargaRp),

                        staBaru       : job?.t402StaTambahKurang ? (job?.t402StaTambahKurang=="0" ? "Tambah" : "Kurang") : "",

                        staApproveJob : job?.t402StaApproveTambahKurang ? staApproveJob[job?.t402StaApproveTambahKurang?.toInteger()] : "",

                        staPart       : parts.size()>0 ? "ada" : "tidak",

                        idReception   : job?.receptionId,

                        totalNominal  : conversi.toRupiah(Double.valueOf(totalNominal).longValue())

                ]
            }
            [sEcho: params.sEcho, iTotalRecords: jobRcp.size(), iTotalDisplayRecords: jobRcp.size(), aaData: rows]
        } else {
            [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]
        }
    }

    def workDatatablesList(def params){
        def rows = []
        def size = 0
        if (params."idJob") {
            def mappingWork = MappingWorkItems.findAllByOperation(Operation.findById(params."idJob" as Long))

            mappingWork.each {
                rows << [
                        namaWork : it.workItems.m039WorkItems,
                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: rows]
    }
    def partDatatablesList(def params){
        def rows = []
        def size = 0

        if(params."idReception"){
            if (params."idJob") {
                Operation job = JobRCP.get(params."idJob" as Long).operation
                if (job) {
                def partsRCP = PartsRCP.findAllByReceptionAndOperationAndStaDel(Reception.get(params.idReception.toLong()), job,"0",[sort: 'staDel',order: 'asc'])
                    def konversi = new Konversi()
                    int no = 0;
                    String picking = ""
                    partsRCP.each {
                        no++;
                        String isBP = ""
                        def custIn = it?.reception?.customerIn
                        if(custIn){
                            isBP = custIn?.tujuanKedatangan?.m400Tujuan
                        }
                        def nominalPart = (it?.t403StaTambahKurang=="1" && it?.t403StaApproveTambahKurang=="0") ? 0:(it?.t403Jumlah1 * it?.t403HargaRp)
                        picking = " - (Belum Anfrak)";
                        String billTo = "";
                        String billToJenis = "";
                        try {
                            billToJenis = BillToReception.findByReceptionAndNamaBillTo(it.reception,it.t403namaBillTo).jenisBayar
                        }catch(Exception e){}

                        if(it?.t403namaBillTo){
                            billTo = " - BillTo ("+it?.t403namaBillTo+") - ("+billToJenis+")"
                        }

                        try {
                            def pickLis = PickingSlip.findByReceptionAndStaDel(it.reception,'0')
                            if(pickLis){
                                def pickingLisD = PickingSlipDetail.findByGoodsAndStaDelAndPickingSlip(it.goods,'0',pickLis)
                                if(pickingLisD){
                                    picking = ""
                                }else{
                                    picking = " - (Belum Anfrak)";
                                }
                            }
                        }catch(Exception e){}
                        def diskon = "";
                        try {
                            if(it?.t403DiscPersen>0){
                                diskon  = konversi.toRupiah(it?.t403DiscPersen)  + "% / " + konversi.toRupiah2(it.t403DiscRp)
                            }
                        }catch (Exception e){

                        }

                        rows << [
                                no            : no,
                                idPart        : it.id,
                                namaPart      : it.goods?.m111ID+"||"+it.goods?.m111Nama+billTo+" "+picking,
                                qty           : it.t403Jumlah1,
                                satuan        : it.goods.satuan.m118Satuan1,
                                staBaru       : it?.t403StaTambahKurang ? (it?.t403StaTambahKurang=="0" ? "Tambah" : "Kurang") : "",
                                staApprove    : it?.t403StaApproveTambahKurang ? staApproveJob[it?.t403StaApproveTambahKurang?.toInteger()] : "",
                                harga         : it?.t403HargaRp ? konversi?.toRupiah(it?.t403HargaRp) : 0,
                                rate          : "",
                                staIntExt       : it?.t403StaIntExt=="e" ? "e" : "i",
                                statusWarranty: diskon,
                                nominal       : konversi?.toRupiah(nominalPart),
                                isBP          : ((it?.t403StaTambahKurang=="1"  && it?.t403StaApproveTambahKurang=="0") || it?.reception?.t401StaInvoice)?"NO":isBP
                        ]
                    }
                }
            }
        }

        [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: rows]
    }

    def savePreDiagnose(def params){
        Reception receptionInstance = Reception.get(params.receptionId)

        Prediagnosis prediagnosis = new Prediagnosis()
        prediagnosis.reception = receptionInstance
        prediagnosis.t406KeluhanCust = params.t406KeluhanCust
        prediagnosis.t406StaGejalaHariIni = params.t406StaGejalaHariIni
        prediagnosis.t406StaGejalaMingguLalu = params.t406StaGejalaMingguLalu
        prediagnosis.t406StaGejalaLainnya = params.t406StaGejalaLainnya
        prediagnosis.t406GejalaLainnya = params.t406GejalaLainnya
        if(params.t406Km){
            prediagnosis.t406Km = params.t406Km as int
        }
        prediagnosis.t406TglDiagnosis = datatablesUtilService?.syncTime()
        prediagnosis.t406StaFrekSekali = params.t406StaFrekSekali
        prediagnosis.t406StaFrekKadang = params.t406StaFrekKadang
        prediagnosis.t406StaFrekSelalu = params.t406StaFrekSelalu
        prediagnosis.t406StaFrekLainnya = params.t406StaFrekLainnya
        prediagnosis.t406FrekLainnya = params.t406FrekLainnya
        prediagnosis.t406StaMILOn = params.t406StaMILOn
        prediagnosis.t406StaMILKedip = params.t406StaMILKedip
        prediagnosis.t406StaMILOff = params.t406StaMILOff
        prediagnosis.t406MILLainnya = params.t406MILLainnya
        prediagnosis.t406StaMILIdling = params.t406StaMILIdling
        prediagnosis.t406StaMILStarting = params.t406StaMILStarting
        prediagnosis.t406StaMILKonstan = params.t406StaMILKonstan
        prediagnosis.t406MILKecepatanLainnya = params.t406MILKecepatanLainnya
        prediagnosis.t406StaMesinPanas = params.t406StaMesinPanas
        prediagnosis.t406StaMesinDingin = params.t406StaMesinDingin
        prediagnosis.t406StaPanasMesinLainnya = params.t406StaPanasMesinLainnya
        prediagnosis.t406PanasMesinLainnya = params.t406PanasMesinLainnya
        prediagnosis.t406Suhu = params.t406PanasMesinLainnya
        prediagnosis.t406StaGigi1 = params.t406StaGigi1
        prediagnosis.t406StaGigi2 = params.t406StaGigi2
        prediagnosis.t406StaGigi3 = params.t406StaGigi3
        prediagnosis.t406StaGigi4 = params.t406StaGigi4
        prediagnosis.t406StaGigi5 = params.t406StaGigi5
        prediagnosis.t406StaGigiP = params.t406StaGigiP
        prediagnosis.t406StaGigiR = params.t406StaGigiR
        prediagnosis.t406StaGigiN = params.t406StaGigiN
        prediagnosis.t406StaGigiD = params.t406StaGigiD
        prediagnosis.t406StaGigiS = params.t406StaGigiS
        prediagnosis.t406StaGigiLainnya = params.t406StaGigiLainnya
        prediagnosis.t406GigiLainnya = params.t406GigiLainnya
        prediagnosis.t406Kecepatan = params.t406Kecepatan as int
        prediagnosis.t406RPM = params.t406RPM as int
        prediagnosis.t406Beban = params.t406Beban as int
        prediagnosis.t406JmlPenumpang = Integer.parseInt(params.t406JmlPenumpang)
        prediagnosis.t406StaDalamKota = params.t406StaDalamKota
        prediagnosis.t406StaJalanLurus = params.t406StaJalanLurus
        prediagnosis.t406StaAkselerasi = params.t406StaAkselerasi
        prediagnosis.t406StaLuarKota = params.t406StaLuarKota
        prediagnosis.t406StaDatar = params.t406StaDatar
        prediagnosis.t406StaRem = params.t406StaRem
        prediagnosis.t406StaTol = params.t406StaTol
        prediagnosis.t406StaTanjakan = params.t406StaTanjakan
        prediagnosis.t406StaBelok = params.t406StaBelok
        prediagnosis.t406StaTurunan = params.t406StaTurunan
        prediagnosis.t406StaKondisiLainnya = params.t406StaKondisiLainnya
        prediagnosis.t406KondisiLainnya = params.t406KondisiLainnya
        prediagnosis.t406StaMacet = params.t406StaMacet
        prediagnosis.t406StaLancar = params.t406StaLancar
        prediagnosis.t406StaLalinLainnya = params.t406StaLalinLainnya
        prediagnosis.t406LalinLainnya = params.t406LalinLainnya
        prediagnosis.t406StaCerah = params.t406StaCerah
        prediagnosis.t406StaBerawan = params.t406StaBerawan
        prediagnosis.t406StaHujan = params.t406StaHujan
        prediagnosis.t406StaPanas = params.t406StaPanas
        prediagnosis.t406StaLembab = params.t406StaLembab
        if(params.t406Temperatur){
            prediagnosis.t406Temperatur = params.t406Temperatur as int
        }
        prediagnosis.t406StaEG = params.t406StaEG
        prediagnosis.t406StaSuspensi = params.t406StaSuspensi
        prediagnosis.t406StaKlasifikasiRem = params.t406StaKlasifikasiRem
        prediagnosis.t406StaLainnya = params.t406StaLainnya
        prediagnosis.t406KlasifikasiLainnya = params.t406KlasifikasiLainnya
        if(params.t406BlowerSpeed){
            prediagnosis.t406BlowerSpeed = params.t406BlowerSpeed as int
        }
        if(params.t406TempSetting){
            prediagnosis.t406TempSetting = params.t406TempSetting as int
        }
        prediagnosis.t406StaReci1 = params.t406StaReci1
        prediagnosis.t406StaReci2 = params.t406StaReci2
        prediagnosis.t406StaReci3 = params.t406StaReci3
        prediagnosis.t406StaReci4 = params.t406StaReci4
        prediagnosis.t406StaReci5 = params.t406StaReci5
        prediagnosis.t406StaReci6 = params.t406StaReci6
        prediagnosis.t406StaPerluDTR = params.t406StaPerluDTR
        prediagnosis.t406StaTidaKPerluDTR = params.t406StaTidaKPerluDTR
        prediagnosis.t406DetailPekerjaan = params.t406DetailPekerjaan
        prediagnosis.t406KonfirmasiAkhir = params.t406KonfirmasiAkhir
        prediagnosis.t406StaOK = params.t406StaOK
        prediagnosis.t406StaNG = params.t406StaNG
        prediagnosis.staDel = '0'
        prediagnosis.createdBy = 'admin'
        prediagnosis.lastUpdated = datatablesUtilService.syncTime()
        prediagnosis.dateCreated = datatablesUtilService.syncTime()
        prediagnosis.updatedBy = 'admin'
        prediagnosis.lastUpdProcess = 'INSERT'

        prediagnosis.save(flush: true)
        prediagnosis.errors.each{ //println "err " + it
             }
        def result = [success: "1"]

        def countPemeriksaanAwal = new Integer(params.countPemeriksaanAwal)
        def indexRow = 1
        def diagnosisDetail


        while(indexRow < countPemeriksaanAwal){
            diagnosisDetail = new DiagnosisDetail()
            diagnosisDetail.prediagnosis = prediagnosis
            diagnosisDetail.reception = receptionInstance
            diagnosisDetail.t416staAwalUlang = 'A'
            diagnosisDetail.t416System = params.get("inputSystemPA" + indexRow)
            diagnosisDetail.t416DTC = params.get("inputDTCPA" + indexRow)
            diagnosisDetail.t416StatusPCH = params.get("inputStatusPA" + indexRow)
            diagnosisDetail.t416Desc = params.get("inputDescPA" + indexRow)
            diagnosisDetail.t416Freeze = params.get("inputFreezePA" + indexRow)

            diagnosisDetail.staDel = 0
            diagnosisDetail.createdBy = "system"
            diagnosisDetail.lastUpdProcess = "INSERT"
            diagnosisDetail.dateCreated = datatablesUtilService.syncTime()
            diagnosisDetail.lastUpdated = datatablesUtilService.syncTime()

            diagnosisDetail.save(flush:true)
            indexRow++
        }

        def countCekUlang = new Integer(params.countCekUlang)
        indexRow = 1

        while(indexRow < countCekUlang){
            diagnosisDetail = new DiagnosisDetail()
            diagnosisDetail.prediagnosis = prediagnosis
            diagnosisDetail.reception = receptionInstance
            diagnosisDetail.t416staAwalUlang = 'U'
            diagnosisDetail.t416System = params.get("inputSystemCU" + indexRow)
            diagnosisDetail.t416DTC = params.get("inputDTCCU" + indexRow)
            diagnosisDetail.t416StatusPCH = params.get("inputStatusCU" + indexRow)
            diagnosisDetail.t416Desc = params.get("inputDescCU" + indexRow)
            diagnosisDetail.t416Freeze = params.get("inputFreezeCU" + indexRow)

            diagnosisDetail.staDel = 0
            diagnosisDetail.createdBy = "system"
            diagnosisDetail.lastUpdProcess = "INSERT"
            diagnosisDetail.dateCreated = datatablesUtilService.syncTime()
            diagnosisDetail.lastUpdated = datatablesUtilService.syncTime()

            diagnosisDetail.save(flush:true)
            indexRow++
        }

        return prediagnosis;
    }

    def addPartDatatablesList(def params){
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_goods") {
                ilike("m111ID", "%" + (params."sCriteria_goods" as String) + "%")
            }

            if (params."sCriteria_goods2") {
                ilike("m111Nama", "%" + (params."sCriteria_goods2" as String) + "%")
            }

            // ilike("statusParts", "1")
//            switch (sortProperty) {
//                default:
//                    order(sortProperty, sortDir)
//                    break;
//            }
        }

        def rows = []
        def nos = 0
        results.each {
            nos = nos + 1
            def ghj = GoodsHargaJual.findByGoodsAndStaDel(it, "0")
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN
            def stokR =0
            try {
                stokR = PartsStok.findByGoodsAndCompanyDealerAndStaDel(it,session.userCompanyDealer,'0')?.t131Qty1
            }catch (Exception e){

            }
            //it.goods?.goodsHargaJual?.t151HargaTanpaPPN
            rows << [

                    id: it.id,

                    goods: it.m111ID,

                    goods2: it.m111Nama,
                    satuan: it.satuan?.m118Satuan1,
                    stokR: stokR,
                    totalHarga: conversi.toRupiah(hargaTanpaPPN)

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]


    }

    def savePart(params){
        def result = [:]
        def fail = { Map m ->
            if(result.receptionInstance && m.field)
                result.receptionInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["Reception", params.id] ]
            return result
        }

        Reception receptionInstance = Reception.get(params.receptionId as Long)

        result.receptionInstance =  receptionInstance

        Reception reception = receptionInstance

        def jsonArray = JSON.parse(params.part_ids)
        jsonArray.each {


            Goods goods = Goods.get(params."part_${it}" as Long)
            NamaProsesBP prosesBP = NamaProsesBP.get(params."prosesBP_${it}" as Long)
            PartJob pj = PartJob.findByGoodsAndNamaProsesBP(goods, prosesBP)
            if(pj){
                PartsApp pa = new PartsApp()
                pa.setGoods(goods)
                pa.setOperation(pj.operation)
                pa.reception = reception
                pa.namaProsesBP = prosesBP
                pa.t303Jumlah1 = params."qty_${it}" as Long
                pa.t303StaDel = "0"
                pa.lastUpdated = datatablesUtilService.syncTime()
                pa.dateCreated = datatablesUtilService.syncTime()
                pa.save(flush: true)
                pa.errors.each { //println it
                    }

                JobApp ja = new JobApp()
                ja.reception = reception
                ja.operation = pj.operation
                ja.t302StaDel = "0"
                ja.lastUpdated = datatablesUtilService.syncTime()
                ja.dateCreated = datatablesUtilService.syncTime()
                ja.save(flush: true)
                ja.errors.each { //println it
                    }

            }
        }


        if(result.receptionInstance.hasErrors() || !result.receptionInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        result."status" = "ok"
        // success
        return result
    }

    def saveSummary(params){
        def result = [success: "1"]

        def id = params.id
        def countRow = new Integer(params.countRow)
        def indexRow = 1
        def settlement

        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm")

        Reception receptionInstance = Reception.get(id)
        receptionInstance.t401TglJamPenyerahan =  sdf.parse(params.get("tanggalJamJanjiPenyerahan"))


        while(indexRow < countRow){
            settlement = new Settlement()
            settlement.reception = receptionInstance
            settlement.metodeBayar = com.kombos.maintable.MetodeBayar.get(params.get("metodePembayaran" + indexRow))
            settlement.t704JmlBayar = new Double(params.get("jumlahPembayaran" + indexRow))
            settlement.t704keterangan = params.get("keteranganPembayaran" + indexRow)
            settlement.staDel = 0
            settlement.createdBy = "system"
            settlement.lastUpdProcess = "INSERT"
            settlement.dateCreated = datatablesUtilService.syncTime()
            settlement.lastUpdated = datatablesUtilService.syncTime()
            settlement.save(flush:true)
            indexRow++
        }

        receptionInstance.save(flush:true)

        return result
    }

    def orderPart(def params, CompanyDealer companyDealer) {

        Reception reception = Reception.get(params.idReception.toLong())
        RPP rpp=null;
        def rppc = RPP.createCriteria()
        def resRPP = rppc.list {
            eq("companyDealer", companyDealer)
            order("m161TglBerlaku", "desc")
        }

        if (resRPP.size() > 0) {
            rpp = resRPP.get(0)
        }

        DpParts dpParts=null;
        def dpc = DpParts.createCriteria()
        def resDpParts = dpc.list {
            eq("companyDealer", companyDealer)
            order("m162TglBerlaku", "desc")
        }

        if (resDpParts.size() > 0) {
            dpParts = resDpParts.get(0)
        }

        def paList = JSON.parse(params.paIds)
        if (paList) {
            def req = Request.findByT162NoReffAndStaDel(reception?.t401NoWO,"0");
            if(!req){
                req = new Request()
                req.companyDealer = reception.companyDealer
            }
            req.t162TglRequest = datatablesUtilService?.syncTime()
            req.t162NoReff = reception.t401NoWO
            req.dateCreated = datatablesUtilService.syncTime()
            req.lastUpdated = datatablesUtilService.syncTime()
            req.t162NamaPemohon = org.apache.shiro.SecurityUtils.subject.principal.toString()
            req.t162xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
            req.t162xNamaDivisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi?.m012NamaDivisi
            req.dateCreated = datatablesUtilService?.syncTime()
            req.lastUpdated = datatablesUtilService?.syncTime()
            req.save(flush: true)
            req.errors.each {
                //println it
                          }
            paList.each{
                PartsRCP pa = PartsRCP.get(it.toString().toLong());
                RequestDetail reqDetail = RequestDetail.findByRequestAndGoods(req,pa.goods);
                if(!reqDetail){
                    reqDetail = new RequestDetail()
                }
                reqDetail.status = RequestStatus.BELUM_VALIDASI
                reqDetail.goods = pa.goods
                reqDetail.t162Qty1 = pa.t403Jumlah1
                reqDetail.t162Qty2 = pa.t403Jumlah2
                reqDetail.dateCreated = datatablesUtilService.syncTime()
                reqDetail.lastUpdated = datatablesUtilService.syncTime()
                reqDetail.t162Qty1Available = pa.goods?.partsStok?.last()?.t131Qty1
                reqDetail.t162Qty2Available = pa.goods?.partsStok?.last()?.t131Qty2
                reqDetail.request = req
                reqDetail.dateCreated = datatablesUtilService?.syncTime()
                reqDetail.lastUpdated = datatablesUtilService?.syncTime()
                reqDetail.save(flush: true)
                reqDetail.errors.each {
                    //println it
                                  }
                def hargaTanpaPPN = 0
                if (pa.goods && pa.goods.satuan) {
                    def ghj = GoodsHargaJual.findByGoodsAndStaDel(pa.goods, "0")
                    hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                    //it.goods?.goodsHargaJual?.t151HargaTanpaPPN
                }
                ValidasiOrder vo = reqDetail.validasiOrder
                if (!vo) {
                    vo = validasiOrderService.createValidasiOrder(reqDetail)
                    def dp = 0
                    if (dpParts) {
                        vo.t163DP = (dpParts?.m162PersenDP / 100) * hargaTanpaPPN

                    }
                    List ptos = PartTipeOrder.findAllByM112TipeOrder(params."${'tipeOrder_' + it}")
                    if (ptos.size() > 0) {
                        PartTipeOrder pto = ptos.get(0)
                        vo.partTipeOrder = pto
                        Date date =  datatablesUtilService?.syncTime()
                        date.setHours(date.getHours() + pto.m112JamLamaDatang1)
                        vo.t163ETA = date

                    }
                    vo.lastUpdated = datatablesUtilService.syncTime()
                    vo.t163HargaSatuan = hargaTanpaPPN
                    vo.t163MasaPengajuanRPP = rpp?.m161MasaPengajuanRPP
                    vo.save(flush: true)
                }
            }
            if (req.hasErrors() || !req.save(flush: true)) {
                //println("Error creating request")
            }
        }
        def res = [:]
        res.status = "ok"
        res
    }

    def requestPartDatatablesList(def params, CompanyDealer companyDealer) {
        def kodeKota = params.kodeKota
        def nomorTengah = params.nomorTengah
        def nomorBelakang = params.nomorBelakang

        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def reception = Reception.get(params.receptionId.toLong())
        RPP rpp;
        def rppc = RPP.createCriteria()
        def resRPP = rppc.list {
            eq("companyDealer", companyDealer)
            order("m161TglBerlaku", "desc")
        }

        if (resRPP.size() > 0) {
            rpp = resRPP.get(0)
        }

        DpParts dpParts;
        def dpc = DpParts.createCriteria()
        def resDpParts = dpc.list {
            eq("companyDealer", companyDealer)
            order("m162TglBerlaku", "desc")
        }

        if (resDpParts.size() > 0) {
            dpParts = resDpParts.get(0)
        }


        def c = PartsRCP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0");
            eq("reception",reception);
            goods {
                order("m111ID", "asc")
            }
        }

        def rows = []
        for(cari in results){
            def goods = cari.goods
            if (goods) {
                def hargaTanpaPPN = 0
                if (goods && goods.satuan) {
                    def ghj = GoodsHargaJual.findByGoodsAndStaDel(goods, "0")
                    hargaTanpaPPN = ghj?.t151HargaTanpaPPN
                    //it.goods?.goodsHargaJual?.t151HargaTanpaPPN
                }
                def qty = cari.t403Jumlah1
                def satuan = goods?.satuan?.m118Satuan1
                def availabilityQty = goods?.partsStok?.t131Qty1Free ?: "0" as int
                def orderQty = goods?.partsStok?.t131Qty1Reserved ?: "0" as int
                def dp = 0
                if (dpParts) {
//                    dp = (dpParts.m162PersenDP / 100) * hargaTanpaPPN
                }
                rows << [
                        id                : cari.id,
                        status            : "",//reqDetail.status?.toString(),
                        validated         : "0",
                        kodePart          : goods?.m111ID ?: "",
                        namaPart          : goods?.m111Nama ?: "",
                        requestQty        : qty,
                        requestSatuan     : satuan,
                        availabilityQty   : availabilityQty,
                        availabilitySatuan: satuan,
                        orderQty          : orderQty,
                        orderSatuan       : satuan,
                        eta               : "",
                        tipeOrder         : "",
                        rpp               : rpp?.m161MaxDapatRPP,
                        dp                : conversi.toRupiah(dp),
                        harga             : conversi.toRupiah(hargaTanpaPPN),
                        total             : (hargaTanpaPPN) ? hargaTanpaPPN * qty : "0" as int
                ]
            }
        }


        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def cancelEditBookingFeeDatatablesList(def params, CompanyDealer companyDealer) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        Reception reception = Reception.get(params.receptionId as Long)

        def partsapps = [:]
        def reqs = [:]
        DpParts dpParts;
        def dpc = DpParts.createCriteria()
        def resDpParts = dpc.list {
            eq("companyDealer", companyDealer)
            order("m162TglBerlaku", "desc")
        }

        if (resDpParts.size() > 0) {
            dpParts = resDpParts.get(0)
        }


        def c = RequestDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            request {
                eq("t162NoReff", reception.t401NoWO)
            }
            goods {
                order("m111ID", "asc")
            }
        }

        results.each { RequestDetail rd ->
            def goods = rd.goods
            if (goods) {
                reqs."${goods.id}" = rd
            }
        }

        c = PartsRCP.createCriteria()
        results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("reception", reception)
            goods {
                order("m111ID", "asc")
            }
        }

        def rows = []
        results.each { PartsRCP pa ->
            def goods = pa.goods
            if (goods) {
//                partsapps."${goods.id}" = pa
                RequestDetail rd = reqs."${goods.id}"
                def hargaTanpaPPN = 0
                if (goods && goods.satuan) {
                    def ghj = GoodsHargaJual.findByGoodsAndStaDel(goods, "0")
                    hargaTanpaPPN = ghj?.t151HargaTanpaPPN ?: 0
                    //it.goods?.goodsHargaJual?.t151HargaTanpaPPN
                }
                def requestQty = pa?.t403Jumlah1 ?: 0
                def requestQty2 = pa.t403Jumlah1 ?: requestQty
                def qty = pa.t403Jumlah1
                def satuan = goods?.satuan?.m118Satuan1
                def availabilityQty = goods?.partsStok?.t131Qty1Free ?: "0" as int
                def orderQty = goods?.partsStok?.t131Qty1Reserved ?: "0" as int
                def dp = 0
                if (dpParts) {
                    dp = (dpParts.m162PersenDP / 100) * hargaTanpaPPN
                }

                def dp2 = pa.t403DPRp ?: 0
                def total2 = hargaTanpaPPN * requestQty2

                rows << [
                        id            : pa.id,
                        kodePart      : goods?.m111ID ?: "",
                        namaPart      : goods?.m111Nama ?: "",
                        requestQty    : requestQty,
                        requestSatuan : satuan,
                        dp            : dp,
                        harga         : hargaTanpaPPN,
                        total         : hargaTanpaPPN * requestQty,
                        requestQty2   : requestQty2,
                        requestSatuan2: satuan,
                        dp2           : dp2,
                        harga2        : hargaTanpaPPN,
                        total2        : total2,
                        keterangan    : pa.t403xKet ?: "",
                        req           : (dpParts == null) ? "0" : 1

                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def getNewReception(def params){
        def hasil = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def kodeKotaNoPol = KodeKotaNoPol.get(params.kodeKotaNoPol.toLong())
        def findRecep = Reception.createCriteria().list {
            eq("companyDealer",params.companyDealer)
            if(params?.kategori=="Nomor WO"){
                ilike("t401NoWO","%"+params?.noWO+"%");
            }else{
                historyCustomerVehicle{
                    eq("kodeKotaNoPol",kodeKotaNoPol);
                    eq("t183NoPolTengah",params.noPolTengah.replace(" ",""),[ignoreCase: true])
                    eq("t183NoPolBelakang",params.noPolBelakang.replace(" ",""),[ignoreCase: true])
                }
            }
            eq("staDel","0")
            eq("staSave","0")
            order("dateCreated")
        }
        if(findRecep.size()>0){
            def reception = findRecep?.last()
            def custTemp = findRecep?.last()?.historyCustomer
            def appointment = Appointment.findByReceptionAndStaDelAndStaSaveAndCompanyDealer(reception,"0","0", params.companyDealer);
            def spk = new SPK()
            if(custTemp?.company){
                def getspk = SPK.findAllByCompanyAndStaDelAndT191TglAwalLessThanEqualsAndT191TglAkhirGreaterThanEquals(custTemp?.company,"0",new Date(),new Date())
                if(getspk.size()>0 && !reception?.sPK){
                    reception?.sPK = getspk.last()
                    spk = getspk.last()
                }
            }
            def fa = VehicleFA.findByCustomerVehicleAndHistoryCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle,reception?.historyCustomerVehicle)
            def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(reception?.historyCustomerVehicle?.customerVehicle,"0");
            def staJDPower = CustomerSurveyDetail.createCriteria().list {
                eq("staDel","0")
                eq("customerVehicle",reception?.historyCustomerVehicle?.customerVehicle)
                customerSurvey{
                    le("t104TglAwal",new Date())
                    ge("t104TglAkhir",new Date())
                }
            }
            def spkAsuransi = SPKAsuransi.findAllByCustomerVehicleAndT193TglAwalLessThanEqualsAndT193TglAkhirGreaterThanEqualsAndStaDel(reception?.historyCustomerVehicle?.customerVehicle,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00") ,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00"),"0");
            if(spkAsuransi.size()>0){

                if(!reception?.sPkAsuransi){
                    reception?.sPkAsuransi = spkAsuransi.last()
                }

                if(!reception?.t401StaButuhSPKSebelumProd){
                    reception?.t401StaButuhSPKSebelumProd = "1"
                    reception?.lastUpdated = datatablesUtilService?.syncTime()
                    reception?.save(flush: true);
                }
            }
            def arrKeluhan = []
            def keluhans = KeluhanRcp.findAllByReceptionAndStaDel(reception,"0");
            keluhans.each {
                arrKeluhan << [it?.t411NamaKeluhan,it?.t411StaButuhDiagnose]
            }
            def kategori = JobRCP.findByReceptionAndStaDel(reception,"0",[sort : 'dateCreated',order: 'desc']);
            hasil << [
                    fa : fa?.fa?.m185NamaFA,
                    tpss : "-",
                    fu : "-",
                    pks : "-",
                    fullnopol : reception?.historyCustomerVehicle?.fullNoPol,
                    pemakai : custTemp?.t182NamaBelakang ? custTemp?.fullNama : custTemp?.t182NamaDepan,
                    driver : "",
                    mobil : reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    staBooking : appointment ? "Booking" : "Walk In",
                    tanggalBayar : reception?.t401TglJamJanjiBayarDP ? reception?.t401TglJamJanjiBayarDP?.format("dd MMMM yyyy / HH:mm") : "-",
                    tanggalDatang : reception?.t401TglJamRencana ? reception?.t401TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                    tanggalMulai : reception?.t401TglJamRencana ? reception?.t401TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                    tanggalDelivery : reception?.t401TglJamJanjiPenyerahan ? reception?.t401TglJamJanjiPenyerahan?.format("dd MMMM yyyy / HH:mm") : "-",
                    telp : custTemp?.t182NoHp,
                    alamat : custTemp?.t182Alamat,
                    idCV : reception?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                    vincode : reception?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                    jobSuggest: jobSuggest?.t503KetJobSuggest,
                    idReception : reception?.id ? reception?.id : "",
                    noWO : reception?.t401NoWO ? reception?.t401NoWO : "",
                    tgglReception : reception?.t401TglJamCetakWO ? reception?.t401TglJamCetakWO?.format("dd MMMM yyyy") : reception?.dateCreated?.format("dd MMMM yyyy"),
                    hasil: "ada",
                    spk : spk?.t191JmlSPK ? spk?.t191JmlSPK : "-1",
                    staJDPower : staJDPower?.size()>0 ? "0" : "1",
                    staAsuransi : spkAsuransi.size()>0 ? "Y" : "N",
                    jpb : reception?.countJPB,
                    isBpGr: reception?.customerIn?.tujuanKedatangan?.m400Tujuan,
                    noSpk : reception?.sPkAsuransi?.t193NomorSPK,
                    staTwc : reception?.staTwc,
                    kmSekarang : reception?.t401KmSaatIni,
                    keluhan : arrKeluhan,
                    kategori : kategori?.operation?.kategoriJob?.id,
                    statusWarranty : kategori?.statusWarranty?.id,
                    staInvoice : reception?.t401StaInvoice,
                    kendaraan : reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    pemilik : reception?.historyCustomer?.fullNama,
                    kodeKota : reception?.historyCustomerVehicle?.kodeKotaNoPol?.id,
                    tengah : reception?.historyCustomerVehicle?.t183NoPolTengah,
                    belakang : reception?.historyCustomerVehicle?.t183NoPolBelakang,
                    namaSA: reception?.t401NamaSA,
                    jenisWo: reception?.customerIn?.tujuanKedatangan?.m400Tujuan,
                    teknisi: JPB.findByReceptionAndStaDel(reception,"0")?.namaManPower?.t015NamaLengkap.toString(),

            ]
        }else{
                    hasil << [hasil : "noreception"];
                }
        return hasil
    }

    def getReception(def params){
        def hasil = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def reception = Reception.createCriteria().list {
            eq("companyDealer",params?.companyDealer);
            eq("staDel","0");
            eq("t401NoWO",params.noWo.trim(),[ignoreCase: true])
        }

        if(reception){
            def tempRecep = reception.last()
            def histCV = tempRecep?.historyCustomerVehicle
            def custTemp = new HistoryCustomer(),driverTemp = new HistoryCustomer()
            def cariPakai = false,cariDriver=false;
            def custVehicle = histCV.last().customerVehicle
            def fa = VehicleFA.findByCustomerVehicleAndHistoryCustomerVehicle(custVehicle,custVehicle?.getCurrentCondition())
            def mappingCV = MappingCustVehicle.findAllByCustomerVehicle(custVehicle,[sort : 'dateCreated',order : 'desc'])
            for(cari in mappingCV){
                if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik dan Penanggung Jawab') && !cariPakai){
                    custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                    cariPakai=true;
                }
                if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Driver') && !cariDriver){
                    driverTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                    cariDriver=true;
                }
                if(cariPakai && cariDriver){
                    break
                }else{
                    if(!cariPakai){
                        custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                    }
                }
            }
            def appointment = Appointment.findByCustomerVehicleAndHistoryCustomerVehicleAndStaDelAndStaSaveAndCompanyDealer(custVehicle, custVehicle?.getCurrentCondition(), "0","0", params.companyDealer, [sort: 'dateCreated',order: 'asc']);
            def spk = new SPK()
            if(custTemp?.company){
                spk = SPK.findByCompanyAndStaDelAndT191TglAwalLessThanEqualsAndT191TglAkhirGreaterThanEquals(custTemp?.company,"0",new Date(),new Date())
            }
            def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(custVehicle,"0");
            def staJDPower = CustomerSurveyDetail.createCriteria().list {
                eq("staDel","0")
                eq("customerVehicle",custVehicle)
                customerSurvey{
                    le("t104TglAwal",new Date())
                    ge("t104TglAkhir",new Date())
                }
            }

            def spkAsuransi = SPKAsuransi.findByCustomerVehicleAndT193TglAwalLessThanEqualsAndT193TglAkhirGreaterThanEqualsAndStaDel(custVehicle,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00") ,new Date().parse("dd/MM/yyyy HH:mm",new SimpleDateFormat("dd/MM/yyyy").format(new Date())+" 00:00"),"0");
            def arrKeluhan = []
            def keluhans = KeluhanRcp.findAllByReceptionAndStaDel(tempRecep,"0");
            keluhans.each {
                arrKeluhan << [it?.t411NamaKeluhan,it?.t411StaButuhDiagnose]
            }
            def kategori = JobRCP.findByReceptionAndStaDel(tempRecep,"0",[sort : 'dateCreated',order: 'desc']);
            hasil << [
                    fa : fa?.fa?.m185NamaFA,
                    tpss : "-",
                    fu : "-",
                    pks : "-",
                    kodeKota : tempRecep?.historyCustomerVehicle?.kodeKotaNoPol?.id,
                    tengah : tempRecep?.historyCustomerVehicle?.t183NoPolTengah,
                    belakang : tempRecep?.historyCustomerVehicle?.t183NoPolBelakang,
                    pemakai : custTemp?.t182NamaBelakang ? custTemp?.fullNama : custTemp?.t182NamaDepan,
                    driver : driverTemp?.t182NamaBelakang ? driverTemp?.fullNama : driverTemp?.t182NamaDepan,
                    mobil : custVehicle?.getCurrentCondition()?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    staBooking : appointment ? "Booking" : "Walk In",
                    tanggalBayar : appointment?.t301TglJamJanjiBayarDP ? appointment?.t301TglJamJanjiBayarDP?.format("dd MMMM yyyy / HH:mm") : "-",
                    tanggalDatang : appointment?.t301TglJamRencana ? appointment?.t301TglJamRencana?.format("dd MMMM yyyy / HH:mm") : "-",
                    telp : custTemp?.t182NoHp,
                    alamat : custTemp?.t182Alamat,
                    tanggalMulai : "-",
                    tanggalDelivery : "-",
                    idCV : custVehicle?.t103VinCode,
                    jobSuggest: jobSuggest?.t503KetJobSuggest,
                    idReception : tempRecep?.id ? tempRecep?.id : "",
                    noWO : tempRecep?.t401NoWO ? tempRecep?.t401NoWO : "",
                    tgglReception : tempRecep?.t401TglJamCetakWO ? tempRecep?.t401TglJamCetakWO?.format("dd MMMM yyyy") : tempRecep?.dateCreated?.format("dd MMMM yyyy"),
                    hasil: "ada",
                    spk : spk?.t191JmlSPK ? spk?.t191JmlSPK : "-1",
                    staJDPower : staJDPower?.size()>0 ? "0" : "1",
                    staAsuransi : spkAsuransi ? "Y-"+tempRecep?.t401StaButuhSPKSebelumProd : "N",
                    jpb : tempRecep?.countJPB,
                    isBpGr: tempRecep?.customerIn?.tujuanKedatangan?.m400Tujuan,
                    noSpk : tempRecep?.sPkAsuransi?.t193NomorSPK,
                    staTwc : tempRecep?.staTwc,
                    kmSekarang : tempRecep?.t401KmSaatIni,
                    keluhan : arrKeluhan,
                    kategori : kategori?.operation?.kategoriJob?.id,
                    statusWarranty : kategori?.statusWarranty?.id,
                    staInvoice : tempRecep?.t401StaInvoice,
                    kendaraan : tempRecep?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    vincode :custVehicle?.t103VinCode,
                    pemilik : tempRecep?.historyCustomer?.fullNama,
                    namaSA: tempRecep?.t401NamaSA,
                    jenisWo: tempRecep?.customerIn?.tujuanKedatangan?.m400Tujuan,
                    teknisi: JPB.findByReceptionAndStaDel(tempRecep,"0")?.namaManPower?.t015NamaLengkap.toString(),
            ]
        }else{
            hasil << [hasil : "nothing"];
        }
        return hasil
    }

    def jobKeluhan(def params){
        def result = [:]
        def user = User.findByUsernameAndStaDel(org.apache.shiro.SecurityUtils.subject.principal.toString(),"0");
        result.petugas = user.fullname
        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0")
            kodeKotaNoPol{
                eq("id",params.kode.toLong())
            }
            eq("t183NoPolTengah",params.tengah.trim(),[ignoreCase: true])
            eq("t183NoPolBelakang",params.belakang.trim(),[ignoreCase: true])
        }
        def idCV = histCV?.size()>0 ? histCV?.last()?.customerVehicle?.id : -1
        def c = Reception.createCriteria()
        def results = c.list() {
            eq("staDel","0")
            historyCustomerVehicle{
                customerVehicle{
                    eq("id",idCV.toLong());
                }
            }
        }
        def km = results?.size()>1 ? results?.last()?.t401KmSaatIni : 0
        result.minKM = km
        return result
    }

    def estimasiDatatablesList(def params, CompanyDealer companyDealer){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def x = 0

        def c = MappingWorkItems.createCriteria()
        def results = c.list() {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("operation", "operation")
            }
        }
        def rows = []
        results.each {
            def hargaParts = 300000
            def hargaJasa = 450000
            def hargaTotal = hargaParts + hargaJasa
            rows << [
                    idJob                  : it.operation.id,
                    workItem            : it.operation.m053NamaOperation,
                    area : '',
                    repairDifficulty:'',
                    plasticBumper:'',
                    hargaParts: hargaParts,
                    hargaJasa:hargaJasa,
                    hargaTotal: hargaTotal,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }
}