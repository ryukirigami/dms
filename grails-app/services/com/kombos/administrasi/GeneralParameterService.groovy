package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.maintable.Diskon
import grails.converters.JSON
import grails.validation.ValidationException
import org.hibernate.criterion.CriteriaSpecification

class GeneralParameterService {

	def grailsApplication

	boolean transactional = false
	
    def dcvDatatablesList(def params) {
		def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def searchCriterias = [
			[field:"tanggalDec", label:"Tanggal DEC", type: "date"],
			[field:"model", label:"Model", type: "string"],
			[field:"tanggalStnk", label:"Tanggal STNK", type: "date"],
			[field:"tanggalServis", label:"Tanggal Servis", type: "date"],
			[field:"vinCode", label:"Vin Code", type: "string"],
			[field:"noPolisi", label:"Nomor Polisi", type: "string"],
			[field:"namaStnk", label:"Nama STNK", type: "string"],
			[field:"tanggalAwal", label:"Tanggal Awal", type: "date"],
			[field:"tanggalAkhir", label:"Tanggal Akhir", type: "date"]
			]
		
		def ct = 0
		if(params.sCriterias) {
			ct = params.sCriterias as int;
		}
		
		List results = CustomerVehicle.createCriteria().list {
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			createAlias('diskons', 'd', CriteriaSpecification.LEFT_JOIN)
			projections {		
					groupProperty('id', 'id')
					groupProperty('t103VinCode', 'vinCode')
					groupProperty('d.t173TglAwal','tglAwal')
					groupProperty('d.t173TglAkhir','tglAkhir')
					groupProperty('d.t173PersenDiskonJasa','diskonJasa')
					groupProperty('d.t173PersenDiskonParts','diskonPart')
					histories {		
						max('fullNoPol', 'fullNoPol')
						max('t183NamaSTNK', 'namaSTNK')
						fullModelCode {
							baseModel {
								max('m102NamaBaseModel', 'model')
							}
						}
						max('t183TglDEC', 'tglDEC')
					}
			}		
			for(int i = 0 ; i < ct ; i++){
				def op = params."sCriteriaOperator${i}"
				def field =  params."sCriteriaField${i}"
				if(op == 'BETWEEN'){
					def valFrom = params."sCriteriaValueFrom${i}"
					def valTo = params."sCriteriaValueTo${i}"
				} else {
					def val =  params."sCriteriaValue${i}"
				}
				
				if(field == 'model'){
					histories{
						fullModelCode{
							or{
								baseModel{
									if(op == '='){
										eq('m102NamaBaseModel',params."sCriteriaValue${i}") 
									} else if(op == 'LIKE'){
										ilike('m102NamaBaseModel','%'+params."sCriteriaValue${i}"+'%')
									}
								}
								modelName{
									if(op == '='){
										eq('m104NamaModelName',params."sCriteriaValue${i}") 
									} else if(op == 'LIKE'){
										ilike('m104NamaModelName','%'+params."sCriteriaValue${i}"+'%')
									}
								}
							}
						}
					}
				} else if(field == 'vinCode'){
					if(op == '='){
						eq('t103VinCode',params."sCriteriaValue${i}") 
					} else if(op == 'LIKE'){
						ilike('t103VinCode','%'+params."sCriteriaValue${i}"+'%')
					}
				} else if(field == 'namaStnk'){
					histories{
						if(op == '='){
							eq('t183NamaSTNK',params."sCriteriaValue${i}") 
						} else if(op == 'LIKE'){
							ilike('t183NamaSTNK','%'+params."sCriteriaValue${i}"+'%')
						}
					}
				} else if(field == 'noPolisi'){
					histories{
						if(op == '='){
							eq('fullNoPol',params."sCriteriaValue${i}") 
						} else if(op == 'LIKE'){
							ilike('fullNoPol','%'+params."sCriteriaValue${i}"+'%')
						}
					}
				} else if(field == 'tanggalDec'){
					histories{
						if(op == '='){
							def tanggal = Date.parse('dd/MM/yyyy', params."sCriteriaValue${i}") 
							eq('t183TglDEC', tanggal) 
						} else if(op == 'BETWEEN'){
							def tanggalFrom = Date.parse('dd/MM/yyyy', params."sCriteriaValueFrom${i}")
							def tanggalTo = Date.parse('dd/MM/yyyy', params."sCriteriaValueTo${i}") + 1
							ge("t183TglDEC",tanggalFrom)
							lt("t183TglDEC",tanggalTo)
						}
					}
				} else if(field == 'tanggalStnk'){
					histories{
						if(op == '='){
							def tanggal = Date.parse('dd/MM/yyyy', params."sCriteriaValue${i}") 
							eq('t183TglSTNK', tanggal) 
						} else if(op == 'BETWEEN'){
							def tanggalFrom = Date.parse('dd/MM/yyyy', params."sCriteriaValueFrom${i}")
							def tanggalTo = Date.parse('dd/MM/yyyy', params."sCriteriaValueTo${i}") + 1
							ge("t183TglSTNK",tanggalFrom)
							lt("t183TglSTNK",tanggalTo)
						}
					}
				} else if(field == 'tanggalServis'){
					histories{
						if(op == '='){
							def tanggal = Date.parse('dd/MM/yyyy', params."sCriteriaValue${i}") 
							eq('t183TglTransaksi', tanggal) 
						} else if(op == 'BETWEEN'){
							def tanggalFrom = Date.parse('dd/MM/yyyy', params."sCriteriaValueFrom${i}")
							def tanggalTo = Date.parse('dd/MM/yyyy', params."sCriteriaValueTo${i}") + 1
							ge("t183TglTransaksi",tanggalFrom)
							lt("t183TglTransaksi",tanggalTo)
						}
					}
				}  else if(field == 'tanggalAwal'){
						if(op == '='){
							def tanggal = Date.parse('dd/MM/yyyy', params."sCriteriaValue${i}") 
							eq('d.t173TglAwal', tanggal) 
						} else if(op == 'BETWEEN'){
							def tanggalFrom = Date.parse('dd/MM/yyyy', params."sCriteriaValueFrom${i}")
							def tanggalTo = Date.parse('dd/MM/yyyy', params."sCriteriaValueTo${i}") + 1
							ge("d.t173TglAwal",tanggalFrom)
							lt("d.t173TglAwal",tanggalTo)
						}
				}  else if(field == 'tanggalAkhir'){
						if(op == '='){
							def tanggal = Date.parse('dd/MM/yyyy', params."sCriteriaValue${i}") 
							eq('d.t173TglAkhir', tanggal) 
						} else if(op == 'BETWEEN'){
							def tanggalFrom = Date.parse('dd/MM/yyyy', params."sCriteriaValueFrom${i}")
							def tanggalTo = Date.parse('dd/MM/yyyy', params."sCriteriaValueTo${i}") + 1
							ge("d.t173TglAkhir",tanggalFrom)
							lt("d.t173TglAkhir",tanggalTo)
						}
				}
			}
			
		}
		
		def rows = []
		
		results.each {
			rows << [
						id: it.id,
						vinCode: it.vinCode,
						nomorPolisi: it.fullNoPol,
						model: it.model,
						namaSTNK: it.namaSTNK,
						tglDEC: it.tglDEC?it.tglDEC.format(dateFormat):"",
						tglAwal: it.tglAwal?it.tglAwal.format(dateFormat):"",
						tglAkhir: it.tglAkhir?it.tglAkhir.format(dateFormat):"",
						diskonPart: it.diskonPart,
						diskonJasa: it.diskonJasa		
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
				
	}
	
	def setDiscountCustVehicle(def params){
		def jsonArray = JSON.parse(params.ids)
		jsonArray.each{
			def cv = CustomerVehicle.get(it as long)
			if(cv){
				def d = new Diskon(
					customerVehicle: cv,
					t173TglAwal: params.tglAwal,
					t173TglAkhir: params.tglAkhir,
					t173PersenDiskonParts: params.diskonPart,
					t173PersenDiskonJasa: params.diskonJasa)
				d.save(flush:true)
			}
		}
		def result = [:]
		result.status = 'ok'
		result
	}
}
