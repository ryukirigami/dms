package com.kombos.administrasi



import com.kombos.baseapp.AppSettingParam

class KecamatanService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Kecamatan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_provinsi") {
                provinsi{
                    ilike("m001NamaProvinsi","%"+params."sCriteria_provinsi"+"%")
                }
            }

            if (params."sCriteria_kabKota") {
                kabKota{
                    ilike("m002NamaKabKota","%"+params."sCriteria_kabKota"+"%")
                }
            }

            if (params."sCriteria_m003ID") {
                ilike("m003ID", "%" + (params."sCriteria_m003ID" as String) + "%")
            }

            if (params."sCriteria_m003NamaKecamatan") {
                ilike("m003NamaKecamatan", "%" + (params."sCriteria_m003NamaKecamatan" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            eq('staDel','0')

            switch (sortProperty) {
                case "m003ID" :
                        order("id", sortDir);
                    break;
                case "provinsi" :
                    provinsi{
                        order("m001NamaProvinsi", sortDir);
                    }
                    break;
                case "kabKota" :
                    kabKota{
                        order("m002NamaKabKota", sortDir);
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    provinsi: it.provinsi?.m001NamaProvinsi,

                    kabKota: it.kabKota?.m002NamaKabKota,

                    m003ID: it.m003ID,

                    m003NamaKecamatan: it.m003NamaKecamatan,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Kecamatan", params.id]]
            return result
        }

        result.kecamatanInstance = Kecamatan.get(params.id)

        if (!result.kecamatanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Kecamatan", params.id]]
            return result
        }

        result.kecamatanInstance = Kecamatan.get(params.id)

        if (!result.kecamatanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Kecamatan", params.id]]
            return result
        }

        result.kecamatanInstance = Kecamatan.get(params.id)

        if (!result.kecamatanInstance)
            return fail(code: "default.not.found.message")

        try {
            result.kecamatanInstance.staDel = '1'
            result.kecamatanInstance.lastUpdProcess = 'DELETE'
            result.kecamatanInstance.save(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
      //  Kecamatan.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
//                status.setRollbackOnly()
                if (result.kecamatanInstance && m.field)
                    result.kecamatanInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Kecamatan", params.id]]
                return result
            }

            result.kecamatanInstance = Kecamatan.get(params.id)
            def cek = Kecamatan.createCriteria()
            def resultCek = cek.list() {
                and{
                    eq("m003NamaKecamatan",params.m003NamaKecamatan, [ignoreCase: true])
                    kabKota{
                        eq("id",params.kabKota.id as Long)
                    }
                }
                eq("staDel",'0')
            }
            if(resultCek && Kecamatan.findByM003NamaKecamatanIlikeAndKabKotaAndStaDel(params.m003NamaKecamatan,KabKota.findById(params.kabKota.id as Long),'0')?.id != (params.id as Long)){
                return fail(code:"dataGanda")
            }

            if (!result.kecamatanInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.kecamatanInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.kecamatanInstance.properties = params
            result.kecamatanInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.kecamatanInstance.lastUpdProcess = "UPDATE"


            if (result.kecamatanInstance.hasErrors() || !result.kecamatanInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

       // } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Kecamatan", params.id]]
            return result
        }

        result.kecamatanInstance = new Kecamatan()
        result.kecamatanInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.kecamatanInstance && m.field)
                result.kecamatanInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Kecamatan", params.id]]
            return result
        }

        result.kecamatanInstance = new Kecamatan(params)
        result.kecamatanInstance.staDel = '0'
        result.kecamatanInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.kecamatanInstance.lastUpdProcess = "INSERT"
        def cek = result.kecamatanInstance.createCriteria()
        def resultCek = cek.list() {
            eq("m003NamaKecamatan",result.kecamatanInstance.m003NamaKecamatan?.trim(), [ignoreCase: true])
            kabKota{
                eq("id",result.kecamatanInstance.kabKota.id as Long)
            }
            eq("staDel",'0')
        }
        if(resultCek){
            return fail(code:"dataGanda")
        }

        if (result.kecamatanInstance.hasErrors() || !result.kecamatanInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def kecamatan = Kecamatan.findById(params.id)
        if (kecamatan) {
            kecamatan."${params.name}" = params.value
            kecamatan.save()
            if (kecamatan.hasErrors()) {
                throw new Exception("${kecamatan.errors}")
            }
        } else {
            throw new Exception("Kecamatan not found")
        }
    }

}