package com.kombos.administrasi



import com.kombos.baseapp.AppSettingParam

class JenisAlertService {
	boolean transactional = false

	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = JenisAlert.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
			if(params."sCriteria_m901Id"){
				ilike("m901Id","%" + (params."sCriteria_m901Id" as String) + "%")
			}

			if(params."sCriteria_m901NamaAlert"){
				ilike("m901NamaAlert","%" + (params."sCriteria_m901NamaAlert" as String) + "%")
			}

			if(params."sCriteria_m901StaPerluTindakLanjut"){
				ilike("m901StaPerluTindakLanjut","%" + (params."sCriteria_m901StaPerluTindakLanjut" as String) + "%")
			}

			if(params."sCriteria_m901NamaFormTindakLanjut"){
				eq("m901NamaFormTindakLanjut",params."sCriteria_m901NamaFormTindakLanjut" as String)
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}


			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

						id: it.id,

						m901Id: it.m901Id,

						m901NamaAlert: it.m901NamaAlert,

						m901StaPerluTindakLanjut: it.m901StaPerluTindakLanjut,

						m901NamaFormTindakLanjut: it.m901NamaFormTindakLanjut,

						lastUpdProcess: it.lastUpdProcess,

			]
		}

		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

	}

	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["JenisAlert", params.id] ]
			return result
		}

		result.jenisAlertInstance = JenisAlert.get(params.id)

		if(!result.jenisAlertInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}

	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["JenisAlert", params.id] ]
			return result
		}

		result.jenisAlertInstance = JenisAlert.get(params.id)

		if(!result.jenisAlertInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["JenisAlert", params.id] ]
			return result
		}

		result.jenisAlertInstance = new JenisAlert()
		result.jenisAlertInstance.properties = params

		// success
		return result
	}

	def updateField(def params){
		def jenisAlert =  JenisAlert.findById(params.id)
		if (jenisAlert) {
			jenisAlert."${params.name}" = params.value
			jenisAlert.save()
			if (jenisAlert.hasErrors()) {
				throw new Exception("${jenisAlert.errors}")
			}
		}else{
			throw new Exception("JenisAlert not found")
		}
	}

}