package com.kombos.administrasi



import com.kombos.baseapp.AppSettingParam

class KabKotaService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = KabKota.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_provinsi") {
                provinsi{
                    ilike("m001NamaProvinsi","%"+params."sCriteria_provinsi"+"%")
                }
            }

            if (params."sCriteria_m002ID") {
                ilike("m002ID", "%" + (params."sCriteria_m002ID" as String) + "%")
            }

            if (params."sCriteria_m002NamaKabKota") {
                ilike("m002NamaKabKota", "%" + (params."sCriteria_m002NamaKabKota" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            eq('staDel','0')


            switch (sortProperty) {
                case "provinsi" :
                    provinsi{
                        order("m001NamaProvinsi", sortDir);
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    provinsi: it.provinsi?.m001NamaProvinsi,

                    m002ID: it.m002ID,

                    m002NamaKabKota: it.m002NamaKabKota,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KabKota", params.id]]
            return result
        }

        result.kabKotaInstance = KabKota.get(params.id)

        if (!result.kabKotaInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KabKota", params.id]]
            return result
        }

        result.kabKotaInstance = KabKota.get(params.id)

        if (!result.kabKotaInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KabKota", params.id]]
            return result
        }

        result.kabKotaInstance = KabKota.get(params.id)

        if (!result.kabKotaInstance)
            return fail(code: "default.not.found.message")

        try {
            result.kabKotaInstance.staDel = '1'
            result.kabKotaInstance.lastUpdProcess = 'DELETE'
            result.kabKotaInstance.save(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
      //  KabKota.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
              //  status.setRollbackOnly()
                if (result.kabKotaInstance && m.field)
                    result.kabKotaInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["KabKota", params.id]]
                return result
            }

            result.kabKotaInstance = KabKota.get(params.id)

            def cek = KabKota.createCriteria()
            def resultCek = cek.list() {
                and{
                    eq("m002NamaKabKota",params.m002NamaKabKota, [ignoreCase: true])
                    provinsi{
                        eq("id",params.provinsi.id as Long)
                    }
                }
                eq("staDel",'0')
            }
            if(resultCek && KabKota.findByM002NamaKabKotaIlikeAndProvinsiAndStaDel(params.m002NamaKabKota,Provinsi.findById(params.provinsi.id as Long),'0')?.id != (params.id as Long)){
                return fail(code:"dataGanda")
            }

            if (!result.kabKotaInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.kabKotaInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.kabKotaInstance.properties = params
            result.kabKotaInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.kabKotaInstance.lastUpdProcess = "UPDATE"


            if (result.kabKotaInstance.hasErrors() || !result.kabKotaInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

   //     } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KabKota", params.id]]
            return result
        }

        result.kabKotaInstance = new KabKota()
        result.kabKotaInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.kabKotaInstance && m.field)
                result.kabKotaInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["KabKota", params.id]]
            return result
        }

        result.kabKotaInstance = new KabKota(params)
        result.kabKotaInstance.staDel = '0'
        result.kabKotaInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.kabKotaInstance.lastUpdProcess = "INSERT"

        def cek = result.kabKotaInstance.createCriteria()
        def resultCek = cek.list() {
            eq("m002NamaKabKota",result.kabKotaInstance.m002NamaKabKota?.trim(), [ignoreCase: true])
            provinsi{
                eq("id",result.kabKotaInstance.provinsi.id as Long)
            }
            eq("staDel",'0')
        }
        if(resultCek){
            return fail(code:"dataGanda")
        }

        if (result.kabKotaInstance.hasErrors() || !result.kabKotaInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def kabKota = KabKota.findById(params.id)
        if (kabKota) {
            kabKota."${params.name}" = params.value
            kabKota.save()
            if (kabKota.hasErrors()) {
                throw new Exception("${kabKota.errors}")
            }
        } else {
            throw new Exception("KabKota not found")
        }
    }

}