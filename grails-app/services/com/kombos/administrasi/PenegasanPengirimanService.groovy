package com.kombos.administrasi



import com.kombos.baseapp.AppSettingParam

class PenegasanPengirimanService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PenegasanPengiriman.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_jalur") {
                ilike("jalur", "%" + (params."sCriteria_jalur" as String) + "%")
            }

            if (params."sCriteria_keterangan") {
                ilike("keterangan", "%" + (params."sCriteria_keterangan" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    jalur: it.jalur,

                    keterangan: it.keterangan,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PenegasanPengiriman", params.id]]
            return result
        }

        result.penegasanPengirimanInstance = PenegasanPengiriman.get(params.id)

        if (!result.penegasanPengirimanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PenegasanPengiriman", params.id]]
            return result
        }

        result.penegasanPengirimanInstance = PenegasanPengiriman.get(params.id)

        if (!result.penegasanPengirimanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PenegasanPengiriman", params.id]]
            return result
        }

        result.penegasanPengirimanInstance = PenegasanPengiriman.get(params.id)

        if (!result.penegasanPengirimanInstance)
            return fail(code: "default.not.found.message")

        try {
            result.penegasanPengirimanInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        PenegasanPengiriman.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.penegasanPengirimanInstance && m.field)
                    result.penegasanPengirimanInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["PenegasanPengiriman", params.id]]
                return result
            }

            result.penegasanPengirimanInstance = PenegasanPengiriman.get(params.id)

            if (!result.penegasanPengirimanInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.penegasanPengirimanInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.penegasanPengirimanInstance.properties = params

            if (result.penegasanPengirimanInstance.hasErrors() || !result.penegasanPengirimanInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PenegasanPengiriman", params.id]]
            return result
        }

        result.penegasanPengirimanInstance = new PenegasanPengiriman()
        result.penegasanPengirimanInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.penegasanPengirimanInstance && m.field)
                result.penegasanPengirimanInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["PenegasanPengiriman", params.id]]
            return result
        }

        result.penegasanPengirimanInstance = new PenegasanPengiriman(params)

        if (result.penegasanPengirimanInstance.hasErrors() || !result.penegasanPengirimanInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def penegasanPengiriman = PenegasanPengiriman.findById(params.id)
        if (penegasanPengiriman) {
            penegasanPengiriman."${params.name}" = params.value
            penegasanPengiriman.save()
            if (penegasanPengiriman.hasErrors()) {
                throw new Exception("${penegasanPengiriman.errors}")
            }
        } else {
            throw new Exception("PenegasanPengiriman not found")
        }
    }

}