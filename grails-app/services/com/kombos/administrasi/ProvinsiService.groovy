package com.kombos.administrasi



import com.kombos.baseapp.AppSettingParam

class ProvinsiService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Provinsi.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_m001ID"){
				ilike("m001ID","%" + (params."sCriteria_m001ID" as String) + "%")
			}

			if(params."sCriteria_m001NamaProvinsi"){
				ilike("m001NamaProvinsi","%" + (params."sCriteria_m001NamaProvinsi" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}


            eq('staDel', '0')

			switch(sortProperty){
                case "m001ID" :
                    order("id",sortDir)
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m001ID: it.m001ID,
			
						m001NamaProvinsi: it.m001NamaProvinsi,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Provinsi", params.id] ]
			return result
		}

		result.provinsiInstance = Provinsi.get(params.id)

		if(!result.provinsiInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Provinsi", params.id] ]
			return result
		}

		result.provinsiInstance = Provinsi.get(params.id)

		if(!result.provinsiInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Provinsi", params.id] ]
			return result
		}

		result.provinsiInstance = Provinsi.get(params.id)

		if(!result.provinsiInstance)
			return fail(code:"default.not.found.message")

		try {
			result.provinsiInstance.staDel = "1"
			result.provinsiInstance.lastUpdProcess = "DELETE"
            result.provinsiInstance.save(flush: true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
	//	Provinsi.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
			//	status.setRollbackOnly()
				if(result.provinsiInstance && m.field)
					result.provinsiInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Provinsi", params.id] ]
				return result
			}

			result.provinsiInstance = Provinsi.get(params.id)
            def cek2 = Provinsi.createCriteria()
            def result2 = cek2.list() {
                eq("m001NamaProvinsi",params.m001NamaProvinsi, [ignoreCase: true])
                eq("staDel",'0')
            }
            if(result2 &&  Provinsi.findByM001NamaProvinsiIlikeAndStaDel(params.m001NamaProvinsi,'0')?.id != (params.id as Long)){
                return fail(code:"dataGanda")
            }
			if(!result.provinsiInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.provinsiInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.provinsiInstance.properties = params
            result.provinsiInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.provinsiInstance.lastUpdProcess = "UPDATE"



            if(result.provinsiInstance.hasErrors() || !result.provinsiInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

//		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Provinsi", params.id] ]
			return result
		}

		result.provinsiInstance = new Provinsi()
		result.provinsiInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.provinsiInstance && m.field)
				result.provinsiInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Provinsi", params.id] ]
			return result
		}

		result.provinsiInstance = new Provinsi(params)
        result.provinsiInstance.staDel = '0'
        result.provinsiInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.provinsiInstance.lastUpdProcess = "INSERT"

        def cek = result.provinsiInstance.createCriteria()
        def resultCek = cek.list() {
                eq("m001NamaProvinsi",result.provinsiInstance.m001NamaProvinsi?.trim(), [ignoreCase: true])
                eq("staDel",'0')
        }
        if(resultCek){
            return fail(code:"dataGanda")
        }

        if(result.provinsiInstance.hasErrors() || !result.provinsiInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def provinsi =  Provinsi.findById(params.id)
		if (provinsi) {
			provinsi."${params.name}" = params.value
			provinsi.save()
			if (provinsi.hasErrors()) {
				throw new Exception("${provinsi.errors}")
			}
		}else{
			throw new Exception("Provinsi not found")
		}
	}

}