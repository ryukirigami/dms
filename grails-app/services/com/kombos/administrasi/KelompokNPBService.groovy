package com.kombos.administrasi



import com.kombos.baseapp.AppSettingParam

class KelompokNPBService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = KelompokNPB.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_kodeKelompok"){
				ilike("kodeKelompok","%" + (params."sCriteria_kodeKelompok" as String) + "%")
			}

			if(params."sCriteria_namaKelompok"){
				ilike("namaKelompok","%" + (params."sCriteria_namaKelompok" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						kodeKelompok: it.kodeKelompok,
			
						namaKelompok: it.namaKelompok,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["KelompokNPB", params.id] ]
			return result
		}

		result.kelompokNPBInstance = KelompokNPB.get(params.id)

		if(!result.kelompokNPBInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["KelompokNPB", params.id] ]
			return result
		}

		result.kelompokNPBInstance = KelompokNPB.get(params.id)

		if(!result.kelompokNPBInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["KelompokNPB", params.id] ]
			return result
		}

		result.kelompokNPBInstance = KelompokNPB.get(params.id)

		if(!result.kelompokNPBInstance)
			return fail(code:"default.not.found.message")

		try {
			result.kelompokNPBInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		KelompokNPB.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.kelompokNPBInstance && m.field)
					result.kelompokNPBInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["KelompokNPB", params.id] ]
				return result
			}

			result.kelompokNPBInstance = KelompokNPB.get(params.id)

			if(!result.kelompokNPBInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.kelompokNPBInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}


            def cek = KelompokNPB.createCriteria()
            def hasil = cek.list() {
                and{
                    ne("id",Long.parseLong(params.id))
                    or{
                        eq("kodeKelompok",params.kodeKelompok?.trim(), [ignoreCase: true])
                        eq("namaKelompok",params.namaKelompok?.trim(), [ignoreCase: true])
                    }
                }
            }
            if(!hasil){
                result.kelompokNPBInstance.properties = params
                result.kelompokNPBInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                result.kelompokNPBInstance?.lastUpdProcess = "UPDATE"

                if(result.kelompokNPBInstance.hasErrors() || !result.kelompokNPBInstance.save())
                    return fail(code:"default.not.updated.message")
            } else {
                return 'Data has been used.'
            }

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["KelompokNPB", params.id] ]
			return result
		}

		result.kelompokNPBInstance = new KelompokNPB()
		result.kelompokNPBInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.kelompokNPBInstance && m.field)
				result.kelompokNPBInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["KelompokNPB", params.id] ]
			return result
		}

		result.kelompokNPBInstance = new KelompokNPB(params)

        def cek = KelompokNPB.createCriteria()
        def hasil = cek.list() {
            and{
                eq("kodeKelompok",result.kelompokNPBInstance.kodeKelompok?.trim(), [ignoreCase: true])
                eq("namaKelompok",result.kelompokNPBInstance.namaKelompok?.trim(), [ignoreCase: true])
            }
        }

        //find(operationInstance)
        if(!hasil){
            result.kelompokNPBInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.kelompokNPBInstance?.lastUpdProcess = "INSERT"

            if(result.kelompokNPBInstance.hasErrors() || !result.kelompokNPBInstance.save(flush: true))
                return fail(code:"default.not.created.message")

        } else {
            return 'Data has been used.'
        }


		// success
		return result
	}
	
	def updateField(def params){
		def kelompokNPB =  KelompokNPB.findById(params.id)
		if (kelompokNPB) {
			kelompokNPB."${params.name}" = params.value
			kelompokNPB.save()
			if (kelompokNPB.hasErrors()) {
				throw new Exception("${kelompokNPB.errors}")
			}
		}else{
			throw new Exception("KelompokNPB not found")
		}
	}

}