package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam


class BiayaAdministrasiService{
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = BiayaAdministrasi.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params."sCriteria_biaya"){
                eq("biaya",params."sCriteria_biaya".toDouble())
            }

            if(params."sCriteria_tglBerlaku"){
                ge("tglBerlaku",params."sCriteria_tglBerlaku")
                lt("tglBerlaku",params."sCriteria_tglBerlaku" + 1)
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    tglBerlaku: it.tglBerlaku.format("dd/MM/yyyy"),

                    biaya: it.biaya
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BiayaAdministrasi", params.id]]
            return result
        }

        result.biayaAdministrasiInstance = BiayaAdministrasi.get(params.id)

        if (!result.biayaAdministrasiInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BiayaAdministrasi", params.id]]
            return result
        }

        result.biayaAdministrasiInstance = BiayaAdministrasi.get(params.id)

        if (!result.biayaAdministrasiInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BiayaAdministrasi", params.id]]
            return result
        }

        result.biayaAdministrasiInstance = BiayaAdministrasi.get(params.id)

        if (!result.biayaAdministrasiInstance)
            return fail(code: "default.not.found.message")

        try {
            result.biayaAdministrasiInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        BiayaAdministrasi.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.biayaAdministrasiInstance && m.field)
                    result.biayaAdministrasiInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["BiayaAdministrasi", params.id]]
                return result
            }

            result.biayaAdministrasiInstance = BiayaAdministrasi.get(params.id)

            if (!result.biayaAdministrasiInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.biayaAdministrasiInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.biayaAdministrasiInstance.properties = params

            if (result.biayaAdministrasiInstance.hasErrors() || !result.biayaAdministrasiInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BiayaAdministrasi", params.id]]
            return result
        }

        result.biayaAdministrasiInstance = new BiayaAdministrasi()
        result.biayaAdministrasiInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.biayaAdministrasiInstance && m.field)
                result.biayaAdministrasiInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["BiayaAdministrasi", params.id]]
            return result
        }

        result.biayaAdministrasiInstance = new BiayaAdministrasi(params)

        if (result.biayaAdministrasiInstance.hasErrors() || !result.biayaAdministrasiInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def biayaAdministrasi = BiayaAdministrasi.findById(params.id)
        if (biayaAdministrasi) {
            biayaAdministrasi."${params.name}" = params.value
            biayaAdministrasi.save()
            if (biayaAdministrasi.hasErrors()) {
                throw new Exception("${biayaAdministrasi.errors}")
            }
        } else {
            throw new Exception("BiayaAdministrasi not found")
        }
    }

}