package com.kombos.customercomplaint

import com.kombos.administrasi.DealerPenjual
import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.baseapp.AppSettingParam
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.Customer
import grails.converters.JSON
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.*

class ComplaintService {
	boolean transactional = false
    def datatablesUtilService
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Complaint.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params."search_id"){
                eq("id",new Long(params."search_id"))
            }else{

                if(params."sCriteria_customerVehicle"){
                    eq("customerVehicle",params."sCriteria_customerVehicle")
                }

                if(params."sCriteria_t921ID"){
                    ilike("t921ID","%" + (params."sCriteria_t921ID" as String) + "%")
                }

                if (params."sCriteria_t921TglComplain" && params."sCriteria_t921TglComplainAkhir") {
                    ge("t921TglComplain", params."sCriteria_t921TglComplain")
                    lt("t921TglComplain", params."sCriteria_t921TglComplainAkhir" + 1)
                }
                if(params."sCriteria_historyCustomerVehicle"){
                    eq("historyCustomerVehicle",params."sCriteria_historyCustomerVehicle")
                }

                if(params."sCriteria_customer"){
                    eq("customer",params."sCriteria_customer")
                }

                if(params."sCriteria_historyCustomer"){
                    eq("historyCustomer",params."sCriteria_historyCustomer")
                }

                if(params."sCriteria_t921Km"){
                    eq("t921Km",params."sCriteria_t921Km")
                }

                if(params."sCriteria_t921StaPakaiKendaraan"){
                    ilike("t921StaPakaiKendaraan","%" + (params."sCriteria_t921StaPakaiKendaraan" as String) + "%")
                }

                if(params."sCriteria_t921Peran"){
                    ilike("t921Peran","%" + (params."sCriteria_t921Peran" as String) + "%")
                }

                if(params."sCriteria_t921NamaPelanggan"){
                    ilike("t921NamaPelanggan","%" + (params."sCriteria_t921NamaPelanggan" as String) + "%")
                }

                if(params."sCriteria_t921TelpRumah"){
                    ilike("t921TelpRumah","%" + (params."sCriteria_t921TelpRumah" as String) + "%")
                }

                if(params."sCriteria_t921TelpHp"){
                    ilike("t921TelpHp","%" + (params."sCriteria_t921TelpHp" as String) + "%")
                }

                if(params."sCriteria_t921Fax"){
                    ilike("t921Fax","%" + (params."sCriteria_t921Fax" as String) + "%")
                }

                if(params."sCriteria_t921Email"){
                    ilike("t921Email","%" + (params."sCriteria_t921Email" as String) + "%")
                }

                if(params."sCriteria_t921Alamat"){
                    ilike("t921Alamat","%" + (params."sCriteria_t921Alamat" as String) + "%")
                }

                if(params."sCriteria_t921ProfilPelanggan"){
                    ilike("t921ProfilPelanggan","%" + (params."sCriteria_t921ProfilPelanggan" as String) + "%")
                }

                if(params."sCriteria_t921NoReff"){
                    ilike("t921NoReff","%" + (params."sCriteria_t921NoReff" as String) + "%")
                }

                if(params."sCriteria_t921TglKasusDiterima"){
                    ge("t921TglKasusDiterima",params."sCriteria_t921TglKasusDiterima")
                    lt("t921TglKasusDiterima",params."sCriteria_t921TglKasusDiterima" + 1)
                }

                if(params."sCriteria_t921TglLPKDikirim"){
                    ge("t921TglLPKDikirim",params."sCriteria_t921TglLPKDikirim")
                    lt("t921TglLPKDikirim",params."sCriteria_t921TglLPKDikirim" + 1)
                }

                if(params."sCriteria_t921StaMediaKeluhan"){
                    ilike("t921StaMediaKeluhan","%" + (params."sCriteria_t921StaMediaKeluhan" as String) + "%")
                }

                if(params."sCriteria_t921AreaKeluhan"){
                    ilike("t921AreaKeluhan","%" + (params."sCriteria_t921AreaKeluhan" as String) + "%")
                }

                if(params."sCriteria_t921DealerPembelian"){
                    ilike("t921DealerPembelian","%" + (params."sCriteria_t921DealerPembelian" as String) + "%")
                }

                if(params."sCriteria_t921DealerKomplain"){
                    ilike("t921DealerKomplain","%" + (params."sCriteria_t921DealerKomplain" as String) + "%")
                }

                if(params."sCriteria_t921DealerDiKomplain"){
                    ilike("t921DealerDiKomplain","%" + (params."sCriteria_t921DealerDiKomplain" as String) + "%")
                }

                if(params."sCriteria_t921StaKategoriKeluhan"){
                    ilike("t921StaKategoriKeluhan","%" + (params."sCriteria_t921StaKategoriKeluhan" as String) + "%")
                }

                if(params."sCriteria_t921Keluhan"){
                    ilike("t921Keluhan","%" + (params."sCriteria_t921Keluhan" as String) + "%")
                }

                if(params."sCriteria_t921Permintaan"){
                    ilike("t921Permintaan","%" + (params."sCriteria_t921Permintaan" as String) + "%")
                }

                if(params."sCriteria_t921LatarBelakangTgl"){
                    ge("t921LatarBelakangTgl",params."sCriteria_t921LatarBelakangTgl")
                    lt("t921LatarBelakangTgl",params."sCriteria_t921LatarBelakangTgl" + 1)
                }

                if(params."sCriteria_t921LatarBelakangPihakTerlibat"){
                    ilike("t921LatarBelakangPihakTerlibat","%" + (params."sCriteria_t921LatarBelakangPihakTerlibat" as String) + "%")
                }

                if(params."sCriteria_t921LatarBelakangFakta"){
                    ilike("t921LatarBelakangFakta","%" + (params."sCriteria_t921LatarBelakangFakta" as String) + "%")
                }

                if(params."sCriteria_t921SebabPelanggan"){
                    ilike("t921SebabPelanggan","%" + (params."sCriteria_t921SebabPelanggan" as String) + "%")
                }

                if(params."sCriteria_t921SebabKendaraan"){
                    ilike("t921SebabKendaraan","%" + (params."sCriteria_t921SebabKendaraan" as String) + "%")
                }

                if(params."sCriteria_t921SebabDealer"){
                    ilike("t921SebabDealer","%" + (params."sCriteria_t921SebabDealer" as String) + "%")
                }

                if(params."sCriteria_t921StaPenanganan"){
                    ilike("t921StaPenanganan","%" + (params."sCriteria_t921StaPenanganan" as String) + "%")
                }

                if(params."sCriteria_t921StrategiPenyelesaian"){
                    ilike("t921StrategiPenyelesaian","%" + (params."sCriteria_t921StrategiPenyelesaian" as String) + "%")
                }

                if(params."sCriteria_t921TindakanTgl"){
                    ge("t921TindakanTgl",params."sCriteria_t921TindakanTgl")
                    lt("t921TindakanTgl",params."sCriteria_t921TindakanTgl" + 1)
                }

                if(params."sCriteria_t921TindakanPihakTerlibat"){
                    ilike("t921TindakanPihakTerlibat","%" + (params."sCriteria_t921TindakanPihakTerlibat" as String) + "%")
                }

                if(params."sCriteria_t921TindakanFakta"){
                    ilike("t921TindakanFakta","%" + (params."sCriteria_t921TindakanFakta" as String) + "%")
                }

                if(params."sCriteria_t921StaSolusi"){
                    ilike("t921StaSolusi","%" + (params."sCriteria_t921StaSolusi" as String) + "%")
                }

                if(params."sCriteria_t921StaKepuasanPelanggan"){
                    ilike("t921StaKepuasanPelanggan","%" + (params."sCriteria_t921StaKepuasanPelanggan" as String) + "%")
                }

                if(params."sCriteria_t921AlasanKepuasan"){
                    ilike("t921AlasanKepuasan","%" + (params."sCriteria_t921AlasanKepuasan" as String) + "%")
                }

                if(params."sCriteria_t921TglPenyelesaianKeluhan"){
                    ge("t921TglPenyelesaianKeluhan",params."sCriteria_t921TglPenyelesaianKeluhan")
                    lt("t921TglPenyelesaianKeluhan",params."sCriteria_t921TglPenyelesaianKeluhan" + 1)
                }

                if(params."sCriteria_t921StaLangkahPencegahan"){
                    ilike("t921StaLangkahPencegahan","%" + (params."sCriteria_t921StaLangkahPencegahan" as String) + "%")
                }

                if(params."sCriteria_t921TextLangkahPencegahan"){
                    ilike("t921TextLangkahPencegahan","%" + (params."sCriteria_t921TextLangkahPencegahan" as String) + "%")
                }

                if(params."sCriteria_lastUpdProcess"){
                    ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
                }
            }
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						customerVehicle: it.customerVehicle.t103VinCode,
			
						t921ID: it.t921ID,
			
						t921TglComplain: it.t921TglComplain?it.t921TglComplain.format(dateFormat):"",
			
						historyCustomerVehicle: ""+it.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.historyCustomerVehicle.t183NoPolTengah+" "+it.historyCustomerVehicle.t183NoPolBelakang,
			
						customer: it.customer,
			
						historyCustomer: it.historyCustomer,
			
						t921Km: it.t921Km,
			
						t921StaPakaiKendaraan: it.t921StaPakaiKendaraan=='n'?"New":(it.t921StaPakaiKendaraan=='u'?"Used":(it.t921StaPakaiKendaraan=='s'?"Sewa":"")),
			
						t921Peran: it.t921Peran,
			
						t921NamaPelanggan: it.t921NamaPelanggan,
			
						t921TelpRumah: it.t921TelpRumah,
			
						t921TelpHp: it.t921TelpHp,
			
						t921Fax: it.t921Fax,
			
						t921Email: it.t921Email,
			
						t921Alamat: it.t921Alamat,
			
						t921ProfilPelanggan: it.t921ProfilPelanggan,
			
						t921NoReff: it.t921NoReff,
			
						t921TglKasusDiterima: it.t921TglKasusDiterima?it.t921TglKasusDiterima.format(dateFormat):"",
			
						t921TglLPKDikirim: it.t921TglLPKDikirim?it.t921TglLPKDikirim.format(dateFormat):"",
			
						t921StaMediaKeluhan: it.t921StaMediaKeluhan!=null?(it.t921StaMediaKeluhan=="T"?"Telepon":(it.t921StaMediaKeluhan=="S"?"SMS":"Email")):"",
			
						t921AreaKeluhan: it.t921AreaKeluhan!=null?(it.t921AreaKeluhan=="1"?"Product":(it.t921AreaKeluhan=="2"?"Sales":"Service & Parts")):"" ,

						t921DealerPembelian: it.t921DealerPembelian,
			
						t921DealerKomplain: it.t921DealerKomplain,
			
						t921DealerDiKomplain: it.t921DealerDiKomplain,
			
						t921StaKategoriKeluhan: it.t921StaKategoriKeluhan=='s'?"Serius":"Normal",
			
						t921Keluhan: it.t921Keluhan,
			
						t921Permintaan: it.t921Permintaan,
			
						t921LatarBelakangTgl: it.t921LatarBelakangTgl?it.t921LatarBelakangTgl.format(dateFormat):"",
			
						t921LatarBelakangPihakTerlibat: it.t921LatarBelakangPihakTerlibat,
			
						t921LatarBelakangFakta: it.t921LatarBelakangFakta,
			
						t921SebabPelanggan: it.t921SebabPelanggan,
			
						t921SebabKendaraan: it.t921SebabKendaraan,
			
						t921SebabDealer: it.t921SebabDealer,
			
						t921StaPenanganan: it.t921StaPenanganan=='n'?"Not Yet Respond":(it.t921StaPenanganan=='i'?"In Process":(it.t921StaPenanganan=='s'?"Solved":"Closed")),
			
						t921StrategiPenyelesaian: it.t921StrategiPenyelesaian,
			
						t921TindakanTgl: it.t921TindakanTgl?it.t921TindakanTgl.format(dateFormat):"",
			
						t921TindakanPihakTerlibat: it.t921TindakanPihakTerlibat,
			
						t921TindakanFakta: it.t921TindakanFakta,
			
						t921StaSolusi: it.t921StaSolusi=='w'?"Warranty":(it.t921StaSolusi=='g'?"Goodwill":(it.t921StaSolusi=='t'?"Tidak Ada":"")),

						t921StaKepuasanPelanggan: it.t921StaKepuasanPelanggan,
			
						t921AlasanKepuasan: it.t921AlasanKepuasan,
			
						t921TglPenyelesaianKeluhan: it.t921TglPenyelesaianKeluhan?it.t921TglPenyelesaianKeluhan.format(dateFormat):"",
			
						t921StaLangkahPencegahan: it.t921StaLangkahPencegahan,
			
						t921TextLangkahPencegahan: it.t921TextLangkahPencegahan,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}

    def datatablesListUpload(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PathComplainDoc.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params.complaintId){
                eq("complaint",Complaint.findById(new Long(params.complaintId)))
            }  else
            if(params.sessionId)
            {
                eq("sessionIdCreation", params.sessionId)//should return zero data
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t922NamaFile : it.t922NamaFile,

                    t922Ket : it.t922Ket



            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def findNoPol(params){
//        def historyCVInstance = new HistoryCustomerVehicle()
//        historyCVInstance.properties = params
//        historyCVInstance = HistoryCustomerVehicle.findByKodeKotaNoPolAndT183NoPolTengahAndT183NoPolBelakang(KodeKotaNoPol.findById(Long.parseLong(params.kode)),
//                            params.tengah, params.belakang)
        def historyCVInstance = HistoryCustomerVehicle.createCriteria()
        def results = historyCVInstance.list() {
            eq("kodeKotaNoPol",KodeKotaNoPol.findById(Long.parseLong(params.kode)))
            ilike("t183NoPolTengah","%"+params.tengah+"%")
            ilike("t183NoPolBelakang","%"+params.belakang+"%")
        }

        def rows = []

        results.each {
            rows << [

                    id : it.id,

                    t183NamaSTNK : it.t183NamaSTNK,

                    t183AlamatSTNK : it.t183AlamatSTNK,

                    customerVehicle : it.customerVehicle.t103VinCode,

                    fullModelCode : it.fullModelCode.t110FullModelCode,

                    fullModelCodeBaseModel : it.fullModelCode.baseModel.m102KodeBaseModel,

                    fullModelCodeGear : it.fullModelCode.gear.m106NamaGear,

                    warna : it.warna.m092NamaWarna,

                    t183ThnBlnRakit : it.t183ThnBlnRakit,

                    t183TglDEC : it.t183TglDEC
            ]
        }

        return rows
    }

    def uploadPathComplainDoc(params, def gambarParams){
        def pathComplainDocInstance = new PathComplainDoc()
        //println gambarParams
//        pathComplainDocInstance.staDel = '0'
        pathComplainDocInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        pathComplainDocInstance?.lastUpdProcess = "INSERT"

        def gambar = gambarParams
        def notAllowCont = ['application/octet-stream']
        if(gambar !=null && !notAllowCont.contains(gambar.getContentType()) ){
            log.info('Logo ada')
            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
//            if (! okcontents.contains(gambar.getContentType())) {
//                flash.message = "Jenis gambar harus berupa: ${okcontents}"
//                render(view:'create', model:[pathComplainDocInstance: pathComplainDocInstance])
//                return;
//            }

            pathComplainDocInstance?.t922Gambar = gambar.getBytes()
            pathComplainDocInstance?.t922Ket = params.t922Ket
//            pathComplainDocInstance.imageMime = gambar.getContentType()
        }


//        if (!pathComplainDocInstance.save(flush: true)) {
//            render(view: "create", model: [pathComplainDocInstance: pathComplainDocInstance])
//            return
//        }
//
//        flash.message = message(code: 'default.created.message', args: [message(code: 'pathComplainDoc.label', default: 'CompanyDealer'), pathComplainDocInstance.id])
//        redirect(action: "show", id: pathComplainDocInstance.id)
    }

    def delPath(params){
        //println "sercice"
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def hapus = new PathComplainDoc()
            hapus= PathComplainDoc.get(it)
            hapus.delete()
        }
        return "ok"
    }

    def tambahDealer(params){
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << DealerPenjual.get(it)  }
        int a=1
        String hasil=""
        oList.each{
            def dealer=new DealerPenjual()
            dealer=it
            if(a==1){
                hasil+=dealer.m091NamaDealer
            }else{
                hasil+=","+dealer.m091NamaDealer
            }
            a++
        }
        return hasil
    }
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Complaint", params.id] ]
			return result
		}

		result.complaintInstance = Complaint.get(params.id)

		if(!result.complaintInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Complaint", params.id] ]
			return result
		}

		result.complaintInstance = Complaint.get(params.id)

		if(!result.complaintInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Complaint", params.id] ]
			return result
		}

		result.complaintInstance = Complaint.get(params.id)

		if(!result.complaintInstance)
			return fail(code:"default.not.found.message")

		try {
//			result.complaintInstance.delete(flush:true)
            result.complaintInstance.staDel = "1";
            result.complaintInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.complaintInstance.lastUpdProcess = "DELETE"
            result.complaintInstance.save(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		Complaint.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.complaintInstance && m.field)
					result.complaintInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Complaint", params.id] ]
				return result
			}

			result.complaintInstance = Complaint.get(params.id)

			if(!result.complaintInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.complaintInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

            params.customerVehicle = (params.customerVehicle==null||params.customerVehicle=="")?null:CustomerVehicle.findByT103VinCode(params.customerVehicle)
            params.historyCustomerVehicle = (params.customerVehicle==null||params.customerVehicle=="")?null:HistoryCustomerVehicle.findById(new Long(params.historyCustomerVehicle))

            result.complaintInstance.properties = params

			if(result.complaintInstance.hasErrors() || !result.complaintInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Complaint", params.id] ]
			return result
		}

		result.complaintInstance = new Complaint()
		result.complaintInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.complaintInstance && m.field)
				result.complaintInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Complaint", params.id] ]
			return result
		}

        CustomerVehicle custInstance
        custInstance = CustomerVehicle.findByT103VinCode(params.customerVehicle)

        Customer customer

        HistoryCustomer historyCustomer

        HistoryCustomerVehicle historyCustomerVehicle
        historyCustomerVehicle = HistoryCustomerVehicle.findById(params.historyCustomerVehicle)

        def complaintInstance = new Complaint()
        complaintInstance?.t921TglComplain = new Date()
        complaintInstance?.t921NoReff = params.t921NoReff
        complaintInstance?.t921StaMediaKeluhan = params.t921StaMediaKeluhan
        complaintInstance?.customerVehicle = custInstance
        complaintInstance?.t921DealerDiKomplain = params.t921DealerDiKomplain
        complaintInstance?.historyCustomerVehicle = historyCustomerVehicle
        complaintInstance?.t921StaKategoriKeluhan = params.t921StaKategoriKeluhan
        complaintInstance?.t921AreaKeluhan = params.t921AreaKeluhan
        complaintInstance?.t921NamaPelanggan = params.t921NamaPelanggan
        complaintInstance?.customer = customer
        complaintInstance?.historyCustomer = historyCustomer
        complaintInstance?.t921Km = Integer.parseInt(((params.t921Km==null||params.t921Km=="")?"0":params.t921Km))
        complaintInstance?.t921StaPakaiKendaraan = params.t921StaPakaiKendaraan
        complaintInstance?.t921Peran = params.t921Peran
        complaintInstance?.t921TelpRumah = params.t921TelpRumah
        complaintInstance?.t921TelpHp = params.t921TelpHp
        complaintInstance?.t921Fax = params.t921Fax
        complaintInstance?.t921Email = params.t921Email
        complaintInstance?.t921Alamat = params.t921Alamat
        complaintInstance?.t921ProfilPelanggan = params.t921ProfilPelanggan
        complaintInstance?.t921TglKasusDiterima = params.t921TglKasusDiterima
        complaintInstance?.t921TglLPKDikirim = params.t921TglLPKDikirim
        complaintInstance?.t921DealerPembelian = params.t921DealerPembelian
        complaintInstance?.t921DealerKomplain = params.t921DealerKomplain
        complaintInstance?.t921Keluhan = params.t921Keluhan
        complaintInstance?.t921Permintaan = params.t921Permintaan
        complaintInstance?.t921LatarBelakangTgl = params.t921LatarBelakangTgl
        complaintInstance?.t921LatarBelakangPihakTerlibat = params.t921LatarBelakangPihakTerlibat
        complaintInstance?.t921LatarBelakangFakta = params.t921LatarBelakangFakta
        complaintInstance?.t921SebabPelanggan = params.t921SebabPelanggan
        complaintInstance?.t921SebabDealer = params.t921SebabDealer
        complaintInstance?.t921StaPenanganan = params.t921StaPenanganan
        complaintInstance?.t921StrategiPenyelesaian = params.t921StrategiPenyelesaian
        complaintInstance?.t921TindakanTgl = params.t921TindakanTgl
        complaintInstance?.t921TindakanPihakTerlibat = params.t921TindakanPihakTerlibat
        complaintInstance?.t921TindakanFakta = params.t921TindakanFakta
        complaintInstance?.t921StaSolusi = params.t921StaSolusi
        complaintInstance?.t921StaKepuasanPelanggan = params.t921StaKepuasanPelanggan
        complaintInstance?.t921AlasanKepuasan = params.t921AlasanKepuasan
        complaintInstance?.t921TglPenyelesaianKeluhan = params.t921TglPenyelesaianKeluhan
        complaintInstance?.t921StaLangkahPencegahan = params.t921StaLangkahPencegahan
        complaintInstance?.t921TextLangkahPencegahan = params.t921TextLangkahPencegahan
        complaintInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        complaintInstance?.lastUpdProcess = "INSERT"
        complaintInstance?.staDel = '0'
        complaintInstance?.dateCreated = datatablesUtilService?.syncTime()
        complaintInstance?.lastUpdated = datatablesUtilService?.syncTime()
        complaintInstance.save(flush: true)
//        complaintInstance.errors.each{ //println it }

//        if(complaintInstance.errors)
//			return fail(code:"default.not.created.message")

		// success
        if(complaintInstance.errors==null){
            PathComplainDoc.executeUpdate("update PathComplainDoc a set a.complaint = :complaint where a.sessionIdCreation = :sessionId", [complaint : complaintInstance,sessionId : params.sessionId])

            PathComplainDoc.executeUpdate("update PathComplainDoc a set a.sessionIdCreation = '' where a.complaint = :complaint", [complaint : complaintInstance])
        }
        return complaintInstance
	}
	
	def updateField(def params){
		def complaint =  Complaint.findById(params.id)
		if (complaint) {
			complaint."${params.name}" = params.value
			complaint.save()
			if (complaint.hasErrors()) {
				throw new Exception("${complaint.errors}")
			}
		}else{
			throw new Exception("Complaint not found")
		}
	}

    private File createReport(def list) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        WorkbookSettings workbookSettings = new WorkbookSettings()
        workbookSettings.locale = Locale.default

        def file = File.createTempFile('myExcelDocument', '.xls')
        file.deleteOnExit()

        WritableWorkbook workbook = Workbook.createWorkbook(file, workbookSettings)

        WritableFont font = new WritableFont(WritableFont.ARIAL, 12)
        WritableCellFormat format = new WritableCellFormat(font)

        def row = 0
        WritableSheet sheet = workbook.createSheet('MySheet', 0)

        sheet.addCell(new Label(0, row, "ID", format))
        sheet.addCell(new Label(1, row, "VinCode", format))
        sheet.addCell(new Label(2, row, "Tanggal Komplain", format))
        sheet.addCell(new Label(3, row, "History Customer Vehicle", format))
        sheet.addCell(new Label(4, row, "Pelanggan", format))
        sheet.addCell(new Label(5, row, "History Customer", format))
        sheet.addCell(new Label(6, row, "Odometer", format))
        sheet.addCell(new Label(7, row, "Status Pakai Kendaraan", format))
        sheet.addCell(new Label(8, row, "Peran", format))
        sheet.addCell(new Label(9, row, "Nama Pelanggan", format))
        sheet.addCell(new Label(10, row, "Telepon Rumah", format))
        sheet.addCell(new Label(11, row, "Telepon Selular", format))
        sheet.addCell(new Label(12, row, "Fax", format))
        sheet.addCell(new Label(13, row, "Email", format))
        sheet.addCell(new Label(14, row, "Alamat", format))
        sheet.addCell(new Label(15, row, "Profil Pelanggan", format))
        sheet.addCell(new Label(16, row, "No Referensi", format))
        sheet.addCell(new Label(17, row, "Tanggal Kasus Diterima", format))
        sheet.addCell(new Label(18, row, "Tanggal LPK Dikirim", format))
        sheet.addCell(new Label(19, row, "Media Keluhan", format))
        sheet.addCell(new Label(20, row, "Area Keluhan", format))
        sheet.addCell(new Label(21, row, "Dealer Tempat Pembelian", format))
        sheet.addCell(new Label(22, row, "Dealer Tempat Komplain", format))
        sheet.addCell(new Label(23, row, "Dealer Yang Dikomplain", format))
        sheet.addCell(new Label(24, row, "Status Kategori Keluhan", format))
        sheet.addCell(new Label(25, row, "Keluhan", format))
        sheet.addCell(new Label(26, row, "Permintaan", format))
        sheet.addCell(new Label(27, row, "Tanggal Latar Belakang", format))
        sheet.addCell(new Label(28, row, "Latar BelakangPihak Terlibat", format))
        sheet.addCell(new Label(29, row, "Latar Belakang Fakta", format))
        sheet.addCell(new Label(30, row, "Sebab Pelanggan", format))
        sheet.addCell(new Label(31, row, "Sebab Kendaraan", format))
        sheet.addCell(new Label(32, row, "Sebab Dealer", format))
        sheet.addCell(new Label(33, row, "Status Penanganan", format))
        sheet.addCell(new Label(34, row, "Strategi Penyelesaian", format))
        sheet.addCell(new Label(35, row, "Tanggal Tindakan", format))
        sheet.addCell(new Label(36, row, "Tindakan Pihak Terlibat", format))
        sheet.addCell(new Label(37, row, "Tindakan Fakta", format))
        sheet.addCell(new Label(38, row, "Status Solusi", format))
        sheet.addCell(new Label(39, row, "Status Kepuasan Pelanggan", format))
        sheet.addCell(new Label(40, row, "Alasan Kepuasan", format))
        sheet.addCell(new Label(41, row, "Tanggal Penyelesaian Keluhan", format))
        sheet.addCell(new Label(42, row, "Status Langkah Pencegahan", format))
        sheet.addCell(new Label(43, row, "Langkah Pencegahan", format))
        sheet.addCell(new Label(44, row, "Last Update Process", format))

        list.each {
            row++
            sheet.addCell(new Label(0, row, ""+it.id, format))
            sheet.addCell(new Label(1, row, ""+it.customerVehicle.t103VinCode, format))
            sheet.addCell(new Label(2, row, ""+(it.t921TglComplain?it.t921TglComplain.format(dateFormat):""), format))
            sheet.addCell(new Label(3, row, ""+it.historyCustomerVehicle, format))
            sheet.addCell(new Label(4, row, ""+it.customer, format))
            sheet.addCell(new Label(5, row, ""+it.historyCustomer, format))
            sheet.addCell(new Label(6, row, ""+it.t921Km, format))
            sheet.addCell(new Label(7, row, ""+it.t921StaPakaiKendaraan, format))
            sheet.addCell(new Label(8, row, ""+it.t921Peran, format))
            sheet.addCell(new Label(9, row, ""+it.t921NamaPelanggan, format))
            sheet.addCell(new Label(10, row, ""+it.t921TelpRumah, format))
            sheet.addCell(new Label(11, row, ""+it.t921TelpHp, format))
            sheet.addCell(new Label(12, row, ""+it.t921Fax, format))
            sheet.addCell(new Label(13, row, ""+it.t921Email, format))
            sheet.addCell(new Label(14, row, ""+it.t921Alamat, format))
            sheet.addCell(new Label(15, row, ""+it.t921ProfilPelanggan, format))
            sheet.addCell(new Label(16, row, ""+it.t921NoReff, format))
            sheet.addCell(new Label(17, row, ""+(it.t921TglKasusDiterima?it.t921TglKasusDiterima.format(dateFormat):""), format))
            sheet.addCell(new Label(18, row, ""+(it.t921TglLPKDikirim?it.t921TglLPKDikirim.format(dateFormat):""), format))
            sheet.addCell(new Label(19, row, ""+(it.t921StaMediaKeluhan!=null?(it.t921StaMediaKeluhan=="T"?"Telepon":(it.t921StaMediaKeluhan=="S"?"SMS":"Email")):""), format))
            sheet.addCell(new Label(20, row, ""+(it.t921AreaKeluhan!=null?(it.t921AreaKeluhan=="1"?"Product":(it.t921AreaKeluhan=="2"?"Sales":"Service & Parts")):""), format))
            sheet.addCell(new Label(21, row, ""+it.t921DealerPembelian, format))
            sheet.addCell(new Label(22, row, ""+it.t921DealerKomplain, format))
            sheet.addCell(new Label(23, row, ""+it.t921DealerDiKomplain, format))
            sheet.addCell(new Label(24, row, ""+it.t921StaKategoriKeluhan, format))
            sheet.addCell(new Label(25, row, ""+it.t921Keluhan, format))
            sheet.addCell(new Label(26, row, ""+it.t921Permintaan, format))
            sheet.addCell(new Label(27, row, ""+(it.t921LatarBelakangTgl!=null?it.t921LatarBelakangTgl.format(dateFormat):""), format))
            sheet.addCell(new Label(28, row, ""+it.t921LatarBelakangPihakTerlibat, format))
            sheet.addCell(new Label(29, row, ""+it.t921LatarBelakangFakta, format))
            sheet.addCell(new Label(30, row, ""+it.t921SebabPelanggan, format))
            sheet.addCell(new Label(31, row, ""+it.t921SebabKendaraan, format))
            sheet.addCell(new Label(32, row, ""+it.t921SebabDealer, format))
            sheet.addCell(new Label(33, row, ""+it.t921StaPenanganan, format))
            sheet.addCell(new Label(34, row, ""+it.t921StrategiPenyelesaian, format))
            sheet.addCell(new Label(35, row, ""+(it.t921TindakanTgl!=null?it.t921TindakanTgl.format(dateFormat):""), format))
            sheet.addCell(new Label(36, row, ""+it.t921TindakanPihakTerlibat, format))
            sheet.addCell(new Label(37, row, ""+it.t921TindakanFakta, format))
            sheet.addCell(new Label(38, row, ""+it.t921StaSolusi, format))
            sheet.addCell(new Label(39, row, ""+it.t921StaKepuasanPelanggan, format))
            sheet.addCell(new Label(40, row, ""+it.t921AlasanKepuasan, format))
            sheet.addCell(new Label(41, row, ""+(it.t921TglPenyelesaianKeluhan!=null?it.t921TglPenyelesaianKeluhan.format(dateFormat):""), format))
            sheet.addCell(new Label(42, row, ""+it.t921StaLangkahPencegahan, format))
            sheet.addCell(new Label(43, row, ""+it.t921TextLangkahPencegahan, format))
            sheet.addCell(new Label(44, row, ""+it.lastUpdProcess, format))
        }

        workbook.write()
        workbook.close()
        return file
    }

}
