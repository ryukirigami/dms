package com.kombos.board

import com.kombos.administrasi.*
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.hrd.AbsensiKaryawanDetail
import com.kombos.hrd.CutiKaryawan
import com.kombos.maintable.JobApp
import com.kombos.maintable.StatusActual
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import groovy.time.TimeCategory
import org.codenarc.rule.logging.PrintlnAstVisitor
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class JPBService {

    public static final int FIFTEEN = 15
    public static final int THIRTY = 30
    public static final int FOURTY_FIVE = 45

    public static final String SZERO = "00"
    public static final String SFIFTEEN = "15"
    public static final String STHIRTY = "30"
    public static final String SFOURTY_FIVE = "45"

    public static final int JAM7 = 420
    public static final int JAM730 = 450
    public static final int JAM715 = 435
    public static final int JAM745 = 465
    public static final int JAM8 = 480
    public static final int JAM830 = 510
    public static final int JAM815 = 495
    public static final int JAM845 = 525
    public static final int JAM9 = 540
    public static final int JAM930 = 570
    public static final int JAM915 = 555
    public static final int JAM945 = 585
    public static final int JAM10 = 600
    public static final int JAM1030 = 630
    public static final int JAM1015 = 615
    public static final int JAM1045 = 645
    public static final int JAM11 = 660
    public static final int JAM1130 = 690
    public static final int JAM1115 = 675
    public static final int JAM1145 = 705
    public static final int JAM12 = 720
    public static final int JAM1230 = 750
    public static final int JAM1215 = 735
    public static final int JAM1245 = 765
    public static final int JAM13 = 780
    public static final int JAM1330 = 810
    public static final int JAM1315 = 795
    public static final int JAM1345 = 825
    public static final int JAM14 = 840
    public static final int JAM1430 = 870
    public static final int JAM1415 = 855
    public static final int JAM1445 = 885
    public static final int JAM15 = 900
    public static final int JAM1530 = 930
    public static final int JAM1515 = 915
    public static final int JAM1545 = 945
    public static final int JAM16 = 960
    public static final int JAM1630 = 990
    public static final int JAM1615 = 975
    public static final int JAM1645 = 1005
    public static final int JAM17 = 1020

    def datatablesUtilService = new DatatablesUtilService()

    def loadAllManPowerStall(CompanyDealer companyDealer, def params){
        def res = []
        def c = ManPowerStall.createCriteria()
        def tanggalView = new Date().clearTime()
        if (params."sCriteria_tglView") {
            tanggalView = params."sCriteria_tglView"
            tanggalView.clearTime()
        }

        def listAbsen = AbsensiKaryawanDetail.createCriteria().list {
            eq("staDel","0")
            eq("status","1");
            ge("tanggal",tanggalView)
            lt("tanggal",tanggalView+1)
            terminal{
                eq("companyDealer",companyDealer)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            absensiKaryawan{
                karyawan{
                    projections {
                        groupProperty("nomorPokokKaryawan", "nomorPokokKaryawan")
                    }
                }
            }
        }

        def listNoPresent = CutiKaryawan.createCriteria().list {
            eq("staDel","0")
            ge("tanggalAkhirCuti",tanggalView)
            eq("companyDealer",companyDealer)
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            karyawan{
                projections {
                    groupProperty("nomorPokokKaryawan", "nomorPokokKaryawan")
                }
            }
        }

        def results = c.list () {
            le("t019Tanggal", tanggalView)
            eq("companyDealer", companyDealer)
            eq("staDel","0")
            stall{
                jenisStall{
                    if(params.jenis){
                        if(params.jenis.contains("GR")){
                            not{
                                ilike("m021NamaJenisStall","%BP%")
                            }
                        }else{
                            ilike("m021NamaJenisStall","%BP%")
                        }
                    }
                }
            }
            namaManPower {
                if (params?.jenis && params?.jenis?.toString()?.contains("BP") || params?.jenis && params?.jenis?.toString()?.contains("GR")){
                    if (listAbsen.size() > 0) {
                        inList("t015IdManPower", listAbsen?.nomorPokokKaryawan)
                    } else {
                        eq("id", -10000.toLong())
                    }
                    if (listNoPresent.size() > 0) {
                        not {
                            inList("t015IdManPower", listNoPresent?.nomorPokokKaryawan)
                        }
                    }
                }

                groupManPower{
                    eq("staDel","0");
                    namaManPowerForeman{
                        order("t015NamaBoard");
                    }
                    order("t021Group");
                }
            }
        }

        results.each { ManPowerStall mps ->
            ASBLine asbLine = new ASBLine()
            asbLine.foremanId = mps?.namaManPower?.groupManPower?.namaManPowerForeman?.id
            asbLine.foreman= mps?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard
            asbLine.groupId= mps.namaManPower?.groupManPower?.id
            //asbLine.asbId = mps.id
            asbLine.teknisiId= mps.namaManPower?.id
            asbLine.group= mps.namaManPower?.groupManPower?.t021Group
            asbLine.teknisi= mps.namaManPower?.t015NamaBoard
            asbLine.stall= mps.stall?.m022NamaStall
            asbLine.stallId= mps.id
            asbLine.date = tanggalView
            res << asbLine
            if(params.aksi && params.aksi.toString().equalsIgnoreCase("view")){
                ASBLine asbActual = new ASBLine()
                asbActual.foremanId = mps?.namaManPower?.groupManPower?.namaManPowerForeman?.id
                asbActual.foreman= mps?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard
                asbActual.groupId= mps.namaManPower?.groupManPower?.id
                asbActual.actual = true
                asbActual.teknisiId= mps.namaManPower?.id
                asbActual.group= mps.namaManPower?.groupManPower?.t021Group
                asbActual.teknisi= mps.namaManPower?.t015NamaBoard
                asbActual.stall= mps.stall?.m022NamaStall
                asbActual.stallId= mps.id
                asbActual.date = tanggalView
                res << asbActual
            }
        }
        res
    }

    def showJPB(List<JPB> results, def manPowerStalls, String aksi,String dari, boolean mechanic) {
        def alfabet = "V"
        def alfabet1 = "V1"
        DateFormat hh = new SimpleDateFormat("HH")
        DateFormat mm = new SimpleDateFormat("mm")
        results.each { JPB jpb ->
            def start = true
            def aStart = true
            int jamSkrg = datatablesUtilService?.syncTime()?.format("HH")?.toInteger()
            int mntSkrg = datatablesUtilService?.syncTime()?.format("mm")?.toInteger()
            int jamMenitSkrg = jamSkrg * 60 + mntSkrg
            manPowerStalls.each { ASBLine asbLine ->
                if (jpb.stall.m022NamaStall.equals(asbLine.stall)) {
                    Reception rec = jpb.reception
                    if(!aksi.equalsIgnoreCase("input")){
                        if(aksi=="viewASBJPB"){
                            def jobRcp = JobApp.findAllByReceptionAndT302StaDel(rec,"0")
                            jobRcp.each { JobApp job ->
                                if (job.operation) {
                                    alfabet = 'V' + job.operation.kategoriJob.m055KategoriJob
                                    alfabet1 = 'V1' + job.operation.kategoriJob.m055KategoriJob
                                }
                            }
                        }else{
                            def jobRcp = JobRCP.findAllByReceptionAndStaDel(rec,"0")
                            jobRcp.each { JobRCP job ->
                                if (job.operation) {
                                    alfabet = 'V' + job.operation.kategoriJob.m055KategoriJob
                                    alfabet1 = 'V1' + job.operation.kategoriJob.m055KategoriJob
                                }
                            }
                            if(jobRcp?.size()<1){
                                jobRcp = JobApp.findAllByReceptionAndT302StaDel(rec,"0")
                                jobRcp.each { JobApp job ->
                                    if (job.operation) {
                                        alfabet = 'V' + job.operation.kategoriJob.m055KategoriJob
                                        alfabet1 = 'V1' + job.operation.kategoriJob.m055KategoriJob
                                    }
                                }
                            }
                        }
                    }
                    def jamJanjiMulai = jpb?.t451TglJamPlan
                    def jamJanjiSelesai = jpb?.t451TglJamPlanFinished
                    int valJamMenitSkrg = 0
                    use(TimeCategory){
                        int tempJam = datatablesUtilService?.syncTime()?.format("HH")?.toInteger()
                        int tempMenit = datatablesUtilService?.syncTime()?.format("mm")?.toInteger()
                        valJamMenitSkrg = (tempJam * 60) + tempMenit
                    }
                    if(jamJanjiSelesai){
                        int tempJam2 = jamJanjiSelesai.format("HH").toInteger()
                        int tempMenit2 = jamJanjiSelesai.format("mm").toInteger()
                        int valJam2 = tempJam2 * 60 + tempMenit2
                        if((valJamMenitSkrg <= valJam2 && valJam2 < valJamMenitSkrg+15) && dari=="mboard" && mechanic==true){
                            alfabet1 = "V1FI"
                            alfabet = "VFI"

                        }
                    }
                    int shhMulai = Integer.parseInt(hh.format(jamJanjiMulai))
                    int smmMulai = Integer.parseInt(mm.format(jamJanjiMulai))
                    int shhSelesai = Integer.parseInt(hh.format(jamJanjiSelesai))
                    int smmSelesai = Integer.parseInt(mm.format(jamJanjiSelesai))
                    int mulai = shhMulai * 60 + smmMulai
                    int selesai = shhSelesai * 60 + smmSelesai
                    String delTime = rec?.t401TglJamJanjiPenyerahan ? rec?.t401TglJamJanjiPenyerahan?.format("HH:mm") : ""
                    if(!asbLine?.actual){
                        if (JAM7 >= mulai && JAM7 < selesai) {
                            if (start) {
                                asbLine.jam7 = alfabet1
                                asbLine.noPolJam7 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam7 = alfabet
                            }
                        }
                        if(JAM7 <= jamMenitSkrg && jamMenitSkrg < JAM715 && dari=="mboard"){
                            asbLine.jam7 = "NOW"
                            if (start) {
                                asbLine.noPolJam7 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM715 >= mulai && JAM715 < selesai) {
                            if (start) {
                                asbLine.jam715 = alfabet1
                                asbLine.noPolJam715 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam715 = alfabet
                            }
                        }
                        if(JAM715 <= jamMenitSkrg && jamMenitSkrg < JAM730 && dari=="mboard"){
                            asbLine.jam715 = "NOW"
                            if (start) {
                                asbLine.noPolJam715 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM730 >= mulai && JAM730 < selesai) {
                            if (start) {
                                asbLine.jam730 = alfabet1
                                asbLine.noPolJam730 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam730 = alfabet
                            }
                        }
                        if(JAM730 <= jamMenitSkrg && jamMenitSkrg < JAM745 && dari=="mboard"){
                            asbLine.jam730 = "NOW"
                            if (start) {
                                asbLine.noPolJam730 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM745 >= mulai && JAM745 < selesai) {
                            if (start) {
                                asbLine.jam745 = alfabet1
                                asbLine.noPolJam745 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam745 = alfabet
                            }
                        }
                        if(JAM745 <= jamMenitSkrg && jamMenitSkrg < JAM8 && dari=="mboard"){
                            asbLine.jam745 = "NOW"
                            if (start) {
                                asbLine.noPolJam745 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM8 >= mulai && JAM8 < selesai) {
                            if (start) {
                                asbLine.jam8 = alfabet1
                                asbLine.noPolJam8 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam8 = alfabet
                            }
                        }
                        if(JAM8 <= jamMenitSkrg && jamMenitSkrg < JAM815 && dari=="mboard"){
                            asbLine.jam8 = "NOW"
                            if (start) {
                                asbLine.noPolJam8 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM815 >= mulai && JAM815 < selesai) {
                            if (start) {
                                asbLine.jam815 = alfabet1
                                asbLine.noPolJam815 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam815 = alfabet
                            }
                        }
                        if(JAM815 <= jamMenitSkrg && jamMenitSkrg < JAM830 && dari=="mboard"){
                            asbLine.jam815 = "NOW"
                            if (start) {
                                asbLine.noPolJam815 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM830 >= mulai && JAM830 < selesai) {
                            if (start) {
                                asbLine.jam830 = alfabet1
                                asbLine.noPolJam830 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam830 = alfabet
                            }
                        }
                        if(JAM830 <= jamMenitSkrg && jamMenitSkrg < JAM845 && dari=="mboard"){
                            asbLine.jam830 = "NOW"
                            if (start) {
                                asbLine.noPolJam830 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM845 >= mulai && JAM845 < selesai) {
                            if (start) {
                                asbLine.jam845 = alfabet1
                                asbLine.noPolJam845 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam845 = alfabet
                            }
                        }
                        if(JAM845 <= jamMenitSkrg && jamMenitSkrg < JAM9 && dari=="mboard"){
                            asbLine.jam845 = "NOW"
                            if (start) {
                                asbLine.noPolJam845 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM9 >= mulai && JAM9 < selesai) {
                            if (start) {
                                asbLine.jam9 = alfabet1
                                asbLine.noPolJam9 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam9 = alfabet
                            }
                        }
                        if(JAM9 <= jamMenitSkrg && jamMenitSkrg < JAM915 && dari=="mboard"){
                            asbLine.jam9 = "NOW"
                            if (start) {
                                asbLine.noPolJam9 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM915 >= mulai && JAM915 < selesai) {
                            if (start) {
                                asbLine.jam915 = alfabet1
                                asbLine.noPolJam915 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam915 = alfabet
                            }
                        }
                        if(JAM915 <= jamMenitSkrg && jamMenitSkrg < JAM930 && dari=="mboard"){
                            asbLine.jam915 = "NOW"
                            if (start) {
                                asbLine.noPolJam915 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM930 >= mulai && JAM930 < selesai) {
                            if (start) {
                                asbLine.jam930 = alfabet1
                                asbLine.noPolJam930 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam930 = alfabet
                            }
                        }
                        if(JAM930 <= jamMenitSkrg && jamMenitSkrg < JAM945 && dari=="mboard"){
                            asbLine.jam930 = "NOW"
                            if (start) {
                                asbLine.noPolJam930 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM945 >= mulai && JAM945 < selesai) {
                            if (start) {
                                asbLine.jam945 = alfabet1
                                asbLine.noPolJam945 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam945 = alfabet
                            }
                        }
                        if(JAM945 <= jamMenitSkrg && jamMenitSkrg < JAM10 && dari=="mboard"){
                            asbLine.jam945 = "NOW"
                            if (start) {
                                asbLine.noPolJam945 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM10 >= mulai && JAM10 < selesai) {
                            if (start) {
                                asbLine.jam10 = alfabet1
                                asbLine.noPolJam10 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam10 = alfabet
                            }
                        }
                        if(JAM10 <= jamMenitSkrg && jamMenitSkrg < JAM1015 && dari=="mboard"){
                            asbLine.jam10 = "NOW"
                            if (start) {
                                asbLine.noPolJam10 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1015 >= mulai && JAM1015 < selesai) {
                            if (start) {
                                asbLine.jam1015 = alfabet1
                                asbLine.noPolJam1015 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1015 = alfabet
                            }
                        }
                        if(JAM1015 <= jamMenitSkrg && jamMenitSkrg < JAM1030 && dari=="mboard"){
                            asbLine.jam1015 = "NOW"
                            if (start) {
                                asbLine.noPolJam1015 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1030 >= mulai && JAM1030 < selesai) {
                            if (start) {
                                asbLine.jam1030 = alfabet1
                                asbLine.noPolJam1030 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1030 = alfabet
                            }
                        }
                        if(JAM1030 <= jamMenitSkrg && jamMenitSkrg < JAM1045 && dari=="mboard"){
                            asbLine.jam1030 = "NOW"
                            if (start) {
                                asbLine.noPolJam1030 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1045 >= mulai && JAM1045 < selesai) {
                            if (start) {
                                asbLine.jam1045 = alfabet1
                                asbLine.noPolJam1045 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1045 = alfabet
                            }
                        }
                        if(JAM1045 <= jamMenitSkrg && jamMenitSkrg < JAM11 && dari=="mboard"){
                            asbLine.jam1045 = "NOW"
                            if (start) {
                                asbLine.noPolJam1045 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM11 >= mulai && JAM11 < selesai) {
                            if (start) {
                                asbLine.jam11 = alfabet1
                                asbLine.noPolJam11 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam11 = alfabet
                            }
                        }
                        if(JAM11 <= jamMenitSkrg && jamMenitSkrg < JAM1115 && dari=="mboard"){
                            asbLine.jam11 = "NOW"
                            if (start) {
                                asbLine.noPolJam11 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1115 >= mulai && JAM1115 < selesai) {
                            if (start) {
                                asbLine.jam1115 = alfabet1
                                asbLine.noPolJam1115 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1115 = alfabet
                            }
                        }
                        if(JAM1115 <= jamMenitSkrg && jamMenitSkrg < JAM1130 && dari=="mboard"){
                            asbLine.jam1115 = "NOW"
                            if (start) {
                                asbLine.noPolJam1115 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1130 >= mulai && JAM1130 < selesai) {
                            if (start) {
                                asbLine.jam1130 = alfabet1
                                asbLine.noPolJam1130 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1130 = alfabet
                            }
                        }
                        if(JAM1130 <= jamMenitSkrg && jamMenitSkrg < JAM1145 && dari=="mboard"){
                            asbLine.jam1130 = "NOW"
                            if (start) {
                                asbLine.noPolJam1130 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1145 >= mulai && JAM1145 < selesai) {
                            if (start) {
                                asbLine.jam1145 = alfabet1
                                asbLine.noPolJam1145 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1145 = alfabet
                            }
                        }
                        if(JAM1145 <= jamMenitSkrg && jamMenitSkrg < JAM12 && dari=="mboard"){
                            asbLine.jam1145 = "NOW"
                            if (start) {
                                asbLine.noPolJam1145 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM12 >= mulai && JAM12 < selesai) {
                            if (start) {
                                asbLine.jam12 = alfabet1
                                asbLine.noPolJam12 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam12 = alfabet
                            }
                        }
                        if(JAM12 <= jamMenitSkrg && jamMenitSkrg < JAM1215 && dari=="mboard"){
                            asbLine.jam12 = "NOW"
                            if (start) {
                                asbLine.noPolJam12 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1215 >= mulai && JAM1215 < selesai) {
                            if (start) {
                                asbLine.jam1215 = alfabet1
                                asbLine.noPolJam1215 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1215 = alfabet
                            }
                        }
                        if(JAM1215 <= jamMenitSkrg && jamMenitSkrg < JAM1230 && dari=="mboard"){
                            asbLine.jam1215 = "NOW"
                            if (start) {
                                asbLine.noPolJam1215 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1230 >= mulai && JAM1230 < selesai) {
                            if (start) {
                                asbLine.jam1230 = alfabet1
                                asbLine.noPolJam1230 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1230 = alfabet
                            }
                        }
                        if(JAM1230 <= jamMenitSkrg && jamMenitSkrg < JAM1245 && dari=="mboard"){
                            asbLine.jam1230 = "NOW"
                            if (start) {
                                asbLine.noPolJam1230 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1245 >= mulai && JAM1245 < selesai) {
                            if (start) {
                                asbLine.jam1245 = alfabet1
                                asbLine.noPolJam1245 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1245 = alfabet
                            }
                        }
                        if(JAM1245 <= jamMenitSkrg && jamMenitSkrg < JAM13 && dari=="mboard"){
                            asbLine.jam1245 = "NOW"
                            if (start) {
                                asbLine.noPolJam1245 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM13 >= mulai && JAM13 < selesai) {
                            if (start) {
                                asbLine.jam13 = alfabet1
                                asbLine.noPolJam13 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam13 = alfabet
                            }
                        }
                        if(JAM13 <= jamMenitSkrg && jamMenitSkrg < JAM1315 && dari=="mboard"){
                            asbLine.jam13 = "NOW"
                            if (start) {
                                asbLine.noPolJam13 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1315 >= mulai && JAM1315 < selesai) {
                            if (start) {
                                asbLine.jam1315 = alfabet1
                                asbLine.noPolJam1315 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1315 = alfabet
                            }
                        }
                        if(JAM1315 <= jamMenitSkrg && jamMenitSkrg < JAM1330 && dari=="mboard"){
                            asbLine.jam1315 = "NOW"
                            if (start) {
                                asbLine.noPolJam1315 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1330 >= mulai && JAM1330 < selesai) {
                            if (start) {
                                asbLine.jam1330 = alfabet1
                                asbLine.noPolJam1330 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1330 = alfabet
                            }
                        }
                        if(JAM1330 <= jamMenitSkrg && jamMenitSkrg < JAM1345 && dari=="mboard"){
                            asbLine.jam1330 = "NOW"
                            if (start) {
                                asbLine.noPolJam1330 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1345 >= mulai && JAM1345 < selesai) {
                            if (start) {
                                asbLine.jam1345 = alfabet1
                                asbLine.noPolJam1345 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1345 = alfabet
                            }
                        }
                        if(JAM1345 <= jamMenitSkrg && jamMenitSkrg < JAM14 && dari=="mboard"){
                            asbLine.jam1345 = "NOW"
                            if (start) {
                                asbLine.noPolJam1345 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM14 >= mulai && JAM14 < selesai) {
                            if (start) {
                                asbLine.jam14 = alfabet1
                                asbLine.noPolJam14 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam14 = alfabet
                            }
                        }
                        if(JAM14 <= jamMenitSkrg && jamMenitSkrg < JAM1415 && dari=="mboard"){
                            asbLine.jam14 = "NOW"
                            if (start) {
                                asbLine.noPolJam14 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1415 >= mulai && JAM1415 < selesai) {
                            if (start) {
                                asbLine.jam1415 = alfabet1
                                asbLine.noPolJam1415 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1415 = alfabet
                            }
                        }
                        if(JAM1415 <= jamMenitSkrg && jamMenitSkrg < JAM1430 && dari=="mboard"){
                            asbLine.jam1415 = "NOW"
                            if (start) {
                                asbLine.noPolJam1415 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1430 >= mulai && JAM1430 < selesai) {
                            if (start) {
                                asbLine.jam1430 = alfabet1
                                asbLine.noPolJam1430 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1430 = alfabet
                            }
                        }
                        if(JAM1430 <= jamMenitSkrg && jamMenitSkrg < JAM1445 && dari=="mboard"){
                            asbLine.jam1430 = "NOW"
                            if (start) {
                                asbLine.noPolJam1430 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1445 >= mulai && JAM1445 < selesai) {
                            if (start) {
                                asbLine.jam1445 = alfabet1
                                asbLine.noPolJam1445 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1445 = alfabet
                            }
                        }
                        if(JAM1445 <= jamMenitSkrg && jamMenitSkrg < JAM15 && dari=="mboard"){
                            asbLine.jam1445 = "NOW"
                            if (start) {
                                asbLine.noPolJam1445 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM15 >= mulai && JAM15 < selesai) {
                            if (start) {
                                asbLine.jam15 = alfabet1
                                asbLine.noPolJam15 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam15 = alfabet
                            }
                        }
                        if(JAM15 <= jamMenitSkrg && jamMenitSkrg < JAM1515 && dari=="mboard"){
                            asbLine.jam15 = "NOW"
                            if (start) {
                                asbLine.noPolJam15 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1515 >= mulai && JAM1515 < selesai) {
                            if (start) {
                                asbLine.jam1515 = alfabet1
                                asbLine.noPolJam1515 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1515 = alfabet
                            }
                        }
                        if(JAM1515 <= jamMenitSkrg && jamMenitSkrg < JAM1530 && dari=="mboard"){
                            asbLine.jam1515 = "NOW"
                            if (start) {
                                asbLine.noPolJam1515 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1530 >= mulai && JAM1530 < selesai) {
                            if (start) {
                                asbLine.jam1530 = alfabet1
                                asbLine.noPolJam1530 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1530 = alfabet
                            }
                        }
                        if(JAM1530 <= jamMenitSkrg && jamMenitSkrg < JAM1545 && dari=="mboard"){
                            asbLine.jam1530 = "NOW"
                            if (start) {
                                asbLine.noPolJam1530 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1545 >= mulai && JAM1545 < selesai) {
                            if (start) {
                                asbLine.jam1545 = alfabet1
                                asbLine.noPolJam1545 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1545 = alfabet
                            }
                        }
                        if(JAM1545 <= jamMenitSkrg && jamMenitSkrg < JAM16 && dari=="mboard"){
                            asbLine.jam1545 = "NOW"
                            if (start) {
                                asbLine.noPolJam1545 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM16 >= mulai && JAM16 < selesai) {
                            if (start) {
                                asbLine.jam16 = alfabet1
                                asbLine.noPolJam16 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam16 = alfabet
                            }
                        }
                        if(JAM16 <= jamMenitSkrg && jamMenitSkrg < JAM1615 && dari=="mboard"){
                            asbLine.jam16 = "NOW"
                            if (start) {
                                asbLine.noPolJam16 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1615 >= mulai && JAM1615 < selesai) {
                            if (start) {
                                asbLine.jam1615 = alfabet1
                                asbLine.noPolJam1615 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1615 = alfabet
                            }
                        }
                        if(JAM1615 <= jamMenitSkrg && jamMenitSkrg < JAM1630 && dari=="mboard"){
                            asbLine.jam1615 = "NOW"
                            if (start) {
                                asbLine.noPolJam1615 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1630 >= mulai && JAM1630 < selesai) {
                            if (start) {
                                asbLine.jam1630 = alfabet1
                                asbLine.noPolJam1630 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1630 = alfabet
                            }
                        }
                        if(JAM1630 <= jamMenitSkrg && jamMenitSkrg < JAM1645 && dari=="mboard"){
                            asbLine.jam1630 = "NOW"
                            if (start) {
                                asbLine.noPolJam1630 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                        if (JAM1645 >= mulai && JAM1645 < selesai) {
                            if (start) {
                                asbLine.jam1645 = alfabet1
                                asbLine.noPolJam1645 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                                start = false
                            } else {
                                asbLine.jam1645 = alfabet
                            }
                        }
                        if(JAM1645 <= jamMenitSkrg && jamMenitSkrg <= JAM17 && dari=="mboard"){
                            asbLine.jam1645 = "NOW"
                            if (start) {
                                asbLine.noPolJam1645 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')+"&nbsp;|&nbsp;D-"+delTime
                            }
                        }
                    }else{
                        if(aksi.equalsIgnoreCase("view")){
                            Date wktTemp = jpb?.t451TglJamPlan
                            Date skrg = new Date().parse("dd/MM/yyyy HH:mm",wktTemp?.format("dd/MM/yyyy")+" 00:00")
                            Date bsok = skrg+1
                            def actualStar = Actual.createCriteria().list {
                                eq("reception", rec)
                                statusActual{
                                    or{
                                        ilike("m452StatusActual","%Clock On%")
                                        ilike("m452StatusActual","%Resume%")
                                    }
                                }
                                eq("staDel","0")
                                eq("namaManPower",jpb?.namaManPower)
                                ge("t452TglJam",skrg)
                                lt("t452TglJam",bsok)
                                order("dateCreated","desc")
                            }
                            def stopStar = Actual.createCriteria().list {
                                eq("reception", rec)
                                statusActual{
                                    or{
                                        ilike("m452StatusActual","%Clock Off%")
                                        ilike("m452StatusActual","%Pause%")
                                    }
                                }
                                eq("staDel","0")
                                eq("namaManPower",jpb?.namaManPower)
                                ge("t452TglJam",skrg)
                                lt("t452TglJam",bsok)
                                order("dateCreated","desc")
                            }
                            def doStart = actualStar[0]
                            def doStop = stopStar[0]
//                            def doStop = Actual.findByReceptionAndStatusActualAndStaDelAndNamaManPower(rec,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%"+"Clock Off"+"%","0"),"0",jpb?.namaManPower,[sort: 'dateCreated',order : 'desc'])
                            def actStart = doStart ? doStart?.t452TglJam : null
                            def actStop = doStop ? doStop?.t452TglJam : (doStart ? datatablesUtilService?.syncTime() : null)
                            if(actStart){
                                def actJamJanjiMulai = actStart
                                def actJamJanjiSelesai = actStop
                                int actShhMulai = Integer.parseInt(hh.format(actJamJanjiMulai))
                                int actSmmMulai = Integer.parseInt(mm.format(actJamJanjiMulai))
                                int actShhSelesai = Integer.parseInt(hh.format(actJamJanjiSelesai))
                                int actSmmSelesai = Integer.parseInt(mm.format(actJamJanjiSelesai))
                                int actmulai = actShhMulai * 60 + actSmmMulai
                                int actselesai = actShhSelesai * 60 + actSmmSelesai
                                String alActual = "A"
                                String alActual1 = "A1"
                                if ((JAM7 >= actmulai || cekJam(actShhMulai, actSmmMulai,7,0)) && JAM7 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam7 = alActual1
                                        asbLine.noPolJam7 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam7 = alActual
                                    }
                                }
                                if ((JAM715 >= actmulai || cekJam(actShhMulai, actSmmMulai,7,15)) && JAM715 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam715 = alActual1
                                        aStart = false
                                        asbLine.noPolJam715 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam715 = alActual
                                    }
                                }
                                if ((JAM730 >= actmulai || cekJam(actShhMulai, actSmmMulai,7,30))  && JAM730 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam730 = alActual1
                                        asbLine.noPolJam730 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam730 = alActual
                                    }
                                }
                                if ((JAM745 >= actmulai || cekJam(actShhMulai, actSmmMulai,7,45)) && JAM745 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam745 = alActual1
                                        asbLine.noPolJam745 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam745 = alActual
                                    }
                                }
                                if ((JAM8 >= actmulai || cekJam(actShhMulai, actSmmMulai,8,0)) && JAM8 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam8 = alActual1

                                        asbLine.noPolJam8 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam8 = alActual
                                    }
                                }
                                if ((JAM815 >= actmulai || cekJam(actShhMulai, actSmmMulai,8,15)) && JAM815 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam815 = alActual1
                                        asbLine.noPolJam815 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam815 = alActual
                                    }
                                }
                                if ((JAM830 >= actmulai || cekJam(actShhMulai, actSmmMulai,8,30)) && JAM830 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam830 = alActual1
                                        asbLine.noPolJam830 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam830 = alActual
                                    }
                                }
                                if ((JAM845 >= actmulai || cekJam(actShhMulai, actSmmMulai,8,45)) && JAM845 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam845 = alActual1
                                        asbLine.noPolJam845 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam845 = alActual
                                    }
                                }
                                if ((JAM9 >= actmulai || cekJam(actShhMulai, actSmmMulai,9,0)) && JAM9 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam9 = alActual1
                                        asbLine.noPolJam9 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam9 = alActual
                                    }
                                }
                                if ((JAM915 >= actmulai || cekJam(actShhMulai, actSmmMulai,9,15)) && JAM915 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam915 = alActual1
                                        asbLine.noPolJam915 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam915 = alActual
                                    }
                                }
                                if ((JAM930 >= actmulai || cekJam(actShhMulai, actSmmMulai,9,30)) && JAM930 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam930 = alActual1
                                        asbLine.noPolJam930 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam930 = alActual
                                    }
                                }
                                if ((JAM945 >= actmulai || cekJam(actShhMulai, actSmmMulai,9,45)) && JAM945 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam945 = alActual1
                                        asbLine.noPolJam945 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                        aStart = false
                                    } else {
                                        asbLine.jam945 = alActual
                                    }
                                }
                                if ((JAM10 >= actmulai || cekJam(actShhMulai, actSmmMulai,10,0)) && JAM10 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam10 = alActual1
                                        aStart = false
                                        asbLine.noPolJam10 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam10 = alActual
                                    }
                                }
                                if ((JAM1015 >= actmulai || cekJam(actShhMulai, actSmmMulai,10,15)) && JAM1015 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1015 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1015 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1015 = alActual
                                    }
                                }
                                if ((JAM1030 >= actmulai || cekJam(actShhMulai, actSmmMulai,10,30)) && JAM1030 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1030 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1030 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1030 = alActual
                                    }
                                }
                                if ((JAM1045 >= actmulai || cekJam(actShhMulai, actSmmMulai,10,45)) && JAM1045 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1045 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1045 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1045 = alActual
                                    }
                                }
                                if ((JAM11 >= actmulai || cekJam(actShhMulai, actSmmMulai,11,0)) && JAM11 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam11 = alActual1
                                        aStart = false
                                        asbLine.noPolJam11 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam11 = alActual
                                    }
                                }
                                if ((JAM1115 >= actmulai || cekJam(actShhMulai, actSmmMulai,11,15)) && JAM1115 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1115 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1115 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1115 = alActual
                                    }
                                }
                                if ((JAM1130 >= actmulai || cekJam(actShhMulai, actSmmMulai,11,30)) && JAM1130 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1130 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1130 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1130 = alActual
                                    }
                                }
                                if ((JAM1145 >= actmulai || cekJam(actShhMulai, actSmmMulai,11,45)) && JAM1145 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1145 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1145 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1145 = alActual
                                    }
                                }
                                if ((JAM12 >= actmulai || cekJam(actShhMulai, actSmmMulai,12,0)) && JAM12 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam12 = alActual1
                                        aStart = false
                                        asbLine.noPolJam12 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam12 = alActual
                                    }
                                }
                                if ((JAM1215 >= actmulai || cekJam(actShhMulai, actSmmMulai,12,15)) && JAM1215 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1215 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1215 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1215 = alActual
                                    }
                                }
                                if ((JAM1230 >= actmulai || cekJam(actShhMulai, actSmmMulai,12,30)) && JAM1230 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1230 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1230 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1230 = alActual
                                    }
                                }
                                if ((JAM1245 >= actmulai || cekJam(actShhMulai, actSmmMulai,12,45)) && JAM1245 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1245 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1245 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1245 = alActual
                                    }
                                }
                                if ((JAM13 >= actmulai || cekJam(actShhMulai, actSmmMulai,13,0)) && JAM13 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam13 = alActual1
                                        aStart = false
                                        asbLine.noPolJam13 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam13 = alActual
                                    }
                                }
                                if ((JAM1315 >= actmulai || cekJam(actShhMulai, actSmmMulai,13,15)) && JAM1315 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1315 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1315 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1315 = alActual
                                    }
                                }
                                if ((JAM1330 >= actmulai || cekJam(actShhMulai, actSmmMulai,13,30)) && JAM1330 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1330 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1330 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1330 = alActual
                                    }
                                }
                                if ((JAM1345 >= actmulai || cekJam(actShhMulai, actSmmMulai,13,45)) && JAM1345 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1345 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1345 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1345 = alActual
                                    }
                                }
                                if ((JAM14 >= actmulai || cekJam(actShhMulai, actSmmMulai,14,0)) && JAM14 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam14 = alActual1
                                        aStart = false
                                        asbLine.noPolJam14 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam14 = alActual
                                    }
                                }
                                if ((JAM1415 >= actmulai || cekJam(actShhMulai, actSmmMulai,14,15)) && JAM1415 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1415 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1415 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1415 = alActual
                                    }
                                }
                                if ((JAM1430 >= actmulai || cekJam(actShhMulai, actSmmMulai,14,30)) && JAM1430 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1430 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1430 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1430 = alActual
                                    }
                                }
                                if ((JAM1445 >= actmulai || cekJam(actShhMulai, actSmmMulai,14,45)) && JAM1445 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1445 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1445 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1445 = alActual
                                    }
                                }
                                if ((JAM15 >= actmulai || cekJam(actShhMulai, actSmmMulai,15,0)) && JAM15 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam15 = alActual1
                                        aStart = false
                                        asbLine.noPolJam15 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam15 = alActual
                                    }
                                }
                                if ((JAM1515 >= actmulai || cekJam(actShhMulai, actSmmMulai,15,15)) && JAM1515 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1515 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1515 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1515 = alActual
                                    }
                                }
                                if ((JAM1530 >= actmulai || cekJam(actShhMulai, actSmmMulai,15,30)) && JAM1530 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1530 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1530 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1530 = alActual
                                    }
                                }
                                if ((JAM1545 >= actmulai || cekJam(actShhMulai, actSmmMulai,15,45)) && JAM1545 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1545 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1545 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1545 = alActual
                                    }
                                }
                                if ((JAM16 >= actmulai || cekJam(actShhMulai, actSmmMulai,16,0)) && JAM16 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam16 = alActual1
                                        aStart = false
                                        asbLine.noPolJam16 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam16 = alActual
                                    }
                                }
                                if ((JAM1615 >= actmulai || cekJam(actShhMulai, actSmmMulai,16,15)) && JAM1615 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1615 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1615 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1615 = alActual
                                    }
                                }
                                if ((JAM1630 >= actmulai || cekJam(actShhMulai, actSmmMulai,16,30)) && JAM1630 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1630 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1630 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1630 = alActual
                                    }
                                }
                                if ((JAM1645 >= actmulai || cekJam(actShhMulai, actSmmMulai,16,45)) && JAM1645 < actselesai) {
                                    if (aStart) {
                                        asbLine.jam1645 = alActual1
                                        aStart = false
                                        asbLine.noPolJam1645 = rec?.historyCustomerVehicle?.fullNoPol?.replaceAll(' ','&nbsp;')
                                    } else {
                                        asbLine.jam1645 = alActual
                                    }
                                }
                            }
                            if(JAM7 <= jamMenitSkrg && jamMenitSkrg < JAM715 && dari=="mboard"){
                                asbLine.jam7 = "NOW"
                            }
                            if(JAM715 <= jamMenitSkrg && jamMenitSkrg < JAM730 && dari=="mboard"){
                                asbLine.jam715 = "NOW"
                            }
                            if(JAM730 <= jamMenitSkrg && jamMenitSkrg < JAM745 && dari=="mboard"){
                                asbLine.jam730 = "NOW"
                            }
                            if(JAM745 <= jamMenitSkrg && jamMenitSkrg < JAM8 && dari=="mboard"){
                                asbLine.jam745 = "NOW"
                            }
                            if(JAM8 <= jamMenitSkrg && jamMenitSkrg < JAM815 && dari=="mboard"){
                                asbLine.jam8 = "NOW"
                            }
                            if(JAM815 <= jamMenitSkrg && jamMenitSkrg < JAM830 && dari=="mboard"){
                                asbLine.jam815 = "NOW"
                            }
                            if(JAM830 <= jamMenitSkrg && jamMenitSkrg < JAM845 && dari=="mboard"){
                                asbLine.jam830 = "NOW"
                            }
                            if(JAM845 <= jamMenitSkrg && jamMenitSkrg < JAM9 && dari=="mboard"){
                                asbLine.jam845 = "NOW"
                            }
                            if(JAM9 <= jamMenitSkrg && jamMenitSkrg < JAM915 && dari=="mboard"){
                                asbLine.jam9 = "NOW"
                            }
                            if(JAM915 <= jamMenitSkrg && jamMenitSkrg < JAM930 && dari=="mboard"){
                                asbLine.jam915 = "NOW"
                            }
                            if(JAM930 <= jamMenitSkrg && jamMenitSkrg < JAM945 && dari=="mboard"){
                                asbLine.jam930 = "NOW"
                            }
                            if(JAM945 <= jamMenitSkrg && jamMenitSkrg < JAM10 && dari=="mboard"){
                                asbLine.jam945 = "NOW"
                            }
                            if(JAM10 <= jamMenitSkrg && jamMenitSkrg < JAM1015 && dari=="mboard"){
                                asbLine.jam10 = "NOW"
                            }
                            if(JAM1015 <= jamMenitSkrg && jamMenitSkrg < JAM1030 && dari=="mboard"){
                                asbLine.jam1015 = "NOW"
                            }
                            if(JAM1030 <= jamMenitSkrg && jamMenitSkrg < JAM1045 && dari=="mboard"){
                                asbLine.jam1030 = "NOW"
                            }
                            if(JAM1045 <= jamMenitSkrg && jamMenitSkrg < JAM11 && dari=="mboard"){
                                asbLine.jam1045 = "NOW"
                            }
                            if(JAM11 <= jamMenitSkrg && jamMenitSkrg < JAM1115 && dari=="mboard"){
                                asbLine.jam11 = "NOW"
                            }
                            if(JAM1115 <= jamMenitSkrg && jamMenitSkrg < JAM1130 && dari=="mboard"){
                                asbLine.jam1115 = "NOW"
                            }
                            if(JAM1130 <= jamMenitSkrg && jamMenitSkrg < JAM1145 && dari=="mboard"){
                                asbLine.jam1130 = "NOW"
                            }
                            if(JAM1145 <= jamMenitSkrg && jamMenitSkrg < JAM12 && dari=="mboard"){
                                asbLine.jam1145 = "NOW"
                            }
                            if(JAM12 <= jamMenitSkrg && jamMenitSkrg < JAM1215 && dari=="mboard"){
                                asbLine.jam12 = "NOW"
                            }
                            if(JAM1215 <= jamMenitSkrg && jamMenitSkrg < JAM1230 && dari=="mboard"){
                                asbLine.jam1215 = "NOW"
                            }
                            if(JAM1230 <= jamMenitSkrg && jamMenitSkrg < JAM1245 && dari=="mboard"){
                                asbLine.jam1230 = "NOW"
                            }
                            if(JAM1245 <= jamMenitSkrg && jamMenitSkrg < JAM13 && dari=="mboard"){
                                asbLine.jam1245 = "NOW"
                            }
                            if(JAM13 <= jamMenitSkrg && jamMenitSkrg < JAM1315 && dari=="mboard"){
                                asbLine.jam13 = "NOW"
                            }
                            if(JAM1315 <= jamMenitSkrg && jamMenitSkrg < JAM1330 && dari=="mboard"){
                                asbLine.jam1315 = "NOW"
                            }
                            if(JAM1330 <= jamMenitSkrg && jamMenitSkrg < JAM1345 && dari=="mboard"){
                                asbLine.jam1330 = "NOW"
                            }
                            if(JAM1345 <= jamMenitSkrg && jamMenitSkrg < JAM14 && dari=="mboard"){
                                asbLine.jam1345 = "NOW"
                            }
                            if(JAM14 <= jamMenitSkrg && jamMenitSkrg < JAM1415 && dari=="mboard"){
                                asbLine.jam14 = "NOW"
                            }
                            if(JAM1415 <= jamMenitSkrg && jamMenitSkrg < JAM1430 && dari=="mboard"){
                                asbLine.jam1415 = "NOW"
                            }
                            if(JAM1430 <= jamMenitSkrg && jamMenitSkrg < JAM1445 && dari=="mboard"){
                                asbLine.jam1430 = "NOW"
                            }
                            if(JAM1445 <= jamMenitSkrg && jamMenitSkrg < JAM15 && dari=="mboard"){
                                asbLine.jam1445 = "NOW"
                            }
                            if(JAM15 <= jamMenitSkrg && jamMenitSkrg < JAM1515 && dari=="mboard"){
                                asbLine.jam15 = "NOW"
                            }
                            if(JAM1515 <= jamMenitSkrg && jamMenitSkrg < JAM1530 && dari=="mboard"){
                                asbLine.jam1515 = "NOW"
                            }
                            if(JAM1530 <= jamMenitSkrg && jamMenitSkrg < JAM1545 && dari=="mboard"){
                                asbLine.jam1530 = "NOW"
                            }
                            if(JAM1545 <= jamMenitSkrg && jamMenitSkrg < JAM16 && dari=="mboard"){
                                asbLine.jam1545 = "NOW"
                            }
                            if(JAM16 <= jamMenitSkrg && jamMenitSkrg < JAM1615 && dari=="mboard"){
                                asbLine.jam16 = "NOW"
                            }
                            if(JAM1615 <= jamMenitSkrg && jamMenitSkrg < JAM1630 && dari=="mboard"){
                                asbLine.jam1615 = "NOW"
                            }
                            if(JAM1630 <= jamMenitSkrg && jamMenitSkrg < JAM1645 && dari=="mboard"){
                                asbLine.jam1630 = "NOW"
                            }
                            if(JAM1645 <= jamMenitSkrg && jamMenitSkrg <= JAM17 && dari=="mboard"){
                                asbLine.jam1645 = "NOW"
                            }
                        }
                    }
                }else{
                    findActualBoard(asbLine,dari,jamMenitSkrg)
                }
            }
        }
        manPowerStalls
    }

    def findActualBoard(ASBLine asbLine,String dari,int jamMenitSkrg){
        if(JAM7 <= jamMenitSkrg && jamMenitSkrg < JAM715 && dari=="mboard"){
            asbLine.jam7 = "NOW"
        }
        if(JAM715 <= jamMenitSkrg && jamMenitSkrg < JAM730 && dari=="mboard"){
            asbLine.jam715 = "NOW"
        }
        if(JAM730 <= jamMenitSkrg && jamMenitSkrg < JAM745 && dari=="mboard"){
            asbLine.jam730 = "NOW"
        }
        if(JAM745 <= jamMenitSkrg && jamMenitSkrg < JAM8 && dari=="mboard"){
            asbLine.jam745 = "NOW"
        }
        if(JAM8 <= jamMenitSkrg && jamMenitSkrg < JAM815 && dari=="mboard"){
            asbLine.jam8 = "NOW"
        }
        if(JAM815 <= jamMenitSkrg && jamMenitSkrg < JAM830 && dari=="mboard"){
            asbLine.jam815 = "NOW"
        }
        if(JAM830 <= jamMenitSkrg && jamMenitSkrg < JAM845 && dari=="mboard"){
            asbLine.jam830 = "NOW"
        }
        if(JAM845 <= jamMenitSkrg && jamMenitSkrg < JAM9 && dari=="mboard"){
            asbLine.jam845 = "NOW"
        }
        if(JAM9 <= jamMenitSkrg && jamMenitSkrg < JAM915 && dari=="mboard"){
            asbLine.jam9 = "NOW"
        }
        if(JAM915 <= jamMenitSkrg && jamMenitSkrg < JAM930 && dari=="mboard"){
            asbLine.jam915 = "NOW"
        }
        if(JAM930 <= jamMenitSkrg && jamMenitSkrg < JAM945 && dari=="mboard"){
            asbLine.jam930 = "NOW"
        }
        if(JAM945 <= jamMenitSkrg && jamMenitSkrg < JAM10 && dari=="mboard"){
            asbLine.jam945 = "NOW"
        }
        if(JAM10 <= jamMenitSkrg && jamMenitSkrg < JAM1015 && dari=="mboard"){
            asbLine.jam10 = "NOW"
        }
        if(JAM1015 <= jamMenitSkrg && jamMenitSkrg < JAM1030 && dari=="mboard"){
            asbLine.jam1015 = "NOW"
        }
        if(JAM1030 <= jamMenitSkrg && jamMenitSkrg < JAM1045 && dari=="mboard"){
            asbLine.jam1030 = "NOW"
        }
        if(JAM1045 <= jamMenitSkrg && jamMenitSkrg < JAM11 && dari=="mboard"){
            asbLine.jam1045 = "NOW"
        }
        if(JAM11 <= jamMenitSkrg && jamMenitSkrg < JAM1115 && dari=="mboard"){
            asbLine.jam11 = "NOW"
        }
        if(JAM1115 <= jamMenitSkrg && jamMenitSkrg < JAM1130 && dari=="mboard"){
            asbLine.jam1115 = "NOW"
        }
        if(JAM1130 <= jamMenitSkrg && jamMenitSkrg < JAM1145 && dari=="mboard"){
            asbLine.jam1130 = "NOW"
        }
        if(JAM1145 <= jamMenitSkrg && jamMenitSkrg < JAM12 && dari=="mboard"){
            asbLine.jam1145 = "NOW"
        }
        if(JAM12 <= jamMenitSkrg && jamMenitSkrg < JAM1215 && dari=="mboard"){
            asbLine.jam12 = "NOW"
        }
        if(JAM1215 <= jamMenitSkrg && jamMenitSkrg < JAM1230 && dari=="mboard"){
            asbLine.jam1215 = "NOW"
        }
        if(JAM1230 <= jamMenitSkrg && jamMenitSkrg < JAM1245 && dari=="mboard"){
            asbLine.jam1230 = "NOW"
        }
        if(JAM1245 <= jamMenitSkrg && jamMenitSkrg < JAM13 && dari=="mboard"){
            asbLine.jam1245 = "NOW"
        }
        if(JAM13 <= jamMenitSkrg && jamMenitSkrg < JAM1315 && dari=="mboard"){
            asbLine.jam13 = "NOW"
        }
        if(JAM1315 <= jamMenitSkrg && jamMenitSkrg < JAM1330 && dari=="mboard"){
            asbLine.jam1315 = "NOW"
        }
        if(JAM1330 <= jamMenitSkrg && jamMenitSkrg < JAM1345 && dari=="mboard"){
            asbLine.jam1330 = "NOW"
        }
        if(JAM1345 <= jamMenitSkrg && jamMenitSkrg < JAM14 && dari=="mboard"){
            asbLine.jam1345 = "NOW"
        }
        if(JAM14 <= jamMenitSkrg && jamMenitSkrg < JAM1415 && dari=="mboard"){
            asbLine.jam14 = "NOW"
        }
        if(JAM1415 <= jamMenitSkrg && jamMenitSkrg < JAM1430 && dari=="mboard"){
            asbLine.jam1415 = "NOW"
        }
        if(JAM1430 <= jamMenitSkrg && jamMenitSkrg < JAM1445 && dari=="mboard"){
            asbLine.jam1430 = "NOW"
        }
        if(JAM1445 <= jamMenitSkrg && jamMenitSkrg < JAM15 && dari=="mboard"){
            asbLine.jam1445 = "NOW"
        }
        if(JAM15 <= jamMenitSkrg && jamMenitSkrg < JAM1515 && dari=="mboard"){
            asbLine.jam15 = "NOW"
        }
        if(JAM1515 <= jamMenitSkrg && jamMenitSkrg < JAM1530 && dari=="mboard"){
            asbLine.jam1515 = "NOW"
        }
        if(JAM1530 <= jamMenitSkrg && jamMenitSkrg < JAM1545 && dari=="mboard"){
            asbLine.jam1530 = "NOW"
        }
        if(JAM1545 <= jamMenitSkrg && jamMenitSkrg < JAM16 && dari=="mboard"){
            asbLine.jam1545 = "NOW"
        }
        if(JAM16 <= jamMenitSkrg && jamMenitSkrg < JAM1615 && dari=="mboard"){
            asbLine.jam16 = "NOW"
        }
        if(JAM1615 <= jamMenitSkrg && jamMenitSkrg < JAM1630 && dari=="mboard"){
            asbLine.jam1615 = "NOW"
        }
        if(JAM1630 <= jamMenitSkrg && jamMenitSkrg < JAM1645 && dari=="mboard"){
            asbLine.jam1630 = "NOW"
        }
        if(JAM1645 <= jamMenitSkrg && jamMenitSkrg <= JAM17 && dari=="mboard"){
            asbLine.jam1645 = "NOW"
        }
    }

    boolean cekJam(int jam,int menit,int jamAcuan, int menitAcuan){
        boolean benar = false
        int totalAct = (jam * 60) + menit
        int totalAcuan = (jamAcuan * 60) + menitAcuan
        if(totalAct >= totalAcuan && totalAct < totalAcuan+15){
            benar = true
        }
        return benar
    }

    def dssAppointment(def params, CompanyDealer companyDealer){
        def res = [:]
        def operations = []
        Double totalRate = 0.0
        def arrJob = []
        if(params.noWo){
            def rec = Reception.findByT401NoWO(params.noWo)
            if(rec){
                def jobRcp = JobRCP.findAllByReceptionAndStaDel(rec,"0")
                jobRcp.each { JobRCP job ->
                    arrJob << job.operation.id
                    if (job.operation && rec.historyCustomerVehicle?.fullModelCode?.baseModel) {
                        operations << job.operation
                        def r = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(job.operation,rec?.historyCustomerVehicle?.fullModelCode?.baseModel,"0",datatablesUtilService?.syncTime(),[sort: 'id',order: 'desc'])
                        if (r)
                            totalRate = totalRate + r.t113ProductionRate
                    }
                }
            }
        } else if(params.idApp) {
            def app = Appointment.get(params.idApp as Long)
            def jobApps = JobApp.findAllByReception(app.reception)

            jobApps.each { JobApp job ->
                arrJob << job.operation.id
                if (job.operation && app.historyCustomerVehicle?.fullModelCode?.baseModel) {
                    operations << job.operation
                    def r = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(job.operation,app?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel,"0",datatablesUtilService?.syncTime(),[sort: 'id',order: 'desc'])
                    if (r)
                        totalRate = totalRate + r.t113ProductionRate
                }
            }
        }
        totalRate = totalRate * 60.0
        if(params?.statusRate){
            totalRate=params.durasi.toLong()
        }

        if(params.checkJanjiDatang){
            String rencana = params.tglRencana + " " + params.jamRencana
            def rencanaDate = new Date().parse('d-MM-yyyy HH:mm',rencana.toString())
            params."sCriteria_tglView" = rencanaDate
        } else if(params.checkJanjiPenyerahan){
            String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
            def penyerahanDate =  new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
            params."sCriteria_tglView" = penyerahanDate

        }
        def tanggalView = datatablesUtilService?.syncTime()
        if (params."sCriteria_tglView") {
            tanggalView = params."sCriteria_tglView"
            tanggalView.clearTime()
        }
        def manPowerStalls = loadAllManPowerStall(companyDealer, params)

        def c = JPB.createCriteria()

        def results = c.list {
            eq("staDel","0")
            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", tanggalView)
                lt("t451TglJamPlan", tanggalView + 1)
                params.tglView = tanggalView
            }
        }
        manPowerStalls = showJPB(results, manPowerStalls, params.aksi,"other",false)

        def candidates = []
        Date canDate = params."sCriteria_tglView"
        boolean  doAgain = true;
        def jmlJobCovered = []
        for(ASBLine asbLine: manPowerStalls) {
            //println " Screening stall " + asbLine.stall
            def jmlJobTemp = 0
            def teknisi = NamaManPower.get(asbLine.teknisiId.toLong())
            def skillMap = ManPowerSertifikat.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companyDealer)
                eq("namaManPower",teknisi)
                le("t016TglAwalSertifikat",new Date().clearTime())
                ge("t016TglAkhirSertifikat",new Date().clearTime())
            }
            def sertifikat = SertifikatJobMap.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companyDealer)
                if(skillMap){
                    inList("sertifikat",skillMap.sertifikat)
                }else{
                    eq("id",-1000.toLong())
                }
                operation{
                    inList("id",arrJob)
                }
                projections {
                    groupProperty("operation")
                }
            }
            sertifikat.each {
                if(arrJob.indexOf(it.id)>=0){
                    jmlJobTemp++;
                }
            }
            jmlJobCovered << [
                    asbLine : asbLine,
                    jmlJob  : jmlJobTemp,
                    staBisa : jmlJobTemp>0 ? "ya" : "tidak"
            ]
            for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                //println " Screening freeSlot " + freeSlot + "Length : "+freeSlot.length + " TOTAL  RATE ${totalRate}"
                if ((freeSlot.length >= totalRate || totalRate>600) && jmlJobTemp==arrJob.size()) {
                    candidates << freeSlot
                    doAgain = false
                }
            }

        }
        if(doAgain){
            if(jmlJobCovered.size()>0 && jmlJobCovered.staBisa.contains("ya")){
                for(int a = 0 ; a < jmlJobCovered.size() ; a++){
                    for(int b = a+1; b < jmlJobCovered.size();b++){
                        if(jmlJobCovered[a].jmlJob < jmlJobCovered[b].jmlJob){
                            def temp = jmlJobCovered[a]
                            jmlJobCovered[a] = jmlJobCovered[b]
                            jmlJobCovered[b] = temp
                        }
                    }
                }
            }

            jmlJobCovered.each {
                ASBLine asbLine = it.asbLine
                //println " Screening stall AGAIN " + asbLine.stall
                for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                    //println " Screening freeSlot AGAIN " + freeSlot + "Length : "+freeSlot.length + " TOTAL  RATE ${totalRate}"
                    if (freeSlot.length >= totalRate || totalRate>600) {
                        candidates << freeSlot
                    }
                }
            }
        }

        def harusDatang = null
        def targetSelesai = null
        def candidate = null
        def sisaRate = null
        for (ASBFreeSlot freeSlot : candidates) {

            Date slotStart = freeSlot.startDateTime
            Date slotEnd = null
            def breakEnd = null
            use( TimeCategory ) {
                slotEnd = slotStart + (freeSlot.length + freeSlot.breakLength).minutes
                if(freeSlot.breakStart && freeSlot.breakLength)
                    breakEnd = freeSlot.breakStart + freeSlot.breakLength.minutes
            }
            //println " Screening candidate " + slotStart + " to " + slotEnd + " @" + freeSlot.line.stall
            if(slotEnd) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                def waktuSekarang = df.format(params."sCriteria_tglView")
                Date jam5 = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 17:00")
                Date jamTujuh = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 07:00")
                if (params.checkJanjiPenyerahan) {
                    String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
                    def penyerahanDate = new Date().parse('d-MM-yyyy HH:mm', penyerahan.toString())

                    if(breakEnd && freeSlot.breakStart < penyerahanDate && breakEnd > penyerahanDate)
                        penyerahanDate = breakEnd
                    if(penyerahanDate <= slotEnd){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = penyerahanDate - iTotalRate.minutes
                            if(tempDate < jamTujuh){
                                def tempSisaRate = jamTujuh - tempDate
                                tempDate = jamTujuh
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(breakEnd > tempDate && breakEnd <= penyerahanDate){
                                    harusDatang = penyerahanDate - (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    harusDatang = tempDate
                                }
                            } else {
                                harusDatang = tempDate
                            }
                        }
                        if(harusDatang && harusDatang >= slotStart) {
                            if (harusDatang.minutes > 45) {
                                harusDatang.minutes = 0
                                harusDatang.hours = harusDatang.hours + 1
                            } else if (harusDatang.minutes > 30)  {
                                harusDatang.minutes = 45
                            }else if (harusDatang.minutes > 15) {
                                harusDatang.minutes = 30
                            }else if (harusDatang.minutes > 0) {
                                harusDatang.minutes = 15
                            } else {
                                harusDatang.minutes = 0
                            }

                            targetSelesai = penyerahanDate
                            candidate = freeSlot
                            break
                        }

                    }
                }
                else if (params.checkJanjiDatang) {
                    String rencana = params.tglRencana + " " + params.jamRencana
                    def rencanaDate = new Date().parse('dd-MM-yyyy HH:mm',rencana.toString())
                    if(breakEnd && freeSlot.breakStart < rencanaDate && breakEnd > rencanaDate)
                        rencanaDate = freeSlot.breakStart
                    if(rencanaDate >= slotStart){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = rencanaDate + iTotalRate.minutes
                            if(tempDate > jam5){
                                def tempSisaRate = tempDate - jam5
                                tempDate = jam5
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                                iTotalRate = iTotalRate - sisaRate
                            }
                            if(freeSlot.breakLength > 0){
                                if(freeSlot.breakStart >= rencanaDate && freeSlot.breakStart < tempDate){
                                    targetSelesai = rencanaDate + (iTotalRate + freeSlot.breakLength).minutes
                                    if(targetSelesai.after(jam5)){
                                        targetSelesai = jam5
                                    }
                                } else {
                                    targetSelesai = tempDate
                                }
                            } else {
                                targetSelesai = tempDate
                            }
                        }
                        if(targetSelesai && targetSelesai  <= slotEnd) {
                            if (targetSelesai.minutes > 45) {
                                targetSelesai.minutes = 0
                                targetSelesai.hours = targetSelesai.hours + 1
                            } else if (targetSelesai.minutes > 30)  {
                                targetSelesai.minutes = 45
                            }else if (targetSelesai.minutes > 15) {
                                targetSelesai.minutes = 30
                            }else if (targetSelesai.minutes > 0) {
                                targetSelesai.minutes = 15
                            } else {
                                targetSelesai.minutes = 0
                            }

                            harusDatang = rencanaDate
                            candidate = freeSlot
                            break
                        }
                    }
                }
            }

        }
        if(candidate) {
            def arrStart = []
            def arrEnd = []
            def arrSlot = []
            def arrStallId = []
            def arrTanggal = []
            def arrTanggalDSS = []
            def arrTotalRate= []
            def slot = []
            def currentTime = harusDatang
            while(currentTime < targetSelesai){

                slot << "jam"+currentTime.hours+(currentTime.minutes==0?"":currentTime.minutes)
                use( TimeCategory ) {
                    currentTime = currentTime + 15.minutes
                }
            }
            arrStart << harusDatang
            arrEnd << targetSelesai
            arrSlot << slot
            arrStallId << candidate.line.stallId
            arrTanggal << canDate
            arrTanggalDSS << canDate.format("dd/MM/yyyy")
            arrTotalRate << totalRate

            if(sisaRate>0){
                int tambah = 0
                while (sisaRate>0){
                    tambah++
                    if (params.checkJanjiPenyerahan) {
                        params.jamPenyerahan = "17:00"
                        params.tglPenyerahan = (tanggalView-tambah).format("dd-MM-yyyy")
                    }else if(params.checkJanjiDatang){
                        params.jamRencana = "07:00"
                        params.tglRencana = (tanggalView+tambah).format("dd-MM-yyyy")
                    }
                    def lagi = findMore(params, companyDealer, sisaRate,"satu")
                    if(lagi."status"=="found"){
                        arrStart << lagi."start"
                        arrEnd << lagi."end"
                        arrSlot << lagi.slot
                        arrStallId << lagi."stallId"
                        arrTanggal << lagi."tanggal"
                        arrTanggalDSS << lagi."tanggal".format("dd/MM/yyyy")
                        arrTotalRate << lagi."totalRate"
                        sisaRate = lagi."sisaRate"
                    }
                }
            }

            res.start = arrStart
            res.end = arrEnd
            res.slot = arrSlot
            res.stallId = arrStallId
            res.tanggal = arrTanggal
            res.tanggalDSS = arrTanggalDSS
            res.totalRate = arrTotalRate
            res."jumData" = arrStart?.size()
            res."status" = "found"
        } else {
            res."status" = "notfound"
        }
        res
    }

    def findMore(def params,CompanyDealer companyDealer,Double aSisaRate, String dari){
        def res = [:]
        Double totalRate = aSisaRate
        if(params.checkJanjiDatang){
            String rencana = params.tglRencana + " " + params.jamRencana
            def rencanaDate = new Date().parse('d-MM-yyyy HH:mm',rencana.toString())
            params."sCriteria_tglView" = rencanaDate
        } else if(params.checkJanjiPenyerahan){
            String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
            def penyerahanDate =  new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
            params."sCriteria_tglView" = penyerahanDate

        }
        def tanggalView = datatablesUtilService?.syncTime()
        if (params."sCriteria_tglView") {
            tanggalView = params."sCriteria_tglView"
            tanggalView.clearTime()
        }
        def manPowerStalls = loadAllManPowerStall(companyDealer, params)
        def c = JPB.createCriteria()
        def results = c.list () {
            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", tanggalView)
                lt("t451TglJamPlan", tanggalView + 1)
            }
        }
        manPowerStalls = showJPB(results, manPowerStalls ,params.aksi,"other",false)

        def candidates = []
        def canDate = params."sCriteria_tglView"
        for(ASBLine asbLine: manPowerStalls) {
            for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                if (freeSlot.length >= totalRate || totalRate>600) {
                    candidates << freeSlot
                }
            }

        }

        def harusDatang = null
        def targetSelesai = null
        def candidate = null
        def sisaRate = null
        for (ASBFreeSlot freeSlot : candidates) {
            Date slotStart = freeSlot.startDateTime
            Date slotEnd = null
            def breakEnd = null
            use( TimeCategory ) {
                slotEnd = slotStart + (freeSlot.length + freeSlot.breakLength).minutes
                if(freeSlot.breakStart && freeSlot.breakLength)
                    breakEnd = freeSlot.breakStart + freeSlot.breakLength.minutes
            }

            if(slotEnd) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                def waktuSekarang = df.format(params."sCriteria_tglView")
                Date jam5 = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 17:00")
                Date jamTujuh = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 07:00")
                if (params.checkJanjiPenyerahan) {
                    String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
                    def penyerahanDate = new Date().parse('d-MM-yyyy HH:mm', penyerahan.toString())

                    if(breakEnd && freeSlot.breakStart < penyerahanDate && breakEnd > penyerahanDate)
                        penyerahanDate = breakEnd
                    if(penyerahanDate <= slotEnd){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = penyerahanDate - iTotalRate.minutes
                            if(tempDate < jamTujuh){
                                def tempSisaRate = jamTujuh - tempDate
                                tempDate = jamTujuh
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                                iTotalRate = iTotalRate - sisaRate
                            }
                            if(freeSlot.breakLength > 0){
                                if(breakEnd > tempDate && breakEnd <= penyerahanDate){
                                    harusDatang = penyerahanDate - (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    harusDatang = tempDate
                                }
                            } else {
                                harusDatang = tempDate
                            }
                        }
                        if(harusDatang && harusDatang >= slotStart) {
                            if (harusDatang.minutes > 45) {
                                harusDatang.minutes = 0
                                harusDatang.hours = harusDatang.hours + 1
                            } else if (harusDatang.minutes > 30)  {
                                harusDatang.minutes = 45
                            }else if (harusDatang.minutes > 15) {
                                harusDatang.minutes = 30
                            }else if (harusDatang.minutes > 0) {
                                harusDatang.minutes = 15
                            } else {
                                harusDatang.minutes = 0
                            }

                            targetSelesai = penyerahanDate
                            candidate = freeSlot
                            break
                        }

                    }
                }
                else if (params.checkJanjiDatang) {
                    String rencana = params.tglRencana + " " + params.jamRencana
                    def rencanaDate = new Date().parse('dd-MM-yyyy HH:mm',rencana.toString())
                    if(breakEnd && freeSlot.breakStart < rencanaDate && breakEnd > rencanaDate)
                        rencanaDate = freeSlot.breakStart
                    if(rencanaDate >= slotStart){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = rencanaDate + iTotalRate.minutes
                            if(tempDate > jam5){
                                def tempSisaRate = tempDate - jam5
                                tempDate = jam5
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(freeSlot.breakStart >= rencanaDate && freeSlot.breakStart < tempDate){
                                    targetSelesai = rencanaDate + (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    targetSelesai = tempDate
                                }
                            } else {
                                targetSelesai = tempDate
                            }
                        }
                        if(targetSelesai && targetSelesai  <= slotEnd) {
                            if (targetSelesai.minutes > 45) {
                                targetSelesai.minutes = 0
                                targetSelesai.hours = targetSelesai.hours + 1
                            } else if (targetSelesai.minutes > 30)  {
                                targetSelesai.minutes = 45
                            }else if (targetSelesai.minutes > 15) {
                                targetSelesai.minutes = 30
                            }else if (targetSelesai.minutes > 0) {
                                targetSelesai.minutes = 15
                            } else {
                                targetSelesai.minutes = 0
                            }

                            harusDatang = rencanaDate
                            candidate = freeSlot
                            break
                        }
                    }
                }
            }

        }
        if(candidate) {
            def slot = []
            def currentTime = harusDatang
            while(currentTime < targetSelesai){

                slot << "jam"+currentTime.hours+(currentTime.minutes==0?"":currentTime.minutes)
                use( TimeCategory ) {
                    currentTime = currentTime + 15.minutes
                }
            }
            res."start" = harusDatang
            res."end" = targetSelesai
            res."tanggalCari" = harusDatang?.format("")
            res.slot = slot
            res."stallId" = candidate.line.stallId
            res."tanggal" = canDate
            res."totalRate" = totalRate
            res."sisaRate" = sisaRate
            res."status" = "found"
        } else {
            res."status" = "notfound"
        }
        res
    }

    def datatablesList(def params, CompanyDealer companyDealer) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")

        def manPowerStalls = loadAllManPowerStall(companyDealer, params)

        def c = JPB.createCriteria()

        def results = c.list {
            eq("staDel","0")
            eq("companyDealer",companyDealer)
            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", params."sCriteria_tglView")
                lt("t451TglJamPlan", params."sCriteria_tglView" + 1)
                params.tglView = sdf.format(params."sCriteria_tglView")
            }else{
                ge("t451TglJamPlan", dateAwal)
                lt("t451TglJamPlan", dateAkhir)
            }
            if(params.aksi && params.aksi == "viewASBJPB"){
                reception{
                    isNotNull("t401NoAppointment")
                }
            }
        }
        manPowerStalls = showJPB(results, manPowerStalls, params.aksi,params?.dari,false)
        def totalFree = 0.0
        def manPowerStallsWithActual = []
        manPowerStalls.each {ASBLine line ->
            if(!line?.actual){
                line.findFreeSlots().each {ASBFreeSlot freeSlot->
                    if(freeSlot){
                        totalFree = totalFree + freeSlot.length
                    }
                }
            }
            manPowerStallsWithActual << line
        }


        totalFree = totalFree / 60

        [sEcho: params.sEcho, iTotalRecords:  manPowerStallsWithActual.size(), iTotalDisplayRecords: manPowerStallsWithActual.size(), aaData: manPowerStallsWithActual, appTotal: results.size(), sisa : "" +totalFree + " jam"/*asbs.sisa*/]


    }

    def datatablesListMechanicalBoard(def params, CompanyDealer companyDealer) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")

        def manPowerStalls = loadAllManPowerStall(companyDealer, params)

        def c = JPB.createCriteria()
        def results = c.list {
            eq("staDel","0")
            eq("companyDealer", companyDealer)
            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", params."sCriteria_tglView")
                lt("t451TglJamPlan", params."sCriteria_tglView" + 1)
                params.tglView = sdf.format(params."sCriteria_tglView")
            }else{
                ge("t451TglJamPlan", dateAwal)
                lt("t451TglJamPlan", dateAkhir)
            }
        }
        manPowerStalls = showJPB(results, manPowerStalls, params.aksi,"mboard",true)
        def totalFree = 0.0
        def manPowerStallsWithActual = []
        manPowerStalls.each {ASBLine line ->
            line.findFreeSlots().each {ASBFreeSlot freeSlot->
                if(freeSlot)
                    totalFree = totalFree + freeSlot.length
            }
            manPowerStallsWithActual << line
        }

        totalFree = totalFree / 60

        [sEcho: params.sEcho, iTotalRecords:  manPowerStallsWithActual.size(), iTotalDisplayRecords: manPowerStallsWithActual.size(), aaData: manPowerStallsWithActual, appTotal: results.size()]


    }

    def datatablesListADay(def params) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        def c = JPB.createCriteria()
        def results = c.list {
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer)

            if(params.sCriteria_dealer){
                eq("id", params.sCriteria_dealer as Long)
            }

            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", params."sCriteria_tglView")
                lt("t451TglJamPlan", params."sCriteria_tglView" + 1)
            } else {
                Date now = sdf.parse(sdf.format(new Date()))
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.DATE, 1);
                Date tomorrow = cal.getTime();

                ge("t451TglJamPlan", now)
                lt("t451TglJamPlan", tomorrow)
            }
        }
        def res = selectJpb(results, false)
        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: res.rows, appTotal: res.apps.size()]
    }

    def datatablesListNextDay(def params) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        def c = JPB.createCriteria()
        def results = c.list {
            eq("staDel","0")
            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", params."sCriteria_tglView" + 1)
                lt("t451TglJamPlan", params."sCriteria_tglView" + 2)
            } else {
                Date now = sdf.parse(sdf.format(new Date()))
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.DATE, 1);
                Date tomorrow = cal.getTime();

                cal.add(Calendar.DATE, 1);
                Date dayAfterTomorrow = cal.getTime();

                ge("t451TglJamPlan", tomorrow)
                lt("t451TglJamPlan", dayAfterTomorrow)
            }
        }
        def res = selectJpb(results, false)
        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: res.rows, appTotal: res.apps.size()]
    }

    def datatablesListWeek(def params, CompanyDealer companyDealer) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        DateFormat dayFormat = new SimpleDateFormat("EEE");
        def c = JPB.createCriteria()

        Date theDay = sdf.parse(sdf.format(new Date()));
        if (params."sCriteria_tglViewWeek") {
            theDay = params."sCriteria_tglViewWeek"
        }

        String theDayName = dayFormat.format(theDay)
        Date startDate = datatablesUtilService?.syncTime()
        if (theDayName.equalsIgnoreCase("MON") ) {
            startDate = theDay
        } else if (theDayName.equalsIgnoreCase("TUE") ) {
            startDate = theDay - 1
        } else if (theDayName.equalsIgnoreCase("WED") ) {
            startDate = theDay - 2
        } else if (theDayName.equalsIgnoreCase("THU") ) {
            startDate = theDay - 3
        } else if (theDayName.equalsIgnoreCase("FRI") ) {
            startDate = theDay - 4
        } else if (theDayName.equalsIgnoreCase("SAT") ) {
            startDate = theDay - 5
        } else if (theDayName.equalsIgnoreCase("SUN") ) {
            startDate = theDay - 6
        }

        def results = c.list {
            eq("companyDealer",companyDealer)
            eq("staDel","0")
            reception{
                not{
                    isNull("tipeKerusakan")
                }
            }
            ge("t451TglJamPlan", startDate)
            lt("t451TglJamPlan", startDate + 7)
            params.tglView = sdf.format(theDay)
        }

        ArrayList rows = selectJpbWeek(results)

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    def datatablesListMonth(def params, CompanyDealer companyDealer) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")

        def c = JPB.createCriteria()

        Date theDay = sdf.parse(sdf.format(new Date()));
        if (params."sCriteria_tglViewMonth") {
            theDay = params."sCriteria_tglViewMonth"
        }

        def results = c.list {
            eq("companyDealer",companyDealer)
            eq("staDel","0")
            ge("t451TglJamPlan", theDay)
            lt("t451TglJamPlan", theDay + 31)
        }

        ArrayList rows = selectJpbMonth(results, theDay)

        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }

    private Map selectJpb(List<JPB> results, boolean isShowPlat) {
        def res = [:]
        def apps = []
        def rows = []

        def x = 0
        results.each {
            def appointments = it?.appointments
            DateFormat hh = new SimpleDateFormat("HH")
            DateFormat mm = new SimpleDateFormat("mm")
            Map newMap = new HashMap()
            def jpbId = it?.id
            def alfabet = "V"
            def alfabet1 = "V1"
            appointments.each {
                if(it?.t301TglJamRencana ||it?.t301TglJamPenyerahanSchedule){
                    if(!apps.contains(it.reception?.t401NoWO)){
                        apps<<it.reception?.t401NoWO
                    }
                    def jamJanjiMulai = it?.t301TglJamRencana
                    def jamJanjiSelesai = it?.t301TglJamPenyerahanSchedule
                    int shhMulai = Integer.parseInt(hh.format(jamJanjiMulai))
                    int smmMulai = Integer.parseInt(mm.format(jamJanjiMulai))
                    int shhSelesai = Integer.parseInt(hh.format(jamJanjiSelesai))
                    int smmSelesai = Integer.parseInt(mm.format(jamJanjiSelesai))
                    alfabet = (x == 0) ? "V" : (x == 1) ? "W" : (x == 2) ? "X" : (x == 3) ? "Y" : "Z"

                    alfabet1 = (x == 0) ? "V1" : (x == 1) ? "W1" : (x == 2) ? "X1" : (x == 3) ? "Y1" : "Z1"
                    if (isShowPlat) {
                        alfabet1 += it?.historyCustomerVehicle?.fullNoPol + "<br/>" + it?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName
                    }
                    x = (x == 4) ? 0 : x + 1
                    def isFirst = true
                    def alfa = (isFirst) ? alfabet1 : alfabet
                    switch (shhMulai) {
                        case 7:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "07" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "07" + THIRTY, alfa)
                            if (shhSelesai >= 8) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "08" + "00", alfabet)
                                if (shhSelesai >= 9) {
                                    newMap.put("" + jpbId + "08" + "00", alfabet)
                                    newMap.put("" + jpbId + "08" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "09" + "00", alfabet)
                                    if (shhSelesai >= 10) {
                                        newMap.put("" + jpbId + "09" + "00", alfabet)
                                        newMap.put("" + jpbId + "09" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "10" + "00", alfabet)
                                        if (shhSelesai >= 11) {
                                            newMap.put("" + jpbId + "10" + "00", alfabet)
                                            newMap.put("" + jpbId + "10" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                                            if (shhSelesai >= 12) {
                                                newMap.put("" + jpbId + "11" + "00", alfabet)
                                                newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                                if (shhSelesai >= 13) {
                                                    newMap.put("" + jpbId + "12" + "00", alfabet)
                                                    newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                                    if (shhSelesai >= 14) {
                                                        newMap.put("" + jpbId + "13" + "00", alfabet)
                                                        newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                                        if (shhSelesai >= 15) {
                                                            newMap.put("" + jpbId + "14" + "00", alfabet)
                                                            newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                                            if (shhSelesai >= 16) {
                                                                newMap.put("" + jpbId + "15" + "00", alfabet)
                                                                newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                                if (shhSelesai >= 17) {
                                                                    newMap.put("" + jpbId + "16" + "00", alfabet)
                                                                    newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break
                        case 8:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "08" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "08" + THIRTY, alfa)
                            if (shhSelesai >= 9) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "09" + "00", alfabet)
                                if (shhSelesai >= 10) {
                                    newMap.put("" + jpbId + "09" + "00", alfabet)
                                    newMap.put("" + jpbId + "09" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "10" + "00", alfabet)
                                    if (shhSelesai >= 11) {
                                        newMap.put("" + jpbId + "10" + "00", alfabet)
                                        newMap.put("" + jpbId + "10" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                                        if (shhSelesai >= 12) {
                                            newMap.put("" + jpbId + "11" + "00", alfabet)
                                            newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                            if (shhSelesai >= 13) {
                                                newMap.put("" + jpbId + "12" + "00", alfabet)
                                                newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                                if (shhSelesai >= 14) {
                                                    newMap.put("" + jpbId + "13" + "00", alfabet)
                                                    newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                                    if (shhSelesai >= 15) {
                                                        newMap.put("" + jpbId + "14" + "00", alfabet)
                                                        newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                                        if (shhSelesai >= 16) {
                                                            newMap.put("" + jpbId + "15" + "00", alfabet)
                                                            newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                            if (shhSelesai >= 17) {
                                                                newMap.put("" + jpbId + "16" + "00", alfabet)
                                                                newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break
                        case 9:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "09" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "09" + THIRTY, alfa)
                            if (shhSelesai >= 10) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "10" + "00", alfabet)
                                if (shhSelesai >= 11) {
                                    newMap.put("" + jpbId + "10" + "00", alfabet)
                                    newMap.put("" + jpbId + "10" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                                    if (shhSelesai >= 12) {
                                        newMap.put("" + jpbId + "11" + "00", alfabet)
                                        newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                        if (shhSelesai >= 13) {
                                            newMap.put("" + jpbId + "12" + "00", alfabet)
                                            newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                            if (shhSelesai >= 14) {
                                                newMap.put("" + jpbId + "13" + "00", alfabet)
                                                newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                                if (shhSelesai >= 15) {
                                                    newMap.put("" + jpbId + "14" + "00", alfabet)
                                                    newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                                    if (shhSelesai >= 16) {
                                                        newMap.put("" + jpbId + "15" + "00", alfabet)
                                                        newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                        if (shhSelesai >= 17) {
                                                            newMap.put("" + jpbId + "16" + "00", alfabet)
                                                            newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break
                        case 10:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "10" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "10" + THIRTY, alfa)
                            if (shhSelesai >= 11) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                                if (shhSelesai >= 12) {
                                    newMap.put("" + jpbId + "11" + "00", alfabet)
                                    newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                    if (shhSelesai >= 13) {
                                        newMap.put("" + jpbId + "12" + "00", alfabet)
                                        newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                        if (shhSelesai >= 14) {
                                            newMap.put("" + jpbId + "13" + "00", alfabet)
                                            newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                            if (shhSelesai >= 15) {
                                                newMap.put("" + jpbId + "14" + "00", alfabet)
                                                newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                                if (shhSelesai >= 16) {
                                                    newMap.put("" + jpbId + "15" + "00", alfabet)
                                                    newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                    if (shhSelesai >= 17) {
                                                        newMap.put("" + jpbId + "16" + "00", alfabet)
                                                        newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break
                        case 11:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "11" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "11" + THIRTY, alfa)
                            if (shhSelesai >= 12) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                if (shhSelesai >= 13) {
                                    newMap.put("" + jpbId + "12" + "00", alfabet)
                                    newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                    if (shhSelesai >= 14) {
                                        newMap.put("" + jpbId + "13" + "00", alfabet)
                                        newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                        if (shhSelesai >= 15) {
                                            newMap.put("" + jpbId + "14" + "00", alfabet)
                                            newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                            if (shhSelesai >= 16) {
                                                newMap.put("" + jpbId + "15" + "00", alfabet)
                                                newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                if (shhSelesai >= 17) {
                                                    newMap.put("" + jpbId + "16" + "00", alfabet)
                                                    newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break
                        case 12:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "12" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "12" + THIRTY, alfa)
                            if (shhSelesai >= 13) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                if (shhSelesai >= 14) {
                                    newMap.put("" + jpbId + "13" + "00", alfabet)
                                    newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                    if (shhSelesai >= 15) {
                                        newMap.put("" + jpbId + "14" + "00", alfabet)
                                        newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                        if (shhSelesai >= 16) {
                                            newMap.put("" + jpbId + "15" + "00", alfabet)
                                            newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                            if (shhSelesai >= 17) {
                                                newMap.put("" + jpbId + "16" + "00", alfabet)
                                                newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                            }
                                        }
                                    }
                                }
                            }
                            break
                        case 13:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "13" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "13" + THIRTY, alfa)
                            if (shhSelesai >= 14) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                if (shhSelesai >= 15) {
                                    newMap.put("" + jpbId + "14" + "00", alfabet)
                                    newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                    if (shhSelesai >= 16) {
                                        newMap.put("" + jpbId + "15" + "00", alfabet)
                                        newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                        if (shhSelesai >= 17) {
                                            newMap.put("" + jpbId + "16" + "00", alfabet)
                                            newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                        }
                                    }
                                }
                            }
                            break
                        case 14:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "14" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "14" + THIRTY, alfa)
                            if (shhSelesai >= 15) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                if (shhSelesai >= 16) {
                                    newMap.put("" + jpbId + "15" + "00", alfabet)
                                    newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                    if (shhSelesai >= 17) {
                                        newMap.put("" + jpbId + "16" + "00", alfabet)
                                        newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                    }
                                }
                            }
                            break
                        case 15:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "15" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "15" + THIRTY, alfa)
                            if (shhSelesai >= 16) {
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                if (shhSelesai >= 17) {
                                    newMap.put("" + jpbId + "16" + "00", alfabet)
                                    newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                }
                            }
                            break
                        case 16:
                            if (smmMulai < THIRTY) {
                                newMap.put("" + jpbId + "16" + "00", alfa)
                                isFirst = false
                            }
                            alfa = (isFirst) ? alfabet1 : alfabet
                            newMap.put("" + jpbId + "16" + THIRTY, alfa)
                            if (shhSelesai >= 17) {
                            }
                            break
                        default:
                            break
                    }
                }

            }

            rows << [
                    foreman: it?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,
                    foremanId : it?.namaManPower?.groupManPower?.namaManPowerForeman?.id,
                    group: it.namaManPower?.groupManPower?.t021Group,
                    groupId: it.namaManPower?.groupManPower?.id,
                    jpbId : it.id,
                    teknisi: it.namaManPower?.t015NamaBoard,
                    teknisiId: it.namaManPower?.id,
                    stall: it.stall?.m022NamaStall,

                    jam7: newMap.get("" + jpbId + "07" + "00"),
                    jam730: newMap.get("" + jpbId + "07" + THIRTY),
                    jam8: newMap.get("" + jpbId + "08" + "00"),
                    jam830: newMap.get("" + jpbId + "08" + THIRTY),
                    jam9: newMap.get("" + jpbId + "09" + "00"),
                    jam930: newMap.get("" + jpbId + "09" + THIRTY),
                    jam10: newMap.get("" + jpbId + "10" + "00"),
                    jam1030: newMap.get("" + jpbId + "10" + THIRTY),
                    jam11: newMap.get("" + jpbId + "11" + "00"),
                    jam1130: "BREAK",
                    jam12: "BREAK",
                    jam1230: newMap.get("" + jpbId + "12" + THIRTY),
                    jam13: newMap.get("" + jpbId + "13" + "00"),
                    jam1330: newMap.get("" + jpbId + "13" + THIRTY),
                    jam14: newMap.get("" + jpbId + "14" + "00"),
                    jam1430: newMap.get("" + jpbId + "14" + THIRTY),
                    jam15: newMap.get("" + jpbId + "15" + "00"),
                    jam1530: newMap.get("" + jpbId + "15" + THIRTY),
                    jam16: newMap.get("" + jpbId + "16" + "00"),
                    jam1630: newMap.get("" + jpbId + "16" + THIRTY),
            ]
        }
        res.rows = rows
        res.apps = apps
        res
    }

    private ArrayList selectJpbWeek(List<JPB> results) {
        def rows = []
        DateFormat hh = new SimpleDateFormat("HH")
        DateFormat dayFormat = new SimpleDateFormat("EEE");
        def x = 0
        def alfa = "V"

        results.each {
            def appointments = it?.appointments

            Map newMap = new HashMap()
            def jpbId = it?.id
            alfa = (x == 0) ? "V" : (x == 1) ? "W" : (x == 2) ? "X" : (x == 3) ? "Y" : "Z"
            x = (x == 4) ? 0 : x + 1
            appointments.each {
                def jamJanjiMulai = it?.t301TglJamRencana
                def jamJanjiSelesai = it?.t301TglJamPenyerahanSchedule

                String dayApp = dayFormat.format(jamJanjiMulai)
                int shhMulai = Integer.parseInt(hh.format(jamJanjiMulai))

                if (dayApp.equalsIgnoreCase("MON")) {
                    if (shhMulai < 12) newMap.put("" + jpbId + "SENIN" + "FIRST", alfa)
                    else newMap.put("" + jpbId + "SENIN" + "SECOND", alfa)
                } else if (dayApp.equalsIgnoreCase("TUE")) {
                    if (shhMulai < 12) newMap.put("" + jpbId + "SELASA" + "FIRST", alfa)
                    else newMap.put("" + jpbId + "SELASA" + "SECOND", alfa)
                } else if (dayApp.equalsIgnoreCase("WED")) {
                    if (shhMulai < 12) newMap.put("" + jpbId + "RABU" + "FIRST", alfa)
                    else newMap.put("" + jpbId + "RABU" + "SECOND", alfa)
                } else if (dayApp.equalsIgnoreCase("THU")) {
                    if (shhMulai < 12) newMap.put("" + jpbId + "KAMIS" + "FIRST", alfa)
                    else newMap.put("" + jpbId + "KAMIS" + "SECOND", alfa)
                } else if (dayApp.equalsIgnoreCase("FRI")) {
                    if (shhMulai < 12) newMap.put("" + jpbId + "JUMAT" + "FIRST", alfa)
                    else newMap.put("" + jpbId + "JUMAT" + "SECOND", alfa)
                } else if (dayApp.equalsIgnoreCase("SAT")) {
                    if (shhMulai < 12) newMap.put("" + jpbId + "SABTU" + "FIRST", alfa)
                    else newMap.put("" + jpbId + "SABTU" + "SECOND", alfa)
                } else {
                    if (shhMulai < 12) newMap.put("" + jpbId + "MINGGU" + "FIRST", alfa)
                    else newMap.put("" + jpbId + "MINGGU" + "SECOND", alfa)
                }
            }

            rows << [
                    foreman: it?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,
                    foremanId : it?.namaManPower?.groupManPower?.namaManPowerForeman?.id,
                    group: it.namaManPower?.groupManPower?.t021Group,
                    groupId: it.namaManPower?.groupManPower?.id,
                    jpbId : it.id,
                    teknisi: it.namaManPower?.t015NamaBoard,
                    teknisiId: it.namaManPower?.id,
                    stall: it.stall?.m022NamaStall,

                    seninFirst: newMap.get("" + jpbId + "SENIN" + "FIRST" ),
                    seninSecond: newMap.get("" + jpbId + "SENIN" + "SECOND"),
                    selasaFirst: newMap.get("" + jpbId + "SELASA" + "FIRST"),
                    selasaSecond: newMap.get("" + jpbId + "SELASA" + "SECOND"),
                    rabuFirst: newMap.get("" + jpbId + "RABU" + "FIRST"),
                    rabuSecond: newMap.get("" + jpbId + "RABU" + "SECOND"),
                    kamisFirst: newMap.get("" + jpbId + "KAMIS" + "FIRST"),
                    kamisSecond: newMap.get("" + jpbId + "KAMIS" + "SECOND"),
                    jumatFirst: newMap.get("" + jpbId + "JUMAT" + "FIRST"),
                    jumatSecond: newMap.get("" + jpbId + "JUMAT" + "SECOND"),
                    sabtuFirst: newMap.get("" + jpbId + "SABTU" + "FIRST"),
                    sabtuSecond: newMap.get("" + jpbId + "SABTU" + "SECOND"),
                    mingguFirst: newMap.get("" + jpbId + "MINGGU" + "FIRST"),
                    mingguSecond: newMap.get("" + jpbId + "MINGGU" + "SECOND"),
            ]
        }
        rows
    }

    private ArrayList selectJpbMonth(List<JPB> results, Date startDate) {
        DateFormat dFormat = new SimpleDateFormat("dd")
        DateFormat mFormat = new SimpleDateFormat("MM")
        DateFormat yFormat = new SimpleDateFormat("yyyy")
        DateFormat mmddFormat = new SimpleDateFormat("MMdd")
        String sdt = dFormat.format(startDate)
        int dt = Integer.parseInt(sdt)
        String smo = mFormat.format(startDate)
        int mo = Integer.parseInt(smo)
        int yy = Integer.parseInt(smo)
        int lastDate = 30
        if (mo == 1 || mo == 3 || mo == 5 || mo == 7 || mo == 8 || mo == 10 || mo == 12) lastDate = 30
        else if (mo == 2) {
            if (yy % 4 == 0 ) lastDate = 29
            else lastDate = 28
        }
        String dt1 = smo + sdt
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt2 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt3 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt4 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt5 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt6 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt7 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt8 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt9 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt10 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt11 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt12 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt13 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt14 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt15 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt16 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt17 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt18 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt19 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt20 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt21 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt22 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt23 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt24 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt25 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt26 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt27 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt28 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt29 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt30 = smo + ((dt < 10) ? "0" + dt : dt)
        (smo, dt) = checkMonthDate(dt, lastDate, mo, smo)
        String dt31 = smo + ((dt < 10) ? "0" + dt : dt)

        def rows = []

        def x = 0
        def alfa = "V"
        results.each {
            def appointments = it?.appointments

            Map newMap = new HashMap()
            def jpbId = it?.id
            alfa = (x == 0) ? "V" : (x == 1) ? "W" : (x == 2) ? "X" : (x == 3) ? "Y" : "Z"
            x = (x == 4) ? 0 : x + 1
            appointments.each {
                def jamJanjiMulai = it?.t301TglJamRencana

                newMap.put("" + jpbId + mmddFormat.format(jamJanjiMulai), alfa)
            }
            rows << [
                    foreman: it?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,
                    foremanId : it?.namaManPower?.groupManPower?.namaManPowerForeman?.id,
                    group: it.namaManPower?.groupManPower?.t021Group,
                    groupId: it.namaManPower?.groupManPower?.id,
                    jpbId : it.id,
                    teknisi: it.namaManPower?.t015NamaBoard,
                    teknisiId: it.namaManPower?.id,
                    stall: it.stall?.m022NamaStall,

                    date1: newMap.get("" + jpbId + dt1),
                    date2: newMap.get("" + jpbId + dt2),
                    date3: newMap.get("" + jpbId + dt3),
                    date4: newMap.get("" + jpbId + dt4),
                    date5: newMap.get("" + jpbId + dt5),
                    date6: newMap.get("" + jpbId + dt6),
                    date7: newMap.get("" + jpbId + dt7),
                    date8: newMap.get("" + jpbId + dt8),
                    date9: newMap.get("" + jpbId + dt9),
                    date10: newMap.get("" + jpbId + dt10),
                    date11: newMap.get("" + jpbId + dt11),
                    date12: newMap.get("" + jpbId + dt12),
                    date13: newMap.get("" + jpbId + dt13),
                    date14: newMap.get("" + jpbId + dt14),
                    date15: newMap.get("" + jpbId + dt15),
                    date16: newMap.get("" + jpbId + dt16),
                    date17: newMap.get("" + jpbId + dt17),
                    date18: newMap.get("" + jpbId + dt18),
                    date19: newMap.get("" + jpbId + dt19),
                    date20: newMap.get("" + jpbId + dt20),
                    date21: newMap.get("" + jpbId + dt21),
                    date22: newMap.get("" + jpbId + dt22),
                    date23: newMap.get("" + jpbId + dt23),
                    date24: newMap.get("" + jpbId + dt24),
                    date25: newMap.get("" + jpbId + dt25),
                    date26: newMap.get("" + jpbId + dt26),
                    date27: newMap.get("" + jpbId + dt27),
                    date28: newMap.get("" + jpbId + dt28),
                    date29: newMap.get("" + jpbId + dt29),
                    date30: newMap.get("" + jpbId + dt30),
                    date31: newMap.get("" + jpbId + dt31),
            ]
        }
        rows
    }

    private List checkMonthDate(int dt, int lastDate, int mo, String smo) {
        dt += 1
        if (dt == lastDate) {
            mo += 1
            smo = (mo > 12) ? "01" + mo : (mo > 10) ? "" + mo : "0" + mo
            dt = 1
        }
        [smo, dt]
    }

    def datatablesListDriver(def params) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        def c = JPB.createCriteria()
        def results = c.list {
            eq("staDel","0")
            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", params."sCriteria_tglView")
                lt("t451TglJamPlan", params."sCriteria_tglView" + 1)
            } else {
                Date now = sdf.parse(sdf.format(new Date()))
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.DATE, 1);
                Date tomorrow = cal.getTime();

                ge("t451TglJamPlan", now)
                lt("t451TglJamPlan", tomorrow)
            }
        }
        ArrayList rows = selectJpbDriver(results)
        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
    }

    private ArrayList selectJpbDriver(List<JPB> results) {
        def rows = []

        def x = 0
        results.each {
            def appointments = it?.appointments
            DateFormat hh = new SimpleDateFormat("HH")
            DateFormat mm = new SimpleDateFormat("mm")
            Map newMap = new HashMap()
            def jpbId = it?.id
            def alfabet = "V"
            def alfabet1 = "V1"
            appointments.each {
                def jamJanjiMulai = it?.t301TglJamRencana
                def jamJanjiSelesai = it?.t301TglJamPenyerahanSchedule
                int shhMulai = Integer.parseInt(hh.format(jamJanjiMulai))
                int smmMulai = Integer.parseInt(mm.format(jamJanjiMulai))
                int shhSelesai = Integer.parseInt(hh.format(jamJanjiSelesai))
                int smmSelesai = Integer.parseInt(mm.format(jamJanjiSelesai))
                alfabet = (x == 0) ? "V" : (x == 1) ? "W" : (x == 2) ? "X" : (x == 3) ? "Y" : "Z"
                alfabet1 = (x == 0) ? "V1" : (x == 1) ? "W1" : (x == 2) ? "X1" : (x == 3) ? "Y1" : "Z1"
                x = (x == 4) ? 0 : x + 1
                def isFirst = true
                def alfa = (isFirst) ? alfabet1 : alfabet
                switch (shhMulai) {
                    case 7:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "07" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "07" + THIRTY, alfa)
                        if (shhSelesai >= 8) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "08" + "00", alfabet)
                            if (shhSelesai >= 9) {
                                newMap.put("" + jpbId + "08" + "00", alfabet)
                                newMap.put("" + jpbId + "08" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "09" + "00", alfabet)
                                if (shhSelesai >= 10) {
                                    newMap.put("" + jpbId + "09" + "00", alfabet)
                                    newMap.put("" + jpbId + "09" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "10" + "00", alfabet)
                                    if (shhSelesai >= 11) {
                                        newMap.put("" + jpbId + "10" + "00", alfabet)
                                        newMap.put("" + jpbId + "10" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                                        if (shhSelesai >= 12) {
                                            newMap.put("" + jpbId + "11" + "00", alfabet)
                                            newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                            if (shhSelesai >= 13) {
                                                newMap.put("" + jpbId + "12" + "00", alfabet)
                                                newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                                if (shhSelesai >= 14) {
                                                    newMap.put("" + jpbId + "13" + "00", alfabet)
                                                    newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                                    if (shhSelesai >= 15) {
                                                        newMap.put("" + jpbId + "14" + "00", alfabet)
                                                        newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                                        if (shhSelesai >= 16) {
                                                            newMap.put("" + jpbId + "15" + "00", alfabet)
                                                            newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                            if (shhSelesai >= 17) {
                                                                newMap.put("" + jpbId + "16" + "00", alfabet)
                                                                newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 8:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "08" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "08" + THIRTY, alfa)
                        if (shhSelesai >= 9) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "09" + "00", alfabet)
                            if (shhSelesai >= 10) {
                                newMap.put("" + jpbId + "09" + "00", alfabet)
                                newMap.put("" + jpbId + "09" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "10" + "00", alfabet)
                                if (shhSelesai >= 11) {
                                    newMap.put("" + jpbId + "10" + "00", alfabet)
                                    newMap.put("" + jpbId + "10" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                                    if (shhSelesai >= 12) {
                                        newMap.put("" + jpbId + "11" + "00", alfabet)
                                        newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                        if (shhSelesai >= 13) {
                                            newMap.put("" + jpbId + "12" + "00", alfabet)
                                            newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                            if (shhSelesai >= 14) {
                                                newMap.put("" + jpbId + "13" + "00", alfabet)
                                                newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                                if (shhSelesai >= 15) {
                                                    newMap.put("" + jpbId + "14" + "00", alfabet)
                                                    newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                                    if (shhSelesai >= 16) {
                                                        newMap.put("" + jpbId + "15" + "00", alfabet)
                                                        newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                        if (shhSelesai >= 17) {
                                                            newMap.put("" + jpbId + "16" + "00", alfabet)
                                                            newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 9:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "09" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "09" + THIRTY, alfa)
                        if (shhSelesai >= 10) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "10" + "00", alfabet)
                            if (shhSelesai >= 11) {
                                newMap.put("" + jpbId + "10" + "00", alfabet)
                                newMap.put("" + jpbId + "10" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                                if (shhSelesai >= 12) {
                                    newMap.put("" + jpbId + "11" + "00", alfabet)
                                    newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                    if (shhSelesai >= 13) {
                                        newMap.put("" + jpbId + "12" + "00", alfabet)
                                        newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                        if (shhSelesai >= 14) {
                                            newMap.put("" + jpbId + "13" + "00", alfabet)
                                            newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                            if (shhSelesai >= 15) {
                                                newMap.put("" + jpbId + "14" + "00", alfabet)
                                                newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                                if (shhSelesai >= 16) {
                                                    newMap.put("" + jpbId + "15" + "00", alfabet)
                                                    newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                    if (shhSelesai >= 17) {
                                                        newMap.put("" + jpbId + "16" + "00", alfabet)
                                                        newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 10:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "10" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "10" + THIRTY, alfa)
                        if (shhSelesai >= 11) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "11" + "00", alfabet)
                            if (shhSelesai >= 12) {
                                newMap.put("" + jpbId + "11" + "00", alfabet)
                                newMap.put("" + jpbId + "11" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                                if (shhSelesai >= 13) {
                                    newMap.put("" + jpbId + "12" + "00", alfabet)
                                    newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                    if (shhSelesai >= 14) {
                                        newMap.put("" + jpbId + "13" + "00", alfabet)
                                        newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                        if (shhSelesai >= 15) {
                                            newMap.put("" + jpbId + "14" + "00", alfabet)
                                            newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                            if (shhSelesai >= 16) {
                                                newMap.put("" + jpbId + "15" + "00", alfabet)
                                                newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                                if (shhSelesai >= 17) {
                                                    newMap.put("" + jpbId + "16" + "00", alfabet)
                                                    newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 11:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "11" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "11" + THIRTY, alfa)
                        if (shhSelesai >= 12) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "12" + "00", alfabet)
                            if (shhSelesai >= 13) {
                                newMap.put("" + jpbId + "12" + "00", alfabet)
                                newMap.put("" + jpbId + "12" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                                if (shhSelesai >= 14) {
                                    newMap.put("" + jpbId + "13" + "00", alfabet)
                                    newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                    if (shhSelesai >= 15) {
                                        newMap.put("" + jpbId + "14" + "00", alfabet)
                                        newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                        if (shhSelesai >= 16) {
                                            newMap.put("" + jpbId + "15" + "00", alfabet)
                                            newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                            if (shhSelesai >= 17) {
                                                newMap.put("" + jpbId + "16" + "00", alfabet)
                                                newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 12:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "12" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "12" + THIRTY, alfa)
                        if (shhSelesai >= 13) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "13" + "00", alfabet)
                            if (shhSelesai >= 14) {
                                newMap.put("" + jpbId + "13" + "00", alfabet)
                                newMap.put("" + jpbId + "13" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                                if (shhSelesai >= 15) {
                                    newMap.put("" + jpbId + "14" + "00", alfabet)
                                    newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                    if (shhSelesai >= 16) {
                                        newMap.put("" + jpbId + "15" + "00", alfabet)
                                        newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                        if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                        if (shhSelesai >= 17) {
                                            newMap.put("" + jpbId + "16" + "00", alfabet)
                                            newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                        }
                                    }
                                }
                            }
                        }
                        break
                    case 13:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "13" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "13" + THIRTY, alfa)
                        if (shhSelesai >= 14) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "14" + "00", alfabet)
                            if (shhSelesai >= 15) {
                                newMap.put("" + jpbId + "14" + "00", alfabet)
                                newMap.put("" + jpbId + "14" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                                if (shhSelesai >= 16) {
                                    newMap.put("" + jpbId + "15" + "00", alfabet)
                                    newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                    if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                    if (shhSelesai >= 17) {
                                        newMap.put("" + jpbId + "16" + "00", alfabet)
                                        newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                    }
                                }
                            }
                        }
                        break
                    case 14:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "14" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "14" + THIRTY, alfa)
                        if (shhSelesai >= 15) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "15" + "00", alfabet)
                            if (shhSelesai >= 16) {
                                newMap.put("" + jpbId + "15" + "00", alfabet)
                                newMap.put("" + jpbId + "15" + THIRTY, alfabet)
                                if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                                if (shhSelesai >= 17) {
                                    newMap.put("" + jpbId + "16" + "00", alfabet)
                                    newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                                }
                            }
                        }
                        break
                    case 15:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "15" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "15" + THIRTY, alfa)
                        if (shhSelesai >= 16) {
                            if (smmSelesai >= THIRTY) newMap.put("" + jpbId + "16" + "00", alfabet)
                            if (shhSelesai >= 17) {
                                newMap.put("" + jpbId + "16" + "00", alfabet)
                                newMap.put("" + jpbId + "16" + THIRTY, alfabet)
                            }
                        }
                        break
                    case 16:
                        if (smmMulai < THIRTY) {
                            newMap.put("" + jpbId + "16" + "00", alfa)
                            isFirst = false
                        }
                        alfa = (isFirst) ? alfabet1 : alfabet
                        newMap.put("" + jpbId + "16" + THIRTY, alfa)
                        if (shhSelesai >= 17) {
                        }
                        break
                    default:
                        break
                }

            }

            rows << [
                    driver: it?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,

                    jam7: newMap.get("" + jpbId + "07" + "00"),
                    jam730: newMap.get("" + jpbId + "07" + THIRTY),
                    jam8: newMap.get("" + jpbId + "08" + "00"),
                    jam830: newMap.get("" + jpbId + "08" + THIRTY),
                    jam9: newMap.get("" + jpbId + "09" + "00"),
                    jam930: newMap.get("" + jpbId + "09" + THIRTY),
                    jam10: newMap.get("" + jpbId + "10" + "00"),
                    jam1030: newMap.get("" + jpbId + "10" + THIRTY),
                    jam11: newMap.get("" + jpbId + "11" + "00"),
                    jam1130: "BREAK",
                    jam12: "BREAK",
                    jam1230: newMap.get("" + jpbId + "12" + THIRTY),
                    jam13: newMap.get("" + jpbId + "13" + "00"),
                    jam1330: newMap.get("" + jpbId + "13" + THIRTY),
                    jam14: newMap.get("" + jpbId + "14" + "00"),
                    jam1430: newMap.get("" + jpbId + "14" + THIRTY),
                    jam15: newMap.get("" + jpbId + "15" + "00"),
                    jam1530: newMap.get("" + jpbId + "15" + THIRTY),
                    jam16: newMap.get("" + jpbId + "16" + "00"),
                    jam1630: newMap.get("" + jpbId + "16" + THIRTY),
                    jam17: newMap.get("" + jpbId + "17" + "00"),
                    jam1730: newMap.get("" + jpbId + "17" + THIRTY),
                    jam18: newMap.get("" + jpbId + "18" + "00"),
                    jam1830: newMap.get("" + jpbId + "18" + THIRTY),
                    jam19: newMap.get("" + jpbId + "19" + "00"),
                    jam1930: newMap.get("" + jpbId + "19" + THIRTY),
                    jam20: newMap.get("" + jpbId + "20" + "00"),
                    jam2030: newMap.get("" + jpbId + "20" + THIRTY),
                    jam21: newMap.get("" + jpbId + "21" + "00"),
                    jam2130: newMap.get("" + jpbId + "21" + THIRTY),
                    jam22: newMap.get("" + jpbId + "22" + "00"),
                    jam2230: newMap.get("" + jpbId + "22" + THIRTY),
                    jam23: newMap.get("" + jpbId + "23" + "00"),
                    jam2330: newMap.get("" + jpbId + "23" + THIRTY),
            ]
        }
        rows
    }
    def datatablesListTab(def params, CompanyDealer companyDealer) {
        //println("DEALER"+companyDealer)
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        def c = JPB.createCriteria()
        def results = c.list {
            eq("staDel","0")
            reception{
                isNull("tipeKerusakan")
            }
            eq("companyDealer", companyDealer)
            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", params."sCriteria_tglView")
                lt("t451TglJamPlan", params."sCriteria_tglView" + 1)
            } else {
                Date now = sdf.parse(sdf.format(new Date()))
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.DATE, 1);
                Date tomorrow = cal.getTime();

                ge("t451TglJamPlan", now)
                lt("t451TglJamPlan", tomorrow)
            }
        }
        ArrayList rows = selectJpbTab(results)
        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
    }


    def datatablesListTabBP(def params, CompanyDealer companyDealer) {
        //println("DEALERbp"+companyDealer)
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        def c = JPB.createCriteria()
        def results = c.list {
            eq("staDel","0")
            eq("companyDealer", companyDealer)
            reception{
                not{
                    isNull("tipeKerusakan")
                }
            }

            if (params."sCriteria_tglView") {
                ge("t451TglJamPlan", params."sCriteria_tglView")
                lt("t451TglJamPlan", params."sCriteria_tglView" + 1)
            } else {
                Date now = sdf.parse(sdf.format(new Date()))
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.DATE, 1);
                Date tomorrow = cal.getTime();

                ge("t451TglJamPlan", now)
                lt("t451TglJamPlan", tomorrow)
            }
        }
        ArrayList rows = selectJpbTab(results)
        [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
    }

    private ArrayList selectJpbTab(List<JPB> results) {
        def rows = []

        results.each {
            def adakah = true
            def appointments = it?.appointments
            DateFormat hh = new SimpleDateFormat("HH")
            DateFormat hhmm = new SimpleDateFormat("HH:mm")
            Map newMap = new HashMap()
            def jpbId = it?.id
            appointments.each {
                adakah = false
                def nopol = it?.historyCustomerVehicle?.fullNoPol
                def jamAppointment = it?.t301TglJamApp
                int shhApp = Integer.parseInt(hh.format(jamAppointment))
                def alfa = nopol + " ; " +  hhmm.format(jamAppointment)
                switch (shhApp) {
                    case 7:
                        newMap.put("" + jpbId + "07", alfa)
                        break
                    case 8:
                        newMap.put("" + jpbId + "08", alfa)
                        break
                    case 9:
                        newMap.put("" + jpbId + "09", alfa)
                        break
                    case 10:
                        newMap.put("" + jpbId + "10", alfa)
                        break
                    case 11:
                        newMap.put("" + jpbId + "11", alfa)
                        break
                    case 12:
                        newMap.put("" + jpbId + "12", alfa)
                        break
                    case 13:
                        newMap.put("" + jpbId + "13", alfa)
                        break
                    case 14:
                        newMap.put("" + jpbId + "14", alfa)
                        break
                    case 15:
                        newMap.put("" + jpbId + "15", alfa)
                        break
                    case 16:
                        newMap.put("" + jpbId + "16", alfa)
                        break
                    default:
                        break
                }

            }

            if(adakah){
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                def waktuSekarang = df.format(new Date())
                Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
                Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
                def jpbTemp = it
                def asbs = ASB.createCriteria().list {
                    eq("staDel","0")
                    eq("reception",jpbTemp?.reception);
                    ge("t351TglJamApp",dateAwal)
                    le("t351TglJamApp",dateAkhir)
                }

                def cobaLagi = true
                asbs.each {
                    cobaLagi = false
                    def nopol = jpbTemp?.reception?.historyCustomerVehicle?.fullNoPol
                    def jamAppointment = it?.appointment?.t301TglJamRencana
                    int shhApp = Integer.parseInt(hh.format(jamAppointment))
                    def alfa = nopol + " ; " +  hhmm.format(jamAppointment)
                    switch (shhApp) {
                        case 7:
                            newMap.put("" + jpbId + "07", alfa)
                            break
                        case 8:
                            newMap.put("" + jpbId + "08", alfa)
                            break
                        case 9:
                            newMap.put("" + jpbId + "09", alfa)
                            break
                        case 10:
                            newMap.put("" + jpbId + "10", alfa)
                            break
                        case 11:
                            newMap.put("" + jpbId + "11", alfa)
                            break
                        case 12:
                            newMap.put("" + jpbId + "12", alfa)
                            break
                        case 13:
                            newMap.put("" + jpbId + "13", alfa)
                            break
                        case 14:
                            newMap.put("" + jpbId + "14", alfa)
                            break
                        case 15:
                            newMap.put("" + jpbId + "15", alfa)
                            break
                        case 16:
                            newMap.put("" + jpbId + "16", alfa)
                            break
                        default:
                            break
                    }

                }

                if(cobaLagi){
                    def nopol = it?.reception?.historyCustomerVehicle?.fullNoPol
                    def jamAppointment = it?.reception?.t401TglJamRencana
                    int shhApp = Integer.parseInt(hh.format(jamAppointment))
                    def alfa = nopol + " ; " +  hhmm.format(jamAppointment)
                    switch (shhApp) {
                        case 7:
                            newMap.put("" + jpbId + "07", alfa)
                            break
                        case 8:
                            newMap.put("" + jpbId + "08", alfa)
                            break
                        case 9:
                            newMap.put("" + jpbId + "09", alfa)
                            break
                        case 10:
                            newMap.put("" + jpbId + "10", alfa)
                            break
                        case 11:
                            newMap.put("" + jpbId + "11", alfa)
                            break
                        case 12:
                            newMap.put("" + jpbId + "12", alfa)
                            break
                        case 13:
                            newMap.put("" + jpbId + "13", alfa)
                            break
                        case 14:
                            newMap.put("" + jpbId + "14", alfa)
                            break
                        case 15:
                            newMap.put("" + jpbId + "15", alfa)
                            break
                        case 16:
                            newMap.put("" + jpbId + "16", alfa)
                            break
                        default:
                            break
                    }
                }
            }

            rows << [
                    foreman: it?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,

                    jam7: (newMap.get("" + jpbId + "07") != null) ? newMap.get("" + jpbId + "07") : "&nbsp;",
                    jam8: (newMap.get("" + jpbId + "08") != null) ? newMap.get("" + jpbId + "08") : "&nbsp;",
                    jam9: (newMap.get("" + jpbId + "09") != null) ? newMap.get("" + jpbId + "09") : "&nbsp;",
                    jam10: (newMap.get("" + jpbId + "10") != null) ? newMap.get("" + jpbId + "10") : "&nbsp;",
                    jam11: (newMap.get("" + jpbId + "11") != null) ? newMap.get("" + jpbId + "11") : "&nbsp;",
                    jam12: (newMap.get("" + jpbId + "12") != null) ? newMap.get("" + jpbId + "12") : "&nbsp;",
                    jam13: (newMap.get("" + jpbId + "13") != null) ? newMap.get("" + jpbId + "13") : "&nbsp;",
                    jam14: (newMap.get("" + jpbId + "14") != null) ? newMap.get("" + jpbId + "14") : "&nbsp;",
                    jam15: (newMap.get("" + jpbId + "15") != null) ? newMap.get("" + jpbId + "15") : "&nbsp;",
                    jam16: (newMap.get("" + jpbId + "16") != null) ? newMap.get("" + jpbId + "16") : "&nbsp;",
            ]
        }

        if(results.size()<1){
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            def waktuSekarang = df.format(new Date())
            Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
            Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
            def asbs = ASB.createCriteria().list {
                eq("staDel","0")
                ge("t351TglJamApp",dateAwal)
                le("t351TglJamApp",dateAkhir)
            }

            asbs.each {
                DateFormat hh = new SimpleDateFormat("HH")
                DateFormat hhmm = new SimpleDateFormat("HH:mm")
                Map newMap = new HashMap()
                def jpbId = it?.id
                def nopol = it?.appointment?.historyCustomerVehicle?.fullNoPol
                def jamAppointment = it?.appointment?.t301TglJamRencana
                int shhApp = Integer.parseInt(hh.format(jamAppointment))
                def alfa = nopol + " ; " +  hhmm.format(jamAppointment)
                switch (shhApp) {
                    case 7:
                        newMap.put("" + jpbId + "07", alfa)
                        break
                    case 8:
                        newMap.put("" + jpbId + "08", alfa)
                        break
                    case 9:
                        newMap.put("" + jpbId + "09", alfa)
                        break
                    case 10:
                        newMap.put("" + jpbId + "10", alfa)
                        break
                    case 11:
                        newMap.put("" + jpbId + "11", alfa)
                        break
                    case 12:
                        newMap.put("" + jpbId + "12", alfa)
                        break
                    case 13:
                        newMap.put("" + jpbId + "13", alfa)
                        break
                    case 14:
                        newMap.put("" + jpbId + "14", alfa)
                        break
                    case 15:
                        newMap.put("" + jpbId + "15", alfa)
                        break
                    case 16:
                        newMap.put("" + jpbId + "16", alfa)
                        break
                    default:
                        break
                }
                rows << [
                        foreman: it?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,

                        jam7: (newMap.get("" + jpbId + "07") != null) ? newMap.get("" + jpbId + "07") : "&nbsp;",
                        jam8: (newMap.get("" + jpbId + "08") != null) ? newMap.get("" + jpbId + "08") : "&nbsp;",
                        jam9: (newMap.get("" + jpbId + "09") != null) ? newMap.get("" + jpbId + "09") : "&nbsp;",
                        jam10: (newMap.get("" + jpbId + "10") != null) ? newMap.get("" + jpbId + "10") : "&nbsp;",
                        jam11: (newMap.get("" + jpbId + "11") != null) ? newMap.get("" + jpbId + "11") : "&nbsp;",
                        jam12: (newMap.get("" + jpbId + "12") != null) ? newMap.get("" + jpbId + "12") : "&nbsp;",
                        jam13: (newMap.get("" + jpbId + "13") != null) ? newMap.get("" + jpbId + "13") : "&nbsp;",
                        jam14: (newMap.get("" + jpbId + "14") != null) ? newMap.get("" + jpbId + "14") : "&nbsp;",
                        jam15: (newMap.get("" + jpbId + "15") != null) ? newMap.get("" + jpbId + "15") : "&nbsp;",
                        jam16: (newMap.get("" + jpbId + "16") != null) ? newMap.get("" + jpbId + "16") : "&nbsp;",
                ]

            }
        }
        rows
    }
}