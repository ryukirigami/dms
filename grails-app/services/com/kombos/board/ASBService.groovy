package com.kombos.board

import com.kombos.administrasi.*
import com.kombos.hrd.CutiKaryawan
import com.kombos.maintable.JobApp
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import groovy.time.TimeCategory
import org.activiti.engine.impl.pvm.delegate.ActivityExecution
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class ASBService {

    def datatablesUtilService
    boolean gormReady = false
    boolean locked = false


    public static final int THIRTY = 30
    public static final int JAM7 = 420
    public static final int JAM730 = 450
    public static final int JAM715 = 435
    public static final int JAM745 = 465
    public static final int JAM8 = 480
    public static final int JAM830 = 510
    public static final int JAM815 = 495
    public static final int JAM845 = 525
    public static final int JAM9 = 540
    public static final int JAM930 = 570
    public static final int JAM915 = 555
    public static final int JAM945 = 585
    public static final int JAM10 = 600
    public static final int JAM1030 = 630
    public static final int JAM1015 = 615
    public static final int JAM1045 = 645
    public static final int JAM11 = 660
    public static final int JAM1130 = 690
    public static final int JAM1115 = 675
    public static final int JAM1145 = 705
    public static final int JAM12 = 720
    public static final int JAM1230 = 750
    public static final int JAM1215 = 735
    public static final int JAM1245 = 765
    public static final int JAM13 = 780
    public static final int JAM1330 = 810
    public static final int JAM1315 = 795
    public static final int JAM1345 = 825
    public static final int JAM14 = 840
    public static final int JAM1430 = 870
    public static final int JAM1415 = 855
    public static final int JAM1445 = 885
    public static final int JAM15 = 900
    public static final int JAM1530 = 930
    public static final int JAM1515 = 915
    public static final int JAM1545 = 945
    public static final int JAM16 = 960
    public static final int JAM1630 = 990
    public static final int JAM1615 = 975
    public static final int JAM1645 = 1005
    public static final int JAM17 = 1020

    def datatablesList(def params, CompanyDealer comp) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def manPowerStalls = loadAllManPowerStall(comp, params);
        def c = ASB.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer", comp)

            if (params."sCriteria_tglView") {
                ge("t351TglJamApp", params."sCriteria_tglView")
                lt("t351TglJamApp", params."sCriteria_tglView" + 1)
            }
            else{
                ge("t351TglJamApp", dateAwal)
                lt("t351TglJamApp", dateAkhir)
            }
        }
        manPowerStalls = showASB(results, manPowerStalls, params.aksi,"no")
        def totalFree = 0.0

        manPowerStalls.each {ASBLine line ->
            line.findFreeSlots().each {ASBFreeSlot freeSlot->
                totalFree = totalFree + freeSlot.length
            }
        }
        totalFree = totalFree / 120
        [sEcho: params.sEcho, iTotalRecords:  manPowerStalls.size(), iTotalDisplayRecords: manPowerStalls.size(), aaData: manPowerStalls, appTotal: results.size(), sisa : "" +totalFree + " jam"/*asbs.sisa*/]

    }

    def datatablesListPlusZero(def params, CompanyDealer comp) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        Date dateOnly = sdf.parse(sdf.format(new Date()))
        if(!params."sCriteria_tglView3Hari"){
            params."sCriteria_tglView3Hari" = new Date().clearTime()
        }
        params."sCriteria_tglView" = params."sCriteria_tglView3Hari"
        def manPowerStalls = loadAllManPowerStall(comp, params)

        def c = ASB.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer", comp)
            if (params."sCriteria_tglView3Hari") {
                ge("t351TglJamApp", params."sCriteria_tglView3Hari")
                lt("t351TglJamApp", params."sCriteria_tglView3Hari" + 1)
            } else {
                ge("t351TglJamApp", dateOnly)
                lt("t351TglJamApp", dateOnly + 1)
            }
        }
        manPowerStalls = showASB(results, manPowerStalls , params.aksi,"no")
        def totalFree = 0.0

        manPowerStalls.each {ASBLine line ->
            line.findFreeSlots().each {ASBFreeSlot freeSlot->
                totalFree = totalFree + freeSlot.length
            }
        }

        totalFree = totalFree / 120
        //println totalFree
        [sEcho: params.sEcho, iTotalRecords:  manPowerStalls.size(), iTotalDisplayRecords: manPowerStalls.size(), aaData: manPowerStalls, appTotal: results.size()/*asbs.apps.size()*/, sisa : "" +totalFree + " jam"/*asbs.sisa*/]

    }

    def datatablesListPlusOne(def params, CompanyDealer comp) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        Date dateOnly = sdf.parse(sdf.format(new Date()))
        if(!params."sCriteria_tglView3Hari"){
            params."sCriteria_tglView3Hari" = new Date().clearTime()
        }
        params."sCriteria_tglView" = params."sCriteria_tglView3Hari" + 1
        def manPowerStalls = loadAllManPowerStall(comp, params)

        def c = ASB.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",comp)
            if (params."sCriteria_tglView3Hari") {
                ge("t351TglJamApp", params."sCriteria_tglView3Hari" + 1)
                lt("t351TglJamApp", params."sCriteria_tglView3Hari" + 2)
            } else {
                ge("t351TglJamApp", dateOnly + 1)
                lt("t351TglJamApp", dateOnly + 2)
            }
        }

        manPowerStalls = showASB(results, manPowerStalls ,params.aksi,"no")
        def totalFree = 0.0

        manPowerStalls.each {ASBLine line ->
            line.findFreeSlots().each {ASBFreeSlot freeSlot->
                totalFree = totalFree + freeSlot.length
            }
        }
        totalFree = totalFree / 120
        //println totalFree
        [sEcho: params.sEcho, iTotalRecords:  manPowerStalls.size(), iTotalDisplayRecords: manPowerStalls.size(), aaData: manPowerStalls, appTotal: results.size()/*asbs.apps.size()*/, sisa : "" + totalFree + " jam"/*asbs.sisa*/]

    }

    def datatablesListPlusTwo(def params, CompanyDealer comp) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy")
        Date dateOnly = sdf.parse(sdf.format(new Date()))
        if(!params."sCriteria_tglView3Hari"){
            params."sCriteria_tglView3Hari" = new Date().clearTime()
        }
        params."sCriteria_tglView" = params."sCriteria_tglView3Hari" + 2
        def manPowerStalls = loadAllManPowerStall(comp, params)
        def c = ASB.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer", comp)
            if (params."sCriteria_tglView3Hari") {
                ge("t351TglJamApp", params."sCriteria_tglView3Hari" + 2)
                lt("t351TglJamApp", params."sCriteria_tglView3Hari" + 3)
            } else {
                ge("t351TglJamApp", dateOnly + 2)
                lt("t351TglJamApp", dateOnly + 3)
            }
        }
        manPowerStalls = showASB(results, manPowerStalls ,params.aksi,"no")

        def totalFree = 0.0

        manPowerStalls.each {ASBLine line ->
            line.findFreeSlots().each {ASBFreeSlot freeSlot->
                totalFree = totalFree + freeSlot.length
            }
        }
        totalFree = totalFree / 120
        //println totalFree
        [sEcho: params.sEcho, iTotalRecords:  manPowerStalls.size(), iTotalDisplayRecords: manPowerStalls.size(), aaData: manPowerStalls, appTotal: results.size()/*asbs.apps.size()*/, sisa : "" +totalFree+ " jam"/*asbs.sisa*/]

    }

    def dssAppointment(def params, CompanyDealer companyDealer){
        def res = [:]
        def operations = []
        def arrJob = []
        def app = Appointment.get(params.idApp as Long)
        def jobApps = JobApp.findAllByReceptionAndT302StaDel(app.reception,"0")
        Double totalRate = 0
        jobApps.each { JobApp job ->
            arrJob << job?.operation?.id
            if (job.operation && app.historyCustomerVehicle?.fullModelCode?.baseModel) {
                operations << job.operation
                FlatRate r = FlatRate.findByOperationAndBaseModel(job.operation,app.historyCustomerVehicle?.fullModelCode?.baseModel)
                if (r)
                    totalRate = totalRate + r.t113ProductionRate
            }
        }

        totalRate = totalRate * 60.0
        //println "Total Rate: "  + totalRate
        if(params.checkJanjiDatang){
            String rencana = params.tglRencana + " " + params.jamRencana
            def rencanaDate = new Date().parse('d-MM-yyyy HH:mm',rencana.toString())
            params."sCriteria_tglView" = rencanaDate
        } else if(params.checkJanjiPenyerahan){
            String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
            def penyerahanDate =  new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
            params."sCriteria_tglView" = penyerahanDate

        }
        def tanggalView = new Date()
        if (params."sCriteria_tglView") {
            tanggalView = params."sCriteria_tglView"
            tanggalView.clearTime()
        }
        def manPowerStalls = loadAllManPowerStall(companyDealer, params)

        def c = ASB.createCriteria()
        def results = c.list () {
            if (tanggalView) {
                ge("t351TglJamApp", tanggalView)
                lt("t351TglJamApp", tanggalView + 1)
            }
        }
        manPowerStalls = showASB(results, manPowerStalls ,params.aksi,"no")

        boolean doAgain = true;
        def jmlJobCovered = []
        def candidates = []
        Date canDate = params."sCriteria_tglView"
        for(ASBLine asbLine: manPowerStalls) {
            //println " Screening stall " + asbLine.stall
            def jmlJobTemp = 0
            def teknisi = NamaManPower.get(asbLine.teknisiId.toLong())
            def skillMap = ManPowerSertifikat.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companyDealer)
                eq("namaManPower",teknisi)
                le("t016TglAwalSertifikat",new Date().clearTime())
                ge("t016TglAkhirSertifikat",new Date().clearTime())
            }
            def sertifikat = SertifikatJobMap.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companyDealer)
                if(skillMap){
                    inList("sertifikat",skillMap.sertifikat)
                }else{
                    eq("id",-1000.toLong())
                }
                operation{
                    inList("id",arrJob)
                }
                projections {
                    groupProperty("operation")
                }
            }
            sertifikat.each {
                if(arrJob.indexOf(it.id)>=0){
                    jmlJobTemp++;
                }
            }
            jmlJobCovered << [
                    asbLine : asbLine,
                    jmlJob  : jmlJobTemp,
                    staBisa : jmlJobTemp>0 ? "ya" : "tidak"
            ]
            for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                if ((freeSlot.length >= totalRate || totalRate>540) && jmlJobTemp==arrJob.size()) {
                    candidates << freeSlot
                    doAgain = false
                }
            }

        }
        if(doAgain){
            if(jmlJobCovered.size()>0 && jmlJobCovered.staBisa.contains("ya")){
                for(int a = 0 ; a < jmlJobCovered.size() ; a++){
                    for(int b = a+1; b < jmlJobCovered.size();b++){
                        if(jmlJobCovered[a].jmlJob < jmlJobCovered[b].jmlJob){
                            def temp = jmlJobCovered[a]
                            jmlJobCovered[a] = jmlJobCovered[b]
                            jmlJobCovered[b] = temp
                        }
                    }
                }
            }

            jmlJobCovered.each {
                ASBLine asbLine = it.asbLine
                //println " Screening stall AGAIN " + asbLine.stall
                for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                    if (freeSlot.length >= totalRate || totalRate>540) {
                        candidates << freeSlot
                    }
                }
            }
        }
        def harusDatang = null
        def targetSelesai = null
        def candidate = null
        def sisaRate = null
        boolean found = candidates.size() > 0
        for (ASBFreeSlot freeSlot : candidates) {

            Date slotStart = freeSlot.startDateTime
            Date slotEnd = null
            def breakEnd = null
            use( TimeCategory ) {
                slotEnd = slotStart + (freeSlot.length + freeSlot.breakLength).minutes
                if(freeSlot.breakStart && freeSlot.breakLength)
                    breakEnd = freeSlot.breakStart + freeSlot.breakLength.minutes

            }
            //println " Screening candidate " + slotStart + " to " + slotEnd + " @" + freeSlot.line.stall + " breakEnd "+breakEnd
            if(slotEnd) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                def waktuSekarang = df.format(params."sCriteria_tglView")
                Date jam5 = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 17:00")
                Date jamTujuh = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 07:00")
                if (params.checkJanjiPenyerahan) {
                    String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
                    def penyerahanDate = new Date().parse('d-MM-yyyy HH:mm', penyerahan.toString())

                    if(breakEnd && freeSlot.breakStart < penyerahanDate && breakEnd > penyerahanDate)
                        penyerahanDate = breakEnd
                    if(penyerahanDate <= slotEnd){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = penyerahanDate - iTotalRate.minutes
                            if(tempDate < jamTujuh){
                                def tempSisaRate = jamTujuh - tempDate
                                tempDate = jamTujuh
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(breakEnd > tempDate && breakEnd <= penyerahanDate){
                                    harusDatang = penyerahanDate - (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    harusDatang = tempDate
                                }
                            } else {
                                harusDatang = tempDate
                            }
                        }
                        if(harusDatang && harusDatang >= slotStart) {
                            if (harusDatang.minutes > 45) {
                                harusDatang.minutes = 0
                                harusDatang.hours = harusDatang.hours + 1
                            } else if (harusDatang.minutes > 30)  {
                                harusDatang.minutes = 45
                            }else if (harusDatang.minutes > 15) {
                                harusDatang.minutes = 30
                            }else if (harusDatang.minutes > 0) {
                                harusDatang.minutes = 15
                            } else {
                                harusDatang.minutes = 0
                            }

                            targetSelesai = penyerahanDate
                            candidate = freeSlot
                            //println("Harus Datang: " + harusDatang)
                            //println("Target Selesai: " + targetSelesai)
                            break
                        }

                    }
                }
                else if (params.checkJanjiDatang) {
                    String rencana = params.tglRencana + " " + params.jamRencana
                    def rencanaDate = new Date().parse('dd-MM-yyyy HH:mm',rencana.toString())
                    if(breakEnd && freeSlot.breakStart < rencanaDate && breakEnd > rencanaDate)
                        rencanaDate = freeSlot.breakStart
                    if(rencanaDate >= slotStart){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = rencanaDate + iTotalRate.minutes
                            if(tempDate > jam5){
                                def tempSisaRate = tempDate - jam5
                                tempDate = jam5
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(freeSlot.breakStart >= rencanaDate && freeSlot.breakStart < tempDate){
                                    targetSelesai = rencanaDate + (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    targetSelesai = tempDate
                                }
                            } else {
                                targetSelesai = tempDate
                            }
                        }
                        if(targetSelesai && targetSelesai  <= slotEnd) {
                            if (targetSelesai.minutes > 45) {
                                targetSelesai.minutes = 0
                                targetSelesai.hours = targetSelesai.hours + 1
                            } else if (targetSelesai.minutes > 30)  {
                                targetSelesai.minutes = 45
                            }else if (targetSelesai.minutes > 15) {
                                targetSelesai.minutes = 30
                            }else if (targetSelesai.minutes > 0) {
                                targetSelesai.minutes = 15
                            } else {
                                targetSelesai.minutes = 0
                            }

                            harusDatang = rencanaDate
                            candidate = freeSlot
                            //println("Harus Datang: " + harusDatang)
                            //println("Target Selesai: " + targetSelesai)
                            break
                        }
                    }
                }
            }

        }
        if(candidate) {
            def arrStart = []
            def arrEnd = []
            def arrSlot = []
            def arrStallId = []
            def arrTanggal = []
            def arrTanggalDSS = []
            def arrTotalRate= []
            def slot = []
            def currentTime = harusDatang
            while(currentTime < targetSelesai){

                slot << "jam"+currentTime.hours+(currentTime.minutes==0?"":currentTime.minutes)
                use( TimeCategory ) {
                    currentTime = currentTime + 15.minutes
                }
            }
            arrStart << harusDatang
            arrEnd << targetSelesai
            arrSlot << slot
            arrStallId << candidate.line.stallId
            arrTanggal << canDate
            arrTanggalDSS << canDate.format("dd/MM/yyyy")
            arrTotalRate << totalRate

            if(sisaRate>0){
                int tambah = 0
                while (sisaRate>0){
                    tambah++
                    if (params.checkJanjiPenyerahan) {
                        params.jamPenyerahan = "17:00"
                        params.tglPenyerahan = (tanggalView-tambah).format("dd-MM-yyyy")
                    }else if(params.checkJanjiDatang){
                        params.jamRencana = "07:00"
                        params.tglRencana = (tanggalView+tambah).format("dd-MM-yyyy")
                    }
                    def lagi = findMore(params, companyDealer, sisaRate,"satu")
                    if(lagi."status"=="found"){
                        arrStart << lagi."start"
                        arrEnd << lagi."end"
                        arrSlot << lagi.slot
                        arrStallId << lagi."stallId"
                        arrTanggal << lagi."tanggal"
                        arrTanggalDSS << lagi."tanggal".format("dd/MM/yyyy")
                        arrTotalRate << lagi."totalRate"
                        sisaRate = lagi."sisaRate"
                    }
                }
            }

            res.start = arrStart
            res.end = arrEnd
            res.slot = arrSlot
            res.stallId = arrStallId
            res.tanggal = arrTanggal
            res.tanggalDSS = arrTanggalDSS
            res.totalRate = arrTotalRate
            res."jumData" = arrStart?.size()
            res."status" = "found"
        } else {
            res."status" = "notfound"
        }
        res
    }

    def dssAppointment3(def params, CompanyDealer companyDealer){
        def res = [:]
        def operations = []
        def app = Appointment.get(params.idApp as Long)
        def jobApps = JobApp.findAllByReceptionAndT302StaDel(app.reception,"0")
        def arrJob = []
        def jmlJobCovered=[]
        boolean doAgain = true;
        Double totalRate = 0.0
        jobApps.each { JobApp job ->
            arrJob << job.operation.id
            if (job.operation && app.historyCustomerVehicle?.fullModelCode?.baseModel) {
                operations << job.operation
                FlatRate r = FlatRate.findByOperationAndBaseModelAndStaDel(job.operation,app.historyCustomerVehicle?.fullModelCode?.baseModel,"0")
                if (r)
                    totalRate = totalRate + r.t113ProductionRate
            }
        }
        totalRate = totalRate * 60.0
        //println "Total Rate: "  + totalRate
        if(params.checkJanjiDatang3){
            String rencana = params.tglRencana3 + " " + params.jamRencana3
            def rencanaDate = new Date().parse('d-MM-yyyy HH:mm',rencana.toString())
            params."sCriteria_tglView" = rencanaDate
        } else if(params.checkJanjiPenyerahan3){
            String penyerahan = params.tglPenyerahan3 + " " + params.jamPenyerahan3
            def penyerahanDate =  new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
            params."sCriteria_tglView" = penyerahanDate

        }
        def tanggalView = new Date()
        if (params."sCriteria_tglView") {
            tanggalView = params."sCriteria_tglView"
            tanggalView.clearTime()
        }
        def manPowerStalls = loadAllManPowerStall(companyDealer, params)
        def c = ASB.createCriteria()
        def results = c.list () {
            if (tanggalView) {
                ge("t351TglJamApp", tanggalView)
                lt("t351TglJamApp", tanggalView + 1)
            }
        }
        manPowerStalls = showASB(results, manPowerStalls ,params.aksi,"no")
        def candidates = []
        Date canDate = params."sCriteria_tglView"

        for(ASBLine asbLine: manPowerStalls) {
            //println " Screening stall " + asbLine.stall
            def jmlJobTemp = 0
            def teknisi = NamaManPower.get(asbLine.teknisiId.toLong())
            def skillMap = ManPowerSertifikat.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companyDealer)
                eq("namaManPower",teknisi)
                le("t016TglAwalSertifikat",new Date().clearTime())
                ge("t016TglAkhirSertifikat",new Date().clearTime())
            }
            def sertifikat = SertifikatJobMap.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companyDealer)
                if(skillMap){
                    inList("sertifikat",skillMap.sertifikat)
                }else{
                    eq("id",-1000.toLong())
                }
                operation{
                    inList("id",arrJob)
                }
                projections {
                    groupProperty("operation")
                }
            }
            sertifikat.each {
                if(arrJob.indexOf(it.id)>=0){
                    jmlJobTemp++;
                }
            }
            jmlJobCovered << [
                    asbLine : asbLine,
                    jmlJob  : jmlJobTemp,
                    staBisa : jmlJobTemp>0 ? "ya" : "tidak"
            ]
            for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                if ((freeSlot.length >= totalRate || totalRate>540) && jmlJobTemp==arrJob.size()) {
                    candidates << freeSlot
                    doAgain = false
                }
            }

        }
        if(doAgain){
            if(jmlJobCovered.size()>0 && jmlJobCovered.staBisa.contains("ya")){
                for(int a = 0 ; a < jmlJobCovered.size() ; a++){
                    for(int b = a+1; b < jmlJobCovered.size();b++){
                        if(jmlJobCovered[a].jmlJob < jmlJobCovered[b].jmlJob){
                            def temp = jmlJobCovered[a]
                            jmlJobCovered[a] = jmlJobCovered[b]
                            jmlJobCovered[b] = temp
                        }
                    }
                }
            }

            jmlJobCovered.each {
                ASBLine asbLine = it.asbLine
                //println " Screening stall AGAIN " + asbLine.stall
                for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                    if (freeSlot.length >= totalRate || totalRate>540) {
                        candidates << freeSlot
                    }
                }
            }
        }

        def harusDatang = null
        def targetSelesai = null
        def candidate = null
        def sisaRate = null
        boolean found = candidates.size() > 0
        for (ASBFreeSlot freeSlot : candidates) {
            Date slotStart = freeSlot.startDateTime
            Date slotEnd = null
            def breakEnd = null
            use( TimeCategory ) {
                slotEnd = slotStart + (freeSlot.length + freeSlot.breakLength).minutes
                breakEnd = freeSlot.breakStart + freeSlot.breakLength.minutes
            }
            //println " Screening candidate " + slotStart + " to " + slotEnd + " @" + freeSlot.line.stall
            if(slotEnd) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                def waktuSekarang = df.format(params."sCriteria_tglView")
                Date jam5 = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 17:00")
                Date jamTujuh = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 07:00")
                if (params.checkJanjiPenyerahan3) {
                    String penyerahan = params.tglPenyerahan3 + " " + params.jamPenyerahan3
                    def penyerahanDate = new Date().parse('d-MM-yyyy HH:mm', penyerahan.toString())

                    if(breakEnd && freeSlot.breakStart < penyerahanDate && breakEnd > penyerahanDate)
                        penyerahanDate = breakEnd
                    if(penyerahanDate <= slotEnd){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = penyerahanDate - iTotalRate.minutes
                            if(tempDate < jamTujuh){
                                def tempSisaRate = jamTujuh - tempDate
                                tempDate = jamTujuh
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(breakEnd > tempDate && breakEnd <= penyerahanDate){
                                    harusDatang = penyerahanDate - (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    harusDatang = tempDate
                                }
                            } else {
                                harusDatang = tempDate
                            }
                        }
                        if(harusDatang && harusDatang >= slotStart) {
                            if (harusDatang.minutes > 45) {
                                harusDatang.minutes = 0
                                harusDatang.hours = harusDatang.hours + 1
                            } else if (harusDatang.minutes > 30)  {
                                harusDatang.minutes = 45
                            }else if (harusDatang.minutes > 15) {
                                harusDatang.minutes = 30
                            }else if (harusDatang.minutes > 0) {
                                harusDatang.minutes = 15
                            } else {
                                harusDatang.minutes = 0
                            }

                            targetSelesai = penyerahanDate
                            candidate = freeSlot
                            //println("Harus Datang: " + harusDatang)
                            //println("Target Selesai: " + targetSelesai)
                            break
                        }

                    }
                }
                else if (params.checkJanjiDatang3) {
                    String rencana = params.tglRencana3 + " " + params.jamRencana3
                    def rencanaDate = new Date().parse('dd-MM-yyyy HH:mm',rencana.toString())
                    if(breakEnd && freeSlot.breakStart < rencanaDate && breakEnd > rencanaDate)
                        rencanaDate = freeSlot.breakStart
                    if(rencanaDate >= slotStart){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = rencanaDate + iTotalRate.minutes
                            if(tempDate > jam5){
                                def tempSisaRate = tempDate - jam5
                                tempDate = jam5
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(freeSlot.breakStart >= rencanaDate && freeSlot.breakStart < tempDate){
                                    targetSelesai = rencanaDate + (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    targetSelesai = tempDate
                                }
                            } else {
                                targetSelesai = tempDate
                            }
                        }
                        if(targetSelesai && targetSelesai  <= slotEnd) {
                            if (targetSelesai.minutes > 45) {
                                targetSelesai.minutes = 0
                                targetSelesai.hours = targetSelesai.hours + 1
                            } else if (targetSelesai.minutes > 30)  {
                                targetSelesai.minutes = 45
                            }else if (targetSelesai.minutes > 15) {
                                targetSelesai.minutes = 30
                            }else if (targetSelesai.minutes > 0) {
                                targetSelesai.minutes = 15
                            } else {
                                targetSelesai.minutes = 0
                            }

                            harusDatang = rencanaDate
                            candidate = freeSlot
                            //println("Harus Datang: " + harusDatang)
                            //println("Target Selesai: " + targetSelesai)
                            break
                        }
                    }
                }
            }

        }
        if(candidate) {
            def arrStart = []
            def arrEnd = []
            def arrSlot = []
            def arrStallId = []
            def arrTanggal = []
            def arrTanggalDSS = []
            def arrTotalRate= []
            def slot = []
            def currentTime = harusDatang
            while(currentTime < targetSelesai){

                slot << "jam"+currentTime.hours+(currentTime.minutes==0?"":currentTime.minutes)
                use( TimeCategory ) {
                    currentTime = currentTime + 15.minutes
                }
            }
            arrStart << harusDatang
            arrEnd << targetSelesai
            arrSlot << slot
            arrStallId << candidate.line.stallId
            arrTanggal << canDate
            arrTanggalDSS << canDate.format("dd/MM/yyyy")
            arrTotalRate << totalRate

            if(sisaRate>0){
                int tambah = 0
                while (sisaRate>0){
                    tambah++
                    if (params.checkJanjiPenyerahan3) {
                        params.jamPenyerahan3 = "17:00"
                        params.tglPenyerahan3 = (tanggalView-tambah).format("dd-MM-yyyy")
                    }else if(params.checkJanjiDatang3){
                        params.jamRencana3 = "07:00"
                        params.tglRencana3 = (tanggalView+tambah).format("dd-MM-yyyy")
                    }
                    //println "SISA RATE ${tambah} ---- "+sisaRate
                    def lagi = findMore(params, companyDealer, sisaRate,"tiga")
                    //println lagi
                    if(lagi."status"=="found"){
                        arrStart << lagi."start"
                        arrEnd << lagi."end"
                        arrSlot << lagi.slot
                        arrStallId << lagi."stallId"
                        arrTanggal << lagi."tanggal"
                        arrTanggalDSS << lagi."tanggal".format("dd/MM/yyyy")
                        arrTotalRate << lagi."totalRate"
                        sisaRate = lagi."sisaRate"
                    }
                }
            }

            res.start = arrStart
            res.end = arrEnd
            res.slot = arrSlot
            res.stallId = arrStallId
            res.tanggal = arrTanggal
            res.tanggalDSS = arrTanggalDSS
            res.totalRate = arrTotalRate
            res."jumData" = arrStart?.size()
            res."status" = "found"
        } else {
            res."status" = "notfound"
        }
        res
    }

    def findMore(def params,CompanyDealer companyDealer,Double aSisaRate, String dari){
        //println params
        def res = [:]
        Double totalRate = aSisaRate
        String cekDatang = "checkJanjiDatang",cekSerah = "checkJanjiPenyerahan",tanggalRencana = "tglRencana", jamRencana = "jamRencana",
               tanggalSerah = "tglPenyerahan", jamPenyerahan = "jamPenyerahan"
        if(dari.equalsIgnoreCase("tiga")){
            cekDatang+="3";cekSerah +="3";tanggalRencana +="3"; jamRencana +="3"; tanggalSerah +="3"; jamPenyerahan +="3"
        }
        if(params."${cekDatang}"){
            String rencana = params."${tanggalRencana}" + " " + params."${jamRencana}"
            def rencanaDate = new Date().parse('d-MM-yyyy HH:mm',rencana.toString())
            params."sCriteria_tglView" = rencanaDate
        } else if(params."${cekSerah}"){
            String penyerahan = params."${tanggalSerah}" + " " + params."${jamPenyerahan}"
            def penyerahanDate =  new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
            params."sCriteria_tglView" = penyerahanDate

        }
        //println("Waktu " + params."sCriteria_tglView")
        def tanggalView = new Date()
        if (params."sCriteria_tglView") {
            tanggalView = params."sCriteria_tglView"
            tanggalView.clearTime()
        }
        def manPowerStalls = loadAllManPowerStall(companyDealer, params)
        def c = ASB.createCriteria()
        def results = c.list () {
            if (tanggalView) {
                ge("t351TglJamApp", tanggalView)
                lt("t351TglJamApp", tanggalView + 3)
            }
        }
        manPowerStalls = showASB(results, manPowerStalls ,params.aksi,"carry")

        def candidates = []
        def canDate = params."sCriteria_tglView"
        for(ASBLine asbLine: manPowerStalls) {
            for (ASBFreeSlot freeSlot : asbLine.findFreeSlots()) {
                if (freeSlot.length >= totalRate || totalRate>540) {
                    candidates << freeSlot
                }
            }

        }

        def harusDatang = null
        def targetSelesai = null
        def candidate = null
        def sisaRate = null
        boolean found = candidates.size() > 0
        for (ASBFreeSlot freeSlot : candidates) {
            Date slotStart = freeSlot.startDateTime
            Date slotEnd = null
            def breakEnd = null
            use( TimeCategory ) {
                slotEnd = slotStart + (freeSlot.length + freeSlot.breakLength).minutes
                breakEnd = freeSlot.breakStart + freeSlot.breakLength.minutes
            }

            if(slotEnd) {
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                def waktuSekarang = df.format(params."sCriteria_tglView")
                Date jam5 = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 17:00")
                Date jamTujuh = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 07:00")
                if (params."${cekSerah}") {
                    String penyerahan = params."${tanggalSerah}" + " " + params."${jamPenyerahan}"
                    def penyerahanDate = new Date().parse('d-MM-yyyy HH:mm', penyerahan.toString())

                    if(breakEnd && freeSlot.breakStart < penyerahanDate && breakEnd > penyerahanDate)
                        penyerahanDate = breakEnd
                    if(penyerahanDate <= slotEnd){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = penyerahanDate - iTotalRate.minutes
                            if(tempDate < jamTujuh){
                                def tempSisaRate = jamTujuh - tempDate
                                tempDate = jamTujuh
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(breakEnd > tempDate && breakEnd <= penyerahanDate){
                                    harusDatang = penyerahanDate - (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    harusDatang = tempDate
                                }
                            } else {
                                harusDatang = tempDate
                            }
                        }
                        if(harusDatang && harusDatang >= slotStart) {
                            if (harusDatang.minutes > 45) {
                                harusDatang.minutes = 0
                                harusDatang.hours = harusDatang.hours + 1
                            } else if (harusDatang.minutes > 30)  {
                                harusDatang.minutes = 45
                            }else if (harusDatang.minutes > 15) {
                                harusDatang.minutes = 30
                            }else if (harusDatang.minutes > 0) {
                                harusDatang.minutes = 15
                            } else {
                                harusDatang.minutes = 0
                            }

                            targetSelesai = penyerahanDate
                            candidate = freeSlot
                            //println("Harus Datang: " + harusDatang)
                            //println("Target Selesai: " + targetSelesai)
                            break
                        }

                    }
                }
                else if (params."${cekDatang}") {
                    String rencana = params."${tanggalRencana}" + " " + params."${jamRencana}"
                    def rencanaDate = new Date().parse('dd-MM-yyyy HH:mm',rencana.toString())
                    if(breakEnd && freeSlot.breakStart < rencanaDate && breakEnd > rencanaDate)
                        rencanaDate = freeSlot.breakStart

                    if(rencanaDate < slotStart){
                        rencanaDate = slotStart
                    }
                    if(rencanaDate >= slotStart){
                        int iTotalRate = Math.round(totalRate) as int
                        use( TimeCategory ) {
                            def tempDate = rencanaDate + iTotalRate.minutes
                            if(tempDate > jam5){
                                def tempSisaRate = tempDate - jam5
                                tempDate = jam5
                                if(tempSisaRate.hours>0){
                                    sisaRate = (tempSisaRate.hours * 60) + tempSisaRate.minutes
                                }else {
                                    sisaRate = tempSisaRate.minutes
                                }
                            }
                            if(freeSlot.breakLength > 0){
                                if(freeSlot.breakStart >= rencanaDate && freeSlot.breakStart < tempDate){
                                    targetSelesai = rencanaDate + (iTotalRate + freeSlot.breakLength).minutes
                                } else {
                                    targetSelesai = tempDate
                                }
                            } else {
                                targetSelesai = tempDate
                            }
                        }
                        if(targetSelesai && targetSelesai  <= slotEnd) {
                            if (targetSelesai.minutes > 45) {
                                targetSelesai.minutes = 0
                                targetSelesai.hours = targetSelesai.hours + 1
                            } else if (targetSelesai.minutes > 30)  {
                                targetSelesai.minutes = 45
                            }else if (targetSelesai.minutes > 15) {
                                targetSelesai.minutes = 30
                            }else if (targetSelesai.minutes > 0) {
                                targetSelesai.minutes = 15
                            } else {
                                targetSelesai.minutes = 0
                            }

                            harusDatang = rencanaDate
                            candidate = freeSlot
                            break
                        }
                    }
                }
            }

        }
        //println candidate
        if(candidate) {
            def slot = []
            def currentTime = harusDatang
            while(currentTime < targetSelesai){

                slot << "jam"+currentTime.hours+(currentTime.minutes==0?"":currentTime.minutes)
                use( TimeCategory ) {
                    currentTime = currentTime + 15.minutes
                }
            }
            res."start" = harusDatang
            res."end" = targetSelesai
            res."tanggalCari" = harusDatang?.format("")
            res.slot = slot
            res."stallId" = candidate.line.stallId
            res."tanggal" = canDate
            res."totalRate" = totalRate
            res."sisaRate" = sisaRate
            res."status" = "found"
        } else {
            res."status" = "notfound"
        }
        res
    }

    def showASB(List<ASB> results, def manPowerStalls, String aksi, String isCarry) {
        def alfabet = "V"
        def alfabet1 = "V1"
        DateFormat hh = new SimpleDateFormat("HH")
        DateFormat mm = new SimpleDateFormat("mm")
        results.each { ASB asb ->
            //println("ASB: " + asb?.t351TglJamApp)
            def start = true
            manPowerStalls.each { ASBLine asbLine ->
                if (asb.stall.m022NamaStall.equals(asbLine.stall)) {
                    //println("stall: " + asb.stall.m022NamaStall)
                    Appointment app = asb.appointment
                    if(aksi.equalsIgnoreCase("view")){
                        def jobApps = JobApp.findAllByReceptionAndT302StaDel(app.reception,"0")
                        jobApps.each { JobApp job ->
                            if (job.operation) {
                                alfabet = 'V' + job.operation.kategoriJob.m055KategoriJob
                                alfabet1 = 'V1' + job.operation.kategoriJob.m055KategoriJob
                            }
                        }
                    }

                    def jamJanjiMulai = asb?.t351TglJamApp
                    def jamJanjiSelesai = asb?.t351TglJamFinished
                    //println("app mulai: " + jamJanjiMulai)
                    //println("app jamJanjiSelesai: " + jamJanjiSelesai)
                    int shhMulai = Integer.parseInt(hh.format(jamJanjiMulai))
                    int smmMulai = Integer.parseInt(mm.format(jamJanjiMulai))
                    int shhSelesai = Integer.parseInt(hh.format(jamJanjiSelesai))
                    int smmSelesai = Integer.parseInt(mm.format(jamJanjiSelesai))


                    int mulai = shhMulai * 60 + smmMulai
                    int selesai = shhSelesai * 60 + smmSelesai
                    //println "Show mulai: " + mulai + ", slese: " + selesai

                    if (JAM7 >= mulai && JAM7 < selesai) {
                        if (start) {
                            asbLine.jam7 = alfabet1
                            asbLine.noPolJam7 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam7 = alfabet
                        }
                    }
                    if (JAM715 >= mulai && JAM715 < selesai) {
                        if (start) {
                            asbLine.jam715 = alfabet1
                            asbLine.noPolJam715 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam715 = alfabet
                        }
                    }
                    if (JAM730 >= mulai && JAM730 < selesai) {
                        if (start) {
                            asbLine.jam730 = alfabet1
                            asbLine.noPolJam730 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam730 = alfabet
                        }
                    }
                    if (JAM745 >= mulai && JAM745 < selesai) {
                        if (start) {
                            asbLine.jam745 = alfabet1
                            asbLine.noPolJam745 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam745 = alfabet
                        }
                    }
                    if (JAM8 >= mulai && JAM8 < selesai) {
                        if (start) {
                            asbLine.jam8 = alfabet1
                            asbLine.noPolJam8 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam8 = alfabet
                        }
                    }
                    if (JAM815 >= mulai && JAM815 < selesai) {
                        if (start) {
                            asbLine.jam815 = alfabet1
                            asbLine.noPolJam815 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam815 = alfabet
                        }
                    }
                    if (JAM830 >= mulai && JAM830 < selesai) {
                        if (start) {
                            asbLine.jam830 = alfabet1
                            asbLine.noPolJam830 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam830 = alfabet
                        }
                    }
                    if (JAM845 >= mulai && JAM845 < selesai) {
                        if (start) {
                            asbLine.jam845 = alfabet1
                            asbLine.noPolJam845 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam845 = alfabet
                        }
                    }
                    if (JAM9 >= mulai && JAM9 < selesai) {
                        if (start) {
                            asbLine.jam9 = alfabet1
                            asbLine.noPolJam9 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam9 = alfabet
                        }
                    }
                    if (JAM915 >= mulai && JAM915 < selesai) {
                        if (start) {
                            asbLine.jam915 = alfabet1
                            asbLine.noPolJam915 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam915 = alfabet
                        }
                    }
                    if (JAM930 >= mulai && JAM930 < selesai) {
                        if (start) {
                            asbLine.jam930 = alfabet1
                            asbLine.noPolJam930 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam930 = alfabet
                        }
                    }
                    if (JAM945 >= mulai && JAM945 < selesai) {
                        if (start) {
                            asbLine.jam945 = alfabet1
                            asbLine.noPolJam945 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam945 = alfabet
                        }
                    }
                    if (JAM10 >= mulai && JAM10 < selesai) {
                        if (start) {
                            asbLine.jam10 = alfabet1
                            asbLine.noPolJam10 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam10 = alfabet
                        }
                    }
                    if (JAM1015 >= mulai && JAM1015 < selesai) {
                        if (start) {
                            asbLine.jam1015 = alfabet1
                            asbLine.noPolJam1015 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1015 = alfabet
                        }
                    }
                    if (JAM1030 >= mulai && JAM1030 < selesai) {
                        if (start) {
                            asbLine.jam1030 = alfabet1
                            asbLine.noPolJam1030 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1030 = alfabet
                        }
                    }
                    if (JAM1045 >= mulai && JAM1045 < selesai) {
                        if (start) {
                            asbLine.jam1045 = alfabet1
                            asbLine.noPolJam1045 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1045 = alfabet
                        }
                    }
                    if (JAM11 >= mulai && JAM11 < selesai) {
                        if (start) {
                            asbLine.jam11 = alfabet1
                            asbLine.noPolJam11 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam11 = alfabet
                        }
                    }
                    if (JAM1115 >= mulai && JAM1115 < selesai) {
                        if (start) {
                            asbLine.jam1115 = alfabet1
                            asbLine.noPolJam1115 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1115 = alfabet
                        }
                    }
                    if (JAM1130 >= mulai && JAM1130 < selesai) {
                        if (start) {
                            asbLine.jam1130 = alfabet1
                            asbLine.noPolJam1130 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1130 = alfabet
                        }
                    }
                    if (JAM1145 >= mulai && JAM1145 < selesai) {
                        if (start) {
                            asbLine.jam1145 = alfabet1
                            asbLine.noPolJam1145 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1145 = alfabet
                        }
                    }
                    if (JAM12 >= mulai && JAM12 < selesai) {
                        if (start) {
                            asbLine.jam12 = alfabet1
                            asbLine.noPolJam12 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam12 = alfabet
                        }
                    }
                    if (JAM1215 >= mulai && JAM1215 < selesai) {
                        if (start) {
                            asbLine.jam1215 = alfabet1
                            asbLine.noPolJam1215 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1215 = alfabet
                        }
                    }
                    if (JAM1230 >= mulai && JAM1230 < selesai) {
                        if (start) {
                            asbLine.jam1230 = alfabet1
                            asbLine.noPolJam1230 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1230 = alfabet
                        }
                    }
                    if (JAM1245 >= mulai && JAM1245 < selesai) {
                        if (start) {
                            asbLine.jam1245 = alfabet1
                            asbLine.noPolJam1245 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1245 = alfabet
                        }
                    }
                    if (JAM13 >= mulai && JAM13 < selesai) {
                        if (start) {
                            asbLine.jam13 = alfabet1
                            asbLine.noPolJam13 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam13 = alfabet
                        }
                    }
                    if (JAM1315 >= mulai && JAM1315 < selesai) {
                        if (start) {
                            asbLine.jam1315 = alfabet1
                            asbLine.noPolJam1315 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1315 = alfabet
                        }
                    }
                    if (JAM1330 >= mulai && JAM1330 < selesai) {
                        if (start) {
                            asbLine.jam1330 = alfabet1
                            asbLine.noPolJam1330 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1330 = alfabet
                        }
                    }
                    if (JAM1345 >= mulai && JAM1345 < selesai) {
                        if (start) {
                            asbLine.jam1345 = alfabet1
                            asbLine.noPolJam1345 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1345 = alfabet
                        }
                    }
                    if (JAM14 >= mulai && JAM14 < selesai) {
                        if (start) {
                            asbLine.jam14 = alfabet1
                            asbLine.noPolJam14 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam14 = alfabet
                        }
                    }
                    if (JAM1415 >= mulai && JAM1415 < selesai) {
                        if (start) {
                            asbLine.jam1415 = alfabet1
                            asbLine.noPolJam1415 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1415 = alfabet
                        }
                    }
                    if (JAM1430 >= mulai && JAM1430 < selesai) {
                        if (start) {
                            asbLine.jam1430 = alfabet1
                            asbLine.noPolJam1430 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1430 = alfabet
                        }
                    }
                    if (JAM1445 >= mulai && JAM1445 < selesai) {
                        if (start) {
                            asbLine.jam1445 = alfabet1
                            asbLine.noPolJam1445 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1445 = alfabet
                        }
                    }
                    if (JAM15 >= mulai && JAM15 < selesai) {
                        if (start) {
                            asbLine.jam15 = alfabet1
                            asbLine.noPolJam15 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam15 = alfabet
                        }
                    }
                    if (JAM1515 >= mulai && JAM1515 < selesai) {
                        if (start) {
                            asbLine.jam1515 = alfabet1
                            asbLine.noPolJam1515 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1515 = alfabet
                        }
                    }
                    if (JAM1530 >= mulai && JAM1530 < selesai) {
                        if (start) {
                            asbLine.jam1530 = alfabet1
                            asbLine.noPolJam1530 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1530 = alfabet
                        }
                    }
                    if (JAM1545 >= mulai && JAM1545 < selesai) {
                        if (start) {
                            asbLine.jam1545 = alfabet1
                            asbLine.noPolJam1545 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1545 = alfabet
                        }
                    }
                    if (JAM16 >= mulai && JAM16 < selesai) {
                        if (start) {
                            asbLine.jam16 = alfabet1
                            asbLine.noPolJam16 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam16 = alfabet
                        }
                    }
                    if (JAM1615 >= mulai && JAM1615 < selesai) {
                        if (start) {
                            asbLine.jam1615 = alfabet1
                            asbLine.noPolJam1615 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1615 = alfabet
                        }
                    }
                    if (JAM1630 >= mulai && JAM1630 < selesai) {
                        if (start) {
                            asbLine.jam1630 = alfabet1
                            asbLine.noPolJam1630 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1630 = alfabet
                        }
                    }
                    if (JAM1645 >= mulai && JAM1645 < selesai) {
                        if (start) {
                            asbLine.jam1645 = alfabet1
                            asbLine.noPolJam1645 = app.historyCustomerVehicle.fullNoPol.replaceAll(' ','&nbsp;')
                            start = false
                        } else {
                            asbLine.jam1645 = alfabet
                        }
                    }


                }


            }
        }

        manPowerStalls
    }

    def loadAllManPowerStall(CompanyDealer company, def params){
        def res = []
        def c = ManPowerStall.createCriteria()
        def tanggalView = new Date().clearTime()
        if (params."sCriteria_tglView") {
            tanggalView = params."sCriteria_tglView"
            tanggalView.clearTime()
        }

        def listNoPresent = CutiKaryawan.createCriteria().list {
            eq("staDel","0")
            ge("tanggalAkhirCuti",tanggalView)
            eq("companyDealer",company)
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            karyawan{
                projections {
                    groupProperty("nomorPokokKaryawan", "nomorPokokKaryawan")
                }
            }
        }

        def results = c.list () {
            le("t019Tanggal", tanggalView)
            eq("companyDealer", company)
            eq("staDel","0")
            stall{
                jenisStall{
                    not{
                        ilike("m021NamaJenisStall","%BP%")
                    }
                }
            }
            namaManPower{
                if(listNoPresent.size()>0){
                    not{
                        inList("t015IdManPower",listNoPresent?.nomorPokokKaryawan)
                    }
                }

                groupManPower{
                    eq("staDel","0");
                    namaManPowerForeman{
                        order("t015NamaBoard");
                    }
                    order("t021Group");
                }
            }
        }

        results.each { ManPowerStall mps ->
            ASBLine asbLine = new ASBLine()
            asbLine.foremanId = mps?.namaManPower?.groupManPower?.namaManPowerForeman?.id
            asbLine.foreman= mps?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard
            asbLine.groupId= mps.namaManPower?.groupManPower?.id
            //asbLine.asbId = mps.id
            asbLine.teknisiId= mps.namaManPower?.id
            asbLine.group= mps.namaManPower?.groupManPower?.t021Group
            asbLine.teknisi= mps.namaManPower?.t015NamaBoard
            asbLine.stall= mps.stall?.m022NamaStall
            asbLine.stallId= mps.id
            asbLine.date = tanggalView
            res << asbLine
        }
        res
    }

    def toJPB(ActivityExecution execution) throws Exception {
        if(gormReady && !locked) {
            locked = true
            def variables = execution.getVariables()
            Date date = new Date()
            date.clearTime()
            def c = ASB.createCriteria()
            def results = c.list () {
                eq("staDel","0")
                ge("t351TglJamApp", date)
                lt("t351TglJamApp", date + 1)
            }
            //println "MOVING ASB TO JPB"
            results.each { ASB asb->
                Appointment app = asb.appointment
                Reception reception = app.reception

                if (reception) {
                    reception?.companyDealer = app?.companyDealer
                    reception?.t401KmSaatIni = app?.t301KmSaatIni
                    reception?.t401TglJamRencana = app?.t301TglJamRencana
                    reception?.historyCustomer = app?.historyCustomer
                    reception?.historyCustomerVehicle = app?.historyCustomerVehicle
                    reception?.t401TglJamJanjiPenyerahan = app?.t301TglJamPenyerahanSchedule

                    reception?.save(flush: true)
                    reception.errors.each {
                        //println it
                    }
                }

                JPB jpb = new JPB(
                        companyDealer: asb.companyDealer,
                        reception: reception,
                        t451TglJamPlan: asb?.t351TglJamApp,
                        t451TglJamPlanFinished: asb?.t351TglJamFinished,
                        namaManPower: asb.namaManPower,
                        stall: asb.stall,
                        flatRate: asb.flatRate,
                        t451StaTambahWaktu: '1',
                        t451staOkCancelReSchedule: '1',
                        t451AlasanReSchedule: '1',
                        staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(), dateCreated: datatablesUtilService.syncTime(), lastUpdated: datatablesUtilService.syncTime()
                )

                jpb.addToAppointments(app)
                jpb.save(flush: true)
                jpb.errors.each {
                    //println it
                }
                app.jpb = jpb
                app?.save(flush: true)
                app.errors.each {
                    //println it
                }
            }
            locked = false
        }
    }

    def doMoveManual(CompanyDealer cd) {
        Date date = new Date()
        date.clearTime()
        def c = ASB.createCriteria()
        def results = c.list () {
            eq("companyDealer",cd);
            eq("staDel","0");
            ge("t351TglJamApp", date);
            lt("t351TglJamApp", date + 1);
        }
        results.each { ASB asb->
            Appointment app = asb?.appointment
            Reception reception = app?.reception
            if (reception) {
                reception?.companyDealer = app?.companyDealer
                reception?.t401KmSaatIni = app?.t301KmSaatIni
                reception?.t401TglJamRencana = app?.t301TglJamRencana
                reception?.historyCustomer = app?.historyCustomer
                reception?.historyCustomerVehicle = app?.historyCustomerVehicle
                reception?.t401TglJamJanjiPenyerahan = app?.t301TglJamPenyerahanSchedule
                reception?.dateCreated = datatablesUtilService?.syncTime()
                reception?.lastUpdated = datatablesUtilService?.syncTime()
                reception?.save(flush: true)
                reception.errors.each {
                    //println it
                }

                def asbHasMove = JPB.findByReceptionAndStaDel(reception,"0")

                if(!asbHasMove){
                    JPB jpb = new JPB(
                            companyDealer: asb.companyDealer,
                            reception: reception,
                            t451TglJamPlan: asb?.t351TglJamApp,
                            t451TglJamPlanFinished: asb?.t351TglJamFinished,
                            namaManPower: asb.namaManPower,
                            stall: asb.stall,
                            flatRate: asb.flatRate,
                            t451StaTambahWaktu: '1',
                            t451staOkCancelReSchedule: '1',
                            t451AlasanReSchedule: '1',
                            staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            dateCreated: datatablesUtilService?.syncTime(), lastUpdated: datatablesUtilService?.syncTime()
                    )
                    jpb.addToAppointments(app)
                    jpb.save(flush: true)
                    jpb.errors.each {
                        //println it
                    }
                    app.jpb = jpb
                    app?.save(flush: true)
                    app.errors.each {
                        //println it
                    }
                }
            }
        }
        return "OK"
    }
}