package com.kombos.home

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.StockOpnameLog
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.hrd.AbsensiKaryawanDetail
import com.kombos.maintable.*
import com.kombos.parts.NotaPesananBarang
import com.kombos.parts.StatusApproval
import com.kombos.reception.Appointment
import com.kombos.reception.CustomerIn
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class DashboardService {
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    def  waktuSekarang = df.format(new Date())
    Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
    Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")

    def approvalAndAlertDatatablesList(def params) {

        def session = RequestContextHolder.currentRequestAttributes().getSession()
		def propertiesToRender = params.sColumns.split(",")
		def x = 0
		
		def username = null
			//= SecurityUtils.subject.principal.toString()
		def apc = ApprovalT770.createCriteria()
		def approvalResults = apc.list () {
            if (!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("companyDealer",session.userCompanyDealer)
            }
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
				kegiatanApproval{
					groupProperty("m770KegiatanApproval", "kegiatanApproval")
				}
                eq("t770Status",StatusApproval.WAIT_FOR_APPROVAL)
				countDistinct("id","jumlahRequest")
			}
			if (username){
				kegiatanApproval{
					namaApprovals{
						userProfile{
							eq('username', username)
						}
					}
				}
			}
		}



		def rows = []
		
		approvalResults.each { appr ->
            def tampil = false
            String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
            User user = User.findByUsername(usernameShiro)
            def roles = user.roles
            roles.each { rol ->
               if(NamaApproval.findByApprovelRoleAndKegiatanApproval(rol,KegiatanApproval.findByM770KegiatanApproval(appr.kegiatanApproval))){
                   tampil=true
               }else if(rol.authority == 'ROLE_ADMIN'){
                   tampil=true
               }else if(rol.authority == 'ROLE_APPROVAL'){
                   tampil=true
               }else if(rol.authority == 'KEPALA_BENGKEL'){
                   tampil=true
               }
            }
            if(tampil==true){
                rows << [
                        tipe: "Approval",
                        jenis: appr.kegiatanApproval + " ("+appr.jumlahRequest+")",
                        kegiatan : appr.kegiatanApproval
                ]
            }

		}

        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        def autorities = [:]

        roles.each {
            if(it.authority == "PETUGAS_NPB_HO")
                autorities.put("petugasNPBHO", true)
            else if(it.authority == "PETUGAS_DO")
                autorities.put("petugasDO",true)
            else if(it.authority == "PETUGAS_NPB_JKT")
                autorities.put("petugasNPBJKT",true)
            else if(it.authority == "PARTS_HA")
                autorities.put("partsHA", true)
            else if(it.authority == "KEPALA_BENGKEL")
                autorities.put("kepalaBengkel", true)
            else if(it.authority == "PARTSMAN")
                autorities.put("partsman", true)
            else if(it.authority == "ADMINISTRATION_HEAD")
                autorities.put("adminHead", true)
            else if(it.authority == "TEKNISI")
                autorities.put("teknisi", true)
            else if(it.authority == "SERVICE_ADVISOR_GENERAL_REPAIR")
                autorities.put("SAGR", true)
            else if(it.authority == "ROLE_ADMIN")
                autorities.put("ROLE_ADMIN", true)

        }

        if(autorities.size()!=0){
            def appNPB = NotaPesananBarang.createCriteria()
            def appNPBResult = appNPB.list (){
                if(autorities.get("kepalaBengkel"))
                {
                    eq("kabengApprovalSta","0")
                    eq("cabangPengirim", user.companyDealer)
                }

                else if(autorities.get("petugasNPBHO")){
                    eq("cabangPengirim", user.companyDealer)
                    eq("kabengApprovalSta", "1")
                    eq("partsHOApproveSta", "0")
                }

                else if(autorities.get("petugasNPBJKT")){
                    eq("cabangTujuan", user.companyDealer)
                    eq("ptgsNPBJKTSta", "0")
                    isNull("cabangDiteruskan")
                    eq("kabengApprovalSta", "1")
                    eq("partsHOApproveSta", "1")
                }

                else{
                    eq("cabangDiteruskan",CompanyDealer.findById(user.companyDealer.id))
                    isNull("cabangDiteruskanSta")
                }
            }

           if(appNPBResult)
           {
               rows << [
                       tipe : "Approval NPB",
                       jenis : "NPB Approval (" + appNPBResult.size() + ")"
               ]
           }
        }


		def alc = Alert.createCriteria()
		def alertResults = alc.list () {
            eq("companyDealer",session?.userCompanyDealer)
            ge("dateCreated", dateAwal)
            lt("dateCreated", dateAkhir)
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
                jenisAlert{
                    groupProperty("m901NamaAlert", "jenisAlert")
                }
				countDistinct('id', 'jumlahAlert')
			}
		}
		
		alertResults.each {
			rows << [
				tipe: "Alert",
				jenis: it?.jenisAlert + " ("+ it?.jumlahAlert +")"
				]
		}
				
		[sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]
				
	}
	
	def todaySummary(def params){
        try {
            waktuSekarang = df.format(datatablesUtilService.syncTime())
        }catch(Exception e){
            waktuSekarang = df.format(new Date())
        }
        dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  params.userCompanyDealer
		def result = [:]
		result.status = 'ok'
		def ittc = NamaManPower.createCriteria()
		def ittcResults = ittc.list {
            eq("staDel","0")
			eq('companyDealer', companyDealer)
            manPowerDetail{
                or{
                    ilike("m015LevelManPower","%Apprentice%")
                    ilike("m015LevelManPower","%Technician%")
                }
            }
        }
		
		def itcResults = AbsensiKaryawanDetail.createCriteria().list {
			ge('tanggal', new Date().clearTime())
			lt('tanggal', new Date().clearTime()+1)
            terminal{
                eq("companyDealer",companyDealer)
                eq("staDel","0")
            }
            absensiKaryawan{
                resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                projections {
                    groupProperty("karyawan","karyawan")
                }
            }
		}

//		result.infoTeknisi = (itcResults.size() as String) + "/" + (ittcResults.size() as String)
		result.infoTeknisi = (ittcResults.size() as String) + "/" + (ittcResults.size() as String)

		def twfdc = Reception.createCriteria()
        def twfdcResults = twfdc.list {
			eq("companyDealer",companyDealer)
            eq("staDel","0")
            eq("staSave","0")
            isNull("t401StaInvoice")
            ge("t401TglJamJanjiPenyerahan",new Date().clearTime())
            lt("t401TglJamJanjiPenyerahan", new Date().clearTime()+1)
        }

		result.todayWaitingForDelivery = twfdcResults.size()
		
		def tuec = CustomerIn.createCriteria().list {
            eq("companyDealer",params?.userCompanyDealer)
			ge("dateCreated", new Date().clearTime())
            lt("dateCreated",new Date().clearTime()+1)
		}
		result.todayUnitEntry = tuec?.size()

        def tnodcResultss = InvoiceT701.executeQuery("select inv from InvoiceT701 inv , Reception rcp where inv.reception.id = rcp.id and inv.dateCreated > rcp.t401TglJamJanjiPenyerahan and inv.dateCreated >= ? and inv.companyDealer = ? ", [new Date().clearTime(), companyDealer])
        result.todayNonOntimeDelivery = tnodcResultss.size()

        def c = Appointment.createCriteria()
        def tacResults = c.list {
            ge("t301TglJamRencana",dateAwal)
            le("t301TglJamRencana",dateAkhir)
            eq('companyDealer', companyDealer)
            eq("staDel","0")
            eq("staSave","0")
        }

        result.todayAppointment = tacResults.size()
		
		def tuicResults = InvoiceT701.createCriteria().list {
            eq("t701StaDel","0")
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            eq("companyDealer",companyDealer)
            ge("dateCreated",new Date().clearTime())
		}
		result.todayUnitInvoice = tuicResults.size()
		
		
		def twic = CustomerIn.createCriteria()
		def twicResults = twic.list {
            eq("staDel","0");
            ilike("t400NomorAntrian","w%")
            eq("companyDealer",companyDealer)
            ge("dateCreated",new Date().clearTime())
            isNotNull("reception")
		}
		result.todayWalkIn = twicResults.size()
		
		def tsc = Settlement.createCriteria()
		def tsResults = tsc.list {
			eq("companyDealer",companyDealer)
            ge("dateCreated", new Date().clearTime())
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("invoice","invoice")
            }
		}
		result.todaySettlement = tsResults.size()

        def todayCustIn = CustomerIn.createCriteria().list {
            eq("staDel","0")
            eq("companyDealer",companyDealer)
            ge("dateCreated",new Date().clearTime())
        }
		def tnsac = Appointment.createCriteria()
		def tnsacResults = tnsac.list {
            ge("t301TglJamRencana",dateAwal)
            lt("t301TglJamRencana",dateAkhir)
            eq("staDel","0")
            eq("staSave","0")
            eq('companyDealer', companyDealer)
            le("t301TglJamRencana", new Date())
            if(todayCustIn?.size()>0){
                historyCustomerVehicle{
                    not {
                        inList("customerVehicle",todayCustIn?.customerVehicle)
                    }
                }
            }
		}
		result.todayNoShowAppointment = tnsacResults.size()
		
		def tmac = Appointment.createCriteria()
		def tmacResults = tmac.list {
            eq("staDel","0")
            eq("staSave","0")
            ge("t301TglJamRencana",dateAwal+1)
            lt("t301TglJamRencana",dateAkhir+2)
            eq('companyDealer', companyDealer)
		}
		result.tommorowAppointment = tmacResults.size()

        def tuinv = InvoiceT701.createCriteria().list {
            eq("t701StaDel","0")
            eq("companyDealer",companyDealer)
            eq("t701JenisInv","T",[ignoreCase:true])
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            eq("t701StaSettlement","0")
            order("t701TglJamInvoice")
        }

		result.todayUnpaidInvoice = tuinv?.size()

        def orUnpaid = Reception.createCriteria().list () {
            eq("companyDealer",companyDealer)
            customerIn{
                tujuanKedatangan{
                    ilike("m400Tujuan","%BP%")
                }
            }
            eq("staDel","0")
            eq("staSave","0")
            or{
                isNull("t401StaInvoice")
                ne("t401StaInvoice","0")
            }
            or{
                isNull("t401StaBayarOnRisk")
                eq("t401StaBayarOnRisk","0")
            }
            isNotNull("t401OnRisk")
        }

        result.todayUnpaidOnRisk = orUnpaid?.size()
		result
	}
	
	def informasiPenting() {
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def c = InformasiPenting.createCriteria()
		def results = c.list () {
			eq('companyDealer', companyDealer)
            eq('m032StaTampil',"0")
		}
		def rows = []
		results.each {			
			rows << [ 
			
						id: it.id,
			
						tanggal: it.m032Tanggal?.format(dateFormat),

                        judul: it?.m032Judul ? it?.m032Judul:"Tidak Ada Judul",

                        content: it?.m032Konten ? it?.m032Konten:"Tidak Ada Konten",

					]
		}
		rows
	}
	
	def runningText() {
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
//		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def c = GeneralParameter.createCriteria()
		def result = c.get () {

//			switch(sortProperty){
//				default:
//					order(sortProperty,sortDir)
//					break;
//			}
			eq('companyDealer', companyDealer)
			or{
				eq('m000StaWaktuAkhir', '0')
				and{
					eq('m000StaWaktuAkhir', '1')
					lt('m000TglJamWaktuAkhir', new Date())
				}
			}
		}
		def	runningText = result?.m000RunningTextInformation
        if(companyDealer){
            def stokOpnameStatus = StockOpnameLog.findByCompanyDealerAndStaDel(companyDealer,'0')
            if(stokOpnameStatus){
                if(stokOpnameStatus.last().staStockOpname=="0"){
                    runningText = "SISTEM SEDANG MELAKUKAN STOKOPNAME"
                }
            }
        }
		runningText
	}
	
	def infoTeknisiDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = NamaManPower.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("companyDealer", companyDealer)
            eq("staDel","0")

            manPowerDetail{
                or{
                    ilike("m015LevelManPower","%Technician%")
                    ilike("m015LevelManPower","%Apprentice%")
                }

            }

        }


		def today = new Date().clearTime()
		def now = new Date()

        def rows = []

		results.each {
			def status
			if(it?.lastActual >= now ){
				status = 'Idle'
			} else {
				status = "Aktif"
			}


			def hadir
			if(it?.lastHadir >= today ){
				hadir = 'Tidak Hadir'
			} else {
				hadir = "Hadir"
			}

//
//            def chek = it?.t015NamaLengkap
//            if (chek=="DENNY LALENOH" || chek=="Berny Lontoh" || chek=="Deasy Weno" ||
//                chek=="Ronny rangian" || chek=="Jackson Eman" || chek=="Jackson Eman"
//            ){
//                chek="-"
//            }
//
//            def inisial = it?.t015NamaBoard
//            if (inisial=="DENNY"){
//                inisial="-"
//            }

			rows << [

						id: it.id,

                        namaTeknisi: it?.t015NamaLengkap?.toUpperCase(),

						inisial: it?.t015NamaBoard?.toUpperCase(),

						status:  status ? status:"-",

						kehadiran: hadir ? hadir:"-"
					]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	}

	def todayWaitingForDeliveryDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

        def c = Reception.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",companyDealer)
            eq("staDel","0")
            eq("staSave","0")
            ge("t401TglJamJanjiPenyerahan",new Date().clearTime())
            lt("t401TglJamJanjiPenyerahan", new Date().clearTime()+1)
            isNull("t401StaInvoice")
		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
            def jamProd = ""
            def recept = Reception.findById(params.id)
            //chek//
            def actAwal = Actual?.findByReceptionAndStatusActualAndStaDel(recept,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%Clock On%","0"),"0",[sort : 'dateCreated'])
            if(actAwal){
                jamProd = actAwal?.t452TglJam?.format("dd/MM/yyyy HH:mm")
            }
			rows << [
						id: it?.id,
						noUrut: noUrut,
						noPolisi: it?.historyCustomerVehicle?.fullNoPol,
						model:  it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: it?.historyCustomer?.fullNama,
						noHp: it?.historyCustomer?.t182NoHp,
						noWO: it?.t401NoWO,
						jamProduction: jamProd,
						jamJanjiPenyerahan: it?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm")
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}

	def todayUnitEntryDatatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = CustomerIn.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("companyDealer",companyDealer);
			ge("dateCreated",new Date().clearTime())
			lt("dateCreated",new Date().clearTime()+1)
		}

		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
            def jamProd = ""
            if(it?.reception){
                def actAwal = Actual?.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%Clock On%","0"),"0",[sort : 'dateCreated'])
                if(actAwal){
                    jamProd = actAwal?.t452TglJam?.format("dd/MM/yyyy HH:mm")
                }
            }
            def histC = it?.reception?.historyCustomer
            if(!histC && it?.customerVehicle){
                def custVehicle = it?.customerVehicle
                def mapping = MappingCustVehicle.createCriteria().list {
                    eq("customerVehicle",custVehicle)
                }
                histC = HistoryCustomer.createCriteria().get {
                    if(mapping.size()<1){
                        eq("id",-1000.toLong());
                    }else{
                        inList("customer",mapping?.customer);
                    }
                    or{
                        customer{
                            peranCustomer{
                                or{
                                    ilike("m115NamaPeranCustomer","%pemilik%")
                                    ilike("m115NamaPeranCustomer","%penanggung jawab%")
                                }
                            }
                        }
                        peranCustomer{
                            or{
                                ilike("m115NamaPeranCustomer","%pemilik%")
                                ilike("m115NamaPeranCustomer","%penanggung jawab%")
                            }
                        }
                    }
                    order("dateCreated","desc")
                    maxResults(1);
                }
            }
            rows << [
						id: it.id,
						noUrut: noUrut,
						noPolisi: it?.customerVehicle ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol : it?.t400noPol ,
						model:  it?.customerVehicle ? it?.customerVehicle?.getCurrentCondition()?.fullModelCode?.baseModel?.m102NamaBaseModel : "",
						namaCustomer: histC ? histC?.fullNama : "",
						noHp: histC ? histC?.t182NoHp : "",
						noWO: it?.reception ? it?.reception?.t401NoWO : "",
						namaSA: it?.reception ? it?.reception?.t401NamaSA : "",
						jamProduction: jamProd,
						tujuanKedatangan: it.reception ? it.reception.tipeKerusakan==null?"GR":"BP" : it?.tujuanKedatangan?.m400Tujuan,
						jamJanjiPenyerahan: it?.reception ? it?.reception?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm") : ""
					]
					noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	def todayNonOntimeDeliveryDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

        def results = InvoiceT701.executeQuery("select inv from InvoiceT701 inv , Reception rcp where inv.reception.id = rcp.id and inv.dateCreated > rcp.t401TglJamJanjiPenyerahan and inv.dateCreated >= :skrg and inv.companyDealer = :company and rcp.t401TglJamJanjiPenyerahan is not null ", [skrg : new Date().clearTime(), company : companyDealer , max: params.iDisplayLength as int, offset: params.iDisplayStart as int])
        def count = InvoiceT701.executeQuery("select inv from InvoiceT701 inv , Reception rcp where inv.reception.id = rcp.id and inv.dateCreated > rcp.t401TglJamJanjiPenyerahan and inv.dateCreated >= :skrg and inv.companyDealer = :company and rcp.t401TglJamJanjiPenyerahan is not null ", [skrg : new Date().clearTime(), company : companyDealer])?.size()
		def rows = []

		def noUrut = 1
		results.each {
			def awalProd = ""
            def lastFinish = ""
//            def CarijamNotifikasiSA =InvoiceT701?.findByReceptionAndT701TglJamCetakAndT701StaDel(it?.reception,)
            def actAwal = Actual?.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%Clock On%","0"),"0",[sort : 'dateCreated'])
            if(actAwal){
                awalProd = actAwal?.t452TglJam?.format("dd/MM/yyyy HH:mm")
            }
            def actAkhir = Actual?.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%Clock Off%","0"),"0",[sort : 'dateCreated',order : 'desc'])
            if(actAkhir){
                lastFinish = actAkhir?.t452TglJam?.format("dd/MM/yyyy HH:mm")
            }

			rows << [
						id: it.id,
						noUrut: noUrut,
						noPolisi: it.reception?.historyCustomerVehicle?.fullNoPol ? it.reception?.historyCustomerVehicle?.fullNoPol:"-",
						model:  it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel:"-",
						namaCustomer: it?.historyCustomer?.fullNama ? it?.historyCustomer?.fullNama:"-",
						noHp: it?.reception?.historyCustomer?.t182NoHp ? it?.reception?.historyCustomer?.t182NoHp:"-",
						noWO: it.reception?.t401NoWO ? it.reception?.t401NoWO:"-",
                        jamNotifikasi:it?.t701TglJamCetak?.format("dd/MM/yyyy HH:mm"),
						janjiPenyerahan: it?.reception?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm")
					]
					noUrut++
		}
		[sEcho: params.sEcho, iTotalRecords:  count, iTotalDisplayRecords: count, aaData: rows]
				
	}
	def todayAppointmentDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session?.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
        def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			ge("t301TglJamRencana",dateAwal)
            le("t301TglJamRencana",dateAkhir)
			eq('companyDealer', companyDealer)
		}

		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
			def namaCustomer = ""

			rows << [
                    id: it.id,
                    noUrut: noUrut,
                    noPolisi: it.historyCustomerVehicle?.fullNoPol,
                    model:  it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    namaCustomer: it?.historyCustomer?.fullNama,
                    noHp: it.historyCustomer?.t182NoHp,
                    noWO: it?.reception?.t401NoWO,
                    noAppointment: it?.reception?.t401NoAppointment,
                    jamProduction : it?.t301TglJamRencana ? it?.t301TglJamRencana?.format("dd/MM/yyyy HH:mm") : "",
                    jamJanjiPenyerahan : it?.t301TglJamPenyerahanSchedule ? it?.t301TglJamPenyerahanSchedule?.format("dd/MM/yyyy HH:mm") : ""
            ]
            noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	def todayUnitInvoiceDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = InvoiceT701.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t701StaDel","0")
            or{
                isNull("t701StaApprovedReversal")
                ne("t701StaApprovedReversal","0")
            }
            eq("companyDealer",companyDealer)
            ge("dateCreated",new Date().clearTime())
            isNull("t701NoInv_Reff")
            gt("t701PPnRp",0.toDouble())
        }
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
            def jamProd = ""
            def actAwal = Actual?.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%Clock On%","0"),"0",[sort : 'dateCreated'])
            if(actAwal){
                jamProd = actAwal?.t452TglJam?.format("dd/MM/yyyy HH:mm")
            }
            rows << [
                id: it.id,
                noUrut: noUrut,
                noPolisi: it?.reception?.historyCustomerVehicle?.fullNoPol,
                model:  it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                namaCustomer: it?.t701Customer,
                noHp: "",
                noWO: it?.reception?.t401NoWO,
                jamProduction: jamProd,
                jamJanjiPenyerahan: it?.reception?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm")
            ]
            noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	def todayWalkInDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = CustomerIn.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			ge("dateCreated", new Date().clearTime())
            eq("companyDealer",companyDealer)
            ilike("t400NomorAntrian","w%")
            not {
                ilike("t400NomorAntrian","% %")
            }
            isNotNull("reception")

		}
		
		def today = new Date().clearTime()
		def now = new Date()
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
            def histC = it?.reception?.historyCustomer
            def jamProd = ""
            if(it?.reception){
                def actAwal = Actual?.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%Clock On%","0"),"0",[sort : 'dateCreated'])
                if(actAwal){
                    jamProd = actAwal?.t452TglJam?.format("dd/MM/yyyy HH:mm")
                }
            }
            if(!histC && it?.customerVehicle){
                def custVehicle = it?.customerVehicle
                def mapping = MappingCustVehicle.createCriteria().list {
                    eq("customerVehicle",custVehicle)
                }
                histC = HistoryCustomer.createCriteria().get {
                    if(mapping.size()<1){
                        eq("id",-1000.toLong());
                    }else{
                        inList("customer",mapping?.customer);
                    }
                    or{
                        customer{
                            peranCustomer{
                                or{
                                    ilike("m115NamaPeranCustomer","%pemilik%")
                                    ilike("m115NamaPeranCustomer","%penanggung jawab%")
                                }
                            }
                        }
                        peranCustomer{
                            or{
                                ilike("m115NamaPeranCustomer","%pemilik%")
                                ilike("m115NamaPeranCustomer","%penanggung jawab%")
                            }
                        }
                    }
                    order("dateCreated","desc")
                    maxResults(1);
                }
            }
			rows << [
			
                id: it.id,
                noUrut: noUrut,
                noPolisi: it?.customerVehicle ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol : it?.t400noPol,
                model:  it?.customerVehicle ? it?.customerVehicle?.getCurrentCondition()?.fullModelCode?.baseModel?.m102NamaBaseModel : "",
                namaCustomer: histC ? histC?.fullNama : "-",
                noHp: histC ?  histC?.t182NoHp : "-",
                noWO: it.reception ? it?.reception?.t401NoWO : "",
                namaSA: it.reception ? it?.reception?.t401NamaSA : "",
                jamProduction: jamProd ? jamProd:"-",
                jamJanjiPenyerahan: it.reception ? it?.reception?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm") : "-"
            ]
            noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	def todaySettlementDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

        def c = Settlement.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
            eq("companyDealer",companyDealer)
            ge("dateCreated", new Date().clearTime())
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("invoice","invoice")
            }
        }
		
		def rows = []
		def noUrut = 1
		results.each {
            InvoiceT701 invoice = it?.invoice
            def jamProd = ""
            def actAwal = Actual?.findByReceptionAndStatusActualAndStaDel(invoice?.reception,StatusActual.findByM452StatusActualIlikeAndM452StaDel("%Clock On%","0"),"0",[sort : 'dateCreated'])
            if(actAwal){
                jamProd = actAwal?.t452TglJam?.format("dd/MM/yyyy HH:mm")
            }
            rows << [
						id: invoice?.id,
						noUrut: noUrut,
						noPolisi: invoice?.t701Nopol,
						model:  invoice?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
						namaCustomer: invoice?.t701Customer,
						noHp: invoice?.reception?.historyCustomer?.t182NoHp,
						noWO: invoice?.reception?.t401NoWO,
						jamProduction: jamProd,
						jamJanjiPenyerahan: invoice?.reception?.t401TglJamPenyerahan
					]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	def todayNoShowAppointmentDatatablesList(def params) {
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def x = 0

        def todayCustIn = CustomerIn.createCriteria().list {
            eq("staDel","0")
            eq("companyDealer",companyDealer)
            ge("dateCreated",new Date().clearTime())
        }
        def c = Appointment.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            ge("t301TglJamRencana",dateAwal)
            lt("t301TglJamRencana",dateAkhir)
            eq("staDel","0")
            eq("staSave","0")
            eq('companyDealer', companyDealer)
            le("t301TglJamRencana", new Date())
            if(todayCustIn?.size()>0){
                historyCustomerVehicle{
                    not {
                        inList("customerVehicle",todayCustIn?.customerVehicle)
                    }
                }
            }
        }

		def rows = []
		
		def noUrut = 1
		
		results.each {
			rows << [
                id: it.id,
                noUrut: noUrut,
                noPolisi: it.historyCustomerVehicle?.fullNoPol,
                model:  it.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                namaCustomer: it?.historyCustomer?.fullNama,
                noHp: it.historyCustomer?.t182NoHp,
                noAppointment: it.reception?.t401NoAppointment,
                jamJanjiDatang: it?.t301TglJamRencana ? it?.t301TglJamRencana?.format("dd/MM/yyyy HH:mm") : ""
            ]
            noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	def tommorowAppointmentDatatablesList(def params) {
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")

		def c = Appointment.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            eq('companyDealer', companyDealer)
            ge("t301TglJamRencana",dateAwal+1) // tommorrow appointmen +1
            lt("t301TglJamRencana",dateAkhir+1)
            order("t301TglJamRencana", "asc")

		}
		
		def rows = []
		
		def noUrut = 1
		
		results.each {
			rows << [
                    id: it?.id,
                    noUrut: noUrut,
                    noPolisi: it?.historyCustomerVehicle?.fullNoPol,
                    model:  it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    namaCustomer: it?.historyCustomer?.fullNama,
                    noHp: it?.historyCustomer?.t182NoHp,
                    noAppointment: it?.reception?.t401NoAppointment,
                    jamJanjiDatang: it?.t301TglJamRencana ? it?.t301TglJamRencana?.format("dd/MM/yyyy HH:mm") : ""
                ]
                noUrut++
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}

	def todayUnpaidInvoiceDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
        Date skrg = new Date().clearTime()

        def c = InvoiceT701.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t701StaDel","0")
			eq("companyDealer",companyDealer)
            eq("t701JenisInv","T",[ignoreCase:true])
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            eq("t701StaSettlement","0")
            order("t701TglJamInvoice")
		}
		def rows = []

		def noUrut = 1

		results.each {
            rows << [
                    id: it.id,
                    no: noUrut,
                    noInvoice: it?.t701NoInv,
                    tgglInv: it?.t701TglJamInvoice ? it?.t701TglJamInvoice?.format("dd/MM/yyyy") : it?.dateCreated?.format("dd/MM/yyyy"),
                    noWo: it.reception?.t401NoWO,
                    tgglWo: it.reception?.t401TglJamCetakWO ? it.reception?.t401TglJamCetakWO?.format("dd/MM/yyyy") : it?.dateCreated?.format("dd/MM/yyyy"),
                    noPolisi: it.reception?.historyCustomerVehicle?.fullNoPol,
                    model:  it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    namaCustomer: it?.t701Customer,
                    noHp: it?.reception?.historyCustomer?.t182NoHp,
                    sa: it?.reception?.t401NamaSA
            ]
            noUrut++
		}

		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

	}

    def todayUnpaidOnRiskDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		def session = RequestContextHolder.currentRequestAttributes().getSession()
		def companyDealer =  session?.userCompanyDealer
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",companyDealer)
            customerIn{
                tujuanKedatangan{
                    ilike("m400Tujuan","%BP%")
                }
            }
            eq("staDel","0")
            eq("staSave","0")
            or{
                isNull("t401StaInvoice")
                ne("t401StaInvoice","0")
            }
            or{
                isNull("t401StaBayarOnRisk")
                eq("t401StaBayarOnRisk","0")
            }
            isNotNull("t401OnRisk")
            order("dateCreated")
		}

		def rows = []

		def noUrut = 1

		results.each {
            rows << [
                    id: it.id,
                    no: noUrut,
                    noWo: it?.t401NoWO,
                    tanggal: it?.t401TglJamCetakWO ? it?.t401TglJamCetakWO?.format("dd/MM/yyyy") : it?.dateCreated?.format("dd/MM/yyyy"),
                    noPolisi: it?.historyCustomerVehicle?.fullNoPol,
                    model:  it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    namaCustomer: it?.historyCustomer?.fullNama,
                    noHp: it?.historyCustomer?.t182NoHp
            ]
            noUrut++
		}

		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

	}
}