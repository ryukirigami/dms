package com.kombos.maintable

class noPajakService {



    def datatablesList(def params) {
//        def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.maintable.NoPajak.DATE_FORMAT)
//        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NoPajak.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params."search_noawal"){
                eq("noawal", Integer.parseInt(params."sCriteria_noawal"))
            }

            if(params."search_noakhir"){
                ilike("noakhir","%" + (params."sCriteria_noakhir" as String) + "%")
            }

            if (params."search_sisa"){
                ilike("sisa","%" + (params."sCriteria_sisa" as String) + "%")
            }

            if(params."search_noakhirdigunakan"){
                ilike("noakhirdigunakan","%" + (params."sCriteria_noakhirdigunakan" as String) + "%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []
        def sia
        def status
        results.each {
            def sta
           status=it.statushabis
            sia = it.sisa
            if(status==0)
            {
                sta="Habis"
            }
            else{
                sta="Tersedia"
            }


            rows << [

                    id: it.id,
                    tglawalpajak : it.tglawalpajak.format("dd/MM/yyyy"),
                    tglakhirpajak: it.tglakhirpajak.format("dd/MM/yyyy"),
                    noawal       : it?.noawal ? it?.noawal :"-",
                    noakhir      : it.noakhir ? it?.noakhir :"-",
                    statushabis  : sta,
                    sisa   : sia ? sia:"-",
                   noakhirdigunakan : it?.terakhirpakai ? it?.terakhirpakai :"-"


            ]

        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["NoPajak", params.id] ]
            return result
        }

        result.noPajakInstance = new NoPajak()
        result.noPajakInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.noPajakInstance && m.field)
                result.noPajakInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["NoPajak", params.id] ]
            return result
        }

        NoPajak noPajak = new NoPajak(params)
        noPajak.lastUpdProcess="INSERT"
        noPajak.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        noPajak.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        noPajak.staDel = "0"
        noPajak.statushabis="0"
        noPajak.save(flush: true)

        if(noPajak.hasErrors()) {
            //println noPajak.errors
            return fail(code:"default.not.created.message")
        } else {
            // insert juga ke tabel TH003_HistoryKaryawan dengan history "MASUK".

            result.noPajakInstance = noPajak

//            if (insertToHistoryKaryawan(karyawan) == FAIL) {
//                return fail(code:"default.not.created.message")
//            }

        }

        // success
        return result
    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["NoPajak", params.id] ]
            return result
        }

        result.noPajakInstance = NoPajak.get(params.id)

        if(!result.noPajakInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["NoPajak", params.id] ]
            return result
        }

        result.noPajakInstance = NoPajak.get(params.id)

        if(!result.noPajakInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["NoPajak", params.id] ]
            return result
        }

        result.noPajakInstance = NoPajak.get(params.id)

        if(!result.noPajakInstance)
            return fail(code:"default.not.found.message")

        try {
            result.noPajakInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }


    def update(params) {
        NoPajak.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.noPajakInstance && m.field)
                    result.noPajakInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["NoPajak", params.id] ]
                return result
            }

            result.noPajakInstance = NoPajak.get(params.id)

            if(!result.noPajakInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.noPajakInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.noPajakInstance.properties = params

            if(result.noPajakInstance.hasErrors() || !result.noPajakInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }
}
