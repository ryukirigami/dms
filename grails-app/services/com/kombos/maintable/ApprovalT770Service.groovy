package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaApproval
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.parts.StatusApproval
import org.springframework.context.ApplicationContext
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class ApprovalT770Service {
	boolean transactional = false

	def grailsApplication

	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def appSettingParamTimeFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.TIME_FORMAT)

	def datatablesList(def params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue

		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = ApprovalT770.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("companyDealer",session.userCompanyDealer)
            }
			if(params."sCriteria_kegiatanApproval"){
				kegiatanApproval{
					ilike("m770KegiatanApproval","%" + params."sCriteria_kegiatanApproval" + "%")
				}
			}


			if(params."sCriteria_kegiatanCari"){
				kegiatanApproval{
					ilike("m770KegiatanApproval","%" + params."sCriteria_kegiatanCari" + "%")
				}
			}


			if(params."sCriteria_t770NoDokumen"){
				ilike("t770NoDokumen","%" + (params."sCriteria_t770NoDokumen" as String) + "%")
			}

			if(params."sCriteria_t770xNamaUser"){
				ilike("t770xNamaUser","%" + (params."sCriteria_t770xNamaUser" as String) + "%")
			}

			if(params."sCriteria_t770TglJamSend"){
				ge("t770TglJamSend",params."sCriteria_t770TglJamSend")
				lt("t770TglJamSend",params."sCriteria_t770TglJamSend" + 1)
			}

			if(params."sCriteria_t770Status"){
				eq("t770Status",params."sCriteria_t770Status")
			}

			if(params."sCriteria_t770CurrentLevel"){
				eq("t770CurrentLevel",params."sCriteria_t770CurrentLevel")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

            eq("t770Status",StatusApproval.WAIT_FOR_APPROVAL)
			order("dateCreated","desc")
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}

		def rows = []

		results.each {
            def tampil = false
            User user = User.findByUsername(params.user)
            def roles = user.roles
            roles.each { rol ->
                if(NamaApproval.findByApprovelRoleAndKegiatanApproval(rol,it.kegiatanApproval)){
                    tampil = true
				}else if(rol.authority == 'ROLE_ADMIN'){
					tampil=true
				}else if(rol.authority == 'ROLE_APPROVAL'){
					tampil=true
				}else if(rol.authority == 'KEPALA_BENGKEL'){
					tampil=true
				}
            }
            if(tampil == true){
                Date waktuSekarang = new DatatablesUtilService()?.syncTime();

//                Calendar dt = Calendar.getInstance();
//                Calendar cal = Calendar.getInstance();
//                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
//                String hasil = sdf.format(cal.getTime());
                def tgl2 =waktuSekarang?.format("dd")?.toLong()
                def bulan2=waktuSekarang?.format("MM")?.toLong()
                def jam2 = waktuSekarang?.format("HH")?.toLong()
                def menit2 = waktuSekarang?.format("mm")?.toLong()
                def detik2 = waktuSekarang?.format("ss")?.toLong()

                def  total2=(tgl2*24)+(bulan2*720)+jam2

                def tgl1=it.t770TglJamSend.format("dd").toLong()
                def bulan1=it.t770TglJamSend.format("MM").toLong()
                def jam1=it.t770TglJamSend.format("HH").toLong()
                def menit1=it.t770TglJamSend.format("mm").toLong()
                def detik1=it.t770TglJamSend.format("ss").toLong()
                def total1=(tgl1*24)+(bulan1*720)+jam1

                def selisih=total2-total1
                def hari=selisih.intValue()/24
                def jam

                def totaljam1=(jam1*3600)+(menit1*60)+detik1
                def totaljam2=(jam2*3600)+(menit2*60)+detik2
                def selisihjam=totaljam2-totaljam1
                def j=selisihjam/3600
                if(hari==0)
                {

                   jam=j

                }   else{
                jam=hari*24
                }

                rows <<[
                    id: it.id,
                    kegiatanApproval: it.kegiatanApproval.m770KegiatanApproval,
                    t770NoDokumen: it.t770NoDokumen,
                    t770xNamaUser: it.t770xNamaUser,
                    t770TglJamSend: it.t770TglJamSend?it.t770TglJamSend.format("dd/MM/yyyy HH:mm"):"",
                    t770Status: it.t770Status.getKey(),
                    t770CurrentLevel: it.t770CurrentLevel,
                    durasi         : jam.intValue()
                ]
            }
		}

		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	}

	def historyDatatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue

		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = HistoryApproval.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
//            eq("companyDealer", params.companyDealer)
		    if(params."sCriteria_approval"){
                approval{
					eq("id", params."sCriteria_approval" as long)
				}
			}
			order('level','asc')
		}

		def rows = []

		results.each {
			rows << [

				id: it.id,

				approver: it?.namaApproved,

				status: it?.status?.getKey(),

				tanggal: it?.tglJamApproved?it.tglJamApproved.format(dateFormat + " " + timeFormat):"",

			]
		}

		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
	}

	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ApprovalT770", params.id]]
			return result
		}

		result.approvalT770Instance = ApprovalT770.get(params.id)

		if(!result.approvalT770Instance)
			return fail(code:"default.not.found.message")

		def c = NamaApproval.createCriteria()
		result.currentApprover = c.get () {
			kegiatanApproval{
				eq("id", result.approvalT770Instance.kegiatanApproval.id)
			}

			eq("m771Level", result.approvalT770Instance.t770CurrentLevel + 1)
            maxResults(1);
		}

		return result
	}

	def showHistory(params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: [
					"HistoryApproval",
					params.id]
			]
			return result
		}

		def historyInstance = HistoryApproval.get(params.id)
		
		result.waktuApproval = historyInstance.tglJamApproved.format(dateFormat + " " + timeFormat)
		result.statusApproval = historyInstance.status.getKey()
		result.pesanApprover = historyInstance.keterangan
		
		// Success.
		return result
	}

	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ApprovalT770", params.id]]
			return result
		}

		result.approvalT770Instance = ApprovalT770.get(params.id)

		if(!result.approvalT770Instance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}

	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ApprovalT770", params.id]]
			return result
		}

		result.approvalT770Instance = ApprovalT770.get(params.id)

		if(!result.approvalT770Instance)
			return fail(code:"default.not.found.message")

		try {
			result.approvalT770Instance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}
	}

	def update(params) {
		ApprovalT770.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.approvalT770Instance && m.field)
					result.approvalT770Instance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["ApprovalT770", params.id]]
				return result
			}

			result.approvalT770Instance = ApprovalT770.get(params.id)

			if(!result.approvalT770Instance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.approvalT770Instance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.approvalT770Instance.properties = params

			if(result.approvalT770Instance.hasErrors() || !result.approvalT770Instance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["ApprovalT770", params.id]]
			return result
		}

		result.approvalT770Instance = new ApprovalT770()
		result.approvalT770Instance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.approvalT770Instance && m.field)
				result.approvalT770Instance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["ApprovalT770", params.id]]
			return result
		}

		result.approvalT770Instance = new ApprovalT770(params)

		if(result.approvalT770Instance.hasErrors() || !result.approvalT770Instance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}

	def updateField(def params){
		def approvalT770 =  ApprovalT770.findById(params.id)
		if (approvalT770) {
			approvalT770."${params.name}" = params.value
			approvalT770.save()
			if (approvalT770.hasErrors()) {
				throw new Exception("${approvalT770.errors}")
			}
		}else{
			throw new Exception("ApprovalT770 not found")
		}
	}


	def respondApproval(def params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
		ApprovalT770.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.approvalT770Instance && m.field)
					result.approvalT770Instance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["ApprovalT770", params.id]]
				return result
			}

			result.approvalT770Instance = ApprovalT770.get(params.id)

			if(!result.approvalT770Instance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.approvalT770Instance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			//result.approvalT770Instance.properties = params
			
			//keterangan & status
			
			
			StatusApproval statusApproval = StatusApproval.getEnumFromId(params.status)
			if(result.approvalT770Instance.t770Status != StatusApproval.REJECTED)
				result.approvalT770Instance.t770Status = statusApproval
			
			result.approvalT770Instance.t770CurrentLevel = result.approvalT770Instance.t770CurrentLevel + 1
			try {
				if(result.approvalT770Instance.kegiatanApproval.m770KegiatanApproval=="Approval Special Discount" &&
						result.approvalT770Instance.t770Status != StatusApproval.REJECTED){
					String addFK ="#"
					if(params.discJasa){
						addFK+=params.discJasa
					}

					addFK +="#"
					if(params.discPart){
						addFK+=params.discPart
					}

					addFK +="#"
					if(params.discMaterial){
						addFK+=params.discMaterial
					}

					addFK +="#"
					if(params.discPart){
						addFK+=params.discOli
					}

					result.approvalT770Instance.t770FK = result.approvalT770Instance.t770FK+addFK
				}
			}catch (Exception e){

			}


			def historyApproval = new HistoryApproval(
                CompanyDealer:session?.userCompanyDealer,
				approval: result.approvalT770Instance,
				status: statusApproval,
				level: result.approvalT770Instance.t770CurrentLevel,
				tglJamApproved: new Date(),
				namaApproved: org.apache.shiro.SecurityUtils.subject.principal.toString(),
				keterangan: params.keterangan
				) 
			historyApproval.save(flush:true)

			def afterApprovedServiceName = result.approvalT770Instance.kegiatanApproval.afterApprovalService
			if(afterApprovedServiceName){
				ApplicationContext ctx = (ApplicationContext)grailsApplication.getMainContext();
				AfterApprovalInterface aaService = (AfterApprovalInterface) ctx.getBean(afterApprovedServiceName);
				aaService.afterApproval(result.approvalT770Instance.t770FK,statusApproval,new Date(), params.keterangan, org.apache.shiro.SecurityUtils.subject.principal.toString());
			}
			
			
			if(result.approvalT770Instance.hasErrors() || !result.approvalT770Instance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
		


	}

}