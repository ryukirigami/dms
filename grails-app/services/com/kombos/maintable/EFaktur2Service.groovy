package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class EFaktur2Service {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def jasperService
    def excelImportService
    def datatablesUtilService


    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = EFaktur.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("jenisInputan","PPN_KELUARAN")
            eq("staOriRev","0")
            order("noUrutInvoice","asc")
            if (params.sCriteria_docNumber){
                eq("docNumber",params.sCriteria_docNumber)
            }
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("staDel","0")
                eq("jenisInputan","PPN_KELUARAN")
                eq("staOriRev","0")
                eq("staAprove","1") // ini staAprove 1 buat yang HO
                order("noUrutInvoice","asc")
                if (params.sCriteria_docNumber){
                    eq("docNumber",params.sCriteria_docNumber)
                }

            }else{
                eq("companyDealer",params.userCompanyDealer)
            }

            if(params.sCriteria_Tanggal && params.sCriteria_Tanggal2){
                ge("tanggal",params.sCriteria_Tanggal)
                lt("tanggal",params.sCriteria_Tanggal2 + 1)
            }

            if(params."sCriteria_FK"){
                ilike("FK","%" + (params."sCriteria_FK" as String) + "%")
            }

            if(params."sCriteria_KD_JENIS_TRANSAKSI"){
                ilike("KD_JENIS_TRANSAKSI","%" + (params."sCriteria_KD_JENIS_TRANSAKSI" as String) + "%")
            }

            if(params."sCriteria_FG_PENGGANTI"){
                ilike("FG_PENGGANTI","%" + (params."sCriteria_FG_PENGGANTI" as String) + "%")
            }

            if(params."sCriteria_NOMOR_FAKTUR"){
                ilike("NOMOR_FAKTUR","%" + (params."sCriteria_NOMOR_FAKTUR" as String) + "%")
            }

            if(params."sCriteria_MASA_PAJAK"){
                ilike("MASA_PAJAK","%" + (params."sCriteria_MASA_PAJAK" as String) + "%")
            }

            if(params."sCriteria_TAHUN_PAJAK"){
                ilike("TAHUN_PAJAK","%" + (params."sCriteria_TAHUN_PAJAK" as String) + "%")
            }

            //tanggalfaktur
            if(params."sCriteria_TANGGAL_FAKTUR"){
                ge("TANGGAL_FAKTUR",params."sCriteria_TANGGAL_FAKTUR")
                lt("TANGGAL_FAKTUR",params."sCriteria_TANGGAL_FAKTUR" + 1)
            }


            if(params."sCriteria_NPWP"){
                ilike("NPWP","%" + (params."sCriteria_NPWP" as String) + "%")
            }

            if(params."sCriteria_NAMA"){
                ilike("NAMA","%" + (params."sCriteria_NAMA" as String) + "%")
            }

            if(params."sCriteria_ALAMAT_LENGKAP"){
                ilike("ALAMAT_LENGKAP","%" + (params."sCriteria_ALAMAT_LENGKAP" as String) + "%")
            }

            if(params."sCriteria_NAMA_OBJEK"){
                ilike("NAMA_OBJEK","%" + (params."sCriteria_NAMA_OBJEK" as String) + "%")
            }

            if(params."sCriteria_JUMLAH_DPP"){
                ilike("JUMLAH_DPP","%" + (params."sCriteria_JUMLAH_DPP") + "%")
            }

            if(params."sCriteria_JUMLAH_PPN"){
                ilike("JUMLAH_PPN","%" + (params."sCriteria_JUMLAH_PPN" as String) + "%")
            }

            if(params."sCriteria_JUMLAH_PPNBM"){
                ilike("JUMLAH_PPNBM","%" + (params."sCriteria_JUMLAH_PPNBM" as String) + "%")
            }

            if(params."sCriteria_ID_KETERANGAN_TAMBAHAN"){
                ilike("ID_KETERANGAN_TAMBAHAN","%" + (params."sCriteria_ID_KETERANGAN_TAMBAHAN" as String) + "%")
            }

            if(params."sCriteria_FG_UANG_MUKA"){
                ilike("FG_UANG_MUKA","%" + (params."sCriteria_FG_UANG_MUKA") + "%")
            }

            if(params."sCriteria_UANG_MUKA_PPN"){
                ilike("UANG_MUKA_PPN","%" + (params."sCriteria_UANG_MUKA_PPN" as String) + "%")
            }

            if(params."sCriteria_UANG_MUKA_PPNBM"){
                ilike("UANG_MUKA_PPNBM","%" + (params."sCriteria_UANG_MUKA_PPNBM" as String) + "%")
            }

            if(params."sCriteria_REFERENSI"){
                ilike("REFERENSI","%" + (params."sCriteria_REFERENSI" as String) + "%")
            }

            if(params."sCriteria_LT"){
                ilike("LT","%" + (params."sCriteria_LT" as String) + "%")
            }

            if(params."sCriteria_NPWP2"){
                ilike("NPWP2","%" + (params."sCriteria_NPWP2" as String) + "%")
            }

            if(params."sCriteria_NAMA2"){
                ilike("NAMA2","%" + (params."sCriteria_NAMA2" as String) + "%")
            }

            if(params."sCriteria_JALAN"){
                ilike("JALAN","%" + (params."sCriteria_JALAN" as String) + "%")
            }

            if(params."sCriteria_BLOK"){
                ilike("BLOK","%" + (params."sCriteria_BLOK" as String) + "%")
            }

            if(params."sCriteria_NOMOR"){
                ilike("NOMOR","%" + (params."sCriteria_NOMOR" as String) + "%")
            }

            if(params."sCriteria_RT"){
                ilike("RT","%" + (params."sCriteria_RT" as String) + "%")
            }

            if(params."sCriteria_RW"){
                ilike("RW","%" + (params."sCriteria_RW" as String) + "%")
            }

            if(params."sCriteria_KECAMATAN"){
                ilike("KECAMATAN","%" + (params."sCriteria_KECAMATAN" as String) + "%")
            }

            if(params."sCriteria_KELURAHAN"){
                ilike("KELURAHAN","%" + (params."sCriteria_KELURAHAN" as String) + "%")
            }

            if(params."sCriteria_KELURAHAN"){
                ilike("KELURAHAN","%" + (params."sCriteria_KELURAHAN" as String) + "%")
            }

            if(params."sCriteria_KABUPATEN"){
                ilike("KABUPATEN","%" + (params."sCriteria_KABUPATEN" as String) + "%")
            }

            if(params."sCriteria_PROPINSI"){
                ilike("PROPINSI","%" + (params."sCriteria_PROPINSI" as String) + "%")
            }

            if(params."sCriteria_KODE_POS"){
                ilike("KODE_POS","%" + (params."sCriteria_KODE_POS" as String) + "%")
            }

            if(params."sCriteria_NOMOR_TELEPON"){
                ilike("NOMOR_TELEPON","%" + (params."sCriteria_NOMOR_TELEPON" as String) + "%")
            }


            if(params."sCriteria_OF_"){
                ilike("OF_","%" + (params."sCriteria_OF_" as String) + "%")
            }

            if(params."sCriteria_KODE_OBJEK"){
                ilike("KODE_OBJEK","%" + (params."sCriteria_KODE_OBJEK" as String) + "%")
            }

            if(params."sCriteria_NAMA"){
                ilike("NAMA","%" + (params."sCriteria_NAMA" as String) + "%")
            }

            if(params."sCriteria_HARGA_SATUAN"){
                ilike("HARGA_SATUAN","%" + (params."sCriteria_HARGA_SATUAN" as String) + "%")
            }

            if(params."sCriteria_JUMLAH_BARANG"){
                ilike("JUMLAH_BARANG","%" + (params."sCriteria_JUMLAH_BARANG" as String) + "%")
            }

            if(params."sCriteria_HARGA_TOTAL"){
                ilike("HARGA_TOTAL","%" + (params."sCriteria_HARGA_TOTAL" as String) + "%")
            }


            if(params."sCriteria_DISKON"){
                ilike("DISKON","%" + (params."sCriteria_DISKON" as String) + "%")
            }

            if(params."sCriteria_DPP"){
                ilike("DPP","%" + (params."sCriteria_DPP" as String) + "%")
            }

            if(params."sCriteria_PPN"){
                ilike("PPN","%" + (params."sCriteria_PPN" as String) + "%")
            }

            if(params."sCriteria_TARIF_PPNBM"){
                ilike("TARIF_PPNBM","%" + (params."sCriteria_TARIF_PPNBM" as String) + "%")
            }

            if(params."sCriteria_PPNBM"){
                ilike("PPNBM","%" + (params."sCriteria_PPNBM" as String) + "%")
            }

            if(params."sCriteria_docNumberr"){
                ilike("docNumber","%" + (params."sCriteria_docNumberr" as String) + "%")
            }

        }

        def rowss = []
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        def spc = " "
        def nol = 0
        results.each {
            rowss << [

                    id: it?.id,

                    FK : it?.FK ? it?.FK:"-",

                    KD_JENIS_TRANSAKSI : ((it?.KD_JENIS_TRANSAKSI == "" || it?.KD_JENIS_TRANSAKSI == null || it?.KD_JENIS_TRANSAKSI == "null")? spc : it.KD_JENIS_TRANSAKSI) ,

                    FG_PENGGANTI : ((it?.FG_PENGGANTI == "" || it?.FG_PENGGANTI == null || it?.FG_PENGGANTI == "null")? spc : it.FG_PENGGANTI),

                    NOMOR_FAKTUR : ((it?.NOMOR_FAKTUR == "" || it?.NOMOR_FAKTUR == null || it?.NOMOR_FAKTUR == "null")? spc : it.NOMOR_FAKTUR),

                    MASA_PAJAK : ((it?.MASA_PAJAK == "" || it?.MASA_PAJAK == null || it?.MASA_PAJAK == "null")? spc : it.MASA_PAJAK),

                    TAHUN_PAJAK : ((it?.TAHUN_PAJAK == "" || it?.TAHUN_PAJAK == null || it?.TAHUN_PAJAK == "null")? spc : it.TAHUN_PAJAK),

                    TANGGAL_FAKTUR : it?.TANGGAL_FAKTUR?DATE_FORMAT.format(it?.TANGGAL_FAKTUR): spc,
//                    TANGGAL_FAKTUR : new Date().parse("dd-MMM-yy"),

                    NPWP : ((it?.NPWP == "" || it?.NPWP == null || it?.NPWP == "null")? spc : it.NPWP),

                    NAMA_OBJEK : ((it?.NAMA_OBJEK == "" || it?.NAMA_OBJEK == null || it?.NAMA_OBJEK == "null")? spc : it.NAMA_OBJEK),

                    ALAMAT_LENGKAP : ((it?.ALAMAT_LENGKAP == "" || it?.ALAMAT_LENGKAP == null || it?.ALAMAT_LENGKAP == "null")? spc : it.ALAMAT_LENGKAP),

                    JUMLAH_DPP : ((it?.JUMLAH_DPP == "" || it?.JUMLAH_DPP == null || it?.JUMLAH_DPP == "null")? nol : it.JUMLAH_DPP),

                    JUMLAH_PPN : ((it?.JUMLAH_PPN == "" || it?.JUMLAH_PPN == null || it?.JUMLAH_PPN == "null")? nol : it.JUMLAH_PPN),

                    JUMLAH_PPNBM : ((it?.JUMLAH_PPNBM == "" || it?.JUMLAH_PPNBM == null || it?.JUMLAH_PPNBM == "null")? nol : it.JUMLAH_PPNBM),

                    ID_KETERANGAN_TAMBAHAN : ((it?.ID_KETERANGAN_TAMBAHAN == "" || it?.ID_KETERANGAN_TAMBAHAN == null || it?.ID_KETERANGAN_TAMBAHAN == "null")? spc : it.ID_KETERANGAN_TAMBAHAN),

                    FG_UANG_MUKA : ((it?.FG_UANG_MUKA == "" || it?.FG_UANG_MUKA == null || it?.FG_UANG_MUKA == "null")? nol : it.FG_UANG_MUKA),

                    UANG_MUKA_DPP : ((it?.UANG_MUKA_DPP == "" || it?.UANG_MUKA_DPP == null || it?.UANG_MUKA_DPP == "null")? nol : it.UANG_MUKA_DPP),

                    UANG_MUKA_PPN : ((it?.UANG_MUKA_PPN == "" || it?.UANG_MUKA_PPN == null || it?.UANG_MUKA_PPN == "null")? nol : it.UANG_MUKA_PPN),

                    UANG_MUKA_PPNBM : ((it?.UANG_MUKA_PPNBM == "" || it?.UANG_MUKA_PPNBM == null || it?.UANG_MUKA_PPNBM == "null")? nol : it.UANG_MUKA_PPNBM),

                    REFERENSI : ((it?.REFERENSI == "" || it?.REFERENSI == null || it?.REFERENSI == "null")? spc : it.REFERENSI),

                    LT : ((it?.LT == "" || it?.LT == null || it?.LT == "null")? spc : it.LT),

                    NPWP2 : ((it?.NPWP2 == "" || it?.NPWP2 == null || it?.NPWP2 == "null")? spc : it.NPWP2),

                    NAMA2 : ((it?.NAMA2 == "" || it?.NAMA2 == null || it?.NAMA2 == "null")? spc : it.NAMA2),

                    JALAN : ((it?.JALAN == "" || it?.JALAN == null || it?.JALAN == "null")? spc : it.JALAN),

                    BLOK : ((it?.BLOK == "" || it?.BLOK == null || it?.BLOK == "null")? spc : it.BLOK),

                    NOMOR : ((it?.NOMOR == "" || it?.NOMOR == null || it?.NOMOR == "null")? spc : it.NOMOR),

                    RT : ((it?.RT == "" || it?.RT == null || it?.RT == "null")? spc : it.RT),

                    RW : ((it?.RW == "" || it?.RW == null || it?.RW == "null")? spc : it.RW),

                    KECAMATAN : ((it?.KECAMATAN == "" || it?.KECAMATAN == null || it?.KECAMATAN == "null")? spc : it.KECAMATAN),

                    KELURAHAN : ((it?.KELURAHAN == "" || it?.KELURAHAN == null || it?.KELURAHAN == "null")? spc : it.KELURAHAN),

                    KABUPATEN : ((it?.KABUPATEN == "" || it?.KABUPATEN == null || it?.KABUPATEN == "null")? spc : it.KABUPATEN),

                    PROPINSI : ((it?.PROPINSI == "" || it?.PROPINSI == null || it?.PROPINSI == "null")? spc : it.PROPINSI),

                    KODE_POS : ((it?.KODE_POS == "" || it?.KODE_POS == null || it?.KODE_POS == "null")? spc : it.KODE_POS),

                    NOMOR_TELEPON : ((it?.NOMOR_TELEPON == "" || it?.NOMOR_TELEPON == null || it?.NOMOR_TELEPON == "null")? spc : it.NOMOR_TELEPON),

                    OF_ : ((it?.OF_ == "" || it?.OF_ == null || it?.OF_ == "null")? spc : it.OF_),

                    KODE_OBJEK : ((it?.KODE_OBJEK == "" || it?.KODE_OBJEK == null || it?.KODE_OBJEK == "null")? spc : it.KODE_OBJEK),

                    NAMA : ((it?.NAMA == "" || it?.NAMA == null || it?.NAMA == "null")? spc : it.NAMA),

                    HARGA_SATUAN : ((it?.HARGA_SATUAN == "" || it?.HARGA_SATUAN == null || it?.HARGA_SATUAN == "null")? nol : it.HARGA_SATUAN),

                    JUMLAH_BARANG : ((it?.JUMLAH_BARANG == "" || it?.JUMLAH_BARANG == null || it?.JUMLAH_BARANG == "null")? nol : it.JUMLAH_BARANG),

                    HARGA_TOTAL : ((it?.HARGA_TOTAL == "" || it?.HARGA_TOTAL == null || it?.HARGA_TOTAL == "null")? nol : it.HARGA_TOTAL),

                    DISKON : ((it?.DISKON == "" || it?.DISKON == null || it?.DISKON == "null")? nol : it.DISKON),

                    DPP : ((it?.DPP == "" || it?.DPP == null || it?.DPP == "null")? nol : it.DPP),

                    PPN : ((it?.PPN == "" || it?.PPN == null || it?.PPN == "null")? nol : it.PPN),

                    TARIF_PPNBM : ((it?.TARIF_PPNBM == "" || it?.TARIF_PPNBM == null || it?.TARIF_PPNBM == "null")? nol : it.TARIF_PPNBM),

                    PPNBM : ((it?.PPNBM == "" || it?.PPNBM == null || it?.PPNBM == "null")? nol : it.PPNBM),

                    docNumberr : it?.docNumber,

                    staAprove : it?.staAprove=="1"?"Approved":(it?.staAprove=="2"?"Rejected":"Wait")


            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rowss]

    }


    def datatablesList1(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c2 = EFaktur.createCriteria()
        def resultsKon = c2.list {
            eq("staDel","0")
            eq("jenisInputan","PPN_KELUARAN")
            eq("staOriRev","0")
            order("tanggal","DESC")
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
                eq("staAprove","1")
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal && params.sCriteria_Tanggal2){
                ge("tanggal",params.sCriteria_Tanggal)
                lt("tanggal",params.sCriteria_Tanggal2 + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                min("staAprove", "staAprove")
                max("tanggal", "tanggal")
                max("lastUpdProcess", "lastUpdProcess")
                max("companyDealer", "companyDealer")
                order("tanggal", "asc")
                order("staAprove", "asc")
                order("lastUpdProcess", "desc")
            }
        }



        def c = EFaktur.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("jenisInputan","PPN_KELUARAN")
            eq("staOriRev","0")
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
                eq("staAprove","1")
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal && params.sCriteria_Tanggal2){
                ge("tanggal",params.sCriteria_Tanggal)
                lt("tanggal",params.sCriteria_Tanggal2 + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                min("staAprove", "staAprove")
                max("tanggal", "tanggal")
                min("lastUpdProcess", "lastUpdProcess")
                max("companyDealer", "companyDealer")
                order("tanggal", "asc")
                order("staAprove", "asc")
                if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                    order("lastUpdProcess", "desc")
                }
            }
        }

        def rows = []
        results.each {
            rows << [

                    id: it?.id,

                    companyDealer: it?.companyDealer?.m011NamaWorkshop,

                    staPrint: it?.lastUpdProcess,

                    docNumber: it?.docNumber ? it?.docNumber:"-",

                    tanggal: it?.tanggal.format("dd/MM/yyyy"),

                    staAprove : (it.staAprove as String)=="1"?"APPROVED":((it.staAprove as String)=="2"?"REJECTED":"WAITING FOR APPROVAL"),

                    staAproveId : (it.staAprove as String)

            ]
        }
        [sEcho: params.sEcho, iTotalRecords: resultsKon.size(), iTotalDisplayRecords: resultsKon.size(), aaData: rows]

    }

    def printEfaktur(def params){
        List<JasperReportDef> reportDefList = []
        def reportData = printEfakturDetail(params)

        def reportDef = new JasperReportDef(name:'reportEfaktur.jasper',
                fileFormat:JasperExportFormat.CSV_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)
        return reportDefList
    }

    def printEfakturDetail(def params){
        def docNumber = params.docNumber
        def reportData = new ArrayList();
        def efaktur = EFaktur.createCriteria().list {
            eq("docNumber",docNumber)
            eq("staDel","0")
            eq("jenisInputan","PPN_KELUARAN")
            eq("staAprove","1")
        }

        efaktur.each {
            def data = [:]
            data.put("COLUMN","FK")
            data.put("COLUMN_1","KD_JENIS_TRANSAKSI")
            data.put("COLUMN_2","FG_PENGGANTI")
            data.put("COLUMN_3","NOMOR_FAKTUR")
            data.put("COLUMN_4","MASA_PAJAK")
            data.put("COLUMN_5","TAHUN_PAJAK")
            data.put("COLUMN_6","TANGGAL_FAKTUR")
            data.put("COLUMN_7","NPWP")
            data.put("COLUMN_8","NAMA")
            data.put("COLUMN_9","ALAMAT_LENGKAP")
            data.put("COLUMN_10","JUMLAH_DPP")
            data.put("COLUMN_11","JUMLAH_PPN")
            data.put("COLUMN_12","JUMLAH_PPNBM")
            data.put("COLUMN_13","ID_KETERANGAN_TAMBAHAN")
            data.put("COLUMN_14","FG_UANG_MUKA")
            data.put("COLUMN_15","UANG_MUKA_DPP")
            data.put("COLUMN_16","UANG_MUKA_PPN")
            data.put("COLUMN_17","UANG_MUKA_PPNBM")
            data.put("COLUMN_18","REFERENSI")
            data.put("COLUMN_19","LT")
            data.put("COLUMN_20","NPWP2") // MENUNJUKAN NPWP2
            data.put("COLUMN_21","NAMA2") // MENUNJUKKAN NAMA2
            data.put("COLUMN_22","JALAN")
            data.put("COLUMN_23","BLOK")
            data.put("COLUMN_24","NOMOR")
            data.put("COLUMN_25","RT")
            data.put("COLUMN_26","RW")
            data.put("COLUMN_27","KECAMATAN")
            data.put("COLUMN_28","KELURAHAN")
            data.put("COLUMN_29","KABUPATEN")
            data.put("COLUMN_30","PROPINSI")
            data.put("COLUMN_31","KODE_POS")
            data.put("COLUMN_32","NOMOR_TELEPON")
            data.put("COLUMN_33","OF")
            data.put("COLUMN_34","KODE_OBJEK")
            data.put("COLUMN_35","NAMA_OBJEK")
            data.put("COLUMN_36","HARGA_SATUAN")
            data.put("COLUMN_37","JUMLAH_BARANG")
            data.put("COLUMN_38","HARGA_TOTAL")
            data.put("COLUMN_39","DISKON")
            data.put("COLUMN_40","DPP")
            data.put("COLUMN_41","PPN")
            data.put("COLUMN_42","TARIF_PPNBM")
            data.put("COLUMN_43","PPNBM")

            data.put("F_COLUMN",it.getFK())
            data.put("F_COLUMN_1",it.getKD_JENIS_TRANSAKSI())
            data.put("F_COLUMN_2",it.getFG_PENGGANTI())
            data.put("F_COLUMN_3",it.getNOMOR_FAKTUR())
            data.put("F_COLUMN_4",it.getMASA_PAJAK())
            data.put("F_COLUMN_5",it.getTAHUN_PAJAK())
            data.put("F_COLUMN_6",it.getTANGGAL_FAKTUR())
            data.put("F_COLUMN_7",it.getNPWP())
            data.put("F_COLUMN_8",it.getNAMA())
            data.put("F_COLUMN_9",it.getNAMA())
            data.put("F_COLUMN_10",it.getJUMLAH_DPP())
            data.put("F_COLUMN_11",it.getJUMLAH_PPN())
            data.put("F_COLUMN_12",it.getJUMLAH_PPNBM())
            data.put("F_COLUMN_13",it.getID_KETERANGAN_TAMBAHAN())
            data.put("F_COLUMN_14",it.getFG_UANG_MUKA())
            data.put("F_COLUMN_15",it.getUANG_MUKA_DPP())
            data.put("F_COLUMN_16",it.getUANG_MUKA_PPN())
            data.put("F_COLUMN_17",it.getUANG_MUKA_PPNBM())
            data.put("F_COLUMN_18",it.getREFERENSI())
            data.put("F_COLUMN_19",it.getLT())
            data.put("F_COLUMN_20",it.getNPWP2())
            data.put("F_COLUMN_21",it.getJALAN())
            data.put("F_COLUMN_22",it.getNAMA2())
            data.put("F_COLUMN_23",it.getBLOK())
            data.put("F_COLUMN_24",it.getNOMOR())
            data.put("F_COLUMN_25",it.getRT())
            data.put("F_COLUMN_26",it.getRW())
            data.put("F_COLUMN_27",it.getKECAMATAN())
            data.put("F_COLUMN_28",it.getKELURAHAN())
            data.put("F_COLUMN_29",it.getKABUPATEN())
            data.put("F_COLUMN_30",it.getPROPINSI())
            data.put("F_COLUMN_31",it.getKODE_POS())
            data.put("F_COLUMN_32",it.getNOMOR_TELEPON())
            data.put("F_COLUMN_33",it.getOF_())
            data.put("F_COLUMN_34",it.getKODE_OBJEK())
            data.put("F_COLUMN_35",it.getNAMA_OBJEK())
            data.put("F_COLUMN_36",it.getHARGA_SATUAN())
            data.put("F_COLUMN_37",it.getJUMLAH_BARANG())
            data.put("F_COLUMN_38",it.getHARGA_TOTAL())
            def diskonVal = it?.getDISKON()
            if (diskonVal==null){
                diskonVal=0
            }
//            data.put("F_COLUMN_39",it.getDISKON())
            data.put("F_COLUMN_39",diskonVal)
            def dPP = it?.getDPP()
            def pPN = it?.getPPN()
            def tARIFPPNBM = it?.getTARIF_PPNBM()
            if (dPP==null){
                dPP=0
            }

            if (pPN==null){
                pPN=0
            }
            if (tARIFPPNBM==null){
                tARIFPPNBM=0
            }

            data.put("F_COLUMN_40",dPP)
            data.put("F_COLUMN_41",pPN)
            data.put("F_COLUMN_42",tARIFPPNBM)
            def pPNBM = it?.getPPNBM()
            if (pPNBM==null){
                pPNBM=0
            }
            data.put("F_COLUMN_43",pPNBM)
//            data.put("F_COLUMN_43",it.getPPNBM())
            reportData.add(data)
        }

        return reportData
    }
}
