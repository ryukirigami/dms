package com.kombos.maintable



import com.kombos.baseapp.AppSettingParam

class MasterRangeDpService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = MasterRangeDp.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_nilaiParts1") {
                eq("nilaiParts1", params."sCriteria_nilaiParts1")
            }

            if (params."sCriteria_nilaiParts2") {
                eq("nilaiParts2", params."sCriteria_nilaiParts2")
            }

            if (params."sCriteria_persen") {
                eq("persen", params."sCriteria_persen")
            }



            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    nilaiParts1: it.nilaiParts1 + " - " + it.nilaiParts2 + " Juta",

                    persen: it.persen + "%",

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MasterRangeDp", params.id]]
            return result
        }

        result.masterRangeDpInstance = MasterRangeDp.get(params.id)

        if (!result.masterRangeDpInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MasterRangeDp", params.id]]
            return result
        }

        result.masterRangeDpInstance = MasterRangeDp.get(params.id)

        if (!result.masterRangeDpInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MasterRangeDp", params.id]]
            return result
        }

        result.masterRangeDpInstance = MasterRangeDp.get(params.id)

        if (!result.masterRangeDpInstance)
            return fail(code: "default.not.found.message")

        try {
            result.masterRangeDpInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        MasterRangeDp.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.masterRangeDpInstance && m.field)
                    result.masterRangeDpInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["MasterRangeDp", params.id]]
                return result
            }

            result.masterRangeDpInstance = MasterRangeDp.get(params.id)

            if (!result.masterRangeDpInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.masterRangeDpInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.masterRangeDpInstance.properties = params
            result.masterRangeDpInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.masterRangeDpInstance.lastUpdProcess = 'UPDATE'
            if (result.masterRangeDpInstance.hasErrors() || !result.masterRangeDpInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MasterRangeDp", params.id]]
            return result
        }

        result.masterRangeDpInstance = new MasterRangeDp()
        result.masterRangeDpInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.masterRangeDpInstance && m.field)
                result.masterRangeDpInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["MasterRangeDp", params.id]]
            return result
        }

        result.masterRangeDpInstance = new MasterRangeDp(params)
        result.masterRangeDpInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.masterRangeDpInstance.lastUpdProcess = 'INSERT'

        if (result.masterRangeDpInstance.hasErrors() || !result.masterRangeDpInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def masterRangeDp = MasterRangeDp.findById(params.id)
        if (masterRangeDp) {
            masterRangeDp."${params.name}" = params.value
            masterRangeDp.save()
            if (masterRangeDp.hasErrors()) {
                throw new Exception("${masterRangeDp.errors}")
            }
        } else {
            throw new Exception("MasterRangeDp not found")
        }
    }

}