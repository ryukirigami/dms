package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

class WoTransactionService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c2 = WoTransaction.createCriteria()
        def resultsCount = c2.list {
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal){
                ge("tglUpload",params.sCriteria_Tanggal)
                lt("tglUpload",params.sCriteria_Tanggal + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                max("tglUpload", "tglUpload")
                max("staDel", "staDel")
                max("fileName", "fileName")
                max("companyDealer", "companyDealer")
            }
            order("tglUpload", "desc")
            order("companyDealer", "desc")

        }
        def c = WoTransaction.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal){
                ge("tglUpload",params.sCriteria_Tanggal)
                lt("tglUpload",params.sCriteria_Tanggal + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                max("tglUpload", "tglUpload")
                max("staDel", "staDel")
                max("fileName", "fileName")
                max("companyDealer", "companyDealer")
            }
            order("staDel", "asc")
            switch (sortProperty) {
                case "dateCreated" :
                    order("tglUpload", sortDir)
                    break
                case "companyDealer" :
                    order("companyDealer", sortDir)
                    break
                default:
                    order("companyDealer", sortDir)
                    break;
            }

        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it?.companyDealer.m011NamaWorkshop,

                    docNumber: it.docNumber + " / "+ it.fileName,

                    docNumberId: it.docNumber,

                    dateCreated: it.tglUpload.format("dd/MM/yyyy"),

                    staDel: it.staDel,

            ]
        }
        def cd = []
        try {
            def cdDefault = ["CV. KOMBOS JAKARTA", "HEAD OFFICE (HO) MANADO"]
            if(results.size()==0){
                cd = CompanyDealer.findAllByM011NamaWorkshopNotInList(cdDefault).m011NamaWorkshop.toString()
            }else{
                cd = CompanyDealer.findAllByM011NamaWorkshopNotInListAndM011NamaWorkshopNotInList(rows.companyDealer,cdDefault).m011NamaWorkshop.toString()
            }
        }catch(Exception e){

        }
        [sEcho: params.sEcho, iTotalRecords: resultsCount.size(), iTotalDisplayRecords: resultsCount.size(), aaData: rows, cd : cd ]

    }

    def printWoTrxDetail(def params){

        def jsonArray = JSON.parse(params.docNumber)
        def reportData = new ArrayList();
        jsonArray.each {
            def wo = WoTransaction.findAllByDocNumberAndStaDel(it,'0')
            wo.each {
                def data = [:]
                data.put("DEALER_CODE",it.dealerCode.toString())
                data.put("AREA_CODE",it.areaCode.toString())
                data.put("OUTLET_CODE",it.outletCode.toString())
                data.put("NOWO",it.noWo.toString().replace(',','#'))
                data.put("TGLWO",it.tglWo.toString().replace('-','/'))
                data.put("NOPOL",it.noPol.toString().replace(' ',''))
                data.put("NO_VEHICLE",it.noVehicle.toString().replace(',','#'))
                data.put("TIPE_VEHICLE",it.typeVehicle.toString().replace(',','#'))
                data.put("IS_EM",it.isEm.toString())
                data.put("TIPE_WO",it.woType.toString().replace(',','#'))
                data.put("OPERATION_DESC",it.operation.toString().replace(',','#'))
                data.put("OPERATION_TYPE",it.operationType.toString())
                data.put("FLAT_RATE",it.flatRate.toString().replace(',','#'))
                data.put("WARRANTY",it.warranty.toString())
                data.put("FLAG_COLOR","BLACK")
                reportData.add(data)
            }
        }

        return reportData
    }
}