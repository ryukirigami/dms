package com.kombos.maintable



import com.kombos.baseapp.AppSettingParam

class HistoryApprovalService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = HistoryApproval.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_kegiatan"){
				approval{
					kegiatanApproval {
						ilike("m770KegiatanApproval","%" + (params."sCriteria_kegiatan" as String) + "%")
						
					}
				}
				
			}

			if(params."sCriteria_noDokumen"){
				approval{
					eq("t770NoDokumen",params."sCriteria_noDokumen")
				}
			}

			if(params."sCriteria_namaApproved"){
				ilike("namaApproved","%" + (params."sCriteria_namaApproved" as String) + "%")
			}

			if(params."sCriteria_status"){
				eq("status",params."sCriteria_status")
			}

			if(params."sCriteria_tglJamApproved"){
				ge("tglJamApproved",params."sCriteria_tglJamApproved")
				lt("tglJamApproved",params."sCriteria_tglJamApproved" + 1)
			}

			if(params."sCriteria_tglPermohonan"){
				approval{
					ge("t770TglJamSend",params."sCriteria_tglPermohonan")
					lt("t770TglJamSend",params."sCriteria_tglPermohonan" + 1)
				}
			}
			
			switch(sortProperty){
				case 'kegiatan':
					approval{
						kegiatanApproval {
							order("m770KegiatanApproval",sortDir)
						}
					}
					break;
				case 'noDokumen':
					approval{
						order("t770NoDokumen",sortDir)
					}
					break;
				case 'tglPermohonan':
					approval{
						order("t770TglJamSend",sortDir)
					}
					break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						kegiatan: it.approval?.kegiatanApproval?.m770KegiatanApproval,
			
						noDokumen: it.approval?.t770NoDokumen,
			
						namaApproved: it.namaApproved,
			
						status: it?.status?.key ? it?.status?.key:"-",
			
						tglJamApproved: it.tglJamApproved?it.tglJamApproved.format(dateFormat):"",
						
						tglPermohonan: it.approval?.t770TglJamSend?it.approval?.t770TglJamSend.format(dateFormat):"",
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryApproval", params.id] ]
			return result
		}

		result.historyApprovalInstance = HistoryApproval.get(params.id)

		if(!result.historyApprovalInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryApproval", params.id] ]
			return result
		}

		result.historyApprovalInstance = HistoryApproval.get(params.id)

		if(!result.historyApprovalInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryApproval", params.id] ]
			return result
		}

		result.historyApprovalInstance = HistoryApproval.get(params.id)

		if(!result.historyApprovalInstance)
			return fail(code:"default.not.found.message")

		try {
			result.historyApprovalInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		HistoryApproval.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.historyApprovalInstance && m.field)
					result.historyApprovalInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["HistoryApproval", params.id] ]
				return result
			}

			result.historyApprovalInstance = HistoryApproval.get(params.id)

			if(!result.historyApprovalInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.historyApprovalInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.historyApprovalInstance.properties = params

			if(result.historyApprovalInstance.hasErrors() || !result.historyApprovalInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryApproval", params.id] ]
			return result
		}

		result.historyApprovalInstance = new HistoryApproval()
		result.historyApprovalInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.historyApprovalInstance && m.field)
				result.historyApprovalInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["HistoryApproval", params.id] ]
			return result
		}

		result.historyApprovalInstance = new HistoryApproval(params)

		if(result.historyApprovalInstance.hasErrors() || !result.historyApprovalInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def historyApproval =  HistoryApproval.findById(params.id)
		if (historyApproval) {
			historyApproval."${params.name}" = params.value
			historyApproval.save()
			if (historyApproval.hasErrors()) {
				throw new Exception("${historyApproval.errors}")
			}
		}else{
			throw new Exception("HistoryApproval not found")
		}
	}

}