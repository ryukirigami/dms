package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

class PartStockTransactionService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c2 = PartStockTransaction.createCriteria()
        def resultsCount = c2.list {
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal && params.sCriteria_Tanggal2){
                ge("tglUpload",params.sCriteria_Tanggal)
                lt("tglUpload",params.sCriteria_Tanggal2 + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                max("tglUpload", "tglUpload")
                max("fileName", "fileName")
                max("companyDealer", "companyDealer")
            }
            order("tglUpload", "desc")
            order("companyDealer", "desc")

        }
        def c = PartStockTransaction.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal && params.sCriteria_Tanggal2){
                ge("tglUpload",params.sCriteria_Tanggal)
                lt("tglUpload",params.sCriteria_Tanggal2 + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                max("tglUpload", "tglUpload")
                max("fileName", "fileName")
                max("companyDealer", "companyDealer")
            }
            order("tglUpload", "desc")
            order("companyDealer", "desc")

        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it?.companyDealer.m011NamaWorkshop,

                    docNumber: it.docNumber + " / "+ it.fileName,

                    docNumberId: it.docNumber,

                    dateCreated: it.tglUpload.format("dd/MM/yyyy HH:mm"),


            ]
        }
        def cd = []
        try {
            if(results.size()==0){
                cd = CompanyDealer.findAll().m011NamaWorkshop.toString()
            }else{
                cd = CompanyDealer.findAllByM011NamaWorkshopNotInList(rows.companyDealer).m011NamaWorkshop.toString()
            }
        }catch(Exception e){

        }
        [sEcho: params.sEcho, iTotalRecords: resultsCount.size(), iTotalDisplayRecords: resultsCount.size(), aaData: rows, cd : cd ]

    }

    def printPartStockTrxDetail(def params){

        def jsonArray = JSON.parse(params.docNumber)
        def reportData = new ArrayList();
        jsonArray.each {
            def wo = PartStockTransaction.findAllByDocNumberAndStaDel(it,'0')
            wo.each {
                def data = [:]
                data.put("DEALER_CODE",it.dealerCode.toString())
                data.put("AREA_CODE",it.areaCode.toString())
                data.put("OUTLET_CODE",it.outletCode.toString())
                data.put("PART_NOMOR",it.partNo.toString())
                data.put("PART_DESC",it.partDesc.toString())
                data.put("STOCK",it.stock.toString())
                data.put("MAD_UPDATE",it.madUpdate.toString())
                data.put("FLAG_COLOR","BLACK")
                reportData.add(data)
            }
        }

        return reportData
    }
}