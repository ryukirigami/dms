package com.kombos.maintable

import com.kombos.administrasi.KabKota
import com.kombos.baseapp.AppSettingParam

class NoEfakturService {

    boolean transactional = false

    def datatablesUtilService

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NoEfaktur.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params."sCriteria_suffix"){
                ilike("suffix","%" + (params."sCriteria_suffix" as String) + "%")
            }

            if(params."sCriteria_prefix"){
                ilike("prefix","%" + (params."sCriteria_prefix" as String) + "%")
            }

            if (params."sCriteria_noAwal"){
                eq("noAwal",params."sCriteria_noAwal".toBigDecimal())
            }

            if (params."sCriteria_noAkhir"){
                eq("noAkhir",params."sCriteria_noAkhir".toBigDecimal())
            }


            if (params."sCriteria_tglPermohonanPajak"){
                ge("tglPermohonanPajak",params."sCriteria_tglPermohonanPajak")
                lt("tglPermohonanPajak",params."sCriteria_tglPermohonanPajak" + 1)
            }
            if (params."sCriteria_tglAkhirPajak"){
                ge("tglAkhirPajak",params."sCriteria_tglAkhirPajak")
                lt("tglAkhirPajak",params."sCriteria_tglAkhirPajak" + 1)
            }


            if (params."sCriteria_jumlah"){
                eq("jumlah",params."sCriteria_jumlah".toBigDecimal())
            }

            if (params."sCriteria_sisa"){
                eq("sisa",params."sCriteria_sisa".toBigDecimal())
            }


        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    suffix:it?.suffix,

                    prefix: it?.prefix,

                    noAwal: it?.noAwal,

                    noAkhir: it?.noAkhir ,

                    tglPermohonanPajak : it?.tglPermohonanPajak.format("yyyy-MM-dd"),

                    tglAkhirPajak : it?.tglAkhirPajak.format("yyyy-MM-dd"),

                    jumlah: it?.jumlah,

                    sisa: it?.sisa

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["noEfakturInstance", params.id]]
            return result
        }

        result.noEfakturInstance = new NoEfaktur()
        result.noEfakturInstance.properties = params

        // success
        return result
    }

//    ukhti
    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.noEfakturInstance && m.field)
                result.noEfakturInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["noEfakturInstance", params.id]]
            return result
        }

        result.noEfakturInstance = new NoEfaktur(params)
        result.noEfakturInstance.staDel = '0'
        result.noEfakturInstance.terakhirPakai = ((params.noAwal as int )-1).toBigDecimal()
        result.noEfakturInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.noEfakturInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.noEfakturInstance.lastUpdProcess = "INSERT"
        result.noEfakturInstance.statusHabis = '0'
        result.noEfakturInstance.dateCreated = datatablesUtilService?.syncTime()
        result.noEfakturInstance.lastUpdated = datatablesUtilService?.syncTime()

        if (result.noEfakturInstance.hasErrors() || !result.noEfakturInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["noEfakturInstance", params.id]]
            return result
        }

        result.noEfakturInstance = NoEfaktur.get(params.id)

        if (!result.noEfakturInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MasterEfaktur", params.id]]
            return result
        }

        result.masterEfakturInstance = MasterEfaktur.get(params.id)

        if (!result.masterEfakturInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MasterEfaktur", params.id]]
            return result
        }

        result.masterEfakturInstance = MasterEfaktur.get(params.id)

        if (!result.masterEfakturInstance)
            return fail(code: "default.not.found.message")

        try {
            result.masterEfakturInstance.staDel = '1'
            result.masterEfakturInstance.lastUpdProcess = 'DELETE'
            result.masterEfakturInstance.save(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        //  KabKota.withTransaction { status ->
        def result = [:]

        def fail = { Map m ->
            //  status.setRollbackOnly()
            if (result.noEfakturInstance && m.field)
                result.noEfakturInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["noEfakturInstance", params.id]]
            return result
        }

        result.noEfakturInstance = NoEfaktur.get(params.id)
        def cek = NoEfaktur.createCriteria()

        if (!result.noEfakturInstance)
            return fail(code: "default.not.found.message")

        // Optimistic locking check.
        if (params.version) {
            if (result.noEfakturInstance.version > params.version.toLong())
                return fail(field: "version", code: "default.optimistic.locking.failure")
        }

        result.noEfakturInstance.properties = params
        result.noEfakturInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.noEfakturInstance.lastUpdProcess = "UPDATE"
        result.noEfakturInstance.lastUpdated = datatablesUtilService?.syncTime()


        if (result.noEfakturInstance.hasErrors() || !result.noEfakturInstance.save())
            return fail(code: "default.not.updated.message")

        // Success.
        return result

        //     } //end withTransaction
    }  // end update()



}
