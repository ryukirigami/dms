package com.kombos.maintable

import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import org.activiti.engine.impl.pvm.delegate.ActivityExecution

class PengirimanInvitationSecaraOtomatisService {

    def invitationService
    def datatablesUtilService

    def invitationScheduler(ActivityExecution execution) throws Exception {
        def variables = execution.getVariables()
        log.info("invitationScheduler " + execution.getVariables())
        executeAll()
    }

    def executeAll() {
        try{
             batchProcessMenggunakanEngineSMS()

        }catch (e){}
        try{
//            batchProcessMenggunakanEngineEmail()
        }catch (e){}
        try{
//            println "batchProcessSMSUlangTahun"
            batchProcessSMSUlangTahun()
        }catch (e){}
        try{
//            batchProcessSMSSTNKJatuhTempo()
        }catch (e){}
    }


    def batchProcessMenggunakanEngineSMS() {
//        println "batchProcessMenggunakanEngineSMS"
        Calendar calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        Date tglKirimSMSMulai = calendar.time
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1)
        Date tglKirimSMSSampai = calendar.time

        def invitations = Invitation.withCriteria {
            isNull("t202TglJamKirim")
            reminder {
                between("t201TglSMS", tglKirimSMSMulai, tglKirimSMSSampai)
            }
            jenisInvitation {
                ilike("m204JenisInvitation", "%SMS%")
            }
        }



        invitations.each { Invitation invitation ->
            String smsContent = invitationService.formatContentSMS(invitation)
            invitation.t202TglJamKirim = new Date()
            invitation.t202StaAction = "3"
            invitation.lastUpdProcess = "INSERT"
            invitation.createdBy = "SYSTEM"
            invitation.lastUpdated = datatablesUtilService?.syncTime()
            invitation.save(flush: true)
            invitation.errors.each {
                println it
            }

            invitationService.formatContentSMS(invitation)

        }
    }

    def batchProcessMenggunakanEngineEmail() {
        Calendar calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        Date tglKirimEmailMulai = calendar.time
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1)
        Date tglKirimEmailSampai = calendar.time



        def invitations = Invitation.withCriteria {
            isNull("t202TglJamKirim")
            reminder {
                between("t201TglEmail", tglKirimEmailMulai, tglKirimEmailSampai)
            }
            jenisInvitation {
                eq("m204JenisInvitation", "Email")
            }
        }



        invitations.each { invitation ->
            String smsContent = invitationService.formatContentEmail(invitation)
            invitation.t202TglJamKirim = new Date()
            invitation.t202StaAction = "2"
            invitation.lastUpdated = datatablesUtilService?.syncTime()
            invitation.save(flush: true)
            invitation.errors.each {
                println it
            }

            // Pengiriman Email belum tersedia

        }
    }


    def batchProcessSMSUlangTahun() {
        Calendar calendar = Calendar.getInstance()

        Integer bulan = calendar.get(Calendar.MONTH)
        Integer tanggal = calendar.get(Calendar.DATE)

        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        Date mulai = calendar.time
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1)
        Date selesai = calendar.time



        def historyCustomers = HistoryCustomer.withCriteria {
            eq("t182TglLahirMonth", bulan)
            eq("t182TglLahirDate", tanggal)
            or {
                isNull("terakhirTanggalKirimUlangTahun")
                not {
                    between("terakhirTanggalKirimUlangTahun", mulai, selesai)
                }
            }
        }
        historyCustomers.each { historyCustomer ->
            String smsContent = invitationService.formatContentSMSUlangTahun(historyCustomer)
            if (smsContent && !smsContent.empty) {
                historyCustomer.terakhirTanggalKirimUlangTahun = new Date()
                historyCustomer.lastUpdated = datatablesUtilService?.syncTime()
                historyCustomer.save(flush: true)
                // Pengiriman SMS belum tersedia

            }
        }

    }


    def batchProcessSMSSTNKJatuhTempo() {
        Calendar calendar = Calendar.getInstance()

        Integer bulan = calendar.get(Calendar.MONTH)
        Integer tanggal = calendar.get(Calendar.DATE)

        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        Date mulai = calendar.time
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1)
        Date selesai = calendar.time



        def histories = HistoryCustomerVehicle.withCriteria {
            eq("t183TglSTNKMonth", bulan)
            eq("t183TglSTNKDate", tanggal)
            or {
                isNull("terakhirKirimJatuhTempoTglSTNK")
                not {
                    between("terakhirKirimJatuhTempoTglSTNK", mulai, selesai)
                }
            }
        }

        histories.each { history ->
            String smsContent = invitationService.formatContentSMSSTNKJatuhTempo(history)
            if (smsContent && !smsContent.empty) {
                history.terakhirKirimJatuhTempoTglSTNK = new Date()
                history.lastUpdated = datatablesUtilService?.syncTime()
                history.save(flush: true)
                // Pengiriman SMS belum tersedia

            }

        }
    }
}
