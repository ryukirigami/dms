package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam

class NpwpEfakturService {

    boolean transactional = false

    def datatablesUtilService

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NpwpEfaktur.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params."sCriteria_namaPerusahaan"){
                ilike("namaPerusahaan","%" + (params."sCriteria_namaPerusahaan" as String) + "%")
            }
            if(params."sCriteria_npwp"){
                ilike("npwp","%" + (params."sCriteria_npwp" as String) + "%")
            }
            if(params."sCriteria_alamatNpwp"){
                ilike("alamatNpwp","%" + (params."sCriteria_alamatNpwp" as String) + "%")
            }
            if(params."sCriteria_penandaTangan"){
                ilike("penandaTangan","%" + (params."sCriteria_penandaTangan" as String) + "%")
            }
            if(params."sCriteria_jabatan"){
                ilike("jabatan","%" + (params."sCriteria_jabatan" as String) + "%")
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    namaPerusahaan: it?.namaPerusahaan ? it?.namaPerusahaan:"-",

                    npwp: it?.npwp ? it?.npwp:"-",

                    alamatNpwp: it?.alamatNpwp ? it?.alamatNpwp :"-",

                    penandaTangan:it?.penandaTangan ? it?.penandaTangan:"-",

                    jabatan:it?.jabatan ? it?.jabatan:"-"

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["npwpEfakturInstance", params.id]]
            return result
        }

        result.npwpEfakturInstance = new NpwpEfaktur()
        result.npwpEfakturInstance.properties = params

        // success
        return result
    }

//    ukhti
    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.npwpEfakturInstance && m.field)
                result.npwpEfakturInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["npwpEfakturInstance", params.id]]
            return result
        }

        result.npwpEfakturInstance = new NpwpEfaktur(params)
        result.npwpEfakturInstance.staDel = '0'
        result.npwpEfakturInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.npwpEfakturInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.npwpEfakturInstance.lastUpdProcess = "INSERT"
        result.npwpEfakturInstance.dateCreated = datatablesUtilService?.syncTime()
        result.npwpEfakturInstance.lastUpdated = datatablesUtilService?.syncTime()

        if (result.npwpEfakturInstance.hasErrors() || !result.npwpEfakturInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }


    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["npwpEfakturInstance", params.id]]
            return result
        }

        result.npwpEfakturInstance = NpwpEfaktur.get(params.id)

        if (!result.npwpEfakturInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["NpwpEfaktur", params.id]]
            return result
        }

        result.npwpEfakturInstance = NpwpEfaktur.get(params.id)

        if (!result.npwpEfakturInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["NpwpEfaktur", params.id]]
            return result
        }

        result.npwpEfakturInstance = NpwpEfaktur.get(params.id)

        if (!result.npwpEfakturInstance)
            return fail(code: "default.not.found.message")

        try {
            result.npwpEfakturInstance.staDel = '1'
            result.npwpEfakturInstance.lastUpdProcess = 'DELETE'
            result.npwpEfakturInstance.lastUpdated = datatablesUtilService?.syncTime()
            result.npwpEfakturInstance.save(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        //  KabKota.withTransaction { status ->
        def result = [:]

        def fail = { Map m ->
            //  status.setRollbackOnly()
            if (result.npwpEfakturInstance && m.field)
                result.npwpEfakturInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["NpwpEfaktur", params.id]]
            return result
        }

        result.npwpEfakturInstance = NpwpEfaktur.get(params.id)

        def cek = NpwpEfaktur.createCriteria()

        if (!result.npwpEfakturInstance)
            return fail(code: "default.not.found.message")

        // Optimistic locking check.
        if (params.version) {
            if (result.npwpEfakturInstance.version > params.version.toLong())
                return fail(field: "version", code: "default.optimistic.locking.failure")
        }

        result.npwpEfakturInstance.properties = params
        result.npwpEfakturInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.npwpEfakturInstance.lastUpdProcess = "UPDATE"
        result.npwpEfakturInstance.lastUpdated = datatablesUtilService?.syncTime()


        if (result.npwpEfakturInstance.hasErrors() || !result.npwpEfakturInstance.save())
            return fail(code: "default.not.updated.message")

        // Success.
        return result

        //     } //end withTransaction
    }  // end update()



}
