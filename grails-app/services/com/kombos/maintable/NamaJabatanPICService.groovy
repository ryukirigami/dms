package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam

class NamaJabatanPICService {

    def generateCodeService

    boolean transactional = false

    def datatablesList(def params) {
        def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = NamaJabatanPIC.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params."sCriteria_m093ID"){
                eq("m093ID", Integer.parseInt(params."sCriteria_m093ID"))
            }

            if(params."sCriteria_m093NamaJabatan"){
                ilike("m093NamaJabatan","%" + (params."sCriteria_m093NamaJabatan" as String) + "%")
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m093ID: it.m093ID,

                    m093NamaJabatan: it.m093NamaJabatan

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["NamaJabatanPIC", params.id] ]
            return result
        }

        result.namaJabatanPICInstance = new NamaJabatanPIC()
        result.namaJabatanPICInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.namaJabatanPICInstance && m.field)
                result.namaJabatanPICInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["NamaJabatanPIC", params.id] ]
            return result
        }

        def namaJabatanPICInstance = new NamaJabatanPIC(params)
        namaJabatanPICInstance.m093ID = Integer.parseInt(generateCodeService.codeGenerateSequence("M093_ID",null))
        result.namaJabatanPICInstance = namaJabatanPICInstance

        if(result.namaJabatanPICInstance.hasErrors() || !result.namaJabatanPICInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        // success
        return result
    }

    def show(id) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["NamaJabatanPIC", id as long] ]
            return result
        }

        result.namaJabatanPICInstance = NamaJabatanPIC.get(id)

        if(!result.namaJabatanPICInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def update(params) {
        NamaJabatanPIC.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.namaJabatanPICInstance && m.field)
                    result.namaJabatanPICInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["NamaJabatanPIC", params.id] ]
                return result
            }

            result.namaJabatanPICInstance = NamaJabatanPIC.get(params.id)

            if(!result.namaJabatanPICInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.namaJabatanPICInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.namaJabatanPICInstance.properties = params

            if(result.namaJabatanPICInstance.hasErrors() || !result.namaJabatanPICInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["NamaJabatanPIC", params.id] ]
            return result
        }

        result.namaJabatanPICInstance = NamaJabatanPIC.get(params.id)

        if(!result.namaJabatanPICInstance)
            return fail(code:"default.not.found.message")

        try {
            result.namaJabatanPICInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }
}
