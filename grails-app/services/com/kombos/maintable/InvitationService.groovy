package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.GeneralParameter
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.reception.Reception
import org.apache.commons.lang.StringUtils

import java.text.SimpleDateFormat

class InvitationService {
    def smsService
    def datatablesUtilService
    def currentGeneralParameter(CompanyDealer companyDealer) {
        def generalParameter = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameter) {
            generalParameter = new GeneralParameter()
            generalParameter.companyDealer = companyDealer
        }
        if (generalParameter?.m000FormatSMSService == null || generalParameter?.m000FormatSMSService?.trim()?.empty) {
            generalParameter?.m000FormatSMSService = GeneralParameter.defaultm000FormatSMSService
        }
        if (generalParameter?.m000FormatEmail == null || generalParameter?.m000FormatEmail?.trim()?.empty) {
            generalParameter?.m000FormatEmail = GeneralParameter.defaultm000FormatEmailService
        }
        return generalParameter
    }

    def formatContentDM(Invitation invitation) {
        def currentCondition = invitation?.customerVehicle?.currentCondition
        def customer = MappingCustVehicle.findByCustomerVehicle(currentCondition.customerVehicle, [sort: "dateCreated", order: "desc"])?.customer
        def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "dateCreated", order: "desc"])
        def reception = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(currentCondition,"0","0", [sort: "dateCreated", order: "desc"])

        GeneralParameter generalParameter = currentGeneralParameter(invitation?.companyDealer)
        String smsContent = generalParameter.m000TextCatatanDM
        smsContent = StringUtils.replace(smsContent, "<br>", "\n")
        smsContent = StringUtils.replace(smsContent, "<WorkshopTAM>", invitation?.companyDealer?.m011NamaWorkshop)
        smsContent = StringUtils.replace(smsContent, "<NamaCustomer>", historyCustomer?.t182NamaDepan + " " + historyCustomer?.t182NamaBelakang)
        smsContent = StringUtils.replace(smsContent, "<JenisSB>", historyCustomer?.jenisCustomer)
        smsContent = StringUtils.replace(smsContent, "<Kendaraan>", currentCondition?.fullModelCode?.baseModel?.m102NamaBaseModel)
        smsContent = StringUtils.replace(smsContent, "<Warna>", currentCondition?.warna?.m092NamaWarna)
        smsContent = StringUtils.replace(smsContent, "<Nopol>", currentCondition?.fullNoPol)
        smsContent = StringUtils.replace(smsContent, "<TanggalService>", reception?.t401TanggalWO?.format("dd-MM-yyyy"))
        smsContent = StringUtils.replace(smsContent, "<Km>", invitation?.reminder?.t201LastKM?.toString())
        smsContent = StringUtils.replace(smsContent, "<Waktu>", invitation?.reminder?.t201TglJatuhTempo?.format("dd-MM-yyyy"))

        return smsContent
    }

    def formatContentEmail(Invitation invitation) {
        def currentCondition = invitation?.customerVehicle?.currentCondition
        def customer = MappingCustVehicle.findByCustomerVehicle(currentCondition.customerVehicle, [sort: "dateCreated", order: "desc"])?.customer
        def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "dateCreated", order: "desc"])
        def reception = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(currentCondition,"0","0", [sort: "dateCreated", order: "desc"])

        GeneralParameter generalParameter = currentGeneralParameter(invitation?.companyDealer)
        String smsContent = generalParameter.m000FormatEmail
        smsContent = StringUtils.replace(smsContent, "<br>", "\n")
        smsContent = StringUtils.replace(smsContent, "<WorkshopTAM>", invitation?.companyDealer?.m011NamaWorkshop)
        smsContent = StringUtils.replace(smsContent, "<NamaCustomer>", historyCustomer?.t182NamaDepan + " " + historyCustomer?.t182NamaBelakang)
        smsContent = StringUtils.replace(smsContent, "<JenisSB>", historyCustomer?.jenisCustomer)
        smsContent = StringUtils.replace(smsContent, "<Kendaraan>", currentCondition?.fullModelCode?.baseModel?.m102NamaBaseModel)
        smsContent = StringUtils.replace(smsContent, "<Warna>", currentCondition?.warna?.m092NamaWarna)
        smsContent = StringUtils.replace(smsContent, "<Nopol>", currentCondition?.fullNoPol)
        smsContent = StringUtils.replace(smsContent, "<TanggalService>", reception?.t401TanggalWO?.format("dd-MM-yyyy"))
        smsContent = StringUtils.replace(smsContent, "<Km>", invitation?.reminder?.t201LastKM?.toString())
        smsContent = StringUtils.replace(smsContent, "<Waktu>", invitation?.reminder?.t201TglJatuhTempo?.format("dd-MM-yyyy"))

        return smsContent
    }

    def formatContentSMS(Invitation invitation) {
        def currentCondition = invitation?.customerVehicle?.currentCondition
        def customer = MappingCustVehicle.findByCustomerVehicle(currentCondition.customerVehicle, [sort: "dateCreated", order: "desc"])?.customer
        def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "dateCreated", order: "desc"])
        def reception = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(currentCondition,"0","0", [sort: "dateCreated", order: "desc"])

        GeneralParameter generalParameter = currentGeneralParameter(invitation?.companyDealer)
        String smsContent = generalParameter.m000FormatSMSService
        smsContent = StringUtils.replace(smsContent, "<br>", "\n")
        smsContent = StringUtils.replace(smsContent, "<WorkshopTAM>", invitation?.companyDealer?.m011NamaWorkshop)
        smsContent = StringUtils.replace(smsContent, "<NamaCustomer>", historyCustomer?.t182NamaDepan + " " + historyCustomer?.t182NamaBelakang)
        smsContent = StringUtils.replace(smsContent, "<JenisSB>", historyCustomer?.jenisCustomer)
        smsContent = StringUtils.replace(smsContent, "<Kendaraan>", currentCondition?.fullModelCode?.baseModel?.m102NamaBaseModel)
        smsContent = StringUtils.replace(smsContent, "<Warna>", currentCondition?.warna?.m092NamaWarna)
        smsContent = StringUtils.replace(smsContent, "<Nopol>", currentCondition?.fullNoPol)
        smsContent = StringUtils.replace(smsContent, "<TanggalService>", reception?.t401TanggalWO?.format("dd-MM-yyyy"))
        smsContent = StringUtils.replace(smsContent, "<Km>", invitation?.reminder?.t201LastKM?.toString())
        smsContent = StringUtils.replace(smsContent, "<Waktu>", invitation?.reminder?.t201TglJatuhTempo?.format("dd-MM-yyyy"))

        if(historyCustomer.t182NoHp)
            smsService.queueSms(historyCustomer.t182NoHp, smsContent)

        return smsContent
    }


    def NamaHariIni = new SimpleDateFormat("EEEEE", new Locale("id"))
    def TanggalHariIni = new SimpleDateFormat("dd MMMMM yyyy", new Locale("id"))

    def formatContentSMSUlangTahun(HistoryCustomer historyCustomer) {
        def currentCondition = historyCustomer?.historyCustomerVehicle
        def companyDealer = currentCondition?.customerVehicle?.companyDealer
        def retention = Retention.findByM207NamaRetention(Retention.ULANG_TAHUN)

        Integer usia = 0
        if (historyCustomer?.t182TglLahir) {
            Calendar calendar = Calendar.getInstance()
            int tahunSekatang = calendar.get(Calendar.YEAR)
            calendar.time = historyCustomer?.t182TglLahir
            usia = tahunSekatang - calendar.get(Calendar.YEAR)
        }

        String smsContent = retention ? retention?.m207FormatSms : ""
        smsContent = StringUtils.replace(smsContent, "<br>", "\n")
        smsContent = StringUtils.replace(smsContent, "<WorkshopTAM>", companyDealer?.m011NamaWorkshop)
        smsContent = StringUtils.replace(smsContent, "<NamaCustomer>", historyCustomer?.t182NamaDepan + " " + historyCustomer?.t182NamaBelakang)
        smsContent = StringUtils.replace(smsContent, "<JenisSB>", historyCustomer?.jenisCustomer)
        smsContent = StringUtils.replace(smsContent, "<Kendaraan>", currentCondition?.fullModelCode?.baseModel?.m102NamaBaseModel)
        smsContent = StringUtils.replace(smsContent, "<Warna>", currentCondition?.warna?.m092NamaWarna)
        smsContent = StringUtils.replace(smsContent, "<Nopol>", currentCondition?.fullNoPol)
        smsContent = StringUtils.replace(smsContent, "<UsiaCustomer>", usia?.toString())
        smsContent = StringUtils.replace(smsContent, "<NamaHariIni>", NamaHariIni.format(new Date()))
        smsContent = StringUtils.replace(smsContent, "<TanggalHariIni>", TanggalHariIni.format(new Date()))

        if(historyCustomer.t182NoHp)
            smsService.queueSms(historyCustomer.t182NoHp, smsContent)

        return smsContent
    }


    def formatContentSMSSTNKJatuhTempo(HistoryCustomerVehicle currentCondition) {
        def customer = MappingCustVehicle.findByCustomerVehicle(currentCondition.customerVehicle, [sort: "id", order: "desc"])?.customer
        def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "id", order: "desc"])
        if (historyCustomer) {
            def companyDealer = currentCondition?.customerVehicle?.companyDealer
            def retention = Retention.findByM207NamaRetention(Retention.STNK_JATUH_TEMPO)
            String smsContent = retention ? retention?.m207FormatSms : ""
            smsContent = StringUtils.replace(smsContent, "<br>", "\n")
            smsContent = StringUtils.replace(smsContent, "<WorkshopTAM>", companyDealer?.m011NamaWorkshop)
            smsContent = StringUtils.replace(smsContent, "<NamaCustomer>", historyCustomer?.t182NamaDepan + " " + historyCustomer?.t182NamaBelakang)
            smsContent = StringUtils.replace(smsContent, "<JenisSB>", historyCustomer?.jenisCustomer)
            smsContent = StringUtils.replace(smsContent, "<Kendaraan>", currentCondition?.fullModelCode?.baseModel?.m102NamaBaseModel)
            smsContent = StringUtils.replace(smsContent, "<Warna>", currentCondition?.warna?.m092NamaWarna)
            smsContent = StringUtils.replace(smsContent, "<Nopol>", currentCondition?.fullNoPol)

            if(historyCustomer.t182NoHp)
                smsService.queueSms(historyCustomer.t182NoHp, smsContent)

            return smsContent
        }
        return null
    }


    def editOrAddReminder(
            JenisInvitation jenisInvitation,
            Reception reception,
            JenisReminder jenisReminder,
            StatusReminder statusReminder,
            GeneralParameter generalParameter) {


        def current = reception?.historyCustomerVehicle
        def reminder = Reminder.findByCustomerVehicleAndT201LastKM(reception?.historyCustomerVehicle?.customerVehicle, reception?.t401KmSaatIni)
        if (!reminder) {
            reminder = new Reminder()
            reminder.companyDealer = generalParameter.companyDealer
            reminder.dateCreated = datatablesUtilService?.syncTime()
        }
        reminder.customerVehicle = reception?.historyCustomerVehicle?.customerVehicle
        reminder.lastUpdated = datatablesUtilService?.syncTime()
        reminder.m201ID = JenisReminder.findById(5 as long)
        reminder.m202ID = statusReminder
        reminder.staDel = "0"
        reminder.t201LastKM = reception.t401KmSaatIni
        reminder.operation = reception?.operation
        reminder.t201LastServiceDate = reception?.t401TanggalWO

        //jatuh tempo sementara
        reminder.t201TglJatuhTempo = new Date()
        if (generalParameter?.m000StaAktifDM?.trim()?.equalsIgnoreCase("1")) {
            reminder.t201TglDM = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusTglReminderDM?.toDouble()?.intValue()
        }
        if (generalParameter?.m000StaAktifEmail?.trim()?.equalsIgnoreCase("1")) {
            reminder.t201TglEmail = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderEmail?.toDouble()?.intValue()
        }
        if (generalParameter?.m000StaAktifSMS?.trim()?.equalsIgnoreCase("1")) {
            reminder.t201TglSMS = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderSMS?.toDouble()?.intValue()
        }
        if (generalParameter?.m000StaAktifCall?.trim()?.equalsIgnoreCase("1")) {
            reminder.t201TglCall = reminder.t201TglJatuhTempo - generalParameter?.m000HMinusReminderCall?.toDouble()?.intValue()
        }


        reminder.save(flush: true)


        current.t183StaReminderCustPasif = "1"
        current.lastUpdated = datatablesUtilService?.syncTime()
        current.save(flush: true)


        def invitation = Invitation.findByReminderAndJenisInvitation(reminder, jenisInvitation);
        if (!invitation) {
            invitation = new Invitation()
            invitation.reminder = reminder
            invitation.companyDealer = generalParameter.companyDealer
            invitation.dateCreated = datatablesUtilService?.syncTime()
        }

        invitation.lastUpdated = datatablesUtilService?.syncTime()
        invitation.jenisInvitation = jenisInvitation
        invitation.customerVehicle = reception?.historyCustomerVehicle?.customerVehicle
        invitation.t202TglNextReContact = new Date()
        invitation.t202KetReContact = "-"

        invitation.save(flush: true)

        reception.flagInvitation = invitation
        reception.lastUpdated = datatablesUtilService?.syncTime()
        reception.save(flush: true)
    }


}
