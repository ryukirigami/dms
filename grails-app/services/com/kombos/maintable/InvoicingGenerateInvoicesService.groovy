package com.kombos.maintable

import com.kombos.administrasi.BiayaAdministrasi
import com.kombos.administrasi.Operation
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.parts.Konversi
import com.kombos.parts.StatusApproval
import com.kombos.reception.JobInv
import com.kombos.woinformation.JobRCP
import org.hibernate.criterion.CriteriaSpecification

/**
 * Created by Ahmad Fawaz on 27/12/14.
 */
class InvoicingGenerateInvoicesService implements AfterApprovalInterface {

    def datatablesUtilService

    @Override
    def afterApproval(String fks, StatusApproval staApproval, Date tglApproveUnApprove, String alasanUnApprove, String namaUserApproveUnApprove) {
        def staAprove = ""
        def sAlsan = ""
        if (staApproval == StatusApproval.APPROVED) {
            staAprove = "0"
        }else{
            staAprove="1"
            sAlsan = alasanUnApprove
        }
        def invoice = InvoiceT701.get(fks.toLong())
        invoice?.t701StaApprovedReversal = staAprove
        invoice?.t701AlasanUnApproved = sAlsan
        invoice?.lastUpdated = datatablesUtilService?.syncTime()
        invoice?.save(flush: true)
    }

    boolean transactional = false
    def conversi = new Konversi()
    def datatablesList(def params) {

        def c = InvoiceT701.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t701StaDel","0")
            reception{
                eq("t401NoWO",params.noWO,[ignoreCase : true])
            }
            order("t701NoInv")
        }

        def rows = []

        results.each { inv ->
            def nPart = ""
            def partSlip = PartInv.createCriteria().list {
                invoice{
                    eq("id", inv?.id)
                }
                eq("t703StaDel", "0")

                resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                projections {
                    groupProperty("t703NoPartInv", "t703NoPartInv")
                }
                order("t703NoPartInv")
            }

            int ind=1

            partSlip.each {
                if(ind==partSlip.size()){
                    nPart+=it?.t703NoPartInv
                }else{
                    nPart+=it?.t703NoPartInv+", "
                }
                ind++;
            }
            String isEnable = (inv?.t701JenisInv?.contains("T") && inv?.t701StaSettlement=="1" ?"TIDAK":(inv?.t701JenisInv?.contains("K") && inv?.t701PrintKe>0?"TIDAK":"YA"))
            if(inv?.t701StaApprovedReversal && inv?.t701StaApprovedReversal=="0"){
                isEnable = "TIDAK"
            }
            if(inv?.t701NoInv_Reff){
                isEnable = "TIDAK"
            }
            rows << [
                id: inv?.id,
                t701NoInv : inv?.t701NoInv,
                t701Nopol : inv?.t701Nopol,
                t701TglJamMulai : inv?.t701TglJamMulai,
                t701TglJamInvoice : inv?.t701TglJamInvoice?.format("dd/MM/yyyy"),
                t703NoPartInv : nPart,
                t701StaApprovedReversal : inv?.t701StaApprovedReversal,
                reception : inv?.reception?.t401NoWO,
                t701NoInv_Reff : inv?.t701NoInv_Reff,
                metodeBayar : inv?.metodeBayar?.m701MetodeBayar,
                historyCustomer : inv?.historyCustomer?.fullNama,
                t701Customer : inv?.t701Customer,
                t701Alamat : inv?.t701Alamat,
                t701NPWP : inv?.t701NPWP,
                t701JasaRp : conversi.toRupiah(inv?.t701JasaRp),
                t701SubletRp : conversi.toRupiah(inv?.t701SubletRp),
                t701PartsRp : conversi.toRupiah(inv?.t701PartsRp),
                t701OliRp : conversi.toRupiah(inv?.t701OliRp),
                t701MaterialRp : inv?.t701MaterialRp ? conversi.toRupiah(inv?.t701MaterialRp) : 0,
                t701AdmRp : inv?.t701AdmRp ? conversi.toRupiah(inv?.t701AdmRp) : 0,
                t701SubTotalRp : conversi.toRupiah(inv?.t701SubTotalRp),
                t701DiscSpecialRp : inv?.t701DiscSpecialRp,
                t701TotalDiscRp : conversi.toRupiah(inv?.t701TotalDiscRp),
                t701PPnRp : conversi.toRupiah(inv?.t701PPnRp),
                t701MateraiRp : inv?.t701MateraiRp ? conversi.toRupiah(inv?.t701MateraiRp) : 0,
                t701TotalRp : conversi.toRupiah(inv?.t701TotalRp),
                t701TotalInv : conversi.toRupiah(inv?.t701TotalInv),
                t701BookingFee : conversi?.toRupiah(inv?.t701BookingFee),
                t701OnRisk : conversi?.toRupiah(inv?.t701OnRisk),
                t701TotalBayarRp : conversi.toRupiah(inv?.t701TotalBayarRp),
                t701Catatan : inv?.t701Catatan,
                t701IntExt : inv?.t701IntExt?.equalsIgnoreCase("1") ? "EXT" : "INT",
                t701StaWarranty : inv?.t701StaWarranty,
                t701Tipe : inv?.t701Tipe?.contains("0") ? "GR" : "BP",
                isEnable : isEnable,
                t701JenisInv : inv?.t701JenisInv?.contains("K") ? "Kredit" : "Tunai",

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesJasa(def params) {

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
//        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def inv = InvoiceT701.createCriteria().get {
            eq("t701NoInv",params?.noInvoice,[ignoreCase : true])
            maxResults(1);
        }
        def c = JobInv.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("invoice",inv);
        }

        def rows = []
        def subtot = 0
        results.each {
            def jobs = JobRCP.findByReceptionAndOperation(it.reception,it.operation)
            subtot+= it?.t702HargaRp
            rows << [
                id: it?.id,
                operation: it?.operation?.m053NamaOperation,
                rate: jobs?.t402Rate,
                total: conversi.toRupiah(it?.t702HargaRp),
                subtot: conversi.toRupiah(subtot) ? conversi.toRupiah(subtot) : 0
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows,catatan : inv?.t701Catatan]

    }

    def datatablesPart(def params) {

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
//        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PartInv.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t703StaDel","0")
            invoice{
                eq("t701NoInv",params.noInvoice,[ignoreCase : true])
            }
        }

        def rows = []
        def subtotpart = 0
        results.each {
            subtotpart += it?.t703TotalRp
            rows << [
                id: it?.id,
                kodePart : it?.goods?.m111ID,
                namaPart : it?.goods?.m111Nama,
                qty : it?.t703Jumlah1,
                harga : conversi.toRupiah(it?.t703HargaRp),
                disc : it?.t703DiscPersen,
                extAmmount : conversi.toRupiah(it?.t703TotalRp),
                stotpart : conversi.toRupiah(subtotpart) ? conversi.toRupiah(subtotpart) : 0
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesDiscount(def params) {

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
//        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = JobInv.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            invoice{
                eq("t701NoInv",params?.noInvoice,[ignoreCase : true])
            }
        }

        def rows = []
        def p, subtot = 0
        def totPart = 0
        results.each {
            def parts = PartInv.findAllByOperationAndInvoiceAndT703StaDel(it.operation, it.invoice, "0")

            parts.each {
                p = it
                totPart += p.t703HargaRp ? p.t703HargaRp : 0
            }

            subtot+= it?.t702TotalRp
            rows << [
                    id: it?.id,
                    idJob : it.operationId,
                    idInv : it.invoiceId,
                    namaJob: it?.operation?.m053NamaOperation,
                    harga: conversi.toRupiah(it?.t702TotalRp),
                    kategori: "",
                    staPart : parts.size() > 0 ? "ya" : "tidak",
                    subtot: conversi.toRupiah(subtot),
                    totalHrga: conversi.toRupiah(totPart + subtot),
                    cvPersen: "",
                    cvRp: "",
                    staticPersen: "",
                    staticRp: "",
                    totalDisStc: "",
                    dinPersen: "",
                    dinRp: "",
                    totalDisDin: ""
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }

    def partDatatablesList(def params){
        def rows = []
        if(params."idJob"){
            def invoic = InvoiceT701.get(params.idInv as long)
            def job = JobInv.findByOperationAndInvoiceAndStaDel(Operation.get(params."idJob" as Long), invoic, "0")

            if(job){
                def results = PartInv.findAllByOperationAndInvoiceAndT703StaDel(job.operation, invoic, "0")

                def subtotpart = 0
                results.each {
                    subtotpart += it?.t703TotalRp
                    rows << [
                            id: it?.id,
                            namaPart : it?.goods?.m111Nama,
                            qty : it?.t703Jumlah1,
                            harga : conversi.toRupiah(it?.t703HargaRp),
                            kategori: "",
                            cvPersen: "",
                            cvRp: "",
                            staticPersen: "",
                            staticRp: "",
                            totalDisStc: "",
                            dinPersen: "",
                            dinRp: "",
                            totalDisDin: ""
                    ]
                }
            }
        }
        [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]
    }
}
