package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification

import java.text.SimpleDateFormat

class EFakturKeluaranService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def jasperService
    def excelImportService
    def datatablesUtilService


    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = EFakturKeluaran.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staOriRev","0")
            order("NO_FAKTUR","asc")
            if (params.sCriteria_docNumber){
                eq("docNumber",params.sCriteria_docNumber)
            }
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("staDel","0")
                eq("staOriRev","0")
                eq("staAprove","1") // ini staAprove 1 buat yang HO
                order("NO_FAKTUR","asc")
                if (params.sCriteria_docNumber){
                    eq("docNumber",params.sCriteria_docNumber)
                }

            }else{
                eq("companyDealer",params.userCompanyDealer)
            }

            if(params.sCriteria_TGL_SI && params.sCriteria_TGL_SI){
                ge("TGL_SI",params.sCriteria_TGL_SI)
                lt("TGL_SI",params.sCriteria_TGL_SI + 1)
            }


            if(params."sCriteria_NO_FAKTUR"){
                ilike("NO_FAKTUR","%" + (params."sCriteria_NO_FAKTUR" as String) + "%")
            }

            if(params."sCriteria_MASA_PAJAK"){
                ilike("MASA_PAJAK","%" + (params."sCriteria_MASA_PAJAK" as String) + "%")
            }

            if(params."sCriteria_TAHUN_PAJAK"){
                ilike("TAHUN_PAJAK","%" + (params."sCriteria_TAHUN_PAJAK" as String) + "%")
            }

            if(params."sCriteria_REFERENSI"){
                ilike("REFERENSI","%" + (params."sCriteria_REFERENSI" as String) + "%")
            }

            if(params."sCriteria_NO_SI"){
                ilike("NO_SI","%" + (params."sCriteria_NO_SI" as String) + "%")
            }

            if(params."sCriteria_NAMA_CUSTOMER"){
                ilike("NAMA_CUSTOMER","%" + (params."sCriteria_NAMA_CUSTOMER" as String) + "%")
            }

            if(params."sCriteria_NPWP"){
                ilike("NPWP","%" + (params."sCriteria_NPWP" as String) + "%")
            }

            if(params."sCriteria_ALAMAT"){
                ilike("ALAMAT","%" + (params."sCriteria_ALAMAT" as String) + "%")
            }

            if(params."sCriteria_BIAYA_JASA"){
                eq("BIAYA_JASA",(params."sCriteria_BIAYA_JASA" as String).toBigDecimal())
            }

            if(params."sCriteria_BIAYA_PARTS"){
                eq("BIAYA_PARTS",(params."sCriteria_BIAYA_PARTS" as String).toBigDecimal())
            }

            if(params."sCriteria_BIAYA_OLI"){
                eq("BIAYA_OLI",(params."sCriteria_BIAYA_OLI" as String).toBigDecimal())
            }

            if(params."sCriteria_BIAYA_MATERIAL"){
                eq("BIAYA_MATERIAL",(params."sCriteria_BIAYA_MATERIAL" as String).toBigDecimal())
            }

            if(params."sCriteria_BIAYA_SUBLET"){
                eq("BIAYA_SUBLET",(params."sCriteria_BIAYA_SUBLET" as String).toBigDecimal())
            }

            if(params."sCriteria_BIAYA_ADM"){
                eq("BIAYA_ADM",(params."sCriteria_BIAYA_ADM" as String).toBigDecimal())
            }

            if(params."sCriteria_DISC_JASA"){
                eq("DISC_JASA",(params."sCriteria_DISC_JASA" as String).toBigDecimal())
            }

            if(params."sCriteria_DISC_PARTS"){
                eq("DISC_PARTS",(params."sCriteria_DISC_PARTS" as String).toBigDecimal())
            }

            if(params."sCriteria_DISC_BAHAN"){
                eq("DISC_BAHAN",(params."sCriteria_DISC_BAHAN" as String).toBigDecimal())
            }

            if(params."sCriteria_DISC_OLI"){
                eq("DISC_OLI",(params."sCriteria_DISC_OLI" as String).toBigDecimal())
            }

            if(params."sCriteria_DPP"){
                eq("DPP",(params."sCriteria_DPP" as String).toBigDecimal())
            }

            if(params."sCriteria_PPN"){
                eq("PPN",(params."sCriteria_PPN" as String).toBigDecimal())
            }

            if(params."sCriteria_TOTAL"){
                eq("TOTAL",(params."sCriteria_TOTAL" as String).toBigDecimal())
            }

            if(params."sCriteria_docNumber"){
                ilike("docNumber","%" + (params."sCriteria_docNumber" as String) + "%")
            }

            if(params."sCriteria_staAprove"){
                ilike("staAprove","%" + (params."sCriteria_staAprove" as String) + "%")
            }
        }

        def rowss = []
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        results.each {
            rowss << [

                    id: it?.id,

                    NO_FAKTUR : it?.NO_FAKTUR ? it?.NO_FAKTUR:"-",

                    MASA_PAJAK : it?.MASA_PAJAK ? it?.MASA_PAJAK:"-",

                    TAHUN_PAJAK : it?.TAHUN_PAJAK ? it?.TAHUN_PAJAK:"-",

                    REFERENSI : it?.REFERENSI ? it?.REFERENSI:"-",

                    NO_SI : it?.NO_SI ? it?.NO_SI:"-",

                    TGL_SI : it?.TGL_SI ? it?.TGL_SI.format("dd-MM-yyyy"):"-",

                    NAMA_CUSTOMER : it?.NAMA_CUSTOMER ? it?.NAMA_CUSTOMER:"-",

                    NPWP : it?.NPWP ? it?.NPWP:"-",

                    ALAMAT : it?.ALAMAT ? it?.ALAMAT:"-",

                    BIAYA_JASA : it?.BIAYA_JASA ? it?.BIAYA_JASA:"-",

                    BIAYA_PARTS : it?.BIAYA_PARTS ? it?.BIAYA_PARTS:"-",

                    BIAYA_OLI : it?.BIAYA_OLI ? it?.BIAYA_OLI:"-",

                    BIAYA_MATERIAL : it?.BIAYA_MATERIAL ? it?.BIAYA_MATERIAL:"-",

                    BIAYA_SUBLET : it?.BIAYA_SUBLET ? it?.BIAYA_SUBLET:"-",

                    BIAYA_ADM : it?.BIAYA_ADM ? it?.BIAYA_ADM:"-",

                    DISC_JASA : it?.DISC_JASA ? it?.DISC_JASA:"-",

                    DISC_PARTS : it?.DISC_PARTS ? it?.DISC_PARTS:"-",

                    DISC_BAHAN : it?.DISC_BAHAN ? it?.DISC_BAHAN:"-",

                    DISC_OLI : it?.DISC_OLI ? it?.DISC_OLI:"-",

                    DPP : it?.DPP ? it?.DPP:"-",

                    PPN : it?.PPN ? it?.PPN:"-",

                    TOTAL : it?.TOTAL ? it?.TOTAL:"-",

                    docNumber : it?.docNumber ? it?.docNumber:"-",

                    staAprove : it?.staAprove=="1"?"Approved":(it?.staAprove=="2"?"Rejected":"Wait")


            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rowss]

    }


    def datatablesList1(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c2 = EFakturKeluaran.createCriteria()
        def resultsKon = c2.list {
            eq("staDel","0")
            eq("staOriRev","0")
            order("tanggal","DESC")
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
                eq("staAprove","1")
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal && params.sCriteria_Tanggal2){
                ge("tanggal",params.sCriteria_Tanggal)
                lt("tanggal",params.sCriteria_Tanggal2 + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                min("staAprove", "staAprove")
                max("tanggal", "tanggal")
                max("lastUpdProcess", "lastUpdProcess")
                max("companyDealer", "companyDealer")
                order("tanggal", "asc")
                order("staAprove", "asc")
                order("lastUpdProcess", "desc")
            }
        }



        def c = EFakturKeluaran.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staOriRev","0")
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
                eq("staAprove","1")
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal && params.sCriteria_Tanggal2){
                ge("tanggal",params.sCriteria_Tanggal)
                lt("tanggal",params.sCriteria_Tanggal2 + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                min("staAprove", "staAprove")
                max("tanggal", "tanggal")
                min("lastUpdProcess", "lastUpdProcess")
                max("companyDealer", "companyDealer")
                order("tanggal", "asc")
                order("staAprove", "asc")
                if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                    order("lastUpdProcess", "desc")
                }
            }
        }

        def rows = []
        results.each {
            rows << [

                    id: it?.id,

                    companyDealer: it?.companyDealer?.m011NamaWorkshop,

                    staPrint: it?.lastUpdProcess,

                    docNumber: it?.docNumber ? it?.docNumber:"-",

                    tanggal: it?.tanggal.format("dd/MM/yyyy"),

                    staAprove : (it.staAprove as String)=="1"?"APPROVED":((it.staAprove as String)=="2"?"REJECTED":"WAITING FOR APPROVAL"),

                    staAproveId : (it.staAprove as String)

            ]
        }
        [sEcho: params.sEcho, iTotalRecords: resultsKon.size(), iTotalDisplayRecords: resultsKon.size(), aaData: rows]

    }

    def printEfaktur(def params){
        List<JasperReportDef> reportDefList = []
        def reportData = printEfakturDetail(params)

        def reportDef = new JasperReportDef(name:'reportEfaktur.jasper',
                fileFormat:JasperExportFormat.CSV_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)
        return reportDefList
    }

    def printEfakturDetail(def params){
        def docNumber = params.docNumber
        def reportData = new ArrayList();
        def efaktur = EFakturKeluaran.createCriteria().list {
            eq("docNumber",docNumber)
            eq("staDel","0")
            eq("staAprove","1")
        }

        efaktur.each {
            def data = [:]
            data.put("COLUMN","FK")
            data.put("COLUMN_1","KD_JENIS_TRANSAKSI")
            data.put("COLUMN_2","FG_PENGGANTI")
            data.put("COLUMN_3","NOMOR_FAKTUR")
            data.put("COLUMN_4","MASA_PAJAK")
            data.put("COLUMN_5","TAHUN_PAJAK")
            data.put("COLUMN_6","TANGGAL_FAKTUR")
            data.put("COLUMN_7","NPWP")
            data.put("COLUMN_8","NAMA")
            data.put("COLUMN_9","ALAMAT_LENGKAP")
            data.put("COLUMN_10","JUMLAH_DPP")
            data.put("COLUMN_11","JUMLAH_PPN")
            data.put("COLUMN_12","JUMLAH_PPNBM")
            data.put("COLUMN_13","ID_KETERANGAN_TAMBAHAN")
            data.put("COLUMN_14","FG_UANG_MUKA")
            data.put("COLUMN_15","UANG_MUKA_DPP")
            data.put("COLUMN_16","UANG_MUKA_PPN")
            data.put("COLUMN_17","UANG_MUKA_PPNBM")
            data.put("COLUMN_18","REFERENSI")
            data.put("COLUMN_19","LT")
            data.put("COLUMN_20","NPWP2") // MENUNJUKAN NPWP2
            data.put("COLUMN_21","NAMA2") // MENUNJUKKAN NAMA2
            data.put("COLUMN_22","JALAN")
            data.put("COLUMN_23","BLOK")
            data.put("COLUMN_24","NOMOR")
            data.put("COLUMN_25","RT")
            data.put("COLUMN_26","RW")
            data.put("COLUMN_27","KECAMATAN")
            data.put("COLUMN_28","KELURAHAN")
            data.put("COLUMN_29","KABUPATEN")
            data.put("COLUMN_30","PROPINSI")
            data.put("COLUMN_31","KODE_POS")
            data.put("COLUMN_32","NOMOR_TELEPON")
            data.put("COLUMN_33","OF")
            data.put("COLUMN_34","KODE_OBJEK")
            data.put("COLUMN_35","NAMA_OBJEK")
            data.put("COLUMN_36","HARGA_SATUAN")
            data.put("COLUMN_37","JUMLAH_BARANG")
            data.put("COLUMN_38","HARGA_TOTAL")
            data.put("COLUMN_39","DISKON")
            data.put("COLUMN_40","DPP")
            data.put("COLUMN_41","PPN")
            data.put("COLUMN_42","TARIF_PPNBM")
            data.put("COLUMN_43","PPNBM")

            data.put("F_COLUMN",it.getFK())
            data.put("F_COLUMN_1",it.getKD_JENIS_TRANSAKSI())
            data.put("F_COLUMN_2",it.getFG_PENGGANTI())
            data.put("F_COLUMN_3",it.getNOMOR_FAKTUR())
            data.put("F_COLUMN_4",it.getMASA_PAJAK())
            data.put("F_COLUMN_5",it.getTAHUN_PAJAK())
            data.put("F_COLUMN_6",it.getTANGGAL_FAKTUR())
            data.put("F_COLUMN_7",it.getNPWP())
            data.put("F_COLUMN_8",it.getNAMA())
            data.put("F_COLUMN_9",it.getNAMA())
            data.put("F_COLUMN_10",it.getJUMLAH_DPP())
            data.put("F_COLUMN_11",it.getJUMLAH_PPN())
            data.put("F_COLUMN_12",it.getJUMLAH_PPNBM())
            data.put("F_COLUMN_13",it.getID_KETERANGAN_TAMBAHAN())
            data.put("F_COLUMN_14",it.getFG_UANG_MUKA())
            data.put("F_COLUMN_15",it.getUANG_MUKA_DPP())
            data.put("F_COLUMN_16",it.getUANG_MUKA_PPN())
            data.put("F_COLUMN_17",it.getUANG_MUKA_PPNBM())
            data.put("F_COLUMN_18",it.getREFERENSI())
            data.put("F_COLUMN_19",it.getLT())
            data.put("F_COLUMN_20",it.getNPWP2())
            data.put("F_COLUMN_21",it.getJALAN())
            data.put("F_COLUMN_22",it.getNAMA2())
            data.put("F_COLUMN_23",it.getBLOK())
            data.put("F_COLUMN_24",it.getNOMOR())
            data.put("F_COLUMN_25",it.getRT())
            data.put("F_COLUMN_26",it.getRW())
            data.put("F_COLUMN_27",it.getKECAMATAN())
            data.put("F_COLUMN_28",it.getKELURAHAN())
            data.put("F_COLUMN_29",it.getKABUPATEN())
            data.put("F_COLUMN_30",it.getPROPINSI())
            data.put("F_COLUMN_31",it.getKODE_POS())
            data.put("F_COLUMN_32",it.getNOMOR_TELEPON())
            data.put("F_COLUMN_33",it.getOF_())
            data.put("F_COLUMN_34",it.getKODE_OBJEK())
            data.put("F_COLUMN_35",it.getNAMA_OBJEK())
            data.put("F_COLUMN_36",it.getHARGA_SATUAN())
            data.put("F_COLUMN_37",it.getJUMLAH_BARANG())
            data.put("F_COLUMN_38",it.getHARGA_TOTAL())
            def diskonVal = it?.getDISKON()
            if (diskonVal==null){
                diskonVal=0
            }
            data.put("F_COLUMN_39",diskonVal)
            def dPP = it?.getDPP()
            def pPN = it?.getPPN()
            def tARIFPPNBM = it?.getTARIF_PPNBM()
            if (dPP==null){
                dPP=0
            }

            if (pPN==null){
                pPN=0
            }
            if (tARIFPPNBM==null){
                tARIFPPNBM=0
            }

            data.put("F_COLUMN_40",dPP)
            data.put("F_COLUMN_41",pPN)
            data.put("F_COLUMN_42",tARIFPPNBM)
            def pPNBM = it?.getPPNBM()
            if (pPNBM==null){
                pPNBM=0
            }
            data.put("F_COLUMN_43",pPNBM)
            reportData.add(data)
        }

        return reportData
    }
}
