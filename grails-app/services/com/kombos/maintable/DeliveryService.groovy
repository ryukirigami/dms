package com.kombos.maintable

import com.kombos.administrasi.*
import com.kombos.customerprofile.*
import com.kombos.finance.BankAccountNumber
import com.kombos.finance.Collection
import com.kombos.finance.DocumentCategory
import com.kombos.finance.Journal
import com.kombos.finance.PenerimaanLainLainController
import com.kombos.finance.RefundUangMukaJournalService
import com.kombos.finance.Transaction
import com.kombos.finance.TransactionType
import com.kombos.finance.UMServiceAsuransiJournalService
import com.kombos.finance.UMServicePelangganJournalService
import com.kombos.generatecode.GenerateCodeService
import com.kombos.hrd.Karyawan
import com.kombos.parts.Konversi
import com.kombos.reception.Appointment
import com.kombos.reception.CustomerIn
import com.kombos.reception.JobInv
import com.kombos.reception.Reception
import com.kombos.utils.MoneyUtil
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import groovy.sql.Sql
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.context.request.RequestContextHolder

/**
 *
 * @author Febry Indra Setyawan
 */
class DeliveryService {

    boolean transactional = false
    def dataSource
    def generateCodeService
    def datatablesUtilService
    def conversi = new Konversi()
    private MoneyUtil moneyUtil = new MoneyUtil()

    private String BASE_QUERY_INVOICE_MASTER = """
        SELECT DISTINCT
            TO_CHAR(Invoice.T701_TglJamInvoice,'DD-MM-YYYY') as TanggalInvoice,
            TO_CHAR(Invoice.T701_DueDate, 'DD-MM-YYYY') as JatuhTempo,
            Invoice.T701_NoInv as NoInvoice,
            Reception.T401_NoWO as NomorWO,
            Invoice.T701_NOPOL AS NOPOL,
            Invoice.T701_Customer as NamaCustomer,
            Invoice.T701_TIPE as TipeInv,
            Reception.T401_StaKtgrCustUmumSPKAsrnsi as SPK,
            Invoice.T701_TOTALBAYARRP as TotalInvoice,
            Settlement.T704_T701_NoInv as Status,
            Invoice.T701_JENISINV as Jenis,
            Invoice.T701_PRINTKE as jmlPrint
        FROM T701_INVOICE Invoice
        INNER JOIN T401_RECEPTION Reception
            ON (Invoice.T701_T401_NoWO = Reception.ID AND Reception.T401_StaDel = '0')
        LEFT JOIN T704_SETTLEMENT Settlement
            ON (Invoice.ID = Settlement.T704_T701_NoInv AND Settlement.T704_StaDel = '0')
    """.intern()

    private String BASE_QUERY_INVOICE_DETAIL = """
        SELECT
            Invoice.T701_NoInv as NoInvoice,
            TO_CHAR(Settlement.T704_TglJamSettlement, 'DD-MM-YYYY') as TglSettlement,
            MetodeBayar.M701_MetodeBayar as MetodePembayaran,
            Bank.M702_NamaBank as NamaBank,
            MesinEDC.M704_NamaMesinEDC as NamaMesinEDC,
            Settlement.T704_NoKartu as NomorKartu,
            Settlement.T704_M703_BankCharge as BankCharge,
            Settlement.T704_NoWBS as NomorWBS,
            Settlement.T704_JmlBayar as JumlahSettlement
        FROM T701_INVOICE Invoice
        INNER JOIN T401_RECEPTION Reception
            ON (Invoice.T701_T401_NoWO = Reception.ID and Reception.T401_StaDel = '0')
        LEFT JOIN T704_SETTLEMENT Settlement
            ON (Invoice.ID = Settlement.T704_T701_NOINV and Settlement.T704_StaDel = '0')
        LEFT JOIN M701_METODEBAYAR MetodeBayar
            ON (Settlement.T704_M701_ID = MetodeBayar.ID and MetodeBayar.M701_StaDel = '0')
        LEFT JOIN M702_BANK Bank
            ON (Settlement.T704_M702_ID = Bank.ID and Bank.M702_staDel = '0')
        LEFT JOIN M704_MESINEDC MesinEDC
            ON (Settlement.M704_M704_ID = MesinEDC.ID and MesinEDC.M704_StaDel = '0')
    """.intern()

    private String BASE_QUERY_PARTS = """
        SELECT
            TO_CHAR(Invoice.T701_TglJamInvoice, 'DD-MM-YYYY') as TanggalPartsSlip,
            PartInv.T703_NoPartInv as NoPartsSlip,
            Rec.T401_NOWO as NomorWO,
            Invoice.T701_Nopol as NomorPolisi,
            Invoice.T701_Customer as Customer,
            Invoice.T701_Alamat as Alamat,
            SUM((PartInv.T703_Jumlah1 * PartInv.T703_HargaRp) - PartInv.T703_DiscRp) as GrandTotal,
            Invoice.T701_NoInv as NoInvoice
        FROM T703_PARTINV PartInv
        INNER JOIN T701_INVOICE Invoice ON (PartInv.T703_T701_NoInv = Invoice.ID AND Invoice.T701_StaDel = '0')
        INNER JOIN T401_RECEPTION Rec ON (Invoice.T701_T401_NOWO = Rec.ID)
        INNER JOIN T161_KLASIFIKASIGOODS GroupGoods ON (PartInv.T703_M111_ID = GroupGoods.T161_M111_ID)
        INNER JOIN M117_FRANC GroupPart ON (GroupGoods.T161_M117_ID = GroupPart.ID)
    """.intern()

    private String BASE_QUERY_PARTS_GROUP = """
        SELECT
            PartInv.T703_NoPartInv as partInvoice,
            GroupPart.ID as GroupId,
            CONCAT('Sub Total ', GroupPart.M117_NAMAFRANC) as GroupGood,
            SUM((PartInv.T703_Jumlah1 * PartInv.T703_HargaRp) - PartInv.T703_DiscRp) as SubTotal,
            Invoice.ID as InvId
        FROM T703_PARTINV PartInv
        INNER JOIN T701_INVOICE Invoice ON (PartInv.T703_T701_NoInv = Invoice.ID AND Invoice.T701_StaDel = '0')
        INNER JOIN M111_GOODS Goods ON (PartInv.T703_M111_ID = Goods.ID AND Goods.M111_StaDel = '0')
        INNER JOIN T161_KLASIFIKASIGOODS GroupGoods ON (Goods.ID = GroupGoods.T161_M111_ID)
        INNER JOIN M117_FRANC GroupPart ON (GroupGoods.T161_M117_ID = GroupPart.ID)
    """.intern()

    private String BASE_QUERY_PARTS_DETAIL = """
        SELECT
            Goods.M111_ID as PartNo,
            Goods.M111_Nama as PartName,
            PartInv.T703_Jumlah1 as Quantity,
            PartInv.T703_HargaRp as UnitPrice,
            PartInv.T703_DiscPersen as Discount,
            ((PartInv.T703_Jumlah1 * PartInv.T703_HargaRp) - PartInv.T703_DiscRp) as ExtAmount,
            PartInv.T703_Remark as Remark
        FROM T703_PARTINV PartInv
        INNER JOIN T701_INVOICE Invoice ON (PartInv.T703_T701_NoInv = Invoice.ID AND Invoice.T701_StaDel = '0')
        INNER JOIN M111_GOODS Goods ON (PartInv.T703_M111_ID = Goods.ID AND Goods.M111_StaDel = '0')
        INNER JOIN T161_KLASIFIKASIGOODS GroupGoods ON (Goods.ID = GroupGoods.T161_M111_ID)
        INNER JOIN M117_FRANC GroupPart ON (GroupGoods.T161_M117_ID = GroupPart.ID)
      """.intern()

    private String BASE_QUERY_FAKTUR = """
        SELECT
            TO_CHAR(FakturPajak.T711_TglJamFP, 'DD-MM-YYYY') as TanggalFaktur,
            FakturPajak.T711_NoFakturPajak as NoFakturPajak,
            FakturPajak.T711_T701_NoInv as NoInvoice,
            CompanyDealer.M011_NamaNPWP as Penjual,
            CompanyDealer.M011_AlamatNPWP as AlamatPenjual,
            CompanyDealer.M011_NPWP as NpwpPenjual,
            Invoice.T701_Customer as Penerima,
            Invoice.T701_Alamat as AlamatPenerima,
            Invoice.T701_NPWP as NpwpPenerima,
            FakturPajak.T711_BiayaJasa as Jasa,
            FakturPajak.T711_BiayaGoods as Parts,
            FakturPajak.T711_DasarKenaPajak as DPP,
            FakturPajak.T711_PPN as PPN
        FROM T711_FAKTURPAJAK FakturPajak
        INNER JOIN M011_COMPANYTAM CompanyDealer
            ON (FakturPajak.T711_M011_Cabang = CompanyDealer.ID and CompanyDealer.M011_StaDel = '0')
        INNER JOIN T701_INVOICE Invoice
            ON (FakturPajak.T711_T701_NoInv = Invoice.ID and Invoice.T701_StaDel = '0')
        INNER JOIN T182_HISTORYCUSTOMER HistoryCustomer
            ON (Invoice.T701_T182_ID = HistoryCustomer.ID and HistoryCustomer.T182_StaDel = '0')
    """.intern()

    private String BASE_QUERY_NOTA = """
        SELECT
            TO_CHAR(FakturPajak.T711_TglJamFP, 'DD-MM-YYYY') as TanggalNota,
            FakturPajak.T711_NoFakturPajak as NoNotaRetur,
            FakturPajak.T711_NoFakturPajakReff as NoFakturPajak,
            CompanyDealer.M011_NamaNPWP as Penjual,
            CompanyDealer.M011_AlamatNPWP as AlamatPenjual,
            CompanyDealer.M011_NPWP as NpwpPenjual,
            Invoice.T701_Customer as Pembeli,
            Invoice.T701_Alamat as AlamatPembeli,
            Invoice.T701_NPWP as NpwpPembeli,
            FakturPajak.T711_BiayaJasa as Jasa,
            FakturPajak.T711_BiayaGoods as Parts,
            FakturPajak.T711_DasarKenaPajak as Total,
            FakturPajak.T711_PPN as PPN
        FROM T711_FAKTURPAJAK FakturPajak
        INNER JOIN M011_COMPANYTAM CompanyDealer
            ON (FakturPajak.T711_M011_Cabang = CompanyDealer.ID and CompanyDealer.M011_StaDel = '0')
        INNER JOIN T701_INVOICE Invoice
            ON (FakturPajak.T711_T701_NoInv = Invoice.ID and Invoice.T701_StaDel = '0')
        INNER JOIN T182_HISTORYCUSTOMER HistoryCustomer
            ON (Invoice.T701_T182_ID = HistoryCustomer.ID and HistoryCustomer.T182_StaDel = '0')
    """.intern()

    private String BASE_QUERY_KUITANSI_MASTER = """
        SELECT DISTINCT
            TO_CHAR(BookingFee.T721_TglJamBookingFee, 'DD-MM-YYYY') as TanggalKwitansi,
            Kwitansi.T722_NoKwitansi as NoKwitansi,
            Kwitansi.T722_staBookingOnRisk as JenisBayar,
            Reception.T401_NoWO as NomorWO,
            HistoryCustomer.T182_ID as NoCustomer,
            Kwitansi.T722_Nama as TerimaDari,
            Kwitansi.T722_Ket as Pembayaran,
            Kwitansi.T722_JmlUang as TotalBayar,
            Kwitansi.CREATED_BY as Kasir
        FROM T721_BOOKINGFEE BookingFee
        INNER JOIN T722_KWITANSI Kwitansi
            ON (BookingFee.T721_T722_NoKwitansi = Kwitansi.ID and Kwitansi.T722_StaDel = '0')
        INNER JOIN M701_METODEBAYAR MetodeBayar
            ON (BookingFee.T721_M701_ID = MetodeBayar.ID and MetodeBayar.M701_StaDel = '0')
        INNER JOIN T182_HISTORYCUSTOMER HistoryCustomer
            ON (Kwitansi.T722_T182_ID = HistoryCustomer.ID and HistoryCustomer.T182_StaDel = '0')
        LEFT JOIN T401_RECEPTION Reception
            ON (Kwitansi.T722_T401_NoWO = Reception.ID and Reception.T401_StaDel = '0')
    """.intern()

    private String BASE_QUERY_KUITANSI_DETAIL = """
        SELECT
            MetodeBayar.M701_MetodeBayar as Metode,
            Bank.M702_NamaBank as NamaBank,
            MesinEdc.M704_NamaMesinEdc as Mesin,
            BookingFee.T721_NoKartu as NoKartu,
            BookingFee.T721_M703_BankCharge as BankCharge,
            BookingFee.T721_JmlhBayar as Jumlah
        FROM T721_BOOKINGFEE BookingFee
        INNER JOIN T722_KWITANSI Kwitansi
            ON (BookingFee.T721_T722_NoKwitansi = Kwitansi.ID and Kwitansi.T722_StaDel = '0')
        INNER JOIN M701_METODEBAYAR MetodeBayar
            ON (BookingFee.T721_M701_ID = MetodeBayar.ID and MetodeBayar.M701_StaDel = '0')
        LEFT JOIN M702_BANK Bank
            ON (BookingFee.T721_M702_ID = Bank.ID and Bank.M702_staDel = '0')
        LEFT JOIN M704_MESINEDC MesinEdc
            ON (BookingFee.T721_M704_ID = MesinEdc.ID and MesinEdc.M704_StaDel = '0')
    """.intern()

    private String BASE_QUERY_REFUND = """
        SELECT
            TO_CHAR(Refund.T706_TgJamlRefund, 'DD-MM-YYYY') as TanggalRefund,
            Refund.T706_NoRefund as NoRefund,
            Refund.T706_JenisRefund as JenisRefund,
            Refund.T706_NomorKwitansi as NoKuitansi,
            Refund.T706_NomorInvoice as NoInvoice,
            Refund.T706_NomorWO as NomorWO,
            HistoryCustomer.T182_ID as NoCustomer,
            Refund.T706_NamaCustomer as Customer,
            Refund.T706_Nopol as NoPol,
            Refund.T706_StaCashTransfer as StatusCashTransfer,
            Bank.M702_NamaBank as NamaBank,
            Refund.T706_NamaAccount as AcctName,
            Refund.T706_NoAccount as AcctNo,
            AlasanRefund.M071_AlasanRefund as AlasanRefund,
            Refund.T706_JmlRefund as JmlRefund,
            Refund.CREATED_BY as Kasir,
            Refund.T706_AlasanRefundLainnya
        FROM T706_REFUND Refund
        LEFT JOIN M071_ALASANREFUND AlasanRefund
            ON (Refund.T706_M071_ID = AlasanRefund.ID and AlasanRefund.M071_StaDel = '0')
        INNER JOIN T182_HISTORYCUSTOMER HistoryCustomer
            ON (Refund.T706_T182_ID = HistoryCustomer.ID and HistoryCustomer.T182_StaDel = '0')
        LEFT JOIN M702_BANK Bank
            ON (Refund.T706_M702_ID = Bank.ID and Bank.M702_staDel = '0')
    """.intern()

    /* Process SQL Statement */
    private def processQuery(def queryString, def queryParam, def resultOffset, def resultMax) {
        def resultList = []
        try {
            //println 'Query : ' + queryString
            //println 'Parameter : ' + queryParam
            def sql = new Sql(dataSource)
            if (queryParam) {
                resultList = sql.rows(queryString.toString(), queryParam, resultOffset, resultMax)
            } else {
                resultList = sql.rows(queryString.toString(), resultOffset, resultMax)
            }
        } catch (Exception e) {
            log.error(e.getMessage())
        }
        return resultList
    }

    /* Process SQL Statement */
    private def processQueryTotal(def queryString, def queryParam) {
        def resultList = []
        try {
            //println 'Query : ' + queryString
            //println 'Parameter : ' + queryParam
            def sql = new Sql(dataSource)
            if (queryParam) {
                resultList = sql.rows(queryString.toString(), queryParam)
            } else {
                resultList = sql.rows(queryString.toString())
            }
        } catch (Exception e) {
            log.error(e.getMessage())
        }
        return resultList
    }

    /* Combo Box - Get Metode Bayar List */
    def getMetodeBayarList() {
        def metodeBayar = MetodeBayar.createCriteria().list(){eq("m701StaDel","0");order("m701MetodeBayar")}
        return metodeBayar
    }
    /* Combo Box - Get Tipe Invoice List */
    def getTipeInvoiceList() {
        def result = [[tipeKey: 'T', tipeValue: 'TUNAI'], [tipeKey: 'K', tipeValue: 'KREDIT']]
        return result
    }
    /* Combo Box - Get Customer SPK List */
    def getCustomerSpkList() {
        def result = [[tipeKey: '0', tipeValue: 'YA'], [tipeKey: '1', tipeValue: 'TIDAK']]
        return result
    }
    /* Combo Box - Get Jenis Bayar List */
    def getJenisBayarList() {
        def result = [[tipeKey: '0', tipeValue: 'BOOKING FEE PARTS'] , [tipeKey: '1', tipeValue: 'BOOKING FEE SERVICE'], [tipeKey: '2', tipeValue: 'ON RISK']]
        return result
    }
    /* Combo Box - Get Metode Refund List */
    def getMetodeRefundList() {
        def result = [[tipeKey: '0', tipeValue: 'CASH'], [tipeKey: '1', tipeValue: 'TRANSFER']]
        return result
    }
    /* Combo Box - Get Jenis Refund List */
    def getJenisRefundList() {
        def result = [[tipeKey: '0', tipeValue: 'BOOKING FEE PART'], [tipeKey: '1', tipeValue: 'BOOKING FEE SERVICE'], [tipeKey: '2', tipeValue: 'ON RISK']]
        return result
    }
    /* Combo Box - Get Alasan Refund List */
    def getAlasanRefundList() {
        def alasanRefund = AlasanRefund.createCriteria().list {eq("staDel","0");order("m071AlasanRefund")}
        return alasanRefund
    }
    /* Combo Box - Get Bank List */
    def getBankList() {
        def bank = BankAccountNumber.createCriteria().list {eq("staDel","0");bank{order("m702NamaBank")}}
        return bank
    }
    /* Combo Box - Get Mesin EDC List */
    def getMesinEDCList() {
        def mesin = MesinEdc.createCriteria().list {eq("staDel","0");order("m704NamaMesinEdc")}
        return mesin
    }

    /* Ajax - Get Rate */
    def getRate(def params) {
        def namaBank = params.namaBank
        def bankInstance = Bank.findByM702ID(namaBank)

        def mesinEDC = params.mesinEDC
        def mesinInstance = MesinEdc.findByM704Id(mesinEDC)

        def rateInstance = RateBank.findByBankAndMesinEdc(bankInstance,mesinInstance)

        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['rateBank'] = rateInstance

        if(rateInstance){
            return rateInstance
        }else{
            return [m703RatePersen: 0]
        }
    }

    /* Get Invoice List */
    def getInvoiceList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        filters << " WHERE Invoice.T701_StaDel = '0' AND Invoice.COMPANY_DEALER_ID = " + params.companyDealer

        def paramsFilter = [:]

        //tanggal invoice
        if (params."chkTanggalInvoice" && params."tanggalInvoiceStart" && params."tanggalInvoiceEnd") {
            def tanggalInvoiceStart = params."tanggalInvoiceStart"
            def tanggalInvoiceEnd = params."tanggalInvoiceEnd"
            filters << "(Invoice.T701_TglJamInvoice BETWEEN TO_DATE(:invoiceStart) AND TO_DATE(:invoiceEnd))"
            paramsFilter.invoiceStart = tanggalInvoiceStart.format("dd-MMM-YY")
            paramsFilter.invoiceEnd = tanggalInvoiceEnd.format("dd-MMM-YY")
        }

        //tanggal settlement
        if (params."chkTanggalSettlement" && params."tanggalSettlementStart" && params."tanggalSettlementEnd") {
            def tanggalSettlementStart = params."tanggalSettlementStart"
            def tanggalSettlementEnd = params."tanggalSettlementEnd"
            filters << "(Settlement.T704_TglJamSettlement BETWEEN TO_DATE(:settlementStart) AND TO_DATE(:settlementEnd))"
            paramsFilter.settlementStart = tanggalSettlementStart.format("dd-MMM-YY")
            paramsFilter.settlementEnd = tanggalSettlementEnd.format("dd-MMM-YY")
        }

        //status invoice (SEMUA, CLOSING, OUTSTANDING)
        if (params."chkStatusInvoice" && params."statusInvoice") {
            def statusInvoice = params."statusInvoice"
            if ("CLOSE".equalsIgnoreCase(statusInvoice)) {
                filters << "(Settlement.T704_T701_NoInv IS NOT NULL)"
            } else if ("OPEN".equalsIgnoreCase(statusInvoice)) {
                filters << "(Settlement.T704_T701_NoInv IS NULL)"
            }
        }

        //customer SPK (SEMUA, YA, TIDAK)
        if (params."chkCustomerSPK" && params."customerSPK") {
            def customerSPK = params."customerSPK"
            if ("SEMUA".equalsIgnoreCase(customerSPK)) {
                filters << "(Reception.T401_StaKtgrCustUmumSPKAsrnsi IN (:statusSPKYa, :statusSPKTidak))"
                paramsFilter.statusSPKYa = '0'
                paramsFilter.statusSPKTidak = '1'
            } else {
                filters << "(Reception.T401_StaKtgrCustUmumSPKAsrnsi = :customerSPK)"
                paramsFilter.customerSPK = customerSPK
            }
        }

        //metode bayar (SEMUA / lookup MetodeBayar)
        if (params."chkMetodeBayar" && params."metodeBayar") {
            def metodeBayar = params."metodeBayar"
            if ("SEMUA".equalsIgnoreCase(metodeBayar)) {
                filters << "(Settlement.T704_M701_ID IN (select metode.M701_ID from M701_METODEBAYAR as metode))"
            } else {
                filters << "(Settlement.T704_M701_ID = :metodeBayar)"
                paramsFilter.metodeBayar = metodeBayar
            }
        }

        //nomor invoice
        if (params."chkNomorInvoice" && params."nomorInvoice") {
            def nomorInvoice = params."nomorInvoice"
            filters << "(Invoice.T701_NoInv LIKE(:nomorInvoice))"
            paramsFilter.nomorInvoice = '%'+nomorInvoice+'%'
        }

        //jenis invoice
        if (params."chkJenisInvoice" && params."jenisInvoice") {
            def jenisInvoice = params."jenisInvoice"
            if ("SEMUA".equalsIgnoreCase(jenisInvoice)) {
                filters << "(Invoice.T701_TIPE IN (:jenisInvoiceGR, :jenisInvoiceBP))"
                paramsFilter.jenisInvoiceGR = '0'
                paramsFilter.jenisInvoiceBP = '1'
            } else {
                filters << "(Invoice.T701_TIPE = :jenisInvoice)"
                paramsFilter.jenisInvoice = jenisInvoice
            }
        }


        //tipe invoice
        if (params."chkTipeInvoice" && params."tipeInvoice") {
            def tipeInvoice = params."tipeInvoice"
            if ("SEMUA".equalsIgnoreCase(tipeInvoice)) {
                filters << "(Invoice.T701_JENISINV IN (:tipeInvoiceTunai, :tipeInvoiceKredit))"
                paramsFilter.tipeInvoiceTunai = 'T'
                paramsFilter.tipeInvoiceKredit = 'K'
            } else {
                filters << "(Invoice.T701_JENISINV = :tipeInvoice)"
                paramsFilter.tipeInvoice = tipeInvoice
            }
        }

        //nama customer
        if (params."chkCustomerName" && params."customerName") {
            def customerName = params."customerName"
            filters << "(Invoice.T701_Customer LIKE(:customerName))"
            paramsFilter.customerName = '%'+customerName+'%'
        }

        //aging
        if (params."chkAging" && params."startAging" && params."endAging") {
            def startAging = params."startAging"
            def endAging = params."endAging"
            filters << "(DATEDIFF('DAY',SYSDATE,Invoice.T701_DueDate) BETWEEN TO_DATE(:agingStart) AND TO_DATE(:agingEnd))"
            paramsFilter.agingStart = startAging.format("dd-MMM-YY")
            paramsFilter.agingEnd = endAging.format("dd-MMM-YY")
        }

        //nomor polisi
        if (params."chkNomorPolisi" && params."nomorPolisi") {
            def nomorPolisi = params."nomorPolisi"
            filters << "(Invoice.T701_NOPOL LIKE(:nomorPolisi))"
            paramsFilter.nomorPolisi = '%'+nomorPolisi+'%'
        }

        def query = new StringBuilder(BASE_QUERY_INVOICE_MASTER)

        def invoiceList = []
        def inv = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int
        offset = offset+1
        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            query.append(" ORDER BY Invoice.T701_NoInv DESC")
            invoiceList = processQuery(query, paramsFilter, offset, max)
            inv = processQueryTotal(query, paramsFilter)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            query.append(" ORDER BY Invoice.T701_NoInv DESC")
            invoiceList = processQuery(query, null, offset, max)
            inv = processQueryTotal(query, null)
        }

        def rows = []
        invoiceList?.each {
            def status = (it[9])?'CLOSE':'OPEN'
            def metode = "-"
            try {
                if(it[10]?.toString()?.contains("K")){
                    def col = Collection.findByStaDelAndInvoiceT071('0',InvoiceT701.findByT701NoInv(it[2])).paymentStatus
                    if(col=='LUNAS'){
                        status = 'CLOSE'
                    }
                }else{
                    def set = Settlement?.findByInvoiceAndStaDel(InvoiceT701.findByT701NoInv(it[2]),"0")
                    if(set){
                        metode = set?.metodeBayar.m701MetodeBayar
                        if(set.bank){
                            metode += " - " + (set.bank.m702NamaBank+" "+set.bank.m702Cabang)
                        }
                    }
                }
            }catch (Exception e){

            }
            if(InvoiceT701.findByT701NoInv(it[2]).t701NoInv_Reff){
                status = "BATAL"
            }else if(InvoiceT701.findByT701NoInv(it[2]).getT701StaApprovedReversal()=="0"){
                status = "BATAL"
            }
            rows << [
                tanggalInvoice: it[0],
                noInvoice     : it[2],
                nomorWO       : it[3],
                nomorPolisi   : it[4],
                namaCustomer  : it[5],
                jenisInvoice  : (it[6]?.toString()?.contains("0"))?'GR':'BP',
                spk           : ('0'.equals(it[7]))?'YA':'TIDAK',
                totalInvoice  : conversi.toRupiah(it[8]),
                status        : status,
                tipeInv       : (it[10]?.toString()?.contains("T"))?'TUNAI':'KREDIT',
                jmlPrint      : (it[11])?(it[11])?.toString():0?.toString(),
                metode        : metode
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = inv.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }
    def getInvoiceSubList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        def paramsFilter = [:]

        def id = params.id
        filters << " WHERE Invoice.T701_StaDel = '0' AND Invoice.T701_NoInv LIKE '"+id+"%' "
        paramsFilter.invoiceNo = id

//        //tanggal invoice
//        if (params."chkTanggalInvoice" && params."tanggalInvoiceStart" && params."tanggalInvoiceEnd") {
//            def tanggalInvoiceStart = params."tanggalInvoiceStart"
//            def tanggalInvoiceEnd = params."tanggalInvoiceEnd"
//            filters << "(Invoice.T701_TglJamInvoice BETWEEN TO_DATE(:invoiceStart) AND TO_DATE(:invoiceEnd))"
//            paramsFilter.invoiceStart = tanggalInvoiceStart.format("dd-MMM-YY")
//            paramsFilter.invoiceEnd = tanggalInvoiceEnd.format("dd-MMM-YY")
//        }
//
//        //tanggal settlement
//        if (params."chkTanggalSettlement" && params."tanggalSettlementStart" && params."tanggalSettlementEnd") {
//            def tanggalSettlementStart = params."tanggalSettlementStart"
//            def tanggalSettlementEnd = params."tanggalSettlementEnd"
//            filters << "(Settlement.T704_TglJamSettlement BETWEEN TO_DATE(:settlementStart) AND TO_DATE(:settlementEnd))"
//            paramsFilter.settlementStart = tanggalSettlementStart.format("dd-MMM-YY")
//            paramsFilter.settlementEnd = tanggalSettlementEnd.format("dd-MMM-YY")
//        }
//
//        //status invoice (SEMUA, CLOSING, OUTSTANDING)
//        if (params."chkStatusInvoice" && params."statusInvoice") {
//            def statusInvoice = params."statusInvoice"
//            if ("CLOSING".equalsIgnoreCase(statusInvoice)) {
//                filters << "(Settlement.T704_T701_NoInv IS NOT NULL)"
//            } else if ("OUTSTANDING".equalsIgnoreCase(statusInvoice)) {
//                filters << "(Settlement.T704_T701_NoInv IS NULL)"
//            }
//        }
//
//        //customer SPK (SEMUA, YA, TIDAK)
//        if (params."chkCustomerSPK" && params."customerSPK") {
//            def customerSPK = params."customerSPK"
//            if ("SEMUA".equalsIgnoreCase(customerSPK)) {
//                filters << "(Reception.T401_StaKtgrCustUmumSPKAsrnsi IN (:statusSPKYa, :statusSPKTidak))"
//                paramsFilter.statusSPKYa = '0'
//                paramsFilter.statusSPKTidak = '1'
//            } else {
//                filters << "(Reception.T401_StaKtgrCustUmumSPKAsrnsi = :customerSPK)"
//                paramsFilter.customerSPK = customerSPK
//            }
//        }
//
//        //metode bayar (SEMUA / lookup MetodeBayar)
//        if (params."chkMetodeBayar" && params."metodeBayar") {
//            def metodeBayar = params."metodeBayar"
//            if ("SEMUA".equalsIgnoreCase(metodeBayar)) {
//                filters << "(Settlement.T704_M701_ID IN (select metode.M701_ID from M701_METODEBAYAR as metode))"
//            } else {
//                filters << "(Settlement.T704_M701_ID = :metodeBayar)"
//                paramsFilter.metodeBayar = metodeBayar
//            }
//        }
//
//        //nomor invoice
//        if (params."chkNomorInvoice" && params."nomorInvoice") {
//            def nomorInvoice = params."nomorInvoice"
//            filters << "(Invoice.T701_NoInv LIKE(:nomorInvoice))"
//            paramsFilter.nomorInvoice = '%'+nomorInvoice+'%'
//        }
//
//        //tipe invoice
//        if (params."chkTipeInvoice" && params."tipeInvoice") {
//            def tipeInvoice = params."tipeInvoice"
//            if ("SEMUA".equalsIgnoreCase(tipeInvoice)) {
//                filters << "(Invoice.T701_IntExt IN (:tipeInvoiceInternal, :tipeInvoiceExternal))"
//                paramsFilter.tipeInvoiceInternal = '0'
//                paramsFilter.tipeInvoiceExternal = '1'
//            } else {
//                filters << "(Invoice.T701_IntExt = :tipeInvoice)"
//                paramsFilter.tipeInvoice = tipeInvoice
//            }
//        }
//
//        //nama customer
//        if (params."chkCustomerName" && params."customerName") {
//            def customerName = params."customerName"
//            filters << "(Invoice.T701_Customer LIKE(:customerName))"
//            paramsFilter.customerName = '%'+customerName+'%'
//        }
//
//        //aging
//        if (params."chkAging" && params."startAging" && params."endAging") {
//            def startAging = params."startAging"
//            def endAging = params."endAging"
//            filters << "(DATEDIFF('DAY',SYSDATE,Invoice.T701_DueDate) BETWEEN :agingStart AND :agingEnd)"
//            paramsFilter.agingStart = startAging
//            paramsFilter.agingEnd = endAging
//        }
//
//        //nomor polisi
//        if (params."chkNomorPolisi" && params."nomorPolisi") {
//            def nomorPolisi = params."nomorPolisi"
//            filters << "(Invoice.T701_Nopol = :nomorPolisi)"
//            paramsFilter.nomorPolisi = nomorPolisi
//        }

        def query = new StringBuilder(BASE_QUERY_INVOICE_DETAIL)

        def invoiceList = []
        def inv = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            invoiceList = processQuery(query, paramsFilter, offset, max)
            inv = processQueryTotal(query, paramsFilter)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            invoiceList = processQuery(query, null, offset, max)
            inv = processQueryTotal(query, null)
        }

        def rows = []
        invoiceList?.each {
            rows << [
                tanggalSettlement: it[1],
                metodeBayar      : it[2],
                namaBank         : it[3],
                mesinEdc         : it[4],
                nomorKartu       : it[5],
                bankCharge       : it[6],
                nomorWbs         : it[7],
                jumlahSettlement : conversi.toRupiah(it[8])
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = inv.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }
    def searchInvoiceByNoInvoice(def params) {
        def invoiceInstance
        def noInvoice
        if(params?.noInvoices){
            invoiceInstance = InvoiceT701.get(params?.noInvoices?.toLong())
        }

        if(params?.noInvoice){
            noInvoice = params.noInvoice
            invoiceInstance = InvoiceT701.findByT701NoInvAndT701StaDelAndCompanyDealer(noInvoice, "0", params.companyDealers)
        }

        def receptionInstance = (Reception) invoiceInstance?.reception
        def operationInstance = (Operation) receptionInstance?.operation
        def historyCustomerVehicle = (HistoryCustomerVehicle) receptionInstance?.historyCustomerVehicle
        def customerVehicle = (CustomerVehicle) historyCustomerVehicle?.customerVehicle
        def jobSugest = JobSuggestion.findByCustomerVehicle(customerVehicle)
        def companyDealer = CompanyDealer.findById(params.companyDealer)
        def leftDetails = []
        def leftTotal = invoiceInstance?.t701JasaRp ? invoiceInstance?.t701JasaRp : 0
        def discJasa = 0
        def staFreeJasa = false
        def staFree = false
        def daftarJob = ""
        def jobInvoice = JobInv.findAllByInvoiceAndStaDel(invoiceInstance, "0")

        jobInvoice.each {
            def jb = JobRCP.findByReceptionAndOperationAndStaDel(it?.reception,it?.operation,"0");
            if(!jb?.t402namaBillTo){
                staFreeJasa = true
                daftarJob += it?.operation?.m053NamaOperation + ","
            }
            discJasa += jb?.t402DiscRp ? jb?.t402DiscRp : 0

            leftDetails << [
                desc: it?.operation?.m053NamaOperation,
                rate: jb?.t402Rate,
                rupiah: conversi.toRupiah(it?.t702TotalRp)
            ]
        }
        if(jobInvoice.size()<1){
            def jbs = JobRCP.createCriteria().list {
                eq("reception", invoiceInstance.reception)
                eq("staDel","0")
                or{
                    ilike("t402StaTambahKurang","0")
                    and{
                        not{
                            ilike("t402StaTambahKurang","%1%")
                            ilike("t402StaApproveTambahKurang","%1%")
                        }
                    }
                    and {
                        isNull("t402StaTambahKurang")
                        isNull("t402StaApproveTambahKurang")
                    }
                }
            }
            jbs.each {
                staFreeJasa = true
                staFree = true
                daftarJob += it?.operation?.m053NamaOperation + ","
            }
        }
        try {
            if(staFreeJasa == true){
                def jbs = JobRCP.createCriteria().list {
                    eq("reception", invoiceInstance.reception)
                    eq("staDel","0")
                    or{
                        ilike("t402StaTambahKurang","0")
                        and{
                            not{
                                ilike("t402StaTambahKurang","%1%")
                                ilike("t402StaApproveTambahKurang","%1%")
                            }
                        }
                        and {
                            isNull("t402StaTambahKurang")
                            isNull("t402StaApproveTambahKurang")
                        }
                    }
                }

                jbs.each {
                    if(staFree == true){
                        if( it.operation.m053NamaOperation.toUpperCase().contains("SB")){
                            leftDetails << [
                                    desc : it?.operation?.m053NamaOperation + " (Free Jasa)",
                                    rate : it?.t402Rate,
                                    rupiah : conversi.toRupiah(0)
                            ]
                        }
                    }else{
                        if(!daftarJob.toUpperCase().contains(it.operation.m053NamaOperation.toUpperCase())
                                && it.operation.m053NamaOperation.toUpperCase().contains("SB")){
                            leftDetails << [
                                    desc : it?.operation?.m053NamaOperation + " (Free Jasa)",
                                    rate : it?.t402Rate,
                                    rupiah : conversi.toRupiah(0)
                            ]
                        }
                    }
                }
            }
        }catch (Exception e){}
        def nPart = ""
        def partSlip = PartInv.createCriteria().list {
            invoice{
                eq("id", invoiceInstance?.id)
            }
            eq("t703StaDel", "0")

            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t703NoPartInv", "t703NoPartInv")
            }
            order("t703NoPartInv")
        }

        int ind=1

        partSlip.each {
            if(ind==partSlip.size()){
                nPart+=it?.t703NoPartInv
            }else{
                nPart+=it?.t703NoPartInv+", "
            }
            ind++;
        }

        def p = PartInv.findByInvoiceAndT703StaDel(invoiceInstance, "0")

        def discPart = 0
        def pr = PartsRCP.findAllByReceptionAndStaDel(p?.invoice?.reception, "0")
        pr.each {
            discPart += (it?.t403DiscRp ? it?.t403DiscRp : 0)
        }

        def KB = NamaManPower.createCriteria().get {
            eq("staDel","0");
            eq("companyDealer", params.companyDealers)
            manPowerDetail{
                manPower{
                    or{
                        eq("m014JabatanManPower","KEPALA BENGKEL",[ignoreCase: true])
                        eq("m014Inisial","KBNG",[ignoreCase: true])
                    }
                }
            }
            maxResults(1);
        }
        def row = [
                namaWorkshop: companyDealer?.m011NamaWorkshop,
                alamatWorkshop: companyDealer?.m011Alamat,
                teleponWorkshop: companyDealer?.m011Telp,
                noInvoice: invoiceInstance?.t701NoInv,
                npwpPenjual: companyDealer?.m011NPWP,
                pkp: companyDealer?.m011PKP,
                pelanggan: invoiceInstance?.t701Customer,
                alamatPelanggan: invoiceInstance?.t701Alamat,
                npwpPelanggan: invoiceInstance?.t701NPWP,
                nomorPolisi: invoiceInstance?.t701Nopol,
                model: historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                nomorMesin: historyCustomerVehicle?.t183NoMesin,
                nomorRangka: customerVehicle?.t103VinCode,
                nomorOrder: receptionInstance?.t401NoWO,
                serviceAdv: receptionInstance?.t401NamaSA,
                intExt: invoiceInstance?.t701IntExt = "1" ? "EXT" : "INT",
                teknisi: invoiceInstance?.t701Teknisi,
                tanggalCetak: invoiceInstance?.t701TglJamCetak ? invoiceInstance?.t701TglJamCetak?.format("dd/mm/yy HH:mm") : "-",
                admBilling: invoiceInstance?.t701AdmBilling,
                detailsJasa: leftDetails,
                subTotalJasa: conversi.toRupiah(leftTotal),

                jasaLabor: conversi.toRupiah(leftTotal),
                sukuCadang: conversi.toRupiah(invoiceInstance?.t701PartsRp),
                oil: conversi.toRupiah(invoiceInstance?.t701OliRp),
                material: conversi.toRupiah(invoiceInstance?.t701MaterialRp),
                sublet: conversi.toRupiah(invoiceInstance?.t701SubletRp),
                adm : conversi.toRupiah(invoiceInstance?.t701AdmRp),

                subTot1: conversi.toRupiah(invoiceInstance?.t701SubTotalRp),
                discJasa: conversi.toRupiah(discJasa),
                discPart: conversi.toRupiah(discPart),

                subTot2: conversi.toRupiah(invoiceInstance?.t701TotalRp),
                ppn: conversi.toRupiah(invoiceInstance?.t701PPnRp),
                materai: conversi.toRupiah(invoiceInstance?.t701MateraiRp),

                totalInvoice: conversi.toRupiah(invoiceInstance?.t701TotalInv),
                bookingFee: conversi.toRupiah(invoiceInstance?.t701BookingFee),

                totalBayar: conversi.toRupiah(invoiceInstance?.t701TotalBayarRp),
                terbilang: invoiceInstance?.t701TotalInvTerbilang,
                supply: nPart,
                catatan: invoiceInstance?.t701Catatan,
                saran: invoiceInstance?.t701Catatan,
                keluhan: companyDealer?.m011Telp,
                kaBeng: KB?.t015NamaLengkap ? KB?.t015NamaLengkap : "-"
        ]

        return row
    }

    /* Get Parts Slip List */
    def getPartSlipList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        filters << " WHERE PartInv.T703_StaDel = '0' AND Invoice.COMPANY_DEALER_ID = " + params.companyDealer +
                   " AND GroupPart.M117_NAMAFRANC IN ('Parts Toyota', 'Parts Campuran', 'Material', 'Bahan', 'Cat', 'Accesories') "

        def paramsFilter = [:]

        //tanggal parts
        if (params."chkTanggalParts" && params."tanggalStart" && params."tanggalEnd") {
            def tanggalStart = params."tanggalStart"
            def tanggalEnd = params."tanggalEnd"
            filters << "(Invoice.T701_TglJamInvoice BETWEEN TO_DATE(:tanggalStart) AND TO_DATE(:tanggalEnd))"
            paramsFilter.tanggalStart = tanggalStart.format("dd-MMM-YY")
            paramsFilter.tanggalEnd = tanggalEnd.format("dd-MMM-YY")
        }

        //nomor parts
        if (params."chkNomorParts" && params."nomorParts") {
            def nomorParts = params."nomorParts"
            filters << "(PartInv.T703_NoPartInv LIKE(:nomorParts))"
            paramsFilter.nomorParts = '%'+nomorParts+'%'
        }

        //nomor WO
        if (params."chkNomorWO" && params."nomorWO") {
            def nomorWO = params."nomorWO"
            filters << "(Rec.T401_NOWO LIKE(:nomorWO))"
            paramsFilter.nomorWO = '%'+nomorWO+'%'
        }

        //nomor polisi
        if (params."chkNomorPolisi" && params."nomorPolisi") {
            def nomorPolisi = params."nomorPolisi"
            filters << "(Invoice.T701_Nopol LIKE(:nomorPolisi))"
            paramsFilter.nomorPolisi = '%'+nomorPolisi+'%'
        }

        def query = new StringBuilder(BASE_QUERY_PARTS)

        def partList = []
        def partListSize = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int


        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            query.append(" GROUP BY PartInv.T703_NoPartInv, Invoice.T701_TglJamInvoice, Rec.T401_NOWO, Invoice.T701_Nopol, Invoice.T701_Customer, Invoice.T701_Alamat, Invoice.T701_NoInv")
            query.append(" ORDER BY TanggalPartsSlip, PartInv.T703_NoPartInv, Rec.T401_NOWO")
            partList = processQuery(query, paramsFilter, offset, max)
            partListSize = processQueryTotal(query, paramsFilter)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            query.append(" GROUP BY PartInv.T703_NoPartInv, Invoice.T701_TglJamInvoice, Rec.T401_NOWO, Invoice.T701_Nopol, Invoice.T701_Customer, Invoice.T701_Alamat, Invoice.T701_NoInv")
            query.append(" ORDER BY TanggalPartsSlip, PartInv.T703_NoPartInv, Rec.T401_NOWO")
            partList = processQuery(query, null, offset, max)
            partListSize = processQueryTotal(query, null)
        }

        def rows = []
        partList?.each {
            rows << [
                tanggalPartsSlip: it[0],
                noPartsSlip     : it[1],
                nomorWO         : it[2],
                noPol           : it[3],
                customer        : it[4],
                alamat          : it[5],
                grandTotal      : conversi.toRupiah(it[6]),
                noInvoice       : it[7]
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = partListSize.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords


        return dataToRender
    }
    def getPartSlipSubList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        def paramsFilter = [:]

        def id = params.id
        def noInvoice = params.noInvoice
        filters << " WHERE PartInv.T703_StaDel = '0' AND PartInv.COMPANY_DEALER_ID = " + params.companyDealer +
                   " AND GroupPart.M117_NAMAFRANC IN ('Parts Toyota', 'Parts Campuran', 'Material', 'Bahan', 'Cat', 'Accesories')" +
                   " AND PartInv.T703_NoPartInv = '"+id+"'" +
                   " AND Invoice.T701_NoInv LIKE '"+noInvoice+"%'"
        paramsFilter.partsInvNo = id

        //tanggal parts
//        if (params."chkTanggalParts" && params."tanggalStart" && params."tanggalEnd") {
//            def tanggalStart = params."tanggalStart"
//            def tanggalEnd = params."tanggalEnd"
//            filters << "(Invoice.T701_TglJamInvoice BETWEEN TO_DATE(:tanggalStart) AND TO_DATE(:tanggalEnd))"
//            paramsFilter.tanggalStart = tanggalStart.format("dd-MMM-YY")
//            paramsFilter.tanggalEnd = tanggalEnd.format("dd-MMM-YY")
//        }
//
//        //nomor parts
//        if (params."chkNomorParts" && params."nomorParts") {
//            def nomorParts = params."nomorParts"
//            filters << "(PartInv.T703_NoPartInv LIKE(:nomorParts))"
//            paramsFilter.nomorParts = '%'+nomorParts+'%'
//        }
//
//        //nomor WO
//        if (params."chkNomorWO" && params."nomorWO") {
//            def nomorWO = params."nomorWO"
//            filters << "(Rec.T401_NOWO LIKE(:nomorWO))"
//            paramsFilter.nomorWO = '%'+nomorWO+'%'
//        }
//
//        //nomor polisi
//        if (params."chkNomorPolisi" && params."nomorPolisi") {
//            def nomorPolisi = params."nomorPolisi"
//            filters << "(Invoice.T701_Nopol LIKE(:nomorPolisi))"
//            paramsFilter.nomorPolisi = '%'+nomorPolisi+'%'
//        }

        def query = new StringBuilder(BASE_QUERY_PARTS_GROUP)

        def partsSubSubList = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            query.append(" GROUP BY PartInv.T703_NoPartInv, GroupPart.ID, GroupPart.M117_NAMAFRANC, Invoice.ID")
            partsSubSubList = processQuery(query, paramsFilter, offset, max)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            query.append(" GROUP BY PartInv.T703_NoPartInv, GroupPart.ID, GroupPart.M117_NAMAFRANC, Invoice.ID")
            partsSubSubList = processQuery(query, null, offset, max)
        }

        def rows = []
        partsSubSubList?.each {
            rows << [
                noPartsSlip: it[0],
                goodId     : it[1],
                groupGood  : it[2],
                subTotal   : conversi.toRupiah(it[3]),
                invId      : it[4]
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = rows.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }
    def getPartSlipSubSubList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        def paramsFilter = [:]

        def id = params.id
        def group = params.group
        def invId = params.invId
        filters << " WHERE PartInv.T703_StaDel = '0' AND PartInv.COMPANY_DEALER_ID = " + params.companyDealer +
                   " AND GroupPart.M117_NAMAFRANC IN ('Parts Toyota', 'Parts Campuran', 'Material', 'Bahan', 'Cat', 'Accesories') " +
                   " AND PartInv.T703_NoPartInv = '"+id.toString().trim()+"' " +
                   " AND GroupPart.ID = "+group+" AND PartInv.T703_T701_NoInv = "+invId
        paramsFilter.partsInvNo = id
        paramsFilter.groupId = group

        //tanggal parts
//        if (params."chkTanggalParts" && params."tanggalStart" && params."tanggalEnd") {
//            def tanggalStart = params."tanggalStart"
//            def tanggalEnd = params."tanggalEnd"
//            filters << "(Invoice.T701_TglJamInvoice BETWEEN TO_DATE(:tanggalStart) AND TO_DATE(:tanggalEnd))"
//            paramsFilter.tanggalStart = tanggalStart.format("dd-MMM-YY")
//            paramsFilter.tanggalEnd = tanggalEnd.format("dd-MMM-YY")
//        }
//
//        //nomor parts
//        if (params."chkNomorParts" && params."nomorParts") {
//            def nomorParts = params."nomorParts"
//            filters << "(PartInv.T703_NoPartInv LIKE(:nomorParts))"
//            paramsFilter.nomorParts = '%'+nomorParts+'%'
//        }
//
//        //nomor WO
//        if (params."chkNomorWO" && params."nomorWO") {
//            def nomorWO = params."nomorWO"
//            filters << "(Rec.T401_NOWO LIKE(:nomorWO))"
//            paramsFilter.nomorWO = '%'+nomorWO+'%'
//        }
//
//        //nomor polisi
//        if (params."chkNomorPolisi" && params."nomorPolisi") {
//            def nomorPolisi = params."nomorPolisi"
//            filters << "(Invoice.T701_Nopol LIKE(:nomorPolisi))"
//            paramsFilter.nomorPolisi = '%'+nomorPolisi+'%'
//        }

        def query = new StringBuilder(BASE_QUERY_PARTS_DETAIL)

        def partsSubSubList = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            partsSubSubList = processQuery(query, paramsFilter, offset, max)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            partsSubSubList = processQuery(query, null, offset, max)
        }

        def rows = []
        partsSubSubList?.each {
            rows << [
                partNo   : it[0],
                partName : it[1],
                qty      : it[2],
                unitPrice: conversi.toRupiah(it[3]),
                discount : it[4],
                extAmount: conversi.toRupiah(it[5]),
                remark   : it[6]
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = rows.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }
    def searchPartsSlipByNomorPartsSlip(def params){

        def partInv = params.noPartsSlip

        def partsInstance = PartInv.findAllByT703NoPartInvIlikeAndCompanyDealer(partInv, params.companyDealer)

        def invoiceInstance = InvoiceT701.findByT701NoInvIlikeAndCompanyDealer(partsInstance[0]?.invoice?.t701NoInv, params.companyDealer)

        def details = []
        int i = 0;
        def total = 0
        partsInstance.each {
            i++
            def extAmount = (it?.t703Jumlah1 * it?.t703HargaRp) - it?.t703DiscRp
            total+=extAmount
            details << [
                no:i,
                partNo: it?.goods?.m111ID,
                partName: it?.goods?.m111Nama,
                qty: it?.t703Jumlah1,
                unitPrice: conversi.toRupiah(it?.t703HargaRp),
                discount: conversi.toRupiah(it?.t703DiscRp),
                extAmount: conversi.toRupiah(extAmount),
                remark: it?.t703Remark
            ]
        }

        def row = [
            nomorPolisi: invoiceInstance?.t701Nopol,
            customer: invoiceInstance?.t701Customer,
            addess: invoiceInstance?.t701Alamat,
            nomorWO: invoiceInstance?.reception?.t401NoWO,
            slipNo: partInv,
            tanggal: invoiceInstance?.t701TglJamInvoice,
            details:details,
            total: total
        ]

        return row
    }

    /* Get Faktur Pajak List */
    def getFakturPajakList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        filters << " WHERE FakturPajak.T711_StaDel = '0' AND FakturPajak.T711_JenisFP = '1'"

        def paramsFilter = [:]

        //tanggal faktur
        if (params."chkTanggalFaktur" && params."tanggalFakturStart" && params."tanggalFakturEnd") {
            def tanggalFakturStart = params."tanggalFakturStart"
            def tanggalFakturEnd = params."tanggalFakturEnd"
            filters << "(FakturPajak.T711_TglJamFP BETWEEN TO_DATE(:fakturStart) AND TO_DATE(:fakturEnd))"
            paramsFilter.fakturStart = tanggalFakturStart.format("dd-MMM-YY")
            paramsFilter.fakturEnd = tanggalFakturEnd.format("dd-MMM-YY")
        }

        //kategori & kata kunci
        if (params."chkKategoriFaktur" && params."kategoriFaktur" && params."chkKataKunci" && params."kataKunci") {
            def kategori = params."kategoriFaktur"
            def kataKunci = params."kataKunci"
            if ("NOMOR FAKTUR".equalsIgnoreCase(kategori)) {
                filters << "(FakturPajak.T711_NoFakturPajak = :noFaktur)"
                paramsFilter.noFaktur = kataKunci
            } else if ("NOMOR INVOICE".equalsIgnoreCase(kategori)) {
                filters << "(Invoice.T701_NoInv = :noInvoice)"
                paramsFilter.noInvoice = kataKunci
            } else if ("CUSTOMER".equalsIgnoreCase(kategori)) {
                filters << "(HistoryCustomer.T182_T102_ID = :noCustomer)"
                paramsFilter.noCustomer = kataKunci
            }
        }

        def query = new StringBuilder(BASE_QUERY_FAKTUR)

        def fakturList = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            fakturList = processQuery(query, paramsFilter, offset, max)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            fakturList = processQuery(query, null, offset, max)
        }

        def rows = []
        fakturList?.each {
            rows << [
                tanggalFaktur : it[0],
                noFaktur      : it[1],
                invoice       : it[2],
                penjual       : it[3],
                alamatPenjual : it[4],
                npwpPenjual   : it[5],
                penerima      : it[6],
                alamatPenerima: it[7],
                npwpPenerima  : it[8],
                jasa          : it[9],
                parts         : it[10],
                dpp           : it[11],
                ppn           : it[12]
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = rows.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }

    /* Get Nota Retur List */
    def getNotaReturList(def params) {

        FakturPajak.findAll().each {
            //println it as JSON
        }
        //println FakturPajak.findAll().size()

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        filters << " WHERE FakturPajak.T711_StaDel = '0' AND FakturPajak.T711_JenisFP IN ('2','3')"

        def paramsFilter = [:]

        //tanggal nota
        if (params."chkTanggalNota" && params."tanggalNotaStart" && params."tanggalNotaEnd") {
            def tanggalNotaStart = params."tanggalNotaStart"
            def tanggalNotaEnd = params."tanggalNotaEnd"
            filters << "(FakturPajak.T711_TglJamFP BETWEEN TO_DATE(:notaStart) AND TO_DATE(:notaEnd))"
            paramsFilter.notaStart = tanggalNotaStart.format("dd-MMM-YY")
            paramsFilter.notaEnd = tanggalNotaEnd.format("dd-MMM-YY")
        }

        //kategori & kata kunci
        if (params."chkKategoriNota" && params."kategoriNota" && params."chkKataKunci" && params."kataKunci") {
            def kategori = params."kategoriNota"
            def kataKunci = params."kataKunci"
            if ("NOMOR NOTA".equalsIgnoreCase(kategori)) {
                filters << "(FakturPajak.T711_NoFakturPajak = :noNota)"
                paramsFilter.noNota = kataKunci
            } else if ("NOMOR FAKTUR".equalsIgnoreCase(kategori)) {
                filters << "(FakturPajak.T711_NoFakturPajakReff = :noFaktur)"
                paramsFilter.noFaktur = kataKunci
            } else if ("NOMOR INVOICE".equalsIgnoreCase(kategori)) {
                filters << "(Invoice.T701_NoInv = :noInvoice)"
                paramsFilter.noInvoice = kataKunci
            } else if ("CUSTOMER".equalsIgnoreCase(kategori)) {
                filters << "(HistoryCustomer.T182_T102_ID = :noCustomer)"
                paramsFilter.noCustomer = kataKunci
            }
        }

        def query = new StringBuilder(BASE_QUERY_NOTA)

        def notaList = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            notaList = processQuery(query, paramsFilter, offset, max)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            notaList = processQuery(query, null, offset, max)
        }

        def rows = []
        notaList?.each {
            rows << [
                tanggalNotaRetur : it[0],
                noNotaRetur      : it[1],
                noFakturPajak    : it[2],
                penjual          : it[3],
                alamatPenjual    : it[4],
                npwpPenjual      : it[5],
                pembeli          : it[6],
                alamatPembeli    : it[7],
                npwpPembeli      : it[8],
                jasa             : it[9],
                parts            : it[10],
                totalDikembalikan: it[11],
                ppn              : it[12]
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = rows.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }

    /* Search Faktur Pajak / Nota Retur */
    def searchFakturByNoFaktur(def params) {

        def noFaktur = params.nomorFaktur
        def jenisFaktur = params.jenisFaktur

        FakturPajak.findAll().each {
            //println it as JSON
        }

        def fakturInstance = FakturPajak.findByT711NoFakturPajakAndT711JenisFPInList(noFaktur,jenisFaktur)

        def fakturPajakInstance = FakturPajak.findByT711NoFakturPajak(fakturInstance?.t711NoFakturPajakReff)

        def invoiceInstance = (InvoiceT701) fakturInstance?.invoiceT701

        def companyDealer = (CompanyDealer) fakturInstance?.companyDealer

        def row = [
            namaPembeli: invoiceInstance?.t701Customer,
            alamatPembeli: invoiceInstance?.t701Alamat,
            npwpPembeli: invoiceInstance?.t701NPWP,
            noFakturPajak: fakturInstance?.t711NoFakturPajakReff,
            tanggalFakturPajak: fakturPajakInstance?.t711TglJamFP,
            noFaktur:fakturInstance?.t711NoFakturPajak,
            tanggalFaktur:fakturInstance?.t711TglJamFP,
            namaPenjual:companyDealer?.m011NamaWorkshop,
            alamatWorkshop: companyDealer?.m011Alamat,
            npwpPenjual: companyDealer?.m011NPWP,
            tanggalPKP: fakturInstance?.t711TglKukuh,
            biayaJasa: fakturInstance?.t711BiayaJasa,
            biayaGoods: fakturInstance?.t711BiayaGoods,
            harga: fakturInstance?.t711HargaJual,
            ppn: fakturInstance?.t711PPN,
            potongan: fakturInstance?.t711PotHarga,
            uangMuka: fakturInstance?.t711UangMuka,
            dasarKenaPajak: fakturInstance?.t711DasarKenaPajak,
            invoice: invoiceInstance?.t701NoInv
        ]

        return row
    }
    def printFakturByNoFaktur(def params) {

        def noFaktur = params.nomorFaktur
        def jenisFaktur = params.jenisFaktur

        FakturPajak.findAll().each {
            //println it as JSON
        }

        def fakturInstance = FakturPajak.findByT711NoFakturPajakAndT711JenisFPInList(noFaktur,jenisFaktur)

        def fakturPajakInstance = FakturPajak.findByT711NoFakturPajak(fakturInstance?.t711NoFakturPajakReff)

        def invoiceInstance = (InvoiceT701) fakturInstance?.invoiceT701

        def companyDealer = (CompanyDealer) fakturInstance?.companyDealer

        def row = [
                noFaktur: fakturInstance?.t711NoFakturPajak,
                kodeFakturPajak: fakturInstance?.t711NoFakturPajak,
                namaPenjual: companyDealer?.m011NamaWorkshop,
                alamatPenjual: companyDealer?.m011Alamat,
                npwpPenjual: companyDealer?.m011NPWP,
                tanggalPKP: fakturInstance?.t711TglKukuh,
                namaPembeli: invoiceInstance?.t701Customer,
                alamatPembeli:invoiceInstance?.t701Alamat,
                npwpPembeli:invoiceInstance?.t701NPWP,
                hargaJasa:fakturInstance?.t711BiayaJasa,
                hargaOther: fakturInstance?.t711BiayaGoods,
                totalHargaJual: fakturInstance?.t711HargaJual,
                totalPotongan: fakturInstance?.t711PotHarga,
                totalUangMuka: fakturInstance?.t711UangMuka,
                totalKenaPajak: fakturInstance?.t711DasarKenaPajak,
                totalPPN: fakturInstance?.t711PPN,
                tanggalFaktur: fakturInstance?.t711TglJamFP
        ]

        return row
    }
    def printNotaReturByNoFaktur(def params) {

        def noFaktur = params.nomorFaktur
        def jenisFaktur = params.jenisFaktur

        FakturPajak.findAll().each {
            //println it as JSON
        }

        def fakturInstance = FakturPajak.findByT711NoFakturPajakAndT711JenisFPInList(noFaktur,jenisFaktur)

        def fakturPajakInstance = FakturPajak.findByT711NoFakturPajak(fakturInstance?.t711NoFakturPajakReff)

        def invoiceInstance = (InvoiceT701) fakturInstance?.invoiceT701

        def companyDealer = (CompanyDealer) fakturInstance?.companyDealer

        def row = [
                noFaktur: fakturInstance?.t711NoFakturPajak,
                namaPembeli: invoiceInstance?.t701Customer,
                alamatPembeli: invoiceInstance?.t701Alamat,
                kotaPembeli: '',
                npwpPembeli: invoiceInstance?.t701NPWP,
                noFakturPajak: fakturInstance?.t711NoFakturPajakReff,
                tanggalFakturPajak: fakturPajakInstance?.t711TglJamFP,
                invoiceNo:fakturInstance?.t711NoFakturPajak,
                tanggalInvoice:fakturInstance?.t711TglJamFP,
                namaPenjual:companyDealer?.m011NamaWorkshop,
                alamatPenjual: companyDealer?.m011Alamat,
                npwpPenjual: companyDealer?.m011NPWP,
                tanggalPengukuhan: fakturInstance?.t711TglKukuh,
                kwantumJasa: 0,
                hargaSatuanJasa: 0,
                hargaJualJasa: fakturInstance?.t711BiayaJasa,
                kwantumOther: 0,
                hargaSatuanOther: 0,
                hargaJualOther: fakturInstance?.t711BiayaGoods,
                total: fakturInstance?.t711HargaJual,
                ppn: invoiceInstance?.t701PPnRp
        ]

        return row
    }

    /* Get Kuitansi List */
    def getKuitansiList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        filters << " WHERE Kwitansi.COMPANY_DEALER_ID = " + params.companyDealer
//        filters << " WHERE Kwitansi.COMPANY_DEALER_ID = 13291"

        def paramsFilter = [:]

        //tanggal kuitansi
        if (params."chkTanggalKuitansi" && params."tanggalKuitansiStart" && params."tanggalKuitansiEnd") {

            def tanggalKuitansiStart = params."tanggalKuitansiStart"
            def tanggalKuitansiEnd = params."tanggalKuitansiEnd"
            filters << "(BookingFee.T721_TglJamBookingFee BETWEEN TO_DATE(:kuitansiStart) AND TO_DATE(:kuitansiEnd))"
            paramsFilter.kuitansiStart = tanggalKuitansiStart.format("dd-MMM-YY")
            paramsFilter.kuitansiEnd = tanggalKuitansiEnd.format("dd-MMM-YY")
        }

        //jenis bayar
        if (params."chkJenisBayar" && params."jenisBayar") {
            def jenisBayar = params."jenisBayar"
            if ("SEMUA".equalsIgnoreCase(jenisBayar)) {
                filters << "(Kwitansi.T722_staBookingOnRisk IN (:jenisBayarBooking, :jenisBayarOnRisk))"
                paramsFilter.jenisBayarBooking = "0"
                paramsFilter.jenisBayarOnRisk = "1"
            } else {
                filters << "(Kwitansi.T722_staBookingOnRisk = :jenisBayar)"
                paramsFilter.jenisBayar = jenisBayar
            }
        }

        //metode bayar
        if (params."chkMetodeBayar" && params."metodeBayar") {
            def metodeBayar = params."metodeBayar"
            if ("SEMUA".equalsIgnoreCase(metodeBayar)) {
                filters << "(BookingFee.T721_M701_ID IN (select metode.ID from M701_METODEBAYAR as metode))"
            } else {
                filters << "(BookingFee.T721_M701_ID = :metodeBayar)"
                paramsFilter.metodeBayar = metodeBayar
            }
        }

        //nomor kuitansi
        if (params."chkNomorKuitansi" && params."nomorKuitansi") {
            def nomorKuitansi = params."nomorKuitansi"
            filters << "(Kwitansi.T722_NoKwitansi LIKE(:nomorKuitansi))"
            paramsFilter.nomorKuitansi = '%'+nomorKuitansi+'%'
        }

        //nama kasir
        if (params."chkNamaKasir" && params."namaKasir") {
            def namaKasir = params."namaKasir"
            filters << "(Kwitansi.CREATED_BY LIKE(:namaKasir))"
            paramsFilter.namaKasir = '%'+namaKasir+'%'
        }

        def query = new StringBuilder(BASE_QUERY_KUITANSI_MASTER)

        def refundList = []
        def jmlKw =[]
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            query.append(" ORDER BY Kwitansi.T722_NoKwitansi, TanggalKwitansi")
            refundList = processQuery(query, paramsFilter, offset, max)
            jmlKw = processQueryTotal(query, paramsFilter)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            query.append(" ORDER BY Kwitansi.T722_NoKwitansi, TanggalKwitansi")
            refundList = processQuery(query, null, offset, max)
            jmlKw = processQueryTotal(query, null)
        }

        def rows = []
        refundList?.each {
            rows << [
                tanggalKuitansi: it[0],
                noKuitansi     : it[1],
                jenisBayar     : ('0'.equals(it[2]))?'Booking Fee':'On Risk',
                nomorWO        : it[3],
                noCustomer     : it[4],
                terimaDari     : it[5],
                untukPembayaran: it[6],
                totalBayar     : conversi.toRupiah(it[7]),
                kasir          : it[8]
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = jmlKw.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }
    def getKuitansiSubList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        def paramsFilter = [:]

        def id = params.id
        Date tglKui = new Date().parse("dd-MM-yyyy", params."tglKui")
        paramsFilter.noKuitansi = '%'+id+'%'
//        paramsFilter.tanggalKuitansi = tglKui.format("dd-MMM-YY")
        def tanggalKuitansi = tglKui.format("dd-MMM-YY")
        filters << " WHERE Kwitansi.T722_NoKwitansi = '"+id+"'" +
                   " AND BookingFee.T721_TglJamBookingFee BETWEEN TO_DATE('"+tanggalKuitansi+" 0:00:00', 'DD-MON-YY HH24:MI:SS') AND TO_DATE('"+tanggalKuitansi+" 23:59:59', 'DD-MON-YY HH24:MI:SS') "


        //tanggal kuitansi
        if (params."chkTanggalKuitansi" && params."tanggalKuitansiStart" && params."tanggalKuitansiEnd") {

            def tanggalKuitansiStart = params."tanggalKuitansiStart"
            def tanggalKuitansiEnd = params."tanggalKuitansiEnd"
            filters << "(BookingFee.T721_TglJamBookingFee BETWEEN TO_DATE(:kuitansiStart) AND TO_DATE(:kuitansiEnd))"
            paramsFilter.kuitansiStart = tanggalKuitansiStart.format("dd-MMM-YY")
            paramsFilter.kuitansiEnd = tanggalKuitansiEnd.format("dd-MMM-YY")
        }

        //jenis bayar
        if (params."chkJenisBayar" && params."jenisBayar") {
            def jenisBayar = params."jenisBayar"
            if ("SEMUA".equalsIgnoreCase(jenisBayar)) {
                filters << "(Kwitansi.T722_staBookingOnRisk IN (:jenisBayarBooking, :jenisBayarOnRisk))"
                paramsFilter.jenisBayarBooking = "0"
                paramsFilter.jenisBayarOnRisk = "1"
            } else {
                filters << "(Kwitansi.T722_staBookingOnRisk = :jenisBayar)"
                paramsFilter.jenisBayar = jenisBayar
            }
        }

        //metode bayar
        if (params."chkMetodeBayar" && params."metodeBayar") {
            def metodeBayar = params."metodeBayar"
            if ("SEMUA".equalsIgnoreCase(metodeBayar)) {
                filters << "(BookingFee.T721_M701_ID IN (select metode.ID from M701_METODEBAYAR as metode))"
            } else {
                filters << "(BookingFee.T721_M701_ID = :metodeBayar)"
                paramsFilter.metodeBayar = metodeBayar
            }
        }

        //nomor kuitansi
        if (params."chkNomorKuitansi" && params."nomorKuitansi") {
            def nomorKuitansi = params."nomorKuitansi"
            filters << "(Kwitansi.T722_NoKwitansi LIKE(:nomorKuitansi))"
            paramsFilter.nomorKuitansi = '%'+nomorKuitansi+'%'
        }

        //nama kasir
        if (params."chkNamaKasir" && params."namaKasir") {
            def namaKasir = params."namaKasir"
            filters << "(Kwitansi.CREATED_BY LIKE(:namaKasir))"
            paramsFilter.namaKasir = '%'+namaKasir+'%'
        }

        def query = new StringBuilder(BASE_QUERY_KUITANSI_DETAIL)

        def refundList = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            refundList = processQuery(query, paramsFilter, offset, max)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            refundList = processQuery(query, null, offset, max)
        }

        def rows = []

        refundList?.each {
            rows << [
                metodeBayar: it[0],
                namaBank   : it[1],
                mesinEdc   : it[2],
                nomorKartu : it[3],
                bankCharge : it[4],
                jumlah     : conversi.toRupiah(it[5])
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = rows.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }

    /* Module Delivery Kuitansi */
    def searchKuitansiByNomorWO(def params) {
        def noWO = params.nomorWO

        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailPembayaran'] = []
        currentRequest.session['totalPembayaran'] = 0.0
        currentRequest.session['receptionInstance'] = null
        currentRequest.session['historyCustomerInstance'] = null
        currentRequest.session['customerInstance'] = null
        currentRequest.session['companyDealerInstance'] = null
        def appoint = null;
        def recept = null;

        if(params.jenisBayar=="0"){
            def cari = Appointment.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer", params.companyDealer)
                reception{
                    ilike("t401NoAppointment","%"+params.nomorWO)
                }
            }
            appoint = cari?.size()>0 ? cari?.last().reception :  Reception.findByT401NoWOIlikeAndStaDelAndCompanyDealer("%"+params.nomorWO+"%","0", params.companyDealer)
        }else {
            recept = Reception.findByT401NoWOIlikeAndStaDelAndCompanyDealer("%"+params.nomorWO+"%","0", params.companyDealer)
        }
        def receptionInstance = params.jenisBayar=="0"?appoint:recept

        def historyCustomerInstance = receptionInstance?.historyCustomer

        def customerInstance = historyCustomerInstance?.customer

        def companyDealer = customerInstance?.company?.companyDealer

        def jum = PartsApp.executeQuery("select sum(t303DPRpP) from PartsApp where reception = ${receptionInstance?.id}")

        def kw = Kwitansi.findByReceptionAndT722StaDel(receptionInstance,"0");
        def kasir = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KASIR%"),'0',params.companyDealer)?.nama
        def row = [
            nomorWO: params.jenisBayar=="0"?(appoint?.t401NoAppointment ? appoint?.t401NoAppointment : appoint?.t401NoWO):recept?.t401NoWO,
            idReception : params.jenisBayar=="0"?appoint?.id:recept?.id,
            jenisService: receptionInstance?.customerIn ? receptionInstance?.customerIn?.tujuanKedatangan?.m400Tujuan : "-",
            namaCustomer: params.jenisBayar=="0"?appoint?.historyCustomer?.fullNama:recept?.historyCustomer?.fullNama,
            nomorPolisi: params.jenisBayar=="0"?appoint?.historyCustomerVehicle?.fullNoPol:recept?.historyCustomerVehicle?.fullNoPol,
            model: params.jenisBayar=="0"?appoint?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel:recept?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
            customerSPK: ('1'.equals(receptionInstance?.t401StaKategoriCustUmumSPKAsuransi))?'Ya':'Tidak',
            jumlahOnRisk: params?.jenisBayar=="0" ? 0 : (recept?.t401OnRisk ? recept?.t401OnRisk : 0),
            jumlahMinBookingFee: params?.jenisBayar=="0"?(jum?.size() > 0?(jum.first()==null ? 0 : jum.first()):0 ):(recept?.t401DPRp ? recept?.t401DPRp : 0),
            kasir: kasir,
            divisi: receptionInstance?.t401xDivisi,
            total: kw ? kw?.t722JmlUang : 0,
            noKw : kw ? kw?.t722NoKwitansi : ""
        ]
        currentRequest.session['receptionInstance'] = receptionInstance
        currentRequest.session['historyCustomerInstance'] = historyCustomerInstance
        currentRequest.session['customerInstance'] = customerInstance
        currentRequest.session['companyDealerInstance'] = companyDealer
        return row
    }

    def searchKwitansiByNomorKwitansi(def params) {
        def nomorKuitansi = params.nomorKuitansi

        def kuitansiInstance = Kwitansi.findByT722NoKwitansiAndCompanyDealer(nomorKuitansi, params.companyDealer)

        def bookingInstance = BookingFee.findByKwitansi(kuitansiInstance)

        def row = [
                namaWorkshop: kuitansiInstance?.historyCustomer?.company?.companyDealer?.m011NamaWorkshop,
                alamatWorkshop: kuitansiInstance?.historyCustomer?.company?.companyDealer?.m011Alamat,
                alamat2Workshop: kuitansiInstance?.historyCustomer?.company?.companyDealer?.kelurahan?.m004NamaKelurahan +', '+ kuitansiInstance?.historyCustomer?.company?.companyDealer?.kecamatan?.m003NamaKecamatan,
                alamat3Workshop: kuitansiInstance?.historyCustomer?.company?.companyDealer?.kabKota?.m002NamaKabKota,
                teleponWorkshop: kuitansiInstance?.historyCustomer?.company?.companyDealer?.m011Telp,
                extWorkshop: kuitansiInstance?.historyCustomer?.company?.companyDealer?.m011TelpExt,
                faxWorkshop: kuitansiInstance?.historyCustomer?.company?.companyDealer?.m011Fax,
                jenisBayar: ('0'.equals(kuitansiInstance?.t722StaBookingOnRisk))?'Booking Fee':'On Risk',
                nomorWO: kuitansiInstance?.reception?.t401NoWO,
                nomorKuitansi: kuitansiInstance?.t722NoKwitansi,
                terimaDari: kuitansiInstance?.t722Nama,
                terbilang: kuitansiInstance?.t722Terbilang,
                pembayaran: kuitansiInstance?.t722Ket,
                nomorPolisi: kuitansiInstance?.t722Nopol,
                metodeBayar: bookingInstance?.metodeBayar?.m701MetodeBayar,
                jumlah: kuitansiInstance?.t722JmlUang,
                printDate : datatablesUtilService?.syncTime(),
                tanggalKuitansi: bookingInstance?.t721TglJamBookingFee,
                kasir: kuitansiInstance?.t722xNamaUser
        ]

        return row
    }
    def getKwitansiDataTables(def params) {
        def currentRequest = RequestContextHolder.requestAttributes
        def rows = currentRequest?.session?.detailPembayaran
        if(!rows){
            def kwitansi = Kwitansi.findAllByReceptionAndT722StaDelAndCompanyDealer(Reception.get(params?.idWO?.toLong()),"0", params.companyDealer);
            def total = 0;
            kwitansi?.each {
                def kw = it
                def pembayaran = BookingFee.findAllByKwitansiAndT721StaDel(it,"0");
                int no = 0;
                pembayaran.each {
                    no++;
                    rows << [
                            id: no,
                            tanggalBayar: it?.t721TglJamBookingFee?.format("dd/MM/yyyy HH:mm"),
                            metodeBayar: it?.metodeBayar?.m701MetodeBayar,
                            metodeBayarInstance: it?.metodeBayar,
                            namaBank: it?.bank?.m702NamaBank,
                            namaBankInstance: it?.bank,
                            nomorKartu: it?.t721NoKartu,
                            mesinEdc: it?.mesinEdc?.m704NamaMesinEdc,
                            mesinEdcInstance: it?.mesinEdc,
                            nomorBayar: it?.t721NoBuktiTransfer,
                            bunga: it?.rateBankBankCharge?.m703RateRp,
                            bungaInstance: it?.rateBankBankCharge,
                            jumlah: conversi.toRupiah(it?.t721JmlhBayar)
                    ]
                    total += it?.t721JmlhBayar
                }
            }
            currentRequest.session['detailPembayaran'] = rows
            currentRequest.session['totalPembayaran'] = total
            currentRequest.session['rateBank'] = null
        }
        def output = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        return output
    }
    def addPembayaran(def params) {
        def result
        def currentRequest = RequestContextHolder.requestAttributes
        def tanggalBayar = datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm");

        def metodeBayar = params?.metodeBayar
        def metodeBayarInstance = MetodeBayar.findByM701Id(metodeBayar)

        def namaBank = params?.namaBank
        def namaBankInstance = Bank.findByM702ID(namaBank)

        def mesinEDC = params?.mesinEDC
        def mesinEDCInstance = MesinEdc.findByM704Id(mesinEDC)

        def nomorKartu = params?.nomorKartu
        def nomorBayar = params?.noBuktiTransfer
        double bunga = (params?.bankCharge)?new Double(params?.bankCharge):0.0
        double jumlah = (params?.totalBayar)?new Double(params?.totalBayar):0.0
        double total = (currentRequest?.session?.totalPembayaran)?new Double(currentRequest?.session?.totalPembayaran):0.0

        def rows = currentRequest?.session.detailPembayaran
        int id
        if(rows){
            id = rows.size()
            if(id<3) {
                rows << [
                    id: id + 1,
                    tanggalBayar: tanggalBayar,
                    metodeBayar: (metodeBayar) ? metodeBayarInstance?.toString() : null,
                    metodeBayarInstance: (metodeBayar) ? metodeBayarInstance : null,
                    namaBank: (namaBank) ? namaBankInstance?.toString() : null,
                    namaBankInstance: (namaBank) ? namaBankInstance : null,
                    nomorKartu: (nomorKartu) ? nomorKartu : null,
                    mesinEdc: (mesinEDC) ? mesinEDCInstance?.toString() : null,
                    mesinEdcInstance: (mesinEDC) ? mesinEDCInstance : null,
                    nomorBayar: (nomorBayar) ? nomorBayar : null,
                    bunga: (bunga) ? bunga : null,
                    bungaInstance: currentRequest?.session?.rateBank,
                    jumlah: jumlah
                ]
                total += jumlah
                result = [detail : rows, total: total]
            }else{
                result = [detail : rows, total: total, err:'Maksimal 3 jenis pembayaran']
            }
        }else{
            rows = []
            rows << [
                id: 1,
                tanggalBayar: tanggalBayar,
                metodeBayar: (metodeBayar) ? metodeBayarInstance?.toString() : null,
                metodeBayarInstance: (metodeBayar) ? metodeBayarInstance : null,
                namaBank: (namaBank) ? namaBankInstance?.toString() : null,
                namaBankInstance: (namaBank) ? namaBankInstance : null,
                nomorKartu: (nomorKartu) ? nomorKartu : null,
                mesinEdc: (mesinEDC) ? mesinEDCInstance?.toString() : null,
                mesinEdcInstance: (mesinEDC) ? mesinEDCInstance : null,
                nomorBayar: (nomorBayar) ? nomorBayar : null,
                bunga: (bunga) ? bunga : null,
                bungaInstance: currentRequest?.session?.rateBank,
                jumlah: jumlah
            ]
            total = jumlah
            result = [detail : rows, total: total]
        }

        currentRequest.session['detailPembayaran'] = rows
        currentRequest.session['totalPembayaran'] = total
        currentRequest.session['rateBank'] = null

        return result
    }
    def saveKwitansiData(def params) {
        def journalCode = new GenerateCodeService().generateJournalCode("TJ")
        def result

        def jenisBayar = params?.jenisBayar!="2" ? "0" : "1"
        double total = new Double(params?.totalUang)
        def currentRequest = RequestContextHolder.requestAttributes
        def reception = (Reception) currentRequest?.session.receptionInstance
        def customer = (Customer) currentRequest?.session.customerInstance
        def historyCustomer = (HistoryCustomer) currentRequest?.session.historyCustomerInstance
        def kwitansiId = generateCodeService.codeGenerateSequence('T722_ID',(CompanyDealer)currentRequest?.session.userCompanyDealer)
        kwitansiId = kwitansiId.padLeft(12,'0')
        def tanggalJamBookingFee = datatablesUtilService?.syncTime()
        def kwitansiInstance = new Kwitansi()
        def app = Appointment.findByReceptionAndStaDel(reception,"0")
        if(app){
            app?.t301TglJamJanjiBayarDP = datatablesUtilService?.syncTime()
            app?.save(flush: true)
        }

        def tempCust = reception?.historyCustomer ? reception?.historyCustomer : app?.historyCustomer
        if(kwitansiInstance) {
            kwitansiInstance?.companyDealer = params?.companyDealer
            kwitansiInstance.t722NoKwitansi = kwitansiId
            kwitansiInstance.t722StaBookingOnRisk = jenisBayar
            kwitansiInstance.reception = reception
            kwitansiInstance.customer = tempCust?.customer
            kwitansiInstance.historyCustomer = tempCust
            kwitansiInstance.t722Nama = params?.terimaDari
            kwitansiInstance.t722Nopol = params?.nopol
            kwitansiInstance.t722JmlUang = total
            kwitansiInstance.t722Ket = params?.untukPembayaran
            kwitansiInstance.t722Terbilang = moneyUtil.convertMoneyToWords(total)
            kwitansiInstance.t722xNamaUser = params?.kasir
            kwitansiInstance.t722xDivisi = params?.divisi
            kwitansiInstance.t722StaBFPartService = params?.jenisBayar
            kwitansiInstance.dateCreated = datatablesUtilService?.syncTime()
            kwitansiInstance.lastUpdated = datatablesUtilService?.syncTime()
            if (kwitansiInstance.hasErrors() || !kwitansiInstance.save(flush: true, insert:true)) {
                kwitansiInstance.errors.each {
                    //println it
                }
                result = null
            } else {
                result = kwitansiInstance.t722NoKwitansi
                def rows = currentRequest?.session?.detailPembayaran
                if(rows){
                    def totalBayar = 0
                    def metodeBayar = ""
                    def idBank = -1
                    rows.each{
                        def bookingFee = new BookingFee()
                        bookingFee.kwitansi = kwitansiInstance
                        bookingFee.t721ID = it?.id
                        bookingFee.t721TglJamBookingFee = tanggalJamBookingFee
                        bookingFee.metodeBayar = it?.metodeBayarInstance
                        bookingFee.bank = it?.namaBankInstance
                        bookingFee.mesinEdc = it?.mesinEdcInstance
                        bookingFee.rateBankBankCharge = it?.bungaInstance
                        bookingFee.t721NoKartu = it?.nomorKartu
                        bookingFee.t721JmlhBayar = it?.jumlah
                        bookingFee.t721NoBuktiTransfer = it?.nomorBayar
                        bookingFee.t721xNamaUser = params?.kasir
                        bookingFee.t721xDivisi = params?.divisi
                        bookingFee.dateCreated = datatablesUtilService?.syncTime()
                        bookingFee.lastUpdated = datatablesUtilService?.syncTime()
                        totalBayar+=(it?.jumlah?it?.jumlah:0)
                        metodeBayar=bookingFee?.metodeBayar?.m701MetodeBayar
                        idBank=bookingFee?.bank?.id
                        if(bookingFee.hasErrors() || !bookingFee.save(flush:true,insert: true)){
                            bookingFee.errors.each {
                                //println it
                            }
                        }
                    }
                    def journal = null

                    def arrMetode = ["TRANSFER","DEBIT","KARTU KREDIT"]
                    def metodh = ""
                    if(arrMetode.contains(metodeBayar.toUpperCase())){
                        metodh = "BANK"
                    }else{
                        metodh = "KAS"
                    }
                    if((params.jenisBayar=="0" || params.jenisBayar=="1" ) && rows.size()>0){
                        params.docNumber = (reception?.t401NoAppointment==null || reception?.t401NoAppointment=="")?reception?.t401NoWO:reception?.t401NoAppointment
                        params.totalBiaya = totalBayar
                        params.journalCode = journalCode
                        params.tipeBayar = metodh
                        params.jenisWo = Reception.findByT401NoWOAndStaDel(reception?.t401NoWO,"0")?.tipeKerusakan?.m401NamaTipeKerusakan==null?"GR":"BP"
                        if(metodh.equalsIgnoreCase("BANK")){
                            params.idBank = idBank
                        }
                        def service = new UMServicePelangganJournalService()
                        journal = service.createJournal(params)
                    }else{
                        params.docNumber = reception?.t401NoWO
                        params.totalBiaya = totalBayar
                        params.journalCode = journalCode
                        params.tipeBayar = metodh
                        params.jenisWo = Reception.findByT401NoWOAndStaDel(reception?.t401NoWO,"0")?.tipeKerusakan?.m401NamaTipeKerusakan==null?"GR":"BP"
                        if(metodh.equalsIgnoreCase("BANK")){
                            params.idBank = idBank
                        }
                        def service = new UMServiceAsuransiJournalService()
                        journal = service.createJournal(params)
                    }
                    def recepTemp = Reception.findByT401NoWOAndStaDel(reception?.t401NoWO,"0")
                    recepTemp.t401StaBayarOnRisk = "1"
                    recepTemp.lastUpdated = datatablesUtilService?.syncTime()
                    recepTemp.save(flush: true)
                    def doc = new DocumentCategory()
                    def description = "";
                    String docNumbers = reception?.t401NoWO;
                    if(params?.jenisBayar=="0"){
                        doc = DocumentCategory.findByDocumentCategoryAndStaDel("DOCDPP","0")?DocumentCategory.findByDocumentCategoryAndStaDel("DOCDPP","0"):new DocumentCategory(documentCategory: "DOCDPP",description: "Penerimaan DP Parts",staDel: "0",createdBy: "SYSTEM",lastUpdProcess: "CREATE",dateCreated: datatablesUtilService?.syncTime(),lastUpdated: datatablesUtilService?.syncTime()).save(flush: true)
                        description = "Penerimaan DP Parts, No. wo / App. "+params.docNumber
                        docNumbers = params.docNumber
                    }else if(params?.jenisBayar=="1"){
                        doc = DocumentCategory.findByDocumentCategoryAndStaDel("DOCDPS","0")?DocumentCategory.findByDocumentCategoryAndStaDel("DOCDPS","0"):new DocumentCategory(documentCategory: "DOCDPS",description: "Penerimaan DP Service",staDel: "0",createdBy: "SYSTEM",lastUpdProcess: "CREATE",dateCreated: datatablesUtilService?.syncTime(),lastUpdated: datatablesUtilService?.syncTime()).save(flush: true)
                        description = "Penerimaan DP Service, No. WO. "+reception?.t401NoWO
                    }else if(params?.jenisBayar=="2"){
                        doc = DocumentCategory.findByDocumentCategoryAndStaDel("DOCOR","0")?DocumentCategory.findByDocumentCategoryAndStaDel("DOCOR","0"):new DocumentCategory(documentCategory: "DOCOR",description: "Penerimaan Pembayaran On Risk",staDel: "0",createdBy: "SYSTEM",lastUpdProcess: "CREATE",dateCreated: datatablesUtilService?.syncTime(),lastUpdated: datatablesUtilService?.syncTime()).save(flush: true)
                        description = "Penerimaan On Risk (Resiko Sendiri), No. WO. "+reception?.t401NoWO
                    }
                    def transactionCode        = new GenerateCodeService().generateTransactionCode()
                    def transactionType        = TransactionType.findByTransactionTypeIlike("%"+metodh+"%")
                    def transactionDate        = datatablesUtilService?.syncTime()
                    def amount                 = kwitansiInstance?.t722JmlUang
                    def bankAccountNumber      = params.bank ? BankAccountNumber?.get(params.bank.toLong()) : null  //BankAccountNumber.findByAccountNumber(kasAccount)

                    def transaction = new Transaction(
                            documentCategory: doc,
                            transactionCode: transactionCode,
                            transactionType: transactionType,
                            staOperasional: "1",
                            transactionDate: transactionDate,
                            companyDealer: params?.companyDealer,
                            transferDate: datatablesUtilService?.syncTime(),
                            inOutType: "IN",
                            amount: amount,
                            journal : Journal.findByJournalCode(journalCode),
                            description: description,
                            bankAccountNumber: bankAccountNumber,
                            dueDate: null,
                            staDel: "0",
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            docNumber : docNumbers,
                            dateCreated : datatablesUtilService?.syncTime(),
                            lastUpdated : datatablesUtilService?.syncTime()
                    ).save(flush: true)
                }
            }

            return [kwitansiId: result]
        }
    }

    /* Get Refund Form List */
    def getRefundFormList(def params) {

        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData = []
        dataToRender.iTotalRecords = 0
        dataToRender.iTotalDisplayRecords = 0

        def filters = []
        filters << " WHERE Refund.COMPANY_DEALER_ID = "+params.companyDealer

        def paramsFilter = [:]

        //tanggal refund
        if (params."chkTanggalRefund" && params."tanggalRefundStart" && params."tanggalRefundEnd") {
            def tanggalRefundStart = params."tanggalRefundStart"
            def tanggalRefundEnd = params."tanggalRefundEnd"
            filters << "(Refund.T706_TgJamlRefund BETWEEN TO_DATE(:refundStart) AND TO_DATE(:refundEnd))"
            paramsFilter.refundStart = tanggalRefundStart.format("dd-MMM-YY")
            paramsFilter.refundEnd = tanggalRefundEnd.format("dd-MMM-YY")
        }

        //jenis refund
        if (params."chkJenisRefund" && params."jenisRefund") {
            def jenisRefund = params."jenisRefund"
            if ("SEMUA".equalsIgnoreCase(jenisRefund)) {
                filters << "(Refund.T706_JenisRefund IN (:booking, :pph, :ppn))"
                paramsFilter.booking = "0"
                paramsFilter.pph = "1"
                paramsFilter.ppn = "2"
            } else {
                filters << "(Refund.T706_JenisRefund = :jenisRefund)"
                paramsFilter.jenisRefund = jenisRefund
            }
        }

        //metode refund
        if (params."chkMetodeRefund" && params."metodeRefund") {
            def metodeRefund = params."metodeRefund"
            if ("SEMUA".equalsIgnoreCase(metodeRefund)) {
                filters << "(Refund.T706_StaCashTransfer IN (:metodeCash, :metodeTransfer))"
                paramsFilter.metodeCash = "0"
                paramsFilter.metodeTransfer = "1"
            } else {
                filters << "(Refund.T706_StaCashTransfer = :metodeRefund)"
                paramsFilter.metodeRefund = metodeRefund
            }
        }

        //nomor refund
        if (params."chkNomorRefund" && params."nomorRefund") {
            def nomorRefund = params."nomorRefund"
            filters << "(Refund.T706_NoRefund LIKE(:nomorRefund))"
            paramsFilter.nomorRefund = '%'+nomorRefund+'%'
        }

        def query = new StringBuilder(BASE_QUERY_REFUND)

        def refundList = []
        def max = params.iDisplayLength as int
        def offset = params.iDisplayStart as int

        if (filters.size > 1) {
            def filter = filters.join(" AND ")
            query.append("${filter}")
            query.append(" ORDER BY Refund.T706_NoRefund, TanggalRefund")
            refundList = processQuery(query, paramsFilter, offset, max)
        } else {
            def filter = filters[0]
            query.append("${filter}")
            query.append(" ORDER BY Refund.T706_NoRefund, TanggalRefund")
            refundList = processQuery(query, null, offset, max)
        }

        def rows = []
        refundList?.each {
            rows << [
                tanggalRefund     : it[0],
                noRefund          : it[1],
                jenisRefund       : ('0'.equals(it[2]))?'BOOKING FEE':('1'.equals(it[2]))?'PPH 23':'PPN',
                noKuitansi        : it[3],
                noInvoice         : it[4],
                nomorWO           : it[5],
                noCustomer        : it[6],
                customer          : it[7],
                noPol             : it[8],
                statusCashTransfer: ('0'.equals(it[9]))?'CASH':'TRANSFER',
                namaBank          : (it[10])?it[10]:'-',
                acctName          : it[11],
                acctNo            : it[12],
                alasanRefund      : (it[13])?it[13]:it[16],
                jmlRefund         : it[14],
                kasir             : it[15]
            ]
        }

        dataToRender.aaData = rows
        dataToRender.iTotalRecords = rows.size()
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        return dataToRender
    }

    /* Module Settlement */
    def searchSettlement(def params){

        def metodeSettlement = params.metodeSettlement
        def nomorInvoice = params.nomorInvoice
        def company = params?.company
        if('1'.equals(metodeSettlement)){
            return searchSettlementByNomorWO(nomorInvoice,company)
        }else if('2'.equals(metodeSettlement)){
            return searchSettlementByNomorInvoice(nomorInvoice,company)
        }else{
            return [status:'err', message:'Metode Settlement Salah']
        }
    }
    private def searchSettlementByNomorWO(def nomorWO,CompanyDealer company){

        def receptionInstance = Reception.findByT401NoWOIlikeAndStaDelAndCompanyDealer("%"+nomorWO+"%","0",company,[sort : 'dateCreated',order : 'desc'])
        def operationInstance = (Operation) receptionInstance?.operation
        def kategoriJob = (KategoriJob) operationInstance?.kategoriJob
        def historiCustomerInstance = (HistoryCustomer) receptionInstance?.historyCustomer
        def historyCustomerVehicleInstance = (HistoryCustomerVehicle) receptionInstance?.historyCustomerVehicle
        def kodeKotaInstance = (KodeKotaNoPol) historyCustomerVehicleInstance?.kodeKotaNoPol
        def fullModelCodeInstance = (FullModelCode) historyCustomerVehicleInstance?.fullModelCode
        def baseModelInstance = (BaseModel) fullModelCodeInstance?.baseModel
        def invoiceInstance = InvoiceT701.createCriteria().get {
            eq("t701StaDel","0");
            eq("reception",receptionInstance);
            ilike("t701JenisInv","%T%");
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            maxResults(1);
        }
        def refundBF = invoiceInstance?.t701TotalInv ? invoiceInstance?.t701TotalInv - invoiceInstance?.t701BookingFee : 0
        def customerInstance = historiCustomerInstance?.customer
        def companyDealer = customerInstance?.company?.companyDealer
        def generalParameter = GeneralParameter.findByCompanyDealer(companyDealer)
        def tax = generalParameter?.m000Tax
        def pph23 = generalParameter?.m000PersenPPh
        def settlementList = Settlement.findAllByInvoice(invoiceInstance)
        def totalBayar = 0
        settlementList.each {
            totalBayar += it?.t704JmlBayar
        }

        def row
        if(!invoiceInstance){
            return [status:'err', message: 'Proses Settlement Tidak Dapat Dilanjutkan Karena Belum Ada No. Invoice']
        }else{
            def konversi = new Konversi()
            def custIn = CustomerIn.findByReceptionAndStaDel(invoiceInstance?.reception,"0")
            row = [
                outNomorWO: receptionInstance?.t401NoWO,
                outNamaCustomer: historiCustomerInstance?.t182NamaDepan + ' ' + (historiCustomerInstance?.t182NamaBelakang ? historiCustomerInstance?.t182NamaBelakang : ""),
                outNomorPolisi: kodeKotaInstance?.m116ID + ' ' + historyCustomerVehicleInstance?.t183NoPolTengah + ' ' + historyCustomerVehicleInstance?.t183NoPolBelakang,
                outModel: baseModelInstance?.m102NamaBaseModel,
                outCustomerPKS: ('1'.equals(receptionInstance?.t401StaKategoriCustUmumSPKAsuransi))?'Ya':'Tidak',
                outJenisInvoice: ('1'.equals(invoiceInstance?.t701IntExt))?'External':'Internal',
                outJumlahOR: invoiceInstance?.t701OnRisk,
                outJasaLabor: invoiceInstance?.t701JasaRp,
                outSublet: invoiceInstance?.t701SubletRp,
                outParts: invoiceInstance?.t701PartsRp + invoiceInstance?.t701OliRp + invoiceInstance?.t701MaterialRp,
                outPpnTax: invoiceInstance?.t701PPnRp,
                outMaterai: invoiceInstance?.t701MateraiRp,
                outTotalInvoice: konversi?.toRupiah(invoiceInstance?.t701TotalInv),
                outKasir: Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KASIR%"),'0',receptionInstance.companyDealer)?.nama,
                outDivisi: receptionInstance?.t401xDivisi,
                outBookingFee: invoiceInstance?.t701BookingFee==0?invoiceInstance?.t701OnRisk:invoiceInstance?.t701BookingFee,
                outRefundBF: (refundBF < 0)?refundBF:0,
                outDiscountGW: invoiceInstance?.t701GoodWillRp,
                outTax: tax,
                outPph23:pph23,
                outTotal: totalBayar,
                outNoInvoice: invoiceInstance?.t701NoInv,
                outJenisService : custIn ? custIn?.tujuanKedatangan?.m400Tujuan : "-",
                idCari: receptionInstance?.id,
                idInvoice: invoiceInstance?.id,
                metodeCari : "reception"
            ]
        }
        return [status:'ok', detail: row]
    }
    private def searchSettlementByNomorInvoice(def nomorInvoice,CompanyDealer company){

        def invoiceInstance = InvoiceT701.findByT701NoInvIlikeAndT701StaDelAndCompanyDealer("%"+nomorInvoice+"%","0",company,[sort : 'dateCreated',order: 'desc'])
        def invReff = InvoiceT701.findByT701NoInv_ReffIlike(invoiceInstance?.t701NoInv+"%")
        def receptionInstance = (Reception) invoiceInstance?.reception
        def operationInstance = (Operation) receptionInstance?.operation
//        def kategoriJob = (KategoriJob) operationInstance?.kategoriJob
        def historiCustomerInstance = (HistoryCustomer) receptionInstance?.historyCustomer
        def historyCustomerVehicleInstance = (HistoryCustomerVehicle) receptionInstance?.historyCustomerVehicle
        def kodeKotaInstance = (KodeKotaNoPol) historyCustomerVehicleInstance?.kodeKotaNoPol
        def fullModelCodeInstance = (FullModelCode) historyCustomerVehicleInstance?.fullModelCode
        def baseModelInstance = (BaseModel) fullModelCodeInstance?.baseModel
        def refundBF = invoiceInstance?.t701TotalInv ? invoiceInstance?.t701TotalInv - invoiceInstance?.t701BookingFee : 0
        def customerInstance = historiCustomerInstance?.customer
        def companyDealer = customerInstance?.company?.companyDealer
        def generalParameter = GeneralParameter.findByCompanyDealer(companyDealer)
        def tax = generalParameter?.m000Tax
        def pph23 = generalParameter?.m000PersenPPh
        def settlementList = Settlement.findAllByInvoice(invoiceInstance)
        def totalBayar = 0
        settlementList.each {
            totalBayar += it?.t704JmlBayar
        }

        def row
//        || invoiceInstance?.t701NoInv_Reff == null
        if(!invoiceInstance){
            return [status:'err', message: 'Proses Settlement Tidak Dapat Dilanjutkan Karena Belum Ada No. Invoice']
        }else if(invoiceInstance?.t701NoInv_Reff != null){
            return [status:'err', message: 'Proses Settlement Tidak Dapat Dilanjutkan!!! Ini Invoice Reversal']
        }else if(invReff){
            return [status:'err', message: 'Proses Settlement Tidak Dapat Dilanjutkan!!! Ini Merupakan Invoice Reversal']
        }else{
            def konversi = new Konversi()
            def custIn = CustomerIn.findByReceptionAndStaDel(receptionInstance,"0")
            row = [
                outNomorWO: receptionInstance?.t401NoWO,
                outNamaCustomer: historiCustomerInstance?.t182NamaDepan + ' ' + (historiCustomerInstance?.t182NamaBelakang ? historiCustomerInstance?.t182NamaBelakang : ""),
                outNomorPolisi: kodeKotaInstance?.m116ID + ' ' + historyCustomerVehicleInstance?.t183NoPolTengah + ' ' + historyCustomerVehicleInstance?.t183NoPolBelakang,
                outModel: baseModelInstance?.m102NamaBaseModel,
                outCustomerPKS: ('1'.equals(receptionInstance?.t401StaKategoriCustUmumSPKAsuransi))?'Ya':'Tidak',
                outJenisInvoice: ('1'.equals(invoiceInstance?.t701IntExt))?'External':'Internal',
                outJumlahOR: invoiceInstance?.t701OnRisk,
                outJasaLabor: invoiceInstance?.t701JasaRp,
                outSublet: invoiceInstance?.t701SubletRp,
                outParts: invoiceInstance?.t701PartsRp + invoiceInstance?.t701OliRp + invoiceInstance?.t701MaterialRp,
                outPpnTax: invoiceInstance?.t701PPnRp,
                outMaterai: invoiceInstance?.t701MateraiRp,
                outTotalInvoice: konversi?.toRupiah(invoiceInstance?.t701TotalInv),
                outKasir: Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KASIR%"),'0',receptionInstance.companyDealer)?.nama,
                outDivisi: receptionInstance?.t401xDivisi,
                outBookingFee: invoiceInstance?.t701OnRisk==0?invoiceInstance?.t701BookingFee:invoiceInstance?.t701OnRisk,
                outRefundBF: (refundBF < 0)?refundBF:0,
                outDiscountGW: invoiceInstance?.t701GoodWillRp,
                outTax: tax,
                outPph23:pph23,
                outTotal: totalBayar,
                outJenisService: custIn ? custIn?.tujuanKedatangan?.m400Tujuan : "-",
                outNoInvoice : invoiceInstance?.t701NoInv,
                outJenisInvoice : invoiceInstance?.t701JenisInv,
                idCari: invoiceInstance?.id,
                idInvoice: invoiceInstance?.id,
                metodeCari : "invoice"
            ]
        }

        return [status:'ok', detail: row]
    }
    def searchRefundByNomorWO(def params) {
        def nomorWO = params.nomorWO

        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['receptionInstance'] = null
        currentRequest.session['kwitansiInstance'] = null
        currentRequest.session['historyCustomerInstance'] = null
        currentRequest.session['historyCustomerVehicleInstance'] = null
        currentRequest.session['customerInstance'] = null
        currentRequest.session['invoiceInstance'] = null
        currentRequest.session['appointmentInstance'] = null
        currentRequest.session['companyDealerInstance'] = null

        def appoint = null;
        def recept = null;
        if(params.jenisRefund=="0"){
            def cari = Appointment.createCriteria().list {
                eq("staDel","0")
                reception{
                    ilike("t401NoAppointment","%"+params.nomorWO+"%")
                }
                eq("companyDealer",params?.companyDealer)
            }
            appoint = cari?.size()>0 ? cari?.last() : null
        }else {
            recept = Reception.findByT401NoWOIlikeAndStaDelAndStaSaveAndCompanyDealer("%"+params.nomorWO+"%","0","0",params?.companyDealer);
        }
        def receptionInstance = params.jenisRefund=="0"?appoint?.reception:recept
        def kwitansiInstance = Kwitansi.findByReceptionAndT722StaDelAndT722StaBFPartService(receptionInstance,"0",params?.jenisRefund)
        def historyCustomerVehicleInstance = params.jenisRefund=="0"?appoint?.historyCustomerVehicle:recept?.historyCustomerVehicle
        def historyCustomerInstance = params.jenisRefund=="0"?appoint?.historyCustomer:recept?.historyCustomer
        def customerInstance = params.jenisRefund=="0"?appoint?.historyCustomer?.customer:recept?.historyCustomer?.customer
        def companyDealer = currentRequest.session.userCompanyDealer
        def generalParameter = GeneralParameter.findByCompanyDealer(companyDealer)
        def invoiceInstance = InvoiceT701.findByReception(receptionInstance)
        def appointmentInstance = params.jenisRefund=="0" ? appoint : Appointment.findByReceptionAndStaDel(receptionInstance,"0")
        String cariNoWo = params.jenisRefund=="0"?appoint?.reception?.t401NoAppointment:recept?.t401NoWO
        def refundInstance = Refund.findByT706NomorWOAndT706JenisRefundAndT706StaDel(cariNoWo,params.jenisRefund,"0");
        def refundBF
        if(kwitansiInstance && invoiceInstance){
            refundBF = kwitansiInstance?.t722JmlUang - invoiceInstance?.t701TotalInv
        }else{
            refundBF = kwitansiInstance?.t722JmlUang
        }
        def settlementInstance = Settlement.findByInvoice(invoiceInstance)

        def row = [
            id: params.jenisRefund=="0"?appoint?.reception?.id:recept?.id,
            outNomorWO: params.jenisRefund=="0"?appoint?.reception?.t401NoAppointment:recept?.t401NoWO,
            outNamaCustomer: params.jenisRefund=="0"?appoint?.historyCustomer?.fullNama:recept?.historyCustomer?.fullNama,
            outNomorPolisi: params.jenisRefund=="0"?appoint?.historyCustomerVehicle?.fullNoPol:recept?.historyCustomerVehicle?.fullNoPol,
            outModel: params.jenisRefund=="0"?appoint?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel:recept?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
            outStatusApproval: appointmentInstance?.t301StaOkCancelReSchedule,
            outNoKuitansi:kwitansiInstance?.t722NoKwitansi,
            outBookingFee:kwitansiInstance?.t722JmlUang,
            outNoInvoice:invoiceInstance?.t701NoInv,
            outTotalInvoice:invoiceInstance?.t701TotalInv,
            outRefundBF:refundBF,
            outRefundPPH:settlementInstance?.t704JmlPPh23,
            outRefundPPN:settlementInstance?.t704JmlPPN,
            outKasir:Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KASIR%"),'0',receptionInstance.companyDealer)?.nama,
            outParamSetting:generalParameter?.m000MaksBayarCashRefund,
            outDivisi:receptionInstance?.t401xDivisi,
            staDone : refundInstance?"1":"0",
            outNoRefund : refundInstance ? refundInstance?.t706NoRefund : ""
        ]

        currentRequest.session['receptionInstance'] = receptionInstance
        currentRequest.session['kwitansiInstance'] = kwitansiInstance
        currentRequest.session['historyCustomerInstance'] = historyCustomerInstance
        currentRequest.session['historyCustomerVehicleInstance'] = historyCustomerVehicleInstance
        currentRequest.session['customerInstance'] = customerInstance
        currentRequest.session['invoiceInstance'] = invoiceInstance
        currentRequest.session['appointmentInstance'] = appointmentInstance
        currentRequest.session['companyDealerInstance'] = companyDealer

        return row
    }
    def searchRefundByNomorRefund(def params) {
        def nomorRefund = params?.nomorRefund

        def refundInstance = Refund.findByT706NoRefundIlikeAndT706StaDelIlike("%"+nomorRefund+"%","%0%")

        def row = [
            namaWorkshop: refundInstance?.companyDealer?.m011NamaWorkshop,
            alamatWorkshop: refundInstance?.historyCustomer?.company?.companyDealer?.m011Alamat,
            teleponWorkshop: refundInstance?.historyCustomer?.company?.companyDealer?.m011Telp,
            extWorkshop: refundInstance?.historyCustomer?.company?.companyDealer?.m011TelpExt,
            faxWorkshop: refundInstance?.historyCustomer?.company?.companyDealer?.m011Fax,
            jenisRefund: ('0'.equals(refundInstance?.t706JenisRefund))?'(Booking Fee Part)':'(Booking Fee Service)',
            nomorRefund: refundInstance?.t706NoRefund,
            nomorWO: refundInstance?.t706NomorWO,
            nomorPolisi: refundInstance?.t706Nopol,
            customer: refundInstance?.t706NamaCustomer,
            invoice: refundInstance?.t706NomorInvoice,
            amount: refundInstance?.t706JmlRefund ? new Konversi().toRupiah(refundInstance?.t706JmlRefund) : "0",
            sayInWord: refundInstance?.t706TerbilangRefund,
            reason: refundInstance?.alasanRefund?.m071AlasanRefund,
            reasonOther: refundInstance?.t706AlasanRefundLainnya,
            namaBank: (refundInstance?.bank?.m702NamaBank)?refundInstance?.bank?.m702NamaBank:'n/a',
            actName: (refundInstance?.t706NamaAccount)?refundInstance?.t706NamaAccount:'n/a',
            actNo: (refundInstance?.t706NoAccount)?refundInstance?.t706NoAccount:'n/a',
            kotaDealer: refundInstance?.companyDealer?.kabKota?.m002NamaKabKota ? refundInstance?.companyDealer?.kabKota?.m002NamaKabKota+"," : "",
            tanggalRefund: refundInstance?.t706TgJamlRefund?.format("yyyy-MM-dd HH:mm:ss"),
            method: refundInstance?.t706StaCashTransfer
        ]

        return row
    }
    def getKuitansiDataTables(def params) {
        def kuitansiNo = params.nomorKuitansi

        def kuitansiInstance = Kwitansi.findByT722NoKwitansiLikeAndCompanyDealer(kuitansiNo+"%" as String, params.companyDealer)
        def bookingFeeInstance = BookingFee.findByKwitansi(kuitansiInstance)

        def rows = []
        if(kuitansiNo && kuitansiInstance){
            rows << [
                    id:kuitansiInstance?.id,
                    tanggalBayar:bookingFeeInstance?.t721TglJamBookingFee?.format("dd-mm-yyyy"),
                    noWO:kuitansiInstance?.reception?.t401NoWO ? kuitansiInstance?.reception?.t401NoWO : kuitansiInstance?.reception?.t401NoAppointment,
                    customer:kuitansiInstance?.t722Nama,
                    pembayaran:kuitansiInstance?.t722Ket,
                    //pembayaran:('0'.equals(kuitansiInstance?.t722StaBookingOnRisk))?'Booking Fee' : 'On Risk',
                    jumlah:kuitansiInstance?.t722JmlUang ? conversi.toRupiah(kuitansiInstance?.t722JmlUang) : "0"
            ]
        }
        def output = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        return output
    }
    def saveRefundData(def params) {

        double jumlah = new Double(params?.jumlah)

        def currentRequest = RequestContextHolder.requestAttributes

        def receptionInstance = (Reception) currentRequest?.session.receptionInstance
        def kwitansiInstance = (Kwitansi) currentRequest?.session.kwitansiInstance
//        def historyCustomerVehicleInstance = (HistoryCustomerVehicle) currentRequest?.session.historyCustomerVehicleInstance
        def historyCustomerInstance = (HistoryCustomer) currentRequest?.session.historyCustomerInstance
//        def customerInstance = (Customer) currentRequest?.session.customerInstance
        def invoiceInstance = (InvoiceT701) currentRequest?.session.invoiceInstance
//        def appointmentInstance = (Appointment) currentRequest?.session.appointmentInstance
        def companyDealerInstance = (CompanyDealer) currentRequest?.session.userCompanyDealer
        def bank = (params?.namaBank)?BankAccountNumber.get(params?.namaBank?.toLong()):null
        def nomorRefund = generateCodeService.codeGenerateSequence('T706_ID',companyDealerInstance)
        nomorRefund = nomorRefund.padLeft(12,'0')

        def refundInstance = new Refund()
        if(refundInstance){
            def rec = ""
            def invoice = ""
            try{
                rec= Reception.findByT401NoWOIlike("%" + params?.nomorWO as String)
                params?.nomorWO = rec?.t401NoWO
                invoice = InvoiceT701?.findByReception(rec)?.t701NoInv
            }catch (Exception e){

            }
            refundInstance.t706NoRefund = nomorRefund
            refundInstance.t706TgJamlRefund = datatablesUtilService?.syncTime()
            refundInstance.t706JenisRefund = params?.jenisRefund
            refundInstance.t706NomorWO = params?.nomorWO
            refundInstance.t706NomorKwitansi = kwitansiInstance?.t722NoKwitansi
            refundInstance.t706NomorInvoice = invoice
            refundInstance.t706Nopol = params?.nopol
            refundInstance.historyCustomer = historyCustomerInstance
            refundInstance.t706NamaCustomer = params?.namaCustomer
            refundInstance.t706StaCashTransfer = params?.metodeRefund
            refundInstance.bank = bank?.bank
            refundInstance.companyDealer = params?.companyDealer
            refundInstance.t706NoAccount = params?.noAccount
            refundInstance.t706NamaAccount = params?.namaAccount
            refundInstance.t706StaFCKTP = params?.fcKTP
            refundInstance.t706StaFCBukuTab = params?.fcTab
            refundInstance.alasanRefund = (params?.alasan)?AlasanRefund.findByM071ID(params?.alasan):null
            refundInstance.t706AlasanRefundLainnya = params?.alasanLain
            refundInstance.t706JmlRefund = jumlah
            refundInstance.t706TerbilangRefund = moneyUtil.convertMoneyToWords(jumlah)
            refundInstance.t706xNamaUser = params?.kasir
            refundInstance.t706xDivisi = params?.divisi
            refundInstance.dateCreated = datatablesUtilService.syncTime()
            refundInstance.lastUpdated = datatablesUtilService.syncTime()

            if(refundInstance.hasErrors() || !refundInstance.save(flush:true,insert:true)){
                refundInstance.errors.each {
                    log.error(it)
                }
                return [err:'Data tidak berhasil disimpan']
            }else{
                def metodh = params?.metodeRefund=="1" ? "BANK" : "KAS"
                def service = new RefundUangMukaJournalService()
                if(params.jenisRefund=="0" ){
                    params.docNumber = receptionInstance?.t401NoAppointment
                    params.totalBiaya = refundInstance?.t706JmlRefund
                    params.tipeBayar = metodh
                    params.judulJurnal = "No. Appointment "+receptionInstance?.t401NoAppointment
                    params.subLedger = Appointment.findByReceptionAndStaDelAndStaSave(receptionInstance,"0","0")?.historyCustomer?.id
                    if(metodh.equalsIgnoreCase("BANK")){
                        params.idBank = bank?.id
                    }
                }else{
                    params.docNumber = receptionInstance?.t401NoWO
                    params.totalBiaya = refundInstance?.t706JmlRefund
                    params.tipeBayar = metodh
                    params.judulJurnal = "No. WO "+receptionInstance?.t401NoWO
                    params.subLedger = receptionInstance?.historyCustomer?.id
                    if(params?.jenisRefund=="2"){
                        params.judulJurnal += " (OR) "
                    }
                    if(metodh.equalsIgnoreCase("BANK")){
                        params.idBank = bank?.id
                    }
                }
                service.createJournal(params)
                def doc = new DocumentCategory()
                def description = "";
                String docNumbers = receptionInstance?.t401NoWO;
                if(params?.jenisRefund=="0"){
                    doc = DocumentCategory.findByDocumentCategoryAndStaDel("DOCPDPP","0")?DocumentCategory.findByDocumentCategoryAndStaDel("DOCPDPP","0"):new DocumentCategory(documentCategory: "DOCPDPP",description: "Pengembaliam DP Parts",staDel: "0",createdBy: "SYSTEM",lastUpdProcess: "CREATE",dateCreated: new Date(),lastUpdated: new Date()).save(flush: true)
                    description = "Pengembalian DP Parts, No. App. "+receptionInstance?.t401NoAppointment
                    docNumbers = receptionInstance?.t401NoAppointment
                }else if(params?.jenisRefund=="1"){
                    doc = DocumentCategory.findByDocumentCategoryAndStaDel("DOCPDPS","0")?DocumentCategory.findByDocumentCategoryAndStaDel("DOCPDPS","0"):new DocumentCategory(documentCategory: "DOCPDPS",description: "Pengembalian DP Service",staDel: "0",createdBy: "SYSTEM",lastUpdProcess: "CREATE",dateCreated: new Date(),lastUpdated: new Date()).save(flush: true)
                    description = "Pengembalian DP Service, No. WO. "+receptionInstance?.t401NoWO
                }else if(params?.jenisRefund=="2"){
                    doc = DocumentCategory.findByDocumentCategoryAndStaDel("DOCPDPOR","0")?DocumentCategory.findByDocumentCategoryAndStaDel("DOCPDPOR","0"):new DocumentCategory(documentCategory: "DOCPDPOR",description: "Pengembalian OR",staDel: "0",createdBy: "SYSTEM",lastUpdProcess: "CREATE",dateCreated: new Date(),lastUpdated: new Date()).save(flush: true)
                    description = "Pengembalian OR, No. WO. "+receptionInstance?.t401NoWO
                }
                def transactionCode        = new GenerateCodeService().generateTransactionCode()
                def transactionType        = TransactionType.findByTransactionTypeIlike("%"+metodh+"%")
                def transactionDate        = datatablesUtilService?.syncTime()
                def amount                 = jumlah
                def bankAccountNumber      = params.namaBank ? BankAccountNumber?.get(params.namaBank.toLong()) : null  //BankAccountNumber.findByAccountNumber(kasAccount)

                def transaction = new Transaction(
                        documentCategory: doc,
                        transactionCode: transactionCode,
                        transactionType: transactionType,
                        staOperasional: "1",
                        transactionDate: transactionDate,
                        companyDealer: params?.companyDealer,
                        transferDate: datatablesUtilService?.syncTime(),
                        inOutType: "OUT",
                        amount: amount,
                        description: description,
                        bankAccountNumber: bankAccountNumber,
                        dueDate: null,
                        staDel: "0",
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        docNumber : docNumbers,
                        dateCreated : datatablesUtilService?.syncTime(),
                        lastUpdated : datatablesUtilService?.syncTime()
                ).save(flush: true)

                return [nomorRefund:nomorRefund, err:null]
            }
        }
    }

}