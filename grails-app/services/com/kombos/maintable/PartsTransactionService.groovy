package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

class PartsTransactionService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c2 = PartsTransaction.createCriteria()
        def resultsCount = c2.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }

            if(params.sCriteria_Tanggal){
                ge("tglUpload",params.sCriteria_Tanggal)
                lt("tglUpload",params.sCriteria_Tanggal + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                max("tglUpload", "tglUpload")
                max("fileName", "fileName")
                max("staDel", "staDel")
                max("companyDealer", "companyDealer")
            }
            order("tglUpload", "desc")
            order("companyDealer", "desc")

        }

        def c = PartsTransaction.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if(params.sCriteria_companyDealer!='-'){
                    eq("companyDealer",CompanyDealer.findById(params.sCriteria_companyDealer))
                }
            }else{
                eq("companyDealer",params.userCompanyDealer)
            }
            if(params.sCriteria_Tanggal){
                ge("tglUpload",params.sCriteria_Tanggal)
                lt("tglUpload",params.sCriteria_Tanggal + 1)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
                max("tglUpload", "tglUpload")
                max("fileName", "fileName")
                max("staDel", "staDel")
                max("companyDealer", "companyDealer")
            }
            order("staDel", "asc")
            switch (sortProperty) {
                case "dateCreated" :
                    order("tglUpload", sortDir)
                    break
                case "companyDealer" :
                    order("companyDealer", sortDir)
                    break
                default:
                    order("companyDealer", sortDir)
                    break;
            }

        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it?.companyDealer.m011NamaWorkshop,

                    docNumber: it?.docNumber + " / " + it.fileName,

                    docNumberId: it?.docNumber,

                    staDel: it?.staDel,

                    dateCreated: it?.tglUpload?.format("dd/MM/yyyy"),

            ]
        }
        def cd = []
        try {
            def cdDefault = ["CV. KOMBOS JAKARTA", "HEAD OFFICE (HO) MANADO"]
            if(results.size()==0){
                cd = CompanyDealer.findAllByM011NamaWorkshopNotInList(cdDefault).m011NamaWorkshop.toString()
            }else{
                cd = CompanyDealer.findAllByM011NamaWorkshopNotInListAndM011NamaWorkshopNotInList(rows.companyDealer,cdDefault).m011NamaWorkshop.toString()
            }
        }catch(Exception e){

        }
        [sEcho: params.sEcho, iTotalRecords: resultsCount.size(), iTotalDisplayRecords: resultsCount.size(), aaData: rows, cd : cd ]

    }

    def printPartTrx(def params){
        def jsonArray = JSON.parse(params.docNumber)
        def reportData = new ArrayList();
        jsonArray.each {
            def ptr = PartsTransaction.findAllByDocNumberAndStaDel(it,'0')
            ptr.each {
                def data = [:]
                data.put("DEALER_CODE",it.dealerCode.toString())
                data.put("AREA_CODE",it.areaCode.toString())
                data.put("OUTLET_CODE",it.outletCode.toString())
                data.put("NOWO",it.noWo.toString().replace(',','#'))
                data.put("TGLWO",it.tanggalWo.toString().replace('-','/'))
                data.put("PART_NO_REQUEST",it.partNoRequest.toString().replace(',','#'))
                data.put("PART_DESC_REQUEST",it.partDescRequest.toString().replace(',','#'))
                data.put("PART_NO_SUPPLIED",it.partNoSupplied.toString().replace(',','#'))
                data.put("PART_DESC_SUPPLIED",it.partDescSupplied.toString().replace(',','#'))
                data.put("NON_TOYOTA_OIL",it.nonToyotaOil.toString().replace('-',''))
                data.put("REQUEST_DATE",it.requestDate.toString().replace('-','/'))
                data.put("REQUEST_QUANTITY",it.requestQty.toString().replace(',','#'))
                data.put("QUANTITY",it.qty.toString().replace(',','#'))
                data.put("CANCEL_QUANTITY",it.cancelQty.toString())
                data.put("SATUAN",it.satuan.toString().replace(',','#'))
                data.put("FILL_RATE",it.fillRate.toString().replace(',','#'))
                data.put("RATE",it.rate.toString().replace(',','#'))
                data.put("FLAG_COLOR","BLACK")
                reportData.add(data)
            }
        }

        return reportData
    }
}