package com.kombos.maintable



import com.kombos.baseapp.AppSettingParam
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class AlertService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
//    def session = RequestContextHolder?.currentRequestAttributes()?.getSession()
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy / hh:mm:ss");

        def c = Alert.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", params.companyDealer)
            ge("tglJamAlert",new Date().clearTime())
			if(params."sCriteria_jenisAlert"){
				jenisAlert {
					ilike("m901NamaAlert",params."sCriteria_jenisAlert")
				}
			}

			if(params."sCriteria_xNamaUserBaca"){
				ilike("xNamaUserBaca","%" + (params."sCriteria_xNamaUserBaca" as String) + "%")
			}

            if(params."sCriteria_namaFK"){
                ilike("namaFK","%"+params."sCriteria_namaFK"+"%");
            }

			if(params."sCriteria_noDokumen"){
                ilike("noDokumen","%"+params."sCriteria_noDokumen"+"%");
            }

			if(params."sCriteria_tglJamAlert"){
				ge("tglJamAlert",params."sCriteria_tglJamAlert")
				lt("tglJamAlert",params."sCriteria_tglJamAlert" + 1)
			}

			if(params."sCriteria_statusAksi"){
				ilike("statusAksi","%" + (params."sCriteria_statusAksi" as String) + "%")
			}

			if(params."sCriteria_tglJamAksi"){
				ge("tglAksi",params."sCriteria_tglJamAksi")
				lt("tglAksi",params."sCriteria_tglJamAksi" + 1)
			}

			if(params."sCriteria_userAksi"){
				ilike("userAksi","%" + (params."sCriteria_userAksi" as String) + "%")
			}

			switch(sortProperty){
                case "jenisAlert":
                    jenisAlert{
                        order("m901NamaAlert",sortDir)
                    }
                    break;
                case "namaFK":
                        order("namaFK",sortDir)
                    break;
                case "noDokumen":
                    order("noDokumen",sortDir)
                    break;
                case "xNamaUserBaca":
                    order("xNamaUserBaca",sortDir)
                    break;
                case "tglJamAlert":
                    order("tglJamAlert",sortDir)
                    break;
                case "statusAksi":
                    order("statusAksi",sortDir)
                    break;
                case "tglAksi":
                    order("tglAksi",sortDir)
                    break;
                case "userAksi":
                    order("userAksi",sortDir)
                    break;
                default:
					order('dateCreated',sortDir);
					break;
			}
		}
		
		def rows = []
		
		results.each {

			rows << [
			
						id: it.id,
			
						jenisAlert: it?.jenisAlert?.m901NamaAlert,

                        namaFK: it?.namaFK,

                    noDokumen: it?.noDokumen,

//						namaTabel: it.namaTabel,

                    xNamaUserBaca: it?.xNamaUserBaca,

						tglJamAlert: it?.tglJamAlert?it.tglJamAlert.format(dateFormat):"",

						statusAksi: it?.statusAksi,
			
						tglAksi: it?.tglAksi ? it?.tglAksi.format(dateFormat):"",

						userAksi: it?.userAksi
									
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Alert", params.id] ]
			return result
		}

		result.alertInstance = Alert.get(params.id)

		if(!result.alertInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Alert", params.id] ]
			return result
		}

		result.alertInstance = Alert.get(params.id)

		if(!result.alertInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Alert", params.id] ]
			return result
		}

		result.alertInstance = Alert.get(params.id)

		if(!result.alertInstance)
			return fail(code:"default.not.found.message")

		try {
			result.alertInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		Alert.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.alertInstance && m.field)
					result.alertInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Alert", params.id] ]
				return result
			}

			
			result.alertInstance = Alert.get(params.id)

			if(!result.alertInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.alertInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.alertInstance.properties = params
			
			if(params.statusAksi == 'BACA'){
				result.alertInstance.staBaca = '1'
				result.alertInstance.staTindakLanjut = '0'
				
				result.alertInstance.tglJamBaca = new Date()
				result.alertInstance.xNamaUserBaca = org.apache.shiro.SecurityUtils.subject.principal.toString()
			} else {
				result.alertInstance.staBaca = '0'
				result.alertInstance.staTindakLanjut = '1'
				
				result.alertInstance.tglJamTinjakLanjut = new Date()
				result.alertInstance.xNamaUserTindakLanjut = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}

			if(result.alertInstance.hasErrors() || !result.alertInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Alert", params.id] ]
			return result
		}

		result.alertInstance = new Alert()
		result.alertInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.alertInstance && m.field)
				result.alertInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Alert", params.id] ]
			return result
		}

		result.alertInstance = new Alert(params)

		if(result.alertInstance.hasErrors() || !result.alertInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def alert =  Alert.findById(params.id)
		if (alert) {
			alert."${params.name}" = params.value
			alert.save()
			if (alert.hasErrors()) {
				throw new Exception("${alert.errors}")
			}
		}else{
			throw new Exception("Alert not found")
		}
	}
}