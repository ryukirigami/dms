package com.kombos.baseapp

class AuditTrailPartialUploadService {

    def datatablesUtilService

    def getList(def params){
        def ret
        def res = datatablesUtilService.getDomainList(AuditTrailPartialUpload, params)
        def rows = []
        AuditTrailPartialUpload temp
        res?.rows.each {
            temp = it
            rows << [
					temp.partialUploadJobId,
					temp.actionType, 
					temp.oldvalue, 
					temp.newvalue,
					temp.createdate
            ]
        }
        res.rows = rows
        ret = [sEcho:res.sEcho, iTotalRecords:res.iTotalRecords, iTotalDisplayRecords:res.iTotalDisplayRecords, aaData:res.rows]

        return ret
    }
}
