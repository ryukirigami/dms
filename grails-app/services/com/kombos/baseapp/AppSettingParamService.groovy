package com.kombos.baseapp

class AppSettingParamService {

    static def APPLICATION_TITLE = 'application_title'
    static def WELCOME_TEXT = 'welcome_text'
    static def ATTACHMENT_MAX_SIZE = 'attachment_max_size'
    static def MAX_NUMBER_LINES_DISPLAYED = 'max_number_lines_displayed'
    static def APPLICATION_VERSION = 'application_version'

    static def THEME = 'theme'
    static def DEFAULT_LANGUAGE = 'default_language'
    static def DATE_FORMAT = 'date_format'
    static def TIME_FORMAT = 'time_format'
    static def USERS_DISPLAY_FORMAT = 'users_display_format'

	def auditTrailLogUtilService

    def gridUtilService

    def getList(def params){
        def ret = null

        try {
            def res = gridUtilService.getDomainList(AppSettingParam, params)
            def rows = []
            def counter = 0
            ((List<AppSettingParam>)(res?.rows))?.each{
                rows << [
                        id: counter ++,
                        cell:[
                            
                            it.approvalStatus,  
                            it.category,  
                            it.code,  
                            it.label,  
                            it.value,
                            it.defaultValue
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception{
        def appSettingParam = new AppSettingParam()
        
        appSettingParam.approvalStatus = params.approvalStatus 
        appSettingParam.category = params.category 
        appSettingParam.code = params.code 
        appSettingParam.label = params.label 
        appSettingParam.value = params.value 

        appSettingParam.save()
        if (appSettingParam.hasErrors()) {
            throw new Exception("${appSettingParam.errors}")
        }
    }

    def edit(params){
        def appSettingParam = AppSettingParam.findByCode(params.code)
        if (appSettingParam) {
            
            //appSettingParam.approvalStatus = params.approvalStatus
            //appSettingParam.category = params.category
            //appSettingParam.code = params.code
            //appSettingParam.label = params.label
            appSettingParam.value = params.value 

            appSettingParam.save()
            if (appSettingParam.hasErrors()) {
                throw new Exception("${appSettingParam.errors}")
            }
        }else{
            throw new Exception("AppSettingParam not found")
        }
    }

    def delete(params){
        def appSettingParam = AppSettingParam.findByCode(params.code)
        if (appSettingParam) {
            appSettingParam.delete()
        }else{
            throw new Exception("AppSettingParam not found")
        }
    }

    def editBatch(HashMap parameters, def params, def request){		
        AppSettingParam appSettingParam
        Map.Entry mapEntry
        Iterator iterator = parameters.entrySet().iterator();
        while (iterator.hasNext()) {
			def oldString = new StringBuilder()
			def newString = new StringBuilder()
            mapEntry = (Map.Entry) iterator.next()
			AppSettingParam.resetThis(mapEntry.getKey())
            appSettingParam = AppSettingParam.findByCode(mapEntry.getKey())
			
			def oldValue = appSettingParam.value
            appSettingParam.value = mapEntry.value
			def newValue = mapEntry.value
			if(oldValue != newValue && !(oldValue == null && newValue == '')){
				oldString.append(appSettingParam.code).append('=').append(oldValue)
				newString.append(appSettingParam.code).append('=').append(newValue)
				auditTrailLogUtilService.saveToAuditTrail(
					AuditTrailLog.UPDATE
					,appSettingParam.code
					,oldString.toString()
					,newString.toString()
					,auditTrailLogUtilService.setParamRequest(params, request, 'MENU_APP_SETTING')
				)
			}
			
            appSettingParam.save(flush: true)
			
        }
		
		
    }
}