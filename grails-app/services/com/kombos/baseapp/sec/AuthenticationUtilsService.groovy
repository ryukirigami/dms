package com.kombos.baseapp.sec

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.apache.shiro.SecurityUtils
import org.apache.shiro.crypto.hash.Sha512Hash

class AuthenticationUtilsService {

    def serviceMethod() {

    }

    def getUser(){
        return SecurityUtils.subject.principal
    }

    def hashPassword(String password){
        return new Sha512Hash(password).toHex()
    }

    def logout(HttpServletRequest request, HttpServletResponse response){
        //
    }
}
