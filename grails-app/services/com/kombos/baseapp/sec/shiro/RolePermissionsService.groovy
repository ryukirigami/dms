package com.kombos.baseapp.sec.shiro

import com.kombos.baseapp.ActionType
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.menu.Menu
import com.kombos.baseapp.menu.RoleMenu
import groovy.sql.Sql

class RolePermissionsService {
	final static String viewModule = 'index,getList,list,show'
	final static String addModule = 'create,save'
	final static String editModule = 'edit,update,updatefield'
	final static String deleteModule = 'delete,massdelete'

	def datatablesUtilService

	def auditTrailLogUtilService

	def dataSource

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Menu.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_menu") {
                ilike("label", "%"+params."sCriteria_menu"+"%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    label: it.label

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def getListExport(def params, def roleId)
    {
        try
        {
            def ret = null
            def sql=new Sql(dataSource)
            def rows=[]
            sql.eachRow("""\
select dm.parent_id as parent_menu,dm.label as menu,
case when permiss.view_s IS NULL  then 'No' else 'Yes' end as views,
case when permiss.add_s IS NULL  then 'No' else 'Yes' end as adds,
case when permiss.edit_s IS NULL  then 'No' else 'Yes' end as edits,
case when permiss.delete_s IS NULL  then 'No' else 'Yes' end as deletes
from dom_menu dm left outer join
(select
menu_id,
min(link_controller) link_controller,
min(case when p.action_type = 'VIEW' then action_string end) as view_s,
min(case when p.action_type = 'ADD' then action_string end) as add_s,
min(case when p.action_type = 'EDIT' then action_string end) as edit_s,
min(case when p.action_type = 'DELETE' then action_string end) as delete_s
from dom_shiropermissions p inner join
dom_shirorole_permissions rp on rp.permissions_string like p.link_controller+':'+p.action_string+'%' where p.checker = 1 and rp.role_id = :roleid
group by menu_id) permiss on dm.id = permiss.menu_id where dm.link_controller is not null order by dm.parent_id, dm.seq
 """, [roleid: params.role_id])
                    {
                        rows<<
                                [
                                        "parent_menu":it.parent_menu
                                        , "menu":it.menu
                                        , "views":it.views
                                        , "adds":it.adds
                                        , "edits":it.edits
                                        , "deletes":it.deletes
                                ]
                    }

            def tot = sql.firstRow("""\
select count(*) as total
from dom_menu dm left outer join
(select
menu_id,
link_controller,
min(case when p.action_type = 'VIEW' then action_string end) as view_s,
min(case when p.action_type = 'ADD' then action_string end) as add_s,
min(case when p.action_type = 'EDIT' then action_string end) as edit_s,
min(case when p.action_type = 'DELETE' then action_string end) as delete_s
from dom_shiropermissions p inner join
dom_shirorole_permissions rp on rp.permissions_string like p.link_controller||':'||p.action_string||'%' where p.checker = 1 and rp.role_id = :roleid
group by menu_id,
link_controller) permiss on dm.id = permiss.menu_id where dm.link_controller is not null
 """, [roleid: roleId])
            ret = [iTotalRecords:tot.total, iTotalDisplayRecords:tot.total, aaData:rows]
            return ret
        }
        catch (Exception e)
        {
            //println "Could not getListExport: ${e.getMessage()}"
        }
    }

	def getList(def params){
		def ret = null
        String lbl = ""
        if(params.label){
            lbl = params.label.replace(" ","")
            if(lbl.length()>0){
                lbl = params.label
            }
        }

        def results = RoleMenu.createCriteria().list {
            role{
                eq("id",params.role_id)
            }
            menu{
                if(lbl){
                    ilike("label","%"+lbl+"%")
                }
                order("label")
            }
        }
        def row = []
        int i=1
        results.each {
            String isView = "No"
            String isAdd = "No"
            String isEdit = "No"
            String isDelete = "No"
            def details = Permission.findByRoleAndMenu(it.role,it.menu)
            if(details){
                if(details.viewString && details.viewString=="0"){
                    isView = "Yes"
                }
                if(details.addString && details.addString=="0"){
                    isAdd = "Yes"
                }
                if(details.editString && details.editString=="0"){
                    isEdit = "Yes"
                }
                if(details.deleteString && details.deleteString=="0"){
                    isDelete = "Yes"
                }
            }
            row << [
                    params.role_id,
                    it.menu.label,
                    "",
                    it.menu.linkController,
                    isView,
                    isAdd,
                    isEdit,
                    isDelete,
                    it.menu.menuCode,
                    i
            ]
            i++
        }

		ret = [sEcho:params.sEcho, iTotalRecords:results.size(), iTotalDisplayRecords:results.size(), aaData:row]

		return ret
	}

	def add(params) throws Exception{
		def role = new Role()

		role.name = params.name
		role.permissions = params.permissions
		role.dateCreated =  datatablesUtilService?.syncTime()
		role.lastUpdated =  datatablesUtilService?.syncTime()

		role.save()
		if (role.hasErrors()) {
			throw new Exception("${role.errors}")
		}
	}


    def edit(params) {

        def role = Role.findById(params.role_id)
        if (role) {
            Menu menu = Menu.findByMenuCode(params.menu_code)
            def cekMenu = RoleMenu.findByRoleAndMenu(role,menu)
            role.removeFromAksesMenu(params.menu_code)
            boolean addAkses = false
            def willDelete = []

            if (menu) {
                def menuPermission = Permission.findAllByMenu(menu)
                menuPermission.each() { Permission permission ->
                    willDelete << permission.linkController + ":" + permission.actionString
                }

                willDelete.each {
                    role.removeFromPermissions(it)
                }

                menuPermission.each() { Permission permission ->

                    def viewString
                    def addString
                    def editString
                    def deleteString

                    def actionType = permission.actionType

                    if (actionType == ActionType.VIEW) {
                        viewString = permission.actionString
                    }

                    if (actionType == ActionType.ADD) {
                        addString = permission.actionString
                    }

                    if (actionType == ActionType.EDIT) {
                        editString = permission.actionString
                    }

                    if (actionType == ActionType.DELETE) {
                        deleteString = permission.actionString
                    }

                    if (params.allow_module.equals("Yes") && viewString) {
                        viewString.tokenize(",").each { perms ->
                            role.addToPermissions(permission.linkController + ":" + perms)
                        }
                        addAkses = true
                    }

                    if (params.allow_add.equals("Yes") && addString) {
                        addString.tokenize(",").each { perms ->
                            role.addToPermissions(permission.linkController + ":" + perms)
                        }
                        addAkses = true
                    }

                    if (params.allow_edit.equals("Yes") && editString) {
                        editString.tokenize(",").each { perms ->
                            role.addToPermissions(permission.linkController + ":" + perms)
                        }
                        addAkses = true
                    }

                    if (params.allow_delete.equals("Yes") && deleteString) {
                        deleteString.tokenize(",").each { perms ->
                            role.addToPermissions(permission.linkController + ":" + perms)
                        }
                        addAkses = true
                    }

                    if (addAkses) {
                        role.addToAksesMenu(menu.menuCode)
                        addTopMenu(role, menu.parent)
                    }

                    if (role.hasErrors()) {
                        throw new Exception("${role.errors}")
                    } else {

                    }
                }

            }
        } else {
            throw new Exception("Role not found")
        }
    }

    def addTopMenu(Role role, Menu menu) {
        if(menu){
            if (menu.parent) {
                role.addToAksesMenu(menu.menuCode)
                addTopMenu(role, menu.parent)
            }else {
                role.addToAksesMenu(menu.menuCode)
            }
        }
    }

	def delete(params){
		def role = Role.findBy(params.null)
		if (role) {
			role.delete()
		}else{
			throw new Exception("Role not found")
		}
	}
}
