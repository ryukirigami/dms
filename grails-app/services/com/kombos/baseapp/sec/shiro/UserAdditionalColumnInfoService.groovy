package com.kombos.baseapp.sec.shiro

class UserAdditionalColumnInfoService {

    def gridUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(UserAdditionalColumnInfo, params)
            def rows = []
            def counter = 0
            ((List<UserAdditionalColumnInfo>) (res?.rows))?.each {
                rows << [
                        id: counter++,
                        cell: [

                                it.columnName,
                                it.label
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def userAdditionalColumnInfo = new UserAdditionalColumnInfo()

        userAdditionalColumnInfo.columnName = params.columnName
        userAdditionalColumnInfo.label = params.label

        userAdditionalColumnInfo.save()
        if (userAdditionalColumnInfo.hasErrors()) {
            throw new Exception("${userAdditionalColumnInfo.errors}")
        }
    }

    def edit(params) {
        def userAdditionalColumnInfo = UserAdditionalColumnInfo.findByColumnName(params.columnName)
        if (userAdditionalColumnInfo) {

            userAdditionalColumnInfo.columnName = params.columnName
            userAdditionalColumnInfo.label = params.label

            userAdditionalColumnInfo.save()
            if (userAdditionalColumnInfo.hasErrors()) {
                throw new Exception("${userAdditionalColumnInfo.errors}")
            }
        } else {
            throw new Exception("UserAdditionalColumnInfo not found")
        }
    }

    def delete(params) {
        def userAdditionalColumnInfo = UserAdditionalColumnInfo.findByColumnName(params.columnName)
        if (userAdditionalColumnInfo) {
            userAdditionalColumnInfo.delete()
        } else {
            throw new Exception("UserAdditionalColumnInfo not found")
        }
    }

}