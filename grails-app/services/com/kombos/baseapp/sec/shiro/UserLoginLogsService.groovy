package com.kombos.baseapp.sec.shiro

import java.text.SimpleDateFormat

class UserLoginLogsService {

    def datatablesUtilService

    def appSettingParamDateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def appSettingParamTimeFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.TIME_FORMAT)

    def getList(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat + " " + timeFormat)
        def ret
        def res = datatablesUtilService.getDomainList(UserLoginLogs, params)
        def rows = []
        UserLoginLogs temp

        List<UserLoginLogs> export = new ArrayList<UserLoginLogs>()

        res?.rows.each {
            temp = it
            export.add(temp)
            rows << [
                    temp.pkid,
                    temp.username,
                    sdf.format(temp.loginDatetime),
                    temp.loginStatusLookupCode
            ]
        }
        res.rows = rows
        ret = [sEcho:res.sEcho, iTotalRecords:res.iTotalRecords, iTotalDisplayRecords:res.iTotalDisplayRecords, aaData:res.rows,export:export]

        return ret
    }

    def getListExport(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat+" "+timeFormat)
        def ret
        def res = getDomainListExport(UserLoginLogs, params)
        def rows = []
        UserLoginLogs temp
        res?.rows.each {
            temp = it
            rows << [
                    "pkid":temp.pkid,
                    "username":temp.username,
                    "loginDatetime":sdf.format(temp.loginDatetime),
                    "loginStatusLookupCode":temp.loginStatusLookupCode
            ]
        }
        res.rows = rows
        ret = [sEcho:res.sEcho, iTotalRecords:res.iTotalRecords, iTotalDisplayRecords:res.iTotalDisplayRecords, aaData:res.rows]

        return ret
    }

    def getDomainListExport(Class domain, def params){
        def propertiesToRender
        def sortProperty
        if(params.sColumns)
        {
            propertiesToRender = params.sColumns.split(",")
            sortProperty = propertiesToRender[params.iSortCol_0 as int]
        }

        def filters = []
        def filterParams = [:]
        def x = 0
        def columnFilters = []
        def columnFilterSearch = false
        if ( params.sSearch ) {
            filterParams.filter= "%${params.sSearch}%"
        }

        propertiesToRender.each { prop ->
            filters << "p.${prop} like :filter"
            if(params."sSearch_${x}"){
                if(params.initoperator && params."sFilter_${x}"){
                    switch(params."sFilter_${x}"){
                        case "beginwith":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)+ "%"
                            break;
                        case "contains":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) +"%"
                            break;
                        case "endswith":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = "%" + (params."sSearch_${x}" as String)
                            break;
                        case "equals":
                            columnFilters << "UPPER(p.${prop}) = UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "doesntequal":
                            columnFilters << "UPPER(p.${prop}) <> UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "islessthan":
                            columnFilters << "UPPER(p.${prop}) < UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "islessthanorequalto":
                            columnFilters << "UPPER(p.${prop}) <= UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "isgreaterthan":
                            columnFilters << "UPPER(p.${prop}) > UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "isgreaterthanorequalto":
                            columnFilters << "UPPER(p.${prop}) >= UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                    }

                }else{
                    columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                    filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) + "%"
                }

                columnFilterSearch = true
            }
            x++
        }
        def filter = filters.join(" OR ")
        def columnFilter = columnFilters.join(" AND ")


        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData=[]                // Array of products.

        dataToRender.iTotalRecords = domain.count();
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        def query = new StringBuilder("from ${domain.getName()} as p where 1=1")
        def countQuery = new StringBuilder("select count(*) from ${domain.getName()} as p where 1=1")
//        def defaultQueryParams = [max: params.iDisplayLength as int, offset: params.iDisplayStart as int]
        def queryparams
        if(params.initsearch){
            query.append(" and ("+params.initquery+")")
            countQuery.append(" and ("+params.initquery+")")

            if (params.queryparamsdate){
                queryparams = params.queryparamsdate
                Date test
                queryparams?.each{
                    test = it.value
                    filterParams."${it.key}" = it.value as Date
                }
            }
        }

        if ( params.sSearch ) {
            query.append(" and (${filter})")
        }
        if (columnFilterSearch){
            query.append(" and (${columnFilter})")
        }

        def sortDir
        if(params.sColumns)
        {
            sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
            query.append(" order by p.${sortProperty} ${sortDir}")
        }

        def rows = []
        def result
        if ( params.sSearch || columnFilterSearch) {
            // Revise the number of total display records after applying the filter
//			if(params.initsearch){
//                countQuery.append(" and ("+params.initquery+")")
//            }

            if ( params.sSearch ) {
                countQuery.append(" and (${filter})")
            }

            if (columnFilterSearch){
                countQuery.append(" and (${columnFilter})")
            }

            result = domain.executeQuery(countQuery.toString(),filterParams)

            if ( result ) {
                dataToRender.iTotalDisplayRecords = result[0]
            }

//            rows = domain.findAll(query.toString(),
//                    filterParams,
//                    [max: params.iDisplayLength as int, offset: params.iDisplayStart as int])

            rows = domain.findAll(query.toString(),
                    filterParams)

        } else {
//            rows = domain.findAll(query.toString(),
//            [max: params.iDisplayLength as int, offset: params.iDisplayStart as int])

            rows = domain.findAll(query.toString(),filterParams)

            result=domain.executeQuery(countQuery.toString(),filterParams)

            if ( result ) {
                dataToRender.iTotalDisplayRecords = result[0]
            }
        }

//			rows?.each { row ->
//				def rowData = [:]
//				propertiesToRender.each { prop ->
//					rowData."$prop" = row."$prop"
//				}
//			   dataToRender.aaData << rowData
//			}


        dataToRender.rows = rows

        return dataToRender
    }
}
