package com.kombos.baseapp.sec.shiro

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

import java.text.SimpleDateFormat

import org.apache.shiro.crypto.hash.Sha512Hash
import org.apache.shiro.mgt.DefaultSecurityManager
import org.apache.shiro.subject.SimplePrincipalCollection
import org.apache.shiro.subject.Subject

class UserService {
    def menuService

    def datatablesList(def params) {
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        def c = User.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            //			switch(sortProperty){
            //				default:
            if(params."sCriteria_tanggalExpired"){
                ge("t001ExpiredAccountDate",params."sCriteria_tanggalExpired")
                lt("t001ExpiredAccountDate",params."sCriteria_tanggalExpired" + 1)
            }
            if(params."sCriteria_lastLoggedIn"){
                ge("lastLoggedIn",params."sCriteria_lastLoggedIn")
                lt("lastLoggedIn",params."sCriteria_lastLoggedIn" + 1)
            }
            if(params."sCriteria_divisi"){
                divisi{
                    ilike("m012NamaDivisi","%" + (params."sCriteria_divisi" as String) + "%")
                }
            }
            if(params."sCriteria_companyDealer"){
                companyDealer{
                    ilike("m011NamaWorkshop","%" + (params."sCriteria_companyDealer" as String) + "%")
                }
            }
            if(params."sCriteria_namaPegawai"){
                ilike("t001NamaPegawai","%" + (params."sCriteria_namaPegawai" as String) + "%")
            }
            if(params."sCriteria_username"){
                ilike("username","%" + (params."sCriteria_username" as String) + "%")
            }
            if(params."sCriteria_inisial"){
                ilike("t001Inisial","%" + (params."sCriteria_inisial" as String) + "%")
            }
            if(params."sCriteria_roles"){
                roles{
                    ilike("name","%" + (params."sCriteria_roles" as String) + "%")
                }
            }
            if(params."sCriteria_status"){
                eq("status",params."sCriteria_status",[ignoreCase : true])
            }
            if(params."sCriteria_loggedInFrom"){
                ilike("loggedInFrom","%" + (params."sCriteria_loggedInFrom" as String) + "%")
            }
            if(params."sCriteria_createdBy"){
                ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
            }
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("companyDealer", params.companyDealer)
            }

            switch (sortProperty) {
                case "divisi":
                    divisi{
                        order("m012NamaDivisi",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)

                    break;
            }

        }

        HashSet hs = new HashSet();
        hs.addAll(results);
        results.clear();
        results.addAll(hs);


        def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def sdf = new SimpleDateFormat(dateFormat)
        def rows = []


        results.each {
            def lastLoggedIn = ""
            if(it.lastLoggedIn)
                lastLoggedIn = sdf.format(it.lastLoggedIn)

            def updatedOn = ""
            if(it.updatedOn)
                updatedOn = sdf.format(it.updatedOn)

            def effectiveStartDate = ""
            if(it.effectiveStartDate)
                effectiveStartDate = sdf.format(it.effectiveStartDate)

            def effectiveEndDate = ""
            if(it.effectiveEndDate)
                effectiveEndDate = sdf.format(it.effectiveEndDate)


            rows << [
                    id : it.id,
                    companyDealer: it?.companyDealer ? it?.companyDealer?.m011NamaWorkshop : "",
                    username : it.username,
                    divisi : it?.divisi?.m012NamaDivisi,
                    namaPegawai : it.t001NamaPegawai,
                    inisial : it.t001Inisial,
                    tanggalExpired : it?.t001ExpiredAccountDate ? it?.t001ExpiredAccountDate?.format("dd/MM/yyyy") : "",
                    roles : it.roles.name,
                    lastLoggedIn : lastLoggedIn,
                    loggedInFrom : it.loggedInFrom,
                    createdBy : it.createdBy,
                    updatedOn : updatedOn,
                    updatedBy : it.updatedBy,
                    fullname : it.fullname
            ]

        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        ret
    }

    def checkUserAuthorities(){
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        def autorities = [:]

        roles.each {
            if(it.authority == "PETUGAS_NPB_HO")
                autorities.put("petugasNPBHO", true)
            else if(it.authority == "PETUGAS_DO")
                autorities.put("petugasDO",true)
            else if(it.authority == "PETUGAS_NPB_JKT")
                autorities.put("petugasNPBJKT",true)
            else if(it.authority == "PARTS_HA")
                autorities.put("partsHA", true)
            else if(it.authority == "KEPALA_BENGKEL")
                autorities.put("kepalaBengkel", true)
            else if(it.authority == "PARTSMAN")
                autorities.put("partsman", true)
            else if(it.authority == "ADMINISTRATION_HEAD")
                autorities.put("adminHead", true)
            else if(it.authority == "TEKNISI")
                autorities.put("teknisi", true)
            else if(it.authority == "SERVICE_ADVISOR_GENERAL_REPAIR")
                autorities.put("SAGR", true)

        }

        autorities.put("user",user)


        return autorities

    }
}
