package com.kombos.baseapp.sec.shiro



import com.kombos.baseapp.AppSettingParam

class RoleService {
    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def generateCodeService
    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Role.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_role") {
                ilike("name", "%" + (params."sCriteria_role" as String) + "%")
            }

            if (params."sCriteria_maxDiscGoodwillPersen") {
                eq("maxDiscGoodwillPersen", Double.parseDouble(params."sCriteria_maxDiscGoodwillPersen"))
            }

            if (params."sCriteria_maxDiscGoodwillRp") {
                eq("maxDiscGoodwillRp", Double.parseDouble(params."sCriteria_maxDiscGoodwillRp"))
            }

            if (params."sCriteria_maxPersenSpDiscJasa") {
                eq("maxPersenSpDiscJasa", Double.parseDouble(params."sCriteria_maxPersenSpDiscJasa"))
            }

            if (params."sCriteria_maxPersenSpDiscPart") {
                eq("maxPersenSpDiscParts", Double.parseDouble(params."sCriteria_maxPersenSpDiscPart"))
            }

            if (params."sCriteria_lihatReport") {
                ilike("staLihatReportWorkshopLain", "%" + (params."sCriteria_lihatReport" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    name: it.name,

                    maxDiscGoodwillPersen: it.maxDiscGoodwillPersen,

                    maxDiscGoodwillRp: it.maxDiscGoodwillRp,

                    maxPersenSpDiscRp: it.maxPersenSpDiscRp,

                    maxPersenSpDiscJasa: it.maxPersenSpDiscJasa,

                    maxPersenSpDiscParts: it.maxPersenSpDiscParts,

                    staLihatReportWorkshopLain: it.staLihatReportWorkshopLain,

                    lastUpdProcess: it.lastUpdProcess,

                    permissions: it.permissions,

                    users: it.users,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Role", params.id]]
            return result
        }

        result.roleInstance = Role.get(params.id)

        if (!result.roleInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Role", params.id]]
            return result
        }

        result.roleInstance = Role.get(params.id)

        if (!result.roleInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Role", params.id]]
            return result
        }

        result.roleInstance = Role.get(params.id)

        if (!result.roleInstance)
            return fail(code: "default.not.found.message")

        try {
            def cek = User.createCriteria().list {
                eq("staDel","0")
            }
            if(cek){
                for(find in cek.roles){
                    if(find==result.roleInstance){
                        return [ada: "ada"]
                    }
                }
            }
            result.roleInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            result.roleInstance?.setLastUpdProcess("DELETE")
            result.roleInstance?.setStaDel("1")
            result.roleInstance.save(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        Role.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.roleInstance && m.field)
                    result.roleInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Role", params.id]]
                return result
            }

            result.roleInstance = Role.get(params.id)
            def role = Role.createCriteria().list {
                eq("staDel","0")
                eq("name",params.name,[ignoreCase:true])
                eq("staLihatReportWorkshopLain",params.staLihatReportWorkshopLain)
                eq("maxPersenSpDiscParts",Double.parseDouble(params.maxPersenSpDiscParts))
                eq("maxPersenSpDiscJasa",Double.parseDouble(params.maxPersenSpDiscJasa))
                eq("maxDiscGoodwillPersen",Double.parseDouble(params.maxDiscGoodwillPersen))
                eq("maxDiscGoodwillPersen",Double.parseDouble(params.maxDiscGoodwillPersen))
            }
            def role2 = Role.createCriteria().list {
                eq("staDel","0")
                eq("name",params.name,[ignoreCase:true])
            }

            if(role){
                for(find in role){
                    if(find?.id!=result.roleInstance.id){
                        return [ada : "ada",instance : result.roleInstance]
                        break
                    }
                }
            }
            if(role2){
                for(find in role2){
                    if(find?.id!=result.roleInstance.id){
                        return [ada : "ada",instance : result.roleInstance]
                        break
                    }
                }
            }

            if (!result.roleInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.roleInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }
            params.lastUpdated =  datatablesUtilService?.syncTime()
            result.roleInstance.properties = params
//            result.roleInstance?.id = result.roleInstance?.name
            result.roleInstance?.authority = result.roleInstance?.name
            result.roleInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            result.roleInstance?.setLastUpdProcess("UPDATE")


            if (result.roleInstance.hasErrors() || !result.roleInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Role", params.id]]
            return result
        }

        result.roleInstance = new Role()
        result.roleInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.roleInstance && m.field)
                result.roleInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Role", params.id]]
            return result
        }

        result.roleInstance = new Role(params)
//        result.roleInstance?.id = result.roleInstance?.name
        result.roleInstance?.authority = result.roleInstance?.name
        result.roleInstance?.setStaDel("0")
        result.roleInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        result.roleInstance?.setLastUpdProcess("INSERT")
//        result.roleInstance?.setKodeRole(generateCodeService.codeGenerateSequence("kodeRole",null))

        def role = Role.createCriteria().list {
            eq("staDel","0")
            eq("name",params.name,[ignoreCase:true])
            if(params.maxPersenSpDiscJasa){
                eq("staLihatReportWorkshopLain",params.staLihatReportWorkshopLain)
            }
            eq("maxPersenSpDiscParts",Double.parseDouble(params.maxPersenSpDiscParts))
            eq("maxPersenSpDiscJasa",Double.parseDouble(params.maxPersenSpDiscJasa))
            eq("maxDiscGoodwillPersen",Double.parseDouble(params.maxDiscGoodwillPersen))
            eq("maxDiscGoodwillPersen",Double.parseDouble(params.maxDiscGoodwillPersen))
        }
        def role2 = Role.createCriteria().list {
            eq("staDel","0")
            eq("name",params.name,[ignoreCase:true])
        }
        if(role){
            return [ada : "ada",instance : result.roleInstance]
        }
        if(role2){
            return [ada : "ada",instance : result.roleInstance]
        }
        if (result.roleInstance.hasErrors() || !result.roleInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def role = Role.findById(params.id)
        if (role) {
            role."${params.name}" = params.value
            role.save()
            if (role.hasErrors()) {
                throw new Exception("${role.errors}")
            }
        } else {
            throw new Exception("Role not found")
        }
    }

}