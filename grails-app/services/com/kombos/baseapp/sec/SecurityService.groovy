package com.kombos.baseapp.sec

import com.kombos.baseapp.sec.shiro.User
import com.kombos.baseapp.sec.shiro.UserLoginLogs
import com.kombos.baseapp.utils.SecurityChecklistUtilService
import org.apache.shiro.authc.AuthenticationException

import javax.servlet.http.HttpServletRequest

class SecurityService {

	def grailsApplication

	def serviceMethod() {
	}

	def doAuthenticationChecklist(User user, HttpServletRequest request){
		org.apache.shiro.web.servlet.ShiroHttpServletRequest
		if (user){
			SecurityChecklistUtilService securityChecklistUtil
			//checking password expiry
			securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PASSWORD_CYCLES)
			if(securityChecklistUtil.enablePasswordCycles()){
				if(user.status == User.STATUS_PASSWORD_EXPIRED){ // if already expired
					throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('login.failed.password_cycles', null, "Your password has expired, please contact administrator", request.getLocale()))
				} else if (user.canPasswordExpire && securityChecklistUtil.getIntervalPasswordExpiry(user.username) < 0){ // if expired
					user.status = User.STATUS_PASSWORD_EXPIRED
					user.save()
					throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('login.failed.password_cycles', null, "Your password has expired, please contact administrator", request.getLocale()))
				}
			}

			//Checking the inactivity
			securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_AUTOMATIC_DISABLE_USER_PROFILE_AFTER_INACTIVE)
			def daysInactiveAllowed = securityChecklistUtil.getAutomaticDisableUserProfileAfterInactive()
			//            if(user.accountExpired){
			//                throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('login.failed.exceed_maximum_inactivity_allowed', [daysInactiveAllowed] as Object[], "You exceed maximum days allowed for inactivity ( "+daysInactiveAllowed+" days), your user is locked", request.getLocale()))
			//            }else if(!securityChecklistUtil.validateInactiveDaysAllowed(user.username)) {
			//                user.accountExpired = true
			//                user.status = User.STATUS_EXPIRED
			//                user.save()
			//                throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('login.failed.exceed_maximum_inactivity_allowed', [daysInactiveAllowed] as Object[], "You exceed maximum days allowed for inactivity ( "+daysInactiveAllowed+" days), your user is locked", request.getLocale()))
			//            }
			if(user.status == User.STATUS_DORMANT) {
				throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('login.failed.exceed_maximum_inactivity_allowed', [daysInactiveAllowed] as Object[], "You exceed maximum days allowed for inactivity ( "+daysInactiveAllowed+" days)", request.getLocale()))
			}

			//checking password locked
			securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_DISABLE_ACCOUNT_AFTER_FAILED_LOGIN)
			if (securityChecklistUtil.enableDisableAccountAfterFailedLogin()){
				if(user.status == User.STATUS_LOCKED){
					throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('login.failed.user_locked', null, "Your account is locked, please contact administrator", request.getLocale()))
				}
			}

			if(user.status != User.STATUS_ACTIVE || !user.enabled){
				throw new AuthenticationException("Your account has been locked")
			}

			user.loggedInFrom = "${request.getRemoteUser()}:${request.getRemoteAddr()}/${request.getLocalName()}:${request.getLocalAddr()}:${request.getLocalPort()}"
			user.lastLoggedIn = new Date()
			user.wrongPasswordCounter = 0
			if(user.save()){
				new UserLoginLogs(username: user.username, loginStatusLookupCode:UserLoginLogs.SUCCESS_LOGIN_STATUS, loginDatetime:new Date()).save(flush: true)
			}
		}else{
			throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('security.denied.notfound', null, 'User not found', request.getLocale()))
		}
	}
}
