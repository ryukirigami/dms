package com.kombos.baseapp.utils

import grails.converters.JSON
import groovy.time.TimeCategory
import org.springframework.web.context.request.RequestContextHolder

class DatatablesUtilService {

    def serviceMethod() {

    }
	
	def updateField(Class domain, def params){
		def instance =  domain.findById(params.id)
		if (instance) {
			instance."${params.name}" = params.value
			instance.save()
			if (instance.hasErrors()) {
				throw new Exception("${domain.errors}")
			}
		}else{
			throw new Exception("object not found")
		}
	}

    def getDomainListFormManagement(Class domain, def params){
        def propertiesToRender = params.sColumns.split(",")

        def filters = []
        def filterParams = [:]
        def x = 0
        def columnFilters = []
        def columnFilterSearch = false
        if ( params.sSearch ) {
            filterParams.filter= "%${params.sSearch}%"
        }

        propertiesToRender.each { prop ->
            filters << "p.${prop} like :filter"
            if(params."sFilter_${x}"){
                if(params.initoperator && params."sFilter_${x}"){
                    switch(params."sFilter_${x}"){
                        case "beginwith":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = (params."sFilter_${x}" as String)+ "%"
                            break;
                        case "contains":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = "%" + (params."sFilter_${x}" as String) +"%"
                            break;
                        case "endswith":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = "%" + (params."sFilter_${x}" as String)
                            break;
                        case "equals":
                            columnFilters << "UPPER(p.${prop}) = UPPER(:${prop})"
                            filterParams."${prop}" = (params."sFilter_${x}" as String)
                            break;
                        case "doesntequal":
                            columnFilters << "UPPER(p.${prop}) <> UPPER(:${prop})"
                            filterParams."${prop}" = (params."sFilter_${x}" as String)
                            break;
                        case "islessthan":
                            columnFilters << "UPPER(p.${prop}) < UPPER(:${prop})"
                            filterParams."${prop}" = (params."sFilter_${x}" as String)
                            break;
                        case "islessthanorequalto":
                            columnFilters << "UPPER(p.${prop}) <= UPPER(:${prop})"
                            filterParams."${prop}" = (params."sFilter_${x}" as String)
                            break;
                        case "isgreaterthan":
                            columnFilters << "UPPER(p.${prop}) > UPPER(:${prop})"
                            filterParams."${prop}" = (params."sFilter_${x}" as String)
                            break;
                        case "isgreaterthanorequalto":
                            columnFilters << "UPPER(p.${prop}) >= UPPER(:${prop})"
                            filterParams."${prop}" = (params."sFilter_${x}" as String)
                            break;
                    }

                }else{
                    columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                    filterParams."${prop}" = "%" + (params."sFilter_${x}" as String) + "%"
                }

                columnFilterSearch = true
            }
            x++
        }
        def filter = filters.join(" OR ")
        def columnFilter = columnFilters.join(" AND ")


        def dataToRender = [:]
        dataToRender.sEcho = params.sEcho
        dataToRender.aaData=[]                // Array of products.

        dataToRender.iTotalRecords = domain.count();
        dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

        def query = new StringBuilder("from ${domain.getName()} as p where 1=1")
        def countQuery = new StringBuilder("select count(*) from ${domain.getName()} as p where 1=1")
        def defaultQueryParams = [max: params.iDisplayLength as int, offset: params.iDisplayStart as int]
        def queryparams
        if(params.initsearch){
            query.append(" and ("+params.initquery+")")
            countQuery.append(" and ("+params.initquery+")")

            if (params.queryparamsdate){
                queryparams = params.queryparamsdate
                Date test
                queryparams?.each{
                    test = it.value
                    filterParams."${it.key}" = it.value as Date
                }
            }
        }

        if ( params.sSearch ) {
            query.append(" and (${filter})")
        }
        if (columnFilterSearch){
            query.append(" and (${columnFilter})")
        }
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        query.append(" order by p.${sortProperty} ${sortDir}")

        def rows = []
        def result
        if ( params.sSearch || columnFilterSearch) {
            // Revise the number of total display records after applying the filter
//			if(params.initsearch){
//                countQuery.append(" and ("+params.initquery+")")
//            }

            if ( params.sSearch ) {
                countQuery.append(" and (${filter})")
            }

            if (columnFilterSearch){
                countQuery.append(" and (${columnFilter})")
            }

            result = domain.executeQuery(countQuery.toString(),filterParams)

            if ( result ) {
                dataToRender.iTotalDisplayRecords = result[0]
            }

//            rows = domain.findAll(query.toString(),
//                    filterParams,
//                    [max: params.iDisplayLength as int, offset: params.iDisplayStart as int])

            rows = domain.findAll(query.toString(),
                    filterParams,
                    defaultQueryParams)
        } else {
//            rows = domain.findAll(query.toString(),
//            [max: params.iDisplayLength as int, offset: params.iDisplayStart as int])
            rows = domain.findAll(query.toString(),filterParams,defaultQueryParams)

            result=domain.executeQuery(countQuery.toString(),filterParams)

            if ( result ) {
                dataToRender.iTotalDisplayRecords = result[0]
            }
        }

//			rows?.each { row ->
//				def rowData = [:]
//				propertiesToRender.each { prop ->
//					rowData."$prop" = row."$prop"
//				}
//			   dataToRender.aaData << rowData
//			}
        dataToRender.rows = rows

        return dataToRender
    }
	
	def getDomainList(Class domain, def params){
		def propertiesToRender = params.sColumns.split(",")
		
		def filters = []
		def filterParams = [:]
		def x = 0
		def columnFilters = []
		def columnFilterSearch = false
		if ( params.sSearch ) {
			filterParams.filter= "%${params.sSearch}%"
		}
		
		propertiesToRender.each { prop ->
			   filters << "p.${prop} like :filter"
			   if(params."sSearch_${x}"){
				   if(params.initoperator && params."sFilter_${x}"){
					   switch(params."sFilter_${x}"){
						   case "beginwith":
						   columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
						   filterParams."${prop}" = (params."sSearch_${x}" as String)+ "%" 
						   break;
						   case "contains":
							   columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
							   filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) +"%"
							   break;
						   case "endswith":
							   columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
							   filterParams."${prop}" = "%" + (params."sSearch_${x}" as String)
							   break;
						   case "equals":
							   columnFilters << "UPPER(p.${prop}) = UPPER(:${prop})"
							   filterParams."${prop}" = (params."sSearch_${x}" as String)
							   break;
						   case "doesntequal":
							   columnFilters << "UPPER(p.${prop}) <> UPPER(:${prop})"
							   filterParams."${prop}" = (params."sSearch_${x}" as String)
							   break;
						   case "islessthan":
							   columnFilters << "UPPER(p.${prop}) < UPPER(:${prop})"
							   filterParams."${prop}" = (params."sSearch_${x}" as String)
							   break;
						   case "islessthanorequalto":
							   columnFilters << "UPPER(p.${prop}) <= UPPER(:${prop})"
							   filterParams."${prop}" = (params."sSearch_${x}" as String)
							   break;
						   case "isgreaterthan":
							   columnFilters << "UPPER(p.${prop}) > UPPER(:${prop})"
							   filterParams."${prop}" = (params."sSearch_${x}" as String)
							   break;
						   case "isgreaterthanorequalto":
							   columnFilters << "UPPER(p.${prop}) >= UPPER(:${prop})"
							   filterParams."${prop}" = (params."sSearch_${x}" as String)
							   break;
						   }
					   
					   	}else{
						   columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
						   filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) + "%"
					   	}
				   
				   columnFilterSearch = true
			   } 
			   x++
		}
		def filter = filters.join(" OR ")
		def columnFilter = columnFilters.join(" AND ")
		
		
		def dataToRender = [:]
		dataToRender.sEcho = params.sEcho
		dataToRender.aaData=[]                // Array of products.
		
		dataToRender.iTotalRecords = domain.count();
		dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords
		
		def query = new StringBuilder("from ${domain.getName()} as p where 1=1")
        def countQuery = new StringBuilder("select count(*) from ${domain.getName()} as p where 1=1")
        def defaultQueryParams = [max: params.iDisplayLength as int, offset: params.iDisplayStart as int]
        def queryparams
        if(params.initsearch){
            query.append(" and ("+params.initquery+")")
            countQuery.append(" and ("+params.initquery+")")

            if (params.queryparamsdate){
                queryparams = params.queryparamsdate
                Date test
                queryparams?.each{
                    test = it.value
                    filterParams."${it.key}" = it.value as Date
                }
            }
        }

		if ( params.sSearch ) {
	        query.append(" and (${filter})")
		}
		if (columnFilterSearch){
			query.append(" and (${columnFilter})")
		}
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		query.append(" order by p.${sortProperty} ${sortDir}")

		def rows = []
        def result
		if ( params.sSearch || columnFilterSearch) {
			   // Revise the number of total display records after applying the filter
//			if(params.initsearch){
//                countQuery.append(" and ("+params.initquery+")")
//            }

            if ( params.sSearch ) {
	            countQuery.append(" and (${filter})")
			}

			if (columnFilterSearch){
				countQuery.append(" and (${columnFilter})")
			}
			
			result = domain.executeQuery(countQuery.toString(),filterParams)

            if ( result ) {
              dataToRender.iTotalDisplayRecords = result[0]
            }

//            rows = domain.findAll(query.toString(),
//                    filterParams,
//                    [max: params.iDisplayLength as int, offset: params.iDisplayStart as int])

            rows = domain.findAll(query.toString(),
                    filterParams,
                    defaultQueryParams)
        } else {
//            rows = domain.findAll(query.toString(),
//            [max: params.iDisplayLength as int, offset: params.iDisplayStart as int])
            rows = domain.findAll(query.toString(),filterParams,defaultQueryParams)

            result=domain.executeQuery(countQuery.toString(),filterParams)

            if ( result ) {
                dataToRender.iTotalDisplayRecords = result[0]
            }
        }

//			rows?.each { row ->
//				def rowData = [:]
//				propertiesToRender.each { prop ->
//					rowData."$prop" = row."$prop"
//				}
//			   dataToRender.aaData << rowData
//			}
        dataToRender.rows = rows

        return dataToRender
	}

    def massDelete(Class domain, def params){
        log.info(params.ids)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << domain.get(it)  }

        oList*.discard() //detach all the objects from session
        oList.each{ it.delete() }
    }

    def massDeleteStaDel(Class domain, def params){
        log.info(params.ids)


        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << domain.get(it)  }

        //oList*.discard() //detach all the objects from session
        oList.each{
            it?.staDel = "1";
            it?.lastUpdProcess = "DELETE via Mass Delete"
            it?.lastUpdated = syncTime()
            it.save(flush: true)
        }
    }

    def massDeleteStaDelNew(Class domain, def params){
        log.info(params.ids)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << domain.get(it)  }

        //oList*.discard() //detach all the objects from session
        oList.each{
            it?.staDel = "1";
            it?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            it?.lastUpdProcess = "DELETE"
            it?.lastUpdated = syncTime()
            it.save(flush:true)
        }
    }

    Date syncTime(){
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        Date skrg = new Date()
        def timezonePC = TimeZone.getDefault();
        TimeZone timeZone1 = TimeZone.getTimeZone(timezonePC.ID);
        String timZeoneJKT = "Asia/Jakarta";
        TimeZone timeZone2 = TimeZone.getTimeZone(timZeoneJKT);
        int selisih = 0
        if(timeZone1.ID!=timeZone2.ID){
            selisih = compare(timeZone1,timeZone2)
        }
        if(session?.selisihWaktu && session?.selisihWaktu?.toInteger()>0){
            use(TimeCategory){
                skrg = skrg + session?.selisihWaktu?.hour - selisih.hour
            }
        }
        return skrg
    }

    public int compare(TimeZone tz1, TimeZone tz2){
        int difference = (tz1.getOffset(Calendar.getInstance().getTimeInMillis()) - tz2.getOffset(Calendar.getInstance().getTimeInMillis()))/ 3600000;
        return difference;
    }

}

			
	