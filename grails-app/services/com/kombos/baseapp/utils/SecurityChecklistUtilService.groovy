package com.kombos.baseapp.utils

import com.kombos.baseapp.SecurityChecklist
import com.kombos.baseapp.sec.shiro.User
import edu.vt.middleware.password.*

import java.text.SimpleDateFormat

class SecurityChecklistUtilService {
    SecurityChecklist sc

    SecurityChecklistUtilService(code){
        sc = SecurityChecklist.findByCode(code)
    }

    def getValueInteger(){
        return (!sc.value?sc.defaultValue:sc.value).toInteger()
    }

    def getValueString(){
        return (!sc.value?sc.defaultValue:sc.value)
    }

    static def CODE_MINIMUM_PASSWORD_LENGTH = 'minimum_password_length'
    def validateMinimumPasswordLength(password){
        def minimumPasswordLength = getMinimumPasswordLength()
        def inputPasswordLength = password.toString().length()

        if (sc.enable){
            if (inputPasswordLength < minimumPasswordLength){
                return false
            }else{
                return true
            }
        }else{
            return true
        }
    }

    def getMinimumPasswordLength(){
        return getValueInteger()
    }

    static def CODE_PASSWORD_HISTORY = 'password_history'
    def validatePasswordHistory(username, password){
        User user = User.findByUsername(username)
        boolean result = false
        if(user){
            def passwordHistory = user.getPasswordHistory()
            passwordHistory.each {
                if(it.passwordHash.equals(password)){
                    result = true
                    return
                }
            }
        }
        return result
    }

    def enablePasswordHistory(){
//        return sc.enable
        return sc.value?true:false
    }

    def countPasswordHistory(username){
        User user = User.findByUsername(username)
        if(user){
            return User.findByUsername(username).getPasswordHistory().size()
        }else{
            return 0
        }
    }

    def getPasswordHistory(){
        return getValueInteger()
    }

    static def CODE_PASSWORD_CYCLES = 'password_cycle'
    def enablePasswordCycles(){
        return sc.enable
    }

    def getPasswordCycles(){
        return getValueInteger()
    }

    def getIntervalPasswordExpiry(username){
        User user = User.findByUsername(username)
        long interval = (!user.getPasswordExpiredOn()?new Date().getTime():((Date)user.getPasswordExpiredOn()).getTime()) - new Date().getTime()
        return Math.ceil(interval / (1000 * 60 * 60 * 24)).intValue()
    }

    static def CODE_PREVENTING_GUESSABLE_PASSWORD = "preventing_guessable_password"
    static def CODE_NO_WHITESPACE = "no_whitespace"
    static def CODE_NO_ALPHABETICAL_SEQUENCE = "no_alphabetical_sequence"
    static def CODE_NO_QWERTY_SEQUENCE = "no_qwerty_sequence"
    static def CODE_NUMERICAL_SEQUENCE_RULE = "numerical_sequence_rule"
    static def CODE_REPEAT_CHARACTER_RULE = "repeat_character_rule"
    static def CODE_DIGIT_CHARACTER_RULE = "digit_character_rule"
    static def CODE_NON_ALPHANUMERIC_CHARACTER_RULE = "non_alphanumeric_character_rule"
    static def CODE_UPPER_CASE_CHARACTER_RULE = "upper_case_character_rule"
    static def CODE_LOWER_CASE_CHARACTER_RULE = "lower_case_character_rule"
    def enablePreventingGuessablePassword(){
        return sc.enable
    }

    def validatePreventingGuessablePassword(password){
        HashMap mapResult = new HashMap()

        if(sc.enable){
            SecurityChecklist scChildren
            Set scGuessablePasswordDetails = sc.children

            // group all rules together in a List
            List<Rule> ruleList = new ArrayList<Rule>()

            if(((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_NO_WHITESPACE)).enable){
                // don't allow whitespace
                ruleList.add(new WhitespaceRule())
            }

            if(((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_NO_ALPHABETICAL_SEQUENCE)).enable){
                // don't allow alphabetical sequences
                ruleList.add(new AlphabeticalSequenceRule())
            }

            if(((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_NO_QWERTY_SEQUENCE)).enable){
                // don't allow qwerty sequences
                ruleList.add(new QwertySequenceRule())
            }

            scChildren = ((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_NUMERICAL_SEQUENCE_RULE))
            if(scChildren.value){
                // don't allow numerical sequences of length X
                ruleList.add(new NumericalSequenceRule((!scChildren.value?scChildren.defaultValue:scChildren.value).toInteger(),true))
            }

            scChildren = ((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_REPEAT_CHARACTER_RULE))
            if(scChildren.value){
                // don't allow X repeat characters
                ruleList.add(new RepeatCharacterRegexRule((!scChildren.value?scChildren.defaultValue:scChildren.value).toInteger()))
            }

            CharacterCharacteristicsRule charRule = new CharacterCharacteristicsRule()
            int numberOfCharacteristic = 0
            scChildren = ((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_DIGIT_CHARACTER_RULE))
            if(scChildren.value){
                // require at least X digit in passwords
                charRule.getRules().add(new DigitCharacterRule((!scChildren.value?scChildren.defaultValue:scChildren.value).toInteger()))
                numberOfCharacteristic++
            }

            scChildren = ((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_NON_ALPHANUMERIC_CHARACTER_RULE))
            if(scChildren.value){
                // require at least X non-alphanumeric char
                charRule.getRules().add(new NonAlphanumericCharacterRule((!scChildren.value?scChildren.defaultValue:scChildren.value).toInteger()))
                numberOfCharacteristic++
            }

            scChildren = ((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_UPPER_CASE_CHARACTER_RULE))
            if(scChildren.value){
                // require at least X upper case char
                charRule.getRules().add(new UppercaseCharacterRule((!scChildren.value?scChildren.defaultValue:scChildren.value).toInteger()))
                numberOfCharacteristic++
            }

            scChildren = ((SecurityChecklist)getGuessablePasswordCodeValue(scGuessablePasswordDetails, CODE_LOWER_CASE_CHARACTER_RULE))
            if(scChildren.value){
                // require at least X lower case char
                charRule.getRules().add(new LowercaseCharacterRule((!scChildren.value?scChildren.defaultValue:scChildren.value).toInteger()))
                numberOfCharacteristic++
            }

            if(numberOfCharacteristic > 0){
                charRule.setNumberOfCharacteristics(numberOfCharacteristic)
                // control allowed characters
                ruleList.add(charRule)
            }

            PasswordValidator validator = new PasswordValidator(ruleList)
            PasswordData passwordData = new PasswordData(new Password(password))
            RuleResult result = validator.validate(passwordData)

            mapResult.put("isValid",result.isValid())
            mapResult.put("message",validator.getMessages(result))
            return mapResult
        }else{
            mapResult.put("isValid",true)
            mapResult.put("message","")
            return mapResult
        }
    }

    def getGuessablePasswordCodeValue(Set<SecurityChecklist> set, String code){
        def result = null
        try{
            set?.each {
                if(it.code.equals(code)){
                    result = it
                    throw new Exception("return from closure")
                }
            }
        }catch(Exception e){
        }
        return result
    }

    static def CODE_DISABLE_ACCOUNT_AFTER_FAILED_LOGIN = "disable_account_after_failed_login"
    def enableDisableAccountAfterFailedLogin(){
        return sc.value?true:false
    }

    def getDisableAccountAfterFailedLogin(){
        return this.getValueInteger()
    }

    static def CODE_RELOGIN_AFTER_CHANGE_PASSWORD = "relogin_after_change_password"
    def enableReloginAfterChangePassword(){
        return sc.enable
    }

    static def CODE_PREVIOUS_SUCCESSFUL_LOG_ON = "previous_successful_log_on"
    def enablePreviousSuccessfulLogOn(){
        return sc.enable
    }

    static def CODE_PREVIOUS_UNSUCCESSFUL_LOG_ON = "previous_unsuccessful_log_on"
    def enablePreviousUnsuccessfulLogOn(){
        return sc.enable
    }

    static def CODE_AUTOMATIC_LOG_OUT = "automatic_log_out"
    def enableAutomaticLogOut(){
        return sc.enable
    }

    def getAutomaticLogOut(){
        return this.getValueInteger()
    }

    static def CODE_AUTOMATIC_DISABLE_USER_PROFILE_AFTER_INACTIVE = "automatic_disable_user_profile_after_inactive"
    def enableAutomaticDisableUserProfileAfterInactive(){
        return sc.enable
    }

    def getAutomaticDisableUserProfileAfterInactive(){
        return this.getValueInteger()
    }

    def validateInactiveDaysAllowed(String username){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy HH:mm:ss")
        boolean result = true
        if(sc.value){
            def daysInactiveAllowed = this.getAutomaticDisableUserProfileAfterInactive()
            User user = User.findByUsername(username)
            if(user){
                if(user.lastLoggedIn){ //check if first log in
                    Date lastLoggedInAdded
                    Calendar c = Calendar.getInstance()
                    c.setTime(user.lastLoggedIn)
                    c.add(Calendar.DATE, daysInactiveAllowed)
                    lastLoggedInAdded = c.getTime()
                    if(lastLoggedInAdded <= new Date()){
//                        user.accountLocked = true
//                        user.save(flush: true)
                        result = false
                    }
                }
            }
        }
        return result
    }
}
