package com.kombos.baseapp

class SecurityChecklistService {

    def gridUtilService
	
	def auditTrailLogUtilService

    def getList(def params) {
        def ret = null

        try {
            def res = gridUtilService.getDomainList(SecurityChecklist, params)
            def rows = []
            def counter = 0
            ((List<SecurityChecklist>) (res?.rows))?.each {
                rows << [
                        id: counter++,
                        cell: [

                                it.approvalStatus,
                                it.category,
                                it.code,
                                it.defaultValue,
                                it.enable,
                                it.label,
                                it.type,
                                it.value
                        ]
                ]
            }

            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]

        } catch (e) {
            e.printStackTrace()
            throw e
        }

        return ret
    }

    def add(params) throws Exception {
        def securityChecklist = new SecurityChecklist()

        securityChecklist.approvalStatus = params.approvalStatus
        securityChecklist.category = params.category
        securityChecklist.code = params.code
        securityChecklist.defaultValue = params.defaultValue
        securityChecklist.enable = params.enable
        securityChecklist.label = params.label
        securityChecklist.type = params.type
        securityChecklist.value = params.value

        securityChecklist.save()
        if (securityChecklist.hasErrors()) {
            throw new Exception("${securityChecklist.errors}")
        }
    }

    def edit(params) {
        def securityChecklist = SecurityChecklist.findByCode(params.code)
        if (securityChecklist) {

            securityChecklist.approvalStatus = params.approvalStatus
            securityChecklist.category = params.category
            securityChecklist.code = params.code
            securityChecklist.defaultValue = params.defaultValue
            securityChecklist.enable = params.enable
            securityChecklist.label = params.label
            securityChecklist.type = params.type
            securityChecklist.value = params.value

            securityChecklist.save()
            if (securityChecklist.hasErrors()) {
                throw new Exception("${securityChecklist.errors}")
            }
        } else {
            throw new Exception("SecurityChecklist not found")
        }
    }

    def delete(params) {
        def securityChecklist = SecurityChecklist.findByCode(params.code)
        if (securityChecklist) {
            securityChecklist.delete()
        } else {
            throw new Exception("SecurityChecklist not found")
        }
    }

    def editBatch(HashMap parameters, def params, def request){
		
        SecurityChecklist securityChecklist
        Map.Entry mapEntry
        Iterator iterator = parameters.entrySet().iterator();
        while (iterator.hasNext()) {
			def oldString = new StringBuilder()
			def newString = new StringBuilder()
            mapEntry = (Map.Entry) iterator.next()
            securityChecklist = SecurityChecklist.findByCode(mapEntry.getKey())
			
            if(securityChecklist.type.equals(SecurityChecklist.TYPE[3])){
				def oldValue = securityChecklist.enable
                securityChecklist.enable = (mapEntry.value.toString().equalsIgnoreCase("true"))?true:false				
				def newValue = securityChecklist.enable
				if(oldValue != newValue && !(oldValue == null && newValue == '')){
					oldString.append(securityChecklist.code).append('=').append(oldValue)
					newString.append(securityChecklist.code).append('=').append(newValue)
					auditTrailLogUtilService.saveToAuditTrail(
						AuditTrailLog.UPDATE
						,securityChecklist.code
						,oldString.toString()
						,newString.toString()
						,auditTrailLogUtilService.setParamRequest(params, request, 'MENU_APP_SETTING')
					)
				}
            } else{
				def oldValue = securityChecklist.value
                securityChecklist.value = mapEntry.value
				def newValue = mapEntry.value
				if(oldValue != newValue && !(oldValue == null && newValue == '')){
					oldString.append(securityChecklist.code).append('=').append(oldValue)
					newString.append(securityChecklist.code).append('=').append(newValue)
					auditTrailLogUtilService.saveToAuditTrail(
						AuditTrailLog.UPDATE
						,securityChecklist.code
						,oldString.toString()
						,newString.toString()
						,auditTrailLogUtilService.setParamRequest(params, request, 'MENU_APP_SETTING')
					)
				}
            }
			
			
			
            securityChecklist.save(flush: true)
        }
		
    }
}