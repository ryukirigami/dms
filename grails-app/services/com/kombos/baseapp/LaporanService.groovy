package com.kombos.baseapp


import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.*

/**
 * Created with IntelliJ IDEA.
 * User: fauzi
 * Date: 18/02/13
 * Time: 22:18
 * To change this template use File | Settings | File Templates.
 */
class LaporanService {

    public File createReport(def headers, def list, def urutan) {
        def listUrutan = new ArrayList()

        WorkbookSettings workbookSettings = new WorkbookSettings()
        workbookSettings.locale = Locale.default

        def file = File.createTempFile('myExcelDocument', '.xls')
        file.deleteOnExit()

        WritableWorkbook workbook = Workbook.createWorkbook(file, workbookSettings)

        WritableFont font = new WritableFont(WritableFont.ARIAL, 12)
        WritableCellFormat format = new WritableCellFormat(font)

        def row = 0
        WritableSheet sheet = workbook.createSheet('MySheet', 0)

        def col = 0
        if (urutan != null) {
            headers.each {
                col = 0
                for (def urut : urutan) {
                    if (it.toString().toLowerCase().equals(urut.toString().toLowerCase())) {
                        listUrutan.add(col)
                    }
                    col++
                }

            }
        }
        col = 0
        headers.each {
            if (listUrutan.size() > 0)
                sheet.addCell(new Label(listUrutan.get(col++), row, it, format))
            else
                sheet.addCell(new Label(col++, row, it, format))
        }
        row++
        list.each {
            col = 0
            for (def data : it) {
                if (listUrutan.size() > 0)
                    sheet.addCell(new Label(listUrutan.get(col++), row, data ? data.toString() : "", format))
                else
                    sheet.addCell(new Label(col++, row, data ? data.toString() : "", format))
            }
            row++
        }
        workbook.write()
        workbook.close()
        return file
    }

    public File createReportTanpaUrutan(def headers, def list) {
        WorkbookSettings workbookSettings = new WorkbookSettings()
        workbookSettings.locale = Locale.default

        def file = File.createTempFile('myExcelDocument', '.xls')
        file.deleteOnExit()

        WritableWorkbook workbook = Workbook.createWorkbook(file, workbookSettings)

        WritableFont font = new WritableFont(WritableFont.ARIAL, 12)
        WritableCellFormat format = new WritableCellFormat(font)

        def row = 0
        WritableSheet sheet = workbook.createSheet('MySheet', 0)

        def col = 0
        headers.each {
            sheet.addCell(new Label(col++, row, it, format))
        }
        row++
        list.each {
            col = 0
            for (def data : it) {
                sheet.addCell(new Label(col++, row, data ? data.toString() : "", format))
            }
            row++
        }
        workbook.write()
        workbook.close()
        return file
    }

}
