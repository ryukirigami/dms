package com.kombos.baseapp.menu

import com.kombos.baseapp.ActionType
import com.kombos.baseapp.StockOpnameLog
import com.kombos.baseapp.sec.shiro.Permission
import com.kombos.baseapp.sec.shiro.User
import org.apache.shiro.SecurityUtils
import org.apache.shiro.mgt.DefaultSecurityManager
import org.apache.shiro.subject.SimplePrincipalCollection
import org.apache.shiro.subject.Subject
import org.springframework.web.context.request.RequestContextHolder

class MenuService {

    def shiroSecurityManager

    def loadAdminMenu() {
        def ret = []

        def user = User.findByUsername(org?.apache?.shiro?.SecurityUtils?.subject?.principal?.toString())
        if(user){
            user.roles.each { data->
                def c = RoleMenu.createCriteria()
                def results = c.list {
                    role{
                        eq("id",data.id)
                    }
                    menu{
                        parent{
                            eq('menuCode', 'MENU_ADMIN')
                        }
                    }
                }
                results.each {
                    if(ret?.indexOf(it.menu)<0){
                        ret << it.menu
                    }
                }
            }
        }

        return ret
    }


    private boolean childrenHasPermission(Menu menu) {
        boolean ret = false
        if (menu.children) {
            menu.children.each() {
                ret |= childrenHasPermission(it)
            }
        } else {

            Permission p = Permission.findByMenuAndActionTypeAndLinkController(menu, ActionType.VIEW, menu.linkController)
            if (p == null) {
                ret = SecurityUtils.subject.isPermitted(menu.linkController + ":" + menu.linkAction)
            } else {
                ret = SecurityUtils.subject.isPermitted(p.linkController + ":" + p.actionString)
            }
        }
        return ret
    }

    def loadMenu() {
        return Menu.findAll("from Menu where parent is null and menuCode <> :menuCode order by seq asc", [menuCode: 'MENU_ADMIN'])
    }

    def loadPermittedMenu() {
        def userMenu = []
        def roleMenu = []
            def status = "1"
            def session = RequestContextHolder.currentRequestAttributes().getSession()
            def stokOpnameStatus = StockOpnameLog.findByCompanyDealerAndStaDel(session.userCompanyDealer,'0')
            if(stokOpnameStatus){
                if(stokOpnameStatus.last())
                    status = stokOpnameStatus?.staStockOpname
            }

            def user = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).roles.each { data->
                def c = RoleMenu.createCriteria()
                def results = c.list {
                    role{
                        eq("id",data.id)
                    }
                    if (status=="0"){
                        menu {
                            ne('menuCode', 'MENU_ADMIN')
                            eq('stockOpnameEnable','0')
                            isNull('parent')
                            order('seq', 'asc')
                        }
                    }else{
                        menu {
                            ne('menuCode', 'MENU_ADMIN')
                            isNull('parent')
                            order('seq', 'asc')
                        }
                    }

                }
                results.each {
                    if(roleMenu?.indexOf(it.menu)<0){
                        roleMenu << it.menu
                    }
                }

                c = RoleMenu.createCriteria()
                results = c.list() {
                    role{
                        eq("id",data.id)
                    }

                    if (status=="0"){
                        menu {
                            eq('stockOpnameEnable','0')
                            ne('menuCode', 'MENU_ADMIN')
                            isNotNull('parent')
                            order('parent', 'asc')
                            order('seq', 'asc')
                        }
                    }else{
                        menu {
                            ne('menuCode', 'MENU_ADMIN')
                            isNotNull('parent')
                            order('parent', 'asc')
                            order('seq', 'asc')
                        }
                    }

                }

                results.each {
                    if(roleMenu?.indexOf(it.menu)<0){
                        roleMenu << it.menu
                    }
                }
            }
            roleMenu
    }

    def childrenHasPermission2(Subject subject, Menu menu) {

        boolean ret = false
        if (menu.children) {
            for (item in menu.children) {
                def c = childrenHasPermission2(subject, item)
                if (c) {
                    ret = true
                    break
                }
            }
        } else {
            ret = subject.isPermitted(menu.linkController + ":" + menu.linkAction)
        }
        return ret
    }

    def updateUserMenu(User user) {
        def realm = shiroSecurityManager.realms.find { it }
        def bootstrapSecurityManager = new DefaultSecurityManager(realm)
        def principals = new SimplePrincipalCollection(user.username, realm.name)
        def subject = new Subject.Builder(bootstrapSecurityManager).principals(principals).buildSubject()
        def roles = user.roles
        def oList = UserMenu.findByUser(user)

        oList*.discard() //detach all the objects from session
        oList.each { it.delete() }

        roles.each {
            def menus = RoleMenu.findAllByRole(it)
            menus.each {
                def menu = it.menu
                if (childrenHasPermission2(subject, menu)) {
                    UserMenu um = new UserMenu(user: user, menu: menu)
                    um.save()
                }
            }
        }
    }

    def loadUserMenu(User user) {
        def userMenu = []
        UserMenu.findAllByUser(user).each { UserMenu um ->
            userMenu << um.menu
        }

        userMenu
    }

    def userHasMenu(Menu menu) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        def userMenu = session.userMenu
        for (m in userMenu) {
            if (m.menuCode == menu.menuCode) {
                return true
            }
        }
        return false
    }

    def bootstrap() {
        bootstrapAdminMenu()
        bootstrapLatihan()

        bootstrapAdministrationMenu()

        bootstrapPartMenu()

        bootstrapCustomerProfileMenu()

        bootstrapCustomerComplaintMenu()

        bootstrapBoardMenu()
        bootsrapProductionMenu()
        bootstrapCustomerFollowUpMenu()

        bootstrapInterface()

        def customerComplaint = Menu.findByMenuCode('CUSTOMER_COMPLAINT') ?: new Menu(menuCode: 'CUSTOMER_COMPLAINT', label: 'Customer Complaint', details: 'Customer Complaint', seq: 5)
        customerComplaint.save()

        def mrs = Menu.findByMenuCode('MRS') ?: new Menu(menuCode: 'MRS', label: 'MRS', details: 'MRS', seq: 6).save()
        def mrsGeneralParameter = Menu.findByMenuCode('MRS_GENERAL_PARAMETER') ?: new Menu(menuCode: 'MRS_GENERAL_PARAMETER', label: 'General Parameter', details: 'General Parameter', parent: mrs, linkController: 'mrsGeneralParameter', targetMode: Menu._LOAD_, seq: 1).save()
        def mrsRetention = Menu.findByMenuCode('MRS_RETENTATION') ?: new Menu(menuCode: 'MRS_RETENTATION', label: 'Template SMS Retention', details: 'Template SMS Retention', parent: mrs, linkController: 'retention', targetMode: Menu._LOAD_, seq: 2).save()
        def mrsReminderGenerator = Menu.findByMenuCode('MRS_REMINDER_GENERATOR') ?: new Menu(menuCode: 'MRS_REMINDER_GENERATOR', label: 'Retrieve Data Customer Pasif', details: 'Retrieve Data Customer Pasif', parent: mrs, linkController: 'retrieveDataCustomerPasif', targetMode: Menu._LOAD_, seq: 3).save()
        def mrsReminder = Menu.findByMenuCode('MRS_REMINDER') ?: new Menu(menuCode: 'MRS_REMINDER', label: 'Reminder List', details: 'Reminder List', parent: mrs, linkController: 'reminder', targetMode: Menu._LOAD_, seq: 4).save()
        def mrsInvitation = Menu.findByMenuCode('MRS_INVITATION') ?: new Menu(menuCode: 'MRS_INVITATION', label: 'Invitation', details: 'Invitation', parent: mrs, linkController: 'invitation', targetMode: Menu._LOAD_, seq: 5).save()
        def sendSMSRetention = Menu.findByMenuCode('MRS_SendSMSRetention') ?: new Menu(menuCode: 'MRS_SendSMSRetention', label: 'Send SMS Retention', details: 'Send SMS Retention', parent: mrs, linkController: 'sendSMSRetention', targetMode: Menu._LOAD_, seq: 6).save()
        def customerFeedback = Menu.findByMenuCode('MRS_CustomerFeedback') ?: new Menu(menuCode: 'MRS_CustomerFeedback', label: 'Customer Feedback', details: 'Customer Feedback', parent: mrs, linkController: 'customerFeedback', targetMode: Menu._LOAD_, seq: 7).save()


        mrs.addToChildren(mrsGeneralParameter)
        mrs.addToChildren(mrsRetention)
        mrs.addToChildren(mrsReminderGenerator)
        mrs.addToChildren(mrsReminder)
        mrs.addToChildren(mrsInvitation)
        mrs.addToChildren(sendSMSRetention)
        mrs.addToChildren(customerFeedback)
        mrs.save()

        def appointment = Menu.findByMenuCode('APPOINTMENT') ?: new Menu(menuCode: 'APPOINTMENT', label: 'Appointment', details: 'Appointment', seq: 7).save()
		def appointmentAppointment = Menu.findByMenuCode('APPOINTMENT_APPOINTMENT') ?: new Menu(menuCode: 'APPOINTMENT_APPOINTMENT', label: 'Appointment', details: 'Appointment', parent: appointment, linkController: 'appointment', targetMode: Menu._LOAD_, seq: 1).save()
		def appointmentCustomer = Menu.findByMenuCode('APPOINTMENT_CUSTOMER') ?: new Menu(menuCode: 'APPOINTMENT_CUSTOMER', label: 'Customer', details: 'Customer', parent: appointment, linkController: 'appointmentCustomer', targetMode: Menu._LOAD_, seq: 2).save()
        
		def belumBayarBookingFee = Menu.findByMenuCode('APPOINTMENT_BELUMBAYARBOOKINGFEE') ?: new Menu(menuCode: 'APPOINTMENT_BELUMBAYARBOOKINGFEE', label: 'View Customer Appointment Belum Bayar Booking Fee', details: 'View Customer Appointment Belum Bayar Booking Fee', parent: appointment, linkController: 'viewAppointment', linkAction: 'belumBayarBookingFee', targetMode: Menu._LOAD_, seq: 3).save()
		def sudahBayarBookingFeeBelumOrderParts = Menu.findByMenuCode('APPOINTMENT_SUDAHBAYARBOOKINGFEEBELUMORDERPARTS') ?: new Menu(menuCode: 'APPOINTMENT_SUDAHBAYARBOOKINGFEEBELUMORDERPARTS', label: 'View Customer Appointment Sudah Bayar Booking Fee Belum Order Parts', details: 'View Customer Appointment Sudah Bayar Booking Fee Belum Order Parts', parent: appointment, linkController: 'viewAppointment', linkAction: 'sudahBayarBookingFeeBelumOrderParts', targetMode: Menu._LOAD_, seq: 4).save()
		def tungguETAParts = Menu.findByMenuCode('APPOINTMENT_TUNGGUETAPARTS') ?: new Menu(menuCode: 'APPOINTMENT_TUNGGUETAPARTS', label: 'View Customer Appointment Tunggu ETA Parts', details: 'View Customer Appointment Tunggu ETA Parts', parent: appointment, linkController: 'viewAppointment', linkAction: 'tungguETAParts', targetMode: Menu._LOAD_, seq: 5).save()
		def registrasiAccountWeb = Menu.findByMenuCode('APPOINTMENT_REGISTRASIACCOUNTWEB') ?: new Menu(menuCode: 'APPOINTMENT_REGISTRASIACCOUNTWEB', label: 'View Customer Registrasi Account Web', details: 'View Customer Registrasi Account Web', parent: appointment, linkController: 'viewAppointment', linkAction: 'registrasiAccountWeb', targetMode: Menu._LOAD_, seq: 6).save()
		def viaWebBelumPlotASBJPB = Menu.findByMenuCode('APPOINTMENT_VIAWEBBELUMPLOTASBJPB') ?: new Menu(menuCode: 'APPOINTMENT_VIAWEBBELUMPLOTASBJPB', label: 'View Customer Appointment Via Web Belum Plot ASB/JPB', details: 'View Customer Appointment Via Web Belum Plot ASB/JPB', parent: appointment, linkController: 'viewAppointment', linkAction: 'viaWebBelumPlotASBJPB', targetMode: Menu._LOAD_, seq: 7).save()
		def sudahBayarBookingFeeBelumPlotJPB = Menu.findByMenuCode('APPOINTMENT_SUDAHBAYARBOOKINGFEEBELUMPLOTJPB') ?: new Menu(menuCode: 'APPOINTMENT_SUDAHBAYARBOOKINGFEEBELUMPLOTJPB', label: 'View Customer Appointment Sudah Bayar Booking Fee Belum Plot JPB', details: 'View Customer Appointment Sudah Bayar Booking Fee Belum Plot JPB', parent: appointment, linkController: 'viewAppointment', linkAction: 'sudahBayarBookingFeeBelumPlotJPB', targetMode: Menu._LOAD_, seq: 8).save()
		def menungguKelengkapanDokumenAsuransi = Menu.findByMenuCode('APPOINTMENT_MENUNGGUKELENGKAPANDOKUMENASURANSI') ?: new Menu(menuCode: 'APPOINTMENT_MENUNGGUKELENGKAPANDOKUMENASURANSI', label: 'View Customer Appointment Menunggu Kelengkapan Dokumen Asuransi', details: 'View Customer Appointment Menunggu Kelengkapan Dokumen Asuransi', parent: appointment, linkController: 'viewAppointment', linkAction: 'menungguKelengkapanDokumenAsuransi', targetMode: Menu._LOAD_, seq: 9).save()
		def menungguSurvey = Menu.findByMenuCode('APPOINTMENT_MENUNGGUSURVEY') ?: new Menu(menuCode: 'APPOINTMENT_MENUNGGUSURVEY', label: 'View Customer Appointment Menunggu Survey', details: 'View Customer Appointment Menunggu Survey', parent: appointment, linkController: 'viewAppointment', linkAction: 'menungguSurvey', targetMode: Menu._LOAD_, seq: 10).save()
		def menungguSPK = Menu.findByMenuCode('APPOINTMENT_MENUNGGUSPK') ?: new Menu(menuCode: 'APPOINTMENT_MENUNGGUSPK', label: 'View Customer Appointment Menunggu SPK', details: 'View Customer Appointment Menunggu SPK', parent: appointment, linkController: 'viewAppointment', linkAction: 'menungguSPK', targetMode: Menu._LOAD_, seq: 11).save()
		def followUpAppointment = Menu.findByMenuCode('APPOINTMENT_FOLLOWUPAPPOINTMENT') ?: new Menu(menuCode: 'APPOINTMENT_FOLLOWUPAPPOINTMENT', label: 'View Customer Follow Up Appointment', details: 'View Customer Follow Up Appointment', parent: appointment, linkController: 'viewAppointment', linkAction: 'followUpAppointment', targetMode: Menu._LOAD_, seq: 12).save()
		def perluReschedule	 = Menu.findByMenuCode('APPOINTMENT_PERLURESCHEDULE') ?: new Menu(menuCode: 'APPOINTMENT_PERLURESCHEDULE', label: 'View Customer Perlu Reschedule', details: 'View Customer Perlu Reschedule', parent: appointment, linkController: 'viewAppointment', linkAction: 'perluReschedule', targetMode: Menu._LOAD_, seq: 13).save()
		def revisiJanjiPenyerahan = Menu.findByMenuCode('APPOINTMENT_REVISIJANJIPENYERAHAN') ?: new Menu(menuCode: 'APPOINTMENT_REVISIJANJIPENYERAHAN', label: 'View Customer Revisi Janji Penyerahan', details: 'View Customer Revisi Janji Penyerahan', parent: appointment, linkController: 'viewAppointment', linkAction: 'revisiJanjiPenyerahan', targetMode: Menu._LOAD_, seq: 14).save()
		
		//appointment.addToChildren(appointmentAppointment)
		//appointment.addToChildren(appointmentCustomer)
		//appointment.save()

        def reception = Menu.findByMenuCode('RECEPTION') ?: new Menu(menuCode: 'RECEPTION', label: 'Reception', details: 'Reception', seq: 9).save()
        def customerIn = Menu.findByMenuCode('CUSTOMER_IN') ?: new Menu(menuCode: 'CUSTOMER_IN', label: 'Customer In', details: 'Customer In', parent: reception, linkController: 'customerIn', targetMode: Menu._LOAD_, seq: 901).save()
        def viewKendaraanTowing = Menu.findByMenuCode('VIEW_KENDARAAN_TOWING') ?: new Menu(menuCode: 'VIEW_KENDARAAN_TOWING', label: 'View Kendaraan Towing', details: 'View Kendaraan Towing', parent: reception, linkController: 'towingMalam', targetMode: Menu._LOAD_, seq: 902).save()
        def boardAntrianBP = Menu.findByMenuCode('BOARD_ANTRIAN_BP') ?: new Menu(menuCode: 'BOARD_ANTRIAN_BP', label: 'Board Antrian Body Paint', details: 'Board Antrian Body Paint', parent: reception, linkController: 'boardAntrianBP', targetMode: Menu._LOAD_, seq: 903).save()
        def boardAntrianGR = Menu.findByMenuCode('BOARD_ANTRIAN_GR') ?: new Menu(menuCode: 'BOARD_ANTRIAN_GR', label: 'Board Antrian General Repair', details: 'Board Antrian General Repair', parent: reception, linkController: 'boardAntrianGR', targetMode: Menu._LOAD_, seq: 903).save()
        def inputNomorLoket = Menu.findByMenuCode('INPUT_NOMOR_LOKET') ?: new Menu(menuCode: 'INPUT_NOMOR_LOKET', label: 'Input Nomor Loket', details: 'Input Nomor Loket', parent: reception, linkController: 'inputNomorLoket', targetMode: Menu._LOAD_, seq: 903).save()
        def customerOut = Menu.findByMenuCode('CUSTOMER_OUT') ?: new Menu(menuCode: 'CUSTOMER_OUT', label: 'Customer Out', details: 'Customer Out', parent: reception, linkController: 'customerOut', targetMode: Menu._LOAD_, seq: 905).save()
        def viewProgressKendaraan = Menu.findByMenuCode('VIEW_PROGRESS_KENDARAAN') ?: new Menu(menuCode: 'VIEW_PROGRESS_KENDARAAN', label: 'View Progress Kendaraan', details: 'View Progress Kendaraan', parent: reception, linkController: 'viewProgressKendaraan', targetMode: Menu._LOAD_, seq: 906).save()
        def viewKendaraanTungguSurvey = Menu.findByMenuCode('VIEW_KENDARAAN_TUNGGU_SURVEY') ?: new Menu(menuCode: 'VIEW_KENDARAAN_TUNGGU_SURVEY', label: 'View Kendaraan Tunggu Survey/PKS', details: 'View Kendaraan Tunggu Survey/PKS', parent: reception, linkController: 'viewKendaraanTungguSurvey', targetMode: Menu._LOAD_, seq: 907).save()
        def viewStatusKendaraan = Menu.findByMenuCode('VIEW_STATUS_KENDARAAN') ?: new Menu(menuCode: 'VIEW_STATUS_KENDARAAN', label: 'View Status Kendaraan', details: 'View Status Kendaraan', parent: reception, linkController: 'viewStatusKendaraan', targetMode: Menu._LOAD_, seq: 908).save()
        def viewCustomerReception = Menu.findByMenuCode('VIEW_CUSTOMER_RECEPTION') ?: new Menu(menuCode: 'VIEW_CUSTOMER_RECEPTION', label: 'View Customer Reception', details: 'View Customer Reception', parent: reception, linkController: 'reception/list', targetMode: Menu._LOAD_, seq: 909).save()
        def viewCustomerReceptionBPCenter = Menu.findByMenuCode('VIEW_CUSTOMER_RECEPTION_BP_RECEPTION') ?: new Menu(menuCode: 'VIEW_CUSTOMER_RECEPTION_BP_RECEPTION', label: 'View Customer Reception BP Center', details: 'View Customer Reception BP Center', parent: reception, linkController: 'viewReceptionBpCenter', targetMode: Menu._LOAD_, seq: 9010).save()
        def inputReception = Menu.findByMenuCode('INPUT_RECEPTION') ?: new Menu(menuCode: 'INPUT_RECEPTION', label: 'Input Reception', details: 'Input Reception', parent: reception, linkController: 'reception', targetMode: Menu._LOAD_, seq: 9011).save()
        def viewSalesQuotation = Menu.findByMenuCode('VIEW_SALES_QUOTATION') ?: new Menu(menuCode: 'VIEW_SALES_QUOTATION', label: 'View Sales Quotation', details: 'View Sales Quotation', parent: reception, linkController: 'viewSalesQuotation', targetMode: Menu._LOAD_, seq: 9013).save()
        def salesQuotation = Menu.findByMenuCode('SALES_QUOTATION') ?: new Menu(menuCode: 'SALES_QUOTATION', label: 'Sales Quotation', details: 'Sales Quotation', parent: reception, linkController: 'salesQuotation', targetMode: Menu._LOAD_, seq: 9014).save()
        def editJobParts = Menu.findByMenuCode('EDIT_JOB_foPARTS') ?: new Menu(menuCode: 'EDIT_JOB_PARTS', label: 'Edit Job And Parts', details: 'Edit Job And Parts', parent: reception, linkController: 'editJobParts', targetMode: Menu._LOAD_, seq: 9015).save()
        def viewSubletPerWo = Menu.findByMenuCode('VIEW_SUBLET_PER_WO') ?: new Menu(menuCode: 'VIEW_SUBLET_PER_WO', label: 'View Sublet Per WO', details: 'View Sublet Per WO', parent: reception, linkController: 'POSublet', targetMode: Menu._LOAD_, seq: 9016).save()
        def sendPoSublet = Menu.findByMenuCode('SEND_PO_SUBLET') ?: new Menu(menuCode: 'SEND_PO_SUBLET', label: 'Send PO Sublet', details: 'Send PO Sublet', parent: reception, linkController: 'sendPOSublet', targetMode: Menu._LOAD_, seq: 9017).save()
        def viewPoInvoiceReception = Menu.findByMenuCode('VIEW_PO_INVOICE_RECEPTION') ?: new Menu(menuCode: 'VIEW_PO_INVOICE_RECEPTION', label: 'View PO Invoice', details: 'View PO Invoice', parent: reception, linkController: 'viewPoInvoice', targetMode: Menu._LOAD_, seq: 9018).save()
        def viewInvoiceReception = Menu.findByMenuCode('VIEW_INVOICE_RECEPTION') ?: new Menu(menuCode: 'VIEW_INVOICE_RECEPTION', label: 'View Invoice', details: 'View Invoice', parent: reception, linkController: 'viewInvoice', targetMode: Menu._LOAD_, seq: 9019).save()
        def viewMaterialPerWo = Menu.findByMenuCode('VIEW_MATERIAL_PER_WO') ?: new Menu(menuCode: 'VIEW_MATERIAL_PER_WO', label: 'View Material Per WO', details: 'View Material Per WO', parent: reception, linkController: 'viewMaterialPerWo', targetMode: Menu._LOAD_, seq: 9021).save()
//        def inputMos = Menu.findByMenuCode('INPUT_MOS') ?: new Menu(menuCode: 'INPUT_MOS', label: 'Input MOS', details: 'Input MOS', parent: reception, linkController: 'inputMos', targetMode: Menu._LOAD_, seq: 9022).save()
        def viewOrderCat = Menu.findByMenuCode('VIEW_ORDER_CAT') ?: new Menu(menuCode: 'VIEW_ORDER_CAT', label: 'View Order Cat', details: 'View Order Cat', parent: reception, linkController: 'viewOrderCat', targetMode: Menu._LOAD_, seq: 9023).save()
        def boardAntrian = Menu.findByMenuCode('BOARD_ANTRIAN') ?: new Menu(menuCode: 'BOARD_ANTRIAN', label: 'Board Antrian', details: 'Board Antrian', parent: reception, linkController: 'boardAntrian', targetMode: Menu._LOAD_, seq: 9024).save()
        reception.addToChildren(customerIn)
        reception.addToChildren(viewKendaraanTowing)
        reception.addToChildren(boardAntrianBP)
        reception.addToChildren(boardAntrianGR)
        reception.addToChildren(inputNomorLoket)
        reception.addToChildren(customerOut)
        reception.addToChildren(viewProgressKendaraan)
        reception.addToChildren(viewKendaraanTungguSurvey)
        reception.addToChildren(viewStatusKendaraan)
        reception.addToChildren(viewCustomerReception)
        reception.addToChildren(viewCustomerReceptionBPCenter)
        reception.addToChildren(inputReception)
        reception.addToChildren(viewSalesQuotation)
        reception.addToChildren(salesQuotation)
        reception.addToChildren(editJobParts)
        reception.addToChildren(viewSubletPerWo)
        reception.addToChildren(sendPoSublet)
        reception.addToChildren(viewPoInvoiceReception)
        reception.addToChildren(viewInvoiceReception)
        reception.addToChildren(viewMaterialPerWo)
//        reception.addToChildren(inputMos)
        reception.addToChildren(viewOrderCat)
        reception.addToChildren(boardAntrian)
        reception.save()

        def woinformationMenu = Menu.findByMenuCode('WO_INFORMATION') ?: new Menu(menuCode: 'WO_INFORMATION', label: 'WO Information', details: 'WO Information',linkController: 'woInformation', seq: 10)
        def woinformation = Menu.findByMenuCode('WOINFORMATION') ?: new Menu(menuCode: 'WOINFORMATION', label: 'WO Information', details: 'WO Information', parent: woinformationMenu, linkController: 'woInformation', targetMode: Menu._LOAD_, seq: 1001).save()
        woinformationMenu.addToChildren(woinformation)
        woinformationMenu.save()



        def delivery = Menu.findByMenuCode('DELIVERY') ?: new Menu(menuCode: 'DELIVERY', label: 'Delivery', details: 'Delivery', seq: 12)
        delivery.save()

        def boardAntrianCuci = Menu.findByMenuCode('BoardAntrianCuci') ?: new Menu(menuCode: 'BoardAntrianCuci', label: 'Board Antrian Cuci', details: 'Board Antrian Cuci', parent: delivery, linkController: 'boardAntrianCuci', targetMode: Menu._LOAD_, seq: 1).save()
        def antrianCuci = Menu.findByMenuCode('AntrianCuci') ?: new Menu(menuCode: 'AntrianCuci', label: 'Antrian Cuci List', details: 'Antrian Cuci List', parent: delivery, linkController: 'antriCuci', targetMode: Menu._LOAD_, seq: 2).save()
        def jobOrderCompletion = Menu.findByMenuCode('JobOrderCompletion') ?: new Menu(menuCode: 'JobOrderCompletion', label: 'Job Order Completion', details: 'Job Order Completion', parent: delivery, linkController: 'jobOrderCompletion', targetMode: Menu._LOAD_, seq: 3).save()
        def explanation = Menu.findByMenuCode('Explanation') ?: new Menu(menuCode: 'Explanation', label: 'Explanation', details: 'Explanation', parent: delivery, linkController: 'explanation', targetMode: Menu._LOAD_, seq: 4).save()
        def slipList = Menu.findByMenuCode('SlipList') ?: new Menu(menuCode: 'SlipList', label: 'Slip List', details: 'Slip List', parent: delivery, linkController: 'slipList', targetMode: Menu._LOAD_, seq: 5).save()
        def invoicing = Menu.findByMenuCode('Invoicing') ?: new Menu(menuCode: 'Invoicing', label: 'Invoicing', details: 'Invoicing', parent: delivery, linkController: 'invoicingViewWO', targetMode: Menu._LOAD_, seq: 5).save()
        def refund = Menu.findByMenuCode('Refund') ?: new Menu(menuCode: 'Refund', label: 'Refund', details: 'Refund', parent: delivery, linkController: 'refund', targetMode: Menu._LOAD_, seq: 6).save()
        def settlement = Menu.findByMenuCode('Settlement') ?: new Menu(menuCode: 'Settlement', label: 'Settlement', details: 'Settlement', parent: delivery, linkController: 'settlement', targetMode: Menu._LOAD_, seq: 7).save()
        def kwitansi = Menu.findByMenuCode('Kwitansi') ?: new Menu(menuCode: 'Kwitansi', label: 'Kwitansi', details: 'Kwitansi', parent: delivery, linkController: 'kwitansi', targetMode: Menu._LOAD_, seq: 8).save()
        def sendApprovalDiscountGoodWill = Menu.findByMenuCode('SendApprovalDiscountGoodWill') ?: new Menu(menuCode: 'SendApprovalDiscountGoodWill', label: 'Send Approval Discount Good Will', details: 'Send Approval Discount Good Will', parent: delivery, linkController: 'sendApprovalDiscountGoodWill', targetMode: Menu._LOAD_, seq: 9).save()
        def responseApprovalDiscountGoodWill = Menu.findByMenuCode('ResponseApprovalDiscountGoodWill') ?: new Menu(menuCode: 'ResponseApprovalDiscountGoodWill', label: 'Response Approval Discount Good Will', details: 'Response Approval Discount Good Will', parent: delivery, linkController: 'ResponseApprovalDiscountGoodWill', targetMode: Menu._LOAD_, seq: 10).save()
        def setoranBank = Menu.findByMenuCode('SetoranBank') ?: new Menu(menuCode: 'SetoranBank', label: 'Setoran Bank', details: 'Setoran Bank', parent: delivery, linkController: 'setoranBank', targetMode: Menu._LOAD_, seq: 11).save()
        def exportFile = Menu.findByMenuCode('ExportFile') ?: new Menu(menuCode: 'ExportFile', label: 'Export File', details: 'Export File', parent: delivery, linkController: 'exportFile', targetMode: Menu._LOAD_, seq: 12).save()
        def FakturPajak = Menu.findByMenuCode('FakturPajak') ?: new Menu(menuCode: 'FakturPajak', label: 'Faktur Pajak', details: 'Faktur Pajak', parent: delivery, linkController: 'FakturPajak', targetMode: Menu._LOAD_, seq: 13).save()
        def MasternoPajak = Menu.findByMenuCode('NoPajak') ?: new Menu(menuCode: 'NoPajak', label: 'Master No Pajak', details: 'Master No Pajak', parent: delivery, linkController: 'NoPajak', targetMode: Menu._LOAD_, seq: 14).save()


        delivery.addToChildren(boardAntrianCuci)
        delivery.addToChildren(antrianCuci)
        delivery.addToChildren(jobOrderCompletion)
        delivery.addToChildren(explanation)
        delivery.addToChildren(slipList)
        delivery.addToChildren(invoicing)
        delivery.addToChildren(refund)
        delivery.addToChildren(settlement)
        delivery.addToChildren(kwitansi)
        delivery.addToChildren(sendApprovalDiscountGoodWill)
        delivery.addToChildren(responseApprovalDiscountGoodWill)
        delivery.addToChildren(setoranBank)
        delivery.addToChildren(exportFile)
        delivery.addToChildren(FakturPajak)
        delivery.addToChildren(MasternoPajak)
        delivery.save()

        def gatePass = Menu.findByMenuCode('GATE_PASS') ?: new Menu(menuCode: 'GATE_PASS', label: 'Gate Pass', details: 'Gate Pass',   seq: 13)

        def gatePass2 = Menu.findByMenuCode('GATE_PASS2') ?: new Menu(menuCode: 'GATE_PASS2', label: 'Gate Pass', details: 'gate Pass', parent: gatePass, linkController: 'gatePassT800', targetMode: Menu._LOAD_, seq: 1).save()
        gatePass.addToChildren(gatePass2)
//        def statistik = Menu.findByMenuCode('GATE_PASS3') ?: new Menu(menuCode: 'GATE_PASS3', label: 'View Statistic Gate Pass', details: 'View Statistic Gate Pass', parent: gatePass, linkController: 'gatePassGraphic', targetMode: Menu._LOAD_, seq: 2).save()
//        gatePass.addToChildren(statistik)
        gatePass.save()
        def kriteriaReports = Menu.findByMenuCode('KRITERIA_REPORTS') ?: new Menu(menuCode: 'KRITERIA_REPORTS', label: 'Kriteria Reports', details: 'Kriteria Reports', seq: 15)
        kriteriaReports.save()
        def KR_mrs = Menu.findByMenuCode('KR_MRS') ?: new Menu(menuCode: 'KR_MRS', label: 'MRS', details: 'MRS', parent: kriteriaReports, linkController: 'krMrs', targetMode: Menu._LOAD_, seq: 1).save()
        def KR_appointmentGR = Menu.findByMenuCode('KR_appointmentGR') ?: new Menu(menuCode: 'KR_appointmentGR', label: 'Appointment GR', details: 'Appointment GR', parent: kriteriaReports, linkController: 'kr_appointment_gr', targetMode: Menu._LOAD_, seq: 2).save()
        def KR_appointmentBP = Menu.findByMenuCode('KR_appointmentBP') ?: new Menu(menuCode: 'KR_appointmentBP', label: 'Appointment BP', details: 'Appointment BP', parent: kriteriaReports, linkController: 'kr_appointment_bp', targetMode: Menu._LOAD_, seq: 3).save()
        def KR_receptionGR = Menu.findByMenuCode('KR_receptionGR') ?: new Menu(menuCode: 'KR_receptionGR', label: 'Reception GR', details: 'Reception GR', parent: kriteriaReports, linkController: 'kr_reception_gr', targetMode: Menu._LOAD_, seq: 3).save()
        def KR_receptionBP = Menu.findByMenuCode('KR_receptionBP') ?: new Menu(menuCode: 'KR_receptionBP', label: 'Reception BP', details: 'Reception BP', parent: kriteriaReports, linkController: 'kr_reception_bp', targetMode: Menu._LOAD_, seq: 3).save()
        def KR_receptionBP_Center = Menu.findByMenuCode('KR_receptionBP_Center') ?: new Menu(menuCode: 'KR_receptionBP_Center', label: 'Reception BP Center', details: 'Reception BP Center', parent: kriteriaReports, linkController: 'kr_reception_bp_center', targetMode: Menu._LOAD_, seq: 3).save()
        def KR_productionGR = Menu.findByMenuCode('KR_productionGR') ?: new Menu(menuCode: 'KR_productionGR', label: 'Production GR', details: 'Production GR', parent: kriteriaReports, linkController: 'production_gr', targetMode: Menu._LOAD_, seq: 20).save()
        def KR_productionBP = Menu.findByMenuCode('KR_productionBP') ?: new Menu(menuCode: 'KR_productionBP', label: 'Production BP', details: 'Production BP', parent: kriteriaReports, linkController: 'production_bp', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_deliveryGR = Menu.findByMenuCode('KR_deliveryGR') ?: new Menu(menuCode: 'KR_deliveryGR', label: 'Delivery GR', details: 'Delivery GR', parent: kriteriaReports, linkController: 'kr_delivery_gr', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_deliveryBP = Menu.findByMenuCode('KR_deliveryBP') ?: new Menu(menuCode: 'KR_deliveryBP', label: 'Delivery BP', details: 'Delivery BP', parent: kriteriaReports, linkController: 'kr_delivery_bp', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_deliveryBP_Center = Menu.findByMenuCode('KR_deliveryBP_Center') ?: new Menu(menuCode: 'KR_deliveryBP_Center', label: 'Delivery BP Center', details: 'Delivery BP Center', parent: kriteriaReports, linkController: 'kr_delivery_bp_center', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_deliveryFinance = Menu.findByMenuCode('KR_deliveryFinance') ?: new Menu(menuCode: 'KR_deliveryFinance', label: 'Delivery Finance', details: 'Delivery Finance', parent: kriteriaReports, linkController: 'kr_delivery_finance', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_Customer_Followup_GR = Menu.findByMenuCode('KR_Customer_Followup_GR') ?: new Menu(menuCode: 'KR_Customer_Followup_GR', label: 'Customer followUp GR', details: 'Customer followUp GR', parent: kriteriaReports, linkController: 'kr_customer_followup_gr', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_Customer_Followup_BP = Menu.findByMenuCode('KR_Customer_Followup_BP') ?: new Menu(menuCode: 'KR_Customer_Followup_BP', label: 'Customer followUp BP', details: 'Customer followUp BP', parent: kriteriaReports, linkController: 'kr_customer_followup_bp', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_Goods_GR = Menu.findByMenuCode('KR_Goods_GR') ?: new Menu(menuCode: 'KR_Goods_GR', label: 'Goods GR', details: 'Goods GR', parent: kriteriaReports, linkController: 'kr_goods_gr', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_Goods_BP = Menu.findByMenuCode('KR_Goods_BP') ?: new Menu(menuCode: 'KR_Goods_BP', label: 'Goods BP', details: 'Goods BP', parent: kriteriaReports, linkController: 'kr_goods_bp', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_Gate_Pass = Menu.findByMenuCode('KR_Gate_Pass') ?: new Menu(menuCode: 'KR_Gate_Pass', label: 'Gate Pass', details: 'Gate Pass', parent: kriteriaReports, linkController: 'gate_pass', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_KPI = Menu.findByMenuCode('KR_KPI') ?: new Menu(menuCode: 'KR_KPI', label: 'KPI', details: 'KPI', parent: kriteriaReports, linkController: 'kr_kpi', targetMode: Menu._LOAD_, seq: 20).save()     
        def KR_timeTrackingGR = Menu.findByMenuCode('KR_TIMETRACKINGGR') ?: new Menu(menuCode: 'KR_TIMETRACKINGGR', label: 'Time Tracking GR', details: 'Time Tracking GR', parent: kriteriaReports, linkController: 'timeTrackingGR', targetMode: Menu._LOAD_, seq: 19).save()
        def KR_Customer_Field_Action = Menu.findByMenuCode('KR_Customer_Field_Action') ?: new Menu(menuCode: 'KR_Customer_Field_Action', label: 'Customer Field Action', details: 'Customer Field Action', parent: kriteriaReports, linkController: 'kr_customer_field_action', targetMode: Menu._LOAD_, seq: 19).save()        
        def KR_Combined_Report = Menu.findByMenuCode('KR_Combined_Report') ?: new Menu(menuCode: 'KR_Combined_Report', label: 'Combined Report', details: 'Combined Report', parent: kriteriaReports, linkController: 'kr_combined_report', targetMode: Menu._LOAD_, seq: 19).save()        		
		def KR_Part_Sales = Menu.findByMenuCode('KR_Part_Sales') ?: new Menu(menuCode: 'KR_Part_Sales', label: 'Part Sales', details: 'Part Sales', parent: kriteriaReports, linkController: 'partSalesReport', targetMode: Menu._LOAD_, seq: 20).save()		
		
		
        kriteriaReports.addToChildren(KR_mrs)
        kriteriaReports.addToChildren(KR_appointmentGR)
        kriteriaReports.addToChildren(KR_appointmentBP)
        kriteriaReports.addToChildren(KR_receptionGR)
        kriteriaReports.addToChildren(KR_receptionBP)
        kriteriaReports.addToChildren(KR_receptionBP_Center)
        kriteriaReports.addToChildren(KR_productionGR)
        kriteriaReports.addToChildren(KR_productionBP)
        kriteriaReports.addToChildren(KR_deliveryGR)
        kriteriaReports.addToChildren(KR_deliveryBP)
        kriteriaReports.addToChildren(KR_deliveryBP_Center)
        kriteriaReports.addToChildren(KR_deliveryFinance)
        kriteriaReports.addToChildren(KR_Customer_Followup_GR)
        kriteriaReports.addToChildren(KR_Customer_Followup_BP)
        kriteriaReports.addToChildren(KR_Goods_GR)
        kriteriaReports.addToChildren(KR_Goods_BP)
        kriteriaReports.addToChildren(KR_Gate_Pass)
        kriteriaReports.addToChildren(KR_KPI)
        kriteriaReports.addToChildren(KR_timeTrackingGR)
        kriteriaReports.addToChildren(KR_Customer_Field_Action)
        kriteriaReports.addToChildren(KR_Combined_Report)
        kriteriaReports.addToChildren(KR_Part_Sales)

        
        def mobileFeature = Menu.findByMenuCode('MOBILE_FEATURE') ?: new Menu(menuCode: 'MOBILE_FEATURE', label: 'Mobile Feature', details: 'Mobile Feature', seq: 16)
        mobileFeature.save()

        def webBooking = Menu.findByMenuCode('WEB_BOOKING') ?: new Menu(menuCode: 'WEB_BOOKING', label: 'Web Booking', details: 'Web Booking', seq: 17)
        webBooking.save()


        bootstrapFinanceMenu()
        bootstrapHrdMenu()

    }

    def bootstrapAdminMenu() {
        def adminMenu = Menu.findByMenuCode('MENU_ADMIN') ?: new Menu(menuCode: 'MENU_ADMIN', label: 'Administrations', details: 'Admins', seq: 1).save()
        def userMenu = Menu.findByMenuCode('MENU_USER') ?: new Menu(menuCode: 'MENU_USER', label: 'User', details: 'User Administration', parent: adminMenu, linkController: 'user', targetMode: Menu._LOAD_, seq: 2).save()
//        def roleMenu = Menu.findByMenuCode('MENU_ROLE') ?: new Menu(menuCode: 'MENU_ROLE', label: 'Role', details: 'Role Administration', divider: true, parent: adminMenu, linkController: 'role', targetMode: Menu._LOAD_, seq: 3).save()
        def appSettingMenu = Menu.findByMenuCode('MENU_APP_SETTING') ?: new Menu(menuCode: 'MENU_APP_SETTING', label: 'Application Settings', details: 'Application Settings', parent: adminMenu, linkController: 'appSettingParam', targetMode: Menu._LOAD_, seq: 4).save()
        def userChangePassword = Menu.findByMenuCode('MENU_USER_CHANGE_PASSWORD') ?: new Menu(menuCode: 'MENU_USER_CHANGE_PASSWORD', label: 'Change Password', details: 'Change Password', parent: userMenu, linkController: 'user/changePassword', targetMode: Menu._LOAD_, seq: 6).save()

        def contohMenu = Menu.findByMenuCode('CONTOH') ?: new Menu(menuCode: 'CONTOH', label: 'Contoh', details: 'Contoh Saja', parent: adminMenu, linkController: 'contoh', targetMode: Menu._LOAD_, seq: 101).save()
        def cekcekMenu = Menu.findByMenuCode('CEKCEK') ?: new Menu(menuCode: 'CEKCEK', label: 'Cek Cek', details: 'Cekcek', parent: adminMenu, linkController: 'cekcek', targetMode: Menu._LOAD_, seq: 102).save()


        adminMenu.addToChildren(userMenu)
//        adminMenu.addToChildren(roleMenu)
        adminMenu.addToChildren(appSettingMenu)
        adminMenu.addToChildren(userChangePassword)
        adminMenu.addToChildren(contohMenu)
        adminMenu.addToChildren(cekcekMenu)

        adminMenu.save()
    }

    def bootstrapAdministrationMenu() {
        def administrationMenu = Menu.findByMenuCode('ADM') ?: new Menu(menuCode: 'ADM', label: 'Administration', details: 'Administration', seq: 2).save()
//		1.1. Company Dealer
        def companyDealerMenu = Menu.findByMenuCode('COMPANYDEALERMENU') ?: new Menu(menuCode: 'COMPANYDEALERMENU', parent: administrationMenu, label: 'Company Dealer', details: 'Company Dealer', seq: 1).save()

        def companyDealer = Menu.findByMenuCode('COMPANYDEALER') ?: new Menu(menuCode: 'COMPANYDEALER', label: 'Company Dealer', details: 'Company Dealer', parent: companyDealerMenu, linkController: 'companyDealer', targetMode: Menu._LOAD_, seq: 1).save()
        def contactPerson = Menu.findByMenuCode('CONTACTPERSON') ?: new Menu(menuCode: 'CONTACTPERSON', label: 'Contact Person', details: 'Contact Person', parent: companyDealerMenu, linkController: 'contactPerson', targetMode: Menu._LOAD_, seq: 2).save()
        def divisi = Menu.findByMenuCode('DIVISI') ?: new Menu(menuCode: 'DIVISI', label: 'Divisi', details: 'Divisi', parent: companyDealerMenu, linkController: 'divisi', targetMode: Menu._LOAD_, seq: 3).save()
        def tipeJamKerja = Menu.findByMenuCode('TIPEJAMKERJA') ?: new Menu(menuCode: 'TIPEJAMKERJA', label: 'Tipe Jam Kerja', details: 'Tipe Jam Kerja', parent: companyDealerMenu, linkController: 'jenisJamKerja', targetMode: Menu._LOAD_, seq: 4).save()
        def dealerPenjual = Menu.findByMenuCode('DEALERPENJUAL') ?: new Menu(menuCode: 'DEALERPENJUAL', label: 'Dealer Penjual', details: 'Dealer Penjual', parent: companyDealerMenu, linkController: 'dealerPenjual', targetMode: Menu._LOAD_, seq: 5).save()
        def jenisAlert = Menu.findByMenuCode('JENIS_ALERT') ?: new Menu(menuCode: 'JENIS_ALERT', label: 'Jenis Alert', details: 'Jenis Alert', parent: companyDealerMenu, linkController: 'jenisAlert', targetMode: Menu._LOAD_, seq: 6).save()
//        def namaJabatanPIC = Menu.findByMenuCode('NAMA_JABATAN_PIC') ?: new Menu(menuCode: 'NAMA_JABATAN_PIC', label: 'Nama Jabatan PIC', details: 'Nama Jabatan PIC', parent: companyDealerMenu, linkController: 'namaJabatanPIC', targetMode: Menu._LOAD_, seq: 7).save()
//        def jenisPerusahaan = Menu.findByMenuCode('JENIS_PERUSAHAAN') ?: new Menu(menuCode: 'JENIS_PERUSAHAAN', label: 'Jenis Perusahaan', details: 'Jenis Perusahaan', parent: companyDealerMenu, linkController: 'jenisPerusahaan', targetMode: Menu._LOAD_, seq: 8).save()
        def contohMenu = Menu.findByMenuCode('CONTOH2') ?: new Menu(menuCode: 'CONTOH2', label: 'Contoh2', details: 'Contoh Saja', parent: administrationMenu, linkController: 'contoh', targetMode: Menu._LOAD_, seq: 101).save()

        companyDealerMenu.addToChildren(companyDealer)
        companyDealerMenu.addToChildren(contactPerson)
        companyDealerMenu.addToChildren(divisi)
        companyDealerMenu.addToChildren(tipeJamKerja)
        companyDealerMenu.addToChildren(dealerPenjual)
//        companyDealerMenu.addToChildren(namaJabatanPIC)
//        companyDealerMenu.addToChildren(jenisPerusahaan)

//		1.2. User
//		1.3. General Parameter
        def generalParameter = Menu.findByMenuCode('GENERAL_PARAMETER') ?: new Menu(menuCode: 'GENERAL_PARAMETER', parent: administrationMenu, label: 'General Parameter', details: 'General Parameter', linkController: 'generalParameter', targetMode: Menu._LOAD_, seq: 3).save()

//		Kelompok NPB
        def kelompokNPB = Menu.findByMenuCode('KELOMPOKNPB') ?: new Menu(menuCode: 'KELOMPOKNPB', parent: administrationMenu, label: 'Kelompok NPB', details: 'Kelompok NPB', linkController: 'kelompokNPB', targetMode: Menu._LOAD_, seq: 3).save()

//		1.4. Master Lokasi
        def masterLokasiMenu = Menu.findByMenuCode('MASTERLOKASIMENU') ?: new Menu(menuCode: 'MASTERLOKASIMENU', parent: administrationMenu, label: 'Master Lokasi', details: 'Master Lokasi', seq: 4).save()
        def lokasiMenu = Menu.findByMenuCode('LOKASIMENU') ?: new Menu(menuCode: 'LOKASIMENU', parent: masterLokasiMenu, label: 'Master Lokasi', details: 'Lokasi', linkController: 'kelurahan', targetMode: Menu._LOAD_, seq: 1).save()
        def provinsiMenu = Menu.findByMenuCode('MASTERPROVINSIMENU') ?: new Menu(menuCode: 'MASTERPROVINISMENU', parent: masterLokasiMenu, label: 'Master Provinsi', details: 'Master Provinsi', linkController: 'provinsi', targetMode: Menu._LOAD_, seq: 2).save()
        def kabupatenMenu = Menu.findByMenuCode('MASTERKABUPATENMENU') ?: new Menu(menuCode: 'MASTERKABUPATENMENU', parent: masterLokasiMenu, label: 'Master Kabupaten', details: 'Master Kabupaten', linkController: 'kabKota', targetMode: Menu._LOAD_, seq: 3).save()
        def kecamatanMenu = Menu.findByMenuCode('MASTERKECAMATANMENU') ?: new Menu(menuCode: 'MASTERKECAMATANMENU', parent: masterLokasiMenu, label: 'Master Kecamatan', details: 'Master Kecamatan', linkController: 'kecamatan', targetMode: Menu._LOAD_, seq: 4).save()


//		1.5. Master Bank
        def masterBankMenu = Menu.findByMenuCode('MASTERBANKMENU') ?: new Menu(menuCode: 'MASTERBANKMENU', parent: administrationMenu, label: 'Master Bank', details: 'Master Bank', seq: 5).save()

        def bank = Menu.findByMenuCode('BANK') ?: new Menu(menuCode: 'BANK', label: 'Master Bank', details: 'Master Bank', parent: masterBankMenu, linkController: 'bank', targetMode: Menu._LOAD_, seq: 1).save()
        def persenCharge = Menu.findByMenuCode('PERSENCHARGE') ?: new Menu(menuCode: 'PERSENCHARGE', label: 'Persen Charge', details: 'Persen Charge', parent: masterBankMenu, linkController: 'rateBank', targetMode: Menu._LOAD_, seq: 2).save()
        def mesinEdc = Menu.findByMenuCode('MESINEDC') ?: new Menu(menuCode: 'MESINEDC', label: 'Mesin EDC', details: 'Mesin Edc', parent: masterBankMenu, linkController: 'mesinEdc', targetMode: Menu._LOAD_, seq: 3).save()

        masterBankMenu.addToChildren(bank)
        masterBankMenu.addToChildren(persenCharge)
        masterBankMenu.addToChildren(mesinEdc)

//		1.6. Master Vehicle
        def masterVehicle = Menu.findByMenuCode('MASTERVEHICLE') ?: new Menu(menuCode: 'MASTERVEHICLE', parent: administrationMenu, label: 'Master Vehicle', details: 'Master Vehicle', seq: 6).save()

        def kategoriKendaraan = Menu.findByMenuCode('KATEGORIKENDARAAN') ?: new Menu(menuCode: 'KATEGORIKENDARAAN', label: 'Kategori Kendaraan', details: 'Kategori Kendaraan', parent: masterVehicle, linkController: 'kategoriKendaraan', targetMode: Menu._LOAD_, seq: 1).save()
        def baseModel = Menu.findByMenuCode('VBASEMODEL') ?: new Menu(menuCode: 'VBASEMODEL', label: 'Base Model', details: 'Base Model', parent: masterVehicle, linkController: 'baseModel', targetMode: Menu._LOAD_, seq: 2).save()
        def stir = Menu.findByMenuCode('STIR') ?: new Menu(menuCode: 'STIR', label: 'Stir', details: 'Stir', parent: masterVehicle, linkController: 'stir', targetMode: Menu._LOAD_, seq: 3).save()
        def modelName = Menu.findByMenuCode('MODELNAME') ?: new Menu(menuCode: 'MODELNAME', label: 'Model Name', details: 'Model Name', parent: masterVehicle, linkController: 'modelName', targetMode: Menu._LOAD_, seq: 4).save()
        def bodyType = Menu.findByMenuCode('BODYTYPE') ?: new Menu(menuCode: 'BODYTYPE', label: 'Body Type', details: 'Body Type', parent: masterVehicle, linkController: 'bodyType', targetMode: Menu._LOAD_, seq: 5).save()
        def gear = Menu.findByMenuCode('GEAR') ?: new Menu(menuCode: 'GEAR', label: 'Gear', details: 'Gear', parent: masterVehicle, linkController: 'gear', targetMode: Menu._LOAD_, seq: 6).save()
        def grade = Menu.findByMenuCode('GRADE') ?: new Menu(menuCode: 'GRADE', label: 'Grade', details: 'Grade', parent: masterVehicle, linkController: 'grade', targetMode: Menu._LOAD_, seq: 7).save()
        def engine = Menu.findByMenuCode('ENGINE') ?: new Menu(menuCode: 'ENGINE', label: 'Engine', details: 'Engine', parent: masterVehicle, linkController: 'engine', targetMode: Menu._LOAD_, seq: 8).save()
        def country = Menu.findByMenuCode('COUNTRY') ?: new Menu(menuCode: 'COUNTRY', label: 'Country', details: 'Country', parent: masterVehicle, linkController: 'country', targetMode: Menu._LOAD_, seq: 9).save()
        def formOfVehicle = Menu.findByMenuCode('FORMOFVEHICLE') ?: new Menu(menuCode: 'FORMOFVEHICLE', label: 'Form Of Vehicle', details: 'Form Of Vehicle', parent: masterVehicle, linkController: 'formOfVehicle', targetMode: Menu._LOAD_, seq: 10).save()
        def bahanBakar = Menu.findByMenuCode('BAHANBAKAR') ?: new Menu(menuCode: 'BAHANBAKAR', label: 'Bahan Bakar', details: 'Bahan Bakar', parent: masterVehicle, linkController: 'bahanBakar', targetMode: Menu._LOAD_, seq: 11).save()
        def fullModel = Menu.findByMenuCode('FULLMODEL') ?: new Menu(menuCode: 'FULLMODEL', label: 'Full Model', details: 'Full Model', parent: masterVehicle, linkController: 'fullModelCode', targetMode: Menu._LOAD_, seq: 12).save()
        def upload = Menu.findByMenuCode('UPLOAD') ?: new Menu(menuCode: 'UPLOAD', label: 'Upload', details: 'Upload', parent: masterVehicle, linkController: 'uploadFullModelCode', targetMode: Menu._LOAD_, seq: 13).save()
        def tarifPerJam = Menu.findByMenuCode('TARIFPERJAM') ?: new Menu(menuCode: 'TARIFPERJAM', label: 'Tarif Per Jam', details: 'Tarif Perjam', parent: masterVehicle, linkController: 'tarifPerJam', targetMode: Menu._LOAD_, seq: 14).save()
        def tarifPerjamCbuNtam = Menu.findByMenuCode('TARIFPERJAMCBUNTAM') ?: new Menu(menuCode: 'TARIFPERJAMCBUNTAM', label: 'Tarif Per Jam CBUN Tam', details: 'Tarif Perjam CB UNTAM', parent: masterVehicle, linkController: 'tarifCBUNTAM', targetMode: Menu._LOAD_, seq: 15).save()
        def kodeKotaNopol = Menu.findByMenuCode('KODEKOTANOPOL') ?: new Menu(menuCode: 'KODEKOTANOPOL', label: 'Kode Kota NoPol', details: 'Kode Kota NoPol', parent: masterVehicle, linkController: 'kodeKotaNoPol', targetMode: Menu._LOAD_, seq: 16).save()
        def nextService = Menu.findByMenuCode('NEXTSERVICE') ?: new Menu(menuCode: 'NEXTSERVICE', label: 'Next Service', details: 'Next Service', parent: masterVehicle, linkController: 'perhitunganNextService', targetMode: Menu._LOAD_, seq: 17).save()
        def nextServiceBaseModel = Menu.findByMenuCode('NEXTSERVICEPERBASEMODEL') ?: new Menu(menuCode: 'NEXTSERVICEPERBASEMODEL', label: 'Next Service per Base Model', details: 'Next Service per Base Model', parent: masterVehicle, linkController: 'perhitunganNextServiceBaseModel', targetMode: Menu._LOAD_, seq: 18).save()
        def masterVehicleWac = Menu.findByMenuCode('MASTERVEHICLE-WAC') ?: new Menu(menuCode: 'MASTERVEHICLE-WAC', label: 'WAC', details: 'WAC', parent: masterVehicle, linkController: 'masterWac', targetMode: Menu._LOAD_, seq: 19).save()

        masterVehicle.addToChildren(kategoriKendaraan)
        masterVehicle.addToChildren(baseModel)
        masterVehicle.addToChildren(stir)
        masterVehicle.addToChildren(modelName)
        masterVehicle.addToChildren(bodyType)
        masterVehicle.addToChildren(gear)
        masterVehicle.addToChildren(grade)
        masterVehicle.addToChildren(engine)
        masterVehicle.addToChildren(country)
        masterVehicle.addToChildren(formOfVehicle)
        masterVehicle.addToChildren(bahanBakar)
        masterVehicle.addToChildren(fullModel)
        masterVehicle.addToChildren(upload)
        masterVehicle.addToChildren(tarifPerJam)
        masterVehicle.addToChildren(tarifPerjamCbuNtam)
        masterVehicle.addToChildren(kodeKotaNopol)
        masterVehicle.addToChildren(nextService)
        masterVehicle.addToChildren(nextServiceBaseModel)
        masterVehicle.addToChildren(masterVehicleWac)

//		1.7. Master TWC
        def masterTWCMenu = Menu.findByMenuCode('MASTERTWC') ?: new Menu(menuCode: 'MASTERTWC', parent: administrationMenu, label: 'Master TWC', details: 'Master TWC', seq: 7).save()

        def tarifPerJamTWC = Menu.findByMenuCode('TARIFPERJAMTWC') ?: new Menu(menuCode: 'TARIFPERJAMTWC', label: 'Tarif Per Jam TWC', details: 'Tarif Per Jam TWC', parent: masterTWCMenu, linkController: 'tarifTWC', targetMode: Menu._LOAD_, seq: 1).save()
        def persenPWC = Menu.findByMenuCode('PERSENPWC') ?: new Menu(menuCode: 'PERSENPWC', label: 'Persen PWC', details: 'Persen PWC', parent: masterTWCMenu, linkController: 'persenPwc', targetMode: Menu._LOAD_, seq: 2).save()

        masterTWCMenu.addToChildren(tarifPerJamTWC)
        masterTWCMenu.addToChildren(persenPWC)

//		1.8. Kalender Kerja
        def kalenderKerja = Menu.findByMenuCode('KALENDERKERJA') ?: new Menu(menuCode: 'KALENDERKERJA', label: 'Kalender Kerja', details: 'Kalender Kerja', parent: administrationMenu, linkController: 'kalenderKerja', targetMode: Menu._LOAD_, seq: 8).save()

//		1.9. ManPower
        def manPowerMenu = Menu.findByMenuCode('MANPOWER') ?: new Menu(menuCode: 'MANPOWER', parent: administrationMenu, label: 'Man Power', details: 'Man Power', seq: 9).save()

        def jabatan = Menu.findByMenuCode('JABATAN') ?: new Menu(menuCode: 'JABATAN', label: 'Jabatan', details: 'Jabatan', parent: manPowerMenu, linkController: 'manPower', targetMode: Menu._LOAD_, seq: 1).save()
        def level = Menu.findByMenuCode('LEVEL') ?: new Menu(menuCode: 'LEVEL', label: 'Level', details: 'Level', parent: manPowerMenu, linkController: 'manPowerDetail', targetMode: Menu._LOAD_, seq: 2).save()
        def sertifikat = Menu.findByMenuCode('SERTIFIKAT') ?: new Menu(menuCode: 'SERTIFIKAT', label: 'Sertifkat', details: 'Sertifikat', parent: manPowerMenu, linkController: 'sertifikat', targetMode: Menu._LOAD_, seq: 3).save()
        def sertifikatJob = Menu.findByMenuCode('SERTIFIKATJOB') ?: new Menu(menuCode: 'SERTIFIKATJOB', label: 'Sertifkat Job', details: 'Sertifikat Job', parent: manPowerMenu, linkController: 'sertifikatJobMap', targetMode: Menu._LOAD_, seq: 4).save()
        def namaBiodataPower = Menu.findByMenuCode('NAMABIODATAMANPOWER') ?: new Menu(menuCode: 'NAMABIODATAMANPOWER', label: 'Biodata Man Power', details: 'Biodata Man Power', parent: manPowerMenu, linkController: 'namaManPower', targetMode: Menu._LOAD_, seq: 5)
        def skillMap = Menu.findByMenuCode('SKILLMAP') ?: new Menu(menuCode: 'SKILLMAP', label: 'Skill Map', details: 'Skill Map', parent: manPowerMenu, linkController: 'manPowerSertifikat', targetMode: Menu._LOAD_, seq: 6).save()
        def klasifikasiJob = Menu.findByMenuCode('KLASIFIKASIJOB') ?: new Menu(menuCode: 'KLASIFIKASIJOB', label: 'Klasifikasi Job', details: 'Klasifikasi Job', parent: manPowerMenu, linkController: 'manPowerJob', targetMode: Menu._LOAD_, seq: 7).save()
        def manPowerGroup = Menu.findByMenuCode('GROUPMANPOWER') ?: new Menu(menuCode: 'GROUPMANPOWER', label: 'Group', details: 'Group', parent: manPowerMenu, linkController: 'groupManPower', targetMode: Menu._LOAD_, seq: 8).save()
        def manPowerStall = Menu.findByMenuCode('STALLMAPPING') ?: new Menu(menuCode: 'STALLMAPPING', label: 'Stall Mapping', details: 'Stall Mapping', parent: manPowerMenu, linkController: 'manPowerStall', targetMode: Menu._LOAD_, seq: 9).save()
        def jamKerja = Menu.findByMenuCode('JAMKERJA') ?: new Menu(menuCode: 'JAMKERJA', label: 'Jam Kerja', details: 'Jam Kerja', parent: manPowerMenu, linkController: 'manPowerJamKerja', targetMode: Menu._LOAD_, seq: 10).save()
        def absensi = Menu.findByMenuCode('ABSENSI') ?: new Menu(menuCode: 'ABSENSI', label: 'Rencana Ketidakhadiran', details: 'Rencana Ketidakhadiran', parent: manPowerMenu, linkController: 'manPowerAbsensi', targetMode: Menu._LOAD_, seq: 11).save()

        manPowerMenu.addToChildren(absensi)
        manPowerMenu.addToChildren(jabatan)
        manPowerMenu.addToChildren(level)
        manPowerMenu.addToChildren(sertifikat)
        manPowerMenu.addToChildren(skillMap)
        manPowerMenu.addToChildren(manPowerGroup)
        manPowerMenu.addToChildren(manPowerStall)
        manPowerMenu.addToChildren(jamKerja)
        manPowerMenu.addToChildren(sertifikatJob)
        manPowerMenu.addToChildren(klasifikasiJob)
        manPowerMenu.addToChildren(namaBiodataPower)

//		1.10. Master Stall
        def masterStall = Menu.findByMenuCode('MASTERSTALL') ?: new Menu(menuCode: 'MASTERSTALL', parent: administrationMenu, label: 'Master Stall', details: 'Master Stall', seq: 10).save()

        def jenisStall = Menu.findByMenuCode('JENISSTALL') ?: new Menu(menuCode: 'JENISSTALL', label: 'Jenis Stall', details: 'Jenis Stall', parent: masterStall, linkController: 'jenisStall', targetMode: Menu._LOAD_, seq: 1).save()
        def subJenisStall = Menu.findByMenuCode('SUBJENISSTALL') ?: new Menu(menuCode: 'SUBJENISSTALL', label: 'Sub Jenis Stall', details: 'Sub Jenis Stall', parent: masterStall, linkController: 'subJenisStall', targetMode: Menu._LOAD_, seq: 2).save()
        def namaStall = Menu.findByMenuCode('NAMASTALL') ?: new Menu(menuCode: 'NAMASTALL', label: 'Nama Stall', details: 'Nama Stall', parent: masterStall, linkController: 'stall', targetMode: Menu._LOAD_, seq: 3).save()
        def mappingStallJob = Menu.findByMenuCode('MAPPINGSTALLJOB') ?: new Menu(menuCode: 'MAPPINGSTALLJOB', label: 'Mapping Stall Job', details: 'Mapping Stall Job', parent: masterStall, linkController: 'stallJob', targetMode: Menu._LOAD_, seq: 4).save()
        def tipeBerat = Menu.findByMenuCode('TIPEBERAT') ?: new Menu(menuCode: 'TIPEBERAT', label: 'Tipe Berat', details: 'Tipe Berat', parent: masterStall, linkController: 'tipeBerat', targetMode: Menu._LOAD_, seq: 5).save()
        def mappingBeratModel = Menu.findByMenuCode('MAPPINGBERATMODEL') ?: new Menu(menuCode: 'MAPPINGBERATMODEL', label: 'Mapping Berat-Model', details: 'Mapping Berat-Model', parent: masterStall, linkController: 'mappingBeratBaseModel', targetMode: Menu._LOAD_, seq: 6).save()
        def mappingStallBerat = Menu.findByMenuCode('MAPPINGSTALLBERAT') ?: new Menu(menuCode: 'MAPPINGSTALLBERAT', label: 'Mapping Stall-Berat', details: 'Mapping Stall-Berat', parent: masterStall, linkController: 'mappingStallTipeBerat', targetMode: Menu._LOAD_, seq: 7).save()
        def lift = Menu.findByMenuCode('LIFT') ?: new Menu(menuCode: 'LIFT', label: 'Lift', details: 'Lift', parent: masterStall, linkController: 'lift', targetMode: Menu._LOAD_, seq: 8).save()

        masterStall.addToChildren(jenisStall)
        masterStall.addToChildren(subJenisStall)
        masterStall.addToChildren(namaStall)
        masterStall.addToChildren(mappingStallJob)
        masterStall.addToChildren(tipeBerat)
        masterStall.addToChildren(mappingBeratModel)
        masterStall.addToChildren(mappingStallBerat)
        masterStall.addToChildren(lift)

//		1.11. Master Job
        def masterJob = Menu.findByMenuCode('MASTERJOB') ?: new Menu(menuCode: 'MASTERJOB', parent: administrationMenu, label: 'Master Job', details: 'Master Job', seq: 11).save()

        def kategori = Menu.findByMenuCode('KATEGORI') ?: new Menu(menuCode: 'KATEGORI', label: 'Kategori', details: 'Kategori', parent: masterJob, linkController: 'kategoriJob', targetMode: Menu._LOAD_, seq: 1).save()
        def section = Menu.findByMenuCode('SECTION') ?: new Menu(menuCode: 'SECTION', label: 'Section', details: 'Section', parent: masterJob, linkController: 'section', targetMode: Menu._LOAD_, seq: 2).save()
        def serial = Menu.findByMenuCode('SERIAL') ?: new Menu(menuCode: 'SERIAL', label: 'Serial', details: 'Serial', parent: masterJob, linkController: 'serial', targetMode: Menu._LOAD_, seq: 3).save()
        def repair = Menu.findByMenuCode('REPAIR') ?: new Menu(menuCode: 'REPAIR', label: 'Repair', details: 'Repair', parent: masterJob, linkController: 'operation', targetMode: Menu._LOAD_, seq: 4).save()
        def operationDetail = Menu.findByMenuCode('OPDETAIL') ?: new Menu(menuCode: 'OPDETAIL', label: 'Operation Detail', details: 'Operation Detail', parent: masterJob, linkController: 'operationDetail', targetMode: Menu._LOAD_, seq: 5).save()
        def uploadJob = Menu.findByMenuCode('UPLOADJOB') ?: new Menu(menuCode: 'UPLOADJOB', label: 'Upload Job', details: 'Upload Job', parent: masterJob, linkController: 'uploadJob', targetMode: Menu._LOAD_, seq: 6).save()
        def panel = Menu.findByMenuCode('PANEL') ?: new Menu(menuCode: 'PANEL', label: 'Panel', details: 'Panel', parent: masterJob, linkController: 'masterPanel', targetMode: Menu._LOAD_, seq: 7).save()
        def serviceGratis = Menu.findByMenuCode('SERVICEGRATIS') ?: new Menu(menuCode: 'SERVICEGRATIS', label: 'Service Gratis', details: 'Service Gratis', parent: masterJob, linkController: 'serviceGratis', targetMode: Menu._LOAD_, seq: 8).save()
        def mappingJobPanel = Menu.findByMenuCode('MAPPINGJOBPANEL') ?: new Menu(menuCode: 'MAPPINGJOBPANEL', label: 'Mapping Job Panel', details: 'Mapping Job Panel', parent: masterJob, linkController: 'mappingJobPanel', targetMode: Menu._LOAD_, seq: 9).save()
        def stdTimeRR = Menu.findByMenuCode('STDTIMER&R') ?: new Menu(menuCode: 'STDTIMER&R', label: 'Std. Time R & R', details: 'Std. Time R & R', parent: masterJob, linkController: 'stdTimeWorkItems', targetMode: Menu._LOAD_, seq: 10).save()
        def repairDifficulty = Menu.findByMenuCode('REPAIRDIFFICULTY') ?: new Menu(menuCode: 'REPAIRDIFFICULTY', label: 'Repair Difficulty', details: 'Repair Difficulty', parent: masterJob, linkController: 'repairDifficulty', targetMode: Menu._LOAD_, seq: 11).save()
        def panelRepair = Menu.findByMenuCode('PANELREPAIR') ?: new Menu(menuCode: 'PANELREPAIR', label: 'Panel Repair', details: 'Panel Repair', parent: masterJob, linkController: 'panelRepair', targetMode: Menu._LOAD_, seq: 12).save()
        def paintingApplicationTime = Menu.findByMenuCode('PAINTINGAPPTIME') ?: new Menu(menuCode: 'PAINTINGAPPTIME', label: 'Painting App. Time', details: 'Painting App. Time', parent: masterJob, linkController: 'paintingApplicationTime', targetMode: Menu._LOAD_, seq: 13).save()

        def plasticBumperPaintingTime = Menu.findByMenuCode('PLASTICBUMPERPAINTINGTIME') ?: new Menu(menuCode: 'PLASTICBUMPERPAINTINGTIME', label: 'Plastic Bumper Painting Time', details: 'Plastic Bumper Painting Time', parent: masterJob, linkController: 'bumperPaintingTime', targetMode: Menu._LOAD_, seq: 15).save()
        def colorMatching = Menu.findByMenuCode('COLORMATCH') ?: new Menu(menuCode: 'COLORMATCH', label: 'Color Match', details: 'Color Match', parent: masterJob, linkController: 'colorMatching', targetMode: Menu._LOAD_, seq: 16).save()
        def temporaryJob = Menu.findByMenuCode('TEMPORARYJOB') ?: new Menu(menuCode: 'TEMPORARYJOB', label: 'Temporary Job', details: 'Temporary Job', parent: masterJob, linkController: 'temporaryJob', targetMode: Menu._LOAD_, seq: 17).save()
        def workItem = Menu.findByMenuCode('WORKITEM') ?: new Menu(menuCode: 'WORKITEM', label: 'Work Item', details: 'Work Item', parent: masterJob, linkController: 'workItems', targetMode: Menu._LOAD_, seq: 18).save()
        def mappingJobWorkItem = Menu.findByMenuCode('MAPPINGJOBWORKITEM') ?: new Menu(menuCode: 'MAPPINGJOBWORKITEM', label: 'Mapping Job Work Item', details: 'Mapping Job Work Item', parent: masterJob, linkController: 'mappingWorkItems', targetMode: Menu._LOAD_, seq: 19).save()
        def hargaJobSublet = Menu.findByMenuCode('HARGAJOBSUBLET') ?: new Menu(menuCode: 'HARGAJOBSUBLET', label: 'Tarif Job Sublet', details: 'Tarif Job Sublet', parent: masterJob, linkController: 'hargaJobSublet', targetMode: Menu._LOAD_, seq: 20).save()
        def defaultVendorSublet = Menu.findByMenuCode('DEFAULTVENDORJOBSUBLET') ?: new Menu(menuCode: 'DEFAULTVENDORJOBSUBLET', label: 'Default Vendor Job Sublet', details: 'Default Vendor Job Sublet', parent: masterJob, linkController: 'defaultVendorSublet', targetMode: Menu._LOAD_, seq: 21).save()
        def material = Menu.findByMenuCode('MATERIAL') ?: new Menu(menuCode: 'MATERIAL', label: 'Material', details: 'Material', parent: masterJob, linkController: 'material', targetMode: Menu._LOAD_, seq: 22).save()

        masterJob.addToChildren(kategori)
        masterJob.addToChildren(section)
        masterJob.addToChildren(serial)
        masterJob.addToChildren(repair)
        masterJob.addToChildren(uploadJob)
        masterJob.addToChildren(operationDetail)
        masterJob.addToChildren(panel)
        masterJob.addToChildren(mappingJobPanel)
        masterJob.addToChildren(repairDifficulty)
        masterJob.addToChildren(stdTimeRR)
        masterJob.addToChildren(panelRepair)
        masterJob.addToChildren(paintingApplicationTime)
        masterJob.addToChildren(plasticBumperPaintingTime)
        masterJob.addToChildren(colorMatching)
        masterJob.addToChildren(temporaryJob)
        masterJob.addToChildren(workItem)
        masterJob.addToChildren(mappingJobWorkItem)
        masterJob.addToChildren(serviceGratis)
        masterJob.addToChildren(hargaJobSublet)
        masterJob.addToChildren(defaultVendorSublet)
        masterJob.addToChildren(material)

//		1.12. Flat Rate
//		1.13. Price List
        def priceList = Menu.findByMenuCode('PRICE_LIST') ?: new Menu(menuCode: 'PRICE_LIST', parent: administrationMenu, label: 'Price List Service', details: 'Price List Service', linkController: 'priceList', targetMode: Menu._LOAD_, seq: 13).save()


//		1.14. Mapping FullModel Parts
//		1.15. Mapping Job Parts
        def mappingJobParts = Menu.findByMenuCode('MAPPINGJOB') ?: new Menu(menuCode: 'MAPPINGJOB', parent: administrationMenu, label: 'Mapping Job Parts', details: 'Mapping Job Parts', seq: 15).save()

        def jobPartsBiasa = Menu.findByMenuCode('JOBPARTSBIASA') ?: new Menu(menuCode: 'JOBPARTSBIASA', label: 'Mapping Job Parts', details: 'Mapping Job Parts', parent: mappingJobParts, linkController: 'partJob', targetMode: Menu._LOAD_, seq: 1).save()
        def jobPartsUpload = Menu.findByMenuCode('JOBPARTSUPLOAD') ?: new Menu(menuCode: 'JOBPARTSUPLOAD', label: 'Upload Mapping Parts Job ', details: 'Upload Mapping Parts Job', parent: mappingJobParts, linkController: 'uploadPartJob', targetMode: Menu._LOAD_, seq: 2).save()
        def jobPartsChange = Menu.findByMenuCode('JOBPARTSCHAGE') ?: new Menu(menuCode: 'JOBPARTSCHAGE', label: 'Mapping Job Parts Minor Change', details: 'Mapping Job Parts Minor Change', parent: mappingJobParts, linkController: 'XPartJobMinorChange', targetMode: Menu._LOAD_, seq: 3).save()

        mappingJobParts.addToChildren(jobPartsBiasa)
        mappingJobParts.addToChildren(jobPartsUpload)
        mappingJobParts.addToChildren(jobPartsChange)

//		1.16. Vendor Cat
        def vendorCat = Menu.findByMenuCode('VENDORCAT') ?: new Menu(menuCode: 'VENDORCAT', parent: administrationMenu, label: 'Vendor Cat', details: 'Vendor Cat', seq: 16).save()

        def masterVendorCat = Menu.findByMenuCode('MASTERVENDORCAT') ?: new Menu(menuCode: 'MASTERVENDORCAT', label: 'Master Vendor Cat', details: 'Master Vendor Cat', parent: vendorCat, linkController: 'vendorCat', targetMode: Menu._LOAD_, seq: 1).save()
        def masterWarnaCat = Menu.findByMenuCode('MASTERWARNACAT') ?: new Menu(menuCode: 'MASTERWARNACAT', label: 'Master Warna Cat', details: 'Master Warna Cat', parent: vendorCat, linkController: 'warnaVendor', targetMode: Menu._LOAD_, seq: 2).save()
        def defaultWarnaCat = Menu.findByMenuCode('DEFAULTWARNACAT') ?: new Menu(menuCode: 'DEFAULTWARNACAT', label: 'Default Warna Cat', details: 'Default Warna Cat', parent: vendorCat, linkController: 'defaultWarna', targetMode: Menu._LOAD_, seq: 3).save()
        def uploadMasterWarnaCat = Menu.findByMenuCode('UPLOADMASTERWARNACAT') ?: new Menu(menuCode: 'UPLOADMASTERWARNACAT', label: 'Upload Master Warna Cat', details: 'Upload Master Warna Cat', parent: vendorCat, linkController: 'uploadWarnaVendor', targetMode: Menu._LOAD_, seq: 4).save()

        vendorCat.addToChildren(masterVendorCat)
        vendorCat.addToChildren(defaultWarnaCat)
        vendorCat.addToChildren(masterWarnaCat)
        vendorCat.addToChildren(uploadMasterWarnaCat)

//		1.17. Master Approval
        def masterApproval = Menu.findByMenuCode('MASTERAPPROVAL') ?: new Menu(menuCode: 'MASTERAPPROVAL', parent: administrationMenu, label: 'Master Approval', details: 'Master Approval', seq: 17).save()

        def masterKegiatanApproval = Menu.findByMenuCode('MASTERKEGIATANAPPROVAL') ?: new Menu(menuCode: 'MASTERKEGIATANAPPROVAL', label: 'Master Kegiatan Approval', details: 'Master Kegiatan Approval', parent: masterApproval, linkController: 'kegiatanApproval', targetMode: Menu._LOAD_, seq: 1).save()
        def masterApprover = Menu.findByMenuCode('MASTERAPPROVER') ?: new Menu(menuCode: 'MASTERAPPROVER', label: 'Master Approver', details: 'Master Approver', parent: masterApproval, linkController: 'namaApproval', targetMode: Menu._LOAD_, seq: 2).save()

        masterApproval.addToChildren(masterKegiatanApproval)
        masterApproval.addToChildren(masterApprover)

//		1.18. Print History
        def printHistory = Menu.findByMenuCode('PRINTHISTORY') ?: new Menu(menuCode: 'PRINTHISTORY', label: 'Print History', details: 'Print History', parent: administrationMenu, linkController: 'historyPrint', targetMode: Menu._LOAD_, seq: 18).save()

//		1.19. Audit Trail
        def auditTrail = Menu.findByMenuCode('AUDITTRAIL') ?: new Menu(menuCode: 'AUDITTRAIL', label: 'Audit Trail', details: 'Audit Trali', parent: administrationMenu, linkController: 'auditTrail', targetMode: Menu._LOAD_, seq: 19).save()

//		1.20. Performance Log
        def performanceLog = Menu.findByMenuCode('PERFORMANCELOG') ?: new Menu(menuCode: 'PERFORMANCELOG', label: 'Performance Log', details: 'Performance Log', parent: administrationMenu, linkController: 'performanceLog', targetMode: Menu._LOAD_, seq: 20).save()

//		1.21. Error Log
        def errorLog = Menu.findByMenuCode('ERRORLOG') ?: new Menu(menuCode: 'ERRORLOG', label: 'Error Log', details: 'Error Log', parent: administrationMenu, linkController: 'errorLog', targetMode: Menu._LOAD_, seq: 21).save()

//		1.22. Error Translation
        def errorTranslation = Menu.findByMenuCode('ERRORTRANSLATION') ?: new Menu(menuCode: 'ERRORTRANSLATION', label: 'Error Translation', details: 'Error Translation', parent: administrationMenu, linkController: 'errorTranslation', targetMode: Menu._LOAD_, seq: 22).save()

//		1.23. Master Durasi Perjalanan

        def durasiPerjalanan = Menu.findByMenuCode('DURASI') ?: new Menu(menuCode: 'DURASI', label: 'Master Durasi Perjalanan', details: 'Master Durasi Perjalanan', parent: administrationMenu, linkController: 'masterDurasiPerjalananDriver', targetMode: Menu._LOAD_, seq: 23).save()

//		1.24. Controller
        def controller = Menu.findByMenuCode('CONTROLLER') ?: new Menu(menuCode: 'CONTROLLER', parent: administrationMenu, label: 'Controller', details: 'Controller', seq: 24).save()
        def statusMP = Menu.findByMenuCode('STATUSMP') ?: new Menu(menuCode: 'STATUSMP', label: 'ManPower Status', details: 'ManPower Status', parent: controller, linkController: 'statusManPower', targetMode: Menu._LOAD_, seq: 1).save()
        def statusStall = Menu.findByMenuCode('STATUSSTALL') ?: new Menu(menuCode: 'STATUSSTALL', label: 'Stall Status', details: 'Stall Status', parent: controller, linkController: 'statusStall', targetMode: Menu._LOAD_, seq: 2).save()
        controller.addToChildren(statusMP)
        controller.addToChildren(statusStall)
//		1.25. Master Leasing
        def masterLeasing = Menu.findByMenuCode('MASTERLEASING') ?: new Menu(menuCode: 'MASTERLEASING', parent: administrationMenu, label: 'Master Leasing', details: 'Master Leasing', linkController: 'leasing', targetMode: Menu._LOAD_, seq: 25).save()
//		1.26. Master Vendor Asuransi
        def masterVendorAsuransi = Menu.findByMenuCode('MASTERVENDORASURANSI') ?: new Menu(menuCode: 'MASTERVENDORASURANSI', parent: administrationMenu, label: 'Master Vendor Asuransi', details: 'Master Vendor Asuransi', linkController: 'vendorAsuransi', targetMode: Menu._LOAD_, seq: 26).save()

//		1.27. Master Informasi Penting.
        def masterInformasiPenting = Menu.findByMenuCode('MASTERINFORMASIPENTING') ?: new Menu(menuCode: 'MASTERINFORMASIPENTING', parent: administrationMenu, label: 'Master Informasi Penting', details: 'Master Informasi Penting', linkController: 'informasiPenting', targetMode: Menu._LOAD_, seq: 27).save()

//		1.28. Master Target.

//      Master Flat Rate
        def masterFlatRate = Menu.findByMenuCode('MASTERFLATRATE') ?: new Menu(menuCode: 'MASTERFLATRATE', parent: administrationMenu, label: 'Master Flat Rate', details: 'Master Flat Rate', seq: 29).save()
        def inputFlatRate = Menu.findByMenuCode('INPUTFLATRATE') ?: new Menu(menuCode: 'INPUTFLATRATE', label: 'Input Flat Rate', details: 'Input Flat Rate', parent: masterFlatRate, linkController: 'flatRate', targetMode: Menu._LOAD_, seq: 1).save()
        def uploadFlatRate = Menu.findByMenuCode('UPLOADFLATRATE') ?: new Menu(menuCode: 'UPLOADFLATRATE', label: 'Upload Flat Rate', details: 'Upload Flat Rate', parent: masterFlatRate, linkController: 'uploadFlatRate', targetMode: Menu._LOAD_, seq: 2).save()

//      Mapping FUll Model Parts
        def masterMappingFullMode = Menu.findByMenuCode('MASTERMAPPINGFULLMODE') ?: new Menu(menuCode: 'MASTERMAPPINGFULLMODE', parent: administrationMenu, label: 'Mapping Full Model', details: 'Mapping Full Model', seq: 28).save()
        def mappingFullModeParts = Menu.findByMenuCode('MAPPINGFULLMODEPARTS') ?: new Menu(menuCode: 'MAPPINGFULLMODEPARTS', label: 'Mapping Full Model Parts', details: 'Mapping Full Model Parts', parent: masterMappingFullMode, linkController: 'partsMapping', targetMode: Menu._LOAD_, seq: 1).save()
        def uploadFullModeParts = Menu.findByMenuCode('UPLOADFULLMODEPARTS') ?: new Menu(menuCode: 'UPLOADFULLMODEPARTS', label: 'Upload Full Model Parts', details: 'Upload Full Model Parts', parent: masterMappingFullMode, linkController: 'uploadPartsMapping', targetMode: Menu._LOAD_, seq: 2).save()
        def mappingFullModePartsMinorChange = Menu.findByMenuCode('MAPPINGFULLMODEPARTSMINORCHANGE') ?: new Menu(menuCode: 'MAPPINGFULLMODEPARTSMINORCHANGE', label: 'Mapping Full Model Parts Minor Change', details: 'Mapping Full Mode Parts Minor Change', parent: masterMappingFullMode, linkController: 'XPartsMappingMinorChange', targetMode: Menu._LOAD_, seq: 3).save()

//      Master ANtrian
        def masterAntrian = Menu.findByMenuCode('MASTERANTRIAN') ?: new Menu(menuCode: 'MASTERANTRIAN', parent: administrationMenu, label: 'Master Antrian', details: 'Master Antrian', seq: 30).save()
        def loketAntrian = Menu.findByMenuCode('LOKETANTRIAN') ?: new Menu(menuCode: 'LOKETANTRIAN', label: 'Master Loket', details: 'Loket', parent: masterAntrian, linkController: 'loket', targetMode: Menu._LOAD_, seq: 1).save()
        def masterSuara = Menu.findByMenuCode('MASTERSUARA') ?: new Menu(menuCode: 'MASTERSUARA', label: 'Master Suara', details: 'Suara', parent: masterAntrian, linkController: 'audio', targetMode: Menu._LOAD_, seq: 2).save()


        masterFlatRate.addToChildren(inputFlatRate)
        masterFlatRate.addToChildren(uploadFlatRate)
        masterFlatRate.save()

        //master Antrian Childs
        masterAntrian.addToChildren(loketAntrian)
        masterAntrian.addToChildren(masterSuara)



        companyDealerMenu.save()
        //masterLokasiMenu.save()
        masterBankMenu.save()
        masterVehicle.save()
        masterTWCMenu.save()
        manPowerMenu.save()
        masterStall.save()
        masterJob.save()
        mappingJobParts.save()
        vendorCat.save()
        masterApproval.save()
        masterAntrian.save()
        controller.save()

        def penegasanPengiriman = Menu.findByMenuCode('PENEGASANPENGIRIMAN') ?: new Menu(menuCode: 'PENEGASANPENGIRIMAN', label: 'Penegasan Pengiriman', details: 'Penegasan Pengiriman', parent: administrationMenu, linkController: 'penegasanPengiriman', targetMode: Menu._LOAD_, seq: 99).save()

        def maserTarget = Menu.findByMenuCode('MASTER_TARGET') ?: new Menu(menuCode: 'MASTER_TARGET', parent: administrationMenu, label: 'Master Target', details: 'Master Target', seq: 30).save()
        def leadTime = Menu.findByMenuCode('LEADTIME_STAGNATION') ?: new Menu(menuCode: 'LEADTIME_STAGNATION', label: 'Leadtime & Stagnation', details: 'Leadtime & Stagnation', parent: maserTarget, linkController: 'leadTime', targetMode: Menu._LOAD_, seq: 1).save()
        def targetGR = Menu.findByMenuCode('TARGET_GR') ?: new Menu(menuCode: 'TARGET_GR', label: 'Target GR', details: 'Target GR', parent: maserTarget, linkController: 'targetGR', targetMode: Menu._LOAD_, seq: 2).save()
        def targetBP = Menu.findByMenuCode('TARGET_BP') ?: new Menu(menuCode: 'TARGET_BP', label: 'Target BP', details: 'Target BP', parent: maserTarget, linkController: 'targetBP', targetMode: Menu._LOAD_, seq: 3).save()
        def uploadTechnicalWarranty = Menu.findByMenuCode('UPLOAD_TECHNICAL_WARRANTY') ?: new Menu(menuCode: 'UPLOAD_TECHNICAL_WARRANTY', label: 'Upload Technnical Warranty', details: 'Leadtime & Stagnation', parent: maserTarget, linkController: 'uploadTechnicalWarranty', targetMode: Menu._LOAD_, seq: 4).save()
        def uploadCustomerSatisfaction = Menu.findByMenuCode('UPLOAD_CUSTOMER_SATISFACTION') ?: new Menu(menuCode: 'UPLOAD_CUSTOMER_SATISFACTION', label: 'Upload Customer Satisfaction', details: 'Leadtime & Stagnation', parent: maserTarget, linkController: 'uploadCustomerSatisfaction', targetMode: Menu._LOAD_, seq: 5).save()
        def uploadCOGS = Menu.findByMenuCode('UPLOAD_COGS') ?: new Menu(menuCode: 'UPLOAD_COGS', label: 'Upload COGS', details: 'Upload COGS', parent: maserTarget, linkController: 'uploadCOGS', targetMode: Menu._LOAD_, seq: 6).save()

        maserTarget.addToChildren(leadTime)
        maserTarget.addToChildren(targetGR)
        maserTarget.addToChildren(targetBP)
        maserTarget.addToChildren(uploadTechnicalWarranty)
        maserTarget.addToChildren(uploadCustomerSatisfaction)
        maserTarget.addToChildren(uploadCOGS)


        administrationMenu.addToChildren(maserTarget)
        administrationMenu.addToChildren(priceList)
        administrationMenu.addToChildren(companyDealerMenu)
        administrationMenu.addToChildren(masterLokasiMenu)
        administrationMenu.addToChildren(masterBankMenu)
        administrationMenu.addToChildren(masterTWCMenu)
        administrationMenu.addToChildren(masterVehicle)
        administrationMenu.addToChildren(kalenderKerja)
        administrationMenu.addToChildren(manPowerMenu)
        administrationMenu.addToChildren(masterStall)
        administrationMenu.addToChildren(masterJob)
        administrationMenu.addToChildren(mappingJobParts)
        administrationMenu.addToChildren(vendorCat)
        administrationMenu.addToChildren(masterLeasing)
        administrationMenu.addToChildren(masterVendorAsuransi)
        administrationMenu.addToChildren(masterInformasiPenting)
        administrationMenu.addToChildren(controller)
        administrationMenu.addToChildren(durasiPerjalanan)
        administrationMenu.addToChildren(printHistory)
        administrationMenu.addToChildren(auditTrail)
        administrationMenu.addToChildren(errorLog)
        administrationMenu.addToChildren(errorTranslation)
        administrationMenu.addToChildren(performanceLog)
        administrationMenu.addToChildren(masterAntrian)
        administrationMenu.addToChildren(contohMenu)
        administrationMenu.addToChildren(penegasanPengiriman)

        administrationMenu.save()

    }

    def bootstrapCustomerProfileMenu() {
        def customerProfile = Menu.findByMenuCode('CUSTOMER_PROFILE') ?: new Menu(menuCode: 'CUSTOMER_PROFILE', label: 'Customer Profile', details: 'Customer Profile', seq: 4).save()

        def customerCompany = Menu.findByMenuCode('CUSTOMER_COMPANY') ?: new Menu(menuCode: 'CUSTOMER_COMPANY', label: 'Customer Company', details: 'Customer Company', parent: customerProfile, linkController: 'company', targetMode: Menu._LOAD_, seq: 1).save()
        def historyCustomerVehicle = Menu.findByMenuCode('HISTORY_CUSTOMER_VEHICLE') ?: new Menu(menuCode: 'HISTORY_CUSTOMER_VEHICLE', label: 'Vehicle', details: 'Vehicle', parent: customerProfile, linkController: 'historyCustomerVehicle', targetMode: Menu._LOAD_, seq: 2).save()
        def customerForm = Menu.findByMenuCode('CUSTOMERFORM') ?: new Menu(menuCode: 'CUSTOMERFORM', label: 'Customer', details: 'Customer', parent: customerProfile, linkController: 'historyCustomer', targetMode: Menu._LOAD_, seq: 3).save()
        //Master Customer
        def masterCustomer = Menu.findByMenuCode('MASTERCUSTOMER') ?: new Menu(menuCode: 'MASTERCUSTOMER', parent: customerProfile, label: 'Master Customer', details: 'Master Customer', seq: 4).save()
        def idCard = Menu.findByMenuCode('IDCARD') ?: new Menu(menuCode: 'IDCARD', label: 'ID Card', details: 'ID Card', parent: masterCustomer, linkController: 'jenisIdCard', targetMode: Menu._LOAD_, seq: 1).save()
        def agama = Menu.findByMenuCode('AGAMA') ?: new Menu(menuCode: 'AGAMA', label: 'Agama', details: 'Agama', parent: masterCustomer, linkController: 'agama', targetMode: Menu._LOAD_, seq: 2).save()
        def hobby = Menu.findByMenuCode('HOBBY') ?: new Menu(menuCode: 'HOBBY', label: 'Hobby', details: 'Hobby', parent: masterCustomer, linkController: 'hobby', targetMode: Menu._LOAD_, seq: 3).save()
        def survey = Menu.findByMenuCode('SURVEY') ?: new Menu(menuCode: 'SURVEY', label: 'Survey', details: 'Survey', parent: masterCustomer, linkController: 'jenisSurvey', targetMode: Menu._LOAD_, seq: 4).save()
        def merk = Menu.findByMenuCode('MERK') ?: new Menu(menuCode: 'MERK', label: 'Merk', details: 'Merk', parent: masterCustomer, linkController: 'merk', targetMode: Menu._LOAD_, seq: 5).save()
        def model = Menu.findByMenuCode('MODEL') ?: new Menu(menuCode: 'MODEL', label: 'Model', details: 'Model', parent: masterCustomer, linkController: 'model', targetMode: Menu._LOAD_, seq: 6).save()
        def dealer = Menu.findByMenuCode('DEALER') ?: new Menu(menuCode: 'DEALER', label: 'Dealer', details: 'Dealer', parent: masterCustomer, linkController: 'dealer', targetMode: Menu._LOAD_, seq: 7).save()
        def warna = Menu.findByMenuCode('WARNA') ?: new Menu(menuCode: 'WARNA', label: 'Warna', details: 'Warna', parent: masterCustomer, linkController: 'warna', targetMode: Menu._LOAD_, seq: 8).save()
        def statusImport = Menu.findByMenuCode('STATUSIMPORT') ?: new Menu(menuCode: 'STATUSIMPORT', label: 'Status Import', details: 'Status Import', parent: masterCustomer, linkController: 'staImport', targetMode: Menu._LOAD_, seq: 9).save()
        def namaJabatanPIC = Menu.findByMenuCode('NAMA_JABATAN_PIC') ?: new Menu(menuCode: 'NAMA_JABATAN_PIC', label: 'Nama Jabatan PIC', details: 'Nama Jabatan PIC', parent: masterCustomer, linkController: 'namaJabatanPIC', targetMode: Menu._LOAD_, seq: 10).save()
        def jenisPerusahaan = Menu.findByMenuCode('JENIS_PERUSAHAAN') ?: new Menu(menuCode: 'JENIS_PERUSAHAAN', label: 'Jenis Perusahaan', details: 'Jenis Perusahaan', parent: masterCustomer, linkController: 'jenisPerusahaan', targetMode: Menu._LOAD_, seq: 11).save()

        def spk = Menu.findByMenuCode('SPK') ?: new Menu(menuCode: 'SPK', label: 'SPK', details: 'SPK', parent: customerProfile, linkController: 'SPK', targetMode: Menu._LOAD_, seq: 5).save()
        def spkAsuransi = Menu.findByMenuCode('SPK_ASURANSI') ?: new Menu(menuCode: 'SPK_ASURANSI', label: 'SPK Asuransi', details: 'SPK Asuransi', parent: customerProfile, seq: 6).save()
            def dokumenAsuransi = Menu.findByMenuCode('DOK_ASURANSI') ?: new Menu(menuCode: 'DOK_ASURANSI', label: 'Dokumen Asuransi', details: 'Dokumen Asuransi', parent: spkAsuransi, linkController: 'SPKAsuransi', targetMode: Menu._LOAD_, seq: 61).save()
        //upload data sales
        def uploadDataSales = Menu.findByMenuCode('UPLOADDATASALES') ?: new Menu(menuCode: 'UPLOADDATASALES', parent: customerProfile, linkController: 'uploadDataSales', label: 'Upload Data Sales', details: 'Upload Data Sales', seq: 7).save()
        def masterJDPowerSurvey = Menu.findByMenuCode('MASTER_JD_SURVEY') ?: new Menu(menuCode: 'MASTER_JD_SURVEY', label: 'Master JD Power Survey', details: 'Master JD Power Survey', parent: customerProfile, linkController: 'customerSurvey', targetMode: Menu._LOAD_, seq: 8).save()
        def komposisiKendaraan = Menu.findByMenuCode('KOMPOSISIKENDARAAN') ?: new Menu(menuCode: 'KOMPOSISIKENDARAAN', label: 'Komposisi Kendaraan', details: 'Komposisi Kendaraan', parent: customerProfile, linkController: 'komposisiKendaraan', targetMode: Menu._LOAD_, seq: 9).save()
        def uploadTpss = Menu.findByMenuCode('UPLOADDATATPSS') ?: new Menu(menuCode: 'UPLOADDATATPSS', label: 'Upload Data TPSS', details: 'Upload Data TPSS', parent: customerProfile, linkController: 'uploadTpss', targetMode: Menu._LOAD_, seq: 10).save()
        def masterFieldAction = Menu.findByMenuCode('MASTER_FIELD_ACTION') ?: new Menu(menuCode: 'MASTER_FIELD_ACTION', label: 'Master Field Action', details: 'Master Field Action', parent: customerProfile, linkController: 'FA', targetMode: Menu._LOAD_, seq: 11).save()
        def masterDetailFieldAction = Menu.findByMenuCode('ADD_MASTER_DETAIL_FIELD_ACTION') ?: new Menu(menuCode: 'ADD_MASTER_DETAIL_FIELD_ACTION', label: 'Add Detail Field Action', details: 'Add Detail Field Action', parent: customerProfile, linkController: 'addDetailFieldAction', targetMode: Menu._LOAD_, seq: 12).save()
        def uploadDetailFieldAction = Menu.findByMenuCode('UPLOAD_DETAIL_FIELD_ACTION') ?: new Menu(menuCode: 'UPLOAD_DETAIL_FIELD_ACTION', label: 'Upload Detail Field Action', details: 'Upload Detail Field Action', parent: customerProfile, linkController: 'uploaDetailFieldAction', targetMode: Menu._LOAD_, seq: 13).save()
        def partsAndJobUntukFieldAction = Menu.findByMenuCode('PARTS_AND_JOB_UNTUK_FIELD_ACTION') ?: new Menu(menuCode: 'PARTS_AND_JOB_UNTUK_FIELD_ACTION', label: 'Parts & Job untuk Field Action', details: 'Parts & Job untuk Field Action', parent: customerProfile, linkController: 'partsJobFieldAction', targetMode: Menu._LOAD_, seq: 14).save()

        spkAsuransi.addToChildren(dokumenAsuransi)
        spkAsuransi.save()

        masterCustomer.addToChildren(idCard)
        masterCustomer.addToChildren(agama)
        masterCustomer.addToChildren(hobby)
        masterCustomer.addToChildren(survey)
        masterCustomer.addToChildren(merk)
        masterCustomer.addToChildren(model)
        masterCustomer.addToChildren(dealer)
        masterCustomer.addToChildren(warna)
        masterCustomer.addToChildren(statusImport)
        masterCustomer.addToChildren(namaJabatanPIC)
        masterCustomer.addToChildren(jenisPerusahaan)
        masterCustomer.save()

        customerProfile.addToChildren(masterCustomer)
        customerProfile.addToChildren(komposisiKendaraan)
        customerProfile.addToChildren(spkAsuransi)
        customerProfile.addToChildren(uploadDataSales)
        customerProfile.addToChildren(spk)
        customerProfile.addToChildren(masterJDPowerSurvey)
        customerProfile.addToChildren(masterFieldAction)
        customerProfile.addToChildren(masterDetailFieldAction)
        customerProfile.addToChildren(uploadDetailFieldAction)
        customerProfile.addToChildren(partsAndJobUntukFieldAction)


        customerProfile.save()

    }

    def bootstrapPartMenu() {

        def part = Menu.findByMenuCode('PARTS') ?: new Menu(menuCode: 'PARTS', label: 'Parts', details: 'Parts', seq: 3).save()

        //parameter
        def parameter = Menu.findByMenuCode('PARAMETER') ?: new Menu(menuCode: 'PARAMETER', parent: part, label: 'Parameter', details: 'Parameter', seq: 2).save()
        def MIP = Menu.findByMenuCode('MIP') ?: new Menu(menuCode: 'MIP', label: 'MIP', details: 'MIP', parent: parameter, linkController: 'MIP', targetMode: Menu._LOAD_, seq: 1).save()
        def tipeOrder = Menu.findByMenuCode('TIPEORDER') ?: new Menu(menuCode: 'TIPEORDER', label: 'Tipe Order', details: 'Tipe Order', parent: parameter, linkController: 'partTipeOrder', targetMode: Menu._LOAD_, seq: 2).save()
        def ICC = Menu.findByMenuCode('ICC') ?: new Menu(menuCode: 'ICC', label: 'ICC', details: 'ICC', parent: parameter, targetMode: Menu._LOAD_, seq: 3).save()
        def kodeICC = Menu.findByMenuCode('KODEICC') ?: new Menu(menuCode: 'KODEICC', label: 'Kode ICC', details: 'Kode ICC', parent: ICC, linkController: 'parameterICC', targetMode: Menu._LOAD_, seq: 1).save()
        def lokasiICC = Menu.findByMenuCode('LOKASIICC') ?: new Menu(menuCode: 'LOKASIICC', label: 'Lokasi ICC', details: 'Lokasi ICC', parent: ICC, linkController: 'location', targetMode: Menu._LOAD_, seq: 2).save()
        def RPP = Menu.findByMenuCode('RPP') ?: new Menu(menuCode: 'RPP', label: 'RPP', details: 'RPP', parent: parameter, linkController: 'RPP', targetMode: Menu._LOAD_, seq: 4).save()
        def dpParts = Menu.findByMenuCode('DPPARTS') ?: new Menu(menuCode: 'DPPARTS', label: 'DP Parts', details: 'DP Parts', parent: parameter, linkController: 'dpParts', targetMode: Menu._LOAD_, seq: 5).save()
        def batasPengembalianDPParts = Menu.findByMenuCode('BATASPENGEMBALIANDPPARTS') ?: new Menu(menuCode: 'BATASPENGEMBALIANDPPARTS', label: 'Batas Pengembalian DP Parts', details: 'Batas Pengembalian DP Parts', parent: parameter, linkController: 'pengembalianDP', targetMode: Menu._LOAD_, seq: 6).save()
        def batasWaktuDiBinning = Menu.findByMenuCode('BATASWAKTUDIBINNING') ?: new Menu(menuCode: 'BATASWAKTUDIBINNING', label: 'Batas Waktu Di Binning', details: 'Batas Waktu Di Binning', parent: parameter, linkController: 'umurBinning', targetMode: Menu._LOAD_, seq: 7).save()
        def masterRangeDP = Menu.findByMenuCode('MASTERRANGEDP') ?: new Menu(menuCode: 'MASTERRANGEDP', label: 'Master Maintenance Range DP', details: 'Master Maintenance Range DP', parent: parameter, linkController: 'masterRangeDp', targetMode: Menu._LOAD_, seq: 8).save()

        //klasifikasi goods
        def klasifikasiGoods = Menu.findByMenuCode('INPUTKLASIFIKASIGOODS') ?: new Menu(menuCode: 'INPUTKLASIFIKASIGOODS', label: 'Input Klasifikasi Goods', details: 'Input Klasifikasi Goods', parent: part, linkController: 'KlasifikasiGoods', targetMode: Menu._LOAD_, seq: 3).save()

        //master
        def master = Menu.findByMenuCode('MASTER') ?: new Menu(menuCode: 'MASTER', parent: part, label: 'Master', details: 'Master', seq: 1).save()
        def vendor = Menu.findByMenuCode('VENDOR') ?: new Menu(menuCode: 'VENDOR', label: 'Vendor', details: 'Vendor', parent: master, linkController: 'vendor', targetMode: Menu._LOAD_, seq: 1).save()
        def SCC = Menu.findByMenuCode('SCC') ?: new Menu(menuCode: 'SCC', label: 'SCC', details: 'SCC', parent: master, linkController: 'SCC', targetMode: Menu._LOAD_, seq: 2).save()
        def franc = Menu.findByMenuCode('FRANCHISE') ?: new Menu(menuCode: 'FRANCHISE', label: 'Franchise', details: 'Franchise', parent: master, linkController: 'franc', targetMode: Menu._LOAD_, seq: 3).save()
        def groupParts = Menu.findByMenuCode('GROUPPARTS') ?: new Menu(menuCode: 'GROUPPARTS', label: 'Group Parts', details: 'Group Parts', parent: master, linkController: 'group', targetMode: Menu._LOAD_, seq: 4).save()
        def satuan = Menu.findByMenuCode('SATUAN') ?: new Menu(menuCode: 'SATUAN', label: 'Satuan', details: 'Satuan', parent: master, linkController: 'satuan', targetMode: Menu._LOAD_, seq: 5).save()
        //goods
        def goodsMenu = Menu.findByMenuCode('GOODSMENU') ?: new Menu(menuCode: 'GOODSMENU', label: 'Goods', details: 'Goods', parent: master, targetMode: Menu._LOAD_, seq: 6).save()
        def goods = Menu.findByMenuCode('GOODS') ?: new Menu(menuCode: 'GOODS', label: 'Master Goods', details: 'Master Goods', parent: goodsMenu, linkController: 'goods', targetMode: Menu._LOAD_, seq: 1).save()
        def goodsUpload = Menu.findByMenuCode('GOODSUPLOAD') ?: new Menu(menuCode: 'GOODSUPLOAD', label: 'Upload Goods', details: 'Upload Goods', parent: goodsMenu, linkController: 'uploadGoods', targetMode: Menu._LOAD_, seq: 2).save()
        //diskon
        def discount = Menu.findByMenuCode('DISCOUNT') ?: new Menu(menuCode: 'DISCOUNT', label: 'Discount', details: 'Discount', parent: master, targetMode: Menu._LOAD_, seq: 7).save()
        def perGroup = Menu.findByMenuCode('PERGROUP') ?: new Menu(menuCode: 'PERGROUP', label: 'Discount Per Group', details: 'Discount Per Group', parent: discount, linkController: 'groupDiskon', targetMode: Menu._LOAD_, seq: 1).save()
        def perKode = Menu.findByMenuCode('PERKODE') ?: new Menu(menuCode: 'PERKODE', label: 'Discount Per Kode', details: 'Diskon Per Kode', parent: discount, linkController: 'goodsDiskon', targetMode: Menu._LOAD_, seq: 2).save()
        def perGroupUpload = Menu.findByMenuCode('PERGROUPUPLOAD') ?: new Menu(menuCode: 'PERGROUPUPLOAD', label: 'Upload Per Group', details: 'Upload Per Group', parent: discount, linkController: 'uploadGroupDiskon', targetMode: Menu._LOAD_, seq: 3).save()
        def perKodeUpload = Menu.findByMenuCode('PERKODEUPLOAD') ?: new Menu(menuCode: 'PERKODEUPLOAD', label: 'Upload Per Kode', details: 'Upload Per Kode', parent: discount, linkController: 'uploadGoodsDiskon', targetMode: Menu._LOAD_, seq: 4).save()
        def alasanAdjusment = Menu.findByMenuCode('ALASANADJUSMENT') ?: new Menu(menuCode: 'ALASANADJUSMENT', label: 'Alasan Adjustment', details: 'Alasan Adjustment', parent: master, linkController: 'alasanAdjusment', targetMode: Menu._LOAD_, seq: 8).save()
        //master -> harga jual goods
        def menuHargaJualGoods = Menu.findByMenuCode('MENUHARGAJUALGOODS') ?: new Menu(menuCode: 'MENUHARGAJUALGOODS', label: 'Harga Jual Goods', details: 'Harga Jual Goods', parent: master, targetMode: Menu._LOAD_, seq: 9).save()
        def hargaJualGoods = Menu.findByMenuCode('HARGAJUALGOODS') ?: new Menu(menuCode: 'HARGAJUALGOODS', label: 'Harga Jual Goods', details: 'Harga Jual Goods', parent: menuHargaJualGoods, linkController: 'goodsHargaJual', targetMode: Menu._LOAD_, seq: 1).save()
        def uploadHargaJualGoods = Menu.findByMenuCode('UPLOADHARGAJUALGOODS') ?: new Menu(menuCode: 'UPLOADHARGAJUALGOODS', label: 'Upload Harga Jual Goods', details: 'Upload Harga Jual Goods', parent: menuHargaJualGoods, linkController: 'uploadHargaJualGoods', targetMode: Menu._LOAD_, seq: 2).save()
        //harga jual non SPLD
        def hargaJualNonSPLD = Menu.findByMenuCode('HARGAJUALNONSPLD') ?: new Menu(menuCode: 'HARGAJUALNONSPLD', label: 'Harga Jual Non SPLD', details: 'Harga Jual Non SPLD', parent: master, targetMode: Menu._LOAD_, seq: 10).save()
        def semuaParts = Menu.findByMenuCode('SEMUAPARTS') ?: new Menu(menuCode: 'SEMUAPARTS', parent: hargaJualNonSPLD, label: 'Semua Parts', linkController: 'hargaJualNonSPLD', details: 'Semua Parts', seq: 1).save()
        def perParts = Menu.findByMenuCode('PERPARTS') ?: new Menu(menuCode: 'PERPARTS', parent: hargaJualNonSPLD, label: 'Per Part', linkController: 'hargaJualNonSPLDPerPart', details: 'Per Parts', seq: 2).save()
        def uploadHargaJual = Menu.findByMenuCode('UPLOADHARGAJUALNONSPLD') ?: new Menu(menuCode: 'UPLOADHARGAJUALNONSPLD', parent: hargaJualNonSPLD, label: 'Upload Harga Jual Non SPLD', linkController: 'uploadHargaJualNonSPLD', details: 'Upload Harga Jual Non SPLD', seq: 3).save()
        def warrantyClaim = Menu.findByMenuCode('WARRANTYCLAIM') ?: new Menu(menuCode: 'WARRANTYCLAIM', label: 'Warranty Claim', details: 'Warranty Claim', parent: master, linkController: 'warrantyClaim', targetMode: Menu._LOAD_, seq: 11).save()

        def soq = Menu.findByMenuCode('SOQ') ?: new Menu(menuCode: 'SOQ', label: 'SOQ', details: 'SOQ', parent: part, linkController: 'SOQ', targetMode: Menu._LOAD_, seq: 4).save()
        def request = Menu.findByMenuCode('REQUEST') ?: new Menu(menuCode: 'REQUEST', label: 'Request', details: 'Request', parent: part, linkController: 'request', targetMode: Menu._LOAD_, seq: 5).save()
		def cekKetersediaanParts = Menu.findByMenuCode('CEK_KETERSEDIAAN_PARTS') ?: new Menu(menuCode: 'CEK_KETERSEDIAAN_PARTS', label: 'Cek Ketersediaan Parts', details: 'Cek Ketersediaan Parts', parent: part, linkController: 'cekKetersediaanParts', targetMode: Menu._LOAD_, seq: 6).save()
		def validasiOrderParts = Menu.findByMenuCode('VALIDASI_ORDER_PARTS') ?: new Menu(menuCode: 'VALIDASI_ORDER_PARTS', label: 'Validasi Order Parts', details: 'Validasi Order Parts', parent: part, linkController: 'validasiOrderParts', targetMode: Menu._LOAD_, seq: 7).save()
		def unapprovedOrderParts = Menu.findByMenuCode('VIEW_UNAPPROVED_ORDER_PARTS') ?: new Menu(menuCode: 'VIEW_UNAPPROVED_ORDER_PARTS', label: 'View UnApproved Order Parts', details: 'View UnApproved Order Parts', parent: part, linkController: 'unapprovedOrderParts', targetMode: Menu._LOAD_, seq: 8).save()

        def sendPO = Menu.findByMenuCode('SENDPO') ?: new Menu(menuCode: 'SENDPO', label: 'Send PO', details: 'Send PO', parent: part, linkController: 'SendPO',  seq: 9).save()
        def etaPO = Menu.findByMenuCode('ETAPO') ?: new Menu(menuCode: 'ETAPO', label: 'Eta PO', details: 'ETA PO', parent: part, linkController: 'ETAPO', targetMode: Menu._LOAD_, seq: 10).save()
        def uploadETA = Menu.findByMenuCode('UploadETA') ?: new Menu(menuCode: 'UPLOADETA', label: 'Upload ETA PO', details: 'Upload ETA PO', parent: part, linkController: 'UploadETA', targetMode: Menu._LOAD_, seq: 11).save()

//        def po = Menu.findByMenuCode('PO') ?: new Menu(menuCode: 'PO', label: 'PO', details: 'PO', parent: part, seq: 9).save()
//        def etaPO = Menu.findByMenuCode('ETA_PO') ?: new Menu(menuCode: 'ETA_PO', label: 'Eta PO', details: 'ETA PO', parent: po, linkController: 'ETAPO', targetMode: Menu._LOAD_, seq: 1).save()
//        def sendPO = Menu.findByMenuCode('SENDPO') ?: new Menu(menuCode: 'SENDPO', label: 'Send PO', details: 'Send PO', parent: po, linkController: 'SendPO', targetMode: Menu._LOAD_, seq: 2).save()
//        def uploadETA = Menu.findByMenuCode('UploadETA') ?: new Menu(menuCode: 'UploadETA', label: 'Upload ETA PO', details: 'Upload ETA PO', parent: po, linkController: 'UploadETA', targetMode: Menu._LOAD_, seq: 3).save()

        //invoice
        def invoice = Menu.findByMenuCode('VIEW_PO') ?: new Menu(menuCode: 'VIEW_PO', label: 'View PO', details: 'View PO', parent: part, targetMode: Menu._LOAD_, seq: 12).save()
        def viewPoInvoicePart = Menu.findByMenuCode('INVOICE') ?: new Menu(menuCode: 'INVOICE', label: 'Invoice', details: 'Invoice', parent: invoice, linkController: 'poInvoice', targetMode: Menu._LOAD_, seq: 1).save()
        invoice.addToChildren(viewPoInvoicePart)

        def viewInvoicePart = Menu.findByMenuCode('VIEW_INVOICE') ?: new Menu(menuCode: 'VIEW_INVOICE', label: 'View Invoice', details: 'View Invoice', parent: part, linkController: 'invoice', targetMode: Menu._LOAD_, seq: 13).save()
        def inputInvoicePart = Menu.findByMenuCode('INPUT_INVOICE') ?: new Menu(menuCode: 'INPUT_INVOICE', label: 'Input Invoice', details: 'Input Invoice', parent: part, linkController: 'inputInvoice', targetMode: Menu._LOAD_, seq: 14).save()
        def uploadInvoice = Menu.findByMenuCode('UPLOAD_INVOICE') ?: new Menu(menuCode: 'UPLOAD_INVOICE', label: 'Upload Invoice', details: 'Upload Invoice', parent: part, linkController: 'uploadInvoice', targetMode: Menu._LOAD_, seq: 15).save()

        def nonOntimeETA = Menu.findByMenuCode('VIEW_NON_ONTIME_ETA') ?: new Menu(menuCode: 'VIEW_NON_ONTIME_ETA', label: 'View Non On Time ETA', details: 'View Non On Time ETA', parent: part, linkController: 'viewETA', targetMode: Menu._LOAD_, seq: 16).save()

        def goodsReceive = Menu.findByMenuCode('PARTSRECEIVE') ?: new Menu(menuCode: 'PARTSRECEIVE', label: 'Parts Receive', details: 'Parts Receive', parent: part, linkController: 'goodsReceive', targetMode: Menu._LOAD_, seq: 17).save()
//        def goodsReceiveView = Menu.findByMenuCode('PARTSRECEIVE_VIEW') ?: new Menu(menuCode: 'PARTSRECEIVE_VIEW', label: 'View Parts Receive', details: 'View Parts Receive', parent: goodsReceive, linkController: 'goodsReceive', targetMode: Menu._LOAD_, seq: 1).save()
//        def goodsReceiveInput = Menu.findByMenuCode('PARTSRECEIVE_INPUT') ?: new Menu(menuCode: 'PARTSRECEIVE_INPUT', label: 'Input Parts Receive', details: 'Input Parts Receive', parent: goodsReceive, linkController: 'inputGoodsReceive', targetMode: Menu._LOAD_, seq: 2).save()
//        goodsReceive.addToChildren(goodsReceiveView)
//        goodsReceive.addToChildren(goodsReceiveInput)

        def binning = Menu.findByMenuCode('PARTS_BINNING') ?: new Menu(menuCode: 'PARTS_BINNING', label: 'Parts Binning', details: 'Parts Binning', parent: part, linkController: 'binning', targetMode: Menu._LOAD_, seq: 18).save()

        def stockIN = Menu.findByMenuCode('STOCKIN') ?: new Menu(menuCode: 'STOCKIN', label: 'Stock In', details: 'Stock In', parent: part, linkController: 'stockIN', targetMode: Menu._LOAD_, seq: 19).save()

        def poMaintenance = Menu.findByMenuCode('PO_MAINTENANCE') ?: new Menu(menuCode: 'PO_MAINTENANCE', label: 'PO Maintenance', details: 'PO Maintenance', parent: part, linkController: 'poMaintenance', targetMode: Menu._LOAD_, seq: 20).save()

        def claim = Menu.findByMenuCode('PARTSCLAIM') ?: new Menu(menuCode: 'PARTSCLAIM', label: 'Parts Claim', details: 'Parts Claim', parent: part, linkController: 'claim', targetMode: Menu._LOAD_, seq: 21).save()
//        def claimInput = Menu.findByMenuCode('CLAIMINPUT') ?: new Menu(menuCode: 'CLAIMINPUT', label: 'Input Parts Claim', details: 'Input Parts Claim', parent: claim, linkController: 'claimInput', targetMode: Menu._LOAD_, seq: 1).save()
//        def claimView = Menu.findByMenuCode('CLAIMVIEW') ?: new Menu(menuCode: 'CLAIMVIEW', label: 'View Parts Claim', details: 'View Parts Claim', parent: claim, linkController: 'claim', targetMode: Menu._LOAD_, seq: 2).save()
//        claim.addToChildren(claimInput)
//        claim.addToChildren(claimView)

        def returns = Menu.findByMenuCode('PARTSRETURNS') ?: new Menu(menuCode: 'PARTSRETURNS', label: 'Parts Return', details: 'Parts Return', parent: part,  linkController: 'returns', targetMode: Menu._LOAD_, seq: 22).save()
//        def returnsView = Menu.findByMenuCode('RETURNSVIEW') ?: new Menu(menuCode: 'RETURNSVIEW', label: 'View Parts Returns', details: 'View Parts Returns', parent: returns, linkController: 'returns', targetMode: Menu._LOAD_, seq: 1).save()
//        def returnsInput = Menu.findByMenuCode('RETURNSINPUT') ?: new Menu(menuCode: 'RETURNSINPUT', label: 'Input Parts Returns', details: 'Input Parts Returns', parent: returns, linkController: 'returnsInput', targetMode: Menu._LOAD_, seq: 2).save()
//        returns.addToChildren(returnsView)
//        returns.addToChildren(returnsInput)

        def prePicking = Menu.findByMenuCode('PREPICKING') ?: new Menu(menuCode: 'PREPICKING', label: 'Pre Picking', details: 'Pre Picking', parent: part, linkController: 'prePickingSlip', targetMode: Menu._LOAD_, seq: 23).save()

        def pickingSlip = Menu.findByMenuCode('PICKINGSLIP') ?: new Menu(menuCode: 'PICKINGSLIP', label: 'Picking Slip', details: 'Picking Slip', parent: part, seq: 24).save()
        def pickingSlipView = Menu.findByMenuCode('PICKINGSLIPVIEW') ?: new Menu(menuCode: 'PICKINGSLIPVIEW', label: 'View Picking Slip', details: 'View Picking Slip', parent: pickingSlip, linkController: 'pickingSlip', targetMode: Menu._LOAD_, seq: 1).save()
        def pickingSlipInput = Menu.findByMenuCode('PICKINGSLIPINPUT') ?: new Menu(menuCode: 'PICKINGSLIPINPUT', label: 'Input Picking Slip', details: 'Input Picking Slip', parent: pickingSlip, linkController: 'pickingSlipInput', targetMode: Menu._LOAD_, seq: 2).save()
        pickingSlip.addToChildren(pickingSlipView)
        pickingSlip.addToChildren(pickingSlipInput)

        def blockStokParts = Menu.findByMenuCode('BLOCK_STOCK') ?: new Menu(menuCode: 'BLOCK_STOCK', label: 'Block Stock', details: 'Block Stock', parent: part, linkController: 'blockStok', targetMode: Menu._LOAD_, seq: 25).save()

        def perhtunganICC = Menu.findByMenuCode('PERHITUNGAN_ICC')?: new Menu(menuCode: 'PERHITUNGAN_ICC', label: 'Perhitungan ICC', details: 'Perhitungan ICC', parent: part, linkController: 'ICC', targetMode: Menu._LOAD_, seq:29).save()

        //view stock
        def stock = Menu.findByMenuCode('VIEW_STOCK') ?: new Menu(menuCode: 'VIEW_STOCK', label: 'View Stock', details: 'View Stock', parent: part, linkController: 'partsStok', targetMode: Menu._LOAD_, seq:30 ).save()

        def partsAdjust = Menu.findByMenuCode('PARTS_ADJUST') ?: new Menu(menuCode: 'PARTS_ADJUST', label: 'On Hand Adjustment', details: 'On Hand Adjustment', parent: part, seq: 31).save()
        def viewPartsAdjust = Menu.findByMenuCode('VIEW_PARTS_ADJUST') ?: new Menu(menuCode: 'VIEW_PARTS_ADJUST', label: 'View On Hand Adjustment', details: 'View On Hand Adjustment', parent: partsAdjust, linkController: 'partsAdjust', targetMode: Menu._LOAD_, seq: 1).save()
        def inputPartsAdjust = Menu.findByMenuCode('INPUT_PARTS_ADJUST') ?: new Menu(menuCode: 'INPUT_PARTS_ADJUST', label: 'Input On Hand Adjustment', details: 'Input On Hand Adjustment', parent: partsAdjust, linkController: 'inputOnHandAdjustment', targetMode: Menu._LOAD_, seq: 2).save()
        partsAdjust.addToChildren(viewPartsAdjust)
        partsAdjust.addToChildren(inputPartsAdjust)

        def partsDisposal = Menu.findByMenuCode('PARTS_DISPOSAL') ?: new Menu(menuCode: 'PARTS_DISPOSAL', label: 'Disposal', details: 'Disposal', linkController: 'partsDisposal', parent: part, seq: 32).save()
        def pts = Menu.findByMenuCode('PARTS_TRANSFER_STOK')?: new Menu(menuCode: 'PARTS_TRANSFER_STOK', label: 'Parts Transfer Stok', details: 'Parts Transfer Stok', parent: part, seq:33 ).save()
        def ptsKirim = Menu.findByMenuCode('PARTS_TRANSFER_STOK_KIRIM')?: new Menu(menuCode: 'PARTS_TRANSFER_STOK_KIRIM', label: 'Kirim Permintaan', details: 'Kirim Permintaan', parent: pts, linkController: 'partsTransferStok', targetMode: Menu._LOAD_, seq:1 ).save()
        def ptsCek = Menu.findByMenuCode('PARTS_TRANSFER_STOK_CEK')?: new Menu(menuCode: 'PARTS_TRANSFER_STOK_CEK', label: 'Cek Ketersediaan', details: 'Cek Ketersediaan', parent: pts, linkController: 'partsTransferStokCek', targetMode: Menu._LOAD_, seq:2 ).save()
        pts.addToChildren(ptsKirim)
        pts.addToChildren(ptsCek)
        def parts_reconsile = Menu.findByMenuCode('PARTS_RECONSILE')?: new Menu(menuCode: 'PARTS_RECONSILE', label: 'Parts Reconsile', details: 'Parts Reconsile', parent: part, linkController: 'partsReconsile', targetMode: Menu._LOAD_, seq:34 ).save()
//        def viewPartsDisposal = Menu.findByMenuCode('VIEW_PARTS_DISPOSAL') ?: new Menu(menuCode: 'VIEW_PARTS_DISPOSAL', label: 'View Disposal', details: 'View Disposal', parent: partsDisposal, linkController: 'partsDisposal', targetMode: Menu._LOAD_, seq: 1).save()
//        def inputPartsDisposal = Menu.findByMenuCode('INPUT_PARTS_DISPOSAL') ?: new Menu(menuCode: 'INPUT_PARTS_DISPOSAL', label: 'Input Disposal', details: 'Input Disposal', parent: partsDisposal, linkController: 'inputDisposal', targetMode: Menu._LOAD_, seq: 2).save()
//        partsDisposal.addToChildren(viewPartsDisposal)
//        partsDisposal.addToChildren(inputPartsDisposal)
        def partSales = Menu.findByMenuCode('DIRECTSALES') ?: new Menu(menuCode: 'DIRECTSALES', label: 'Penjualan Langsung', details: 'Penjualan Langsung', parent: part, targetMode: Menu._LOAD_, seq: 36).save()
        def customerSales = Menu.findByMenuCode('CUSTOMERSALES') ?: new Menu(menuCode: 'CUSTOMERSALES', label: 'Pelanggan Penjualan', details: 'Pelanggan Penjualan', parent: partSales, linkController: 'customerSales', targetMode: Menu._LOAD_, seq: 1).save()
        def salesOrder = Menu.findByMenuCode('SALESORDER') ?: new Menu(menuCode: 'SALESORDER', label: 'Penjualan Barang', details: 'Penjualan Barang', parent: partSales, linkController: 'salesOrder', targetMode: Menu._LOAD_, seq: 2).save()
        def deliveryOrder = Menu.findByMenuCode('DELIVERYORDER') ?: new Menu(menuCode: 'DELIVERYORDER', label: 'Pengiriman Barang', details: 'Pengiriman Barang', parent: partSales, linkController: 'deliveryOrder', targetMode: Menu._LOAD_, seq: 3).save()
        def returPenjualan = Menu.findByMenuCode('RETURPENJUALAN') ?: new Menu(menuCode: 'RETURPENJUALAN', label: 'Retur Penjualan', details: 'Retur Penjualan', parent: partSales, linkController: 'salesRetur', targetMode: Menu._LOAD_, seq: 4).save()
        //stock opname
        def stockOpnameMenu = Menu.findByMenuCode('STOCKOPNAMEMENU') ?: new Menu(menuCode: 'STOCKOPNAMEMENU', label: 'Stock Opname', details: 'Stock Opname', parent: part, linkController: '', targetMode: Menu._LOAD_, seq: 34).save()
        def stockOpname = Menu.findByMenuCode('STOCKOPNAME') ?: new Menu(menuCode: 'STOCKOPNAME', label: 'Stock Opname', details: 'Stock Opname', parent: stockOpnameMenu, linkController: 'stokOPNameFreeze', targetMode: Menu._LOAD_, seq: 1).save()
        def viewStockOpname = Menu.findByMenuCode('VIEWSTOCKOPNAME') ?: new Menu(menuCode: 'VIEWSTOCKOPNAME', label: 'View Stock Opname', details: 'View Stock Opname', parent: stockOpnameMenu, linkController: 'stokOPName', targetMode: Menu._LOAD_, seq: 2).save()
        def inputHasilStockOpname = Menu.findByMenuCode('INPUTHASILSTOCKOPNAME') ?: new Menu(menuCode: 'INPUTHASILSTOCKOPNAME', label: 'Input Hasil Stock Opname', details: 'Input Hasil Stock Opname', parent: stockOpnameMenu, linkController: 'inputHasilStokOpname', targetMode: Menu._LOAD_, seq: 4).save()
        def viewStockOpnameTidakSesuai = Menu.findByMenuCode('VIEWINPUTSTOCKOPNAMETDKSESUAI') ?: new Menu(menuCode: 'VIEWINPUTSTOCKOPNAMETDKSESUAI', label: 'View Stock Opname Yang Tidak Sesuai', details: 'View Stock Opname Yang Tidak Sesuai', parent: stockOpnameMenu, linkController: 'viewSOpnameTdkSesuai', targetMode: Menu._LOAD_, seq: 5).save()

        def npb = Menu.findByMenuCode('NOTA_PESANAN_BARANG')?: new Menu(menuCode: 'NOTA_PESANAN_BARANG', label: 'Nota Pesanan Barang', details: 'Nota Pesanan Barang', parent: part, linkController: 'notaPesananBarang', targetMode: Menu._LOAD_, seq:99).save()
        def cpofa = Menu.findByMenuCode('CONTROL_PART_ORDER')?: new Menu(menuCode: 'CONTROL_PART_ORDER', label: 'Control Part Order From Appointment', details: 'Control Part Order From Appointment', parent: part, linkController: 'controlPartOrder', targetMode: Menu._LOAD_, seq:100).save()
        def pfifo = Menu.findByMenuCode('PARTS_AGING')?: new Menu(menuCode: 'PARTS_AGING', label: 'Parts Aging (FIFO)', details: 'Parts Aging (FIFO)', parent: part, linkController: 'partsAging', targetMode: Menu._LOAD_, seq:101).save()


        ICC.addToChildren(kodeICC)
        ICC.addToChildren(lokasiICC)
	//	ICC.addToChildren(perhtunganICC)

        parameter.addToChildren(MIP)
        parameter.addToChildren(tipeOrder)
        parameter.addToChildren(ICC)
        parameter.addToChildren(RPP)
        parameter.addToChildren(dpParts)
        parameter.addToChildren(batasPengembalianDPParts)
        parameter.addToChildren(batasWaktuDiBinning)
        parameter.addToChildren(masterRangeDP)

        menuHargaJualGoods.addToChildren(hargaJualGoods)
        menuHargaJualGoods.addToChildren(uploadHargaJualGoods)
        menuHargaJualGoods.save()

        master.addToChildren(vendor)
        master.addToChildren(SCC)
        master.addToChildren(franc)
        master.addToChildren(groupParts)
        master.addToChildren(satuan)
        master.addToChildren(goodsMenu)
        master.addToChildren(discount)
        master.addToChildren(alasanAdjusment)
        master.addToChildren(menuHargaJualGoods)
        master.addToChildren(warrantyClaim)
        master.addToChildren(hargaJualNonSPLD)
 
        goodsMenu.addToChildren(goods)
        goodsMenu.addToChildren(goodsUpload)

        hargaJualNonSPLD.addToChildren(semuaParts)
        hargaJualNonSPLD.addToChildren(perParts)
        hargaJualNonSPLD.addToChildren(uploadHargaJual)

        discount.addToChildren(perGroup)
        discount.addToChildren(perKode)
        discount.addToChildren(perGroupUpload)
        discount.addToChildren(perKodeUpload)

        partSales.addToChildren(customerSales)
        partSales.addToChildren(salesOrder)
        partSales.addToChildren(deliveryOrder)
        partSales.addToChildren(returPenjualan)
        partSales.save()

        stockOpnameMenu.addToChildren(stockOpname)
        stockOpnameMenu.addToChildren(viewStockOpname)
        stockOpnameMenu.addToChildren(inputHasilStockOpname)
        stockOpnameMenu.addToChildren(viewStockOpnameTidakSesuai)
        stockOpnameMenu.save()

        part.addToChildren(parameter)
        part.addToChildren(klasifikasiGoods)
        part.addToChildren(master)
        part.addToChildren(soq)
        part.addToChildren(request)
		part.addToChildren(cekKetersediaanParts)
		part.addToChildren(validasiOrderParts)
		part.addToChildren(unapprovedOrderParts)
        part.addToChildren(etaPO)
        part.addToChildren(sendPO)
        part.addToChildren(uploadETA)
        part.addToChildren(invoice)
        part.addToChildren(viewInvoicePart)
        part.addToChildren(inputInvoicePart)
        part.addToChildren(uploadInvoice)
        part.addToChildren(nonOntimeETA)
        part.addToChildren(goodsReceive)
        part.addToChildren(binning)
        part.addToChildren(stockIN)
        part.addToChildren(poMaintenance)
        part.addToChildren(claim)
        part.addToChildren(npb)
        part.addToChildren(returns)
        part.addToChildren(prePicking)
        part.addToChildren(pickingSlip)
        part.addToChildren(stock)
        part.addToChildren(stockOpnameMenu)
        part.addToChildren(partsAdjust)
        part.addToChildren(partsDisposal)
        part.addToChildren(perhtunganICC)
        part.addToChildren(blockStokParts)
        part.addToChildren(pts)
        part.addToChildren(parts_reconsile)
        part.addToChildren(cpofa)
        part.addToChildren(partSales)
        part.addToChildren(pfifo)

        part.save()
    }

    def bootstrapCustomerComplaintMenu() {

        def customerComplaint = Menu.findByMenuCode('CUSTOMER_COMPLAINT') ?: new Menu(menuCode: 'CUSTOMER_COMPLAINT', label: 'Customer Complaint', details: 'Customer Complaint', seq: 5).save()
        def complaintList = Menu.findByMenuCode('COMPLAINT_LIST') ?: new Menu(menuCode: 'COMPLAINT_LIST', label: 'List Complaint', details: 'List Complaint', parent: customerComplaint, linkController: 'complaint', targetMode: Menu._LOAD_, seq: 1).save()

        customerComplaint.addToChildren(complaintList)

        customerComplaint.save()

    }

    def bootstrapBoardMenu() {
        def scheduleBoard = Menu.findByMenuCode('SCHEDULE_BOARD') ?: new Menu(menuCode: 'SCHEDULE_BOARD', label: 'Schedule Board', details: 'Schedule Board', seq: 8)
        scheduleBoard.save()

        def asbGr = Menu.findByMenuCode('ASB_GR') ?: new Menu(menuCode: 'ASB_GR', parent: scheduleBoard, label: 'ASB GR', details: 'ASB GR', seq: 1).save()
        def asbGrInput = Menu.findByMenuCode('ASB_GR_INPUT') ?: new Menu(menuCode: 'ASB_GR_INPUT', label: 'Input ASB GR', details: 'Input ASB GR', parent: asbGr, linkController: 'ASB', targetMode: Menu._LOAD_, seq: 1).save()
        def asbGrView = Menu.findByMenuCode('ASB_GR_VIEW') ?: new Menu(menuCode: 'ASB_GR_VIEW', label: 'View ASB GR', details: 'View ASB GR', parent: asbGr, linkController: 'ASBGRView', targetMode: Menu._LOAD_, seq: 2).save()
        asbGr.addToChildren(asbGrInput)
        asbGr.addToChildren(asbGrView)

        def jpbGr = Menu.findByMenuCode('JPB_GR') ?: new Menu(menuCode: 'JPB_GR', parent: scheduleBoard, label: 'JPB GR', details: 'JPB GR', seq: 2).save()
        def jpbGrInput = Menu.findByMenuCode('JPB_GR_INPUT') ?: new Menu(menuCode: 'JPB_GR_INPUT', label: 'Input JPB GR', details: 'Input JPB GR', parent: jpbGr, linkController: 'JPBGRInput', targetMode: Menu._LOAD_, seq: 1).save()
        def jpbGrView = Menu.findByMenuCode('JPB_GR_VIEW') ?: new Menu(menuCode: 'JPB_GR_VIEW', label: 'View JPB GR', details: 'View JPB GR', parent: jpbGr, linkController: 'JPBGRView', targetMode: Menu._LOAD_, seq: 2).save()
        jpbGr.addToChildren(jpbGrInput)
        jpbGr.addToChildren(jpbGrView)

        def jpbBp = Menu.findByMenuCode('JPB_BP') ?: new Menu(menuCode: 'JPB_BP', parent: scheduleBoard, label: 'JPB BP', details: 'JPB BP', seq: 3).save()
        def jpbBpInput = Menu.findByMenuCode('JPB_BP_INPUT') ?: new Menu(menuCode: 'JPB_BP_INPUT', label: 'Input JPB BP', details: 'Input JPB BP', parent: jpbBp, linkController: 'JPBBPInput', targetMode: Menu._LOAD_, seq: 1).save()
        def jpbBpView = Menu.findByMenuCode('JPB_BP_VIEW') ?: new Menu(menuCode: 'JPB_BP_VIEW', label: 'View JPB BP', details: 'View JPB BP', parent: jpbBp, linkController: 'JPBBPView', targetMode: Menu._LOAD_, seq: 2).save()
        jpbBp.addToChildren(jpbBpInput)
        jpbBp.addToChildren(jpbBpView)

        def jpbBpTpsLine = Menu.findByMenuCode('JPB_BP_TPS_LINE') ?: new Menu(menuCode: 'JPB_BP_TPS_LINE', parent: scheduleBoard, label: 'JPB BP TPS LINE', details: 'JPB BP TPS LINE', seq: 4).save()
        def jpbBpTpsLineInput = Menu.findByMenuCode('JPB_BP_TPS_LINE_INPUT') ?: new Menu(menuCode: 'JPB_BP_TPS_LINE_INPUT', label: 'Input JPB BP TPS LINE', details: 'Input JPB BP TPS LINE', parent: jpbBpTpsLine, linkController: 'JPBTPSLineInput', targetMode: Menu._LOAD_, seq: 1).save()
        def jpbBpTpsLineView = Menu.findByMenuCode('JPB_BP_TPS_LINE_VIEW') ?: new Menu(menuCode: 'JPB_BP_TPS_LINE_VIEW', label: 'View JPB TPS LINE BP', details: 'View JPB TPS LINE BP', parent: jpbBpTpsLine, linkController: 'JPBTPSLineView', targetMode: Menu._LOAD_, seq: 2).save()
        jpbBpTpsLine.addToChildren(jpbBpTpsLineInput)
        jpbBpTpsLine.addToChildren(jpbBpTpsLineView)

        def jpbBpDriverBp = Menu.findByMenuCode('JPB_BP_DRIVER_BP') ?: new Menu(menuCode: 'JPB_BP_DRIVER_BP', parent: scheduleBoard, label: 'JPB BP DRIVER BP', details: 'JPB BP DRIVER BP', seq: 5).save()
        def jpbBpDriverBpInput = Menu.findByMenuCode('JPB_BP_DRIVER_BP_INPUT') ?: new Menu(menuCode: 'JPB_BP_DRIVER_BP_INPUT', label: 'Input JPB BP DRIVER BP', details: 'Input JPB BP DRIVER BP', parent: jpbBpDriverBp, linkController: 'JPBDriverBPInput', targetMode: Menu._LOAD_, seq: 1).save()
        def jpbBpDriverBpView = Menu.findByMenuCode('JPB_BP_DRIVER_BP_VIEW') ?: new Menu(menuCode: 'JPB_BP_DRIVER_BP_VIEW', label: 'View JPB BP DRIVER BP', details: 'View JPB BP DRIVER BP', parent: jpbBpDriverBp, linkController: 'JPBDriverBPView', targetMode: Menu._LOAD_, seq: 2).save()
        jpbBpDriverBp.addToChildren(jpbBpDriverBpInput)
        jpbBpDriverBp.addToChildren(jpbBpDriverBpView)

        def todayAppointmentBoard = Menu.findByMenuCode('TODAY_APPOINTMENT_BOARD') ?: new Menu(menuCode: 'TODAY_APPOINTMENT_BOARD', label: 'Today Appointment Board', details: 'Today Appointment Board', parent: scheduleBoard, linkController: 'TodayAppointmentBoard', targetMode: Menu._LOAD_, seq: 6)
        todayAppointmentBoard.save()
        def asbToJpb = Menu.findByMenuCode('ASB_TO_JPB') ?: new Menu(menuCode: 'ASB_TO_JPB', label: 'ASB To JPB', details: 'ASB To JPB', parent: scheduleBoard, linkController: 'ASBToJPB', targetMode: Menu._LOAD_, seq: 7)
        asbToJpb.save()
        def vehicleTrackingStatusBoard = Menu.findByMenuCode('VEHICLE_TRACKING_STATUS_BOARD') ?: new Menu(menuCode: 'VEHICLE_TRACKING_STATUS_BOARD', label: 'Vehicle Tracking Status', details: 'Vehicle Tracking Status', parent: scheduleBoard, linkController: 'vehicleTrackingStatusBoard', targetMode: Menu._LOAD_, seq: 9)
        vehicleTrackingStatusBoard.save()
        def mechanicBoard = Menu.findByMenuCode('MECHANIC_BOARD') ?: new Menu(menuCode: 'MECHANIC_BOARD', label: 'Mechanic Board', details: 'Mechanic Board', parent: scheduleBoard, linkController: 'mechanicBoard', targetMode: Menu._LOAD_, seq: 8)
        mechanicBoard.save()
    }

    def bootsrapProductionMenu(){
        def production = Menu.findByMenuCode('PRODUCTION') ?: new Menu(menuCode: 'PRODUCTION', label: 'Production', details: 'Production', seq: 11)
        production.save()

        def absensiTeknisi = Menu.findByMenuCode('absensiTeknisi') ?: new Menu(menuCode: 'absensiTeknisi', label: 'Absensi Teknisi', details: 'Absensi Teknisi', parent: production, linkController: 'absensiTeknisi', targetMode: Menu._LOAD_, seq: 1).save()
        def searchTeknisi = Menu.findByMenuCode('SEARCHTEKNISI') ?: new Menu(menuCode: 'SEARCHTEKNISI', label: 'Search Teknisi', details: 'Search Teknisi', parent: production, linkController: 'searchTeknisi', targetMode: Menu._LOAD_, seq: 2).save()
//        def respondProblemFindingList = Menu.findByMenuCode('respondProblemFindingList') ?: new Menu(menuCode: 'respondProblemFindingList', label: 'Respond Problem Finding List', details: 'Respond Problem Finding List', parent: production, linkController: 'responProblemFinding', targetMode: Menu._LOAD_, seq: 3).save()
        def statusForeman = Menu.findByMenuCode('STATUSFOREMAN') ?: new Menu(menuCode: 'STATUSFOREMAN', label: 'Status Foreman', details: 'Status Foreman', parent: production, linkController: 'foreman', targetMode: Menu._LOAD_, seq: 4).save()
        def jobForTeknisi = Menu.findByMenuCode('jobForTeknisi') ?: new Menu(menuCode: 'jobForTeknisi', label: 'Job For Teknisi', details: 'Job For Teknisi', parent: production, linkController: 'jobForTeknisi', targetMode: Menu._LOAD_, seq: 5).save()
//        def responprob = Menu.findByMenuCode('ResponProblemFinding') ?: new Menu(menuCode: 'ResponProblemFinding', label: 'Respon Problem Finding List', details: 'Job For Teknisi', parent: production, linkController: 'jobForTeknisi', targetMode: Menu._LOAD_, seq: 5).save()

        def bodyPaint = Menu.findByMenuCode('BODYPAINT') ?: new Menu(menuCode: 'BODYPAINT', label: 'Body & Paint', details: 'Body & Paint', parent: production , targetMode: Menu._LOAD_, seq: 6).save()
            def jobInstruction = Menu.findByMenuCode('JOBINSTRUCTION') ?: new Menu(menuCode: 'JOBINSTRUCTION', label: 'Job Instruction', details: 'Job Instruction', parent: bodyPaint, linkController: 'BPJobInstruction', targetMode: Menu._LOAD_, seq: 1).save()
            def woDetailBP = Menu.findByMenuCode('WODETAIL') ?: new Menu(menuCode: 'WODETAIL', label: 'WO Detail', details: 'WO Detail', parent: bodyPaint, linkController: 'BPWODetail', targetMode: Menu._LOAD_, seq: 2).save()
            def problemFinding = Menu.findByMenuCode('PROBLEMFINDING') ?: new Menu(menuCode: 'PROBLEMFINDING', label: 'Problem Finding List', details: 'Problem Finding List', parent: bodyPaint, linkController: 'BPProblemFindingList', targetMode: Menu._LOAD_, seq: 3).save()
            def qualityControl = Menu.findByMenuCode('QUALITYCONTROL') ?: new Menu(menuCode: 'QUALITYCONTROL', label: 'Quality Control & Inspection List', details: 'Quality Control & Inspection List', parent: bodyPaint, linkController: 'BPQualityControl', targetMode: Menu._LOAD_, seq: 4).save()

        def generalRepair = Menu.findByMenuCode('generalRepair') ?: new Menu(menuCode: 'generalRepair', parent: production, label: 'General Repair', details: 'generalRepair', seq: 7).save()
            def jobIntruction = Menu.findByMenuCode('jobIntruction') ?: new Menu(menuCode: 'jobIntruction', label: 'Job Instruction', details: 'Job Instruction', parent: generalRepair, linkController: 'jobInstructionGr', targetMode: Menu._LOAD_, seq: 1).save()
            def woDetail = Menu.findByMenuCode('woDetail') ?: new Menu(menuCode: 'woDetail', label: 'WO Details', details: 'WO Details', parent: generalRepair, linkController: 'woDetailGr', targetMode: Menu._LOAD_, seq: 2).save()
            def problemFindingList = Menu.findByMenuCode('problemFindingList') ?: new Menu(menuCode: 'problemFindingList', label: 'Problem Finding List', details: 'Problem Finding List', parent: generalRepair, linkController: 'problemFindingGr', targetMode: Menu._LOAD_, seq: 3).save()
            def idril = Menu.findByMenuCode('idril') ?: new Menu(menuCode: 'idril', label: 'Inspection During Repair & Inspection List', details: 'Inspection During Repair & Inspection List', parent: generalRepair, linkController: 'inspectionDuringGr', targetMode: Menu._LOAD_, seq: 4).save()
            //def teknisi = Menu.findByMenuCode('teknisi') ?: new Menu(menuCode: 'teknisi', label: 'Teknisi', details: 'Teknisi', parent: generalRepair, linkController: 'teknisiGr', targetMode: Menu._LOAD_, seq: 5).save()


        bodyPaint.addToChildren(jobInstruction)
        bodyPaint.addToChildren(woDetailBP)
        bodyPaint.addToChildren(problemFinding)
        bodyPaint.addToChildren(qualityControl)

        generalRepair.addToChildren(jobIntruction)
        generalRepair.addToChildren(woDetail)
        generalRepair.addToChildren(problemFindingList)
        generalRepair.addToChildren(idril)
        //generalRepair.addToChildren(teknisi)

    }

    def bootstrapFinanceMenu()
    {
        def financeMenu = Menu.findByMenuCode('FINANCE') ?: new Menu(menuCode: 'FINANCE', label: 'Finance & Accounting', details: 'Finance & Accounting', seq: 19).save()

        def masterFinance = Menu.findByMenuCode('MASTERFINANCE') ?: new Menu(menuCode: 'MASTERFINANCE', label: 'Master', details: 'Master', seq: 1, parent: financeMenu).save()
        def accountNumberMenu = Menu.findByMenuCode('ACCOUNTNUMBER') ?: new Menu(menuCode: 'ACCOUNTNUMBER', parent: masterFinance, label: 'Account Number', details: 'Account Number', linkController: 'accountNumber', targetMode: Menu._LOAD_, seq: 2).save()
        def accountTypeMenu = Menu.findByMenuCode('ACCOUNTTYPE') ?: new Menu(menuCode: 'ACCOUNTTYPE', parent: masterFinance, label: 'Account Type', details: 'Account Type', linkController: 'accountType', targetMode: Menu._LOAD_, seq: 1).save()
        def assetStatusMenu = Menu.findByMenuCode('ASSETSTATUS') ?: new Menu(menuCode: 'ASSETSTATUS', parent: masterFinance, label: 'Asset Status', details: 'Asset Status', linkController: 'assetStatus', targetMode: Menu._LOAD_, seq: 3).save()
        def assetTypeMenu = Menu.findByMenuCode('ASSETTYPE') ?: new Menu(menuCode: 'ASSETTYPE', parent: masterFinance, label: 'Asset Type', details: 'Asset Type', linkController: 'assetType', targetMode: Menu._LOAD_, seq: 4).save()
        def bankAccountNumberMenu = Menu.findByMenuCode('BANKACCOUNTNUMBER') ?: new Menu(menuCode: 'BANKACCOUNTNUMBER', parent: masterFinance, label: 'Bank Account Number', details: 'Bank Account Number', linkController: 'bankAccountNumber', targetMode: Menu._LOAD_, seq: 5).save()
        def documentCategoryMenu = Menu.findByMenuCode('DOCUMENTCATEGORY') ?: new Menu(menuCode: 'DOCUMENTCATEGORY', parent: masterFinance, label: 'Document Category', details: 'Document Category', linkController: 'documentCategory', targetMode: Menu._LOAD_, seq: 6).save()
        def journalTypeMenu = Menu.findByMenuCode('JOURNALTYPE') ?: new Menu(menuCode: 'JOURNALTYPE', parent: masterFinance, label: 'Journal Type', details: 'Journal Type', linkController: 'journalType', targetMode: Menu._LOAD_, seq: 7).save()
        def pecahanUangMenu = Menu.findByMenuCode('PECAHANUANG') ?: new Menu(menuCode: 'PECAHANUANG', parent: masterFinance, label: 'Pecahan Uang', details: 'Pecahan Uang', linkController: 'pecahanUang', targetMode: Menu._LOAD_, seq: 8).save()
        def subTypeMenu = Menu.findByMenuCode('SUBTYPE') ?: new Menu(menuCode: 'SUBTYPE', parent: masterFinance, label: 'Sub Type', details: 'Sub Type', linkController: 'subType', targetMode: Menu._LOAD_, seq: 9).save()

        def transactionTypeMenu = Menu.findByMenuCode('TRANSACTIONTYPE') ?: new Menu(menuCode: 'TRANSACTIONTYPE', parent: masterFinance, label: 'Transaction Type', details: 'Transaction Type', linkController: 'transactionType', targetMode: Menu._LOAD_, seq: 10).save()
        /*def journalMenu = Menu.findByMenuCode('JOURNAL') ?: new Menu(menuCode: 'JOURNAL', parent: masterFinance, label: 'Journal', details: 'Journal', linkController: 'journal', targetMode: Menu._LOAD_, seq: 11).save()*/

        def keuangan = Menu.findByMenuCode('KEUANGAN') ?: new Menu(menuCode: 'KEUANGAN', label: 'Keuangan', details: 'Keuangan', seq: 3, parent: financeMenu).save()
        def rekonsiliasiBank = Menu.findByMenuCode('REKONSILIASIBANK') ?: new Menu(menuCode: 'REKONSILIASIBANK', parent: keuangan, label: 'Rekonsiliasi Bank', details: 'Rekonsiliasi Bank', linkController: 'bankReconciliation', targetMode: Menu._LOAD_, seq: 1).save()
        def daftarPiutang = Menu.findByMenuCode('DAFTARPIUTANG') ?: new Menu(menuCode: 'DAFTARPIUTANG', parent: keuangan, label: 'Daftar Piutang', details: 'Daftar Piutang', linkController: 'collection', targetMode: Menu._LOAD_, seq: 2).save()
        def daftarPiutangUpdate = Menu.findByMenuCode('DAFTARPIUTANGUPDATE') ?: new Menu(menuCode: 'DAFTARPIUTANGUPDATE', parent: keuangan, label: 'Daftar Piutang - Update', details: 'Daftar Piutang - Update', linkController: 'collection/forUpdate', targetMode: Menu._LOAD_, seq: 3).save()
        def daftarAsset = Menu.findByMenuCode('DAFTARASSET') ?: new Menu(menuCode: 'DAFTARASSET', parent: keuangan, label: 'Daftar Asset', details: 'Daftar Asset', linkController: 'asset', targetMode: Menu._LOAD_, seq: 4).save()

        keuangan.addToChildren(rekonsiliasiBank).save(flush: true, failOnError: true)
        keuangan.addToChildren(daftarPiutang)
        keuangan.addToChildren(daftarPiutangUpdate)
        keuangan.addToChildren(daftarAsset)

        masterFinance.addToChildren(accountNumberMenu)
        masterFinance.addToChildren(accountTypeMenu)
        masterFinance.addToChildren(assetTypeMenu)
        masterFinance.addToChildren(assetStatusMenu)
        masterFinance.addToChildren(bankAccountNumberMenu)
        masterFinance.addToChildren(documentCategoryMenu)
        masterFinance.addToChildren(journalTypeMenu)
        masterFinance.addToChildren(pecahanUangMenu)
        masterFinance.addToChildren(subTypeMenu)
        masterFinance.addToChildren(transactionTypeMenu)
        //masterFinance.addToChildren(journalMenu)
        masterFinance.save(flush: true, failOnError: true)


        def kasirMenu = Menu.findByMenuCode('KASIR') ?: new Menu(menuCode: 'KASIR', label: 'Kasir', details: 'Kasir', seq: 3, parent: financeMenu).save()
        def kasirCariTransaksi = Menu.findByMenuCode('CARITRANSAKSI') ?: new Menu(menuCode: 'CARITRANSAKSI', parent: kasirMenu, label: 'Cari Transaksi', details: 'Cari Transaksi', linkController: 'transaction/cariTransaksi', targetMode: Menu._LOAD_, seq: 1).save()
        /*def kasirPenerimaanHasilUangMukaService = Menu.findByMenuCode('PENERIMAANHASILUANGMUKASERVICE') ?: new Menu(menuCode: 'PENERIMAANHASILUANGMUKASERVICE', parent: kasirMenu, label: 'Penerimaan Hasil Uang Muka Service', details: 'Penerimaan Hasil Uang Muka Service', linkController: '#', targetMode: Menu._LOAD_, seq: 2).save()*/
        def kasirHasilUangMukaPenjualanBarang = Menu.findByMenuCode('HASILUANGMUKAPENJUALANBARANG') ?: new Menu(menuCode: 'HASILUANGMUKAPENJUALANBARANG', parent: kasirMenu, label: 'Hasil Uang Muka Penjualan Barang', details: 'Hasil Uang Muka Penjualan Barang', linkController: '#', targetMode: Menu._LOAD_, seq: 3).save()

        def kasirHasilTagihanServiceInvoice = Menu.findByMenuCode('HASILTAGIHANSERVICEINVOICE') ?: new Menu(menuCode: 'HASILTAGIHANSERVICEINVOICE', parent: kasirMenu, label: 'Penerimaan Hasil Tagihan Service Invoice', details: 'Penerimaan Hasil Tagihan Service Invoice', linkController: 'transaction/penerimaanHasilTagihanServiceInvoice', targetMode: Menu._LOAD_, seq: 5).save()
        def kasirPenerimaanLainLain = Menu.findByMenuCode('PENERIMAANLAINLAIN') ?: new Menu(menuCode: 'PENERIMAANLAINLAIN', parent: kasirMenu, label: 'Penerimaan Lain Lain', details: 'Penerimaan Lain Lain', linkController: 'penerimaanLainLain', targetMode: Menu._LOAD_, seq: 6).save()
        def kasirPengeluaranPembayaranHutang = Menu.findByMenuCode('PENGELUARANPEMBAYARANHUTANG') ?: new Menu(menuCode: 'PENGELUARANPEMBAYARANHUTANG', parent: kasirMenu, label: 'Pengeluaran Pembayaran Hutang / Kuitansi', details: 'Pengeluaran Pembayaran Hutang / Kuitansi', linkController: 'transaction/pembayaranHutang', targetMode: Menu._LOAD_, seq: 7).save()
        def kasirPengeluaranPembayaranSublet = Menu.findByMenuCode('PENGELUARANPEMBAYARANSUBLET') ?: new Menu(menuCode: 'PENGELUARANPEMBAYARANSUBLET', parent: kasirMenu, label: 'Pengeluaran Pembayaran Sublet / ', details: 'Pengeluaran Pembayaran Sublet', linkController: 'transaction/pembayaranSublet', targetMode: Menu._LOAD_, seq: 8).save()
        def kasirPengeluaranLainLain = Menu.findByMenuCode('PENGELUARANLAINLAIN') ?: new Menu(menuCode: 'PENGELUARANLAINLAIN', parent: kasirMenu, label: 'Pengeluaran Lain-lain ', details: 'Pengeluaran Lain-lain', linkController: 'pengeluaranLainLain', targetMode: Menu._LOAD_, seq: 9).save()
        def kasirSaldoKasir = Menu.findByMenuCode('SALDOKASIR') ?: new Menu(menuCode: 'SALDOKASIR', parent: kasirMenu, label: 'Saldo Kasir ', details: 'Saldo Kasir', linkController: 'cashBalance', targetMode: Menu._LOAD_, seq: 10).save()


        kasirMenu.addToChildren(kasirCariTransaksi)
        /*kasirMenu.addToChildren(kasirPenerimaanHasilUangMukaService)*/
        kasirMenu.addToChildren(kasirHasilUangMukaPenjualanBarang)

        kasirMenu.addToChildren(kasirHasilTagihanServiceInvoice)
        kasirMenu.addToChildren(kasirPenerimaanLainLain)
        kasirMenu.addToChildren(kasirPengeluaranPembayaranHutang)
        kasirMenu.addToChildren(kasirPengeluaranPembayaranSublet)
        kasirMenu.addToChildren(kasirPengeluaranLainLain)
        kasirMenu.addToChildren(kasirSaldoKasir)

        kasirMenu.save(failOnError: true)

        def pembukuanMenu = Menu.findByMenuCode('PEMBUKUAN') ?: new Menu(menuCode: 'PEMBUKUAN', label: 'Pembukuan', details: 'Pembukuan', seq: 4, parent: financeMenu).save()
        def pembukuanJurnalMemorial = Menu.findByMenuCode('JURNALMEMORIAL') ?: new Menu(menuCode: 'JURNALMEMORIAL', parent: pembukuanMenu, label: 'Jurnal Memorial', details: 'Jurnal Memorial', linkController: 'journal', targetMode: Menu._LOAD_, seq: 1).save()
        def pembukuanJurnalTransaksi = Menu.findByMenuCode('JURNALTRANSAKSI') ?: new Menu(menuCode: 'JURNALTRANSAKSI', parent: pembukuanMenu, label: 'Jurnal Transaksi', details: 'Jurnal Transaksi', linkController: 'journal/transactionList', targetMode: Menu._LOAD_, seq: 2).save()
        def pembukuanBukuBesar = Menu.findByMenuCode('BUKUBESAR') ?: new Menu(menuCode: 'BUKUBESAR', parent: pembukuanMenu, label: 'Buku Besar', details: 'Buku Besar', linkController: 'journal/bukuBesar', targetMode: Menu._LOAD_, seq: 3).save()
        def pembukuanJurnalMapping = Menu.findByMenuCode('MAPPINGJOURNAL') ?: new Menu(menuCode: 'MAPPINGJOURNAL', parent: pembukuanMenu, label: 'Mapping Journal', details: 'Mapping Journal', linkController: 'mappingJournal', targetMode: Menu._LOAD_, seq: 4).save()
        def pembukuanJurnalMappingDetail = Menu.findByMenuCode('MAPPINGJOURNALDETAIL') ?: new Menu(menuCode: 'MAPPINGJOURNALDETAIL', parent: pembukuanMenu, label: 'Mapping Journal Detail', details: 'Mapping Journa Detail', linkController: 'mappingJournalDetail', targetMode: Menu._LOAD_, seq: 5).save()
        def pembukuanJurnalDetail = Menu.findByMenuCode('JOURNALDETAIL') ?: new Menu(menuCode: 'JOURNALDETAIL', parent: pembukuanMenu, label: 'Journal Detail', details: 'Journal Detail', linkController: 'journalDetail', targetMode: Menu._LOAD_, seq: 6).save()
        def pembukuanJurnalApproval = Menu.findByMenuCode('JOURNALAPPROVAL') ?: new Menu(menuCode: 'JOURNALAPPROVAL', parent: pembukuanMenu, label: 'Journal Approval', details: 'Journal Approval', linkController: 'journal/approval', targetMode: Menu._LOAD_, seq: 7).save()

        pembukuanMenu.addToChildren(pembukuanJurnalMemorial)
        pembukuanMenu.addToChildren(pembukuanJurnalTransaksi)
        pembukuanMenu.addToChildren(pembukuanBukuBesar)
        pembukuanMenu.addToChildren(pembukuanJurnalMapping)
        pembukuanMenu.addToChildren(pembukuanJurnalMappingDetail)
        pembukuanMenu.addToChildren(pembukuanJurnalDetail)
        pembukuanMenu.addToChildren(pembukuanJurnalApproval)
        pembukuanMenu.save(failOnError: true)

        def laporanKeuanganMenu = Menu.findByMenuCode('LAPORANKEUANGAN') ?: new Menu(menuCode: 'LAPORANKEUANGAN', label: 'Laporan Keuangan', details: 'Laporan Keuangan', seq: 5, parent: financeMenu).save()
        def laporanKeuanganNeraca = Menu.findByMenuCode('NERACA') ?: new Menu(menuCode: 'NERACA', parent: laporanKeuanganMenu, label: 'Neraca', details: 'Neraca', linkController: '#', targetMode: Menu._LOAD_, seq: 1).save()
        def laporanKeuanganRugiLaba = Menu.findByMenuCode('RUGILABA') ?: new Menu(menuCode: 'RUGILABA', parent: laporanKeuanganMenu, label: 'Rugi Laba', details: 'Rugi Laba', linkController: '#', targetMode: Menu._LOAD_, seq: 2).save()

        laporanKeuanganMenu.addToChildren(laporanKeuanganNeraca)
        laporanKeuanganMenu.addToChildren(laporanKeuanganRugiLaba)
        laporanKeuanganMenu.save(failOnError: true)

        financeMenu.addToChildren(masterFinance)
        financeMenu.addToChildren(kasirMenu)
        financeMenu.addToChildren(keuangan)
        financeMenu.addToChildren(pembukuanMenu)
        financeMenu.addToChildren(laporanKeuanganMenu)


        financeMenu.save(flush: true, failOnError: true)
    }
    def bootstrapCustomerFollowUpMenu() {
        def customerFollowUp = Menu.findByMenuCode('CUSTOMER_FOLLOW_UP') ?: new Menu(menuCode: 'CUSTOMER_FOLLOW_UP', label: 'Customer Follow Up', details: 'Customer Follow Up', seq: 14)
        customerFollowUp.save()

        def followUpByPhone = Menu.findByMenuCode('FOLLOWUPBYPHONE') ?: new Menu(menuCode: 'FOLLOWUPBYPHONE', label: 'Jobs Follow Up By Phone', details: 'Jobs Follow Up By Phone', parent: customerFollowUp, linkController: 'followUpByPhone', targetMode: Menu._LOAD_, seq: 1).save()
        def pertanyaanFu = Menu.findByMenuCode('PERTANYAANFU') ?: new Menu(menuCode: 'PERTANYAANFU', label: 'Master Pertanyaan FIR', details: 'Master Pertanyaan FIR', parent: customerFollowUp, linkController: 'pertanyaanFu', targetMode: Menu._LOAD_, seq: 2).save()
        def followUp = Menu.findByMenuCode('FOLLOWUP') ?: new Menu(menuCode: 'FOLLOWUP', label: 'Follow Up', details: 'Follow Up', parent: customerFollowUp, linkController: 'followUp', targetMode: Menu._LOAD_, seq: 3).save()
        def serviceHistory = Menu.findByMenuCode('SERVICEHISTORY') ?: new Menu(menuCode: 'SERVICEHISTORY', label: 'Service History', details: 'Service History', parent: customerFollowUp, linkController: 'serviceHistory', targetMode: Menu._LOAD_, seq: 4).save()
        def smsHistory = Menu.findByMenuCode('SMSHISTORY') ?: new Menu(menuCode: 'SMSHISTORY', label: 'SMS History', details: 'SMS History', parent: customerFollowUp, linkController: 'smsHistory', targetMode: Menu._LOAD_, seq: 5).save()
        def callHistory = Menu.findByMenuCode('CALLHISTORY') ?: new Menu(menuCode: 'CALLHISTORY', label: 'Call History', details: 'Call History', parent: customerFollowUp, linkController: 'callHistory', targetMode: Menu._LOAD_, seq: 6).save()
        def smsTerkirim = Menu.findByMenuCode('SMSTERKIRIM') ?: new Menu(menuCode: 'SMSTERKIRIM', label: 'SMS Terkirim', details: 'SMS Terkirim', parent: customerFollowUp, linkController: 'smsTerkirim', targetMode: Menu._LOAD_, seq: 7).save()
        def counterMeasure = Menu.findByMenuCode('COUNTERMEASURE') ?: new Menu(menuCode: 'COUNTERMEASURE', label: 'Counter Measure', details: 'Counter Measure', parent: customerFollowUp, linkController: 'counterMeasure', targetMode: Menu._LOAD_, seq: 8).save()
        def jobSuggestionInformation = Menu.findByMenuCode('JOBSUGGESTIONINFORMATION') ?: new Menu(menuCode: 'JOBSUGGESTIONINFORMATION', label: 'Job Suggestion Information', details: 'Job Suggestion Information', parent: customerFollowUp, linkController: 'jobSuggestion', targetMode: Menu._LOAD_, seq: 9).save()
    }
    def bootstrapHrdMenu() {
        def hrdMenu = Menu.findByMenuCode('HRD') ?: new Menu(menuCode: 'HRD', label: 'HRD', details: 'HRD', seq: 20).save()
        def masterHrd = Menu.findByMenuCode('MASTERHRD') ?: new Menu(menuCode: 'MASTERHRD', label: 'Master', details: 'Master', seq: 1, parent: hrdMenu).save()
        def pendidikan = Menu.findByMenuCode('PENDIDIKANHRD') ?: new Menu(menuCode: 'PENDIDIKANHRD', parent: masterHrd, label: 'Pendidikan', details: 'Pendidikan', linkController: 'pendidikan', targetMode: Menu._LOAD_, seq: 1).save()
        def divisi = Menu.findByMenuCode('DIVISIHRD') ?: new Menu(menuCode: 'DIVISIHRD', parent: masterHrd, label: 'Divisi', details: 'Divisi', linkController: 'divisiHRD', targetMode: Menu._LOAD_, seq: 3).save()
        def staKaryawan = Menu.findByMenuCode('STATUSKARYAWANHRD') ?: new Menu(menuCode: 'STATUSKARYAWANHRD', parent: masterHrd, label: 'Status Karyawan', details: 'Status Karyawan', linkController: 'statusKaryawan', targetMode: Menu._LOAD_, seq: 4).save()
        def warning = Menu.findByMenuCode('WARNINGHRD') ?: new Menu(menuCode: 'WARNINGHRD', parent: masterHrd, label: 'Warning', details: 'Warning', linkController: 'warningKaryawan', targetMode: Menu._LOAD_, seq: 5).save()
        def reward = Menu.findByMenuCode('REWARDHRD') ?: new Menu(menuCode: 'REWARDHRD', parent: masterHrd, label: 'Reward', details: 'Reward', linkController: 'rewardKaryawan', targetMode: Menu._LOAD_, seq: 6).save()
        def historiMaster = Menu.findByMenuCode('MHISTORIHRD') ?: new Menu(menuCode: 'MHISTORIHRD', parent: masterHrd, label: 'Histori', details: 'Histori', linkController: 'history', targetMode: Menu._LOAD_, seq: 9).save()
        def cutiMaster = Menu.findByMenuCode('MCUTIHRD') ?: new Menu(menuCode: 'MCUTIHRD', parent: masterHrd, label: 'Cuti Karyawan', details: 'Cuti Karyawan', linkController: 'cuti', targetMode: Menu._LOAD_, seq: 7).save()

        masterHrd.addToChildren(pendidikan)
        masterHrd.addToChildren(divisi)
        masterHrd.addToChildren(staKaryawan)
        masterHrd.addToChildren(warning)
        masterHrd.addToChildren(reward)
        masterHrd.addToChildren(cutiMaster)
        masterHrd.addToChildren(historiMaster)
        masterHrd.save(flush: true, failOnError: true)

        def karyawan = Menu.findByMenuCode('KARYAWANHRD') ?: new Menu(menuCode: 'KARYAWANHRD', label: 'Karyawan', details: 'Karyawan', seq: 2, parent: hrdMenu).save()
        def dataKaryawan =  Menu.findByMenuCode('DATAKARYAWANHRD') ?: new Menu(menuCode: 'DATAKARYAWANHRD', parent: karyawan, label: 'Data Karyawan', details: 'Data Karyawan', linkController: 'karyawan', targetMode: Menu._LOAD_, seq: 1).save()
        def historiKaryawan =  Menu.findByMenuCode('HISTORIKARYAWANHRD') ?: new Menu(menuCode: 'HISTORIKARYAWANHRD', parent: karyawan, label: 'Histori Karyawan', details: 'Histori Karyawan', linkController: 'historyKaryawan', targetMode: Menu._LOAD_, seq: 2).save()
        def rewardKaryawan =  Menu.findByMenuCode('REWARDKARYAWANHRD') ?: new Menu(menuCode: 'REWARDKARYAWANHRD', parent: karyawan, label: 'Reward Karyawan', details: 'Reward Karyawan', linkController: 'historyRewardKaryawan', targetMode: Menu._LOAD_, seq: 3).save()
        def warningKaryawan =  Menu.findByMenuCode('WARNINGKARYAWANHRD') ?: new Menu(menuCode: 'WARNINGKARYAWANHRD', parent: karyawan, label: 'Warning Karyawan', details: 'Warning Karyawan', linkController: 'historyWarningKaryawan', targetMode: Menu._LOAD_, seq: 4).save()
        def absensiKaryawan =  Menu.findByMenuCode('ABSENSIKARYAWANHRD') ?: new Menu(menuCode: 'ABSENSIKARYAWANHRD', parent: karyawan, label: 'Absensi Karyawan', details: 'Absensi Karyawan', linkController: 'absensiKaryawan', targetMode: Menu._LOAD_, seq: 5).save()
        def tugasLuar =  Menu.findByMenuCode('TUGASLUARHRD') ?: new Menu(menuCode: 'TUGASLUARHRD', parent: karyawan, label: 'Tugas Luar', details: 'Tugas Luar', linkController: 'tugasLuarKaryawan', targetMode: Menu._LOAD_, seq: 6).save()
        def cutiKaryawan =  Menu.findByMenuCode('CUTIKARYAWANHRD') ?: new Menu(menuCode: 'CUTIKARYAWANHRD', parent: karyawan, label: 'Cuti Karyawan', details: 'Cuti Karyawan', linkController: 'cutiKaryawan', targetMode: Menu._LOAD_, seq: 7).save()
        def penilaianKaryawan =  Menu.findByMenuCode('PENILAIANKARYAWANHRD') ?: new Menu(menuCode: 'PENILAIANKARYAWANHRD', parent: karyawan, label: 'Penilaian Karyawan', details: 'Penilaian Karyawan', linkController: 'penilaianKaryawan', targetMode: Menu._LOAD_, seq: 8).save()

        karyawan.addToChildren(dataKaryawan)
        karyawan.addToChildren(historiKaryawan)
        karyawan.addToChildren(rewardKaryawan)
        karyawan.addToChildren(warningKaryawan)
        karyawan.addToChildren(absensiKaryawan)
        karyawan.addToChildren(tugasLuar)
        karyawan.addToChildren(cutiKaryawan)
        karyawan.addToChildren(penilaianKaryawan)
        karyawan.save(flush: true, failOnError: true)

        def training = Menu.findByMenuCode('TRAININGHRD') ?: new Menu(menuCode: 'TRAININGHRD', label: 'Training', details: 'Training', seq: 3, parent: hrdMenu).save()
        def tipeTraining = Menu.findByMenuCode('TIPETRAININGHRD') ?: new Menu(menuCode: 'TIPETRAININGHRD', parent: training, label: 'Tipe Training', details: 'Tipe Training', linkController: 'trainingType', targetMode: Menu._LOAD_, seq: 1).save()
        def tipeSertifikasi = Menu.findByMenuCode('TIPESERTIFIKASIHRD') ?: new Menu(menuCode: 'TIPESERTIFIKASIHRD', parent: training, label: 'Tipe Sertifikasi', details: 'Tipe Sertifikasi', linkController: 'certificationType', targetMode: Menu._LOAD_, seq: 2).save()
        def instrukturTraining = Menu.findByMenuCode('INSTRUKTURTRAININGHRD') ?: new Menu(menuCode: 'INSTRUKTURTRAININGHRD', parent: training, label: 'Instruktur Training', details: 'Instruktur Training', linkController: 'trainingInstructor', targetMode: Menu._LOAD_, seq: 3).save()
        def kelasTraining = Menu.findByMenuCode('KELASTRAININGHRD') ?: new Menu(menuCode: 'KELASTRAININGHRD', parent: training, label: 'Kelas Training', details: 'Kelas Training', linkController: 'trainingClassRoom', targetMode: Menu._LOAD_, seq: 4).save()
        def pesertaTraining = Menu.findByMenuCode('PESERTATRAININGHRD') ?: new Menu(menuCode: 'PESERTATRAININGHRD', parent: training, label: 'Peserta Training', details: 'Peserta Training', linkController: 'trainingClassRoom/pesertaTraining', targetMode: Menu._LOAD_, seq: 5).save()
        training.addToChildren(tipeTraining)
        training.addToChildren(tipeSertifikasi)
        training.addToChildren(instrukturTraining)
        training.addToChildren(kelasTraining)
        training.addToChildren(pesertaTraining)
        training.save(flush: true, failOnError: true)

        def report = Menu.findByMenuCode('REPORT') ?: new Menu(menuCode: 'REPORT', label: 'REPORT', details: 'REPORT',linkController: 'ReportHRD',targetMode: Menu._LOAD_ ,seq: 4, parent: hrdMenu).save()
         hrdMenu.addToChildren(report)
         report.addToChildren(report)
         hrdMenu.save()

        report.save(flush: true, failOnError: true)


    }

    def bootstrapInterface(){
        def interfacing = Menu.findByMenuCode('INTERFACE') ?: new Menu(menuCode: 'INTERFACE', label: 'Interface', details: 'Interface', seq: 16).save()
        def tPoss = Menu.findByMenuCode('TPOSS') ?: new Menu(menuCode: 'TPOSS', label: 'T Poss', details: 'TPOSS', parent: interfacing, url: 'https://portal.oasis.toyota.astra.co.id', targetMode: Menu._NEW_WINDOW_, seq: 1).save()
        def tFirst = Menu.findByMenuCode('TFIRST') ?: new Menu(menuCode: 'TFIRST', label: 'T First', details: 'TFIRST', parent: interfacing, url: 'https://portal.oasis.toyota.astra.co.id', targetMode: Menu._NEW_WINDOW_, seq: 2).save()
        def warranty = Menu.findByMenuCode('WARRANTY') ?: new Menu(menuCode: 'WARRANTY', label: 'Warranty Link', details: 'Warranty Link', parent: interfacing, url: 'https://portal.oasis.toyota.astra.co.id', targetMode: Menu._NEW_WINDOW_, seq: 3).save()
        def claimAsuransi = Menu.findByMenuCode('CLAIMASS') ?: new Menu(menuCode: 'CLAIMASS', label: 'Klaim Asuransi', details: 'Klaim Asuransi', parent: interfacing, url: 'http://indonesia.merimen.com/claims', targetMode: Menu._NEW_WINDOW_, seq: 4).save()

        interfacing.addToChildren(tPoss)
        interfacing.addToChildren(tFirst)
        interfacing.addToChildren(warranty)
        interfacing.addToChildren(claimAsuransi)
        interfacing.save()
    }

    def bootstrapLatihan(){

//        def penjualan = Menu.findByMenuCode('PENJUALAN') ?: new Menu(menuCode: 'PENJUALAN', label: 'HRD', details: 'PENJUALAN', seq: 21).save()
//        //def barang = Menu.findByMenuCode('BARANG') ?: new Menu(menuCode: 'BARANG', label: 'BARANG', details: 'BARANG', seq: 1, parent: Penjualan).save()
//        def barang= Menu.findByMenuCode('BARANG') ?: new Menu(menuCode: 'BARANG', parent: penjualan, label: 'BARANG', details: 'Instruktur Training', linkController: 'Barang', targetMode: Menu._LOAD_, seq: 3).save()
//        penjualan.addToChildren(barang)
//        penjualan.save()
    }
}