package com.kombos.baseapp.approval

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.Approval
import com.kombos.baseapp.ApprovalStatus
import com.kombos.baseapp.menu.Menu
import grails.converters.JSON

// XXX activiti
//import org.activiti.engine.impl.pvm.delegate.ActivityExecution
class ApprovalService {
	def grailsApplication
	
	static genericApprovalRefId = [:]
	static {
		genericApprovalRefId.formulamanagement = 'id'
		genericApprovalRefId.branch = 'id'
		genericApprovalRefId.glbicodemapping = 'id'
		genericApprovalRefId.parameterizedvalue = 'id'
		genericApprovalRefId.commoncode = 'id'
		genericApprovalRefId.commoncodedetail = 'id'
		genericApprovalRefId.grouplocalbranch = 'id'
		genericApprovalRefId.grouplocalbranchdetail = 'id'
		genericApprovalRefId.groupform = 'id'
		genericApprovalRefId.groupformdetail = 'id'
		genericApprovalRefId.role = 'id'
		genericApprovalRefId.user = 'id'
		genericApprovalRefId.rolepermissions = 'id'
		
	}
	
	def reportTo(def username) {
		def ret = []
		def user = com.kombos.baseapp.sec.shiro.User.findByUsername(username)

		def reportTo = user.reportTo
		reportTo.each { 
			if(it.enabled)
				ret << it.username 
		}


		return ret
	}

	def addApproval(Class clazz, def domainInstance, def params){
		def domainClass = grailsApplication.getDomainClass(clazz.name)
		if(domainClass.hasProperty("approval")){
			def converter = new JSON(params)
			def paramsJson = converter.toString()
			def inputObject = JSON.parse(paramsJson)

			Approval approval = new Approval()
			approval.requestOn = new Date()
			def currentUser = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
			approval.requestBy = currentUser.username
			approval.params = paramsJson
			approval.save(flush:true)
			domainInstance.approval = approval
			domainInstance.addToApprovalHistory(approval)
			domainInstance.approvalStatus = approval.approvalStatus
		}
	}

	def createNewApproval(def params){
		def converter = new JSON(params)
		def paramsJson = converter.toString()
		def inputObject = JSON.parse(paramsJson)

		Approval approval = new Approval()
		approval.requestOn = new Date()
		def currentUser = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
		approval.requestBy = currentUser.username
		approval.params = paramsJson
		approval.save(flush:true)
		approval
	}
	
	def createNewApproval(def module, def params){
		def converter = new JSON(params)
		def paramsJson = converter.toString()
		def inputObject = JSON.parse(paramsJson)

		Approval approval = new Approval()
		if(module == 'ADJUST_SOURCE_DATA' || module == 'ADJUST_FORM_DATA' || module == 'DELETE_SOURCE_DATA' || module == 'DELETE_FORM_DATA'){
			approval.refForm = params.id
			approval.refPkid = params.pkid
		} else {
			def pkname = genericApprovalRefId."${params.originController}"
			approval.refPkid = params."${pkname}" 
		}
		
		approval.module = module
		approval.requestOn = new Date()
		def currentUser = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
		approval.requestBy = currentUser.username
		approval.params = paramsJson
		approval.save(flush:true)
		approval
	}

	def needApproval(Class clazz) {
		boolean ret = false
		def domainClass = grailsApplication.getDomainClass(clazz.name)
		if(domainClass.hasProperty("approval")){
			ret = AppSettingParam.valueFor(domainClass.clazz.simpleName.toLowerCase() + "_approval")
		}
		return ret
	}
	
	def pendingApproval(def module, def form, def pkid) {
		def total = Approval.findAllByApprovalStatusAndModuleAndRefFormAndRefPkid(ApprovalStatus.PENDING, module, form, pkid).size()
		return total
	}
	
	def pendingApproval(def module, def pkid) {
		def total = Approval.findAllByApprovalStatusAndModuleLikeAndRefPkid(ApprovalStatus.PENDING, module + "%", pkid).size()
		return total
	}

	def needApproval(String controllerName, String actionName) {
		boolean ret = true

		Menu menu = null

		if(controllerName) {
			if(actionName) {
				menu = Menu.findByLinkControllerAndLinkAction(controllerName, actionName)
			} else {
				menu = Menu.findByLinkController(controllerName)
			}
		}
		if(menu){
			ret = menu.needApproval
		}

		def currentUser = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
		def reportTo = currentUser.reportTo
		def rtSize = reportTo.size()
		if(rtSize == 0){
			ret = false
		} else if(rtSize == 1) {
			reportTo.each {
				if(currentUser.username.equals(it.username)){
					ret = true
				}
			}
		}

		return ret
	}
	
	// XXX activiti

//	def autoApproval(ActivityExecution execution) throws Exception {
//		def variables = execution.getVariables()
//		def domainClass = grailsApplication.getDomainClass(variables.domainClassName);
//		def instance = domainClass.newInstance(variables);
//		instance.save(flush: true);
//	}
}
