package com.kombos.baseapp.approval

import groovy.sql.Sql

import java.text.SimpleDateFormat


class ApprovalTaskService {
	def dataSource
	
	def taskService
	
	def appSettingParamDateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def getList(def params) {
		def ret
		def res = getListFromSQL(params)
		ret = [sEcho:res.sEcho, iTotalRecords:res.iTotalRecords, iTotalDisplayRecords:res.iTotalDisplayRecords, aaData:res.rows]

		ret
	}
    private getListFromSQL(def params) {
		def propertiesToRender = params.sColumns.split(",")

		def filters = []
		def filterParams = [:]
		def x = 0
		def columnFilters = []
		
		columnFilters << "user_id = :currentUser"
		filterParams.currentUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
		def columnFilterSearch = true
		if ( params.sSearch ) {
			filterParams.filter= "%${params.sSearch}%"
		}

		propertiesToRender.each { prop ->
			filters << "${prop} like :filter"
			if(params."sSearch_${x}"){
				columnFilters << "${prop} like :${prop}"
				filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) + "%"
				columnFilterSearch = true
			}
			x++
		}
		def filter = filters.join(" OR ")
		def columnFilter = columnFilters.join(" AND ")


		def dataToRender = [:]
		dataToRender.sEcho = params.sEcho
		dataToRender.aaData=[]
		

		def queryTotal = new StringBuilder("""\
		select count(*) as total from dom_shirouser_dom_shirouser reportto inner join (select t.id_ as taskid, t.name_ as taskname, t.create_time_ as requeston,
max(case when v.name_ = 'approvalId' then v.text_ end) as approvalid,
max(case when v.name_ = 'activitiUsername' then v.text_ end) as requestby
from act_ru_variable v inner join act_ru_task t on t.execution_id_ = v.execution_id_ 
having max(case when v.name_ = 'approvalId' then v.text_ end) is not null and 
max(case when v.name_ = 'activitiUsername' then v.text_ end) is not null
group by t.id_, t.name_, t.create_time_)approvaltask on reportto.user_report_to_id = approvaltask.requestby where 1=1 
""")

		def columns = params.sColumns.split(",")

		def query = new StringBuilder("""\
select ${params.sColumns} from (select approvaltask.*, reportto.user_id from dom_shirouser_dom_shirouser reportto inner join (select t.id_ as taskid, t.name_ as taskname, t.create_time_ as requeston,
max(case when v.name_ = 'approvalId' then v.text_ end) as approvalid,
max(case when v.name_ = 'activitiUsername' then v.text_ end) as requestby
from act_ru_variable v inner join act_ru_task t on t.execution_id_ = v.execution_id_ 
having max(case when v.name_ = 'approvalId' then v.text_ end) is not null and 
max(case when v.name_ = 'activitiUsername' then v.text_ end) is not null
group by t.id_, t.name_, t.create_time_)approvaltask on reportto.user_report_to_id = approvaltask.requestby) where 1=1 
""")
		if(params.initsearch){
			query.append(" and ("+params.initquery+")")
			queryTotal.append(" and ("+params.initquery+")")
		}
		if ( params.sSearch ) {
			query.append(" and (${filter})")
			queryTotal.append(" and ("+params.initquery+")")
		}
		if (columnFilterSearch){
			query.append(" and (${columnFilter})")
			queryTotal.append(" and (${columnFilter})")
		}
		def sortingCol =  params.iSortingCols as int
		if(sortingCol > 0){
			query.append(" order by ")

			for (int i = 0; i < sortingCol; i++) {

				def sortDir = params.("sSortDir_"+i)?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
				def sortProperty = columns[params.("iSortCol_"+i) as int]
				query.append("${sortProperty} ${sortDir}")
				if(i < sortingCol - 1){
					query.append(",")
				}
			}
		}

		def rows = []
		
		def sql = new Sql(dataSource)
		if(filterParams.size() > 0){
			def tot = sql.firstRow(queryTotal.toString(), filterParams)
			if(tot){
				dataToRender.iTotalRecords = tot.total;
				dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

				sql.eachRow(query.toString(), filterParams, params.iDisplayStart as int, params.iDisplayLength as int) { 
					def rowData = [:]
					
					propertiesToRender.each { prop ->
						if(it."${prop}" instanceof oracle.sql.TIMESTAMP){
							String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
							rowData."$prop" = new SimpleDateFormat(dateFormat).format(it."$prop".dateValue())
						} else {
							rowData."$prop" = it."$prop"
						}
					}
					
					def variables = taskService.getVariables(rowData.taskid)
					
					if(rowData.taskname == 'review Approval Request'){
						if(variables.originAction == 'save')
							rowData.taskname = "Approval add " + variables.originController.toLowerCase()
						else if(variables.originAction == 'update')
							rowData.taskname = "Approval edit " + variables.originController.toLowerCase()
						else
							rowData.taskname = "Approval delete " + variables.originController.toLowerCase()
					}
					
					rows << rowData 
					
				}
				dataToRender.rows = rows
			} else {
				dataToRender.iTotalRecords = 0;
				dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords
				dataToRender.rows = rows
			}
		} else {
			def tot = sql.firstRow(queryTotal.toString())
			if(tot){
				dataToRender.iTotalRecords = tot.total;
				dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords

				sql.eachRow(query.toString(), params.iDisplayStart as int, params.iDisplayLength as int) { 
					def rowData = [:]
					propertiesToRender.each { prop ->
						rowData."$prop" = it."$prop"
					}
					rows << rowData 
					//println("Row: " + it)
				}
				dataToRender.rows = rows
			} else {
				dataToRender.iTotalRecords = 0;
				dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords
				dataToRender.rows = rows
			}
		}

		return dataToRender
	}
}
