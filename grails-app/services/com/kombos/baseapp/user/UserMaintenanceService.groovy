package com.kombos.baseapp.user

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Divisi
import com.kombos.baseapp.menu.RoleMenu
import com.kombos.baseapp.sec.shiro.PasswordHistory
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User
import com.kombos.baseapp.utils.SecurityChecklistUtilService
import groovy.sql.Sql
import org.activiti.engine.impl.pvm.delegate.ActivityExecution
import org.springframework.context.i18n.LocaleContextHolder as LCH

class UserMaintenanceService {
    def messageSource
    def dataSource
    def appSettingParamDateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def authenticationUtilsService
    def dataTablesGridService

    def serviceMethod() {

    }

    def datatablesUtilService

    def resultmap(def temp, def map){
        map << [
                temp.username,
                temp.username,
                temp.staffNo,
                temp.firstName,
                temp.lastName,
                temp.effectiveStartDate,
                temp.effectiveEndDate,
                temp.roles,
                temp.localgroup,
                temp.groupForm,
                temp.reportTo,
                temp.lastLoggedIn,
                temp.canPasswordExpire,
                temp.accountLocked,
                temp.isActive,
                temp.country
                , temp.createdOn
                , temp.status
        ]
    }

    def resultmapExport(def temp, def map){
        map << [
                "username1":temp.username,
                "username":temp.username,
                "staffno":temp.staffNo,
                "firstname":temp.firstName,
                "lastname":temp.lastName,
                "effectivestartdate":temp.effectiveStartDate,
                "effectiveenddate":temp.effectiveEndDate,
                "roles":temp.roles,
                "localgroup":temp.localgroup,
                "groupform":temp.groupForm,
                "reportto":temp.reportTo,
                "lastloggedin":temp.lastLoggedIn,
                "canpasswordexpire":temp.canPasswordExpire,
                "accountlocked":temp.accountLocked,
                "isactive":temp.isActive,
                "country":temp.country,
                "createdon":temp.createdOn,
                "status":temp.status
        ]
    }

    def getListExport(def params)
    {
        try
        {
            String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
            def ret
            def sql=new Sql(dataSource)
            def query =
                """
			SELECT * FROM (
			WITH USER_ROLES AS(
			   SELECT UR.USER_ID, RTRIM(XMLAGG(XMLELEMENT(R,R.NAME,', ').EXTRACT('//text()')),', ') ROLE
			   FROM   DOM_SHIROROLE R, DOM_SHIROUSER_ROLES UR, DOM_SHIROUSER U
			   WHERE R.ID = UR.ROLE_ID AND U.USERNAME = UR.USER_ID
			   GROUP BY UR.USER_ID
			),
			REPORT_TO AS (
			    SELECT R.USER_REPORT_TO_ID, RTRIM(XMLAGG(XMLELEMENT(R,R.USER_ID,', ').EXTRACT('//text()')),', ') REPORT
			    FROM DOM_SHIROUSER_DOM_SHIROUSER R
			    GROUP BY R.USER_REPORT_TO_ID
			)
			SELECT   U.USERNAME username,
			         U.STAFF_NO staffNo,
			         U.FIRST_NAME firstName,
			         U.LAST_NAME lastName,
			         to_char(U.EFFECTIVE_START_DATE,'${dateFormat}') effectiveStartDate,
			         to_char(U.EFFECTIVE_END_DATE,'${dateFormat}') effectiveEndDate,
			         R.ROLE roles,
			         G.GROUPNAME AS localgroup,
			         F.GROUPNAME AS groupForm,
			         RT.REPORT reportTo,
			         to_char(U.LAST_LOGGED_IN,'${dateFormat} hh:mi:ss') lastLoggedIn,
			         CASE WHEN (U.CAN_PASSWORD_EXPIRE= 1) THEN 'true' ELSE 'false' END canPasswordExpire,
			         CASE WHEN (U.ACCOUNT_LOCKED=1) THEN 'true' ELSE 'false' END accountLocked,
			         CASE WHEN (U.IS_ACTIVE=1) THEN 'true' ELSE 'false' END isActive,
			         U.COUNTRY country
			         , to_char(U.created_on,'${dateFormat} hh:mi:ss') CreatedOn
					 , U.STATUS
			FROM     DOM_SHIROUSER U,
			         USER_ROLES R,
			         REPORT_TO RT,
			         TBLM_GROUPFORMH F,
			         TBLM_GROUPLOCALBRANCHH G
			WHERE   U.GROUP_LOCAL_BRANCH_ID = G.PKID(+)
			AND     U.GROUP_FORM_ID = F.PKID(+)
			AND     R.USER_ID(+) = U.USERNAME
			AND     RT.USER_REPORT_TO_ID(+) = U.USERNAME
            AND     U.ENABLED = 1
			) p WHERE 1=1
		"""
            query = query.replaceAll('\\s+', ' ')
            def columnFilters = []
            def columnFiltersNew=""
            def columnFilterSearch = false
            def filterParams = [:]
            def x = 0
            def sortProperty
            if(params.sColumns)
            {
                def propertiesToRender = params.sColumns.split(",")
                sortProperty = propertiesToRender[params.iSortCol_0 as int]

                propertiesToRender.each { prop ->
                    if(params."sSearch_${x}"){
                        columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                        columnFiltersNew+="UPPER(p.${prop}) like UPPER('%${params."sSearch_${x}"}%') AND "
                        filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) + "%"
                        columnFilterSearch = true
                    }

                    x++
                }
            }
            def rows=[]
            def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
            int total=0

            def columnFilter = columnFilters.join(" AND ")
            if(columnFilterSearch){
                columnFiltersNew=columnFiltersNew.substring(0,columnFiltersNew.length()-4)
//                query += " and (${columnFilter})"
                query += " and (${columnFiltersNew})"
            }
            if(params.sColumns)
            {
                query +=" order by p.${sortProperty} ${sortDir}"
            }
            def countQuery = "select count(*) as total from ("+query+")"
            if (columnFilterSearch){
                sql.eachRow(query,
                        {
                            row->resultmapExport(row,rows)
                        })
//                sql.eachRow(query, (params.iDisplayStart as int) + 1, params.iDisplayLength as int,{row -> resultmapExport(row,rows)})
                total = sql.firstRow(countQuery).total
            }else{
                sql.eachRow(query,
                        {
                            row->resultmapExport(row,rows)
                        })
//                sql.eachRow(query,(params.iDisplayStart as int) + 1, params.iDisplayLength as int,{row -> resultmapExport(row,rows)})
                total = sql.firstRow(countQuery).total
            }
//            if(columnFilterSearch)
//                rows=rows.find {"username='dewi888'"}
            ret = [sEcho:params.sEcho, iTotalRecords:total, iTotalDisplayRecords:total, aaData:rows]
            return ret
        }
        catch(Exception e)
        {
            //println "Could not getListExport: ${e.getMessage()}"
        }
    }

    def getList(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def ret
        def Sql sql = new Sql(dataSource)
        def query =
            """
			SELECT username,
			         staffNo,
			         firstName,
			         lastName,
			         to_char(effectiveStartDate,'${dateFormat}') effectiveStartDate,
			         to_char(effectiveEndDate,'${dateFormat}') effectiveEndDate,
			         roles,
			         localgroup,
			         groupForm,
			         reportTo,
			         to_char(lastLoggedIn,'${dateFormat} hh:mi:ss') lastLoggedIn,
			         canPasswordExpire,
			         accountLocked,
			         isActive,
			         country
			         , to_char(CreatedOn,'${dateFormat} hh:mi:ss') CreatedOn
					, Status
            FROM (
			WITH USER_ROLES AS(
			   SELECT UR.USER_ID, RTRIM(XMLAGG(XMLELEMENT(R,R.NAME,', ').EXTRACT('//text()')),', ') ROLE
			   FROM   DOM_SHIROROLE R, DOM_SHIROUSER_ROLES UR, DOM_SHIROUSER U
			   WHERE R.ID = UR.ROLE_ID AND U.USERNAME = UR.USER_ID
			   GROUP BY UR.USER_ID
			),
			REPORT_TO AS (
			    SELECT R.USER_REPORT_TO_ID, RTRIM(XMLAGG(XMLELEMENT(R,R.USER_ID,', ').EXTRACT('//text()')),', ') REPORT
			    FROM DOM_SHIROUSER_DOM_SHIROUSER R
			    GROUP BY R.USER_REPORT_TO_ID
			)
			SELECT   U.USERNAME username,
			         U.STAFF_NO staffNo,
			         U.FIRST_NAME firstName,
			         U.LAST_NAME lastName,
			         U.EFFECTIVE_START_DATE effectiveStartDate,
			         U.EFFECTIVE_END_DATE effectiveEndDate,
			         R.ROLE roles,
			         G.GROUPNAME AS localgroup,
			         F.GROUPNAME AS groupForm,
			         RT.REPORT reportTo,
			         U.LAST_LOGGED_IN lastLoggedIn,
			         CASE WHEN (U.CAN_PASSWORD_EXPIRE= 1) THEN 'true' ELSE 'false' END canPasswordExpire,
			         CASE WHEN (U.ACCOUNT_LOCKED=1) THEN 'true' ELSE 'false' END accountLocked,
			         CASE WHEN (U.IS_ACTIVE=1) THEN 'true' ELSE 'false' END isActive,
			         U.COUNTRY country
			         , U.created_on CreatedOn
		             , U.status Status
			FROM     DOM_SHIROUSER U,
			         USER_ROLES R,
			         REPORT_TO RT,
			         TBLM_GROUPFORMH F,
			         TBLM_GROUPLOCALBRANCHH G
			WHERE   U.GROUP_LOCAL_BRANCH_ID = G.PKID(+)
			AND     U.GROUP_FORM_ID = F.PKID(+)
			AND     R.USER_ID(+) = U.USERNAME
			AND     RT.USER_REPORT_TO_ID(+) = U.USERNAME
            AND     U.ENABLED = 1
			) p WHERE 1=1
		"""
        query = query.replaceAll('\\s+', ' ')
        def propertiesToRender = params.sColumns.split(",")
        def rows = []
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def filterParams = [:]
        def x = 0
        def columnFilters = []
        def columnFilterSearch = false
        int total

        propertiesToRender.each { prop ->
            if(params."sSearch_${x}"){
                if(params."sFilter_${x}"){
                    switch(params."sFilter_${x}"){
                        case "beginwith":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)+ "%"
                            break;
                        case "contains":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) +"%"
                            break;
                        case "endswith":
                            columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                            filterParams."${prop}" = "%" + (params."sSearch_${x}" as String)
                            break;
                        case "equals":
                            columnFilters << "UPPER(p.${prop}) = UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "doesntequal":
                            columnFilters << "UPPER(p.${prop}) <> UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "islessthan":
                            columnFilters << "UPPER(p.${prop}) < UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "islessthanorequalto":
                            columnFilters << "UPPER(p.${prop}) <= UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "isgreaterthan":
                            columnFilters << "UPPER(p.${prop}) > UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                        case "isgreaterthanorequalto":
                            columnFilters << "UPPER(p.${prop}) >= UPPER(:${prop})"
                            filterParams."${prop}" = (params."sSearch_${x}" as String)
                            break;
                    }

                }else{
                    columnFilters << "UPPER(p.${prop}) like UPPER(:${prop})"
                    filterParams."${prop}" = "%" + (params."sSearch_${x}" as String) + "%"
                }

                columnFilterSearch = true
            }
            x++
        }
        def columnFilter = columnFilters.join(" AND ")
        if(columnFilterSearch){
            query += " and (${columnFilter})"
        }
        query +=" order by p.${sortProperty} ${sortDir}"
        def countQuery = "select count(*) as total from ("+query+")"
        if (columnFilterSearch){
            sql.eachRow(filterParams, query, (params.iDisplayStart as int) + 1, params.iDisplayLength as int,{row -> resultmap(row,rows)})
            total = sql.firstRow(countQuery, filterParams).total
        }else{
            sql.eachRow(query,(params.iDisplayStart as int) + 1, params.iDisplayLength as int,{row -> resultmap(row,rows)})
            total = sql.firstRow(countQuery).total
        }

        ret = [sEcho:params.sEcho, iTotalRecords:total, iTotalDisplayRecords:total, aaData:rows]
        return ret
    }


    def add(params) throws Exception{
        def arrMenu = []
        def user = new User()
        user.username        = params.username
        user.staDel          = '0'
        user.lastUpdProcess  = "INSERT"
        user.createdBy       = params.createdBy
        user.t001Inisial     = params.t001Inisial
        user.t001NamaPegawai = params.t001NamaPegawai
        user.fullname        = params.t001NamaPegawai
        user.t001Email       = params.t001Email
        user.dateCreated     = params.dateCreated
        user.lastUpdated     = params.lastUpdated
        user.companyDealer   = CompanyDealer.findById(params?.companyDealer.id?.toLong())
        user.divisi          = Divisi.findById(params?.divisi.id?.toLong())
        user.passwordHash    = authenticationUtilsService.hashPassword(params.passwordHash)
        user.status          = User.STATUS_ACTIVE
        user.enabled         = true
        user.effectiveStartDate  = params.effectiveStartDate
        if(params.canPasswordExpire){
            user.effectiveEndDate = params.effectiveEndDate
            user.t001ExpiredAccountDate = params.t001ExpiredAccountDate
        }
        user.addToPermissions("*:changePassword,editChangePassword")
//        user.accountExpired  = params.accountExpired?Boolean.valueOf(params.accountExpired):false
//        user.accountLocked   = params.accountLocked?Boolean.valueOf(params.accountLocked):false
//        user.passwordExpired = params.passwordExpired?Boolean.valueOf(params.passwordExpired):false

        SecurityChecklistUtilService sc = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PASSWORD_CYCLES)
        Calendar c = Calendar.getInstance()
        c.setTime(new Date())
        c.add(Calendar.DATE, sc.getValueInteger())

        user.passwordExpiredOn = c.getTime()
//        if(params.effectiveStartDate){
//            if(params.effectiveStartDate instanceof Date){
//                user.effectiveStartDate  = params.effectiveStartDate
//            } else {
//                //            String effectiveStartDate = params.effectiveStartDate_day+"/"+params.effectiveStartDate_month+"/"+params.effectiveStartDate_year
//                String effectiveStartDate = params.effectiveStartDate
//                try{
//                    user.effectiveStartDate = new SimpleDateFormat("dd/MM/yyyy").parse(effectiveStartDate)
//                }catch (Exception e){
//                    throw new Exception(messageSource.getMessage("user.invalid_date.error",[messageSource.getMessage("user.effectiveStartDate.label", null,"Effective Start Date", LCH.getLocale())] as Object[], "Invalid date for "+messageSource.getMessage("user.effectiveStartDate.label", null,"Effective Start Date", LCH.getLocale()), LCH.getLocale()))
//                }
//            }
//        }
//        if(params.effectiveEndDate){
//
//            if(params.effectiveEndDate instanceof Date){
//                user.effectiveEndDate = params.effectiveEndDate
//            } else {
//                //            String effectiveEndDate = params.effectiveEndDate_day+"/"+params.effectiveEndDate_month+"/"+params.effectiveEndDate_year
//                String effectiveEndDate = params.effectiveEndDate
//                try{
//                    user.effectiveEndDate = new SimpleDateFormat("dd/MM/yyyy").parse(effectiveEndDate)
//                }catch (Exception e){
//                    throw new Exception(messageSource.getMessage("user.invalid_date.error",[messageSource.getMessage("user.effectiveEndDate.label", null,"Effective End Date", LCH.getLocale())] as Object[], "Invalid date for "+messageSource.getMessage("user.effectiveEndDate.label", null,"Effective End Date", LCH.getLocale()), LCH.getLocale()))
//                }
//            }
//        }

        if(params.groupFormId){
            user.groupFormId = new Long(params.groupFormId)
        }

        if(params.groupLocalBranchId){
            user.groupLocalBranchId = new Long(params.groupLocalBranchId)
        }

        Set<User> reportToChanged = new ArrayList<User>()
        if(params.reportTo){
            def reporTo
            try{
                if(params.reportTo.length > 0){
                    for(int i = 0; i < params.reportTo.length;i++){
                        reporTo = User.findByUsername(params.reportTo[i])
                        if(reporTo){
                            reportToChanged.add(reporTo)
                        }
                    }
                }
            }catch(Exception e){
                reporTo = User.findByUsername(params.reportTo)
                if(reporTo)
                    reportToChanged.add(reporTo)
            }
        }

        user.setReportTo(reportToChanged)

        if(params.accountLocked){
            user.accountLocked = params.accountLocked.equals('on')?true:false
        }

        if(params.canPasswordExpire){
            user.canPasswordExpire = params.canPasswordExpire.equals('on')?true:false
        }
//
//        if(params.isActive){
//            user.isActive = params.isActive.equals('on')?true:false
//        }
//
//        if(params.staffNo){
//            user.staffNo = params.staffNo
//        }
//
//        if(params.firstName){
//            user.firstName = params.firstName
//        }
//
//        if(params.lastName){
//            user.lastName = params.lastName
//        }
//
//        if(params.country){
//            user.country = params.country
//        }

        if(params.roles){
            try{
                if(params.roles.length > 0){
                    def role
                    for(int i = 0; i < params.roles.length;i++){
                        role = Role.findById(params.roles[i])
                        if(role){
                            user.addToRoles(role)
                            def menus = RoleMenu.findAllByRole(role)
                            menus.each {
                                user.addToPermissions(it.menu.linkController+":*")
                            }
                        }
                    }
                }
            }catch(Exception e){
                user.addToRoles(Role.findById(params.roles))
                def menus = RoleMenu.findAllByRole(Role.findById(params.roles))
                menus.each {
                    user.addToPermissions(it.menu.linkController+":*")
                }
            }
        }
//        validatePassword(user,params.passwordHash)

        return user
    }

    def unlock(params){
        def user = User.findByUsername(params.unlock)
        if (user) {
            user.accountLocked = false
            user.isActive = true
            user.wrongPasswordCounter = 0
            user.lastUpdated = datatablesUtilService?.syncTime()
            user.save(flush: true)

            if (user.hasErrors()) {
                throw new Exception("${user.errors}")
            }
        }else{
            throw new Exception("User not found")
        }
    }

    def edit(params){
        def user = User.findByUsername(params.username)
        if (user) {
            user.lastUpdProcess  = "UPDATE"
            user.updatedBy       = params.updatedBy
            user.t001Inisial     = params.t001Inisial
            user.t001NamaPegawai = params.t001NamaPegawai
            user.fullname        = params.t001NamaPegawai
            user.t001ExpiredAccountDate = params.t001ExpiredAccountDate
            user.t001Email       = params.t001Email
            user.companyDealer   = CompanyDealer.findById(params?.companyDealer.id?.toLong())
            user.divisi          = Divisi.findById(params?.divisi.id?.toLong())
            user.accountExpired  = params.accountExpired?Boolean.valueOf(params.accountExpired):false
            user.accountLocked   = params.accountLocked?Boolean.valueOf(params.accountLocked):false
            user.effectiveStartDate = params.effectiveStartDate
            if(params.canPasswordExpire){
                user.effectiveEndDate = params.effectiveEndDate
                user.t001ExpiredAccountDate = params.t001ExpiredAccountDate
            }
            user.t001ExpiredAccountDate = params.t001ExpiredAccountDate
            user.effectiveEndDate = params.effectiveEndDate
            if(params.imageProfile){
                String dirImage = "C:/image/profile"
                try{
                    boolean fileSuccessfullyDeleted =  new File(user.t001Foto).delete()
                }catch(Exception e){

                }
                String newName = user.username+"."+params.imageProfile.toString().tokenize(".").last()
                File file = new File(params.imageProfile)
                file.renameTo(new File(dirImage+"/"+newName))
                user.t001Foto = dirImage+"/"+newName
            }
//            if(params.passwordExpiredOn){
//                String passwordExpiredOn = params.passwordExpiredOn_day+"/"+params.passwordExpiredOn_month+"/"+params.passwordExpiredOn_year
//                try{
//                user.passwordExpiredOn = new SimpleDateFormat("d/M/yyyy").parse(passwordExpiredOn)
//                }catch (Exception e){
//                    throw new Exception(messageSource.getMessage("user.invalid_date.error",[messageSource.getMessage("user.passwordExpiredOn.label", null,"Password Expired On", LCH.getLocale())] as Object[], "Invalid date for "+messageSource.getMessage("user.passwordExpiredOn.label", null,"Password Expired On", LCH.getLocale()), LCH.getLocale()))
//                }
//            }

//            if(params.effectiveStartDate){
////                String effectiveStartDate = params.effectiveStartDate_day+"/"+params.effectiveStartDate_month+"/"+params.effectiveStartDate_year
//                String effectiveStartDate = params.effectiveStartDate
//                try{
//                    user.effectiveStartDate = new SimpleDateFormat("dd/MM/yyyy").parse(effectiveStartDate)
//                }catch (Exception e){
//                    throw new Exception(messageSource.getMessage("user.invalid_date.error",[messageSource.getMessage("user.effectiveStartDate.label", null,"Effective Start Date", LCH.getLocale())] as Object[], "Invalid date for "+messageSource.getMessage("user.effectiveStartDate.label", null,"Effective Start Date", LCH.getLocale()), LCH.getLocale()))
//                }
//            }
//
//            if(params.effectiveEndDate){
////                String effectiveEndDate = params.effectiveEndDate_day+"/"+params.effectiveEndDate_month+"/"+params.effectiveEndDate_year
//                try{
//                    user.effectiveEndDate = params.effectiveEndDate
//                }catch (Exception e){
//                    throw new Exception(messageSource.getMessage("user.invalid_date.error",[messageSource.getMessage("user.effectiveEndDate.label", null,"Effective End Date", LCH.getLocale())] as Object[], "Invalid date for "+messageSource.getMessage("user.effectiveEndDate.label", null,"Effective End Date", LCH.getLocale()), LCH.getLocale()))
//                }
//            }

//            if(params.groupFormId){
//                user.groupFormId = new Long(params.groupFormId)
//            }
//
//            if(params.groupLocalBranchId){
//                user.groupLocalBranchId = new Long(params.groupLocalBranchId)
//            }

            Set<User> reportToChanged = new ArrayList<User>()
            if(params.reportTo){
                def reporTo
                try{
                    if(params.reportTo.length > 0){
                        for(int i = 0; i < params.reportTo.length;i++){
                            reporTo = User.findByUsername(params.reportTo[i])
                            if(reporTo){
                                reportToChanged.add(reporTo)
                            }
                        }
                    }
                }catch(Exception e){
                    reporTo = User.findByUsername(params.reportTo)
                    if(reporTo)
                        reportToChanged.add(reporTo)
                }
            }

            user.setReportTo(reportToChanged)

            if(params.accountLocked){
                user.accountLocked = params.accountLocked.equals('on')?true:false
            }

            if(params.canPasswordExpire){
                user.canPasswordExpire = params.canPasswordExpire.equals('on')?true:false
            }
            else{
                user.canPasswordExpire=false
            }
//
//            if(params.isActive){
//                user.isActive = params.isActive.equals('on')?true:false
//            }
//            else
//            {
//                user.isActive=false
//            }
//
//            if(params.staffNo){
//                user.staffNo = params.staffNo
//            }
//
//            if(params.firstName){
//                user.firstName = params.firstName
//            }
//
//            if(params.lastName){
//                user.lastName = params.lastName
//            }
//
//            if(params.country){
//                user.country = params.country
//            }

            Set<Role> roleChanged = new ArrayList<Role>()
            if(params.roles){
                def role
                try{
                    if(params.roles.length > 0){
                        for(int i = 0; i < params.roles.length;i++){
                            role = Role.findById(params.roles[i])
                            if(role){
                                roleChanged.add(role)
                            }
                        }
                    }
                }catch(Exception e){
                    role = Role.findById(params.roles)
                    if(role)
                        roleChanged.add(role)
                }
            }

            if(params.passwordHash){
                user.passwordHash = authenticationUtilsService.hashPassword(validatePassword(user,params.passwordHash))
                SecurityChecklistUtilService sc = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PASSWORD_CYCLES)
                Calendar c = Calendar.getInstance()
                c.setTime(new Date())
                c.add(Calendar.DATE, sc.getValueInteger())

                user.passwordExpiredOn = c.getTime()
            }

            user.setRoles(roleChanged)
            user.lastUpdated = datatablesUtilService?.syncTime()
            user.save(flush: true)

            if (user.hasErrors()) {
                throw new Exception("${user.errors}")
            }
        }else{
            throw new Exception("User not found")
        }
    }

    def delete(params){
        def user = User.findByUsername(params.username)
        if (user) {
            user.delete()
        }else{
            throw new Exception("User not found")
        }
    }

    def validatePassword(user, newPassword){
        SecurityChecklistUtilService securityChecklistUtil = null

        //check minimum password length
        securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_MINIMUM_PASSWORD_LENGTH)
        def passwordLength = securityChecklistUtil.getMinimumPasswordLength()
        if(!securityChecklistUtil.validateMinimumPasswordLength(newPassword)) {
            throw new Exception(messageSource.getMessage("login.failed.minimum_password_length",[passwordLength.toString()] as Object[], "Minimum password length is "+passwordLength, LCH.getLocale()))
        }

        //validate password strength, preventing guessable password
        securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PREVENTING_GUESSABLE_PASSWORD)
        if(securityChecklistUtil.enablePreventingGuessablePassword()){
            HashMap result = securityChecklistUtil.validatePreventingGuessablePassword(newPassword)
            if(!(result.get("isValid"))){
                String eol = System.getProperty("line.separator");
                def message = ""
                for (String msg : result.get("message")) {
                    message = message+msg+eol
                }
                throw new Exception(message)
            }
        }

        //administer and check password history
        securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PASSWORD_HISTORY)
        if(securityChecklistUtil.enablePasswordHistory()){
            if(!securityChecklistUtil.validatePasswordHistory(user.username, authenticationUtilsService.hashPassword(newPassword))){
                if(securityChecklistUtil.countPasswordHistory(user.username) < securityChecklistUtil.getPasswordHistory()){
//                    user.addToPasswordHistory(authenticationUtilsService.hashPassword(newPassword))
                    user.addToPasswordHistory(new PasswordHistory(passwordHash: authenticationUtilsService.hashPassword(newPassword)).save())
                }else{
                    ArrayList<PasswordHistory> passwordHistories = user.getPasswordHistory()

                    def idFirst = passwordHistories.get(0).id
                    int idx = 0
                    for(int i = 0; i < passwordHistories.size(); i++){
                        if (idFirst > passwordHistories.get(i).id){
                            idFirst = passwordHistories.get(i).id
                            idx = i
                        }
                    }

                    user.removeFromPasswordHistory(passwordHistories.get(idx))
//                    user.addToPasswordHistory(authenticationUtilsService.hashPassword(newPassword))
                    user.addToPasswordHistory(new PasswordHistory(passwordHash: authenticationUtilsService.hashPassword(newPassword)).save())
                }
            }else{
                throw new Exception(messageSource.getMessage("login.failed.password_history",[passwordLength.toString()] as Object[], "You already use this password before, please choose new one", LCH.getLocale()))
            }
        }

        return newPassword
    }

    def addPasswordCycle(){
        //add password cycle
        SecurityChecklistUtilService securityChecklistUtil = null
        securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_PASSWORD_CYCLES)
        Calendar c = Calendar.getInstance()
        c.setTime(new Date())
        c.add(Calendar.DATE, securityChecklistUtil.getPasswordCycles())
        return securityChecklistUtil.enablePasswordCycles()? c.time :null
    }

    def changePassword(params){
        try{
            def oldPassword = params.oldPassword
            def newPassword = params.newPassword
            def confirmNewPassword = params.confirmNewPassword

            if(!newPassword.equals(confirmNewPassword)){
                throw new Exception(messageSource.getMessage("login.failed.confirm_new_password.not_match",[] as Object[], "New Password does not match the Confirm New Password", LCH.getLocale()))
            }

            def user = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
            if (user) {
                def oldPasswordHash  = authenticationUtilsService.hashPassword(oldPassword)
                def passwordHash  = authenticationUtilsService.hashPassword(newPassword)

                if(user.passwordHash != oldPasswordHash){
                    throw new Exception(messageSource.getMessage("login.failed.old_password.incorrect",[] as Object[], "Incorrect Old Password", LCH.getLocale()))
                }

                validatePassword(user,newPassword)
                user.passwordHash = passwordHash
                user.passwordUpdateBy = newPassword
                user.passwordUpdatedOn = datatablesUtilService?.syncTime()
                user.passwordExpiredOn = addPasswordCycle()
                user.lastUpdated = datatablesUtilService?.syncTime()
                user.save()

                if (user.hasErrors()) {
                    throw new Exception("${user.errors}")
                }
            }else{
                throw new Exception("User not found")
            }
        }catch(Exception e){
            //println(e)
            throw e
        }
    }

    def resetPassword(params){
        def user = User.findByUsername(params.username)
        if (user) {
            def newPasswordHash  = authenticationUtilsService.hashPassword(params.newPassword)

            user.passwordHash = newPasswordHash
            user.passwordUpdateBy = user.username
            user.passwordUpdatedOn = datatablesUtilService?.syncTime()
            user.passwordExpiredOn = addPasswordCycle()
            user.lastUpdated = datatablesUtilService?.syncTime()
            user.save()

            if (user.hasErrors()) {
                throw new Exception("${user.errors}")
            }
        }else{
            throw new Exception("User not found")
        }
    }

    def getReloginAfterChangePassword(){
        SecurityChecklistUtilService securityChecklistUtil = null
        securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_RELOGIN_AFTER_CHANGE_PASSWORD)
        return securityChecklistUtil.enableReloginAfterChangePassword()
    }
}
