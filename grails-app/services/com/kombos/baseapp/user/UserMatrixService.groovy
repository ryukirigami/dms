package com.kombos.baseapp.user

import com.kombos.baseapp.StaticResource
import com.kombos.baseapp.sec.shiro.Role

class UserMatrixService {

    def serviceMethod() {

    }

    def getList(){
        def rows = []
        Role.list().each {role ->
            def row = []
            row << role.name
            def rolePermissions = role.permissions
            StaticResource.userPermissions.sort {it.permissionString}.each {userPermission ->
                if(rolePermissions.find {it == userPermission.permissionString} != null){
                    row << 'Yes'
                }else{
                    row << 'No'
                }
            }

            rows << row
        }

        return rows

    }

    def save(String roleName, def userPermissions){
        def role = Role.findByName(roleName)
        if (role) {
            role.permissions = []
            userPermissions?.each{
                role.addToPermissions(it)
            }
            role.save()
        }
    }
}
