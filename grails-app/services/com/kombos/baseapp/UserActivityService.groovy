package com.kombos.baseapp

import groovy.sql.Sql

import java.text.SimpleDateFormat

class UserActivityService {
	def dataSource

	def appSettingParamDateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def appSettingParamTimeFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.TIME_FORMAT)
	def getList(def params){
		def propertiesToRender = params.sColumns.split(",")

		def filters = []
		def filterParams = [:]
		def x = 0
		def columnFilters = []
		def columnFilterSearch = false

		if(params."sCriteriaUser"){
			columnFilters << "user_id like :user"
			filterParams.user = "%" + (params."sCriteriaUser" as String) + "%"
			columnFilterSearch = true
		}
		
		if(params."sCriteriaMenu"){
			columnFilters << "menu_id like :menu"
			filterParams.menu = "%" + (params."sCriteriaMenu" as String) + "%"
			columnFilterSearch = true
		}
		
		if(params."sCriteriaController"){
			columnFilters << "controller like :controller"
			filterParams.controller = "%" + (params."sCriteriaController" as String) + "%"
			columnFilterSearch = true
		}
		
		if(params."sCriteriaAction"){
			columnFilters << "action like :action"
			filterParams.action = "%" + (params."sCriteriaAction" as String) + "%"
			columnFilterSearch = true
		}
		
		if(params."sCriteriaFrom"){
			columnFilters << "access_time >= TO_DATE(:from, 'DD-MM-YYYY')"
			filterParams.from = params."sCriteriaFrom" as String
			columnFilterSearch = true
		}
		
		if(params."sCriteriaTo"){
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy")
			Date to = sdf.parse(params."sCriteriaTo").next()
			
			columnFilters << "access_time <= TO_DATE(:to, 'DD-MM-YYYY')"
			filterParams.to = sdf.format(to)
			columnFilterSearch = true
		}

		def columnFilter = columnFilters.join(" AND ")

		def dataToRender = [:]
		dataToRender.sEcho = params.sEcho
		dataToRender.aaData=[]                // Array of products.
		dataToRender.iTotalRecords = UserActivity.count();
		dataToRender.iTotalDisplayRecords = dataToRender.iTotalRecords



		def query = new StringBuilder("select ${params.sColumns} from TBLG_USER_LOG where 1=1")
		def countQuery = new StringBuilder("select count(*) as total from TBLG_USER_LOG where 1=1")

		if (columnFilterSearch){
			query.append(" and (${columnFilter})")
			countQuery.append(" and (${columnFilter})")
		}
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		query.append(" order by ${sortProperty} ${sortDir}")

		def rows = []
		def result
		if ( params.sSearch || columnFilterSearch) {
			// Revise the number of total display records after applying the filter
			//			if(params.initsearch){
			//				countQuery.append(" and ("+params.initquery+")")
			//			}
			//
			//			if ( params.sSearch ) {
			//				countQuery.append(" and (${filter})")
			//			}
			//
			//			if (columnFilterSearch){
			//				countQuery.append(" and (${columnFilter})")
			//			}

			def sql = new Sql(dataSource)
			def tot = sql.firstRow(countQuery.toString(), filterParams)
			if(tot){
				dataToRender.iTotalRecords = tot.total;
				dataToRender.iTotalDisplayRecords = tot.total;
			}

			sql.eachRow(query.toString(), filterParams, (params.iDisplayStart as int)+1, params.iDisplayLength as int) { row ->
				def rowData = [:]
				rowData.pkid = row.pkid
				rowData.user_id = row.user_id
				rowData.menu_id = row.menu_id
				rowData.controller = row.controller
				rowData.action = row.action
				String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
				String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
				rowData.access_time = new SimpleDateFormat(dateFormat + " " + timeFormat).format(row.access_time.dateValue())

				dataToRender.aaData << rowData
			}

			
		} else {
			def sql = new Sql(dataSource)
			def tot = sql.firstRow(countQuery.toString())
			if(tot){
				dataToRender.iTotalRecords = tot.total;
				dataToRender.iTotalDisplayRecords = tot.total;
			}

			sql.eachRow(query.toString(), (params.iDisplayStart as int)+1, params.iDisplayLength as int) { row ->
				def rowData = [:]
				rowData.pkid = row.pkid
				rowData.user_id = row.user_id
				rowData.menu_id = row.menu_id
				rowData.controller = row.controller
				rowData.action = row.action
				String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
				String timeFormat = appSettingParamTimeFormat.value?appSettingParamTimeFormat.value:appSettingParamTimeFormat.defaultValue
				rowData.access_time = new SimpleDateFormat(dateFormat + " " + timeFormat).format(row.access_time.dateValue())

				dataToRender.aaData << rowData
			}
		}

//		rows?.each { row ->
//			def rowData = [:]
//			rowData.id = row.id
//			rowData.user = row.user.username
//			rowData.menu = row.menu.menuCode
//			rowData.controller = row.controller
//			rowData.action = row.action
//			String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
//			rowData.accessTime = new SimpleDateFormat(dateFormat).format(row.accessTime)
//
//			dataToRender.aaData << rowData
//		}


		//dataToRender.rows = rowData

		return dataToRender
	}
}
