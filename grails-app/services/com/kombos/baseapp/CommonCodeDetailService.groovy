package com.kombos.baseapp

import groovy.sql.Sql

class CommonCodeDetailService {
    def datatablesUtilService
    def dataSource

    def getPkid()
    {
        def sql=new Sql(dataSource)
        def pkid
        def res=sql.firstRow("SELECT TBLM_COMMONCODED_SQ.nextval as p from dual")
        if(res)
            pkid=res.p
        pkid
    }

    def getList(def params){
        def ret
        def res = datatablesUtilService.getDomainList(CommonCodeDetail, params)
        def rows = []
        CommonCodeDetail temp
        res?.rows.each {
            temp = it
            rows << [
                    temp.pkid,
                    temp.pkid,
                    temp.commoncode,
                    temp.bicode,
                    temp.description,
                    temp.parentcode,
                    temp.parentbicode,
                    temp.sequence
//                    temp.refcommoncode,
//                    temp.refid,
//                    temp.createddate,
//                    temp.createdhost,
//                    temp.lasteditby,
//                    temp.lasteditdate,
//                    temp.lastedithost,

            ]
        }
        res.rows = rows
        ret = [sEcho:res.sEcho, iTotalRecords:res.iTotalRecords, iTotalDisplayRecords:res.iTotalDisplayRecords, aaData:res.rows]

        return ret
    }
}
