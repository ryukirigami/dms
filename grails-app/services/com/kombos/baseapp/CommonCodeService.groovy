package com.kombos.baseapp

class CommonCodeService {

//    def gridUtilService
//
//    def getList(def params) {
//        def ret = null
//
//        try {
//            def res = gridUtilService.getDomainList(CommonCode, params)
//            def rows = []
//            def counter = 0
//            ((List<CommonCode>) (res?.rows))?.each {
//                rows << [
//                        id: counter++,
//                        cell: [
//
//                                it.key,
//                                it.value
//                        ]
//                ]
//            }
//
//            ret = [rows: rows, totalRecords: res.totalRecords, page: params.page, totalPage: res.totalPage]
//
//        } catch (e) {
//            e.printStackTrace()
//            throw e
//        }
//
//        return ret
//    }

    def datatablesUtilService

    def getList(def params){
        def ret
        def res = datatablesUtilService.getDomainList(CommonCode, params)
        def rows = []
        CommonCode temp
        res?.rows.each {
            temp = it
            rows << [
                    temp.commoncode,
                    temp.commoncode,
                    temp.commonname,
                    temp.commondesc,
                    temp.commonusage,
                    temp.codetype,
                    temp.parentcode,
            ]
        }
        res.rows = rows
        ret = [sEcho:res.sEcho, iTotalRecords:res.iTotalRecords, iTotalDisplayRecords:res.iTotalDisplayRecords, aaData:res.rows]

        return ret
    }
    /*
    def add(params) throws Exception {
        def commonCode = new CommonCode()

        commonCode.key = params.key
        commonCode.value = params.value

        commonCode.save()
        if (commonCode.hasErrors()) {
            throw new Exception("${commonCode.errors}")
        }
    }

    def edit(params) {
        def commonCode = CommonCode.findByKey(params.key)
        if (commonCode) {

            commonCode.key = params.key
            commonCode.value = params.value

            commonCode.save()
            if (commonCode.hasErrors()) {
                throw new Exception("${commonCode.errors}")
            }
        } else {
            throw new Exception("CommonCode not found")
        }
    }

    def delete(params) {
        def commonCode = CommonCode.findByKey(params.key)
        if (commonCode) {
            commonCode.delete()
        } else {
            throw new Exception("CommonCode not found")
        }
    }
    */
}