package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class MasterEvaluasiTargetService {	
	boolean transactional = false
    def datatablesUtilService
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		//println params.userCompanyDealer
		def c = MasterEvaluasiTarget.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params.userCompanyDealer)
            eq("istarget","1")
			if(params."sCriteria_monthYear"){
				ilike("monthYear","%" + (params."sCriteria_monthYear" as String) + "%")
			}

			if(params."sCriteria_serviceType"){
				ilike("serviceType","%" + (params."sCriteria_serviceType" as String) + "%")
			}

			if(params."sCriteria_jasa"){
				eq("jasa",params."sCriteria_jasa" as BigDecimal)
			}

			if(params."sCriteria_parts"){
				eq("parts",params."sCriteria_parts" as BigDecimal)
			}

			if(params."sCriteria_bahan"){
				eq("bahan",params."sCriteria_bahan" as BigDecimal)
			}

			if(params."sCriteria_spooring"){
				eq("spooring",params."sCriteria_spooring" as BigDecimal)
			}

			if(params."sCriteria_antikarat"){
				eq("antikarat",params."sCriteria_antikarat" as BigDecimal)
			}

			if(params."sCriteria_lainLain"){
				eq("lainLain",params."sCriteria_lainLain" as BigDecimal)
			}

			if(params."sCriteria_gajiMekanik"){
				eq("gajiMekanik",params."sCriteria_gajiMekanik" as BigDecimal)
			}

			if(params."sCriteria_gajiMekanikLainLain"){
				eq("gajiMekanikLainLain",params."sCriteria_gajiMekanikLainLain" as BigDecimal)
			}

			if(params."sCriteria_insentifLemburMek"){
				eq("insentifLemburMek",params."sCriteria_insentifLemburMek" as BigDecimal)
			}

			if(params."sCriteria_gajiAdm"){
				eq("gajiAdm",params."sCriteria_gajiAdm" as BigDecimal)
			}

			if(params."sCriteria_gajiAdmLainLain"){
				eq("gajiAdmLainLain",params."sCriteria_gajiAdmLainLain" as BigDecimal)
			}

			if(params."sCriteria_insentifAdm"){
				eq("insentifAdm",params."sCriteria_insentifAdm" as BigDecimal)
			}

			if(params."sCriteria_biayaParts"){
				eq("biayaParts",params."sCriteria_biayaParts" as BigDecimal)
			}

			if(params."sCriteria_biayaBahan"){
				eq("biayaBahan",params."sCriteria_biayaBahan" as BigDecimal)
			}

			if(params."sCriteria_biayaPekLuar"){
				eq("biayaPekLuar",params."sCriteria_biayaPekLuar" as BigDecimal)
			}

			if(params."sCriteria_biayaSpooring"){
		 		eq("biayaSpooring",params."sCriteria_biayaSpooring" as BigDecimal)
			}

			if(params."sCriteria_biayaPerjalanan"){
				eq("biayaPerjalanan",params."sCriteria_biayaPerjalanan" as BigDecimal)
			}

			if(params."sCriteria_biayaUmum"){
				eq("biayaUmum",params."sCriteria_biayaUmum" as BigDecimal)
			}

			if(params."sCriteria_biayaKomisi"){
				eq("biayaKomisi",params."sCriteria_biayaKomisi" as BigDecimal)
			}

			if(params."sCriteria_claimRepair"){
				eq("claimRepair",params."sCriteria_claimRepair" as BigDecimal)
			}

			if(params."sCriteria_biayaPenyusutan"){
				eq("biayaPenyusutan",params."sCriteria_biayaPenyusutan" as BigDecimal)
			}

			if(params."sCriteria_biayaTakTerduga"){
				eq("biayaTakTerduga",params."sCriteria_biayaTakTerduga" as BigDecimal)
			}

			if(params."sCriteria_biayaSewaGedung"){
				eq("biayaSewaGedung",params."sCriteria_biayaSewaGedung" as BigDecimal)
			}

			if(params."sCriteria_jumlahStall"){
				eq("jumlahStall",params."sCriteria_jumlahStall" as BigDecimal)
			}

			if(params."sCriteria_mekanik"){
				eq("mekanik",params."sCriteria_mekanik" as BigDecimal)
			}

			if(params."sCriteria_repairUnit"){
				eq("repairUnit",params."sCriteria_repairUnit" as BigDecimal)
			}

			if(params."sCriteria_repairOrder"){
				eq("repairOrder",params."sCriteria_repairOrder" as BigDecimal)
			}

			if(params."sCriteria_paymentRate"){
				eq("paymentRate",params."sCriteria_paymentRate" as BigDecimal)
			}

			if(params."sCriteria_creditPaymentRate"){
				eq("creditPaymentRate",params."sCriteria_creditPaymentRate" as BigDecimal)
			}


			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companyDealer: it.companyDealer?.m011NamaWorkshop,
			
						monthYear: it.monthYear,
			
						istarget: it.istarget,
			
						serviceType: it.serviceType,
			
						jasa: it.jasa,
			
						parts: it.parts,
			
						bahan: it.bahan,
			
						spooring: it.spooring,
			
						antikarat: it.antikarat,
			
						lainLain: it.lainLain,
			
						gajiMekanik: it.gajiMekanik,
			
						gajiMekanikLainLain: it.gajiMekanikLainLain,
			
						insentifLemburMek: it.insentifLemburMek,
			
						gajiAdm: it.gajiAdm,
			
						gajiAdmLainLain: it.gajiAdmLainLain,
			
						insentifAdm: it.insentifAdm,
			
						biayaParts: it.biayaParts,
			
						biayaBahan: it.biayaBahan,
			
						biayaPekLuar: it.biayaPekLuar,
			
						biayaSpooring: it.biayaSpooring,
			
						biayaPerjalanan: it.biayaPerjalanan,
			
						biayaUmum: it.biayaUmum,
			
						biayaKomisi: it.biayaKomisi,
			
						claimRepair: it.claimRepair,
			
						biayaPenyusutan: it.biayaPenyusutan,
			
						biayaTakTerduga: it.biayaTakTerduga,
			
						biayaSewaGedung: it.biayaSewaGedung,
			
						jumlahStall: it.jumlahStall,
			
						mekanik: it.mekanik,
			
						repairUnit: it.repairUnit,
			
						repairOrder: it.repairOrder,
			
						paymentRate: it.paymentRate,
			
						creditPaymentRate: it.creditPaymentRate,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MasterEvaluasiTarget", params.id] ]
			return result
		}

		result.masterEvaluasiTargetInstance = MasterEvaluasiTarget.get(params.id)

		if(!result.masterEvaluasiTargetInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MasterEvaluasiTarget", params.id] ]
			return result
		}

		result.masterEvaluasiTargetInstance = MasterEvaluasiTarget.get(params.id)

		if(!result.masterEvaluasiTargetInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MasterEvaluasiTarget", params.id] ]
			return result
		}

		result.masterEvaluasiTargetInstance = MasterEvaluasiTarget.get(params.id)

		if(!result.masterEvaluasiTargetInstance)
			return fail(code:"default.not.found.message")

		try {
			result.masterEvaluasiTargetInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		MasterEvaluasiTarget.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.masterEvaluasiTargetInstance && m.field)
					result.masterEvaluasiTargetInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["MasterEvaluasiTarget", params.id] ]
				return result
			}

			result.masterEvaluasiTargetInstance = MasterEvaluasiTarget.get(params.id)

			if(!result.masterEvaluasiTargetInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.masterEvaluasiTargetInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}
            params.monthYear = params.bulanPeriode + ""+ params.tahunPeriode
            params.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            params.lastUpdProcess  = "UPDATE"
            params.lastUpdated = datatablesUtilService?.syncTime()
			result.masterEvaluasiTargetInstance.properties = params

			if(result.masterEvaluasiTargetInstance.hasErrors() || !result.masterEvaluasiTargetInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MasterEvaluasiTarget", params.id] ]
			return result
		}

		result.masterEvaluasiTargetInstance = new MasterEvaluasiTarget()
		result.masterEvaluasiTargetInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.masterEvaluasiTargetInstance && m.field)
				result.masterEvaluasiTargetInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["MasterEvaluasiTarget", params.id] ]
			return result
		}
        params.lastUpdProcess  = "INSERT"
        params.monthYear = params.bulanPeriode + ""+ params.tahunPeriode
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.istarget = '1'
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		result.masterEvaluasiTargetInstance = new MasterEvaluasiTarget(params)

		if(result.masterEvaluasiTargetInstance.hasErrors() || !result.masterEvaluasiTargetInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def masterEvaluasiTarget =  MasterEvaluasiTarget.findById(params.id)
		if (masterEvaluasiTarget) {
			masterEvaluasiTarget."${params.name}" = params.value
			masterEvaluasiTarget.save()
			if (masterEvaluasiTarget.hasErrors()) {
				throw new Exception("${masterEvaluasiTarget.errors}")
			}
		}else{
			throw new Exception("MasterEvaluasiTarget not found")
		}
	}

}