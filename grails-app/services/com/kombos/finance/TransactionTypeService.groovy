package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class TransactionTypeService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = TransactionType.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_transactionType") {
                ilike("transactionType", "%" + (params."sCriteria_transactionType" as String) + "%")
            }

            if (params."sCriteria_description") {
                ilike("description", "%" + (params."sCriteria_description" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_transaction") {
                transaction {
                    ilike("transactionCode", "%" + (params."sCriteria_transaction" as String) + "%")
                }
                /*eq("transaction", params."sCriteria_transaction")*/
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    transactionType: it.transactionType,

                    description: it.description,

                    lastUpdProcess: it.lastUpdProcess,

                    transaction: it.transaction,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TransactionType", params.id]]
            return result
        }

        result.transactionTypeInstance = TransactionType.get(params.id)

        if (!result.transactionTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TransactionType", params.id]]
            return result
        }

        result.transactionTypeInstance = TransactionType.get(params.id)

        if (!result.transactionTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TransactionType", params.id]]
            return result
        }

        result.transactionTypeInstance = TransactionType.get(params.id)

        if (!result.transactionTypeInstance)
            return fail(code: "default.not.found.message")

        try {
            result.transactionTypeInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        TransactionType.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.transactionTypeInstance && m.field)
                    result.transactionTypeInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["TransactionType", params.id]]
                return result
            }

            result.transactionTypeInstance = TransactionType.get(params.id)

            if (!result.transactionTypeInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.transactionTypeInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.transactionTypeInstance.properties = params

            if (result.transactionTypeInstance.hasErrors() || !result.transactionTypeInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TransactionType", params.id]]
            return result
        }

        result.transactionTypeInstance = new TransactionType()
        result.transactionTypeInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.transactionTypeInstance && m.field)
                result.transactionTypeInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["TransactionType", params.id]]
            return result
        }

        result.transactionTypeInstance = new TransactionType(params)

        if (result.transactionTypeInstance.hasErrors() || !result.transactionTypeInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def transactionType = TransactionType.findById(params.id)
        if (transactionType) {
            transactionType."${params.name}" = params.value
            transactionType.save()
            if (transactionType.hasErrors()) {
                throw new Exception("${transactionType.errors}")
            }
        } else {
            throw new Exception("TransactionType not found")
        }
    }

}