package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class AssetTypeService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = AssetType.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_typeAsset") {
                ilike("typeAsset", "%" + (params."sCriteria_typeAsset" as String) + "%")
            }

            if (params."sCriteria_description") {
                ilike("description", "%" + (params."sCriteria_description" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_asset") {
                eq("asset", params."sCriteria_asset")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,

                    typeAsset: it.typeAsset,

                    description: it.description,

                    lastUpdProcess: it.lastUpdProcess,

                    asset: it.asset,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetType", params.id]]
            return result
        }

        result.assetTypeInstance = params
//        result.assetTypeInstance = AssetType.get(params.idtoLong)
        if (!result.assetTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetType", params.id]]
            return result
        }

        result.assetTypeInstance = AssetType.get(params.id)

        if (!result.assetTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetType", params.id]]
            return result
        }

        result.assetTypeInstance = AssetType.get(params.id)

        if (!result.assetTypeInstance)
            return fail(code: "default.not.found.message")

        try {
            result.assetTypeInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        AssetType.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.assetTypeInstance && m.field)
                    result.assetTypeInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["AssetType", params.id]]
                return result
            }

            result.assetTypeInstance = AssetType.get(params.id)

            if (!result.assetTypeInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.assetTypeInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.assetTypeInstance.properties = params

            if (result.assetTypeInstance.hasErrors() || !result.assetTypeInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetType", params.id]]
            return result
        }

        result.assetTypeInstance = new AssetType()
        result.assetTypeInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.assetTypeInstance && m.field)
                result.assetTypeInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["AssetType", params.id]]
            return result
        }

        result.assetTypeInstance = new AssetType(params)

        if (result.assetTypeInstance.hasErrors() || !result.assetTypeInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def assetType = AssetType.findById(params.id)
        if (assetType) {
            assetType."${params.name}" = params.value
            assetType.save()
            if (assetType.hasErrors()) {
                throw new Exception("${assetType.errors}")
            }
        } else {
            throw new Exception("AssetType not found")
        }
    }

}