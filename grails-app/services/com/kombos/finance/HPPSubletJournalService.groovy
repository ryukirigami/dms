package com.kombos.finance

import com.kombos.generatecode.GenerateCodeService
import com.kombos.maintable.InvoiceT701
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import org.springframework.web.context.request.RequestContextHolder

class HPPSubletJournalService {
    boolean transactional = false
    def datatablesUtilService
    def createJournal(params) {
        /*  params :
            Name
            docNumber           No dokumen referensi
            totalBiaya          Total biaya transaksi
        */
        def total = 0
        def invoice = InvoiceT701.findByT701NoInvAndT701StaDel( (params.noInv as String) ,"0")
        def reception = Reception.findByT401NoWOAndStaDelAndStaSave(params.docNumber,"0","0")
        if(reception){
            def jobRcp = JobRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",reception)
                or{
                    ilike("t402StaTambahKurang","0")
                    and{
                        not{
                            ilike("t402StaTambahKurang","%1%")
                            ilike("t402StaApproveTambahKurang","%1%")
                        }
                    }
                    and {
                        isNull("t402StaTambahKurang")
                        isNull("t402StaApproveTambahKurang")
                    }
                }
                isNotNull("t402JmlSublet")
            }
            jobRcp.each {
                def hargaBeli = it?.t402HargaBeliSublet ? it?.t402HargaBeliSublet?.toDouble() : 0
                total += hargaBeli
            }
        }
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def journalInstance = new Journal()
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.companyDealer = session.userCompanyDealer
        try {
            journalInstance.journalDate =  invoice.t701TglJamInvoice
        }catch (Exception){
            journalInstance.journalDate =  new Date()
        }
        journalInstance.totalDebit = Math.round(total)
        journalInstance.totalCredit = Math.round(total)
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance?.dateCreated = datatablesUtilService?.syncTime()
        journalInstance?.lastUpdated = datatablesUtilService?.syncTime()
        journalInstance.description = "HPP Sublet No. WO "+params?.docNumber
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJSUBLET")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def journalDetail = new JournalDetail(
                        journalTransactionType:journalType?.journalType,
                        creditAmount: (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit")?Math.round(total):0,
                        debitAmount: (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet")?Math.round(total):0,
                        staDel: "0",
                        accountNumber: mappingJournalDetail.accountNumber,
                        journal: journalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime(),
                        subLedger: ""

                )
                if (journalDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                    //println "====> ${rowCount}"
                }
            }
            //println "HPP Sublet Journal Saved"
        }
    }

}
