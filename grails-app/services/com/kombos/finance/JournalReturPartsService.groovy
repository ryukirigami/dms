package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.GoodsHargaBeli
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.Returns
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class JournalReturPartsService {

    boolean transactional = false
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def createJournal(params) {
        /*  params :
            nomorReturn
        */
        def returns = Returns?.findAllByT172ID(params.nomorReturn)
        def totalBayar = 0
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = Math.round(calculateTotal(returns, "debet"))
        journalInstance.totalCredit = Math.round(calculateTotal(returns, "kredit"))
        journalInstance.docNumber = ""
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = "Journal Parts Return"
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal  mappingJournal = MappingJournal.findByMappingCode("MAP-TJRET")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber

                totalBayar = getAmount(accountNumber, returns,(mappingJournalDetail.accountTransactionType as String))
                //println "Akun ===>< " + accountNumber.accountName + " - " + accountNumber.accountNumber
                if(totalBayar!=0){
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: ""
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                        //println "====> ${rowCount}"
                    }
                }
            }
            //println "Cash Invoice Journal Saved"
        }
    }

    def getMappingJournal() {

    }

    def calculateTotal(returns, type) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        if((type as String).equals("debet")) {
            def harga = 0
            returns.each{
                harga += it.t172Qty1Return * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer)?.t150Harga : 0 )
            }
            return harga
        } else if((type as String).equals("kredit")) {
            def harga = 0
            returns.each{
                harga += it.t172Qty1Return * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer)?.t150Harga : 0 )
            }
            return harga
        } else {
            return 0
        }
    }

    def getAmount(accountNumber, returns, accountTransactionType) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber) {
                case "4.10.10.01.001" :
                    def harga = 0
                    returns.each{
                        harga += it.t172Qty1Return * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer)?.t150Harga : 0 )
                    }
                    return harga
                 default:
                    return 0
            }
        } else {
            switch (accountNumber.accountNumber) {
                case "1.60.10.01.001" : //persediaan parts toyota
                    def harga = 0
                    returns.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("TOYOTA")){
                            harga += it.t172Qty1Return * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer)?.t150Harga : 0 )
                        }
                    }
                    return harga                   //untuk sementara 0, menunggu mapping source
                case "1.60.10.01.003" : //persediaan parts campuran
                    def harga = 0
                    returns.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                            harga += it.t172Qty1Return * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer)?.t150Harga : 0 )
                        }
                    }
                    return harga                   //untuk sementara 0, menunggu mapping source
                case "1.60.10.02.001" :  //parts bahan
                    def harga = 0
                    returns.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAT") ){
                            harga += it.t172Qty1Return * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session?.userCompanyDealer)?.t150Harga : 0 )
                        }
                    }
                    return harga
                default:
                    return 0
            }
        }

    }

//    def generateJournalCode(params) {
//        Date date = new Date()
//        SimpleDateFormat dateFormat = new SimpleDateFormat("MMYYYY")
//        String dateString = dateFormat.format(date)
//        String prefix = params
//        String rpadding = "000000"
//        def lastJournal = Journal.last()
//        Long id = 1
//        if (lastJournal) {
//            id = lastJournal.id +1
//        }
//        return prefix + dateString + rpadding + String.valueOf(id)
//    }
}
