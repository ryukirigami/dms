package com.kombos.finance



import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Konversi
import grails.converters.JSON

class CollectionService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Collection.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer);
            eq("staDel","0")
            ilike("paymentStatus","%BELUM%");
            if(params?."sCriteria_dueDate"){
                ge("dueDate",params?."sCriteria_dueDate")
                lt("dueDate",params?."sCriteria_dueDate"+1)
            }

            invoiceT071{
                if(params?.isHasjrat){
                    or{
                        ilike("t701Customer","%HASJRAT%")
                        ilike("t701Customer","%KOMBOS%")
                    }
                }else{
                    or{
                        not {
                            ilike("t701Customer","%HASJRAT%")
                            ilike("t701Customer","%KOMBOS%")
                        }
                    }
                }
                if (params."sCriteria_noInv") {
                    ilike("t701NoInv", "%"+params."sCriteria_noInv"+"%")
                }

                if (params."sCriteria_customer") {
                    ilike("t701Customer", "%"+params."sCriteria_customer"+"%")
                }

                if (params."sCriteria_noPol") {
                    ilike("t701Nopol", "%"+params."sCriteria_noPol"+"%")
                }

                if (params."sCriteria_noWo") {
                    reception{
                        ilike("t401NoWO", "%"+params."sCriteria_noWo"+"%")
                    }
                }

                if (params."sCriteria_sa") {
                    reception{
                        ilike("t401NamaSA", "%"+params."sCriteria_sa"+"%")
                    }
                }

                if (params."sCriteria_jumlah") {
                    eq("t701TotalBayarRp", "%"+params."sCriteria_jumlah"+"%")
                }

                if(params?."sCriteria_tgglInv"){
                    ge("t701TglJamInvoice",params?."sCriteria_tgglInv")
                    lt("t701TglJamInvoice",params?."sCriteria_tgglInv"+1)
                }
            }
            order("dueDate")
        }

        def rows = []
        def konversi = new Konversi()
        results.each {
            rows << [

                    id: it?.id,
                    t701NoInv: it?.invoiceT071?.t701NoInv,
                    t701TglJamInvoice: it?.invoiceT071?.t701TglJamInvoice?it?.invoiceT071?.t701TglJamInvoice?.format("dd/MM/yyyy"):"-",
                    reception: it?.invoiceT071?.reception?.t401NoWO,
                    t701Nopol: it?.invoiceT071?.t701Nopol,
                    t701Customer: it?.invoiceT071?.t701Customer,
                    t701TotalInv: konversi.toRupiah(it?.invoiceT071?.t701TotalInv),
                    t701BookingFee: konversi.toRupiah(it?.invoiceT071?.t701BookingFee),
                    t701TotalBayarRp: konversi.toRupiah(it?.paidAmount),
                    t701TotalRp: konversi.toRupiah(it?.amount-it?.paidAmount),
                    t701xNamaUser: it?.invoiceT071?.reception?.t401NamaSA,
                    t701TglJamCetak: it?.dueDate ? it?.dueDate?.format("dd/MM/yyyy") : ""
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Collection", params.id]]
            return result
        }

        result.collectionInstance = Collection.get(params.id)

        if (!result.collectionInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Collection", params.id]]
            return result
        }

        result.collectionInstance = Collection.get(params.id)

        if (!result.collectionInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Collection", params.id]]
            return result
        }

        result.collectionInstance = new Collection()
        result.collectionInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.collectionInstance && m.field)
                result.collectionInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Collection", params.id]]
            return result
        }

        result.collectionInstance = new Collection(params)

        if (result.collectionInstance.hasErrors() || !result.collectionInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def collection = Collection.findById(params.id)
        if (collection) {
            collection."${params.name}" = params.value
            collection.save()
            if (collection.hasErrors()) {
                throw new Exception("${collection.errors}")
            }
        } else {
            throw new Exception("Collection not found")
        }
    }

}
