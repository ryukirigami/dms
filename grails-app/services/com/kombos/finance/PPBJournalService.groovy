package com.kombos.finance

import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.GoodsReceiveDetail
import com.kombos.parts.KlasifikasiGoods
import org.springframework.web.context.request.RequestContextHolder

class PPBJournalService {

    boolean transactional = false

    /*def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)*/

    def createJournal(params) {
    /*  params :
        transactionType,         "Tunai","Kredit"
        supplier,                "Hasjrat","Other"
        purpose,                 0=none, 1= Toyota, 2= Parts Campuran, 3= Parts Bahan
        usage                    0=none, 1= pemeliharaan bengkel, 2= pemeliharaan kantor, 3= pemeliharaan gedung, 4= kendaraan dinas, 5= cabang, 6= beban karyawan
        totalBiaya               ex: 100000000
        ppn                      dalam persen, ex: 10
    */
        def datatablesUtilService = new DatatablesUtilService()
        def totalPPn = params.totalBiaya * ((params.ppn as Integer) / 100)
        def totalBayar = params?.totalBiaya?.toString()?.toBigDecimal() + totalPPn
        def supplier = ""
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = Math.round(totalBayar)
        journalInstance.totalCredit = Math.round(totalBayar)
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = "Pembelian Parts No. Receive " + params.goodsReceive
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            //println journalInstance.errors
            return "Error"
        } else
        {
            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJPPB1")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                AccountNumber accountNumber = mappingJournalDetail.accountNumber
                def subType = null
                def subLedger = null
                if(mappingJournalDetail?.accountNumber?.accountNumber?.equalsIgnoreCase("4.10.10.01.001"))
                {
                    subType = SubType?.findByDescriptionIlikeAndStaDel("VENDOR%","0");
                    subLedger = params?.idVendor
                }
                if((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet")) {
                    supplier = ""
                    totalBayar = accountNumber.accountName.contains("PPN") ? totalPPn : params.totalBiaya
                }else{
                    totalBayar = accountNumber.accountName.contains("HUTANG") ? (totalPPn+params.totalBiaya) : params.totalBiaya
                    if(accountNumber.accountName.contains("HUTANG")){
                        supplier = params.supplier
                        if(AccountNumber.findByAccountNumber(params.rk as String)){
                            accountNumber = AccountNumber.findByAccountNumber(params.rk as String)
                            subType = null
                            subLedger = null
                        }
                    }
                }
                def krd = (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit")?Math.round(totalBayar):0
                def dbt = (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet")?Math.round(totalBayar):0
                if(krd==0 && dbt==0){
                    //println "Sorry debet n kredit tidak null"
                }else{
                    def jumBayar = 0
                    jumBayar = getAmount(accountNumber, (mappingJournalDetail.accountTransactionType as String), params?.idGoodsReceive?.toLong(),totalBayar, totalPPn)
                    if(jumBayar!=0){
                        def journalDetail = new JournalDetail(
                                journalTransactionType:journalType.journalType,
                                creditAmount: (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit")?Math.round(jumBayar):0,
                                debitAmount: (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet")?Math.round(jumBayar):0,
                                staDel: "0",
                                accountNumber: accountNumber,
                                journal: journalInstance,
                                vendor: supplier,
                                createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                                lastUpdProcess: "INSERT",
                                subType: subType,
                                subLedger : subLedger,
                                dateCreated: datatablesUtilService.syncTime(),
                                lastUpdated: datatablesUtilService.syncTime()
                        )
                        if (journalDetail.save(flush: true, failOnError: true)) {
                            rowCount++
                            //println "====> ${rowCount}"
                        }
                    }
                }
            }
            //println "PPB Journal Saved"
        }

    }

    def getAmount(AccountNumber accountNumber, String accountTransactionType, Long id, def totalBayar, def totalPpn) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber.toLowerCase()) {
                case "4.20.10.04.001" : //PPN MASUKAN BELUM TERIMA FAKTUR PAJAK
                    return totalPpn
                case "1.60.10.02.001" : // PERSEDIAAN BAHAN
                    def list = GoodsReceiveDetail?.createCriteria()?.list {
                        eq("staDel","0")
                        goodsReceive{
                            eq("id",id)
                        }
                    }
                    def hargaBahan = 0
                    list.each {
                        def klas = KlasifikasiGoods?.findByGoods(it?.goods)
                        if(klas && klas?.franc?.m117NomorAkun?.accountNumber == "1.60.10.02.001"){
                            hargaBahan+=(it?.t167NetSalesPrice?it?.t167NetSalesPrice:0)
                        }
                    }
                    return hargaBahan
                case "1.60.10.01.003" : // PERSEDIAAN PART CAMPURAN
                    def list = GoodsReceiveDetail?.createCriteria()?.list {
                        eq("staDel","0")
                        goodsReceive{
                            eq("id",id)
                        }
                    }
                    def hargaCampur = 0
                    list.each {
                        def klas = KlasifikasiGoods?.findByGoods(it?.goods)
                        if(klas && klas?.franc?.m117NomorAkun?.accountNumber == "1.60.10.01.003"){
                            hargaCampur+=(it?.t167NetSalesPrice?it?.t167NetSalesPrice:0)
                        }
                    }
                    return hargaCampur
                case "1.60.10.01.001" : // PERSEDIAAN PART TOYOTA
                    def list = GoodsReceiveDetail?.createCriteria()?.list {
                        eq("staDel","0")
                        goodsReceive{
                            eq("id",id)
                        }
                    }
                    def hargaToyota = 0
                    list.each {
                        def klas = KlasifikasiGoods?.findByGoods(it?.goods)
                        if(klas && klas?.franc?.m117NomorAkun?.accountNumber == "1.60.10.01.001"){
                            hargaToyota+=(it?.t167NetSalesPrice?it?.t167NetSalesPrice:0)
                        }
                    }
                    return hargaToyota
                case "6.10.30.01.003" :
                    return 0
                case "7.10.20.01.006" :
                    return 0
                case "7.10.20.01.009" :
                    return 0
                case "1.70.10.01.002" :
                    return 0
                case "7.10.20.01.008" :
                    return 0
                case "7.10.10.01.007" :
                    return 0
                case "1.30.40.01.001" :
                    return 0
                default:
                    def valReturn = 0
                    return valReturn
            }
        } else {
                return totalBayar
        }

    }

    def createJournalTunai(params) {
        def metodWaktu = new DatatablesUtilService()
        def totalPPn = params.totalBiaya * ((params.ppn as Integer) / 100)
        def totalBayar = params?.totalBiaya?.toString()?.toBigDecimal() + totalPPn
        def supplier = ""
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = Math.round(totalBayar)
        journalInstance.totalCredit = Math.round(totalBayar)
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                "Pembelian Parts (Tunai) No. Receive " + params.goodsReceive : "Pembelian Parts (Tunai - BANK) No. Receive " + params.goodsReceive
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = metodWaktu?.syncTime()
        journalInstance.lastUpdated = metodWaktu?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            //println journalInstance.errors
            return "Error"
        } else {
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                bankAccountNumber = BankAccountNumber.get(params.idBank.toLong())
                if (!bankAccountNumber) {
                    return "Error"
                }
            }

            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJPPBT")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK") && accountNumber?.accountNumber!="1.10.10.01.001") {
                    accountNumber = bankAccountNumber.accountNumber
                }
                def subType = null
                def subLedger = null
                def jumBayar = getAmountTunai(accountNumber, (mappingJournalDetail.accountTransactionType as String), params?.idGoodsReceive?.toLong(),totalBayar, totalPPn,params.tipeBayar)
                if(jumBayar!=0){
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit")?Math.round(jumBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet")?Math.round(jumBayar):0,
                            staDel: "0",
                            accountNumber: mappingJournalDetail.accountNumber,
                            journal: journalInstance,
                            vendor: supplier,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subType: subType,
                            subLedger : subLedger,
                            dateCreated: metodWaktu.syncTime(),
                            lastUpdated: metodWaktu.syncTime()
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                        //println "====> ${rowCount}"
                    }
                }
            }
            //println "PPB TUNAI Journal Saved"
        }

    }

    def getAmountTunai(AccountNumber accountNumber, String accountTransactionType, Long id, def totalBayar, def totalPpn,String tipeBayar) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber.toLowerCase()) {
                case "4.20.10.04.001" : //PPN MASUKAN BELUM TERIMA FAKTUR PAJAK
                    return totalPpn
                case "1.60.10.02.001" : // PERSEDIAAN BAHAN
                    def list = GoodsReceiveDetail?.createCriteria()?.list {
                        eq("staDel","0")
                        goodsReceive{
                            eq("id",id)
                        }
                    }
                    def hargaBahan = 0
                    list.each {
                        def klas = KlasifikasiGoods?.findByGoods(it?.goods)
                        if(klas && klas?.franc?.m117NomorAkun?.accountNumber == "1.60.10.02.001"){
                            hargaBahan+=(it?.t167NetSalesPrice?it?.t167NetSalesPrice:0)
                        }
                    }
                    return hargaBahan
                case "1.60.10.01.003" : // PERSEDIAAN PART CAMPURAN
                    def list = GoodsReceiveDetail?.createCriteria()?.list {
                        eq("staDel","0")
                        goodsReceive{
                            eq("id",id)
                        }
                    }
                    def hargaCampur = 0
                    list.each {
                        def klas = KlasifikasiGoods?.findByGoods(it?.goods)
                        if(klas && klas?.franc?.m117NomorAkun?.accountNumber == "1.60.10.01.003"){
                            hargaCampur+=(it?.t167NetSalesPrice?it?.t167NetSalesPrice:0)
                        }
                    }
                    return hargaCampur
                case "1.60.10.01.001" : // PERSEDIAAN PART TOYOTA
                    def list = GoodsReceiveDetail?.createCriteria()?.list {
                        eq("staDel","0")
                        goodsReceive{
                            eq("id",id)
                        }
                    }
                    def hargaToyota = 0
                    list.each {
                        def klas = KlasifikasiGoods?.findByGoods(it?.goods)
                        if(klas && klas?.franc?.m117NomorAkun?.accountNumber == "1.60.10.01.001"){
                            hargaToyota+=(it?.t167NetSalesPrice?it?.t167NetSalesPrice:0)
                        }
                    }
                    return hargaToyota
                default:
                    def valReturn = 0
                    return valReturn
            }
        } else {
            def nilaiReturn = 0;
            if(accountNumber.accountNumber.toLowerCase()=="1.10.10.01.001"){
                if(tipeBayar?.toUpperCase()?.contains("KAS")){
                    nilaiReturn = totalBayar
                }
            }else{
                if(tipeBayar?.toUpperCase()?.contains("BANK")){
                    nilaiReturn = totalBayar
                }
            }
            return nilaiReturn
        }
    }

}
