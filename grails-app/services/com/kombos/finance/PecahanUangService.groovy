package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class PecahanUangService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PecahanUang.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_pecahanUang") {
                ilike("pecahanUang", "%" + (params."sCriteria_pecahanUang" as String) + "%")
            }

            if (params."sCriteria_nilai") {
                eq("nilai", params."sCriteria_nilai".toInteger())
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_cashBalanceDetail") {
                eq("cashBalanceDetail", params."sCriteria_cashBalanceDetail")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    pecahanUang: it.pecahanUang,

                    nilai: it.nilai,

                    lastUpdProcess: it.lastUpdProcess,

                    cashBalanceDetail: it.cashBalanceDetail,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PecahanUang", params.id]]
            return result
        }

        result.pecahanUangInstance = PecahanUang.get(params.id)

        if (!result.pecahanUangInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PecahanUang", params.id]]
            return result
        }

        result.pecahanUangInstance = PecahanUang.get(params.id)

        if (!result.pecahanUangInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PecahanUang", params.id]]
            return result
        }

        result.pecahanUangInstance = PecahanUang.get(params.id)

        if (!result.pecahanUangInstance)
            return fail(code: "default.not.found.message")

        try {
            result.pecahanUangInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        PecahanUang.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.pecahanUangInstance && m.field)
                    result.pecahanUangInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["PecahanUang", params.id]]
                return result
            }

            result.pecahanUangInstance = PecahanUang.get(params.id)

            if (!result.pecahanUangInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.pecahanUangInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.pecahanUangInstance.properties = params

            if (result.pecahanUangInstance.hasErrors() || !result.pecahanUangInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PecahanUang", params.id]]
            return result
        }

        result.pecahanUangInstance = new PecahanUang()
        result.pecahanUangInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.pecahanUangInstance && m.field)
                result.pecahanUangInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["PecahanUang", params.id]]
            return result
        }

        result.pecahanUangInstance = new PecahanUang(params)

        if (result.pecahanUangInstance.hasErrors() || !result.pecahanUangInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def pecahanUang = PecahanUang.findById(params.id)
        if (pecahanUang) {
            pecahanUang."${params.name}" = params.value
            pecahanUang.save()
            if (pecahanUang.hasErrors()) {
                throw new Exception("${pecahanUang.errors}")
            }
        } else {
            throw new Exception("PecahanUang not found")
        }
    }

}