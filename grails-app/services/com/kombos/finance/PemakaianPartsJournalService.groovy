package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.generatecode.GenerateCodeService
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.PartInv
import com.kombos.maintable.Settlement
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.PartFifo
import com.kombos.parts.SalesOrder
import com.kombos.parts.SalesOrderDetail
import com.kombos.reception.Reception
import org.springframework.web.context.request.RequestContextHolder

class PemakaianPartsJournalService {
    boolean transactional = false
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService = new DatatablesUtilService()
    def createJournal(params) {
        /*  params :
        Name
        noInvoice
    */
        def totParts = 0
        def totBahan = 0
        def toyota = 0
        def campuran = 0
        def reception = Reception.findByT401NoWOAndStaDel(params?.noWo,"0")
        def invoices = InvoiceT701.findAllByT701NoInvIlikeAndReceptionAndT701StaDel("%"+params?.noInv+"%",reception,"0");

        invoices.each {
            boolean proses = true
            if((it.t701StaApprovedReversal && (it?.t701StaApprovedReversal=="0" || it?.t701StaApprovedReversal=="2")) || it?.t701NoInv_Reff){
                proses = false
            }
            if(proses){
                def partInvoice = PartInv.findAllByInvoiceAndT703StaDel(it,"0")
                partInvoice.each {
                    def qty = it?.t703Jumlah1
                    def partFifo = PartFifo.findAllByCompanyDealerAndWo_doAndGoods(params?.companyDealer,it?.invoice?.reception?.id,it?.goods);
                    def harga = 0
                    def klas = KlasifikasiGoods.findByGoods(it.goods)
                    if(partFifo.size()>0){
                        def partCalled = 0
                        partFifo.each {
                            harga = it?.hargaBeli
                            if(partCalled<=qty){
                                if(klas){
                                    if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL")){
                                        totBahan+=Math.round(harga*it?.qtyPakai)
                                    }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                                        totBahan+=Math.round(harga*it?.qtyPakai)
                                    }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("TOYOTA")){
                                        totParts+=Math.round(harga*it?.qtyPakai)
                                        toyota+=Math.round(harga*it?.qtyPakai)
                                    }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN")){
                                        totParts+=Math.round(harga*it?.qtyPakai)
                                        campuran+=Math.round(harga*it?.qtyPakai)
                                    }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                                        totParts+=Math.round(harga*it?.qtyPakai)
                                        campuran+=Math.round(harga*it?.qtyPakai)
                                    }else {
                                        totBahan+=Math.round(harga*it?.qtyPakai)
                                    }
                                }else {
                                    totBahan+=Math.round(harga*it?.qtyPakai)
                                }
                                partCalled+=it?.qtyPakai
                            }
                        }
                    }else{
                        harga = it.t703LandedCost
                        if(klas){
                            if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL")){
                                totBahan+=Math.round(harga*it?.t703Jumlah1)
                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                                totBahan+=Math.round(harga*it?.t703Jumlah1)
                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("TOYOTA")){
                                totParts+=Math.round(harga*it?.t703Jumlah1)
                                toyota+=Math.round(harga*it?.t703Jumlah1)
                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN")){
                                totParts+=Math.round(harga*it?.t703Jumlah1)
                                campuran+=Math.round(harga*it?.t703Jumlah1)
                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                                totParts+=Math.round(harga*it?.t703Jumlah1)
                                campuran+=Math.round(harga*it?.t703Jumlah1)
                            }else {
                                totBahan+=Math.round(harga*it?.t703Jumlah1)
                            }
                        }else {
                            totBahan+=Math.round(harga*it?.t703Jumlah1)
                        }
                    }
                }
            }
        }

        def selisih = totParts - (campuran + toyota);
        if(selisih!=0){
            totParts = totParts + selisih;
        }

        Date tgglJurnl = new Date()
        def settle = Settlement.findByInvoiceAndStaDel(invoices?.first(),"0",[sort: 'dateCreated',order: 'desc'])
        if(settle){
            tgglJurnl = settle?.invoice?.t701TglJamInvoice
        }
        if(invoices?.first()?.t701JenisInv=="K"){
            tgglJurnl = invoices?.first()?.t701TglJamInvoice
        }
        def totalBayar = 0

        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = tgglJurnl
        journalInstance.totalDebit = Math.round((totBahan + totParts)?.toString()?.toBigDecimal())
        journalInstance.totalCredit = Math.round((totBahan + totParts)?.toString()?.toBigDecimal())
        journalInstance.docNumber = params?.noInv
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = "Pemakaian Parts / Bahan No Inv. "+params?.noInv + " | No WO. " + invoices?.first()?.reception?.t401NoWO
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJPP1")
            def mappingJournalDetails = mappingJournal?.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber

                totalBayar = getAmount(accountNumber, (mappingJournalDetail.accountTransactionType as String), totParts, totBahan, toyota, campuran)
                if(totalBayar!=0){
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: mappingJournalDetail.accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: "",
                            dateCreated: datatablesUtilService?.syncTime(),
                            lastUpdated: datatablesUtilService?.syncTime()
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                        //println "====> ${rowCount}"
                    }
                }
            }
            //println "Pemakaian Parts Bahan Journal Saved"
        }
    }

    def createJournalPenjualan(params) {
        /*  params :
        Name
        noInvoice
    */

        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def totalBahan = 0
        def totalCampuran = 0
        def totalToyota = 0
        def totalSemua = 0
        def salesOrder = SalesOrder.findBySorNumberIlikeAndStaDelAndCompanyDealer("%" + (params.sorNumber as String) + "%","0",session?.userCompanyDealer)
        def soDetail = SalesOrderDetail.findAllBySorNumberAndStaDel(salesOrder,"0")
        def totalBayar = 0
        soDetail.each {
            def qty = it?.quantity
            def discTemp = it.discount
            def totTemp = 0
            def partFifo = PartFifo.findAllByCompanyDealerAndWo_doAndGoods(params?.companyDealer,it?.sorNumber?.id,it?.materialCode);
            def klas = KlasifikasiGoods.findByGoods(it.materialCode)
            if(partFifo.size()>0){
                def partCalled = 0
                partFifo.each {
                    if(partCalled<=qty){
                        totTemp = Math.round(it?.qtyPakai * it?.hargaBeli)
                        if(klas.franc.m117NamaFranc.toUpperCase().contains("CAMPURAN")){
                            totalCampuran += totTemp
                        }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                            totalCampuran += totTemp
                        }else if(klas.franc.m117NamaFranc.toUpperCase().contains("TOYOTA")){
                            totalToyota += totTemp
                        }else{
                            totalBahan += totTemp
                        }
                        partCalled+=it?.qtyPakai
                    }
                }
            }else{
                def harga = it?.unitPriceBeli ?  it?.unitPriceBeli : it.unitPriceBeli
                totTemp = discTemp && discTemp!=0 ? Math.round(it.quantity * harga - (it.quantity * harga * discTemp / 100)) : Math.round(it.quantity * harga)
                if(klas){
                    if(klas.franc.m117NamaFranc.toUpperCase().contains("CAMPURAN")){
                        totalCampuran += totTemp
                    }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                        totalCampuran+=totTemp
                     }else if(klas.franc.m117NamaFranc.toUpperCase().contains("TOYOTA")){
                        totalToyota += totTemp
                    }else{
                        totalBahan += totTemp
                    }
                }
            }
            totalSemua += totTemp
        }
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = totalSemua
        journalInstance.totalCredit = totalSemua
        journalInstance.docNumber = params.sorNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = "Pemakaian Parts Bahan No. SO "+params.sorNumber
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJPP1")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber

                totalBayar = getAmountPenjualan(accountNumber, (mappingJournalDetail.accountTransactionType as String),totalBahan, totalCampuran, totalToyota)
                if(totalBayar!=0){
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?totalBayar:0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?totalBayar:0,
                            staDel: "0",
                            accountNumber: mappingJournalDetail.accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: "",
                            dateCreated: datatablesUtilService?.syncTime(),
                            lastUpdated: datatablesUtilService?.syncTime()
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                        //println "====> ${rowCount}"
                    }
                }
            }
            //println "Pemakaian Parts Bahan (untuk penjualan) Journal Saved"
        }
    }

    def getMappingJournal() {

    }

    def getAmount(AccountNumber accountNumber, accountTransactionType,def totParts, def totBahan, def toyota, def campuran) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber) {
                case "7.10.10.01.004" :
                    def harga = totParts
                    return harga
                case "7.10.10.01.005" :
                    def harga = totBahan
                    return harga
                default:
                    return 0
            }
        } else {
            switch (accountNumber.accountNumber) {
                case "1.60.10.01.001" :
                    def harga = toyota
                    return harga
                case "1.60.10.01.003" :
                    def harga = campuran
                    return harga
                case "1.60.10.02.001" :
                    def harga = totBahan
                    return harga
                default:
                    return 0
            }
        }

    }

    def getAmountPenjualan(AccountNumber accountNumber, accountTransactionType, def totalBahan, def totalCampuran, def totalToyota) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber) {
                case "7.10.10.01.004" :
                    def harga = 0
                    harga = totalToyota + totalCampuran
                    return harga
                case "7.10.10.01.005" :
                    def harga = 0
                    harga = totalBahan
                    return harga
                default:
                    return 0
            }
        } else {
            switch (accountNumber.accountNumber) {
                case "1.60.10.01.001" :
                    def harga =  totalToyota
                    return harga
                case "1.60.10.01.003" :
                    def harga = 0
                    harga = totalCampuran
                    return harga
                case "1.60.10.02.001" :
                    def harga = 0
                    harga = totalBahan
                    return harga
                default:
                    return 0
            }
        }

    }

}
