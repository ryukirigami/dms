package com.kombos.finance

import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.KlasifikasiGoods
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class JournalPendingMasukService {

    def createJournal(params) {
        /*  params :
            noWo
        */
        def syncTime = new DatatablesUtilService()
        def partsRcp = PartsRCP.createCriteria().list {
            or{
                eq("t403StaTambahKurang","0")
                isNull("t403StaTambahKurang")
            }
            eq("staDel",'0')
            reception{
                eq("t401NoWO",params.noWo as String)
            }
        }
        def totalBayar = 0
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = Math.round(calculateTotal(partsRcp, "debet"))
        journalInstance.totalCredit = Math.round(calculateTotal(partsRcp, "kredit"))
        journalInstance.docNumber = params.noWo
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.dateCreated = syncTime?.syncTime()
        journalInstance.lastUpdated = syncTime?.syncTime()
        journalInstance.description = "Journal Pending Masuk - No So. " + params.noWo
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal  mappingJournal = MappingJournal.findByMappingCode("MAP-TJJPM")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber

                totalBayar = getAmount(accountNumber, partsRcp,(mappingJournalDetail.accountTransactionType as String))

                if(totalBayar!=0){
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: "",
                            dateCreated: syncTime?.syncTime(),
                            lastUpdated: syncTime?.syncTime()
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++

                    }
                }
            }
        }
    }

    def getMappingJournal() {

    }

    def calculateTotal(partsRcp, type) {
        if((type as String).equals("debet")) {
            def harga = 0
            partsRcp.each{
                harga += Math.round(it.t403TotalRp)
            }
            return harga
        } else if((type as String).equals("kredit")) {
            def harga = 0
            partsRcp.each{
                harga += Math.round(it.t403TotalRp)
            }
            return harga
        } else {
            return 0
        }
    }

    def getAmount(accountNumber, partsRcp, accountTransactionType) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber) {
                case "1.60.10.01.004" : //PERSEDIAAN PARTS PENDING
                    def harga = 0
                    partsRcp.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("PARTS")){
                            harga += Math.round(it.t403TotalRp)
                        }
                    }
                    return harga
                case "1.60.10.02.002" :  //PERSEDIAAN BAHAN PENDING
                    def harga = 0
                    partsRcp.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(!klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("PARTS")){
                            harga += Math.round(it.t403TotalRp)
                        }
                    }
                    return harga
                default:
                    return 0
            }
        } else {
            switch (accountNumber.accountNumber) {
                case "1.60.10.01.001" : //persediaan parts toyota
                    def harga = 0
                    partsRcp.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NomorAkun?.accountNumber=="1.60.10.01.001"){
                            harga += Math.round(it.t403TotalRp)
                        }
                    }
                    return harga                   //untuk sementara 0, menunggu mapping source
                case "1.60.10.01.003" : //persediaan parts campuran
                    def harga = 0
                    partsRcp.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NomorAkun?.accountNumber=="1.60.10.01.003"){
                            harga += Math.round(it.t403TotalRp)
                        }
                    }
                    return harga                   //untuk sementara 0, menunggu mapping source
                case "1.60.10.02.001" :  //parts bahan
                    def harga = 0
                    partsRcp.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NomorAkun?.accountNumber=="1.60.10.02.001" ){
                            harga += Math.round(it.t403TotalRp)
                        }
                    }
                    return harga
                default:
                    return 0
            }
        }

    }

}
