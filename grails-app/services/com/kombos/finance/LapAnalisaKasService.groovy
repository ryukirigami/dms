package com.kombos.finance

import com.kombos.parts.Konversi

class LapAnalisaKasService {
    def laporan(def params) {
        Date tgl = new Date().clearTime()
        Date tglKemarin = new Date().clearTime()
        Date tglAwalBulan = new Date().clearTime()
        def companydealer = params.companydealer
        def reportData = new ArrayList();
        if(params.tanggal){
            tgl = new Date().parse("dd-MM-yyyy",params.tanggal)
            tglAwalBulan = new Date().parse("dd-MM-yyyy","01-"+tgl.format("MM-yyyy"))
            tglKemarin = new Date().parse("dd-MM-yyyy",(tgl-1).format("dd-MM-yyyy"))
        }
        def konversi = new Konversi()
        int count = 0
        def SaldoKas = 0, pengeluaran2 = 0,JumlahPenerimaanUang=0
        def UangKasOperasional=0,setoranKemarin=0,total1=0,serviceTunai=0,serviceDebit=0,piutang=0,DPService=0,DPParts=0,Onrisk=0,Lainnya=0
        def uangDariBank=0,Lainnya2=0,jumlahKasOps=0,total2=0,uangCustKemarin=0,terimaCustomerHariIni=0,total3=0,Total4=0,notaSementara=0,notaPengBelumDibuat=0,pengeluaranLain=0
        def total5=0,total6=0,totalKasToday=0,UangKasOperasional2=0,setoranCust=0,saldoKasToday2=0,saldoLHKB=0,total7=0,selisihUangKas=0
        def resultsTotalKemarin = JournalDetail.createCriteria().list {
            eq("staDel","0")
            accountNumber{
                or{
                    eq("accountNumber","1.10.10.01.001",[ignoreCase: true]);
                    ilike("accountNumber","1.10.20.%")
                }
            }
            journal {
                ge("journalDate",tglAwalBulan)
                lt("journalDate",tglKemarin+1)
                eq("staDel","0")
                eq("companyDealer",companydealer)
                eq("isApproval", "1")
            }
        }

        def resultsKemarin = JournalDetail.createCriteria().list {
            eq("staDel","0")
            accountNumber{
                or{
                    eq("accountNumber","1.10.10.01.001",[ignoreCase: true]);
                    ilike("accountNumber","1.10.20.%")
                }
            }
            journal {
                ge("journalDate",tglKemarin)
                lt("journalDate",tglKemarin+1)
                eq("staDel","0")
                eq("companyDealer",companydealer)
                eq("isApproval", "1")
            }
        }
        def resultsHariIni = JournalDetail.createCriteria().list {
            eq("staDel","0")
            accountNumber{
                or{
                    eq("accountNumber","1.10.10.01.001",[ignoreCase: true]);
                    ilike("accountNumber","1.10.20.%")
                }
            }
            journal {
                ge("journalDate",tgl)
                lt("journalDate",tgl+1)
                eq("staDel","0")
                eq("companyDealer",companydealer)
                eq("isApproval", "1")
            }
        }

        def accNumber = AccountNumber.findByAccountNumberAndStaDel("1.10.10.01.001","0");
        def mb = new MonthlyBalanceController()
        String lastPeriod = mb.lastYearMonth(tgl.format("MM").toInteger(),tgl.format("yyyy").toInteger())
        MonthlyBalance mbalance = MonthlyBalance.findByAccountNumberAndYearMonthAndCompanyDealer(accNumber,lastPeriod,companydealer)
        if(mbalance){
            SaldoKas = mbalance.endingBalance

        }
        resultsTotalKemarin.each {

            if(it.accountNumber.accountName.equalsIgnoreCase("KAS")){
                SaldoKas = SaldoKas + it.debitAmount - it.creditAmount
            }
        }

        resultsKemarin.each {
            if(it.accountNumber.accountName.equalsIgnoreCase("KAS") && it.journal.isOperasional=="1"){
                UangKasOperasional += it.debitAmount
            }else{
                setoranKemarin += (it.debitAmount - it.creditAmount)
            }

        }
        UangKasOperasional = SaldoKas - (UangKasOperasional + setoranKemarin)
        resultsHariIni.each {
            if(it.journal.description.toUpperCase().contains("CASH INVOICE") && (it.accountNumber.accountName.equalsIgnoreCase("KAS"))){
                serviceTunai+= it.debitAmount
            }else if(it.journal.description.toUpperCase().contains("CASH INVOICE") && (it.accountNumber.accountName.contains("BANK"))){
                if(!it.accountNumber.accountNumber.contains("1.10.20.01.002")){
                    serviceDebit+= it.debitAmount
                }
            }
            else if(it.journal.description.toUpperCase().contains("PIUTANG")){
                piutang+= it.debitAmount
            }
            else if(it.journal.description.toUpperCase().contains("U.M. PELANGGAN")){
                DPParts+= it.debitAmount
            }
            else if(it.journal.description.toUpperCase().contains("U.M. SERVICE AN ASURANSI")){
                Onrisk+= it.debitAmount
            }
            else if ((it.journal.isOperasional=="0" || !it.journal.isOperasional) && (it.accountNumber.accountName.equalsIgnoreCase("KAS"))){
                Lainnya2+= it.debitAmount
            }
            else if (it.journal.isOperasional=="1" && (it.accountNumber.accountName.equalsIgnoreCase("KAS"))){
                uangDariBank+= it.debitAmount
            }

            if (it.journal.isOperasional=="1" && (it.accountNumber.accountName.equalsIgnoreCase("KAS"))){
                Total4+= it.creditAmount
            }else if (it.accountNumber.accountName.equalsIgnoreCase("KAS")){
                terimaCustomerHariIni+= it.creditAmount
            }


        }

        total1 = (UangKasOperasional + setoranKemarin)
        JumlahPenerimaanUang = serviceTunai + serviceDebit + piutang + DPService + DPParts + Onrisk + Lainnya
        jumlahKasOps = Lainnya2 + uangDariBank
        total2 = JumlahPenerimaanUang + jumlahKasOps
        total3 = terimaCustomerHariIni + uangCustKemarin
        total5 = Total4 + notaSementara + notaPengBelumDibuat + pengeluaranLain
        total6 = total3 + total5
        totalKasToday = SaldoKas + total2 - total6
        saldoLHKB = totalKasToday
        total7 = UangKasOperasional2 + setoranCust - pengeluaran2
        def cariCB = CashBalance.createCriteria().get {
            eq("staDel","0")
            eq("companyDealer",companydealer)
            ge("requiredDate",tgl)
            lt("requiredDate",tgl+1)
            order("dateCreated","desc")
            maxResults(1);
        }
        if(cariCB){
            if(cariCB?.cashBalance==0){
                saldoKasToday2 = totalKasToday
            }else if(cariCB?.cashBalance==totalKasToday){
                saldoKasToday2 = totalKasToday
            }else{
                saldoKasToday2 = totalKasToday - cariCB?.cashBalance
            }
        }
        selisihUangKas =  saldoKasToday2 - totalKasToday
        def data = [:]
        data.put("cabang",companydealer?.m011NamaWorkshop);
        data.put("tanggal","Tgl "+tgl.format("dd MMMM yyyy"));
        data.put("tanggal2", tgl.format("dd-MM-yyyy"))
        data.put("tgglKemarin2", (tgl-1).format("d MMM yyyy"))
        data.put("tgglKemarin", "- Tgl "+(tgl-1).format("dd-MM-yyyy"))
        data.put("kota", companydealer?.kabKota ? companydealer?.kabKota?.m002NamaKabKota+" , " : "")
        data.put("SaldoKas", konversi.toRupiah(SaldoKas))
        data.put("UangKasOperasional", konversi.toRupiah(UangKasOperasional))
        data.put("SetoranPelangganHariKemarin", konversi.toRupiah(setoranKemarin))
        data.put("Total1", konversi.toRupiah(total1))
        data.put("ServiceTunai", konversi.toRupiah(serviceTunai))
        data.put("ServiceKreditAtauDebit", konversi.toRupiah(serviceDebit))
        data.put("Piutang", konversi.toRupiah(piutang))
        data.put("UangMukaService", konversi.toRupiah(DPService))
        data.put("UangMukaPesananParts", konversi.toRupiah(DPParts))
        data.put("OR", konversi.toRupiah(Onrisk))
        data.put("Lainnya", konversi.toRupiah(Lainnya))
        data.put("JumlahPenerimaanUang", konversi.toRupiah(JumlahPenerimaanUang))
        data.put("UangDariBank", konversi.toRupiah(uangDariBank))
        data.put("Lainnya2", konversi.toRupiah(Lainnya2))
        data.put("JumlahpenerimaanKasOperasional", konversi.toRupiah(jumlahKasOps))
        data.put("Total2", konversi.toRupiah(total2))
        data.put("UangDariCustomerKemarin", konversi.toRupiah(uangCustKemarin))
        data.put("terimaCustomerHariIni", konversi.toRupiah(terimaCustomerHariIni))
        data.put("Total3", konversi.toRupiah(total3))
        data.put("Total4", konversi.toRupiah(Total4))
        data.put("NotaSementara", konversi.toRupiah(notaSementara))
        data.put("NotaPengeluaranBelumDibuat", konversi.toRupiah(notaPengBelumDibuat))
        data.put("PengeluaranLainnya", konversi.toRupiah(pengeluaranLain))
        data.put("Total5", konversi.toRupiah(total5))
        data.put("Total6", konversi.toRupiah(total6))
        data.put("SaldoKasHariIni", konversi.toRupiah(totalKasToday))
        data.put("saldoLHKB", konversi.toRupiah(saldoLHKB))
        data.put("StoranCustomer", konversi.toRupiah(setoranCust))
        data.put("Total7", konversi.toRupiah(total7))
        data.put("SaldoSaldoKasHariIni", konversi.toRupiah(saldoKasToday2))
        data.put("SelisihUangKas", konversi.toRupiah(selisihUangKas))
        reportData.add(data)

        return reportData
    }
}
