package com.kombos.finance

import com.kombos.parts.Konversi
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

class EvaluasiTargetService {

    boolean transactional = false

    def sessionFactory
    def konversi = new Konversi()
    def dataTablesReport(def params, def type){
        def row = []
        def uraian = ["PENJUALAN","JASA","PARTS","BAHAN","ANTI KARAT","SPOORING","LAIN-LAIN", "T O T A L",
                "BIAYA- BIAYA", "GAJI MEKANIK",
                "INSENTIF & LEMBUR MEK","GAJI ADM", "INSENTIF ADM","BY. PARTS","BY. BAHAN","PEK. LUAR",
                "BY. SPOOR/OUTS","PERJALANAN","BY. UMUM","BY KOMISI","CLAIM REPAIR","TAK TERDUGA","PENYUSUTAN","SEWA GEDUNG", "TOTAL",
                "GROSS PROFIT","JUMLAH STALL","MEKANIK","REPAIR UNIT","REPAIR ORDER","REPAIR UNIT / MEKANIK","REPAIR UNIT / STALL",
                "SALES/UNIT","LABOUR COST RATIO","PAYMENT RATE","CREDIT PAYMENT RATE"," ","MARGIN PARTS","MARGIN BAHAN"]

        // target bulan ini
        def tbi = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        if(getSize(params,params.bulan,params.tahun,type,'1',false)>0){
            def resultTargetBulanIni = getAllDataEvaluasiTarget(params,params.bulan,params.tahun,type,'1',false)
            tbi = resultData(resultTargetBulanIni)
        }

        //result bulan lalu
        def rbl = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        if(getSize(params,((params.bulan as int)-1).toString(),params.tahun,type,'0',false)>0){
            if(((params.bulan as int)-1) != 0){
                def resultRealisasiBulanLalu = getAllDataEvaluasiTarget(params,((params.bulan as int)-1).toString(),params.tahun,type,'0',false)
                rbl = resultData(resultRealisasiBulanLalu)
            }
        }

        //result bulan ini
        def rbi = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        if(getSize(params,params.bulan,params.tahun,type,'0',false)>0){
            def resultRealisasiBulanIni = getAllDataEvaluasiTarget(params,params.bulan,params.tahun,type,'0',false)
            rbi = resultData(resultRealisasiBulanIni)
        }

        //akumulasi realisasi sampai bulan ini
        def arsbi = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        if (getSize(params,params.bulan,params.tahun,type,'0',true)>0){
            def resultAkumulasiRealisasi = getAllDataEvaluasiTarget(params,params.bulan,params.tahun,type,'0',true)
            arsbi = resultData(resultAkumulasiRealisasi)
        }

        //akumulasi target sampai bulan ini
        def atsbi = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        if(getSize(params,params.bulan,params.tahun,type,'1',true)>0){
            def resultAkumulasiTarget = getAllDataEvaluasiTarget(params,params.bulan,params.tahun,type,'1',true)
            atsbi = resultData(resultAkumulasiTarget)
        }

        //realisasi LY bulan yg sama
        def rbiLY = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        if(getSize(params,params.bulan,((params.tahun as int)-1),type,'0',false)>0){
            def resultRealisasiBulanIniLY = getAllDataEvaluasiTarget(params,params.bulan,((params.tahun as int)-1),type,'0',false)
            rbiLY = resultData(resultRealisasiBulanIniLY)

        }

        //realisasi LY akumulasi
        def rsbiLY = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        if(getSize(params,params.bulan,((params.tahun as int) -1),type,'0',true)>0){
            def resultAkumulasiRealisasiLY = getAllDataEvaluasiTarget(params,params.bulan,((params.tahun as int) -1),type,'0',true)
            rsbiLY = resultData(resultAkumulasiRealisasiLY)
        }

        int a = 0
        //mapping
        def noPenjualan = 0
        def nomor = ""
        for (a=0;a<=38;a++){
            nomor = noPenjualan
            if(a==0 || a==7 || a==8 || a==24 || a>=26){
                nomor = ""
            }
            if(a==9){
                noPenjualan = 1
                nomor = noPenjualan
            }
            if(a==25){
                nomor = "**"
            }
            if(a==0 || a==8 || a==36){
                row <<[
                        column_0: nomor as String,
                        field1 : uraian.get(a), //
                ]
            }else{
                row <<[
                    column_0: nomor as String,
                    field1 : uraian.get(a), //
                    field2 : konversi.toRupiah(tbi.get(a)) , //target bulan ini
                    field3 : konversi.toRupiah(rbl.get(a)), // realisasi bulan lalu
                    field4 : konversi.toRupiah(rbi.get(a)), //realisasi bulan ini
                    field5 : tbi.get(a)==0?"0": (rbi.get(a)/tbi.get(a)).setScale(2,BigDecimal.ROUND_HALF_UP),   // r:t %
                    field6 : (rbl.get(a)<rbi.get(a))?rbl.get(a)==0?"0":((rbi.get(a)/rbl.get(a)*100)-100).setScale(2,BigDecimal.ROUND_HALF_UP):" ",   // real bulan ini/lalu - naik %
                    field7 : (rbl.get(a)>rbi.get(a))?rbl.get(a)==0?"0":(100-(rbi.get(a)/rbl.get(a)*100)).setScale(2,BigDecimal.ROUND_HALF_UP):" ",  // real bulan ini/lalu - turun %
                    field8 : konversi.toRupiah(atsbi.get(a)), //akumulasi target
                    field9 : konversi.toRupiah(arsbi.get(a)), //akumulasi realisasi
                    field10 : atsbi.get(a)==0?"0":(arsbi.get(a) / atsbi.get(a)).setScale(2,BigDecimal.ROUND_HALF_UP), // r:t %
                    field11 : konversi.toRupiah(rbiLY.get(a)), //realisasi bulan yg sama LY
                    field12 : konversi.toRupiah(rsbiLY.get(a)) //realisasi akumulasi LY
                ]
            }
            noPenjualan++
        }


        return row
    }

    def getSize(def params, def bulan, def tahun, def type, def isTarget,def akumulasi){
        if((bulan as int)==0){
            bulan = "12"
            tahun = (tahun as int) - 1
        }
        if((bulan as int) < 10)
            bulan = "0" + bulan

        def monthYear = bulan+""+tahun
        if(akumulasi==true){
            monthYear = akumulasiMonthYear(bulan,tahun)
        }
        def c = MasterEvaluasiTarget.createCriteria()
        def results = c.list {
            companyDealer{
                eq("id", params.companyDealer as Long)
            }
            inList("monthYear",monthYear)
            if(type!="GAB"){
                eq("serviceType",type)
            }
            eq("istarget",isTarget as String)
        }
        return results.size()
    }
    def getAllDataEvaluasiTarget(def params, def bulan, def tahun, def type, def isTarget,def akumulasi){
        if((bulan as int)==0){
            bulan = "12"
            tahun = (tahun as int) - 1
        }
        if((bulan as int) < 10)
            bulan = "0" + bulan

        def monthYear = bulan+""+tahun
        if(akumulasi==true){
            monthYear = akumulasiMonthYear(bulan,tahun)
        }
        def c = MasterEvaluasiTarget.createCriteria()
        def results = c.list {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                sum("parts","parts")
                sum("jasa","jasa")
                sum("bahan","bahan")
                sum("spooring","spooring")
                sum("antikarat","antikarat")
                sum("lainLain","lainLain")
                sum("gajiMekanik","gajiMekanik")
                sum("gajiMekanikLainLain","gajiMekanikLainLain")
                sum("insentifLemburMek","insentifLemburMek")
                sum("gajiAdm","gajiAdm")
                sum("gajiAdmLainLain","gajiAdmLainLain")
                sum("insentifAdm","insentifAdm")
                sum("biayaParts","biayaParts")
                sum("biayaBahan","biayaBahan")
                sum("biayaPekLuar","biayaPekLuar")
                sum("biayaSpooring","biayaSpooring")
                sum("biayaPerjalanan","biayaPerjalanan")
                sum("biayaUmum","biayaUmum")
                sum("biayaKomisi","biayaKomisi")
                sum("claimRepair","claimRepair")
                sum("biayaPenyusutan","biayaPenyusutan")
                sum("biayaTakTerduga","biayaTakTerduga")
                sum("biayaSewaGedung","biayaSewaGedung")
                sum("jumlahStall","jumlahStall")
                sum("mekanik","mekanik")
                sum("repairUnit","repairUnit")
                sum("repairOrder","repairOrder")
                sum("paymentRate","paymentRate")
                sum("creditPaymentRate","creditPaymentRate")
            }
            companyDealer{
                eq("id", params.companyDealer as Long)
            }
            inList("monthYear",monthYear)
            if(type!="GAB"){
                eq("serviceType",type)
            }
            eq("istarget",isTarget as String)
        }
        return results
    }
    def resultData(def result){
        def arrData = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        def total1=0;
        def total2=0
        result.each {
            arrData.putAt(1,it.jasa);arrData.putAt(2,it.parts);arrData.putAt(3,it.bahan);arrData.putAt(4,it.antikarat);arrData.putAt(5,it.spooring);arrData.putAt(6,it.lainLain)
            total1 = it.jasa + it.parts + it.bahan + it.antikarat +  it.spooring + it.lainLain
            arrData.putAt(7,total1)
            arrData.putAt(9,it.gajiMekanik);arrData.putAt(10,it.insentifLemburMek);arrData.putAt(11,it.gajiAdm);
            arrData.putAt(12,it.insentifAdm);arrData.putAt(13,it.biayaParts);arrData.putAt(14,it.biayaBahan);
            arrData.putAt(15,it.biayaPekLuar);arrData.putAt(16,it.biayaSpooring);arrData.putAt(17,it.biayaPerjalanan);arrData.putAt(18,it.biayaUmum);
            arrData.putAt(19,it.biayaKomisi);arrData.putAt(20,it.claimRepair);arrData.putAt(21,it.biayaTakTerduga);arrData.putAt(22,it.biayaPenyusutan);
            arrData.putAt(23,it.biayaSewaGedung);
            total2 = it.gajiMekanik + it.gajiMekanikLainLain + it.insentifLemburMek +  it.gajiAdm +
                    it.gajiAdmLainLain +  it.insentifAdm + it.biayaParts + it.biayaBahan +
                    it.biayaPekLuar +  it.biayaSpooring +  it.biayaPerjalanan +  it.biayaUmum +
                    it.biayaKomisi +  it.claimRepair +  it.biayaTakTerduga + it.biayaPenyusutan +
                    it.biayaSewaGedung
            arrData.putAt(24,total2)
            arrData.putAt(25,total1-total2)
            arrData.putAt(26,it.jumlahStall);arrData.putAt(27,it.mekanik);arrData.putAt(28,it.repairUnit);arrData.putAt(29,it.repairOrder);
            it.mekanik==0?0:arrData.putAt(30,it.repairUnit/(it.mekanik));
            it.jumlahStall==0?0:arrData.putAt(31,it.repairUnit/(it.jumlahStall));
            it.repairUnit==0?0:arrData.putAt(32,(arrData.get(7)/it.repairUnit)*1000);
            it.jasa==0?0:arrData.putAt(33,(it.gajiMekanik+it.gajiMekanikLainLain+it.insentifLemburMek+it.gajiAdm+it.gajiAdmLainLain+it.insentifAdm)/it.jasa)
            //println it.gajiMekanik+" "+it.gajiMekanikLainLain+" "+it.insentifLemburMek+" "+it.gajiAdm+" "+it.gajiAdmLainLain+it.insentifAdm
            //println it.jasa
            //println("Hasil Penjumlahan " + (it.gajiMekanik+it.gajiMekanikLainLain+it.insentifLemburMek+it.gajiAdm+it.gajiAdmLainLain+it.insentifAdm))
            //println("Hasil Pembagian " + (it.gajiMekanik+it.gajiMekanikLainLain+it.insentifLemburMek+it.gajiAdm+it.gajiAdmLainLain+it.insentifAdm)/it.jasa)
            arrData.putAt(34,it.paymentRate)
            arrData.putAt(35,it.creditPaymentRate)
            it.biayaParts==0?0:arrData.putAt(37,(((it.parts - it.biayaParts)/ it.biayaParts) * 100))
            it.biayaBahan==0?0:arrData.putAt(38,(((it.bahan - it.biayaBahan) / it.biayaBahan) * 100))

        }
        return arrData
    }
    ArrayList akumulasiMonthYear(def bulan, def tahun){
        def monthYear = []
        def bul = ""
        for(int a=1;a<=(bulan as int);a++){
            bul = a
            if(a < 10){
                bul = "0" + bul
            }
            monthYear.add(bul+""+tahun)
        }

        return monthYear
    }

    def dataTablesReportLastYear(def params,def tahun, def type, def isTarget){
        def row = []
        def dataBulanKe = [[]]
        def total = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        for(int b=1;b<=12;b++){
            dataBulanKe[b] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            if(getSize(params,b,tahun,type,isTarget,false)>0){
                def bulan = getDataLastYear(params,b,tahun,type,isTarget)
                dataBulanKe[b] = resultData(bulan)
            }

        }
        int a = 0
        //mapping
        for (a=0;a<=38;a++){
            total[a] = dataBulanKe[1].get(a)+dataBulanKe[2].get(a)+dataBulanKe[3].get(a)+dataBulanKe[4].get(a)+dataBulanKe[5].get(a)+
                    dataBulanKe[6].get(a)+dataBulanKe[7].get(a)+dataBulanKe[8].get(a)+dataBulanKe[9].get(a)+dataBulanKe[10].get(a)+
                    dataBulanKe[11].get(a) + dataBulanKe[12].get(a)
            if(a==0 || a==8 || a==34){
                row <<[
                        field1 : "", //
                ]
            }else{
                row <<[
                        field1 : konversi.toRupiah(dataBulanKe[1].get(a)),
                        field2 : konversi.toRupiah(dataBulanKe[2].get(a)),
                        field3 : konversi.toRupiah(dataBulanKe[3].get(a)),
                        field4 : konversi.toRupiah(dataBulanKe[4].get(a)),
                        field5 : konversi.toRupiah(dataBulanKe[5].get(a)),
                        field6 : konversi.toRupiah(dataBulanKe[6].get(a)),
                        field7 : konversi.toRupiah(dataBulanKe[7].get(a)),
                        field8 : konversi.toRupiah(dataBulanKe[8].get(a)),
                        field9 : konversi.toRupiah(dataBulanKe[9].get(a)),
                        field10 : konversi.toRupiah(dataBulanKe[10].get(a)),
                        field11 : konversi.toRupiah(dataBulanKe[11].get(a)),
                        field12 : konversi.toRupiah(dataBulanKe[12].get(a)),
                        column_0 : konversi.toRupiah(total.get(a))

                ]
            }
        }
        return row
    }
    def getDataLastYear(def params,def bulan, def tahun,  def type, def isTarget){

        def monthYear = bulan+""+tahun

        def c = MasterEvaluasiTarget.createCriteria()
        def results = c.list {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                sum("parts","parts")
                sum("jasa","jasa")
                sum("bahan","bahan")
                sum("spooring","spooring")
                sum("antikarat","antikarat")
                sum("lainLain","lainLain")
                sum("gajiMekanik","gajiMekanik")
                sum("gajiMekanikLainLain","gajiMekanikLainLain")
                sum("insentifLemburMek","insentifLemburMek")
                sum("gajiAdm","gajiAdm")
                sum("gajiAdmLainLain","gajiAdmLainLain")
                sum("insentifAdm","insentifAdm")
                sum("biayaParts","biayaParts")
                sum("biayaBahan","biayaBahan")
                sum("biayaPekLuar","biayaPekLuar")
                sum("biayaSpooring","biayaSpooring")
                sum("biayaPerjalanan","biayaPerjalanan")
                sum("biayaUmum","biayaUmum")
                sum("biayaKomisi","biayaKomisi")
                sum("claimRepair","claimRepair")
                sum("biayaPenyusutan","biayaPenyusutan")
                sum("biayaTakTerduga","biayaTakTerduga")
                sum("biayaSewaGedung","biayaSewaGedung")
                sum("jumlahStall","jumlahStall")
                sum("mekanik","mekanik")
                sum("repairUnit","repairUnit")
                sum("repairOrder","repairOrder")
                sum("paymentRate","paymentRate")
                sum("creditPaymentRate","creditPaymentRate")
            }
            companyDealer{
                eq("id", params.companyDealer as Long)
            }
            inList("monthYear",monthYear)
            if(type!="GAB"){
                eq("serviceType",type)
            }
            eq("istarget",isTarget as String)
        }
        return results
    }
}
