package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.generatecode.GenerateCodeService
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.PartInv
import com.kombos.maintable.Settlement
import com.kombos.parts.KlasifikasiGoods
import com.kombos.reception.Appointment
import com.kombos.reception.JobInv
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class InvoiceTunaiJournalService {
    boolean transactional = false
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService
    def createJournal(params) {
        def totKas = 0
        def totBank = 0
        BankAccountNumber accBank = null
        Date tgglJurn = new Date()

        def inv = InvoiceT701.get(params?.id?.toLong())
        def settle = Settlement.createCriteria().list {
            eq("staDel","0")
            invoice{
                eq("id",params?.id?.toLong())
            }
            order("dateCreated","desc");
        }

        settle.each {
            if(it?.metodeBayar?.m701MetodeBayar?.toUpperCase()?.contains("CASH")){
                totKas+=it.t704JmlBayar
            }else{
                totBank+=it.t704JmlBayar
                accBank = BankAccountNumber.findByBank(it.bank)
            }
            tgglJurn = it?.t704TglJamSettlement
        }
        def totalBayar = 0

        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }

        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalDate = inv?.t701TglJamInvoice
        journalInstance.totalDebit = Math.round(calculateTotal(inv, "debet"))
        journalInstance.totalCredit = Math.round(calculateTotal(inv, "kredit"))
        journalInstance.docNumber = inv?.t701NoInv
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = "Cash Invoice No Inv. "+inv?.t701NoInv+" | "+
                        inv?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan +
                        " | an. "+inv?.t701Customer + " |  No WO. "+inv?.reception?.t401NoWO
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJINVT")

            def mappingJournalDetails = mappingJournal?.mappingJournalDetail
            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail?.accountNumber

                def subType = null
                def subLedger = null
                if(it?.accountNumber?.accountNumber?.trim()=="4.10.20.01.001" || it?.accountNumber?.accountNumber?.trim()=="4.10.20.02.001"){
                    def cari = Reception?.findByIdAndStaDel(inv?.reception.id,"0")

                    if(cari){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER","0")
                        subLedger = cari?.historyCustomer?.id
                    }
                }

                totalBayar = getAmount(accountNumber, inv,(mappingJournalDetail.accountTransactionType as String),totKas,totBank)
                if(totalBayar!=0){
                    if(accountNumber.accountNumber.equals("1.10.20.01.001")){
                        accountNumber = accBank?.accountNumber
                    }
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: subLedger,
                            subType: subType,
                            dateCreated: tgglJurn,
                            lastUpdated: tgglJurn
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                    }
                }
            }
            //println "Cash Invoice Journal Saved"
        }
    }

    def getMappingJournal() {

    }

    def calculateTotal(InvoiceT701 invoice, type) {
        if((type as String).equals("debet")) {
            return (invoice.t701TotalBayarRp + invoice.t701BookingFee + (invoice?.t701TotalDiscRp?invoice?.t701TotalDiscRp:0) + 0 + 0)
        } else if((type as String).equals("kredit")) {
            return (invoice.t701JasaRp + invoice.t701PartsRp + invoice.t701MaterialRp + invoice.t701OliRp +
                    invoice?.t701SubletRp + invoice.t701AdmRp + (invoice?.t701PPnRp?invoice?.t701PPnRp:0) + invoice?.t701MateraiRp)
        } else {
            return 0
        }
    }

    def getAmount(AccountNumber accountNumber,InvoiceT701 invoice, accountTransactionType,def totKas, def totBank) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber.toLowerCase()) {
                case "1.10.10.01.001" :
                    return totKas
                case "1.10.20.01.001" :
                    return totBank
                case "6.10.30.01.001" :
                    def harga = 0
                    def jobInv = JobInv.findAllByInvoiceAndStaDel(invoice,"0")
                    jobInv.each {
                        harga += (it?.t702DiscRp ? it?.t702DiscRp : 0)

                    }

                    return harga                    //untuk sementara 0, menunggu mapping source
                case "6.10.30.01.002" :
                    def harga = 0
                    def parts = PartInv.findAllByInvoiceAndT703StaDel(invoice,"0")
                    parts.each {
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(!klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL")
                                && !klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("BAHAN")
                                && !klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                            harga += (it.t703DiscRp ? it.t703DiscRp : 0)
                        }
                    }
                    return harga                    //untuk sementara 0, menunggu mapping source
                case "6.10.30.01.003" :
                    def harga = 0
                    def parts = PartInv.findAllByInvoiceAndT703StaDel(invoice,"0")
                    parts.each {
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL")
                                || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("BAHAN")||
                                klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                            harga += it.t703DiscRp ? it.t703DiscRp : 0
                        }
                    }
                    return harga                   //untuk sementara 0, menunggu mapping source
                case "4.10.20.01.001" :
                    def harga = 0
                    if(invoice?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.equalsIgnoreCase("GR")){
                        harga = ((invoice?.t701BookingFee ? invoice?.t701BookingFee : 0) + (invoice?.t701OnRisk ? invoice?.t701OnRisk : 0))
                    }
                    return harga
                case "4.10.20.02.001" :
                    def harga = 0
                    if(invoice?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.equalsIgnoreCase("BP")){
                        harga = ((invoice?.t701BookingFee ? invoice?.t701BookingFee : 0) + (invoice?.t701OnRisk ? invoice?.t701OnRisk : 0))
                    }
                    return harga
                default:
                    def valReturn = 0
                    return valReturn
            }
        } else {
            switch (accountNumber.accountNumber) {
                case "6.10.10.01.001" :
                    return invoice?.t701JasaRp
                case "6.10.10.02.001" :
                    return invoice?.t701PartsRp
                case "6.10.10.03.001" :
                    return invoice?.t701MaterialRp + invoice?.t701OliRp
                case "6.10.10.04.001" :
                    return invoice?.t701SubletRp
                case "6.10.10.05.001" :
                    return invoice?.t701AdmRp
                case "4.20.10.01.001" :
                    return invoice?.t701PPnRp
                case "8.10.10.04.001" :
                    return invoice?.t701MateraiRp
                default:
                    return 0
            }
        }

    }
}
