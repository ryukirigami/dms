package com.kombos.finance

import com.kombos.administrasi.Bank
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.generatecode.GenerateCodeService
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class UMServicePelangganJournalService {
    boolean transactional = true
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService = new DatatablesUtilService()

    def createJournal(params) {
        //println params
        /*  params :
            Name
            tipeBayar           KAS or BANK
            namaBank            id bank, nullable
            kodeJurnal
            jenisWo
            docNumber           No dokumen referensi
            totalBiaya          Total biaya transaksi
        */
        def journalCode = new GenerateCodeService().generateJournalCode("TJ")
        try {
            if(params.journalCode){
                journalCode = params.journalCode
            }
        }catch(Exception e){

        }
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = journalCode
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.totalCredit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        journalInstance.description = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                "U.M. Pelanggan "+params.docNumber:"U.M. Pelanggan (Bank) "+params.docNumber
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                Bank bank = Bank.get(params.idBank.toLong())
                //println "BANK " + bank
                bankAccountNumber = BankAccountNumber.findByBank(bank)
                //println "BANK ACCOUNT NUMBER = "+bankAccountNumber
                if (!bankAccountNumber) {
                    return "Error"
                }
            }

            MappingJournal mappingJournal = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                    MappingJournal.findByMappingCode("MAP-TJUMP"):MappingJournal.findByMappingCode("MAP-TJBUMP")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail
            //println "SIZE : "+mappingJournalDetails.size()
            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                    accountNumber = bankAccountNumber.accountNumber
                }
                def subType = null
                def subLedger = null
                if(it?.accountNumber?.accountNumber?.trim()=="4.10.20.01.001"){
                    if(params.jenisWo=="BP"){
                        accountNumber = AccountNumber.findByAccountNumber("4.10.20.02.001");
                    }
                    def cari = Reception?.findByT401NoWOIlikeAndStaDel("%"+params?.docNumber,"0")
                    if(!cari){
                        cari = Reception?.findByT401NoAppointmentIlikeAndStaDel("%"+params?.docNumber,"0")
                    }
                    if(cari){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER","0")
                        subLedger = cari?.historyCustomer?.id
                    }else{
                        cari = Reception?.findByT401NoAppointmentIlikeAndStaDel("%"+params?.docNumber,"0")
                        if(cari){
                            def cari2 = Appointment.findByReceptionAndStaDelAndStaSave(cari,"0","0");
                            subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER","0")
                            subLedger = cari2?.historyCustomer?.id
                        }
                    }
                }

                def journalDetail = new JournalDetail(
                        journalTransactionType:journalType.journalType,
                        creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Kredit")?Math.round(params.totalBiaya):0,
                        debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Debet")?Math.round(params.totalBiaya):0,
                        staDel: "0",
                        accountNumber: accountNumber,
                        journal: journalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime(),
                        subLedger: subLedger,
                        subType:subType
                )
                if (journalDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                    //println "====> ${rowCount}"
                }
            }
            //println "Uang Muka Service an Pelanggan Journal Saved"
        }
        return journalInstance
    }

    def getMappingJournal() {

    }

}
