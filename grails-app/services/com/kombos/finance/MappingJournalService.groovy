package com.kombos.finance



import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON

class MappingJournalService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def datatablesUtilService
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = MappingJournal.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_mappingCode"){
				ilike("mappingCode","%" + (params."sCriteria_mappingCode" as String) + "%")
			}

			if(params."sCriteria_mappingName"){
				ilike("mappingName","%" + (params."sCriteria_mappingName" as String) + "%")
			}

            if(params."sCriteria_description"){
                ilike("description","%" + (params."sCriteria_description" as String) + "%")
            }


            switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
						id: it.id,
						mappingCode: it.mappingCode,
						mappingName: it.mappingName,
                        description: it?.description,
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}

    def accountNumberList(def params) {
        def dtRender = [:]
        def accounts = AccountNumber.list([
                max: params.iDisplayLength,
                offset: params.iDisplayStart
        ])

                        //println params
        dtRender.aaData = []
        dtRender.iTotalRecords = accounts.size()
        dtRender.iTotalDisplayRecords = dtRender.iTotalRecords
        dtRender.iDisplayLength = params.iDisplayLength
        dtRender.iDisplayStart = params.iDisplayStart

        accounts?.each { a ->
            dtRender.aaData << [
                    "id": a.id,
                    "accountName": a.accountName
            ]
        }

        return dtRender
    }
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MappingJournal", params.id] ]
			return result
		}

		result.mappingJournalInstance = MappingJournal.get(params.id)

		if(!result.mappingJournalInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MappingJournal", params.id] ]
			return result
		}

		result.mappingJournalInstance = MappingJournal.get(params.id)

		if(!result.mappingJournalInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MappingJournal", params.id] ]
			return result
		}

		result.mappingJournalInstance = MappingJournal.get(params.id)

		if(!result.mappingJournalInstance)
			return fail(code:"default.not.found.message")

		try {
			result.mappingJournalInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		MappingJournal.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.mappingJournalInstance && m.field)
					result.mappingJournalInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["MappingJournal", params.id] ]
				return result
			}
			result.mappingJournalInstance = MappingJournal.get(params.id)

            if (result.mappingJournalInstance) {
                //println "we are on update" + params
                def jsonArray = JSON.parse(params.mappingDetails)
                def rowCount = 0
                jsonArray.each {d ->
                    def mappingDetail
                    if (d.detailId)
                        mappingDetail = MappingJournalDetail.findById(d.detailId as Long)
                    else {
                        mappingDetail = new MappingJournalDetail()
                        mappingDetail.mappingJournal = result.mappingJournalInstance
                        mappingDetail.nomorUrut = d.nomorUrut as int
                        mappingDetail.staDel = "0"
                        mappingDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        mappingDetail.lastUpdProcess = "INSERT"
                        mappingDetail.dateCreated = datatablesUtilService?.syncTime()
                        mappingDetail?.lastUpdated = datatablesUtilService?.syncTime()
                    }
                    def accountNumber = AccountNumber.findById(Integer.parseInt(d.id as String))
                    mappingDetail.lastUpdated = datatablesUtilService?.syncTime()
                    mappingDetail.accountNumber = accountNumber
                    mappingDetail.lastUpdProcess = "Update"
                    mappingDetail.accountTransactionType = d.accountTypeTransaction
                    if (mappingDetail.save(failOnError: true))
                        rowCount++
                }
                //println "total updated detail row is => ${rowCount}"
            }

			if(!result.mappingJournalInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.mappingJournalInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.mappingJournalInstance.properties = params

			if(result.mappingJournalInstance.hasErrors() || !result.mappingJournalInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["MappingJournal", params.id] ]
			return result
		}
        if (params.id)
            result.mappingJournalInstance = MappingJournal.findById(Integer.parseInt(params.id as String))
        else
		    result.mappingJournalInstance = new MappingJournal()
		result.mappingJournalInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.mappingJournalInstance && m.field)
				result.mappingJournalInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["MappingJournal", params.id] ]
			return result
		}
        result.mappingJournalInstance = new MappingJournal()
        result.mappingJournalInstance.mappingCode = params.mappingCode
        result.mappingJournalInstance.mappingName = params.mappingName
        result.mappingJournalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.mappingJournalInstance.lastUpdProcess = "INSERT"
        result.mappingJournalInstance.description = "DESCRIPTION_"
        result.mappingJournalInstance.staDel = 0
        result.mappingJournalInstance.dateCreated = datatablesUtilService?.syncTime()
        result.mappingJournalInstance.lastUpdated = datatablesUtilService?.syncTime()
        result.mappingJournalInstance.save()

        if(result.mappingJournalInstance.hasErrors() || !result.mappingJournalInstance.save(flush: true))
            return fail(code:"default.not.created.message")
        else {
            // jika sukses
            // bikin detail mapping journal nya sebanyak row
            def jsonArray = JSON.parse(params.mappingDetails)
            def rowCount = 0
            jsonArray.each {d ->
                def an = AccountNumber.findById(Integer.parseInt(d.id as String))
                def mjDetail = new MappingJournalDetail(
                        nomorUrut: d.nomorUrut,
                        accountTransactionType: d.accountTypeTransaction,
                        staDel: 0,
                        accountNumber: an,
                        mappingJournal: result.mappingJournalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime()
                )
                if (mjDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                }
            }
            result.rowCount = rowCount
        }

        // success
		return result
	}
	
	def updateField(def params){
		def mappingJournal =  MappingJournal.findById(params.id)
		if (mappingJournal) {
			mappingJournal."${params.name}" = params.value
			mappingJournal.save()
			if (mappingJournal.hasErrors()) {
				throw new Exception("${mappingJournal.errors}")
			}
		}else{
			throw new Exception("MappingJournal not found")
		}
	}

    def int[] parameterSplit(params) {
        def aId = []
        def sId = params.ids as String
        for(tId in sId.split(",")) {
            try {
                def id = tId.replace("[","")
                        .replace("]","")
                        .replace("\"","")
                        .trim()
                aId.add(Integer.parseInt(id))
            } catch (Exception e) {
                return null
            }
        }
        return aId
    }

}