package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.generatecode.GenerateCodeService
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class RefundUangMukaJournalService {
    boolean transactional = true
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService = new DatatablesUtilService()

    def createJournal(params) {
        //println params
        /*  params :
            Name
            tipeBayar           KAS or BANK
            namaBank            id bank, nullable
            docNumber           No dokumen referensi
            totalBiaya          Total biaya transaksi
        */

        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.totalCredit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                "Pengembalian Uang Muka "+params.judulJurnal:"Pengembalian Uang Muka (Bank) "+params.judulJurnal
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)
        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                bankAccountNumber = BankAccountNumber.get(params?.idBank?.toLong())
                if (!bankAccountNumber) {
                    return "Error"
                }
            }

            MappingJournal mappingJournal = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                    MappingJournal.findByMappingCode("MAP-TJRUM"):MappingJournal.findByMappingCode("MAP-TJBRUM")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail
            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                    accountNumber = bankAccountNumber.accountNumber
                }
                SubType subType = null
                if(accountNumber?.accountNumber?.equalsIgnoreCase("4.10.20.01.001")){
                    subType = SubType.findByDescriptionIlikeAndStaDel("%CUSTOMER","0")
                }
                def journalDetail = new JournalDetail(
                        journalTransactionType:journalType.journalType,
                        creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Kredit")?Math.round(params.totalBiaya):0,
                        debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Debet")?Math.round(params.totalBiaya):0,
                        staDel: "0",
                        accountNumber: accountNumber,
                        journal: journalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        subLedger: params.subLedger ? params.subLedger : "",
                        dateCreated : datatablesUtilService?.syncTime(),
                        lastUpdated : datatablesUtilService?.syncTime(),
                        subType: subType
                )
                if (journalDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                    //println "====> ${rowCount}"
                }
            }
            //println "Pengembalian Uang Muka Journal Saved"
        }
    }

    def getMappingJournal() {

    }

//    def generateJournalCode(params) {
//        Date date = new Date()
//        SimpleDateFormat dateFormat = new SimpleDateFormat("MMYYYY")
//        String dateString = dateFormat.format(date)
//        String prefix = params
//        String rpadding = "000000"
//        def lastJournal = Journal.last()
//        Long id = 1
//        if (lastJournal) {
//            id = lastJournal.id +1
//        }
//        return prefix + dateString + rpadding + String.valueOf(id)
//    }
}
