package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class MappingJournalDetailService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = MappingJournalDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_nomorUrut") {
                eq("nomorUrut", params."sCriteria_nomorUrut")
            }

            if (params."sCriteria_accountTransactionType") {
                ilike("accountTransactionType", "%" + (params."sCriteria_accountTransactionType" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_accountNumber") {
                accountNumber {
                    ilike("accountNumber", "%" + params."sCriteria_accountNumber" + "%")
                }
            }

            if (params."sCriteria_mappingJournal") {
                mappingJournal {
                    ilike("mappingCode", "%" + params."sCriteria_mappingJournal" + "%")
                }
            }

//            order("nomorUrut")
            order("accountTransactionType")
            accountNumber{
                order("accountNumber");
            }
//            switch (sortProperty) {
//                default:
//                    order(sortProperty, sortDir)
//                    break;
//            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    nomorUrut: it.nomorUrut,

                    accountTransactionType: it.accountTransactionType,

                    lastUpdProcess: it.lastUpdProcess,

                    accountNumber: it.accountNumber.accountNumber,

                    mappingJournal: it.mappingJournal.mappingCode,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MappingJournalDetail", params.id]]
            return result
        }

        result.mappingJournalDetailInstance = MappingJournalDetail.get(params.id)

        if (!result.mappingJournalDetailInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MappingJournalDetail", params.id]]
            return result
        }

        result.mappingJournalDetailInstance = MappingJournalDetail.get(params.id)

        if (!result.mappingJournalDetailInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MappingJournalDetail", params.id]]
            return result
        }

        result.mappingJournalDetailInstance = MappingJournalDetail.get(params.id)

        if (!result.mappingJournalDetailInstance)
            return fail(code: "default.not.found.message")

        try {
            result.mappingJournalDetailInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        MappingJournalDetail.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.mappingJournalDetailInstance && m.field)
                    result.mappingJournalDetailInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["MappingJournalDetail", params.id]]
                return result
            }

            result.mappingJournalDetailInstance = MappingJournalDetail.get(params.id)

            if (!result.mappingJournalDetailInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.mappingJournalDetailInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.mappingJournalDetailInstance.properties = params

            if (result.mappingJournalDetailInstance.hasErrors() || !result.mappingJournalDetailInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["MappingJournalDetail", params.id]]
            return result
        }

        result.mappingJournalDetailInstance = new MappingJournalDetail()
        result.mappingJournalDetailInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.mappingJournalDetailInstance && m.field)
                result.mappingJournalDetailInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["MappingJournalDetail", params.id]]
            return result
        }

        result.mappingJournalDetailInstance = new MappingJournalDetail(params)

        if (result.mappingJournalDetailInstance.hasErrors() || !result.mappingJournalDetailInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def mappingJournalDetail = MappingJournalDetail.findById(params.id)
        if (mappingJournalDetail) {
            mappingJournalDetail."${params.name}" = params.value
            mappingJournalDetail.save()
            if (mappingJournalDetail.hasErrors()) {
                throw new Exception("${mappingJournalDetail.errors}")
            }
        } else {
            throw new Exception("MappingJournalDetail not found")
        }
    }

}