package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class JournalTypeService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = JournalType.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_journalType"){
				ilike("journalType","%" + (params."sCriteria_journalType" as String) + "%")
			}

			if(params."sCriteria_description"){
				ilike("description","%" + (params."sCriteria_description" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			if(params."sCriteria_journal"){
				eq("journal",params."sCriteria_journal")
			}
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						journalType: it.journalType,
			
						description: it.description,
			
						lastUpdProcess: it.lastUpdProcess,
			
						journal: it.journal
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["JournalType", params.id] ]
			return result
		}

		result.journalTypeInstance = JournalType.get(params.id)

		if(!result.journalTypeInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["JournalType", params.id] ]
			return result
		}

		result.journalTypeInstance = JournalType.get(params.id)

		if(!result.journalTypeInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["JournalType", params.id] ]
			return result
		}

		result.journalTypeInstance = JournalType.get(params.id)

		if(!result.journalTypeInstance)
			return fail(code:"default.not.found.message")

		try {
			result.journalTypeInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		JournalType.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.journalTypeInstance && m.field)
					result.journalTypeInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["JournalType", params.id] ]
				return result
			}

			result.journalTypeInstance = JournalType.get(params.id)

			if(!result.journalTypeInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.journalTypeInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.journalTypeInstance.properties = params

			if(result.journalTypeInstance.hasErrors() || !result.journalTypeInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["JournalType", params.id] ]
			return result
		}

		result.journalTypeInstance = new JournalType()
		result.journalTypeInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.journalTypeInstance && m.field)
				result.journalTypeInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["JournalType", params.id] ]
			return result
		}

		result.journalTypeInstance = new JournalType(params)

		if(result.journalTypeInstance.hasErrors() || !result.journalTypeInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def journalType =  JournalType.findById(params.id)
		if (journalType) {
			journalType."${params.name}" = params.value
			journalType.save()
			if (journalType.hasErrors()) {
				throw new Exception("${journalType.errors}")
			}
		}else{
			throw new Exception("JournalType not found")
		}
	}

}