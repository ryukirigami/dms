package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.DeliveryOrder
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.SalesOrderDetail
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class PenerimaanPenjualanJournalService {
    boolean transactional = true
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService = new DatatablesUtilService()

    def createJournal(params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def totalParts = 0;
        def totalBahan = 0;
        def totalSemua = 0;
        def ppn = 0;
        def dO = DeliveryOrder.findByDoNumberAndStaDelAndCompanyDealer(params.docNumber,"0",session?.userCompanyDealer)
        if(dO){
            def details = SalesOrderDetail.findAllBySorNumberAndStaDel(dO.sorNumber,"0")
            int a = 0
            details.each {
                a++
                def discTemp = it.discount
                def totTemp = discTemp && discTemp!=0 ? (it.quantity * it.unitPrice - (it.quantity * it.unitPrice) * discTemp / 100) : it.quantity * it.unitPrice
                def klas = KlasifikasiGoods.findByGoods(it.materialCode)
                if(klas){
                    if(klas.franc.m117NamaFranc.toUpperCase().contains("CAMPURAN")){
                        totalParts += Math.round(totTemp)
                    }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESSORIS")){
                        totalParts+=  Math.round(totTemp)
                    }else if(klas.franc.m117NamaFranc.toUpperCase().contains("TOYOTA")){
                        totalParts += Math.round(totTemp)
                    }else{
                        totalBahan +=  Math.round(totTemp)
                    }
                }
            }
        }
        def perPpn = params.ppn.toDouble();
        ppn =  perPpn / 100 * params.sebelumPpn?.toDouble();
        if(ppn>0){
            ppn = Math.round(ppn);
        }
        totalSemua = params?.totalBiaya;

        //percobaan biar tidak selisih jurnalnya
        try {
            def selisih = totalSemua - (totalBahan + totalParts + ppn);
            if(selisih != 0){
                ppn = ppn + selisih
            }
        }catch (Exception e){
            println e.printStackTrace();
        }


        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = Date.parse("dd/MM/yyyy", params.transactionDate as String)
        journalInstance.totalDebit = Math.round(totalSemua)
        journalInstance.totalCredit = Math.round(totalSemua)
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        journalInstance.description = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                "Pembayaran Pembelian Parts No. SO "+dO?.sorNumber?.sorNumber:"Pembayaran Pembelian Parts (Bank) No. DO "+dO?.sorNumber?.sorNumber
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def totalBayar = 0
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                bankAccountNumber = BankAccountNumber.get(params.idBank.toLong())
                if (!bankAccountNumber) {
                    return "Error"
                }
            }

            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJPJP")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                totalBayar = getAmountPenjualan(it.accountNumber, (it.accountTransactionType as String),params.tipeBayar,params.tipeBeli,totalBahan,totalParts,totalSemua,ppn)
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber

                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                    accountNumber = bankAccountNumber.accountNumber
                }
                 if(totalBayar!=0){
//                    println accountNumber.accountName + " - " + totalBayar
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            dateCreated: datatablesUtilService?.syncTime(),
                            lastUpdated: datatablesUtilService?.syncTime(),
                            subLedger: ""
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                    }
                    //println journalDetail?.errors?.each {//println it}
                }
            }
            //println "Sales Order Journal Saved"
            if(params?.transaction){
                def trx = Transaction.get(params?.transaction?.id?.toLong())
                if(trx){
                    trx?.journal = journalInstance
                    trx?.save(flush: true)
                }else{
                    def iddd = Transaction.findByDocNumber(params.docNumber as String).id
                    trx = Transaction.get(iddd)
                    trx?.journal = journalInstance
                    trx?.save(flush: true)
                }
            }
        }
    }

    def getAmountPenjualan(AccountNumber accountNumber, accountTransactionType,String tipeBayar,String tipeBeli,def totalBahan, def totalParts, def totalSemua, def ppn) {
        def harga = 0
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            if(tipeBeli.equalsIgnoreCase("tunai")){
                switch (accountNumber.accountNumber.toLowerCase()) {
                    case "1.10.20.01" :
                        if(tipeBayar.equalsIgnoreCase("bank")){
                            harga =  Math.round(totalSemua)
                        }
                        return harga
                    case "1.10.10.01.001" :
                        if(tipeBayar.equalsIgnoreCase("kas")){
                            harga =  Math.round(totalSemua)
                        }
                        return harga
                    default:
                        return 0
                }
            }else{
                switch (accountNumber.accountNumber.toLowerCase()) {
                    case "1.30.10.01.001" :
                        harga =  Math.round(totalSemua)
                        return harga
                    default:
                        return 0
                }
            }
        } else {
            switch (accountNumber.accountNumber.toLowerCase()) {
                case "6.10.10.03.002" :
                    if(tipeBeli.equalsIgnoreCase("kredit")){
                        harga =  Math.round(totalBahan)
                    }
                    return harga
                case "6.10.10.03.001" :
                    if(tipeBeli.equalsIgnoreCase("tunai")){
                        harga =  Math.round(totalBahan)
                    }
                    return harga
                case "6.10.10.02.002" :
                    if(tipeBeli.equalsIgnoreCase("kredit")){
                        harga =  Math.round(totalParts)
                    }
                    return harga
                case "6.10.10.02.001" :
                    if(tipeBeli.equalsIgnoreCase("tunai")){
                        harga =  Math.round(totalParts)
                    }
                    return harga
                case "4.20.10.01.001" :
                    harga =  Math.round(ppn)
                    return harga
                default:
                    return 0
            }
        }

    }

    def createJournalKredit(params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def totalParts = 0;
        def totalBahan = 0;
        def totalSemua = 0;
        def ppn = 0;
        def dO = DeliveryOrder.findByDoNumberAndStaDelAndCompanyDealer(params.docNumber,"0",session?.userCompanyDealer)
        if(dO){
            def details = SalesOrderDetail.findAllBySorNumberAndStaDel(dO.sorNumber,"0")
            int a = 0
            details.each {
                a++
                def discTemp = it.discount
                def totTemp = discTemp && discTemp!=0 ? (it.quantity * it.unitPrice - (it.quantity * it.unitPrice) * discTemp / 100) : it.quantity * it.unitPrice
                def klas = KlasifikasiGoods.findByGoods(it.materialCode)
                if(klas){
                    if(klas.franc.m117NamaFranc.toUpperCase().contains("CAMPURAN")){
                        totalParts += Math.round(totTemp)
                    }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESSORIS")){
                        totalParts+=  Math.round(totTemp)
                    }else if(klas.franc.m117NamaFranc.toUpperCase().contains("TOYOTA")){
                        totalParts += Math.round(totTemp)
                    }else{
                        totalBahan +=  Math.round(totTemp)
                    }
                }
            }
        }
        def perPpn = params.ppn.toDouble();
        ppn =  perPpn / 100 * params.sebelumPpn?.toDouble();
        totalSemua = params?.totalBiaya;
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = dO.dateCreated
        journalInstance.totalDebit = Math.round(totalSemua)
        journalInstance.totalCredit = Math.round(totalSemua)
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        journalInstance.description =
                "Pembelian Parts (Kredit) No. SO "+dO?.sorNumber?.sorNumber+"  / No. DO "+dO?.doNumber
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def totalBayar = 0
            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJPJP")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                totalBayar = getAmountPenjualan(it.accountNumber, (it.accountTransactionType as String),params.tipeBayar,params.tipeBeli,totalBahan,totalParts,totalSemua,ppn)
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
//                println accountNumber.accountName + " - " + totalBayar
                if(totalBayar!=0){
                    def subtype = ""
                    def subledger = ""
                    if(accountNumber.accountName.toUpperCase().contains("PIUTANG")){
                        subtype = SubType.findBySubType("CUSTOMER")
                        subledger = params.subledger
                    }

                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            dateCreated: datatablesUtilService?.syncTime(),
                            lastUpdated: datatablesUtilService?.syncTime(),
                            subLedger: subledger,
                            subType: subtype
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                    }
                    //println journalDetail?.errors?.each {//println it}
                }
            }
        }
    }

    def createJournalPembayaranKredit(params) {
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def totalSemua = 0;
        def dO = DeliveryOrder.findByDoNumberAndStaDelAndCompanyDealer(params.docNumber,"0",session?.userCompanyDealer)

        totalSemua = params?.totalBiaya;
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = dO.dateCreated
        journalInstance.totalDebit = Math.round(totalSemua)
        journalInstance.totalCredit = Math.round(totalSemua)
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        journalInstance.description =
                "Pembayaran Pembelian Parts (Kredit) No. SO "+dO?.sorNumber?.sorNumber+"  / No. DO "+dO?.doNumber
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def totalBayar = params?.totalBiaya
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                bankAccountNumber = BankAccountNumber.get(params.idBank.toLong())
                if (!bankAccountNumber) {
                    return "Error"
                }
            }
            MappingJournal mappingJournal = MappingJournal.findByMappingCode("MAP-TJPIK")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                      accountNumber = bankAccountNumber.accountNumber
                }
                if(totalBayar!=0){
                    def subtype = ""
                    def subledger = ""
                    if(accountNumber.accountName.toUpperCase().contains("PIUTANG")){
                        subtype = SubType.findBySubType("CUSTOMER")
                        subledger = HistoryCustomer.findByT182NamaDepan(dO.sorNumber.customerName).id
                    }

                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            dateCreated: datatablesUtilService?.syncTime(),
                            lastUpdated: datatablesUtilService?.syncTime(),
                            subLedger: subledger,
                            subType: subtype
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                    }
                    //println journalDetail?.errors?.each {//println it}
                }
            }
            //println "Sales Order Journal Saved"
            if(params?.transaction){
                def trx = Transaction.get(params?.transaction?.id?.toLong())
                if(trx){
                    trx?.journal = journalInstance
                    trx?.save(flush: true)
                }else{
                    def iddd = Transaction.findByDocNumber(params.docNumber as String).id
                    trx = Transaction.get(iddd)
                    trx?.journal = journalInstance
                    trx?.save(flush: true)
                }
            }
        }
    }
}
