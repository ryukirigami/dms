package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.DealerPenjual
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.JenisPerusahaan
import com.kombos.generatecode.GenerateCodeService
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Vendor
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class PembayaranPiutangKreditJournalService {
    boolean transactional = true
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

    def createJournal(params) {
        //println "PembayaranPiutangKreditJournalService => createJournal"

        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def invoice = InvoiceT701.findByT701NoInvIlikeAndT701StaDel("%"+params.docNumber+"%","0");

        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        try{
            journalInstance.journalDate = Date.parse("dd/MM/yyyy", params.transactionDate as String)
        }catch (Exception e){
            journalInstance.journalDate = new Date()
        }
        journalInstance.totalDebit = Math.round((params.totalBiaya + params.tambahanBiaya))
        journalInstance.totalCredit = Math.round((params.totalBiaya + params.tambahanBiaya))
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance?.dateCreated = datatablesUtilService?.syncTime()
        journalInstance?.lastUpdated = datatablesUtilService?.syncTime()
        journalInstance.description = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                "Pembayaran Piutang Kredit No Inv. "+params.docNumber + " | an. " + invoice.t701Customer + " | No Wo." + invoice.reception.t401NoWO
                :"Pembayaran Piutang Kredit (Bank) No. Inv. "+params.docNumber + " | an. " + invoice.t701Customer + " | No Wo." + invoice.reception.t401NoWO
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                bankAccountNumber = BankAccountNumber.get(params.idBank.toLong())
                if (!bankAccountNumber) {
                    return "Error"
                }
            }

            MappingJournal mappingJournal = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                    MappingJournal.findByMappingCode("MAP-TJPIK"):MappingJournal.findByMappingCode("MAP-TJBPIK")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Debet") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                    accountNumber = bankAccountNumber.accountNumber
                }
                def subType = null
                def subLedger = null
                if(it?.accountNumber?.accountNumber?.trim()=="1.30.10.01.001"){
                    if(params.cabang){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("%VENDOR","0")
                        def cari = Vendor.findByM121NamaIlikeAndStaDel("%"+invoice?.t701Customer+"%","0")
                        if(cari){
                            subLedger = cari?.id
                        }else {
                            cari = Vendor.findByM121NamaIlikeAndStaDel("%HASJRAT%","0")
                            if(cari){
                                subLedger = cari?.id
                            }
                        }
                    }else {
                        subType = SubType?.findById(params.subtype as Long)
                        subLedger = params.subLedger
                        if(!subLedger){
                            def comp = new Company()
                            comp.companyDealer = invoice?.companyDealer
                            comp.kodePerusahaan = new GenerateCodeService().codeGenerateSequence("T101_ID",null)
                            comp.jenisPerusahaan  = JenisPerusahaan.findByNamaJenisPerusahaanAndStaDel("PT","0");
                            comp.namaPerusahaan = invoice?.t701Customer.toString()
                            comp.alamatPerusahaan = "INI DARI SISTEM"
                            comp.telp = "0"
                            comp.namaDepanPIC = "INI DARI SISTEM PEMBAYARAN"
                            comp.alamatPIC = "INI DARI SISTEM PEMBAYARAN"
                            comp.telpPIC = "0"
                            comp.hpPIC = "0"
                            comp.alamatNPWP = "INI DARI SISTEM PEMBAYARAN"
                            comp.staDel = "0"
                            comp.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            comp.lastUpdProcess = "INSERT"
                            comp.dateCreated = datatablesUtilService?.syncTime()
                            comp.lastUpdated = datatablesUtilService?.syncTime()
                            comp.save(flush: true)
                            comp.errors.each {
                                println it
                            }
                            if(!comp?.hasErrors()){
                                subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER COMPANY","0")
                                subLedger = comp?.id
                            }
                        }
                    }

                }

                def journalDetail = new JournalDetail(
                        journalTransactionType:journalType.journalType,
                        creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Kredit")?Math.round((params.totalBiaya+params.tambahanBiaya)):0,
                        debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Debet")?Math.round(params.totalBiaya):0,
                        staDel: "0",
                        accountNumber: accountNumber,
                        journal: journalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        subLedger: subLedger,
                        subType: subType,
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime()
                )
                if (journalDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                    //println "====> ${rowCount}"
                }
            }
            if (params.details) {
                def detailArray = JSON.parse(params.details)
                detailArray.each {detail ->
                    def total       = detail?.total?.toString()?.toBigDecimal()
                    def subtype     = detail.subtype ? SubType.findByDescriptionIlike("%"+detail.subtype+"%") : null
                    def subdleger   = detail.subledger
//                    println "Acount " + AccountNumber.findByAccountNumber(detail.accountNumber)
//                    println "total " + detail.total
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            debitAmount: (detail.transactionType as String).equalsIgnoreCase("Kredit") ? 0 : Math.round(total),
                            creditAmount: (detail.transactionType as String).equalsIgnoreCase("Kredit") ? Math.round(total) : 0,
                            staDel: "0",
                            accountNumber: AccountNumber.findByAccountNumber(detail.accountNumber),
                            subType: subtype,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: subdleger,
                            dateCreated: datatablesUtilService.syncTime(),
                            lastUpdated:  datatablesUtilService.syncTime()

                    ).save(flush: true, failOnError: true)

                    journalDetail?.errors?.each {
                        //println it

                    }
                 }
            }
            //println "Pembayaran Piutang Kredit Journal Saved"

            if(params?.transaction){
                def trx = Transaction.get(params?.transaction?.id?.toLong())
                if(trx){
                    trx?.journal = journalInstance
                    trx?.save(flush: true)
                    trx?.errors?.each {//println it
                    }
                }
            }
        }
    }

    def getMappingJournal() {

    }

}