package com.kombos.finance

import com.kombos.baseapp.AppSettingParam

class AccountNumberService {
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = AccountNumber.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_accountName"){
				ilike("accountName","%" + (params."sCriteria_accountName" as String) + "%")
			}

			if(params."sCriteria_accountNumber"){
				ilike("accountNumber","%" + (params."sCriteria_accountNumber" as String) + "%")
			}

			if(params."sCriteria_accountMutationType"){
				ilike("accountMutationType","%" + (params."sCriteria_accountMutationType" as String) + "%")
			}

			if(params."sCriteria_parentAccount"){
				eq("parentAccount",params."sCriteria_parentAccount")
			}

			if(params."sCriteria_level"){
				eq("level",params."sCriteria_level")

			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			if(params."sCriteria_accountType"){
                accountType{
                    ilike("accountType", "%" + (params."sCriteria_accountType" as String) + "%")
                }

			}

			if(params."sCriteria_bankReconciliationDetail"){
				eq("bankReconciliationDetail",params."sCriteria_bankReconciliationDetail")
			}

			if(params."sCriteria_journalDetail"){
				eq("journalDetail",params."sCriteria_journalDetail")
			}

			if(params."sCriteria_ledgerCardDetail"){
				eq("ledgerCardDetail",params."sCriteria_ledgerCardDetail")
			}

			if(params."sCriteria_mappingJournalDetail"){
				eq("mappingJournalDetail",params."sCriteria_mappingJournalDetail")
			}

			if(params."sCriteria_monthlyBalance"){
				eq("monthlyBalance",params."sCriteria_monthlyBalance")
			}


            eq("staDel","0")

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [

						id: it.id,

						accountName: it.accountName,

						accountNumber: it.accountNumber,

						accountMutationType: it.accountMutationType,

						parentAccount: it.parentAccount,

						accountType: it.accountType.accountType,

//						level: it.level,

//						lastUpdProcess: it.lastUpdProcess,

//						bankReconciliationDetail: it.bankReconciliationDetail,
//
//						journalDetail: it.journalDetail,
//
//						ledgerCardDetail: it.ledgerCardDetail,
//
//						mappingJournalDetail: it.mappingJournalDetail,
//
//						monthlyBalance: it.monthlyBalance,

			]
		}

		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["AccountNumber", params.id] ]
			return result
		}

		result.accountNumberInstance = AccountNumber.get(params.id)

		if(!result.accountNumberInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["AccountNumber", params.id] ]
			return result
		}

		result.accountNumberInstance = AccountNumber.get(params.id)

		if(!result.accountNumberInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["AccountNumber", params.id] ]
			return result
		}

		result.accountNumberInstance = AccountNumber.get(params.id)

		if(!result.accountNumberInstance)
			return fail(code:"default.not.found.message")

		try {
			result.accountNumberInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		AccountNumber.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.accountNumberInstance && m.field)
					result.accountNumberInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["AccountNumber", params.id] ]
				return result
			}

			result.accountNumberInstance = AccountNumber.get(params.id)

			if(!result.accountNumberInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.accountNumberInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.accountNumberInstance.properties = params

			if(result.accountNumberInstance.hasErrors() || !result.accountNumberInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["AccountNumber", params.id] ]
			return result
		}
        if (params.id) {
            result.accountNumberInstance = AccountNumber.findById(Integer.parseInt(params.id as String))
        }
        else {
            result.accountNumberInstance = new AccountNumber()
        }

        result.accountNumberInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.accountNumberInstance && m.field)
				result.accountNumberInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["AccountNumber", params.id] ]
			return result
		}

		result.accountNumberInstance = new AccountNumber(params)

		if(result.accountNumberInstance.hasErrors() || !result.accountNumberInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def accountNumber =  AccountNumber.findById(params.id)
		if (accountNumber) {
			accountNumber."${params.name}" = params.value
			accountNumber.save()
			if (accountNumber.hasErrors()) {
				throw new Exception("${accountNumber.errors}")
			}
		}else{
			throw new Exception("AccountNumber not found")
		}
	}

}