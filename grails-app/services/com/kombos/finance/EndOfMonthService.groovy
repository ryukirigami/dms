package com.kombos.finance

import java.text.DecimalFormat
import java.text.SimpleDateFormat

class EndOfMonthService {

    def execute() {

        AccountNumber accountNumber;
        def accounts = AccountNumber.list([sort: "level",
                                           order: "asc"])

        accounts.each {
            accountNumber = it;
            def eomValue = [debitMutation : 0,
                            creditMutation : 0,
                            subType:0,
                            subLedger:'']
            def total = getTotalAmount(accountNumber.accountNumber,eomValue)

            //println "Account : " + accountNumber.accountNumber + ", Total Debet : " + format(total.debitMutation) +", Total Kredit : " + format(total.creditMutation)


            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM")
            def nowCal = Calendar.instance
            nowCal.add(Calendar.MONTH, -1);
            Date lastMonth = nowCal.getTime()
            MonthlyBalance lastMonthBalance = MonthlyBalance.findByAccountNumberAndYearMonth(accountNumber,dateFormat.format(lastMonth))

            def lastBalance = (lastMonthBalance != null)? lastMonthBalance.endingBalance : 0
            def saldoAkhir = lastBalance + total.debitMutation - total.creditMutation

            Calendar startingCal = Calendar.instance
            startingCal.set(Calendar.DATE, 1)
            Date startingDate = startingCal.getTime()

            Calendar endingCal = Calendar.instance
            endingCal.set(Calendar.DATE, endingCal.getActualMaximum(Calendar.DAY_OF_MONTH))
            Date endingDate = endingCal.getTime()

            new MonthlyBalance(
                    accountNumber : accountNumber,
                    yearMonth : dateFormat.format(new Date()),
                    startingBalance : lastBalance,
                    debitMutation : total.debitMutation,
                    creditMutation : total.creditMutation,
                    endingBalance : saldoAkhir,
                    startingDate : startingDate,
                    endingDate : endingDate,
                    subType:eomValue.subType,
                    subLedger:eomValue.subLedger,
                    dateCreated : new Date(),
                    createdBy : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdated  : new Date(),
                    updatedBy  : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(flush: true, failOnError: true)
        }
    }

    def getTotalAmount(accountNumber,eomValue) {
        def accounts = AccountNumber.findAll("from AccountNumber where parentAccount=?",[accountNumber])

        if(accounts.size() != 0) {
            accounts.each {
                eomValue = getTotalAmount(it.accountNumber,eomValue)
            }
        } else {
            AccountNumber account = AccountNumber.findByAccountNumber(accountNumber)
            def journalDetails = JournalDetail.findAllByAccountNumber(account)

            def subType = SubType.findBySubType("TJ")
            if (!subType) {
                subType = new SubType(
                        description: "Transaction Journal",
                        staDel: "0",
                        createdBy: "System",
                        lastUpdProcess: "INSERT",
                        subType: "TJ"
                ).save(failOnError: true)
            }

            journalDetails.each {
                eomValue.debitMutation += it.debitAmount
                eomValue.creditMutation += it.creditAmount
                eomValue.subType = subType
                eomValue.subLedger = "Sub Ledger"
            }
        }

        return eomValue
    }

    static String format(double d) {
        def format = new DecimalFormat()
        def symbols = format.decimalFormatSymbols
        symbols.groupingSeparator = ','
        format.decimalFormatSymbols = symbols
        return format.format(d)
    }
}
