package com.kombos.finance



import com.kombos.baseapp.AppSettingParam
import org.springframework.web.context.request.RequestContextHolder

class BankAccountNumberService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def session = RequestContextHolder.currentRequestAttributes().getSession()

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = BankAccountNumber.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
//            if (!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("companyDealer",session.userCompanyDealer)
//            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_accountNumber") {
                eq("accountNumber", params."sCriteria_accountNumber")
            }

            if (params."sCriteria_bank") {
                /*eq("bank", params."sCriteria_bank")*/
                bank{
                    ilike("m702NamaBank", "%" + (params."sCriteria_bank" as String) + "%")
                }

            }

            if (params."sCriteria_transaction") {
                eq("transaction", "%" + params."sCriteria_transaction")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    lastUpdProcess: it.lastUpdProcess,

                    accountNumber: it.accountNumber.accountNumber+" - "+it.accountNumber.accountName,

                    bank: it.bank.m702NamaBank+" "+it.bank.m702Cabang,

                    transaction: it.transaction,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankAccountNumber", params.id]]
            return result
        }

        result.bankAccountNumberInstance = BankAccountNumber.get(params.id)

        if (!result.bankAccountNumberInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankAccountNumber", params.id]]
            return result
        }

        result.bankAccountNumberInstance = BankAccountNumber.get(params.id)

        if (!result.bankAccountNumberInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankAccountNumber", params.id]]
            return result
        }

        result.bankAccountNumberInstance = BankAccountNumber.get(params.id)

        if (!result.bankAccountNumberInstance)
            return fail(code: "default.not.found.message")

        try {
            result.bankAccountNumberInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        BankAccountNumber.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.bankAccountNumberInstance && m.field)
                    result.bankAccountNumberInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["BankAccountNumber", params.id]]
                return result
            }

            result.bankAccountNumberInstance = BankAccountNumber.get(params.id)

            if (!result.bankAccountNumberInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.bankAccountNumberInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.bankAccountNumberInstance.properties = params

            if (result.bankAccountNumberInstance.hasErrors() || !result.bankAccountNumberInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankAccountNumber", params.id]]
            return result
        }

        result.bankAccountNumberInstance = new BankAccountNumber()
        result.bankAccountNumberInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.bankAccountNumberInstance && m.field)
                result.bankAccountNumberInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["BankAccountNumber", params.id]]
            return result
        }

        result.bankAccountNumberInstance = new BankAccountNumber(params)

        if (result.bankAccountNumberInstance.hasErrors() || !result.bankAccountNumberInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def bankAccountNumber = BankAccountNumber.findById(params.id)
        if (bankAccountNumber) {
            bankAccountNumber."${params.name}" = params.value
            bankAccountNumber.save()
            if (bankAccountNumber.hasErrors()) {
                throw new Exception("${bankAccountNumber.errors}")
            }
        } else {
            throw new Exception("BankAccountNumber not found")
        }
    }

}