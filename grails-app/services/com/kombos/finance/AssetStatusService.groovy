package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class AssetStatusService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = AssetStatus.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_assetStatus") {
                ilike("assetStatus", "%" + (params."sCriteria_assetStatus" as String) + "%")
            }

            if (params."sCriteria_description") {
                ilike("description", "%" + (params."sCriteria_description" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_asset") {
                eq("asset", params."sCriteria_asset")
            }

            if (params."sCriteria_assetTopName") {
                eq("assetTopName", params."sCriteria_assetTopName")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [
                    id: it?.id,

                    assetStatus: it?.assetStatus,

                    description: it?.description,

                    lastUpdProcess: it?.lastUpdProcess,

                    asset: it?.asset,

                    assetOpname: it?.assetOpname,
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetStatus", params.id]]
            return result
        }

        result.assetStatusInstance = AssetStatus.get(params.id)

        if (!result.assetStatusInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetStatus", params.id]]
            return result
        }

        result.assetStatusInstance = AssetStatus.get(params.id)

        if (!result.assetStatusInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetStatus", params.id]]
            return result
        }

        result.assetStatusInstance = AssetStatus.get(params.id)

        if (!result.assetStatusInstance)
            return fail(code: "default.not.found.message")

        try {
            result.assetStatusInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        AssetStatus.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.assetStatusInstance && m.field)
                    result.assetStatusInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["AssetStatus", params.id]]
                return result
            }

            result.assetStatusInstance = AssetStatus.get(params.id)

            if (!result.assetStatusInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.assetStatusInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.assetStatusInstance.properties = params

            if (result.assetStatusInstance.hasErrors() || !result.assetStatusInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AssetStatus", params.id]]
            return result
        }

        result.assetStatusInstance = new AssetStatus()
        result.assetStatusInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.assetStatusInstance && m.field)
                result.assetStatusInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["AssetStatus", params.id]]
            return result
        }

        result.assetStatusInstance = new AssetStatus(params)

        if (result.assetStatusInstance.hasErrors() || !result.assetStatusInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def assetStatus = AssetStatus.findById(params.id)
        if (assetStatus) {
            assetStatus."${params.name}" = params.value
            assetStatus.save()
            if (assetStatus.hasErrors()) {
                throw new Exception("${assetStatus.errors}")
            }
        } else {
            throw new Exception("AssetStatus not found")
        }
    }

}