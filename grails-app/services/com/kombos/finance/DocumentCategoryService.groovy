package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class DocumentCategoryService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = DocumentCategory.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_documentCategory") {
                ilike("documentCategory", "%" + (params."sCriteria_documentCategory" as String) + "%")
            }

            if (params."sCriteria_description") {
                ilike("description", "%" + (params."sCriteria_description" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_transaction") {
                eq("transaction", params."sCriteria_transaction")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    documentCategory: it.documentCategory,

                    description: it.description,

                    lastUpdProcess: it.lastUpdProcess,

                    transaction: it.transaction,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DocumentCategory", params.id]]
            return result
        }

        result.documentCategoryInstance = DocumentCategory.get(params.id)

        if (!result.documentCategoryInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DocumentCategory", params.id]]
            return result
        }

        result.documentCategoryInstance = DocumentCategory.get(params.id)

        if (!result.documentCategoryInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DocumentCategory", params.id]]
            return result
        }

        result.documentCategoryInstance = DocumentCategory.get(params.id)

        if (!result.documentCategoryInstance)
            return fail(code: "default.not.found.message")

        try {
            result.documentCategoryInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        DocumentCategory.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.documentCategoryInstance && m.field)
                    result.documentCategoryInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["DocumentCategory", params.id]]
                return result
            }

            result.documentCategoryInstance = DocumentCategory.get(params.id)

            if (!result.documentCategoryInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.documentCategoryInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.documentCategoryInstance.properties = params

            if (result.documentCategoryInstance.hasErrors() || !result.documentCategoryInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["DocumentCategory", params.id]]
            return result
        }

        result.documentCategoryInstance = new DocumentCategory()
        result.documentCategoryInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.documentCategoryInstance && m.field)
                result.documentCategoryInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["DocumentCategory", params.id]]
            return result
        }

        result.documentCategoryInstance = new DocumentCategory(params)

        if (result.documentCategoryInstance.hasErrors() || !result.documentCategoryInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def documentCategory = DocumentCategory.findById(params.id)
        if (documentCategory) {
            documentCategory."${params.name}" = params.value
            documentCategory.save()
            if (documentCategory.hasErrors()) {
                throw new Exception("${documentCategory.errors}")
            }
        } else {
            throw new Exception("DocumentCategory not found")
        }
    }

}