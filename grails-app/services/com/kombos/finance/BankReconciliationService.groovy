package com.kombos.finance



import com.kombos.baseapp.AppSettingParam
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class BankReconciliationService {
    boolean transactional = false

    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def konversi = new Konversi()
    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        Date awal = new Date().parse("dd/MM/yyyy","01/"+params?.bulan+"/"+params?.tahun)
        String tgglAkhir = new MonthlyBalanceController().tanggalAkhir(params?.bulan?.toInteger(),params?.tahun?.toInteger())
        Date akhir = new Date().parse("dd/MM/yyyy",tgglAkhir+"/"+params?.bulan+"/"+params?.tahun)
        def c = BankReconciliation.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",session.userCompanyDealer)
            if (params."sCriteria_bank") {
                bank {
                    ilike("m702NamaBank", "%" + params."sCriteria_bank" + "%")
                }
            }

            if (params."bulan" && params.tahun) {
                ge("reconciliationDate", awal)
                lt("reconciliationDate", akhir + 1)
            }

            if (params."sCriteria_saldoAwalBank") {
                eq("saldoAwalBank", params."sCriteria_saldoAwalBank")
            }

            if (params."sCriteria_saldoAkhirBank") {
                eq("saldoAkhirBank", params."sCriteria_saldoAkhirBank")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    bank: it?.bank?.m702NamaBank,

                    reconciliationDate: it?.reconciliationDate ? it?.reconciliationDate.format(dateFormat) : "",

                    saldoAwalBank: konversi.toRupiah(it.saldoAwalBank),

                    saldoAkhirBank: konversi.toRupiah(it.saldoAkhirBank)
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankReconciliation", params.id]]
            return result
        }

        result.bankReconciliationInstance = BankReconciliation.get(params.id)

        if (!result.bankReconciliationInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankReconciliation", params.id]]
            return result
        }

        result.bankReconciliationInstance = BankReconciliation.get(params.id)

        if (!result.bankReconciliationInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankReconciliation", params.id]]
            return result
        }

        result.bankReconciliationInstance = BankReconciliation.get(params.id)

        if (!result.bankReconciliationInstance)
            return fail(code: "default.not.found.message")

        try {
            result.bankReconciliationInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {

            def result = [:]
            def fail = { Map m ->
                if (result.bankReconciliationInstance && m.field)
                    result.bankReconciliationInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["BankReconciliation", params.id]]
                return result
            }

            result.bankReconciliationInstance = BankReconciliation.get(params.id)

            def bankInstance = com.kombos.administrasi.Bank.findById(Long.parseLong(params.bank))

            result.bankReconciliationInstance.reconciliationDate   = Date.parse("dd/MM/yyyy", params.reconciliationDate)
            result.bankReconciliationInstance.bank                 = bankInstance
            result.bankReconciliationInstance.saldoAwalBank        = Float.parseFloat(params.saldoAwalBank)
            result.bankReconciliationInstance.saldoAkhirBank       = Float.parseFloat(params.saldoAkhirBank)
            result.bankReconciliationInstance.saldoAwalPerusahaan  = Float.parseFloat(params.saldoAwalPerusahaan)
            result.bankReconciliationInstance.saldoAkhirPerusahaan = Float.parseFloat(params.saldoAkhirPerusahaan)
            result.bankReconciliationInstance.selisih              = Float.parseFloat(params.selisih)
            result.bankReconciliationInstance.createdBy            = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.bankReconciliationInstance.lastUpdProcess       = "UPDATE"
            result.bankReconciliationInstance.dateCreated          = datatablesUtilService?.syncTime()
            result.bankReconciliationInstance.lastUpdated          = datatablesUtilService?.syncTime()
            result.bankReconciliationInstance.staDel               = "0"
            if (!result.bankReconciliationInstance)
                return fail(code: "default.not.found.message")

            if (result.bankReconciliationInstance.save(flush: true, failOnError: true)) {

                if (params.deletedDetails) {
                    String[] ids = (params.deletedDetails as String).split(",")
                    ids.each {id ->
                        def nId = id.replace("[", "").replace("]", "")
                        def bankReconDetail = BankReconciliationDetail.findById(Long.parseLong(nId))
                        try {
                            if (bankReconDetail)
                                bankReconDetail.delete(flush: true)
                            else
                                result.error = "Bank recon detail tidak ditemukan"
                        } catch (Exception e) {
                            log.error("error when delete bankReconDetail: " + e.getMessage())
                        }
                    }
                }

                def bankArray       = JSON.parse(params.bankDetails)
                def perusahaanArray = JSON.parse(params.perusahaanDetails)
                bankArray.each {b ->
                    def newBankRB
                    if (!b.id) {
                        newBankRB = new BankReconciliationDetail()
                    } else {
                        newBankRB = BankReconciliationDetail.findById(Long.parseLong(String.valueOf(b.id)))
                    }
                    newBankRB.debitAmount          = Float.parseFloat(b.debitAmount)
                    newBankRB.creditAmount         = Float.parseFloat(b.creditAmount)
                    newBankRB.description          = b.description
                    newBankRB.reconTransactionType = "B"
                    newBankRB.bankRecon            = result.bankReconciliationInstance
                    newBankRB.createdBy            = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    newBankRB.lastUpdProcess       = b.id ? "UPDATE" : "INSERT"
                    newBankRB.staDel               = "0"

                    if (!newBankRB.save(failOnError: true, flush: true) || newBankRB.hasErrors()) {
                        result.errpr = newBankRB.errors
                    } else {
                        result.bankReconciliationInstance.addToBankReconciliationDetail(newBankRB)
                    }
                }
                perusahaanArray.each {p ->
                    def newBankRP
                    if (!p.id) {
                        newBankRP = new BankReconciliationDetail()
                    } else {
                        newBankRP = BankReconciliationDetail.findById(Long.parseLong(String.valueOf(p.id)))
                    }
                    newBankRP.debitAmount          = Float.parseFloat(p.debitAmount)
                    newBankRP.creditAmount         = Float.parseFloat(p.creditAmount)
                    newBankRP.description          = p.description
                    newBankRP.reconTransactionType = "P"
                    newBankRP.bankRecon            = result.bankReconciliationInstance
                    newBankRP.createdBy            = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    newBankRP.lastUpdProcess       = p.id ? "UPDATE" : "INSERT"
                    newBankRP.staDel               = "0"

                    if (!newBankRP.save(failOnError: true, flush: true) || newBankRP.hasErrors()) {
                        result.error = newBankRP.errors
                    } else {
                        result.bankReconciliationInstance.addToBankReconciliationDetail(newBankRP)
                    }
                }
            } else {
                result.error = result.bankReconciliationInstance.errors
            }

            // Optimistic locking check.
            if (params.version) {
                if (result.bankReconciliationInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.bankReconciliationInstance.properties = params

           /* if (result.bankReconciliationInstance.hasErrors() || !result.bankReconciliationInstance.save())
                *//*return fail(code: "default.not.updated.message")*//* result.error = result.bankReconciliationInstance.errors*/

            // Success.
            return result

    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BankReconciliation", params.id]]
            return result
        }

        result.bankReconciliationInstance = new BankReconciliation()
        result.bankReconciliationInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.bankReconciliationInstance && m.field)
                result.bankReconciliationInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["BankReconciliation", params.id]]
            return result
        }
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def bankInstance = com.kombos.administrasi.Bank.findById(Long.parseLong(params.bank))

        result.bankReconciliationInstance = new BankReconciliation(
                companyDealer: session.userCompanyDealer,
                reconciliationDate: Date.parse("dd/MM/yyyy", params.reconciliationDate),
                bank: bankInstance,
                saldoAwalBank: Float.parseFloat(params.saldoAwalBank),
                saldoAkhirBank: Float.parseFloat(params.saldoAkhirBank),
                saldoAwalPerusahaan: Float.parseFloat(params.saldoAwalPerusahaan),
                saldoAkhirPerusahaan: Float.parseFloat(params.saldoAkhirPerusahaan),
                selisih: Float.parseFloat(params.selisih),
                createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                lastUpdProcess: "INSERT",
                dataCreted :datatablesUtilService?.syncTime(),
                lastUpdated :datatablesUtilService?.syncTime(),
                staDel: "0"
        )

        if (result.bankReconciliationInstance.hasErrors() || !result.bankReconciliationInstance.save(flush: true))
            return fail(code: "default.not.created.message")
        else {
            def bankArray       = JSON.parse(params.bankDetails)
            def perusahaanArray = JSON.parse(params.perusahaanDetails)
            bankArray.each {b ->
                def newBankRB = new BankReconciliationDetail(
                        debitAmount: Float.parseFloat(b.debitAmount),
                        creditAmount: Float.parseFloat(b.creditAmount),
                        description: b.description,
                        reconTransactionType: "B",
                        bankRecon: result.bankReconciliationInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        staDel: "0"
                )
                if (!newBankRB.save(failOnError: true, flush: true) || newBankRB.hasErrors()) {
                    result = "Error on saving bankReconciliationDetails"
                } else {
                    result.bankReconciliationInstance.addToBankReconciliationDetail(newBankRB)
                }
            }
            perusahaanArray.each {p ->
                def newBankRP = new BankReconciliationDetail(
                        debitAmount: Float.parseFloat(p.debitAmount),
                        creditAmount: Float.parseFloat(p.creditAmount),
                        description: p.description,
                        reconTransactionType: "P",
                        bankRecon: result.bankReconciliationInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        staDel: "0"
                )

                if (!newBankRP.save(failOnError: true, flush: true) || newBankRP.hasErrors()) {
                    result = "Error on saving bankReconciliationDetails"
                }  else {
                    result.bankReconciliationInstance.addToBankReconciliationDetail(newBankRP)
                }
            }
        }

        // success
        return result
    }

    def updateField(def params) {
        def bankReconciliation = BankReconciliation.findById(params.id)
        if (bankReconciliation) {
            bankReconciliation."${params.name}" = params.value
            bankReconciliation.save()
            if (bankReconciliation.hasErrors()) {
                throw new Exception("${bankReconciliation.errors}")
            }
        } else {
            throw new Exception("BankReconciliation not found")
        }
    }

}
