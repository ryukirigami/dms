package com.kombos.finance

import com.kombos.administrasi.Bank
import com.kombos.baseapp.AppSettingParam
import com.kombos.generatecode.GenerateCodeService
import com.kombos.reception.InvoiceSublet
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class UangMukaSubletJournalService {
    boolean transactional = true
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def createJournal(params) {
        /*  params :
            Name
            tipeBayar           KAS or BANK
            namaBank            id bank, nullable
            docNumber           No dokumen referensi
            totalBiaya          Total biaya transaksi
        */
        def invSublet = InvoiceSublet.findByT413NoInv(params.docNumber)
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT",
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime(),
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        try{
            journalInstance.journalDate = Date.parse("dd/MM/yyyy", params.transactionDate as String)
        }catch (Exception e){
            journalInstance.journalDate = new Date()
        }
        def descripksi = ((params.tipeBayar as String).equalsIgnoreCase("KAS")?
                "U.M. Sublet":"U.M. Sublet (Bank)") + " No Wo. " + invSublet?.reception?.t401NoWO +
                ", Pek. $invSublet.operation.m053NamaOperation, Nopol. " + invSublet?.reception?.historyCustomerVehicle?.fullNoPol
        journalInstance.totalDebit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.totalCredit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance?.dateCreated = datatablesUtilService?.syncTime()
        journalInstance?.lastUpdated = datatablesUtilService?.syncTime()
        journalInstance.description = descripksi
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
//                bankAccountNumber = BankAccountNumber.findByBank(Bank.findByM702NamaBank(params.namaBank))
                bankAccountNumber = BankAccountNumber.get(params.idBank.toLong())
                if (!bankAccountNumber) {
                    return "Error"
                }
            }

            MappingJournal mappingJournal = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                    MappingJournal.findByMappingCode("MAP-TJUMSUB"):MappingJournal.findByMappingCode("MAP-TJBUMSUB")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                    accountNumber = bankAccountNumber.accountNumber
                }

                def journalDetail = new JournalDetail(
                        journalTransactionType:journalType.journalType,
                        creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Kredit")?Math.round(params.totalBiaya):0,
                        debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Debet")?Math.round(params.totalBiaya):0,
                        staDel: "0",
                        accountNumber: accountNumber,
                        journal: journalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        subLedger: "",
                        description: descripksi,
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime()
                )
                if (journalDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                    //println "====> ${rowCount}"
                }
            }

            if(params?.transaction){
                def trx = Transaction.get(params?.transaction?.id?.toLong())
                if(trx){
                    trx?.journal = journalInstance
                    trx?.save(flush: true)
                }
            }
        }
    }

    def getMappingJournal() {

    }

}
