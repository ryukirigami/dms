package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.GoodsReceive
import com.kombos.parts.Invoice
import com.kombos.parts.PO
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class HutangTokoLuarJournalService {
    boolean transactional = true
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService = new DatatablesUtilService()

    def createJournal(params) {
        /*  params :
            Name
            tipeBayar           KAS or BANK
            namaBank            id bank, nullable
            docNumber           No dokumen referensi
            totalBiaya          Total biaya transaksi
        */

        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.companyDealer = session.userCompanyDealer
        try {
            journalInstance.journalDate = Transaction.get(params?.transaction?.id?.toLong()).transactionDate
        }catch (Exception e){
            journalInstance.journalDate = new Date()
        }
        journalInstance.totalDebit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.totalCredit = Math.round(params?.totalBiaya?.toString()?.toBigDecimal())
        journalInstance.docNumber = params.docNumber
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                "Hutang Toko Luar No. Inv. "+params?.docNumber:"Hutang Toko Luar (Bank) No. Inv. "+params?.docNumber
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            def bankAccountNumber = new BankAccountNumber()
            if((params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                bankAccountNumber = BankAccountNumber.get(params.idBank.toLong())
                if (!bankAccountNumber) {
                    return "Error"
                }
            }

            MappingJournal mappingJournal = (params.tipeBayar as String).equalsIgnoreCase("KAS")?
                    MappingJournal.findByMappingCode("MAP-TJHTL"):MappingJournal.findByMappingCode("MAP-TJBHTL")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber
                if ((mappingJournalDetail.accountTransactionType as String).equalsIgnoreCase("Kredit") &&
                        (params.tipeBayar as String).equalsIgnoreCase("BANK")) {
                    accountNumber = bankAccountNumber.accountNumber
                }
                def subType = null
                def subLedger = null
                if(mappingJournalDetail?.accountNumber?.accountNumber?.equalsIgnoreCase("4.10.10.01.001")){
                    String noPO = params?.docNumber
                    def cPO = GoodsReceive.findByT167NoReffAndStaDelAndCompanyDealer(params?.docNumber,'0',session.userCompanyDealer)
                    if(cPO){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("VENDOR%","0");
                        subLedger = cPO?.vendor?.id
                    }
                }

                def journalDetail = new JournalDetail(
                        journalTransactionType:journalType.journalType,
                        creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Kredit")?Math.round(params?.totalBiaya?.toString()?.toBigDecimal()):0,
                        debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                .equalsIgnoreCase("Debet")?Math.round(params?.totalBiaya?.toString()?.toBigDecimal()):0,
                        staDel: "0",
                        accountNumber: accountNumber,
                        journal: journalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        subLedger: subLedger,
                        subType: subType,
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime()
                )
                if (journalDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                    //println "====> ${rowCount}"
                }
            }
            //println "Hutang Toko Luar Journal Saved"
            if(params?.transaction){
                def trx = Transaction.get(params?.transaction?.id?.toLong())
                if(trx){
                    trx?.journal = journalInstance
                    trx?.save(flush: true)
                }
            }
        }
    }

    def getMappingJournal() {

    }

}
