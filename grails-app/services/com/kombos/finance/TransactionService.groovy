package com.kombos.finance

import com.kombos.administrasi.DefaultVendorSublet
import com.kombos.baseapp.AppSettingParam
import com.kombos.generatecode.GenerateCodeService
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.DeliveryOrder
import com.kombos.parts.GoodsReceive
import com.kombos.parts.GoodsReceiveDetail
import com.kombos.parts.Invoice
import com.kombos.parts.Konversi
import com.kombos.parts.PO
import com.kombos.parts.Vendor
import com.kombos.reception.InvoiceSublet
import com.kombos.woinformation.JobRCP
import org.hibernate.criterion.CriteriaSpecification

import java.text.SimpleDateFormat

class TransactionService {
    boolean transactional = false

    def datatablesUtilService
    def pembayaranPiutangKreditJournalService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def konversi = new Konversi()
    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def isNoParam = false;

        if(!params."sCriteria_transactionCode" && !params."sCriteria_transactionDate_dp" && !params."sCriteria_transactionDate_dp_to"
                && !params."sCriteria_description" && !params."sCriteria_total" && !params."sCriteria_tipe"
                && !params."sCriteria_kategori"
                && !params."sCriteria_docNumber" && !params."sCriteria_deskripsi" ){
            isNoParam = true;
        }

        def c = Transaction.createCriteria()

        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_transactionCode") {
                ilike("transactionCode", "%" + (params."sCriteria_transactionCode" as String) + "%")
            }

            if (params."sCriteria_transactionDate_dp") {
                ge("transactionDate", Date.parse("dd/MM/yyyy", params."sCriteria_transactionDate_dp"))
                if (params."sCriteria_transactionDate_dp_to")
                    lt("transactionDate", Date.parse("dd/MM/yyyy", params."sCriteria_transactionDate_dp_to")+1)
            }


            if (params."sCriteria_description") {
                ilike("description", "%" + (params."sCriteria_description" as String) + "%")
            }

            if (params."sCriteria_total") {
                eq("amount", params?."sCriteria_total"?.toBigDecimal())
            }

            if (params."sCriteria_tipe") {
                transactionType {
                    ilike("transactionType", "%"+params."sCriteria_tipe"+"%")
                }
            }

            if (params."sCriteria_docNumber") {
                ilike("docNumber", "%" + (params."sCriteria_docNumber" as String) + "%")
            }

            if (params."sCriteria_deskripsi") {
                ilike("description", "%" + (params."sCriteria_deskripsi" as String) + "%")
            }

            if (params."sCriteria_kategori") {
                documentCategory{
                    ilike("description","%"+params."sCriteria_kategori"+"%")
                }
            }

            if(isNoParam){
                eq("id",-1000.toLong())
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {

            rows << [

                    id: it?.id,

                    transactionCode: it?.transactionCode,

                    transactionDate: it?.transactionDate ? it.transactionDate.format(dateFormat) : "",

                    description: it?.description,

                    amount: konversi.toRupiah(it?.amount),

                    journal: it?.journal,

                    docNumber: it?.docNumber,

                    inOutType: it?.inOutType,

                    receiptNumber: it?.receiptNumber,

                    transferDate: it?.transferDate ? it?.transferDate.format(dateFormat) : "",

                    dueDate: it?.dueDate ? it?.dueDate.format(dateFormat) : "",

                    cekNumber: it?.cekNumber,

                    pdcStatus: it?.pdcStatus,

                    pdcTransferDate: it?.pdcTransferDate ? it?.pdcTransferDate.format(dateFormat) : "",

                    lastUpdProcess: it?.lastUpdProcess,

                    approval: it?.journal?.isApproval=="0"?"Need Approval":( it?.journal?.isApproval=="1"?"Approved":"Rejected"),

                    bankAccountNumber: it?.bankAccountNumber,

                    documentCategory: it?.documentCategory?.description,

                    transactionType: it?.transactionType?.transactionType,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def cariTrxList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def c = Transaction.createCriteria()
        def results = c.list{
            eq("staDel","0")
            eq("companyDealer",params?.companyDealer)
            if (params."sCriteria_transactionCode") {
                ilike("transactionCode", "%" + (params."sCriteria_transactionCode" as String))
            }

            if (params."sCriteria_transactionDate_dp") {
                ge("transactionDate", Date.parse("dd/MM/yyyy", params."sCriteria_transactionDate_dp"))
                if (params."sCriteria_transactionDate_dp_to")
                    lt("transactionDate", Date.parse("dd/MM/yyyy", params."sCriteria_transactionDate_dp_to")+1)
            }

            if (params."sCriteria_total") {
                eq("amount", params?."sCriteria_total"?.toBigDecimal())
            }

            if (params."sCriteria_tipe") {
                transactionType {
                    ilike("transactionType", "%"+params."sCriteria_tipe"+"%")
                }
            }

            if (params."sCriteria_docNumber") {
                ilike("docNumber", "%" + (params."sCriteria_docNumber" as String) + "%")
            }

            if (params."sCriteria_deskripsi") {
                ilike("description", "%" + (params."sCriteria_deskripsi" as String) + "%")
            }

            if (params."sCriteria_kategori") {
                documentCategory{
                    ilike("description","%"+params."sCriteria_kategori"+"%")
                }
            }

            order("transactionDate","desc");

        }

        def rows = []

        results.each {

            rows << [

                    id: it?.id,

                    transactionCode: it?.transactionCode,

                    transactionDate: it?.transactionDate ? it.transactionDate.format(dateFormat) : "",

                    description: it?.description,

                    amount: konversi.toRupiah(it?.amount),

                    journal: it?.journal,

                    docNumber: it?.docNumber?it?.docNumber:"",

                    inOutType: it?.inOutType,

                    receiptNumber: it?.receiptNumber,

                    transferDate: it?.transferDate ? it?.transferDate.format(dateFormat) : "",

                    dueDate: it?.dueDate ? it?.dueDate.format(dateFormat) : "",

                    cekNumber: it?.cekNumber,

                    pdcStatus: it?.pdcStatus,

                    pdcTransferDate: it?.pdcTransferDate ? it?.pdcTransferDate.format(dateFormat) : "",

                    lastUpdProcess: it?.lastUpdProcess,

                    approval: it?.journal?.isApproval=="0"?"Need Approval":( it?.journal?.isApproval=="1"?"Approved":"Rejected"),

                    bankAccountNumber: it?.bankAccountNumber,

                    documentCategory: it?.documentCategory?.description,

                    transactionType: it?.transactionType?.transactionType,

            ]
        }

        return rows;

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Transaction", params.id]]
            return result
        }

        result.transactionInstance = Transaction.get(params.id)

        if (!result.transactionInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Transaction", params.id]]
            return result
        }

        result.transactionInstance = Transaction.get(params.id)

        if (!result.transactionInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Transaction", params.id]]
            return result
        }

        result.transactionInstance = Transaction.get(params.id)

        if (!result.transactionInstance)
            return fail(code: "default.not.found.message")

        try {
            result.transactionInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        Transaction.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.transactionInstance && m.field)
                    result.transactionInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Transaction", params.id]]
                return result
            }

            result.transactionInstance = Transaction.get(params.id)

            if (!result.transactionInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.transactionInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.transactionInstance.properties = params

            if (result.transactionInstance.hasErrors() || !result.transactionInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Transaction", params.id]]
            return result
        }

        result.transactionInstance = new Transaction()
        result.transactionInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.transactionInstance && m.field)
                result.transactionInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Transaction", params.id]]
            return result
        }

        result.transactionInstance = new Transaction(params)

        if (result.transactionInstance.hasErrors() || !result.transactionInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def transaction = Transaction.findById(params.id)
        if (transaction) {
            transaction."${params.name}" = params.value
            transaction.save()
            if (transaction.hasErrors()) {
                throw new Exception("${transaction.errors}")
            }
        } else {
            throw new Exception("Transaction not found")
        }
    }

    def nomorPOSublistDT(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = InvoiceSublet.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", params.companyDealer)
            if(params."sCriteria_nomorPOSublet"){
                ilike("t413NoInv", "%"+params.sCriteria_nomorPOSublet+"%")
            }
            if(params."sCriteria_noWo"){
                reception{
                    ilike("t401NoWO", "%"+params.sCriteria_noWo+"%")
                }
            }
            if(params."sCriteria_nopol"){
                reception{
                    historyCustomerVehicle{
                        ilike("fullNoPol", "%"+params.sCriteria_nopol+"%")
                    }
                }
            }
            if(params."sCriteria_namaCustomer"){
                reception{
                    historyCustomer{
                        ilike("fullNama", "%"+params.sCriteria_namaCustomer+"%")
                    }
                }
            }
            if(params."sCriteria_namaBengkel"){
                reception{
                    historyCustomer{
                        ilike("fullNama", "%"+params.sCriteria_namaBengkel+"%")
                    }
                }
            }
            reception{
                order("t401NoWO")
            }
        }

        def rows = []
        def dataNoInvoice = ""

        results.each {
            def harga = 0
            def bayar = 0
            if(dataNoInvoice != it.t413NoInv){
                def invoice = InvoiceSublet.findAllByT413NoInvAndStaDel(it.t413NoInv,'0')
                invoice.each {
                    harga += it?.t413JmlSublet?it?.t413JmlSublet:0
                    bayar = it?.t413JmlBayar?it?.t413JmlBayar:0
                }

                def operation           = it.operation
                def defaultVendorSublet = DefaultVendorSublet.findByOperation(operation)
                def vendor2 = JobRCP.findByOperationAndReceptionAndStaDel(it.operation,it.reception,"0")
                def vendor              = vendor2?.vendor ? vendor2?.vendor?.m121Nama : defaultVendorSublet?.vendor?.m121Nama
                if(bayar != harga){
                    rows << [

                            t412NoPOSublet: it.t413NoInv,
                            reception: it.reception.t401NoWO,
                            nomorPolisi: it.reception.historyCustomerVehicle.fullNoPol,
                            namaPelanggan: it.reception.historyCustomer.fullNama,
                            vendor: vendor,
                            harga : konversi?.toRupiah(harga)
                    ]
                }
            }
            dataNoInvoice = it.t413NoInv
        }

        [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

    }

    def nomorKwitansiDT(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def totalC = Collection167.createCriteria()
        def totalR = totalC.list() {
            eq("companyDealer",params.companyDealer)
            ilike("paymentStatus","%BELUM%")
            goodsReceive{
                eq("companyDealer",params.companyDealer)

                if (params."sCriteria_t167NoReff") {
                    ilike("t167NoReff", "%" + (params."sCriteria_t167NoReff" as String) + "%")
                }
                if (params."sCriteria_vendor") {
                    vendor{
                        ilike("m121Nama", "%" + (params."sCriteria_vendor" as String) + "%")
                    }
                }
                if (params."sCriteria_tanggalKwitansi") {
                        ge("t167TglJamReceive", (params."sCriteria_tanggalKwitansi"))
                        lt("t167TglJamReceive", (params."sCriteria_tanggalKwitansi"))
                }
                order("dateCreated")
            }
        }

        def c = Collection167.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params.companyDealer)
            ilike("paymentStatus","%BELUM%")
            goodsReceive{
                eq("companyDealer",params.companyDealer)

                if (params."sCriteria_t167NoReff") {
                    ilike("t167NoReff", "%" + (params."sCriteria_t167NoReff" as String) + "%")
                }
                if (params."sCriteria_vendor") {
                    vendor{
                        ilike("m121Nama", "%" + (params."sCriteria_vendor" as String) + "%")
                    }
                }
                if (params."sCriteria_tanggalKwitansi") {
                    ge("t167TglJamReceive", (params."sCriteria_tanggalKwitansi"))
                    lt("t167TglJamReceive", (params."sCriteria_tanggalKwitansi"))
                }
                order("dateCreated","desc")
            }
        }

        def rows = []
        results.each {
            def jumlahTagih = it.amount
            rows << [
                    id: it.id,
                    noReff: it.goodsReceive?.t167NoReff,
                    idReff: it.goodsReceive?.id,
                    tglReff: it.goodsReceive?.t167TglJamReceive?.format("dd/MM/yyyy"),
                    vendor: it?.goodsReceive?.vendor?.m121Nama,
                    tagihan: konversi?.toRupiah(jumlahTagih)

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: totalR.size(), iTotalDisplayRecords: totalR.size(), aaData: rows]

    }

    def nomorKwitansiT701DT(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def d = Collection.createCriteria()
        def results = d.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params.companyDealer)
            ilike("paymentStatus","%BELUM%")
            invoiceT071{
                eq("companyDealer",params.companyDealer)
                eq("t701JenisInv","K")

                if (params."sCriteria_noInv") {
                    ilike("t701NoInv", "%" + (params."sCriteria_noInv" as String) + "%")
                }

                if (params."sCriteria_noPol") {
                    ilike("t701Nopol", "%" + (params."sCriteria_noPol" as String) + "%")
                }

                if (params."sCriteria_customer") {
                    ilike("t701Customer", "%" + (params."sCriteria_customer" as String) + "%")
                }

                not{
                    or{
                        ilike("t701Customer", "%hasjrat%")
                        ilike("t701Customer", "%kombos%")
                    }
                }
                order("dateCreated")
            }
        }

        def rows = []

        results.each {
            rows << [
                    t701NoInv: it?.invoiceT071?.t701NoInv,
                    t701Nopol: it?.invoiceT071?.t701Nopol,
                    T701Customer: it?.invoiceT071?.t701Customer

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def nomorDeliveryOrder(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = DeliveryOrder.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",params.companyDealer)
            ilike("paymentStatus","%BELUM%")
            if (params."sCriteria_noDO") {
                ilike("doNumber", "%" + (params."sCriteria_noDO" as String) + "%")
            }

            if (params."sCriteria_noSO") {
                sorNumber{
                    ilike("sorNumber", "%" + (params."sCriteria_noSO" as String) + "%")
                }
            }

            if (params."sCriteria_buyer") {
                sorNumber{
                    ilike("customerName", "%" + (params."sCriteria_buyer" as String) + "%")
                }
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [
                    doNumber: it.doNumber,
                    soNumber: it.sorNumber.sorNumber,
                    buyer: it.sorNumber.customerName

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


    def saveByDocCategory(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.transactionInstance && m.field)
                result.transactionInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Transaction", params.id]]
            return result
        }

        def documentCategory = DocumentCategory.findByDocumentCategory(params.documentCategory)
        def bankAccount      = params.bank ? BankAccountNumber.get(params.bank.toLong()): null
//        def bankAccount      = bank != null? BankAccountNumber.findByBank(bank) : null
        def transactionType  = TransactionType.findByTransactionType(params.transactionType)
        def transaction      = new Transaction()

        transaction.companyDealer     = params.companyDealer
        transaction.transactionCode   = new GenerateCodeService().generateTransactionCode()
        transaction.transactionType   = transactionType
        transaction.documentCategory  = documentCategory
        transaction.docNumber         = params.docNumber
        transaction.amount            = params?.amount?.toString()?.toBigDecimal()
        transaction.transferDate      = params.transferDate? Date.parse("dd/MM/yyyy", params.transferDate as String) : null
        transaction.transactionDate   = Date.parse("dd/MM/yyyy", params.transactionDate as String)
        transaction.description       = params.description
        transaction.cekNumber         = params.cekNumber ? params.cekNumber : null
        transaction.dueDate           = params.dueDate ? Date.parse("dd/MM/yyyy", params.dueDate as String) : null
        transaction.bankAccountNumber = bankAccount != null ? bankAccount : null
        transaction.staDel            = "0"
        transaction.createdBy         = org.apache.shiro.SecurityUtils.subject.principal.toString()
        transaction.lastUpdProcess    = "INSERT"
        transaction?.dateCreated      = datatablesUtilService?.syncTime()
        transaction?.lastUpdated      = datatablesUtilService?.syncTime()
        transaction.save(flush: true, failOnError: true)

        if (transaction.hasErrors()) {
            transaction.errors.each {
                //println it
            }
            result.error = transaction.errors
        }

        def invoice = null
        if ((params.documentCategory as String).equals("DOCSB")) {
            invoice = InvoiceSublet.findAllByT413NoInvAndStaDel(params.docNumber,'0')
            double jumlahSublet = 0
            invoice.each {
                jumlahSublet += it?.t413JmlSublet?it?.t413JmlSublet:0
                def ubhInvoice = InvoiceSublet.get(it.id)
                ubhInvoice?.t413JmlBayar = ubhInvoice?.t413JmlBayar + Double.parseDouble(params.amount)
                ubhInvoice?.save(flush: true)
                ubhInvoice?.errors?.each {
                    //println it
                }
            }

        }
        result.transactionInstance = transaction
        result.success = "Success"

        // success
        return result
    }

}