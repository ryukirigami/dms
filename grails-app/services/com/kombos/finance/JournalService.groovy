package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class JournalService {
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

    def bukuBesarDTList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def akun = params.akun.toString().contains("|") ? params.akun.toString().substring(0,params.akun.toString().indexOf("|")): params.akun.toString().replace(" ","")
        def accNumber  = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")
        def c = JournalDetail.createCriteria()
        def results = c.list{
            if(params.sCriteria_description){
                or{
                    ilike("description","%" + (params.sCriteria_description as String) + "%")
                    journal {
                        ilike("description","%" + (params.sCriteria_description as String) + "%")
                    }
                }
            }

            if(params.sCriteria_debet){
                eq("debitAmount", (params.sCriteria_debet as BigDecimal))
            }

            if(params.sCriteria_credit){
                eq("creditAmount",(params.sCriteria_credit as BigDecimal))
            }

            eq("staDel", "0")
            journal {
                eq("isApproval", "1")
                eq("staDel", "0")
                eq("companyDealer",params?.companyDealer);
            }
            if(params.subLedger){
                eq("subLedger",params.subLedger,[ignoreCase : true])
                subType{
                    eq("id",params.subType.toLong())
                }
            }
            if(params."sCriteria_journalDate_dp"){
                journal {
                    ge("journalDate", Date.parse("dd/MM/yyyy", params."sCriteria_journalDate_dp"))
                    lt("journalDate", Date.parse("dd/MM/yyyy", params."sCriteria_journalDate_dp_to")+1)
                }
            }

            if (params.akun) {
                eq("accountNumber",accNumber)
            }

            journal{
                order("journalDate","asc")
                order("journalCode","asc")
            }
        }

        def rows = []
        def saldoAwal = params.saldoAwal
        try {
            saldoAwal = (saldoAwal as String).replaceAll(',','').toBigDecimal()
        }catch (Exception e){
            saldoAwal = 0.0
        }
        if(accNumber.accountType.saldoNormal == "KREDIT"){
            saldoAwal = saldoAwal* (-1)
        }
        def tempTotal = saldoAwal
        def tempTotDebet = 0
        def tempTotKredit = 0
        def konversi = new Konversi()
        results.each {
            def journal = it.journal
            tempTotal = ( tempTotal + it.debitAmount)- it.creditAmount
            tempTotDebet += it.debitAmount
            tempTotKredit += it.creditAmount
            rows << [

                    id: it.id,

                    idJurnal: journal.id,

                    journalCode: journal.journalCode,

                    journalDate: it.journal.journalDate?it.journal.journalDate.format(dateFormat):"",

                    description: (!it?.description || it?.description=="-")? it.journal?.description:it?.description,

                    totalDebit: konversi?.toRupiah(it.debitAmount),

                    totalCredit: konversi?.toRupiah(it.creditAmount),

                    docNumber: it.journal.docNumber,

                    isApproval: it.journal.isApproval,

                    subType: it.subType,

                    subLedger: it.subLedger,

                    journalType: it.journal.journalType,

                    total: konversi?.toRupiah(tempTotal) ,

                    allDebet: konversi.toRupiah(tempTotDebet),

                    allCredit: konversi?.toRupiah(tempTotKredit)


            ]
        }

        return rows
    }

    def jurnalTrxDTList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def c = Journal.createCriteria()
        def results = c.list{

            if(params.sCriteria_journalType){
                journalType{
                    eq("journalType",(params.sCriteria_journalType as String))
                }
            }

            if(params.sCriteria_description){
                ilike("description","%" + (params.sCriteria_description as String) + "%")
            }

            if(params.sCriteria_debet){
                eq("totalDebit", (params.sCriteria_debet as BigDecimal))
            }

            if(params.sCriteria_credit){
                eq("totalCredit",(params.sCriteria_credit as BigDecimal))
            }

            eq("isApproval", "1")
            eq("staDel", "0")
            eq("companyDealer",params?.companyDealer);

            if(params."sCriteria_journalDate_dp"){
                ge("journalDate", Date.parse("dd/MM/yyyy", params."sCriteria_journalDate_dp"))
                lt("journalDate", Date.parse("dd/MM/yyyy", params."sCriteria_journalDate_dp_to")+1)
            }

            if (params."journalCode") {
                ilike("journalCode","%" + (params.journalCode as String).trim())
            }

            order("journalDate","desc")
        }


        def konversi = new Konversi()

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    journalCode: it.journalCode,

                    journalDate: it.journalDate?it.journalDate.format(dateFormat):"",

                    description: it?.description,

                    totalDebit: konversi?.toRupiah(it.totalDebit),

                    totalCredit: konversi?.toRupiah(it.totalCredit),

                    docNumber: it.docNumber,

                    isApproval: it.isApproval,

                    journalType: it.journalType


            ]
        }

        return rows
    }

	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Journal", params.id] ]
			return result
		}

		result.journalInstance = Journal.get(params.id)

		if(!result.journalInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Journal", params.id] ]
			return result
		}

		result.journalInstance = Journal.get(params.id)

        if(!result.journalInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def deleteJurnal(params){
        def data = "not"
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { id->
            def journal = Journal.get(id as Long)
            journal.staDel = '1'
            journal.lastUpdProcess = 'DELETE'
            journal.save(flush: true)
            JournalDetail.findAllByJournal(journal).each {
                data = "sukses"
                def jd = JournalDetail.get(it.id as Long)
                jd.staDel = '1'
                jd.lastUpdProcess = 'DELETE'
                jd.save(flush: true)
            }
            try {
                Transaction.findAllByJournal(journal).each {
                    def trx = Transaction.get(it.id as Long)
                    trx.staDel = '1'
                    trx.lastUpdProcess = 'DELETE'
                    trx.save(flush: true)
                }
            }catch (Exception e){

            }
        }
        return data
    }

	def update(params) {
		/*Journal.withTransaction { status ->*/
			def result = [:]

			def fail = { Map m ->
				/*status.setRollbackOnly()*/
				if(result.journalInstance && m.field)
					result.journalInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Journal", params.id] ]
				return result
			}

			result.journalInstance = Journal.get(params.id)
            result.journalInstance.totalCredit = Float.parseFloat(params.totalCredit)
            result.journalInstance.totalDebit = Float.parseFloat(params.totalDebit)
            result.journalInstance.save(flush: true)

            if (result.journalInstance) {
                def jsonArray = JSON.parse(params.journalDetails)
                def rowCount = 0
                jsonArray.each {d ->
                    def journalDetails
                    if (d.detailId)   {
                        journalDetails = JournalDetail.findById(Integer.parseInt(d.detailId as String))
                        journalDetails.lastUpdProcess = "UPDATE"
                        journalDetails?.lastUpdated = datatablesUtilService?.syncTime()
                    } else {
                        journalDetails = new JournalDetail()
                        journalDetails.staDel = "0"
                        journalDetails.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        journalDetails.lastUpdProcess = "INSERT"
                        journalDetails?.dateCreated = datatablesUtilService?.syncTime()
                    }
                    def subType = d.subTypeId ? SubType.findByDescriptionIlike("%"+d.subTypeId+"%") : null
                    journalDetails.subType = subType
                    journalDetails.subLedger = d.subLedger

                    def accountNumber = AccountNumber.findById(Integer.parseInt(d.id as String))
                    journalDetails.accountNumber = accountNumber
                    journalDetails.lastUpdProcess = "Update"
                    journalDetails.description = d.desc
                    journalDetails.journal = result.journalInstance
                    journalDetails.creditAmount = (d.journalTypeTransaction as String).equals("Kredit")?Float.parseFloat(d.jumlah as String):0
                    journalDetails.debitAmount = (d.journalTypeTransaction as String).equals("Debet")?Float.parseFloat(d.jumlah as String):0
                    journalDetails.journalTransactionType = d.journalTypeTransaction

                    if (journalDetails.hasErrors()) {
                        throw new Exception("${journalDetails.errors}")
                    }
                    if (!journalDetails.save(failOnError: true) || journalDetails.hasErrors()) {
                       return fail(code: "default.not.updated.message")
                    }
                }
            }

			if(!result.journalInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.journalInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.journalInstance.properties = params

			if(result.journalInstance.hasErrors() || !result.journalInstance.save())
				return fail(code:"default.not.updated.message")


			// Success.
			return result

		/*} //end withTransaction*/
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Journal", params.id] ]
			return result
		}

		result.journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        params.companyDealer = session.userCompanyDealer
        result.journalInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.journalInstance && m.field)
				result.journalInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Journal", params.id] ]
			return result
		}

        def journalType = JournalType.findByJournalType("MJ")
        if(params.docNumber=='-' || params.docNumber==''){
            params.docNumber = params.journalCode
        }
		result.journalInstance = new Journal()
        result.journalInstance.journalCode = params.journalCode
        result.journalInstance.companyDealer = params.companyDealer
        result.journalInstance.journalDate = Date.parse("dd/MM/yyyy", params.journalDate as String)
        result.journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        result.journalInstance.lastUpdProcess = "INSERT"
        result.journalInstance.dateCreated = datatablesUtilService?.syncTime()
        result.journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        result.journalInstance.journalType = journalType
        result.journalInstance.description = params.description
        result.journalInstance.staDel = 0
        result.journalInstance.isApproval = "0" // need approval
        result.journalInstance.docNumber = params.docNumber
        result.journalInstance.totalCredit = params.totalCredit.toString().toBigDecimal()
        result.journalInstance.totalDebit = params.totalDebit.toString().toBigDecimal()
        result.journalInstance.save(failOnError: true)
		if(result.journalInstance.hasErrors() || !result.journalInstance.save(flush: true))
			return fail(code:"default.not.created.message")
        else {
            // jika sukses
            // bikin detail journal nya sebanyak row
            def jsonArray = JSON.parse(params.journalDetails)
            def rowCount = 0
            jsonArray.each {d ->
                def an = AccountNumber.findById(Integer.parseInt(d.id as String))
                def subType = d.subTypeId ? SubType.findByDescriptionIlike("%"+d.subTypeId+"%") : null
                def journalDetail = new JournalDetail(
                        journalTransactionType:d.journalTypeTransaction,
                        subType: subType,
                        creditAmount: (d.journalTypeTransaction as String).equals("Kredit")?d.jumlah:0,
                        debitAmount: (d.journalTypeTransaction as String).equals("Debet")?d.jumlah:0,
                        staDel: 0,
                        accountNumber: an,
                        journal: result.journalInstance,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime(),
                        subLedger: d.subLedger,
                        description: d.desc
                )
                if (journalDetail.save(flush: true, failOnError: true)) {
                    rowCount++
                    //println "====> ${rowCount}"
                }
            }
            result.rowCount = rowCount
        }

		// success
		return result
	}
	
	def updateField(def params){
		def journal =  Journal.findById(params.id)
		if (journal) {
			journal."${params.name}" = params.value
			journal.save()
			if (journal.hasErrors()) {
				throw new Exception("${journal.errors}")
			}
		}else{
			throw new Exception("Journal not found")
		}
	}


    def accountNumberDT(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = AccountNumber.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("accountMutationType", "MUTASI")
            if (params."sCriteria_nomorAkun") {
                ilike("accountNumber", "%" + (params."sCriteria_nomorAkun" as String) + "%")
            }

            if (params."sCriteria_namaAkun") {
                ilike("accountName", "%" + (params."sCriteria_namaAkun" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,
                    accountNumber: it.accountNumber,
                    accountName: it.accountName
            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
}