package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class AssetService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Asset.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("companyDealer", params?.companyDealer )
			if(params."sCriteria_assetName"){
				ilike("assetName","%" + (params."sCriteria_assetName" as String) + "%")
			}

			if(params."sCriteria_assetCode"){
				ilike("assetCode","%" + (params."sCriteria_assetCode" as String) + "%")
			}
            if(params."sCriteria_assetType"){
                assetType {
                    ilike("typeAsset","%" + (params."sCriteria_assetType" as String) + "%")
                }
            }

            if(params."sCriteria_kondisi"){
                assetStatus{
                    ilike("assetStatus","%" + (params."sCriteria_kondisi" as String) + "%")
                }
            }

			if(params."sCriteria_jumlah"){
				eq("quantity",params."sCriteria_jumlah" as int)
			}

            if(params."sCriteria_tahunPerolehan"){
                ilike("purchaseDate","%" + (params."sCriteria_tahunPerolehan" as String) + "%")
            }

			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						assetName: it.assetName,
			
						assetCode: it.assetCode,
			
						quantity: it.quantity,
			
						purchaseDate: it.purchaseDate,
			
						description: it.description,
			
						assetStatus: it.assetStatus.assetStatus,
			
						assetType: it.assetType.typeAsset
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Asset", params.id] ]
			return result
		}

		result.assetInstance = Asset.get(params.id)

		if(!result.assetInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Asset", params.id] ]
			return result
		}

		result.assetInstance = Asset.get(params.id)

		if(!result.assetInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Asset", params.id] ]
			return result
		}

		result.assetInstance = Asset.get(params.id)

		if(!result.assetInstance)
			return fail(code:"default.not.found.message")

		try {
			result.assetInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		Asset.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.assetInstance && m.field)
					result.assetInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Asset", params.id] ]
				return result
			}

			result.assetInstance = Asset.get(params.id)

			if(!result.assetInstance)
				return fail(code:"default.not.found.message")

            if (params.tanggalOpname && params.petugasOpname) {
                def assetStatus = AssetStatus.findById(Long.parseLong(params.assetStatus.id))
                def opname = new AssetOpname(
                        opnameDate: Date.parse("dd/MM/yyyy", params.tanggalOpname_dp as String),
                        petugasOpname: params.petugasOpname,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "UPDATE",
                        asset: result.assetInstance,
                        staDel: "0",
                        assetStatus: assetStatus,
                        description: params.description,
                        params.lastUpdated = datatablesUtilService?.syncTime()
                ).save(flush: true, failOnError: true)

                if (opname.hasErrors()) {
                    return fail(code: "default.not.updated.message")
                }
            }

			// Optimistic locking check.
			if(params.version) {
				if(result.assetInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.assetInstance.properties = params

			if(result.assetInstance.hasErrors() || !result.assetInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Asset", params.id] ]
			return result
		}

		result.assetInstance = new Asset()
		result.assetInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.assetInstance && m.field)
				result.assetInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Asset", params.id] ]
			return result
		}

        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        result.assetInstance = new Asset(params)

		if(result.assetInstance.hasErrors() || !result.assetInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def asset =  Asset.findById(params.id)
		if (asset) {
			asset."${params.name}" = params.value
			asset.save()
			if (asset.hasErrors()) {
				throw new Exception("${asset.errors}")
			}
		}else{
			throw new Exception("Asset not found")
		}
	}

}