package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class AccountTypeService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = AccountType.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_accountType") {
                ilike("accountType", "%" + (params."sCriteria_accountType" as String) + "%")
            }

            if (params."sCriteria_saldoNormal") {
                ilike("saldoNormal", "%" + (params."sCriteria_saldoNormal" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_accountNumber") {
                eq("accountNumber", params."sCriteria_accountNumber")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    accountType: it.accountType,

                    saldoNormal: it.saldoNormal,

                    lastUpdProcess: it.lastUpdProcess,

                    accountNumber: it.accountNumber.accountName,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AccountType", params.id]]
            return result
        }

        result.accountTypeInstance = AccountType.get(params.id)

        if (!result.accountTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AccountType", params.id]]
            return result
        }

        result.accountTypeInstance = AccountType.get(params.id)

        if (!result.accountTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AccountType", params.id]]
            return result
        }

        result.accountTypeInstance = AccountType.get(params.id)

        if (!result.accountTypeInstance)
            return fail(code: "default.not.found.message")

        try {
            result.accountTypeInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        AccountType.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.accountTypeInstance && m.field)
                    result.accountTypeInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["AccountType", params.id]]
                return result
            }

            result.accountTypeInstance = AccountType.get(params.id)

            if (!result.accountTypeInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.accountTypeInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.accountTypeInstance.properties = params

            if (result.accountTypeInstance.hasErrors() || !result.accountTypeInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AccountType", params.id]]
            return result
        }

        result.accountTypeInstance = new AccountType()
        result.accountTypeInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.accountTypeInstance && m.field)
                result.accountTypeInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["AccountType", params.id]]
            return result
        }

        result.accountTypeInstance = new AccountType(params)

        if (result.accountTypeInstance.hasErrors() || !result.accountTypeInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def accountType = AccountType.findById(params.id)
        if (accountType) {
            accountType."${params.name}" = params.value
            accountType.save()
            if (accountType.hasErrors()) {
                throw new Exception("${accountType.errors}")
            }
        } else {
            throw new Exception("AccountType not found")
        }
    }
//woke
}
