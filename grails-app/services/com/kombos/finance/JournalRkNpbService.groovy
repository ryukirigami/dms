package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.GoodsHargaBeli
import com.kombos.parts.GoodsReceive
import com.kombos.parts.GoodsReceiveDetail
import com.kombos.parts.KlasifikasiGoods
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class JournalRkNpbService {

    boolean transactional = false
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def createJournal(params) {
        /*  params :
              goodsReceive
              noReferensi
        */
        def goodreceive = GoodsReceive.findById(params.goodsReceive as Long)
        def grd = GoodsReceiveDetail.findAllByGoodsReceive(goodreceive)
        //println  "grd ==> " + grd.dump()
        def totalBayar = 0
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.journalDate = new Date()
        journalInstance.totalDebit = Math.round(calculateTotal(grd, "debet",session.userCompanyDealer))
        journalInstance.totalCredit = Math.round(calculateTotal(grd, "kredit",session.userCompanyDealer))
        journalInstance.docNumber = params.noReferensi
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = "Journal R/K " + goodreceive.companyDealerSupply.m011NamaWorkshop
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal  mappingJournal = MappingJournal.findByMappingCode("MAP-TJNPB")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber

                totalBayar = getAmount(accountNumber, grd,(mappingJournalDetail.accountTransactionType as String),session?.userCompanyDealer)
                //println "Akun ===>< " + accountNumber.accountName + " - " + accountNumber.accountNumber + " - " + totalBayar
                if(totalBayar!=0){
                    if(accountNumber.accountNumber.equals("1.1.6.12.002")){
                        //println "Masuk Sini"
                        accountNumber = goodreceive.companyDealerSupply.m011NomorAkunLainnya
                    }
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: ""
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                        //println "====> ${rowCount}"
                    }
                }
            }
            //println "RKNPB succes"
        }
    }

    def getMappingJournal() {

    }

    def calculateTotal(grd, type,CompanyDealer companyDealer) {
        if((type as String).equals("debet")) {
            def harga = 0
            grd.each{
                harga += it.t167Qty1Issued * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer)?.t150Harga : 0)
            }
            return harga
        } else if((type as String).equals("kredit")) {
            def harga = 0
            grd.each{
                harga += it.t167Qty1Issued * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer)?.t150Harga : 0)
            }
            return harga
        } else {
            return 0
        }
    }

    def getAmount(accountNumber, grd, accountTransactionType,CompanyDealer companyDealer) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber) {
                case "1.60.10.01.001" : //persediaan parts toyota
                    def harga = 0
                    grd.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        //println  klas?.franc?.m117NamaFranc
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("TOYOTA")){
                            harga += it.t167Qty1Issued * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer)?.t150Harga : 0)
                        }
                    }
                    return harga                   //untuk sementara 0, menunggu mapping source
                case "1.60.10.01.003" : //persediaan parts campuran
                    def harga = 0
                    grd.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        //println  klas?.franc?.m117NamaFranc
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                            harga += it.t167Qty1Issued * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer)?.t150Harga : 0)
                        }
                    }
                    return harga                   //untuk sementara 0, menunggu mapping source
                case "1.60.10.02.001" :  //parts bahan
                    def harga = 0
                    grd.each{
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAT") ){
                            harga += it.t167Qty1Issued * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer)?.t150Harga : 0)
                        }
                    }
                    return harga
                default:
                    return 0
            }

        } else {
            switch (accountNumber.accountNumber) {
                case "1.1.6.12.002" :  // r/k cvk jakarta
                    def harga = 0
                    grd.each{
                        harga += it.t167Qty1Issued * (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",companyDealer)?.t150Harga : 0)
                    }
                    return harga
                default:
                    return 0
            }
        }

    }

//    def generateJournalCode(params) {
//        Date date = new Date()
//        SimpleDateFormat dateFormat = new SimpleDateFormat("MMYYYY")
//        String dateString = dateFormat.format(date)
//        String prefix = params
//        String rpadding = "000000"
//        def lastJournal = Journal.last()
//        Long id = 1
//        if (lastJournal) {
//            id = lastJournal.id +1
//        }
//        return prefix + dateString + rpadding + String.valueOf(id)
//    }
}
