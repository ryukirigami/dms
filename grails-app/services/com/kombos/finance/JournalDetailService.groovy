package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class JournalDetailService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = JournalDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_journal") {
                journal {
                    ilike("journalCode", "%" + params."sCriteria_journal" + "%")
                }
            }

            if (params."sCriteria_accountNumber") {
                accountNumber {
                    ilike("accountNumber", "%" + params."sCriteria_accountNumber" + "%")
                }
            }

            if (params."sCriteria_subType") {
                subType {
                    ilike("subType", "%" + params."sCriteria_subType" + "%")
                }
            }

            if (params."sCriteria_debitAmount") {
                eq("debitAmount", Float.parseFloat(params."sCriteria_debitAmount"))
            }

            if (params."sCriteria_creditAmount") {
                eq("creditAmount", Float.parseFloat(params."sCriteria_creditAmount"))
            }

            if (params."sCriteria_journalTransactionType") {
                ilike("journalTransactionType", "%" + (params."sCriteria_journalTransactionType" as String) + "%")
            }

            if (params."sCriteria_subLedger") {
                ilike("subLedger", "%" + (params."sCriteria_subLedger" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    journal: it.journal.journalCode,

                    accountNumber: it.accountNumber.accountNumber,

                    subType: it.subType ? it.subType.subType : "",

                    debitAmount: it.debitAmount,

                    creditAmount: it.creditAmount,

                    journalTransactionType: it.journalTransactionType,

                    subLedger: it.subLedger,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JournalDetail", params.id]]
            return result
        }

        result.journalDetailInstance = JournalDetail.get(params.id)

        if (!result.journalDetailInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JournalDetail", params.id]]
            return result
        }

        result.journalDetailInstance = JournalDetail.get(params.id)

        if (!result.journalDetailInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JournalDetail", params.id]]
            return result
        }

        result.journalDetailInstance = JournalDetail.get(params.id)

        if (!result.journalDetailInstance)
            return fail(code: "default.not.found.message")

        try {
            result.journalDetailInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        JournalDetail.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.journalDetailInstance && m.field)
                    result.journalDetailInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["JournalDetail", params.id]]
                return result
            }

            result.journalDetailInstance = JournalDetail.get(params.id)

            if (!result.journalDetailInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.journalDetailInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.journalDetailInstance.properties = params

            if (result.journalDetailInstance.hasErrors() || !result.journalDetailInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["JournalDetail", params.id]]
            return result
        }

        result.journalDetailInstance = new JournalDetail()
        result.journalDetailInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.journalDetailInstance && m.field)
                result.journalDetailInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["JournalDetail", params.id]]
            return result
        }

        result.journalDetailInstance = new JournalDetail(params)

        if (result.journalDetailInstance.hasErrors() || !result.journalDetailInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def journalDetail = JournalDetail.findById(params.id)
        if (journalDetail) {
            journalDetail."${params.name}" = params.value
            journalDetail.save()
            if (journalDetail.hasErrors()) {
                throw new Exception("${journalDetail.errors}")
            }
        } else {
            throw new Exception("JournalDetail not found")
        }
    }

}