package com.kombos.finance



import com.kombos.baseapp.AppSettingParam

class SubTypeService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = SubType.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_subType") {
                ilike("subType", "%" + (params."sCriteria_subType" as String) + "%")
            }

            if (params."sCriteria_description") {
                ilike("description", "%" + (params."sCriteria_description" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_collection") {
                eq("collection", params."sCriteria_collection")
            }

            if (params."sCriteria_journalDetail") {
                eq("journalDetail", params."sCriteria_journalDetail")
            }

            if (params."sCriteria_ledgerCardDetail") {
                eq("ledgerCardDetail", params."sCriteria_ledgerCardDetail")
            }

            if (params."sCriteria_monthlyBalance") {
                eq("monthlyBalance", params."sCriteria_monthlyBalance")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    subType: it.subType,

                    description: it.description,

                    lastUpdProcess: it.lastUpdProcess,

                    collection: it.collection,

                    journalDetail: it.journalDetail,

                    ledgerCardDetail: it.ledgerCardDetail,

                    monthlyBalance: it.monthlyBalance,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SubType", params.id]]
            return result
        }

        result.subTypeInstance = SubType.get(params.id)

        if (!result.subTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SubType", params.id]]
            return result
        }

        result.subTypeInstance = SubType.get(params.id)

        if (!result.subTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SubType", params.id]]
            return result
        }

        result.subTypeInstance = SubType.get(params.id)

        if (!result.subTypeInstance)
            return fail(code: "default.not.found.message")

        try {
            result.subTypeInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        SubType.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.subTypeInstance && m.field)
                    result.subTypeInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["SubType", params.id]]
                return result
            }

            result.subTypeInstance = SubType.get(params.id)

            if (!result.subTypeInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.subTypeInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.subTypeInstance.properties = params

            if (result.subTypeInstance.hasErrors() || !result.subTypeInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["SubType", params.id]]
            return result
        }

        result.subTypeInstance = new SubType()
        result.subTypeInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.subTypeInstance && m.field)
                result.subTypeInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["SubType", params.id]]
            return result
        }

        result.subTypeInstance = new SubType(params)

        if (result.subTypeInstance.hasErrors() || !result.subTypeInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def subType = SubType.findById(params.id)
        if (subType) {
            subType."${params.name}" = params.value
            subType.save()
            if (subType.hasErrors()) {
                throw new Exception("${subType.errors}")
            }
        } else {
            throw new Exception("SubType not found")
        }
    }

}