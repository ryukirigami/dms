package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.JenisPerusahaan
import com.kombos.generatecode.GenerateCodeService
import com.kombos.maintable.Company
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.PartInv
import com.kombos.parts.KlasifikasiGoods
import com.kombos.reception.JobInv
import org.springframework.web.context.request.RequestContextHolder

class InvoiceKreditJournalService {

    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def generateCodeService

    def createJournal(params) {
        /*  params :
            noInvoice
        */

        def invoice = InvoiceT701.findByT701NoInvAndT701StaDel( (params.noInvoice as String) ,"0")
        def totalBayar = 0
        def journalType = JournalType.findByJournalType("TJ")
        if (!journalType) {
            journalType = new JournalType(
                    journalType: "TJ",
                    description: "Transaction Journal", staDel: "0", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess: "INSERT"
            ).save(failOnError: true)
        }
        def journalInstance = new Journal()
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        journalInstance.journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journalInstance.companyDealer = session.userCompanyDealer
        journalInstance.journalDate = invoice?.t701TglJamInvoice
        journalInstance.totalDebit = Math.round(calculateTotal(invoice, "debet"))
        journalInstance.totalCredit = Math.round(calculateTotal(invoice, "kredit"))
        journalInstance.docNumber = params.noInvoice
        journalInstance.journalType = journalType
        journalInstance.isApproval = "1"
        journalInstance.staDel = "0"
        journalInstance.lastUpdProcess = "INSERT"
        journalInstance.description = "KREDIT INVOICE No. Inv "+invoice?.t701NoInv+" | "+invoice?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan +
                " | an. "+invoice?.t701Customer + " | No. Wo "+invoice?.reception.t401NoWO
        journalInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journalInstance.dateCreated = datatablesUtilService?.syncTime()
        journalInstance.lastUpdated = datatablesUtilService?.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journalInstance?.journalCode,journalInstance?.description,journalInstance?.totalDebit,journalInstance?.totalCredit);
        if(checkExist){
            return "Error"
        }
        journalInstance.save(failOnError: true)

        if (journalInstance.hasErrors() || !journalInstance.save(flush: true)) {
            return "Error"
        } else {
            MappingJournal  mappingJournal = MappingJournal.findByMappingCode("MAP-TJPKI")
            def mappingJournalDetails = mappingJournal.mappingJournalDetail

            int rowCount = 0
            mappingJournalDetails.each {
                MappingJournalDetail mappingJournalDetail = it
                def accountNumber = mappingJournalDetail.accountNumber

                totalBayar = getAmount(accountNumber, invoice,(mappingJournalDetail.accountTransactionType as String))

                if(totalBayar!=0){
                    def subType = null
                    def subLedger = null
                    if(it?.accountNumber?.accountNumber?.trim()=="1.30.10.01.001" || it?.accountNumber?.accountNumber?.trim()=="4.10.20.02.001"
                            || it?.accountNumber?.accountNumber?.trim()=="4.10.20.01.001"){
                        if(it?.accountNumber?.accountNumber?.trim()=="1.30.10.01.001" ){
                            def found = false;
                            if(invoice.t701Subtype){
                                subType = SubType?.findById(invoice.t701Subtype as Long)
                                subLedger =  invoice.t701Subledger
                                found = true;
                            }
                            def cari = VendorAsuransi.findByM193NamaIlikeAndStaDel(invoice?.t701Customer,"0")
                            if(cari && !found){
                                subType = SubType?.findByDescriptionIlikeAndStaDel("%ASURANSI","0")
                                subLedger = cari?.id
                                found = true;
                            }
                            def cari4 = Company.findByNamaPerusahaanAndStaDel(invoice?.t701Customer ,"0")
                            if(cari4 && !found){
                                subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER COMPANY","0")
                                subLedger = cari4?.id
                                found = true;
                            }
                            def cari2 = CompanyDealer.findByM011NamaWorkshopIlikeAndStaDel(invoice?.t701Customer,"0")
                            if(cari2 && !found){
                                subType = SubType?.findByDescriptionIlikeAndStaDel("%CABANG","0")
                                subLedger = cari2?.id
                                found = true;
                                accountNumber = cari2?.m011NomorAkunLainnya
                            }
                            def cari3 = HistoryCustomer.findByFullNamaIlikeAndT182AlamatAndStaDel(invoice?.t701Customer,invoice?.t701Alamat,"0")
                            if(cari3 && !found){
                                subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER","0")
                                subLedger = cari3?.id
                                found = true;
                            }
                            if(found == false){
                                def comp = new Company()
                                comp.companyDealer = invoice?.companyDealer
                                comp.kodePerusahaan = new GenerateCodeService().codeGenerateSequence("T101_ID",null)
                                comp.jenisPerusahaan  = JenisPerusahaan.findByNamaJenisPerusahaanAndStaDel("PT","0");
                                comp.namaPerusahaan = invoice?.t701Customer.toString()
                                comp.alamatPerusahaan = "INI DARI SISTEM KREDIT"
                                comp.telp = "0"
                                comp.namaDepanPIC = "INI DARI SISTEM KREDIT"
                                comp.alamatPIC = "INI DARI SISTEM KREDIT"
                                comp.telpPIC = "0"
                                comp.hpPIC = "0"
                                comp.alamatNPWP = "INI DARI SISTEM KREDIT"
                                comp.staDel = "0"
                                comp.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                comp.lastUpdProcess = "INSERT"
                                comp.dateCreated = datatablesUtilService?.syncTime()
                                comp.lastUpdated = datatablesUtilService?.syncTime()
                                comp.save(flush: true)
                                comp.errors.each {
                                    println it
                                }
                                if(!comp?.hasErrors()){
                                    subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER COMPANY","0")
                                    subLedger = comp?.id
                                }
                            }
                        }else{
                            def cari3 = HistoryCustomer.findByFullNamaIlikeAndStaDel(invoice?.reception.historyCustomer.fullNama,"0")
                            if(cari3){
                                subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER","0")
                                subLedger = cari3?.id
                            }
                        }
                    }
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Kredit")?Math.round(totalBayar):0,
                            debitAmount: (mappingJournalDetail.accountTransactionType as String)
                                    .equalsIgnoreCase("Debet")?Math.round(totalBayar):0,
                            staDel: "0",
                            accountNumber: accountNumber,
                            journal: journalInstance,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: subLedger,
                            subType : subType,
                            dateCreated: invoice?.t701TglJamInvoice,
                            lastUpdated: invoice?.t701TglJamInvoice
                    )
                    if (journalDetail.save(flush: true, failOnError: true)) {
                        rowCount++
                        //println "====> ${rowCount}"
                    }
                }
            }
            //println "KREDIT Invoice Journal Saved"
        }
    }

    def getMappingJournal() {

    }

    def calculateTotal(invoice, type) {
        if((type as String).equals("debet")) {
            return (invoice?.t701TotalBayarRp + invoice?.t701BookingFee + 0 + 0 + 0 + invoice?.t701TotalDiscRp + invoice?.t701OnRisk)
        } else if((type as String).equals("kredit")) {
            return (invoice.t701JasaRp + invoice.t701PartsRp + invoice.t701MaterialRp + invoice.t701OliRp +
                    invoice.t701SubletRp + invoice.t701AdmRp + (invoice?.t701PPnRp?invoice?.t701PPnRp:0) + invoice?.t701MateraiRp)
        } else {
            return 0
        }
    }

    def getAmount(AccountNumber accountNumber,InvoiceT701 invoice, accountTransactionType) {
        if(accountTransactionType.equalsIgnoreCase("Debet")) {
            switch (accountNumber.accountNumber) {
                case "1.30.10.01.001" :
                    def disc = 0
                    return invoice.t701TotalBayarRp;
                case "6.10.30.01.001" :
                    def harga = 0
                    def jobRcp = JobInv.findAllByInvoiceAndStaDel(invoice,"0")
                    jobRcp.each {
                        harga += it.t702DiscRp ? it.t702DiscRp : 0
                    }
                    return harga                    //untuk sementara 0, menunggu mapping source
                case "6.10.30.01.002" :
                    def harga = 0
                    def parts = PartInv.findAllByInvoiceAndT703StaDel(invoice,"0")
                    parts.each {
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(!klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL") && !klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                            harga += it.t703DiscRp ? it.t703DiscRp : 0
                        }
                    }
                    return harga                    //untuk sementara 0, menunggu mapping source
                case "6.10.30.01.003" :
                    def harga = 0
                    def parts = PartInv.findAllByInvoiceAndT703StaDel(invoice,"0")
                    parts.each {
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                            harga += it.t703DiscRp ? it.t703DiscRp : 0
                        }
                    }

                    return harga                    //untuk sementara 0, menunggu mapping source
                case "4.10.20.01.001" :
                    def harga = 0
                    if(invoice?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.equalsIgnoreCase("GR")){
                        harga = ((invoice?.t701BookingFee ? invoice?.t701BookingFee : 0) + (invoice?.t701OnRisk ? invoice?.t701OnRisk : 0))
                    }
                    return harga
                case "4.10.20.02.001" :
                    def harga = 0
                    if(invoice?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.equalsIgnoreCase("BP")){
                        harga = ((invoice?.t701BookingFee ? invoice?.t701BookingFee : 0) + (invoice?.t701OnRisk ? invoice?.t701OnRisk : 0))
                    }
                    return harga
                default:
                    return 0
            }
        } else {
            switch (accountNumber?.accountNumber?.toLowerCase()) {
                case "6.10.10.01.002" :
                    return invoice?.t701JasaRp
                case "6.10.10.02.002" :
                    return invoice?.t701PartsRp
                case "6.10.10.03.002" :
                    return invoice?.t701MaterialRp + invoice?.t701OliRp
                case "6.10.10.04.002" :
                    return invoice?.t701SubletRp
                case "6.10.10.05.002" :
                    return invoice?.t701AdmRp
                case "4.20.10.01.001" :
                    return invoice?.t701PPnRp
                case "8.10.10.04.001" :
                    return invoice?.t701MateraiRp
                default:
                    return 0
            }
        }
    }
}
