package com.kombos.sms

import com.kombos.maintable.AntrianSms
import org.activiti.engine.impl.pvm.delegate.ActivityExecution
import org.smslib.AGateway
import org.smslib.IOutboundMessageNotification
import org.smslib.OutboundMessage
import org.smslib.modem.SerialModemGateway

class SmsService {
    public static Long STATUS_ANTRI = 0
    public static Long STATUS_KIRIM = 1
    public static Long STATUS_TERKIRIM = 2
    public static Long STATUS_GAGAL_TERKIRIM = 3
    def grailsApplication

    org.smslib.Service sms

    boolean initiated = false
    boolean gormReady = false
    boolean locked = false

    def startService() {
        String modemPort = grailsApplication.config.modem.port
        int portSpeed = grailsApplication.config.modem.portSpeed?grailsApplication.config.modem.portSpeed as int:115200
        String msc = grailsApplication.config.modem.msc
        def manufacturer = grailsApplication.config.modem.manufacturer
        def model = grailsApplication.config.modem.model
        SerialModemGateway gateway = new SerialModemGateway("modem.com1", modemPort, portSpeed, manufacturer, model);
//        Modem modem  = new Modem("modem1", modemPort, portSpeed, "0000", "0000", msc);
        gateway.setInbound(false);
        gateway.setOutbound(true);
        gateway.setSimPin("0000");
// Explicit SMSC address set is required for some modems.
// Below is for VODAFONE GREECE - be sure to set your own!
        gateway.setSmscNumber(msc);
        sms = org.smslib.Service.instance
        sms.setOutboundMessageNotification(new IOutboundMessageNotification(){
            public void process(AGateway g, OutboundMessage msg)
            {
                //println("Outbound handler called from Gateway: " + g.getGatewayId());
                //println(msg);
            }
        });
        sms.addGateway(gateway);
        //println("Start service")

        try {
            sms.startService();
            initiated = true;
        }catch(Exception e){
        }

    }

    def stopService(){
        if(initiated)
            sms.stopService()
    }

    def queueSms(def destination, def message){
        def cek = AntrianSms.createCriteria().list {
            ge("waktuAntri",new Date().clearTime())
            lt("waktuAntri",new Date().clearTime()+1)
            eq("noTujuan",destination)
            eq("isiPesan",message)
        }
        if(cek?.size()<1){
            def antrianSms = new AntrianSms(
                    noTujuan: destination,
                    isiPesan: message,
                    waktuAntri: new Date(),
                    status: STATUS_ANTRI
            )
            antrianSms.save(flush: true)
            antrianSms.errors.each{ //println it
                 }
        }
    }

    def sendSms(ActivityExecution execution) throws Exception {
        if(gormReady && !locked) {
            locked = true
            def cas = AntrianSms.createCriteria()
            def results = cas.list(max: 1, offset: 0) {
                isNull("waktuKirim")
                eq("status", STATUS_ANTRI)
                order("waktuAntri", "asc");
            }
            results.each { AntrianSms antrianSms ->
                antrianSms.waktuKirim = new Date()
                antrianSms.status = STATUS_KIRIM
                antrianSms.save(flush: true)
                log.info("antrianSms.errors = $antrianSms.errors")

                performSendSms(antrianSms.noTujuan, antrianSms.isiPesan);
            }
            locked = false
        }

    }

    def performSendSms(def destination, def message){
        if(initiated) {
            OutboundMessage msg = new OutboundMessage(destination, message);
            sms.sendMessage(msg);
            log.info("sms gateway not ready for sending " + message + " to " + destination)
        } else {
            log.info("sms gateway not ready for sending " + message + " to " + destination)
        }
    }
}
