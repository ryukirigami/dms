package com.kombos.hrd

import com.kombos.administrasi.Divisi
import com.kombos.baseapp.AppSettingParam

class TrainingTypeService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = TrainingType.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_tipeTraining") {
                ilike("tipeTraining", "%" + (params."sCriteria_tipeTraining" as String) + "%")
            }

            if(params."sCriteria_singkatan"){
                ilike("singkatan","%" + (params."sCriteria_singkatan" as String) + "%")
            }

            if(params."sCriteria_bagian"){
                bagian{
                    ilike("m012NamaDivisi","%" + (params."sCriteria_bagian" as String) + "%")
                }
            }

            if(params."sCriteria_durasi"){
                ilike("durasi","%" + (params."sCriteria_durasi" as String) + "%")
            }

            if(params."sCriteria_intEks"){
                ilike("intEks","%" + (params."sCriteria_intEks" as String) + "%")
            }

            if(params."sCriteria_sertifikasi"){
                ilike("sertifikasi","%" + (params."sCriteria_sertifikasi" as String) + "%")
            }

            if(params."sCriteria_tempatTraining"){
                ilike("tempatTraining","%" + (params."sCriteria_tempatTraining" as String) + "%")
            }

            if (params."sCriteria_keterangan") {
                ilike("keterangan", "%" + (params."sCriteria_keterangan" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def bag
        results.each {
            bag = Divisi.findByIdAndStaDel(it?.bagian?.id, "0")
            rows << [

                    id: it.id,

                    tipeTraining: it?.tipeTraining,

                    singkatan : it?.singkatan,

                    bagian : bag?.m012NamaDivisi,

                    durasi : it?.durasi,

                    intEks : it?.intEks == "1" ? "Internal TAM" : it?.intEks == "2" ? "Internal CVK" : "EKsternal",

                    sertifikasi : it?.sertifikasi == "0" ? "Ya" : "Tidak",

                    tempatTraining : it?.tempatTraining,

                    keterangan: it.keterangan,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingType", params.id]]
            return result
        }

        result.trainingTypeInstance = TrainingType.get(params.id)

        if (!result.trainingTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingType", params.id]]
            return result
        }

        result.trainingTypeInstance = TrainingType.get(params.id)

        if (!result.trainingTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingType", params.id]]
            return result
        }

        result.trainingTypeInstance = TrainingType.get(params.id)

        if (!result.trainingTypeInstance)
            return fail(code: "default.not.found.message")

        try {
            result.trainingTypeInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        TrainingType.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.trainingTypeInstance && m.field)
                    result.trainingTypeInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["TrainingType", params.id]]
                return result
            }

            result.trainingTypeInstance = TrainingType.get(params.id)

            if (!result.trainingTypeInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.trainingTypeInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.trainingTypeInstance.properties = params

            if (result.trainingTypeInstance.hasErrors() || !result.trainingTypeInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingType", params.id]]
            return result
        }

        result.trainingTypeInstance = new TrainingType()
        result.trainingTypeInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.trainingTypeInstance && m.field)
                result.trainingTypeInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["TrainingType", params.id]]
            return result
        }

        result.trainingTypeInstance = new TrainingType(params)

        if (result.trainingTypeInstance.hasErrors() || !result.trainingTypeInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def trainingType = TrainingType.findById(params.id)
        if (trainingType) {
            trainingType."${params.name}" = params.value
            trainingType.save()
            if (trainingType.hasErrors()) {
                throw new Exception("${trainingType.errors}")
            }
        } else {
            throw new Exception("TrainingType not found")
        }
    }

}