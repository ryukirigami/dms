package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class JabatanService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Jabatan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_jabatan"){
				ilike("jabatan","%" + (params."sCriteria_jabatan" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			if(params."sCriteria_keterangan"){
				ilike("keterangan","%" + (params."sCriteria_keterangan" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						jabatan: it.jabatan,
			
						lastUpdProcess: it.lastUpdProcess,
			
						keterangan: it.keterangan,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Jabatan", params.id] ]
			return result
		}

		result.jabatanInstance = Jabatan.get(params.id)

		if(!result.jabatanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Jabatan", params.id] ]
			return result
		}

		result.jabatanInstance = Jabatan.get(params.id)

		if(!result.jabatanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Jabatan", params.id] ]
			return result
		}

		result.jabatanInstance = Jabatan.get(params.id)

		if(!result.jabatanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.jabatanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		Jabatan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.jabatanInstance && m.field)
					result.jabatanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Jabatan", params.id] ]
				return result
			}

			result.jabatanInstance = Jabatan.get(params.id)

			if(!result.jabatanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.jabatanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.jabatanInstance.properties = params

			if(result.jabatanInstance.hasErrors() || !result.jabatanInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Jabatan", params.id] ]
			return result
		}

		result.jabatanInstance = new Jabatan()
		result.jabatanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.jabatanInstance && m.field)
				result.jabatanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Jabatan", params.id] ]
			return result
		}

		result.jabatanInstance = new Jabatan(params)

		if(result.jabatanInstance.hasErrors() || !result.jabatanInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def jabatan =  Jabatan.findById(params.id)
		if (jabatan) {
			jabatan."${params.name}" = params.value
			jabatan.save()
			if (jabatan.hasErrors()) {
				throw new Exception("${jabatan.errors}")
			}
		}else{
			throw new Exception("Jabatan not found")
		}
	}

}