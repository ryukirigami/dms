package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class CertificationKaryawanService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = CertificationKaryawan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_karyawan") {
                eq("karyawan", params."sCriteria_karyawan")
            }

            if(params."dari"){
                if(params.idKaryawan){
                    karyawan {
                        eq("id", params.idKaryawan.toLong())
                    }
                }else{
                    karyawan {
                        eq("id", -10000.toLong())
                    }
                }
            }

            if (params."sCriteria_keterangan") {
                ilike("keterangan", "%" + (params."sCriteria_keterangan" as String) + "%")
            }

            if (params."sCriteria_tglSertifikasi") {
                ge("tglSertifikasi", params."sCriteria_tglSertifikasi")
                lt("tglSertifikasi", params."sCriteria_tglSertifikasi" + 1)
            }

            if (params."sCriteria_tipeSertifikasi") {
                eq("tipeSertifikasi", params."sCriteria_tipeSertifikasi")
            }

            if (params."sCriteria_training") {
                eq("training", params."sCriteria_training")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    lastUpdProcess: it.lastUpdProcess,

                    karyawan: it.karyawan,

                    keterangan: it.keterangan,

                    tglSertifikasi: it.tglSertifikasi ? it.tglSertifikasi.format(dateFormat) : "",

                    tipeSertifikasi: it.tipeSertifikasi.namaSertifikasi,

                    training: it.training.namaTraining,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationKaryawan", params.id]]
            return result
        }

        result.certificationKaryawanInstance = CertificationKaryawan.get(params.id)

        if (!result.certificationKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationKaryawan", params.id]]
            return result
        }

        result.certificationKaryawanInstance = CertificationKaryawan.get(params.id)

        if (!result.certificationKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationKaryawan", params.id]]
            return result
        }

        result.certificationKaryawanInstance = CertificationKaryawan.get(params.id)

        if (!result.certificationKaryawanInstance)
            return fail(code: "default.not.found.message")

        try {
            result.certificationKaryawanInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        CertificationKaryawan.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.certificationKaryawanInstance && m.field)
                    result.certificationKaryawanInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["CertificationKaryawan", params.id]]
                return result
            }

            result.certificationKaryawanInstance = CertificationKaryawan.get(params.id)

            if (!result.certificationKaryawanInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.certificationKaryawanInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.certificationKaryawanInstance.properties = params

            if (result.certificationKaryawanInstance.hasErrors() || !result.certificationKaryawanInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationKaryawan", params.id]]
            return result
        }

        result.certificationKaryawanInstance = new CertificationKaryawan()
        result.certificationKaryawanInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.certificationKaryawanInstance && m.field)
                result.certificationKaryawanInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["CertificationKaryawan", params.id]]
            return result
        }

        if (params.karyawanId) {
            params.karyawan = Karyawan.findById(new Long(params.karyawanId))
        }


        result.certificationKaryawanInstance = new CertificationKaryawan(params)

        if (result.certificationKaryawanInstance.hasErrors() || !result.certificationKaryawanInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def certificationKaryawan = CertificationKaryawan.findById(params.id)
        if (certificationKaryawan) {
            certificationKaryawan."${params.name}" = params.value
            certificationKaryawan.save()
            if (certificationKaryawan.hasErrors()) {
                throw new Exception("${certificationKaryawan.errors}")
            }
        } else {
            throw new Exception("CertificationKaryawan not found")
        }
    }

}