package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class RewardKaryawanService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = RewardKaryawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_rewardKaryawan"){
				ilike("rewardKaryawan","%" + (params."sCriteria_rewardKaryawan" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			if(params."sCriteria_historyRewardKaryawan"){
				eq("historyRewardKaryawan",params."sCriteria_historyRewardKaryawan")
			}

			if(params."sCriteria_keterangan"){
				ilike("keterangan","%" + (params."sCriteria_keterangan" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						rewardKaryawan: it.rewardKaryawan,
			
						lastUpdProcess: it.lastUpdProcess,
			
						historyRewardKaryawan: it.historyRewardKaryawan,
			
						keterangan: it.keterangan,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["RewardKaryawan", params.id] ]
			return result
		}

		result.rewardKaryawanInstance = RewardKaryawan.get(params.id)

		if(!result.rewardKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["RewardKaryawan", params.id] ]
			return result
		}

		result.rewardKaryawanInstance = RewardKaryawan.get(params.id)

		if(!result.rewardKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["RewardKaryawan", params.id] ]
			return result
		}

		result.rewardKaryawanInstance = RewardKaryawan.get(params.id)

		if(!result.rewardKaryawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.rewardKaryawanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		RewardKaryawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.rewardKaryawanInstance && m.field)
					result.rewardKaryawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["RewardKaryawan", params.id] ]
				return result
			}

			result.rewardKaryawanInstance = RewardKaryawan.get(params.id)

			if(!result.rewardKaryawanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.rewardKaryawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.rewardKaryawanInstance.properties = params

			if(result.rewardKaryawanInstance.hasErrors() || !result.rewardKaryawanInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["RewardKaryawan", params.id] ]
			return result
		}

		result.rewardKaryawanInstance = new RewardKaryawan()
		result.rewardKaryawanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.rewardKaryawanInstance && m.field)
				result.rewardKaryawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["RewardKaryawan", params.id] ]
			return result
		}

		result.rewardKaryawanInstance = new RewardKaryawan(params)

		if(result.rewardKaryawanInstance.hasErrors() || !result.rewardKaryawanInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def rewardKaryawan =  RewardKaryawan.findById(params.id)
		if (rewardKaryawan) {
			rewardKaryawan."${params.name}" = params.value
			rewardKaryawan.save()
			if (rewardKaryawan.hasErrors()) {
				throw new Exception("${rewardKaryawan.errors}")
			}
		}else{
			throw new Exception("RewardKaryawan not found")
		}
	}

}