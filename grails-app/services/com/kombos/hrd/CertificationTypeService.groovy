package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class CertificationTypeService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = CertificationType.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_namaSertifikasi") {
                ilike("namaSertifikasi", "%" + (params."sCriteria_namaSertifikasi" as String) + "%")
            }

            if (params."sCriteria_tipeSertifikasi") {
                ilike("tipeSertifikasi", "%" + (params."sCriteria_tipeSertifikasi" as String) + "%")
            }

            if (params."sCriteria_keterangan") {
                ilike("keterangan", "%" + (params."sCriteria_keterangan" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    namaSertifikasi: it.namaSertifikasi,

                    tipeSertifikasi: it.tipeSertifikasi,

                    keterangan: it.keterangan

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationType", params.id]]
            return result
        }

        result.certificationTypeInstance = CertificationType.get(params.id)

        if (!result.certificationTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationType", params.id]]
            return result
        }

        result.certificationTypeInstance = CertificationType.get(params.id)

        if (!result.certificationTypeInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationType", params.id]]
            return result
        }

        result.certificationTypeInstance = CertificationType.get(params.id)

        if (!result.certificationTypeInstance)
            return fail(code: "default.not.found.message")

        try {
            result.certificationTypeInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        CertificationType.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.certificationTypeInstance && m.field)
                    result.certificationTypeInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["CertificationType", params.id]]
                return result
            }

            result.certificationTypeInstance = CertificationType.get(params.id)

            if (!result.certificationTypeInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.certificationTypeInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.certificationTypeInstance.properties = params

            if (result.certificationTypeInstance.hasErrors() || !result.certificationTypeInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["CertificationType", params.id]]
            return result
        }

        result.certificationTypeInstance = new CertificationType()
        result.certificationTypeInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.certificationTypeInstance && m.field)
                result.certificationTypeInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["CertificationType", params.id]]
            return result
        }

        result.certificationTypeInstance = new CertificationType(params)

        if (result.certificationTypeInstance.hasErrors() || !result.certificationTypeInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def certificationType = CertificationType.findById(params.id)
        if (certificationType) {
            certificationType."${params.name}" = params.value
            certificationType.save()
            if (certificationType.hasErrors()) {
                throw new Exception("${certificationType.errors}")
            }
        } else {
            throw new Exception("CertificationType not found")
        }
    }

}