package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import grails.converters.JSON

class AbsensiKaryawanService {
    boolean transactional = false

    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = AbsensiKaryawan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                or{
                    eq("companyDealer",params?.companyDealer)
                    karyawan {
                        eq("branch",params?.companyDealer)
                    }
                }
            }
            if(params.sCriteria_dealer){
                karyawan {
                    branch{
                        eq("id", params.sCriteria_dealer as Long)
                    }
                }
            }

            if (params."sCriteria_bulan" || params.sCriteria_tahun) {
                eq("bulanTahun", (params."sCriteria_bulan" + "" + params."sCriteria_tahun").toString())
            }
            if (params."namaKaryawan") {
                eq("karyawan", Karyawan.findByNama(params.namaKaryawan))
            }

            karyawan{
                order("nama","asc")
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it?.id,

                    karyawan: it?.karyawan?.nama ? it?.karyawan?.nama:"-",

                    cabang: it?.karyawan?.branch?.m011NamaWorkshop? it?.karyawan?.branch?.m011NamaWorkshop:"-",

                    nomor: it?.karyawan?.nomorPokokKaryawan ? it?.karyawan?.nomorPokokKaryawan:"-",

                    periode: it?.bulanTahun ? it?.bulanTahun:"-",

                    absent: it?.absent ? it?.absent:"-",

                    attend: it?.attend ? it?.attend:"-",

                    leave: it?.leave ? it?.leave:"-",

                    permit : it?.permit ? it?.permit:"-",

                    bulanTahun: it?.bulanTahun ? it?.bulanTahun:"-",

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def datatablesListDetail(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = AbsensiKaryawanDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params.bulanTahun) {
                absensiKaryawan{
                    eq("bulanTahun", params.bulanTahun.toString())
                }
            }

            if (params."namaKaryawan") {
                absensiKaryawan{
                    eq("karyawan", Karyawan.findByNama(params.namaKaryawan))
                }
            }
            eq("staDel","0")
            order("tanggal","asc")
        }

        def rows = []

        results.each {
            def status = ""
            if(it.status=="1")
                status = "Masuk"
            else if(it.status=="2")
                status = "Pulang"
            else if(it.status=="3")
                status = "Istirahat"
            else if(it.status=="4")
                status = "Masuk Istirahat"
            else if(it.status=="5")
                status = "Ijin"
            else if(it.status=="6")
                status = "Alpha"
            else if(it.status=="7")
                status = "Sakit"
            rows << [

                    id: it?.id,

                    masuk: it?.tanggal.format("dd/MM/yyyy HH:mm"),

                    status: status ? status:"-",

                    ket: it.keterangan,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AbsensiKaryawan", params.id]]
            return result
        }

        result.absensiKaryawanInstance = AbsensiKaryawan.get(params.id)

        if (!result.absensiKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AbsensiKaryawan", params.id]]
            return result
        }

        result.absensiKaryawanInstance = AbsensiKaryawan.get(params.id)

        if (!result.absensiKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AbsensiKaryawan", params.id]]
            return result
        }

        result.absensiKaryawanInstance = AbsensiKaryawan.get(params.id)

        if (!result.absensiKaryawanInstance)
            return fail(code: "default.not.found.message")

        try {
            result.absensiKaryawanInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        AbsensiKaryawan.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.absensiKaryawanInstance && m.field)
                    result.absensiKaryawanInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["AbsensiKaryawan", params.id]]
                return result
            }

            result.absensiKaryawanInstance = AbsensiKaryawan.get(params.id)

            if (!result.absensiKaryawanInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.absensiKaryawanInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.absensiKaryawanInstance.properties = params

            if (result.absensiKaryawanInstance.hasErrors() || !result.absensiKaryawanInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["AbsensiKaryawan", params.id]]
            return result
        }

        result.absensiKaryawanInstance = new AbsensiKaryawan()
        result.absensiKaryawanInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.absensiKaryawanInstance && m.field)
                result.absensiKaryawanInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["AbsensiKaryawan", params.id]]
            return result
        }
        params.karyawan = Karyawan.findById(params.karyawan.id)
        params.bulanTahun = params.bulan + "" + params.tahun
        params.attend = 0
        params.absent = 0
        params.permit = 0
        params.leave = 0
        params.overtime = 0
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess  = "INSERT"
        try {
            params.dateCreated = datatablesUtilService?.syncTime()
        }catch (Exception e){

        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        Date date = Date.parse("dd/MM/yyyy HH:mm",params.tanggal_dp + " " + params.jam + ":" + params.menit)
        Date dateSrc = Date.parse("dd/MM/yyyy",params.tanggal_dp)
        def data = AbsensiKaryawan.findByKaryawanAndBulanTahun(params.karyawan,params.bulanTahun)
        if(AbsensiKaryawan.findByKaryawanAndBulanTahun(params.karyawan,params.bulanTahun)){
            result.absensiKaryawanInstance = AbsensiKaryawan.get(data.id)
            def abs = AbsensiKaryawanDetail.findByStatusAndAbsensiKaryawanAndTanggal('1',data,date)
            if(params.status=="1"){
                if(!abs){
                    result.absensiKaryawanInstance.attend = (AbsensiKaryawan.get(data.id).attend+1)
                }
            }else if(params.status=="5"){
                result.absensiKaryawanInstance.permit = (AbsensiKaryawan.get(data.id).permit+1)
            }else if(params.status=="6"){
                result.absensiKaryawanInstance.absent = (AbsensiKaryawan.get(data.id).absent+1)
            }else if(params.status=="7"){
                result.absensiKaryawanInstance.leave = (AbsensiKaryawan.get(data.id).leave+1)
            }

            result.absensiKaryawanInstance.save(flush: true)

        }else{
            if(params.status=="1"){
                params.attend = 1
            }else if(params.status=="5"){
                params.permit = 1
            }else if(params.status=="6"){
                params.absent = 1
            }else if(params.status=="7"){
                params.leave = 1
            }

            result.absensiKaryawanInstance = new AbsensiKaryawan(params)
            result.absensiKaryawanInstance.dateCreated = datatablesUtilService?.syncTime()
            result.absensiKaryawanInstance.save(flush: true)
            result.absensiKaryawanInstance.errors.each{
                //println it
            }
        }
        // success
            def akd = new AbsensiKaryawanDetail()
            akd?.absensiKaryawan = result.absensiKaryawanInstance
            akd?.tanggal = date
//            akd?.tanggal = datatablesUtilService?.syncTime()
            akd?.status = params.status
            akd?.terminal = Terminal.findById(params.terminal.id)
            akd?.staDel = "0"
            akd?.keterangan = params.keterangan
            akd?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            akd?.lastUpdProcess = "INSERT"
            akd?.dateCreated = datatablesUtilService?.syncTime()
            akd?.lastUpdated = datatablesUtilService?.syncTime()
            akd?.save(flush: true)
            akd.errors.each {
                //println it
            }
        return result
    }



    def updateField(def params) {
        def absensiKaryawan = AbsensiKaryawan.findById(params.id)
        if (absensiKaryawan) {
            absensiKaryawan."${params.name}" = params.value
            absensiKaryawan.save()
            if (absensiKaryawan.hasErrors()) {
                throw new Exception("${absensiKaryawan.errors}")
            }
        } else {
            throw new Exception("AbsensiKaryawan not found")
        }
    }

    def massDelete(params){
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def data = AbsensiKaryawan.findById(it as Long)
            AbsensiKaryawanDetail.findAllByAbsensiKaryawan(data).each {
                it.delete()
            }
            data.delete()
        }
    }

}