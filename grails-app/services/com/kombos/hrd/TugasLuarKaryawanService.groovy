package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class TugasLuarKaryawanService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = TugasLuarKaryawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                or{
                    eq("companyDealer",params?.companyDealer)
                    karyawan {
                        eq("branch",params?.companyDealer)
                    }
                }
            }
            if(params.sCriteria_dealer){
                karyawan {
                    branch{
                        eq("id", params.sCriteria_dealer as Long)
                    }
                }
            }
			if(params."sCriteria_karyawan"){
                karyawan {
                    ilike("nama", "%" + (params."sCriteria_karyawan" as String) + "%")
                }
			}

			if(params."sCriteria_assignmentObjectives"){
				ilike("assignmentObjectives","%" + (params."sCriteria_assignmentObjectives" as String) + "%")
			}

			if(params."sCriteria_dateBegin"){
				ge("dateBegin",params."sCriteria_dateBegin")
			}

			if(params."sCriteria_dateFinish"){
				lt("dateFinish",params."sCriteria_dateFinish")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,

						karyawan: it.karyawan.nama,

						cabang: it.karyawan.branch?.m011NamaWorkshop,

                        dateBegin: it.dateBegin?it.dateBegin.format(dateFormat):"",

						assignmentObjectives: it.assignmentObjectives,
			
						assignmentReason: it.assignmentReason,

						explanation: it.explanation
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TugasLuarKaryawan", params.id] ]
			return result
		}

		result.tugasLuarKaryawanInstance = TugasLuarKaryawan.get(params.id)

		if(!result.tugasLuarKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TugasLuarKaryawan", params.id] ]
			return result
		}

		result.tugasLuarKaryawanInstance = TugasLuarKaryawan.get(params.id)

		if(!result.tugasLuarKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TugasLuarKaryawan", params.id] ]
			return result
		}

		result.tugasLuarKaryawanInstance = TugasLuarKaryawan.get(params.id)

		if(!result.tugasLuarKaryawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.tugasLuarKaryawanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		TugasLuarKaryawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.tugasLuarKaryawanInstance && m.field)
					result.tugasLuarKaryawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["TugasLuarKaryawan", params.id] ]
				return result
			}

			result.tugasLuarKaryawanInstance = TugasLuarKaryawan.get(params.id)

			if(!result.tugasLuarKaryawanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.tugasLuarKaryawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.tugasLuarKaryawanInstance.properties = params

			if(result.tugasLuarKaryawanInstance.hasErrors() || !result.tugasLuarKaryawanInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TugasLuarKaryawan", params.id] ]
			return result
		}

		result.tugasLuarKaryawanInstance = new TugasLuarKaryawan()
		result.tugasLuarKaryawanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.tugasLuarKaryawanInstance && m.field)
				result.tugasLuarKaryawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["TugasLuarKaryawan", params.id] ]
			return result
		}

		result.tugasLuarKaryawanInstance = new TugasLuarKaryawan(params)

		if(result.tugasLuarKaryawanInstance.hasErrors() || !result.tugasLuarKaryawanInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def tugasLuarKaryawan =  TugasLuarKaryawan.findById(params.id)
		if (tugasLuarKaryawan) {
			tugasLuarKaryawan."${params.name}" = params.value
			tugasLuarKaryawan.save()
			if (tugasLuarKaryawan.hasErrors()) {
				throw new Exception("${tugasLuarKaryawan.errors}")
			}
		}else{
			throw new Exception("TugasLuarKaryawan not found")
		}
	}

}