package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class StatusKaryawanService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = StatusKaryawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_statusKaryawan"){
				ilike("statusKaryawan","%" + (params."sCriteria_statusKaryawan" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			if(params."sCriteria_keterangan"){
				ilike("keterangan","%" + (params."sCriteria_keterangan" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						statusKaryawan: it.statusKaryawan,
			
						lastUpdProcess: it.lastUpdProcess,
			
						keterangan: it.keterangan,
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["StatusKaryawan", params.id] ]
			return result
		}

		result.statusKaryawanInstance = StatusKaryawan.get(params.id)

		if(!result.statusKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["StatusKaryawan", params.id] ]
			return result
		}

		result.statusKaryawanInstance = StatusKaryawan.get(params.id)

		if(!result.statusKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["StatusKaryawan", params.id] ]
			return result
		}

		result.statusKaryawanInstance = StatusKaryawan.get(params.id)

		if(!result.statusKaryawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.statusKaryawanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		StatusKaryawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.statusKaryawanInstance && m.field)
					result.statusKaryawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["StatusKaryawan", params.id] ]
				return result
			}

			result.statusKaryawanInstance = StatusKaryawan.get(params.id)

			if(!result.statusKaryawanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.statusKaryawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.statusKaryawanInstance.properties = params

			if(result.statusKaryawanInstance.hasErrors() || !result.statusKaryawanInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["StatusKaryawan", params.id] ]
			return result
		}

		result.statusKaryawanInstance = new StatusKaryawan()
		result.statusKaryawanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.statusKaryawanInstance && m.field)
				result.statusKaryawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["StatusKaryawan", params.id] ]
			return result
		}

		result.statusKaryawanInstance = new StatusKaryawan(params)

		if(result.statusKaryawanInstance.hasErrors() || !result.statusKaryawanInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
		return result
	}
	
	def updateField(def params){
		def statusKaryawan =  StatusKaryawan.findById(params.id)
		if (statusKaryawan) {
			statusKaryawan."${params.name}" = params.value
			statusKaryawan.save()
			if (statusKaryawan.hasErrors()) {
				throw new Exception("${statusKaryawan.errors}")
			}
		}else{
			throw new Exception("StatusKaryawan not found")
		}
	}

}