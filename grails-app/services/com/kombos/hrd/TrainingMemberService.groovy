package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class TrainingMemberService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = TrainingMember.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_karyawan") {
                eq("karyawan", params."sCriteria_karyawan")
            }

            if (params."sCriteria_training") {
                eq("training", params."sCriteria_training")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_point") {
                eq("point", params."sCriteria_point")
            }

            if (params."sCriteria_pointInstruktur") {
                eq("pointInstruktur", params."sCriteria_pointInstruktur")
            }

            if (params."sCriteria_staGraduate") {
                ilike("staGraduate", "%" + (params."sCriteria_staGraduate" as String) + "%")
            }

            if (params."sCriteria_tglGraduate") {
                ge("tglGraduate", params."sCriteria_tglGraduate")
                lt("tglGraduate", params."sCriteria_tglGraduate" + 1)
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    karyawan: it.karyawan,

                    training: it.training,

                    lastUpdProcess: it.lastUpdProcess,

                    point: it.point,

                    pointIntruktur: it.pointInstruktur,

                    staGraduate: it.staGraduate,

                    tglGraduate: it.tglGraduate ? it.tglGraduate.format(dateFormat) : "",

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingMember", params.id]]
            return result
        }

        result.trainingMemberInstance = TrainingMember.get(params.id)

        if (!result.trainingMemberInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingMember", params.id]]
            return result
        }

        result.trainingMemberInstance = TrainingMember.get(params.id)

        if (!result.trainingMemberInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingMember", params.id]]
            return result
        }

        result.trainingMemberInstance = TrainingMember.get(params.id)

        if (!result.trainingMemberInstance)
            return fail(code: "default.not.found.message")

        try {
            result.trainingMemberInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        TrainingMember.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.trainingMemberInstance && m.field)
                    result.trainingMemberInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["TrainingMember", params.id]]
                return result
            }

            result.trainingMemberInstance = TrainingMember.get(params.id)

            if (!result.trainingMemberInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.trainingMemberInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.trainingMemberInstance.properties = params

            if (result.trainingMemberInstance.hasErrors() || !result.trainingMemberInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingMember", params.id]]
            return result
        }

        result.trainingMemberInstance = new TrainingMember()
        result.trainingMemberInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.trainingMemberInstance && m.field)
                result.trainingMemberInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["TrainingMember", params.id]]
            return result
        }

        result.trainingMemberInstance = new TrainingMember(params)

        if (result.trainingMemberInstance.hasErrors() || !result.trainingMemberInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def trainingMember = TrainingMember.findById(params.id)
        if (trainingMember) {
            trainingMember."${params.name}" = params.value
            trainingMember.save()
            if (trainingMember.hasErrors()) {
                throw new Exception("${trainingMember.errors}")
            }
        } else {
            throw new Exception("TrainingMember not found")
        }
    }

}