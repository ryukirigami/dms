package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam

class CutiKaryawanService {
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = CutiKaryawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                or{
                    eq("companyDealer",params?.companyDealer)
                    karyawan {
                        eq("branch",params?.companyDealer)
                    }
                }
            }

			if(params."sCriteria_karyawan"){
                karyawan {
                    ilike("nama","%" + params."sCriteria_karyawan" + "%")
                }
			}

            if(params.sCriteria_dealer){
                karyawan {
                    branch{
                        eq("id", params.sCriteria_dealer as Long)
                    }
                }
            }

            if(params.filter_tipeCuti){
                cutiKaryawan{
                    eq("id", params.filter_tipeCuti as Long)
                }
            }

			if(params."sCriteria_jumlahHariCuti"){
				eq("jumlahHariCuti",params."sCriteria_jumlahHariCuti")
			}

			if(params."sCriteria_tanggalAkhirCuti"){
				lt("tanggalAkhirCuti",params."sCriteria_tanggalAkhirCuti" + 1)
			}

			if(params."sCriteria_tanggalMulaiCuti"){
				ge("tanggalMulaiCuti",params."sCriteria_tanggalMulaiCuti")
			}

//            eq("companyDealer",params?.companyDealer)
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						karyawan: it.karyawan.nama,

                        tanggalMulaiCuti: it.tanggalMulaiCuti?it.tanggalMulaiCuti.format(dateFormat):"",

                        tanggalAkhirCuti:  it.tanggalAkhirCuti?it.tanggalAkhirCuti.format(dateFormat):"",

                        cutiKaryawan: it.cutiKaryawan.cuti,
			
						jumlahHariCuti: it.jumlahHariCuti,
			
						keterangan: it.keterangan,

                        cabang : it?.karyawan?.branch?.m011NamaWorkshop ? it?.karyawan?.branch?.m011NamaWorkshop:"-"
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["CutiKaryawan", params.id] ]
			return result
		}

		result.cutiKaryawanInstance = CutiKaryawan.get(params.id)

		if(!result.cutiKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["CutiKaryawan", params.id] ]
			return result
		}

		result.cutiKaryawanInstance = CutiKaryawan.get(params.id)

		if(!result.cutiKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["CutiKaryawan", params.id] ]
			return result
		}
        CutiKaryawan ck = CutiKaryawan.get(params.id)
		result.cutiKaryawanInstance = ck

		if(!result.cutiKaryawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.cutiKaryawanInstance.delete(flush:true)
            Karyawan k = ck.karyawan

            SisaCutiKaryawan sisa = SisaCutiKaryawan.findByKaryawanAndTahunCuti(k, ck.tahunCuti)
            sisa.delete(flush: true)

			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		CutiKaryawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.cutiKaryawanInstance && m.field)
					result.cutiKaryawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["CutiKaryawan", params.id] ]
				return result
			}

            CutiKaryawan c = CutiKaryawan.get(params.id)
			result.cutiKaryawanInstance = c

			if(!result.cutiKaryawanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.cutiKaryawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.cutiKaryawanInstance.properties = params

			if(result.cutiKaryawanInstance.hasErrors() || !result.cutiKaryawanInstance.save())
				return fail(code:"default.not.updated.message")
            else {
                def cList = CutiKaryawan.findAllByTahunCutiAndKaryawan(params.tahunCuti as int, c.karyawan)
                int totalHari = 0
                if (cList.size() > 0) {
                    cList.each {
                        totalHari+= it.jumlahHariCuti
                    }
                    SisaCutiKaryawan sisa = SisaCutiKaryawan.findByKaryawanAndTahunCuti(c.karyawan, params.tahunCuti as int)
                    /*  if (sisa) {*/
                    sisa.sisaHariCuti = 12 - totalHari
                    sisa.save(flush: true)
                    /*  }*/
                }
            }


            // Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["CutiKaryawan", params.id] ]
			return result
		}

		result.cutiKaryawanInstance = new CutiKaryawan()
		result.cutiKaryawanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.cutiKaryawanInstance && m.field)
				result.cutiKaryawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["CutiKaryawan", params.id] ]
			return result
		}

		result.cutiKaryawanInstance = new CutiKaryawan(params)

		if(result.cutiKaryawanInstance.hasErrors() || !result.cutiKaryawanInstance.save(flush: true))
			return fail(code:"default.not.created.message")
        else {
            CutiKaryawan c = result.cutiKaryawanInstance
            Karyawan k = c.karyawan

            SisaCutiKaryawan sisa = SisaCutiKaryawan.findByKaryawanAndTahunCuti(k, params.tahunCuti as int)
            /*def sisa = SisaCutiKaryawan.createCriteria().list {
                karyawan {
                    eq ("id", c.karyawan.id)
                }
                eq("tahunCuti", Integer.parseInt(params.tahunCuti))
            }*/
            /*def lis = SisaCutiKaryawan.list()
            lis.each {

            }*/

            if (!sisa) {
                int jumlahHariCuti = Integer.parseInt(params.jumlahHariCuti)
                sisa = new SisaCutiKaryawan()
                sisa.karyawan = k
                sisa.tahunCuti = params.tahunCuti as int
                sisa.sisaHariCuti = 12 - jumlahHariCuti
                sisa.dateCreated = datatablesUtilService?.syncTime()
                sisa.lastUpdated = datatablesUtilService?.syncTime()
                sisa.save(flush: true, failOnError: true)
            } else {
                int jumlahHariCuti = Integer.parseInt(params.jumlahHariCuti)
                sisa.sisaHariCuti = sisa.sisaHariCuti - jumlahHariCuti
                sisa.save(flush: true, failOnError: true)
            }

        }

		// success
		return result
	}
	
	def updateField(def params){
		def cutiKaryawan =  CutiKaryawan.findById(params.id)
		if (cutiKaryawan) {
			cutiKaryawan."${params.name}" = params.value
			cutiKaryawan.save()
			if (cutiKaryawan.hasErrors()) {
				throw new Exception("${cutiKaryawan.errors}")
			}
		}else{
			throw new Exception("CutiKaryawan not found")
		}
	}

}
