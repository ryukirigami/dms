package com.kombos.hrd

import com.kombos.administrasi.KegiatanApproval
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import com.kombos.parts.StatusApproval
import org.springframework.web.context.request.RequestContextHolder

class HistoryKaryawanService implements AfterApprovalInterface {
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def datatablesUtilService

    def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def c = HistoryKaryawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("cabangAfter",params?.companyDealer)
            }else{
                if(params."sCriteria_cabangAfter"){
                    cabangAfter{
                        eq("id",params."sCriteria_cabangAfter" as Long)
                    }
                }
            }
			if(params."sCriteria_karyawan"){
                karyawan {
                    ilike("nama", "%" + params."sCriteria_karyawan" + "%")
                }
			}

			if(params."dari"){
                if(params.idKaryawan){
                    karyawan {
                        eq("id", params.idKaryawan.toLong())
                    }
                }else{
                    karyawan {
                        eq("id", -10000.toLong())
                    }
                }
			}

			if(params."sCriteria_cabangBefore"){
				eq("cabangBefore",params."sCriteria_cabangBefore")
			}

			if(params."sCriteria_explanation"){
				ilike("explanation","%" + (params."sCriteria_explanation" as String) + "%")
			}

			if(params."sCriteria_history"){
                history {
                    ilike("history", "%"+params."sCriteria_history" + "%")
                }
			}

			if(params."sCriteria_letterNumber"){
				ilike("letterNumber","%" + (params."sCriteria_letterNumber" as String) + "%")
			}

			if(params."sCriteria_reason"){
				ilike("reason","%" + (params."sCriteria_reason" as String) + "%")
			}

			if(params."sCriteria_tanggal"){
				ge("tanggal",params."sCriteria_tanggal")
				lt("tanggal",params."sCriteria_tanggal" + 1)
			}

//            eq("companyDealer",params?.companyDealer)
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						karyawan: it.karyawan?.nama,

						cabangAfter: it?.cabangAfter?.m011NamaWorkshop,
			
						cabangBefore: it?.cabangBefore?.m011NamaWorkshop,
			
						explanation: it?.explanation,
			
						history: it?.history?.history,
			
						letterNumber: it?.letterNumber,
			
						reason: it?.reason,

                        approval : it?.staApproval==StatusApproval.WAIT_FOR_APPROVAL?"Belum":(it.staApproval==StatusApproval.APPROVED?"Approved":"Un Approved"),
			
						tanggal: it?.tanggal?it.tanggal.format(dateFormat):"",
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryKaryawan", params.id] ]
			return result
		}

		result.historyKaryawanInstance = HistoryKaryawan.get(params.id)

		if(!result.historyKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryKaryawan", params.id] ]
			return result
		}

		result.historyKaryawanInstance = HistoryKaryawan.get(params.id)

		if(!result.historyKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryKaryawan", params.id] ]
			return result
		}

		result.historyKaryawanInstance = HistoryKaryawan.get(params.id)

		if(!result.historyKaryawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.historyKaryawanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		HistoryKaryawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.historyKaryawanInstance && m.field)
					result.historyKaryawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["HistoryKaryawan", params.id] ]
				return result
			}

			result.historyKaryawanInstance = HistoryKaryawan.get(params.id)

			if(!result.historyKaryawanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.historyKaryawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.historyKaryawanInstance.properties = params

			if(result.historyKaryawanInstance.hasErrors() || !result.historyKaryawanInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryKaryawan", params.id] ]
			return result
		}

		result.historyKaryawanInstance = new HistoryKaryawan()
		result.historyKaryawanInstance.properties = params

		// success
		return result
	}


	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.historyKaryawanInstance && m.field)
				result.historyKaryawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["HistoryKaryawan", params.id] ]
			return result
		}

        if (params.karyawanId) {
            params.karyawan = Karyawan.findById(new Long(params.karyawanId))
        }

        params.companyDealer = params.companyDealer
        if(params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
            params.staApproval = StatusApproval.APPROVED
        }
		result.historyKaryawanInstance = new HistoryKaryawan(params)

		if(result.historyKaryawanInstance.hasErrors() || !result.historyKaryawanInstance.save(flush: true))
			return fail(code:"default.not.created.message")

        if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
            def pesan = "karyawan bernama " + Karyawan.findById(new Long(params.karyawanId)).nama
            pesan += ", status " + result?.historyKaryawanInstance?.history?.history
            def approval = new ApprovalT770(
                    t770FK:result.historyKaryawanInstance.id as String,
                    kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.HISTORY_KARYAWAN),
                    t770NoDokumen: params.letterNumber,
                    t770TglJamSend: datatablesUtilService?.syncTime(),
                    t770Pesan: pesan,
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime(),
                    companyDealer: params?.companyDealer,
            )
            approval.save(flush:true)
            approval.errors.each{ //println it
			}
        }
				// success
		return result
	}
	
	def updateField(def params){
		def historyKaryawan =  HistoryKaryawan.findById(params.id)
		if (historyKaryawan) {
			historyKaryawan."${params.name}" = params.value
			historyKaryawan.save()
			if (historyKaryawan.hasErrors()) {
				throw new Exception("${historyKaryawan.errors}")
			}
		}else{
			throw new Exception("HistoryKaryawan not found")
		}
	}
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            def hk= HistoryKaryawan.get(it as Long)
            hk.staApproval = staApproval
            hk.save(flush: true)
        }
    }
}