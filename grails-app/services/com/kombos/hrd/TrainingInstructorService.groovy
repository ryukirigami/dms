package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class TrainingInstructorService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = TrainingInstructor.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_karyawan") {
                karyawan {
                    eq("id", params."sCriteria_karyawan" as Long)
                }
            }

            if (params."sCriteria_namaInstruktur") {
                ilike("namaInstruktur", "%" + (params."sCriteria_namaInstruktur" as String) + "%")
            }



            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    karyawan: it.karyawan?.id,

                    namaInstruktur: it.namaInstruktur,

                   jenisinstruktur : it.jenisInstruktur,

                    jabatan : it.manPower?.m014JabatanManPower,

                    sertifikatterakhir : it.certificationType?.namaSertifikasi,

                    companydealer : it.companyDealer?.m011NamaWorkshop,

                    kelastraining : it.trainingClassRoom?.namaTraining,

                    isAktif : it.th020isAktif=="0"? "Aktif":"Tidak Aktif",

                    isTam : it.th020isTam =="0"? "YA":"Tidak"

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingInstructor", params.id]]
            return result
        }

        result.trainingInstructorInstance = TrainingInstructor.get(params.id)

        if (!result.trainingInstructorInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingInstructor", params.id]]
            return result
        }

        result.trainingInstructorInstance = TrainingInstructor.get(params.id)

        if (!result.trainingInstructorInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingInstructor", params.id]]
            return result
        }

        result.trainingInstructorInstance = TrainingInstructor.get(params.id)

        if (!result.trainingInstructorInstance)
            return fail(code: "default.not.found.message")

        try {
            result.trainingInstructorInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        TrainingInstructor.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.trainingInstructorInstance && m.field)
                    result.trainingInstructorInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["TrainingInstructor", params.id]]
                return result
            }

            result.trainingInstructorInstance = TrainingInstructor.get(params.id)

            if (!result.trainingInstructorInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.trainingInstructorInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.trainingInstructorInstance.properties = params

            if (result.trainingInstructorInstance.hasErrors() || !result.trainingInstructorInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["TrainingInstructor", params.id]]
            return result
        }

        result.trainingInstructorInstance = new TrainingInstructor()
        result.trainingInstructorInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.trainingInstructorInstance && m.field)
                result.trainingInstructorInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["TrainingInstructor", params.id]]
            return result
        }

        result.trainingInstructorInstance = new TrainingInstructor(params)

        if (result.trainingInstructorInstance.hasErrors() || !result.trainingInstructorInstance.save(flush: true))
        {
            result.trainingInstructorInstance.errors.each{
                //println it
            }
            return fail(code: "default.not.created.message")

        }

        // success
        result.trainingInstructorInstance.errors.each{
            //println it
        }
        return result
    }

    def updateField(def params) {
        def trainingInstructor = TrainingInstructor.findById(params.id)
        if (trainingInstructor) {
            trainingInstructor."${params.name}" = params.value
            trainingInstructor.save()
            if (trainingInstructor.hasErrors()) {
                throw new Exception("${trainingInstructor.errors}")
            }
        } else {
            throw new Exception("TrainingInstructor not found")
        }
    }

}