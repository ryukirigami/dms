package com.kombos.hrd

import com.kombos.administrasi.KegiatanApproval
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import com.kombos.parts.StatusApproval
import org.springframework.web.context.request.RequestContextHolder

class HistoryRewardKaryawanService implements AfterApprovalInterface {
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def datatablesUtilService

    def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = HistoryRewardKaryawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                or{
                    eq("companyDealer",params?.companyDealer)
                    karyawan {
                        eq("branch",params?.companyDealer)
                    }
                }
            }
            if(params.sCriteria_companyDealer){
                karyawan {
                    branch{
                        eq("id", params.sCriteria_companyDealer as Long)
                    }
                }
            }
			if(params."sCriteria_tanggalReward"){
				ge("tanggalReward",params."sCriteria_tanggalReward")
				lt("tanggalReward",params."sCriteria_tanggalReward" + 1)
			}

			if(params."sCriteria_karyawan"){
                karyawan {
                    ilike("nama", "%" + params."sCriteria_karyawan" + "%")
                }
			}
            if(params."dari"){
                if(params.idKaryawan){
                    karyawan {
                        eq("id", params.idKaryawan.toLong())
                    }
                }else{
                    karyawan {
                        eq("id", -10000.toLong())
                    }
                }
            }


            if(params."sCriteria_keterangan"){
				ilike("keterangan","%" + (params."sCriteria_keterangan" as String) + "%")
			}

			if(params."sCriteria_rewardKaryawan"){
                rewardKaryawan {
                    ilike("rewardKaryawan", "%" + params."sCriteria_rewardKaryawan" + "%")
                }

			}

			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it?.id,
			
						tanggalReward: it?.tanggalReward ? it?.tanggalReward?.format(dateFormat):"-",
			
						karyawan: it?.karyawan?.nama ? it?.karyawan?.nama : "-",

						workshop: it?.karyawan?.branch?.m011NamaWorkshop ? it?.karyawan?.branch?.m011NamaWorkshop   : "-",

						keterangan: it?.keterangan ? it?.keterangan : "-",
			
						rewardKaryawan: it?.rewardKaryawan?.rewardKaryawan ? it?.rewardKaryawan?.rewardKaryawan : "-",

                        Approval : it?.staApproval==StatusApproval.WAIT_FOR_APPROVAL?"Belum":(it?.staApproval==StatusApproval.APPROVED?"Approved":"Un Approved"),
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryRewardKaryawan", params.id] ]
			return result
		}

		result.historyRewardKaryawanInstance = HistoryRewardKaryawan.get(params.id)

		if(!result.historyRewardKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryRewardKaryawan", params.id] ]
			return result
		}

		result.historyRewardKaryawanInstance = HistoryRewardKaryawan.get(params.id)

		if(!result.historyRewardKaryawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryRewardKaryawan", params.id] ]
			return result
		}

		result.historyRewardKaryawanInstance = HistoryRewardKaryawan.get(params.id)

		if(!result.historyRewardKaryawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.historyRewardKaryawanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		HistoryRewardKaryawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.historyRewardKaryawanInstance && m.field)
					result.historyRewardKaryawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["HistoryRewardKaryawan", params.id] ]
				return result
			}

			result.historyRewardKaryawanInstance = HistoryRewardKaryawan.get(params.id)

			if(!result.historyRewardKaryawanInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.historyRewardKaryawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.historyRewardKaryawanInstance.properties = params

			if(result.historyRewardKaryawanInstance.hasErrors() || !result.historyRewardKaryawanInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["HistoryRewardKaryawan", params.id] ]
			return result
		}

		result.historyRewardKaryawanInstance = new HistoryRewardKaryawan()
		result.historyRewardKaryawanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.historyRewardKaryawanInstance && m.field)
				result.historyRewardKaryawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["HistoryRewardKaryawan", params.id] ]
			return result
		}

        if (params.karyawanId) {
            params.karyawan = Karyawan.findById(new Long(params.karyawanId))
        }
        params.companyDealer = params.cd

        result.historyRewardKaryawanInstance = new HistoryRewardKaryawan(params)
        if(params.cd?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
            params.staApproval = StatusApproval.APPROVED
        }

		if(result.historyRewardKaryawanInstance.hasErrors() || !result.historyRewardKaryawanInstance.save(flush: true))
			return fail(code:"default.not.created.message")


        if(!params.cd?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
            def approval = new ApprovalT770(
                    t770FK:result.historyRewardKaryawanInstance.id as String,
                    kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.REWARD_KARYAWAN),
                    t770NoDokumen: "-",
                    t770TglJamSend: datatablesUtilService?.syncTime(),
                    t770Pesan: "Silahkan Di Approve",
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime(),
                    companyDealer: params?.cd
            )
            approval.save(flush:true)
            approval.errors.each{ //println it
			}
        }
		// success
		return result
	}
	
	def updateField(def params){
		def historyRewardKaryawan =  HistoryRewardKaryawan.findById(params.id)
		if (historyRewardKaryawan) {
			historyRewardKaryawan."${params.name}" = params.value
			historyRewardKaryawan.save()
			if (historyRewardKaryawan.hasErrors()) {
				throw new Exception("${historyRewardKaryawan.errors}")
			}
		}else{
			throw new Exception("HistoryRewardKaryawan not found")
		}
	}
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            def reward= HistoryRewardKaryawan.get(it as Long)
            reward.staApproval = staApproval
            reward.save(flush: true)
        }
    }
}