package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam
import org.hibernate.criterion.CriteriaSpecification

class TrainingClassRoomService {	
	boolean transactional = false
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	
	def datatablesList(def params) {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = TrainingClassRoom.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

			if(params."sCriteria_dateBegin"){
				ge("dateBegin",params."sCriteria_dateBegin")
                lt("dateBegin",params."sCriteria_dateFinish" + 1)
			}

			/*if(params."sCriteria_dateFinish"){
				lt("dateBegin",params."sCriteria_dateFinish" + 1)
			}*/

			if(params."sCriteria_namaInstruktur"){
                createAlias("firstInstructor", "first", CriteriaSpecification.LEFT_JOIN)
                createAlias("secondInstructor", "second", CriteriaSpecification.LEFT_JOIN)
                createAlias("thirdInstructor", "third", CriteriaSpecification.LEFT_JOIN)

                or {
                    ilike("first.namaInstruktur", "%" + (params."sCriteria_namaInstruktur" as String) + "%")
                    ilike("second.namaInstruktur", "%" + (params."sCriteria_namaInstruktur" as String) + "%")
                    ilike("third.namaInstruktur", "%" + (params."sCriteria_namaInstruktur" as String) + "%")
                }
            }

			if(params."sCriteria_namaTraining"){
				ilike("namaTraining","%" + (params."sCriteria_namaTraining" as String) + "%")
			}

			if(params."sCriteria_tipeTraining"){
                trainingType {
                    eq("id", params."sCriteria_tipeTraining" as Long)
                }
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,

                        namaTraining: it.namaTraining,

                        dateBegin: it.dateBegin?it.dateBegin.format(dateFormat):"",

                        dateFinish: it.dateFinish?it.dateFinish.format(dateFormat):"",

                        firstInstructor: it.firstInstructor?.namaInstruktur,

                        secondInstructor: it.secondInstructor?.namaInstruktur,

                        thirdInstructor: it.thirdInstructor?.namaInstruktur,
			
						tipeTraining: it.trainingType?.tipeTraining
			
			]
		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TrainingClassRoom", params.id] ]
			return result
		}

		result.trainingClassRoomInstance = TrainingClassRoom.get(params.id)

		if(!result.trainingClassRoomInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TrainingClassRoom", params.id] ]
			return result
		}

		result.trainingClassRoomInstance = TrainingClassRoom.get(params.id)

		if(!result.trainingClassRoomInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TrainingClassRoom", params.id] ]
			return result
		}

		result.trainingClassRoomInstance = TrainingClassRoom.get(params.id)

		if(!result.trainingClassRoomInstance)
			return fail(code:"default.not.found.message")

		try {
			result.trainingClassRoomInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		TrainingClassRoom.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.trainingClassRoomInstance && m.field)
					result.trainingClassRoomInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["TrainingClassRoom", params.id] ]
				return result
			}

			result.trainingClassRoomInstance = TrainingClassRoom.get(params.id)

			if(!result.trainingClassRoomInstance)
				return fail(code:"default.not.found.message")

			// Optimistic locking check.
			if(params.version) {
				if(result.trainingClassRoomInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.trainingClassRoomInstance.properties = params

			if(result.trainingClassRoomInstance.hasErrors() || !result.trainingClassRoomInstance.save())
				return fail(code:"default.not.updated.message")

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["TrainingClassRoom", params.id] ]
			return result
		}

		result.trainingClassRoomInstance = new TrainingClassRoom()
		result.trainingClassRoomInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.trainingClassRoomInstance && m.field)
				result.trainingClassRoomInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["TrainingClassRoom", params.id] ]
			return result
		}

		result.trainingClassRoomInstance = new TrainingClassRoom(params)

		if(result.trainingClassRoomInstance.hasErrors() || !result.trainingClassRoomInstance.save(flush: true))
			return fail(code:"default.not.created.message")

		// success
        return result
	}
	
	def updateField(def params){
		def trainingClassRoom =  TrainingClassRoom.findById(params.id)
		if (trainingClassRoom) {
			trainingClassRoom."${params.name}" = params.value
			trainingClassRoom.save()
			if (trainingClassRoom.hasErrors()) {
				throw new Exception("${trainingClassRoom.errors}")
			}
		}else{
			throw new Exception("TrainingClassRoom not found")
		}
	}

}