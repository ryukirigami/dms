package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class CutiService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Cuti.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_cuti") {
                ilike("cuti", "%" + (params."sCriteria_cuti" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_keterangan") {
                ilike("keterangan", "%" + (params."sCriteria_keterangan" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    cuti: it.cuti,

                    lastUpdProcess: it.lastUpdProcess,

                    keterangan: it.keterangan,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Cuti", params.id]]
            return result
        }

        result.cutiInstance = Cuti.get(params.id)

        if (!result.cutiInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Cuti", params.id]]
            return result
        }

        result.cutiInstance = Cuti.get(params.id)

        if (!result.cutiInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Cuti", params.id]]
            return result
        }

        result.cutiInstance = Cuti.get(params.id)

        if (!result.cutiInstance)
            return fail(code: "default.not.found.message")

        try {
            result.cutiInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        Cuti.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.cutiInstance && m.field)
                    result.cutiInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Cuti", params.id]]
                return result
            }

            result.cutiInstance = Cuti.get(params.id)

            if (!result.cutiInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.cutiInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.cutiInstance.properties = params

            if (result.cutiInstance.hasErrors() || !result.cutiInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Cuti", params.id]]
            return result
        }

        result.cutiInstance = new Cuti()
        result.cutiInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.cutiInstance && m.field)
                result.cutiInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Cuti", params.id]]
            return result
        }

        result.cutiInstance = new Cuti(params)

        if (result.cutiInstance.hasErrors() || !result.cutiInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def cuti = Cuti.findById(params.id)
        if (cuti) {
            cuti."${params.name}" = params.value
            cuti.save()
            if (cuti.hasErrors()) {
                throw new Exception("${cuti.errors}")
            }
        } else {
            throw new Exception("Cuti not found")
        }
    }

}