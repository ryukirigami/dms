package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class KeluargaKaryawanService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = KeluargaKaryawan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_hubungan") {
                eq("hubungan", params."sCriteria_hubungan")
            }

            if (params."sCriteria_nama") {
                ilike("nama", "%" + (params."sCriteria_nama" as String) + "%")
            }

            if (params."sCriteria_karyawan") {
                eq("karyawan", params."sCriteria_karyawan")
            }

            if(params."dari"){
                if(params.idKaryawan){
                    karyawan {
                        eq("id", params.idKaryawan.toLong())
                    }
                }else{
                    karyawan {
                        eq("id", -10000.toLong())
                    }
                }
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_jabatan") {
                ilike("jabatan", "%" + (params."sCriteria_jabatan" as String) + "%")
            }

            if (params."sCriteria_jenisKelamin") {
                ilike("jenisKelamin", "%" + (params."sCriteria_jenisKelamin" as String) + "%")
            }

            if (params."sCriteria_pekerjaan") {
                ilike("pekerjaan", "%" + (params."sCriteria_pekerjaan" as String) + "%")
            }

            if (params."sCriteria_tanggalLahir") {
                ge("tanggalLahirKel", params."sCriteria_tanggalLahir")
                lt("tanggalLahirKel", params."sCriteria_tanggalLahir" + 1)
            }

            if (params."sCriteria_tempatLahir") {
                ilike("tempatLahir", "%" + (params."sCriteria_tempatLahir" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    hubungan: it.hubungan.key,

                    nama: it.nama,

                    karyawan: it.karyawan,

                    lastUpdProcess: it.lastUpdProcess,

                    jabatan: it.jabatan,

                    jenisKelamin: it.jenisKelamin,

                    pekerjaan: it.pekerjaan,

                    tanggalLahirKel: it.tanggalLahirKel ? it.tanggalLahirKel.format(dateFormat) : "",

                    tempatLahir: it.tempatLahir,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KeluargaKaryawan", params.id]]
            return result
        }

        result.keluargaKaryawanInstance = KeluargaKaryawan.get(params.id)

        if (!result.keluargaKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KeluargaKaryawan", params.id]]
            return result
        }

        result.keluargaKaryawanInstance = KeluargaKaryawan.get(params.id)

        if (!result.keluargaKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KeluargaKaryawan", params.id]]
            return result
        }

        result.keluargaKaryawanInstance = KeluargaKaryawan.get(params.id)

        if (!result.keluargaKaryawanInstance)
            return fail(code: "default.not.found.message")

        try {
            result.keluargaKaryawanInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        KeluargaKaryawan.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.keluargaKaryawanInstance && m.field)
                    result.keluargaKaryawanInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["KeluargaKaryawan", params.id]]
                return result
            }

            result.keluargaKaryawanInstance = KeluargaKaryawan.get(params.id)

            if (!result.keluargaKaryawanInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.keluargaKaryawanInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.keluargaKaryawanInstance.properties = params

            if (result.keluargaKaryawanInstance.hasErrors() || !result.keluargaKaryawanInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["KeluargaKaryawan", params.id]]
            return result
        }

        result.keluargaKaryawanInstance = new KeluargaKaryawan()
        result.keluargaKaryawanInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.keluargaKaryawanInstance && m.field)
                result.keluargaKaryawanInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["KeluargaKaryawan", params.id]]
            return result
        }

        params.hubungan = HubunganKeluarga.findByValue(params.hubungan as int)
        if (params.karyawanId) {
            params.karyawan = Karyawan.findById(new Long(params.karyawanId))
        }

        result.keluargaKaryawanInstance = new KeluargaKaryawan(params)

        if (result.keluargaKaryawanInstance.hasErrors() || !result.keluargaKaryawanInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def keluargaKaryawan = KeluargaKaryawan.findById(params.id)
        if (keluargaKaryawan) {
            keluargaKaryawan."${params.name}" = params.value
            keluargaKaryawan.save()
            if (keluargaKaryawan.hasErrors()) {
                throw new Exception("${keluargaKaryawan.errors}")
            }
        } else {
            throw new Exception("KeluargaKaryawan not found")
        }
    }

}