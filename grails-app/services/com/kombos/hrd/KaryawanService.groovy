package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KegiatanApproval
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.ApprovalT770
import com.kombos.parts.StatusApproval
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

class KaryawanService implements AfterApprovalInterface {
	boolean transactional = false

    final static String FAIL = 0
    final static String SUCCESS = 1
    def datatablesUtilService
	def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
	def generateCodeService

	def datatablesList(def params) {

		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def c = Karyawan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("branch",params?.companyDealer)
            }else{
                if(params."sCriteria_companyDealer"){
                    branch{
                        eq("id",params."sCriteria_companyDealer" as Long)
                    }
                }
            }
			if(params."sCriteria_nama"){
				ilike("nama","%" + (params."sCriteria_nama" as String) + "%")
			}

            if(params."sCriteria_npk"){
                ilike("nomorPokokKaryawan","%" + (params."sCriteria_npk" as String) + "%")
            }

            if(params."sCriteria_pekerjaan"){
                jabatan{
                    ilike("m014JabatanManPower","%" + (params."sCriteria_pekerjaan" as String) + "%")
                }
            }

            if (params."sCriteria_cabang"){
                branch{
                    eq("id",params."sCriteria_cabang" as Long)
                }
            }

            if(params."sCriteria_statusKaryawan"){
                statusKaryawan{
                    ilike("statusKaryawan","%" + (params."sCriteria_statusKaryawan" as String) + "%")
                }
            }

			if(params."sCriteria_tanggalLahir"){
				ge("tanggalLahir",params."sCriteria_tanggalLahir")
				lt("tanggalLahir",params."sCriteria_tanggalLahir" + 1)
			}

			if(params."sCriteria_tanggalMasukFrom"){
				ge("tanggalMasuk", params."sCriteria_tanggalMasukFrom")
			}

            if(params."sCriteria_tanggalMasukTo"){
                lt("tanggalMasuk", params."sCriteria_tanggalMasukTo" + 1)
            }

			if(params."sCriteria_alamat"){
				ilike("alamat","%" + (params."sCriteria_alamat" as String) + "%")
			}

			if(params."sCriteria_staAvailable"){
				ilike("staAvailable","%" + (params."sCriteria_staAvailable" as String) + "%")
			}

            eq("staApproval",StatusApproval.APPROVED)
            eq("staDel",'0')

			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						nomorPokokKaryawan: it?.nomorPokokKaryawan ? it?.nomorPokokKaryawan:"-",
			
						nama: it?.nama,
			
						tanggalLahir: it?.tanggalLahir?it.tanggalLahir.format(dateFormat):"",
			
						alamat: it?.alamat,
			
						jabatan: it?.jabatan?.m014JabatanManPower,
			
						staAvailable: it?.staAvailable=="1"?"Available":"Tidak Available",
			
						statusKaryawan: it?.statusKaryawan?.statusKaryawan,

                        cabang: it?.branch?.m011NamaWorkshop,

                        tanggalMasuk: it?.tanggalMasuk ? it?.tanggalMasuk.format("dd/MM/yyyy") :"",


			]

		}
		
		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
				
	}
	
	def show(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Karyawan", params.id] ]
			return result
		}

		result.karyawanInstance = Karyawan.get(params.id)

		if(!result.karyawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def edit(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Karyawan", params.id] ]
			return result
		}

		result.karyawanInstance = Karyawan.get(params.id)

		if(!result.karyawanInstance)
			return fail(code:"default.not.found.message")

		// Success.
		return result
	}
	
	def delete(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Karyawan", params.id] ]
			return result
		}

		result.karyawanInstance = Karyawan.get(params.id)

		if(!result.karyawanInstance)
			return fail(code:"default.not.found.message")

		try {
			result.karyawanInstance.delete(flush:true)
			return result //Success.
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
			return fail(code:"default.not.deleted.message")
		}

	}
	
	def update(params) {
		Karyawan.withTransaction { status ->
			def result = [:]

			def fail = { Map m ->
				status.setRollbackOnly()
				if(result.karyawanInstance && m.field)
					result.karyawanInstance.errors.rejectValue(m.field, m.code)
				result.error = [ code: m.code, args: ["Karyawan", params.id] ]
				return result
			}

			result.karyawanInstance = Karyawan.get(params.id as long)

			if(!result.karyawanInstance){
                return fail(code:"default.not.found.message")
            }

			// Optimistic locking check.
			if(params.version) {
				if(result.karyawanInstance.version > params.version.toLong())
					return fail(field:"version", code:"default.optimistic.locking.failure")
			}

			result.karyawanInstance.properties = params

			if(result.karyawanInstance.hasErrors() || !result.karyawanInstance.save()){
                return fail(code:"default.not.updated.message")
            }

			// Success.
			return result

		} //end withTransaction
	}  // end update()

	def create(params) {
		def result = [:]
		def fail = { Map m ->
			result.error = [ code: m.code, args: ["Karyawan", params.id] ]
			return result
		}

		result.karyawanInstance = new Karyawan()
		result.karyawanInstance.properties = params

		// success
		return result
	}

	def save(params) {
		def result = [:]
		def fail = { Map m ->
			if(result.karyawanInstance && m.field)
				result.karyawanInstance.errors.rejectValue(m.field, m.code)
			result.error = [ code: m.code, args: ["Karyawan", params.id] ]
			return result
		}

        if(params.role == 'HRD'){
            params.staApproval = StatusApproval.WAIT_FOR_APPROVAL
        }else{
            params.staApproval = StatusApproval.APPROVED
        }
        result.karyawanInstance = new Karyawan(params)
        if(params.nomorPokokKaryawan){
            if(Karyawan.findByNomorPokokKaryawan(params.nomorPokokKaryawan)){
                return fail(code:"dataGanda")
            }
        }

        result.karyawanInstance.save(flush: true)


		if(result.karyawanInstance.hasErrors()) {
            return fail(code:"default.not.created.message")
        } else {
            // insert juga ke tabel TH003_HistoryKaryawan dengan history "MASUK".


            if (insertToHistoryKaryawan(result.karyawanInstance,result.karyawanInstance.branch) == FAIL) {
                return fail(code:"default.not.created.message")
            }

        }

        if(!KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.KARYAWAN_BARU)){
            def kg = new KegiatanApproval(
                    m770IdApproval : generateCodeService.codeGenerateSequence("M770_IDApproval",null),
                    m770KegiatanApproval : KegiatanApproval.KARYAWAN_BARU,
                    m770NamaTabel : 'MH009_Karyawan',
                    m770NamaDomain : 'Karyawan',
                    m770NamaFk : 'id',
                    m770NamaFieldDiupdate :'id',
                    m770NamaFormDetail : 'Karyawan',
                    m770StaButuhNilai : '0',
                    afterApprovalService :'karyawanService',
                    staDel : "0",
                    createdBy : 'SYSTEM',
                    lastUpdProcess : "INSERT"
            ).save(flush: true)
        }
        if(params.role == 'HRD'){
            def approval = new ApprovalT770(
                    t770FK:result.karyawanInstance.id as String,
                    kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.KARYAWAN_BARU),
                    t770NoDokumen: 'karyawanBaru'+result.karyawanInstance.id as String,
                    t770TglJamSend: datatablesUtilService?.syncTime(),
                    t770Pesan: "Karyawan Baru " + result.karyawanInstance.nama,
                    companyDealer: params?.cd,
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
            )
            approval.save(flush:true)
            approval.errors.each{ //println it
			}
        }

		// success
		return result
	}
	
	def updateField(def params){
		def karyawan =  Karyawan.findById(params.id)
		if (karyawan) {
			karyawan."${params.name}" = params.value
			karyawan.save()
			if (karyawan.hasErrors()) {
				throw new Exception("${karyawan.errors}")
			}
		}else{
			throw new Exception("Karyawan not found")
		}
	}

    int insertToHistoryKaryawan(Karyawan karyawan,CompanyDealer companyDealer) {
        History history = History.findByHistory("MASUK")


        if (history) {
            HistoryKaryawan hk = new HistoryKaryawan(
                    tanggal: new Date(),
                    history: history,
                    companyDealer: companyDealer,
                    divisi : karyawan.divisi,
                    staApproval: StatusApproval.APPROVED,
                    jabatanAfter : karyawan.jabatan,
                    cabangAfter : karyawan.branch,
                    karyawan: karyawan,
                    createdBy : 'SYSTEM',
                    lastUpdProcess : "INSERT",
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
            ).save(flush: true, failOnError: true)

            if (!hk.hasErrors()) {
                return SUCCESS
            } else {
                //println hk.errors
            }

        } else {
            log.error("ERROR when inserting data karyawan to History Karyawan! (history not found)")
        }

        return FAIL
    }

    List<HistoryKaryawan> findHistoryKaryawan(Karyawan karyawan) {
        List<HistoryKaryawan> historyKaryawanList =
                HistoryKaryawan.findAllByKaryawan(karyawan)
        return historyKaryawanList
    }
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            def karyawan = Karyawan.get(it as Long)
            karyawan.staApproval = staApproval
            karyawan.save(flush: true)
        }
    }

    def massDelete(def params) {
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def k = Karyawan.get(it)
            k.staDel = '1'
            k.save(flush: true)
        }
    }
}
