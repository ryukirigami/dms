package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class PendidikanKaryawanService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PendidikanKaryawan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_karyawan") {
                eq("karyawan", params."sCriteria_karyawan")
            }

            if(params."dari"){
                if(params.idKaryawan){
                    karyawan {
                        eq("id", params.idKaryawan.toLong())
                    }
                }else{
                    karyawan {
                        eq("id", -10000.toLong())
                    }
                }
            }

            if (params."sCriteria_pendidikan") {
                eq("pendidikan", params."sCriteria_pendidikan")
            }

            if (params."sCriteria_namaSekolah") {
                ilike("namaSekolah", "%" + (params."sCriteria_namaSekolah" as String) + "%")
            }

            if (params."sCriteria_tahunMasuk") {
                eq("tahunMasuk", params."sCriteria_tahunMasuk")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_jurusan") {
                ilike("jurusan", "%" + (params."sCriteria_jurusan" as String) + "%")
            }

            if (params."sCriteria_nemIpk") {
                eq("nemIpk", params."sCriteria_nemIpk")
            }

            if (params."sCriteria_statusLulus") {
                ilike("statusLulus", "%" + (params."sCriteria_statusLulus" as String) + "%")
            }

            if (params."sCriteria_tahunLulus") {
                eq("tahunLulus", params."sCriteria_tahunLulus")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    karyawan: it.karyawan,

                    pendidikan: it.pendidikan.pendidikan,

                    namaSekolah: it.namaSekolah,

                    tahunMasuk: it.tahunMasuk,

                    lastUpdProcess: it.lastUpdProcess,

                    jurusan: it.jurusan,

                    nemIpk: it.nemIpk,

                    statusLulus: it.statusLulus,

                    tahunLulus: it.tahunLulus,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PendidikanKaryawan", params.id]]
            return result
        }

        result.pendidikanKaryawanInstance = PendidikanKaryawan.get(params.id)

        if (!result.pendidikanKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PendidikanKaryawan", params.id]]
            return result
        }

        result.pendidikanKaryawanInstance = PendidikanKaryawan.get(params.id)

        if (!result.pendidikanKaryawanInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PendidikanKaryawan", params.id]]
            return result
        }

        result.pendidikanKaryawanInstance = PendidikanKaryawan.get(params.id)

        if (!result.pendidikanKaryawanInstance)
            return fail(code: "default.not.found.message")

        try {
            result.pendidikanKaryawanInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        PendidikanKaryawan.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.pendidikanKaryawanInstance && m.field)
                    result.pendidikanKaryawanInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["PendidikanKaryawan", params.id]]
                return result
            }

            result.pendidikanKaryawanInstance = PendidikanKaryawan.get(params.id)

            if (!result.pendidikanKaryawanInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.pendidikanKaryawanInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.pendidikanKaryawanInstance.properties = params

            if (result.pendidikanKaryawanInstance.hasErrors() || !result.pendidikanKaryawanInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PendidikanKaryawan", params.id]]
            return result
        }

        result.pendidikanKaryawanInstance = new PendidikanKaryawan()
        result.pendidikanKaryawanInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.pendidikanKaryawanInstance && m.field)
                result.pendidikanKaryawanInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["PendidikanKaryawan", params.id]]
            return result
        }

        if (params.karyawanId) {
            params.karyawan = Karyawan.findById(new Long(params.karyawanId))
        }

        result.pendidikanKaryawanInstance = new PendidikanKaryawan(params)

        if (result.pendidikanKaryawanInstance.hasErrors() || !result.pendidikanKaryawanInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def pendidikanKaryawan = PendidikanKaryawan.findById(params.id)
        if (pendidikanKaryawan) {
            pendidikanKaryawan."${params.name}" = params.value
            pendidikanKaryawan.save()
            if (pendidikanKaryawan.hasErrors()) {
                throw new Exception("${pendidikanKaryawan.errors}")
            }
        } else {
            throw new Exception("PendidikanKaryawan not found")
        }
    }

}