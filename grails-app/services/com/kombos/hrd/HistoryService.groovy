package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class HistoryService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(params) {

        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = History.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_history") {
                ilike("history", "%" + (params."sCriteria_history" as String) + "%")
            }

            if (params."sCriteria_keterangan") {
                ilike("keterangan", "%" + (params."sCriteria_keterangan" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    history: it.history,

                    keterangan: it.keterangan,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["History", params.id]]
            return result
        }

        result.historyInstance = History.get(params.id)

        if (!result.historyInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["History", params.id]]
            return result
        }

        result.historyInstance = History.get(params.id)

        if (!result.historyInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["History", params.id]]
            return result
        }

        result.historyInstance = History.get(params.id)

        if (!result.historyInstance)
            return fail(code: "default.not.found.message")

        try {
            result.historyInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        History.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.historyInstance && m.field)
                    result.historyInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["History", params.id]]
                return result
            }

            result.historyInstance = History.get(params.id)

            if (!result.historyInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.historyInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.historyInstance.properties = params

            if (result.historyInstance.hasErrors() || !result.historyInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["History", params.id]]
            return result
        }

        result.historyInstance = new History()
        result.historyInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.historyInstance && m.field)
                result.historyInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["History", params.id]]
            return result
        }

        result.historyInstance = new History(params)

        if (result.historyInstance.hasErrors() || !result.historyInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def history = History.findById(params.id)
        if (history) {
            history."${params.name}" = params.value
            history.save()
            if (history.hasErrors()) {
                throw new Exception("${history.errors}")
            }
        } else {
            throw new Exception("History not found")
        }
    }

}