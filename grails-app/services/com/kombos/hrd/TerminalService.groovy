package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class TerminalService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Terminal.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_kodeTerminal") {
                ilike("kodeTerminal", "%" + (params."sCriteria_kodeTerminal" as String) + "%")
            }

            if (params."sCriteria_namaTerminal") {
                ilike("namaTerminal", "%" + (params."sCriteria_namaTerminal" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kodeTerminal: it.kodeTerminal,

                    namaTerminal: it.namaTerminal,

                    lastUpdProcess: it.lastUpdProcess,

                    companyDealer: it.companyDealer.m011NamaWorkshop,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Terminal", params.id]]
            return result
        }

        result.terminalInstance = Terminal.get(params.id)

        if (!result.terminalInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Terminal", params.id]]
            return result
        }

        result.terminalInstance = Terminal.get(params.id)

        if (!result.terminalInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Terminal", params.id]]
            return result
        }

        result.terminalInstance = Terminal.get(params.id)

        if (!result.terminalInstance)
            return fail(code: "default.not.found.message")

        try {
            result.terminalInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        Terminal.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.terminalInstance && m.field)
                    result.terminalInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["Terminal", params.id]]
                return result
            }

            result.terminalInstance = Terminal.get(params.id)

            if (!result.terminalInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.terminalInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.terminalInstance.properties = params

            if (result.terminalInstance.hasErrors() || !result.terminalInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["Terminal", params.id]]
            return result
        }

        result.terminalInstance = new Terminal()
        result.terminalInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.terminalInstance && m.field)
                result.terminalInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["Terminal", params.id]]
            return result
        }
        params?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params?.lastUpdProcess = "INSERT"
        params?.staDel = '0'
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        result.terminalInstance = new Terminal(params)

        if (result.terminalInstance.hasErrors() || !result.terminalInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def terminal = Terminal.findById(params.id)
        if (terminal) {
            terminal."${params.name}" = params.value
            terminal.save()
            if (terminal.hasErrors()) {
                throw new Exception("${terminal.errors}")
            }
        } else {
            throw new Exception("Terminal not found")
        }
    }
}