package com.kombos.hrd



import com.kombos.baseapp.AppSettingParam

class BranchHRDService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = BranchHRD.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_namaCabang") {
                ilike("namaCabang", "%" + (params."sCriteria_namaCabang" as String) + "%")
            }

            if (params."sCriteria_alamat") {
                ilike("alamat", "%" + (params."sCriteria_alamat" as String) + "%")
            }

            if (params."sCriteria_contactPerson") {
                ilike("contactPerson", "%" + (params."sCriteria_contactPerson" as String) + "%")
            }

            if (params."sCriteria_telepon") {
                ilike("telepon", "%" + (params."sCriteria_telepon" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    namaCabang: it.namaCabang,

                    alamat: it.alamat,

                    contactPerson: it.contactPerson,

                    telepon: it.telepon,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BranchHRD", params.id]]
            return result
        }

        result.branchHRDInstance = BranchHRD.get(params.id)

        if (!result.branchHRDInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BranchHRD", params.id]]
            return result
        }

        result.branchHRDInstance = BranchHRD.get(params.id)

        if (!result.branchHRDInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BranchHRD", params.id]]
            return result
        }

        result.branchHRDInstance = BranchHRD.get(params.id)

        if (!result.branchHRDInstance)
            return fail(code: "default.not.found.message")

        try {
            result.branchHRDInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        BranchHRD.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.branchHRDInstance && m.field)
                    result.branchHRDInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["BranchHRD", params.id]]
                return result
            }

            result.branchHRDInstance = BranchHRD.get(params.id)

            if (!result.branchHRDInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.branchHRDInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            result.branchHRDInstance.properties = params

            if (result.branchHRDInstance.hasErrors() || !result.branchHRDInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["BranchHRD", params.id]]
            return result
        }

        result.branchHRDInstance = new BranchHRD()
        result.branchHRDInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.branchHRDInstance && m.field)
                result.branchHRDInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["BranchHRD", params.id]]
            return result
        }

        result.branchHRDInstance = new BranchHRD(params)

        if (result.branchHRDInstance.hasErrors() || !result.branchHRDInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def branchHRD = BranchHRD.findById(params.id)
        if (branchHRD) {
            branchHRD."${params.name}" = params.value
            branchHRD.save()
            if (branchHRD.hasErrors()) {
                throw new Exception("${branchHRD.errors}")
            }
        } else {
            throw new Exception("BranchHRD not found")
        }
    }
}
//woke