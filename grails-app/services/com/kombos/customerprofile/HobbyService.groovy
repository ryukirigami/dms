package com.kombos.customerprofile

class HobbyService {

    def datatablesList(def params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Hobby.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
              eq("staDel","0")
            if(params."sCriteria_m063ID"){
                ilike("m063ID","%" + (params."sCriteria_m063ID" as String) + "%")
            }
            if(params."sCriteria_m063NamaHobby"){
                ilike("m063NamaHobby","%" + (params."sCriteria_m063NamaHobby" as String) + "%")
            }
            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }
        def rows = []
        results.each {
            rows << [
                id: it.id,
                m063ID: it.m063ID,
                m063NamaHobby: it.m063NamaHobby,
                staDel: it.staDel,
                createdBy: it.createdBy,
                updatedBy: it.updatedBy,
                lastUpdProcess: it.lastUpdProcess,
            ]
        }
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }
}
