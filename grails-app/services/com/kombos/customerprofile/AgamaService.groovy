package com.kombos.customerprofile

class AgamaService {

    def datatablesList(def params) {
   		def propertiesToRender = params.sColumns.split(",")
   		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
   		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
   		def x = 0

   		def c = Agama.createCriteria()
   		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
               eq("staDel","0")
   			if(params."sCriteria_m061ID"){
   				ilike("m061ID","%" + (params."sCriteria_m061ID" as String) + "%")
   			}
   			if(params."sCriteria_m061NamaAgama"){
   				ilike("m061NamaAgama","%" + (params."sCriteria_m061NamaAgama" as String) + "%")
   			}
   			switch(sortProperty){
   				default:
   					order(sortProperty,sortDir)
   					break;
   			}
   		}

   		def rows = []

   		results.each {
   			rows << [
   						id: it.id,
   						m061ID: it.m061ID,
   						m061NamaAgama: it.m061NamaAgama,
   						staDel: it.staDel,
   						createdBy: it.createdBy,
   						updatedBy: it.updatedBy,
   						lastUpdProcess: it.lastUpdProcess,
   			]
   		}
   		[sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
   	}

}
