package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam

class JenisIdCardService {
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = JenisIdCard.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
              eq("staDel","0")
            if(params."sCriteria_m060ID"){
                ilike("m060ID","%" + (params."sCriteria_m060ID" as String) + "%")
            }

            if(params."sCriteria_m060JenisIDCard"){
                ilike("m060JenisIDCard","%" + (params."sCriteria_m060JenisIDCard" as String) + "%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                        id: it.id,

                        m060ID: it.m060ID,

                        m060JenisIDCard: it.m060JenisIDCard.toUpperCase(),

                        staDel: it.staDel,

                        createdBy: it.createdBy,

                        updatedBy: it.updatedBy,

                        lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
    }
}
