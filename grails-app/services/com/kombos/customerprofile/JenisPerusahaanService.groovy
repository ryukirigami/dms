package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam

class JenisPerusahaanService {

    boolean transactional = false
    def  datatablesUtilService

    def datatablesList(def params) {
        def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = JenisPerusahaan.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params."sCriteria_nomorPerusahaan"){
                eq("nomorPerusahaan", Integer.parseInt(params."sCriteria_nomorPerusahaan"))
            }
            if(params."sCriteria_namaJenisPerusahaan"){
                ilike("namaJenisPerusahaan","%" + (params."sCriteria_namaJenisPerusahaan" as String) + "%")
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    nomorPerusahaan: it.nomorPerusahaan,

                    namaJenisPerusahaan: it.namaJenisPerusahaan

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["JenisPerusahaan", params.id] ]
            return result
        }

        result.JenisPerusahaanInstance = new JenisPerusahaan()
        result.JenisPerusahaanInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if(result.jenisPerusahaanInstance && m.field)
                result.jenisPerusahaanInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["JenisPerusahaan", params.id] ]
            return result
        }

        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        result.jenisPerusahaanInstance = new JenisPerusahaan(params)

        if(result.jenisPerusahaanInstance.hasErrors() || !result.jenisPerusahaanInstance.save(flush: true))
            return fail(code:"default.not.created.message")

        // success
        return result
    }

    def show(id) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["JenisPerusahaan", id as long] ]
            return result
        }

        result.jenisPerusahaanInstance = JenisPerusahaan.get(id)

        if(!result.jenisPerusahaanInstance)
            return fail(code:"default.not.found.message")

        // Success.
        return result
    }

    def update(params) {
        JenisPerusahaan.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if(result.jenisPerusahaanInstance && m.field)
                    result.jenisPerusahaanInstance.errors.rejectValue(m.field, m.code)
                result.error = [ code: m.code, args: ["JenisPerusahaan", params.id] ]
                return result
            }

            params.lastUpdated = datatablesUtilService?.syncTime()
            result.jenisPerusahaanInstance = JenisPerusahaan.get(params.id)

            if(!result.jenisPerusahaanInstance)
                return fail(code:"default.not.found.message")

            // Optimistic locking check.
            if(params.version) {
                if(result.jenisPerusahaanInstance.version > params.version.toLong())
                    return fail(field:"version", code:"default.optimistic.locking.failure")
            }

            result.jenisPerusahaanInstance.properties = params

            if(result.jenisPerusahaanInstance.hasErrors() || !result.jenisPerusahaanInstance.save())
                return fail(code:"default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["JenisPerusahaan", params.id] ]
            return result
        }

        result.jenisPerusahaanInstance = JenisPerusahaan.get(params.id)

        if(!result.jenisPerusahaanInstance)
            return fail(code:"default.not.found.message")

        try {
            result.jenisPerusahaanInstance.delete(flush:true)
            return result //Success.
        }
        catch(org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code:"default.not.deleted.message")
        }

    }
}