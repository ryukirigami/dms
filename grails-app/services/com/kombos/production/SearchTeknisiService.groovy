package com.kombos.production

import com.kombos.administrasi.ManPowerSertifikat
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.reception.Reception

class SearchTeknisiService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = NamaManPower.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            manPowerDetail{
                manPower{
                    ilike("m014JabatanManPower","%TEKNISI%");
                }
            }
            if (params."sCriteria_initial") {
                    ilike("t015NamaBoard", "%" + (params."sCriteria_initial" as String) + "%")
            }
            if (params."sCriteria_nama") {
                ilike("t015NamaLengkap", "%" + (params."sCriteria_nama" as String) + "%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    initial : it?.t015NamaBoard,

                    nama : it?.t015NamaLengkap

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesHistoryList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        //System.out.println( "cetak id params"+params."sCriteria_Tanggal")
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")

            namaManPower{
                eq("id", params."sCriteria_id" as Long)
            }


            if(params."sCriteria_Tanggal" && params."sCriteria_TanggalAkhir"){
                ge("t401TanggalWO",params."sCriteria_Tanggal")
                lt("t401TanggalWO",params."sCriteria_TanggalAkhir" + 1)
            }
            order("t401NoWO")
            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t401TanggalWO : it.t401TanggalWO,

                    t401NoWO : it.t401NoWO,

                    operation : it.operation?.m053NamaOperation

            ]
        }
  //println("cetak rows"+results.size() )
        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def findData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def namaManPowerInstance = NamaManPower.createCriteria()
        def results = namaManPowerInstance.list() {
            if(params.kategori=="Inisial Teknisi"){
                manPowerDetail{
                    ilike("m015Inisial","%"+params.key+"%")
                }
            }else{
                ilike("t015NamaLengkap","%"+params.key+"%")
            }
        }


        def rows = []

        results.each {
            String sertif = "-"
            def manPowSert = ManPowerSertifikat.findByNamaManPower(it)
            if(manPowSert!=null){
                sertif=manPowSert?.sertifikat?.m016NamaSertifikat;
            }
            rows << [
                    id : it?.id,

                    initNama : it?.manPowerDetail?.m015Inisial+" - "+it?.t015NamaLengkap ,

                    tglLahir : it?.t015TanggalLahir ? it?.t015TanggalLahir.format(dateFormat) : "",

                    telepon  : it?.t015NoTelp ? it?.t015NoTelp:"-",

                    sertifikat  : sertif ? sertif:"-",

                    subJenis  : it?.manPowerDetail?.m015LevelManPower,

                    aktif  : it?.t015StaAktif==1 ? "YA" : "TIDAK",

                    alamat  : it?.t015Alamat,

                    ket  : it?.t015Ket
            ]
        }

        return rows
    }

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["Complaint", params.id] ]
            return result
        }

        result.complaintInstance = new NamaManPower()
        result.complaintInstance.properties = params

        // success
        return result
    }
}