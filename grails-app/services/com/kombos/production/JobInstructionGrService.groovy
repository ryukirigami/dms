package com.kombos.production

import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Operation
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.maintable.ApprovalT770
import com.kombos.maintable.StatusActual
import com.kombos.parts.StatusApproval
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP

import java.text.DateFormat
import java.text.SimpleDateFormat

class JobInstructionGrService implements AfterApprovalInterface{

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        def ambilWO = "semua"
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            isNull("t401StaInvoice");
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","GR")
                }
            }

            if(params."sCriteria_noWO"){
                ilike("t401NoWO","%" + (params."sCriteria_noWO" as String) + "%")
            }

            if(params."sCriteria_noPol"){
                historyCustomerVehicle{
                    ilike("fullNoPol","%" + (params."sCriteria_noPol" as String) + "%")
                }
            }

            if (params."sCriteria_Tanggal" && params."sCriteria_Tanggalakhir") {
                ge("t401TglJamCetakWO", params."sCriteria_Tanggal")
                lt("t401TglJamCetakWO", params."sCriteria_Tanggalakhir" + 1)
            }

            if (params."sCriteria_staAmbilWO") {
                if(params."sCriteria_staAmbilWO"=="Belum Ambil WO"){
                    ambilWO="belum"
                    eq("t401StaAmbilWO","0")
                }
                if(params."sCriteria_staAmbilWO"=="Sudah Ambil WO"){
                    eq("t401StaAmbilWO","1")
                }
            }

            if (params."sCriteria_staCarryOver") {
                if(params."sCriteria_staCarryOver"=="Ya"){
                    eq("t401StaTinggalMobil","1")
                }else{
                    eq("t401StaTinggalMobil","0")
                }
            }

            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            eq("staSave","0")
            isNotNull("t401NoWO")
            order("id",'desc')
        }
        def rows = []
        int index = 0;
        results.each {
            def receptt = it
            String startParams = "-";
            String stopParams = "-";
            String finalInspect = "-";
            String teknisiParams = "-";
            def actual = Actual.findAllByReceptionAndStaDelAndStatusActual(Reception.findByT401NoWO(it.t401NoWO),"0",StatusActual.findByM452Id("1"))
            def actual2 = Actual.findAllByReceptionAndStaDelAndStatusActual(Reception.findByT401NoWO(it.t401NoWO),"0",StatusActual.findByM452Id("4"))
            def finalIns = FinalInspection.findByReception(Reception.findByT401NoWO(it.t401NoWO))
            def jpb = JPB.createCriteria().list {
                eq("staDel","0");
                eq("reception",receptt);
                order("t451TglJamPlan");
            }
            def jobRCP = JobRCP.findAllByReceptionAndStaDel(Reception.findByT401NoWO(it.t401NoWO),"0")
            if(finalIns){
                finalInspect = df.format(finalIns.t508TglJamSelesai);
            }
            if(jpb){
                int a=0;
                jpb.each {
                    a++;
                    teknisiParams+=it.namaManPower.t015NamaBoard;
                    if(a<jpb.size()){
                        teknisiParams+=" , "
                    }
                }
            }
            if(it?.t401TglJamRencana){
                startParams = it?.t401TglJamRencana?.format("dd/MM/yyyy HH:mm")
            }
            if(it?.t401TglJamJanjiPenyerahan){
                stopParams = it?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm")
            }
            def tampil=true
            def cek = []

            if (params."sCriteria_staProblemFinding") {
                if(jpb?.size()>0){
                    def masalah = Masalah.findByJpb(jpb?.last())
                    if(params."sCriteria_staProblemFinding"=="Teknisi"){
                        if(masalah==null || (masalah && masalah?.t501staProblemTeknis!="1")){
                            tampil=false
                        }
                    }else{
                        if(masalah==null || (masalah && masalah?.t501staProblemTeknis!="0")){
                            tampil=false
                        }
                    }
                }
            }



            if (params."sCriteria_staFinalInspection") {
                if(params."sCriteria_staFinalInspection"=="Sudah Final Inspection"){
                    if(finalIns==null){
                        tampil=false;
                    }
                }else{
                    if(finalIns!=null){
                        tampil=false;
                    }
                }
            }
            if (params."sCriteria_staJobTambah") {
                if(params."sCriteria_staJobTambah"=="Ya"){
                    tampil=false
                    if(jobRCP==null){
                        cek << "0"
                    }else{
                        jobRCP.each {
                            if(it.t402StaPekerjaanTambahanCust!="1"){
                                cek << "0"
                            }else{
                                cek << "1"
                            }
                        }
                    }
                }else{
                    if(jobRCP==null){
                        cek << "0"
                    }else{
                        jobRCP.each {
                            if(it.t402StaPekerjaanTambahanCust!="0"){
                                cek << "0"
                            }else{
                                cek << "1"
                            }
                        }
                    }

                }
            }
            if(tampil==true || cek.contains("1")){
                index++;
                rows << [

                        t401NoWO: it.t401NoWO,

                        noPolisi: it.historyCustomerVehicle?it.historyCustomerVehicle.fullNoPol:"",

                        t401TanggalWO : it?.t401TglJamCetakWO? df.format(it.t401TglJamCetakWO):df.format(it?.dateCreated),

                        t401NamaSA : it?.t401NamaSA?User?.findByUsername(it?.t401NamaSA)?.fullname:"",

                        t401TglJamJanjiPenyerahan : it?.t401TglJamJanjiPenyerahan? df.format(it.t401TglJamJanjiPenyerahan):"-",

                        start : startParams,

                        stop : stopParams,

                        statAmbilWO : it.t401StaAmbilWO,

                        finalInspection : finalInspect,

                        teknisi : teknisiParams

                ]

            }
        }



        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubList(def params) {
        //println "datatablesSubList " + params.t401NoWO
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def jpb = JPB.findAllByReceptionAndStaDel(Reception.findByT401NoWO(params.t401NoWO),"0")
        def teknisiParams = ""
        def foremanParams = ""
        if(jpb){
            int a=0;
            jpb.each {
                a++;
                teknisiParams+=it?.namaManPower?.t015NamaBoard;
                foremanParams+=it?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard;
                if(a<jpb.size()){
                    teknisiParams+=" , "
                    foremanParams+=" , "
                }
            }
        }
        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
            or{
                isNull("t402StaOkCancel")
                eq("t402StaOkCancel","0")
            }
        }

        def rows = []
        int index=0
        def staKurangiJob = 0
        results.each {
            staKurangiJob = 0
            String startParams = "-";
            String stopParams = "-";
            def startAct = Actual.findByReceptionAndStatusActualAndStaDelAndOperation(it.reception,StatusActual.findByM452Id("1"),"0",it.operation)
            if(startAct){
                startParams = df.format(startAct.t452TglJam);
            }

            def stopAct = Actual.findByReceptionAndStatusActualAndStaDelAndOperation(it.reception,StatusActual.findByM452Id("4"),"0",it.operation)
            if(stopAct){
                stopParams = df.format(stopAct.t452TglJam);
                staKurangiJob = 1
            }
            index++
            rows << [

                    id: it.id,

                    t401NoWO: params.t401NoWO,

                    namaJob: it.operation ? it.operation.m053NamaOperation : "",

                    startJob : startParams,

                    stopJob : stopParams,

                    staTambah : it.t402StaPekerjaanTambahanCust,

                    teknisi : teknisiParams,

                    staKurangiJob:staKurangiJob,

                    foreman : foremanParams,

                    idJob : it.operation.id

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  index, iTotalDisplayRecords: index, aaData: rows]

    }

    def datatablesSubSubList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        //println "datatablesSubList " + params.t401NoWO
        def c = JPB.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params.t401NoWO){
                reception{
                    eq("t401NoWO", params.t401NoWO)
                }
            }
            eq("staDel", "0")
        }

        def rows = []

        results.each {
            String startParams = "-";
            String stopParams = "-";
            def startAct = Actual.findByReceptionAndStatusActualAndStaDelAndOperation(it.reception,StatusActual.findByM452Id("1"),"0",Operation.findById(params.idJob as Long))
            if(startAct){
                startParams = df.format(startAct.t452TglJam);
            }

            def stopAct = Actual.findByReceptionAndStatusActualAndStaDelAndOperation(it.reception,StatusActual.findByM452Id("4"),"0",Operation.findById(params.idJob as Long))
            if(stopAct){
                stopParams = df.format(stopAct.t452TglJam);
            }
            rows << [

                    id: it.id,

                    start  : startParams ,

                    stop : stopParams,

                    teknisi : it.namaManPower?.t015NamaBoard,

                    t401NoWO : params.t401NoWO,

                    idJob : params.idJob

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def datatablesSubSubSubList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        //println "datatablesSubSubSubList teknisi : " + params.teknisi
        def nmp = NamaManPower.findByT015NamaBoard(params.teknisi)
        //println "datatablesSubSubSubList job : " + params.idJob
        def x = 0
        def c = Actual.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
            operation{
                eq("id",params.idJob as Long)
            }
            eq("namaManPower",nmp)
            order("dateCreated","asc")
        }

        def rows = []

       //println("cetak aktual "+results.size())

        results.each {
            def problem = "-"
            if(it.t452staProblemTeknis == "1"){
                problem = "Teknis"
            }else if(it.t452staProblemTeknis == "0"){
                problem = "Non Teknis"
            }
            rows << [

                    id: it.id,

                    status: it?.statusActual?.m452StatusActual,

                    tanggalStatus: df.format(it?.t452TglJam),

                    ket : it?.t452Ket,

                    problem : problem,

                    aksi : it?.statusActual?.id==2?it?.statusActual?.id:"-",

                    foremanGanti : it?.namaManPower?.manPowerDetail?.m015Inisial,

                    noWo : params.t401NoWO,
                    idJob : params.idJob,
                    teknisi : params.teknisi


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            if(staApproval==StatusApproval.APPROVED){
                def jobrcp = JobRCP.findById(it as Long)
                def ubhData = JobRCP.get(jobrcp.id)
                ubhData?.t402TglOkCancel = new Date()
                ubhData?.t402StaOkCancel = "1"
                ubhData?.save(flush: true)
                def partsrcp = PartsRCP.findAllByOperationAndReceptionAndStaDel(jobrcp.operation,jobrcp.reception,"0")
                partsrcp.each {
                    def ubahParts = PartsRCP.get(it.id as Long)
                    ubahParts?.t403TglCancel = new Date()
                    ubahParts?.t403StaOkCancel = "1"
                    ubahParts?.save(flush: true)
                }
            }
        }
    }
}
