package com.kombos.production

import com.kombos.administrasi.ManPowerAbsensi
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.StaHadir

import java.text.DateFormat
import java.text.SimpleDateFormat

class AbsensiTeknisiService {

    boolean transactional = false

    def datatablesUtilService

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [ code: m.code, args: ["absensiTeknisi", params.id] ]
            return result
        }

        result.absensiTeknisi = new ManPowerAbsensi()
        result.absensiTeknisi.properties = params

        // success
        return result
    }

//    ianian
    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        //println waktuSekarang
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def results = ManPowerAbsensi.createCriteria().list {
//            ge("dateCreated",dateAwal)
//            lt("dateCreated",waktuSekarang + 1)
            eq("staHadir",StaHadir.findByM017ID("0 "))
        }

        def rows = []
        def count = 0
        results.each {
            count += 1
            rows << [

                    nomor: count,

                    teknisi: it?.namaManPower.t015NamaLengkap,

                    jamMasuk:it?.t017JamDatang? it?.t017JamDatang.format("dd/MM/yyyy HH:mm:ss"): "",

                    jamKeluar:it?.t017JamPulang ? it?.t017JamPulang.format("dd/MM/yyyy HH:mm:ss") : ""

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

    }
//    ianian

    def save(params) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def result = [:]
        def fail = { Map m ->
            if(result.manPowerAbsensiInstance && m.field)
                result.manPowerAbsensiInstance.errors.rejectValue(m.field, m.code)
            result.error = [ code: m.code, args: ["manPowerAbsensi", params.id] ]
            return result
        }

        Date tanggal = df.parse(params.tanggal)
        def MPA = ManPowerAbsensi.findByNamaManPowerAndT017Tanggal(NamaManPower.findById(params.namaManPower.id),tanggal)
        if(MPA){
            //println "masyuk mpa"
            result.manPowerAbsensiInstance = ManPowerAbsensi.get(MPA.id)
            result.manPowerAbsensiInstance?.lastUpdProcess = "UPDATE"
            result.manPowerAbsensiInstance?.lastUpdated = datatablesUtilService?.syncTime()

        }else {
            //println "tdk masyuk mpa"
            result.manPowerAbsensiInstance = new ManPowerAbsensi()
            result.manPowerAbsensiInstance?.namaManPower = NamaManPower.findById(params.namaManPower.id)
            result.manPowerAbsensiInstance?.t017Tanggal = tanggal
            result.manPowerAbsensiInstance?.staHadir = StaHadir.findByM017ID("0 ")
            result.manPowerAbsensiInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.manPowerAbsensiInstance?.lastUpdProcess = "INSERT"
            result.manPowerAbsensiInstance?.t017Ket = ''
            result.manPowerAbsensiInstance?.staDel = '0'
            result.manPowerAbsensiInstance?.dateCreated = datatablesUtilService?.syncTime()
            result.manPowerAbsensiInstance.companyDealer = params.company
        }
        if(params.statusAbsen=='0'){
            //println "status absen 0 ==> " + params.statusAbsen
             result.manPowerAbsensiInstance?.t017JamDatang = new Date()
        }else{
            //println "status absen tdk 0 ==> " + params.statusAbsen
            result.manPowerAbsensiInstance?.t017JamPulang = new Date()
        }

        result.manPowerAbsensiInstance.save(flush: true)
        result.manPowerAbsensiInstance.errors.each{
            //println it
        }

         return "sukses"
    }

//    ian
    def findIdTeknisi(params){
        def idTeknisi = NamaManPower.findById(params.id)
        //println('kiriman :' +params.id)
        def rows =[:]
                    rows << [


                    t015IdManPower :idTeknisi.t015IdManPower,

            ]

        return rows
    }
//        Dita
}
