package com.kombos.production

import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.board.JPB
import com.kombos.maintable.StatusActual
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.*
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class BPProblemFindingListService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        int jumlah = 0
        def ambilWO = "semua"
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","bp", [ignoreCase: true])
                }
            }
            if (params."sCriteria_Tanggal" && params."sCriteria_Tanggalakhir") {
                ge("t401TglJamCetakWO", params."sCriteria_Tanggal")
                lt("t401TglJamCetakWO", params."sCriteria_Tanggalakhir" + 1)
            }

            order("t401NoWO")
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t401NoWO", "t401NoWO")
                groupProperty("historyCustomerVehicle", "noPolisi")
                groupProperty("t401TglJamCetakWO", "t401TglJamCetakWO")
                groupProperty("t401NamaSA", "t401NamaSA")
                groupProperty("t401TglJamJanjiPenyerahan", "t401TglJamJanjiPenyerahan")
            }

        }
        def rows = []

        results.each {
            String startParams = "-";
            String stopParams = "-";
            String finalInspect = "-";
            def staMasalah=false;
            def finalIns = FinalInspection.findByReception(Reception.findByT401NoWO(it.t401NoWO))
            def jpb = JPB.findByReceptionAndStaDel(Reception.findByT401NoWO(it.t401NoWO),"0")
            def cek = Masalah.findByJpb(jpb)
            if(cek){
                staMasalah=true;
            }
            def startAct = Actual.findByReceptionAndStatusActualAndStaDel(Reception.findByT401NoWO(it.t401NoWO),StatusActual.findByM452Id("1"),"0")
            if(startAct){
                startParams = df.format(startAct.t452TglJam);
            }

            def stopAct = Actual.findByReceptionAndStatusActualAndStaDel(Reception.findByT401NoWO(it.t401NoWO),StatusActual.findByM452Id("4"),"0")
            if(stopAct){
                stopParams = df.format(stopAct.t452TglJam);
            }

            if(finalIns){
                finalInspect = df.format(finalIns.t508TglJamSelesai);
            }
            def tampil=true
            if(jpb){
                if (params."sCriteria_staProblem") {
                    def find = Masalah.findByJpbAndCompanyDealer(jpb,jpb?.companyDealer)
                    if(params."sCriteria_staProblem"=="Solved"){
                        if(find){
                            if(find?.t501StaSolved=="0"){
                                tampil=false
                            }
                        }
                    }else{
                        if(find){
                            if(find?.t501StaSolved=="1"){
                                tampil=false
                            }
                        }
                    }
                }

            }

            if(tampil==true && staMasalah==true){
                jumlah+=1
                rows << [

                        t401NoWO: it.t401NoWO,
                        noPolisi: it.noPolisi?it.noPolisi.fullNoPol:"",

                        t401TanggalWO : it.t401TglJamCetakWO ? df.format(it.t401TglJamCetakWO) : "-",

                        t401NamaSA : it.t401NamaSA,

                        t401TglJamJanjiPenyerahan : it.t401TglJamJanjiPenyerahan ? df.format(it.t401TglJamJanjiPenyerahan) : "",

                        start : startParams,

                        stop : stopParams,

                        finalInspection : finalInspect

                ]

            }
        }



        [sEcho: params.sEcho, iTotalRecords: jumlah, iTotalDisplayRecords: jumlah, aaData: rows]

    }

    def datatablesSub1List(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            eq("t401NoWO", params.t401NoWO)

//            operation{
//                kategoriJob{
//                    eq("m055KategoriJob","BP",[ignoreCase: true])
//                }
//            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    namaJob: it?.operation ? it.operation.m053NamaOperation : "",

                    t401NoWO: it.t401NoWO

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSub2List(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            eq("t401NoWO", params.t401NoWO)

        }

        def rows = []

        int index = 0;
        results.each {
            String startParams = "-";
            String stopParams = "-";
            def lastProb = null,lastResp = null, jumProb = null,getForeman = "",getTeknisi = "",getSTatus = ""
            def staMasalah = false
            def jpb=JPB.findByReceptionAndStaDel(it,"0")
            if(jpb){
                getTeknisi = jpb?.namaManPower?.manPowerDetail?.m015Inisial
                for(find in jpb?.namaManPower?.userProfile?.roles){
                    if(find.name.equalsIgnoreCase("foreman")){
                        getForeman = jpb?.namaManPower?.manPowerDetail?.m015Inisial
                    }
                }
                def masalah = Masalah.findAllByJpb(jpb)
                if(masalah){
                    getSTatus = masalah.t501StaSolved=="1" ? "Solved" : "Not Solved"
                    jumProb = masalah.size()
                    staMasalah = true
//                    lastProb = df.format(masalah?.t501TglJamKirim)
                    def respon = Respon.findByMasalah(masalah.first())
                    if(respon){
                        lastResp = df.format(respon.last().t505TglUpdate)
                    }
                }
            }
            if(staMasalah){
                index+=1
                rows << [

                        id: it.id,

                        proses: it.stall ? it?.stall?.namaProsesBP?.m190NamaProsesBP : "",

                        lastProblem : lastProb,

                        lastRespon : lastResp,

                        jumlahProblem : jumProb,

                        teknisi : getTeknisi,

                        foreman : getForeman,

                        status : getSTatus,

                        t401NoWO: it.t401NoWO
                ]

            }
        }

        [sEcho: params.sEcho, iTotalRecords:  index, iTotalDisplayRecords: index, aaData: rows]

    }

    def datatablesSub3List(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            eq("t401NoWO", params.t401NoWO)
        }

        def rows = []


        results.each {
            def jpb = JPB.findByReceptionAndStaDel(it,"0")
            def getSender = "",tanggalKir = null,getDesc = "",staMasalah= false,tangSolved = "", staSolved = ""
            if(jpb){
                getSender = jpb.namaManPower.manPowerDetail.m015Inisial
                def masalah = Masalah.findByJpb(jpb)
                if(masalah){
                    staMasalah=true
                    getDesc = masalah?.actual?.t452Ket
                    tangSolved = masalah?.t501TglSolved
                    staSolved = masalah?.t501StaSolved=="1" ? "Solved" : "Not Solved"
//                    tanggalKir = df.format(masalah?.t501TanggalKirim )
                }
            }
            if(staMasalah){
                rows << [

                        id: it.id,

                        rolePengirim: "Foreman",

                        namaPengirim : getSender,

                        tanggalKirim : tanggalKir,

                        deskMasalah : getDesc,

                        tanggalSolved : tangSolved,

                        status: staSolved,

                        t401NoWO: it.t401NoWO

                ]

            }
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSub4List(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            eq("t401NoWO", params.t401NoWO)
        }

        def rows = []

        results.each {
            def jpb=JPB.findByReceptionAndStaDel(it,"0")
            def actual=Actual.findByReceptionAndStaDel(it,"0")
            def getRole = "",getNama=""
            if(jpb && actual){
                def masalah=Masalah.findByJpbAndActual(jpb,actual)
                if(masalah){
                    def respon=Respon.findAllByJpbAndActualAndMasalah(jpb,actual,masalah)
                    if(respon){
                        respon.each {
                            def nama=NamaManPower.findByUserProfileAndStaDel(it.userProfile,"0")
                            if(nama){
                                getNama = nama.manPowerDetail.m015Inisial
                                                                }
                            if(it.userProfile){
                                for(find in it.userProfile.roles){
                                    getRole+=find?.name+" "
                                }
                            }

                            rows << [

                                    id: it.id,

                                    roleRespon: getRole,

                                    namaResponder : getNama,

                                    tanggalRespon : it.t505TglUpdate,

                                    respon : it.t505Respon,

                                    solusi : it.t505StaSolusi=="1" ? "Ya" : "Tidak"

                            ]
                        }
                    }
                }
            }

        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    private File createReport(def list) {
        DateFormat dfJam=new SimpleDateFormat("dd-MM-yyyy HH:mm")

        WorkbookSettings workbookSettings = new WorkbookSettings()
        workbookSettings.locale = Locale.default

        def file = File.createTempFile('myExcelDocument', '.xls')
        file.deleteOnExit()

        WritableWorkbook workbook = Workbook.createWorkbook(file, workbookSettings)

        WritableFont fontTitle = new WritableFont(WritableFont.ARIAL, 12)
        fontTitle.setBoldStyle(WritableFont.BOLD)
        WritableCellFormat format = new WritableCellFormat(fontTitle)

        WritableFont font = new WritableFont(WritableFont.ARIAL, 10)
        WritableCellFormat formatIsi = new WritableCellFormat(font)

        def row = 0
        WritableSheet sheet = workbook.createSheet('MySheet', 0)

        sheet.addCell(new Label(0, row, "No.", format))
        sheet.addCell(new Label(1, row, "Nomor WO", format))
        sheet.addCell(new Label(2, row, "Nomor Polisi", format))
        sheet.addCell(new Label(3, row, "Tanggal WO", format))
        sheet.addCell(new Label(4, row, "Model Kendaraan", format))
        sheet.addCell(new Label(5, row, "Nama SA", format))
        sheet.addCell(new Label(6, row, "Waktu Penyerahan", format))
        sheet.addCell(new Label(7, row, "Nama Job", format))
        sheet.addCell(new Label(8, row, "Nama Foreman", format))
        sheet.addCell(new Label(9, row, "Nama Teknisi", format))
        sheet.addCell(new Label(10, row, "Proses", format))
        sheet.addCell(new Label(11, row, "Nama Pengirim", format))
        sheet.addCell(new Label(12, row, "Tanggal Kirim", format))
        sheet.addCell(new Label(13, row, "Deskripsi Masalah", format))
        sheet.addCell(new Label(14, row, "Jenis Masalah", format))
        sheet.addCell(new Label(15, row, "Catatan Tambahan", format))
        sheet.addCell(new Label(16, row, "Status Butuh RSA", format))
        sheet.addCell(new Label(17, row, "Bisa Dikerjakan Sendiri", format))
        sheet.addCell(new Label(18, row, "Perlu Tambahan Waktu", format))
        sheet.addCell(new Label(19, row, "Butuh Tambahan Job", format))
        sheet.addCell(new Label(20, row, "Butuh Parts/Material", format))
        sheet.addCell(new Label(21, row, "Butuh Pause Job", format))
        sheet.addCell(new Label(22, row, "Butuh Cat Ulang", format))
        sheet.addCell(new Label(23, row, "Tagih Kepada", format))
        sheet.addCell(new Label(24, row, "Vendor Cat", format))
        sheet.addCell(new Label(25, row, "Status Kendaraan", format))
        sheet.addCell(new Label(26, row, "Role Respon", format))
        sheet.addCell(new Label(27, row, "Responder", format))
        sheet.addCell(new Label(28, row, "Tanggal Respon", format))
        sheet.addCell(new Label(29, row, "Deskripsi Respon", format))
        sheet.addCell(new Label(30, row, "Status Solusi", format))

        list.each {
            def jpb = JPB.findByReceptionAndStaDel(it,"0")
            def actual = Actual.findByReceptionAndJpbAndStaDel(it,jpb,"0")
            def masalah = Masalah.findByJpbAndActual(jpb,actual)
            def respon = Respon.findByJpbAndActualAndMasalahAnd(jpb,actual,masalah)
            if(masalah){
                def foreman = "",getRoles="",getResponder=""
                if(jpb?.namaManPower?.userProfile){
                    for(find in jpb?.namaManPower?.userProfile?.roles){
                        if(find.name.equalsIgnoreCase("foreman")){
                            foreman=jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"
                        }
                    }
                }
                if(respon?.userProfile){
                    def nama=NamaManPower.findByUserProfileAndStaDel(respon?.userProfile,"0")
                    if(nama){
                        getResponder = nama?.t015NamaBoard
                    }
                    for(find in respon?.userProfile?.roles){
                        getRoles+=find.name+" "
                    }
                }
                row++
                sheet.addCell(new Label(0, row, ""+row, formatIsi))
                sheet.addCell(new Label(1, row, ""+it?.t401NoWO, formatIsi))
                sheet.addCell(new Label(2, row, ""+it?.historyCustomerVehicle?.fullNoPol, formatIsi))
                sheet.addCell(new Label(3, row, ""+it?.t401TglJamCetakWO ? dfJam.format(it?.t401TglJamCetakWO) : "", formatIsi))
                sheet.addCell(new Label(4, row, ""+it?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName, formatIsi))
                sheet.addCell(new Label(5, row, ""+it?.t401NamaSA, formatIsi))
                sheet.addCell(new Label(6, row, ""+it?.t401TglJamJanjiPenyerahan, formatIsi))
                sheet.addCell(new Label(7, row, ""+it?.operation?.m053NamaOperation, formatIsi))
                sheet.addCell(new Label(8, row, ""+foreman, formatIsi))
                sheet.addCell(new Label(9, row, ""+it?.namaManPower ? it?.namaManPower?.t015NamaLengkap+" ("+it?.namaManPower?.manPowerDetail?.m015Inisial+")" : "", formatIsi))
                sheet.addCell(new Label(10, row, ""+jpb?.stall?.namaProsesBP?.m190NamaProsesBP, formatIsi))
                sheet.addCell(new Label(11, row, ""+actual?.namaManPower ? actual?.namaManPower?.t015NamaLengkap+" ("+actual?.namaManPower?.manPowerDetail?.m015Inisial+")" : "", formatIsi))
                sheet.addCell(new Label(12, row, ""+masalah?.t501TanggalKirim ? dfJam.format(masalah?.t501TanggalKirim) : "", formatIsi))
                sheet.addCell(new Label(13, row, ""+actual?.t452Ket, formatIsi))
                sheet.addCell(new Label(14, row, ""+masalah?.t501staProblemTeknis=="1"?"Teknis" : "Non Teknis", formatIsi))
                sheet.addCell(new Label(15, row, ""+masalah?.t501CatatanTambahan, formatIsi))
                sheet.addCell(new Label(16, row, ""+masalah?.t501StaButuhRSA=="1" ? "Ya" : "Tidak", formatIsi))
                sheet.addCell(new Label(17, row, ""+masalah?.t501StaBisaKrjSdri=="1" ? "Ya" : "Tidak", formatIsi))
                sheet.addCell(new Label(18, row, ""+masalah?.t501StaBthTambahWkt=="1" ? "Ya" : "Tidak", formatIsi))
                sheet.addCell(new Label(19, row, ""+masalah?.t501StaBthJob=="1" ? "Ya" : "Tidak", formatIsi))
                sheet.addCell(new Label(20, row, ""+masalah?.t501StaBthParts=="1" ? "Ya" : "Tidak", formatIsi))
                sheet.addCell(new Label(21, row, ""+masalah?.t501StaButuhPause=="1" ? "Ya" : "Tidak", formatIsi))
                sheet.addCell(new Label(22, row, ""+masalah?.t501StaButuhCat, formatIsi))
                sheet.addCell(new Label(23, row, ""+masalah?.t501StaTagihKepada, formatIsi))
                sheet.addCell(new Label(24, row, ""+masalah?.vendorCat?.m191Nama, formatIsi))
                sheet.addCell(new Label(25, row, ""+masalah?.statusKendaraan?.m999NamaStatusKendaraan, formatIsi))
                sheet.addCell(new Label(26, row, ""+getRoles, formatIsi))
                sheet.addCell(new Label(27, row, ""+getResponder, formatIsi))
                sheet.addCell(new Label(28, row, ""+respon?.t505TglUpdate, formatIsi))
                sheet.addCell(new Label(29, row, ""+respon?.t505Respon, formatIsi))
                sheet.addCell(new Label(30, row, ""+respon?.t505StaSolusi=="1" ? "Ya" : "Tidak", formatIsi))

            }

        }

        workbook.write()
        workbook.close()
        return file
    }


}