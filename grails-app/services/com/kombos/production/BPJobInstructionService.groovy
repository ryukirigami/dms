package com.kombos.production

import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.NamaProsesBP
import com.kombos.administrasi.Operation
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.maintable.StatusActual
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.Progress

import java.text.DateFormat
import java.text.SimpleDateFormat

class BPJobInstructionService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("staSave","0")
            ge("countJPB",0)
            eq("companyDealer",params?.companyDealer)
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","bp", [ignoreCase: true])
                }
            }

            if(params."sCriteria_noWO"){
                ilike("t401NoWO","%" + (params."sCriteria_noWO" as String) + "%")
            }

            if(params."sCriteria_noPol"){
                historyCustomerVehicle{
                    ilike("fullNoPol","%" + (params."sCriteria_noPol" as String) + "%")
                }
            }

            if (params."sCriteria_Tanggal" && params."sCriteria_Tanggalakhir") {
                ge("t401TglJamCetakWO", params."sCriteria_Tanggal")
                lt("t401TglJamCetakWO", params."sCriteria_Tanggalakhir" + 1)
            }
            if (params."sCriteria_staAmbilWO") {
                if(params."sCriteria_staAmbilWO"=="Belum Ambil WO"){
                    eq("t401StaAmbilWO","0")
                }
                if(params."sCriteria_staAmbilWO"=="Sudah Ambil WO"){
                    eq("t401StaAmbilWO","1")
                }
            }
            if (params."sCriteria_staCarryOver") {
                if(params."sCriteria_staCarryOver"=="Ya"){
                    eq("t401StaTinggalMobil","1")
                }else{
                    eq("t401StaTinggalMobil","0")
                }
            }
            order("dateCreated","desc")
        }

        def rows = []
        int index = 0;
        results.each {
            String startParams = "-";
            String stopParams = "-";
            def finalInspect = "-";
            String teknisiParams = "-";
            def reception = it
                //println("cetak wo "+it.t401NoWO)
            def finalIns = FinalInspection.findByReception(it)
                //println("total Fi "+finalIns?.reception)
           def rec=Reception.findByT401NoWOAndStaDel(it.t401NoWO,"0")
            def x = JPB.createCriteria()
            def jpb = x.list {
                eq("staDel","0")
                eq("reception",it)
                order("t451TglJamPlan")
            }
            def jobRCP = JobRCP.findAllByReceptionAndStaDel(it,"0")
            def startAct = Actual.findByReceptionAndStatusActualAndStaDel(it,StatusActual.findByM452Id("1"),"0")
            if(startAct){
                startParams = df.format(startAct?.t452TglJam);
            }

            def stopAct = Actual.findByReceptionAndStatusActualAndStaDel(it,StatusActual.findByM452Id("4"),"0")
            if(stopAct){
                stopParams = df.format(stopAct.t452TglJam);
            }
            if(finalIns){
                finalInspect = finalIns.t508TglJamSelesai.format("dd/MM/yyyy HH:mm");
                //println("cetak inspection "+finalInspect)
            }

            def z=JPB.createCriteria()
            def jpb2=z.list {
                eq("staDel","0")
                eq("reception",rec)
                order("t451TglJamPlan")
            }


            if(jpb2){
                teknisiParams=""
                int i=0;
                jpb2.each {
                    i++;
                    teknisiParams+=it?.namaManPower?.t015NamaBoard;
                    if(i<jpb2.size()){
                        teknisiParams+=", "
                    }

                }
            }

            if(jpb){
//               teknisiParams=""
                int a=0;
                jpb.each {
                    a++;
                    if(a==1){
                        startParams = it?.t451TglJamPlan?.format("dd/MM/yyyy HH:mm")
                    }
//                    teknisiParams+=it?.namaManPower?.t015NamaBoard;
//                    if(a<jpb.size()){
//                        teknisiParams+=", "
//                    }
                    if(a==jpb.size()){
                        stopParams = it?.t451TglJamPlanFinished?.format("dd/MM/yyyy HH:mm")
                    }
                }
            }
            boolean tampil=true
            def cek = []
            if (params."sCriteria_staProblemFinding") {
                if(jpb?.size()>0){
                    def masalah = Masalah.findByJpb(jpb?.last())
                    if(params."sCriteria_staProblemFinding"=="Teknisi"){
                        if(masalah==null || (masalah && masalah?.t501staProblemTeknis!="1")){
                            tampil=false
                        }
                    }else{
                        if(masalah==null || (masalah && masalah?.t501staProblemTeknis!="0")){
                            tampil=false
                        }
                    }
                }
            }


            if (params."sCriteria_staFinalInspection") {
                if(params."sCriteria_staFinalInspection"=="Sudah Final Inspection"){
                    if(finalIns==null){
                        tampil=false;
                    }
                }else{
                    if(finalIns!=null){
                        tampil=false;
                    }
                }
            }
            if (params."sCriteria_staJobTambah") {
                if(params."sCriteria_staJobTambah"=="Ya"){
                    tampil=false
                    if(jobRCP==null){
                        cek << "0"
                    }else{
                        jobRCP.each {
                            if(it.t402StaPekerjaanTambahanCust!="1"){
                                cek << "0"
                            }else{
                                cek << "1"
                            }
                        }
                    }
                }else{
                    if(jobRCP==null){
                        cek << "0"
                    }else{
                        jobRCP.each {
                            if(it.t402StaPekerjaanTambahanCust!="0"){
                                cek << "0"
                            }else{
                                cek << "1"
                            }
                        }
                    }

                }
            }
            def re=FinalInspection.findByReception(reception)
            //println("cetak FI "+re?.t508TglJamSelesai)
            if(tampil || cek.contains("1")){
                index++;

                rows << [

                        t401NoWO: it?.t401NoWO,

                        noPolisi: it.historyCustomerVehicle ? it?.historyCustomerVehicle?.fullNoPol : "",

                        t401TanggalWO : it.t401TglJamCetakWO ? it.t401TglJamCetakWO.format("dd/MM/yyyy HH:mm") : it?.dateCreated?.format("dd/MM/yyyy HH:mm"),

                        t401NamaSA : it?.t401NamaSA ? User.findByUsernameAndStaDel(it?.t401NamaSA,"0")?.fullname : "",

                        t401TglJamJanjiPenyerahan :it?.t401TglJamJanjiPenyerahan ? it.t401TglJamJanjiPenyerahan.format("dd/MM/yyyy HH:mm") : "",

                        start : startParams!="" ? startParams : "-",

                        stop : stopParams!="" ? stopParams : "-",

                        finalInspection : finalInspect!="" ? finalInspect:"-",

                        teknisi : teknisiParams!="" ? teknisiParams : "-",

                        ambilWO : it?.t401StaAmbilWO


                ]

            }
        }

        def hasil = []
        int mulai = params.iDisplayStart.toLong()
        for(int a = mulai;a< (mulai + 10 <= rows.size() ? mulai+10 : rows.size()) ;a++){
            hasil << rows[a]
        }

        [sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: hasil]

    }

    def datatablesSub1List(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
        }

        def rows = []
        int index=0
        results.each {
            String startParams = "-";
            String stopParams = "-";
            def startAct = Actual.findByReceptionAndOperationAndStatusActualAndStaDel(it.reception,it.operation,StatusActual.findByM452Id("1"),"0")
            if(startAct){
                startParams = df.format(startAct.t452TglJam);
            }

            def stopAct = Actual.findByReceptionAndOperationAndStatusActualAndStaDel(it.reception,it.operation,StatusActual.findByM452Id("4"),"0")
            if(stopAct){
                stopParams = df.format(stopAct.t452TglJam);
            }
            index++
            rows << [

                    id: it.id,

                    t401NoWO: it.reception?.t401NoWO,

                    idJob : it?.operation ? it?.operation?.id : "",

                    namaJob: it.operation ? it.operation.m053NamaOperation : "",

                    startJob : startParams,

                    stopJob : stopParams,

                    staTambah : it.t402StaPekerjaanTambahanCust

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  index, iTotalDisplayRecords: index, aaData: rows]

    }

    def datatablesSub2List(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
            operation{
                eq("id",Long.parseLong(params.idJob))
            }
            maxResults(1);
        }

        def rows = []
        int index=0
        results.each {
            Long idJpb= null
            String startParams = "-";
            String stopParams = "-";
            String qc="-";
            String getTeknisi="-";
            String getForeman="-";
            def startAct = Actual.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452Id("1"),"0")
            if(startAct){
                startParams = df.format(startAct?.t452TglJam);
            }
            def stopAct = Actual.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452Id("4"),"0")
            if(stopAct){
                stopParams = df.format(stopAct?.t452TglJam);
            }
            def jpb = JPB.findAllByReceptionAndStaDel(it?.reception,"0")
            if(jpb){
                int a=0;
                getTeknisi="";
                jpb.each {
                    idJpb=it?.id
                    a++;
                    getTeknisi+=it?.namaManPower?.t015NamaBoard
                    if(a<jpb.size()){
                        getTeknisi+=" , "
                    }
                    def grup = it?.namaManPower?.groupManPower
                    if(grup){
                        getForeman = grup?.namaManPowerForeman?.t015NamaBoard
                    }
                }
            }
            def qcontrol = InspectionQC.findByReception(it?.reception)
            if(qcontrol){
                if(qcontrol?.t507StaLolosQC=="1"){
                    qc=df.format(qcontrol?.t507TglJamSelesai)
                }
            }
            index++
            def temp = it
            def prosess = NamaProsesBP?.findAll()

            prosess?.each {
                rows << [

                        id: temp?.id,

                        idJpb: idJpb,

                        t401NoWO: temp?.reception?.t401NoWO,

                        proses : it?.m190NamaProsesBP,

                        start : startParams,

                        qualityControl : qc,

                        stop : stopParams,

                        foreman : getForeman,

                        teknisi: getTeknisi

                ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  index, iTotalDisplayRecords: index, aaData: rows]

    }

    def datatablesSub3List(def params) {
//        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
//        def c = Actual.where {
//            eq("staDel","0")
//            reception{
//                eq("t401NoWO", params.t401NoWO)
//            }
//        }
//        def results = c.list().unique{it.namaManPower}
//
//        def rows = []
//        int index=0
//        results.each {
//            String startParams = "-";
//            String stopParams = "-";
//            def staRedo = "";
//            def finalIns = FinalInspection.findByReception(it?.reception)
//            if(finalIns){
//                staRedo = finalIns?.t508StaRedo;
//            }
//            def startAct = Actual.findByReceptionAndStatusActualAndStaDel(it.reception,StatusActual.findByM452Id("1"),"0")
//            if(startAct){
//                startParams = df.format(startAct.t452TglJam);
//            }
//            def stopAct = Actual.findByReceptionAndStatusActualAndStaDel(it.reception,StatusActual.findByM452Id("4"),"0")
//            if(stopAct){
//                stopParams = df.format(stopAct.t452TglJam);
//            }
//            index++
//            rows << [
//
//                    id: it.id,
//
//                    idManPower : it.namaManPower ? it?.namaManPower?.id : "",
//
//                    teknisi : it.namaManPower?.t015NamaBoard ? it?.namaManPower?.t015NamaBoard :"-",
//
//                    start : startParams,
//
//                    stop : stopParams,
//
//                    t401NoWO: it?.reception?.t401NoWO,
//
//                    staRedo : staRedo
//
//            ]
//        }
//
//        [sEcho: params.sEcho, iTotalRecords:  index, iTotalDisplayRecords: index, aaData: rows]




        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        //println "datatablesSubList " + params.t401NoWO
        def c = JPB.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(params.t401NoWO){
                reception{
                    eq("t401NoWO", params.t401NoWO)
                }
            }
        }

        def rows = []

        results.each {
            String startParams = "-";
            String stopParams = "-";

            def startAct = Actual.findByReceptionAndStatusActualAndStaDel(it.reception,StatusActual.findByM452Id("1"),"0")
            if(startAct){
                startParams = df.format(startAct.t452TglJam);
            }
            def stopAct = Actual.findByReceptionAndStatusActualAndStaDel(it.reception,StatusActual.findByM452Id("4"),"0")
            if(stopAct){
                stopParams = df.format(stopAct.t452TglJam);
            }
            rows << [

                    id: it.id,
                    idManPower : it.namaManPower ? it?.namaManPower?.id : "",
                    start  : startParams ,

                    stop : stopParams,

                    teknisi : it.namaManPower?.t015NamaBoard,

                    t401NoWO : params.t401NoWO,

                    idJob : params.idJob

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]



    }

    def datatablesSub4List(def params) {
        def idAktual = "5,6"
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        //println "datatablesSubSubSubList teknisi : " + params.teknisi
        def nmp = NamaManPower.findByT015NamaBoard(params.teknisi)
        //println "datatablesSubSubSubList job : " + params.idJob
        def c = Actual.createCriteria()
//        def results = c.list () {
//            namaManPower{
//                eq("id", params.idManPower.toLong())
//            }
//            reception{
//                eq("t401NoWO", params.t401NoWO)
//            }
//            eq("staDel","0")
//        }

        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
//            operation{
//                eq("id",params.idJob as Long)
//            }
//            eq("namaManPower",nmp)
            order("dateCreated","asc")
        eq("staDel","0")
        }
        //println("cetak aktual "+results.size())
        def rows = []
        int index=0;
        //println results
        results.each {

            def problem = "-"
            if(it.t452staProblemTeknis == "1"){
                problem = "Teknis"
            }else if(it.t452staProblemTeknis == "0"){
                problem = "Non Teknis"
            }
           else{
                problem="-"
            }

            def progress=Progress.findByReception(it.reception)
            def masalah = Masalah.findByActual(it)
            index++;
            rows << [
                        id: it?.id,
                        t401NoWO: it?.reception?.t401NoWO,
                        status: it?.statusActual ? (idAktual?.indexOf(it?.statusActual?.m452Id)>-1 ? "Problem" : it?.statusActual?.m452StatusActual ) : "",
                        tanggalStatus : it?.t452TglJam ? it?.t452TglJam.format("dd/MM/yyyy HH:mm") : "",
                        keterangan : it?.t452Ket,
                        problem : problem,
                        foreman : masalah?.t501NamaKirim ? masalah?.t501NamaKirim : "-",



//                    noWo : params.t401NoWO,
//                    idJob : params.idJob,
//                    teknisi : params.teknisi

                ]

        }

        [sEcho: params.sEcho, iTotalRecords:  index, iTotalDisplayRecords: index, aaData: rows]

    }



}