package com.kombos.production

import com.kombos.administrasi.FlatRate
import com.kombos.approval.AfterApprovalInterface
import com.kombos.baseapp.AppSettingParam
import com.kombos.board.JPB
import com.kombos.customercomplaint.Complaint
import com.kombos.maintable.ApprovalT770
import com.kombos.parts.Konversi
import com.kombos.parts.StatusApproval
import com.kombos.reception.PermintaanCustomer
import com.kombos.reception.Reception
import com.kombos.woinformation.ActualRateTeknisi
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import com.kombos.woinformation.Prediagnosis

import java.text.DateFormat
import java.text.SimpleDateFormat

class WoDetailGrService implements AfterApprovalInterface{

    boolean transactional = false
    def datatablesUtilService
    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def konversi = new Konversi()
    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Complaint.createCriteria()
        def results = c.list () {
            eq("staDel","0")


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def findData(params){
        //println "masuk findData" + params.noWO
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def receptionInstance = Reception.createCriteria()
        def results = receptionInstance.list() {
            eq("companyDealer",params?.companyDealer)
//            liki("t401NoWO",params.noWO)

            like("t401NoWO","%" + (params."noWO" as String) + "%")
            eq("staDel","0")
            eq("staSave","0")
        }

        def rows = []

        results.each {
            def instance=Prediagnosis.findByReception(it)
            def jpb=JPB.findByReception(it)
            def permintaanCus = PermintaanCustomer.findById(1)
            String getKeluhan = "";
            String getPermintaan = "";
            String getStall= "-";
            if(instance!=null){
                getKeluhan=instance?.t406KeluhanCust ? instance?.t406KeluhanCust :"-"
            }
            if(jpb){
                getStall=jpb?.stall?.m022NamaStall ? jpb?.stall?.m022NamaStall:"-"
            }
            if(permintaanCus){
                getPermintaan=permintaanCus?.permintaan ? permintaanCus?.permintaan :"-"
            }

            rows << [
                    id : it.id,

                    noPolAwal : it.historyCustomerVehicle.kodeKotaNoPol.m116ID,

                    noPolTengah : it.historyCustomerVehicle.t183NoPolTengah,

                    noPolAkhir : it.historyCustomerVehicle.t183NoPolBelakang,

                    t401NoWo : it.t401NoWO ,

                    noPolisi : it.historyCustomerVehicle.fullNoPol,

                    modelKendaraan : it.historyCustomerVehicle.fullModelCode.baseModel.m102NamaBaseModel,

                    namaStall : getStall,

                    tanggalWO : it.t401TanggalWO ? df.format(it.t401TanggalWO) : "",

                  //  waktuPenyerahan : it.t401TglJamJanjiPenyerahan ? df.format(it.t401TglJamJanjiPenyerahan) : "",
                    waktuPenyerahan :it?.t401TglJamJanjiPenyerahan? it?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm"): "-",

                    namaCustomer : it?.historyCustomer?.t182NamaDepan+" "+it?.historyCustomer?.t182NamaBelakang,
                   // namaCustomer : it.historyCustomer.fullNama,
                    alamatCustomer : it?.historyCustomer?.t182Alamat,

                    telponCustomer : it?.historyCustomer?.t182NoHp,

                    keluhan : getKeluhan,

                    permintaan : getPermintaan,

                    staObatJalan : it?.t401StaObatJalan


            ]
        }

        return rows
   }

    def getTableBiayaData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def subtotal = 0,total=0
        def rows = []
        def c = JobRCP.createCriteria()
        def results = c.list() {
            reception{
                eq("t401NoWO",params.noWO)
            }
            eq("staDel","0")
        }

        def hargaJob = 0
        results.each {
            total = 0
            hargaJob = it.t402HargaRp?:0

            total += hargaJob
            subtotal+=total
            rows << [
                    namaJobParts : it.operation.m053NamaOperation ,

                    hargaJob : konversi.toRupiah(hargaJob),

                    hargaParts : 0,

                    total : konversi.toRupiah(total)

            ]

        }
        def hargaParts=0
        def partrcp = PartsRCP.findAllByReceptionAndStaDel(Reception.findByT401NoWO(params.noWO),'0')
        if(partrcp){
            partrcp.each {
                hargaParts = it.t403TotalRp?:0
                total=0
                total += hargaParts
                subtotal+=total
                rows << [

                        namaJobParts : "Parts "+it.goods.m111Nama ,

                        hargaJob : 0,

                        hargaParts : konversi.toRupiah(hargaParts),

                        total : konversi.toRupiah(total)

                ]
            }
        }

        if(results){
            rows << [
                    namaJobParts : 'SUB TOTAL',

                    hargaJob : '',

                    hargaParts : '',

                    total: konversi.toRupiah(subtotal)

            ]

            def ppn = (10*subtotal)/100
            rows << [
                    namaJobParts : 'PPN (10%)',

                    hargaJob : ' ',

                    hargaParts : ' ',

                    total: ppn?konversi.toRupiah(ppn):0

            ]

            rows << [
                    namaJobParts : 'GRAND TOTAL',

                    hargaJob : " ",

                    hargaParts : ' ',

                    total: konversi.toRupiah(subtotal+ppn)

            ]

            rows << [
                    namaJobParts : 'BOOKING FEE (25%)',

                    hargaJob : '',

                    hargaParts : '',

                    total: ''

            ]

        }


        return rows
    }
    def getTableRateData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def rows2 = []

        def e = JobRCP.createCriteria()
        def resultsRate = e.list() {
            reception{
                eq("t401NoWO",params.noWO)
            }
            eq("staDel","0")
        }

        String tek = ""

        resultsRate.each {
            //cari Inisialnya
//            def jpb = JPB.findAllByReceptionAndStaDel(it?.reception, "0")*.namaManPower?.userProfile?.t001Inisial?.unique()
            def jpb = JPB.findAllByReceptionAndStaDel(it?.reception, "0")*.namaManPower?.t015NamaBoard?.unique()
            def cFr = JPB.findByReceptionAndStaDel(it?.reception, "0")
            def actualRate = ActualRateTeknisi.findByReceptionAndOperationAndNamaManPower(it.reception,it.operation,cFr.namaManPower)
            Double findRate = null

            jpb.each {
                tek = jpb?.toString()?.substring(1,jpb?.toString()?.length() - 1)
            }

            if(actualRate){
                findRate = actualRate?.t453ActualRate
            }else{
                findRate = FlatRate.findByOperation(it.operation)?.t113FlatRate
            }
            rows2 << [

                    id : it.id,
                    namaJob : it?.operation?.m053NamaOperation ,

                    teknisi : tek,

                    rate : findRate,

                    noWo : params.noWO

            ]
        }

        return rows2
    }
    def save(params) {
        return "sukses"
    }
    def afterApproval(String fks,
                      StatusApproval staApproval,
                      Date tglApproveUnApprove,
                      String alasanUnApprove,
                      String namaUserApproveUnApprove) {
        fks.split(ApprovalT770.FKS_SEPARATOR).each {
            if(staApproval==StatusApproval.APPROVED){
                def reception = Reception.findByT401NoWO(it)
                def ubhData = Reception.get(reception.id)
                ubhData?.t401TglStaObatJalan = datatablesUtilService?.syncTime()
                ubhData?.t401StaObatJalan = "1"
                ubhData?.save(flush: true)
            }
        }
    }
}
