package com.kombos.production

import com.kombos.administrasi.ManPower
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.reception.Reception

class ForemanService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params, def session) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def listJobForeMan = ManPower.createCriteria().list {
            eq("staDel","0");
            ilike("m014JabatanManPower","%foreman%")
        }
        def c = NamaManPower.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)
            eq("staDel","0")
            if(params.sCriteria_nama){
                ilike("t015NamaLengkap","%"+params.sCriteria_nama+"%")
            }

            manPowerDetail{
                if(listJobForeMan.size()>0){
                    inList("manPower",listJobForeMan)
                }else{
                    eq("id",-10000.toLong())
                }
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        def index=0;
        results.each {
            String getStatus = "Available",getKeterangan = "";
            def reception = Reception.findByNamaManPower(it)
            def finalInspection = FinalInspection.findByReception(reception)
            def inspectQC = InspectionQC.findByReception(reception)
            def idr = IDR.findByReception(reception)
            if(finalInspection){
                if(finalInspection.t508TglJamMulai==null || finalInspection.t508TglJamSelesai==null){
                    if(finalInspection.t508TglJamMulai!=null || finalInspection.t508TglJamSelesai!=null){
                        getStatus = "Busy";
                        getKeterangan += "Final Inspection";
                    }
                }
            }
            if(inspectQC){
                    if(inspectQC.t507TglJamMulai==null || inspectQC?.t507TglJamSelesai==null){
                        if(inspectQC.t507TglJamMulai!=null || inspectQC?.t507TglJamSelesai!=null){
                            getStatus = "Busy";
                            if(getKeterangan.length()>1){
                                getKeterangan+=" , "
                            }
                            getKeterangan += "Quality Control";
                        }
                    }
            }
            if(idr){
                if(idr.t506TglJamMulai==null || idr.t506TglJamSelesai==null){
                    if(idr.t506TglJamMulai!=null || idr.t506TglJamSelesai!=null){
                        if(getKeterangan.length()>1){
                            getKeterangan+=" , "
                        }
                        getStatus = "Busy";
                        getKeterangan += "Quality Control";
                    }
                }
            }
            rows << [

                    nama : it.t015NamaLengkap,

                    initial : it.t015NamaBoard,

                    status : getStatus,

                    keterangan : getKeterangan

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

}