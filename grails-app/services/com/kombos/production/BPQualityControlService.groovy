package com.kombos.production

import com.kombos.administrasi.NamaProsesBP
import com.kombos.baseapp.AppSettingParam
import com.kombos.board.JPB
import com.kombos.maintable.StatusActual
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class BPQualityControlService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        def ambilWO = "semua"
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            eq("staDel","0")
            eq("staSave","0")
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","bp", [ignoreCase: true])
                }
            }

            if(params."sCriteria_noWO"){
                ilike("t401NoWO","%" + (params."sCriteria_noWO" as String) + "%")
            }

            if(params."sCriteria_noPol"){
                historyCustomerVehicle{
                    ilike("fullNoPol","%" + (params."sCriteria_noPol" as String) + "%")
                }
            }

            if (params."sCriteria_Tanggal" && params."sCriteria_Tanggalakhir") {
                ge("t401TanggalWO", params."sCriteria_Tanggal")
                lt("t401TanggalWO", params."sCriteria_Tanggalakhir" + 1)
            }
            if (params."sCriteria_sa") {
                def recep=Reception.findByT401NoWO(params."sCriteria_sa")
                eq("t401NamaSA",recep?.t401NamaSA)
            }
            gt("countJPB",0.toInteger())
            order("t401NoWO")
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t401NoWO", "t401NoWO")
                groupProperty("historyCustomerVehicle", "noPolisi")
                groupProperty("t401TanggalWO", "t401TanggalWO")
                groupProperty("t401NamaSA", "t401NamaSA")
                groupProperty("t401TglJamJanjiPenyerahan", "t401TglJamJanjiPenyerahan")
            }
        }
        def rows = []
        int index=0
        results.each {
            String startParams = "-";
            String stopParams = "-";
            String finalInspect = "-";
            def actual = Actual.findAllByReceptionAndStaDel(Reception.findByT401NoWO(it.t401NoWO),"0")
            def finalIns = FinalInspection.findByReception(Reception.findByT401NoWO(it.t401NoWO))
            def jpb = JPB.findByReceptionAndStaDel(Reception.findByT401NoWO(it.t401NoWO),"0")
            if(actual){
                actual.each {
                    if(it?.statusActual?.m452StatusActual?.equalsIgnoreCase("Clock On")){
                        startParams = "";
                    }
                    if(it?.statusActual?.m452StatusActual?.equalsIgnoreCase("Clock Off")){
                        stopParams = "";
                    }
                }

            }
            if(finalIns){
                finalInspect = df.format(finalIns.t508TglJamSelesai);
            }
            def tampil=true

            if (params."sCriteria_staLolosQC") {
                def inspect = InspectionQC.findByReception(Reception.findByT401NoWO(it.t401NoWO))
                if(params."sCriteria_staLolosQC"=="Lolos QC"){
                    if(inspect==null || (inspect?.t507StaLolosQC && inspect?.t507StaLolosQC!="1")){
                        tampil=false;
                    }
                }else{
                    if(inspect==null || (inspect?.t507StaLolosQC && inspect?.t507StaLolosQC!="0")){
                        tampil=false;
                    }
                }
            }

            if (params."sCriteria_staQualityControl") {
                def inspect = InspectionQC.findByReception(Reception.findByT401NoWO(it.t401NoWO))
                if(params."sCriteria_staQualityControl"=="Sudah Quality Control (QC)"){
                    if(inspect==null){
                        tampil=false;
                    }
                }else{
                    if(inspect!=null){
                        tampil=false;
                    }
                }
            }

            if (params."sCriteria_staFinalInspection") {
                def finalIn = FinalInspection.findByReception(Reception.findByT401NoWO(it.t401NoWO))
                if(params."sCriteria_staFinalInspection"=="Sudah Final Inspection"){
                    if(finalIn==null){
                        tampil=false;
                    }
                }else{
                    if(finalIn!=null){
                        tampil=false;
                    }
                }
            }

            if (params."sCriteria_staLolosFI") {
                def finalIn = FinalInspection.findByReception(Reception.findByT401NoWO(it.t401NoWO))
                if(params."sCriteria_staLolosFI"=="Lolos"){
                    if(finalIn==null || (finalIn?.t508StaInspection && finalIn?.t508StaInspection!="1")){
                        tampil=false;
                    }
                }else{
                    if(finalIn==null || (finalIn?.t508StaInspection && finalIn?.t508StaInspection!="0")){
                        tampil=false;
                    }
                }
            }

            if (params."sCriteria_staRedoJob") {
                def finalIn = FinalInspection.findByReception(Reception.findByT401NoWO(it.t401NoWO))
                if(params."sCriteria_staRedoJob"=="Ada Redo"){
                    if(finalIn==null || (finalIn?.t508StaRedo && finalIn?.t508StaRedo!="1")){
                        tampil=false;
                    }
                }else{
                    if((finalIn?.t508StaRedo && finalIn?.t508StaRedo!="0") || finalIn==null){
                        tampil=false;
                    }
                }
            }

            if(jpb){
                if (params."sCriteria_staProblem") {
                    def find = Masalah.findByJpbAndActualAndCompanyDealer(jpb,actual.first(),jpb?.companyDealer)
                    if(params."sCriteria_staProblem"=="Solved"){
                        if(find){
                            if(find?.t501StaSolved=="0"){
                                tampil=false
                            }
                        }
                    }else{
                        if(find){
                            if(find?.t501StaSolved=="1"){
                                tampil=false
                            }
                        }
                    }
                }

            }

            if(tampil==true){
                index++
                rows << [

                        t401NoWO: it.t401NoWO,

                        noPolisi: it.noPolisi?it.noPolisi.fullNoPol:"",

                        t401TanggalWO : it.t401TanggalWO ? df.format(it.t401TanggalWO) : "",

                        t401NamaSA : it.t401NamaSA,

                ]

            }
        }



        [sEcho: params.sEcho, iTotalRecords:  results?.totalCount, iTotalDisplayRecords: results?.totalCount, aaData: rows,ambilWO : ambilWO]

    }

    def datatablesSub1List(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
//        def c = Reception.createCriteria()
//        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
//            eq("staDel","0")
//            eq("t401NoWO", params.t401NoWO)
//            //println "datatables sublist 1 "+params.t401NoWO
//        }

        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    namaJob: it.operation ? it.operation.m053NamaOperation : "-",

                    t401NoWO : it?.reception?.t401NoWO,

                    idJob : it.operation ? it.operation.id : ""

            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSub2List(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def x = 0
//        def c = Reception.createCriteria()
//        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
//            eq("staDel","0")
//            eq("t401NoWO", params.t401NoWO)
//            //println "datatables sublist 2 "+params.t401NoWO
//        }
//        def rows = []
//        results.each {
//            def find=false;
//            def getForeman= "";
//            def cek = it?.namaManPower?.userProfile
//
//            if(cek){
//                for(a in cek.roles){
//                    if(a.name.equalsIgnoreCase("foreman")){
//                        find=true;
//                    }
//                }
//            }
//
//
//            if(find){
//                getForeman = it?.namaManPower?.manPowerDetail?.m015Inisial
//            }
//            rows << [
//                    t401NoWO: it.t401NoWO,
//                    proses: it.stall ? it?.stall?.namaProsesBP?.m190NamaProsesBP : "",
//                    foreman: getForeman
//
//
//            ]
//        }



        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
//            operation{
//                eq("id",Long.parseLong(params.idJob))
//            }
            maxResults(1);
        }

        def rows = []
        int index=0
        results.each {
            Long idJpb= null
            String startParams = "-";
            String stopParams = "-";
            String qc="-";
            String getTeknisi="-";
            String getForeman="-";
            def startAct = Actual.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452Id("1"),"0")
            if(startAct){
                startParams = df.format(startAct?.t452TglJam);
            }
            def stopAct = Actual.findByReceptionAndStatusActualAndStaDel(it?.reception,StatusActual.findByM452Id("4"),"0")
            if(stopAct){
                stopParams = df.format(stopAct?.t452TglJam);
            }
            def jpb = JPB.findAllByReceptionAndStaDel(it?.reception,"0")
            if(jpb){
                int a=0;
                getTeknisi="";
                jpb.each {
                    idJpb=it?.id
                    a++;
                    getTeknisi+=it?.namaManPower?.t015NamaBoard
                    if(a<jpb.size()){
                        getTeknisi+=" , "
                    }
                    def grup = it?.namaManPower?.groupManPower
                    if(grup){
                        getForeman = grup?.namaManPowerForeman?.t015NamaBoard
                    }
                }
            }
            def qcontrol = InspectionQC.findByReception(it?.reception)
            if(qcontrol){
                if(qcontrol?.t507StaLolosQC=="1"){
                    qc=df.format(qcontrol?.t507TglJamSelesai)
                }
            }
            index++
            def temp = it
            def prosess = NamaProsesBP?.findAll()

            prosess?.each {
                rows << [

                        id: temp?.id,

                       //idJpb: idJpb,

                        t401NoWO: temp?.reception?.t401NoWO,

                        proses : it?.m190NamaProsesBP,
//
//                        start : startParams,
//
//                        qualityControl : qc,
//
//                        stop : stopParams,

                        foreman : getForeman,

                       // teknisi: getTeknisi

                ]
            }
        }

       [sEcho: params.sEcho, iTotalRecords:  index, iTotalDisplayRecords: index, aaData: rows]


      //  [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSub3QualityList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def c = InspectionQC.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
        }

        def rows = []


        results.each {
            rows << [

                    quality : "Quality Control",

                    start : it?.t507TglJamMulai ? df.format(it.t507TglJamMulai) : "",

                    stop : it?.t507TglJamSelesai ? df.format(it.t507TglJamSelesai) : "",

                    lolos : it?.t507StaLolosQC=="1" ? "Ya" : "Tidak",

                    alasan : it?.t507Catatan ? it?.t507Catatan : "",

                    redo : it?.t507M053_JobID_Redo && it?.t507RedoKeProses_M190_ID ? it?.t507M053_JobID_Redo?.m053NamaOperation+" - "+it?.t507RedoKeProses_M190_ID?.m053NamaOperation : "-"


            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSub3InspectionList(def params) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def c = IDR.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO", params.t401NoWO)
            }
        }

        def rows = []


        results.each {
            rows << [

                    inspect: "Ins. Drg. Repair",

                    start : it?.t506TglJamMulai ? df.format(it.t506TglJamMulai) : "-",

                    stop : it?.t506TglJamSelesai ? df.format(it.t506TglJamSelesai) : "-",

                    status : it?.t506StaIDR=="1" ? "OK" : "Not OK",

                    alasan : it?.t506AlasanIDR ? it?.t506AlasanIDR : "",

                    redo : it?.t506M053_JobID_Redo && it?.t506RedoKeProses_M190_ID ? it?.t506M053_JobID_Redo?.m053NamaOperation+" - "+it?.t506RedoKeProses_M190_ID?.m053NamaOperation : "-"
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


}