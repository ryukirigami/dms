package com.kombos.customerFollowUp



import com.kombos.baseapp.AppSettingParam

class PertanyaanFuService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = PertanyaanFu.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m803Id") {
                eq("m803Id", params."sCriteria_m803Id")
            }

            if (params."sCriteria_m803Pertanyaan") {
                ilike("m803Pertanyaan", "%" + (params."sCriteria_m803Pertanyaan" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m803Id: it.m803Id,

                    m803Pertanyaan: it.m803Pertanyaan,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PertanyaanFu", params.id]]
            return result
        }

        result.pertanyaanFuInstance = PertanyaanFu.get(params.id)

        if (!result.pertanyaanFuInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PertanyaanFu", params.id]]
            return result
        }

        result.pertanyaanFuInstance = PertanyaanFu.get(params.id)

        if (!result.pertanyaanFuInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PertanyaanFu", params.id]]
            return result
        }

        result.pertanyaanFuInstance = PertanyaanFu.get(params.id)

        if (!result.pertanyaanFuInstance)
            return fail(code: "default.not.found.message")

        try {
            result.pertanyaanFuInstance.delete(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def update(params) {
        PertanyaanFu.withTransaction { status ->
            def result = [:]

            def fail = { Map m ->
                status.setRollbackOnly()
                if (result.pertanyaanFuInstance && m.field)
                    result.pertanyaanFuInstance.errors.rejectValue(m.field, m.code)
                result.error = [code: m.code, args: ["PertanyaanFu", params.id]]
                return result
            }

            result.pertanyaanFuInstance = PertanyaanFu.get(params.id)

            if (!result.pertanyaanFuInstance)
                return fail(code: "default.not.found.message")

            // Optimistic locking check.
            if (params.version) {
                if (result.pertanyaanFuInstance.version > params.version.toLong())
                    return fail(field: "version", code: "default.optimistic.locking.failure")
            }

            def cek = PertanyaanFu.createCriteria().list {
                eq("m803Pertanyaan",params.m803Pertanyaan.trim(),[ignoreCase : true])
            }

            if(cek){
                for(find in cek){
                    if(find?.id != result.pertanyaanFuInstance.id){
                        return [ada : "ada" , instance : result.pertanyaanFuInstance]
                    }
                }
            }


            result.pertanyaanFuInstance.properties = params

            if (result.pertanyaanFuInstance.hasErrors() || !result.pertanyaanFuInstance.save())
                return fail(code: "default.not.updated.message")

            // Success.
            return result

        } //end withTransaction
    }  // end update()

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["PertanyaanFu", params.id]]
            return result
        }

        result.pertanyaanFuInstance = new PertanyaanFu()
        result.pertanyaanFuInstance.properties = params

        // success
        return result
    }

    def save(params) {
        def result = [:]
        def fail = { Map m ->
            if (result.pertanyaanFuInstance && m.field)
                result.pertanyaanFuInstance.errors.rejectValue(m.field, m.code)
            result.error = [code: m.code, args: ["PertanyaanFu", params.id]]
            return result
        }

        result.pertanyaanFuInstance = new PertanyaanFu(params)

        def cek = PertanyaanFu.createCriteria().list {
            eq("m803Pertanyaan",params.m803Pertanyaan.trim(),[ignoreCase : true])
        }

        if(cek){
            return [ada : "ada" , instance : result.pertanyaanFuInstance]
        }


        if (result.pertanyaanFuInstance.hasErrors() || !result.pertanyaanFuInstance.save(flush: true))
            return fail(code: "default.not.created.message")

        // success
        return result
    }

    def updateField(def params) {
        def pertanyaanFu = PertanyaanFu.findById(params.id)
        if (pertanyaanFu) {
            pertanyaanFu."${params.name}" = params.value
            pertanyaanFu.save()
            if (pertanyaanFu.hasErrors()) {
                throw new Exception("${pertanyaanFu.errors}")
            }
        } else {
            throw new Exception("PertanyaanFu not found")
        }
    }

}