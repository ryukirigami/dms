package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam

class SmsHistoryService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = RealisasiFU.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", params.companyDealer)

            metodeFu{
                eq("m801NamaMetodeFu","SMS")
            }

            if(params.statusFollowUp){
                followUp{
                    eq("t801StaBerhasilFU",params.statusFollowUp)
                }
            }
            if(params.inisialSA){
                eq("t802NamaSA_FU",params.inisialSA)
            }

            if(params.nomorWo){
                followUp{
                    reception{
                        eq("t401NoWO",params.nomorWo)
                    }
                }
            }
            if(params.nopol1 && params.nopol2 && params.nopol3){
                followUp{
                    reception{
                        historyCustomerVehicle{
                            kodeKotaNoPol{
                                eq("id",params.nopol1 as Long)
                            }
                        }
                    }
                }
                followUp{
                    reception{
                        historyCustomerVehicle{
                            eq("t183NoPolTengah",params.nopol2)
                        }
                    }
                }
                followUp{
                    reception{
                        historyCustomerVehicle{
                            eq("t183NoPolBelakang",params.nopol3)
                        }
                    }
                }
            }

            if (params."search_TanggalSms" && params."search_TanggalSmsAkhir") {
                ge("t802TglJamFU", params."search_TanggalSms")
                lt("t802TglJamFU", params."search_TanggalSmsAkhir" + 1)
            }
            if (params."search_TanggalService" && params."search_TanggalServiceAkhir") {
                or{
                    followUp{
                        reception{
                            ge("t401TglJamPenyerahan", params."search_TanggalService")
                            lt("t401TglJamPenyerahan", params."search_TanggalServiceAkhir" + 1)
                        }
                    }

                    followUp{
                        reception{
                            ge("t401TanggalWO", params."search_TanggalService")
                            lt("t401TanggalWO", params."search_TanggalServiceAkhir" + 1)
                        }
                    }
                }

            }

                order("t802TglJamFU","desc")

        }
        def rows = []
        results.each {
            def nopol = it.followUp.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.followUp.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.followUp.reception.historyCustomerVehicle.t183NoPolBelakang
                 rows << [

                        tanggalSMS: it.t802TglJamFU.format("dd/MM/yyyy HH:mm:ss"),

                        SA: it.t802NamaSA_FU,

                        statusFollowUp : it.followUp.t801StaBerhasilFU=="1"?"Sudah":"Belum",

                        namaCustomer : it.followUp?.reception?.historyCustomer?.t182NamaDepan + " " + it.followUp?.reception?.historyCustomer?.t182NamaBelakang,

                        noPol: nopol,

                        noHp : it.followUp.reception.historyCustomer.t182NoHp,

                        tanggalService : it.followUp?.reception.t401TglJamPenyerahan? it.followUp?.reception?.t401TglJamPenyerahan?.format(dateFormat) : it.followUp?.reception?.t401TanggalWO?.format(dateFormat),

                        nomorWo : it.followUp?.reception?.t401NoWO

                ]

        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }


}
