package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.reception.Reception
import org.hibernate.criterion.CriteriaSpecification

class SmsTerkirimService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = RealisasiFU.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", params.companyDealer)
            if (params."search_TanggalSms" && params."search_TanggalSmsAkhir") {
                ge("t802TglJamFU", params."search_TanggalSms")
                lt("t802TglJamFU", params."search_TanggalSmsAkhir" + 1)
            }
            if (params."search_TanggalService" && params."search_TanggalServiceAkhir") {
                followUp{
                    reception{
                        ge("t401TglJamPenyerahan", params."search_TanggalService")
                        lt("t401TglJamPenyerahan", params."search_TanggalServiceAkhir" + 1)
                    }
                }
            }
            if(params.statusBalasan){
                eq("t802StaBalasanSMS",params.statusBalasan)
            }
            if(params.inisialSA){
                eq("t802NamaSA_FU",params.inisialSA)
            }
            metodeFu{
                eq("m801NamaMetodeFu","SMS")
            }
            if(params.noHp){
                followUp{
                    reception{
                        historyCustomer{
                            eq("t182NoHp",params.noHp)
                        }
                    }
                }
            }
            if(params.nomorWo){
                followUp{
                    reception{
                        eq("t401NoWO",params.nomorWo)
                    }
                }
            }
            if(params.nopol1 && params.nopol2 && params.nopol3){
                followUp{
                    reception{
                        historyCustomerVehicle{
                            kodeKotaNoPol{
                                eq("id",params.nopol1 as Long)
                            }
                        }
                    }
                }
                followUp{
                    reception{
                        historyCustomerVehicle{
                            eq("t183NoPolTengah",params.nopol2)
                        }
                    }
                }
                followUp{
                    reception{
                        historyCustomerVehicle{
                            eq("t183NoPolBelakang",params.nopol3)
                        }
                    }
                }
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)

            projections {
                groupProperty("followUp", "followUp")
                groupProperty("t802NamaSA_FU", "t802NamaSA_FU")
                max("t802TglJamFU", "t802TglJamFU")
                max("t802StaBalasanSMS", "t802StaBalasanSMS")
            }

                order("t802TglJamFU","desc")

        }
        def rows = []
        int size = 0
        results.each {
            size++
            def nopol = it.followUp.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.followUp.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.followUp.reception.historyCustomerVehicle.t183NoPolBelakang
            def followUps = FollowUp.findByReception(Reception.findByT401NoWO(it.followUp?.reception.t401NoWO))?.t801ID

            def e = RealisasiFU.createCriteria()
            def resultsRate = e.list() {
                metodeFu{
                    eq("m801NamaMetodeFu","SMS")
                }
                followUp{
                    eq("t801ID",followUps)
                }

            }
            int co = 0
            resultsRate.each {
                co++
            }
            int jumlahSms = co
            rows << [

                    tanggalSMS: it.t802TglJamFU.format("dd/MM/yyyy HH:mm:ss"),

                    SA: it.t802NamaSA_FU,

                    statusBalasan : it.t802StaBalasanSMS=="1"?"Ya":"Tidak",

                    namaCustomer : it.followUp?.reception?.historyCustomer?.t182NamaDepan + " " + it.followUp?.reception?.historyCustomer?.t182NamaBelakang,

                    noPol: nopol,

                    noHp : it.followUp.reception.historyCustomer.t182NoHp,

                    tanggalService : it.followUp?.reception?.t401TglJamPenyerahan ? it.followUp?.reception?.t401TglJamPenyerahan?.format(dateFormat) : it.followUp?.reception?.t401TanggalWO.format(dateFormat),

                    nomorWo : it.followUp?.reception?.t401NoWO,

                    jumlahSms : jumlahSms

            ]

        }

        [sEcho: params.sEcho, iTotalRecords:  size, iTotalDisplayRecords: size, aaData: rows]

    }
    def getSmsTerkirimHistory(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def followUps = FollowUp.findByReception(Reception.findByT401NoWO(params.nomorWo))?.t801ID
            def rows2 = []

        def e = RealisasiFU.createCriteria()
        def resultsRate = e.list() {
            metodeFu{
                eq("m801NamaMetodeFu","SMS")
            }
            followUp{
                eq("t801ID",followUps)
            }
            order("t802TglJamFU","desc")
        }

        resultsRate.each {
            def nopol = it.followUp.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.followUp.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.followUp.reception.historyCustomerVehicle.t183NoPolBelakang
            rows2 << [

                    id : it.id,

                    tanggalSms : it.t802TglJamFU.format("dd/MM/yyyy HH:mm:ss"),

                    SA : it.t802NamaSA_FU,

                    nopol : nopol,

                    noHp : it.followUp.reception.historyCustomer.t182NoHp,

                    noWo : it.followUp?.reception.t401NoWO,

                    statusSms : it.t802StaTerhubung=="1"?"Terkirim":"Gagal"

            ]
        }

        return rows2
    }
}
