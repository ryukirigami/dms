package com.kombos.customerFollowUp

import com.kombos.administrasi.GroupManPower
import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.baseapp.AppSettingParam
import com.kombos.board.JPB
import com.kombos.parts.Konversi
import com.kombos.reception.KeluhanRcp
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP

import java.text.DateFormat
import java.text.SimpleDateFormat

class ServiceHistoryService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
    def conversi = new Konversi()

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def d = Reception.createCriteria()
        def results2 = d.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
//            eq("companyDealer",params?.companyDealer)
            eq("staSave","0")
            eq("staDel","0")
            eq("t401StaReceptionEstimasiSalesQuotation","reception")

            if (params."sCriteria_t017Tanggal" && params."sCriteria_t017Tanggal2") {
                or{
                    and{
                        ge("t401TglJamCetakWO", params."sCriteria_t017Tanggal")
                        lt("t401TglJamCetakWO", params."sCriteria_t017Tanggal2" + 1)
                    }
                    and{
                        isNull("t401TglJamCetakWO");
                        ge("dateCreated", params."sCriteria_t017Tanggal")
                        lt("dateCreated", params."sCriteria_t017Tanggal2" + 1)
                    }
                }
            }

            if(params.nomorWo){
                ilike("t401NoWO","%"+ params.nomorWo +"%")
            }

            if (params."vincode") {
               historyCustomerVehicle{
                   customerVehicle {
                       ilike("t103VinCode", "%" + (params."vincode" as String) + "%")
                   }
               }
            }

            if(params.model){
                historyCustomerVehicle{
                    fullModelCode{
                        baseModel{
                            ilike("m102NamaBaseModel","%"+ params.model +"%")
                        }
                    }
                }
            }

            if(params.nopol1 && params.nopol2 && params.nopol3){
                def kodeKota = KodeKotaNoPol.get(params.nopol1 as Long)
                String nopol = kodeKota?.m116ID +" "+ params.nopol2 +" "+ params.nopol3;
                historyCustomerVehicle{
                    eq("fullNoPol",nopol,[ignoreCase : true])
                }
            }
            order("t401NoWO","asc")
        }

        def rows = []

        results2.each {
            String tek = ""
            String kel = ""
            def rcp = it
            def keluhanItemsDb = KeluhanRcp.createCriteria().list {
                eq("reception",rcp)
                eq("staDel","0")
            }
            int a = 0
            keluhanItemsDb.each{
                a++;
                kel+=it?.t411NamaKeluhan
                if(a!=keluhanItemsDb?.size()){
                    kel+=", "
                }
            }

            def teknisi = JPB.findAllByReceptionAndStaDel(it, "0")*.namaManPower?.t015NamaBoard
            def cFr = JPB.findByReceptionAndStaDel(it, "0")
            def fr = GroupManPower.findById(cFr?.namaManPower?.groupManPower?.id)

            teknisi.each {
                tek = teknisi?.toString()?.substring(1,teknisi?.toString()?.length() - 1)
            }

            rows << [
                    cabang : it?.companyDealer.m011NamaWorkshop,
                    keluhan :kel,
                    vincode : it?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                    noWo: it?.t401NoWO,
                    nama: it?.historyCustomer?.t182NamaDepan,
                    alamat: it?.historyCustomer?.t182Alamat,
                    telp: it?.historyCustomer?.t182NoHp,
                    noPol: it?.historyCustomerVehicle?.fullNoPol,
                    model: it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    waktuReception: it?.t401TglJamCetakWO ? it?.t401TglJamCetakWO?.format("dd/MM/yyyy") : "",
                    waktuNotifikasi: it?.t401TglJamNotifikasi ? it?.t401TglJamNotifikasi?.format("dd/MM/yyyy") : "",
                    kmSaatIni: it?.t401KmSaatIni ? conversi.toKM(it?.t401KmSaatIni) : "-",
                    SA: it?.t401NamaSA,
                    teknisi: tek,
                    namaForeman : fr?.namaManPowerForeman?.t015NamaBoard ? fr?.namaManPowerForeman?.t015NamaBoard : "-",
                    waktuPenyerahan: it?.t401TglJamPenyerahan ? it?.t401TglJamPenyerahan?.format("dd/MM/yyyy") : cFr?.t451TglJamPlanFinished?.format("dd/MM/yyyy"),
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results2.totalCount, iTotalDisplayRecords: results2.totalCount, aaData: rows]
    }

    def datatablesSubList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def x = 0
        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                eq("t401NoWO",params.noWo)
            }
            or{
                ilike("t402StaTambahKurang","0")
                and{
                    not{
                        ilike("t402StaTambahKurang","%1%")
                        ilike("t402StaApproveTambahKurang","%1%")
                    }
                }
                and {
                    isNull("t402StaTambahKurang")
                    isNull("t402StaApproveTambahKurang")
                }
            }
        }

        def rows = []
        def rateService
        def hargaParts
        def hargaService
        results.each {
            def jobRCP = it
            rateService = it?.t402Rate ? it?.t402Rate : 0
            hargaParts = PartsRCP.createCriteria().listDistinct {
                eq("staDel","0")
                eq("reception",jobRCP?.reception)
                eq("operation",jobRCP?.operation)
                or{
                    ilike("t403StaTambahKurang","%0%")
                    and{
                        not{
                            ilike("t403StaTambahKurang","%1%")
                            ilike("t403StaApproveTambahKurang","%1%")
                        }
                    }
                    and{
                        isNull("t403StaTambahKurang")
                        isNull("t403StaApproveTambahKurang")
                    }
                }
            }
            hargaService = it?.t402HargaRp ? it?.t402HargaRp : 0
            def rega = 0

            hargaParts.each {
                def hrg = it?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.equalsIgnoreCase("GR") ? (it?.t403HargaRp*it?.t403Jumlah1) : it?.t403HargaRp
                rega += hrg
            }

            rows << [
                    job: it.operation.m053NamaOperation,
                    hargaParts : conversi.toRupiah(rega),
                    rateService : rateService,
                    hargaService : conversi.toRupiah(hargaService),
                    total : conversi.toRupiah(hargaService+rega),
                    noWo : params.noWo,
            ]

        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def datatablesSubSubList(def params) {

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = PartsRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("t401NoWO",params.noWo)
            }
            eq("staDel","0");
            operation{
                eq("m053NamaOperation",params.job)
            }
            or{
                ilike("t403StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t403StaTambahKurang","%1%")
                        ilike("t403StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t403StaTambahKurang")
                    isNull("t403StaApproveTambahKurang")
                }
            }
        }

        def rows = []

        results.each {
            rows << [
                    id : it?.id,
                    namaParts : it?.goods?.m111Nama,
                    qty: it?.t403Jumlah1,
                    satuan : it?.goods?.satuan?.m118Satuan1,
                    hargaParts: it?.t403HargaRp ? conversi.toRupiah(it?.t403HargaRp) : 0
            ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
}
