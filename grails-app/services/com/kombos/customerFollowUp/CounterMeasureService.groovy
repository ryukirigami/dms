package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.Pertanyaan

class CounterMeasureService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = RealisasiFU.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",params?.companyDealer)
            followUp{
                reception{
                    order("t401NoWO")
                }
            }

            if(params.inisialSA){
                followUp{
                    reception{
                        ilike("t401NamaSA","%"+params.inisialSA+"%")
                    }
                }
            }

            if(params.nomorWo){
                followUp{
                    reception{
                        ilike("t401NoWO","%"+params.nomorWo+"%")
                    }
                }
            }

            if(params.nopol1 && params.nopol2 && params.nopol3){
                followUp{
                    reception{
                        historyCustomerVehicle{
                            kodeKotaNoPol{
                                eq("id",params.nopol1 as Long)
                            }
                            eq("t183NoPolTengah",params.nopol2,[ignoreCase : true])
                            eq("t183NoPolBelakang",params.nopol3,[ignoreCase : true])
                        }
                    }
                }
            }

            if(params.statusCounterMeasure){
                eq('t802StaCounterMeasure',params.statusCounterMeasure)
            }

            if (params."search_TanggalFollowUp" && params."search_TanggalFollowUpAkhir") {
                ge("t802TglJamFU", params."search_TanggalFollowUp")
                lt("t802TglJamFU", params."search_TanggalFollowUpAkhir" + 1)
            }
            if (params."search_TanggalService" && params."search_TanggalServiceAkhir") {
                followUp{
                    reception{
                        ge("t401TglJamPenyerahan", params."search_TanggalService")
                        lt("t401TglJamPenyerahan", params."search_TanggalServiceAkhir" + 1)
                    }
                }
            }

//            order("t802TglJamFU","desc")
        }
        def rows = []
        int size = 0
        results.each {
            def tampil = false
            def pert1,pert2,pert3,pert4,pert5
            pert1 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(1))
            pert2 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(2))
            pert3 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(3))
            pert4 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(4))
            pert5 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(5))
            if(pert1 || pert2 || pert3 || pert4 || pert5){
                tampil = true
            }
            def nopol = it.followUp.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.followUp.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.followUp.reception.historyCustomerVehicle.t183NoPolBelakang
            if(tampil){
                def status = ""
                if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="1"){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="0"){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="0" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="0" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="0" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="0" && pert2?.t803StaPuasTdkPuas=="" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if((pert1?.t803StaPuasTdkPuas!="1" && pert1?.t803StaPuasTdkPuas!="0") || (pert2?.t803StaPuasTdkPuas!="1" && pert2?.t803StaPuasTdkPuas!="0") || (pert3?.t803StaPuasTdkPuas!="1" && pert3?.t803StaPuasTdkPuas!="0") || (pert4?.t803StaPuasTdkPuas!="1" && pert4?.t803StaPuasTdkPuas!="0") || (pert5?.t803StaPuasTdkPuas!="1" && pert5?.t803StaPuasTdkPuas!="0")){
                    status = "Please Validate!!!"
                }else{
                    status = "Invalid"
                }
                    size++
                    rows << [

                            t802TglJamFU: it.t802TglJamFU.format("dd/MM/yyyy HH:mm:ss"),

                            SA: it?.followUp?.reception?.t401NamaSA,

                            noPol: nopol,

                            nomorWo : it?.followUp?.reception?.t401NoWO,

                            Q1 : pert1?.t803StaPuasTdkPuas==""?"-":pert1?.t803StaPuasTdkPuas,
                            Q2 : pert2?.t803StaPuasTdkPuas==""?"-":pert2?.t803StaPuasTdkPuas,
                            Q3 : pert3?.t803StaPuasTdkPuas==""?"-":pert3?.t803StaPuasTdkPuas,
                            Q4 : pert4?.t803StaPuasTdkPuas==""?"-":pert4?.t803StaPuasTdkPuas,
                            Q5 : pert5?.t803StaPuasTdkPuas==""?"-":pert5?.t803StaPuasTdkPuas,

                            status : status,

                            ket : it.t802StaCounterMeasure=='1' ? 'Sudah' : 'Belum',

                            id : it.id

                    ]
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  size, iTotalDisplayRecords: size, aaData: rows]

    }

    def datatablesListEdit(params){
        def c = RealisasiFU.createCriteria()
        def results = c.list() {
            eq('id',params.id.toLong())
        }
        def rows = []
        int size = 0
        results.each {
            def tampil = false
            def pert1,pert2,pert3,pert4,pert5
            pert1 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(1))
            pert2 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(2))
            pert3 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(3))
            pert4 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(4))
            pert5 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(5))
            if(pert1 || pert2 || pert3 || pert4 || pert5){
                tampil = true
            }
            def nopol = it.followUp.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.followUp.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.followUp.reception.historyCustomerVehicle.t183NoPolBelakang
            if(tampil){
                size++
                rows << [
                        noWO: it?.followUp?.reception?.t401NoWO,
                        alasanQ1 : pert1.t803AlasanTdkPuas ? pert1.t803AlasanTdkPuas :"-",
                        alasanQ2 : pert2.t803AlasanTdkPuas ? pert2.t803AlasanTdkPuas :"-",
                        alasanQ3 : pert3.t803AlasanTdkPuas ? pert3.t803AlasanTdkPuas :"-",
                        alasanQ4 : pert4.t803AlasanTdkPuas ? pert4.t803AlasanTdkPuas :"-",
                        alasanQ5 : pert5.t803AlasanTdkPuas ? pert5.t803AlasanTdkPuas :"-"
                ]
            }
        }

        return rows
    }

}
