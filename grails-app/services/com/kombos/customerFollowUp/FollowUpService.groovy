package com.kombos.customerFollowUp



import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.Pertanyaan

class FollowUpService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = RealisasiFU.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", params.companyDealer)
            if (params."search_TanggalPlan" && params."search_TanggalPlanAkhir") {
                followUp{
                    ge("t801TglJamFU", params."search_TanggalPlan")
                    lt("t801TglJamFU", params."search_TanggalPlanAkhir" + 1)
                }
            }

            if (params."search_TanggalAction" && params."search_TanggalActionAkhir") {
                ge("t802TglJamFU", params."search_TanggalAction")
                lt("t802TglJamFU", params."search_TanggalActionAkhir" + 1)
            }

            if(params.statusFollowUp){
                followUp{
                    eq("t801StaBerhasilFU",params.statusFollowUp)
                }
            }
            if(params.statusSms){
                    eq("t802StaBalasanSMS",params.statusSms)
            }

            if(params.kategoriWaktuFu){
                followUp{
                    kategoriWaktuFu{
                        eq("id",params.kategoriWaktuFu as Long)
                    }
                }
            }

            if(params.metodeFu){
//                followUp{
                    metodeFu{
                        eq("id",params.metodeFu as Long)
                    }
//                }
            }

            if(params.inisialSaPlan){
                followUp{
                    reception{
                        eq("t401NamaSA",params.inisialSaPlan)
                    }
                }
            }

            if(params.inisialSaAction){
                eq("t802NamaSA_FU",params.inisialSaAction)
            }

            followUp{
                eq("staDel", "0")
                order("t801TglJamFU","asc")
            }
//            switch (sortProperty) {
//                default:
//                    order(sortProperty, sortDir)
//                    break;
//            }
        }

        def rows = []

        results.each {
            def nopol = it?.followUp?.reception?.historyCustomerVehicle?.fullNoPol
            rows << [

                    id: it?.followUp?.id,

                    nomorWo: it.followUp.reception.t401NoWO,

                    noHp: it.followUp.reception.historyCustomer.t182NoHp,

                    cpTambahan: it?.followUp?.reception?.historyCustomer?.t182CPTambahan ? it?.followUp?.reception?.historyCustomer?.t182CPTambahan:"-",

                    noPol: nopol,

                    model: it.followUp.reception.historyCustomerVehicle.fullModelCode?.baseModel?.m102NamaBaseModel,

                    waktu: it.followUp.kategoriWaktuFu.m804Kategori + " / " + it.followUp.metodeFu.m801NamaMetodeFu,

                    metodeFu: it.followUp.metodeFu.m801NamaMetodeFu,

                    tanggalPlan: it.followUp.t801TglJamFU ? it.followUp.t801TglJamFU.format(dateFormat) : "",

                    tanggalAction: it.t802TglJamFU ? it.t802TglJamFU.format(dateFormat) : "",

                    inisialSA: it.t802NamaSA_FU,

                    tanggalService: it?.followUp?.reception?.t401TglJamCetakWO ? it.followUp.reception.t401TglJamCetakWO.format(dateFormat) : it?.followUp?.reception?.dateCreated?.format(dateFormat),

                    t801StaBerhasilFU: it.followUp.t801StaBerhasilFU,

                    namaCustomer: it.followUp?.reception?.historyCustomer?.fullNama,

                    keterangn : it.followUp?.t801Ket ?  it.followUp?.t801Ket :"-",

                    statusFollowUp : it.followUp.t801StaBerhasilFU=="1"?"Sudah":"Belum"

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }
    def getData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def realis = RealisasiFU.findByFollowUpAndCompanyDealer(FollowUp.get(params.id as Long), params."companyDealer")
        def cari = Pertanyaan.findByRealisasiFUAndCompanyDealer(realis, params."companyDealer")
        def results, rows

        if(cari){
            def c = Pertanyaan.createCriteria()
            results = c.list () {
                realisasiFU{
                    followUp{
                        eq("id",params.id as Long)
                    }
                }
            }
            rows = []

            results.each {
                rows << [
                        id: it.id,
                        idFU : it.pertanyaanFu.id,
                        pertanyaan : it.pertanyaanFu.m803Pertanyaan,
                        alasan : it.t803AlasanTdkPuas? it.t803AlasanTdkPuas : "-",
                        staPuas : it.t803StaPuasTdkPuas
                ]
            }
        }else{
            def c = PertanyaanFu.createCriteria()
            results = c.list(){}

            rows = []
            def temp = 0
            results.each {
                rows << [
                        id: temp,
                        idFU : it.id,
                        pertanyaan : it.m803Pertanyaan,
                        alasan : "",
                        staPuas : ""
                ]
                temp++
            }
        }
        [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
    }

}