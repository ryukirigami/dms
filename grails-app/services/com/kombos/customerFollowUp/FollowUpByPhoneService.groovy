package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam

class FollowUpByPhoneService {
    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = FollowUpByPhone.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_operation") {
                operation{
                    ilike("m053NamaOperation","%"+params."sCriteria_operation"+"%")
                }
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    operation: it?.operation?.m053NamaOperation,

                    m802Ket: it.m802Ket,

            ]
        }

        [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

    }

    def show(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["FollowUpByPhone", params.id]]
            return result
        }

        result.followUpByPhoneInstance = FollowUpByPhone.get(params.id)

        if (!result.followUpByPhoneInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def edit(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["FollowUpByPhone", params.id]]
            return result
        }

        result.followUpByPhoneInstance = FollowUpByPhone.get(params.id)

        if (!result.followUpByPhoneInstance)
            return fail(code: "default.not.found.message")

        // Success.
        return result
    }

    def delete(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["FollowUpByPhone", params.id]]
            return result
        }

        result.followUpByPhoneInstance = FollowUpByPhone.get(params.id)

        if (!result.followUpByPhoneInstance)
            return fail(code: "default.not.found.message")

        try {
            result.followUpByPhoneInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            result.followUpByPhoneInstance.lastUpdProcess = "DELETE"
            result.followUpByPhoneInstance.staDel = '1'
            result.followUpByPhoneInstance.save(flush: true)
            return result //Success.
        }
        catch (org.springframework.dao.DataIntegrityViolationException e) {
            return fail(code: "default.not.deleted.message")
        }

    }

    def create(params) {
        def result = [:]
        def fail = { Map m ->
            result.error = [code: m.code, args: ["FollowUpByPhone", params.id]]
            return result
        }

        result.followUpByPhoneInstance = new FollowUpByPhone()
        result.followUpByPhoneInstance.properties = params

        // success
        return result
    }

    def updateField(def params) {
        def followUpByPhone = FollowUpByPhone.findById(params.id)
        if (followUpByPhone) {
            followUpByPhone."${params.name}" = params.value
            followUpByPhone.save()
            if (followUpByPhone.hasErrors()) {
                throw new Exception("${followUpByPhone.errors}")
            }
        } else {
            throw new Exception("FollowUpByPhone not found")
        }
    }

}