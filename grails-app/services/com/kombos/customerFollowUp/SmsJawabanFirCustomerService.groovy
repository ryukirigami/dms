package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.maintable.Pertanyaan

class SmsJawabanFirCustomerService {

    boolean transactional = false

    def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    def datatablesList(def params) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = RealisasiFU.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", params.companyDealer)
            metodeFu{
                eq("m801NamaMetodeFu","SMS")
            }


            if(params.inisialSA){
                eq("t802NamaSA_FU",params.inisialSA)
            }
            if(params.nomorWo){
                followUp{
                    reception{
                        eq("t401NoWO",params.nomorWo)
                    }
                }
            }
            if(params.nopol1 && params.nopol2 && params.nopol3){
                followUp{
                    reception{
                        historyCustomerVehicle{
                            kodeKotaNoPol{
                                eq("id",params.nopol1 as Long)
                            }
                        }
                    }
                }
                followUp{
                    reception{
                        historyCustomerVehicle{
                            eq("t183NoPolTengah",params.nopol2)
                        }
                    }
                }
                followUp{
                    reception{
                        historyCustomerVehicle{
                            eq("t183NoPolBelakang",params.nopol3)
                        }
                    }
                }
            }

            if (params."search_TanggalSms" && params."search_TanggalSmsAkhir") {
                ge("t802TglJamFU", params."search_TanggalSms")
                lt("t802TglJamFU", params."search_TanggalSmsAkhir" + 1)
            }
            if (params."search_TanggalService" && params."search_TanggalServiceAkhir") {
                followUp{
                    reception{
                        ge("t401TglJamPenyerahan", params."search_TanggalService")
                        lt("t401TglJamPenyerahan", params."search_TanggalServiceAkhir" + 1)
                    }
                }
            }

            order("t802TglJamFU","desc")
        }
        def rows = []
        int size = 0
        results.each {
            def tampil = false
            def pert1,pert2,pert3,pert4,pert5
            pert1 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(1))
            pert2 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(2))
            pert3 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(3))
            pert4 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(4))
            pert5 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(5))
            if(pert1 || pert2 || pert3 || pert4 || pert5){
                tampil = true
            }
            def nopol = it.followUp.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.followUp.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.followUp.reception.historyCustomerVehicle.t183NoPolBelakang
            if(tampil == true){
                def status = ""
                if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="1"){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="0"){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="0" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="0" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="0" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if(pert1?.t803StaPuasTdkPuas=="0" && pert2?.t803StaPuasTdkPuas=="" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    status = "Valid"
                }else if((pert1?.t803StaPuasTdkPuas!="1" && pert1?.t803StaPuasTdkPuas!="0") || (pert2?.t803StaPuasTdkPuas!="1" && pert2?.t803StaPuasTdkPuas!="0") || (pert3?.t803StaPuasTdkPuas!="1" && pert3?.t803StaPuasTdkPuas!="0") || (pert4?.t803StaPuasTdkPuas!="1" && pert4?.t803StaPuasTdkPuas!="0") || (pert5?.t803StaPuasTdkPuas!="1" && pert5?.t803StaPuasTdkPuas!="0")){
                    status = "Please Validate!!!"
                }else{
                    status = "Invalid"
                }
                def ok = false
                if(params.statusBalasan==status){
                    ok = true
                }else if(params.statusBalasan==null){
                    ok = true
                }
                if(ok == true){
                    size++
                    rows << [

                            tanggalSMS: it.t802TglJamFU.format("dd/MM/yyyy HH:mm:ss"),

                            SA: it.t802NamaSA_FU,

                            noPol: nopol,

                            nomorWo : it.followUp?.reception?.t401NoWO,

                            Q1 : pert1?.t803StaPuasTdkPuas==""?"-":pert1?.t803StaPuasTdkPuas,
                            Q2 : pert2?.t803StaPuasTdkPuas==""?"-":pert2?.t803StaPuasTdkPuas,
                            Q3 : pert3?.t803StaPuasTdkPuas==""?"-":pert3?.t803StaPuasTdkPuas,
                            Q4 : pert4?.t803StaPuasTdkPuas==""?"-":pert4?.t803StaPuasTdkPuas,
                            Q5 : pert5?.t803StaPuasTdkPuas==""?"-":pert5?.t803StaPuasTdkPuas,

                            status : status,

                            ket : status,

                            id : it.id

                    ]
                }
            }
        }

        [sEcho: params.sEcho, iTotalRecords:  size, iTotalDisplayRecords: size, aaData: rows]

    }

    def getData(params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def c2 = RealisasiFU.createCriteria()
        def results2 = c2.list () {
            metodeFu{
                eq("m801NamaMetodeFu","SMS")
            }
        }
        int yaQ1 = 0, yaQ2 = 0, yaQ3 = 0, yaQ4 = 0, yaQ5 = 0
        int tdkQ1 = 0, tdkQ2 = 0, tdkQ3 = 0, tdkQ4 = 0, tdkQ5 = 0
        int blankQ1 = 0, blankQ2 = 0, blankQ3 = 0, blankQ4 = 0, blankQ5 = 0
        int teksQ1 = 0, teksQ2 = 0, teksQ3 = 0, teksQ4 = 0, teksQ5 = 0
        results2.each {
            def tampil = false
            def pert1,pert2,pert3,pert4,pert5
            pert1 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(1))
            pert2 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(2))
            pert3 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(3))
            pert4 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(4))
            pert5 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(5))
            if(pert1 || pert2 || pert3 || pert4 || pert5){
                tampil = true
            }
            if(tampil == true){
                if(pert1?.t803StaPuasTdkPuas=="1"){
                    yaQ1++
                }else if(pert1?.t803StaPuasTdkPuas=="0"){
                    tdkQ1++
                }else if(pert1?.t803StaPuasTdkPuas=="-"){
                    blankQ1++
                }else{
                    teksQ1++
                }


                if(pert2?.t803StaPuasTdkPuas=="1"){
                    yaQ2++
                }else if(pert2?.t803StaPuasTdkPuas=="0"){
                    tdkQ2++
                }else if(pert2?.t803StaPuasTdkPuas=="-"){
                    blankQ2++
                }else{
                    teksQ2++
                }


                if(pert3?.t803StaPuasTdkPuas=="1"){
                    yaQ3++
                }else if(pert3?.t803StaPuasTdkPuas=="0"){
                    tdkQ3++
                }else if(pert3?.t803StaPuasTdkPuas=="-"){
                    blankQ3++
                }else{
                    teksQ3++
                }

                if(pert4?.t803StaPuasTdkPuas=="1"){
                    yaQ4++
                }else if(pert4?.t803StaPuasTdkPuas=="0"){
                    tdkQ4++
                }else if(pert4?.t803StaPuasTdkPuas=="-"){
                    blankQ4++
                }else{
                    teksQ4++
                }

                if(pert5?.t803StaPuasTdkPuas=="1"){
                    yaQ5++
                }else if(pert5?.t803StaPuasTdkPuas=="0"){
                    tdkQ5++
                }else if(pert5?.t803StaPuasTdkPuas=="-"){
                    blankQ5++
                }else{
                    teksQ5++
                }
            }
        }
        def rows2 = []


            rows2 << [

                    jawaban : 'Ya (1)',
                    Q1 : yaQ1,
                    Q2 : yaQ2,
                    Q3 : yaQ3,
                    Q4 : yaQ4,
                    Q5 : yaQ5

            ]

            rows2 << [

                    jawaban : 'Tidak (0)',
                    Q1 : tdkQ1,
                    Q2 : tdkQ2,
                    Q3 : tdkQ3,
                    Q4 : tdkQ4,
                    Q5 : tdkQ5

            ]

            rows2 << [

                    jawaban : 'Blank ()',
                    Q1 : blankQ1,
                    Q2 : blankQ2,
                    Q3 : blankQ3,
                    Q4 : blankQ4,
                    Q5 : blankQ5

            ]

            rows2 << [

                    jawaban : 'Teks (..abc..)',
                    Q1 : teksQ1,
                    Q2 : teksQ2,
                    Q3 : teksQ3,
                    Q4 : teksQ4,
                    Q5 : teksQ5

            ]

        [sEcho: params.sEcho, iTotalRecords:  rows2.size, iTotalDisplayRecords: rows2.size, aaData: rows2]
    }
}
