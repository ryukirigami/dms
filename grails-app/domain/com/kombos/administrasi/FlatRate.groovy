package com.kombos.administrasi

class FlatRate {
    CompanyDealer companyDealer
	Operation operation
	BaseModel baseModel
	Date t113TMT
	Double t113FlatRate
	String t113StaPriceMenu
    Double t113ProductionRate
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		operation nullable: false,blank: false
		baseModel nullable: false, blank: false
		companyDealer nullable: true, blank: true
		t113TMT blank : false, nullable:false
        t113StaPriceMenu blank : false, nullable:false, maxSize : 1
        t113FlatRate blank : false, nullable:false, maxSize : 8
        t113ProductionRate blank : false, nullable:false, maxSize : 8, validator :{
          val,obj ->
            return !FlatRate.findByOperationAndBaseModelAndT113StaPriceMenuAndT113FlatRateAndT113TMTAndT113ProductionRateAndStaDel(obj.operation, obj.baseModel, obj.t113StaPriceMenu, obj.t113FlatRate, obj.t113TMT, obj.t113ProductionRate, obj.staDel)
        }


		staDel nullable:false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T113_FLATRATE'
		operation column :'T113_M053_JobID'
		baseModel column : 'T113_M102_ID'
		t113TMT column : 'T113_TMT', sqlType : 'date'
		t113FlatRate column : 'T113_FlatRate'
        t113ProductionRate column: 'T113_ProductionFlatRate'
		t113StaPriceMenu column : 'T113_StaPriceMenu', sqlType : 'char(1)'
		staDel column : 'T113_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return t113FlatRate
	}
}
