package com.kombos.administrasi

import java.util.Date;

class MesinEdc {

	String m704Id
	String m704NamaMesinEdc
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	static mapping = {
		table 'M704_MESINEDC'
		
		m704Id column : 'M704_ID', sqlType : 'varchar(7)'
		m704NamaMesinEdc column : 'M704_NamaMesinEdc', sqlType : 'varchar(50)'
		staDel column : 'M704_StaDel', sqlType : 'char(1)'
	}
	
    static constraints = {
		m704Id (nullable : false, blank : false, unique : false, maxSize : 7)
		m704NamaMesinEdc (nullable : false, blank : false, maxSize : 50)
		staDel (nullable : false, blank : false, maxSize : 1)
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m704NamaMesinEdc
	}
}
