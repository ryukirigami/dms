package com.kombos.administrasi

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder


class GeneralParameter {

    static String defaultm000FormatSMSService = "Kami dari <WorkshopTAM> mengundang Bapak/Ibu <NamaCustomer> untuk melakukan Service Berkala <JenisSB> atas kendaraan <Kendaraan> warna <Warna> nomor polisi <Nopol> pada tanggal <TanggalService>. Balas SMS ini dengan mengetik 1 jika bersedia booking, atau 2 jika tidak bersedia booking. Terima kasih"
    static String defaultm000FormatEmailService = "Pemilik Kendaraan TOYOTA yang terhormat, \n" +
            "Kami mengucapkan terima kasih atas kepercayaan Anda kepada kami dan dengan bangga kami menyambut Anda menjadi keluarga besar Toyota. \n" +
            "Sebagai ungkapan terima kasih, kami mengundang Bapak/Ibu untuk melakukan pemeriksaan <JenisSB> di bengkel kami, jika Toyota Anda sudah mencapai <Km> atau <Waktu> (yang mana tercapai terlebih dahulu). \n" +
            "Pemeriksaan ini penting untuk memastikan kondisi kendaraan Anda prima, aman, dan nyaman setelah menempuh <Km> atau dipakai selama <Waktu>. Pemeriksaan ini juga merupakan syarat berlakunya TWC (Toyota Warranty Claim). \n" +
            "Untuk kemudahan dan kenyamanan Anda, manfaatkanlah fasilitas booking di bengkel kami. \n" +
            "Sekali lagi, terima kasih atas kepercayaan yang telah Anda berikan. \n  \n" +
            "TOYOTA"


    CompanyDealer companyDealer
    Integer m000VersionAplikasi = 0
    String m000StaLihatDataBengkelLain = "1"
    String m000FormatSMSService = defaultm000FormatSMSService
    String m000FormatEmail = defaultm000FormatEmailService
    Date m000JamKirimSMS = new Date()
    Integer m000HMinusReminderSMS = 0
    Date m000JamKirimEmail = new Date()
    Integer m000HMinusReminderEmail = 0
    Integer m000HMinusReminderCall = 10
    Date m000JamKirimSMSSTNK = new Date()
    Integer m000HMinusReminderSTNK = 0
    Integer m000HPlusUndanganFA = 0
    String m000HMinusTglReminderDM = "10"
    String m000ReminderDMOp1 = "0"
    Double m000ReminderDMKm1 = 0.0
    String m000ReminderDMOp2 = "0"
    Double m000ReminderDMKm2 = 0.0
    String m000ReminderEmailOp1 = "0"
    Double m000ReminderEmailKm1 = 0.0
    String m000ReminderEmailOp2 = "0"
    Double m000ReminderEmailKm2 = 0.0
    String m000ReminderSMSOp1 = "0"
    Double m000ReminderSMSKm1 = 0.0
    String m000ReminderSMSOp2 = "0"
    Double m000ReminderSMSKm2 = 0.0
    String m000TextCatatanDM = "Catatan untuk ditampilkan di blangko DM SBE silahkan ketik di sini ..."
    Integer m000HMinusFee = 0
    Integer m000mMinusFee = 0
    Integer m000HPlusCancelBookingFee = 0
    Integer m000JmlMaxInvitation = 0
    Double m000SettingRPP = 0.0
    Integer m000HMinusCekDokAsuransi = 0
    Double m000JmlToleransiAsuransiBP = 0.0
    String m000AlamatFTP = "-"
    Double m000PersenPPN = 0.0
    Double m000NominalMaterai = 0.0
    String m000StaAktifDM = "0"
    String m000StaAktifEmail = "1"
    String m000StaAktifSMS = "1"
    String m000StaAktifCall = "0"
    String m000DefaultPUK = "0"
    Integer m000JmlMaxItemWAC = 0

    // Web
    String m000StaEnablePartTambahanWeb = "0"

    // Security
    Date m000WaktuSessionLog = new Date()
    Integer m000JmlMaksimalKesalahan = 0
    Date m000PenguncianPassword = new Date()
    Integer m000MaxPasswordExpired = 0
    Integer m000PreExpiredAlert = 0
    Integer m000MinLastPassTdkBolehDigunakan = 0
    String m000DefaultUserPass = "-"
    Integer m000DefaultMasaBerlakuAccount = 0
    String m000DisableConcurrentLogin = "0"

    // Report
    Integer m000Aging11 = 0
    Integer m000Aging12 = 0
    Integer m000Aging21 = 0
    Integer m000Aging22 = 0
    Integer m000Aging31 = 0
    Integer m000Aging32 = 0
    Integer m000Aging41 = 0
    Integer m000Aging42 = 0
    Integer m000Aging51 = 0
    Integer m000Aging52 = 0

    //Reception
    String m000StaSalon = "0"
    Integer m000JmlSKipCustomer = 0
    Double m000PersenKenaikanJobSublet = 0.0
    String m000SOHeader = "0"

    // Production
    String m000staVendorCatByCc = "0"
    Double m000SBEDisc = 0.0
    Double m000ProgressDisc = 0.0
    Integer m000HMinParameterReserved = 0
    Double m000PersenPemasanganPartBP = 60
    Double m000PersenPelepasanPartBP = 40
    Double m000PreparationTime = 0.0
    Double m000PersenPreparation = 40
    Double m000PersenPainting = 30
    Double m000PersenPoleshing = 30
    Double m000PersenPemborong = 0.0
    Double m000PersenWorkshop = 0.0
    Double m000PersenMaxGunakanMaterial = 0.0
    Double m000LamaCuciMobil = 0.0
    Double m000LamaWaktuMinCuci = 0.0
    Date m000WaktuNotifikasiTargetClockOffJob = new Date()
    Date m000ToleransiAmbilWO = new Date()
    Double m000ToleransiIDRPerProses = 0.0
    Double m000ToleransiIDRPerJob = 0.0
    Date m000ToleransiProsesBP = new Date()

    // Man Power
    JenisJamKerja m000staJenisJamKerjaSenin
    JenisJamKerja m000staJenisJamKerjaSelasa
    JenisJamKerja m000staJenisJamKerjaRabu
    JenisJamKerja m000staJenisJamKerjaKamis
    JenisJamKerja m000staJenisJamKerjaJumat
    JenisJamKerja m000staJenisJamKerjaSabtu
    JenisJamKerja m000staJenisJamKerjaMinggu
    Integer m000BulanWarningSertifikat = 0
    String m000Person1 = "0"
    String m000Person2 = "0"
    String m000Person3 = "0"
    String m000Person4 = "0"
    String m000Person5 = "0"

    // General
    Integer m000MaxRecordPerPage = 0
    String m000NamaHostOrIPAddress = "0.0.0.0"
    String m000Port = 8080
    String m000UserName = "user"
    String m000Password = "password"
    String m000RunningTextInformation = "Dealer"
    String m000StaWaktuAkhir = "0"
    Date m000TglJamWaktuAkhir = null

    // Follow Up
    Integer m000JmlMaxKirimSMSFollowUp = 0

    // Delivery
    Date m000BufferDeliveryTimeGR = new Date()
    Date m000BufferDeliveryTimeBP = new Date()
    Double m000MaksBayarCashRefund = 0.0
    Double m000Tax = 0.0
    Double m000PersenPPh = 0.0
    String m000FormatSMSNotifikasi = "FormatSMSNotifikasi"
    String m000InvoiceFooter = "Footer"
    String m000NamaTWC = "NamaTWC"
    String m000AlamatTWC = "AlamatTWC"
    String m000NPWPTWC = "NPWPTWC"
    String m000NamaPWC = "NamaPWC"
    String m000AlamatPWC = "AlamatPWC"
    String m000NPWPPWC = "NPWPPWC"
    Date m000ToleransiDeliveryGR = new Date()
    Date m000ToleransiDeliveryBP = new Date()

    Date m000JedaWaktuTelpCust = new Date()
    // Appointment
    Integer m000KonfirmasiHariSebelum = 0
    Date m000KonfirmasiJamSebelum = new Date()
    Date m000KonfirmasiTerlambat = new Date()
    Integer m000HMinusSMSAppointment = 0
    Date m000JMinusSMSAppointment = new Date()
    Date m000JPlusSMSTerlambat = new Date()
    Double m000MaxPersenKembaliBookingFee = 0.0
    String m000FormatSMSHMinus = "0"
    String m000FormatSMSJMinus = "0"

    //finance
    Double m000SaldoKasOperasional = 0.0

    String m000StaDel = "0"
    Date dateCreated = new Date() // Menunjukkan kapan baris isian ini dibuat.
    String createdBy = "0" //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated = new Date() //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy = "0" //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "insert"  //Baris ini, terakhir prosess apa ?? insert, update, delete


    public GeneralParameter() {

    }


    static constraints = {
        companyDealer(nullable: true, blank: false)
        m000VersionAplikasi(nullable: false, blank: false)
        m000StaLihatDataBengkelLain(nullable: false, blank: false)
        m000FormatSMSService(nullable: false, blank: false, maxSize: 1000)
        m000FormatEmail(nullable: false, blank: false, maxSize: 10000)
        m000JamKirimSMS(nullable: false, blank: false)
        m000HMinusReminderSMS(nullable: false, blank: false)
        m000JamKirimEmail(nullable: false, blank: false)
        m000HMinusReminderEmail(nullable: false, blank: false)
        m000HMinusReminderCall(nullable: false, blank: false)
        m000JamKirimSMSSTNK(nullable: false, blank: false)
        m000HMinusReminderSTNK(nullable: false, blank: false)
        m000HPlusUndanganFA(nullable: false, blank: false)
        m000HMinusTglReminderDM(nullable: false, blank: false, maxSize: 10)
        m000ReminderDMOp1(nullable: false, blank: false)
        m000ReminderDMKm1(nullable: false, blank: false)
        m000ReminderDMOp2(nullable: false, blank: false)
        m000ReminderDMKm2(nullable: false, blank: false)
        m000ReminderEmailOp1(nullable: false, blank: false)
        m000ReminderEmailKm1(nullable: false, blank: false)
        m000ReminderEmailOp2(nullable: false, blank: false)
        m000ReminderEmailKm2(nullable: false, blank: false)
        m000ReminderSMSOp1(nullable: false, blank: false)
        m000ReminderSMSKm1(nullable: false, blank: false)
        m000ReminderSMSOp2(nullable: false, blank: false)
        m000ReminderSMSKm2(nullable: false, blank: false)
        m000TextCatatanDM(nullable: false, blank: false, maxSize: 255)
        m000HMinusFee(nullable: false, blank: false)
        m000mMinusFee(nullable: false, blank: false)
        m000HPlusCancelBookingFee(nullable: false, blank: false)
        m000JmlMaxInvitation(nullable: false, blank: false)
        m000SettingRPP(nullable: false, blank: false)
        m000HMinusCekDokAsuransi(nullable: false, blank: false)
        m000JmlToleransiAsuransiBP(nullable: false, blank: false)
        m000AlamatFTP(nullable: false, blank: false, maxSize: 50)
        m000PersenPPN(nullable: false, blank: false)
        m000NominalMaterai(nullable: false, blank: false)
        m000StaAktifDM(nullable: false, blank: false)
        m000StaAktifEmail(nullable: false, blank: false)
        m000StaAktifSMS(nullable: false, blank: false)
        m000StaAktifCall(nullable: false, blank: false)
        m000DefaultPUK(nullable: false, blank: false, maxSize: 50)
        m000JmlMaxItemWAC(nullable: false, blank: false)
        m000JmlMaxItemWAC(nullable: false, blank: false)

        // Web
        m000StaEnablePartTambahanWeb(nullable: false, blank: false)

        // Security
        m000WaktuSessionLog(nullable: false, blank: false)
        m000JmlMaksimalKesalahan(nullable: false, blank: false)
        m000PenguncianPassword(nullable: false, blank: false)
        m000MaxPasswordExpired(nullable: false, blank: false)
        m000PreExpiredAlert(nullable: false, blank: false)
        m000MinLastPassTdkBolehDigunakan(nullable: false, blank: false)
        m000DefaultUserPass(nullable: false, blank: false, maxSize: 50)
        m000DefaultMasaBerlakuAccount(nullable: false, blank: false)
        m000DisableConcurrentLogin(nullable: false, blank: false)

        // Report
        m000Aging11(nullable: false, blank: false)
        m000Aging12(nullable: false, blank: false)
        m000Aging21(nullable: false, blank: false)
        m000Aging22(nullable: false, blank: false)
        m000Aging31(nullable: false, blank: false)
        m000Aging32(nullable: false, blank: false)
        m000Aging41(nullable: false, blank: false)
        m000Aging42(nullable: false, blank: false)
        m000Aging51(nullable: false, blank: false)
        m000Aging52(nullable: false, blank: false)

        // Reception
        m000StaSalon(nullable: false, blank: false)
        m000JmlSKipCustomer(nullable: false, blank: false)
        m000PersenKenaikanJobSublet(nullable: false, blank: false)
        m000SOHeader(nullable: true, blank: true, maxSize: 512)

        // Production
        m000staVendorCatByCc(nullable: false, blank: false)
        m000SBEDisc(nullable: false, blank: false)
        m000ProgressDisc(nullable: false, blank: false)
        m000HMinParameterReserved(nullable: false, blank: false)
        m000PersenPemasanganPartBP(nullable: false, blank: false)
        m000PersenPelepasanPartBP(nullable: false, blank: false)
        m000PreparationTime(nullable: false, blank: false)
        m000PersenPreparation(nullable: false, blank: false)
        m000PersenPainting(nullable: false, blank: false)
        m000PersenPoleshing(nullable: false, blank: false)
        m000PersenPemborong(nullable: false, blank: false)
        m000PersenWorkshop(nullable: false, blank: false)
        m000PersenMaxGunakanMaterial(nullable: false, blank: false)
        m000LamaCuciMobil(nullable: false, blank: false)
        m000LamaWaktuMinCuci(nullable: false, blank: false)
        m000WaktuNotifikasiTargetClockOffJob(nullable: true, blank: true)
        m000ToleransiAmbilWO(nullable: false, blank: false)
        m000ToleransiIDRPerProses(nullable: false, blank: false)
        m000ToleransiIDRPerJob(nullable: false, blank: false)
        m000ToleransiProsesBP(nullable: false, blank: false)

        // Man Power
        m000staJenisJamKerjaSenin(nullable: true, blank: false)
        m000staJenisJamKerjaSelasa(nullable: true, blank: false)
        m000staJenisJamKerjaRabu(nullable: true, blank: false)
        m000staJenisJamKerjaKamis(nullable: true, blank: false)
        m000staJenisJamKerjaJumat(nullable: true, blank: false)
        m000staJenisJamKerjaSabtu(nullable: true, blank: false)
        m000staJenisJamKerjaMinggu(nullable: true, blank: false)
        m000BulanWarningSertifikat(nullable: false, blank: false)
        m000Person1(nullable: false, blank: false, maxSize: 50)
        m000Person2(nullable: false, blank: false, maxSize: 50)
        m000Person3(nullable: false, blank: false, maxSize: 50)
        m000Person4(nullable: false, blank: false, maxSize: 50)
        m000Person5(nullable: false, blank: false, maxSize: 50)

        // General
        m000MaxRecordPerPage(nullable: false, blank: false)
        m000NamaHostOrIPAddress(nullable: false, blank: false, maxSize: 50)
        m000Port(nullable: false, blank: false, maxSize: 50)
        m000UserName(nullable: false, blank: false, maxSize: 50)
        m000Password(nullable: false, blank: false, maxSize: 50)
        m000RunningTextInformation(nullable: true, blank: true, maxSize: 512)
        m000StaWaktuAkhir(nullable: false, blank: false)
        m000TglJamWaktuAkhir(nullable: true, blank: true)

        // Follow Up
        m000JmlMaxKirimSMSFollowUp(nullable: false, blank: false)

        // Delivery
        m000BufferDeliveryTimeGR(nullable: false, blank: false)
        m000BufferDeliveryTimeBP(nullable: false, blank: false)
        m000MaksBayarCashRefund(nullable: false, blank: false)
        m000Tax(nullable: false, blank: false)
        m000PersenPPh(nullable: false, blank: false)
        m000FormatSMSNotifikasi(nullable: false, blank: false)
        m000InvoiceFooter(nullable: false, blank: false)
        m000NamaTWC(nullable: false, blank: false, maxSize: 50)
        m000AlamatTWC(nullable: false, blank: false)
        m000NPWPTWC(nullable: true, blank: true, maxSize: 50)
        m000NamaPWC(nullable: false, blank: false, maxSize: 50)
        m000AlamatPWC(nullable: false, blank: false)
        m000NPWPPWC(nullable: true, blank: true, maxSize: 50)
        m000ToleransiDeliveryGR(nullable: false, blank: false)
        m000ToleransiDeliveryBP(nullable: false, blank: false)

        // Appointment
        m000KonfirmasiHariSebelum(nullable: false, blank: false)
        m000KonfirmasiJamSebelum(nullable: false, blank: false)
        m000KonfirmasiTerlambat(nullable: false, blank: false)
        m000HMinusSMSAppointment(nullable: false, blank: false)
        m000JMinusSMSAppointment(nullable: false, blank: false)
        m000JPlusSMSTerlambat(nullable: false, blank: false)
        m000MaxPersenKembaliBookingFee(nullable: false, blank: false)
        m000FormatSMSHMinus(nullable: false, blank: false)
        m000FormatSMSJMinus(nullable: false, blank: false)

        //finance
        m000SaldoKasOperasional(nullable: true, blank: true)

        m000StaDel(nullable: false, blank: false)
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "M000_GENERALPARAMETER"
        companyDealer column: "M000_M011_ID", unique: true
        m000VersionAplikasi column: "M000_VersionAplikasi", sqlType: 'int'
        m000StaLihatDataBengkelLain column: "M000_StaLihatDataBengkelLain", sqlType: "char(1)"
        m000FormatSMSService column: "M000_FormatSMSService"//, sqlType: "text"
        m000FormatEmail column: "M000_FormatEmail"//, sqlType: "text"
        m000JamKirimSMS column: "M000_JamKirimSMS"//, sqlType: 'date'
        m000HMinusReminderSMS column: "M000_HMinusReminderSMS", sqlType: 'int'
        m000JamKirimEmail column: "M000_JamKirimEmail"//, sqlType: 'date'
        m000HMinusReminderEmail column: "M000_HMinusReminderEmail", sqlType: 'int'
        m000HMinusReminderCall column: "M000_HMinusReminderCall", sqlType: 'int'
        m000JamKirimSMSSTNK column: "M000_JamKirimSMSSTNK"//, sqlType: 'date'
        m000JedaWaktuTelpCust column: "M000_JedaWaktuTelpCust"//, sqlType: 'date'
        m000HMinusReminderSTNK column: "M000_HMinusReminderSTNK", sqlType: 'int'
        m000HPlusUndanganFA column: "M000_HPlusUndanganFA", sqlType: 'int'
        m000HMinusTglReminderDM column: "M000_HMinusTglReminderDM", sqlType: "char(10)"
        m000ReminderDMOp1 column: "M000_ReminderDMOp1", sqlType: "varchar(2)"
        m000ReminderDMKm1 column: "M000_ReminderDMKm1"
        m000ReminderDMOp2 column: "M000_ReminderDMOp2", sqlType: "varchar(2)"
        m000ReminderDMKm2 column: "M000_ReminderDMKm2"
        m000ReminderEmailOp1 column: "M000_ReminderEmailOp1", sqlType: "varchar(2)"
        m000ReminderEmailKm1 column: "M000_ReminderEmailKm1"
        m000ReminderEmailOp2 column: "M000_ReminderEmailOp2", sqlType: "varchar(2)"
        m000ReminderEmailKm2 column: "M000_ReminderEmailKm2"
        m000ReminderSMSOp1 column: "M000_ReminderSMSOp1", sqlType: "varchar(2)"
        m000ReminderSMSKm1 column: "M000_ReminderSMSKm1"
        m000ReminderSMSOp2 column: "M000_ReminderSMSOp2", sqlType: "varchar(2)"
        m000ReminderSMSKm2 column: "M000_ReminderSMSKm2"
        m000TextCatatanDM column: "M000_TextCatatanDM", sqlType: "varchar(255)"
        m000HMinusFee column: "M000_HMinusFee", sqlType: 'int'
        m000mMinusFee column: "M000_mMinusFee", sqlType: 'int'
        m000HPlusCancelBookingFee column: "M000_HPlusCancelBookingFee", sqlType: 'int'
        m000JmlMaxInvitation column: "M000_JmlMaxInvitation", sqlType: 'int'
        m000SettingRPP column: "M000_SettingRPP"
        m000HMinusCekDokAsuransi column: "M000_HMinusCekDokAsuransi", sqlType: 'int'
        m000JmlToleransiAsuransiBP column: "M000_JmlToleransiAsuransiBP"
        m000AlamatFTP column: "M000_AlamatFTP", sqlType: 'varchar(50)'
        m000PersenPPN column: "M000_PersenPPN"
        m000NominalMaterai column: "M000_NominalMaterai"
        m000StaAktifDM column: "M000_StaAktifDM", sqlType: 'char(1)'
        m000StaAktifEmail column: "M000_StaAktifEmail", sqlType: 'char(1)'
        m000StaAktifSMS column: "M000_StaAktifSMS", sqlType: 'char(1)'
        m000StaAktifCall column: "M000_StaAktifCall", sqlType: 'char(1)'
        m000DefaultPUK column: "M000_DefaultPUK", sqlType: 'varchar(50)'
        m000JmlMaxItemWAC column: "M000_JmlMaxItemWAC", sqlType: 'int'

        // Web
        m000StaEnablePartTambahanWeb column: "M000_StaEnablePartTambahanWeb", sqlType: 'char(1)'

        // Security
        m000WaktuSessionLog column: "M000_WaktuSessionLog"//, sqlType: 'date'
        m000JmlMaksimalKesalahan column: "M000_JmlMaksimalKesalahan", sqlType: 'int'
        m000PenguncianPassword column: "M000_PenguncianPassword"//, sqlType: 'date'
        m000MaxPasswordExpired column: "M000_MaxPasswordExpired", sqlType: 'int'
        m000PreExpiredAlert column: "M000_PreExpiredAlert", sqlType: 'int'
        m000MinLastPassTdkBolehDigunakan column: "M000_MinLastPassTdkBlhDignkn", sqlType: 'int'
        m000DefaultUserPass column: "M000_DefaultUserPass", sqlType: 'varchar(50)'
        m000DefaultMasaBerlakuAccount column: "M000_DefaultMasaBerlakuAccount", sqlType: 'int'
        m000DisableConcurrentLogin column: "M000_DisableConcurrentLogin", sqlType: 'char(1)'

        // Report
        m000Aging11 column: "M000_Aging11", sqlType: 'int'
        m000Aging12 column: "M000_Aging12", sqlType: 'int'
        m000Aging21 column: "M000_Aging21", sqlType: 'int'
        m000Aging22 column: "M000_Aging22", sqlType: 'int'
        m000Aging31 column: "M000_Aging31", sqlType: 'int'
        m000Aging32 column: "M000_Aging32", sqlType: 'int'
        m000Aging41 column: "M000_Aging41", sqlType: 'int'
        m000Aging42 column: "M000_Aging42", sqlType: 'int'
        m000Aging51 column: "M000_Aging51", sqlType: 'int'
        m000Aging52 column: "M000_Aging52", sqlType: 'int'

        // Reception
        m000StaSalon column: "M000_StaSalon", sqlType: 'char(1)'
        m000JmlSKipCustomer column: "M000_JmlSKipCustomer", sqlType: 'int'
        m000PersenKenaikanJobSublet column: "M000_PersenKenaikanJobSublet"
        m000SOHeader column: "M000_SO_Header"//, sqlType: 'text'

        // Production
        m000staVendorCatByCc column: "M000_staVendorCatByCc", sqlType: 'char(1)'
        m000SBEDisc column: "M000_SBEDisc"
        m000ProgressDisc column: "M000_ProgressDisc"
        m000HMinParameterReserved column: "M000_HMinParameterReserved", sqlType: 'int'
        m000PersenPemasanganPartBP column: "M000_PersenPemasanganPartBP"
        m000PersenPelepasanPartBP column: "M000_PersenPelepasanPartBP"
        m000PreparationTime column: "M000_PreparationTime"
        m000PersenPreparation column: "M000_PersenPreparation"
        m000PersenPainting column: "M000_PersenPainting"
        m000PersenPoleshing column: "M000_PersenPoleshing"
        m000PersenPemborong column: "M000_PersenPemborong"
        m000PersenWorkshop column: "M000_PersenWorkshop"
        m000PersenMaxGunakanMaterial column: "M000_PersenMaxGunakanMaterial"
        m000LamaCuciMobil column: "M000_LamaCuciMobil"
        m000LamaWaktuMinCuci column: "M000_LamaWaktuMinCuci"
        m000WaktuNotifikasiTargetClockOffJob column: "M000_WktNotifTrgtClockOffJob"//, sqlType: 'date'
        m000ToleransiAmbilWO column: "M000_ToleransiAmbilWO"//, sqlType: 'date'
        m000ToleransiIDRPerProses column: "M000_ToleransiIDRPerProses"//, sqlType: 'int'
        m000ToleransiIDRPerJob column: "M000_ToleransiIDRPerJob"//, sqlType: 'int'
        m000ToleransiProsesBP column: "M000_ToleransiProsesBP"//, sqlType: 'int'

        // Man Power
        m000staJenisJamKerjaSenin column: "M000_StaJenisJamKerjaSenin"
        m000staJenisJamKerjaSelasa column: "M000_StaJenisJamKerjaSelasa"
        m000staJenisJamKerjaRabu column: "M000_StaJenisJamKerjaRabu"
        m000staJenisJamKerjaKamis column: "M000_StaJenisJamKerjaKamis"
        m000staJenisJamKerjaJumat column: "M000_StaJenisJamKerjaJumat"
        m000staJenisJamKerjaSabtu column: "M000_StaJenisJamKerjaSabtu"
        m000staJenisJamKerjaMinggu column: "M000_StaJenisJamKerjaMinggu"
        m000BulanWarningSertifikat column: "M000_BulanWarningSertifikat", sqlType: 'int'
        m000Person1 column: "M000_Person1", sqlType: 'varchar(50)'
        m000Person2 column: "M000_Person2", sqlType: 'varchar(50)'
        m000Person3 column: "M000_Person3", sqlType: 'varchar(50)'
        m000Person4 column: "M000_Person4", sqlType: 'varchar(50)'
        m000Person5 column: "M000_Person5", sqlType: 'varchar(50)'

        // General
        m000MaxRecordPerPage column: "M000_MaxRecordPerPage", sqlType: 'int'
        m000NamaHostOrIPAddress column: "M000_NamaHostOrIPAddress", sqlType: "varchar(50)"
        m000Port column: "M000_Port", sqlType: "varchar(50)"
        m000UserName column: "M000_UserName", sqlType: "varchar(50)"
        m000Password column: "M000_Password", sqlType: "varchar(50)"
        m000RunningTextInformation column: "M000_RunningTextInformation"//, sqlType: 'text'
        m000StaWaktuAkhir column: "M000_StaWaktuAkhir", sqlType: 'char(1)'
        m000TglJamWaktuAkhir column: "M000_TglJamWaktuAkhir"//, sqlType: 'date'

        // Follow Up
        m000JmlMaxKirimSMSFollowUp column: "M000_JmlMaxKirimSMSFollowUp", sqlType: 'int'

        // Delivery
        m000BufferDeliveryTimeGR column: "M000_BufferDeliveryTimeGR"//, sqlType: 'date'
        m000BufferDeliveryTimeBP column: "M000_BufferDeliveryTimeBP"//, sqlType: 'date'
        m000MaksBayarCashRefund column: "M000_MaksBayarCashRefund"
        m000Tax column: "M000_Tax"
        m000PersenPPh column: "M000_PersenPPh"
        m000FormatSMSNotifikasi column: "M000_FormatSMSNotifikasi"//, sqlType: 'text'
        m000InvoiceFooter column: "M000_InvoiceFooter"//, sqlType: 'text'
        m000NamaTWC column: "M000_NamaTWC", sqlType: 'varchar(50)'
        m000AlamatTWC column: "M000_AlamatTWC"//, sqlType: 'text'
        m000NPWPTWC column: "M000_PWPTWC", sqlType: 'varchar(50)'
        m000NamaPWC column: "M000_NamaPWC", sqlType: 'varchar(50)'
        m000AlamatPWC column: "M000_AlamatPWC"//, sqlType: 'text'
        m000NPWPPWC column: "M000_NPWPPWC", sqlType: 'varchar(50)'
        m000ToleransiDeliveryGR column: "M000_ToleransiDeliveryGR"//, sqlType: 'date'
        m000ToleransiDeliveryBP column: "M000_ToleransiDeliveryBP"//, sqlType: 'date'

        // Appointment
        m000KonfirmasiHariSebelum column: "M000_KonfirmasiHariSebelum"
        m000KonfirmasiJamSebelum column: "M000_KonfirmasiJamSebelum"//, sqlType: 'date'
        m000KonfirmasiTerlambat column: "M000_KonfirmasiTerlambat"//, sqlType: 'date'
        m000MaxPersenKembaliBookingFee column: "M000_MaxPrsnKmbliBookingFee"
        m000FormatSMSHMinus column: "M000_FormatSMSHMinus"//, sqlType: 'text'
        m000FormatSMSJMinus column: "M000_FormatSMSJMinus"//, sqlType: 'text'
        m000HMinusSMSAppointment column: "M000_HMinusSMSAppointment", sqlType: 'int'
        m000JMinusSMSAppointment column: "M000_JMinusSMSAppointment"//, sqlType: 'date'
        m000JPlusSMSTerlambat column: "M000_JPlusSMSTerlambat"//, sqlType: 'date'

        //finance
        m000SaldoKasOperasional column: "M000_SaldoKasOperasional"//, float

        m000StaDel column: "M000_StaDel", sqlType: 'char(1)'
    }

    String toString() {
        return companyDealer?.m011NamaWorkshop;
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "update"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        m000StaDel = "0"

//        GrailsWebRequest request = RequestContextHolder.currentRequestAttributes()
//        companyDealer = session.userCompanyDealer
        JenisJamKerja jenisJamKerja = JenisJamKerja.first()
        m000staJenisJamKerjaSenin = jenisJamKerja
        m000staJenisJamKerjaSelasa = jenisJamKerja
        m000staJenisJamKerjaRabu = jenisJamKerja
        m000staJenisJamKerjaKamis = jenisJamKerja
        m000staJenisJamKerjaJumat = jenisJamKerja
        m000staJenisJamKerjaSabtu = jenisJamKerja
        m000staJenisJamKerjaMinggu = jenisJamKerja

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if (!lastUpdProcess)
            lastUpdProcess = "insert"
        if (!lastUpdated)
            lastUpdated = new Date()
        if (!m000StaDel)
            m000StaDel = "0"
        if (!createdBy) {
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}