package com.kombos.administrasi

class Audio {

//	int m405Id
//	CompanyDealer companyDealer
	String m405Text
//	byte[] m405Audio

    String audioPath

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
//		m405Id column : 'M405_ID', sqlType : 'int'
		table 'M405_AUDIO'
		
	//	companyDealer column : 'M405_M011_ID'
		m405Text column : 'M405_Text', sqlType : 'varchar(50)'
	//	m405Audio column : 'M405_Audio', sqlType : 'blob'
	}
	
	String toString(){
		return m405Text
	}
    static constraints = {
//		m405Id (nullable : false, blank : false, unique : true, maxSize : 4)
//		companyDealer (nullable : false, blank : false)
		m405Text (nullable : false, blank : false, maxSize : 50)
	//	m405Audio (nullable : false, blank : false, maxSize: 10485760)
        audioPath nullable : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
