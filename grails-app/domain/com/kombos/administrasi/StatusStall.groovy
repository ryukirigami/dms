package com.kombos.administrasi

import java.util.Date;

class StatusStall {

	Stall stall
	Date m025Tmt
	String m025Sta
	//String m025StaBookingWeb
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	static mapping = {
		stall column : 'M025_M022_StallId', unique: false
        m025Tmt column : 'M025_TMT', sqlType : 'date', unique: true
		m025Sta column : 'M025_Sta', sqlType : 'varchar(1)'
		//m025StaBookingWeb column : 'M025_StaBookingWeb', sqlType : 'char(1)'
		table 'M025_STATUSSTALL'
	}
	
    static constraints = {
        stall nullable: false, blank:false
        m025Tmt nullable: false, blank:false
        m025Sta nullable: false, blank:false
        //m025StaBookingWeb nullable: false, blank:false
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
