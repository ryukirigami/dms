package com.kombos.administrasi

import com.kombos.parts.Goods

class JobPackage {

    KategoriJob kategori
    Operation job

    static mapping = {
        table "A999_JOBPACKAGE"
        kategori column:"KATEGORI_ID"
        job column:"OPERATION_ID"
    }
}
