package com.kombos.administrasi

class InformasiPenting {
    Date m032Tanggal
	String m032UserUpload
    String m032Judul
	String m032Konten
	byte[] m032Image1
	String m032StaTampil
    byte[] m032Image2
    byte[] m032Image3
    byte[] m032Image4
    byte[] m032Image5
    String imageMime
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    CompanyDealer companyDealer
    Integer m032ID

    static constraints = {

        m032Tanggal (blank:true, nullable: true)
        m032UserUpload (blank:true, nullable: true)
        m032Judul (blank:true, nullable: true, maxSize: 50)
		m032Konten (blank:true, nullable: true, maxSize: 1000)
		m032Image1 (nullable : true, blank : true,maxSize: 10485760)
        m032StaTampil (blank:true, nullable: true)
		m032Image2 (blank:true, nullable: true)
		m032Image3 (blank:true, nullable: true)
		m032Image4 (blank:true, nullable: true)
		m032Image5 (blank:true, nullable: true)
        imageMime maxSize : 50, nullable : true
        companyDealer (blank:true, nullable: true)
        m032ID (blank:true, nullable: true)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table "M032_INFORMASIPENTING"
        m032ID column: "M032_ID", sqlType : 'int'
		companyDealer column:"M032_M011_ID"
		m032Tanggal column: "M032_Tangal"//, sqlType: 'date'
		m032UserUpload column:"M032_UserUpload", sqlType:'varchar(50)'
        m032Judul column:"M032_Judul", sqlType:'varchar(50)'
		m032Konten column:"M032_Konten", sqlType:'varchar(1000)'//, sqlType:"text"
		m032Image1 column:"M032_Image1", sqlType:"blob"
//		m032Image2 column:"M032_Image2", sqlType:"blob"
//		m032Image3 column:"M032_Image3", sqlType:"blob"
//		m032Image4 column:"M032_Image4", sqlType:"blob"
//		m032Image5 column:"M032_Image5", sqlType:"blob"
		m032StaTampil column:"M032_StaTampil", sqlType:"char(1)"
	}
}
