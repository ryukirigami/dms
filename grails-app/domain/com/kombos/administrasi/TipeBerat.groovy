package com.kombos.administrasi

class TipeBerat {

    String m026Id
    String m026NamaTipeBerat
    double m026Berat1
    double m026Berat2
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m026NamaTipeBerat nullable: false,blank: false
        m026Berat1 nullable: false, blank: false, number:true
        m026Berat2 nullable: false, blank: false, numeric:true
        m026Id nullable: true,blank: true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    
    static mapping = {
        m026Id column: 'M026_ID', sqlType : "varchar(1)"
        m026NamaTipeBerat column: 'M026_NamaTipeBerat', sqlType: "varchar(50)"
        m026Berat1 column: 'M026_Berat1'
        m026Berat2 column: 'M026_Berat2'
        table "M026_TIPEBERAT"
    }

    String toString(){
        return m026NamaTipeBerat;
    }
}
