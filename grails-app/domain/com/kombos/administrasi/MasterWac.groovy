package com.kombos.administrasi

class MasterWac {

	int m403Id
	CompanyDealer companyDealer
	int m403Nomor
    String m403NamaPerlengkapan
    byte[] m403KondisiWAC
    Double m403JmlhMaksItemWAC
    String imageMime
	String m403StaGRBP
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m403Id column : 'M403_ID', sqlType : 'int'
		table 'M403_MASTERWAC'
		
		companyDealer column : 'M403_M011_ID'
		m403Nomor column : 'M403_Nomor', sqlType : 'int'
		m403NamaPerlengkapan column : 'M403_NamaPerlengkapan', sqlType : 'varchar(50)'

		m403StaGRBP column : 'M403_StaGR_BP', sqlType : 'char(1)'
	}
	
	String toString(){
		return m403NamaPerlengkapan
	}
    static constraints = {
		m403Id (nullable : true, blank : true, unique : false, maxSize : 4)
		companyDealer (nullable : true, blank : true)
		m403Nomor (nullable : false, blank : false, maxSize : 4)
        m403KondisiWAC (nullable : true, blank : true,maxSize: 10485760)
		m403NamaPerlengkapan (nullable : false, blank : false, maxSize : 50)
        m403JmlhMaksItemWAC(nullable : false, blank : true)
        imageMime(nullable : true)
		m403StaGRBP (nullable : true, blank : true, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
