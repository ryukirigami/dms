package com.kombos.administrasi

class StdTimeWorkItems {

	CompanyDealer companyDealer
	WorkItems workItems
	Date m041TanggalBerlaku
	double m041RdanR
	double m041Replace
	double m041Overhaul
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		companyDealer column : 'M041_M011_ID'
		workItems column : 'M041_M039_WorkID'
		m041TanggalBerlaku column : 'M041_TglBerlaku', sqlType : 'date'
		m041RdanR column : 'M041_RdanR'
        m041Replace column: 'M041_Replace'
		m041Overhaul column : 'M041_Overhaul'
		table 'M041_STDTIMEWORKITEMS'
	}
	
	String toString(){
		return m041TanggalBerlaku
	}
    static constraints = {
        workItems blank:false, nullable:false
        m041TanggalBerlaku blank:false, nullable:false
        m041RdanR blank:false, nullable:false
        m041Replace blank:false, nullable:false
        m041Overhaul blank:false, nullable:false
        companyDealer blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
