package com.kombos.administrasi
class WarnaVendor {

	VendorCat vendorCat
    String m192IDWarna
	String m192NamaWarna
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        vendorCat nullable:false, blank:false
        m192IDWarna nullaable:false, blank:false, maxSize:10//, unique:true
        m192NamaWarna nullable:false, blank:false, maxSize : 50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        staDel maxSize : 1, nullable : false, blank : false
    }

	static mapping = {
		vendorCat column : 'M192_M191_ID'
        m192IDWarna column : 'M192_IDWarna', sqltype : 'varchar', length : 10
		m192NamaWarna column : 'M192_NamaWarna', sqltype : 'varchar', length : 50
		staDel column : 'M192_StaDel', sqltype : 'char', length : 1
		table 'M192_WARNA'
	}

	public String toString() {
		return m192NamaWarna 
	}
}
