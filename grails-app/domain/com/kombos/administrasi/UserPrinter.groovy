package com.kombos.administrasi

import com.kombos.baseapp.sec.shiro.User
import com.kombos.maintable.MyTask

class UserPrinter {
    CompanyDealer companyDealer
	User userProfile
	MyTask myTask
    NamaDokumen namaDokumen
	String t005PathPrinter
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T005_USERPRINTER'
		namaDokumen column : 'T005_M007_IDNamaDokumen'
		userProfile column : 'T005_T001_IDUser'
		myTask column : 'T005_T007_ID'
		t005PathPrinter column : 'T005_PathPrinter', sqlType : 'varchar(50)'
		staDel column : 'T005_StaDel', sqlType : 'char(1)'
		
	}
	
    static constraints = {
        companyDealer (blank:true, nullable:true)
		userProfile (nullable : false, blank : false)
		myTask (nullable : true, blank : true)
        namaDokumen (nullable : false, blank : false)
		t005PathPrinter (nullable : false, blank : false, maxSize : 50)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return userProfile.toString()
	}
}
