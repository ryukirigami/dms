package com.kombos.administrasi

import com.kombos.customerprofile.StaImport

class FullModelCode {
    CompanyDealer companyDealer
	String t110FullModelCode
	KategoriKendaraan kategoriKendaraan
	BaseModel baseModel
	Stir stir
	ModelName modelName
	BodyType bodyType
	Gear gear
	Grade grade
	Engine engine
	Country country
	FormOfVehicle formOfVehicle
	String staDel	
	StaImport staImport
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	
	static constraints = {
        companyDealer blank:true, nullable:true
		kategoriKendaraan (nullable:  false, blank : false)
		baseModel (nullable:  false, blank : false)
		stir (nullable:  false, blank : false)
		modelName (nullable:  false, blank : false)
		bodyType (nullable:  false, blank : false)
		gear (nullable:  false, blank : false)
		grade (nullable:  false, blank : false)
		engine (nullable:  false, blank : false)
		country (nullable:  false, blank : false)
		formOfVehicle (nullable:  false, blank : false)
		staImport (nullable:  true, blank : true)
		t110FullModelCode (nullable:  true, blank : true)
		staDel (nullable:  false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T110_FULLMODELCODE'
		kategoriKendaraan column : 'T110_M101_ID'
		baseModel column : 'T110_M102_ID'
		stir column : 'T110_M103_ID'
		modelName column : 'T110_M104_ID'
		bodyType column : 'T110_M105_ID'
		gear column : 'T110_M106_ID'
		grade column : 'T110_M107_ID'
		engine column : 'T110_M108_ID'
		country column : 'T110_M109_ID'
		formOfVehicle column : 'T110_M114_ID'
		staImport column : 'T110_M100_ID'
		t110FullModelCode : 'T110_FullModelCode'
		staDel column : 'T110_StaDel'
	}

    String toString(){
        return t110FullModelCode
    }
}
