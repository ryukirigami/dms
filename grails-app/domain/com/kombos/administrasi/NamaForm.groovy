package com.kombos.administrasi

class NamaForm {
    CompanyDealer companyDealer
	String t004Id
	String t004NamaObjek
	String t004NamaAlias
	String t004JenisFR
	String t004StaAuditTrail
	String t004StaPerformanceLog
	String t004StaFreeze
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T004_NAMAFORM'
		
		t004Id column : 'T004_ID', sqlType : 'char(2)'
		t004NamaObjek column  : 'T004_NamaObjek', sqlType : 'varchar(50)'
		t004NamaAlias column : 'T004_NamaAlias', sqlType : 'varchar(50)'
		t004JenisFR column : 'T004_JenisFR', sqlType : 'char(1)'
		t004StaAuditTrail column : 'T004_StaAuditTrail', sqlType : 'char(1)'
		t004StaPerformanceLog column : 'T004_StaPerformanceLog', sqlType : 'char(1)'
		t004StaFreeze column : 'T004_StaFreeze', sqlType : 'char(1)'
		staDel column : 'T004_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t004Id nullable : false, blank : true, maxSize : 2, unique : true
		t004NamaObjek nullable : false, blank : true, maxSize : 50
		t004NamaAlias nullable : false, blank : false, maxSize : 50
		t004JenisFR nullable : false, blank : true, maxSize : 1
		t004StaAuditTrail nullable : false, blank : false, maxSize : 1
		t004StaPerformanceLog nullable : false, blank : false, maxSize : 1
		t004StaFreeze nullable : false, blank : false, maxSize : 1
		staDel nullable : false, blank : true, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t004NamaObjek
	}
}
