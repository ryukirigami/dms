package com.kombos.administrasi

import com.kombos.baseapp.sec.shiro.User

import java.sql.Time

class PerformanceLog {
	
	Date m778Tanggal
	String m778Id
	User userProfile
	String m778NamaActivity
	Time m778TglJamClick
	Date m778TglJamTampil
	Integer m778DurasiDetik
	String m778xNamaUser
	String m778xNamaDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M778_PERFORMANCELOG'
		m778Tanggal column : 'M778_Tanggal', sqlType : 'date'
		m778Id column : 'M778_ID', sqlType : 'char(10)'
		userProfile column : 'M778_T001_IDUser'
		m778NamaActivity column : 'M778_NamaActivity', sqlType : 'varchar(50)'
		m778TglJamClick column : 'M778_JamClick', sqlType : 'date'
		m778TglJamTampil column : 'M778_JamTampil', sqlType : 'date'
		m778DurasiDetik column : 'M778_DurasiDetik', sqlType : 'int'
		m778xNamaUser column : 'M778_xNamaUser', sqlType : 'varchar(50)'
		m778xNamaDivisi column : 'M778_xNamaDivisi', sqlType : 'varchar(50)' 
		staDel column : 'M778_StaDel', sqlType : 'char(1)'
	}
    static constraints = {
		m778Tanggal (nullable : false, blank : false)
		m778Id (nullable : true, blank : true)
		userProfile (nullable : true, blank : true)
		m778NamaActivity (nullable : false, blank : false, maxSize : 50)
		m778TglJamClick (nullable : true, blank : true)
		m778TglJamTampil (nullable : true, blank : true)
		m778DurasiDetik (nullable : true, blank : true, maxSize : 4)
		m778xNamaUser (nullable : true, blank : true, maxSize : 50)
		m778xNamaDivisi (nullable : true, blank : true, maxSize : 50)
		staDel (nullable : true, blank : true, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
