package com.kombos.administrasi

import com.kombos.baseapp.sec.shiro.User

class ErrorLog {

	Date m779Tanggal
	Integer m779Id
    Date m779Jam
    User userProfile
	NamaForm namaForm
	String m779Event
	String m779xNamaUser
	String m779xNamaDivisi
	String m779ErrorMsg
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M779_ERRORLOG'
		m779Jam sqlType: "date"
		m779Tanggal column : 'M779_Tanggal'
		m779Id column : 'M779_ID', sqlType : 'int'
		namaForm column : 'M779_T004_ID'
		m779Event column : 'M779_Event', sqlType : 'varchar(50)'
		m779xNamaUser column : 'M779_xNamaUser', sqlType : 'varchar(20)'
		m779xNamaDivisi column : 'M779_xNamaDivisi', sqlType : 'varchar(20)'
		m779ErrorMsg column : 'M779_ErrorMsg', sqlType : 'varchar(255)'
		staDel column : 'M779_StaDel', sqlType : 'char(1)'
	}
	
	
    static constraints = {
		m779Tanggal (nullable : false, blank : false)
        m779Jam (nullable : true, blank : true)
        userProfile (nullable : false, blank : false)
        namaForm (nullable : true, blank : true)
        m779Event (nullable : true, blank : true, maxSize : 50)
        m779ErrorMsg (nullable : true, blank : true, maxSize : 255)
        m779Id (nullable : true, blank : true)
		m779xNamaUser (nullable : true, blank : true, maxSize : 20)
		m779xNamaDivisi (nullable : true, blank : true, maxSize : 20)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
}
