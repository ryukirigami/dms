package com.kombos.administrasi

import java.util.Date;

class KodeKotaNoPol {
	String m116ID
	String m116NamaKota
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
		m116ID blank:true, nullable:true, maxSize:2, unique:false
		m116NamaKota blank:false, nullable:false, maxSize:50
		staDel blank:false, nullable:false, maxSize:1
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table "M116_KODEKOTANOPOL"
		m116ID column:"M116_ID", sqlType:"varchar(2)"
		m116NamaKota column:"M116_NamaKota", sqlType:"varchar(50)"
		staDel column:"M116_StaDel", sqlType:"char(1)"
	}
	
	String toString() {
		return m116ID
	}
}
