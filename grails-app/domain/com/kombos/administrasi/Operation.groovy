package com.kombos.administrasi

import com.kombos.parts.StatusApproval

class Operation {

    String m053JobsId
    Section section
	Serial serial
    String m053Id
    String m053NamaOperation
	String m053StaPaket
	KategoriJob kategoriJob
	String m053StaPaint
	String m053StaLift
	String m053Ket
	String staDel
    Double m053Km = 0.0
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m053Km column: "M053_Km"
        section column : 'M053_M051_ID'
        serial column : 'M053_M052_ID'
        m053JobsId column : 'M053_JobID'//, sqlType : 'varchar(7)'
        m053Id column : 'M053_ID', sqlType : 'varchar(7)'
		m053NamaOperation column : 'M053_NamaOperation', sqlType : 'varchar(255)'
        m053StaLift column : 'M053_StaLift', sqlType : 'varchar(1)'
        m053StaPaket column : 'M053_StaPaket', sqlType : 'varchar(1)'
        kategoriJob column : 'M053_M055_ID'
		m053StaPaint column : 'M053_StaPaint', sqlType : 'varchar(1)'
		m053Ket column : 'M053_ket', sqlType : 'varchar(255)'
		staDel column : 'M053_StaDel', sqlType : 'varchar(1)'
        table 'M053_OPERATION'
	}
	
	public String toString() {
		return m053JobsId+" . "+serial?.m052NamaSerial+" - "+m053NamaOperation
	}

    static constraints = {
        m053Km blank:true, nullable:true
        section blank:true, nullable:true
        serial blank:true, nullable:true
        m053Id blank:true, nullable:true//, maxSize: 7
        m053NamaOperation blank:true, nullable:true, maxSize: 255
        m053StaLift blank:true, nullable:true
        m053StaPaket blank:true, nullable:true
        kategoriJob blank:true, nullable:true
        m053StaPaint blank:true, nullable:true
        m053Ket blank:true, nullable:true,maxSize: 255
        m053JobsId blank:true, nullable:true
        staDel blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}