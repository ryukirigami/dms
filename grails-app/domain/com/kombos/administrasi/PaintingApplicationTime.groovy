package com.kombos.administrasi

class PaintingApplicationTime {

	CompanyDealer companyDealer
	Date m044TglBerlaku
	MasterPanel masterPanel
	double m044NewMultiple
	double m044NewUnit
	double m044RepMultiple
	double m044RepUnit
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        companyDealer column : 'M044_M011_ID'
		m044TglBerlaku column : 'M044_TglBerlaku', sqlType : 'date'
        masterPanel column : 'M044_M094_ID'
		m044NewMultiple column : 'M044_NewMultiple'
		m044NewUnit column : 'M044_NewUnit'
		m044RepMultiple column : 'M044_RepMultiple'
		m044RepUnit column : 'M044_RepUnit'
		table 'M044_PAINTINGAPPLICATIONTIME'
	}

	String toString() {
		return m044NewMultiple
	}
	
    static constraints = {
        m044TglBerlaku blank:false, nullable:false
        masterPanel blank:false, nullable:false
        m044NewMultiple blank:true, nullable:true
        m044NewUnit blank:true, nullable:true
        m044RepMultiple blank:true, nullable:true
        m044RepUnit blank:true, nullable:true
        companyDealer blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
