package com.kombos.administrasi

class Grade {
    Integer m107ID
    BaseModel baseModel
    ModelName modelName
    BodyType bodyType
    Gear gear
    String m107KodeGrade
    String m107NamaGrade
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        m107ID blank:true, nullable:true
        baseModel blank:false, nullable:false
        modelName blank:false, nullable:false
        bodyType blank:false, nullable:false
        gear blank:false, nullable:false
        m107KodeGrade blank:false, nullable:false, maxSize:2
        m107NamaGrade blank:false, nullable:false, maxSize:20
        staDel blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
    static mapping = {
        table "M107_GRADE"
        m107ID column:"M107_ID", sqlType:"int"
        baseModel column:"M107_M102_ID"
        modelName column:"M107_M104_ID"
        bodyType column:"M107_M105_ID"
        gear column:"M107_M106_ID"
        m107KodeGrade column:"M107_KodeGrade", sqlType:"varchar(2)"
        m107NamaGrade column:"M107_NamaGrade", sqlType:"varchar(20)"
        staDel column:"M107_StaDel", sqlType:"char(1)"
    }
        
    String toString(){
        return m107KodeGrade
    }
}
