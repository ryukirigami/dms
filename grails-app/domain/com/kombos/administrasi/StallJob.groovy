package com.kombos.administrasi

class StallJob {

	Stall stall
	Operation operation
	Date m024Tanggal
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	
	static mapping = {
        autoTimestamp false
        stall column : 'M024_M022_StallID'//, unique: true
        operation column : 'M024_M053_JobID'//, unique: true
		m024Tanggal column : 'M024_Tanggal', sqlType : 'date'//, unique: true
		staDel column : 'M024_StaDel', sqlType : 'char(1)'
		table 'M024_STALLJOB'
	}
	
	
	String toString(){
		return operation.m053NamaOperation
	}
	
    static constraints = {
        stall nullable: false, blank:false
        m024Tanggal nullable: false, blank:false
        operation nullable: false, blank:false
        staDel nullable: false, blank:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
