package com.kombos.administrasi

import com.kombos.administrasi.NamaDokumen
import com.kombos.baseapp.sec.shiro.User

class HistoryPrint {
    CompanyDealer companyDealer
    NamaDokumen namaDokumen
    String t951NoDokumen
    Integer t951PrintCounter
    Date t951Tanggal
    User userProfile
    String t951Alasan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer blank:true, nullable:true
        namaDokumen nullable: false, blank : false
        t951NoDokumen nullable: false, blank: false, maxSize: 50
        t951PrintCounter nullable: false, blank : false
        t951Tanggal nullable: false, blank : false
        userProfile nullable: false,blank : false
        t951Alasan nullable: false, blank: false, maxSize: 50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        table "T951_HISTORYPRINT"
        namaDokumen column: "T951_M007_ID"
        t951NoDokumen column: "T951_NoDokumen", sqlType: "varchar(50)"
        t951PrintCounter column: "T951_PrintCounter", sqlType: "int"
        t951Tanggal column: "T951_Tanggal"//, sqlType: 'date'
        userProfile column: "T951_T001_ID"
        t951Alasan column: "T951_Alasan", sqlType: "varchar(50)"
    }
}
