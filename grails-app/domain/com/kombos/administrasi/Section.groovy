package com.kombos.administrasi

import com.kombos.customerprofile.Warna

class Section {
    String m051ID
    String m051NamaSection
    String m051WarnaSection
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m051ID column : 'M051_ID', sqlType : 'varchar(3)',unique: true
        m051WarnaSection column : 'M051_WarnaSection', sqlType : 'varchar(10)'
		m051NamaSection column : 'M051_NamaSection', sqlType : 'varchar(50)'
		staDel column : 'M051_StaDel', sqlType : 'char(1)'
		table 'M051_SECTION'
	}
	
	public String toString() {
		return m051NamaSection
	}

	static constraints = {
        m051ID blank:false, nullable:false
        m051NamaSection blank:false, nullable:false
        m051WarnaSection blank:false, nullable:false
        staDel blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
