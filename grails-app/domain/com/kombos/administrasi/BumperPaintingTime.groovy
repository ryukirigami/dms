package com.kombos.administrasi

class BumperPaintingTime {

	CompanyDealer companyDealer
	Date m045TglBerlaku
	MasterPanel masterPanel
	double m0452New
	double m0452Def
	double m0452Scr
	double m0453New
	double m0453Def
    double m0453Scr
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        companyDealer column : 'M045_M011_ID'
        m045TglBerlaku column: 'M045_TglBerlaku'//, sqlType: 'date'
        masterPanel column : 'M045_M094_ID'
		m0452New column : 'M045_2New'
		m0453New column : 'M045_3New'
		m0452Def column : 'M045_2Def'
		m0453Def column : 'M045_3Def'
		m0452Scr column : 'M045_2Scr'
		m0453Scr column : 'M045_3Scr'
		table 'M045_BUMPERPAINTINGTIME'
	}
	
	public String toString() {
		return m0452New
	}

    static constraints = {

        m045TglBerlaku blank:false, nullable:false
        masterPanel blank:false, nullable:false
        m0452New blank:true, nullable:true
        m0453New blank:true, nullable:true
        m0452Def blank:true, nullable:true
        m0453Def blank:true, nullable:true
        m0452Scr blank:true, nullable:true
        m0453Scr blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        companyDealer blank:true, nullable:true
    }
}
