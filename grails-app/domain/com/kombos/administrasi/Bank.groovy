package com.kombos.administrasi

import com.kombos.finance.BankAccountNumber
import com.kombos.finance.BankReconciliation

class Bank {

    String m702ID
    String m702NamaBank
    String m702Cabang
    String m702Alamat
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [bankReconciliation: BankReconciliation, bankAccountNumber: BankAccountNumber]

    static constraints = {
        m702ID blank:false, nullable : false, maxSize : 4
        m702NamaBank blank:false, nullable : false, maxSize : 50
        m702Cabang blank:false, nullable : false, maxSize : 50
        m702Alamat blank:false, nullable : false, maxSize : 100
        staDel blank:false, nullable : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    
    static mapping = {
        autoTimestamp false
        m702ID column: 'M702_ID'
        m702NamaBank column: 'M702_NamaBank'
        m702Cabang column: 'M702_Cabang'
        m702Alamat column: 'M702_Alamat'
        staDel column: 'M702_staDel', sqlType: "char(1)"
        table "M702_BANK"
        namaBank formula: "concat(M702_NamaBank,'-',M702_Cabang)"
    }

    String toString(){
        return m702NamaBank + " - " + m702Cabang;
    }
}
