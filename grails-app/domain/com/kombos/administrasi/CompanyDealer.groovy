package com.kombos.administrasi

import com.kombos.finance.AccountNumber

class CompanyDealer {

    String m011ID
    String m011NamaWorkshop
    String m011Alamat
    Provinsi provinsi
    KabKota kabKota
    Kecamatan kecamatan
    Kelurahan kelurahan
    JenisDealer jenisDealer
    Bank bank
    AccountNumber m011NomorAkunTransfer
    AccountNumber m011NomorAkunLainnya
    Integer m011SelisihWaktu
    String m011Telp
    String m011OutletCode
    String m011TelpExt
    String m011Email
    String m011EmailComplainSerius
    String m011EmailComplainTdkSerius
    String m011Fax
    String m011FaxExt
    String m011NPWP
    String m011NamaNpwp
    String m011AlamatNpwp
    String m011NomorRekening
    String m011AtasNama
    String m011StaBPCenter
    String m011Ket
    byte[] m011Logo
    String m011PKP
    String staDel
    String imageMime
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m011ID (nullable : false, blank : false, maxSize : 3)
        staDel(nullable : true)
        m011NomorAkunTransfer(nullable : true,blank:true)
        m011OutletCode(nullable : true,blank:true)
        m011NomorAkunLainnya(nullable : true,blank:true)
        m011NamaWorkshop (nullable : false, blank : false, maxSize : 25)
        jenisDealer (nullable : false, blank : false)
        m011Alamat (nullable : false, blank : false, maxSize : 1000)
        provinsi (nullable : false, blank : false)
        kabKota (nullable : false, blank : false)
        kecamatan (nullable : false, blank : false)
        kelurahan (nullable : false, blank : false)
        m011Fax (nullable : true, blank : true,maxSize : 20)
        m011Telp (nullable : true, blank : true,maxSize : 20)
        m011Email (nullable : true, blank : true,maxSize : 50,email: true)
        m011NPWP (nullable : false, blank : false,maxSize : 25)
        m011NamaNpwp (nullable : false, blank : false,maxSize : 50)
        m011AlamatNpwp (nullable : false, blank : false,maxSize : 1000)
        m011Logo (nullable : true, blank : false,maxSize: 10485760) //10mb
        imageMime (nullable : true, blank : false)
        m011NomorRekening (nullable : true, blank : true,maxSize : 50)
        bank (nullable : true, blank : true)
        m011AtasNama (nullable : true, blank : true,maxSize : 50)
        m011Ket (nullable : true, blank : true,maxSize : 20)
        m011EmailComplainSerius (nullable : true, blank : true,maxSize : 50)
        m011EmailComplainTdkSerius (nullable : true, blank : true,maxSize : 50)
        m011StaBPCenter (nullable : true, blank : true)
        m011TelpExt nullable : true, blank : true
        m011FaxExt nullable : true, blank : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        m011SelisihWaktu nullable: true, blank : true
    }
    
    static mapping = {
        autoTimestamp false
        table "M011_COMPANYTAM"
        m011ID column : "M011_ID" //, sqlType : "char(4)"
        m011NamaWorkshop column : "M011_NamaCompanyTAM",sqlType: "varchar(25)"
        m011Alamat column : "M011_Alamat ",sqlType: "varchar(100)"
        provinsi column : "M011_M001_ID"
        kabKota column : "M011_M002_ID"
        kecamatan column : "M011_M003_ID"
        kelurahan column : "M011_M004_ID"
        jenisDealer column : "M011_M005_ID"
        m011Telp column : "M011_Telp",sqlType: "varchar(20)"
        m011Email column : "M011_Email",sqlType: "varchar(50)"
        m011EmailComplainSerius column : "M011_EmailComplainSerius",sqlType: "varchar(50)"
        m011EmailComplainTdkSerius column : "M011_EmailComplainTdkSerius",sqlType: "varchar(50)"
        m011Fax column : "M011_Fax ",sqlType: "varchar(20)"
        m011NPWP column : "M011_NPWP",sqlType: "varchar(25)"
        m011NamaNpwp column : "M011_NamaNPWP",sqlType: "varchar(50)"
        m011AlamatNpwp column : "M011_AlamatNPWP",sqlType: "varchar(255)"
        bank column : "M011_M702_ID"
        m011NomorRekening column : "M011_NomorRekening",sqlType: "varchar(50)"
        m011AtasNama column : "M011_AtasNama",sqlType: "varchar(50)"
        m011StaBPCenter column : "M011_StaBPCenter ",sqlType: "char(1)"
        m011Ket column : "M011_Ket",sqlType: "varchar(20)"
        m011Logo column : "M011_Logo",sqlType: "blob"
        m011PKP column : "M011_PKP"//,sqlType: "text"
        m011NomorAkunTransfer column : "M011_NOMOR_AKUN_TRANSFER"
        m011NomorAkunLainnya column : "M011_NOMOR_AKUN_LAINNYA"
        staDel column : "M011_StaDel",sqlType: "char(1)"
        m011SelisihWaktu column: "M011_SELISIHWAKTU"
    }

    String toString(){
        return m011NamaWorkshop;
    }
}
