package com.kombos.administrasi

class MasterPanel {
    String m094ID
	String m094NamaPanel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m094NamaPanel nullable:false, blank:false, maxSize: 50
        m094ID nullable:true, blank:true, maxSize: 4
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
		table "M094_MASTERPANEL"
        m094ID column:"M094_ID", sqlType:'varchar(4)'//, unique: true
		m094NamaPanel column:"M094_NamaPanel", sqlType:"varchar(50)", unique: true
	}
	
	String toString(){
		return m094NamaPanel
	}
}
