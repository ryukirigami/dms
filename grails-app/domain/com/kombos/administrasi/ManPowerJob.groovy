package com.kombos.administrasi

class ManPowerJob {
    CompanyDealer companyDealer
	NamaManPower namaManPower
	Operation operation
	Sertifikat sertifikat
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'T019_MANPOWERJOB'

		namaManPower column : 'T020_T015_IDManPower'
		operation column : 'T020_M053_JobID'
		sertifikat column : 'T020_M016_ID'
		staDel column : 'T020_StaDel', sqlType : 'char(1)'
	}


    static constraints = {
        companyDealer blank:true, nullable:true
		namaManPower (nullable : false, blank : false)
		operation (nullable : false, blank : false)
		sertifikat (nullable : true, blank : true)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	String toString(){
		return namaManPower.toString()
	}
}
