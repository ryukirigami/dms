package com.kombos.administrasi

class VendorAsuransi {

	String m193ID
	String m193Nama
	String m193Alamat
	String m193Npwp
	String m193NoTelp
	String m193Email
	Double m193JmlToleransiAsuransiBP
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
    	
		m193Nama (nullable : false, blank : false, maxSize : 50)
		m193Alamat (nullable : false, blank : false, maxSize : 255)
		m193Npwp (nullable : false, blank : false, maxSize : 50)
		m193NoTelp (nullable : false, blank : false, maxSize : 50)
        m193Email (nullable:false, blank:false, email:true, maxSize:50)
        m193JmlToleransiAsuransiBP (nullable:false, blank:false)
		m193ID (nullable : true, blank : true, maxSize : 3)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }
	
	static mapping = {
        autoTimestamp false
        m193ID column : 'M193_ID', sqlType : 'char(3)'
		m193Nama column : 'M193_Nama', sqltype : 'varchar(50)'
		m193Alamat column : 'M193_Alamat', sqltype : 'varchar(255)'
		m193Npwp column : 'M193_NPWP', sqltype : 'varchar(50)'
		m193NoTelp column : 'M193_NoTelp', sqltype : 'varchar(50)'
        m193Email column : 'M193_Email', sqltype : 'varchar(50)'
        m193JmlToleransiAsuransiBP column:'M193_JmlToleransiAsuransiBP'//, sqlType: 'double'
		staDel column : 'M193_StaDel', sqltype : 'char(1)'
		table 'M193_VENDORASURANSI'
	}
	
	String toString() {
		return m193Nama
	}
	
    
}
