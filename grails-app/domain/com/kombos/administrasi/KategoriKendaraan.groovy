package com.kombos.administrasi

import java.util.Date;

//TABEL KATEGORI M101

class KategoriKendaraan {
    Integer m101ID
    String m101KodeKategori
    String m101NamaKategori
    String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
        m101ID nullable: true, blank:true
        m101KodeKategori nullable: false, blank:false, maxSize: 10
        m101NamaKategori nullable: false, blank:false, maxSize: 20
        staDel nullable: false, blank:false, maxSize: 1
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        autoTimestamp false
        m101ID column : "M101_ID", sqlType:"int",unique: true
        m101KodeKategori column: "M101_KodeKategori", sqlType:"varchar(10)"
        m101NamaKategori column: "M101_NamaKategori", sqlType:"varchar(20)"
        staDel column: "M101_StaDel", sqlType:"varchar(1)"
        table "M101_KATEGORI"
    }

    String toString(){
        return m101KodeKategori+" - "+m101NamaKategori;
    }
}
