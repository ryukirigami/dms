package com.kombos.administrasi

class SertifikatJobMap {
    CompanyDealer companyDealer
	    Sertifikat sertifikat
        Section section
	    Operation operation
	    String staDel
        Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
        String createdBy //Menunjukkan siapa yang buat isian di baris ini.
        Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
        String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
        String lastUpdProcess
	
	static mapping = {
        autoTimestamp false
        table 'T018_SERTIFIKATJOBMAP'
		sertifikat column : 'T018_M016_ID'
        section column : 'T018_M053_M051_Section'
		operation column : 'T018_M053_JobID'
		staDel column : 'T018_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
        sertifikat (nullable : false, blank : false)
        section (nullable : false, blank : false)
		operation (nullable : false, blank : false)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return sertifikat.toString()
	}
}
