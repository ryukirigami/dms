package com.kombos.administrasi

class MappingStallTipeBerat {
	
	TipeBerat tipeBerat
	Stall stall
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		stall column : 'M027_M022_StallID'
        tipeBerat column: 'M027_M026_ID'
        table "M027_MAPPINGSTALLTIPEBERAT"
    }

    static constraints = {
        stall nullable : false, blank:false, unique: ['tipeBerat']
        tipeBerat nullable : false, blank:false, unique: ['stall']
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	String toString(){
		return tipeBerat.m026NamaTipeBerat;
	}
}
