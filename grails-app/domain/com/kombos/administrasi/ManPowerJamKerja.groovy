package com.kombos.administrasi

class ManPowerJamKerja {
    CompanyDealer companyDealer
    NamaManPower namaManPower
    Date t022Tanggal
    Date t022TanggalAkhir
    Date t022JamMulai1
    Date t022JamSelesai1
    Date t022JamMulai2
    Date t022JamSelesai2
    Date t022JamMulai3
    Date t022JamSelesai3
    Date t022JamMulai4
    Date t022JamSelesai4
    String t022xNamaUser
    String t022xNamaDivisi
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        autoTimestamp false
        table 'T022_MANPOWERJAMKERJA'
        namaManPower column: 'T022_T015_IDManPower'
        t022Tanggal column: 'T022_Tanggal'//, sqlType: 'date'
        t022TanggalAkhir column: 'T022_TanggalAkhir'//, sqlType: 'date'
        t022JamMulai1 column: 'T022_JamMulai1'//, sqlType: 'date'
        t022JamSelesai1 column: 'T022_JamSelesai1'//, sqlType: 'date'
        t022JamMulai2 column: 'T022_JamMulai2'//, sqlType: 'date'
        t022JamSelesai2 column: 'T022_JamSelesai2'//, sqlType: 'date'
        t022JamMulai3 column: 'T022_JamMulai3'//, sqlType: 'date'
        t022JamSelesai3 column: 'T022_JamSelesai3'//, sqlType: 'date'
        t022JamMulai4 column: 'T022_JamMulai4'//, sqlType: 'date'
        t022JamSelesai4 column: 'T022_JamSelesai4'//, sqlType: 'date'
        t022xNamaUser column: 'T022_xNamaUser', sqlType: 'varchar(20)'
        t022xNamaDivisi column: 'T022_xNamaDivisi', sqlType: 'varchar(20)'
        staDel column: 'T022_StaDel', sqlType: 'char(1)'

    }


    static constraints = {
        companyDealer blank:true, nullable:true
        namaManPower(nullable: false, blank: false)
        t022Tanggal(nullable: false, blank: false)
        t022TanggalAkhir(nullable: false, blank: false)
        t022JamMulai1(nullable: false, blank: false)
        t022JamSelesai1(nullable: false, blank: false)
        t022JamMulai2(nullable: false, blank: false)
        t022JamSelesai2(nullable: false, blank: false)
        t022JamMulai3(nullable: true, blank: true)
        t022JamSelesai3(nullable: true, blank: true)
        t022JamMulai4(nullable: true, blank: true)
        t022JamSelesai4(nullable: true, blank: true)
        t022xNamaUser(nullable: true, blank: true, maxSize: 20)
        t022xNamaDivisi(nullable: true, blank: true, maxSize: 20)
        staDel(nullable: false, blank: false, maxSize: 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    String toString() {
        return namaManPower
    }
}
