package com.kombos.administrasi

class UserAccess {
	
	CompanyDealer companyDealer
	String t002Id
	UserRole userRole
	NamaForm namaForm
	String t002StaBaca
	String t002StaTambah
	String t002StaUbah
	String t002StaHapus
	String t002StaEksekusi
	String t002StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	
	static mapping = {
		table 'T002_USERACCESS'
		
		t002Id column : 'T002_ID', sqlType : 'char(3)'
		companyDealer column : 'T002_M011_ID'
		userRole column : 'T002_T003_ID'
		namaForm column : 'T002_T004_ID'
		t002StaBaca column : 'T002_StaBaca', sqlType : 'char(1)'
		t002StaTambah column : 'T002_StaTambah', sqlType : 'char(1)'
		t002StaUbah column : 'T002_StaUbah', sqlType : 'char(1)'
		t002StaHapus column : 'T002_StaHapus', sqlType : 'char(1)'
		t002StaEksekusi column : 'T002_StaEksekusi', sqlType : 'char(1)'
		t002StaDel column : 'T002_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		t002Id (nullable : false, blank : false, maxSize : 3, unique : true)
		companyDealer (nullable : false, blank : false)
		userRole (nullable : false, blank : false)
		namaForm (nullable : false, blank : false)
		t002StaBaca (nullable : false, blank : false, maxSize : 1)
		t002StaTambah (nullable : false, blank : false, maxSize : 1)
		t002StaUbah (nullable : false, blank : false, maxSize : 1)
		t002StaHapus (nullable : false, blank : false, maxSize : 1)
		t002StaEksekusi (nullable : false, blank : false, maxSize : 1)
		t002StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t002Id
	}
}
