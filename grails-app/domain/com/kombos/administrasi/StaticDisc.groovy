package com.kombos.administrasi


class StaticDisc {

	CompanyDealer companydealer
	Date m019TMT
	Double m019BookingSBJasa
	Double m019BookingSBPart
	Double m019BookingGRBPJasa
	Double m019BookingGRBPPart
	Double m019NonBookingSBJasa
	Double m019NonBookingSBPart
	Double m019NonBookingGRBPJasa
	Double m019NonBookingGRBPPart
	String m019StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companydealer nullable : false, blank : false
        m019TMT nullable : false, blank : false
        m019BookingSBJasa nullable : true, blank : true
        m019BookingSBPart nullable : true, blank : true
        m019BookingGRBPJasa nullable : true, blank : true
        m019BookingGRBPPart nullable : true, blank : true
        m019NonBookingSBJasa nullable : true, blank : true
        m019NonBookingSBPart nullable : true, blank : true
        m019NonBookingGRBPJasa nullable : true, blank : true
        m019NonBookingGRBPPart nullable : true, blank : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
		companydealer column : 'M018_M001_ID'//, unique: true
		m019TMT column : 'M019_TMT', sqlType : 'date'//, unique: true
		m019BookingSBJasa column : 'M019_BookingSBJasa'
		m019BookingSBPart column : 'M019_BookingSBPart'
		m019BookingGRBPJasa column : 'M019_BookingGRBPJasa'
		m019BookingGRBPPart column : 'M019_BookingGRBPPart'
		m019NonBookingSBJasa column : 'M019_NonBookingSBJasa'
		m019NonBookingSBPart column : 'M019_NonBookingSBPart'
		m019NonBookingGRBPJasa column : 'M019_NonBookingGRBPJasa'
		m019NonBookingGRBPPart column : 'M019_NonBookingGRBPPart'
		table 'M019_STATICDISC'
	}

	String toString() {
        return m019TMT
	}
	def beforeUpdate() {
		lastUpdated = new Date();
//		lastUpdProcess = "update"
        if(m019StaDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "insert"
//		lastUpdated = new Date()
		m019StaDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!m019StaDel)
			m019StaDel = "0"
		if(!createdBy){
			createdBy = "_SYSTEM_"
			updatedBy = createdBy
		}
	}
}