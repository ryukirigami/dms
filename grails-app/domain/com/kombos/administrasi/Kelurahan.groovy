package com.kombos.administrasi

class Kelurahan {

    Provinsi provinsi
    KabKota kabkota
    Kecamatan kecamatan
    String m004ID
    String m004NamaKelurahan
    String m004KodePos
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        provinsi (nullable : false, blank : false)
        kabkota (nullable : false, blank : false)
        kecamatan (nullable : false, blank : false)
        m004NamaKelurahan (nullable : false, blank : false, maxSize : 40)
		m004ID (nullable : false, blank : false, maxSize : 5)
        m004KodePos (nullable : false, blank : false, maxSize : 5)
        staDel (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping={
        m004ID column : "M004_ID" , sqlType : "char(5)"
		provinsi column : "M004_M001_ID"
        kabkota column : "M004_M002_ID"
        kecamatan column : "M004_M003_ID"
        m004NamaKelurahan column: "M004_NamaKelurahan", sqlType: "varchar(40)"
        m004KodePos column : "M004_KodePos", sqlType: "varchar(5)"
        staDel column : "M004_StaDel", sqlType : "char(1)"
        table "M004_KELURAHAN"
    }

    String toString(){
        return m004NamaKelurahan;
    }
}
