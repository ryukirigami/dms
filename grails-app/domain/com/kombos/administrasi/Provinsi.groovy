package com.kombos.administrasi

class Provinsi {

    String m001ID
    String m001NamaProvinsi
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

   // static hasMany = [provinsi : Provinsi]
    static constraints = {
        m001ID (nullable : true, blank : true, maxSize : 2)
        m001NamaProvinsi (nullable : false, blank : false, maxSize : 25, unique :false)
        staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    
    static mapping ={
        m001ID column: 'M001_ID', sqlType: "varchar(2)", unique: true
        m001NamaProvinsi column: 'M001_NamaProvinsi', sqlType: "varchar(25)"
        staDel column: 'M001_StaDel', sqlType: "char(1)"
        table 'M001_PROVINSI'
    }

    String toString(){
        return m001NamaProvinsi;
    }
}

