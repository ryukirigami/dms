package com.kombos.administrasi

class TargetBP {
	CompanyDealer companyDealer
	Date m036TglBerlaku
	Double m036TargetSales
	Double m036TargetQty
	Double m036COGSPart
	Double m036COGSOil
	Double m036COGSSublet
	Double m036COGSLabor
	Double m036COGSOverHead
	Double m036COGSDeprecation
	Double m036COGSTotalSGA
	Double m036TotalClaimItem
	Double m036TotalClaimAmount
	Double m036TechReport
	Double m036TWCLeadTime
	Double m036YTDS
	Double m036YTDD
	Double m036ThisMonthS
	Double m036ThisMonthD
	Double m036Q1S
	Double m036Q1D
	Double m036Q2S
	Double m036Q2D
	Double m036Q3S
	Double m036Q3D
	Double m036Q4S
	Double m036Q4D
	Double m036Q5S
	Double m036Q5D
	Double m036Q6S
	Double m036Q6D
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    String staDel

    static constraints = {
		m036TglBerlaku blank:false, nullable:false
		companyDealer blank:false, nullable:false
        m036TargetSales nullable: true, blank: true
        m036TargetQty nullable: true, blank: true
        m036COGSPart nullable: true, blank: true
        m036COGSOil nullable: true, blank: true
        m036COGSSublet nullable: true, blank: true
        m036COGSLabor nullable: true, blank: true
        m036COGSOverHead nullable: true, blank: true
        m036COGSDeprecation nullable: true, blank: true
        m036COGSTotalSGA nullable: true, blank: true
        m036TotalClaimItem nullable: true, blank: true
        m036TotalClaimAmount nullable: true, blank: true
        m036TechReport nullable: true, blank: true
        m036TWCLeadTime nullable: true, blank: true
        m036YTDS nullable: true, blank: true
        m036YTDD nullable: true, blank: true
        m036ThisMonthS nullable: true, blank: true
        m036ThisMonthD nullable: true, blank: true
        m036Q1S nullable: true, blank: true
        m036Q1D nullable: true, blank: true
        m036Q2S nullable: true, blank: true
        m036Q2D nullable: true, blank: true
        m036Q3S nullable: true, blank: true
        m036Q3D nullable: true, blank: true
        m036Q4S nullable: true, blank: true
        m036Q4D nullable: true, blank: true
        m036Q5S nullable: true, blank: true
        m036Q5D nullable: true, blank: true
        m036Q6S nullable: true, blank: true
        m036Q6D nullable: true, blank: true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        staDel maxSize : 1, nullable : false, blank : false
    }
	
	static mapping ={
		table "M036_TARGETBP"
		companyDealer column : "M036_M011_ID"
		m036TglBerlaku column:"M036_TglBerlaku", sqlType : "date", unique: true
		m036TargetSales column:"M036_TargetSales"
		m036TargetQty column:"M036_TargetQty"
		m036COGSPart column:"M036_COGSPart"
		m036COGSOil column:"M036_COGSOil"
		m036COGSSublet column:"M036_COGSSublet"
		m036COGSLabor column:"M036_COGSLabor"
		m036COGSOverHead column:"M036_COGSOverHead"
		m036COGSDeprecation column:"M036_COGSDeprecation"
		m036COGSTotalSGA column:"M036_COGSTotalSGA"
		m036TotalClaimItem column:"M036_TotalClaimItem"
		m036TotalClaimAmount column:"M036_TotalClaimAmount"
		m036TechReport column:"M036_TechReport"
		m036TWCLeadTime column:"M036_TWCLEadTime"
		m036YTDS column:"M036_YTDS"
		m036YTDD column:"M036_YTDD"
		m036ThisMonthS column:"M036_ThisMonthS"
		m036ThisMonthD column:"M036_ThisMonthD"
		m036Q1S column:"M036_Q1S"
		m036Q1D column:"M036_Q1D"
		m036Q2S column:"M036_Q2S"
		m036Q2D column:"M036_Q2D"
		m036Q3S column:"M036_Q3S"
		m036Q3D column:"M036_Q3D"
		m036Q4S column:"M036_Q4S"
		m036Q4D column:"M036_Q4D"
		m036Q5S column:"M036_Q5S"
		m036Q5D column:"M036_Q5D"
		m036Q6S column:"M036_Q6S"
		m036Q6D column:"M036_Q6D"
        staDel column : 'M036_StaDel', sqlType : 'char(1)'
    }
}
