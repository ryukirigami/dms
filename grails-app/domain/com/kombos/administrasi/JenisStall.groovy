package com.kombos.administrasi

class JenisStall {

    String m021ID
    String m021NamaJenisStall
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m021ID maxSize : 2, nullable : true, blank : true
        m021NamaJenisStall maxSize : 50, nullable : false, blank : false
        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        m021ID column : 'M021_ID', sqlType : 'char(2)'
		m021NamaJenisStall column : 'M021_NamaJenisStall', sqlType : 'varchar(50)'
		staDel column : 'M021_StaDel', sqlType : 'char(1)'
		table 'M021_JENISSTALL'
	}
	
	String toString() {
		return m021NamaJenisStall
	}
}
