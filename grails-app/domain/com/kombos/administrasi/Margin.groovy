package com.kombos.administrasi


class Margin {
	
	//Provinsi provinsi
    CompanyDealer companyDealer
	NamaDokumen namaDokumen
	Double m018MarginAtas
	String m018StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        //provinsi
        companyDealer (nullable : false, blank : false)
        namaDokumen (nullable : false, blank : false, unique: 'companyDealer')
        m018MarginAtas (nullable : true, blank : true)
		m018StaDel (nullable: false, blank : true)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        companyDealer column : 'M018_M011_ID'
		namaDokumen column : 'M018_M007_ID'
		m018MarginAtas column : 'M018_MarginAtas'
		table 'M018_MARGIN'
	}
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "update"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "insert"
//		lastUpdated = new Date()
		m018StaDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!m018StaDel)
			m018StaDel = "0"
		if(!createdBy){
			createdBy = "_SYSTEM_"
			updatedBy = createdBy
		}
	}
}