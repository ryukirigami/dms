package com.kombos.administrasi

import org.apache.shiro.SecurityUtils

class KegiatanApproval {
    def datatablesUtilService
	static final APPROVAL_REQUEST_BOOKING_FEE = 'Approval Request Booking Fee' // Cek ketersediaan parts
	static final APPROVAL_ORDER_PARTS = 'Approval Order Parts' // validasi parts order
	
	// Appointment
	static final REQUEST_EDIT_BOOKING_FEE = 'Request Booking Fee' 
	static final CANCEL_BOOKING_FEE = 'Cancel Booking Fee'
	static final CANCEL_APPOINTMENT = 'Cancel Appointment'
	static final CANCEL_RECEPTION = 'Cancel Reception'
    static final PELANGGAN_MENUNGGAK = 'Pelanggan Servis Masih Punya Tunggakan'
    //disposal dan onhand
	static final PARTS_DISPOSAL = 'Parts Disposal'
	static final PARTS_ONHANDADJUSTMENT = 'Parts OnHandAdjustment'
    //reception
    static final SPK_OVER_LIMIT = 'SPK Over Limit'
    static final PO_SUBLET = 'Approval PO Sublet'
    //Edit Job Parts(Reception)
    static final PENGURANGAN_JOB_ATAU_PART = 'Pengurangan Job / Part'
    static final EDIT_RATE = 'Edit Rate'
    static final REQUEST_SPECIAL_DISCOUNT = 'Bill To and Request Discount'
    static final MIGRATE_INVOICE = 'Perubahan Jenis Invoice'
    //JOC
    static final FINISH_JOC = 'Finish JOC'
    //DELIVERY
    static final INVOICE_REVERSAL = 'Invoice Reversal'

    //hrd
    static final REWARD_KARYAWAN = 'REWARD KARYAWAN'
    static final WARNING_KARYAWAN = 'WARNING KARYAWAN'
    static final KARYAWAN_BARU= 'Approval Karyawan Baru'
    static final HISTORY_KARYAWAN = 'HISTORY KARYAWAN'

    //production
    static final APPROVAL_CLOSE_WO_BEROBAT_JALAN= 'Approval Close WO Berobat Jalan'
    static final APPROVAL_KURANGI_JOB= 'Approval Kurangi Job'
    //finance
    static final FINANCE_PENGELUARAN_LAIN= 'Approval Pengeluaran Lain - Lain'

    //parts transfer / used
    static final PARTS_TRANSFER_STOCK= 'Approval Transfer Stock'
    static final PARTS_USED_OWN = 'Approval Penggunaan Parts'
    static final PARTS_SALES = 'Approval Penjualan Parts'


    String m770IdApproval
	String m770KegiatanApproval
	String m770NamaTabel
	String m770NamaDomain
	String m770NamaFk
	String m770NamaFieldDiupdate
	String m770NamaFormDetail
	String m770StaButuhNilai
	String afterApprovalService
	static hasMany = [namaApprovals: NamaApproval]
	String staDel = "0"
//    Date dateCreated = new Date() // Menunjukkan kapan baris isian ini dibuat.
//    Date lastUpdated //Menunjukkan kapan baris isian ini update terakhir.
    Date dateCreated = datatablesUtilService?.syncTime() // Menunjukkan kapan baris isian ini dibuat.
    Date lastUpdated = datatablesUtilService?.syncTime() // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "INSERT"  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M770_KEGIATANAPPROVAL'
		
		m770IdApproval column : 'M770_IDApproval', sqlType : 'varchar(50)'
		m770KegiatanApproval column : 'M770_KegiatanApproval', sqlType : 'varchar(100)'
		m770NamaTabel column : 'M770_NamaTabel', sqlType : 'varchar(50)'
		m770NamaDomain column : 'M770_NamaDomain', sqlType : 'varchar(255)'
		m770NamaFk column : 'M770_NamaFK', sqlType : 'varchar(50)'
		m770NamaFieldDiupdate column : 'M770_NamaFieldDiUpdate', sqlType : 'varchar(50)'
		m770NamaFormDetail column : 'M770_NamaFormDetail', sqlType : 'varchar(50)'
		m770StaButuhNilai column : 'M770_StaButuhNilai', sqlType : 'char(1)'
		afterApprovalService column: 'M770_AfterApprovalService', sqlType : 'varchar(255)'
		staDel column : 'M770_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		m770IdApproval (nullable : false, blank : false, maxSize : 4)
		m770KegiatanApproval (nullable : false, blank : false, maxSize : 50)
		m770NamaTabel (nullable : true, blank : true, maxSize : 50)
		m770NamaDomain (nullable : true, blank : true, maxSize : 255)
		m770NamaFk (nullable : true, blank : true, maxSize : 50)
		m770NamaFieldDiupdate (nullable : true, blank : true, maxSize : 50)
		m770NamaFormDetail (nullable : true, blank : true, maxSize : 50)
		m770StaButuhNilai (nullable : true, blank : true, maxSize : 1)
		afterApprovalService (nullable : true, blank : true, maxSize : 255)
		staDel (nullable : false, blank : false,  maxSize : 1)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdated nullable: true
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString() {
		return m770KegiatanApproval
	}


//    def beforeUpdate = {
//        lastUpdated = new Date();
//        updatedBy = SecurityUtils?.subject?.principal?.toString()
//        lastUpdProcess = "UPDATE"
//    }
//
//    def beforeInsert = {
//        dateCreated = new Date()
//        lastUpdated = new Date()
//        updatedBy = SecurityUtils?.subject?.principal?.toString()
//        createdBy = SecurityUtils?.subject?.principal?.toString()
//        lastUpdProcess = "INSERT"
//        staDel = "0"
//    }
	
	def beforeUpdate() {
		lastUpdated = new Date();
        if(staDel=='0'){
            lastUpdProcess = "UPDATE"
        }else{
            lastUpdProcess = "DELETE"
        }
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "INSERT"
//		lastUpdated = new Date()
		staDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "INSERT"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}
}