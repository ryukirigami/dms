package com.kombos.administrasi

class Gear {
	Integer m106ID
	BaseModel baseModel
	ModelName modelName
	BodyType bodyType
	String m106KodeGear
	String m106NamaGear
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		baseModel blank:false, nullable:false
		modelName blank:false, nullable:false
		bodyType blank:false, nullable:false
		m106ID blank:true, nullable:true
		m106KodeGear blank:false, nullable:false, maxSize:2
		m106NamaGear blank:false, nullable:false, maxSize:50
		staDel blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table "M106_GEAR"
        m106ID column:"M106_ID", sqlType:"int"
		baseModel column:"M106_M102_ID"
		modelName column:"M106_M104_ID"
		bodyType column:"M106_M105_ID"
		m106KodeGear column:"M106_KodeGear", sqlType:"varchar(2)"
		m106NamaGear column:"M106_NamaGear", sqlType:"varchar(50)"
		staDel column:"M106_StaDel", sqlType:"char(1)"
	}
	
	String toString (){
		return m106KodeGear
	}
}
