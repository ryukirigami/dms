package com.kombos.administrasi

import com.kombos.baseapp.sec.shiro.User

class AuditTrail {

	Date m777Tgl
	Integer m777Id
	User userProfile
	String m777StalUD
	String m777NamaForm
	String m777NamaActivity
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
		table 'M777_AUDITTRAIL'
		m777Tgl column : 'M777_Tgl'//, sqlType : 'date'
		m777Id column : 'M777_ID', sqlType : 'int'
		userProfile column : 'M777_T001_IDUser'
		m777StalUD column : 'M777_StalUD', sqlType : 'char(1)'
		m777NamaForm column : 'M777_NamaForm', sqlType : 'varchar(50)'
		m777NamaActivity column : 'M777_NamaActivity', sqlType : 'varchar(250)'
		staDel column : 'M777_StaDel', sqlType : 'char(1)'
	}
	
    static constraints = {
		m777Tgl (nullable : false, blank : false)
		m777Id (nullable : true, blank : true, maxSize : 4)
		userProfile (nullable : false, blank : false)
		m777StalUD (nullable : true, blank : true, maxSize : 1)
		m777NamaForm (nullable : true, blank : true, maxSize : 50)
		m777NamaActivity (nullable : false, blank : false, maxSize : 250)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
