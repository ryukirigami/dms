package com.kombos.administrasi

class NamaProsesBP {
    String m190ID
    String m190NamaProsesBP
    String m190Keterangan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m190ID nullable: false, blank:false, maxSize: 1
        m190NamaProsesBP nullable: false, blank:false, maxSize: 50
        m190Keterangan nullable: true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        m190ID column : "M190_ID", sqlType : "char(1)"
        m190NamaProsesBP column: "M190_NamaProsesBP", sqlType : "varchar(50)"
        table "M190_NAMAPROSESBP"
    }

    String toString(){
        return m190NamaProsesBP;
    }
}
