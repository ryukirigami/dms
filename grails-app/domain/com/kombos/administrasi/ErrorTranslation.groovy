package com.kombos.administrasi

class ErrorTranslation {
	
	String m780Tahun
	Integer m780Id
	String m780ErrorMsg
	String m780ErrTranslation
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M780_ERRORTRANSLATION'
		
		m780Id column : 'M780_ID', sqlType : 'int'
		m780Tahun column : 'M780_Tahun', sqlType : 'char(4)'
		m780ErrorMsg column : 'M780_ErrorMsg', sqlType : 'varchar(510)'
		m780ErrTranslation column : 'M780_ErrTranslation', sqlType : 'varchar(510)'//, sqlType : 'text(16)'
		
	}
	
	
    static constraints = {
		m780Id (nullable : true, blank : true)
		m780Tahun (nullable : true, blank : true, maxSize : 4)
		m780ErrorMsg (nullable : false, blank : false, maxSize : 510)
		m780ErrTranslation (nullable : false, blank : false, maxSize : 510)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m780ErrorMsg
	}
}
