package com.kombos.administrasi

class Materai {

	//String m705Id
	Date m705TglBerlaku
	Double m705Nilai1
	Double m705Nilai2
	Double m705NilaiMaterai
	String m705StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M705_MATERAI'
		
		id column : 'M705_ID', sqlType : 'int'
		m705TglBerlaku column : 'M705_TglBerlaku', sqlType : 'date'
		m705Nilai1 column : 'M705_Nilai1', sqlType : 'float'
		m705Nilai2 column : 'M705_Nilai2', sqlType : 'float'
		m705NilaiMaterai column : 'M705_NilaiMaterai', sqlType : 'float'
	}
	
    static constraints = {
		//m705Id (nullable : false, blank : false, unique : true, maxSize : 4)
		m705TglBerlaku nullable : false, blank : false//, unique: ['m705Nilai1', 'm705Nilai2'])
		m705Nilai1 (nullable : false, blank : false, maxSize : 8)
		m705Nilai2 (nullable : false, blank : false, maxSize : 8)
		m705NilaiMaterai (nullable : false, blank : false, maxSize : 8)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString (){
		return m705TglBerlaku
	}
	def beforeUpdate() {
		lastUpdated = new Date();
        if(m705StaDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
		//lastUpdProcess = "update"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "insert"
//		lastUpdated = new Date()
		m705StaDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!m705StaDel)
			m705StaDel = "0"
		if(!createdBy){
			createdBy = "_SYSTEM_"
			updatedBy = createdBy
		}
	}
}