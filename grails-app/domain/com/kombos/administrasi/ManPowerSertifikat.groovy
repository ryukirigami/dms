package com.kombos.administrasi

import java.util.Date;

class ManPowerSertifikat {
	CompanyDealer companyDealer
	NamaManPower namaManPower
	Sertifikat sertifikat
	Date t016TglAwalSertifikat
	Date t016TglAkhirSertifikat
	String t016Ket
	//String t016xNamaUser
	//String t016NamaDivisi
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        autoTimestamp false
        table 'T016_MANPOWERSERTIFIKAT'
		
		namaManPower column : 'T016_T015_IDManPower'
		sertifikat column : 'T016_Sertifikat_M016_ID'
		t016TglAwalSertifikat column : 'T016_TglAwalSertifikat', sqlType : 'date'
		t016TglAkhirSertifikat column : 'T016_TglAkhirSertifikat', sqlType : 'date'
		t016Ket column : 'T016_Ket', sqlType : 'varchar(255)'
		//t016xNamaUser column : 'T016_xNamaUser', sqlType : 'varchar(20)'
		//t016NamaDivisi column : 'T016_NamaDivisi', sqlType : 'varchar(20)'
		staDel column : 'T016_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer blank:true, nullable:true
		namaManPower (nullable : false, blank : false)
		sertifikat (nullable : false, blank : false)
		t016TglAwalSertifikat (nullable : false, blank : false)
		t016TglAkhirSertifikat (nullable : false, blank : false)
		t016Ket (nullable : true, blank : true, maxSize : 255)
		//t016xNamaUser (nullable : true, blank : true, maxSize : 20)
		//t016NamaDivisi (nullable : true, blank : true, maxSize : 20)
		staDel (nullable : false, blank : false, maxSize : 1)
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return namaManPower.toString()
	}
}
