package com.kombos.administrasi

import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User
import com.kombos.maintable.KondisiApproval

class NamaApproval {

	KegiatanApproval kegiatanApproval
	KondisiApproval kondisiApproval
    Role approvelRole
	User userProfile
	Integer m771Level
    Role delegasiRole
	User userProfileDelegate
	Date m771TglAwal
	Date m771TglAkhir
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M771_NAMAAPPROVAL'
		
		kegiatanApproval column : 'M771_M770_ID_APPROVAL'
		kondisiApproval column : 'M771_M772_ID'
		userProfile column : 'M771_T001_IDUSER'
		m771Level column : 'M771_Level', sqlType : 'int'
		userProfileDelegate column : 'M771_T001_IDUserDelegate'
		m771TglAwal column : 'M771_TglAwal', sqlType : 'date'
		m771TglAkhir column : 'M771_TglAkhir', sqlType : 'date'
		staDel column : 'M771_StaDel', sqlType : 'char(1)'
		
		
	}
	
    static constraints = {
		kegiatanApproval (nullable : false, blank : false)
		kondisiApproval (nullable : true, blank : true)
        approvelRole nullable: true, blank : true
		userProfile (nullable : false, blank : false)
        delegasiRole nullable: true, blank : true
        m771Level (nullable : false, blank : false, maxSize : 4)
		userProfileDelegate (nullable : false, blank : false)
		m771TglAwal (nullable : false, blank : false)
		m771TglAkhir (nullable : false, blank : false)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return kegiatanApproval.m770KegiatanApproval
	}
}
