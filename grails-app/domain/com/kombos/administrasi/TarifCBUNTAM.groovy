package com.kombos.administrasi

class TarifCBUNTAM {

	CompanyDealer companyDealer
	KategoriKendaraan kategori
	Date  t113bTMT
	Double t113bTarifPerjamCBUNTAMSB
	Double t113bTarifPerjamCBUNTAMGR
	Double t113bTarifPerjamCBUNTAMBP
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer unique:false, nullable: false, blank:true
		kategori unique:false , nullable : false, blank : true
		t113bTMT unique:false, nullable : false, blank : true, validator: {val, obj ->
            if(TarifCBUNTAM.get(obj.id)){
                return !TarifCBUNTAM.findByCompanyDealerAndKategoriAndT113bTMTAndIdNotEqualAndStaDel(obj.companyDealer, obj.kategori, val, obj.id,"0")
            }else{
                return !TarifCBUNTAM.findByCompanyDealerAndKategoriAndT113bTMTAndStaDel(obj.companyDealer, obj.kategori, val,"0")
            }
        }
        t113bTarifPerjamCBUNTAMSB nullable : false, blank : true, maxSize : 8, unique : false
		t113bTarifPerjamCBUNTAMGR nullable : false, blank : true, maxSize : 8, unique : false
		t113bTarifPerjamCBUNTAMBP nullable : false, blank : true, maxSize : 8, unique : false
		staDel nullable : true, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T113B_TARIFCBUNTAM'
		companyDealer column : 'T113b_M011_ID'
		kategori column : 'T113b_M101_ID'
		t113bTMT column : 'T113b_TMT', sqlType :'date'
		t113bTarifPerjamCBUNTAMSB column : 'T113b_TarifPerjamCBUNTAMSB'
		t113bTarifPerjamCBUNTAMGR column : 'T113b_TarifPerjamCBUNTAMGR'
		t113bTarifPerjamCBUNTAMBP column : 'T113b_TarifPerjamCBUNTAMBP'
		staDel column : 'T113b_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return t113bTMT
	}
}
