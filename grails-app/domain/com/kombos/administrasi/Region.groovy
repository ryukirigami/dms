package com.kombos.administrasi

class Region {
    String regionID
    String regionNama
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static  mapping = {
        table 'M011A_REGION'
        regionID column : 'M011A_ID'
        regionNama column : 'M011A_NAMA'
        staDel column : 'M011A_StaDel', sqlType : 'char(1)'
    }
    static constraints = {
        regionID (nullable : true, blank : true)
        regionNama (nullable : true, blank : true)
        staDel (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    String toString(){
        return regionNama
    }
}
