package com.kombos.administrasi

class ManPower {
    String m014ID
    String m014JabatanManPower
    String m014Inisial
    String m014staGRBPADM
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {        
        m014JabatanManPower (nullable : false, blank : false, maxSize : 20)
        m014Inisial (nullable : false, blank : false, maxSize : 4)
        m014staGRBPADM (nullable : false, blank : false, maxSize : 1)
        staDel (nullable : false, blank : false, maxSize : 1)
		m014ID (nullable : true, blank : true, maxSize : 2)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping={
        autoTimestamp false
        m014ID column: "M014_ID" , sqlType : "char(2)"
        m014JabatanManPower column: "M014_JabatanManPower", sqlType: "varchar(20)"
        m014Inisial column: "M014_Inisial", sqlType: "varchar(4)"
        m014staGRBPADM column: "M014_staGRBP", sqlType: "char(1)"
        staDel column: "M014_StaDel", sqlType: "char(1)"
        table "M014_MANPOWER"
    }

    String toString(){
        return m014JabatanManPower;
    }
}
