package com.kombos.administrasi

class PenegasanPengiriman {

    String jalur
    String keterangan

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        dateCreated nullable : true // Menunjukkan kapan baris isian ini dibuat.
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        lastUpdated  nullable : true//Menunjukkan kapan baris isian ini update terakhir.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        table 'TBL_NPBPENEGASANPENGIRIMAN'

    }

    String toString(){
        return jalur
    }
}
