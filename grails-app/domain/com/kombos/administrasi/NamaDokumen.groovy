package com.kombos.administrasi

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder
import grails.validation.ValidationException


class NamaDokumen {
	//Integer m007ID
	String m007NamaDokumen
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		// m007ID nullable: false, blank:false, maxSize: 4
		m007NamaDokumen nullable: false, blank:false, maxSize: 50//, unique: true
		staDel nullable: false, blank:false, maxSize: 1
		createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
	}

	static mapping ={
		id column : "M007_ID"//, sqlType : 'int'
		m007NamaDokumen column: "M007_NamaDokumen", sqlType : "varchar(50)"
		staDel column: "M007_StaDel", sqlType : "char(1)"
		table "M007_NAMADOKUMEN"
	}

	String toString(){
		return m007NamaDokumen;
	}

	def beforeUpdate() {
		lastUpdated = new Date();
        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
		    lastUpdProcess = "update"
        }
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "insert"
//		lastUpdated = new Date()
		staDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}
	
	def afterInsert() {
		//println("After insert nama dokumen")
		CompanyDealer.list().each{ CompanyDealer cd ->
			Margin m = new Margin();
			m.companyDealer = cd
			m.namaDokumen = this
			m.m018MarginAtas = 0.0
			try {
				m.save(failOnError: true)
			}
			catch (ValidationException e) {
				e.printStackTrace()
				// deal with exception
			}
		}
	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}
}