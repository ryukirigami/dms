package com.kombos.administrasi

class KategoriJob {

    String m055ID
    String m055KategoriJob
    String m055WarnaBord
    String m055staReadOnly
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        m055KategoriJob nullable: false, blank:false, maxSize : 50, unique: true
        m055staReadOnly nullable: false, blank:false, maxSize : 1
        m055WarnaBord nullable: true, blank:true
        m055ID nullable: true, blank:true, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        m055ID column : 'M055_ID', sqlType : 'varchar(1)'
		m055KategoriJob column : 'M055_KategoriJob', sqlType : 'varchar(50)', unique: true
		m055WarnaBord column : 'M055_WARNABOARD', sqlType : 'varchar(15)'
		m055staReadOnly column : 'M055_staReadOnly', sqlType : 'varchar(1)'
		table 'M055_KATEGORIJOB'
	}
	
	String toString(){
		return m055KategoriJob
	}
}
