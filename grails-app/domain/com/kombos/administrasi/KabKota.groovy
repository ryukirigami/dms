package com.kombos.administrasi

class KabKota {

    Provinsi provinsi
    String m002ID
    String m002NamaKabKota
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        provinsi (nullable : false, blank : false, maxSize : 2)
        m002ID (nullable : true, blank : true, maxSize : 3)
        m002NamaKabKota (nullable : false, blank : false, maxSize : 45)
        staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    
    static mapping={
        provinsi column: "M002_M001_ID"
        m002ID column: "M002_ID" , sqlType : "varchar(3)", unique: true
        m002NamaKabKota column: "M002_NamaKabKota", sqlType: "varchar(45)"
        staDel column: "M002_StaDel", sqlType: "char(1)"
        table "M002_KABKOTA"
    }

    String toString(){
        return m002NamaKabKota;
    }
}
