package com.kombos.administrasi


class HargaJobSublet {

    CompanyDealer companyDealer
	Section section
    Serial serial
    Operation operation
	BaseModel baseModel
	Date t116TMT
	Double t116Harga
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer blank:true, nullable:true
		operation unique:false, nullable: false,blank: false
		baseModel unique:false, nullable: false, blank: false
        section   nullable: false, blank: false
        serial   nullable: false, blank: false
		t116TMT blank : false, nullable:false
		t116Harga blank : false, nullable:false, maxSize : 8
		staDel nullable:false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T116_HARGAJOBSUBLET'
		operation column :'T116_M053_JobID'
        serial column :'M053_M052_ID'
        section column :'M053_M051_ID'
		baseModel column : 'T116_M102_ID'
		t116TMT column : 'T116_TMT', sqlType : 'date'
		t116Harga column : 'T116_Harga'
		staDel column : 'T116_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return t116TMT
	}
}
