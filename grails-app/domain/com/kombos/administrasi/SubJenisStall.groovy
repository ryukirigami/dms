package com.kombos.administrasi

class SubJenisStall {

	JenisStall jenisStall
    String m023ID
    String m023NamaSubJenisStall
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        jenisStall nullable : false, blank : false
        m023NamaSubJenisStall maxSize : 50, nullable : false, blank : false
        m023ID maxSize : 1, nullable : true, blank : true
        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        jenisStall column : 'M023_M021_ID'
        m023ID column : 'M023_ID', sqlType : 'char(1)', unique: true
		m023NamaSubJenisStall column : 'M023_NamaSubJenisStall', sqlType : 'varchar(50)'
		staDel column : 'M023_StaDel', sqlType : 'char(1)'
		table 'M023_SUBJENISSTALL'
	}
	
	String toString(){
		return m023NamaSubJenisStall
	}
}
