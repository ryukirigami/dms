package com.kombos.administrasi

class Kategori {
	SearchArea searchArea
	SubArea subArea
    Integer m083ID
    String m083NamaKategori
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m083ID blank:false, nullable:false
        searchArea blank:false, nullable:false
        subArea blank:false, nullable:false
        m083NamaKategori blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
		table "M083_KATEGORI"
        m083ID column:"M083_ID", sqlType: 'int',unique: true
        searchArea column:"M083_M081_ID",unique: true
		subArea column:"M083_M082_ID",unique: true
		m083NamaKategori column:"M083_NamaKategori", sqlType:"varchar(50)"
	}

    String toString()
    {
        return m083NamaKategori
    }
}
