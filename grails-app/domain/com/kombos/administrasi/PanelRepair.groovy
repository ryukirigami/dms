package com.kombos.administrasi

class PanelRepair {
    Integer m043ID
	CompanyDealer companyDealer
	Date m043TanggalBerlaku
	RepairDifficulty repairDifficulty
	double m043Area1
    double m043Area2
    double m043StdTime
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        companyDealer column : 'M043_M011ID'
        m043ID column : 'M043_ID', sqlType : 'int'//, unique: true
		m043TanggalBerlaku column : 'M043_TglBerlaku', sqlType : 'date'
        repairDifficulty column : 'M043_M042_ID'
		m043Area1 column : 'M043_Area1'
        m043Area2 column : 'M043_Area2'
        m043StdTime column : 'M043_StdTime'
        table : 'M043_PANELREPAIR'

	}
	
	public String toString() {
		return m043Area1 + " - "+ m043Area2
	}
    
	static constraints = {
        companyDealer blank:false, nullable:false
        m043TanggalBerlaku blank:false, nullable:false
        repairDifficulty blank:false, nullable:false
        m043Area1 blank:false, nullable:false
        m043Area2 blank:false, nullable:false
        m043StdTime blank:false, nullable:false
        m043ID blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	
}
