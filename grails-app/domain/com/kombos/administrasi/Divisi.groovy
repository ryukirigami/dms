package com.kombos.administrasi

class Divisi {
    CompanyDealer companyDealer
    String m012ID
    String m012NamaDivisi
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable: false
        m012NamaDivisi blank:false, nullable: false
		m012ID blank:true, nullable:true
        staDel blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        companyDealer column : "M012_M011_ID"
        m012ID column : "M012_ID", sqlType : "char(2)"
        m012NamaDivisi column: "M012_NamaDivisi", sqlType: "varchar(50)"
        staDel column : "M012_StaDel", sqlType : "char(1)"
        table "M012_DIVISI"
    }

    String toString(){
        return m012NamaDivisi;
    }
}
