package com.kombos.administrasi

import com.kombos.parts.Goods

class GoodsJobPackage {

    KategoriJob kategori
    Operation operation
    Goods goods

    static mapping = {
        table "A999_GOODSJOBPACKAGE"
        kategori column:"KATEGORI_ID"
        operation column:"OPERATION_ID"
        goods column:"GOODS_ID"
    }
}
