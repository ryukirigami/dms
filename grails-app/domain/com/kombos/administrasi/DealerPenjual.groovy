package com.kombos.administrasi

class DealerPenjual {

    String m091ID
    String m091NamaDealer
    String m091AlamatDealer
    String m091Npwp
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m091NamaDealer nullable:false, blank:false
        m091AlamatDealer nullable:true, blank:true
        m091Npwp nullable:true, blank:true
		m091ID nullable:true, blank:true
        staDel nullable:false, blank:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M091_DEALERPENJUAL"
        m091ID column:"M091_ID", sqlType:"varchar(5)"
		m091NamaDealer column:"M091_NamaDealer", sqlType:"varchar(50)"
		m091AlamatDealer column:"M091_AlamatDealer", sqlType:"varchar(300)"
		staDel column: "M091_StaDel", sqlType: "char(1)"
	}

    String toString(){
        return m091NamaDealer
    }
}
