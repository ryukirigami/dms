package com.kombos.administrasi

import com.kombos.parts.Goods
import com.kombos.parts.Satuan

class Material {

    Operation operation
    CompanyDealer companyDealer
	//RepairDifficulty repairDifficulty
    PanelRepair panelRepair
	Goods goods
	double m047Qty1
	double m047Qty2
	String staDel
    Satuan satuan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
       companyDealer column : 'M043_M011_ID'//, unique: true
        operation column : 'M047_M053_JobID'//, unique: true
       // repairDifficulty column : 'M047_M042_ID'
       // panelRepair column : 'M047_M043_ID'//, unique: true
        goods column : 'M047_M111_ID'//, unique: true
		m047Qty1 column : 'M047_Qty1'
	//	m047Qty2 column : 'M047_Qty2'
		staDel column : 'M047_StaDel', sqlType : 'char(1)'
		table 'M047_MATERIAL'
	}
	
	public String toString() {
		return m047Qty1
	}

    static constraints = {
       companyDealer blank:false, nullable:false
        operation blank:false, nullable:false
        panelRepair blank : false, nullable : false
        goods blank:false, nullable:false
        m047Qty1 blank:false, nullable:false, validator: {
            val, obj ->
                if(Material.get(obj.id)){
                    return !Material.findByOperationAndPanelRepairAndGoodsAndIdNotEqualAndM047Qty1(obj.operation, obj.panelRepair, obj.goods, obj.id, obj.m047Qty1)
                }else{
                    return !Material.findByOperationAndPanelRepairAndGoods(obj.operation, obj.panelRepair, obj.goods)
                }

        }
        m047Qty2 blank:false, nullable:false
        staDel blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
