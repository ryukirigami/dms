package com.kombos.administrasi

import com.kombos.maintable.StaHadir

import java.sql.Time

class ManPowerAbsensi {
	CompanyDealer companyDealer
	NamaManPower namaManPower
	Date t017Tanggal
	StaHadir staHadir
    Date t017JamDatang
    Date t017JamPulang
	String t017Ket
	//String t017xNamaUser
	//String t017xNamaDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'T017_MANPOWERABSENSI'
		
		namaManPower column : 'T017_T015_IDManPower'
		t017Tanggal column : 'T017_Tanggal', sqlType : 'date'
		staHadir column : 'T017_M017_ID'
		t017JamPulang column : 'T017_JamPulang'
		t017JamDatang column : 'T017_JamDatang'
		t017Ket column : 'T017_Ket', sqlType : 'varchar(1000)'
	//	t017xNamaUser column : 'T017_xNamaUser', sqlType : 'varchar(20)'
	//  t017xNamaDivisi column : 'T017_xNamaDivisi', sqlType : 'varchar(20)'
		staDel column : 'T017_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		namaManPower (nullable : false, blank : false)
		t017Tanggal (nullable : false, blank : false)
		staHadir (nullable : true, blank : true)
		t017JamPulang (nullable : true, blank : true)
		t017JamDatang (nullable : true, blank : true)
		t017Ket (nullable : true, blank : true, maxSize : 1000)
		// t017xNamaUser (nullable : false, blank : false, maxSize : 20)
		// t017xNamaDivisi (nullable : false, blank : false, maxSize : 20)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return namaManPower.toString()
	}
}
