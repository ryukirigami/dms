package com.kombos.administrasi

class ContactPerson {
	
	CompanyDealer companyDealer
	Date m013Tanggal
	String m013NamaCP
	ManPower m013JabatanCP
	String m013Telp1
	String m013Telp2
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (nullable:false, blank:false)
        m013Tanggal (nullable:false, blank:false)
        m013NamaCP (nullable:false, blank:false, maxSize:50)
        m013JabatanCP (nullable:false, blank:false)
        m013Telp1 (maxSize:50,matches : "[0-9]+")
        m013Telp2 (maxSize:50,matches : "[0-9]+")
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'M013_CONTACTPERSON'
        companyDealer column: 'M013_M011_ID'
        m013Tanggal column: 'M013_Tanggal'//, sqlType: 'date'
		m013NamaCP column: 'M013_NamaCP', sqlType: "nvarchar2(50)"
		m013JabatanCP column: 'M013_JabatanCP'
		m013Telp1 column: 'M013_Telp1', sqlType: "nvarchar2(50)"
		m013Telp2 column: 'M013_Telp2', sqlType: "nvarchar2(50)"
	}

    String toString(){
        return m013NamaCP;
    }
}
