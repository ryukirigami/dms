package com.kombos.administrasi

import java.util.Date;

class BodyType {
	int m105ID
	BaseModel baseModel
	ModelName modelName
	String m105KodeBodyType
	String m105NamaBodyType
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
		m105ID blank:true, nullable:true, unique:false
		baseModel blank:false, nullable:false
		modelName blank:false, nullable:false
		m105KodeBodyType blank:false, nullable:false, maxSize:2
		m105NamaBodyType blank:false, nullable:false, maxSize:20
		staDel blank:false, nullable:false, maxSize:1
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
        autoTimestamp false
        table "M105_BODYTYPE"
		m105ID column:"M105_ID", sqlType:'int'
		baseModel column:"M105_M102_ID"
		modelName column:"M105_M104_ID"
		m105KodeBodyType column:"M105_KodeBodyType", sqlType:"varchar(2)"
		m105NamaBodyType column:"M105_NamaBodyType", sqlType:"varchar(20)"
		staDel column:"M105_StaDel", sqlType:"char(1)"
	}
	
	String toString(){
		return m105NamaBodyType;
	}
}
