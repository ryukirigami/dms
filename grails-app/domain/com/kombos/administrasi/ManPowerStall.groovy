package com.kombos.administrasi

import java.util.Date;

class ManPowerStall {
	CompanyDealer companyDealer
	Date t019Tanggal
	NamaManPower namaManPower
	Stall stall
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        autoTimestamp false
        t019Tanggal column : 'T019_Tanggal', sqlType : 'date'
		namaManPower column : 'T019_T015_IDManPower'
		stall column : 'T019_M022_StallID'
		staDel column : 'T019_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer blank:true, nullable:true
		t019Tanggal (nullable : false, blank : false)
		namaManPower (nullable : false, blank : false)
		stall (nullable : false, blank : false)
		staDel (nullable : false, blank : false, maxSize : 1)
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return namaManPower.toString()
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}