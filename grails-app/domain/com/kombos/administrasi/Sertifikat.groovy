package com.kombos.administrasi

class Sertifikat {

    String m016ID
    String m016NamaSertifikat
    String m016Inisial
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	 
    static constraints = {
        m016NamaSertifikat maxSize : 50, nullable : false, blank: false
        m016Inisial maxSize : 8, nullable : false, blank: false
		staDel maxSize : 1, nullable : false, blank: false
		m016ID maxSize : 2, nullable : true, blank: true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		m016ID column : 'M016_ID', sqlType : 'char(2)'
		m016NamaSertifikat column : 'M016_NamaSertifikat', sqlType : 'varchar(50)'
		m016Inisial column : 'M016_Inisial', sqlType : 'varchar(8)'
		staDel column :'M016_StaDel', sqlType:'char(1)'
		table 'M016_SERTIFIKAT'
	}
	
	String toString() {
		return m016NamaSertifikat
	}
}
