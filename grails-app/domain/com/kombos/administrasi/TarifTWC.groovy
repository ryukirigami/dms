package com.kombos.administrasi

class TarifTWC {

	CompanyDealer companyDealer
	KategoriKendaraan kategoriKendaraan
	Date t113aTMT
	Double t113aTarifPerjamTWC
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer blank: false, nullable: false
		kategoriKendaraan blank : false, nullable:false
		t113aTMT blank : false, nullable : false
		t113aTarifPerjamTWC nullable: false, blank : false
		staDel nullable: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
	}
	
	static mapping = {
        autoTimestamp false
        table 'T113A_TARIFTWC'
		companyDealer column : 'T113a_M011_ID'
		kategoriKendaraan column : 'T113a_M101_ID'
		t113aTMT column : 'T113a_TMT', sqlType :'date'
		t113aTarifPerjamTWC column : 'T113a_TarifPerjamTWC'
		staDel column : 'T113a_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return t113aTarifPerjamTWC
	}
}
