package com.kombos.administrasi

class ManPowerDetail {
    ManPower manPower
    String m015ID
    String m015LevelManPower
    String m015Inisial
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        manPower (nullable : false, blank : false)
        m015LevelManPower (nullable : false, blank : false, maxSize : 50)
        m015Inisial (nullable : false, blank : false, maxSize : 4)
        staDel (nullable : false, blank : false, maxSize : 1)
		m015ID (nullable : true, blank : true, maxSize : 2)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        manPower column: "M015_M014_ID"
        m015ID column: "M015_ID" , sqlType : "char(2)"
        m015LevelManPower column: "M015_LevelManPower", sqlType: "varchar(50)"
        m015Inisial column: "M015_Inisial", sqlType: "char(4)"
        staDel column: "M015_StaDel", sqlType: "char(1)"
        table "M015_MANPOWERDETAIL"
    }

    String toString(){
        return m015LevelManPower;
    }
}
