package com.kombos.administrasi

import java.util.Date;

class MasterDurasiPerjalananDriver {
    CompanyDealer companyDealer
    Date m009TglBerlaku
    CompanyDealer companyDealer1
    CompanyDealer companyDealer2
    Double m009Durasi
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        companyDealer (blank:false, nullable: false)
        m009TglBerlaku (blank:false, nullable: false)
        companyDealer1 (blank:false, nullable: false)
        companyDealer2 (blank:false, nullable: false)
        m009Durasi (blank:false, nullable: false)
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping={
        table "M009_MASTERDURASIPERJLNNDRIVER"
        companyDealer column: "M009_M011_ID"
        m009TglBerlaku column: "M009_TglBerlaku"//, sqlType: 'date', unique: true
        companyDealer1 column: "M009_M011_ID_1"
        companyDealer2 column: "M009_M011_ID_2"
        m009Durasi  column: "M009_Durasi"
		
    }

    String toString(){
        return m009Durasi
    }

}
