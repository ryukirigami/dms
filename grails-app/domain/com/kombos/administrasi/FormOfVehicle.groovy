package com.kombos.administrasi

import java.util.Date;

class FormOfVehicle {
	int m114ID
	String m114KodeFoV
	String m114NamaFoV
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
		m114ID blank:true, nullable:true, maxSize:4, unique:false
		m114KodeFoV blank:false, nullable:false, maxSize:2
		m114NamaFoV blank:false, nullable:false, maxSize:20
		staDel blank:false, nullable:false, maxSize:1
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table "M114_FORMOFVEHICLE"
		m114ID column:"M114_ID", sqlType:"int"
		m114KodeFoV column:"M114_KodeFoV", sqlType:"varchar(2)"
		m114NamaFoV column:"M114_NamaFoV", sqlType:"varchar(20)"
		staDel column:"M114_StaDel", sqlType:"char(1)"
	}
	
	String toString(){
		return m114KodeFoV
	}
}
