package com.kombos.administrasi

class RepairDifficulty {

    Integer m042ID
    String m042Tipe
    String m042Keterangan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        m042ID column : 'M042_ID', sqlType: 'int'//, unique : true
		m042Tipe column : 'M042_Tipe', sqlType: 'varchar(50)'
		m042Keterangan column : 'M042_Keterangan', sqlType : 'varchar(1000)'
		table 'M042_REPAIRDIFFICULTY'
	}
	
	String toString(){
		return m042Tipe;
	}
	
    static constraints = {
        m042Tipe blank:false, nullable:false
        m042Keterangan blank:false, nullable:false,maxSize: 1000
        m042ID blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
