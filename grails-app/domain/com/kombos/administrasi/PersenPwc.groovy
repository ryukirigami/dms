package com.kombos.administrasi

class PersenPwc {

    CompanyDealer companyDealer
    Date m008TanggalBerlaku
    Double m008PersenPwc
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (nullable : false, blank : false)
        m008TanggalBerlaku (nullable : false, blank : false)
        m008PersenPwc (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        companyDealer column: "M008_M011_ID"
        m008TanggalBerlaku column: "M008_TanggalBerlaku", sqlType : "date"
        m008PersenPwc column: "M008_PersenPWC"
        table "M008_PERSENPWC"
    }

    String toString(){
        return m008PersenPwc;
    }
}
