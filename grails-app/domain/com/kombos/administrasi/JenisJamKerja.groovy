package com.kombos.administrasi

import java.sql.Time

class JenisJamKerja {

	CompanyDealer companyDealer
	String m030ID
    Time m030JamMulai1
	Time m030JamSelesai1
    Time m030JamMulai2
    Time m030JamSelesai2
    Time m030JamMulai3
    Time m030JamSelesai3
    Time m030JamMulai4
    Time m030JamSelesai4
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer nullable: false,blank: false
        m030ID nullable: false,blank: false, maxSize: 2
        m030JamMulai1 nullable: false,blank: false
        m030JamSelesai1 nullable: false,blank: false
        m030JamMulai2 nullable: true,blank: true
        m030JamSelesai2 nullable: true,blank: true
        m030JamMulai3 nullable: true,blank: true
        m030JamSelesai3 nullable: true,blank: true
        m030JamMulai4 nullable: true,blank: true
        m030JamSelesai4 nullable: true,blank: true
        staDel nullable: false,blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        companyDealer column : 'M030_M011_ID'
        m030ID column : 'M030_ID', sqlType : 'char(2)'
        m030JamMulai1 column : 'M030_JamMulai1'//, sqlType: 'date'
		m030JamSelesai1 column : 'M030_JamSelesai1'//, sqlType: 'date'
        m030JamMulai2 column : 'M030_JamMulai2'//, sqlType: 'date'
        m030JamSelesai2 column : 'M030_JamSelesai2'//, sqlType: 'date'
        m030JamMulai3 column : 'M030_JamMulai3'//, sqlType: 'date'
        m030JamSelesai3 column : 'M030_JamSelesai3'//, sqlType: 'date'
        m030JamMulai4 column : 'M030_JamMulai4'//, sqlType: 'date'
        m030JamSelesai4 column : 'M030_JamSelesai4'//, sqlType: 'date'
        staDel column : 'M030_StaDel', sqlType: "char(1)"
		table 'M030_JENISJAMKERJA'
	}


	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer()
		sb.append("[").append(m030ID).append("].")
		if(m030JamMulai1){
			sb.append(m030JamMulai1)
		} else {
			sb.append('-')
		}
		if(m030JamSelesai1){
			sb.append('-').append(m030JamSelesai1)
		}
		if(m030JamMulai2){
			sb.append(';').append(m030JamMulai2)
		}
		if(m030JamSelesai2){
			sb.append('-').append(m030JamSelesai2)
		}
		if(m030JamMulai3){
			sb.append(';').append(m030JamMulai3)
		}
		if(m030JamSelesai3){
			sb.append('-').append(m030JamSelesai3)
		}
		if(m030JamMulai4){
			sb.append(';').append(m030JamMulai4)
		}
		if(m030JamSelesai4){
			sb.append('-').append(m030JamSelesai4)
		}
		
		sb.toString()
	}
}