package com.kombos.administrasi

class Leasing {

    String m1000NamaLeasing
    String m1000Alamat
    String m1000Npwp
    String m1000NoTelp
    String m1000Email
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m1000NamaLeasing nullable: true, blank: true, maxSize: 75
        m1000Alamat nullable: true, blank: true, maxSize: 255
        staDel nullable: true, blank: true, maxSize: 1
        m1000Npwp (nullable : false, blank : false, maxSize : 50)
        m1000NoTelp (nullable : false, blank : false, maxSize : 50)
        m1000Email (nullable:false, blank:false, email:true, maxSize:50)
        createdBy nullable: true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'M1000_LEASING'
    }
}
