package com.kombos.administrasi

import java.sql.Time

class LeadTime {
	CompanyDealer companyDealer
	Date m035TglBerlaku
    Time m035LTReception
    Time m035LTPreDiagnose
    Time m035LTProduction
    Time m035LTIDR
    Time m035LTFinalInspection
    Time m035Invoicing
    Time m035Washing
    Time m035Delivery
    Time m035JobStopped
    Time m035WFReception
    Time m035WFJobDispatch
    Time m035WFClockOn
    Time m035WFFinalInspection
    Time m035WFInvoicing
    Time m035WFWashing
    Time m035WFNotifikasi
    Time m035WFDelivery
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    String staDel

    static constraints = {
		m035TglBerlaku blank:false, nullable:false
		companyDealer blank:false, nullable:false
        m035LTReception blank:false, nullable:false
        m035LTPreDiagnose blank:false, nullable:false
        m035LTProduction blank:false, nullable:false
        m035LTIDR blank:false, nullable:false
        m035LTFinalInspection blank:false, nullable:false
        m035Invoicing blank:false, nullable:false
        m035Washing blank:false, nullable:false
        m035Delivery blank:false, nullable:false
        m035JobStopped blank:false, nullable:false
        m035WFReception blank:false, nullable:false
        m035WFJobDispatch blank:false, nullable:false
        m035WFClockOn blank:false, nullable:false
        m035WFFinalInspection blank:false, nullable:false
        m035WFInvoicing blank:false, nullable:false
        m035WFWashing blank:false, nullable:false
        m035WFNotifikasi blank:false, nullable:false
        m035WFDelivery blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        staDel maxSize : 1, nullable : false, blank : false
    }
	static mapping = {
		table "M035_LEADTIME"
		companyDealer column : "M035_M011_ID"
		m035TglBerlaku column:"M035_TglNerlaku", sqlType : "date", unique: true
		m035LTReception column:"M035_LTReception", sqlType : "timestamp"
		m035LTPreDiagnose column:"M035_LTPreDiagnose", sqlType : "timestamp"
		m035LTProduction column:"M035_LTProduction", sqlType : "timestamp"
		m035LTIDR column:"M035_LTIDR", sqlType : "timestamp"
		m035LTFinalInspection column:"M035_LTFinalInspection", sqlType : "timestamp"
		m035Invoicing column:"M035_Invoicing", sqlType : "timestamp"
		m035Washing column:"M035_Washing", sqlType : "timestamp"
		m035Delivery column:"M035_Delivery", sqlType : "timestamp"
		m035JobStopped column:"M035_JobStopped", sqlType : "timestamp"
		m035WFReception column:"M035_WFReception", sqlType : "timestamp"
		m035WFJobDispatch column:"M035_WFJobDispatch", sqlType : "timestamp"
		m035WFClockOn column:"M035_WFClockOn", sqlType : "timestamp"
		m035WFFinalInspection column:"M035_WFFinalInspection", sqlType : "timestamp"
		m035WFInvoicing column:"M035_WFInvoicing", sqlType : "timestamp"
		m035WFWashing column:"M035_WFWashhing", sqlType : "timestamp"
		m035WFNotifikasi column:"M035_WFNotifikasi", sqlType : "timestamp"
		m035WFDelivery column:"M035_WFDelivery", sqlType : "timestamp"
        staDel column : 'M035_StaDel', sqlType : 'char(1)'
	}
}
