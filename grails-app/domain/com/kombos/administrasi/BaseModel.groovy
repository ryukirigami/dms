package com.kombos.administrasi

class BaseModel {
    Integer m102ID
    KategoriKendaraan kategoriKendaraan
    String m102KodeBaseModel
    String m102NamaBaseModel
    BahanBakar bahanBakar
    byte[] m102Foto
    byte[] m102FotoWAC
    String staDel
    String m102FotoImageMime
    String m102FotoWACImageMime
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m102ID nullable: true, blank:true
        kategoriKendaraan nullable: false, blank:false
        bahanBakar nullable: false, blank:false
        m102KodeBaseModel nullable: false, blank:false
        m102NamaBaseModel nullable: false, blank:false
        staDel nullable: true, blank:true
        m102Foto nullable: true, blank:true
        m102FotoWAC nullable: true, blank:true
        m102FotoImageMime nullable : true, blank : true
        m102FotoWACImageMime nullable : true, blank : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        autoTimestamp false
        table "M102_BASEMODEL"
        m102ID column : "M102_ID", sqlType:"int"//, unique: true
        kategoriKendaraan column: "M102_M101_ID"
        bahanBakar column: "M102_M110_ID"
        m102NamaBaseModel column: "M102_NamaBaseModel", sqlType:"varchar(25)"
        m102KodeBaseModel column: "M102_KodeBaseModel", sqlType:"varchar(10)", validator : {
            val, obj ->
                if(BaseModel.get(obj.id)){
                    return !BaseModel.findByM102NamaBaseModelIlikeAndKategoriKendaraanAndM102KodeBaseModelIlikeAndStaDelAndIdNotEqual(obj.m102NamaBaseModel, obj.kategoriKendaraan, val, "0", obj.id)

                }else
                    return !BaseModel.findByM102NamaBaseModelIlikeAndKategoriKendaraanAndM102KodeBaseModelIlikeAndStaDel(obj.m102NamaBaseModel, obj.kategoriKendaraan, val, "0")
        }
        staDel column: "M102_StaDel", sqlType:"varchar(1)"
        m102Foto column : "M102_Foto", sqlType: "blob"
        m102FotoWAC column : "M102_FotoWAC", sqlType: "blob", validator : {
            val, obj ->
                if(BaseModel.get(obj.id)){
                    return !BaseModel.findByKategoriKendaraanAndM102KodeBaseModelIlikeAndM102NamaBaseModelIlikeAndStaDelAndBahanBakarAndIdEqual(obj.kategoriKendaraan,
                            obj.m102KodeBaseModel, obj.m102NamaBaseModel, obj.staDel, obj.bahanBakar, obj.id)
                }

                    return !BaseModel.findByKategoriKendaraanAndM102KodeBaseModelIlikeAndM102NamaBaseModelIlikeAndStaDelAndM102FotoAndM102FotoWACAndBahanBakar(obj.kategoriKendaraan,
                            obj.m102KodeBaseModel, obj.m102NamaBaseModel, obj.staDel, obj.m102Foto, obj.m0102.FotoWAC, obj.bahanBakar)

        }
    }

    String toString(){
        return m102KodeBaseModel;
    }
}
