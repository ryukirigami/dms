package com.kombos.administrasi
class VendorCat {

	String m191ID
	String m191Nama
	String m191PIC
	String m191Alamat
	String m191Telp
	String m191Email
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		m191ID unique : true, nullable : false, blank : false, maxSize : 10
		m191Nama nullable : false, blank : false, maxSize : 50
		m191PIC nullable : false, blank : false, maxSize : 50
		m191Alamat nullable : false, blank : false, maxSize : 1000
		m191Telp nullable : false, blank : false, maxSize : 50
		m191Email nullable : true, blank : true, maxSize : 50//, email:true
		createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        staDel nullable : false, maxSize : 1
    }
	
	static mapping = {
		table 'M191_VENDORCAT'
		m191ID column : 'M191_ID', sqlType : 'varchar(10)'
		m191Nama column : 'M191_Nama', sqlType : 'varchar(50)'
		m191PIC column : 'M191_PIC', sqlType : 'varchar(50)'
		m191Alamat column : 'M191_Alamat', sqlType : 'varchar(1000)'
		m191Telp column : 'M191_Telp', sqlType : 'varchar(50)'
		m191Email column : 'M191_Email', sqlType : 'varchar(50)'
		staDel column : 'M191_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m191Nama
	}
}
