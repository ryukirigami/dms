package com.kombos.administrasi

import java.util.Date;

class Country {
	int m109ID
	String m109KodeNegara
	String m109NamaNegara
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
		m109ID blank:true, nullable:true, maxSize:4, unique:false
		m109KodeNegara blank:false, nullable:false, maxsize:2
		m109NamaNegara blank:false, nullable:false, maxsize:20
		staDel blank:false, nullable:false, maxsize:1
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M109_COUNTRY"
		m109ID column:"M109_ID", sqlType:"int"
		m109KodeNegara column:"M109_KodeNegara", sqlType:"varchar(2)"
		m109NamaNegara column:"M109_NamaNegara", sqlType:"varchar(20)"
		staDel column:"M109_StaDel", sqlType:"char(1)"
	}
	
	String toString() {
		return m109KodeNegara
	}
}
