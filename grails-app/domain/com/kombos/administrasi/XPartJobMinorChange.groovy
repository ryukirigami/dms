package com.kombos.administrasi

class XPartJobMinorChange {
    CompanyDealer companyDealer
    Operation operation
	FullModelCode fullModelCode
	PartsMapping partsMapping
	Double t112xJumlah1
	String t112xThnBln
	Double t112xJumlah2
	NamaProsesBP namaProsesBP
	String t112xStaTampilDiWO
	String t112xStaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T112_XPARTJOBMINORCHANGE'
		
		operation column : 'T112_xM053_JobID'
		fullModelCode column : 'T112_xT110_FullModelCode'
		partsMapping column : 'T112_xM111_ID'
		t112xThnBln column : 'T112_xThnBln', sqlType : 'char(6)'
		t112xJumlah1 column : 'T112_xJumlah1', sqlType : 'float'
		t112xJumlah2 column : 'T112_xJumlah2', sqlType : 'float'
		namaProsesBP column : 'T112_xM190_ID'
		t112xStaTampilDiWO column : 'T112_xStaTampilDiWO', sqlType : 'char(1)'
		t112xStaDel column : 'T112_xStaDel', sqlType : 'char(1)'
	}
    static constraints = {
        companyDealer (blank:true, nullable:true)
		operation (nullable : false, blank : false)
		fullModelCode (nullable : false, blank : false)
		partsMapping (nullable : false, blank : false)
		t112xJumlah1 (nullable : false, blank : false, maxSize : 8)
		t112xJumlah2 (nullable : false, blank : false, maxSize : 8)
		namaProsesBP (nullable : false, blank : false)
		t112xThnBln (nullable : false, blank : false)
		t112xStaTampilDiWO (nullable : false, blank : false, maxSize : 1)
		t112xStaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }
	
	String toString(){
		return operation.toString()
	}
}
