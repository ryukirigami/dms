package com.kombos.administrasi

class MappingCompanyRegion {
    CompanyDealer companyDealer
    Region region
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        table 'M011B_MAPPINGCOMPANYREGION'
        region column : 'M011B_M011A_ID'
        companyDealer column : 'M011B_M011_ID'
        staDel column : 'M011B_StaDel', sqlType : 'char(1)'
    }
    static constraints = {
        region (nullable : true, blank : true)
        companyDealer (nullable : true, blank : true)
        staDel (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
