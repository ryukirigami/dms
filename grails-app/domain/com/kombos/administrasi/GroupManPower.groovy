package com.kombos.administrasi

import java.util.Date;

class GroupManPower {
	
	CompanyDealer companyDealer
	String t021Group
	NamaManPower namaManPowerForeman
    static hasMany = [namaManPowers : NamaManPower]
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	static mapping = {
        autoTimestamp false
        table 'T021_GROUPMANPOWER'
		
		t021Group column : 'T021_Group', sqlType : 'varchar(10)'
		namaManPowerForeman : 'T021_T015_IDManPower'
		companyDealer : 'T021_M011_ID'
		staDel column : 'T021_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		t021Group (nullable : true, blank : true, unique : false, maxSize : 10)
		namaManPowerForeman (nullable : false, blank : false, unique:false)
		companyDealer (nullable : true, blank : true)
		staDel (nullable : false, blank : false, maxSize : 1)
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t021Group
	}
}
