package com.kombos.administrasi

class ColorMatching {
	
	CompanyDealer companyDealer
	Date m046TglBerlaku
	int m046Klasifikasi
	int m046JmlPanel
	double m046StdTime
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        companyDealer column : 'M046_M011_ID'
		m046TglBerlaku column : 'M046_TglBerlaku', sqlType : 'date'
		m046Klasifikasi column : 'M046_Klasifikasi', sqlType : 'int'
		m046JmlPanel column : 'M046_JmlPanel', sqlType : 'int'
		m046StdTime column : 'M046_StdTime'
		table 'M046_COLORMATCHING'
	}

	public String toString() {
		return id
	}

    static constraints = {
        
        m046TglBerlaku blank:false, nullable:false
        m046Klasifikasi blank:false, nullable:false
        m046JmlPanel blank:true, nullable:true
        m046StdTime blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        companyDealer blank:true, nullable:true
    }
}
