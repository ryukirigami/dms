package com.kombos.administrasi

import java.util.Date;

class BahanBakar {

    String m110ID
    String m110NamaBahanBakar
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	
    static constraints = {
        m110ID nullable: false, blank:false
        m110NamaBahanBakar nullable: false, blank:false, maxSize: 50
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        m110ID column : "M110_ID", sqlType:"varchar(10)", unique: true
        m110NamaBahanBakar column: "M110_NamaBahanBakar", sqlType : "varchar(50)"
        table "M110_BAHANBAKAR"
    }

    String toString(){
        return m110NamaBahanBakar;
    }
}
