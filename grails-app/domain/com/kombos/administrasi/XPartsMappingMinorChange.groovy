package com.kombos.administrasi

import com.kombos.parts.Goods
import com.kombos.parts.Satuan

class XPartsMappingMinorChange {
    CompanyDealer companyDealer
    FullModelCode fullModelCode
	Goods goods
	String t111xThn  //Tahun
    String t111xBln //Bulan
	String t111xStaIO
	Double t111xJumlah1
    Satuan satuan
    byte[] gambar
    String imageMime
	Double t111xJumlah2
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T111_XPARTSMAPPINGMINORCHANGE'
		
		fullModelCode column : 'T111_xT110_FullModelCode'
		goods column : 'T111_xM111_ID'
	//	t111xThnBln column : 'T111_xThnBln', sqlType : 'char(6)'
		t111xStaIO column : 'T111_xStaIO', sqlType : 'char(1)'
		t111xJumlah1 column : 'T111_xJumlah1', sqlType : 'float'
		t111xJumlah2 column : 'T111_xJumlah2', sqlType : 'float'
		staDel column : 'T111_xStaDel'
	}

    static constraints = {
        companyDealer blank:true, nullable:true
        fullModelCode (nullable : false, blank : false)
	//	t111xThnBln (nullable : false, blank : false)
		t111xStaIO (nullable : false, blank : false) 
		t111xJumlah1 (nullable : false, blank : false, maxSize : 8)
		t111xJumlah2 (nullable : true, blank : true , maxSize : 8)
		staDel (nullable : true, blank : true)
        gambar (nullable : true, blank : true,maxSize: 10485760)
        imageMime nullable : true, maxSize : 20
        t111xThn maxSize : 20
        t111xBln maxSize : 20
       
        goods (nullable : false, blank : false,
                validator: {val, obj ->
                        return !XPartsMappingMinorChange.findByFullModelCodeAndT111xStaIOAndT111xThnAndT111xBlnAndGoodsAndGambarAndT111xJumlah1AndStaDelLike(obj.fullModelCode,
                            obj.t111xStaIO, obj.t111xThn, obj.t111xBln, val, obj.gambar, obj.t111xJumlah1, obj.staDel)
                })

        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return fullModelCode.toString()
	}
}
