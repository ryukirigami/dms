package com.kombos.administrasi

class RateBank {

	Bank bank
	MesinEdc mesinEdc
	Date m703TglBerlaku
	Double m703JmlMin
	Double m703JmlMax
	String m703StaPersenRp //value p: persen ,  dan r: rupiah
	Double m703RatePersen
	Double m703RateRp
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M703_RATEBANK'
		
		bank column : 'M703_M702_ID'
		mesinEdc column : 'M703_M704_ID'
		m703TglBerlaku column : 'M703_TglBerlaku'
		m703JmlMin column : 'M703_JmlMin', sqlType : 'float'
		m703JmlMax column : 'M703_JmlMax', sqlType : 'float'
		m703StaPersenRp column : 'M703_StaPersenRp', sqlType : 'char(1)'
		m703RatePersen column : 'M703_RatePersen', sqlType : 'float'
		m703RateRp column : 'M703_RateRp', sqlType : 'float'
		staDel column : 'M703_StaDel', sqlType : 'char(1)'
	}
	
    static constraints = {
		bank (nullable : false, blank : false)
		mesinEdc (nullable : false, blank : false)
		m703TglBerlaku (nullable : false, blank : false)
		m703JmlMin (nullable : false, blank : false, maxSize : 8)
		m703JmlMax (nullable : false, blank : false, maxSize : 8)
		m703StaPersenRp (nullable : false, blank : false, maxSize : 1)
		m703RatePersen (nullable : true, blank : true, maxSize : 8)
		m703RateRp (nullable : true, blank : true, maxSize : 8)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m703RateRp
	}
}
