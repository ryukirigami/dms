package com.kombos.administrasi

class Serial {

	Section section
    String m052ID
    String m052NamaSerial
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m052ID column : 'M052_ID', sqlType : 'varchar(3)', unique: true
		m052NamaSerial column : 'M052_NamaSerial', sqlType : 'varchar(50)'
        section column : 'M052_M051_ID'
		staDel column : 'M052_StaDel', sqlType : 'varchar(1)'
		table 'M052_SERIAL'
	}
	
	public String toString() {
		return m052NamaSerial
	}
    static constraints = {
        section blank:false, nullable:false
        m052ID blank:false, nullable:false
        m052NamaSerial blank:false, nullable:false
        staDel blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
