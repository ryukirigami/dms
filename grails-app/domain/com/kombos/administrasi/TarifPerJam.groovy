package com.kombos.administrasi

class TarifPerJam {

	CompanyDealer companyDealer
	KategoriKendaraan kategori
    BaseModel baseModel
	Date  t152TMT
	Double t152TarifPerjamGR
	Double t152TarifPerjamSB
	Double t152TarifPerjamBP
	Double t152TarifPerjamPDS
	Double t152TarifPerjamSBI
    Double t152TarifPerjamDealer
	Double t152TarifPerjamSPO
	String staDel
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer unique:false, nullable: false, blank:true
		kategori unique:false , nullable : false, blank : true
		baseModel unique: false, nullable: true
        t152TMT unique:false , nullable : false, blank : true
		t152TarifPerjamSB nullable : false, blank : true, maxSize : 8, unique : false
		t152TarifPerjamGR nullable : false, blank : true, maxSize : 8, unique : false
		t152TarifPerjamBP nullable : false, blank : true, maxSize : 8, unique : false
        t152TarifPerjamPDS nullable : true, blank : true
        t152TarifPerjamPDS nullable: true, blank : true
        t152TarifPerjamSBI nullable: true, blank : true
		t152TarifPerjamSPO nullable: true, blank : true
        t152TarifPerjamDealer nullable: true, blank : true
		staDel nullable : true, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T152_TARIFPERJAM'
		companyDealer column : 'T152_M011_ID'
		kategori column : 'T152_M101_ID'
		t152TMT column : 'T152_TMT', sqlType :'date'
		t152TarifPerjamSB column : 'T152_TarifPerjamSB'
		t152TarifPerjamGR column : 'T152_TarifPerjamGR'
		t152TarifPerjamBP column : 'T152_TarifPerjamBP'
        t152TarifPerjamPDS column: 'T152_TarifCuciPDS'
        t152TarifPerjamSBI column: 'T152_TarifSBI'
		t152TarifPerjamSPO column: 'T152_TarifSPO'
        t152TarifPerjamDealer column: 'T152_TarifDealer'
		staDel column : 'T152_StaDel', sqlType : 'char(1)'
	}

	String toString(){
		return t152TMT
	}
}