package com.kombos.administrasi

class Stall {
    String m022StallID
	CompanyDealer companyDealer
	JenisStall jenisStall
//    boolean showProsesBP=false
	Lift lift
    SubJenisStall subJenisStall
	NamaProsesBP namaProsesBP
	String m022ID
	String m022NamaStall
	String m022Singkat
	String m022StaTPSLine
	String m022StaTHS
	String m022StaTersedia
	Date m022TglJamAwal
	Date m022TglJamAkhir
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	
    static constraints = {
        companyDealer nullable : false, blank : false
        jenisStall nullable : false, blank : false
        subJenisStall nullable : false, blank : false
        m022NamaStall nullable : false, blank : false, maxSize: 50
        m022Singkat nullable : false, blank : false, maxSize: 25
        m022StaTersedia nullable : false, blank : false
        m022TglJamAwal nullable : true, blank : true
        m022TglJamAkhir nullable : true, blank : true
        lift nullable : false, blank : false
        m022StaTPSLine nullable : false, blank : false
        m022StaTHS nullable : true, blank : true
        namaProsesBP nullable : true, blank : true
        staDel nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        m022StallID nullable : true, blank : true, maxSize: 25
        m022ID nullable : true, blank : true
    }
	
	static mapping = {
        autoTimestamp false
        m022StallID column : 'M022_StallID', sqlType : 'varchar(25)',unique: true
		companyDealer column : 'M022_M011_ID'
		jenisStall column : 'M022_M021_ID'
        subJenisStall column : 'M022_M023_ID'
		m022ID column : 'M022_ID', sqlType : 'char(2)'
		m022NamaStall column : 'M022_NamaStal', sqlType : 'varchar(50)'
		m022Singkat column : 'M022_Singkat', sqlType : 'varchar(25)'
		lift column : 'M022_M029_ID'
		m022StaTPSLine column : 'M022_StaTPSLine', sqlType : 'char(1)'
        m022StaTHS column : 'M022_StaTHS', sqlType : 'char(1)'
        namaProsesBP column : 'M022_M190_ID'
		m022StaTersedia column : 'M022_StaTersedia', sqlType : 'char(1)'
		m022TglJamAwal column : 'M022_TglJamAwal'//, sqlType : 'date'
		m022TglJamAkhir column : 'M022_TglJamAkhir'//, sqlType : 'date'
		staDel column : 'M022_StaDel', sqlType : 'char(1)'
		table 'M022_STALL'
	}
	
	String toString(){
		return m022NamaStall
	}
}
