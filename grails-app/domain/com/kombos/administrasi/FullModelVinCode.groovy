package com.kombos.administrasi

import com.kombos.customerprofile.CustomerVehicle


class FullModelVinCode {
    CompanyDealer companyDealer
	CustomerVehicle customerVehicle
	String t109WMI
	String t109VDS
	String t109CekDigit
	String t109VIS
	FullModelCode fullModelCode
	String t109ThnBlnPembuatan
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'T109_FULLMODELVINCODE'

        customerVehicle column : 'T109_T103_VinCode'
		t109WMI column : 'T109_WMI', sqlType : 'varchar(10)'
		t109VDS column : 'T109_VDS', sqlType : 'varchar(10)'
		t109CekDigit column : 'T109_CekDigit', sqlType : 'varchar(10)'
		t109VIS column : 'T109_VIS', sqlType : 'varchar(10)'
		fullModelCode column : 'T109_T110_FullModelCode'
		t109ThnBlnPembuatan column : 'T109_ThnBlnPembuatan', sqlType : 'char(6)'
		staDel column : 'T109_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer blank:true, nullable:true
        customerVehicle (nullable : true, blank : true)
		t109WMI (nullable : false, blank : false, maxSize : 10)
		t109VDS (nullable : false, blank : false, maxSize : 10)
		t109CekDigit (nullable : false, blank : false, maxSize : 10)
		t109VIS (nullable : false, blank : false, maxSize : 10)
		fullModelCode (nullable : false, blank : false)
		t109ThnBlnPembuatan (nullable : false, blank : false, maxSize : 6)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }
	
	String toString(){
		return customerVehicle.toString()
	}
}
