package com.kombos.administrasi

class OperationDetail {

	Operation jobID
	Operation jobIdDetail
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        jobID nullable : false, blank : false
        jobIdDetail nullable : false, blank : false
        staDel nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }
	
	static mapping = {
        jobID column : 'M054_M053_JobID'
        jobIdDetail column : 'M054_M053_JobID_Detail'
		staDel column : 'M054_StaDel', sqlType : 'char(1)'
		table 'M054_OPERATIONDETAIL'
	}
	
	
}
