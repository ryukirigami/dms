package com.kombos.administrasi

import com.sun.org.apache.xerces.internal.impl.xs.identity.UniqueOrKey;

class DefaultWarna {

	WarnaVendor warnaVendor
	VendorCat vendorCat
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		warnaVendor column : 'M192_IDWarna'
		vendorCat column : 'M192_M191_VendorCat'
		table 'm197_DEFAULTWARNA'
	}
	
	
	String toString(){
		return warnaVendor
	}
	
    static constraints = {
    	warnaVendor (nullable : false, blank : false)
		vendorCat (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
