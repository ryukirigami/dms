package com.kombos.administrasi

class UserRole {
    CompanyDealer companyDealer
    String t003ID
    String t003NamaRole
    Double t003MaxDiscGoodwillPersen
    Double t003MaxDiscGoodwillRp
    Double t003MaxPersenSpDiscRp
    String t003StaLihatReportWorkshopLain
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer nullable: false, blank:false
        t003ID nullable: false, blank:false
        t003NamaRole nullable: false, blank:false
        t003MaxDiscGoodwillPersen nullable: false, blank:false
        t003MaxDiscGoodwillRp nullable: false, blank:false
        t003MaxPersenSpDiscRp nullable: false, blank:false
        t003StaLihatReportWorkshopLain nullable: false, blank:false
        staDel nullable: false, blank:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        t003ID column : "T003_ID", sqlType : "char(2)"
        companyDealer column: "T003_M011_ID"
        t003NamaRole column: "T003_NamaRole", sqlType : "varchar(20)"
        t003MaxDiscGoodwillPersen column: "T003_MaxDiscGoodwillPersen"
        t003MaxDiscGoodwillRp column: "T003_MaxDiscGoodwillRp"
        t003MaxPersenSpDiscRp column: "T003_MaxPersenSpDiscRp"
        t003StaLihatReportWorkshopLain column: "T003_StaLhtReportWorkshopLain", sqlType : "char(1)"
        staDel column: "T003_StaDel", sqlType : "char(1)"
        table "T003_USERROLE"
    }

    String toString(){
        return t003NamaRole;
    }
}
