package com.kombos.administrasi

class PriceList {
	CompanyDealer companyDealer
	Operation operation
	BaseModel baseModel
	Date m095TMT
	Double m095Jasa
	Double m095Parts
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false
        operation blank:false, nullable:false
		baseModel blank:false, nullable:false
        m095TMT blank:false, nullable:false
        m095Jasa blank:false, nullable:false
        m095Parts blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
        autoTimestamp false
        table "M095_PRICELIST"
		companyDealer column:"M095_M011_ID"
        operation column:"M095_M053_JobID"
		baseModel column:"M095_M102_ID"
		m095TMT column:"M095_TMT"//, sqlType: 'date'
		m095Jasa column:"M095_Jasa"
		m095Parts column:"M095_Parts"
	}
}
