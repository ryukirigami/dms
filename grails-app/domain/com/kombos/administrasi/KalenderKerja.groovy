package com.kombos.administrasi

import java.sql.Time

class KalenderKerja {
    CompanyDealer companyDealer
    Date m031ID
    String m031StaLibur
    Double m031JamMulai1
    Double m031JamSelesai1
    Double m031JamMulai2
    Double m031JamSelesai2
    Double m031JamMulai3
    Double m031JamSelesai3
    Double m031JamMulai4
    Double m031JamSelesai4
    String m031Ket
    String m031XNamaUser
    String m031XNamaDivisi
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    String staDel


    static mapping = {
        autoTimestamp false
        m031ID column : "M031_ID",sqlType : 'date', unique: true
        companyDealer column : 'M031_M011_ID', unique: false

        m031StaLibur column : 'M031_StaLibur', sqlType : 'char(1)'
        m031JamMulai1 column : 'M031_JamMulai1'
        m031JamMulai2 column : 'M031_JamMulai2'
        m031JamMulai3 column : 'M031_JamMulai3'
        m031JamMulai4 column : 'M031_JamMulai4'

        m031JamSelesai1 column : 'M031_JamSelesai1'
        m031JamSelesai2 column : 'M031_JamSelesai2'
        m031JamSelesai3 column : 'M031_JamSelesai3'
        m031JamSelesai4 column : 'M031_JamSelesai4'

        m031Ket column : 'M031_Ket', sqlType : 'varchar(255)'
        m031XNamaUser column : 'M031_xNamaUser', sqlType : 'varchar(20)'
        m031XNamaDivisi column : 'M031_xNamaDivisi', sqlType : 'varchar(20)'
        staDel column : 'M031_StaDel', sqlType : 'char(1)'

        table 'M031_KALENDERKERJA'

    }

    static constraints = {
        m031ID nullable: false,blank: false
        companyDealer nullable: false,blank: false

        m031StaLibur nullable: false,blank: false
        m031JamMulai1 nullable: false,blank: false
        m031JamMulai2 nullable: false,blank: false
        m031JamMulai3 nullable: false,blank: false
        m031JamMulai4 nullable: false,blank: false

        m031JamSelesai1 nullable: false,blank: false
        m031JamSelesai2 nullable: false,blank: false
        m031JamSelesai3 nullable: false,blank: false
        m031JamSelesai4 nullable: false,blank: false

        m031Ket nullable: false,blank: false
        m031XNamaUser nullable: false,blank: false
        m031XNamaDivisi nullable: false,blank: false

        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
