package com.kombos.administrasi

class Hass {
    CompanyDealer companyDealer
    Double jumlahUnit
    String tglBulanTahun
    Double tunai_bank
    Double piutang
    Double discJasa
    Double discPart
    Double uangMuka
    Double perbaikanUmum
    Double perbaikanBody
    Double perbaikanLain
    Double hasilParts
    Double hasilMaterial
    Double hasilAdmMaterai
    Double ppn
    Double sublet
    Double pds
    Double goli
    Double cuci
    Double warranty

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
    }
    static mapping = {
        autoTimestamp false
        table 'TBL_HASS'
        tglBulanTahun column:'TGL_BULAN_TAHUN'
        jumlahUnit column:'HASIL_UNIT', defaultValue: '0'
        tunai_bank column:'HASIL_TUNAI_BANK', defaultValue: '0'
        piutang column:'HASIL_PIUTANG', defaultValue: '0'
        discJasa column:'HASIL_DISC_JASA', defaultValue: '0'
        discPart column:'HASIL_DISC_PARTS', defaultValue: '0'
        uangMuka column:'HASIL_UANG_MUKA', defaultValue: '0'
        perbaikanUmum column:'HASIL_PERBAIKAN_UMUM', defaultValue: '0'
        perbaikanBody column:'HASIL_PERBAIKAN_BODY', defaultValue: '0'
        perbaikanLain column:'HASIL_PERBAIKAN_LAIN', defaultValue: '0'
        hasilParts column:'HASIL_PARTS', defaultValue: '0'
        hasilMaterial column:'HASIL_MATERIAL', defaultValue: '0'
        hasilAdmMaterai column:'HASIL_ADM_MTR', defaultValue: '0'
        ppn column:'HASIL_PPN', defaultValue: '0'
        sublet column:'HASIL_SUBLET', defaultValue: '0'
        pds column:'HASIL_PDS', defaultValue: '0'
        goli column:'HASIL_GANTI_OLI', defaultValue: '0'
        cuci column:'HASIL_CUCI', defaultValue: '0'
        warranty column:'HASIL_WARRANTY', defaultValue: '0'
    }
}
