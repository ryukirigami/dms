package com.kombos.administrasi

class UserProfile {
	
	String t001IDUser
	CompanyDealer companyDealer
	String t001Id
	UserRole userRole
	Divisi divisi
	String t001NamaUser
	String t001Password
	String t001PUK
	String t001NamaPegawai
	String t001Inisial
	String t001Email
	byte[] t001Foto
	String t001StaLogin
	Date t001LastLogin
	Date t001LastFailed
	String t001MacAddress
	Integer t001CounterFailed
	Date t001TglRegistered
	Date t001ExpiredAccountDate
	String t001ManPowerPengganti
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table'T001_USERPROFILE'
		t001IDUser column : 'T001_IDUser', sqlType : 'char(9)'
		companyDealer column : 'T001_M011_ID'
		t001Id column : 'T001_ID', sqlType : 'char(5)'
		userRole column : 'T001_T003_ID'
		divisi column : 'T001_M012_ID'
		t001NamaUser column : 'T001_NamaUser', sqlType : 'varchar(20)'
		t001Password column : 'T001_Password', sqlType : 'varchar(50)'
		t001PUK column : 'T001_PUK', sqlType : 'varchar(50)'
		t001NamaPegawai column : 'T001_NamaPegawai', sqlType : 'varchar(20)'
		t001Inisial column : 'T001_Inisial', sqlType : 'varchar(4)'
		t001Email column : 'T001_Email', sqlType :'varchar(50)'
		t001Foto column : 'T001_Foto', sqlType : 'blob'
		t001StaLogin column : 'T001_StaLogin', sqlType : 'char(1)'
		t001LastLogin column : 'T001_LastLogin', sqlType : 'date'
		t001LastFailed column : 'T001_LastFailed', sqlType : 'date'
		t001MacAddress column : 'T001_MacAddress', sqltYtpe : 'varchar(50)'
		t001CounterFailed column : 'T001_CounterFailed', sqlType : 'int'
		t001TglRegistered column : 'T001_TglRegistered', sqlType : 'date'
		t001ExpiredAccountDate column : 'T001_ExpiredAccountDate', sqlType : 'date'
		t001ManPowerPengganti column : 'T001_ManPowerPengganti', sqlType : 'varchar(100)'
		staDel column : 'T001_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		t001IDUser (nullable : false, blank : false, maxSize : 9, unique : true)
		companyDealer (nullable : false, blank : false)
		t001Id (nullable : false, blank : false, maxSize : 9)
		userRole (nullable : false, blank : false)
		divisi (nullable : false, blank : false)
		t001NamaUser (nullable : false, blank : false, maxSize : 20)
		t001Password (nullable : false, blank : false, maxSize : 50)
		t001PUK (nullable : false, blank : false, maxSize : 50)
		t001NamaPegawai (nullable : false, blank : false, maxSize : 20)
		t001Inisial (nullable : false, blank : false, maxSize : 4)
		t001Email (nullable : false, blank : false, maxSize : 50)
		t001Foto (nullable : false, blank : false)
		t001StaLogin (nullable : false, blank : false, maxSize : 1)
		t001LastLogin (nullable : false, blank : false)
		t001LastFailed (nullable : false, blank : false)
		t001MacAddress (nullable : false, blank : false, maxSize : 50)
		t001CounterFailed (nullable : false, blank : false, maxSize : 4)
		t001TglRegistered (nullable : false, blank : false)
		t001ExpiredAccountDate (nullable : false, blank : false)
		t001ManPowerPengganti (nullable : false, blank : false, maxSize : 100)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
