package com.kombos.administrasi

class Engine {
    Integer m108ID
    BaseModel baseModel
    ModelName modelName
    BodyType bodyType
    Gear gear
    Grade grade
    String m108KodeEngine
    String m108NamaEngine
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        m108ID blank:true, nullable:true 
        baseModel blank:false, nullable:false
        modelName blank:false, nullable:false
        bodyType blank:false, nullable:false
        gear blank:false, nullable:false
        grade blank:false, nullable:false
        m108KodeEngine blank:false, nullable:false, maxSize:2
        m108NamaEngine blank:false, nullable:false, maxSize:50
        staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
    static mapping = {
        autoTimestamp false
        table "M108_ENGINE"
        m108ID column:"M108_ID", sqlType:"int"
        baseModel column:"M108_M102_ID"
        modelName column:"M108_M104_ID"
        bodyType column:"M108_M105_ID"
        gear column:"M108_M106_ID"
        grade column:"M108_M107_ID"
        m108KodeEngine column:"M108_KodeEngine", sqlType:"varchar(2)"
        m108NamaEngine column:"M108_NamaEngine", sqlType:"varchar(50)"
        staDel column:"M108_StaDel", sqlType:"char(1)"
    }
        
    String toString(){
        return m108KodeEngine
    }
}
