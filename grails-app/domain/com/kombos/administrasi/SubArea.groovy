package com.kombos.administrasi

class SubArea {
	SearchArea searchArea
    Integer m082ID
    String m082NamaSubArea
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        searchArea blank:false, nullable:false
        m082ID blank:false, nullable:false
        m082NamaSubArea blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
		table "M082_SUBAREA"
		searchArea column:"M082_M081_ID",unique: true
        m082ID column:"M082_ID", sqlType:"int",unique: true
		m082NamaSubArea column:"M082_NamaSubArea", sqlType:"varchar(50)"
	}
}
