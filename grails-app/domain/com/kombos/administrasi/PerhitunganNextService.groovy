package com.kombos.administrasi

class PerhitunganNextService {
    CompanyDealer companyDealer
	Date t114TglBerlaku
	String t114KategoriService
	BahanBakar bahanBakar
	Double t114Km
	Double t114Bulan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t114TglBerlaku nullable : false, blank : false
		t114KategoriService nullable : false, blank : false, maxSize : 1
		bahanBakar nullable : false, blank : false
		t114Km (nullable : false, blank : false, maxSize : 8, matches : "[0-9]+")
		t114Bulan (nullable : false, blank : false, maxSize : 8, matches : "[0-9]+")
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T114_PERHITUNGANNEXTSERVICE'
		t114TglBerlaku column : 'T114_TglBerlaku', sqlType : 'date'
		t114KategoriService column : 'T114_KategoriService', sqlType :'char(1)'
		bahanBakar column : 'T114_M110_ID'
		t114Km column : 'T114_Km'
		t114Bulan column : 'T114_Bulan'
	}
	
	String toString(){
		return t114KategoriService
	}
}
