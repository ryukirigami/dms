package com.kombos.administrasi

class Loket {

	int m404Id
	CompanyDealer companyDealer
	int m404NoLoket
	String m404NamaLoket
	String m404Ket
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m404Id column : 'M404_ID', sqlType : 'int', length : 4
		table 'M404_LOKET'
		
		m404NoLoket column : 'M404_NoLoket', sqlType : 'int', length : 4
		m404NamaLoket column : 'M404_NamaLoket', sqlType : 'varchar(50)', length : 50
		m404Ket column : 'M404_Ket', sqlType : 'varchar(60)', length : 1000
	}
	
	String toString(){
		return m404NamaLoket
	}
	
    static constraints = {
		m404Id (nullable : true, blank : true)//, unique : true, maxSize : 4)
		m404NoLoket (nullable : false, blank : false, maxSize : 4)
		m404NamaLoket (nullable : false, blank : false, maxSize : 50)
    /*    m404NamaLoket (validator: {val, obj ->
            return !Loket.findByM404NamaLoketIlikeAndM404NoLoketAndCompanyDealer(val, obj.m404NoLoket, obj.companyDealer)
        })
	*/	m404Ket (nullable : false, blank : false, maxSize : 1000)
        companyDealer (nullable : true, blank : true)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
