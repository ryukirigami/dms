package com.kombos.administrasi

class JenisAlert {

//    static final Jenis_Alert = 'Jenis Alert'
	String m901Id
	String m901NamaAlert
	String m901StaPerluTindakLanjut
	String m901NamaFormTindakLanjut
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M901_JENISALLERT'
		
		m901Id column : 'M901_ID', sqlType : 'char(3)'
		m901NamaAlert column : 'M901_NamaAlert', sqlType : 'varchar(20)'
		m901StaPerluTindakLanjut column : 'M901_StaPerluTindakLanjut', sqlType : 'char(1)'
		m901NamaFormTindakLanjut column : 'M901_NamaFormTindakLanjut', sqlType : 'varchar(50)'
		staDel column : 'M901_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		m901Id (nullable : false, blank : false, maxSize : 3)
		m901NamaAlert (nullable : false, blank : false, maxSize : 20)
		m901StaPerluTindakLanjut (nullable : false, blank : false, maxSize : 1)
		m901NamaFormTindakLanjut (nullable : false, blank : false, maxSize : 50)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m901NamaAlert
	}
}
