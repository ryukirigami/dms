package com.kombos.administrasi

import java.util.Date;

class StatusManPower {
    CompanyDealer companyDealer
	NamaManPower namaManPower
	Date t023Tmt
	//String t023StaBooking
	//String t023StaBookingWeb
	String t023Sta
	Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
	String createdBy //Menunjukkan siapa yang buat isian di baris ini.
	Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
	String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
	String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T023_STATUSMANPOWER'
		
		namaManPower column : 'T023_T015_IDManPower'
		t023Tmt column : 'T023_TMT', sqlType : 'date'
		//t023StaBooking column : 'T023_StaBooking', sqlType : 'char(1)'
		//t023StaBookingWeb column : 'T023_StaBookingWeb', sqlType : 'char(1)'
		t023Sta column : 'T023_Sta', sqlType : 'varchar(20)'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
		namaManPower (nullable : false, blank : false)
		t023Tmt (nullable : false, blank : false)
		//t023StaBooking (nullable : false, blank : false)
		//t023StaBookingWeb (nullable : false, blank : false)
		t023Sta (nullable : false, blank : false)
		createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
		updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
		lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return namaManPower.toString()
	}
}
