package com.kombos.administrasi

import com.kombos.parts.Goods
import com.kombos.parts.Satuan

class PartsMapping {
    CompanyDealer companyDealer
	FullModelCode fullModelCode
	Goods goods
	Double t111Jumlah1
	Satuan satuan
	Double t111Jumlah2
	String staDel
    byte[] foto
    String imageMime
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T111_PARTSMAPPING'
		
		fullModelCode column : 'T111_T110_FullModelCode'
		goods column : 'T111_M111_ID'
		t111Jumlah1 column : 'T111_Jumlah1', sqlType : 'float'
		t111Jumlah2 column : 'T111_Jumlah2', sqlType : 'float'
		staDel column : 'T111_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
		fullModelCode (nullable : false, blank : false, validator: {
            val, obj ->

                if(PartsMapping.get(obj.id)){
                    return !PartsMapping.findByFullModelCodeAndGoodsAndStaDelAndIdNotEqual(obj.fullModelCode, obj.goods, obj.staDel, obj.id)

                }else{
                    return !PartsMapping.findByFullModelCodeAndGoodsAndStaDel(obj.fullModelCode, obj.goods, obj.staDel)
                }
        })
		t111Jumlah1 (nullable : false, blank : false, maxSize : 8)
		t111Jumlah2 (nullable : true, blank : true, maxSize : 8)
		staDel (nullable : false, blank : false, maxSize : 1)
        satuan nullable : true
        foto (nullable : true, blank : false,maxSize: 10485760) //10mb
        imageMime maxSize : 50, nullable : true
        goods (nullable : false, blank : false, validator: {
            val, obj ->
                return !PartsMapping.findByFullModelCodeAndT111Jumlah1AndSatuanAndFotoAndGoodsAndStaDel(obj.fullModelCode, obj.t111Jumlah1, obj.satuan, obj.foto, val, obj.staDel)
        })

        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return fullModelCode.toString()
	}
}
