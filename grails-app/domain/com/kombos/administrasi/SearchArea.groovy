package com.kombos.administrasi

class SearchArea {

    Integer m081ID
    String m081NamaArea
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m081ID blank:true, nullable:true
        m081NamaArea blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
		table "M081_SEARCHAREA"
        m081ID column:"M081_ID", sqlType:"int", unique: true
		m081NamaArea column:"M081_NamaArea", sqlType:"varchar(50)"
	}
	
	public String toString() {
		return m081NamaArea;
	}
}
