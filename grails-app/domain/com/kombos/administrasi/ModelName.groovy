package com.kombos.administrasi

class ModelName {
	int m104ID
	BaseModel baseModel
	String m104KodeModelName
	String m104NamaModelName
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m104ID blank:false, nullable:false
		baseModel nullable: false, blank:false
		m104KodeModelName nullable: false, blank:false, maxSize:2
		m104NamaModelName nullable: false, blank:false, maxSize:30
		staDel nullable: false, blank:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
        autoTimestamp false
        table "M104_MODELNAME"
		m104ID column:"M104_ID", sqlType:'int'
		baseModel column:"M104_M102_ID"
		m104KodeModelName column:"M104_KodeModelName", sqlType:"varchar(2)"
		m104NamaModelName column:"M104_NamaModelName", sqlType:"varchar(30)"
		staDel column:"M104_StaDel", sqlType:"char(1)"
	}
        
    String toString(){
        return m104KodeModelName
    }
}
