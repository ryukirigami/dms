package com.kombos.administrasi


class DinamicDisc {
	
	CompanyDealer companyDealer
	Date m020TMT
	Double m020BookingSBEProg
	Double m020BookingSBEMax
	Double m020NonBookingSBEProg
	Double m020NonBookingSBEMax
	String m020StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer nullable : false, blank : false
        m020TMT nullable : false, blank : true
        m020BookingSBEProg nullable : true, blank : true
        m020BookingSBEMax nullable : true, blank : true
        m020NonBookingSBEProg nullable : true, blank : true
        m020NonBookingSBEMax nullable : true, blank : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        companyDealer column : 'M018_M001_ID'
		m020TMT column : 'M020_TMT', sqlType : 'date'
		m020BookingSBEProg column : 'M020_BookingSBEProg'
		m020BookingSBEMax column : 'M020_BookingSBEMax'
		m020NonBookingSBEProg column : 'M020_NonBookingSBEProg'
		m020NonBookingSBEMax column : 'M020_NonBookingSBEMax'
		table 'M020_DINAMICDISC'
	}
	
	String toString() {
		return m020TMT
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
        if(m020StaDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "insert"
//		lastUpdated = new Date()
		m020StaDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!m020StaDel)
			m020StaDel = "0"
		if(!createdBy){
			createdBy = "_SYSTEM_"
			updatedBy = createdBy
		}
	}
}