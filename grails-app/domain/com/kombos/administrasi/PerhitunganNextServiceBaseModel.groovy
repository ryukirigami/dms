package com.kombos.administrasi

class PerhitunganNextServiceBaseModel {
    CompanyDealer companyDealer
	Date t115TglBerlaku
	String t115KategoriService
	BaseModel baseModel
	Double t115Km
	Double t115Bulan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	
    static constraints = {
        companyDealer (blank:true, nullable:true)
		t115TglBerlaku nullable : false, blank : false
		t115KategoriService nullable : false, blank : false, maxSize : 1
		baseModel  nullable : false, blank : false
		t115Km nullable : false, blank : false, maxSize : 8, matches : "[0-9]+"
		t115Bulan nullable : false, blank : false, maxSize : 8, matches : "[0-9]+"
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
	}
	
	static mapping = {
        autoTimestamp false
        table 'T115_PRHITNGNNXTSRVCBSMODEL'
		t115TglBerlaku column : 'T115_TglBerlaku', sqlType : 'date'
		t115KategoriService column : 'T115_KategoriService', sqlType :'char(1)'
		baseModel column : 'T115_M102_ID'
		t115Km column : 'T115_Km'
		t115Bulan column : 'T115_Bulan'
	}
	
	String toString() {
		return t115KategoriService
	};
}
