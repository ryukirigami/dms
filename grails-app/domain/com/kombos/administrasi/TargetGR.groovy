package com.kombos.administrasi

class TargetGR {
	
	CompanyDealer companyDealer
	Date m037TglBerlaku
	Double m037TargetUnitSales
	Double m037TargetCPUS
	Double m037TargetSBI
	Double m037TargetSBE
	Double m037TargetAmountSales
	Double m037COGSPart
	Double m037COGSOil
	Double m037COGSSublet
	Double m037COGSLabor
	Double m037COGSOverHead
	Double m037COGSDeprecation
	Double m037COGSTotalSGA
	Double m037TotalClaimItem
	Double m037TotalClaimAmount
	Double m037TechReport
	Double m037TWCLeadTime
	Double m037YTDS
	Double m037YTDD
	Double m037ThisMonthS
	Double m037ThisMonthD
	Double m037Q1S
	Double m037Q1D
	Double m037Q2S
	Double m037Q2D
	Double m037Q3S
	Double m037Q3D
	Double m037Q4S
	Double m037Q4D
	Double m037Q5S
	Double m037Q5D
	Double m037Q6S
	Double m037Q6D
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    String staDel

    static constraints = {
		m037TglBerlaku blank:false, nullable:false
		companyDealer blank:false, nullable:false
		m037TargetUnitSales nullable: true, blank: true
		m037TargetCPUS nullable: true, blank: true
		m037TargetSBI nullable: true, blank: true
		m037TargetSBE nullable: true, blank: true
		m037TargetAmountSales nullable: true, blank: true
		m037COGSPart nullable: true, blank: true
		m037COGSOil nullable: true, blank: true
		m037COGSSublet nullable: true, blank: true
		m037COGSLabor nullable: true, blank: true
		m037COGSOverHead nullable: true, blank: true
		m037COGSDeprecation nullable: true, blank: true
		m037COGSTotalSGA nullable: true, blank: true
		m037TotalClaimItem nullable: true, blank: true
		m037TotalClaimAmount nullable: true, blank: true
		m037TechReport nullable: true, blank: true
		m037TWCLeadTime nullable: true, blank: true
		m037YTDS nullable: true, blank: true
		m037YTDD nullable: true, blank: true
		m037ThisMonthS nullable: true, blank: true
		m037ThisMonthD nullable: true, blank: true
		m037Q1S nullable: true, blank: true
		m037Q1D nullable: true, blank: true
		m037Q2S nullable: true, blank: true
		m037Q2D nullable: true, blank: true
		m037Q3S nullable: true, blank: true
		m037Q3D nullable: true, blank: true
		m037Q4S nullable: true, blank: true
		m037Q4D nullable: true, blank: true
		m037Q5S nullable: true, blank: true
		m037Q5D nullable: true, blank: true
		m037Q6S nullable: true, blank: true
		m037Q6D nullable: true, blank: true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        staDel maxSize : 1, nullable : false, blank : false
    }
	
	static mapping ={
		table "M037_TARGETGR"
		companyDealer column : "M037_M011_ID"
		m037TglBerlaku column:"M037_TglBerlaku", unique: true//, sqlType: 'date'
		m037TargetUnitSales column:"M037_TargetUnitSales"
		m037TargetCPUS column:"M037_TargetCPUS"
		m037TargetSBI column:"M037_TargetSBI"
		m037TargetSBE column:"M037_TargetSBE"
		m037TargetAmountSales column:"M037_TargetAmountSalaes"
		m037COGSPart column:"M037_COGSPart"
		m037COGSOil column:"M037_COGSOil"
		m037COGSSublet column:"M037_COGSSublet"
		m037COGSLabor column:"M037_COGSLabor"
		m037COGSOverHead column:"M037_COGSOverHead"
		m037COGSDeprecation column:"M037_COGSDeprecation"
		m037COGSTotalSGA column:"M037_COGSTotalSGA"
		m037TotalClaimItem column:"M037_TotalClaimItem"
		m037TotalClaimAmount column:"M037_TotalClaimAmount"
		m037TechReport column:"M037_TechReport"
		m037TWCLeadTime column:"M037_TWCLEadTime"
		m037YTDS column:"M037_YTDS"
		m037YTDD column:"M037_YTDD"
		m037ThisMonthS column:"M037_ThisMonthS"
		m037ThisMonthD column:"M037_ThisMonthD"
		m037Q1S column:"M037_Q1S"
		m037Q1D column:"M037_Q1D"
		m037Q2S column:"M037_Q2S"
		m037Q2D column:"M037_Q2D"
		m037Q3S column:"M037_Q3S"
		m037Q3D column:"M037_Q3D"
		m037Q4S column:"M037_Q4S"
		m037Q4D column:"M037_Q4D"
		m037Q5S column:"M037_Q5S"
		m037Q5D column:"M037_Q5D"
		m037Q6S column:"M037_Q6S"
		m037Q6D column:"M037_Q6D"
        staDel column : 'M037_StaDel', sqlType : 'char(1)'
	}
}
