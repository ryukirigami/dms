package com.kombos.administrasi

class Stir {
	Integer m103ID
	String m103KodeStir
	String m103NamaStir
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m103ID blank:false, nullable:true
		m103KodeStir blank:false, nullable:false, maxSize:2
		m103NamaStir blank:false, nullable:false, maxSize:20
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
		table 'M103_STIR'
		m103ID column:'M103_ID', sqlType:'int'
		m103KodeStir column:'M103_KodeStir', sqlType:'varchar(2)'
		m103NamaStir column:'M103_NamaStir', sqlTypr:'varchar(20)'
		staDel column:'M103_StaDel', sqlType:'char(1)'
	}
	
	String toString (){
		return m103KodeStir;
	}
}
