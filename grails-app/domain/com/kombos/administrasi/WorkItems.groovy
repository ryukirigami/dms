package com.kombos.administrasi
class WorkItems {
	CompanyDealer companyDealer
	Integer m039WorkID
	String m039WorkItems
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m039WorkItems nullable: false, blank: false
		companyDealer blank:false, nullable: false
		m039WorkID blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table "M039_WORKITEMS"
		companyDealer column : "M039_M011_ID"
		m039WorkID column:"M039_WorkID", sqlType:'int'
		m039WorkItems column:"M039_WorkItems"//, sqlType:"text"
	}

    String toString(){
        return m039WorkItems
    }
}
