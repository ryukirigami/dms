package com.kombos.board

import com.kombos.administrasi.AlasanRefund
import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FlatRate
import com.kombos.administrasi.GroupManPower
import com.kombos.administrasi.ManPowerStall
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Operation
import com.kombos.administrasi.Stall
import com.kombos.reception.Appointment
import com.kombos.reception.Reception;

class ASB {
    CompanyDealer companyDealer
    Reception reception
    Appointment appointment
//    static hasMany = [appointments : Appointment]
	Date t351TglJamApp
	Date t351TglJamFinished
	Operation operation
	NamaManPower namaManPower
	Stall stall
	FlatRate flatRate
	String t351StaOkCancelReSchedule
	String t351StaBookingWeb
	String t351StaTHS
	String t351OkBookingWeb
	AlasanRefund alasanRefund
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:true, nullable:true
        appointment blank:true, nullable:true
		t351TglJamApp blank:true, nullable:true
		t351TglJamFinished blank:true, nullable:true
		operation blank:true, nullable:true, maxSize:7
		namaManPower blank:true, nullable:true, maxSize:10
		stall blank:true, nullable:true, maxSize:8
		flatRate blank:true, nullable:true, maxSize:8
		t351StaOkCancelReSchedule blank:true, nullable:true, maxSize:1
		t351StaBookingWeb blank:true, nullable:true, maxSize:1
		t351StaTHS blank:true, nullable:true, maxSize:1
		t351OkBookingWeb blank:true, nullable:true, maxSize:1
		alasanRefund blank:true, nullable:true, maxSize:1
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T351_ASB'
		reception column:'T351_T401_NoWO'
		t351TglJamApp column:'T351_TglJamApp', sqlType:'TIMESTAMP'
		t351TglJamFinished column:'T351_TglJamFinished'
		operation column:'T351_M053_JobID'
		namaManPower column:'T351_T015_IDManPower'
		stall column:'T351_M022_StallID'
		flatRate column:'T351_T113_FlatRate'
		t351StaOkCancelReSchedule column:'T351_StaOkCancelReSchedule', sqlType:'char(1)'
		t351StaBookingWeb column:'T351_StaBookingWeb', sqlType:'char(1)'
		t351StaTHS column:'T351_StaTHS', sqlType:'char(1)'
		t351OkBookingWeb column:'T351_OkBookingWeb', sqlType:'char(1)'
		alasanRefund column:'T351_M071_ID'
		staDel column:'T351_StaDel', sqlType:'char(1)'
	}
}
