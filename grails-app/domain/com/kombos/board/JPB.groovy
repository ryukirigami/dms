package com.kombos.board

import com.kombos.administrasi.AlasanRefund
import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FlatRate
import com.kombos.administrasi.ManPowerStall
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Stall
import com.kombos.parts.GoodsReceiveDetail
import com.kombos.reception.Appointment
import com.kombos.reception.Reception;

class JPB {
	CompanyDealer companyDealer
    static hasMany = [appointments : Appointment]
	String t451IDJPB
	AlasanRefund alasanRefund
	Reception reception
	Date t451TglJamPlan
    Date t451TglJamPlanFinished
    NamaManPower namaManPower
    Stall stall
	FlatRate flatRate
	String t451StaTambahWaktu
	String t451staOkCancelReSchedule
	String t451AlasanReSchedule
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false
		t451IDJPB blank:true, nullable:true
		alasanRefund blank:true, nullable:true
		reception blank:false, nullable:false
		t451TglJamPlan blank:false, nullable:false
		t451TglJamPlanFinished blank:false, nullable:false
		namaManPower blank:false, nullable:false
		stall blank:false, nullable:false
		flatRate blank:true, nullable:true
		t451StaTambahWaktu blank:false, nullable:false, maxSize:1
		t451staOkCancelReSchedule blank:false, nullable:false, maxSize:1
		t451AlasanReSchedule blank:true, nullable:true
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T451_JPB'
		companyDealer column:'T451_M011_ID'
		t451IDJPB column:'T451_IDJPB', sqlType:'char(12)'
		alasanRefund column:'T451_M071_ID '
		reception column:'T451_T401_NoWO'
		t451TglJamPlan column:'T451_TglJamPlan'
		t451TglJamPlanFinished column:'T451_TglJamPlanFinished'
		namaManPower column:'T451_T015_IDManPower'
		stall column:'T451_M022_StallID'
		flatRate column:'T451_T113_FlatRate'
		t451StaTambahWaktu column:'T451_StaTambahWaktu', sqlType:'char(1)'
		t451staOkCancelReSchedule column:'T451_staOkCancelReSchedule', sqlType:'char(1)'
		t451AlasanReSchedule column:'T451_AlasanReSchedule', sqlType:'varchar(50)'
		staDel column:'T451_StaDel', sqlType:'char(1)'
	}
}
