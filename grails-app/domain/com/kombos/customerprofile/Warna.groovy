package com.kombos.customerprofile

class Warna {

    String m092ID
    String m092NamaWarna
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m092ID nullable:false, blank:false
        m092NamaWarna nullable:false, blank:false
        staDel nullable:false, blank:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
        autoTimestamp false
        table "M092_WARNA"
        m092ID column:"M092_ID", sqlType:'varchar(4)', unique: true
		m092NamaWarna column:"M092_NamaWarna", sqlType:"varchar(50)"
		staDel column:"M092_StaDel", sqlType:"char(1)"
	}
	
	String toString(){
		return m092NamaWarna+" || "+m092ID
	}
}
