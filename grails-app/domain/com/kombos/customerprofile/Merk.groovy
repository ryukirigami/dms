package com.kombos.customerprofile

class Merk {

    String m064ID
    String m064NamaMerk
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        m064ID blank:false, nullable: true
        m064NamaMerk blank:true, nullable: true, maxSize : 50
        staDel blank:false, nullable: true, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        m064ID column : 'M064_ID', sqlType : 'varchar(4)', unique: true
		m064NamaMerk column : 'M064_NamaMerk', sqlType : 'varchar(50)'
        staDel column : 'M064_StaDel', sqlType : 'char(1)'
		table 'M064_MERK'
	}
	
	String toString() {
		return m064NamaMerk
	}
}
