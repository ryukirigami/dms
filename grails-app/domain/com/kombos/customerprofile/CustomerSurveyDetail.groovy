package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer

class CustomerSurveyDetail {
    CompanyDealer companyDealer
	CustomerVehicle customerVehicle
    CustomerSurvey customerSurvey
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        table 'T104A_CUSTOMERSURVEYDETAIL'
        customerVehicle column : 'T104A_T103_VINCODE'
        customerSurvey column : 'T104A_T104_SURVEY'
        staDel column : 'T104A_StaDel', sqlType : 'char(1)'
    }

    static constraints = {
        companyDealer (blank:true, nullable:true)
		customerVehicle (nullable : false, blank : false)
        customerSurvey (nullable : false, blank : false)
        staDel (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    String toString(){
        return customerVehicle?.t103VinCode
    }
}
