package com.kombos.customerprofile

class StaImport {
	String m100ID
	String m100KodeStaImport
	String m100NamaStaImport
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m100ID blank:false, nullable:false, unique:true
		m100KodeStaImport blank:false, nullable:false, maxSize:50
		m100NamaStaImport blank:false, nullable:false, maxSize:50
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping ={
        autoTimestamp false
        table "M100_STAIMPORT"
		m100ID column:"M100_ID", sqlType:"char(5)"
		m100KodeStaImport column:"M100_KodeStaImport", sqlType:"varchar(50)"
		m100NamaStaImport column:"M100_NamaStaImport", sqlType:"varchar(50)"
		staDel column:"M100_StaDel", sqlType:"char(1)"
	}
	
	String toString (){
		return m100NamaStaImport;
	}
}
