package com.kombos.customerprofile

class Hobby {

    String m063ID
    String m063NamaHobby
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        m063ID blank:false, nullable: true, maxSize : 2, matches : "[0-9]{2}"//validasi untuk format 00
        m063NamaHobby blank:true, nullable : true , maxSize : 20
        staDel blank:false, nullable : true , maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        m063ID column : 'M063_ID', sqlType : 'char(2)'
		m063NamaHobby column : 'M063_NamaHobby', sqlType : 'varchar(20)'
        staDel column : 'M063_StaDel', sqlType : 'char(1)'
		table 'M063_HOBBY'
	}
	
	String toString(){
		return m063NamaHobby
	}
}
