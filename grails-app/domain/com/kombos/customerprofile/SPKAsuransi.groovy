package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi

class SPKAsuransi {
    CompanyDealer companyDealer
	String t193IDAsuransi
	VendorAsuransi vendorAsuransi
	CustomerVehicle customerVehicle
	String t193ID
	String t193StaNomorSPK
	String t193NomorSPK
	Date t193TanggalSPK
	String t193NomorPolis
	String t193NamaPolis
	String t193AlamatPolis
	String t193TelpPolis
	Date t193TglPolis
	Double t193JmlPolis
	Date t193TglAwal
	Date t193TglAkhir
    String t193StaUtama
    String t193StaUtamaTambahan
	String t193StaGantiTambahan
    SPKAsuransi spkAsuransi
	String t193StaJanjiKirimDok
	Date t193TglJanjiKirimDok
	String t193StaKirimDok
	Date t193TglKirimDok
	String t193MetodeKirim
	String t193StaReminderSurvey
	Date t193TglReminderSurvey
	String t193StaActualSurvey
	Date t193TglActualSurvey
	String t193Catatan
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static hasMany = [ spkAsuransiTambahan: SPKAsuransi]

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t193IDAsuransi nullable : true, blank : true, maxSize : 24
		vendorAsuransi nullable : false, blank : false
		customerVehicle nullable : false, blank : false
		t193ID nullable : true, blank : true, maxSize : 4
		t193StaNomorSPK nullable : true, blank : true, maxSize : 1
		t193NomorSPK nullable : false, blank : false, maxSize : 50
		t193TanggalSPK nullable : true, blank : true
		t193NomorPolis nullable : false, blank : false, maxSize : 10
		t193NamaPolis nullable : false, blank : false, maxSize : 50
		t193AlamatPolis nullable : true, blank : true, maxSize : 255
		t193TelpPolis nullable : true, blank : true, maxSize : 50
		t193TglPolis nullable : true, blank : true
		t193JmlPolis nullable : true, blank : true
		t193TglAwal nullable : true, blank : true
		t193TglAkhir nullable : true, blank : true
        t193StaUtama nullable : true, blank : true, maxSize : 1
        t193StaUtamaTambahan nullable : true, blank : true, maxSize : 1
		t193StaGantiTambahan nullable : true, blank : true, maxSize : 1
        spkAsuransi nullable : true, blank : true
		t193StaJanjiKirimDok nullable : true, blank : true, maxSize : 1
		t193TglJanjiKirimDok nullable : true, blank : true
		t193StaKirimDok nullable : true, blank : true, maxSize : 1
		t193TglKirimDok nullable : true, blank : true
		t193MetodeKirim nullable : true, blank : true, maxSize : 50
		t193StaReminderSurvey nullable : true, blank : true, maxSize : 1
		t193TglReminderSurvey nullable : true, blank : true
		t193StaActualSurvey nullable : true, blank : true, maxSize : 1
		t193TglActualSurvey nullable : true, blank : true
		t193Catatan nullable : true, blank : true, maxSize : 255
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T193_SPKASURANSI'
		t193IDAsuransi column : 'T193_IDAsuransi'
		vendorAsuransi column : 'T193_M193_ID'
		customerVehicle column : 'T193_T103_VinCode'
		t193ID column : 'T193_ID', sqlType : 'char(4)'
		t193StaNomorSPK column : 'T193_StaNomorSPK', sqlType : 'char(1)'
		t193NomorSPK column : 'T193_NomorSPK', sqlType : 'varchar(50)'
		t193TanggalSPK column : 'T193_TanggalSPK', sqlType : 'date'
		t193NomorPolis column : 'T193_NomorPolis', sqlType : 'char(10)'
		t193NamaPolis column : 'T193_NamaPolis', sqlType : 'varchar(50)'
		t193AlamatPolis column : 'T193_AlamatPolis', sqlType : 'varchar(50)'
		t193TelpPolis column : 'T193_TelpPolis', sqlType : 'varchar(50)'
		t193TglPolis column : 'T193_TglPolis',sqlType : 'date'
		t193JmlPolis column : 'T193_JmlPolis'
		t193TglAwal column : 'T193_TglAwal',sqlType : 'date'
		t193TglAkhir column : 'T193_TglAkhir',sqlType : 'date'
        t193StaUtama column : 'T193_StaUtama', sqlType : 'char(1)'
        t193StaUtamaTambahan column : 'T193_StaUtamaTambahan', sqlType : 'char(1)'
		t193StaGantiTambahan column : 'T193_StaGantiTambahan', sqlType : 'char(1)'
        spkAsuransi column : 'T193_T193_IDAsuransiReff'
		t193StaJanjiKirimDok column : 'T193_StaJanjiKirimDok', sqlType : 'char(1)'
		t193TglJanjiKirimDok column : 'T193_TglJanjiKirimDok',sqlType : 'date'
		t193StaKirimDok column : 'T193_StaKirimDok', sqlType : 'char(1)'
		t193TglKirimDok column : 'T193_TglKirimDok',sqlType : 'date'
		t193MetodeKirim column : 'T193_MetodeKirim', sqlType : 'varchar(50)'
		t193StaReminderSurvey column : 'T193_StaReminderSurvey', sqlType : 'char(1)'
		t193TglReminderSurvey column : 'T193_TglReminderSurvey',sqlType : 'date'
		t193StaActualSurvey column : 'T193_StaActualSurvey', sqlType : 'char(1)'
		t193TglActualSurvey column : 'T193_TglActualSurvey',sqlType : 'date'
		t193Catatan column : 'T193_Catatan', sqlType : 'varchar(50)'
		staDel column : 'T193_StaDel', sqlType : 'char(1)'
	}

    String toString()
    {
        return t193StaNomorSPK+"-"+t193IDAsuransi
    }
}
