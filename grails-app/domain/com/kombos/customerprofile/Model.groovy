package com.kombos.customerprofile

class Model {

	Merk merk
    String m065ID
	String m065NamaModel
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m065ID blank:false, nullable:false
        merk blank:false, nullable:false
        m065NamaModel blank:false, nullable:false, maxSize: 50
        staDel blank:false, nullable:false, maxSize: 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M065_MODEL"
        m065ID column:"M065_ID", sqlType:"varchar(4)", unique: true
		merk column:"M065_M064_ID"
		m065NamaModel column:"M065_NAmaModel", sqlType:"varchar(50)"
        staDel column:"M065_StaDel", sqlType:"char(1)"
	}
	
	String toString(){
		return m065NamaModel
    }
}
