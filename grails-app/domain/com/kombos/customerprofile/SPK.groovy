package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.Company

class SPK {
    CompanyDealer companyDealer
	String t191IDSPK
	String t191NoSPK
    Company company
	Date t191TanggalSPK
	Date t191TglAwal
	Date t191TglAkhir
	Double t191JmlSPK
	Integer t191MaxHariPelunasan
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t191IDSPK nullable : false, blank : false
		t191NoSPK nullable : false, blank : false, maxSize : 50, unique : true
		t191TanggalSPK nullable : false, blank : false
		t191TglAwal nullable : false, blank : false
		t191TglAkhir nullable : false, blank : false
		t191JmlSPK nullable : false, blank : false
		t191MaxHariPelunasan nullable : false, blank : false
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        company nullable : true, unique : false
    }

	static mapping = {
        autoTimestamp false
        table 'T191_SPK'
        t191IDSPK column : 'T191_IDSPK'
		t191NoSPK column : 'T191_NoSPK' , sqlType : 'varchar(50)'
		t191TanggalSPK column : 'T191_TanggalSPK' , sqlType : 'date'
		t191TglAwal column : 'T191_TglAwal' , sqlType : 'date'
		t191TglAkhir column : 'T191_TglAkhir' , sqlType : 'date'
		t191JmlSPK column : 'T191_JmlSPK'
		t191MaxHariPelunasan column : 'T191_MaxHariPelunasan' , sqlType : 'int'
       	staDel column : 'T191_StaDel', sqlType : 'char(1)'
	}

    String toString(){
        return t191NoSPK
    }
}
