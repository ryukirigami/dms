package com.kombos.customerprofile

import com.kombos.hrd.Karyawan

class Agama {

    String m061ID
    String m061NamaAgama
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m061ID blank:false, nullable : false , maxSize : 2, matches : "[0-9]{2}"//validasi untuk format 00
        m061NamaAgama blank:false, nullable : false , maxSize : 20
        staDel blank:false, nullable : false , maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        m061ID column : 'M061_ID', sqlType : 'char(2)'
		m061NamaAgama column : 'M061_NamaAgama', sqlType : 'varchar(20)'
		staDel column : 'M061_StaDel', sqlType : 'char(1)'
		table 'M061_AGAMA'
		
	}
	
	String toString(){
		return m061NamaAgama
	}

    static hasMany = [karyawan: Karyawan]
}
