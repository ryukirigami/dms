package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.Company

class SPKDetail {
    CompanyDealer companyDealer
    Company company
	SPK spk
	CustomerVehicle customerVehicle
	String staDel
    Date dateCreated // Menunjukksan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		spk nullable : false, blank : false
		customerVehicle nullable : false, blank : false, unique: ['spk', 'company']
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }

	static mapping = {
        autoTimestamp false
        table 'T192_SPKDETAIL'
		spk column : 'T192_T191_ID'
		customerVehicle column : 'T192_T103_VinCode'
		staDel column : 'T192_StaDel', sqlType : 'char(1)'
	}




}
