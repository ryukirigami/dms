package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.Diskon
import com.kombos.maintable.MappingCustVehicle

class CustomerVehicle {

	String t103VinCode
    byte[] t103BukuService
	CompanyDealer companyDealer
//    HistoryCustomerVehicle currentCondition
//    Diskon currentDiskon

	static hasMany = [diskons: Diskon, histories: HistoryCustomerVehicle, mappingCustVehicles: MappingCustVehicle]
	static hasOne = [spkAsuransi: SPKAsuransi]
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	
	static mapping = {
		table 'T103_CUSTOMERVEHICLE'
		
		t103VinCode column : 'T103_VinCode'//, sqlType : 'char(17)' // DIILANGIN SQL TYPE, data sampel di bootstrap tidak sesuai dengan char(17)
		companyDealer column : 'T103_M011_ID'
		staDel column : 'T103_StaDel', sqlType : 'char(1)'
        histories sort:'dateCreated'
        diskons sort:'lastUpdated'
        t103BukuService column: 'T103_BukuService', sqlType: 'blob'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t103VinCode (nullable : false, blank : false, unique : true, maxSize : 21)
        t103BukuService nullable: true, blank : true
		spkAsuransi nullable: true
        staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
//        currentCondition nullable: true
//        currentDiskon nullable: true
    }

	String toString()
	{
		return t103VinCode
	}

    static transients = ['currentCondition','currentDiskon']

    HistoryCustomerVehicle getCurrentCondition() {
        return (histories==null||histories.isEmpty())?null:histories.last()
    }

    Diskon getCurrentDiskon() {
        return (diskons==null||diskons.isEmpty())?null:diskons.first()
    }
}
