package com.kombos.customerprofile

import com.kombos.maintable.JenisFA

import java.text.SimpleDateFormat

class FA {

    static SimpleDateFormat format = new SimpleDateFormat("yyyyMM")

//    Integer m185ID
    Date m185TanggalFA
    JenisFA jenisFA
    String m185NomorSurat
    String m185NamaFA
    String m185ThnBlnRakit1 = format.format(new Date())
    String m185ThnBlnRakit2 = format.format(new Date())
    String m185xNamaUser
    String m185xNamaDivisi
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
//        m185ID blank: true, nullable: true, unique: true, maxSize: 4, display: false
        m185TanggalFA blank: false, nullable: false
        jenisFA blank: true, nullable: true
        m185NomorSurat blank: false, nullable: false, maxSize: 50
        m185NamaFA blank: false, nullable: false, maxSize: 50
        m185ThnBlnRakit1 blank: false, nullable: false, maxSize: 6
        m185ThnBlnRakit2 blank: false, nullable: false, maxSize: 6
        m185xNamaUser blank: true, nullable: true, maxSize: 20, display: false
        m185xNamaDivisi blank: true, nullable: true, maxSize: 20, display: false
        createdBy nullable: true, display: false //Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true, display: false //Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true, display: false //Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'M185_FA'
        id column: 'M185_ID'
        m185TanggalFA column: 'M185_TanggalFA'//, sqlType: 'date'
        jenisFA column: 'M185_M184_ID'
        m185NomorSurat column: 'M185_NomorSurat', sqlType: 'varchar(50)'
        m185NamaFA column: 'M185_NamaFA', sqlType: 'varchar(50)'
        m185ThnBlnRakit1 column: 'M185_ThnBlnRakit1', sqlType: 'char(6)'
        m185ThnBlnRakit2 column: 'M185_ThnBlnRakit2', sqlType: 'char(6)'
        m185xNamaUser column: 'M185_xNamaUser', sqlType: 'varchar(20)'
        m185xNamaDivisi column: 'M185_xNamaDivisi', sqlType: 'varchar(20)'
    }

    String toString() {
        return m185NamaFA
    }
}
