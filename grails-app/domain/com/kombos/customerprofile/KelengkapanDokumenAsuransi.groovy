package com.kombos.customerprofile

class KelengkapanDokumenAsuransi {

	int m195Id
	String m195NamaDokumenAsuransi
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m195Id column : 'M195_ID', sqltype : 'int', length : 4
		m195NamaDokumenAsuransi column : 'M195_NamaDokumenAsuransi', sqltype : 'varchar', length : 50
		table 'M195_NAMADOKUMENASURANSI'
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return m195NamaDokumenAsuransi
	}
    
	static constraints = {
		m195Id (nullable : false, unique : true, blank : false, maxSize : 4)
		m195NamaDokumenAsuransi (nullable : false, blank : false, maxSize : 50)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
