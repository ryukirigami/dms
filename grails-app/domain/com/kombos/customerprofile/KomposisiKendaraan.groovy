package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.Company
import com.kombos.maintable.Customer

class KomposisiKendaraan {
    CompanyDealer companyDealer
	Company company
	Customer customer
	Model model
    Merk merk
	String t105Id
	String t105TipeKepemilikan
	Integer t105Jumlah
	String t105Tahun
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'T105_KOMPOSISIKENDARAAN'
		company column : 'T105_T101'
		customer column : 'T105_T102'
		model column : 'T105_M065_ID'
        merk column :'T015_M064_ID'
		t105Id column : 'T105_ID', sqlType : 'int'
		t105TipeKepemilikan column : 'T105_TipeKepemilikan', sqlType : 'char(1)'
		t105Jumlah column : 'T105_Jumlah', sqlType : 'int'
		t105Tahun column : 'T105_Tahun', sqlType : 'char(4)'
		staDel column : 'T105_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
        company (nullable : false, blank : false)
        customer (nullable : true, blank : true)
        merk (nullable : false, blank : false)
		model (nullable : false, blank : false)
		t105Id (nullable : true, blank : true, maxSize : 4)
		t105TipeKepemilikan (nullable : true, blank : true, maxSize : 1)
		t105Jumlah (nullable : false, blank : false, maxSize : 4)
		t105Tahun (nullable : false, blank : false, maxSize : 4)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t105Id
	}
}
