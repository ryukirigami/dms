package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KabKota
import com.kombos.administrasi.Kecamatan
import com.kombos.administrasi.Kelurahan
import com.kombos.administrasi.Leasing
import com.kombos.administrasi.Provinsi
import com.kombos.administrasi.VendorAsuransi
import com.kombos.maintable.Company
import com.kombos.maintable.Customer
import com.kombos.maintable.Nikah
import com.kombos.maintable.PeranCustomer
import org.apache.shiro.SecurityUtils
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class HistoryCustomer {
    CompanyDealer companyDealer
    Customer customer
    String t182ID
    Company company
    PeranCustomer peranCustomer
    String t182GelarD
    String t182NamaDepan
    String t182NamaBelakang
    String t182GelarB
    String t182JenisKelamin
    String t182Pekerjaan
    Date t182TglLahir
    Integer t182TglLahirMonth
    Integer t182TglLahirDate

    String jenisCustomer
    String t182NPWP
    String t182NamaSesuaiNPWP
    String t182NoFakturPajakStd
    JenisIdCard jenisIdCard
    String t182NomorIDCard
    Agama agama
    Nikah nikah
    String t182NoTelpRumah
    String t182NoTelpKantor
    String t182NoFax
    String t182NoHp
    String t182CPTambahan
    String t182Email
    String t182StaTerimaMRS
    String t182Alamat
    String t182RT
    String t182RW
    Kelurahan kelurahan
    Kecamatan kecamatan
    KabKota kabKota
    Provinsi provinsi
    String t182KodePos
    byte[] t182Foto
    String t182AlamatNPWP
    String t182RTNPWP
    String t182RWNPWP
    Kelurahan kelurahan2
    Kecamatan kecamatan2
    KabKota kabKota2
    Provinsi provinsi2
    String t182KodePosNPWP

//    String t182StaCompany
//    Integer t182HMinusKirimSMS
    String t182WebUserName
    String t182WebPassword
    String t182Ket
//    String t182xNamaUser
//    String t182xNamaDivisi
    Date t182TglTransaksi
	String fullNama

    Date terakhirTanggalKirimUlangTahun

    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static SimpleDateFormat codeFormat = new SimpleDateFormat("yyyyMMdd")

    static String generateCode() {
        String date = codeFormat.format(new Date())
        Integer noUrut = Customer.countByT102IDIlike("$date%")
        String kode = "000000" + noUrut
        kode = kode.substring(kode.length() - 4)
        return date + kode
    }


    static hasMany = [hobbies: Hobby]

    static def JENIS_KELAMIN = [[id: "1", nama: "Laki-Laki"], [id: "2", nama: "Perempuan"]]

    static constraints = {
        companyDealer (blank:true, nullable:true)
        customer nullable: true, blank: true
        t182ID nullable: true, blank: true//, unique: true
        company nullable: true, blank: true
        peranCustomer nullable: true, blank: true
        t182GelarD nullable: true, blank: true, maxSize: 50
        t182NamaDepan nullable: true, blank: true, maxSize: 150
        t182NamaBelakang nullable: true, blank: true, maxSize: 50
        t182GelarB nullable: true, blank: true, maxSize: 50
        t182Pekerjaan nullable: true, blank: true, maxSize: 50
        t182JenisKelamin nullable: true, blank: true, maxSize: 1, inList: ["1", "2"]
        t182TglLahir nullable: true, blank: true
        jenisCustomer nullable: true, inList: ["Personal", "Corporate"]

        t182NPWP nullable: true, blank: true, maxSize: 20
        t182NamaSesuaiNPWP nullable: true, blank: true, maxSize: 100
        t182NoFakturPajakStd nullable: true, blank: true, maxSize: 20
        jenisIdCard nullable: true, blank: true
        t182NomorIDCard nullable: true, blank: true, maxSize: 20
        agama nullable: true, blank: true
        nikah nullable: true, blank: true
        t182NoTelpRumah nullable: true, blank: true, maxSize: 50
        t182NoTelpKantor nullable: true, blank: true, maxSize: 50
        t182NoFax nullable: true, blank: true, maxSize: 50
        t182NoHp nullable: true, blank: true, maxSize: 20
        t182CPTambahan nullable: true, blank: true, maxSize: 50
        t182Email nullable: true, blank: true, maxSize: 50,email: true
        t182StaTerimaMRS nullable: true, blank: true, maxSize: 1
        t182Alamat nullable: true, blank: true, maxSize: 255
        t182RT nullable: true, blank: true, maxSize: 50
        t182RW nullable: true, blank: true, maxSize: 50
        kelurahan nullable: true, blank: true
        kecamatan nullable: true, blank: true
        kabKota nullable: true, blank: true
        provinsi nullable: true, blank: true
        t182KodePos nullable: true, blank: true, maxSize: 5
        t182Foto nullable: true, blank: true
        t182AlamatNPWP nullable: true, blank: true, maxSize: 255
        t182RTNPWP nullable: true, blank: true, maxSize: 20
        t182RWNPWP nullable: true, blank: true, maxSize: 20
        kelurahan2 nullable: true, blank: true
        kecamatan2 nullable: true, blank: true
        kabKota2 nullable: true, blank: true
        provinsi2 nullable: true, blank: true
        t182KodePosNPWP nullable: true, blank: true, maxSize: 5
//        t182StaCompany nullable: true, blank: true, maxSize: 1
//        t182HMinusKirimSMS nullable: true, blank: true
        t182WebUserName nullable: true, blank: true, maxSize: 50
        t182WebPassword nullable: true, blank: true, maxSize: 255
        t182Ket nullable: true, blank: true, size: 0..1000
//        t182xNamaUser nullable: true, blank: true, maxSize: 20
//        t182xNamaDivisi nullable: true, blank: true, maxSize: 20
        t182TglTransaksi nullable: true, blank: true
        hobbies minSize: 0
        staDel nullable: true, blank: true, maxSize: 1
        createdBy nullable: true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true//Baris ini, terakhir prosess apa ?? insert, update, delete

        terakhirTanggalKirimUlangTahun nullable: true
        t182TglLahirMonth nullable: true
        t182TglLahirDate nullable: true
    }

    static mapping = {
        autoTimestamp false
        table 'T182_HISTORYCUSTOMER'
        hobbies joinTable: [name: 'T182_M063_Join',
                key: 'T182_M063_Id',
                column: 'Hobby_Id']
        customer column: 'T182_T102_ID'
        t182ID column: 'T182_ID'
        company column: 'T182_T101_ID'
        peranCustomer column: 'T182_M115_ID'
        t182GelarD column: 'T182_GelarD', sqlType: 'varchar(50)'
        t182NamaDepan column: 'T182_NamaDepan', sqlType: 'varchar(50)'
        t182NamaBelakang column: 'T182_NamaBelakang', sqlType: 'varchar(50)'
        t182GelarB column: 'T182_GelarB', sqlType: 'varchar(50)'
        t182Pekerjaan column: 'T182_Pekerjaan', sqlType: 'varchar(50)'
        t182JenisKelamin column: 'T182_JenisKelamin', sqlType: 'char(1)'
        t182TglLahir column: 'T182_TglLahir'//, sqlType: 'date'
        t182NPWP column: 'T182_NPWP', sqlType: 'varchar(20)'
        t182NamaSesuaiNPWP column: 't182_NamaSesuaiNPWP', sqlType: 'varchar(100)'
        t182NoFakturPajakStd column: 'T182_NoFakturPajakStd', sqlType: 'varchar(20)'
        jenisIdCard column: 'T182_M060_ID'
        t182NomorIDCard column: 'T182_NomorIDCard', sqlType: 'varchar(20)'
        agama column: 'T182_M061_ID'
        nikah column: 'T182_M062_ID'
        t182NoTelpRumah column: 'T182_TelpRumah', sqlType: 'varchar(50)'
        t182NoTelpKantor column: 'T182_TelpKantor', sqlType: 'varchar(50)'
        t182NoFax column: 'T182_NoFax', sqlType: 'varchar(50)'
        t182NoHp column: 'T182_NoHp', sqlType: 'varchar(20)'
        t182CPTambahan column: 'T182_CPTambahan', sqlType: 'varchar(50)'
        t182Email column: 'T182_Email', sqlType: 'varchar(50)'
        t182StaTerimaMRS column: 'T182_StaTerimaMRS'
        t182Alamat column: 'T182_Alamat', sqlType: 'varchar(255)'
        t182RT column: 'T182_RT', sqlType: 'varchar(20)'
        t182RW column: 'T182_RW', sqlType: 'varchar(20)'
        kelurahan column: 'T182_M004_ID'
        kecamatan column: 'T182_M003_ID'
        kabKota column: 'T182_M002_ID'
        provinsi column: 'T182_M001_ID'
        t182KodePos column: 'T182_KodePos', sqlType: 'char(5)'
        t182Foto column: 'T182_Foto', sqlType: 'blob'
        t182AlamatNPWP column: 'T182_AlamatNPWP', sqlType: 'varchar(255)'
        t182RTNPWP column: 'T182_RTNPWP', sqlType: 'varchar(20)'
        t182RWNPWP column: 'T182_RWNPWP', sqlType: 'varchar(20)'
        kelurahan2 column: 'T182_M004_IDNPWP'
        kecamatan2 column: 'T182_M003_IDNPWP'
        kabKota2 column: 'T182_M002_IDNPWP'
        provinsi2 column: 'T182_M001_IDNPWP'
        t182KodePosNPWP column: 'T182_KodePosNPWP', sqlTye: 'varchar(20)'
//        t182StaCompany column: 'T182_StaCompany', sqlTye: 'char(1)'
//        t182HMinusKirimSMS column: 'T182_HMinusKirimSMS', sqlTye: 'int'
        t182WebUserName column: 'T182_WebUserName', sqlTye: 'varchar(50)'
        t182WebPassword column: 'T182_WebPassword', sqlTye: 'varchar(255)'
//        t182Ket column: 'T182_Ket', sqlTye: 'varchar(50)'
//        t182xNamaUser column: 'T182_xNamaUser', sqlTye: 'varchar(20)'
//        t182xNamaDivisi column: 'T182_xNamaDivisi', sqlTye: 'varchar(20)'
        t182TglTransaksi column: 'T182_TglTransaksi', sqlTye: 'date'
        staDel column: 'T182_StaDel', sqlTye: 'char(1)'
        terakhirTanggalKirimUlangTahun column: 'lastdatesendbday'
		fullNama formula: "case when T182_GelarD is not null then T182_GelarD || ' ' end || T182_NamaDepan || case when T182_NamaBelakang is not null then ' ' || T182_NamaBelakang end"
    }

    def beforeUpdate = {
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
        lastUpdated = new Date();
        if (customer != null) {
            customer.setUpdatedBy(updatedBy)
            customer.setLastUpdated(lastUpdated)
            customer.lastUpdProcess = "UPDATE"
            customer.lastUpdated = lastUpdated
            customer.peranCustomer = peranCustomer
        }

        if(t182TglLahir){
            Calendar calendar = Calendar.getInstance()
            calendar.time = t182TglLahir
            t182TglLahirMonth = calendar.get(Calendar.MONTH)+1
            t182TglLahirDate = calendar.get(Calendar.DATE)
        }

    }

    def beforeInsert = {
        dateCreated = new Date()
        lastUpdated = new Date()
//        createdBy = SecurityUtils?.subject?.principal?.toString()
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
        lastUpdProcess = "INSERT"
        staDel = "0"

        if(t182TglLahir){
            Calendar calendar = Calendar.getInstance()
            calendar.time = t182TglLahir
            t182TglLahirMonth = calendar.get(Calendar.MONTH)+1
            t182TglLahirDate = calendar.get(Calendar.DATE)
        }
    }
}
