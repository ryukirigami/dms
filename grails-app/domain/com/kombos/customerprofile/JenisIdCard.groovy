package com.kombos.customerprofile

class JenisIdCard {

    String m060ID
    String m060JenisIDCard
    String staDel
    Date dateCreated //Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m060ID blank: false, nullable: false, maxSize : 2
        m060JenisIDCard blank: false, nullable: false, maxSize : 50
        staDel blank: false, nullable: false, maxSize : 1
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        m060ID column : 'M060_ID', sqlType : 'char(2)'
		m060JenisIDCard column : 'M060_JenisIDCard', sqlType :'char(50)'
		staDel column : 'M060_StaDel', sqlType : 'char(1)'
		table 'M060_JENISIDCARD'
	}
	
	String toString(){
		return m060JenisIDCard
	}
}
