package com.kombos.customerprofile

class JenisSurvey {

    String m131ID
   	String m131JenisSurvey
   	String m131TipeSurvey
   	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

   	static constraints = {
   		m131ID unique : true ,blank : false, nullable : false , maxSize  :4
   		m131JenisSurvey blank : false, nullable : false , maxSize  :20
   		m131TipeSurvey blank : false, nullable : false , maxSize :1
   		staDel nullable : false , maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

   	static mapping = {
        autoTimestamp false
        table 'M131_JENISSURVEY'
   		m131ID column : 'M131_ID', sqlType : 'char(4)', unique: true
   		m131JenisSurvey column : 'M131_JenisSurvey', sqlType : 'varchar(20)'
   		m131TipeSurvey column : 'M131_TipeSurvey', sqlType : 'char(1)'  //0 = “Customer”, 1 = “Vehicle”
        staDel column : 'M131_StaDel', sqlType : 'char(1)'
   	}

   	String toString(){
   		return m131JenisSurvey
   	}
}
