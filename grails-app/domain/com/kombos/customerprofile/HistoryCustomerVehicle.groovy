package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.DealerPenjual
import com.kombos.administrasi.FullModelCode
import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.administrasi.Leasing
import com.kombos.administrasi.VendorAsuransi
import com.kombos.customerprofile.Warna
import com.kombos.reception.Reception

class HistoryCustomerVehicle {
    CompanyDealer companyDealer
    CustomerVehicle customerVehicle

    VendorAsuransi vendorAsuransi
    Leasing leasing
//	Integer t183ID
    String t183NamaSales
	String t183NamaSTNK
	String t183AlamatSTNK
	Date t183TglSTNK
    Integer t183TglSTNKMonth
    Integer t183TglSTNKDate

	KodeKotaNoPol kodeKotaNoPol
	String t183NoPolTengah
	String t183NoPolBelakang
	DealerPenjual dealerPenjual
	Warna warna
	FullModelCode fullModelCode
	Date t183TglDEC
	String t183NoMesin
	String t183ThnBlnRakit
	String t183NoKunci
	String t183StaGantiPemilik
    String t183StaTaxi
	String t183Ket
	Date t183TglEntry
	String t183StaProject
	String t183xNamaUser
	String t183xNamaDivisi
	String t183StaReminderSBI
	String t183StaReminderCustPasif
	Date t183TglTransaksi
    String fullNoPol
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    Date terakhirKirimJatuhTempoTglSTNK

    Integer countReception

	static constraints = {
        companyDealer (blank:true, nullable:true)
        vendorAsuransi nullable: true, blank: true
        leasing nullable: true, blank: true
        t183NamaSales nullable: true, blank: true
		customerVehicle nullable : true, blank: true
		t183NamaSTNK nullable : true, blank: true, maxSize : 50
		t183AlamatSTNK nullable : true, blank: true, maxSize : 250
		t183TglSTNK nullable : true, blank: true
		kodeKotaNoPol nullable : true, blank: true
		t183NoPolTengah nullable : true, blank: true, maxSize : 6
		t183NoPolBelakang nullable : true, blank: true, maxSize : 4
		dealerPenjual nullable : true, blank: true
		warna nullable : true, blank: true
		fullModelCode nullable : true, blank: true
		t183TglDEC nullable : true, blank: true
		t183NoMesin nullable : true, blank: true, maxSize : 20
		t183ThnBlnRakit nullable : true, blank: true, maxSize : 6
		t183NoKunci nullable : true, blank: true, maxSize : 5
		t183StaGantiPemilik nullable : true, blank: true, maxSize : 1
        t183StaTaxi nullable : true, blank: true, maxSize : 1
		t183Ket nullable : true, blank: true, maxSize : 50
		t183TglEntry nullable : true, blank: true
		t183StaProject nullable : true, blank: true, maxSize : 1
		t183xNamaUser nullable : true, blank: true, maxSize : 20
		t183xNamaDivisi nullable : true, blank: true, maxSize : 20
		t183StaReminderSBI nullable : true, blank: true, maxSize : 1
		t183StaReminderCustPasif nullable : true, blank: true, maxSize : 1
		t183TglTransaksi nullable : true, blank: true
		staDel nullable : false, blank: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

        terakhirKirimJatuhTempoTglSTNK nullable: true
        t183TglSTNKMonth nullable: true
        t183TglSTNKDate nullable: true
    }
	
	static mapping = {
        autoTimestamp false
        table 'T183_HISTORYCUSTOMERVEHICLE'
		customerVehicle column : 'T183_T103_VinCode'
        t183NamaSales column: 'T183_NamaSales', sqlType: 'varchar(75)'
        vendorAsuransi column: 'T183_M193_ID'
        leasing column: 'T183_M1000_ID'
//		id column : 'T183_ID'
		t183NamaSTNK column : 'T183_NamaSTNK', sqlType : 'varchar(50)'
		t183AlamatSTNK column : 'T183_AlamatSTNK', sqlType : 'varchar(250)'
		t183TglSTNK column : 'T183_TglSTNK', sqlType : 'date'
		kodeKotaNoPol column : 'T183_M116_ID'
		t183NoPolTengah column : 'T183_NoPolTengah', sqlType : 'varchar(15)'
		t183NoPolBelakang column : 'T183_NoPolBelakang', sqlType : 'varchar(15)'
		dealerPenjual column : 'T183_M091_ID'
		warnacolumn : 'T183_M092_ID'
		fullModelCode column : 'T183_T110_FullModelCode'
		t183TglDEC column : 'T183_TglDEC', sqlType : 'date'
		t183NoMesin column : 'T183_NoMesin', sqlType : 'varchar(20)'
		t183ThnBlnRakit column : 'T183_ThnBlnRakit', sqlType : 'char(6)'
		t183NoKunci column : 'T183_NoKunci', sqlType : 'varchar(5)'
		t183StaGantiPemilik column : 'T183_StaGantiPemilik', sqlType : 'char(1)'
        t183StaTaxi column : 'T183_StaTaxi', sqlType : 'char(1)'
		t183Ket column : 'T183_Ket', sqlType : 'varchar(50)'
		t183TglEntry column : 'T183_TglEntry', sqlType : 'date'
		t183StaProject column : 'T183_StaProject', sqlType : 'char(1)'
		t183xNamaUser column : 'T183_xNamaUser', sqlType : 'varchar(20)'
		t183xNamaDivisi column : 'T183_xNamaDivisi', sqlType : 'varchar(20)'
		t183StaReminderSBI column : 'T183_StaReminderSBI', sqlType : 'char(1)'
		t183StaReminderCustPasif column : 'T183_StaReminderCustPasif', sqlType : 'char(1)'
		t183TglTransaksi column : 'T183_TglTransaksi', sqlType : 'date'
		staDel column : 'T183_StaDel', sqlType : 'char(1)'
        terakhirKirimJatuhTempoTglSTNK column: 'lastsendexpdtstnk'
        sort id:'desc'
		fullNoPol formula: "coalesce((SELECT kknp.M116_ID FROM M116_KODEKOTANOPOL kknp WHERE T183_M116_ID=kknp.id),'') || ' ' || coalesce(T183_NoPolTengah,'') || ' ' || coalesce(T183_NoPolBelakang,'')"

        countReception formula: "coalesce((SELECT COUNT(0) FROM T401_RECEPTION r WHERE ID = r.T401_T183_ID), 0)"

    }

    String toString(){
        return kodeKotaNoPol?.m116ID + " " + t183NoPolTengah + " " + t183NoPolBelakang
    }

    def beforeUpdate = {
        lastUpdated = new Date();

        if(t183TglSTNK){
            Calendar calendar = Calendar.getInstance()
            calendar.time = t183TglSTNK
            t183TglSTNKMonth = calendar.get(Calendar.MONTH)
            t183TglSTNKDate = calendar.get(Calendar.DATE)
        }

    }

    def beforeInsert = {
        dateCreated = new Date()
        lastUpdated = new Date()

        if(t183TglSTNK){
            Calendar calendar = Calendar.getInstance()
            calendar.time = t183TglSTNK
            t183TglSTNKMonth = calendar.get(Calendar.MONTH)
            t183TglSTNKDate = calendar.get(Calendar.DATE)
        }
    }
}
