package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer

class DokumenSPK {
    CompanyDealer companyDealer
	SPKAsuransi spkAsuransi
	KelengkapanDokumenAsuransi kelengkapanDokumenAsuransi
	Integer t195ID
	String t195Keterangan
	String t195Foto
	Date t195TglJamUpload
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		spkAsuransi nullable : false, blank : false
		kelengkapanDokumenAsuransi nullable : false, blank : false
		t195ID nullable : true, blank : true
		t195Keterangan nullable : false, blank : false, maxSize : 50
		t195Foto nullable : false, blank : false
		t195TglJamUpload nullable : false, blank : false
        staDel nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping ={
        autoTimestamp false
        table 'T195_DOKUMENSPK'
		spkAsuransi column : 'T195_T193_IDAsuransi'
		kelengkapanDokumenAsuransi column : 'T195_M195_ID'
		t195ID column : 'T195_ID', sqlType :'int'
		t195Keterangan column : 'T195_Keterangan', sqlType : 'varchar(50)'
		t195Foto column : 'T195_Foto', sqlType : 'varchar(1000)'
		t195TglJamUpload column : 'T195_TglJamUpload',sqlType : 'date'
        staDel column : 'T001_StaDel', sqlType : 'char(1)'
	}
}
