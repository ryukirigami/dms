package com.kombos.customerprofile

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.Customer

class CustomerSurvey {
    CompanyDealer companyDealer
//	CustomerVehicle customerVehicle
	Integer t104Id
	JenisSurvey jenisSurvey
	//Customer customer
	//HistoryCustomer historyCustomer
	Date t104TglAwal
	Date t104TglAkhir
	String t104Text
	String t104xNamaUser
	String t104xNamaDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    //static hasMany = [customerVehicles : CustomerVehicle ]

	static mapping = {
        autoTimestamp false
        table 'T104_CUSTOMERSURVEY'

//		customerVehicle column : 'T104_T103_VinCode'
//        customerVehicles joinTable: [name: 'T104_T103_Join',
//                key: 'T104_T103_Id',
//                column: 'Customer_Vehicle_Id']
		t104Id column : 'T104_ID', sqlType : 'int', length : 4
		jenisSurvey column : 'T104_M131_ID'
//		customer column : 'T104_T182_T102_ID'
//		historyCustomer column : 'T104_T182_ID'
		t104TglAwal column : 'T104_TglAwal', sqlType : 'date'
		t104TglAkhir column : 'T104_TglAkhir', sqlType : 'date'
		t104Text column : 'T104_Text', sqlType : 'varchar(50)'
		t104xNamaUser column : 'T104_xNamaUser', sqlType : 'varchar(50)'
		t104xNamaDivisi column : 'T104_xNamaDivisi', sqlType : 'varchar(50)'
		staDel column : 'T104_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
//		customerVehicle (nullable : false, blank : false)
		t104Id (nullable : true, blank : true, maxSize : 4)
		jenisSurvey (nullable : true, blank : true,)
//		customer (nullable : true, blank : false)
//		historyCustomer (nullable : true, blank : false)
		t104TglAwal (nullable : false, blank : false)
		t104TglAkhir(nullable : false, blank : false)
		t104Text (nullable : false, blank : false)
		t104xNamaUser (nullable : true, blank : false)
		t104xNamaDivisi (nullable : true, blank : false)
		staDel (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t104Text
	}
}
