package com.kombos.customerFollowUp

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerFollowUp.MetodeFu
import com.kombos.maintable.KategoriWaktuFu
import com.kombos.reception.Reception

class FollowUp {
    CompanyDealer companyDealer
    Reception reception
    Integer t801ID
    KategoriWaktuFu kategoriWaktuFu
    MetodeFu metodeFu
    Date t801TglJamFU
    String t801StaBerhasilFU
    String t801Ket
    String t801xNamaUser
    String t801xDivisi
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        reception (nullable : false, blank : false)
        t801ID (nullable : false, blank : false, maxSize : 4)
        kategoriWaktuFu (nullable : false, blank : false)
        metodeFu (nullable : false, blank : false)
        t801TglJamFU (nullable : false, blank : false)
        t801StaBerhasilFU (nullable : false, blank : false, maxSize : 1)
        t801Ket (nullable : false, blank : false, maxSize : 50)
        t801xNamaUser (nullable : false, blank : false, maxSize : 20)
        t801xDivisi (nullable : false, blank : false, maxSize : 20)
        staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T801_FOLLOWUP'
        reception column: 'T801_T401_NoWO'
        t801ID column: 'T801_ID', sqlType: 'int'
        kategoriWaktuFu column: 'T801_M804_ID'
        metodeFu column: 'T801_M801'
        t801TglJamFU column: 'T801_TglJamFU'//, sqlType: 'date'
        t801StaBerhasilFU column: 'T801_StaBerhasilFU', sqlType: 'char(1)'
        t801Ket column: 'T801_Ket', sqlType: 'varchar(50)'
        t801xNamaUser column: 'T801_xNamaUser', sqlType: 'varchar(20)'
        t801xDivisi column: 'T801_xDivisi', sqlType: 'varchar(20)'
        staDel column: 'T801_StaDel', sqlType: 'char(1)'

    }
}
