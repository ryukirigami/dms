package com.kombos.customerFollowUp

class PertanyaanFu {
	
	Integer m803Id
	String m803Pertanyaan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M803_PERTANYAANFU'
		
		m803Id column : 'M803_ID', sqlType : 'int'
		m803Pertanyaan column : 'M803_PERTANYAAN', sqlType : 'varchar(510)'
	}

    static constraints = {
		m803Id (nullable : true, blank : true)
		m803Pertanyaan (nullable : false, blank : false, maxSize : 510)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m803Pertanyaan
	}
}
