package com.kombos.customerFollowUp

class MetodeFu {
	
	String m801Id
	String m801NamaMetodeFu
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M801_METODEFU'
		
		m801Id column : 'M801_ID', sqlType : 'char(2)'
		m801NamaMetodeFu column : 'M801_NamaMetodeFu', sqlType : 'varchar(50)'
        staDel column : 'M801_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		m801Id (nullable : false, blank : false, unique : true, maxSize : 2)
		m801NamaMetodeFu (nullable : false, blank : false, maxSize : 50)
        staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m801NamaMetodeFu
	}
}
