package com.kombos.customerFollowUp

import com.kombos.administrasi.CompanyDealer

class RealisasiFU {
    CompanyDealer companyDealer
    //Reception reception
    FollowUp followUp
    Integer t802ID
    String t802NamaSA_FU
    Date t802TglJamFU
    MetodeFu metodeFu
    String t802StaNomorValid
    String t802StaTerhubung
    String t802StaBalasanSMS
    String t802StaPotensiRTJ
    String t802StaCounterMeasure
    String t802CatatanCounterMeasure
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        //reception (nullable : false, blank : false)
        companyDealer (blank:true, nullable:true)
        followUp (nullable : false, blank : false)
        t802ID (nullable : false, blank : false, maxSize : 4)
        t802NamaSA_FU (nullable : false, blank : false, maxSize : 50)
        t802TglJamFU (nullable : false, blank : false)
        metodeFu (nullable : false, blank : false)
        t802StaNomorValid (nullable : false, blank : false, maxSize : 1)
        t802StaTerhubung (nullable : false, blank : false, maxSize : 1)
        t802StaBalasanSMS (nullable : false, blank : false, maxSize : 1)
        t802StaPotensiRTJ (nullable : false, blank : false, maxSize : 1)
        t802StaCounterMeasure (nullable : false, blank : false, maxSize : 1)
        t802CatatanCounterMeasure (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T802_REALISASIFU'
        //reception column: 'T802_T801_T401_NoWO'
        followUp column: 'T802_T801_ID'
        t802ID column: 'T802_ID', sqlType: 'int'
        t802NamaSA_FU column: 'T802_NamaSA_FU', sqlType: 'varchar(50)'
        t802TglJamFU column: 'T802_TglJamFU'//, sqlType: 'date'
        metodeFu column: 'T802_M801_ID'
        t802StaNomorValid column: 'T802_StaNomorValid', sqlType: 'char(1)'
        t802StaTerhubung column: 'T802_StaTerhubung', sqlType: 'char(1)'
        t802StaBalasanSMS column: 'T802_StaBalasanSMS', sqlType: 'char(1)'
        t802StaPotensiRTJ column: 'T802_StaPotensiRTJ', sqlType: 'char(1)'
        t802StaCounterMeasure column: 'T802_StaCounterMeasure', sqlType: 'char(1)'
        t802CatatanCounterMeasure column: 'T802_CatatanCounterMeasure'
    }
}
