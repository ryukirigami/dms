package com.kombos.example

class Contoh {

    String nama
    String alamat
    String kota
    Date tanggalLahir
    int jumlahKendaraan
	String catatanPersetujuan
	String catatanImplementasi
	
	
	static constraints = {
		catatanPersetujuan nullable: true
		catatanImplementasi nullable: true
	}
	
	static mapping = {
	}
}
