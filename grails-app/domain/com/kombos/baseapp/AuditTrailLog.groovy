package com.kombos.baseapp

class AuditTrailLog {
    final static String INSERT = 'INSERT'
    final static String UPDATE = 'UPDATE'
    final static String DELETE = 'DELETE'

    Long logId
    String actionType
    String userName
    String menuCode
    String pkId
    String oldValue
    String newValue
    String createdBy
    Date createdDate
    String createdHost
    String lastEditBy
    Date lastEditDate
    String lastEditHost
    String createdHostName

    static constraints = {
    }

    static mapping = {
        table 'TBLG_AUDITTRAILLOG'
        id name:'logId', generator:'sequence', params:[sequence:'SWS_TBLG_AUDITTRAILLOG_ID_SEQ']
        logId column:'LogId'
        actionType column:'ActionType'
        userName column:'UserName'
        menuCode column:'MenuCode'
        pkId column:'PkId'
        oldValue column:'OldValue', length: 2000
        newValue column:'NewValue', length: 2000
        createdBy column:'CreatedBy'
        createdDate column:'CreatedDate'
        createdHost column:'CreatedHost'
        lastEditBy column:'LastEditBy'
        lastEditDate column:'LastEditDate'
        lastEditHost column:'LastEditHost'
        createdHostName column:'CreatedHostName'
    }
}
