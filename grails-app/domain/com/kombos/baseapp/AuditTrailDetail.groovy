package com.kombos.baseapp

class AuditTrailDetail {
    Long headerId
    String fieldName
    String oldValue
    String newValue
    String createdBy
    Date createdDate
    String createdHost
    String updatedBy
    Date updatedDate
    String updatedHost

    static constraints = {
    }

    static mapping = {
        table 'TBLT_AUDITTRAILDETAIL'
        id generator:'sequence', params:[sequence:'SWS_TBLT_AUDITTRAILD_ID_SEQ']
        headerId column:'HEADERID'
        fieldName column:'FIELDNAME'
        oldValue column:'OLDVALUE'
        newValue column:'NEWVALUE'
        createdBy column:'CREATEDBY'
        createdDate column:'CREATEDDATE'
        createdHost column:'CREATEDHOST'
        updatedBy column: 'UPDATEDBY'
        updatedDate  column: 'UPDATEDDATE'
        updatedHost  column: 'UPDATEDHOST'
    }
}
