package com.kombos.baseapp

class AppSettingParam {
	private static loaded = false
	private static cache = new LinkedHashMap((int) 16, (float) 0.75, (boolean) true)
	private static long maxCacheSize = 8L * 1024L // Cache size in KB (default is 8kb)
	private static long currentCacheSize = 0L
	private static final missingValue = "\b" // an impossible value signifying that no such code exists in the database
	private static long cacheHits = 0L
	private static long cacheMisses = 0L
	
    static TYPE = [
            'NUMERIC'
            ,'ALPHABETIC'
            ,'ALPHANUMERIC'
            ,'BOOLEAN'
    ]

    String code
    String value
    String category
    String approvalStatus
    String label
    String defaultValue
    String listSource
    String description
    String type = TYPE[2]

    static DATE_FORMAT = [
            'dd/MM/yyyy'
            ,'dd/MM/yy'
            ,'dd-MM-yy'
            ,'dd-MM-yyyy'
    ]

    static TIME_FORMAT = [
            'hh:mm:ss a'
            ,'HH:mm:ss'
    ]

    static USER_DISPLAY_FORMAT = [
            'username'
            ,'username, lastLoggedIn'
    ]

    static constraints = {
        code blank: false
        category blank: false
        value nullable: true
        approvalStatus nullable: true
        label blank: false
        defaultValue nullable: true
        listSource nullable: true
        description nullable: true
        type inList: TYPE
    }

    static mapping = {
        table name: 'DOM_APP_SETTING_PARAM'
        id generator:'assigned', name:'code'
    }

    static getListFormat(String format){
        if(format.equalsIgnoreCase("DATE_FORMAT")){
            return this.DATE_FORMAT
        }else if(format.equalsIgnoreCase("TIME_FORMAT")){
            return this.TIME_FORMAT
        }else if(format.equalsIgnoreCase("USER_DISPLAY_FORMAT")){
            return this.USER_DISPLAY_FORMAT
        }
    }
	
	static valueFor(String code) {
		if (!loaded) load()
		if (!code) return null

		def val
		if (maxCacheSize > 0) {
			synchronized(cache) {
				val = cache.get(code)

				if (val) {
					cacheHits++
				} else {
					cacheMisses++
				}
			}
		}
		
		if (!val) {
			val = AppSettingParam.findByCode(code)
			if (val) {
				def type = val.type
				val = val.value?val.value:val.defaultValue
				
				val = AppSettingParam.decodeValue(type, val)
			}

			if (!val) val = missingValue

			if (maxCacheSize > 0) {
				synchronized (cache) {

					// Put it in the cache
					def prev = cache.put(code, val)

					// Another user may have inserted it while we weren't looking
					if (prev != null) currentCacheSize -= code.length() + valSize(prev)

					// Increment the cache size with our data
					currentCacheSize += code.length() + valSize(val)

					// Adjust the cache size if required
					if (currentCacheSize > maxCacheSize) {
						def entries = cache.entrySet().iterator()
						def entry
						while (entries.hasNext() && currentCacheSize > maxCacheSize) {
							entry = entries.next()
							currentCacheSize -= entry.getKey().length() + valSize(entry.getValue())
							entries.remove()
						}
					}
				}
			}
		}

		return (val != missingValue) ? val : null
	}
	
	static valueFor(String code, Object dflt) {
		def val = valueFor(code)

		return (val != null) ? val : dflt
	}

	static resetAll() {
		synchronized(cache) {
			cache.clear()
			currentCacheSize = 0L
			cacheHits = 0L
			cacheMisses = 0L
		}
	}

	static resetThis(String code) {
		synchronized(cache) {
			def val = cache.remove(code)
			if (val) {
				currentCacheSize -= code.length() + valSize(val)
			}
		}
	}
	
	static valSize(val) {
		if (val == null) {
			return 0
		}

		if (val instanceof String) {
			return val.length()
		}

		if (val instanceof Integer) {
			return 4
		}

		if (val instanceof BigDecimal) {
			return 8
		}

		if (val instanceof Date) {
			return 8
		}

		return 8    // A reasonable default
	}

	static statistics() {
		def stats = [:]
		synchronized (cache) {
			stats.max = maxCacheSize
			stats.size = currentCacheSize
			stats.count = cache.size()
			stats.hits = cacheHits
			stats.misses = cacheMisses
		}

		return stats
	}
	
	static load() {		
		def grailsApplication = new AppSettingParam().domainClass.grailsApplication
		
		def size = grailsApplication.config.appSettingParam.cache.size.kb
		
		if (size != null && size instanceof Integer && size >= 0 && size <= 1024 * 1024) {
			maxCacheSize = size * 1024L
		}

		loaded = true
	}
	
	private static decodeValue(String type, val) {
		if (val) {
			switch (type) {
				case "NUMERIC":
				try {
					return new Integer(val)
				} catch (NumberFormatException ne) {}
				break

				case "BOOLEAN":
				try {
					return new Boolean(val)
				} catch (NumberFormatException ne) {}
				break

				default:  // string
				return val
				break
			}
		}
	}
}
