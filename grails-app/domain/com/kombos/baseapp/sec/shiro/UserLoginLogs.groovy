package com.kombos.baseapp.sec.shiro

class UserLoginLogs {
    Long pkid
    Date loginDatetime
    String loginStatusLookupCode
    String username
    String reason

    static STATUS = [
            'FAILED_LOGIN_STATUS'
            ,'SUCCESS_LOGIN_STATUS'
            ,'OUTSIDE_WORKING_HOUR_LOGIN_STATUS'
            ,'OUTSIDE_EFFECTIVE_DATE'
            ,'LOCKOUT_LOGIN_STATUS'
            ,'AUTOMATIC_LOGOUT_STATUS'
			,'NORMAL_LOGOUT_STATUS'
    ]

    static FAILED_LOGIN_STATUS = STATUS[0]
    static SUCCESS_LOGIN_STATUS = STATUS[1]
    static OUTSIDE_WORKING_HOUR_LOGIN_STATUS = STATUS[2]
    static OUTSIDE_EFFECTIVE_DATE = STATUS[3]
    static LOCKOUT_LOGIN_STATUS = STATUS[4]
    static AUTOMATIC_LOGOUT_STATUS = STATUS[5]
	static NORMAL_LOGOUT_STATUS = STATUS[6]

    static constraints = {
        loginDatetime nullable: false
        loginStatusLookupCode nullable: false
        username nullable: false
        reason nullable: true
    }

    static mapping = {
        table name: 'DOM_USER_LOGIN_LOGS'
        id name:'pkid', generator:'sequence', params:[sequence:'SWS_DOM_USER_LOGIN_LOGS_ID_SEQ']
    }
}
