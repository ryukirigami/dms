package com.kombos.baseapp.sec.shiro

class PasswordHistory {

    String passwordHash
    Date insertedDate = new Date()

    static belongsTo = [User]

    static constraints = {
    }

    static mapping = {
//        table name: 'DOM_PASSWORD_HISTORY'
		id generator:'sequence', params:[sequence:'SWS_PASSHIST_ID']
        sort insertedDate : "asc"
    }
}
