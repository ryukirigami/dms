package com.kombos.baseapp.sec.shiro

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Divisi

import javax.persistence.Transient

import com.kombos.baseapp.Approval
import com.kombos.baseapp.ApprovalStatus

class User {

    static STATUS = [
            'ACTIVE'
            ,'INACTIVE'
            ,'EXPIRED'
            ,'LOCKED'
            ,'PASSWORD_EXPIRED'
            ,'PASSWORD_RESET'
            ,'NEW_USER'
            ,'DORMANT'
    ]

    static STATUS_ACTIVE = STATUS[0]
    static STATUS_INACTIVE = STATUS[1]
    static STATUS_EXPIRED = STATUS[2]
    static STATUS_LOCKED = STATUS[3]
    static STATUS_PASSWORD_EXPIRED = STATUS[4]
    static STATUS_PASSWORD_RESET = STATUS[5]
    static STATUS_NEW_USER = STATUS[6]
    static STATUS_DORMANT = STATUS[7]

    String username //t001UserName
    String passwordHash //t001Password

    boolean enabled = false
    String status = STATUS_INACTIVE
    Date lastLoggedIn
    String loggedInFrom
    Date lastUnsuccessfullLoggedIn

    Date createdOn = new Date()
    Date updatedOn
    String passwordUpdateBy
    Date passwordUpdatedOn
    Date passwordExpiredOn
    int wrongPasswordCounter = 0

    Date effectiveStartDate
    Date effectiveEndDate
    String fullname
    boolean accountExpired = false
    boolean accountLocked = false
    boolean canPasswordExpire
    Approval approval
    ApprovalStatus approvalStatus = ApprovalStatus.DRAFT

    CompanyDealer companyDealer
    Divisi divisi
    String t001PUK
    String t001NamaPegawai
    String t001Inisial
    String t001Email
    String t001Foto
    String t001StaLogin
    Date t001LastLogin
    Date t001LastFailed
    String t001MacAddress
    Integer t001CounterFailed
    Date t001TglRegistered
    Date t001ExpiredAccountDate
    String t001ManPowerPengganti
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess
//	static hasMany = [ roles: Role, permissions: String, userAdditionColumns: UserAdditionalColumn, passwordHistory: PasswordHistory, reportTo: User, approvalHistory: Approval ]
    static hasMany = [ roles: Role, permissions: String, passwordHistory: PasswordHistory, reportTo: User, approvalHistory: Approval ]

    static constraints = {
        username nullable: false, blank: false
        passwordHash nullable: false, maxSize: 500
        status nullable: true, inList: STATUS, maxSize: 20
        lastLoggedIn nullable: true
        loggedInFrom nullable: true
        companyDealer nullable: true
        updatedOn nullable: true
        updatedBy nullable: true, maxSize: 50
        passwordHistory nullable: true
        passwordUpdatedOn nullable: true
        passwordUpdateBy nullable: true
        passwordExpiredOn nullable: true
        accountExpired nullable: true
        accountLocked nullable: true
        lastUnsuccessfullLoggedIn nullable: true
        approval nullable: true
        approvalHistory nullable: true
        effectiveStartDate nullable: true
        effectiveEndDate nullable: true,blank : true
        fullname nullable: true
        reportTo nullable: true
        divisi nullable : true
        t001PUK nullable : true, maxSize : 50
        t001NamaPegawai nullable : true, maxSize : 20
        t001Inisial nullable : true, maxSize : 4
        t001Email nullable : true, maxSize : 50
        t001Foto nullable : true
        t001StaLogin nullable : true, maxSize : 1
        t001LastLogin nullable : true
        t001LastFailed nullable : true
        t001MacAddress nullable : true, maxSize : 50
        t001CounterFailed nullable : true, maxSize : 4
        t001TglRegistered nullable : true
        t001ExpiredAccountDate nullable: true,blank : true
        t001ManPowerPengganti nullable : true, maxSize : 100
        staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table name: 'DOM_USER'
        id name:'username', generator:'assigned'
        divisi column : 'T001_M012_ID'
        t001PUK column : 'T001_PUK', sqlType : 'varchar(50)'
        t001NamaPegawai column : 'T001_NamaPegawai', sqlType : 'varchar(20)'
        t001Inisial column : 'T001_Inisial', sqlType : 'varchar(4)'
        t001Email column : 'T001_Email', sqlType :'varchar(50)'
        t001Foto column : 'T001_Foto', sqlType : 'varchar(1000)'
        t001StaLogin column : 'T001_StaLogin', sqlType : 'char(1)'
        t001LastLogin column : 'T001_LastLogin', sqlType : 'date'
        t001LastFailed column : 'T001_LastFailed', sqlType : 'date'
        t001MacAddress column : 'T001_MacAddress', sqltYtpe : 'varchar(50)'
        t001CounterFailed column : 'T001_CounterFailed', sqlType : 'int'
        t001TglRegistered column : 'T001_TglRegistered', sqlType : 'date'
        t001ExpiredAccountDate column : 'T001_ExpiredAccountDate', sqlType : 'date'
        t001ManPowerPengganti column : 'T001_ManPowerPengganti', sqlType : 'varchar(100)'
        staDel column : 'T001_StaDel', sqlType : 'char(1)'
    }

    String toString(){
        return username
    }

    Set<Role> getAuthorities(){
        roles as Set
    }

    @Transient
    String getActivitiUsername(){
        return username
    }
}
