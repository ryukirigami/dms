package com.kombos.baseapp.sec.shiro

class UserAdditionalColumnInfo {

    static TYPE = [
            'CHECKBOX'
            ,'TEXT'
            ,'DATE'
    ]

    String columnName
    String label
    String columnType

    static constraints = {
        columnType inList: TYPE
    }

    static mapping = {
//        id generator:'assigned', name:'columnName'
    }
}
