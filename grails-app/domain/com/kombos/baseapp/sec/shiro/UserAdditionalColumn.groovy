package com.kombos.baseapp.sec.shiro

class UserAdditionalColumn {

    User user
    UserAdditionalColumnInfo userAdditionalColumnInfo
    String value

    static belongsTo = [User, UserAdditionalColumnInfo]

    static constraints = {

    }

    static mapping = {

    }
}
