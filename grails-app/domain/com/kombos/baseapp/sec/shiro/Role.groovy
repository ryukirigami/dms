package com.kombos.baseapp.sec.shiro

import com.kombos.administrasi.CompanyDealer

class Role implements org.activiti.engine.identity.Group
//XXX activiti
//implements org.activiti.engine.identity.Group
{
    String id //t003ID
//    String kodeRole //kode unik
    String name //t003NamaRole
    String authority
    String type

//    CompanyDealer companyDealer
//    String t003ID
//    String t003NamaRole
    Double maxDiscGoodwillPersen
    Double maxDiscGoodwillRp
    Double maxPersenSpDiscRp
    Double maxPersenSpDiscJasa
    Double maxPersenSpDiscParts
    String staLihatReportWorkshopLain
    String staDel
    Date dateCreated //Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [ users: User, aksesMenu: String, permissions: String ]
    static belongsTo = User

    static constraints = {
        name(nullable: false, blank: false)
        authority blank: false, unique: true
        type nullable: true
        //companyDealer nullable: true, blank:true
        maxDiscGoodwillPersen nullable: false, blank:false
        maxDiscGoodwillRp nullable: false, blank:false
        maxPersenSpDiscRp nullable: true, blank:true
        maxPersenSpDiscJasa nullable: false, blank:false
        maxPersenSpDiscParts nullable: false, blank:false
        staLihatReportWorkshopLain nullable: false, blank:false
        staDel nullable: false, blank:false
        createdBy nullable : true, blank:true //Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true, blank:true //Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false //Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table name: 'DOM_SHIROROLE'
        id generator: 'assigned'
        name column: "T003_NamaRole"
        maxDiscGoodwillPersen column: "T003_MaxDiscGdwlPersen"
        maxDiscGoodwillRp column: "T003_MaxDiscGdWlRp"
        maxPersenSpDiscRp column: "T003_MaxPersenSpDiscRp"
        maxPersenSpDiscJasa column: "T003_MaxPersenSpDiscJasa"
        maxPersenSpDiscParts column: "T003_MaxPersenSpDiscParts"
        staLihatReportWorkshopLain column: "T003_StaLhtRprtWrkshpLain", sqlType : "char(1)"
        staDel column: "T003_StaDel", sqlType : "char(1)"
    }

    String toString(){
        return name
    }
}
