package com.kombos.baseapp.sec.shiro

import com.kombos.baseapp.ActionType
import com.kombos.baseapp.menu.Menu;

class Permission {
	Role role
	Menu menu
	String viewString
	String addString
	String editString
	String deleteString
	String linkController
	
	String actionString
	ActionType actionType
	Boolean newPermission
	Boolean checker
	

    static constraints = {
		viewString nullable: true
		addString nullable: true
		editString nullable: true
		deleteString nullable: true
		linkController nullable: true
		menu nullable: true
        role nullable: true,blank : true
		actionString nullable:true
		actionType nullable: true
		newPermission nullable: true
		checker nullable: true
    }
	
	static mapping = {
		table name: 'DOM_SHIROPERMISSIONS'
		id generator:'sequence', params:[sequence:'SWS_PERMISSION_ID'], column: "PKID"
	}
}
