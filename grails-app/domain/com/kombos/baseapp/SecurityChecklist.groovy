package com.kombos.baseapp

class SecurityChecklist {

    static TYPE = [
            'NUMERIC'
            ,'ALPHABETIC'
            ,'ALPHANUMERIC'
            ,'BOOLEAN'
    ]

    String code
    String value
    String category
    String approvalStatus
    String label
    String defaultValue
    String type = TYPE[2]
    boolean enable = false
    String description

    static hasMany = [children:SecurityChecklist]
    static belongsTo = [parent:SecurityChecklist]

    static constraints = {
        code blank: false
        category blank: false
        value nullable: true
        approvalStatus nullable: true
        label blank: false
        defaultValue nullable: true
        type inList: TYPE
        children nullable: true
        parent nullable: true
        description nullable: true
    }

    static mapping = {
        table name: 'DOM_SECURITY_CHECKLIST'
        id generator:'assigned', name:'code'
    }
}
