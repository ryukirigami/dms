package com.kombos.baseapp

class AuditTrailPartialUpload {
	
	Long pkid
	String partialUploadJobId
	String actionType 
	String oldvalue 
	String newvalue
	String createby
	String createdate
	String createdhost

    static constraints = {
    }
	
	static mapping = {
        table 'TBLG_PARTIAL_UPLOAD_DETAIL'
        id name:'pkid', generator:'sequence', params:[sequence:'SWS_TBLG_PRTL_UPL_DET_ID_SEQ'] //SWS_TBLG_PARTIAL_UPLOAD_DETAIL_ID_SEQ
		version(false)
    }
}
