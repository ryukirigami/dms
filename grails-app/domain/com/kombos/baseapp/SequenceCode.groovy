package com.kombos.baseapp

import com.kombos.administrasi.CompanyDealer

class SequenceCode {
    CompanyDealer cabang
    String keyCode
    Integer lastSequence
    Integer sequnceLength
    String sequencepadder
    String sequenceFormat
	String formatDate
    String keterangan

    static hasMany = [nomorUrutHarians : NomorUrutHarian]//, urutBelumTerpakais : UrutBelumTerpakai]

    static mapping = {
        table 'TBL_SEQUENCECODE'
    }

    static constraints = {
        cabang blank:true, nullable:true
        nomorUrutHarians blank:true, nullable:true
        //urutBelumTerpakais blank:true, nullable:true
        keyCode blank:false, nullable:false
        lastSequence blank:false, nullable:false
        sequnceLength blank:false, nullable:false
        sequencepadder blank:false, nullable:false
        sequenceFormat blank:false, nullable:false
        formatDate blank:true, nullable:true
        keterangan blank:false, nullable:false
    }
}
