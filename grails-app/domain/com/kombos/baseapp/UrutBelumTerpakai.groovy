package com.kombos.baseapp

class UrutBelumTerpakai {

    SequenceCode sequenceCode
    String codeBlmTerpakai

    static constraints = {
        sequenceCode blank:false, nullable:false
        codeBlmTerpakai blank:false, nullable:false,unique: true
    }

    static mapping = {
        table 'TBL_URUTBELUMTERPAKAI'
    }
}
