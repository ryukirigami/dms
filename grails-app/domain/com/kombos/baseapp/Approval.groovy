package com.kombos.baseapp

import com.kombos.baseapp.sec.shiro.User
import com.kombos.baseapp.ApprovalStatus

class Approval {
	ApprovalStatus approvalStatus = ApprovalStatus.PENDING
	String module
	String approvalRemark
	Date requestOn
	Date approvedOn
	String requestBy
	String approvedBy
	String params
	
	String refForm
	String refPkid

    static constraints = {
		approvedOn(nullable:true)
		approvedBy(nullable:true)
		approvalRemark(nullable:true)	
		module(nullable:true)	
		refPkid(nullable:true)
		refForm(nullable:true)
    }
	
	static mapping = {
		params type: 'text'
	}
}
