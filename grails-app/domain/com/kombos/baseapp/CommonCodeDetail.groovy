package com.kombos.baseapp

class CommonCodeDetail {
    Long pkid
    String commoncode
    String refcommoncode
    Long refid
    String bicode
    String description
    String parentcode
    String parentbicode
    Long sequence
    Date createddate = new Date()
    String createdhost
    String lasteditby
    Date lasteditdate
    String lastedithost

//    static hasMany = [children:CommonCodeDetail]
//    static belongsTo = [commonCode: CommonCode, parent:CommonCodeDetail]

    static constraints = {
        refcommoncode nullable: true
        refid nullable: true
        bicode nullable: true
        description nullable: true
        parentcode nullable: true
        parentbicode nullable: true
        createddate nullable: true
        createdhost nullable: true
        lasteditby nullable: true
        lasteditdate nullable: true
        lastedithost nullable: true
    }

    static mapping = {
        table name: 'TBLM_COMMONCODED'
//        id name:'pkid'
        id generator:"sequence",params:[sequence:'TBLM_COMMONCODED_SQ'],name: 'pkid'
    }

    String toString(){
        return description
    }
	
	String getBiCodeDescription() {
		if(bicode)
			if(bicode.trim().size() != 0)
				bicode + " - " + description 
			else
				description
		else 
			description
	}
}
