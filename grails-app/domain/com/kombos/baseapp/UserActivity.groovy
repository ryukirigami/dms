package com.kombos.baseapp

import com.kombos.baseapp.sec.shiro.User
import com.kombos.baseapp.menu.Menu;

class UserActivity {
	User user
	Menu menu
	String controller
	String action
	Date accessTime

    static constraints = {
		
    }
	
	static mapping = {
		table name: 'TBLG_USER_LOG'
		id generator:'sequence', params:[sequence:'SWS_USER_LOG_PKID'], column: "PKID"
		version false
	}
}
