package com.kombos.baseapp.menu

import com.kombos.baseapp.sec.shiro.Role

class RoleMenu {
    Role role
    Menu menu
    static constraints = {
        role (nullable: true, blank:true)
        menu (nullable: true, blank:true)
    }
    static mapping = {
        table "DOM_ROLE_MENU"
    }
}
