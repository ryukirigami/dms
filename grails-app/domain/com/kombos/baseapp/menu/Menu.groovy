package com.kombos.baseapp.menu

import com.kombos.baseapp.menu.Menu;

class Menu {
    static _REDIRECT_   = '_REDIRECT_'
    static _NEW_WINDOW_ = '_NEW_WINDOW_'
    static _LOAD_       = '_LOAD_'
    static _ACTION_     = '_ACTION_'
    
    String menuCode
    String label
    String details
    String url
    String linkController
	String iconClass
	boolean divider
    String linkAction
    String targetMode
    int seq = 0
	boolean needApproval

    static hasMany = [children:Menu]
    static belongsTo = [parent:Menu]

    String stockOpnameEnable //0=ya 1=tidak
    static constraints = {
        menuCode(maxSize: 100, unique: true)
        label(maxSize: 100)
        details(maxSize: 200, nullable: true)
        url(maxSize: 255, nullable: true)
        linkController(maxSize: 50, nullable: true)
		iconClass(nullable: true)
		divider(default: false)

        linkAction(maxSize: 50, nullable: true)
        targetMode(inList: ['_REDIRECT_', '_NEW_WINDOW_', '_LOAD_','_ACTION_'], nullable: true)
        parent(nullable: true)
		needApproval(default: false, nullable: true)
        stockOpnameEnable(nullable: true)
    }

    static mapping = {
        //id generator: 'assigned', name: 'menuCode'
        //childs lazy: true
        table 'DOM_MENU'
        children sort: 'seq', order: 'asc'
    }

    String toString(){
        return label
    }
}
