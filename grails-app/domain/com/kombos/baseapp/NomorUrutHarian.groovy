package com.kombos.baseapp

class NomorUrutHarian {
    static belongsTo = [sequenceCode : SequenceCode]
    Date dateUrut
    int tahun
    int bulan
    int hari
    Integer urut
    //Integer sequenceLength

    static mapping = {
        dateUrut sqlType: "date"
        table 'TBL_NOMORURUTHARIAN'
    }

    static constraints = {
        sequenceCode blank:false, nullable:false
        dateUrut blank:false, nullable:false
        urut blank:false, nullable:false
        //sequenceLength blank:false, nullable:false
    }
}
