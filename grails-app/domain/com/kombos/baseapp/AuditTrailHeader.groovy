package com.kombos.baseapp

class AuditTrailHeader {
    Long pkId
    String actionType
    String tableName
    String tablePkIdValue
    String createdBy
    Date createdDate
    String createdHost
    String updatedBy
    Date updatedDate
    String updatedHost

    static constraints = {
    }

    static mapping = {
        table 'TBLT_AUDITTRAILHEADER'
        id name:'pkId', generator:'sequence', params:[sequence:'SWS_TBLT_AUDITTRAILH_ID_SEQ']
        pkId column:'PKID'
        actionType column:'ACTIONTYPE'
        tableName column:'TABLENAME'
        tablePkIdValue column:'TABLEPKIDVALUE'
        createdBy column:'CREATEDBY'
        createdDate column:'CREATEDDATE'
        createdHost column:'CREATEDHOST'
        updatedBy column: 'UPDATEDBY'
        updatedDate  column: 'UPDATEDDATE'
        updatedHost  column: 'UPDATEDHOST'
    }
}
