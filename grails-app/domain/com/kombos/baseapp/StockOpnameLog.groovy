package com.kombos.baseapp

import com.kombos.administrasi.CompanyDealer

class StockOpnameLog {
    CompanyDealer companyDealer
    String staStockOpname //0=ya 1=tidak

    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer blank:false, nullable:false
        staStockOpname blank:false, nullable:false, maxSize:1

        staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    static mapping = {
        table "DOM_STOCKOPNAME_LOG"
    }
}
