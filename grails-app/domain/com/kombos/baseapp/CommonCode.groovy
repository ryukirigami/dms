package com.kombos.baseapp

class CommonCode {
    String commoncode
    String commonname
    String commondesc
    String commonusage
    String codetype
    String parentcode
    String createdby
    Date createddate = new Date()
    String createdhost
    String lasteditby
    Date lasteditdate
    String lastedithost
    Integer issyncronize = 0

//    static hasMany = [commonCodeDetail : CommonCodeDetail]

    static constraints = {
        commoncode blank: false, size: 5..5
        commonname nullable: false
        commondesc nullable: true
        commonusage nullable: true
        codetype nullable: true
        parentcode nullable: true
        createdby nullable: true
        createddate nullable: true
        createdhost nullable: true
        lasteditby nullable: true
        lasteditdate nullable: true
        lastedithost nullable: true
        issyncronize nullable: false
    }

    static mapping = {
        table name: 'TBLM_COMMONCODEH'
        id generator:'assigned', name:'commoncode'
    }
}
