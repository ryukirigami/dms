package com.kombos.finance

import com.kombos.administrasi.CompanyDealer

class TransactionOperasional {
    CompanyDealer companyDealer
    Transaction transaction
    Double ta022Saldo
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
        transaction nullable: true, blank:true
        companyDealer nullable: true, blank:true
        ta022Saldo nullable: true, blank:true
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    static mapping = {
        autoTimestamp false
        table "TA022_TRANSACTION_OPERASIONAL"
        id column: "TA022_ID"//, sqlType: "int"
        transaction column: "TA022_TA021_ID_Transaction"
        ta022Saldo  column: "TA022_Saldo"
        staDel column: "TA022_STADEL", sqlType: "char(1)"

    }
 }
