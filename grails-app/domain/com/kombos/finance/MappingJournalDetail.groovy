package com.kombos.finance

class MappingJournalDetail {

    //Integer id
    Integer nomorUrut
    String accountTransactionType
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [mappingJournal: MappingJournal, accountNumber: AccountNumber]

    static constraints = {
        nomorUrut nullable: false, blank: false
        accountTransactionType nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA006_MAPPINGJOURNALDETAIL"
        id column: "MA006_ID"//, sqlType: "int"
        accountNumber column: "MA006_MA003_IDACCOUNTNUMBER", sqlType: "int"
        mappingJournal column: "MA006_MA005_IDMAPPINGJOURNAL", sqlType: "int"
        nomorUrut column: "MA006_NOMORURUT", sqlType: "int"
        accountTransactionType column: "MA006_ACCOUNTTRANSACTIONTYPE", sqlType: "varchar(16)"
        staDel column: "MA006_STADEL", sqlType: "char(1)"
    }

}
