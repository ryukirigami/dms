package com.kombos.finance

class MappingJournal {

    //Integer id
    String mappingCode
    String mappingName
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [mappingJournalDetail: MappingJournalDetail]

    static constraints = {
        mappingCode nullable: false, blank: false
        mappingName nullable: false, blank: false
        description nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA005_MAPPINGJOURNAL"
        id column: "MA005_ID"//, sqlType: "int"
        mappingCode column: "MA005_MAPPINGCODE" , sqlType: "varchar(32)"
        mappingName column: "MA005_MAPPINGNAME", sqlType: "varchar(32)"
        description column: "MA005_DESCRIPTION", sqlType: "varchar(64)"
        staDel column: "MA005_STADEL", sqlType: "char(1)"
    }

    String toString() {
        return mappingCode
    }
}
