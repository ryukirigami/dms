package com.kombos.finance

class PecahanUang {

    //Integer id
    String pecahanUang
    Integer nilai
    String type
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [cashBalanceDetail: CashBalanceDetail]

    static constraints = {
        pecahanUang nullable: true, blank: true
        nilai nullable: true, blank: true, maxSize: 6
        staDel nullable: false, blank: false
        type inList: ["Kertas", "Logam"], nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA009_PECAHANUANG"
        id column: "MA009_ID"//, sqlType: "int"
        pecahanUang column: "MA009_PECAHANUANG", sqlType: "varchar(32)"
        nilai column: "MA009_NILAI", sqlType: "int"
        staDel column: "MA009_STADEL", sqlType: "char(1)"
        type column: "MA009_TIPE", sqlType: "varchar(16)"
    }

    static transients = ['arrayOfPecahanUangKertas', 'arrayOfPecahanUangLogam']

    Float [] getArrayOfPecahanUangKertas() {
        return new Float()[100000,50000,20000,10000,5000,2000,1000]
    }

    Float [] getArrayOfPecahanUangLogam() {
        return new Float()[1000,500,200,100]
    }

}
