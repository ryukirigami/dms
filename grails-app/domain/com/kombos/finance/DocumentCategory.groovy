package com.kombos.finance

class DocumentCategory {

    //Integer id
    String documentCategory
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [transaction: Transaction]

    static constraints = {
        documentCategory nullable: false, blank: false
        description nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA008_DOCUMENTCATEGORY"
        id column: "MA008_ID"//, sqlType: "int"
        documentCategory column: "MA008_DOCUMENTCATEGORY", sqlType: "varchar(16)"
        description column: "MA008_DESCRIPTION", sqlType: "varchar(64)"
        staDel column: "MA008_STADEL", sqlType: "char(1)"
    }

    String toString() {
        return documentCategory
    }
}
