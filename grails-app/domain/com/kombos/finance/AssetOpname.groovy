package com.kombos.finance

class AssetOpname {
    //Integer id
    Date opnameDate
    String petugasOpname
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [
            asset: Asset,
            assetStatus: AssetStatus
    ]

    static constraints = {
        id maxSize: 4
        asset nullable: true, blank: true, maxSize: 4
        assetStatus nullable: true, blank: true, maxSize: 2
        opnameDate nullable: true, blank: true
        petugasOpname nullable: true, blank: true
        description nullable: true, blank: true
        staDel nullable: true, blank: true
        createdBy nullable: true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        table "TA035_ASSETTOPNAME"
        id column: "TA035_ID"//, sqlType: "int"
        asset column: "TA035_TA034_IDASSET",sqlType: "int"
        opnameDate column: "TA035_OPNAMEDATE"
        petugasOpname column: "TA035_PETUGASOPNAME", sqlType: "varchar(32)"
        description column: "TA035_DESCRIPTION", sqlType: "char(64)"
    }

}
