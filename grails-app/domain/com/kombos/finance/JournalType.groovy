package com.kombos.finance

class JournalType {

    //Integer id
    String journalType
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [journal: Journal]

    static constraints = {
        journalType nullable: false, blank: false
        description nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA004_JOURNALTYPE"
        id column: "MA004_ID"//, sqlType: "int"
        journalType column: "MA004_JOURNALTYPE", sqlType: "varchar(32)"
        description column: "MA004_DESCRIPTION", sqlType: "varchar(64)"
        staDel column: "MA4004_STADEL", sqlType: "char(1)"
    }
}
