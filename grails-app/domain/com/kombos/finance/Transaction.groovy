package com.kombos.finance

import com.kombos.administrasi.CompanyDealer

class Transaction {

    //Integer id
    CompanyDealer companyDealer
    String transactionCode
    Date transactionDate
    String description
    BigDecimal amount
    String docNumber
    String inOutType
    String receiptNumber
    Journal journal
    Date transferDate
    Date dueDate
    String cekNumber
    String pdcStatus
    Date pdcTransferDate
    String isOperasional
    String isSetoranBank
    String staDel
    String staOperasional // 1= ya, 0= tidak, default = 0
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [
            documentCategory: DocumentCategory,
            transactionType: TransactionType,
            bankAccountNumber: BankAccountNumber,
            journal: Journal
    ]

    static constraints = {
        isOperasional nullable: true
        isSetoranBank nullable: true
        transactionCode nullable: false, blank: false
        transactionDate nullable: false, blank: false
        documentCategory nullable: true, blank: true
        bankAccountNumber nullable: true, blank: true
        description nullable: true, blank: true
        amount nullable: true, blank: true, maxSize: 9
        journal nullable: true, blank: true, maxSize: 4
        docNumber nullable: true, blank: true
        inOutType nullable: true, blank: true
        receiptNumber nullable: true, blank: true
        transferDate nullable: true, blank: true
        dueDate nullable: true, blank: true
        cekNumber nullable: true, blank: true
        pdcStatus nullable: true, blank: true
        pdcTransferDate nullable: true, blank: true
        staDel nullable: false, blank: false
        companyDealer nullable: true, blank: true
        staOperasional nullable: true, blank: true
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA021_TRANSACTION"
        id column: "TA021_ID"//, sqlType: "int"
        transactionCode column: "TA021_TRANSACTIONCODE", sqlType: "varchar(21)"
        transactionDate column: "TA021_TRANSACTIONDATE"
        description column: "TA021_DESCRIPTION", sqlType: "varchar(256)"
        amount column: "TA021_AMOUNT", sqlType: "float"
        journal column: "TA021_TA024_IDJOURNAL", sqlType: "int"
        documentCategory column: "TA021_MA008_DOCCATEGORY", sqlType: "int"
        docNumber column: "TA021_DOCNUMBER", sqlType: "varchar(32)"
        inOutType column: "TA021_INOUTTYPE", sqlType: "varchar(8)"
        transactionType column: "TA021_MA012_IDTRANSACTIONTYPE", sqlType: "int"
        receiptNumber column: "TA021_RECEIPTNUMBER", sqlType: "varchar(32)"
        transferDate column: "TA021_TRANSFERDATE"
        bankAccountNumber column: "TA012_MA007_IDBANKACCOUNTNO", sqlType: "int"
        dueDate column: "TA021_DUEDATE"
        cekNumber column: "TA021_CEKNUMBER", sqlType: "varchar(16)"
        pdcStatus column: "TA021_PDCSTATUS", sqlType: "varchar(8)"
        pdcTransferDate column: "TA021_PDCTRANSFERDATE"
        staDel column: "TA021_STADEL", sqlType: "char(1)"
        staOperasional column: "TA021_ISOPERASIONAL", sqlType: "char(1)"
    }

    String toString() {
        return transactionCode
    }

}
