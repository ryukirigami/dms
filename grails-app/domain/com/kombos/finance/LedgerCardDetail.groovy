package com.kombos.finance

class LedgerCardDetail {

    //Integer id
    String journalTransactionType
    float debitAmount
    float creditAmount
    String subLedger
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [
            journal: Journal,
            accountNumber: AccountNumber,
            subType: SubType
    ]

    static constraints = {
        id maxSize: 5
        journal nullable: false, blank: false, maxSize: 5
        accountNumber nullable: false, blank: false, maxSize: 3
        journalTransactionType nullable: false, blank: false
        debitAmount nullable: false, blank: false, maxSize: 9
        creditAmount nullable: false, blank: false, maxSize: 9
        subLedger nullable: false, blank: false
        subType nullable: false, blank: false, maxSize: 2
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA042_LEDGERCARDDETAIL"
        id column: "TA042_ID"//, sqlType: "int"
        journal column: "TA042_TA041_IDJOURNAL", sqlType: "int"
        accountNumber column: "TA042_MA003_IDACCOUNTNUMBER", sqlType: "int"
        journalTransactionType column: "TA042_JOURNALTRANSACTIONTYPE", sqlType: "varchar(8)"
        debitAmount column: "TA042_DEBITAMOUNT", sqlType: "float"
        creditAmount column: "TA042_CREDITAMOUNT", sqlType: "float"
        subLedger column: "TA042_SUBLEDGER", sqlType: "varchar(32)"
        staDel column: "TA042_STADEL", sqlType: "char(1)"
    }
}
