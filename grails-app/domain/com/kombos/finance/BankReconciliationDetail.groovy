package com.kombos.finance

class BankReconciliationDetail {

    //Integer id
    float debitAmount
    float creditAmount
    String reconTransactionType
    String staDel
    String description
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [
            bankRecon: BankReconciliation,
            accountNumber: AccountNumber
    ]

    static constraints = {
        id maxSize: 5
        bankRecon nullable: false, blank: false, maxSize: 5
        accountNumber nullable: true, blank: true, maxSize: 5
        debitAmount nullable: false, blank: false, maxSize: 10
        creditAmount nullable: false, blank: false, maxSize: 10
        description nullable: true, blank: true
        reconTransactionType nullable: true, blank: true
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        table "TA033_BANKRECONCILIATIONDETAIL"
        id column: "TA033_ID"//, sqlType: "int"
        bankRecon column: "TA033_TA032_IDBANKRECON", sqlType: "int"
        accountNumber column: "TA033_MA003_IDACCOUNTNUMBER", sqlType: "int"
        debitAmount column: "TA033_DEBITAMOUNT", sqlType: "float"
        creditAmount column: "TA033_CREDITAMOUNT", sqlType: "float"
        reconTransactionType column: "TA033_RECONTRANSACTIONTYPE", sqlType: "varchar(8)"
        staDel column: "TA033_STADEL", sqlType: "char(1)"
    }
}
