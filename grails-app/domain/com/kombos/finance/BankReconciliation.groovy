package com.kombos.finance

import com.kombos.administrasi.Bank
import com.kombos.administrasi.CompanyDealer

class BankReconciliation {

    //Integer id
    CompanyDealer companyDealer
    Date reconciliationDate
    float saldoAwalBank
    float saldoAkhirBank
    String perusahaan
    float saldoAwalPerusahaan
    float saldoAkhirPerusahaan
    float selisih
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [bankReconciliationDetail: BankReconciliationDetail]

    static belongsTo = [bank: Bank]

    static constraints = {
        id maxSize: 5
        companyDealer nullable: true
        bank nullable: false, blank: false, maxSize: 3
        reconciliationDate nullable: false, blank: false
        saldoAwalBank nullable: false, blank: false
        saldoAkhirBank nullable: false, blank: false
        perusahaan nullable: true, blank: true
        saldoAwalPerusahaan nullable: false, blank: false
        saldoAkhirPerusahaan nullable: false, blank: false
        selisih nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA032_BANKRECONCILIATION"
        id column: "TA032_ID"//, sqlType: "int"
        bank column: "TA032_M072_IDBANK", sqlType: "int"
        reconciliationDate column: "TA032_RECONCILIATIONDATE", sqlType: "date"
        saldoAwalBank column: "TA032_SALDOAWALBANK", sqlType: "float"
        saldoAkhirBank column: "TA032_SALDOAKHIRBANK", sqlType: "float"
        perusahaan column: "TA032_PERUSAHAAN", sqlType: "varchar(32)"
        saldoAwalPerusahaan column: "TA032_SALDOAWALPERUSAHAAN", sqlType: "float"
        saldoAkhirPerusahaan column: "TA032_SALDOAKHIRPERUSAHAAN", sqlType: "float"
        selisih column: "TA032_SELISIH", sqlType: "float"
        staDel column: "TA032_STADEL", sqlType: "char(1)"
        companyDealer column: "COMPANY_ID"
    }

}