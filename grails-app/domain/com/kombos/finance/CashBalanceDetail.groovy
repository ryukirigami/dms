package com.kombos.finance

class CashBalanceDetail {

    //Integer id
    Integer quantity
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [
            cashBalance: CashBalance,
            pecahanUang: PecahanUang
    ]

    static constraints = {
        cashBalance nullable: false, blank: false, maxSize: 4
        pecahanUang nullable: false, blank: false, maxSize: 4
        quantity nullable: false, blank: false, maxSize: 4
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA031_CASHBALANCEDETAIL"
        id column: "TA031_ID"//, sqlType: "int"
        cashBalance column: "TA031_TA030_IDCASHBALANCE", sqlType: "int"
        pecahanUang column: "TA031_MA009_IDPECAHANUANG", sqlType: "int"
        quantity column: "TA031_QUANTITY", sqlType: "int"
        staDel column: "TA031_STADEL", sqlType: "char(1)"
    }
}
