package com.kombos.finance

import com.kombos.parts.Vendor

class JournalDetail {

    //Integer id
    String journalTransactionType
    BigDecimal debitAmount
    BigDecimal creditAmount
    String subLedger
    AccountNumber accountNumber
    String vendor
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [journal: Journal, accountNumber: AccountNumber, subType: SubType]

    static constraints = {
        journal nullable: false, blank: false, maxSize: 5
        accountNumber nullable: false, blank: false, maxSize: 5
        subType nullable: true, blank: true
        vendor nullable: true, blank: true
        description nullable: true, blank: true
        debitAmount nullable: true, blank: true, maxSize: 9
        creditAmount nullable: true, blank: true, maxSize: 9
        journalTransactionType nullable: true, blank: true
        subLedger nullable: true, blank: true
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA025_JOURNALDETAIL"
        id column: "TA025_ID"//, sqlType: "int"
        journal column: "TA025_TA024_IDJOURNAL", sqlType: "int"
        accountNumber column: "TA025_MA003_IDACCOUNTNUMBER", sqlType: "int"
        journalTransactionType column: "TA025_JOURNALTRANSACTIONTYPE", sqlType: "varchar(8)"
        debitAmount column: "TA025_DEBITAMOUNT", sqlType: "float"
        creditAmount column: "TA025_CREDITAMOUNT", sqlType: "float"
        subType column: "TA025_MA011_IDSUBTYPE", sqlType: "int"
        subLedger column: "TA025_SUBLEDGER", sqlType: "varchar(32)"
        staDel column: "TA025_STADEL", sqlType: "char(1)"
        description column: "TA025_DESCRIPTION", sqlType: "varchar(1000)"
    }
}
