package com.kombos.finance

import com.kombos.administrasi.CompanyDealer

class AccountNumber {

    //Integer id
    CompanyDealer companyDealer
    String accountNumber
    String accountName
    String parentAccount
    String accountMutationType
    String tipeNeraca
    String tipeLabaRugi
    String tipeArusKas
    Integer level
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [
            mappingJournalDetail: MappingJournalDetail,
            journalDetail: JournalDetail,
            bankReconciliationDetail: BankReconciliationDetail,
            monthlyBalance: MonthlyBalance,
            ledgerCardDetail: LedgerCardDetail
    ]

    static belongsTo = [accountType: AccountType]

    static constraints = {
        companyDealer nullable: true, blank: true
        tipeNeraca nullable: true, blank: true
        tipeLabaRugi nullable: true, blank: true
        tipeArusKas nullable: true, blank: true
        accountName nullable: false, blank: false
        accountNumber nullable: false, blank: false
        accountMutationType nullable: false, blank: false, inList: ["MUTASI", "TDK MUTASI"]
        parentAccount nullable: true, blank: true
        level nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA003_ACCOUNTNUMBER"
        id column: "MA003_ID"//, sqlType: "int", generator: "increment"
        companyDealer column: "MA003_COMPANYDEALER_ID"
        tipeArusKas column: "MA003_TIPEARUSKAS", sqlType: "varchar(16)"
        tipeNeraca column: "MA003_TIPENERACA", sqlType: "varchar(16)"
        tipeArusKas column: "MA003_TIPEARUSKAS", sqlType: "varchar(16)"
        accountNumber column: "MA003_ACCOUNTNUMBER", sqlType: "varchar(16)"
        parentAccount column: "MA003_PARENTACCOUNT", sqlType: "varchar(32)"
        accountName column: "MA003_ACCOUNTNAME", sqlType: "varchar(64)"
        accountType column: "MA003_MA001_IDACCOUNTTYPE", sqlType: "int"
        accountMutationType column: "MA003_ACCOUNTMUTATIONTYPE", sqlType: "varchar(16)"
        level column: "MA003_LEVEL", sqlType: "int"
        staDel column: "MA003_STADEL", sqlType: "char(1)"
    }

    def static String[] accountMutationType() {
       return ["MUTASI", "TDK MUTASI"]
    }

    String toString() {
        return accountNumber
    }

}
