package com.kombos.finance

class TransactionType {

    //Integer id
    String transactionType
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [transaction: Transaction]

    static constraints = {
        transactionType nullable: false, blank: false
        description nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA012_TRANSACTIONTYPE"
        id column: "MA012_ID"//, sqlType: "int"
        transactionType column: "MA012_TRANSACTIONTYPE", sqlType: "varchar(16)"
        description column: "MA012_DESCRIPTION", sqlType: "varchar(64)"
        staDel column: "MA012_STADEL", sqlType: "char(1)"
    }

    String toString() {
        return transactionType
    }
}
