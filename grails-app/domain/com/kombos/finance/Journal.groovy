package com.kombos.finance

import com.kombos.administrasi.CompanyDealer

class Journal {

    //Integer id
    CompanyDealer companyDealer
    String journalCode
    Date journalDate
    String description
    JournalType journalType
    BigDecimal totalDebit
    BigDecimal totalCredit
    String docNumber
    String isApproval  // 1  = APPROVE, 2 = REJECTED
    String isOperasional //1  = IYA, INSERT KE TRANSACTION
    String isSetoranBank //1 = IYA
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [journalType: JournalType]

    static hasMany = [journalDetail: JournalDetail]

    static constraints = {
        journalCode nullable: false, blank: false
        journalDate nullable: true, blank: true
        description nullable: false, blank: false
        totalDebit nullable: true, blank: true, maxSize: 20
        totalCredit nullable: true, blank: true, maxSize: 20
        docNumber nullable: true, blank: true
        isApproval nullable: true, blank: true
        isOperasional nullable: true, blank: true
        isSetoranBank nullable: true
        staDel nullable: false, blank: false
        journalType nullable: true, blank: true, maxSize: 3
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TR024_JOURNAL"
        id column: "TR024_ID"//, sqlType: "int"
        journalCode column: "TR024_JOURNALCODE", sqlType: "varchar(64)"
        journalDate column: "TR024_JOURNALDATE"
        description column: "TR024_DESCRIPTION", sqlType: "varchar(1000)"
        totalDebit column: "TR024_TOTALDEBIT", sqlType: "float"
        totalCredit column: "TR024_TOTALCREDIT", sqlType: "float"
        docNumber column: "TR024_DOCNUMBER", sqlType: "varchar(32)"
        journalType column: "TR024_MA004_IDJOURNALTYPE", sqlType: "int"
        isApproval column: "TR024_ISAPPROVAL", sqlType: "varchar(1)"
        staDel column: "TR024_STADEL", sqlType: "char(1)"
    }

    String toString() {
        return journalCode
    }
}
