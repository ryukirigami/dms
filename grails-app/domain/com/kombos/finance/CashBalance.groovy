package com.kombos.finance

import com.kombos.administrasi.CompanyDealer

class CashBalance {

    //Integer id
    CompanyDealer companyDealer
    Date requiredDate
    float cashBalance
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [cashBalanceDetail: CashBalanceDetail]

    static constraints = {
        id maxSize: 4
        requiredDate nullable: false, blank: false
        cashBalance nullable: false, blank: false, maxSize: 17
        companyDealer nullable: true, blank: true
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA030_CASHBALANCE"
        id column: "TA030_ID"//, sqlType: "int"
        requiredDate column: "TA030_REQUIREDDATE"
        cashBalance column: "TA030_CASHBALANCE", sqlType: "float"
        staDel column: "TA030_STADEL", sqlType: "char(1)"
    }
}
