package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.GoodsReceive

class Collection167 {

    CompanyDealer companyDealer
    Date collectionDate
    float amount
    Date dueDate
    Date paymentDate
    float paidAmount
    String paymentStatus
    String alasanTdkBayar
    String subLedger
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [
            goodsReceive: GoodsReceive,
            subType: SubType
    ]

    static constraints = {
        companyDealer nullable: true, blank: true
        collectionDate nullable: false, blank: false
        amount nullable: true, blank: true, maxSize: 9
        dueDate nullable: true, blank: true
        paymentDate nullable: true, blank: true
        paidAmount nullable: true, blank: true, maxSize: 9
        paymentStatus nullable: true, blank: true
        alasanTdkBayar nullable: true, blank: true
        subType nullable: true, blank: true
        subLedger nullable: true, blank: true
        staDel nullable: true, blank: true
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA026_COLLECTION_167"
        id column: "TA026_ID"//, sqlType: "int"
        goodsReceive column: "TA026_T167_GOODSRECEIVE"//, sqlType: "varchar(20)"
        collectionDate column: "TA026_COLLECTIONDATE"
        companyDealer column: "COMPANY_ID"
        amount column: "TA026_AMOUNT", sqlType: "float"
        dueDate column: "TA026_DUEDATE"
        paymentDate column: "TA026_PAYMENTDATE"
        paymentStatus column: "TA026_PAYMENTSTATUS", sqlType: "varchar(5)"
        alasanTdkBayar column: "TA026_ALASANTDKBAYAR", sqlType: "varchar(64)"
        subType column: "TA026_MA011_SUBTYPE", sqlType: "int"
        subLedger column: "TA026_SUBLEDGER", sqlType: "varchar(32)"
        staDel column: "TA026_STADEL", sqlType: "char(1)"
    }
}
