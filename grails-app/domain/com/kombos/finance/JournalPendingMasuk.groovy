package com.kombos.finance

import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Goods
import com.kombos.reception.Reception

/**
 * Created by Asief Maniaxx on 2/24/15.
 */
class JournalPendingMasuk {
    Reception reception
    InvoiceT701 invoiceT701
    Goods goods
    Double qty
    Double price
    String periode // diisi bulan & tahun ex. 022015
    String staDel

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        reception nullable: true, blank:true
        invoiceT701 nullable: true, blank:true
        goods nullable: true, blank:true
        qty nullable: true, blank:true
        price nullable: true, blank:true
        periode nullable: true, blank:true

        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA042_JURNALPENDINGMASUK"
        id column: "TA042_ID"//, sqlType: "int"
        reception column: "TA042_WO_ID"
        goods  column: "TA042_PART_ID"
        invoiceT701  column: "TA042_SI_ID"
        qty  column: "TA042_QTY"
        price  column: "TA042_PRICE"
        periode  column: "TA042_PERIODE"
        staDel column: "TA042_STADEL", sqlType: "char(1)"

    }
}
