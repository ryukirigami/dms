package com.kombos.finance

class LedgerCard {

    //Integer id
    Date journalDate
    String description
    float totalDebit
    float totalCredit
    String docNumber
    String journalType
    String isApproval
    String staDel

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        id maxSize: 5
        journalDate nullable: false, blank: false
        description nullable: false, blank: false
        totalDebit nullable: false, blank: false, maxSize: 9
        totalCredit nullable: false, blank: false, maxSize: 9
        docNumber nullable: false, blank: false
        journalType nullable: false, blank: false
        isApproval nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA041_LEDGERCARD"
        id column: "TA041_ID"//, sqlType: "int"
        journalDate column: "TA041_JOURNALDATE"
        description column: "TA041_DESCRIPTION", sqlType: "varchar(64)"
        totalDebit column: "TA041_TOTALDEBIT", sqlType: "float"
        totalCredit column: "TA041_TOTALCREDIT", sqlType: "float"
        docNumber column: "TA041_DOCNUMBER", sqlType: "varchar(32)"
        journalType column: "TA041_JOURNALTYPE", sqlType: "varchar(8)"
        isApproval column: "TA041_ISAPPROVAL", sqlType: "char(1)"
        staDel column: "TA041_STADEL", sqlType: "char(1)"
    }
}
