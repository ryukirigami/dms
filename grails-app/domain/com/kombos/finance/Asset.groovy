package com.kombos.finance

import com.kombos.administrasi.CompanyDealer

class Asset {

    //Integer id
    CompanyDealer companyDealer
    String assetName
    String assetCode
    Integer quantity
    String purchaseDate
    Float price
    String location
    String user
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [assetOpname: AssetOpname]

    static belongsTo = [
            assetType: AssetType,
            assetStatus: AssetStatus,
    ]

    static constraints = {
        id maxSize: 4
        assetName nullable: true, blank: true
        assetCode nullable: true, blank: true
        assetStatus nullable: true, blank: true
        quantity nullable: true, blank: true, maxSize: 4
        purchaseDate nullable: true, blank: true
        price nullable: true, blank: true
        location nullable: true, blank: true
        user nullable: true, blank: true
        description nullable: true, blank: true
        staDel nullable: true, blank: true
        createdBy nullable: true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA034_ASSET"
        id column: "TA034_ID"//, sqlType: "int"
        assetName column: "TA034_ASSETNAME", sqlType: "varchar(256)"
        assetType column: "TA034_MA002_IDASSETTYPE", sqlType: "int"
        assetCode column: "TA034_ASSETCODE", sqlType: "varchar(32)"
        quantity column: "TA034_QUANTITY", sqlType: "int"
        purchaseDate column: "TA034_PURCHASEDATE"
        price column: "TA034_PRICE", sqlType: "float"
        location column: "TA034_LOCATION", sqlType: "varchar(32)"
        user column: "TA034_USER", sqlType: "varchar(32)"
        assetStatus column: "TA034_MA010_IDASSETSTATUS", sqlType: "int"
        description column: "TA034_DESCRIPTION", sqlType: "varchar(64)"
        staDel column: "TA034_STADEL", sqlType: "char(1)"
        companyDealer column: "ID_COMPANY", sqlType: "int"
    }
}
