package com.kombos.finance

class AccountType {

    //Integer id
    String accountType
    String saldoNormal
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [accountNumber: AccountNumber]

    static constraints = {
        accountType nullable: false, blank: false
        saldoNormal nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA001_ACCOUNTTYPE"
        id column: "MA001_ID"//, sqlType: "int"
        accountType column: "MA001_AccountType", sqlType: "varchar(16)"
        saldoNormal column: "MA001_SaldoNormal", sqlType: "varchar(16)"
        staDel column: "MA001_StaDel", sqlType: "char(1)"
    }

    String toString() {
        return accountType;
    }
}
