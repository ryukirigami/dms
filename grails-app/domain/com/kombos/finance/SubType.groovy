package com.kombos.finance

class SubType {

    //Integer id
    String subType
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [
            journalDetail: JournalDetail,
            collection: Collection,
            monthlyBalance: MonthlyBalance,
            ledgerCardDetail: LedgerCardDetail
    ]

    static constraints = {
        subType nullable: false, blank: false
        description nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA011_SUBTYPE"
        id column: "MA011_ID"//, sqlType: "int"
        subType column: "MA011_SUBTYPE", sqlType: "varchar(16)"
        description column: "MA011_DESCRIPTION", sqlType: "varchar(64)"
        staDel column: "MA011_STADEL", sqlType: "char(1)"
    }
}
