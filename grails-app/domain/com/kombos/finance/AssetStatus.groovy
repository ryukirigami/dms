package com.kombos.finance

class AssetStatus {

    //Integer id
    String assetStatus
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [
            asset: Asset,
            assetOpname: AssetOpname
    ]

    static constraints = {
        assetStatus nullable: false, blank: false
        description nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA010_ASSETSTATUS"
        id column: "MA010_ID"//, sqlType: "int"
        assetStatus column: "MA010_ASSETSTATUS", sqlType: "varchar(16)"
        description column: "MA010_DESCRIPTION", sqlType: "varchar(32)"
        staDel column: "MA010_STADEL", sqlType: "char(1)"
    }

    String toString() {
        return assetStatus
    }
}
