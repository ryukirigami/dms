package com.kombos.finance

import com.kombos.administrasi.Bank
import com.kombos.administrasi.CompanyDealer

class BankAccountNumber {

    //Integer id
    CompanyDealer companyDealer
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [transaction: Transaction]

    static belongsTo = [bank: Bank, accountNumber: AccountNumber]

    static constraints = {
        companyDealer nullable: true, blank: true
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA007_BANKACCOUNTNUMBER"
        id column: "MA007_ID"//, sqlType: "int"
        bank column: "MA007_M072_IDBANK"//, sqlType: "varchar(4)"
        accountNumber column: "MA007_MA003_IDACCOUNTNUMBER", sqlType: "int"
        staDel column: "MA007_STADEL", sqlType: "char(1)"
    }

    String toString() {
        return "${bank.m702NamaBank} - ${accountNumber.accountNumber}"
    }
}
