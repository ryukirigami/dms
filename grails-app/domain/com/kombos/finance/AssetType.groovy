package com.kombos.finance

class AssetType {

    //Integer id
    String typeAsset
    String description
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [asset: Asset]

    static constraints = {
        typeAsset nullable: false, blank: false
        description nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MA002_ASSETTYPE"
        id columns: "MA002_ID"//, sqlType: "int"
        typeAsset column: "MA002_TypeAsset", sqlType: "varchar(16)"
        description column: "MA002_Description", sqlType: "varchar(64)"
        staDel column: "MA002_StaDel", sqlType: "char(1)"
    }

    String toString() {
        return typeAsset
    }

}
