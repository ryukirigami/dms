package com.kombos.finance

import com.kombos.administrasi.CompanyDealer

class MonthlyBalance {

    //Integer id
    CompanyDealer companyDealer
    String yearMonth
    String awalTahun
    BigDecimal startingBalance
    BigDecimal debitMutation
    BigDecimal creditMutation
    BigDecimal endingBalance
    BigDecimal realmutation
    String subLedger
    Date startingDate
    Date endingDate
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static belongsTo = [
            accountNumber: AccountNumber,
            subType: SubType
    ]

    static constraints = {
        companyDealer nullable: true, blank : true
        accountNumber nullable: false, blank: false
        yearMonth nullable: false, blank: false
        awalTahun nullable: true, blank: true
        startingBalance nullable: false, blank: false, maxSize: 20
        debitMutation nullable: false, blank: false, maxSize: 20
        creditMutation nullable: false, blank: false, maxSize: 20
        endingBalance nullable: false, blank: false, maxSize: 20
        realmutation nullable: true, blank: true, maxSize: 20
        subType nullable: true, blank: true
        subLedger nullable: true, blank: true
        startingDate nullable: false, blank: false
        endingDate nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TA040_MONTHLYBALANCE"
        id column: "TA040_ID"//, sqlType: "int"
        accountNumber column: "TA040_MA003"
        yearMonth column: "TA040_YEARMONTH", sqlType: "varchar(8)"
        awalTahun column: "TA040_AWALTAHUN", sqlType: "varchar(4)"
        debitMutation column: "TA040_DEBITMUTATION"
        creditMutation column: "TA040_CREDITMUTATION"
        endingBalance column: "TA040_ENDINGBALANCE"
        realmutation column: "TA040_REALMUTATION"
        subType column: "TA040_MA011_IDSUBTYPE"
        subLedger column: "TA040_SUBLEDGER", sqlType: "varchar(32)"
    }
}
