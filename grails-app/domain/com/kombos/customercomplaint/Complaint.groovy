package com.kombos.customercomplaint

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.Customer

class Complaint {
    CompanyDealer companyDealer
    CustomerVehicle customerVehicle
    String t921ID
    Date t921TglComplain
    HistoryCustomerVehicle historyCustomerVehicle
    Customer customer
    HistoryCustomer historyCustomer
    Integer t921Km
    String t921StaPakaiKendaraan
    String t921Peran
    String t921NamaPelanggan
    String t921TelpRumah
    String t921TelpHp
    String t921Fax
    String t921Email
    String t921Alamat
    String t921ProfilPelanggan
    String t921NoReff
    Date t921TglKasusDiterima
    Date t921TglLPKDikirim
    String t921StaMediaKeluhan
    String t921AreaKeluhan
    String t921DealerPembelian
    String t921DealerKomplain
    String t921DealerDiKomplain
    String t921StaKategoriKeluhan
    String t921Keluhan
    String t921Permintaan
    Date t921LatarBelakangTgl
    String t921LatarBelakangPihakTerlibat
    String t921LatarBelakangFakta
    String t921SebabPelanggan
    String t921SebabKendaraan
    String t921SebabDealer
    String t921StaPenanganan
    String t921StrategiPenyelesaian
    Date t921TindakanTgl
    String t921TindakanPihakTerlibat
    String t921TindakanFakta
    String t921StaSolusi
    String t921StaKepuasanPelanggan
    String t921AlasanKepuasan
    Date t921TglPenyelesaianKeluhan
    String t921StaLangkahPencegahan
    String t921TextLangkahPencegahan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    String staDel

    static constraints = {
        companyDealer (blank:true, nullable:true)
        t921ID nullable: true, blank: true
        t921NoReff nullable: false, blank: false
        t921StaMediaKeluhan nullable: false, blank: false
        customerVehicle nullable: true, blank: true
        t921DealerDiKomplain nullable: false, blank: false
        historyCustomerVehicle nullable: false, blank: false
        t921TglComplain nullable: true, blank: true
        t921StaKategoriKeluhan nullable: false, blank: false
        t921AreaKeluhan nullable: false, blank: false
        t921NamaPelanggan nullable: false, blank: false
        customer nullable: true, blank: true
        historyCustomer nullable: true, blank: true
        t921Km nullable: true, blank: true
        t921StaPakaiKendaraan nullable: true, blank: true
        t921Peran nullable: true, blank: false
        t921TelpRumah nullable: false, blank: false
        t921TelpHp nullable: false, blank: false
        t921Fax nullable: false, blank: false
        t921Email nullable: false, blank: false
        t921Alamat nullable: false, blank: false
        t921ProfilPelanggan nullable: true, blank: true
        t921TglKasusDiterima nullable: false, blank: false
        t921TglLPKDikirim nullable: false, blank: false
        t921DealerPembelian nullable: false, blank: false
        t921DealerKomplain nullable: false, blank: false
        t921Keluhan nullable: false, blank: false
        t921Permintaan nullable: true, blank: true
        t921LatarBelakangTgl nullable: false, blank: false
        t921LatarBelakangPihakTerlibat nullable: true, blank: true
        t921LatarBelakangFakta nullable: true, blank: true
        t921SebabPelanggan nullable: true, blank: true
        t921SebabKendaraan nullable: true, blank: true
        t921SebabDealer nullable: true, blank: true
        t921StaPenanganan nullable: false, blank: false
        t921StrategiPenyelesaian nullable: true, blank: true
        t921TindakanTgl nullable: true, blank: true
        t921TindakanPihakTerlibat nullable: true, blank: true
        t921TindakanFakta nullable: true, blank: true
        t921StaSolusi nullable: false, blank: false
        t921StaKepuasanPelanggan nullable: true, blank: true
        t921AlasanKepuasan nullable: true, blank: true
        t921TglPenyelesaianKeluhan nullable: true, blank: true
        t921StaLangkahPencegahan nullable: true, blank: true
        t921TextLangkahPencegahan nullable: true, blank: true
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
        staDel maxSize : 1, nullable : false, blank : false
    }

    static mapping = {
        autoTimestamp false
        table "T921_COMPLAINT"
        customerVehicle column: 'T921_T103_VinCode'
        t921ID column: 'T921_ID', sqlType: 'int'
        t921TglComplain column: 'T921_TglComplain'//, sqlType: 'date'
        historyCustomerVehicle column: 'T921_T183_ID'
        customer  column: 'T921_T102_ID'
        historyCustomer column: 'T921_T182_ID'
        t921Km column: 'T921_Km', sqlType: 'int'
        t921StaPakaiKendaraan column: 'T921_StaPakaiKendaraan', sqlType: 'char(1)'
        t921Peran column: 'T921_Peran', sqlType: 'varchar(50)'
        t921NamaPelanggan column: 'T921_NamaPelanggan', sqlType: 'varchar(50)'
        t921TelpRumah column: 'T921_TelpRumah', sqlType: 'varchar(50)'
        t921TelpHp column: 'T921_TelpHp', sqlType: 'varchar(50)'
        t921Fax column: 'T921_Fax', sqlType: 'varchar(50)'
        t921Email column: 'T921_Email', sqlType: 'varchar(50)'
        t921Alamat column: 'T921_Alamat'//, sqlType: 'text'
        t921ProfilPelanggan column: 'T921_ProfilPelanggan'//, sqlType: 'text'
        t921NoReff column: 'T921_NoReff', sqlType: 'varchar(50)'
        t921TglKasusDiterima column: 'T921_TglKasusDiterima'//, sqlType: 'date'
        t921TglLPKDikirim column: 'T921_TglLPKDikirim'//, sqlType: 'date'
        t921StaMediaKeluhan column: 'T921_StaMediaKeluhan', sqlType: 'char(1)'
        t921AreaKeluhan column: 'T921_AreaKeluhan', sqlType: 'char(1)'
        t921DealerPembelian column: 'T921_DealerPembelian', sqlType: 'varchar(50)'
        t921DealerKomplain column: 'T921_DealerKomplain', sqlType: 'varchar(50)'
        t921DealerDiKomplain column: 'T921_DealerDiKomplain', sqlType: 'varchar(50)'
        t921StaKategoriKeluhan column: 'T921_StaKategoriKeluhan', sqlType: 'char(1)'
        t921Keluhan column: 'T921_Keluhan'//, sqlType: 'text'
        t921Permintaan column: 'T921_Permintaan'//, sqlType: 'text'
        t921LatarBelakangTgl column: 'T921_LatarBelakangTgl'//, sqlType: 'date'
        t921LatarBelakangPihakTerlibat column: 'T921_LtrBelakangPihakTerlibat'//, sqlType: 'text'
        t921LatarBelakangFakta column: 'T921_LatarBelakangFakta'//, sqlType: 'text'
        t921SebabPelanggan column: 'T921_SebabPelanggan'//, sqlType: 'text'
        t921SebabKendaraan column: 'T921_SebabKendaraan'//, sqlType: 'text'
        t921SebabDealer column: 'T921_SebabDealer'//, sqlType: 'text'
        t921StaPenanganan column: 'T921_StaPenanganan', sqlType: 'char(1)'
        t921StrategiPenyelesaian column: 'T921_StrategiPenyelesaian'//, sqlType: 'text'
        t921TindakanTgl column: 'T921_TindakanTgl'//, sqlType: 'date'
        t921TindakanPihakTerlibat column: 'T921_TindakanPihakTerlibat'//, sqlType: 'text'
        t921TindakanFakta column: 'T921_TindakanFakta'//, sqlType: 'text'
        t921StaSolusi column: 'T921_StaSolusi', sqlType: 'char(1)'
        t921StaKepuasanPelanggan column: 'T921_StaKepuasanPelanggan', sqlType: 'char(1)'
        t921AlasanKepuasan column: 'T921_AlasanKepuasan'//, sqlType: 'text'
        t921TglPenyelesaianKeluhan column: 'T921_TglPenyelesaianKeluhan'//, sqlType: 'date'
        t921StaLangkahPencegahan column: 'T921_StaLangkahPencegahan', sqlType: 'char(1)'
        t921TextLangkahPencegahan column: 'T921_TextLangkahPencegahan'//, sqlType: 'text'
        staDel column : 'T921_StaDel', sqlType : 'char(1)'
    }
}
