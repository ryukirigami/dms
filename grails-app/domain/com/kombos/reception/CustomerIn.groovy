package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Loket
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.maintable.TujuanKedatangan
import com.kombos.reception.Reception

class CustomerIn {
	String t400ID
	CustomerVehicle customerVehicle
	TujuanKedatangan tujuanKedatangan
	String t400StaComplain
	String t400NomorAntrian
	Date t400TglCetakNoAntrian
	String t400xNamaUserIn
	String t400StaApp
	String t400StaShow
	Reception reception
	String t400StaReception
	Date t400TglJamReception
	CompanyDealer companyDealer
	Loket loket
	String t400StaSelesaiReception
	Date t400TglJamSelesaiReception
	String t400StaOut
	Date t400TglJamOut
	String t400xNamaUserOut
	String t400noPol
	String t400Keterangan
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		t400ID blank:true, nullable:true, maxSize:15
		customerVehicle blank:true, nullable:true
		tujuanKedatangan blank:false, nullable:false
		t400StaComplain blank:false, nullable:false, maxSize:1
		t400NomorAntrian blank:false, nullable:false
		t400TglCetakNoAntrian blank:true, nullable:true, maxSize:4
		t400xNamaUserIn blank:true, nullable:true, maxSize:50
		t400StaApp blank:true, nullable:true, maxSize:1
		t400StaShow blank:true, nullable:true, maxSize:1
		reception blank:true, nullable:true, maxSize:20
		t400StaReception blank:true, nullable:true, maxSize:1
		t400TglJamReception blank:true, nullable:true
		companyDealer blank:false, nullable:false
		loket blank:true, nullable:true
		t400StaSelesaiReception blank:true, nullable:true, maxSize:1
		t400TglJamSelesaiReception blank:true, nullable:true
		t400StaOut blank:true, nullable:true, maxSize:1
		t400TglJamOut blank:true, nullable:true
		t400noPol blank:true, nullable:true
		t400Keterangan blank:true, nullable:true
		t400xNamaUserOut blank:true, nullable:true, maxSize:50
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "T400_CUSTOMERIN"
		t400ID column:"T400_ID", sqltype:"char(15)"
		customerVehicle column:"T400_T103_VinCode"
		tujuanKedatangan column:"T400_M400_ID"
		t400StaComplain column:"T400_StaComplain", sqlType:"char(1)"
		t400NomorAntrian column:"T400_NomorAntrian", sqlType: "varchar(10)"
		t400TglCetakNoAntrian column:"T400_TglCetakNoAntrian"
		t400xNamaUserIn column:"T400_xNamaUserIn", sqlType:"varchar(50)"
		t400StaApp column:"T400_StaApp", sqlType:"char(1)"
		t400StaShow column:"T400_StaShow", sqlType:"char(1)",  defaultValue: '0'
		reception column:"T400_T301_T401_NoWO_App"
		t400StaReception column:"T400_StaReception", sqlType:"char(1)"
		t400TglJamReception column:"T400_TglJamReception", sqlType: "TIMESTAMP"
		companyDealer column:"T400_M011_ID"
		loket column:"T400_M404_ID"
		t400StaSelesaiReception column:"T400_StaSelesaiReception", sqlType:"char(1)"
		t400TglJamSelesaiReception column:"T400_TglJamSelesaiReception", sqlType: "TIMESTAMP"
		t400StaOut column:"T400_StaOut", sqlType:"char(1)"
		t400TglJamOut column:"T400_TglJamOut", sqlType: "TIMESTAMP"
		t400xNamaUserOut column:"T400_xNamaUserOut", sqlType:"varchar(50)"
		staDel column:"T400_StaDel", sqlType:"char(1)"
	}

}
