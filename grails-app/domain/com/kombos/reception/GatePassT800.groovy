package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.GatePass
import com.kombos.reception.Reception

class GatePassT800 {
    CompanyDealer companyDealer
    Reception reception
    String t800ID
    GatePass gatePass
    Date t800TglJamGatePass
    String t800xKet
    String t800xNamaUser
    String t800xDivisi
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        companyDealer (blank:true, nullable:true)
        reception blank:true, nullable:true
        t800ID blank:true, nullable:true
        gatePass blank:true, nullable:true
        t800TglJamGatePass blank:false, nullable:true
        t800xKet blank:true, nullable:true
        t800xNamaUser blank:true, nullable:true
        t800xDivisi blank:false, nullable:false
        staDel blank:true, nullable:true
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "T800_GATEPASS"
        reception column: 'T800_T401_NoWO'
        t800ID column: 'T800_ID', unique:true
        gatePass column: 'T800_M800_ID'
        t800TglJamGatePass column: 'T800_TglJamGatePass'//, sqlType: 'date'
        t800xKet column: 'T800_xKet', sqlType: 'varchar(50)'
        t800xNamaUser column: 'T800_xNamaUser', sqlType: 'varchar(20)'
        t800xDivisi column: 'T800_xDivisi', sqlType: 'varchar(20)'
        staDel column: 'T800_StaDel', sqlType: 'char(1)'
    }
}
