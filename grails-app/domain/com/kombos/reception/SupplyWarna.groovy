package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorCat
import com.kombos.administrasi.WarnaVendor
import com.kombos.customerprofile.Warna

class SupplyWarna {
    CompanyDealer companyDealer
	Reception reception
	POS pos
	VendorCat vendorcat
	WarnaVendor warna
	Double t420Qty
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false, unique:false, maxSize:20
		pos blank:false, nullable:false, unique:false, maxSize:20
		vendorcat blank:false, nullable:false, unique:false
		warna blank:false, nullable:false, unique:false, maxSize:10
		t420Qty blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T420_SUPPLYWARNA'
		reception column:'T420_T401_NoWO'
		pos column:'T420_T418_NoPOS'
		vendorcat column:'T420_M192_M191_ID'
		warna column:'T420_M192_IDWarna'
		t420Qty column:'T420_Qty'//, sqlType:'double'
	}
}
