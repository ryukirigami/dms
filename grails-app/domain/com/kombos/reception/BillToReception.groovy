package com.kombos.reception

import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP

class BillToReception {

    Reception reception
    String jenisBayar
    String billTo
    String idBillTo
    String namaBillTo
    static hasMany = [jobs : JobRCP, parts : PartsRCP]

    static constraints = {
        reception nullable: true, blank: true
        jenisBayar nullable: true, blank: true
        billTo nullable: true, blank: true
        idBillTo nullable: true, blank: true
        namaBillTo nullable: true, blank: true
        jobs nullable: true
        parts nullable: true
    }

    static mapping = {
        reception column: "Reception"
        jenisBayar column: "JenisBayar"
        billTo column: "BillTo"
        idBillTo column: "IdBillTo"
        namaBillTo column: "NamaBillTo"
        jobs column: "JOBS"
        parts column: "PARTS"
        table "BILLTORECEPTION"
    }
}
