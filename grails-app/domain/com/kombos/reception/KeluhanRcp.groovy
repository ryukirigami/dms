package com.kombos.reception

import com.kombos.administrasi.CompanyDealer

class KeluhanRcp {
    CompanyDealer companyDealer
    Reception reception
    String t411NamaKeluhan //T411_NamaKeluhan
    String t411StaButuhDiagnose //T411_StaButuhDiagnose
	Integer t411NoUrut
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        reception blank:false, nullable:false
        t411NamaKeluhan blank:false, nullable:false
        t411StaButuhDiagnose blank:false, nullable:false, maxSize:1
        t411NoUrut nullable:true
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T411_KELUHANRCP'
        reception column:'T411_T401_NoWO'
        t411NamaKeluhan column:'T411_NamaKeluhan'
        t411StaButuhDiagnose column:'T411_StaButuhDiagnose', sqlType:'char(1)'
        staDel column:'T411_StaDel', sqlType:'char(1)'
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
        lastUpdated = new Date()
        staDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}
