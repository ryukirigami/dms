package com.kombos.reception

class GatePass {
	
	String m800Id
	String m800NamaGatePass
	String m800StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M800_GATEPASS'
		
		m800Id column : 'M800_ID', sqlType : 'char(2)'
		m800NamaGatePass column : 'M800_NamaGatePass', sqlType : 'varchar(50)'
		m800StaDel column : 'M800_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		m800Id (nullable : false, blank : false, unique : true, maxSize : 2)
		m800NamaGatePass (nullable : false, blank : false, maxSize : 50)
		m800StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m800NamaGatePass
	}
}
