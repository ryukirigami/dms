package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.CustomerVehicle

class TowingMalam {
    CompanyDealer companyDealer
	String t421ID
	Date t421TglJamDatang
	CustomerVehicle customerVehicle
	String t421Mobil
	String t421Tahun
	String t421Warna
	String t421NamaPemilik
	String t421NoTelp
	String t421NamaPenerima
	String t421StaMetodeFU
	String t421StaFU
	Date t421TglJamFU
	String t421NamaFU
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t421ID blank:true, nullable: true, maxSize:15
		t421TglJamDatang blank:false, nullable:false
		customerVehicle blank:true, nullable:true
		t421Mobil blank:false, nullable:false, maxSize:50
		t421Tahun blank:false, nullable:false, maxSize:4
		t421Warna blank:false, nullable:false, maxSize:50
		t421NamaPemilik blank:false, nullable:false, maxSize:50
		t421NoTelp blank:false, nullable:false, maxSize:50
		t421NamaPenerima blank:false, nullable:false, maxSize:50
		t421StaMetodeFU blank:true, nullable:true, maxSize:1
		t421StaFU blank:true, nullable:true, maxSize:1
		t421NamaFU blank:true, nullable:true, maxSize:50
        t421TglJamFU blank:true, nullable:true
        staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
		table 'T421_TOWINGMALAM'
		t421ID column:'T421_ID', sqlType:'char(15)'
		t421TglJamDatang column:'T421_TglJamDatang'//, sqlType: 'date'
		customerVehicle column:'T421_T103_VinCode'
		t421Mobil column:'T421_Mobil', sqlType:'varchar(50)'
		t421Tahun column:'T421_Tahun', sqlType:'varchar(4)'
		t421Warna column:'T421_Warna', sqlType:'varchar(50)'
		t421NamaPemilik column:'T421_NamaPemilik', sqlType:'varchar(50)'
		t421NoTelp column:'T421_NoTelp', sqlType:'varchar(50)'
		t421NamaPenerima column:'T421_NamaPenerima', sqlType:'varchar(50)'
		t421StaMetodeFU column:'T421_StaMetodeFU', sqlType:'char(1)'
		t421StaFU column:'T421_StaFU', sqlType:'char(1)'
		t421TglJamFU column:'T421_TglJamFU'//, sqlType: 'date'
		t421NamaFU column:'T421_NamaFU', sqlType:'varchar(50)'
        staDel column:"T421_StaDel", sqlType:"char(1)"
	}
}
