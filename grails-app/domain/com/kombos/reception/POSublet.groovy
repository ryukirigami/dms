package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.reception.Reception

class POSublet {
    CompanyDealer companyDealer
	String t412NoPOSublet
	Date t412TanggalPO
	String t412Perihal
    String staSend = "0"
	Reception reception
	Operation operation
	String t412xKet
	String t412xNamaUser
	String t412xDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t412NoPOSublet blank:false, nullable:false, maxSize:20
		t412TanggalPO blank:false, nullable:false
		t412Perihal blank:false, nullable:false, maxSize:50
		reception blank:false, nullable:false
		operation blank:false, nullable:false
		t412xKet blank:true, nullable:true, maxSize:50
		t412xNamaUser blank:true, nullable:true, maxSize:20
		t412xDivisi blank:true, nullable:true, maxSize:20
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T412_POSUBLET'
		t412NoPOSublet column:'T412_NoPOSublet', sqlType:'char(20)'
		t412TanggalPO column:'T412_TanggalPO'//, sqlType: 'date'
		t412Perihal column:'T412_Perihal', sqlType:'varchar(50)'
		reception column:'T412_T401_NoWO'
		operation column:'T412_T402_M053_JobID'
		t412xKet column:'T412_xKet', sqlType:'varchar(50)'
		t412xNamaUser column:'T412_xNamaUser', sqlType:'varchar(20)'
		t412xDivisi column:'T412_xDivisi', sqlType:'varchar(20)'
		staDel column:'T412_StaDel', sqlType:'char(1)'
	}
}
