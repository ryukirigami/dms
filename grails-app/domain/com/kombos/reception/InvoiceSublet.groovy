package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.reception.POSublet
import com.kombos.reception.Reception

class InvoiceSublet {
    CompanyDealer companyDealer
	String t413NoInv
	Date t413TanggalInv
	POSublet poSublet
	Reception reception
	Operation operation
	Double t413JmlSublet
	Double t413JmlBayar
	String t413xKet
	String t413xNamaUser
	String t413xDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t413NoInv blank:false, nullable:false, maxSize:50
		t413TanggalInv  blank:false, nullable:false
		poSublet blank:false, nullable:false
		reception blank:false, nullable:false
		operation blank:false, nullable:false
		t413JmlSublet  blank:false, nullable:false, maxSize:8
		t413JmlBayar  blank:false, nullable:false, maxSize:8
		t413xKet  blank:true, nullable:true, maxSize:50
		t413xNamaUser  blank:true, nullable:true, maxSize:20
		t413xDivisi  blank:true, nullable:true, maxSize:20
		staDel  blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T413_INVOICESUBLET'
		t413NoInv column:'T413_NoInv', sqlType:'varchar(50)'
		t413TanggalInv column:'T413_TanggalInv'//, sqlType: 'date'
		poSublet column:'T413_T412_NoPOSublet'
		reception column:'T413_T412_T401_NoWO'
		operation column:'T413_T412_T402_M053_JobID'
		t413JmlSublet column:'T413_JmlSublet'
        t413JmlBayar column:'T413_JmlBayar'
		t413xKet column:'T413_xKet', sqlType:'varchar(50)'
		t413xNamaUser column:'T413_xNamaUser', sqlType:'varchar(20)'
		t413xDivisi column:'T413_xDivisi', sqlType:'varchar(20)'
		staDel column:'T413_StaDel', sqlType:'char(1)'
	}
}
