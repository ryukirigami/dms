package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods
import com.kombos.reception.Reception

class MOS {
    CompanyDealer companyDealer
	Reception reception
	String t417NoMOS
	Date t417TglMOS
	Goods goods
	Double t417Qty1
	Double t417Qty2
	String t417xKet
	String t417xNamaUser
	String t417xDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false, maxSize:20, unique:false
		t417NoMOS blank:false, nullable:false, maxSize:20, unique:false
		t417TglMOS blank:false, nullable:false
		goods blank:false, nullable:false, maxSize:14, unique:false
		t417Qty1 blank:false, nullable:false
		t417Qty2 blank:false, nullable:false
		t417xKet blank:false, nullable:false, maxSize:50
		t417xNamaUser blank:false, nullable:true, maxSize:20
		t417xDivisi blank:false, nullable:true, maxSize:50
        staDel blank:false, nullable:true, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T417_MOS'
		reception column:'T417_T401_NoWO'
		t417NoMOS column:'T417_NoMOS', sqlType:'char(20)'
		t417TglMOS column:'T417_TglMOS'//, sqlType: 'date'
		goods column:'T417_M111_ID'
		t417Qty1 column:'T417_Qty1'
		t417Qty2 column:'T417_Qty2'
		t417xKet column:'T417_xKet', sqlType:'varchar(50)'
		t417xNamaUser column:'T417_xNamaUser', sqlType:'varchar(20)'
		t417xDivisi column:'T417_xDivisi', sqlType:'varchar(50)'
        staDel column:'T417_StaDel', sqlType:'char(1)'
	}
}
