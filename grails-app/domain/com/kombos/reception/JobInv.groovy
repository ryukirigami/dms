package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.maintable.InvoiceT701
//import com.kombos.parts.Invoice

class JobInv {
    CompanyDealer companyDealer
	//Invoice invoice
    InvoiceT701 invoice
    InvoiceSublet invoiceSublet
    Reception reception
	Operation operation
	Double t702HargaRp
	Double t702DiscRp
	Double t702DiscPersen
	Double t702TotalRp
	Double t702DPRp
	Double t702SisaTagihanRp
	String t702xKet
	String t702xNamaUser
	String t702xDivisi
	String t702NoUrutKeluhan
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		invoice blank:true, nullable:true
		operation blank:false, nullable:true
        invoiceSublet blank:true, nullable:true
        reception blank:true, nullable:true
        t702HargaRp blank:true, nullable:true
		t702DiscRp blank:true, nullable:true
		t702DiscPersen blank:true, nullable:true
		t702TotalRp blank:true, nullable:true
		t702DPRp blank:true, nullable:true
		t702SisaTagihanRp blank:true, nullable:true
		t702xKet blank:true, nullable:true, maxSize:50
		t702xNamaUser blank:true, nullable:true, maxSize:20
		t702xDivisi blank:true, nullable:true, maxSize:20
		t702NoUrutKeluhan nullable:true, maxSize:200
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T702_JOBINV'
		invoice column:'T702_T701_NoInv'
		operation column:'T702_M053_JobID'
		invoiceSublet column:'T702_T413_NoInv'
		reception column:'T702_t401_JobID'
		t702HargaRp column:'T702_HargaRp'
		t702DiscRp column:'T702_DiscRp'
		t702DiscPersen column:'T702_DiscPersen'
		t702TotalRp column:'T702_TotalRp'
		t702DPRp column:'T702_DPRp'
		t702SisaTagihanRp column:'T702_SisaTagihanRp'
		t702xKet column:'T702_xKet', sqlType:'varchar(50)'
		t702xNamaUser column:'T702_xNamaUser', sqlType:'varchar(20)'
		t702xDivisi column:'T702_xDivisi', sqlType:'varchar(20)'
		staDel column:'T702_StaDel', sqlType:'char(1)'
	}
}
