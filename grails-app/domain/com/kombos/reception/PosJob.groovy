package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation

class PosJob {

    CompanyDealer companyDealer
	Reception reception
	POS pos
	Operation operation
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false, unique:false
		pos blank:false, nullable:false, unique:false
		operation blank:false, nullable:false, unique:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "T419_POSJOB"
		reception column:"T419_T401_NoWO"
		pos column:"T419_T418_NoPOS"
		operation column:"T419_M053_JobID"
	}
}
