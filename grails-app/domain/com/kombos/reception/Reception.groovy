package com.kombos.reception

import com.kombos.administrasi.BumperPaintingTime
import com.kombos.administrasi.ColorMatching
import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Operation
import com.kombos.administrasi.RepairDifficulty
import com.kombos.administrasi.Stall
import com.kombos.board.JPB
import com.kombos.board.ASB
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.customerprofile.SPK
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.maintable.AlasanCancel
import com.kombos.maintable.Invitation
import com.kombos.maintable.TipeKerusakan

class Reception {
    CompanyDealer companyDealer
	String t401NoWO
	Date t401TanggalWO
	String t401StaReceptionEstimasiSalesQuotation
	CustomerIn customerIn
	//CustomerVehicle customerVehicle
	HistoryCustomerVehicle historyCustomerVehicle
    Operation operation
    NamaManPower namaManPower
	//Customer customer
	HistoryCustomer historyCustomer
	String t401StaApp
	String t401NoAppointment
	String t401StaKategoriCustUmumSPKAsuransi
	String t401BawaSPK
	SPK sPK
	String t401BawaAsuransi
	SPKAsuransi sPkAsuransi
	Date t401TglJamCetakWO
	String t401StaAmbilWO
	Date t401TglJamAmbilWO
	String t401PermintaanCust
	String t401StaTinggalMobil
	String t401StaTinggalParts
	String t401StaCustomerTunggu
	String t401StaObatJalan
	Date t401TglStaObatJalan
	String t401StaButuhSPKSebelumProd
	Date t401TglJamKirimBerkasAsuransi
	Date t401TglJamJanjiPenyerahan
	Date t401TglJamRencana
	Date t401TglJamJanjiBayarDP
	Date t401TglJamPenyerahan
	String t401StaOkCancelReSchedule
	Date t401TglJamPenyerahanReSchedule
	String t401AlasanReSchedule
	byte[] t401GambarWAC
	String t401CatatanWAC
	Integer t401KmSaatIni
	Integer t401Bensin
	String t401Catatan
	TipeKerusakan tipeKerusakan
	String t401StaTPSLine
	ColorMatching colorMatchingKlasifikasi
	ColorMatching colorMatchingJmlPanel
	Double t401Proses1 
	Double t401Proses2
	Double t401Proses3
	Double t401Proses4
	Double t401Proses5
	Double t401TotalProses
	String t401HP
	Double t401HargaRp
	Double t401DiscRp
	Double t401DiscPersen
	Double t401TotalRp
	Double t401DPRp
	Double t401OnRisk
	String t401StaBayarOnRisk
	Double t401TagihanRp
	Double t401SpDiscRp
	AlasanCancel alasanCancel
	String t401NamaSA
	String t401staReminderSBE
	Date t401TglJamNotifikasi
	String t401StaPDI
    String staSave      //0 = YANG JADI RECEPTION 1 = TIDAK JADI
    String staApprovalDisc
	String t401StaInvoice
	String t401NamaSAPenerimaBPCenter
	String t401NoExplanation
	Date t401TglJamExplanation
	Date t401TglJamExplanationSelesai
	String t401NamaSAExplaination
	Stall stall
	String t401Area
	RepairDifficulty repairDifficulty
	BumperPaintingTime bumperPaintngTimeCompanyDealer
	BumperPaintingTime bumperPaintngTimeTglBerlaku
	BumperPaintingTime bumperPaintngTimeMasterPanel
	static hasMany = [jpbs : JPB, asbs: ASB, t401PermintaanCusts: PermintaanCustomer, wacItem: WacItem]
    
	String t401xKet
	String t401xNamaUser
	String t401xDivisi

	// from parts app	
	Double totalRencanaDP
	Double totalDPRp
	String partsTersedia
	String staRequestParts
	
	//from asb
	Integer countASB
	
	//from jpb
	Integer countJPB
    Integer countFinished
    Integer countJob
    Invitation flagInvitation

	String staTwc
	String isProgress
	String isPending
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t401NoWO blank:false, nullable:true, maxSize:20, unique:true 
		t401TanggalWO blank:false, nullable:true, maxSize:4
		t401StaReceptionEstimasiSalesQuotation blank:false, nullable:true, maxSize:50
		customerIn blank:false, nullable:true, maxSize:15
		//customerVehicle blank:false, nullable:true, maxSize:17
		historyCustomerVehicle blank:false, nullable:true, maxSize:4
        operation blank:true, nullable:true, maxSize:4
        namaManPower blank:true, nullable:true, maxSize:4
		//customer blank:false, nullable:true, maxSize:12
		historyCustomer blank:false, nullable:true, maxSize:4
		t401StaApp blank:false, nullable:true, maxSize:1
		t401NoAppointment blank:false, nullable:true, maxSize:20
		t401StaKategoriCustUmumSPKAsuransi blank:false, nullable:true, maxSize:1
		t401BawaSPK blank:false, nullable:true, maxSize:1
		sPK blank:false, nullable:true, maxSize:12
		t401BawaAsuransi blank:false, nullable:true, maxSize:1
		sPkAsuransi blank:false, nullable:true, maxSize:24
		t401TglJamCetakWO blank:false, nullable:true, maxSize:4
		t401StaAmbilWO blank:false, nullable:true, maxSize:1
		t401TglJamAmbilWO blank:false, nullable:true, maxSize:4
		t401PermintaanCust blank:true, nullable:true, maxSize:16
		t401StaTinggalMobil blank:false, nullable:true, maxSize:1
		t401StaTinggalParts blank:false, nullable:true, maxSize:1
		t401StaCustomerTunggu blank:false, nullable:true, maxSize:1
		t401StaObatJalan blank:false, nullable:true, maxSize:1
		t401TglStaObatJalan blank:false, nullable:true, maxSize:4
		t401StaButuhSPKSebelumProd blank:false, nullable:true, maxSize:1
		t401TglJamKirimBerkasAsuransi blank:false, nullable:true, maxSize:4
		t401TglJamJanjiPenyerahan blank:false, nullable:true, maxSize:4
		t401TglJamRencana blank:false, nullable:true, maxSize:4
		t401TglJamJanjiBayarDP blank:false, nullable:true, maxSize:4
		t401TglJamPenyerahan blank:false, nullable:true, maxSize:4
		t401StaOkCancelReSchedule blank:false, nullable:true, maxSize:1
		t401TglJamPenyerahanReSchedule blank:false, nullable:true, maxSize:4
		t401AlasanReSchedule blank:false, nullable:true, maxSize:50
		t401GambarWAC blank:true, nullable:true
		t401CatatanWAC blank:true, nullable:true
		t401KmSaatIni blank:false, nullable:true, maxSize:4
		t401Bensin blank:false, nullable:true, maxSize:4
		t401Catatan blank:false, nullable:true, maxSize:50
		tipeKerusakan blank:false, nullable:true, maxSize:1
		t401StaTPSLine blank:false, nullable:true, maxSize:1
		colorMatchingKlasifikasi blank:false, nullable:true, maxSize:4
		colorMatchingJmlPanel blank:false, nullable:true, maxSize:4
		t401Proses1 blank:false, nullable:true, maxSize:8
		t401Proses2 blank:false, nullable:true, maxSize:8
		t401Proses3 blank:false, nullable:true, maxSize:8
		t401Proses4 blank:false, nullable:true, maxSize:8
		t401Proses5 blank:false, nullable:true, maxSize:8
		t401TotalProses blank:false, nullable:true, maxSize:8
		t401HP blank:false, nullable:true, maxSize:50
		t401HargaRp blank:false, nullable:true, maxSize:8
		t401DiscRp blank:false, nullable:true, maxSize:8
		t401DiscPersen blank:false, nullable:true, maxSize:8
		t401TotalRp blank:false, nullable:true, maxSize:8
		t401DPRp blank:false, nullable:true
		t401OnRisk blank:false, nullable:true
        t401StaBayarOnRisk blank:false, nullable:true, maxSize:1
		t401TagihanRp blank:false, nullable:true, maxSize:8
		t401SpDiscRp blank:false, nullable:true, maxSize:8
		alasanCancel blank:false, nullable:true, maxSize:4
		t401NamaSA blank:false, nullable:true, maxSize:20
		t401staReminderSBE blank:false, nullable:true, maxSize:1
		t401TglJamNotifikasi blank:false, nullable:true, maxSize:4
		t401StaPDI blank:false, nullable:true, maxSize:1
		t401StaInvoice blank:false, nullable:true, maxSize:1
		t401NamaSAPenerimaBPCenter blank:false, nullable:true, maxSize:50
		t401NoExplanation blank:false, nullable:true, maxSize:20
		t401TglJamExplanation blank:false, nullable:true, maxSize:4
		t401TglJamExplanationSelesai blank:false, nullable:true, maxSize:4
		t401NamaSAExplaination blank:false, nullable:true, maxSize:50
		stall blank:false, nullable:true, maxSize:8
		t401Area blank:false, nullable:true, maxSize:4
		repairDifficulty blank:false, nullable:true, maxSize:4
		bumperPaintngTimeCompanyDealer blank:false, nullable:true, maxSize:4
		bumperPaintngTimeTglBerlaku blank:false, nullable:true, maxSize:4
		bumperPaintngTimeMasterPanel blank:false, nullable:true, maxSize:4
		t401xKet blank:false, nullable:true, maxSize:50
		t401xNamaUser blank:false, nullable:true, maxSize:20
		t401xDivisi blank:false, nullable:true, maxSize:50
		jpbs  nullable:true
		asbs  nullable:true
        flagInvitation nullable: true
        staSave blank:true, nullable:true, maxSize:1
        staApprovalDisc blank:true, nullable:true, maxSize:1
        staDel blank:false, nullable:true, maxSize:1
        isPending blank:true, nullable:true, maxSize:1
        isProgress blank:true, nullable:true, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
		wacItem nullable:true
        staTwc nullable: true, blank: true
    }
	
	static mapping = {
        autoTimestamp false
        table 'T401_RECEPTION'
        t401PermintaanCusts joinTable: [name: 'T401_RecPermintaanCust',
                key: 'Recp_T401Permintaan_Custs_Id',
                column: 'Permintaan_Customer_Id']
		t401NoWO column:'T401_NoWO'//, sqlType:'char(20)'
		t401TanggalWO column:'T401_TanggalWO'//, sqlType: 'date'
		t401StaReceptionEstimasiSalesQuotation column:'T401_StaRecEstSlsQuot'
		customerIn  column:'T401_T400_ID'
		//customerVehicle column:'T401_T103VinCode'
		historyCustomerVehicle column:'T401_T183_ID'
        operation column:'T401_M053_ID'
        namaManPower column:'T401_T015_ID'
		//customer column:'T401_T102_ID'
		historyCustomer column:'T401_T182_ID'
		t401StaApp column:'T401_StaApp', sqlType:'char(1)'
		t401NoAppointment column:'T401_NoAppointment', sqlType:'char(20)'
		t401StaKategoriCustUmumSPKAsuransi column:'T401_StaKtgrCustUmumSPKAsrnsi', sqlType:'char(1)'
		t401BawaSPK column:'T401_BawaSPK', sqlType:'char(1)'
		sPK column:'T401_T191_IDSPK'
		t401BawaAsuransi column:'T401_BawaAsuransi', sqlType:'char(1)'
		sPkAsuransi column:'T401_T193_IDAsuransi'
		t401TglJamCetakWO column:'T401_TglJamCetakWO'//, sqlType: 'date'
		t401StaAmbilWO column:'T401_StaAmbilWO', sqlType:'char(1)'
		t401TglJamAmbilWO column:'T401_TglJamAmbilWO'//, sqlType: 'date'
		t401PermintaanCust column:'T401_PermintaanCust'//, sqlType:'text'
		t401StaTinggalMobil column:'T401_StaTinggalMobil', sqlType:'char(1)'
		t401StaTinggalParts column:'T401_StaTinggalParts', sqlType:'char(1)'
		t401StaCustomerTunggu column:'T401_StaCustomerTunggu', sqlType:'char(1)'
		t401StaObatJalan column:'T401_StaObatJalan', sqlType:'char(1)'
		t401TglStaObatJalan column:'T401_TglStaObatJalan'//, sqlType: 'date'
		t401StaButuhSPKSebelumProd column:'T401_StaButuhSPKSebelumProd', sqlType:'char(1)'
		t401TglJamKirimBerkasAsuransi column:'T401_TglJamKirimBerkasAsuransi'//, sqlType: 'date'
		t401TglJamJanjiPenyerahan column:'T401_TglJamJanjiPenyerahan'//, sqlType: 'date'
		t401TglJamRencana column:'T401_TglJamRencana'//, sqlType: 'date'
		t401TglJamJanjiBayarDP column:'T401_TglJamJanjiBayarDP'//, sqlType: 'date'
		t401TglJamPenyerahan column:'T401_TglJamPenyerahan'
		t401StaOkCancelReSchedule column:'T401_StaOkCancelReSchedule', sqlType:'char(1)'
		t401TglJamPenyerahanReSchedule column:'T401_TglJamPnyrhnReSchedule'//, sqlType: 'date'
		t401AlasanReSchedule column:'T401_AlasanReSchedule', sqlType:'varchar(50)'
		t401GambarWAC column:'T401_GambarWAC', sqlType:'blob'
		t401CatatanWAC column:'T401_CatatanWAC'//, sqlType:'text'
		t401KmSaatIni column:'T401_KmSaatIni', sqlType:'int'
		t401Bensin column:'T401_Bensin', sqlType:'int'
		t401Catatan column:'T401_Catatan', sqlType:'varchar(50)'
		tipeKerusakan column:'T401_M401_ID'
		t401StaTPSLine column:'T401_StaTPSLine', sqlType:'char(1)'
		colorMatchingKlasifikasi column:'T401_M046_Klasifikasi'
		colorMatchingJmlPanel column:'T401_M046_JmlPanel'
		t401Proses1 column:'T401_Proses1'
		t401Proses2 column:'T401_Proses2'
		t401Proses3 column:'T401_Proses3'
		t401Proses4 column:'T401_Proses4'
		t401Proses5 column:'T401_Proses5'
		t401TotalProses column:'T401_TotalProses'
		t401HP column:'T401_HP', sqlType:'varchar(50)'
		t401HargaRp column:'T401_HargaRp'
		t401DiscRp column:'T401_DiscRp'
		t401DiscPersen column:'T401_DiscPersen'
		t401TotalRp column:'T401_TotalRp'
		t401DPRp column:'T401_DPRp'
		t401OnRisk column:'T401_OnRisk'
        t401StaBayarOnRisk column:'T401_StaBayarOnRisk'
		t401TagihanRp column:'T401_TagihanRp'
		t401SpDiscRp column:'T401_SpDiscRp'
		alasanCancel column:'T401_M402_ID'
		t401NamaSA column:'T401_NamaSA', sqlType:'varchar(20)'
		t401staReminderSBE column:'T401_staReminderSBE', sqlType:'char(1)'
		t401TglJamNotifikasi column:'T401_TglJamNotifikasi'//, sqlType: 'date'
		t401StaPDI column:'T401_StaPDI', sqlType:'char(1)'
		t401StaInvoice column:'T401_StaInvoice', sqlType:'char(1)'
		t401NamaSAPenerimaBPCenter column:'T401_NamaSAPenerimaBPCenter', sqlType:'varchar(50)'
		t401NoExplanation column:'T401_NoExplanation', sqlType:'char(20)'
		t401TglJamExplanation column:'T401_TglJamExplanation'//, sqlType: 'date'
		t401TglJamExplanationSelesai column:'T401_TglJamExplanationSelesai'//, sqlType: 'date'
		t401NamaSAExplaination column:'T401_NamaSAExplaination', sqlType:'varchar(50)'
		stall column:'T401_M022_StallID'
		t401Area column:'T401_Area', sqlType:'int'
		repairDifficulty column:'T401_M042_ID'
		bumperPaintngTimeCompanyDealer column:'T401_M045_M011_ID'
		bumperPaintngTimeTglBerlaku column:'T401_M045_TglBerlaku'
		bumperPaintngTimeMasterPanel column:'T401_M045_M094_ID'
		t401xKet column:'T401_xKet', sqlType:'varchar(50)'
		t401xNamaUser column:'T401_xNamaUser', sqlType:'varchar(20)'
		t401xDivisi column:'T401_xDivisi', sqlType:'varchar(50)'
		staDel column:'T401_StaDel', sqlType:'char(1)'
        staSave column:'T401_StaSave', sqlType:'char(1)'
        staApprovalDisc column:'T401_StaAppDisc', sqlType:'char(1)'
        flagInvitation column: "flag_Invitation"
        staTwc column: "t401_statwc", sqlType: 'varchar(1)'
		totalRencanaDP formula: "coalesce((SELECT SUM(pa.T303_RencanaDP) FROM T303_PARTSAPP pa WHERE id=pa.T303_T401_NoWO AND pa.T303_StaHarusBookingFee = '1') ,0)"
		totalDPRp formula: "coalesce((SELECT SUM(pa.T303_DPRp) FROM T303_PARTSAPP pa WHERE id=pa.T303_T401_NoWO AND pa.T303_StaHarusBookingFee = '1'),0)"
		partsTersedia formula: "case when coalesce((SELECT SUM(ps.T131_Qty1) FROM T303_PARTSAPP pa INNER JOIN T131_PARTSSTOK ps ON pa.T303_M111_ID = ps.T131_M111_ID WHERE id=pa.T303_T401_NoWO), 0) + coalesce((SELECT SUM(rd.T162_Qty1) FROM T303_PARTSAPP pa INNER JOIN T162_REQUEST_DETAIL rd ON pa.T303_M111_ID = rd.T162_M111_ID WHERE id=pa.T303_T401_NoWO), 0) > 0 then 'Tersedia' else 'Tidak Tersedia' end" // TODO cek company dealernya
		staRequestParts formula: "case when coalesce((SELECT COUNT(pa.id) FROM T303_PARTSAPP pa WHERE id = pa.T303_T401_NoWO AND pa.T303_StaRequestPart = '1'), 0) = 0 then '0' else '1' end"
		countASB formula: "coalesce((SELECT COUNT(asb.id) FROM T351_ASB asb WHERE id = asb.T351_T401_NoWO), 0)"
		countJPB formula: "coalesce((SELECT COUNT(jpb.id) FROM T451_JPB jpb WHERE id = jpb.T451_T401_NoWO and jpb.T451_StaDel = '0'), 0)"
		countFinished formula: "coalesce((SELECT COUNT(act.id) FROM T452_ACTUAL act, M452_STATUSACTUAL staAct WHERE ID = act.T452_T401_NoWO AND act.T452_M452_ID=staAct.id and staAct.M452_StatusActual like '%Clock Off%'), 0)"
        countJob formula: "coalesce((SELECT COUNT(job.id) FROM T402_JOBRCP job where ID = job.T402_T401_NoWO AND job.T402_STATAMBAHKURANG is null OR job.T402_STATAMBAHKURANG != '1' AND job.T402_STAAPPROVETAMBAHKURANG!='0' ), 0)"
	}
    String toString(){
        return t401NoWO
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        staDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}