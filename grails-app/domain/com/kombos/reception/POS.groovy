package com.kombos.reception

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorCat
import com.kombos.customerprofile.Warna
import com.kombos.reception.Reception

class POS {
    CompanyDealer companyDealer
	Reception reception
	String t418NoPOS
	Date t418TglPOS
	Warna warna
	VendorCat vendorCat
	String t418KetOrder
	String t418StaTagihKe
	String t418NoSupply
	Date t418TglSupply
	String t418KetSupply
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false, maxSize:20, unique:false
		t418NoPOS blank:false, nullable:false, maxSize:20, unique:false
		t418TglPOS blank:false, nullable:true
		warna blank:false, nullable:true
		vendorCat blank:false, nullable:true
		t418KetOrder blank:true, nullable:true, maxSize:50
		t418StaTagihKe blank:true, nullable:true, maxSize:1
		t418NoSupply blank:true, nullable:true, maxSize:50
		t418TglSupply blank:true, nullable:true
		t418KetSupply blank:true, nullable:true, maxSize:50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T418_POS'
		reception column:'T418_T401_NoWO'
		t418NoPOS column:'T418_NoPOS', sqlType:'char(20)'
		t418TglPOS column:'T418_TglPOS'//, sqlType: 'date'
		warna column:'T418_M092_ID'
		vendorCat column:'T418_M191_ID'
		t418KetOrder column:'T418_KetOrder', sqlType:'varchar(50)'
		t418StaTagihKe column:'T418_StaTagihKe', sqlType:'char(1)'
		t418NoSupply column:'T418_NoSupply', sqlType:'varchar(50)'
		t418TglSupply column:'T418_TglSupply'//, sqlType: 'date'
		t418KetSupply column:'T418_KetSupply', sqlType:'varchar(50)'
	}
}
