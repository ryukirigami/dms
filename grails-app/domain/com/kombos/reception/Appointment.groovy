package com.kombos.reception

import com.kombos.administrasi.ColorMatching
import com.kombos.administrasi.CompanyDealer
import com.kombos.board.ASB
import com.kombos.board.JPB
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.customerprofile.SPK
import com.kombos.maintable.AlasanCancel
import com.kombos.maintable.Customer
import com.kombos.maintable.JenisApp
import com.kombos.maintable.TipeKerusakan

class Appointment {
    CompanyDealer companyDealer
	Reception reception
    ASB asb
    JPB jpb
	CustomerVehicle customerVehicle
	HistoryCustomerVehicle historyCustomerVehicle
	Customer customer
	HistoryCustomer historyCustomer
	String t301StaKategoriCustUmumSPKAsuransi
	SPK sPK
	SPKAsuransi sPKAsuransi
	Date t301TglJamApp
	JenisApp jenisApp
	String t301KategoriCust
	String t301StaFir
	String t301StaTHS
	String t301AlamatTHS
	Double t301KmSaatIni
	String t301NamaSA
	String t301Permintaan
	CompanyDealer companyDealerIDSumber
	CompanyDealer companyDealerIDTujuan
	Date t301TglJamRencana
	Date t301TglJamJanjiBayarDP
	Date t301TglJamPenyerahann
	String t301StaOkCancelReSchedule
	Date t301TglJamPenyerahanSchedule
	String t301AlasanReSchedule
	Double t301SubTotalRp
	Double t301PersenPPN
	Double t301PPNRp
	Double t301GrandTotalRp
	Double t301DPRp
	Double t301SisaRp
	AlasanCancel alasanCancel
	String t301staFU1
	Date t301TglFU1
	String t301xNamaUserFU1
	String t301staFU2
	Date t301TglFU2
	String t301xNamaUserFU2
	String t301Ket
	String t301staDariMRS
	String t301staBookingWalkIn
	String t301staBookingTelp
	String t301staBookingWeb
	String t301NoDokumenEstimasi
	Date t301TglDokumenEstimasi
	String t301TkKerusakan
	TipeKerusakan tipeKerusakan
	String t301StaTPSLine
	ColorMatching colorMatchingKlasifikasi
	ColorMatching colorMatchingJmlPanel
	Double t301Proses1
	Double t301Proses2
	Double t301Proses3
	Double t301Proses4
	Double t301Proses5
	Double t301TotalProses
	String t301staCustomerTunggu
	String t301xNamaUser
	String t301xNamaDivisi
	String staDel
    String staSave
    String staUsed
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    int jamApp
    int menitApp
    String saveFlag

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:true
        asb blank:true, nullable:true
        jpb blank:true, nullable:true
		customerVehicle blank:false, nullable:true, maxSize:17
		historyCustomerVehicle blank:false, nullable:true, maxSize:4
		customer blank:false, nullable:true, maxSize:12
		historyCustomer blank:false, nullable:true, maxSize:4
		t301StaKategoriCustUmumSPKAsuransi blank:false, nullable:true, maxSize:1
		sPK blank:false, nullable:true, maxSize:12
		sPKAsuransi blank:false, nullable:true, maxSize:24
		t301TglJamApp blank:false, nullable:true, maxSize:4
		jenisApp blank:false, nullable:true, maxSize: 2
		t301KategoriCust blank:false, nullable:true, maxSize:1
		t301StaFir blank:false, nullable:true, maxSize:1
		t301StaTHS blank:true, nullable:true, maxSize:1
		t301AlamatTHS blank:true, nullable:true, maxSize:255
		t301KmSaatIni blank:false, nullable:true, maxSize:8
		t301NamaSA blank:false, nullable:true, maxSize:50
		t301Permintaan blank:false, nullable:true, maxSize:16
		companyDealerIDSumber blank:false, nullable:true, maxSize:4
		companyDealerIDTujuan blank:false, nullable:true, maxSize:4
		t301TglJamRencana blank:false, nullable:true, maxSize:4
		t301TglJamJanjiBayarDP blank:false, nullable:true, maxSize:4
		t301TglJamPenyerahann blank:false, nullable:true, maxSize:4
		t301StaOkCancelReSchedule blank:false, nullable:true, maxSize:1
		t301TglJamPenyerahanSchedule blank:false, nullable:true, maxSize:4
		t301AlasanReSchedule blank:false, nullable:true, maxSize:50
		t301SubTotalRp blank:false, nullable:true, maxSize:8
		t301PersenPPN blank:false, nullable:true, maxSize:8
		t301PPNRp blank:false, nullable:true, maxSize:8
		t301GrandTotalRp blank:false, nullable:true, maxSize:8
		t301DPRp blank:false, nullable:true, maxSize:8
		t301SisaRp blank:false, nullable:true, maxSize:8
		alasanCancel blank:false, nullable:true, maxSize:4
		t301staFU1 blank:false, nullable:true, maxSize:1
		t301TglFU1 blank:false, nullable:true, maxSize:4
		t301xNamaUserFU1 blank:false, nullable:true, maxSize:50
		t301staFU2 blank:false, nullable:true, maxSize:1
		t301TglFU2 blank:false, nullable:true, maxSize:4
		t301xNamaUserFU2 blank:false, nullable:true, maxSize:50
		t301Ket blank:false, nullable:true, maxSize:50
		t301staDariMRS blank:false, nullable:true, maxSize:1
		t301staBookingWalkIn blank:false, nullable:true, maxSize:1
		t301staBookingTelp blank:false, nullable:true, maxSize:1
		t301staBookingWeb blank:false, nullable:true, maxSize:1
		t301NoDokumenEstimasi blank:false, nullable:true, maxSize:20
		t301TglDokumenEstimasi blank:false, nullable:true, maxSize:4
		t301TkKerusakan blank:false, nullable:true, maxSize:1
		tipeKerusakan blank:false, nullable:true, maxSize:1
		t301StaTPSLine blank:false, nullable:true, maxSize:1
		colorMatchingKlasifikasi blank:false, nullable:true, maxSize:4
		colorMatchingJmlPanel blank:false, nullable:true, maxSize:4
		t301Proses1 blank:false, nullable:true, maxSize:8
		t301Proses2 blank:false, nullable:true, maxSize:8
		t301Proses3 blank:false, nullable:true, maxSize:8
		t301Proses4 blank:false, nullable:true, maxSize:8
		t301Proses5 blank:false, nullable:true, maxSize:8
		t301TotalProses blank:false, nullable:true, maxSize:8
		t301staCustomerTunggu blank:false, nullable:true, maxSize:1
		t301xNamaUser blank:false, nullable:true, maxSize:20
		t301xNamaDivisi blank:false, nullable:true, maxSize:20
		staDel blank:false, nullable:true, maxSize:1
        staSave blank:true, nullable:true, maxSize:1
        staUsed blank:true, nullable:true, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

        jamApp blank: true, nullable: true
        menitApp blank: true, nullable: true
        saveFlag blank : true , nullable: true
    }
	
	static mapping = {
        autoTimestamp false
        table 'T301_APPOINTMENT'
		reception column:'T301_T401_NoWO'
        asb column: 'T301_T351_ASB'
        jpb column: 'T301_T451_JPB'
		customerVehicle column:'T301_VinCode'
		historyCustomerVehicle column:'T301_T183_ID'
		customer column:'T301_T102_ID'
		historyCustomer column:'T301_T182_ID'
		t301StaKategoriCustUmumSPKAsuransi column:'T301_StaKtgrCustUmumSPKAsrnsi', sqlType:'char(1)'
		sPK column:'T301_T191_IDSPK'
		sPKAsuransi column:'T301_T193_SPKAsuransi'
		t301TglJamApp column:'T301_TglJamApp', sqlType:'TIMESTAMP'
		jenisApp column:'T301_M301_ID'
		t301KategoriCust column:'T301_KategoriCust', sqlType:'char(1)'
		t301StaFir column:'T301_StaFir',sqlType:'char(1)'
		t301StaTHS column:'T301_StaTHS',sqlType:'char(1)'
		t301AlamatTHS column:'T301_AlamatTHS',sqlType:'varchar(255)'
		t301KmSaatIni column:'T301_KMSaatIni'
		t301NamaSA column:'T301_NamaSA', sqlType:'varchar(50)'
		t301Permintaan column:'T301_Permintaan'//, sqlType:'text'
		companyDealerIDSumber column:'T301_M011_ID_Sumber'
		companyDealerIDTujuan column:'T301_M011_ID_Tujuan'
        t301TglJamRencana column:'T301_TglJamRencana', sqlType:'TIMESTAMP'
		t301TglJamJanjiBayarDP column:'T301_TglJamJanjiBayarDP', sqlType:'TIMESTAMP'
		t301TglJamPenyerahann column:'T301_TglJamPenyerahan', sqlType:'TIMESTAMP'
		t301StaOkCancelReSchedule column:'T301_staOkCancelReSchedule', sqlType:'char(1)'
		t301TglJamPenyerahanSchedule column:'T301_TglJamPnyrhnReSchedule', sqlType:'TIMESTAMP'
		t301AlasanReSchedule column:'T301_AlasanReSchedule', sqlType:'varchar(50)'
		t301SubTotalRp column:'T301_SubTotalRp'
		t301PersenPPN column:'T301_PersenPPN'
		t301PPNRp column:'T301_PPNRp'
		t301GrandTotalRp column:'T301_GrandTotalRp'
		t301DPRp column:'T301_DPRp'
		t301SisaRp column:'T301_SisaRp'
		alasanCancel column:'T301_M402_ID'
		t301staFU1 column:'T301_satFU1', sqlType:'char(1)'
		t301TglFU1 column:'T301_TglFU1', sqlType:'TIMESTAMP'
		t301xNamaUserFU1 column:'T301_xNamaUserFU1', sqlType:'varchar(50)'
		t301staFU2 column:'T301_staFU2', sqlType:'char(1)'
		t301TglFU2 column:'T301_TglFU2', sqlType:'TIMESTAMP'
		t301xNamaUserFU2 column:'T301_xNamaUserFU2', sqlType:'varchar(50)'
		t301Ket column:'T301_Ket', sqlType:'varchar(50)'
		t301staDariMRS column:'T301_staDariMRS', sqlType:'char(1)'
		t301staBookingWalkIn column:'T301_staBookingWwalkIn', sqlType:'char(1)'
		t301staBookingTelp column:'T301_staBookingTelp', sqlType:'char(1)'
		t301staBookingWeb column:'T301_staBookingWeb', sqlType:'char(1)'
		t301NoDokumenEstimasi column:'T301_NoDokumenEstimasi', sqlType:'char(20)'
		t301TglDokumenEstimasi column:'T301_TglDokumenEstimasi', sqlType:'TIMESTAMP'
		t301TkKerusakan column:'T301_TkKerusakan', sqlType:'char(1)'
		tipeKerusakan column:'T301_M401_ID'
		t301StaTPSLine column:'T301_StaTPSLine', sqlType:'char(1)'
		colorMatchingKlasifikasi column:'T301_M046_Klasifikasi'
		colorMatchingJmlPanel column:'T301_M046_JmlPanel'
		t301Proses1 column:'T301_Proses1'
		t301Proses2 column:'T301_Proses2'
		t301Proses3 column:'T301_Proses3'
		t301Proses4 column:'T301_Proses4'
		t301Proses5 column:'T301_Proses5'
		t301TotalProses column:'T301_TotalProses'
		t301staCustomerTunggu column:'T301_staCustomerTunggu', sqlType:'char(1)'
		t301xNamaUser column:'T301_xNamaUser', sqlType:'varchar(20)'
		t301xNamaDivisi column:'T301_xNamaDivisi', sqlType:'varchar(20)'
		staDel column:'T301_StaDel', sqlType:'char(1)'

        dateCreated column: 'T301_DateCreated'
        jamApp  column: 'T301_JamApp'
        menitApp column: 'T301_MeniApp'
        saveFlag column: 'T301_SaveFlag'
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "UPDATE"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "INSERT"
//		lastUpdated = new Date()
		staDel = "0"
		staUsed = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "INSERT"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}
}