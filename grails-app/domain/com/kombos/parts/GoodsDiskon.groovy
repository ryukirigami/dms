package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class GoodsDiskon {

	CompanyDealer companyDealer
    Group group
    Goods goods
	Date m171TglAwal
	Date m171TglAkhir
	Double m171PersenDiskon
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer nullable : true, blank : true, maxSize : 4,unique:false
		goods nullable : true, blank : true, maxSize : 14
        group nullable : true, blank : true, maxSize : 14
		m171TglAwal nullable : true, blank : true, unique:false
		m171TglAkhir nullable : true, blank : true
		m171PersenDiskon nullable : true, blank : true, maxSize : 8
        staDel nullable : true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'M171_GOODSDISKON'
		companyDealer column : 'M171_M011_ID'
		group column : 'M171_M181_ID'
        goods column : 'M171_M111_ID'
		m171TglAwal column : 'M171_TglAwal', sqlType : 'date'
		m171TglAkhir column : 'M171_TglAkhir', sqlType : 'date'
		m171PersenDiskon column : 'M171_PersenDiskon'
        staDel column : 'M171_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m171PersenDiskon
	}
}

