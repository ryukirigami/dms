package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.finance.AccountNumber

class CategorySlip {
    CompanyDealer companyDealer
    String categoryCode
    String name
    AccountNumber accountNumber
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer nullable: true, blank: true
        categoryCode maxSize: 12, nullable: false, blank: false
        name maxSize: 64 , nullable: false, blank: false
        accountNumber nullable: false, blank: false
        staDel blank: false, nullable: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "CATEGORY_SLIP"
        companyDealer column: "Company_ID"
        categoryCode column: "CATEGORY_CODE", sqlType: "VARCHAR(12)"
        name column: "NAME", sqlType: "VARCHAR(64)"
        accountNumber column: "ACCOUNT_NUMBER"
    }
}
