package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods
import com.kombos.parts.Location

class StokOPName {
    CompanyDealer companyDealer
	String t132ID
	Date t132Tanggal
	KlasifikasiGoods goods
	Double t132Jumlah1Stock
	Double t132Jumlah2Stock
	Double t132Jumlah11
	Double t132Jumlah21
	Double t132Jumlah12
	Double t132Jumlah22
	Double t132Jumlah13
	Double t132Jumlah23
	Location location
	String t132StaUpdateStock
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t132ID nullable : false, blank : false, maxSize : 20
		t132Tanggal nullable : true, blank : true
		goods nullable : true, blank : true
		t132Jumlah1Stock nullable : true, blank : true
		t132Jumlah2Stock nullable : true, blank : true
		t132Jumlah11 nullable : true, blank : true
		t132Jumlah21 nullable : true, blank : true
		t132Jumlah12 nullable : true, blank : true
		t132Jumlah22 nullable : true, blank : true
		t132Jumlah13 nullable : true, blank : true
		t132Jumlah23 nullable : true, blank : true
		location nullable : true, blank : true
		t132StaUpdateStock nullable : true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T132_STOKOPNAME'
		t132ID column : 'T132_ID', sqlType : 'char(20)'
		t132Tanggal column : 'T132_Tanggal'
		goods column : 'T132_M111_ID'
		t132Jumlah1Stock column : 'T132_Jumlah1Stock'
		t132Jumlah2Stock column : 'T132_Jumlah2Stock'
		t132Jumlah11 column : 'T132_Jumlah11'
		t132Jumlah21 column : 'T132_Jumlah21'
		t132Jumlah12 column : 'T132_Jumlah12'
		t132Jumlah22 column : 'T132_Jumlah22'
		t132Jumlah13 column : 'T132_Jumlah13'
		t132Jumlah23 column : 'T132_Jumlah23'
		location column : 'T132_M120_ID'
		t132StaUpdateStock column : 'T132_StaUpdateStock', sqlType : 'char(1)'
	}
}
