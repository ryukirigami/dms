package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class PartsStok {

	CompanyDealer companyDealer
	Date t131Tanggal
	Goods goods
	Double t131Qty1
	Double t131Qty2
	Double t131Qty1Free
	Double t131Qty2Free
	Double t131Qty1Reserved
	Double t131Qty2Reserved
	Double t131Qty1WIP
	Double t131Qty2WIP
	Double t131Qty1BlockStock
	Double t131Qty2BlockStock
	Double t131LandedCost
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer nullable : false, blank: false
		t131Tanggal nullable : false, blank: false
		goods nullable : true, blank: true
		t131Qty1 nullable : false, blank: false, maxSize : 8
		t131Qty2 nullable : false, blank: false, maxSize : 8
		t131Qty1Free nullable : false, blank: false, maxSize : 8
		t131Qty2Free nullable : false, blank: false, maxSize : 8
		t131Qty1Reserved nullable : false, blank: false, maxSize : 8
		t131Qty2Reserved nullable : false, blank: false, maxSize : 8
		t131Qty1WIP nullable : false, blank: false, maxSize : 8
		t131Qty2WIP nullable : false, blank: false, maxSize : 8
		t131Qty1BlockStock nullable : false, blank: false, maxSize : 8
		t131Qty2BlockStock nullable : false, blank: false, maxSize : 8
		t131LandedCost nullable : false, blank: false, maxSize : 8
		staDel nullable : false,maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T131_PARTSSTOK'
		companyDealer column : 'T131_M011_ID'
		t131Tanggal column : 'T131_Tanggal', sqlType : 'date'
		goods column : 'T131_M111_ID'
		t131Qty1 column : 'T131_Qty1'
		t131Qty2 column : 'T131_Qty2'
		t131Qty1Free column : 'T131_Qty1Free'
		t131Qty2Free column : 'T131_Qty2Free'
		t131Qty1Reserved column : 'T131_Qty1Reserved'
		t131Qty2Reserved column : 'T131_Qty2Reserved'
		t131Qty1WIP column : 'T131_Qty1WIP'
		t131Qty2WIP column : 'T131_Qty2WIP'
		t131Qty1BlockStock column : 'T131_Qty1BlockStock'
		t131Qty2BlockStock column : 'T131_Qty2BlockStock'
		t131LandedCost column : 'T131_LandedCost'
		staDel column : 'T131_StaDel', sqlType : 'char(1)'
	} 
	
}
