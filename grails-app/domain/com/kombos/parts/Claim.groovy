package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.GoodsReceive
import com.kombos.parts.Invoice


class Claim {
    CompanyDealer companyDealer
	String t171ID
	Date t171TglJamClaim
	String t171PetugasClaim
	Vendor vendor
	Invoice invoice
    GoodsReceive goodsReceive
	Goods goods
	Double t171Qty1Issued
	Double t171Qty2Issued

	Double t171Qty1Reff
	Double t171Qty2Reff
	Double t171Qty1Rusak
	Double t171Qty2Rusak
	Double t171Qty1Salah
	Double t171Qty2Salah
	String t171Keterangan
	String t171xNamaDivisi
	String t171xNamaUser
	String staDel
    String status
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t171ID nullable : false, blank:false, maxSize : 20, unique : false
		t171TglJamClaim nullable : true, blank:true
		t171PetugasClaim nullable : true, blank:false, maxSize : 50
		vendor nullable : true, blank:true
        invoice  nullable : true, blank:true
        goodsReceive  nullable : true, blank:true
		goods nullable : true, blank:true, unique : false
		t171Qty1Issued nullable : true, blank:true
		t171Qty2Issued nullable : true, blank:true

		t171Qty1Reff nullable : true, blank:true
		t171Qty2Reff nullable : true, blank:true
		t171Qty1Rusak nullable : true, blank:false
		t171Qty2Rusak nullable : true, blank:true
		t171Qty1Salah nullable : true, blank:true
		t171Qty2Salah nullable : true, blank:true
		t171Keterangan nullable : true, blank:true, maxSize : 50
		t171xNamaUser nullable : true, blank:true,maxSize : 20
		t171xNamaDivisi nullable : true, blank:true,maxSize : 20
		staDel nullable : true, blank:true, maxSize : 1
        status nullable : true, blank:true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T171_CLAIM'
		t171ID column : 'T171_ID', sqlType : 'char(20)'
		t171TglJamClaim column : 'T171_TglJamClaim', sqlType : 'date'
		t171PetugasClaim column : 'T171_PetugasClaim', sqlType : 'varchar(50)'
		vendor column : 'T171_M121_ID'
        invoice column : 'T171_T169_ID'
		goods column : 'T162_M111_ID' //T171_T169_T168_T167_T164_T163_
		t171Qty1Issued column : 'T171_Qty1Issued'
		t171Qty2Issued column : 'T171_Qty2Issued'

		t171Qty1Reff column : 'T171_Qty1Reff'
		t171Qty2Reff column : 'T171_Qty2Reff'
		t171Qty1Rusak column : 'T171_Qty1Rusak'
		t171Qty2Rusak column : 'T171_Qty2Rusak'
		t171Qty1Salah column : 'T171_Qty1Salah'
		t171Qty2Salah column : 'T171_Qty2Salah'
		t171Keterangan column : 'T171_Keterangan', sqlType : 'varchar(50)'
		t171xNamaUser column : 'T171_xNamaUser', sqlType : 'varchar(20)'
		t171xNamaDivisi column : 'T171_xNamaDivisi', sqlType : 'varchar(200)'
		staDel column : 'T171_StaDel', sqlType : 'char(1)'
	}
}
