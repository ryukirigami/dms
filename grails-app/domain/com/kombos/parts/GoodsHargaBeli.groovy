package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Kategori
import com.kombos.parts.Goods

import java.util.Date;

class GoodsHargaBeli {
    CompanyDealer companyDealer
	Date  t150TMT
	Double t150Harga
    Goods goods
	Vendor vendor
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer nullable: false, blank:true
		t150Harga nullable : true, blank : true
		goods nullable:false, blank : false
		vendor nullable:true, blank : false
        staDel nullable : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T150_GOODSHARGABELI'
		companyDealer column : 'T150_M011_ID'
		goods column : 'T162_M111_ID'
		t150Harga column : 'T150_Harga'
        staDel column : 'T150_StaDel', sqlType : 'char(1)'
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "update"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
		lastUpdProcess = "insert"
		staDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}
}