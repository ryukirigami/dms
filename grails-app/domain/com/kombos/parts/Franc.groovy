package com.kombos.parts

import com.kombos.finance.AccountNumber

class Franc {
	String m117ID
	String m117NamaFranc
	String m117Ket
    AccountNumber m117NomorAkun
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m117ID blank:false, nullable:false, maxSize:4
		m117NamaFranc blank:false, nullable:false, maxSize:50
		m117Ket blank:true, nullable:true, maxSize:1000
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M117_FRANC"
		m117ID column:"M117_ID", sqlType:"varchar(4)"
		m117NamaFranc column:"M117_NamaFranc", sqlType:"varchar(50)"
		m117Ket column:"M117_Ket", sqlType:"varchar(1000)"
        m117NomorAkun column: "M117_NomorAkun"
	}
	
	String toString() {
		return m117NamaFranc
	}
}
