package com.kombos.parts

import com.kombos.administrasi.CompanyDealer


class GroupDiskon {

	CompanyDealer companyDealer
	Group group
	Date m172TglAwal
	Date m172TglAkhir
	Double m172PersenDiskon
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer nullable : true, blank : true, maxSize : 4,unique : false
		group nullable : true, blank : true, maxSize : 2,unique : false
		m172TglAwal nullable : true, blank : true,unique : false
		m172TglAkhir nullable : true, blank : true
		m172PersenDiskon nullable : true, blank : true, maxSize : 8
        staDel nullable : true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'M172_GROUPSDISKON'
		companyDealer column : 'M172_M011_ID'
		group column : 'M172_M181_ID'
		m172TglAwal column : 'M172_TglAwal',  sqlType : 'date'
		m172TglAkhir column : 'M172_TglAkhir', sqlType : 'date'
		m172PersenDiskon column : 'M121_PersenDiskon'
        staDel column : 'M121_StaDel', sqlType : 'char(1)'
	}
}
