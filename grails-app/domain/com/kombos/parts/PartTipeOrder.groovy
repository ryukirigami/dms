package com.kombos.parts

import java.sql.Time

class PartTipeOrder {
	String m112ID
	Date m112TglBerlaku
	Vendor vendor
	String m112TipeOrder
	Integer m112JamLamaDatang1
	Integer m112JamLamaDatang2
	Time m112StaCutOffTime
	Time m112CutoffTime1
	Time m112CutoffTime2
	Time m112CutoffTime3
	Time m112CutoffTime4
	Time m112CutoffTime5
	Time m112CutoffTime6
	Time m112CutoffTime7
	Time m112CutoffTime8
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m112TglBerlaku blank:false, nullable:false
		vendor blank:false, nullable:false
		m112TipeOrder blank:false, nullable:false
		m112JamLamaDatang1 blank:false, nullable:false
		m112JamLamaDatang2 blank:true, nullable:true
		m112CutoffTime1 blank:true, nullable:true
		m112CutoffTime2 blank:true, nullable:true
		m112CutoffTime3 blank:true, nullable:true
		m112CutoffTime4 blank:true, nullable:true
		m112CutoffTime5 blank:true, nullable:true
		m112CutoffTime6 blank:true, nullable:true
		m112CutoffTime7 blank:true, nullable:true
		m112CutoffTime8 blank:true, nullable:true
		staDel blank:false, nullable:false, maxSize:1
        m112ID blank:true, nullable:true, maxSize:6
        m112StaCutOffTime blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table "M112_PARTTIPEORDER"
		m112ID column:"M112_ID", sqlType:"char(6)"
		m112TglBerlaku column:"M112_TglBerlaku", sqlType:"date"
		vendor column:"M112_M121_ID"
		m112TipeOrder column:"M112_TipeOrder", sqlType:"varchar(50)"
		m112JamLamaDatang1 column:"M112_JamLamaDatang1", sqlType:"int"
		m112JamLamaDatang2 column:"M112_JamLamaDatang2", sqlType:"int"
		m112StaCutOffTime column:"M122_StaCutOffTime", sqlType:"char(8)"
		m112CutoffTime1 column:"M112_CutOffTime1"
		m112CutoffTime2 column:"M112_CutOffTime2"
		m112CutoffTime3 column:"M112_CutOffTime3"
		m112CutoffTime4 column:"M112_CutOffTime4"
		m112CutoffTime5 column:"M112_CutOffTime5"
		m112CutoffTime6 column:"M112_CutOffTime6"
		m112CutoffTime7 column:"M112_CutOffTime7"
		m112CutoffTime8 column:"M112_CutOffTime8"
		staDel column:"M112_StaDel", sqlType:"char(1)"
	}

	String toString() {
		return m112TipeOrder
	}
}
