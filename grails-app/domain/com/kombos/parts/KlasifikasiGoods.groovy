package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Franc
import com.kombos.parts.Goods
import com.kombos.parts.Group
import com.kombos.parts.Location
import com.kombos.parts.ParameterICC
import com.kombos.parts.SCC

class KlasifikasiGoods {

	CompanyDealer companyDealer
	Goods goods
	Group group
	Franc franc
	SCC scc
	Location location
	ParameterICC parameterICC
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


	static constraints = {
		companyDealer nullable:true, blank:true
		goods nullable:false, blank:false//, unique:true
		group nullable:false, blank:false
		franc nullable:false, blank:false
		scc nullable:false, blank:false
		location nullable:false, blank:false
		parameterICC nullable:true, blank:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table "T161_KLASIFIKASIGOODS"
		companyDealer column : 'T161_M011_ID'
		goods column : 'T161_M111_ID'
		group column : 'T161_M181_ID'
		franc column : 'T161_M117_ID'
		scc column : 'T161_M113_ID'
		location column : 'T161_M120_M155_ID'
		parameterICC column : 'T161_M120_ID'
	}
}
