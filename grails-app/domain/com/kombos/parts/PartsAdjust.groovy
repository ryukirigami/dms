package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import org.apache.shiro.SecurityUtils

import java.text.SimpleDateFormat

class PartsAdjust {
    CompanyDealer companyDealer
    String t145ID
    Date t145TglAdj = new Date()
    String t145PetugasAdj
    String t145StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        companyDealer (blank:true, nullable:true)
        t145ID nullable: true, blank: false, maxSize: 20, unique: true
        t145TglAdj nullable: true, blank: false
        t145PetugasAdj nullable: true, blank: false, maxSize: 50, display: false
        t145StaDel nullable: true, blank: false, maxSize: 1, display: false
        createdBy nullable: true, display: false //Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true, display: false //Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true, display: false //Baris ini, terakhir prosess apa ?? insert, update, delete

    }

    static mapping = {
        autoTimestamp false
        table 'T145_PARTSADJUST'
        t145ID column: 'T145_ID', sqlType: 'char(20)'
        t145TglAdj column: 'T145_TglAdj'//, sqlType: 'date'
        t145PetugasAdj column: 'T145_PetugasAdj', sqlType: 'varchar(50)'
        t145StaDel column: 'T145_StaDel', sqlType: 'char(1)'
    }
    static SimpleDateFormat codeFormat = new SimpleDateFormat("yyyyMMdd")

    static String generateCode() {
        String date = codeFormat.format(new Date())
        Integer noUrut = PartsAdjust.countByT145IDIlike("$date%")
        String kode = "000000" + noUrut
        kode = kode.substring(kode.length() - 4)
        return date + kode
    }

    def beforeUpdate = {
        lastUpdated = new Date();
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        lastUpdProcess = "UPDATE"
    }

    def beforeInsert = {
        dateCreated = new Date()
        lastUpdated = new Date()
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        createdBy = SecurityUtils?.subject?.principal?.toString()
        //t145ID = generateCode()
        lastUpdProcess = "INSERT"
        t145StaDel = "0"
    }
}
