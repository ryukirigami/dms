package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import oracle.sql.TIMESTAMP

import java.text.SimpleDateFormat

class GoodsReceive {
    CompanyDealer companyDealer
	String t167ID
	Date t167TglJamReceive
	String t167PetugasReceive
	String t167NoReff
	Date t167TglReff
    Vendor vendor
    CompanyDealer companyDealerSupply
    static hasMany = [goodsReceiveDetail : GoodsReceiveDetail]
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    // Journal Pembelian Parts Bahan
    String t167TipeBayar
    Integer t167Ppn

	static constraints = {
        companyDealer (blank:true, nullable:true)
        vendor (blank:true, nullable:true)
        companyDealerSupply (blank:true, nullable:true)
		t167ID nullable : false, blank:false, maxSize : 200, unique : true
		t167TglJamReceive nullable : false, blank:false
		t167PetugasReceive nullable : false, blank:false, maxSize : 50
		t167NoReff  nullable : false, blank:false, maxSize : 255
		t167TglReff nullable : false, blank:false
        t167Ppn nullable: true
        t167TipeBayar nullable: true
		staDel nullable : false, blank:false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
		table 'T167_GOODSRECEIVE'
		t167ID column : 'T167_ID', sqlType : 'char(20)'
		t167TglJamReceive column : 'T167_TglJamReceive', sqlType : 'TIMESTAMP'
		t167PetugasReceive column : 'T167_PetugasReceive', sqlType : 'varchar(50)'
		t167NoReff column : 'T167_NoReff', sqlType : 'varchar(50)'
		t167TglReff column : 'T167_TglReff', sqlType : 'date'
        t167TipeBayar column: 'T167_TipeBayar', sqlType: 'varchar(50)'
        t167Ppn column: 'T167_Ppn', sqlType: 'int'
		staDel column : 'T167_StaDel', sqlType : 'char(1)'
	}

    static SimpleDateFormat codeFormat = new SimpleDateFormat("yyyyMMddhhmmss")

    static String generateCode() {
        String date = codeFormat.format(new Date())
        return date
    }

}