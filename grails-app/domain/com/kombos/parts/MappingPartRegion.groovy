package com.kombos.parts

import com.kombos.administrasi.Region

class MappingPartRegion {
    Region region
    Long version
    Double percent
    static mapping = {
        table 'MAPPINGPARTREGION'
        region column : 'M11A_ID'
        percent column : 'PERCENT_HARGA'
        version column: 'VERSION'
    }

    static constraints = {
        region (nullable : true, blank : true)
        percent (nullable : true, blank : true)
        version nullable: true, blank : true
    }
}
