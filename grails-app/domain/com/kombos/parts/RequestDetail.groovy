package com.kombos.parts

import java.util.Date;

class RequestDetail{
    //static belongsTo = [request:Request]
    Goods goods
    Double t162Qty1
    Double t162Qty2
    Request request
	static belongsTo = Request
	RequestStatus status
    Double t162Qty1Available
    Double t162Qty2Available
	
	static hasOne = [validasiOrder : ValidasiOrder]
	
//	Double totalHarga
	
	StatusApproval staApproval
	Date tglApproveUnApprove
	String alasanUnApprove
	String namaUserApproveUnApprove

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        goods nullable : true, blank : true
        t162Qty1 nullable : true, blank : true
        t162Qty2 nullable : true, blank : true
        t162Qty1Available nullable : true, blank : true
        t162Qty2Available nullable : true, blank : true
		status nullable : true, blank : true, maxSize : 1
		staApproval nullable : true, blank : false, maxSize : 1
		tglApproveUnApprove nullable : true, blank : false
		alasanUnApprove nullable : true, blank : false, maxSize : 50
		namaUserApproveUnApprove nullable : true, blank : false, maxSize : 20
		validasiOrder nullable:true, unique:false
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T162_REQUEST_DETAIL'
        goods column : 'T162_M111_ID'
        t162Qty1 column : 'T162_Qty1'
        t162Qty2 column : 'T162_Qty2'
        t162Qty1Available column : 'T162_Qty1Available'
        t162Qty2Available column : 'T162_Qty2Available'
		status column : 'T162_Status'//, enumType: 'ordinal'
		staApproval column : 'T162_StaApproval', sqlType : 'char(1)'
		tglApproveUnApprove column : 'T162_TglApproveUnApprove', sqlType : 'date'
		alasanUnApprove column : 'T162_AlasanUnApprove', sqlType : 'varchar(50)'
		namaUserApproveUnApprove column : 'T162_NamaUserApproveUnApprove', sqlType : 'varchar(20)'
//		totalHarga formula: "coalesce((SELECT hb.T150_Harga FROM T150_GOODSHARGABELI hb WHERE T162_M111_ID=hb.T162_M111_ID AND hb.T150_StaDel='0'),0) * coalesce(T162_Qty1,0)"
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
		if(!status)
			status = RequestStatus.BELUM_REQUEST_BOOKING_FEE
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }

}