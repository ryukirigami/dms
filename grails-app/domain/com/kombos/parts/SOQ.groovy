package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods

class SOQ {
	CompanyDealer companyDealer
	Date t156Tanggal
	Goods goods
	Double t156Qty1
	Double t156Qty2
    String statusParts
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer  nullable:true, blank: true, unique: false
		t156Tanggal nullable:true, blank:true, unique:false
		goods nullable:true, blank: true, unique: true
        statusParts nullable:true, blank: true
		t156Qty1 nullable:true, blank: true, maxSize : 8
		t156Qty2 nullable:true, blank: true, maxSize : 8
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T156_SOQ'
		companyDealer column : 'T156_M011_ID'
		t156Tanggal column : 'T156_Tanggal', sqlType : 'date'
		goods column : 'T156_M111_ID'
        statusParts column: 'T156_STATUS_PARTS',sqlType : 'varchar(2)'
		t156Qty1 column : 'T156_Qty1'
		t156Qty2 column : 'T156_Qty2'
	}
    String toString(){
        return id
    }
}

