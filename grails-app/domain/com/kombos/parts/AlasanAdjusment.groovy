package com.kombos.parts

class AlasanAdjusment {
	
	Integer m165ID
	String m165AlasanAdjusment
    Date dateCreated = new Date() // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated = new Date()  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "INSERT" //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m165ID nullable: false, blank: false
		m165AlasanAdjusment blank:false , nullable : false , maxSize : 510
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : false//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M165_ALASANADJUSTMENT'
		m165ID column : 'M165_ID', sqlType:'int'
		m165AlasanAdjusment column : 'M165_AlasanAdjustment', sqlType : 'varchar(510)'
	}
	
	String toString(){
		return m165AlasanAdjusment
	}
}
