package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class MIP {

	CompanyDealer companyDealer
	Integer m160ID
	Date m160TglBerlaku
	Double m160RangeDAD
	Double m160OrderCycle
	Double m160LeadTime
	Double m160SSLeadTime
	Double m160SSDemand
	Double m160DeadStock
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		m160TglBerlaku nullable : false, blank : false
		m160RangeDAD nullable : false, blank : false
		m160OrderCycle nullable : false, blank : false
		m160LeadTime nullable : false, blank : false
		m160SSLeadTime nullable : false, blank : false
		m160SSDemand nullable : false, blank : false
		m160DeadStock nullable : false,blank : false
        m160ID nullable : true, blank : true, maxSize :4
        companyDealer nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M160_MIP'
		m160ID column : 'M160_ID', sqlType :'int'
		companyDealer column : 'M160_M011_ID'
		m160TglBerlaku column :'M160_TglBerlaku', sqlType :'date'
		m160RangeDAD column :'M160_RangeDad'
		m160OrderCycle column :'M160_OrderCycle'
		m160LeadTime column :'M160_LeadTime'
		m160SSLeadTime column :'M160_SSLeadTime'
		m160SSDemand column :'M160_SSDemand'
		m160DeadStock column :'M160_DeadStock'
	}
}
