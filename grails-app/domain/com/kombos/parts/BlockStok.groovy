package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.Reception

class BlockStok {
    CompanyDealer companyDealer
	String t157ID
	Date t157TglBlockStok
	Reception reception
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t157TglBlockStok nullable : true, blank : true
		reception nullable : true, blank : true
		staDel nullable : true, blank : true
        dateCreated nullable : true
        lastUpdated nullable : true
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
        t157ID nullable : true, blank : true
    }
	
	static mapping = {
	    autoTimestamp false
        table 'T157_BLOCKSTOK'
		t157ID column : 'T157_ID', sqlType : 'char(20)'
		t157TglBlockStok column : 'T157_TglBlokStok', sqlType : 'date'
		reception column : 'T157_T401_NoWO'
		staDel column : 'T157_StaDel', sqlType : 'char(1)'
	}
}
