package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class SupplySlipOwn {

    CompanyDealer companyDealer
    String ssNumber
    CategorySlip categorySlip
    Date ssDate
    Double subTotalSP
    Double subTotalMaterial
    Double total
    String staApprove
    String keterangan
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        ssNumber blank: false, nullable: false, maxSize: 22
        categorySlip nullable: false, blank : false
        keterangan nullable: true, blank: true, maxSize: 144
        ssDate nullable: true, blank: true
        subTotalSP nullable: true, blank: true
        subTotalMaterial nullable: true, blank: true
        total nullable: true, blank: true
        staApprove nullable: false, blank: false, maxSize: 1
        staDel blank: false, nullable: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "SUPPLYSLIPOWN"
        ssNumber column: "SSNUMBER", sqlType: "varchar(22)"
        categorySlip column: "CATEGORYSLIP_ID"
        keterangan column: "keterangan", sqlType: "varchar(144)"
        ssDate column: "SSDATE"
        subTotalSP column: "SubtotalSP"
        subTotalMaterial column: "SubtotalMaterial"
        total column: "total"
        staApprove column: "sta_approve", sqlType: "char(1)"
    }
}
