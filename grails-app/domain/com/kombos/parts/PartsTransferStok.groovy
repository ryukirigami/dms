package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class PartsTransferStok {
    CompanyDealer companyDealer
    CompanyDealer companyDealerTujuan
    Goods goods
    Date tglTransferStok
    Double qtyParts
    String nomorPO
    String noPengiriman
    String cekKetersediaan   // 1 = ada, 0 = tidak Ada
    String cekBaca   // 1 = ada, 0 = tidak Ada
    String staSudahInput   // 1 = ada, 0 = tidak Ada
    String staApprove
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static constraints = {
        companyDealer (blank:true, nullable:true)
        companyDealerTujuan (blank:true, nullable:true)
        goods nullable : true, blank:true, unique : false
        tglTransferStok nullable : true, blank:true
        qtyParts nullable : true, blank:true
        nomorPO nullable : true, blank:true
        noPengiriman nullable : true, blank:true
        staApprove nullable : true, blank:true, maxSize : 1
        staDel nullable : true, blank:true, maxSize : 1
        staSudahInput nullable : true, blank:true, maxSize : 1
        cekKetersediaan nullable : true, blank:true, maxSize : 1
        cekBaca nullable : true, blank:true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    static mapping = {
        autoTimestamp false
        table 'T_PARTS_TRANSFER_STOK'
        tglTransferStok sqlType : 'date'
        nomorPO  sqlType : 'varchar(50)'
        noPengiriman  sqlType : 'varchar(50)'
        staApprove sqlType: "char(1)"
        staDel sqlType : 'char(1)'
        cekKetersediaan sqlType : 'char(1)'
        cekBaca sqlType : 'char(1)'
        staSudahInput sqlType : 'char(1)'
    }
}
