package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class UmurBinning {

	CompanyDealer companyDealer
	Date m163TglBerlaku
	Integer m163BatasWaktuBinning
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {

		m163TglBerlaku nullable : false, blank : false
		m163BatasWaktuBinning nullable : false, blank : false, maxSize :4
        companyDealer nullable : true, blank : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M163_UMURBINNING'
		companyDealer column : 'M163_M011_ID'
		m163TglBerlaku column : 'M163_TglBerlaku', sqlType : 'date'
		m163BatasWaktuBinning column : 'M163_BatasWaktuBinning', sqlType : 'int'
	}
}
