package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class PengembalianDP {
	
	CompanyDealer companyDealer
	Date m166TglBerlaku
	Double m166PersenKembaliDP
	Double m166MaxKembaliDiKasir
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		m166TglBerlaku nullable : false, blank : false
		m166PersenKembaliDP nullable : false, blank : false, maxSize : 8
		m166MaxKembaliDiKasir nullable : false, blank : false, maxSize : 8
        companyDealer nullable : true, blank : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M166_PENGEMBALIANDP'
		companyDealer column : 'M166_M011_ID'
		m166TglBerlaku column : 'M166_TglBerlaku', sqlType : 'date'
		m166PersenKembaliDP column : 'M166_PersenKembaliDP'
		m166MaxKembaliDiKasir column : 'M166_MaxKembaliDiKasir'
	}
}
