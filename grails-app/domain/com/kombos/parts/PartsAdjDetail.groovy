package com.kombos.parts

import org.apache.shiro.SecurityUtils

import java.text.SimpleDateFormat

class PartsAdjDetail {

	PartsAdjust partsAdjust
	Goods goods
	Double t146JmlAwal1
	Double t146JmlAkhir1
	Double t146JmlAwal2
	Double t146JmlAkhir2
    StatusApproval statusApprove
    String t146AlasanUnApprove
	AlasanAdjusment alasanAdjusment
	String t146StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		partsAdjust nullable : false, blank: true
		goods nullable : false, blank: true
		t146JmlAwal1 nullable : true, blank: true, maxSize :8
		t146JmlAkhir1 nullable : true, blank: true, maxSize :8
		t146JmlAwal2 nullable : true, blank: true, maxSize :8
		t146JmlAkhir2 nullable : true, blank: true, maxSize :8
		alasanAdjusment nullable : true, blank: true
        t146AlasanUnApprove nullable : true, blank: true
		t146StaDel nullable : true, blank: true, maxSize :1
        statusApprove nullable : true, blank: true, maxSize :1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T146_PARTSADJDETAIL'
		partsAdjust column : 'T146_T145_ID'
		goods column : 'T146_M111_ID'
		t146JmlAwal1 column : 'T146_JmlAwal1'
		t146JmlAkhir1 column : 'T146_JmlAkhir1'
		t146JmlAwal2 column : 'T146_JmlAwal2'
		t146JmlAkhir2 column : 'T146_JmlAkhir2'
		alasanAdjusment column : 'T146_M165_ID'
        t146AlasanUnApprove column : 'T146_AlasanUnApprove'
		t146StaDel column : 'T146_StaDel', sqlType :'char(1)'
        statusApprove column : 'StatusApprove', sqlType :'char(1)'
	}


    def beforeUpdate = {
        lastUpdated = new Date();
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        lastUpdProcess = "UPDATE"
    }

    def beforeInsert = {
        dateCreated = new Date()
        lastUpdated = new Date()
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        createdBy = SecurityUtils?.subject?.principal?.toString()
        lastUpdProcess = "INSERT"
        t146StaDel = "0"
        statusApprove = StatusApproval.WAIT_FOR_APPROVAL
        t146AlasanUnApprove = ""
    }
}
