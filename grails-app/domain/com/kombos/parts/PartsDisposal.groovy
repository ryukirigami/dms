package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import org.apache.shiro.SecurityUtils

import java.text.SimpleDateFormat

class PartsDisposal {
    CompanyDealer companyDealer
    String t147ID
    Date t147TglDisp = new Date()
    String t147NamaReff
    String t147NoReff
    String t147Ket
    String t147xNamaUser
    String t147xNamaDivisi
    String t147StaDel = "0"
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy = new Date() //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy = new Date() //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "INSERT"  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        t147ID nullable: true, blank: true, maxSize: 20, unique: true
        t147TglDisp nullable: true, blank: true
        t147NamaReff nullable: true, blank: true, maxSize: 20, display: false
        t147NoReff nullable: true, blank: true, maxSize: 20, display: false
        t147Ket nullable: true, blank: true, maxSize: 20, display: false
        t147xNamaUser nullable: true, blank: true, maxSize: 20
        t147xNamaDivisi nullable: true, blank: true, maxSize: 20, display: false
        t147StaDel nullable: true, maxSize: 1, display: false
        createdBy nullable: true, display: false    //Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true, display: false   //Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true, display: false  //Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T147_PARTSDISPOSAL'
        t147ID column: 'T147_ID', sqlType: 'char(20)'
        t147TglDisp column: 'T147_TglDisp'//, sqlType: 'date'
        t147NamaReff column: 'T147_NamaReff', sqlType: 'varchar(20)'
        t147NoReff column: 'T147_NoReff', sqlType: 'varchar(20)'
        t147Ket column: 'T147_Ket', sqlType: 'varchar(50)'
        t147xNamaUser column: 'T147_xNamaUser', sqlType: 'varchar(20)'
        t147xNamaDivisi column: 'T147_xNamaDivisi', sqlType: 'varchar(20)'
        t147StaDel column: 'T147_StaDel', sqlType: 'char(1)'
    }

    static SimpleDateFormat codeFormat = new SimpleDateFormat("yyyyMMdd")

    static String generateCode() {
        String date = codeFormat.format(new Date())
        Integer noUrut = PartsDisposal.countByT147IDIlike("$date%")
        //println "noUrut = $noUrut date = $date"
        String kode = "000000" + noUrut
        kode = kode.substring(kode.length() - 4)
        return date + kode
    }

    String getT147ID(){
        if (t147ID == null || t147ID.trim().empty){
            t147ID = generateCode()
        }
        return t147ID
    }

    def beforeUpdate = {
        lastUpdated = new Date();
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        t147xNamaUser = SecurityUtils?.subject?.principal?.toString()
        lastUpdProcess = "UPDATE"
    }

    def beforeInsert = {
        dateCreated = new Date()
        lastUpdated = new Date()
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        createdBy = SecurityUtils?.subject?.principal?.toString()
        t147xNamaUser = SecurityUtils?.subject?.principal?.toString()
        t147ID = generateCode()
        lastUpdProcess = "INSERT"
        t147StaDel = "0"
    }
}
