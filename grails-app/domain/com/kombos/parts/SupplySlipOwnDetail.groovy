package com.kombos.parts

class SupplySlipOwnDetail {

    SupplySlipOwn supplySlipOwn
    Goods goods
    Double hargaSatuan
    Double qty
    Double discount
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        supplySlipOwn nullable: true,blank:true
        goods nullable: true,blank:true
        hargaSatuan nullable: true,blank:true
        qty nullable: true,blank : true
        discount nullable: true,blank : true
        staDel blank: false, nullable: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "SUPPLYSLIPOWN_DETAIL"
        supplySlipOwn column: "supplyslipown_id"
        goods column: "goods_id"
        hargaSatuan column: "hargasatuan"
    }
}
