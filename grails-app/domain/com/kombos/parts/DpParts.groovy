package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class DpParts {
    CompanyDealer companyDealer
    Integer m162ID
    Date m162TglBerlaku
    Long m162BatasNilaiKenaDP1
    Long m162BatasNilaiKenaDP2
    Double m162PersenDP
    Integer m162MaxHariBayarDP
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m162ID unique : true, nullable:true, blank : true, maxSize :4
        companyDealer nullable : true, blank : true
        m162TglBerlaku nullable:false, blank : false
        m162BatasNilaiKenaDP1 nullable:false, blank : false
        m162BatasNilaiKenaDP2 nullable:false, blank : false
        m162PersenDP nullable:false, blank : false
        m162MaxHariBayarDP nullable:false, blank : false, maxSize : 4
        staDel blank: false, nullable: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'M162_DPPARTS'
        companyDealer column : 'M162_M011_ID'
        m162ID column : 'M162_ID',sqlType : 'int'
        m162TglBerlaku column : 'M162_TglBerlaku', sqlType : 'date'
        m162BatasNilaiKenaDP1 column : 'M162_BatasNilaiKenaDP1'//, sqlType : 'numeric'
        m162BatasNilaiKenaDP2 column : 'M162_BatasNilaiKenaDP2'//, sqlType : 'numeric'
        m162PersenDP column : 'M162_PersenDP'//, sqlType: 'Double'
        m162MaxHariBayarDP column : 'M162_MaxHariBayarDP', sqlType:'int'
        staDel column : 'M162_StaDel', sqlType : 'char(1)'
    }
}