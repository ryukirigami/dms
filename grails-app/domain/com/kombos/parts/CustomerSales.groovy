package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class CustomerSales {

    CompanyDealer companyDealer
    String customerCode
    String nama
    String alamat
    String phoneNumber
    String kodePos
    String npwp
    String alamatNpwp
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess

    static constraints = {
        customerCode maxSize : 16, nullable : false, blank : false
        nama maxSize : 64, nullable : false, blank : false
        alamat maxSize : 128, nullable : false, blank : false
        phoneNumber maxSize : 32, nullable : true, blank : true
        kodePos maxSize : 6, nullable : true, blank : true
        npwp maxSize : 32, nullable : true, blank : true
        alamatNpwp maxSize : 128, nullable : true, blank : true
        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        companyDealer  nullable : true , blank : true
    }

    static mapping = {
        autoTimestamp false

    }
}
