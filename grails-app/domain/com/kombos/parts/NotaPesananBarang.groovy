package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FullModelCode
import com.kombos.administrasi.FullModelVinCode
import com.kombos.administrasi.KelompokNPB
import com.kombos.administrasi.PenegasanPengiriman
import com.kombos.baseapp.sec.shiro.User
import com.kombos.parts.StatusApproval

class NotaPesananBarang {
    CompanyDealer cabangPengirim
    CompanyDealer cabangTujuan
    CompanyDealer cabangDiteruskan
    CompanyDealer cabangJakarta
    String cabangDiteruskanSta
    KelompokNPB kelompokNPB
    String noNpb
    Date tglNpb
    User pemesan

    String namaPemilik
    String noPolisi
    String noRangka
    String noEngine
    FullModelCode tipeKendaraan
    Integer tahunPembuatan
    String noSpk
    Date tglSpk
    String keteranganTambahan //tambahan keterangan pesanan parts
    PenegasanPengiriman penegasanPengiriman
    String keteranganPenegasan //penegasan pesanan inventaris kantor

    StatusApproval staApproval
    Date tglApproveUnApprove
    String alasanUnApprove
    String namaUserApproveUnApprove

    String kabengApprovalSta = "0" // 0 belum diupdate, 1 sudah diapprove, 2 tutup NPB
    User kabengApprover
    String partsHAApprovalSta = "0" // 0 belum diupdate, 1 sudah diapprove, 2 tutup NPB
    User partsHA

    String partsHOApproveSta = "0"
    User partsHO
    String ptgsNPBJKTSta = "0"
    User ptgsNPBJKT
    String ptgsDOJKTSta = "0"
    User ptgsDOJKT

    Date tglHODiterima
    Date tglHOKeJKT
    Date tglJKTDiterima
    Date tglJKTProsesToko
    Date tglJKTBukaDO
    Date tglJKTKirimCabang
    Date tglCabangDiterima
    String keterangan

    String pathDokumenKabeng
    String pathDokumenPartsHA
    String pathDokumenHO
    String namaDokumenKabeng
    String namaDokumenPartsHA
    String namaDokumenHO

    byte[] fotoKabeng
    byte[] fotoHo
    String fotoImageMimeKabeng
    String fotoImageMimeHo

    User cabangPenerima
    String cabangPenerimaSta = "0"

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    //static hasMany = [notaPesananBarangs:NotaPesananBarang]

    static constraints = {
        cabangPengirim nullable : false, blank : false
        cabangTujuan nullable : false, blank : false
        cabangDiteruskan nullable : true, blank : true
        cabangJakarta nullable : true, blank : true
        kelompokNPB nullable : false, blank : false
        noNpb nullable : false, blank : false
        tglNpb nullable : false, blank : false
        pemesan nullable : false, blank : false

        namaPemilik nullable : false, blank : false, maxSize : 30
        noPolisi nullable : true, blank : true
        noRangka nullable : true, blank : true
        noEngine nullable : true, blank : true
        fotoKabeng nullable : true, blank : true
        fotoHo nullable : true, blank : true
        fotoImageMimeKabeng nullable : true, blank : true
        fotoImageMimeHo nullable : true, blank : true
        tipeKendaraan nullable : true, blank : true
        tahunPembuatan nullable : true, blank : true, maxSize : 4
        noSpk nullable : true, blank : true
        tglSpk nullable : true, blank : true
        keteranganTambahan nullable : true, blank : true //tambahan keterangan pesanan parts
        penegasanPengiriman nullable : true, blank : true // kirim pesawat : 0, kirim kapal laut : 1
        keteranganPenegasan nullable : true, blank : true //penegasan pesanan inventaris kantor

        staApproval nullable : true, blank : true, maxSize : 1
        tglApproveUnApprove nullable : true, blank : true
        alasanUnApprove nullable : true, blank : true, maxSize : 50
        namaUserApproveUnApprove nullable : true, blank : true, maxSize : 20

        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

        partsHOApproveSta nullable : true
        partsHO nullable : true
        ptgsNPBJKTSta nullable : true
        ptgsNPBJKT nullable : true
        ptgsDOJKTSta nullable : true
        ptgsDOJKT nullable : true

        tglHODiterima nullable : true
        tglHOKeJKT nullable : true
        tglJKTDiterima nullable : true
        tglJKTProsesToko nullable : true
        tglJKTBukaDO nullable : true
        tglJKTKirimCabang nullable : true
        tglCabangDiterima nullable : true
        keterangan nullable : true

        kabengApprovalSta nullable : true
        kabengApprover nullable : true

        partsHAApprovalSta nullable : true
        partsHA nullable : true

        cabangPenerima nullable : true
        cabangPenerimaSta nullable : true
        cabangDiteruskanSta nullable : true

        pathDokumenKabeng nullable : true, blank : true
        pathDokumenHO nullable : true, blank : true
        pathDokumenPartsHA nullable : true, blank : true

        namaDokumenHO nullable : true, blank : true
        namaDokumenKabeng nullable : true, blank : true
        namaDokumenPartsHA nullable : true, blank : true


    }

    static mapping = {
        autoTimestamp false
        table 'TBL_NPB'
        tglNpb sqlType : 'date'
        tglSpk sqlType : 'date'
        fotoKabeng sqlType : 'blob'
        fotoHo sqlType : 'blob'
        tglApproveUnApprove sqlType : 'date'
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}