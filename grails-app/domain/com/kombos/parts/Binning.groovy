package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class Binning {
    CompanyDealer companyDealer
    String t168ID
	Date t168TglJamBinning
	String t168PetugasBinning
    static hasMany = [binningDetail : BinningDetail]
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t168ID nullable : false, blank : false, unique : true, maxSize : 20
        t168TglJamBinning nullable : false, blank : false
		t168PetugasBinning nullable : false, blank : false, maxSize : 50
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T168_BINNING'
		t168ID column : 'T168_ID', sqlType :'char(20)'
        t168TglJamBinning column : 'T168_TglJamBinning', sqlType : 'TIMESTAMP'
		t168PetugasBinning column : 'T168_PetugasBinning', sqlType : 'varchar(50)'
		staDel column : 'T168_StaDel', sqlType : 'char(1)'
	}

}
