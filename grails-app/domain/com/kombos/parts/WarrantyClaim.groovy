package com.kombos.parts

import com.kombos.parts.Goods

class WarrantyClaim {

	//Integer m164ID
    Goods goods
    Date m164TglBerlaku
	Integer m164BulanWarranty
	Double m164KmWarranty
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
	//	m164ID unique : true,blank:false, nullable:false
		m164TglBerlaku blank:false, nullable:false 
		goods blank:false, nullable:false
		m164BulanWarranty blank:false, nullable:false, maxSize : 4
		m164KmWarranty blank:false, nullable:false, maxSize : 4
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M164_WARRANTYCLAIM'
	//	m164ID column : 'M164_ID', sqlType : 'int'
        goods column : 'M164_M111_ID'
        m164TglBerlaku column : 'M164_TglBerlaku', sqlType : 'date'
		m164BulanWarranty column : 'M164_BulanWarranty',sqlType : 'int'
		m164KmWarranty column :'M164_KmWarranty'
		
	}

    String toString(){
        return goods.toString() + " - Bulan : " + m164BulanWarranty + " - KM : " + m164KmWarranty
    }
}
