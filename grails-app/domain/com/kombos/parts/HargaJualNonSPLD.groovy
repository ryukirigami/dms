package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class HargaJualNonSPLD {

	CompanyDealer companyDealer
	Date t159TMT
	Double t159PersenKenaikanHarga
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //MenunjukkHaan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
    //	companyDealer nullable : false, blank: false, unique : true
		t159TMT nullable : false, blank: false //, unique : true
		t159PersenKenaikanHarga  nullable : false, blank: false, maxSize : 8
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T159_HARGAJUALNONSPLD'
//		companyDealer column : 'T159_M011_ID'
		t159TMT column : 'T159_TMT', sqlType : 'date'
		t159PersenKenaikanHarga column : 'T159_PersenKenaikanHarga'
	}
}
