package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Kategori
import com.kombos.parts.Goods

import java.util.Date;

class GoodsHargaJual {

    CompanyDealer companyDealer
	Kategori kategori
	Date  t151TMT
	Double t151HargaTanpaPPN
	Double t151HargaDenganPPN
	Satuan satuan
    Goods goods
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer nullable: false, blank:true
		kategori nullable : true, blank : true
		t151TMT nullable : false, blank : false
		t151HargaTanpaPPN nullable : false, blank : true
		t151HargaDenganPPN nullable : false, blank : true
		satuan nullable : true, blank : true
        goods nullable:false, blank : false
        staDel nullable : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T151_GOODSHARGAJUAL'
		companyDealer column : 'T151_M011_ID'
		kategori column : 'T151_M101_ID'
		t151TMT column : 'T151_TMT', sqlType :'date'
		t151HargaTanpaPPN column : 'T151_HargaTanpaPPN'
		t151HargaDenganPPN column : 'T151_HargaDenganPPN'
		satuan column : 'T151_StaSatuan', sqlType : 'int'
        staDel column : 'T151_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return t151TMT
	}
}
