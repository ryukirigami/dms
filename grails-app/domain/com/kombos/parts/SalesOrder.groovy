package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class SalesOrder {
    CompanyDealer companyDealer
    String sorNumber
    Date sorDate
    String customerCode
    String paymentType
    String deliveryLocation
    String note
    String owner
    String customerName
    String salesOrderStatus
    String deliveryAddress
    Double deliveryCost
    Integer creditDays
    Date dueDate
    Date deliveryDate
    Double ppn
    String printedBy
    Date printedDate
    String cancelNote
    Franc salesType
    String tradingFor
    String branchCode
    Integer isApprove
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [salesOrderDetail : SalesOrderDetail]

    static constraints = {
        sorNumber nullable : true, blank: true, maxSize: 50
        sorDate nullable : true, blank: true
        customerCode nullable : true, blank: true, maxSize: 16
        paymentType nullable : true, blank: true, maxSize: 16
        deliveryLocation nullable : true, blank: true, maxSize: 32
        note nullable : true, blank: true, maxSize: 128
        owner nullable : true, blank: true, maxSize: 16
        customerName nullable : true, blank: true, maxSize: 64
        salesOrderStatus nullable : true, blank: true, maxSize: 16
        deliveryAddress nullable : true, blank: true, maxSize: 64
        deliveryCost nullable : true, blank: true
        creditDays nullable : true, blank: true
        dueDate nullable : true, blank: true
        deliveryDate nullable : true, blank: true
        ppn nullable : true, blank: true
        printedBy nullable : true, blank: true, maxSize: 16
        printedDate nullable : true, blank: true
        cancelNote nullable : true, blank: true, maxSize: 128
        salesType nullable : true, blank: true, maxSize: 16
        companyDealer nullable: true, blanl: true
        tradingFor nullable : true, blank: true, maxSize: 16
        branchCode nullable : true, blank: true, maxSize: 2
        isApprove nullable : true, blank: true
        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }

    static mapping = {
        autoTimestamp false

    }

}
