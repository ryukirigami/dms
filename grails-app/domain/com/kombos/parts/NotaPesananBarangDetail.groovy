package com.kombos.parts

class NotaPesananBarangDetail {
    NotaPesananBarang notaPesananBarang
    Goods goods
    Integer jumlah
    Date hoTglDiterima
    Date hoTglDiteruskan
    String hoKeterangan
    Date ktrJktTglDiterima
    Date ktrJktTglDiproses
    Date ktrJktTglBukaDO
    String ktrJktKeterangan
    String nomorDO
    Date ktrJktTglKrmKeCabang
    Date cabangTglDiterima
    String cabangKeterangan

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        notaPesananBarang nullable : true, blank : true
        goods nullable : true, blank : true
        jumlah nullable : true, blank : true
        hoTglDiterima nullable : true, blank : true
        hoTglDiteruskan nullable : true, blank : true
        hoKeterangan nullable : true, blank : true
        ktrJktTglDiterima nullable : true, blank : true
        ktrJktTglDiproses nullable : true, blank : true
        ktrJktTglBukaDO nullable : true, blank : true
        ktrJktKeterangan nullable : true, blank : true
        nomorDO nullable : true, blank : true
        ktrJktTglKrmKeCabang nullable : true, blank : true
        cabangTglDiterima nullable : true, blank : true
        cabangKeterangan nullable : true, blank : true

        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'TBL_NPBDETAIL'
        hoTglDiterima sqlType : 'date'
        hoTglDiteruskan sqlType : 'date'
        ktrJktTglDiterima sqlType : 'date'
        ktrJktTglDiproses sqlType : 'date'
        ktrJktTglBukaDO sqlType : 'date'
        ktrJktTglKrmKeCabang sqlType : 'date'
        cabangTglDiterima sqlType : 'date'
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}