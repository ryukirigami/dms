package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class SalesRetur {
    String nomorRetur
    DeliveryOrder nomorDo
    SalesOrder sorNumber
    CompanyDealer companyDealer
    String note
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        nomorRetur nullable: true, blank : true, maxSize : 50
        nomorDo nullable: true, blank : true
        sorNumber nullable: true, blank : true
        note nullable: true, blank : true, maxSize: 255
        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false

    }
}
