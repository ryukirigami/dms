package com.kombos.parts

import org.apache.shiro.SecurityUtils

class PartsDisposalDetail {

	PartsDisposal partsDisposal
	Goods goods
	Double t148Jumlah1
	Double t148Jumlah2
	String t148StaDel
    StatusApproval staApproval
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		partsDisposal nullable:true, blank: true
		goods nullable:true, blank: true
		t148Jumlah1 nullable:true, blank: true, maxSize : 8
		t148Jumlah2 nullable:true, blank: true, maxSize : 8
		t148StaDel nullable:true, blank: true, maxSize : 1
        staApproval nullable:true, blank: true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T148_PARTSDISPOSALDETAIL'
		partsDisposal column : 'T148_T147_ID'
		goods column : 'T148_M111_ID'
		t148Jumlah1 column : 'T148_Jumlah1'
		t148Jumlah2 column : 'T148_Jumlah2'
		t148StaDel column : 'T148_StaDel', sqlType : 'char(1)'
        staApproval column : 'T148_StaApproval', sqlType : 'char(1)'
	}

    def beforeUpdate = {
        lastUpdated = new Date();
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        lastUpdProcess = "UPDATE"
    }

    def beforeInsert = {
        dateCreated = new Date()
        lastUpdated = new Date()
        updatedBy = SecurityUtils?.subject?.principal?.toString()
        createdBy = SecurityUtils?.subject?.principal?.toString()
        lastUpdProcess = "INSERT"
        t148StaDel = "0"
        staApproval = StatusApproval.WAIT_FOR_APPROVAL
    }
}
