package com.kombos.parts

import com.kombos.administrasi.CompanyDealer


class ValidasiOrder {
    CompanyDealer companyDealer
	RequestDetail requestDetail
//	Goods goods
//	Double t163Qty1     //ambil dari requestDetail
//	Double t163Qty2
	Date t163ETA
	PartTipeOrder partTipeOrder
	Integer t163MasaPengajuanRPP
	Double t163DP
	Double t163HargaSatuan
	Double t163TotalHarga
	//Vendor vendor
	String t163NamaUserValidasi
	Date t163TglValidasi
    StatusApproval t163StaApproval
	Date t163TglApproveUnApprove
	String t163AlasanUnApprove
	String t163NamaUserApproveUnApprove
	
	Integer rpp
	
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    //static hasOne = [poDetail: PODetail]

    static constraints = {
        companyDealer (blank:true, nullable:true)
		requestDetail nullable : false, blank : false, unique : true
	//	goods nullable : false, blank : false, unique : true
	//	t163ID nullable : false, blank : false, unique : true, maxSize : 4
//		t163Qty1 nullable : false, blank : false, maxSize : 8
//		t163Qty2 nullable : false, blank : false, maxSize : 8
		t163ETA nullable : true, blank : false
		partTipeOrder nullable : true, blank : false
		t163MasaPengajuanRPP nullable : true, blank : false, maxSize : 4
		t163DP nullable : true, blank : false, maxSize : 8
		t163HargaSatuan nullable : true, blank : false, maxSize : 8
		t163TotalHarga nullable : true, blank : false, maxSize : 8
		//vendor nullable : true, blank : false
		t163NamaUserValidasi nullable : true, blank : false, maxSize : 20
		t163TglValidasi nullable : true, blank : false
		t163StaApproval nullable : true, blank : false, maxSize : 1
		t163TglApproveUnApprove nullable : true, blank : false
		t163AlasanUnApprove nullable : true, blank : false, maxSize : 50
		t163NamaUserApproveUnApprove nullable : true, blank : false, maxSize : 20
//		poDetail nullable: true, blank: true
		
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T163_VALIDASIORDER'
		requestDetail column : 'T163_T162_ID'
	//	goods column : 'T163_M111_ID'
		id column : 'T163_ID', sqlType : 'int'
//		t163Qty1 column : 'T163_Qty1'
//		t163Qty2 column : 'T163_Qty2'
		t163ETA column : 'T163_ETA', sqlType :'date'
		partTipeOrder column : 'T163_M112_ID'
		t163MasaPengajuanRPP column : 'T163_MasaPengajuanRPP', sqlType : 'int'
		t163DP column : 'T163_DP'
		t163HargaSatuan column : 'T163_HargaSatuan'
		t163TotalHarga column : 'T163_TotalHarga'
		//vendor column : 'T163_M121_ID'
		t163NamaUserValidasi column : 'T163_NamaUserValidasi', sqlType : 'varchar(20)'
		t163TglValidasi column : 'T163_TglValidasi', sqlType : 'date'
		t163StaApproval column : 'T163_StaApproval', sqlType : 'char(1)'
		t163TglApproveUnApprove column : 'T163_TglApproveUnApprove', sqlType : 'date'
		t163AlasanUnApprove column : 'T163_AlasanUnApprove', sqlType : 'varchar(50)'
		t163NamaUserApproveUnApprove column : 'T163_NamaUserApproveUnApprove', sqlType : 'varchar(20)'
		staDel column : 'T163_StaDel', sqlType : 'char(1)'
		rpp formula: "coalesce((SELECT max(CASE WHEN rpp.M161_MaxDapatRPP >= T163_TotalHarga THEN rpp.M161_MasaPengajuanRPP ELSE 0 END) FROM M161_RPP rpp WHERE rpp.M161_TglBerlaku>=T163_TglValidasi),0)"
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "UPDATE"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "INSERT"
//		lastUpdated = new Date()
		staDel = "0"
		t163StaApproval = StatusApproval.WAIT_FOR_APPROVAL
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "INSERT"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}
}