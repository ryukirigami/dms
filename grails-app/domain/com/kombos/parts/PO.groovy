package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods
import com.kombos.parts.Request
import com.kombos.parts.Vendor

import java.text.SimpleDateFormat

class PO {
    CompanyDealer companyDealer
	String t164NoPO
	Date t164TglPO
	Vendor vendor
 	POStatus t164StaOpenCancelClose
	String t164xNamaUser
	String t164xNamaDivisi
	String staDel
    String staKirimPrint = "0"
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di
    // baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [eta : ETA]

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t164NoPO nullable:true, blank: true, unique : false, maxSize : 20
		t164TglPO nullable:true, blank: true
		vendor nullable:true, blank: true
		eta nullable:true, blank: true
 		t164StaOpenCancelClose nullable:true, blank: true,maxSize : 1
		t164xNamaUser nullable:true, blank: true,maxSize : 20
		t164xNamaDivisi nullable:true, blank: true,maxSize : 20
		staDel nullable:true, blank: true,maxSize : 1
        staKirimPrint nullable : true, blank : true
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T164_PO'
		t164NoPO column : 'T164_NoPO'//, sqlType : 'char(20)'  diilangin sqltypenya karena data sample di bootstrap tidak sesuai dengan fixed length
		t164TglPO column : 'T164_TglPO', sqlType :'date'
		vendor column : 'T164_M121_ID'
		
	 	t164StaOpenCancelClose column : 'T164_StaOpenCancelClose', sqlType : 'char(1)'

	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "UPDATE"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "INSERT"
//		lastUpdated = new Date()
		staDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "INSERT"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}

    static SimpleDateFormat codeFormat = new SimpleDateFormat("yyyyMMddhhmmss")

    static String generateCode() {
        String date = codeFormat.format(new Date())
        return date;
    }
}