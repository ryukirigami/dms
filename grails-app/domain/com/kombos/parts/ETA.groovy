package com.kombos.parts

import com.kombos.administrasi.CompanyDealer


class ETA {
    CompanyDealer companyDealer
	String t165ID
	PO po
	Goods goods
	Double t165Qty1
    Double t165Qty2
	Date t165ETA
	String t165xNamaUser
	String t165xNamaDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


   	static constraints = {
        companyDealer (blank:true, nullable:true)
		t165ID nullable : true, blank:true, maxSize : 20
		po nullable : true, blank:true
		goods nullable : true, blank:true, unique : false
		t165Qty1 nullable : true, blank:true, maxSize : 8
		t165Qty2 nullable : true, blank:true, maxSize : 8
		t165ETA nullable : true, blank:true
		t165xNamaUser nullable : true, blank:true, maxSize : 20
		t165xNamaDivisi nullable : true, blank:true, maxSize : 20
		staDel nullable : true, blank:true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete */
    }
	
	static mapping = {
        autoTimestamp false
        table 'T165_ETA'
    	t165ID column : 'T165_ID', sqlType : 'char(20)'
		po column : 'T165_T164_NoPO'
		goods column : 'T165_T164_T163_T162_M111_ID'
		t165Qty1 column : 'T165_Qty1'
		t165Qty2 column : 'T165_Qty2'
		t165ETA column : 'T165_ETA', sqlType : 'timestamp'
		t165xNamaUser column : 'T165_xNamaUser', sqlType : 'varchar(20)'
		t165xNamaDivisi column : 'T165_xNamaDivisi', sqlType : 'varchar(20)'
		staDel column : 'T165_StaDel', sqlType : 'char(1)'
	}
}
