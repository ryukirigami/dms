package com.kombos.parts

class BinningDetail {
    Binning binning
    static belongsTo = Binning
   	Double t168Qty1Binning
   	Double t168Qty2Binning
   	String t168xNamaUser
   	String t168xNamaDivisi
   	String staStockIn //0 or null = blm, 1= sudah
    GoodsReceiveDetail goodsReceiveDetail

   	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

   	static constraints = {
   		binning nullable : false, blank : false
   		t168Qty1Binning nullable : false, blank : false, maxSize : 8
   		t168Qty2Binning nullable : false, blank : false, maxSize : 8
   		t168xNamaUser nullable : false, blank : false, maxSize : 20
   		t168xNamaDivisi nullable : false, blank : false, maxSize : 20
        goodsReceiveDetail nullable : true, blank : true
   		staDel nullable : false, blank : false, maxSize : 1
        staStockIn nullable : true, blank : true, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

   	static mapping = {
        autoTimestamp false
        table 'T168_BINNING_DETAIL'
        binning column : 'T168_T168_ID'
   		t168Qty1Binning column : 'T168_Qty1Binning'
   		t168Qty2Binning column : 'T168_Qty2Binning'
   		t168xNamaUser column : 'T168_xNamaUser', sqlType : 'varchar(20)'
   		t168xNamaDivisi column : 'T168_xNamaDivisi', sqlType : 'varchar(20)'
        goodsReceiveDetail column : 'T168_T167_GOODSRECEIVE_DETAIL'
   		staDel column : 'T168_StaDel', sqlType : 'char(1)'
   	}

}
