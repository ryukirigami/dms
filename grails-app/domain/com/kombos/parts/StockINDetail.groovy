package com.kombos.parts

class StockINDetail {
    StockIN stockIN
    static belongsTo = StockIN
	Double t169Qty1StockIn
	Double t169Qty2StockIn
	Location location
	Location locationRusak
	String t169xNamaUser
	String t169xNamaDivisi
    BinningDetail binningDetail

	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        stockIN nullable : false, blank : false
		t169Qty1StockIn nullable : false, blank : false
		t169Qty2StockIn nullable : false, blank : false
		location nullable : true, blank : true
		locationRusak nullable : true, blank : true
		t169xNamaUser nullable : false, blank : false, maxSize : 20
		t169xNamaDivisi nullable : false, blank : false, maxSize : 20
        binningDetail nullable : false, blank : false
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T169_STOCK_DETAIL'
        stockIN column : 'T169_T169_ID'//, sqlType :'char(20)'
		t169Qty1StockIn column : 'T169_Qty1StockIn'
		t169Qty2StockIn column : 'T169_Qty2StockIn'
		location column : 'T169_M120_ID'
		locationRusak column : 'T169_M120_ID_Rusak'
		t169xNamaUser column : 'T169_xNamaUser', sqlType : 'varchar(20)'
		t169xNamaDivisi column : 'T169_xNamaDivisi', sqlType : 'varchar(20)'
        binningDetail column : 'T169_T168_BINNING_DETAIL'
		staDel column : 'T169_StaDel', sqlType : 'char(1)'
	}
}
