package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class DeliveryOrder {

    CompanyDealer companyDealer
    SalesOrder sorNumber
    String doNumber
    Date limitDate
    Date requireDate
    String warehouse
    String paymentCondition
    String owner
    String paymentStatus
    String intext
    String contractNumber
    Date printedDate
    String printedBy
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess

    static constraints = {
        sorNumber nullable : true, blank: true
        doNumber nullable : true, blank: true, maxSize: 50
        limitDate nullable : true, blank: true
        requireDate nullable : true, blank: true
        warehouse nullable : true, blank: true , maxSize: 24
        paymentCondition nullable : true, blank: true , maxSize: 32
        owner nullable : true, blank: true , maxSize: 16
        paymentStatus nullable : true, blank: true , maxSize: 16
        intext nullable : true, blank: true , maxSize: 16
        contractNumber nullable : true, blank: true, maxSize: 32
        printedDate nullable : true, blank: true
        printedBy nullable : true, blank: true, maxSize: 16
        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
    }
}
