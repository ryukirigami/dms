package com.kombos.parts

class Satuan {
	Integer m118ID
	String m118KodeSatuan
	String m118Satuan1
	String m118Satuan2
	Double m118Konversi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m118ID blank:true, nullable:true
		m118KodeSatuan blank:false, nullable:false, maxSize:50
		m118Satuan1 blank:false, nullable:false, maxSize:50
		m118Satuan2 blank:false, nullable:false, maxSize:50
		m118Konversi blank:false, nullable:false, maxSize:8
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M118_SATUAN'
		m118ID column:'M118_ID', sqlType:'int'
		m118KodeSatuan column:'M118_KodeSatuan', sqlType:'varchar(50)'
		m118Satuan1 column:'M118_Satuan1', sqlType:'varchar(50)'
		m118Satuan2 column:'M118_Satuan2', sqlType:'varchar(50)'
		m118Konversi column:'M118_Konversi'//, sqlType:'double'
		staDel column:'M118_StaDel', sqlType:'char(1)'
	}
	
	String toString() {
		return m118Satuan1
	}
}
