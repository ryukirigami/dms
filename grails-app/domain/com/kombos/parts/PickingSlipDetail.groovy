package com.kombos.parts


class PickingSlipDetail {

	PickingSlip pickingSlip
	Goods goods
	Double t142Qty1
	Double t142Qty2
	String t142StaPrePicking
	String t142StaPicking
	String staDel
    String status
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		pickingSlip nullable : false, blank : false, unique : false
		goods nullable : true, blank : true, unique : false

		t142Qty1 nullable : true, blank : true, maxSize : 8
		t142Qty2 nullable : true, blank : true, maxSize : 8
        status nullable:true, maxSize : 1
		t142StaPrePicking nullable : true, blank : true, maxSize : 1
		t142StaPicking nullable : true, blank : true, maxSize : 1
        staDel nullable : true, blank : true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
		table 'T142_PICKINGSLIPDETAIL'

		pickingSlip column : 'T142_T141_ID'

		goods column : 'T142_M111_ID'
		t142Qty1 column : 'T142_Qty1'
		t142Qty2 column : 'T142_Qty2'
		t142StaPrePicking column : 'T142_StaPrePicking', sqlType : 'char(1)'
		t142StaPicking column : 'T142_StaPicking', sqlType : 'char(1)'
        staDel column : 'T142_StaDel', sqlType : 'char(1)'
	}
}
