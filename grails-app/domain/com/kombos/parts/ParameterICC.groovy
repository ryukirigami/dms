package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class ParameterICC {

	CompanyDealer companyDealer
	Integer m155ID
	Date m155Tanggal
	String m155KodeICC
	String m155NamaICC
	Double m155NilaiDAD1
	Double m155NilaiDAD2
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		
		m155Tanggal nullable : false, blank : false
		m155KodeICC nullable : false, blank : false, maxSize : 1
		m155NamaICC nullable : false, blank : false, maxSize : 50
		m155NilaiDAD1 nullable : false, blank : false, maxSize :8
		m155NilaiDAD2 nullable : false, blank : false, maxSize : 8
        m155ID  nullable : true, blank : true,maxSize :4
        companyDealer nullable : true, blank : true
		staDel nullable : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'M155_PARAMETERICC'
		m155ID column : 'M155_ID', sqlType : 'int'
		companyDealer column : 'M155_M011_ID' 
		m155Tanggal column : 'M155_Tanggal', sqlType : 'date'
		m155KodeICC column : 'M155_KodeICC', sqlType : 'char(1)'
		m155NamaICC column : 'M155_NamaICC', sqlType : 'varchar(50)'
		m155NilaiDAD1 column : 'M155_NilaiDAD1'
        m155NilaiDAD2 column : 'M155_NilaiDAD2'
		staDel column : 'M155_StaDel', sqlType : 'char(1)'
	}

	String toString(){
		return m155KodeICC
	}
}
