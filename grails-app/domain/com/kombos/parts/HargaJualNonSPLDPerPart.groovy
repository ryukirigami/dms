package com.kombos.parts


import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods


class HargaJualNonSPLDPerPart {

    CompanyDealer companyDealer
	Date t160TMT
	Goods goods
	Double t160PersenKenaikanHarga
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
    	companyDealer nullable : false, blank: false// unique : true
		t160TMT nullable : false, blank: false//, unique : true
		goods nullable : false, blank: false//, unique : true
		t160PersenKenaikanHarga  nullable : false, blank: false, maxSize : 8
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T160_HARGAJUALNONSPLDPERPART'
		companyDealer column : 'T160_M011_ID'
		t160TMT column : 'T160_TMT', sqlType : 'date'
		goods column : 'T160_M111_ID'
		t160PersenKenaikanHarga column : 'T160_PersenKenaikanHarga'
	}
}
