package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class RPP {

	CompanyDealer companyDealer
	Date m161TglBerlaku
	Integer m161MasaPengajuanRPP
	Double m161MaxDapatRPP
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		m161TglBerlaku nullable : false, blank : false
		m161MasaPengajuanRPP nullable : false, blank : false, maxSize : 4
		m161MaxDapatRPP nullable : false, blank : false
        companyDealer nullable : true, blank : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M161_RPP'
		companyDealer column : 'M161_M011_ID'
		m161TglBerlaku column : 'M161_TglBerlaku', sqlType : 'date'  
		m161MasaPengajuanRPP column : 'M161_MasaPengajuanRPP', sqlType : 'int'
		m161MaxDapatRPP column : 'M161_MaxDapatRPP'
	}
}
