package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods
import com.kombos.parts.ParameterICC

class ICC {

	CompanyDealer companyDealer
	Date t155Tanggal
	Goods goods
	ParameterICC parameterICC
	Integer t155DAD
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer nullable : false, blank : false, unique:false
		t155Tanggal nullable : false, blank : false, unique: false
		goods nullable : false, blank : false, unique:true
		parameterICC nullable : false, blank : false
		t155DAD nullable : false, blank : false, maxSize : 4
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T155_ICC'
		companyDealer column : 'T155_M011_ID'
		t155Tanggal column : 'T155_Tanggal', sqlType : 'date'
		goods column : 'T155_M111_ID'
		parameterICC column : 'T155_M155_ID'
		t155DAD column : 'T155_DAD', sqlType : 'int'
		staDel column : 'T155_StaDel', sqlType : 'char(1)'
	}
}
