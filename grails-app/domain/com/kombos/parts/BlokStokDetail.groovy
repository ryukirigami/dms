package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class BlokStokDetail {
    CompanyDealer companyDealer
	BlockStok blockStok
	Goods goods
	double t158Qty1
	double t158Qty2
	String t158StaDisposal
    String staDel
	Location location
	String t158AlasanBlockStock
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
        blockStok nullable : true, blank : true, unique : false
        goods nullable : true, blank : true, unique : false
        t158Qty1 nullable : true, blank : true, maxSize : 8
        t158Qty2 nullable : true, blank : true, maxSize : 8
        t158StaDisposal nullable : true, blank : true, maxSize : 1
        staDel nullable : true, blank : true, maxSize : 1
        location nullable : true, blank : true
         dateCreated blank : true, nullable : true
        lastUpdated blank : true, nullable : true
        t158AlasanBlockStock nullable : true, blank : true, maxSize : 50
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T158_BLOCKSTOKDETAIL'
		blockStok column : 'T158_T157_ID'
		goods column : 'T158_M111_ID'
		t158Qty1 column : 'T158_Qty1'
		t158Qty2 column : 'T158_Qty2'

		t158StaDisposal column : 'T158_StaDisposal'
		staDel column : 'T158_StaDel'
		location column : 'T158_M120_ID'
		t158AlasanBlockStock column : 'T158_AlasanBlockStock'
	}
}
