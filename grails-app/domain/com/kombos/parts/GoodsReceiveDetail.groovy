package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class GoodsReceiveDetail {
    CompanyDealer companyDealer
    GoodsReceive goodsReceive
    static belongsTo = GoodsReceive
    PODetail poDetail
    Invoice invoice
    Goods goods
    Double t167Qty1Issued
    Double t167Qty2Issued
    Double part_usage
    Double t167Qty1Reff
    Double t167Qty2Reff
    Double t167Qty1Rusak
    Double t167Qty2Rusak
    Double t167Qty1Salah
    Double t167Qty2Salah
    Double t167CIF
    Double t167ImportDuty
    Double t167Unloading
    Double t167WHStorage
    Double t167Trans
    Double t167Other
    Double t167LandedCost
    Double t167RetailPrice
    Double t167Disc
    Double t167NetSalesPrice
    String staDel
    String staBinning  //0 or null=belum, 1=sudah
    String staNPB
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        goodsReceive nullable : false, blank : false
        goods nullable : true, blank:true
   		t167Qty1Issued nullable : false, blank:false, maxSize : 8
   		t167Qty2Issued nullable : false, blank:false, maxSize : 8
        part_usage nullable : true, blank:true
        staBinning nullable : true, blank:true
   		t167Qty1Reff nullable : false, blank:false, maxSize : 8
   		t167Qty2Reff nullable : false, blank:false, maxSize : 8
   		t167Qty1Rusak nullable : false, blank:false, maxSize : 8
   		t167Qty2Rusak nullable : false, blank:false, maxSize : 8
   		t167Qty1Salah nullable : false, blank:false, maxSize : 8
   		t167Qty2Salah nullable : false, blank:false, maxSize : 8
   		t167CIF nullable : false, blank:false, maxSize : 8
   		t167ImportDuty nullable : false, blank:false, maxSize : 8
   		t167Unloading nullable : false, blank:false, maxSize : 8
   		t167WHStorage nullable : false, blank:false, maxSize : 8
   		t167Trans nullable : false, blank:false, maxSize : 8
   		t167Other nullable : false, blank:false, maxSize : 8
   		t167LandedCost nullable : false, blank:false, maxSize : 8
   		t167RetailPrice nullable : false, blank:false, maxSize : 8
   		t167Disc nullable : false, blank:false, maxSize : 8
   		t167NetSalesPrice nullable : false, blank:false, maxSize : 8
   		staDel nullable : false, blank:false, maxSize : 1
        staNPB nullable : true, blank:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        poDetail nullable: true, blank: true
        invoice nullable: true, blank: true
    }

   	static mapping = {
        autoTimestamp false
        table 'T167_GOODSRECEIVE_DETAIL'
        goodsReceive column : 'T167_T167_ID'
        goods column: 'T167_M111_GOODS'
        poDetail column: 'T167_T164_PO_DETAIL'
        invoice column: 'T167_T166_INVOICE'
   		t167Qty1Issued column : 'T167_Qty1Issued'
   		t167Qty2Issued column : 'T167_Qty2Issued'
        part_usage column : 'part_usage'
        staBinning column: 'T167_staBinning'
   		t167Qty1Reff column : 'T167_Qty1Reff'
   		t167Qty2Reff column : 'T167_Qty2Reff'
   		t167Qty1Rusak column : 'T167_Qty1Rusak'
   		t167Qty2Rusak column : 'T167_Qty2Rusak'
   		t167Qty1Salah column : 'T167_Qty1Salah'
   		t167Qty2Salah column : 'T167_Qty2Salah'
   		t167CIF column : 'T167_CIF'
   		t167ImportDuty column : 'T167_ImportDuty'
   		t167Unloading column : 'T167_Unloading'
   		t167WHStorage column : 'T167_WHStorage'
   		t167Trans column : 'T167_Trans'
   		t167Other column : 'T167_Other'
   		t167LandedCost column : 'T167_LandedCost'
   		t167RetailPrice column : 'T167_RetailPrice'
   		t167Disc column : 'T167_Disc'
   		t167NetSalesPrice column : 'T167_NetSalesPrice'
   		staDel column : 'T167_StaDel', sqlType : 'char(1)'
   	}

}
