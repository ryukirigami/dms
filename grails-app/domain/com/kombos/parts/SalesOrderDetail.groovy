package com.kombos.parts

class SalesOrderDetail {
    SalesOrder sorNumber
    Goods materialCode
    Double quantity
    Double unitPrice
    Double unitPriceBeli
    Double discount
    String owner
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess

    static constraints = {
        sorNumber nullable : false, blank: false
        materialCode nullable : false, blank: false, maxSize: 16
        quantity nullable : false, blank: false
        unitPrice nullable : false, blank: false
        unitPriceBeli nullable : true, blank: true
        discount nullable : false, blank: false
        owner nullable : true, blank: true, maxSize: 16
        staDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false

    }
}
