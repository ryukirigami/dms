package com.kombos.parts

class Group {
	
	String m181ID
	Date m181TglBerlaku
	String m181NamaGroup
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m181TglBerlaku nullable : false, blank: false
        m181ID nullable : false, blank: false, maxSize : 2
		m181NamaGroup nullable : false, blank: false, maxSize : 20
		staDel  nullable : false, blank: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M181_GROUP'
		m181ID column : 'M181_ID', sqlType : 'char(2)'
		m181TglBerlaku column : 'M181_TglBerlaku', sqlType : 'date'
		m181NamaGroup column : 'M181_NamaGroup', sqlType : 'varchar(20)'
		staDel column : 'M181_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m181NamaGroup
	}
}
