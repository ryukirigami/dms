package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class Request {
    CompanyDealer companyDealer
	//Integer t162ID
	Date t162TglRequest
	String t162StaFA
	String t162NoReff    
    String t162NamaPemohon
	String t162xNamaUser
	String t162xNamaDivisi


	static hasMany = [requests : RequestDetail]
    

	//Double totalHarga
	
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
	//	t162ID nullable : true, blank : true, unique : true, maxSize : 20
	//	goods nullable : true, blank : true, unique : true
	//	po nullable:true, unique:true
		
        requests nullable:true, unique:false
	//	t162Qty1 nullable : true, blank : true, maxSize : 8
	//	t162Qty2 nullable : true, blank : true, maxSize : 8
    //    satuan1 nullable : true, blank : true
    //   satuan2 nullable : true, blank : true
		t162TglRequest nullable : true, blank : true
		t162StaFA nullable : true, blank : true, maxSize : 1
        t162NoReff nullable : true, blank : true, maxSize : 50
	//	t162Qty1Available nullable : true, blank : true, maxSize : 8
	//	t162Qty2Available nullable : true, blank : true, maxSize : 8
		t162NamaPemohon nullable : true, blank : true, maxSize : 50
		t162xNamaUser nullable : true, blank : true, maxSize : 20
		t162xNamaDivisi nullable : true, blank : true, maxSize : 20
        staDel nullable : true, blank : true, maxSize : 1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T162_REQUEST'
		//t162ID 
		id column : 'T162_ID', sqlType : 'int'
	//	goods column : 'T162_M111_ID'
	//	t162Qty1 column : 'T162_Qty1'
	//	t162Qty2 column : 'T162_Qty2'
		t162TglRequest column : 'T162_TglRequest', sqlType : 'date'
		t162StaFA column : 'T162_StaFA', sqlType : 'char(1)'
		t162NoReff column : 'T162_NoReff', sqlType : 'varchar(50)'
 //       satuan1 column : 'satuan1', sqlType : 'varchar(50)'
  //      satuan2 column : 'satuan2', sqlType : 'varchar(50)'
	//	t162Qty1Available column : 'T162_Qty1Available'
	//	t162Qty2Available column : 'T162_Qty2Available'
		t162NamaPemohon column : 'T162_NamaPemohon', sqlType : 'varchar(50)'
		t162xNamaUser column : 'T162_NamaUser', sqlType : 'varchar(20)'
		t162xNamaDivisi column : 'T162_NamaDivisi', sqlType : 'varchar(20)'
		//status column : 'T162_Status', enumType: 'ordinal'
        staDel column : 'T162_StaDel', sqlType : 'char(1)'
		//totalHarga formula: 'coalesce(SELECT hb.T150_Harga FROM T150_GOODSHARGABELI hb WHERE T162_M111_ID=hb.T162_M111_ID,0) * coalesce(T162_Qty1,0)'
	}

	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "UPDATE"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "INSERT"
//		lastUpdated = new Date()
		staDel = "0"
		
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "INSERT"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}
}