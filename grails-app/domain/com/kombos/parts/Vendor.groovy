package com.kombos.parts

class Vendor {

	String m121ID
	String m121Nama
	String m121staSPLD
	String m121Alamat
	String m121AlamatNPWP
	String m121Telp
	String m121ContactPerson
	String m121TelpCP
	String m121eMail
	String m121NPWP
	String m121NamaBank1
	String m121NoRekBank1
	String m121AtasNamaBank1
	String m121NamaBank2
	String m121NoRekBank2
	String m121AtasNamaBank2
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		m121ID unique : true, blank : false, nullable: false, maxSize : 6
		m121Nama blank : false, nullable : false, maxSize : 50
		m121staSPLD blank : false, nullable : false, maxSize : 1
		m121Alamat blank : false, nullable : false, maxSize : 300
		m121AlamatNPWP blank : false, nullable : false, maxSize : 300
		m121Telp blank : false, nullable : false, maxSize : 20
        m121NPWP blank : false, nullable : false, maxSize : 20
        m121eMail blank : false, nullable : false, maxSize : 20, email : true
		m121ContactPerson blank : false, nullable : false, maxSize : 20
		m121TelpCP blank : false, nullable : false, maxSize : 50
		m121NamaBank1 blank : false, nullable : false, maxSize : 20
		m121NoRekBank1 blank : false, nullable : false, maxSize : 20
		m121AtasNamaBank1 blank : false, nullable : false, maxSize : 20
		m121NamaBank2 blank : true, nullable : true, maxSize : 20
		m121NoRekBank2 blank : true, nullable : true, maxSize : 20
		m121AtasNamaBank2 blank : true, nullable : true, maxSize : 20
		staDel blank : false, nullable : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'M121_VENDOR'
		m121ID column : 'M121_ID', sqlType : 'char(100)'
		m121Nama column : 'M121_NAMA', sqlType : 'varchar(100)'
		m121staSPLD column : 'M121_staSPLD', sqlType : 'char(1)'
		m121Alamat column : 'M121_Alamat', sqlType : 'varchar(300)'
		m121AlamatNPWP column : 'M121_AlamatNPWP', sqlType : 'varchar(300)'
		m121Telp column : 'M121_Telp', sqlType : 'varchar(20)'
		m121ContactPerson column : 'M121_ContactPerson', sqlType : 'varchar(20)'
		m121TelpCP column : 'M121_TelpCP', sqlType : 'varchar(50)'
		m121eMail column : 'M121_eMail', sqlType : 'varchar(20)'
		m121NPWP column : 'M121_NPWP', sqlType : 'varchar(20)'
		m121NamaBank1 column : 'M121_NamaBank1', sqlType : 'varchar(20)'
		m121NoRekBank1 column : 'M121_NoRekBank1', sqlType : 'varchar(20)'
		m121AtasNamaBank1 column : 'M121_AtasNamaBank1', sqlType : 'varchar(20)'
		m121NamaBank2 column : 'M121_NamaBank2', sqlType : 'varchar(20)'
		m121NoRekBank2 column : 'M121_NoRekBank2', sqlType : 'varchar(20)'
		m121AtasNamaBank2 column : 'M121_AtasNamaBank2', sqlType : 'varchar(20)'
		staDel column : 'M121_staDel', sqlType : 'char(1)'
	}

	@Override
	String toString() {
		// TODO Auto-generated method stub
		return m121Nama
	}
}
