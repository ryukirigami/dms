package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.Reception

class PickingSlip {
    CompanyDealer companyDealer
	String t141ID
	Reception reception
	Date t141TglPicking
	String t141NamaTeknisi
	String t141Ket
	String t141xNamaUser
	String t141xNamaDivisi
	String staDel

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t141ID  nullable:true, blank : true, maxSize : 20, unique : false

		reception nullable:true, blank : true
		t141TglPicking nullable:true, blank : true
		t141NamaTeknisi nullable:true, blank : true, maxSize : 255
		t141Ket nullable:true, blank : true, maxSize : 50
		t141xNamaUser nullable:true, blank : true, maxSize : 20
		t141xNamaDivisi nullable:true, blank : true, maxSize : 20
        staDel nullable:true, maxSize : 1

        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
		table 'T141_PICKINGSLIP'
		t141ID column : 'T141_ID', sqlType : 'char(20)'

		reception column : 'T141_T401_NoWO'
		t141TglPicking column : 'T141_TglPicking', sqlType : 'date'
		t141NamaTeknisi column : 'T141_NamaTeknisi', sqlType : 'varchar(255)'
		t141Ket column : 'T141_Ket', sqlType : 'varchar(50)'
		t141xNamaUser column : 'T141_xNamaUser', sqlType : 'varchar(20)'
		t141xNamaDivisi column : 'T141_xNamaDivisi', sqlType : 'varchar(20)'
        staDel column : 'T141_StaDel', sqlType : 'char(1)'
	}
}


