package com.kombos.parts

class SCC {
	int m113ID
	Date m113TglBerlaku
	String m113KodeSCC
	String m113NamaSCC
	String m113StaRPP
	String m113Ket
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {

        m113TglBerlaku blank:false, nullable:false
        m113KodeSCC blank:false, nullable:false, maxSize:2
        m113NamaSCC blank:false, nullable:false, maxSize:50
        m113StaRPP blank:false, nullable:false, maxSize:1
        m113Ket blank:true, nullable:true, maxSize:60
        staDel blank:false, nullable:false, maxSize:1
        m113ID blank:true, nullable:true, maxSize:4
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M113_SCC"
		m113ID column:"M113_ID", sqlType:"int"
		m113TglBerlaku column:"M113_TglBerlaku", sqlType:"date"
		m113KodeSCC column:"M113_KodeSCC", sqlType:"char(2)"
		m113NamaSCC column:"M113_NamaSCC", sqlType:"varchar(50)"
		m113StaRPP column:"M113_StaRPP", sqlType:"char(1)"
		m113Ket column:"M113_Ket", sqlType:"varchar(60)"
		staDel column:"M113_StaDel", sqlType:"char(1)"
	}
	
	String toString(){
		return m113NamaSCC
	}
}
