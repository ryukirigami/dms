package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class StockIN {
    CompanyDealer companyDealer
	String t169ID
	Date t169TglJamStockIn
	String t169PetugasStockIn
	//Binning binning
    static hasMany = [stockINDetail : StockINDetail]
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t169ID nullable : false, blank : false, unique : true, maxSize : 20
		t169TglJamStockIn nullable : false, blank : false
		t169PetugasStockIn nullable : false, blank : false, maxSize : 50
	//	binning nullable : false, blank : false
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T169_STOCKIN'
		t169ID column : 'T169_ID', sqlType :'char(20)'
		t169TglJamStockIn column : 'T169_TglJamStockIn', sqlType : 'TIMESTAMP'
		t169PetugasStockIn column : 'T169_PetugasStockIn', sqlType : 'varchar(50)'
	//	binning column : 'T169_T168_ID'
		staDel column : 'T169_StaDel', sqlType : 'char(1)'
	}
}
