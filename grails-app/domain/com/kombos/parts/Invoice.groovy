package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods
import com.kombos.parts.PO;

import java.util.Date;

class Invoice {
    CompanyDealer companyDealer
//    String t166ID
	String t166NoInv
	Date t166TglInv
	PO po
	Goods goods
	Double t166Qty1
	Double t166Qty2
	Double t166RetailPrice
	Double t166Disc
	Double t166NetSalesPrice
	String t166xNamaUser
	String t166xNamaDivisi
	String t166StaBayar
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
//		t166ID nullable : false, blank:false, maxSize : 20, unique : true
		t166NoInv nullable : false, blank:false, maxSize : 50
		t166TglInv nullable : false, blank:false
		po nullable : false, blank:false
		goods nullable : false, blank:false
		t166Qty1 nullable : false, blank:false, maxSize : 8
		t166Qty2 nullable : false, blank:false, maxSize : 8
		t166RetailPrice nullable : false, blank:false, maxSize : 8
		t166Disc nullable : false, blank:false, maxSize : 8
		t166NetSalesPrice nullable : false, blank:false, maxSize : 8 
		t166xNamaUser nullable : false, blank:false, maxSize : 20
		t166xNamaDivisi nullable : false, blank:false, maxSize : 20
        t166StaBayar nullable : true, blank:true
		staDel nullable : false, blank:false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T166_INVOICE'
//		t166ID column : 'T166_ID', sqlType : 'char(20)'
		t166NoInv column : 'T166_NoInv', sqlType : 'varchar(50)'
		t166TglInv column : 'T166_TglInv', sqlType : 'date'
		po column : 'T166_T164_NoPO'
		goods column : 'T166_T164_T163_T162_M111_ID'
		t166Qty1 column : 'T166_Qty1'
		t166Qty2 column : 'T166_Qty2'
		t166RetailPrice column : 'T166_RetailPrice'
		t166Disc column : 'T166_Disc'
		t166NetSalesPrice column : 'T166_NetSalesPrice'
		t166xNamaUser column : 'T166_xNamaUser', sqlType : 'varchar(20)'
		t166xNamaDivisi column : 'T166_xNamaDivisi', sqlType : 'varchar(20)'
        t166StaBayar column : 'T166_STABAYAR', sqlType : 'varchar(1)'
		staDel column : 'T166_StaDel', sqlType : 'char(1)'
	}

    static transients = ['vendor']

    Vendor getVendor() {
        return po.vendor
    }
}
