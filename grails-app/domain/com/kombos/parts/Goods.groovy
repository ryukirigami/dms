package com.kombos.parts


class Goods {
	String m111ID
	String m111Nama
	Satuan satuan
    Byte m111Foto
	static hasOne = [/*, goodsHargaJual: GoodsHargaJual*/ ]
    static hasMany = [partsStok : PartsStok]

	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m111ID blank:false, nullable:false, maxSize:50
		m111Nama blank:false, nullable:false, maxSize:100
        m111Foto blank:true, nullable:true
		satuan blank:false, nullable:false, maxSize:4
		
		partsStok nullable:true
//		goodsHargaJual nullable:true, unique: true

		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M111_GOODS"
		m111ID column:"M111_ID"//, sqlType:"char(17)"
		m111Nama column:"M111_Nama", sqlType:"varchar(50)"
		satuan column:"M111_M118_ID"

		staDel column:"M111_StaDel", sqlType:"varchar(1)"
	}
	
	String toString() {
		return m111ID +" | "+ m111Nama
	}
}
