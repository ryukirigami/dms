package com.kombos.parts

class PODetail{

    ValidasiOrder validasiOrder
    Double t164Qty1
	Double t164Qty2
	Double t164HargaSatuan
	Double t164TotalHarga
	
	PO po
	static belongsTo = PO
	
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        validasiOrder nullable:true, blank: true
        t164Qty1 nullable:true, blank: true,maxSize : 8
		t164Qty2 nullable:true, blank: true,maxSize : 8
		t164HargaSatuan nullable:true, blank: true,maxSize : 8
		t164TotalHarga nullable:true, blank: true,maxSize : 8
		
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T164_PO_DETAIL'
        validasiOrder column : 'T164_T163_ID'
		t164Qty1 column : 'T164_Qty1'
		t164Qty2 column : 'T164_Qty2'
		t164HargaSatuan column : 'T164_HargaSatuan'
		t164TotalHarga column : 'T164_TotalHarga'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
		try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }

}