package com.kombos.parts

import com.kombos.administrasi.CompanyDealer

class Location {
	Integer m120ID
	CompanyDealer companyDealer
	ParameterICC parameterICC
	String m120NamaLocation
	String m120Ket
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		
		parameterICC blank:true, nullable:true
		m120NamaLocation blank:true, nullable:true, maxSize:50
		m120Ket blank:true, nullable:true, maxSize:55
        m120ID blank:true, nullable:true, maxSize:4
        companyDealer blank:true, nullable:true
		staDel blank:true, nullable:true, maxSize:1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M120_LOCATION"
		companyDealer column:"M120_M011_ID"
		parameterICC column:"M120_M155_ID"
		m120ID column:"M120_ID", sqlType: "int"
		m120NamaLocation column:"M120_NamaLocation", sqlType: "varchar(50)"
		m120Ket column:"M120_Ket", sqlType: "varchar(55)"
		staDel column:"M120_StaDel", sqlType: "char(1)"
	}

    String toString(){
        return m120NamaLocation;
    }
}
