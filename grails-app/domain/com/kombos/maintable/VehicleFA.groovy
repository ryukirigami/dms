package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.FA
import com.kombos.customerprofile.HistoryCustomerVehicle

class VehicleFA {

    CustomerVehicle customerVehicle
    HistoryCustomerVehicle historyCustomerVehicle
    FA fa
    String t185StaReminderFA
    String t185StaSudahDiperiksa
    String t185StaSudahDikerjakan
    CompanyDealer companyDealer
    Date t185TanggalDikerjakan
    String t185Lokasi
    String t185MainDealer
    String t185Dealer
    String t185Ket
    String t185xNamaUser
    String t185xNamaDivisi
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		customerVehicle nullable : true, blank : true
		historyCustomerVehicle nullable : true, blank : true
		fa nullable : false, blank : false
		t185StaReminderFA nullable : true, blank : true, maxSize : 1
		t185StaSudahDiperiksa nullable : true, blank : true, maxSize : 1
		t185StaSudahDikerjakan nullable : true, blank : true, maxSize : 1
		companyDealer nullable : true, blank : true
		t185TanggalDikerjakan nullable : true, blank : true
		t185Lokasi nullable : true, blank : true, maxSize : 1
		t185MainDealer nullable : true, blank : true, maxSize : 50
		t185Dealer nullable : true, blank : true, maxSize : 50
		t185Ket nullable : true, blank : true, maxSize : 50
		t185xNamaUser nullable : true, blank : true, maxSize : 20
		t185xNamaDivisi nullable : true, blank : true, maxSize : 20
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }


	static mapping = {
        autoTimestamp false
        table 'T185_VEHICLEFA'
		customerVehicle column : 'T185_T103_VinCode'
		historyCustomerVehicle column : 'T185_T183_ID'
		fa column : 'T185_M185_ID'
		t185StaReminderFA column : 'T185_StaReminderFA', sqlType : 'char(1)'
		t185StaSudahDiperiksa column : 'T185_StaSudahDiperiksa', sqlType : 'char(1)'
		t185StaSudahDikerjakan column : 'T185_StaSudahDikerjakan', sqlType : 'char(1)'
		companyDealer column : 'T185_M011_ID'
		t185TanggalDikerjakan column : 'T185_TanggalDikerjakan', sqlType : 'date'
		t185Lokasi column : 'T185_Lokasi', sqlType : 'char(1)'
		t185MainDealer column : 'T185_MainDealer', sqlType : 'varchar(50)'
		t185Dealer column : 'T185_Dealer', sqlType : 'varchar(50)'
		t185Ket column : 'T185_Ket', sqlType : 'varchar(50)'
		t185xNamaUser column : 'T185_xNamaUser', sqlType : 'varchar(20)'
		t185xNamaDivisi column : 'T185_xNamaDivisi', sqlType : 'varchar(20)'
	}
}
