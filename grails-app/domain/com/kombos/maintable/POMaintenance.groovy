package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods
import com.kombos.parts.PO;

import java.util.Date;

class POMaintenance {
    CompanyDealer companyDealer
	String t170ID
	Date t170TglMaintenance
	String t170PetugasMaintenance
	PO po
	Goods goods
	String t170xNamaUser
	String t170xNamaDivisi
	String t170StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t170ID nullable : false, blank : false, unique : true, maxSize : 20
		t170TglMaintenance nullable : false, blank : false
		t170PetugasMaintenance nullable : false, blank : false, maxSize : 50
		po nullable : false, blank : false
		goods nullable : false, blank : false, unique :true
		t170xNamaUser nullable : false, blank : false, maxSize : 20
		t170xNamaDivisi nullable : false, blank : false, maxSize : 20
		t170StaDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T170_POMAINTENANCE'
		t170ID column : 'T170_ID', sqlType :'char(20)'
		t170TglMaintenance column : 'T170_TglMaintenance', sqlType : 'date'
		t170PetugasMaintenance column : 'T170_PetugasMaintenance', sqlType : 'varchar(50)'
		po column : 'T170_T164_NoPO'
		goods column : 'T170_T164_T163_T162_M111_ID'
		t170xNamaUser column : 'T170_xNamaUser', sqlType : 'varchar(20)'
		t170xNamaDivisi column : 'T170_xNamaDivisi', sqlType : 'varchar(20)'
		t170StaDel column : 'T170_StaDel', sqlType : 'char(1)'
	}
}
