package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class EFakturMasukan {

    CompanyDealer companyDealer
    String docNumber
    Date tanggal
    String FM
    String KD_JENIS_TRANSAKSI
    String FG_PENGGANTI
    String NOMOR_FAKTUR
    String MASA_PAJAK
    String TAHUN_PAJAK
    Date TANGGAL_FAKTUR
    String NPWP
    String NAMA
    String ALAMAT_LENGKAP
    BigDecimal JUMLAH_DPP
    BigDecimal JUMLAH_PPN
    BigDecimal JUMLAH_PPNBM
    String IS_CREDITABLE
    String staDel
    String staOriRev
    String jenisInputan
    String staAprove   // 1 = APPROVED, 2 = REJECTED, 0 = WAIT_APPROVE
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        docNumber (blank:true, nullable:true)
        jenisInputan (blank: true, nullable: true)
        tanggal (blank: true, nullable: true)
        FM (blank: true, nullable: true)
        KD_JENIS_TRANSAKSI (blank: true, nullable: true)
        FG_PENGGANTI (blank: true, nullable: true)
        NOMOR_FAKTUR (blank: true, nullable: true)
        MASA_PAJAK (blank: true, nullable: true)
        TAHUN_PAJAK (blank: true, nullable: true)
        TANGGAL_FAKTUR (blank: true, nullable: true)
        NPWP (blank: true, nullable: true)
        NAMA (blank: true, nullable: true)
        ALAMAT_LENGKAP (blank: true, nullable: true)
        JUMLAH_DPP (blank: true, nullable: true)
        JUMLAH_PPN (blank: true, nullable: true)
        JUMLAH_PPNBM (blank: true, nullable: true)
        IS_CREDITABLE (blank: true, nullable: true)
        staDel (nullable : false, blank : false, maxSize : 1)
        staAprove (nullable : false, blank : false, maxSize :1)
        staOriRev (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }

    static mapping = {
        autoTimestamp false
        table 'TBL_EFAKTURPPNMASUKAN'
    }
}
