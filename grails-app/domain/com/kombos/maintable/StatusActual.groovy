package com.kombos.maintable

class StatusActual {

	String m452Id
	String m452StatusActual
	String m452StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m452Id column : 'M452_ID', sqlType : 'char(1)'
		table 'M452_STATUSACTUAL'
		m452StatusActual column : 'M452_StatusActual', sqlType : 'varchar(10)'
		m452StaDel column : 'M452_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m452StatusActual
	}
    static constraints = {
		m452Id (nullable : false, blank : false, unique : true, maxSize : 1)
		m452StatusActual (nullable : false, blank : false, maxSize : 10)
		m452StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
