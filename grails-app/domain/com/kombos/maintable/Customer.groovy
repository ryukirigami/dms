package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.HistoryCustomer

class Customer {
    CompanyDealer companyDealer
	String t102ID
	PeranCustomer peranCustomer
	Company company
    static hasMany = [histories: HistoryCustomer, mappingCustVehicles: MappingCustVehicle]
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	
	static mapping = {
        autoTimestamp false
        table 'T102_CUSTOMER'
		
		t102ID column : 'T102_ID', sqlType : 'char(12)'
		peranCustomer column : 'T102_M115_ID'
		company column : 'T102_T101_ID'
		staDel column : 'T102_StaDel', sqlType : 'char(1)'
	}
	
    static constraints = {
        companyDealer (blank:true, nullable:true)
		t102ID (nullable : true, blank : true, maxSize : 12)
		peranCustomer (nullable : true, blank : true)
		company (nullable : true, blank : true)
		staDel (nullable : true, blank : true)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t102ID
	}
}
