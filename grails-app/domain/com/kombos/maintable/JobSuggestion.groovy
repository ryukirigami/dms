package com.kombos.maintable

import com.kombos.customerprofile.CustomerVehicle

class JobSuggestion {
	CustomerVehicle customerVehicle
	Integer t503ID
	String t503KetJobSuggest
	Date t503TglJobSuggest
	String t503staJob
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		customerVehicle blank:false, nullable:false, unique:false, maxSize:50
		t503ID blank:false, nullable:false, unique:false, maxSize:4
		t503KetJobSuggest blank:false, nullable:false, maxSize:250
		t503TglJobSuggest blank:false, nullable:false
		t503staJob blank:false, nullable:false, maxSize:1
        staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T503_JOBSUGGESTION'
		customerVehicle column:'T503_T103_VinCode'
		t503ID column:'T503_ID', sqlTtype:'int'
		t503KetJobSuggest column:'T503_KetJobSuggest', sqlTtype:'varchar(250)'
		t503TglJobSuggest column:'T503_TglJobSuggest', sqlTtype:'date'
		t503staJob column:'T503_staJob', sqlTtype:'char(1)'
        staDel column:'T503_StaDel', sqlTtype:'char(1)'
	}
}
