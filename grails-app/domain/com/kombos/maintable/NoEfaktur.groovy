package com.kombos.maintable

class NoEfaktur {
    String suffix
    String prefix
    BigDecimal noAwal
    BigDecimal noAkhir
    BigDecimal sisa
    BigDecimal jumlah
    String statusHabis
    BigDecimal terakhirPakai
    Date tglAkhirPajak
    Date tglPermohonanPajak
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        suffix (blank: true, nullable: true)
        prefix (blank: true, nullable: true)
        noAwal (blank:true, nullable: true)
        noAkhir (blank: true, nullable: true)
        sisa (blank: true, nullable: true)
        jumlah (blank: true, nullable: true)
        statusHabis (blank: true, nullable: true)
        tglPermohonanPajak (blank: true, nullable: true)
        terakhirPakai (blank: true, nullable: true)
        tglAkhirPajak (blank: true, nullable: true)
    }

    static mapping = {
        table 'NO_PAJAK'

        suffix(column: 'SUFFIX')
        prefix(column: 'PREFIX')
        noAwal (column: 'NOAWAL')
        noAkhir(column: 'NOAKHIR')
        sisa (column: 'SISA')
        jumlah (column: 'JUMLAH')
        statusHabis (column: 'STATUSHABIS')
        terakhirPakai (column: 'TERAKHIRPAKAI')
        tglPermohonanPajak (column: 'TGLPERMOHONANPAJAK')
        tglAkhirPajak (column: 'TGLAKHIRPAJAK')

    }
}
