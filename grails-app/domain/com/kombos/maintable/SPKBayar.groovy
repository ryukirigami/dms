package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.SPK

class SPKBayar {
    CompanyDealer companyDealer
	SPK spk
	String t196ID
	Double t196JmlBayar
	String t196StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		spk nullable : false, blank : false, unique : true
		t196ID  nullable : false, blank : false, unique : true, maxSize : 20
		t196JmlBayar  nullable : false, blank : false
		t196StaDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T196_SPKBAYAR'
		spk column : 'T196_T191_ID'
		t196ID column : 'T196_ID', sqlType : 'char(20)'
		t196JmlBayar column : 'T196_JmlBayar'
		t196StaDel column : 'T196_StaDel', sqlType : 'char(1)'
	}
}
