package com.kombos.maintable

import com.kombos.administrasi.Operation
import com.kombos.customerprofile.FA

class JobFA {

	FA fa
	Operation operation
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		fa nullable : false, blank:false
		operation nullable : false, blank:false
		staDel nullable : false, maxSize :1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'M188_JOBFA'
		fa column : 'M188_M185_ID'
		operation column : 'M188_M053_JobID'
		staDel column : 'M188_StaDel', sqlType : 'char(1)'
	}
}
