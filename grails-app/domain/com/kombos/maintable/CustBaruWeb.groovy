package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class CustBaruWeb {
    CompanyDealer companyDealer
	String t187ID
	String t187UserName
	String t187Password
	String t187Email
	String t187NamaAwal
	String t187NamaAkhir
	String t187TelpHP
	String t187TelpRumah
	String t187JKel
	Date t187TanggalLahir
	String t187AlamatRumah
	String t187StaKonfirmasi
	String fullNama
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		t187ID nullable : false, blank : false, maxSize : 11, unique : true
		t187UserName nullable : false, blank : false, maxSize : 50
		t187Password nullable : false, blank : false, maxSize : 255
		t187Email nullable : false, blank : false, maxSize : 50
		t187NamaAwal nullable : false, blank : false, maxSize : 50
		t187NamaAkhir nullable : true, blank : true, maxSize : 50
		t187TelpHP nullable : false, blank : false, maxSize : 50
		t187TelpRumah nullable : true, blank : true, maxSize : 50
		t187JKel nullable : false, blank : false, maxSize : 1
		t187TanggalLahir nullable : false, blank : false
		t187AlamatRumah nullable : false, blank : false, maxSize : 50
		t187StaKonfirmasi nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T187_CUSTBARUWEB'
		t187ID column : 'T187_', sqlType : 'varchar(50)'
		t187UserName column : 'T187_UserName', sqlType : 'varchar(50)'
		t187Password column : 'T187_Password', sqlType : 'varchar(255)'
		t187Email column : 'T187_Email', sqlType : 'varchar(50)'
		t187NamaAwal column : 'T187_NamaAwal', sqlType : 'varchar(50)'
		t187NamaAkhir column : 'T187_NamaAkhir', sqlType : 'varchar(50)'
		t187TelpHP column : 'T187_TelpHP', sqlType : 'varchar(50)'
		t187TelpRumah column : 'T187_TelpRumah', sqlType : 'varchar(50)'
		t187JKel column : 'T187_JKel', sqlType : 'char(1)'
		t187TanggalLahir column : 'T187_TanggalLahir', sqlType : 'date'
		t187AlamatRumah column : 'T187_AlamatRumah', sqlType : 'varchar(50)'
		t187StaKonfirmasi column : 'T187_StaKonfirmasi', sqlType : 'char(1)'
		fullNama formula: "coalesce(T187_NamaAwal,'') || case when T187_NamaAkhir is not null then ' ' || T187_NamaAkhir end"
	} 
}
