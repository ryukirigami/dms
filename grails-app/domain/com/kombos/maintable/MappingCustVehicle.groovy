package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.CustomerVehicle

class MappingCustVehicle {
    CompanyDealer companyDealer
	Customer customer
	CustomerVehicle customerVehicle
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		customer nullable : false, blank : false//, unique : true
		customerVehicle nullable : false, blank : false//, unique : true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T186_MAPPINGCUSTVEHICLE'
		customer column : 'T186_T102_ID'
		customerVehicle column : 'T186_T103_VinCode'
        sort(id: "desc")
	}
}
