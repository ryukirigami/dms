package com.kombos.maintable

class TipeAlert {
	
	String m902Id
	String m902JenisAlert
	String m902StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M902_TIPEALERT'
		
		m902Id column : 'M902_ID', sqlType : 'char(1)'
		m902JenisAlert column : 'M902_JenisAlert', sqlType : 'varchar(50)'
		m902StaDel column : 'M902_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		m902Id (nullable : false, blank : false, unique : true, maxSize : 1)
		m902JenisAlert (nullable : false, blank : false, maxSize : 50)
		m902StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m902JenisAlert
	}
}
