package com.kombos.maintable

class PeranCustomer {
	int m115ID
	String m115NamaPeranCustomer
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m115ID blank:false, nullable:false, maxSize:4, unique:true
		m115NamaPeranCustomer blank:false, nullable:false, maxSize:50
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M115_PERANCUSTOMER"
		m115ID column:"M115ID", sqlType:"int"
		m115NamaPeranCustomer column:"M115_NamaPeranCustomer", sqlType:"varchar(50)"
		staDel column:"M115_StaDel", sqlType:"char(1)"
	}
	
	String toString() {
		return m115NamaPeranCustomer
	}
}
