package com.kombos.maintable

class NoFakturPajak {

    String noawal
    String noakhir
    Date   tglawalpajak
    Date   tglakhirpajak
    String staDel
    String statushabis
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess

    static constraints = {

        noawal (nullable : true, blank : true)
        noakhir (nullable : true, blank : true)
        staDel (nullable : true, blank : true)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete


    }
}
