package com.kombos.maintable

class MasterRangeDp {
    int nilaiParts1
    int nilaiParts2
    double persen
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        autoTimestamp false
        table 'MASTER_RANGE_DP'
        nilaiParts1 column : 'NILAI_PARTS_1'
        nilaiParts2 column : 'NILAI_PARTS_2'
        persen column : 'PERSEN'
    }

    static constraints = {
        nilaiParts1 (nullable : false, blank : false)
        nilaiParts2 (nullable : false, blank : false)
        persen (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
