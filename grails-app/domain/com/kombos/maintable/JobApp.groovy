package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.administrasi.RepairDifficulty
import com.kombos.reception.Reception

class JobApp {
    CompanyDealer companyDealer
	Reception reception
	Operation operation
	StatusWarranty statusWarranty
	Double t302HargaRp
	Double t302DiscRp
	Double t302DiscPersen
	Double t302TotalRp
	String t302staOkCancel
	String t302Approval
	String t302NoKeluhan
	Double t302LuasArea
	RepairDifficulty repairDifficulty
	String t302BumperPaintingText
	String t302xKet
	String t302xNamaUser
	String t302xDivisi
	String t302StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false, maxSize:20//, unique:true
		operation blank:false, nullable:true, maxSize:7//, unique:true
		statusWarranty blank:false, nullable:true, maxSize:4
		t302HargaRp blank:false, nullable:true, maxSize:8
		t302DiscRp blank:false, nullable:true, maxSize:8
		t302DiscPersen blank:false, nullable:true, maxSize:8
		t302TotalRp blank:false, nullable:true, maxSize:8
		t302staOkCancel blank:false, nullable:true, maxSize:1
		t302Approval blank:false, nullable:true, maxSize:10
		t302NoKeluhan blank:true, nullable:true, maxSize:50
		t302LuasArea blank:false, nullable:true, maxSize:8
		repairDifficulty blank:false, nullable:true, maxSize:4
		t302BumperPaintingText blank:false, nullable:true, maxSize:50
		t302xKet blank:false, nullable:true, maxSize:50
		t302xNamaUser blank:false, nullable:true, maxSize:20
		t302xDivisi blank:false, nullable:true, maxSize:20
		t302StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T301_JOBAPP'
		reception column:'T302_T401_NoWO'
		operation column:'T302_M053_JobID'
		statusWarranty column:'T302_M058_ID'
		t302HargaRp column:'T302_HargaRp'//, sqlType:'double'
		t302DiscRp column:'T302_DiscRp'//, sqlType:'double'
		t302DiscPersen column:'T302_DiscPersen'//, sqlType:'double'
		t302TotalRp column:'T302_TotalRp'//, sqlType:'double'
		t302staOkCancel column:'staOkCancel', sqlType:'char(1)'
		t302Approval column:'T302_Approval', sqlType:'char(10)'
		t302NoKeluhan column:'T302_NoKeluhan', sqlType:'varchar(20)'
		t302LuasArea column:'T302_LuasArea'//, sqlType:'double'
		repairDifficulty column:'T302_M042_ID'
		t302BumperPaintingText column:'T302_BumperPaintingText', sqlType:'varchar(50)'
		t302xKet column:'T302_xKet', sqlType:'varchar(50)'
		t302xNamaUser column:'T302_xNamaUser', sqlType:'varchar(20)'
		t302xDivisi column:'T302_xDivisi', sqlType:'varchar(20)'
		t302StaDel column:'T302_Stadel', sqlType:'char(1)'
	}

    def beforeUpdate() {
//        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
        lastUpdated = new Date()
        t302StaDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!t302StaDel)
            t302StaDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}
