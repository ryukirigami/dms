package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.Hobby

class CustomerHobby {
    CompanyDealer companyDealer
	Customer customer
	HistoryCustomer historyCustomer
	Hobby hobby
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
    	customer nullable : false, blank : false, unique :true
		historyCustomer nullable : false, blank : false, unique :true
		hobby nullable : false, blank : false, unique :true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T184_CUSTOMERHOBBY'
		customer column : 'T184_T102_ID'
		historyCustomer column : 'T184_T182_ID'
		hobby column : 'T184_M063_ID'
	}
}
