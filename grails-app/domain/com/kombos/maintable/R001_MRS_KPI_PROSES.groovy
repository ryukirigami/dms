package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class R001_MRS_KPI_PROSES {

    float cust_diundang_act	//FLOAT
    float cust_diundang_plan	//FLOAT
    float dm_dikirim	//FLOAT
    float dm_plan	//FLOAT
    float akurasi_dm_kirim	//FLOAT
    float akurasi_dm_act	//float
    float sms_kirim	//float
    float sms_plan	//float
    float akurasi_sms_diterima	//FLOAT
    float jml_sms_dikirim	//FLOAT
    float cust_ditelp_act	//FLOAT
    float cust_ditelp_plan	//FLOAT
    float cust_terhubung	//FLOAT
    float akurasi_cust_ditelp	//FLOAT
    float appointment_atcall	//FLOAT
    float appointment_after	//FLOAT
    Date  tanggal_report	//DATE
    CompanyDealer companyDealer
    int version
    int id
    static constraints = { //(nullable : false, blank : false,
//        r001_ID (nullable : false, blank : false, maxSize : 3)
    }

    static mapping = {
        table "R001_MRS_KPI_PROSES"
        id	                 column :"ID"
        version              column :"VERSION"
        companyDealer        column :"R001_M011_ID"
        cust_diundang_act	 column :"CUST_DIUNDANG_ACT"//FLOAT
        cust_diundang_plan	 column :"CUST_DIUNDANG_PLAN"//FLOAT
        dm_dikirim	         column :"DM_DIKIRIM"//FLOAT
        dm_plan	             column :"DM_PLAN"//FLOAT
        akurasi_dm_kirim	 column :"AKURASI_DM_KIRIM"//FLOAT
        akurasi_dm_act	     column :"AKURASI_DM_ACT"
        sms_kirim	         column :"SMS_KIRIM"
        sms_plan	         column :"SMS_PLAN"
        akurasi_sms_diterima column : "AKURASI_SMS_DITERIMA"
        jml_sms_dikirim	     column :"JML_SMS_DIKIRIM"
        cust_ditelp_act	     column :"CUST_DITELP_ACT"
        cust_ditelp_plan     column :"CUST_DITELP_PLAN"
        cust_terhubung       column :"CUST_TERHUBUNG"
        akurasi_cust_ditelp  column :"AKURASI_CUST_DITELP"
        appointment_atcall	 column :"APPOINTMENT_ATCALL"//FLOAT
        appointment_after	 column :"APPOINTMENT_AFTER"//FLOAT
        tanggal_report	     column :"TANGGAL_REPORT"//DATE
    }
}
