package com.kombos.maintable

class StatusReminder {

	String m202Id
	String m202NamaStatusReminder
	String m202StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m202Id column :'M202_ID', sqltype :'char', length : 2
		table 'M202_STATUSREMINDER'
		m202NamaStatusReminder column : 'M202_NamaStatusReminder', sqltype : 'varchar', length : 20
		m202StaDel column : 'M202_StaDel', sqltype : 'char', length :1
	}
	
	String toString(){
		return m202NamaStatusReminder
	}
    static constraints = {
		m202Id (nullable : true, unique : true, blank : true, maxSize : 2)
		m202NamaStatusReminder (nullable : true, blank : true, maxSize : 20)
		m202StaDel (nullable : true, blank : true, maxSize : 1)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
