package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.Reception

class InvoiceTotal {
    CompanyDealer companyDealer
	Reception reception
	Double t700TotalRp
	Double t700BayarRp
	Double t700SisaRp
	String t700StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false, unique:true
		t700TotalRp blank:false, nullable:false, maxSize:8
		t700BayarRp blank:false, nullable:false, maxSize:8
		t700SisaRp blank:false, nullable:false, maxSize: 8
		t700StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T700_INVOICETOTAL'
		reception column:'T700_T401_NoWO'
		t700TotalRp column:'T700_TotalRp'//, sqlType:'double'
		t700BayarRp column:'T700_BayarRp'//, sqlType:'double'
		t700SisaRp column:'T700_SisaRp'//, sqlType:'double'
		t700StaDel column:'T700_StaDel', sqlType:'char(1)'
	}
}
