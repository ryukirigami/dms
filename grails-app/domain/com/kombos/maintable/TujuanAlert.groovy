package com.kombos.maintable

import com.kombos.administrasi.JenisAlert
//import com.kombos.administrasi.UserProfile
import com.kombos.baseapp.sec.shiro.User

class TujuanAlert {
	
	JenisAlert jenisAlert
	User userProfile
	String m903StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M903_TUJUANALERT'
		
		jenisAlert column : 'M903_M901_ID'
		userProfile column : 'M903_T001_IDUser'
		m903StaDel column : 'M903_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		jenisAlert (nullable : false, blank : false)
		userProfile (nullable : false, blank : false)
		m903StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
}
