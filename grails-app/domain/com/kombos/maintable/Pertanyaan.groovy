package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerFollowUp.PertanyaanFu
import com.kombos.customerFollowUp.RealisasiFU

class Pertanyaan {
    //Reception reception
//    FollowUp followUp
    CompanyDealer companyDealer
    RealisasiFU realisasiFU
    Integer t803ID
    PertanyaanFu pertanyaanFu
    Date t803TglRespon
    String t803JawabanFu
    String t803StaPuasTdkPuas
    String t803AlasanTdkPuas
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        //reception (nullable : false, blank : false)
//        followUp (nullable : false, blank : false)
        realisasiFU (nullable : false, blank : false)
        t803ID (nullable : false, blank : false, maxSize : 4)
        pertanyaanFu (nullable : false, blank : false)
        t803TglRespon (nullable : false, blank : false)
        t803JawabanFu (nullable : true, blank : true, maxSize : 250)
        t803StaPuasTdkPuas (nullable : true, blank : true, maxSize : 250)
        t803AlasanTdkPuas (nullable : true, blank : true, maxSize : 250)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T803_PERTANYAAN'
        //reception column: 'T803_T801_T401_NoWO'
//        followUp column: 'T803_T801_ID'
        realisasiFU column: 'T803_T802_ID'
        t803ID column: 'T803_ID', sqlType: 'int'
        pertanyaanFu column: 'T803_M803_Pertanyaan'//, sqlType: 'varchar(250)'
        t803TglRespon column: 'T803_TglRespon'//, sqlType: 'date'
        t803JawabanFu column: 'T803_JawabanFu', sqlType: 'varchar(250)'
        t803StaPuasTdkPuas column: 'T803_StaPuasTdkPuas', sqlType: 'varchar(250)'
        t803AlasanTdkPuas column: 'T803_AlasanTdkPuas', sqlType: 'varchar(250)'
    }
}
