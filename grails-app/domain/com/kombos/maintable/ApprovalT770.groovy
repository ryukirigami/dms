package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KegiatanApproval
import com.kombos.parts.StatusApproval

class ApprovalT770 {
	static final FKS_SEPARATOR = '#'
    CompanyDealer companyDealer
	KegiatanApproval kegiatanApproval
	String t770FK
	String t770NoDokumen
	String t770xNamaUser // beforeInsert
	Date t770TglJamSend
	StatusApproval t770Status // beforeInsert
	Integer t770CurrentLevel // beforeInsert
	String t770Pesan
	Double t770JumlahSeharusnya //TODO ga tau buat apa
	Double t770JumlahDiajukan //TODO ga tau buat apa
	String staDel // beforeInsert
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		kegiatanApproval blank:false, nullable:false, maxSize:4//, unique:true
		t770FK blank:false, nullable:false, maxSize:500 //, unique:true
		//t770ID blank:false, nullable:false, maxSize:4 , unique:true
		t770NoDokumen blank:false, nullable:false, maxSize:50
		t770xNamaUser blank:false, nullable:false, maxSize:20
		t770TglJamSend blank:false, nullable:false, maxSize:4
		t770Status blank:false, nullable:false, maxSize:1
		t770CurrentLevel blank:false, nullable:false, maxSize:4
		t770Pesan blank:false, nullable:false, maxSize:1000
		t770JumlahSeharusnya blank:false, nullable:true, maxSize:8 
		t770JumlahDiajukan blank:false, nullable:true, maxSize: 8
		staDel blank:false, nullable:false, maxSize: 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T770_APPROVAL'
		kegiatanApproval column:'T770_M770_IDApproval'
		t770FK column:'T770_FK', sqlType:'varchar(500)'
		id column:'T770_ID', sqlType:'int'
		//t770ID column:'T770_ID', sqlType:'int'
		t770NoDokumen column:'T770_NoDokumen', sqlType:'varchar(50)'
		t770xNamaUser column:'T770_xNamaUser', sqlType:'varchar(20)'
		t770TglJamSend column:'T770_TglJamSend'//, sqlType: 'date'
		t770Status column:'T770_Status', sqlType:'char(1)'
		t770CurrentLevel column:'T770_CurrentLevel', sqlType:'int'
		t770Pesan column:'T770_Pesan', sqlType:'varchar(1000)'
		t770JumlahSeharusnya column:'T770_JumlahSeharusnya'//, sqlType:'double'
		t770JumlahDiajukan column:'T770_JumlahDiajukan'//, sqlType:'double'
		staDel column:'T770_Stadel', sqlType:'char(1)'
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "UPDATE"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "INSERT"
//		lastUpdated = new Date()
		staDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				t770xNamaUser = createdBy 
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "INSERT"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
			t770xNamaUser = createdBy
		}
		if(!t770Status)
			t770Status = StatusApproval.WAIT_FOR_APPROVAL
		if(!t770CurrentLevel)
			t770CurrentLevel = 0
		
	}
}