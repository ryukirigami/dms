package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Invoice

class FakturPajak {
    CompanyDealer companyDealer
    String t711DeskripsiAkhirTahun
	String t711NoFakturPajak
	String t711JenisFP
	String t711NoFakturPajakReff
	InvoiceT701 invoiceT701
	String t711Tahun
	String t711ID
	String t711KodeFP
	String t711SeriFP
	Date t711TglKukuh
	Double t711BiayaJasa
	Double t711BiayaGoods
	Double t711HargaJual
	Double t711PotHarga
	Double t711UangMuka
	Double t711DasarKenaPajak
	Double t711PPN
    String t711nama
    String t711alamat
    String t711npwp
    String t711deskripsi
	Date t711TglJamFP
	String t711xKet
	String t711xNamaUser
	String t711xDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer blank:true, nullable: true
		t711DeskripsiAkhirTahun blank: true , nullable: true, maxSize: 255
        t711NoFakturPajak  blank:true, nullable:true, maxSize:20
		t711JenisFP  blank:true, nullable:true, maxSize:1
		t711NoFakturPajakReff blank:true, nullable:true, maxSize:20
        invoiceT701 blank:true, nullable:true
		t711Tahun blank:true, nullable:true, maxSize:4
		t711ID blank:true, nullable:true, maxSize:8
		t711KodeFP blank:true, nullable:true, maxSize:50
		t711SeriFP blank:true, nullable:true, maxSize:50
		t711TglKukuh blank:true, nullable:true
		t711BiayaJasa blank:true, nullable:true
		t711BiayaGoods blank:true, nullable:true
		t711HargaJual blank:true, nullable:true
		t711PotHarga blank:true, nullable:true
		t711UangMuka blank:true, nullable:true
		t711DasarKenaPajak blank:true, nullable:true
		t711PPN blank:true, nullable:true
		t711TglJamFP blank:true, nullable:true
		t711xKet blank:true, nullable:true, maxSize:50
		t711xNamaUser blank:true, nullable:true, maxSize:20
		t711xDivisi blank:true, nullable:true, maxSize:20
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete


         t711nama blank:true, nullable:true
        t711alamat blank:true, nullable:true
        t711npwp blank:true, nullable:true
       t711deskripsi blank:true, nullable:true
    }
	
	static mapping = {
        autoTimestamp false
        table 'T711_FAKTURPAJAK'
        companyDealer column:'T711_M011_Cabang'
        t711DeskripsiAkhirTahun column: 'T711_DeskripsiAkhirTahun', sqlType: 'varchar(255)'
        t711NoFakturPajak column:'T711_NoFakturPajak', sqlType:'varchar(20)'
		t711JenisFP column:'T711_JenisFP', sqlType:'char(1)'
		t711NoFakturPajakReff column:'T711_NoFakturPajakReff', sqlType:'varchar(20)'
        invoiceT701 column:'T711_T701_NoInv'
		t711Tahun column:'T711_Tahun', sqlType:'char(4)'
		t711ID column:'T711_ID', sqlType:'char(8)'
		t711KodeFP column:'T711_KodeFP', sqlType:'varchar(50)'
		t711SeriFP column:'T711_SeriFP', sqlType:'varchar(50)'
		t711TglKukuh column:'T711_TglKukuh'//, sqlType: 'date'
		t711BiayaJasa column:'T711_BiayaJasa'
		t711BiayaGoods column:'T711_BiayaGoods'
		t711HargaJual column:'T711_HargaJual'
		t711PotHarga column:'T711_PotHarga'
		t711UangMuka column:'T711_UangMuka'
		t711DasarKenaPajak column:'T711_DasarKenaPajak'
		t711PPN column:'T711_PPN'
		t711TglJamFP column:'T711_TglJamFP'//, sqlType: 'date'
		t711xKet column:'T711_xKet', sqlType:'varchar(50)'
		t711xNamaUser column:'T711_xNamaUser', sqlType:'varchar(20)'
		t711xDivisi column:'T711_xDivisi', sqlType:'varchar(20)'
		staDel column:'T711_StaDel', sqlType:'char(1)'

        t711nama column:'T711_NAMA'
        t711alamat column:'T711_ALAMAT'
        t711npwp column:'T711_NPWP'
        t711deskripsi column:'T711_DESKRIPSI'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        staDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}