package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class EFaktur {

    CompanyDealer companyDealer
    String docNumber // SESUAKAN DENGAN JENIS INPUTAN EFAKTUR
    Date tanggal
    String noUrutInvoice// nourutInv untuk format excell yang belum pakai DMS
    String FK
    String KD_JENIS_TRANSAKSI
    String FG_PENGGANTI
    String NOMOR_FAKTUR
    String MASA_PAJAK
    String TAHUN_PAJAK
//    String TANGGAL_FAKTUR
    Date TANGGAL_FAKTUR
    String NPWP  // Menunjukan NPWP punya nama orang pertama
    String NAMA  //Menunjukan nama orang pertama
    String ALAMAT_LENGKAP
    BigDecimal JUMLAH_DPP
    BigDecimal JUMLAH_PPN
    BigDecimal JUMLAH_PPNBM
    String ID_KETERANGAN_TAMBAHAN
    String FG_UANG_MUKA
    String UANG_MUKA_DPP
    String UANG_MUKA_PPN
    String UANG_MUKA_PPNBM
    String REFERENSI
    String LT
    String NPWP2  // MENUNJUKAN NPWP2
    String NAMA2 // MENUNJUKKAN NAMA2
    String JALAN
    String BLOK
    String NOMOR
    String RT
    String RW
    String KECAMATAN
    String KELURAHAN
    String KABUPATEN
    String PROPINSI
    String KODE_POS
    String NOMOR_TELEPON
    String OF_
    String KODE_OBJEK
    String NAMA_OBJEK // CONTOH DI DOC NYA EK40GMHL-11 TT (YOM 40HP M.Tanah)
    BigDecimal HARGA_SATUAN
    BigDecimal JUMLAH_BARANG
    BigDecimal HARGA_TOTAL
    BigDecimal DISKON
    BigDecimal DPP
    BigDecimal PPN
    String TARIF_PPNBM
    BigDecimal PPNBM
    String staDel
    String staOriRev
    String jenisInputan
    String staAprove   // 1 = APPROVED, 2 = REJECTED, 0 = WAIT_APPROVE
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        companyDealer (blank:true, nullable:true)
        jenisInputan (blank: true, nullable: true)
        noUrutInvoice (blank: true, nullable: true)
        FK (blank: true, nullable: true)
        docNumber (blank: true, nullable: true)
        tanggal (blank: true, nullable: true)
        KD_JENIS_TRANSAKSI(blank: true, nullable: true)
        FG_PENGGANTI (blank: true, nullable: true)
        NOMOR_FAKTUR (blank: true, nullable: true)
        MASA_PAJAK (blank: true, nullable: true)
        TAHUN_PAJAK (blank: true, nullable: true)
        TANGGAL_FAKTUR (blank: true, nullable: true)
        NPWP (blank: true, nullable: true)
        NAMA (blank: true, nullable: true)
        ALAMAT_LENGKAP (blank: true, nullable: true)
        JUMLAH_DPP (blank: true, nullable: true)
        JUMLAH_PPN (blank: true, nullable: true)
        JUMLAH_PPNBM (blank: true, nullable: true)
        ID_KETERANGAN_TAMBAHAN (blank: true, nullable: true)
        FG_UANG_MUKA (blank: true, nullable: true)
        UANG_MUKA_DPP (blank: true, nullable: true)
        UANG_MUKA_PPN (blank: true, nullable: true)
        UANG_MUKA_PPNBM (blank: true, nullable: true)
        REFERENSI (blank: true, nullable: true)
        LT (blank: true, nullable: true)
        NPWP2 (blank: true, nullable: true)
        NAMA2 (blank: true, nullable: true)
        JALAN (blank: true, nullable: true)
        BLOK (blank: true, nullable: true)
        NOMOR (blank: true, nullable: true)
        RT (blank: true, nullable: true)
        RW (blank: true, nullable: true)
        KECAMATAN (blank: true, nullable: true)
        KELURAHAN (blank: true, nullable: true)
        KABUPATEN (blank: true, nullable: true)
        PROPINSI (blank: true, nullable: true)
        KODE_POS (blank: true, nullable: true)
        NOMOR_TELEPON (blank: true, nullable: true)
        OF_ (blank: true, nullable: true)
        KODE_OBJEK (blank: true, nullable: true)
        NAMA_OBJEK (blank: true, nullable: true)
        HARGA_SATUAN (blank: true, nullable: true)
        JUMLAH_BARANG (blank: true, nullable: true)
        HARGA_TOTAL (blank: true, nullable: true)
        DISKON (blank: true, nullable: true)
        DPP (blank: true, nullable: true)
        PPN (blank: true, nullable: true)
        TARIF_PPNBM (blank: true, nullable: true)
        PPNBM (blank: true, nullable: true)
        staDel (nullable : false, blank : false, maxSize : 1)
        staAprove (nullable : false, blank : false, maxSize :1)
        staOriRev (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }


    static mapping = {
        autoTimestamp false
        table 'TBL_EFAKTUR'
    }

}
