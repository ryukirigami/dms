package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.MasterPanel
import com.kombos.administrasi.Stall
import com.kombos.production.Deffect
import com.kombos.production.InspectionQC
import com.kombos.reception.Reception

class JobDeffect {
    CompanyDealer companyDealer
	Reception reception
	Stall stall
	InspectionQC inspectionQC
	MasterPanel masterPanel
	Deffect deffect
	String t503StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        reception blank:false, nullable:false, unique:true, maxSize:20
		stall  blank:false, nullable:false, unique:true, maxSize:8
		inspectionQC blank:false, nullable:false, unique:true, maxSize:4
		masterPanel blank:false, nullable:false, unique:true, maxSize:4
		deffect blank:false, nullable:false, unique:true, maxSize:2
		t503StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T503_JOBDEFFECT'
		reception column:'T503_T401_NoWO'
		stall column:'T507_M022_StallID'
		inspectionQC column:'T503_T507_ID'
		masterPanel column:'T503_M094_ID'
		deffect column:'T503_M503_ID'
		t503StaDel column:'T503_StaDel', sqlType:'char(1)'
	}
}
