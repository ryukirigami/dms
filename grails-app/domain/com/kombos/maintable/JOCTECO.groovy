package com.kombos.maintable

import com.kombos.reception.Reception

class JOCTECO {
	Reception reception
	String t601staSPK
	String t601BawaSPK
	String t601staAsuransi
	String t601BawaAsuransi
	String t601staCash
	String t601staOkCancelJOC
	String t601NamaSAJOC
	Date t601TglJamJOC
	Date t601TglJamJOCSelesai
	String t601xKetJOC
	String t601staOkCancelExp
	String t601staKonfirmasiFU
	String t601xKetExp
	String t601StaJOC
	String t601xKet
	String t601xNamaUser
	String t601xDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		reception blank:true, nullable:true
		t601staSPK blank:true, nullable:true, maxSize:1
		t601BawaSPK blank:true, nullable:true, maxSize:1
		t601staAsuransi blank:true, nullable:true, maxSize:1
		t601BawaAsuransi blank:true, nullable:true, maxSize:1
		t601staCash blank:true, nullable:true, maxSize:1
		t601staOkCancelJOC blank:true, nullable:true, maxSize:1
		t601NamaSAJOC blank:true, nullable:true, maxSize:50
		t601TglJamJOC blank:true, nullable:true
		t601TglJamJOCSelesai blank:true, nullable:true
		t601xKetJOC blank:true, nullable:true, maxSize:50
		t601staOkCancelExp blank:true, nullable:true, maxSize:1
		t601staKonfirmasiFU blank:true, nullable:true, maxSize:1
		t601xKetExp blank:true, nullable:true, maxSize:50
		t601StaJOC blank:true, nullable:true, maxSize:1
		t601xKet blank:true, nullable:true, maxSize:50
		t601xNamaUser blank:true, nullable:true, maxSize:20
		t601xDivisi blank:true, nullable:true, maxSize:20
		staDel blank:true, nullable:true, maxSize:1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T601_JOCTECO'
		reception column:'T601_T401_NoWO'
		t601staSPK column:'T601_staSPK', sqlType:'char(1)'
		t601BawaSPK column:'T601_BawaSPK', sqlType:'char(1)'
		t601staAsuransi column:'T601_staAsuransi', sqlType:'char(1)'
		t601BawaAsuransi column:'T601_BawaAsuransi', sqlType:'char(1)'
		t601staCash column:'T601_staCash', sqlType:'char(1)'
		t601staOkCancelJOC column:'T601_staOkCancelJOC', sqlType:'char(1)'
		t601NamaSAJOC column:'T601_NamaSAJOC', sqlType:'varchar(50)'
		t601TglJamJOC column:'T601_TglJamJOC'//, sqlType: 'date'
		t601TglJamJOCSelesai column:'T601_TglJamJOCSelesai'//, sqlType: 'date'
		t601xKetJOC column:'T601_xKetJOC', sqlType:'varchar(50)'
		t601staOkCancelExp column:'T601_staOkCancelExp', sqlType:'char(1)'
		t601staKonfirmasiFU column:'T601_staKonfirmasiFU', sqlType:'char(1)'
		t601xKetExp column:'T601_xKetExp', sqlType:'varchar(50)'
		t601StaJOC column:'T601_StaJOC', sqlType:'char(1)'
		t601xKet column:'T601_xKet', sqlType:'varchar(50)'
		t601xNamaUser column:'T601_xNamaUser', sqlType:'varchar(20)'
		t601xDivisi column:'T601_xDivisi', sqlType:'varchar(20)'
		staDel column:'T601_StaDel', sqlType:'char(1)'
		
	}
}
