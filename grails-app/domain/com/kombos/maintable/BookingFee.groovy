package com.kombos.maintable

import com.kombos.administrasi.Bank
import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.MesinEdc
import com.kombos.administrasi.RateBank

class BookingFee {
    CompanyDealer companyDealer
	Kwitansi kwitansi
	Integer t721ID
	Date t721TglJamBookingFee
	MetodeBayar metodeBayar
	Bank bank
	MesinEdc mesinEdc
	RateBank rateBankBankCharge
	String t721NoKartu
	Double t721JmlhBayar
	String t721NoBuktiTransfer
	String t721xNamaUser
	String t721xDivisi
	String t721StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		kwitansi blank:false, nullable:false, unique:'t721ID'
		t721ID blank:false, nullable:false, maxSize:4
		t721TglJamBookingFee blank:false, nullable:false
		metodeBayar blank:false, nullable:false
		bank blank:true, nullable:true
		mesinEdc blank:true, nullable:true
		rateBankBankCharge blank:true, nullable:true
		t721NoKartu blank:true, nullable:true, maxSize:50
		t721JmlhBayar blank:false, nullable:false
		t721NoBuktiTransfer blank:true, nullable:true, maxSize:50
		t721xNamaUser blank:true, nullable:true, maxSize:20
		t721xDivisi blank:true, nullable:true, maxSize:20
		t721StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T721_BOOKINGFEE'
		kwitansi column:'T721_T722_NoKwitansi'
		t721ID column:'T721_ID', sqlType:'int'
		t721TglJamBookingFee column:'T721_TglJamBookingFee'//, sqlType: 'date'
		metodeBayar column:'T721_M701_ID'
		bank column:'T721_M702_ID'
		mesinEdc column:'T721_M704_ID'
		rateBankBankCharge column:'T721_M703_BankCharge'
		t721NoKartu column:'T721_NoKartu', sqlType:'varchar(50)'
		t721JmlhBayar column:'T721_JmlhBayar'
		t721NoBuktiTransfer column:'T721_NoBuktiTransfer', sqlType:'varchar(50)'
		t721xNamaUser column:'T721_xNamaUser', sqlType:'varchar(20)'
		t721xDivisi column:'T721_xDivisi', sqlType:'varchar(20)'
		t721StaDel column:'T721_StaDel', sqlType:'char(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        t721StaDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!t721StaDel)
            t721StaDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }

}