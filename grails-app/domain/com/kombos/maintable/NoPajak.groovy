package com.kombos.maintable

class NoPajak {

    Integer noawal
    Integer noakhir
    Integer terakhirpakai
    Integer sisa
    Date   tglawalpajak
    Date   tglakhirpajak
    String staDel
    String statushabis
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess

    static constraints = {
        tglawalpajak  (nullable : true, blank : true)
        tglakhirpajak  (nullable : true, blank : true)
        noawal (nullable : true, blank : true)
        noakhir (nullable : true, blank : true)
        terakhirpakai (nullable : true, blank : true)
        sisa (nullable: true, blank : true)
        staDel (nullable : true, blank : true)
        statushabis (nullable : true, blank : true)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete



    }

    static mapping = {
        autoTimestamp false
    }
}
