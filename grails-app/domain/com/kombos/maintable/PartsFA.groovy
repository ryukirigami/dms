package com.kombos.maintable

import com.kombos.customerprofile.FA
import com.kombos.parts.Goods

class PartsFA {

	FA fa
	Goods goods
	Integer m187Qty1
	Integer m187Qty2
	String staDel
    Date dateCreated = new Date() // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated = new Date()  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		fa nullable: false, blank: false
		goods nullable : false, blank : false
		m187Qty1 nullable: true, blank: true, maxSize : 4
		m187Qty2 nullable: true, blank: true, maxSize : 4
		staDel nullable: false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M187_PARTSFA'
		fa column : 'M187_M185_ID'
		goods column : 'M187_M111_ID'
		m187Qty1 column : 'M187_Qty1', sqlType : 'int'
		m187Qty2 column : 'M187_Qty2', sqlType : 'int'
		staDel column : 'M187_StaDel',sqlType : 'char(1)'
	}
}

