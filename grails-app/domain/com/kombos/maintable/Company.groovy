package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KabKota
import com.kombos.administrasi.Kecamatan
import com.kombos.administrasi.Kelurahan
import com.kombos.administrasi.Provinsi
import com.kombos.customerprofile.JenisPerusahaan

class Company {
	
	CompanyDealer companyDealer
	
	// Customer Company Profile	
	String kodePerusahaan
	JenisPerusahaan jenisPerusahaan
	String namaPerusahaan
	
	// Alamat Korespondensi
	String alamatPerusahaan
	Provinsi provinsi
	KabKota kabKota
	Kecamatan kecamatan
	Kelurahan kelurahan
	String kodePos
	
	// Alamat NPWP
	String alamatNPWP
	Provinsi provinsiNPWP
	KabKota kabKotaNPWP
	Kecamatan kecamatanNPWP
	Kelurahan kelurahanNPWP
	String kodePosNPWP
	
	// Customer Company Profile
	String telp
	String fax
	String website
	String noNPWP
	
	// Customer Company PIC
	String namaDepanPIC
	String namaBelakangPIC
	String jenisKelaminPIC
	Date tglLahirPIC
	String telpPIC
	String hpPIC
	NamaJabatanPIC jabatanPIC
	String emailPIC
	String alamatPIC
	String rtPIC
	String rwPIC
	Provinsi provinsiPIC
	KabKota kabKotaPIC
	Kecamatan kecamatanPIC
	Kelurahan kelurahanPIC
	String kodePosPIC
	
	String t101GelarD	
	String t101GelarB	
	String t101Ket
	String t101xNamaUser
	String t101xNamaDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


	static mapping = {
        autoTimestamp false
        table 'T101_COMPANY'
		
		// Customer Company Profile
		kodePerusahaan column : 'T101_ID', sqlType : 'char(12)'		
		jenisPerusahaan column : 'T101_JenisPerusahaan'//, sqlType : 'varchar(50)'
		namaPerusahaan column : 'T101_NamaPerusahaan', sqlType : 'varchar(50)'
		
		// alamat
		alamatPerusahaan column : 'T101_AlamatPerusahaan', sqlType : 'varchar(100)'
		provinsi column : 'T101_M001_ID'
		kabKota column : 'T101_M002_ID'
		kecamatan column : 'T101_M003_ID'
		kelurahan column : 'T101_M004_ID'
		kodePos column : 'T101_KodePosCompany', sqlType : 'varchar(5)'
		
		// Customer Company Profile
		telp column : 'T101_TelpPerusahaan', sqlType : 'varchar(100)'
		fax column : 'T101_FaxPerusahaan', sqlType : 'varchar(100)'
		website column : 'T101_Website', sqlType : 'varchar(50)'
		noNPWP column : 'T101_NoNPWP', sqlType : 'varchar(20)'
		
		// NPWP
		alamatNPWP column : 'T101_AlamatNPWP', sqlType : 'varchar(100)'
		provinsiNPWP column : 'T101_M001_IDNPWP'
		kabKotaNPWP column : 'T101_M002_IDNPWP'
		kecamatanNPWP column : 'T101_M003_IDNPWP'
		kelurahanNPWP column : 'T101_M004_IDNPWP'
		kodePosNPWP column : 'T101_KodePosNPWP', sqlType : 'varchar(5)'
		
		//PIC
		t101GelarD column : 'T101_GelarD', sqlType : 'varchar(50)'
		namaDepanPIC column : 'T101_NamaDepan', sqlType : 'varchar(50)'
		namaBelakangPIC column : 'T101_NamaBelakang', sqlType : 'varchar(50)'
		t101GelarB column : 'T101_GelarB', sqlType : 'varchar(50)'
		jabatanPIC column : 'T101_M093_ID'
		jenisKelaminPIC column : 'T01_JenisKelamin', sqlType : 'char(1)'
		tglLahirPIC column : 'T101_TglLahir', sqlType : 'date'
		alamatPIC column : 'T101_Alamat', sqlType : 'varchar(100)'
		rtPIC column : 'T101_RT1', sqlType : 'varchar(20)'
		rwPIC column : 'T101_RTW2', sqlType : 'varchar(20)'
		provinsiPIC column : 'T101_M001_ID1'
		kabKota column : 'T101_M002_ID1'
		kecamatan column : 'T101_M003_ID1'
		kelurahan column : 'T101_M004_ID1'
		kodePosPIC column : 'T101_KodePosPIC', sqlType : 'varchar(5)'
		telpPIC column : 'T101_NoTelp', sqlType : 'varchar(40)'
		hpPIC column : 'T101_HP', sqlType : 'varchar(40)'
		emailPIC column : 'T101_Email', sqlType : 'varchar(20)'
		
		
		t101Ket column : 'T101_Ket', sqlType : 'varchar(50)'
		t101xNamaUser column : 'T101_xNamaUser', sqlType : 'varchar(20)'
		t101xNamaDivisi column : 'T101_xNamaDivisi', sqlType : 'varchar(20)'
		staDel column : 'T101_StaDel', sqlType : 'char(1)'
		
	}
	
	

    static constraints = {
		kodePerusahaan (nullable:  false, blank : false, unique : true )
		companyDealer (nullable:  true, blank : true)
		jenisPerusahaan (nullable:  false, blank : false)
		namaPerusahaan (nullable:  false, blank : false) 
		alamatPerusahaan (nullable:  false, blank : false)
		provinsi (nullable:  true, blank : true)
		kabKota (nullable:  true, blank : true)
		kecamatan (nullable:  true, blank : true)
		kelurahan (nullable:  true, blank : true)
		kodePos (nullable:  true, blank : true)
		telp (nullable:  false, blank : false)
		fax (nullable:  true, blank : true)
		website (nullable:  true, blank : true)
		t101GelarD (nullable:  true, blank : true)
		namaDepanPIC (nullable:  false, blank : false) 
		namaBelakangPIC (nullable:  true, blank : true)
		t101GelarB (nullable:  true, blank : true)
		jabatanPIC (nullable:  true, blank : true)
		jenisKelaminPIC (nullable:  true, blank : true)
		tglLahirPIC (nullable:  true, blank : true)
		alamatPIC (nullable:  false, blank : false) 
		rtPIC (nullable:  true, blank : true)
		rwPIC (nullable:  true, blank : true)
		provinsiPIC (nullable:  true, blank : true)
		kabKotaPIC (nullable:  true, blank : true)
		kecamatanPIC (nullable:  true, blank : true)
		kelurahanPIC (nullable:  true, blank : true)
		kodePosPIC (nullable:  true, blank : true)
		telpPIC (nullable:  false, blank : false) 
		hpPIC (nullable:  false, blank : false) 
		emailPIC (nullable:  true, blank : true)
		noNPWP (nullable:  true, blank : true)
		alamatNPWP (nullable:  false, blank : false) 
		provinsiNPWP (nullable:  true, blank : true)
		kabKotaNPWP (nullable:  true, blank : true)
		kecamatanNPWP (nullable:  true, blank : true)
		kelurahanNPWP (nullable:  true, blank : true)
		kodePosNPWP (nullable:  true, blank : true)
		t101Ket (nullable:  true, blank : true)
		t101xNamaUser (nullable:  true, blank : true)
		t101xNamaDivisi (nullable:  true, blank : true)
		staDel (nullable:  false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return namaPerusahaan
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "update"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "insert"
//		lastUpdated = new Date()
		staDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "_SYSTEM_"
			updatedBy = createdBy
		}
	}
}

