package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.Reception
import com.kombos.woinformation.Prediagnosis

class DiagnosisDetail {
    CompanyDealer companyDealer
	Reception reception
	Prediagnosis prediagnosis
	Integer t416ID
	String t416staAwalUlang
	String t416System
	String t416DTC
	String t416Desc
	String t416StatusPCH
	String t416Freeze
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception  blank:false, nullable:false//, unique:true
		prediagnosis blank:false, nullable:false//, unique:true
		t416ID blank:false, nullable:true, maxSize:4 //,unique:true
		t416staAwalUlang blank:false, nullable:false, maxSize:1
		t416System blank:false, nullable:false, maxSize:50
		t416DTC blank:false, nullable:false, maxSize:50
		t416Desc blank:false, nullable:false, maxSize:50
		t416StatusPCH blank:false, nullable:false, maxSize:1
		t416Freeze blank:false, nullable:false, maxSize:50
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T416_DIAGNOSISDETAIL'
		reception column:'T416_T401_NoWO'
		prediagnosis column:'T416_T406_ID'
		t416ID column:'T416_ID', sqlType:'int'
		t416staAwalUlang column:'T416_staAwalUlang', sqlType:'char(1)'
		t416System column:'T416_System', sqlType:'varchar(50)'
		t416DTC column:'T416_DTC', sqlType:'varchar(50)'
		t416Desc column:'T416_Desc', sqlType:'varchar(50)'
		t416StatusPCH column:'T416_StatusPCH', sqlType:'char(1)'
		t416Freeze column:'T416_Freeze', sqlType:'varchar(50)'
		t416StaDel column:'T416_StaDel', sqlType:'char(1)'
	}
}
