package com.kombos.maintable

class KategoriWaktuFu {
	
	Integer m804Id
	String m804Kategori
	Date m804Jam1
	Date m804Jam2
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M804_KATEGORIWAKTUFU'
		
		m804Kategori column : 'M804_Kategori', sqlType : 'varchar(50)'
		m804Id column : 'M804_ID', sqlType : 'int'
		m804Jam1 column : 'M804_Jam1', sqlType : 'date'
		m804Jam2 column : 'M804_Jam2', sqlType : 'date'
	}

    static constraints = {
		m804Id (nullable : false, blank : false, unique : true, maxSize : 4)
		m804Kategori (nullable : false, blank : false, maxSize : 50)
		m804Jam1 (nullable : false, blank : false)
		m804Jam2 (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m804Kategori
	}
}
