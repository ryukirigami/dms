package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class DpBalik {
    CompanyDealer companyDealer
    Date m006Tmt
    Integer m006SelisihDpHari
    Double m006PersenDpBalik
    String m006StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (nullable : false, blank : false)
        m006Tmt (nullable : false, blank : false)
        m006SelisihDpHari (nullable : false, blank : false)
        m006PersenDpBalik (nullable : false, blank : false)
        m006StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        companyDealer column: "M006_M011_ID", unique: true
        m006Tmt column: "M006_TMT" , sqlType : "date", unique: true
        m006SelisihDpHari column: "M006_SelisihDPHari" , sqlType : "int", unique: true
        m006PersenDpBalik column: "M006_PersenDPBalik"
        m006StaDel column: "M006_StaDel", sqlType: "char(1)"
        table "M006_DPBALIK"
    }

    String toString(){
        return m006PersenDpBalik;
    }
}
