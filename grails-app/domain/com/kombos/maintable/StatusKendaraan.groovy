package com.kombos.maintable

class StatusKendaraan {
	
	String m999Id
	String m999NamaStatusKendaraan
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'M999_STATUS_KENDARAAN'
		
		m999Id column : 'M999_ID', sqlType : 'char(2)'
		m999NamaStatusKendaraan column : 'M999_NamaStatusKendaraan', sqlType : 'varchar(50)'
		staDel column : 'M999_StaDel', sqlType : 'char(1)'
	}

    static constraints = {
		m999Id (nullable : false, blank : false, unique : true, maxSize : 2)
		m999NamaStatusKendaraan (nullable : false, blank : false, maxSize : 50)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m999NamaStatusKendaraan
	}
}
