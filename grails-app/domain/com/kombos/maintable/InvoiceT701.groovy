package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.customerprofile.SPK
import com.kombos.reception.Reception

class InvoiceT701 {
    CompanyDealer companyDealer
	String t701NoInv
	String t701Nopol
	Date t701TglJamMulai
	Date t701TglJamInvoice
	String t701JenisInv
	String t701StaApprovedReversal
	String t701AlasanUnApproved
	Reception reception
	String t701NoInv_Reff
	MetodeBayar metodeBayar
	String t701staSPK
	SPK spk
	String t701staAsuransi
	SPKAsuransi spkAsuransi
	//Customer customer
	HistoryCustomer historyCustomer
	String t701Customer
	String t701Alamat
	String t701NPWP
	Double t701AdmRp
	Double t701JasaRp
	Double t701SubletRp
	Double t701PartsRp
	Double t701PartsRpCampuran
	Double t701PartsRpToyota
	Double t701OliRp
	Double t701MaterialRp
	Double t701SubTotalRp
	Double t701DiscSpecialRp
	Double t701TotalDiscRp
	Double t701PPnRp
	Double t701TotalRp
	Double t701MateraiRp
	Double t701TotalInv
	Double t701BookingFee
	Double t701OnRisk
	Double t701TotalBayarRp
	String t701TotalInvTerbilang
	String t701Catatan
	String t701IntExt
	String t701StaWarranty
	String t701Tipe
	String t701Teknisi
	Date t701DueDate
	Integer t701PrintKe
	Date t701TglJamCetak
	String t701AdmBilling
	Date t701TglRequestApprovalGoodWill
	String t701StaRequestPersenRp
	Double t701RequestGoodWillPersen
	Double t701RequestGoodWillRp
	Date t701TglResponApprovalGoodWill
	String t701StaApprovalGoodWill
	String t701AlasanUnApprovedGoodWill
	String t701StaPersenRp
	Double t701GoodWillPersen
	Double t701GoodWillRp
	String t701RefundRp
	String t701StaSettlement
	String t701xKet
	String t701xNamaUser
	String t701xDivisi
	String t701StaDel
	String t701StaApproveMigrate
	String t701Subtype
	String t701Subledger
    Date dateCreated //Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t701NoInv blank:true, nullable:true, maxSize:20
		t701Nopol blank:true, nullable:true, maxSize:50
		t701TglJamMulai blank:true, nullable:true
		t701TglJamInvoice blank:true, nullable:true
		t701JenisInv blank:true, nullable:true, maxSize:1
		t701StaApprovedReversal blank:true, nullable:true, maxSize:1
		t701AlasanUnApproved blank:true, nullable:true, maxSize:50
		reception blank:true, nullable:true
		t701NoInv_Reff blank:true, nullable:true, maxSize:20
		metodeBayar blank:true, nullable:true
		t701staSPK blank:true, nullable:true, maxSize:1
		spk blank:true, nullable:true
		t701staAsuransi blank:true, nullable:true, maxSize:1
		spkAsuransi blank:true, nullable:true
		//customer blank:true, nullable:true
		historyCustomer blank:true, nullable:true
		t701Customer blank:true, nullable:true, maxSize:100
		t701Alamat blank:true, nullable:true, maxSize:255
		t701NPWP blank:true, nullable:true, maxSize:50
		t701AdmRp blank:true, nullable:true
		t701JasaRp blank:true, nullable:true
		t701SubletRp blank:true, nullable:true
		t701PartsRp blank:true, nullable:true
		t701PartsRpCampuran blank:true, nullable:true
		t701PartsRpToyota blank:true, nullable:true
		t701OliRp blank:true, nullable:true
		t701MaterialRp blank:true, nullable:true
		t701SubTotalRp blank:true, nullable:true
		t701DiscSpecialRp blank:true, nullable:true
		t701TotalDiscRp blank:true, nullable:true
		t701PPnRp blank:true, nullable:true
		t701TotalRp blank:true, nullable:true
		t701MateraiRp blank:true, nullable:true
		t701TotalInv blank:true, nullable:true
		t701BookingFee blank:true, nullable:true
		t701OnRisk blank:true, nullable:true
		t701TotalBayarRp blank:true, nullable:true
		t701TotalInvTerbilang blank:true, nullable:true, maxSize:200
		t701Catatan blank:true, nullable:true, maxSize:50
		t701IntExt blank:true, nullable:true, maxSize:1
		t701StaWarranty blank:true, nullable:true, maxSize:1
		t701Tipe blank:true, nullable:true, maxSize:10
		t701Teknisi blank:true, nullable:true, maxSize:50
		t701DueDate blank:true, nullable:true
		t701PrintKe blank:true, nullable:true, maxSize:4
		t701TglJamCetak blank:true, nullable:true
		t701AdmBilling blank:true, nullable:true, maxSize:50
		t701TglRequestApprovalGoodWill blank:true, nullable:true    // blank:true, nullable:true, diisi ketika request approval good will
		t701StaRequestPersenRp blank:true, nullable:true, maxSize:50
		t701RequestGoodWillPersen blank:true, nullable:true
		t701RequestGoodWillRp blank:true, nullable:true
		t701TglResponApprovalGoodWill blank:true, nullable:true     // blank:true, nullable:true, diisi ketika response approval good will
		t701StaApprovalGoodWill blank:true, nullable:true, maxSize:1
		t701AlasanUnApprovedGoodWill blank:true, nullable:true, maxSize:50
		t701StaPersenRp blank:true, nullable:true, maxSize:50
		t701GoodWillPersen blank:true, nullable:true
		t701GoodWillRp blank:true, nullable:true
		t701RefundRp blank:true, nullable:true
		t701StaSettlement blank:true, nullable:true, maxSize:1
		t701xKet blank:true, nullable:true, maxSize:50
		t701Subtype blank:true, nullable:true, maxSize:250
		t701Subledger blank:true, nullable:true, maxSize:250
		t701xNamaUser blank:true, nullable:true, maxSize:20
		t701xDivisi blank:true, nullable:true, maxSize:20
		t701StaDel blank:true, nullable:true, maxSize:1
        t701StaApproveMigrate blank:true, nullable:true, maxSize:1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T701_INVOICE'
		t701NoInv column:'T701_NoInv', sqlType:'char(20)'
		t701Nopol column:'T701_Nopol', sqlType:'varchar(50)'
		t701TglJamMulai column:'T701_TglJamMulai', sqlType : 'TIMESTAMP' //, sqlType: 'date'
		t701TglJamInvoice column:'T701_TglJamInvoice', sqlType : 'TIMESTAMP' //, sqlType: 'date'
		t701JenisInv column:'T701_JenisInv', sqlType:'char(1)'
		t701StaApprovedReversal column:'T701_StaApprovedReversal', sqlType:'char(1)'
		t701AlasanUnApproved column:'T701_AlasanUnApproved', sqlType:'varchar(50)'
		reception column:'T701_T401_NoWO'
		t701NoInv_Reff column:'T701_NoInv_Reff', sqlType:'char(20)'
		metodeBayar column:'T701_M701_ID'
		t701staSPK column:'T701_staSPK', sqlType:'char(1)'
		spk column:'T701_T191_IDSPK'
		t701staAsuransi column:'T701_staAsuransi', sqlType:'char(1)'
		spkAsuransi column:'T701_T193_IDAsuransi'
		//customer column:'T701_T182_T102_ID'
		historyCustomer column:'T701_T182_ID'
		t701Customer column:'T701_Customer', sqlType:'varchar(100)'
		t701Alamat column:'T701_Alamat', sqlType:'varchar(255)'
		t701NPWP column:'T701_NPWP', sqlType:'varchar(50)'
		t701AdmRp column:'T701_AdmRp'
		t701JasaRp column:'T701_JasaRp'
		t701SubletRp column:'T701_SubletRp'
		t701PartsRp column:'T701_PartsRp'
		t701PartsRpCampuran column:'T701_PartsRpCampuran'
		t701PartsRpToyota column:'T701_PartsRpToyota'
		t701OliRp column:'T701_OliRp'
		t701MaterialRp column:'T701_MaterialRp'
		t701SubTotalRp column:'T701_SubTotalRp'
		t701DiscSpecialRp column:'T701_DiscSpecialRp'
		t701TotalDiscRp column:'T701_TotalDiscRp'
		t701PPnRp column:'T701_PPnRp'
		t701TotalRp column:'T701_TotalRp'
		t701MateraiRp column:'T701_MateraiRp'
		t701TotalInv column:'T701_TotalInv'
		t701BookingFee column:'T701_BookingFee'
		t701OnRisk column:'T701_OnRisk'
		t701TotalBayarRp column:'T701_TotalBayarRp'
		t701TotalInvTerbilang column:'T701_TotalInvTerbilang', sqlType:'varchar(200)'
		t701Catatan column:'T701_Catatan', sqlType:'varchar(50)'
		t701IntExt column:'T701_IntExt', sqlType:'char(1)'
		t701StaWarranty column:'T701_StaWarranty', sqlType:'char(1)'
		t701Tipe column:'T701_Tipe', sqlType:'char(10)'
		t701Teknisi column:'T701_Teknisi', sqlType:'varchar(50)'
		t701DueDate column:'T701_DueDate', sqlType : 'TIMESTAMP' //, sqlType: 'date'
		t701PrintKe column:'T701_PrintKe', sqlType:'int'
		t701TglJamCetak column:'T701_TglJamCetak', sqlType : 'TIMESTAMP' //, sqlType: 'date'
		t701AdmBilling column:'T701_AdmBilling', sqlType:'varchar(50)'
		t701TglRequestApprovalGoodWill column:'T701_TglReqApprovalGoodWill', sqlType : 'TIMESTAMP' //, sqlType: 'date'
		t701StaRequestPersenRp column:'T701_StaRequestPersenRp', sqlType:'varchar(50)'
		t701RequestGoodWillPersen column:'T701_RequestGoodWillPersen '
		t701RequestGoodWillRp column:'T701_RequestGoodWillRp'
		t701TglResponApprovalGoodWill column:'T701_TglResponApprovalGoodWill', sqlType : 'TIMESTAMP' //, sqlType: 'date'
		t701StaApprovalGoodWill column:'T701_StaApprovalGoodWill', sqlType:'char(1)'
		t701AlasanUnApprovedGoodWill column:'T701_AlasanUnApprovedGoodWill', sqlType:'varchar(50)'
		t701StaPersenRp column:'T701_StaPersenRp', sqlType:'varchar(50)'
		t701GoodWillPersen column:'T701_GoodWillPersen'
		t701GoodWillRp column:'T701_GoodWillRp'
		t701RefundRp column:'T701_RefundRp'
		t701StaSettlement column:'T701_StaSettlement', sqlType:'char(1)'
		t701xKet column:'T701_xKet', sqlType:'varchar(50)'
		t701xNamaUser column:'T701_xNamaUser', sqlType:'varchar(20)'
		t701xDivisi column:'T701_xDivisi', sqlType:'varchar(20)'
		t701StaDel column:'T701_StaDel', sqlType:'char(1)'
        t701StaApproveMigrate column:'T701_STA_APPROVEMIGRATE', sqlType:'varchar(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        t701StaDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!t701StaDel)
            t701StaDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}