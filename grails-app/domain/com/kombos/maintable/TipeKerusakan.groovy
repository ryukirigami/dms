package com.kombos.maintable

class TipeKerusakan {

	String m401Id
	String m401NamaTipeKerusakan
	String m401StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m401Id column : 'M401_ID', sqlType : 'char(1)'
		table 'M401_TIPEKERUSAKAN'
		
		m401NamaTipeKerusakan column : 'M401_NamaTipeKerusakan', sqlType : 'varchar(50)'
		m401StaDel column : 'M401_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m401NamaTipeKerusakan
	}
    static constraints = {
		m401Id (nullable : false, unique : true, blank : false, maxSize : 1)
		m401NamaTipeKerusakan (nullable : true, blank : false, maxSize : 50)
		m401StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
