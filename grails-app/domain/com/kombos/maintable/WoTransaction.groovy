package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class WoTransaction {
    CompanyDealer companyDealer
    String dealerCode
    String areaCode
    String outletCode
    String noWo
    String tglWo
    String noPol
    String noVehicle
    String typeVehicle
    String isEm
    String woType
    String operation
    String damageLevel
    String customerType
    String operationType
    String flatRate
    String warranty
    String staDel
    String docNumber
    String fileName
    Date tglUpload
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    static mapping = {
        autoTimestamp false
        table 'TBL_WO_TRANSACTION'
    }

    static constraints = {
        staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        damageLevel nullable : true
        customerType nullable : true
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
