package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.SPK
import com.kombos.reception.Reception

class SPKPakai {
    CompanyDealer companyDealer
	SPK spk
	CustomerVehicle customerVehicle
	Reception reception
	Double t194JmlPakai
	String t194StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		spk nullable : false, blank : false, unique : true
		customerVehicle nullable : false, blank : false, unique : true
		reception  nullable : false, blank : false, unique : true
		t194JmlPakai  nullable : false, blank : false
		t194StaDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T194_SPKPAKAI'
		spk column : 'T194_T191_ID'
		customerVehicle column : 'T194_T103_VinCode'
		reception column : 'T194_T401_NoWO'
		t194JmlPakai column : 'T194_JmlPakai'
		t194StaDel column : 'T194_StaDel', sqlType : 'char(1)'
	}
}
