package com.kombos.maintable

import com.kombos.administrasi.AlasanRefund
import com.kombos.administrasi.Bank
import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.HistoryCustomer

class Refund {
    CompanyDealer companyDealer
	String t706NoRefund
	Date t706TgJamlRefund
	String t706JenisRefund //(BOOKING FEE : 0, PPH 23 : 1, PPN : 2)
	String t706NomorWO
	String t706NomorKwitansi
	String t706NomorInvoice
	String t706Nopol
	//Customer customer
	HistoryCustomer historyCustomer
	String t706NamaCustomer
	String t706StaCashTransfer // CASH : 0, TRANSFER : 1
	Bank bank
	String t706NoAccount
	String t706NamaAccount
	String t706StaFCKTP
	String t706StaFCBukuTab
	AlasanRefund alasanRefund
	String t706AlasanRefundLainnya
	Double t706JmlRefund
	String t706TerbilangRefund
	String t706xNamaUser
	String t706xDivisi
	String t706StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t706NoRefund blank:false, nullable:false, maxSize:20, unique:true
		t706TgJamlRefund blank:false, nullable:false
		t706JenisRefund blank:false, nullable:false, maxSize:1
		t706NomorWO blank:false, nullable:false, maxSize:20
		t706NomorKwitansi blank:false, nullable:false, maxSize:20
		t706NomorInvoice blank:true, nullable:true, maxSize:20
		t706Nopol blank:false, nullable:false, maxSize:50
		//customer blank:false, nullable:false
		historyCustomer blank:false, nullable:false
		t706NamaCustomer blank:false, nullable:false, maxSize:50
		t706StaCashTransfer blank:false, nullable:false, maxSize:1
		bank blank:true, nullable:true
		t706NoAccount blank:true, nullable:true, maxSize:50
		t706NamaAccount blank:true, nullable:true, maxSize:50
		t706StaFCKTP  blank:true, nullable:true, maxSize:1
		t706StaFCBukuTab blank:true, nullable:true, maxSize:1
        alasanRefund blank:true, nullable:true
		t706AlasanRefundLainnya blank:true, nullable:true, maxSize:50
		t706JmlRefund blank:false, nullable:false
		t706TerbilangRefund blank:false, nullable:false, maxSize:250
		t706xNamaUser blank:true, nullable:true, maxSize:20
		t706xDivisi blank:true, nullable:true, maxSize:20
		t706StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T706_REFUND'
		t706NoRefund column:'T706_NoRefund', sqlType:'char(20)'
		t706TgJamlRefund column:'T706_TgJamlRefund'//, sqlType: 'date'
		t706JenisRefund column:'T706_JenisRefund', sqlType:'char(1)'
		t706NomorWO  column:'T706_NomorWO', sqlType:'char(20)'
		t706NomorKwitansi column:'T706_NomorKwitansi', sqlType:'char(20)'
		t706NomorInvoice column:'T706_NomorInvoice', sqlType:'char(20)'
		t706Nopol column:'T706_Nopol', sqlType:'varchar(50)'
		//customer column:'T706_T182_T102_ID'
		historyCustomer column:'T706_T182_ID'
		t706NamaCustomer column:'T706_NamaCustomer', sqlType:'varchar(50)'
		t706StaCashTransfer column:'T706_StaCashTransfer', sqlType:'char(1)'
		bank column:'T706_M702_ID'
		t706NoAccount column:'T706_NoAccount', sqlType:'varchar(50)'
		t706NamaAccount column:'T706_NamaAccount', sqlType:'varchar(50)'
		t706StaFCKTP  column:'T706_StaFCKTP', sqlType:'char(1)'
		t706StaFCBukuTab column:'T706_StaFCBukuTab', sqlType:'char(1)'
        alasanRefund column:'T706_M071_ID'
		t706AlasanRefundLainnya column:'T706_AlasanRefundLainnya', sqlType:'varchar(50)'
		t706JmlRefund column:'T706_JmlRefund'
		t706TerbilangRefund column:'T706_TerbilangRefund', sqlType:'varchar(250)'
		t706xNamaUser column:'T706_xNamaUser', sqlType:'varchar(20)'
		t706xDivisi column:'T706_xDivisi', sqlType:'varchar(20)'
		t706StaDel column:'T706_StaDel', sqlType:'char(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        t706StaDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!t706StaDel)
            t706StaDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}