package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.HistoryCustomer

class RetentionT207 {
    CompanyDealer companyDealer
	Customer customer
	HistoryCustomer historyCustomer
	Integer t207ID
	Retention retention
	Date t207TglJamKirim
	String t207StatusSent
	String t207Ket
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		customer blank:false, nullable:false, maxSize:12, unique:true
		historyCustomer blank:false, nullable:false, maxSize:4, unique:true
		t207ID blank:false, nullable:false, maxSize:4, unique:true
		retention blank:false, nullable:false, maxSize:4
		t207TglJamKirim blank:false, nullable:false, maxSize:4
		t207StatusSent blank:false, nullable:false, maxSize:1
		t207Ket blank:false, nullable:false, maxSize:50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T207_RETENTION'
		customer column:'T207_T102_ID'
		historyCustomer column:'T207_T182_ID'
		t207ID column:'T207_ID', sqltype:'int'
		retention column:'T207_M207_ID'
		t207TglJamKirim column:'T207_TglJamKirim'//, sqlType: 'date'
		t207StatusSent column:'T207_StatusSent', sqlType:'char(1)'
		t207Ket column:'T207_Ket', sqlType:'varchar(50)'
	}
}
