package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.reception.Reception

class CustLamaWeb {

    Customer customer
	HistoryCustomer historyCustomer
	String t188ID
	CustomerVehicle customerVehicle
    Reception reception
	Operation operation
	String t188StaButuhPartsTambahan
	Double t188EstimasiBiataService
	Date t188EstimasiWaktuService
	CompanyDealer companyDealer
	Date t188TanggalJamService
	String t188KeluhanTambahan
	Date t188TglDaftar
	String t188StaKonfirmasi
	Date t188TglKonfirmasi
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        reception nullable: true, blank: true
        customer nullable : true, blank : true
        historyCustomer nullable : true, blank : true
        t188ID nullable : true, blank : true
        customerVehicle nullable : true, blank : true
        operation nullable : true, blank : true
        t188StaButuhPartsTambahan nullable : true, blank : true,maxSize : 1
        t188EstimasiBiataService nullable : true, blank : true
        t188EstimasiWaktuService nullable : true, blank : true
        companyDealer nullable : true, blank : true
        t188TanggalJamService nullable : true, blank : true
        t188KeluhanTambahan nullable : true, blank : true
        t188TglDaftar nullable : true, blank : true
        t188StaKonfirmasi nullable : true, blank : true, maxSize : 1
        t188TglKonfirmasi nullable : true, blank : true
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
		table 'T188_CUSTLAMAWEB'
		customer column : 'T188_T102_ID'
		historyCustomer column : 'T188_T182_ID'
		t188ID column : 'T188_ID', sqlType : 'varchar(20)'
		customerVehicle column : 'T188_T103_VinCode'
		operation column : 'T188_M053_JobID'
		t188StaButuhPartsTambahan column : 'T188_StaButuhPartsTambahan', sqlType : 'char(1)'
		t188EstimasiBiataService column : 'T188_EstimasiBiayaService'
		t188EstimasiWaktuService column : 'T188_EstimasiWaktuService', sqlType : 'date'
		companyDealer column : 'T188_M011_ID'
		t188TanggalJamService column : 'T188_TanggalJamService', sqlType : 'date'
		t188KeluhanTambahan column : 'T188_KeluhanTambahan'//, sqlType : 'text'
		t188TglDaftar column : 'T188_TglDaftar', sqlType : 'date'
		t188StaKonfirmasi column : 'T188_StaKonfirmasi', sqlType : 'char(1)'
		t188TglKonfirmasi column : 'T188_TglKonfirmasi', sqlType : 'date'
        statusKendaraan formula: "coalesce((select sk.M999_NamaStatusKendaraan from T999_PROGRESS p INNER JOIN M999_STATUS_KENDARAAN sk on p.STATUS_KENDARAAN_ID = sk.ID INNER JOIN  = WHERE p.RECEPTION_ID = RECEPTION_ID),'')"
        statusKendaraan formula: "(select p.id from T999_PROGRESS p INNER JOIN  = WHERE p.RECEPTION_ID = RECEPTION_ID)"
	}
}
