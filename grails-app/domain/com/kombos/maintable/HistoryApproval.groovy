package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.StatusApproval

class HistoryApproval {
    CompanyDealer companyDealer
	ApprovalT770 approval
	StatusApproval status
	Integer level
	Date tglJamApproved
	String namaApproved
	String keterangan
    //Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    //String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    //Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    //String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    //String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        //createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        //updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        //lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T771_HISTORYAPPROVAL'
		level column : 'T771_Level', sqlType : 'int'
		approval column : 'T771_T770_IDApproval'
		status column : 'T771_Status'
		tglJamApproved column: 'T771_TglJamApproved'
		namaApproved column: 'T771_NamaApproved' 
		keterangan column: 'T771_Ket' 
    }
}
