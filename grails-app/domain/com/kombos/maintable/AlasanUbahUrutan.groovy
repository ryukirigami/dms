package com.kombos.maintable

class AlasanUbahUrutan {

	String m600Id
	String m600Alasan
	String m600StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated = new Date()  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "INSERT"  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
		m600Id column : 'M600_ID', sqlType : 'char(2)'
		table 'M600_ALASANUBAHURUTAN'
		
		m600Alasan column : 'M600_Alasan', sqlType : 'varchar(50)'
		m600StaDel column : 'M600_StaDel', sqlType : 'char(1)'
    }
	
	String toString(){
		return m600Alasan
	}
    static constraints = {
		m600Id (nullable : true, blank : true, unique : true, maxSize : 2)
		m600Alasan (nullable : true, blank : true, maxSize : 50)
		m600StaDel (nullable : true, blank : true, maxSize : 1)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
