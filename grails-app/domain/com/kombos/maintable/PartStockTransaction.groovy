package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class PartStockTransaction {
    CompanyDealer companyDealer

    String dealerCode
    String areaCode
    String outletCode
    String partNo
    String partDesc
    String stock
    String madUpdate

    Date tglUpload
    String staDel
    String docNumber
    String fileName

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess

    static mapping = {
        autoTimestamp false
        table 'TBL_PART_STOCK_TRANSACTION'
    }

    static constraints = {
        staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
