package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.sec.shiro.User

class MyTask {
    CompanyDealer companyDealer
	User userProfile
	TaskList taskList
	Date t007Tanggal
	String t007ID
	String t007Nopol
	String t007NoWO
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T007_MYTASK'
		
		userProfile column : 'T007_T001_IDUser'
		taskList column : 'T007_M033_ID'
		t007Tanggal column : 'T007_Tanggal', sqlType : 'date'
		t007Nopol column : 'T007_Nopol', sqlType : 'varchar(50)'
		t007NoWO column : 'T007_NoWO', sqlType : 'char(20)'
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
		userProfile (nullable : false, blank : false)
		taskList (nullable : false, blank : false)
		t007Tanggal (nullable : false, blank : false)
		t007Nopol (nullable : false, blank : false, maxSize : 50)
		t007NoWO (nullable : false, blank : false, maxSize : 20)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return userProfile.username + " : " + taskList.toString();
	}
}
