package com.kombos.maintable

class TaskList {
    Integer m033ID
    String m033NamaTask
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m033ID (blank:false, nullable: false)
        m033NamaTask (blank:false, nullable: false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table "M033_TASKLIST"
        m033ID column:"M033_ID", sqlType: 'int', unique: true
		m033NamaTask column:"M033_NamaTask", sqlType:"varchar(50)"
	}
	
	String toString(){
		return m033NamaTask
	}
}
