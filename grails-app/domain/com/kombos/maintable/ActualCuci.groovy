package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Stall

class ActualCuci {
	CompanyDealer companyDealer
	AntriCuci antriCuciTanggal
	AntriCuci antriCuciID
	NamaManPower namaManPower
	Stall stall
	Date t602TglJam
	StatusActual statusActual
    Boolean flagClockOff = false
	String t602Ket
	String t602StaDel
    Date dateCreated = new Date() // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated= new Date()  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "INSERT"  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:true, nullable:true, maxSize:4
		antriCuciTanggal blank:true, nullable:true
		antriCuciID blank:true, nullable:true, maxSize:4
		namaManPower blank:true, nullable:true, maxSize:10
		stall blank:true, nullable:true
		t602TglJam blank:true, nullable:true
		statusActual blank:true, nullable:true, maxSize:1
		t602Ket blank:true, nullable:true, maxSize:50
		t602StaDel blank:true, nullable:true, maxSize:1
        flagClockOff nullable: true
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T602_ACTUALCUCI'
		companyDealer column:'T602_M011_ID'
		antriCuciTanggal column:'T602_T600_Tanggal'
		antriCuciID column:'T602_T600_ID'
		namaManPower column:'T602_T015_IDManPower'
		id column:'T602_ID'
		stall column:'T602_M022_StallID'
		t602TglJam column:'T602_TglJam'
		statusActual column:'T602_M452_ID'
		t602Ket column:'T602_Ket', sqlType:'varchar(50)'
		t602StaDel column:'T602_StaDel', sqlType:'char(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "update"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()


        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if (!lastUpdProcess)
            lastUpdProcess = "insert"
        if (!lastUpdated)
            lastUpdated = new Date()
        if (!createdBy) {
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}