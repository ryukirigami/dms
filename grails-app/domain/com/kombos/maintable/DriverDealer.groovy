package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaManPower
import com.kombos.reception.Reception

class DriverDealer {
	CompanyDealer companyDealer
	Date t408Tanggal
	Integer t408ID
	Reception reception
	NamaManPower namaManPower
	String t408StaAntarJemput
	CompanyDealer companyDealerIDAsal
	CompanyDealer companyDealerTujuan
	Date t408TglJamRencanaBerangkat
	String t408staBerangkat
	Date t408TglJamActualBerangkat
	Date t408TglJamRencanaSampai
	String t408staSampai
	Date t408TglJamActualSampai
	String t408StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false, maxSize:4, unique:true
		t408Tanggal blank:false, nullable:false, maxSize:4, unique:true
		t408ID blank:false, nullable:false, maxSize:4, unique:true
		reception blank:false, nullable:false, maxSize:20
		namaManPower blank:false, nullable:false, maxSize:10
		t408StaAntarJemput blank:false, nullable:false, maxSize:1
		companyDealerIDAsal blank:false, nullable:false, maxSize:4
		companyDealerTujuan blank:false, nullable:false, maxSize:4
		t408TglJamRencanaBerangkat blank:false, nullable:false, maxSize:4
		t408staBerangkat blank:false, nullable:false, maxSize:1
		t408TglJamActualBerangkat blank:false, nullable:false, maxSize:4
		t408TglJamRencanaSampai blank:false, nullable:false, maxSize:4
		t408staSampai blank:false, nullable:false, maxSize:1
		t408TglJamActualSampai blank:false, nullable:false, maxSize:4
		t408StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping  = {
		table 'T408_DRIVERDEALER'
		companyDealer column:'T408_M011_ID'
		t408Tanggal column:'T408_Tanggal'//, sqlType: 'date'
		t408ID column:'T408_ID', sqlType:'int'
		reception column:'T408_T401_NoWO'
		namaManPower column:'T408_T015_IDManPower'
		t408StaAntarJemput column:'T408_StaAntarJemput', sqlType:'char(1)'
		companyDealerIDAsal column:'T408_M011_IDAsal'
		companyDealerTujuan column:'T408_M011_Tujuan'
		t408TglJamRencanaBerangkat column:'T408_TglJamRencanaBerangkat'//, sqlType: 'date'
		t408staBerangkat column:'T408_staBerangkat', sqlType:'char(1)'
		t408TglJamActualBerangkat column:'T408_TglJamActualBerangkat'//, sqlType: 'date'
		t408TglJamRencanaSampai column:'T408_TglJamRencanaSampai'//, sqlType: 'date'
		t408staSampai column:'T408_staSampai', sqlType:'char(1)'
		t408TglJamActualSampai column:'T408_TglJamActualSampai'//, sqlType: 'date'
		t408StaDel column:'T408_StaDel', sqlType:'char(1)'
	}
}
