package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class Retention {

    static String ULANG_TAHUN = "Ulang Tahun"
    static String STNK_JATUH_TEMPO = "STNK Jatuh Tempo"
    static String SELESAI_INVOICE = "Pemberitahuan Selesai Invoice"

//	int m207Id
    CompanyDealer companyDealer
    String m207NamaRetention
    String m207FormatSms
    String m207StaOtomatis
    String m207Ket
    String staDel = "0"
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "insert"//Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
        autoTimestamp false
        table 'M207_RETENTION'
        id column: 'M207_Retention'
        m207NamaRetention column: 'M207_NamaRetention', sqltype: 'varchar', length: 50
        m207FormatSms column: 'M207_FormatSMS', type: "text"
        m207StaOtomatis column: 'M207_StaOtomatis', sqltype: 'char', length: 1
        m207Ket column: 'M207_Ket', sqltype: 'varchar', length: 50
        staDel column: 'M207_StaDel', sqltype: 'char', length: 1
    }

    String toString() {
        return m207NamaRetention
    }
    static constraints = {
        companyDealer (nullable: true, blank: true)
//		m207Id (nullable : true, blank : true, display:false)
        m207NamaRetention (nullable: false, blank: false, maxSize: 50)
        m207FormatSms(nullable: false, blank: false)
        m207StaOtomatis(nullable: true, blank: true, maxSize: 1)
        m207Ket(nullable: true, blank: true, maxSize: 50, display: false)
        staDel(nullable: false, blank: false, maxSize: 1, display: false)
        createdBy nullable: true, display: false //Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true, display: false //Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true, display: false //Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "update"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()


        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if (!lastUpdProcess)
            lastUpdProcess = "insert"
        if (!lastUpdated)
            lastUpdated = new Date()
        if (!createdBy) {
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}