package com.kombos.maintable

class TujuanKedatangan {

	String m400Id
	String m400Tujuan
	String m400StaCetakAntrian
	String m400StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        m400Id column : 'M400_ID', sqlType : 'char(2)'
		table 'M400_TUJUANKEDATANGAN'
		
		m400Tujuan column : 'M400_Tujuan', sqlType : 'varchar(50)'
		m400StaCetakAntrian column : 'M400_CetakAntrian', sqlType : 'char(1)'
		m400StaDel column : 'M400_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m400Tujuan
	}
    static constraints = {
		m400Id (nullable : false, unique : true, blank : false, maxSize : 2)
		m400Tujuan (nullable : false, blank : false)
		m400StaCetakAntrian (nullable : false, blank : false, maxSize : 1)
		m400StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
