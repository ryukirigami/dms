package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.sec.shiro.User

class KrRFiDaily {

    long id
    Date tglFi
    User namaForeman
    float fiTarget
    float fiActual
    float otherFi
    float solvedFo
    float solvedRate
    float fiRate
    CompanyDealer companyDealer
    int version

    static constraints = {
    }

    static mapping = {
        table "R003_FI_DAILY"
        id column : "ID"
        tglFi column: 'TGL_FI'
        namaForeman column: 'NAMA_FOREMAN'
        fiTarget column: 'FI_TARGET'
        fiActual column: 'FI_ACTUAL'
        otherFi column: 'OTHER_FI'
        solvedFo column: 'SOLVED_FO'
        solvedRate column: 'SOLVED_RATE'
        fiRate column: 'FI_RATE'
        companyDealer column: 'R003_M011_ID'
        version column: 'VERSION'
    }
}
