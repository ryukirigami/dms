package com.kombos.maintable

class JenisFA {

	String m184KodeJenisFA
	String m184NamaJenisFA
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
    	m184KodeJenisFA unique:true, blank:false , nullable : false, maxSize : 2
		m184NamaJenisFA blank:false , nullable : false, maxSize : 50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'M184_JENISFA'
		m184KodeJenisFA column : 'M184_KodeJenisFA', sqlType : 'char(2)'
		m184NamaJenisFA column : 'M184_NamaJenisFA', sqlType : 'varchar(50)' 
	}
	
	String toString(){
		return m184NamaJenisFA
	}
}
