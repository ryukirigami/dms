package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.CustomerVehicle

class Diskon {
    CompanyDealer companyDealer
	CustomerVehicle customerVehicle
	Date t173TglAwal
	Date t173TglAkhir
	Double t173PersenDiskonParts
	Double t173PersenDiskonJasa
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


	static constraints = {
        companyDealer (blank:true, nullable:true)
		customerVehicle nullable : false, blank : false, unique : 't173TglAwal'
		t173TglAwal nullable : false, blank : false
		t173TglAkhir nullable : false, blank : false
		t173PersenDiskonParts nullable : false, blank : false
		t173PersenDiskonJasa nullable : false, blank : false
		staDel nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	
	static mapping = {
		table 'T173_DISKON'
		customerVehicle column : 'T173_T103_VinCode'
		t173TglAwal column : 'T173_TglAwal', sqlType : 'date'
		t173TglAkhir column : 'T173_TglAkhir', sqlType : 'date'
		t173PersenDiskonParts column : 'T173_PersenDiskonParts'
		t173PersenDiskonJasa column : 'T173_PersenDiskonJasa'
		staDel column : 'T173_StaDel', sqlType : 'char(1)'
        sort lastUpdated:'desc'
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "update"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "insert"
//		lastUpdated = new Date()
		staDel = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "insert"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "_SYSTEM_"
			updatedBy = createdBy
		}
	}
}