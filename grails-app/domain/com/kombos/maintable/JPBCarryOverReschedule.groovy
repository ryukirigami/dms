package com.kombos.maintable

import com.kombos.administrasi.AlasanRefund
import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FlatRate
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Stall
import com.kombos.board.JPB
import com.kombos.reception.Reception;

class JPBCarryOverReschedule {
	CompanyDealer companyDealer
	JPB jPB
	AlasanRefund alasanRefund
	Reception reception
	Date t454TglJamPlan
	NamaManPower namaManPower
	Stall stall
	FlatRate flatRate
	String t454StaTambahWaktu
	String t454staOkCancelReSchedule
	String t454AlasanReSchedule
	String t454StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false, unique:true, maxSize:4
		jPB blank:false, nullable:false, unique:true, maxSize:12
		alasanRefund blank:false, nullable:false, maxSize:1
		reception blank:false, nullable:false, maxSize:20
		t454TglJamPlan blank:false, nullable:false
		namaManPower blank:false, nullable:false, maxSize:10
		stall blank:false, nullable:false, maxSize:8
		flatRate blank:false, nullable:false
		t454StaTambahWaktu blank:false, nullable:false, maxSize:1
		t454staOkCancelReSchedule blank:false, nullable:false, maxSize:1
		t454AlasanReSchedule blank:false, nullable:false, maxSize:50
		t454StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T454_JPBCARRYOVERRESCHEDULE'
		companyDealer column:'T454_T451_M011_ID'
		jPB column:'T454_T451_IDJPB'
		alasanRefund column:'T454_M071_ID'
		reception column:'T454_T401_NoWO'
		t454TglJamPlan column:'T454_TglJamPlan'
		namaManPower column:'T454_T015_IDManPower'
		stall column:'T454_M022_StallID'
		flatRate column:'T454_T113_FlatRate'
		t454StaTambahWaktu column:'T454_StaTambahWaktu', sqlType:'char(1)'
		t454staOkCancelReSchedule column:'T454_staOkCancelReSchedule', sqlType:'char(1)'
		t454AlasanReSchedule column:'T454_AlasanReSchedule', sqlType:'varchar(50)'
		t454StaDel column:'T454_StaDel', sqlType:'char(1)'
	}
}
