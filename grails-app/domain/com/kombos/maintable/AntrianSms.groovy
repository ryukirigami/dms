package com.kombos.maintable

class AntrianSms {
    String noTujuan
    String isiPesan
    Date waktuAntri
    Date waktuKirim
    Date waktuTerkirim
    Long status
    Long reminderId
    Date jadwalReminder

    static constraints = {
        waktuKirim nullable: true
        waktuTerkirim nullable: true
        reminderId nullable: true
        jadwalReminder nullable: true
        isiPesan maxSize: 1000
    }

    static mapping = {
        autoTimestamp false
        table("T1000_ANTRIAN_SMS")
        noTujuan column: "T1000_NO_TUJUAN"
        isiPesan column: "T1000_ISI_PESAN"
        waktuAntri column: "T1000_WAKTU_ANTRI"
        waktuKirim column: "T1000_WAKTU_KIRIM"
        waktuTerkirim column: "T1000_WAKTU_TERKIRIM"
        status column: "T1000_STATUS"
        reminderId column: "T1000_REMINDER_ID"
        jadwalReminder column: "T1000_JADWAL_REMINDER"
    }
}
