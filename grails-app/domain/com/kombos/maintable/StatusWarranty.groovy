package com.kombos.maintable

class StatusWarranty {

    Integer m058ID
    String m058NamaStatusWarranty
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m058ID blank:false, nullable : false
        m058NamaStatusWarranty blank:false, nullable : false, maxSize : 50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        m058ID column : 'M058_ID', sqlType : 'int', unique: true
		m058NamaStatusWarranty column : 'M058_NamaStatusWarranty', sqlType : 'varchar(50)'
		table ' M058_STATUSWARRANTY'
	}

	String toString(){
		return m058NamaStatusWarranty
	}
}
