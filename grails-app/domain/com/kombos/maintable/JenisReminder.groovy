package com.kombos.maintable

class JenisReminder {

	String m201Id
	String m201NamaJenisReminder
	String m201StaDel = "0";
    Date dateCreated = new Date(); // Menunjukkan kapan baris isian ini dibuat.
    String createdBy; //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated = new Date(); //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "insert" //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
	 	m201Id column : 'M201_ID', sqltype : 'char', length : 3
		m201NamaJenisReminder column : 'M201_NamaJenisReminder', sqltype : 'varchar', length : 100
		m201StaDel column : 'M201_StaDel', sqltype : 'char', length :1
		table 'M201_JENISREMINDER'
	}
	
	String toString(){
		return m201NamaJenisReminder
	}
    static constraints = {
		m201Id (nullable : true, blank : true, unique : true, maxSize : 3)
		m201NamaJenisReminder (nullable : true, blank : true, maxSize : 100)
		m201StaDel (nullable : true, blank : false, maxSize : 1)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
