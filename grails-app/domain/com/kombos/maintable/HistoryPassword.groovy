package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

//import com.kombos.administrasi.UserProfile
import com.kombos.baseapp.sec.shiro.User

class HistoryPassword {
    CompanyDealer companyDealer
	User userProfile
	Date t006Tanggal
	String t006Password
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T006_HISTORYPASSWORD'
		
		userProfile column : 'T006_T001_IDUser'
		t006Tanggal column : 'T006_Tanggal', sqlType : 'date'
		t006Password column : 'T006_T001_Password', sqlType : 'varchar(50)'
	}
	
    static constraints = {
        companyDealer (blank:true, nullable:true)
		userProfile (nullable : false, blank : false)
		t006Tanggal (nullable : false, blank : false)
		t006Password (nullable : false, blank : false, maxSize : 50)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return userProfile.toString();
	}
}
