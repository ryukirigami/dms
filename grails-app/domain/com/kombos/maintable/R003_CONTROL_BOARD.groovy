package com.kombos.maintable

class R003_CONTROL_BOARD {


    float SERVICE_RATE
    float SERVICE_RATE_TARGET
    float LINE_ITEM_PARTS_BO
    float LINE_ITEM_SUPPLY_PARTS_BO
    float RATIO_AKURASI_SUPPLY_PARTS_BO
    float PENJUALAN
    float RATA_RATA_PENJUALAN_3_BULAN
    float STOK_AKHIR_BULAN
    float STOK_DAY
    float OVER_STOCK_PARTS
    float DEAD_STOCK_PARTS
    float STOCK_EFESIENSI_PERSEN
    Date  TANGGAL_REPORT
    float SERVICE_ORDER_FILL_RATE
    float SERVICE_ORDER_FILL_RATE_TARGET
    float TYPE_VOR
    float TYPE_SATU
    float TYPE_DUA



    static constraints = {
    }

}
