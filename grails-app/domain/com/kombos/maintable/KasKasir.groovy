package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class KasKasir {
	CompanyDealer companyDealer
	Date t707Tanggal
	Double t707SaldoAwal
	Double t707SaldoAkhir
	Date t707TanggalSetorBank
	String t707NamaBank
	String t707AtasNama
	String t707NoRekening
	Double t707JmlSetor
	Double t707SaldoAkhirMinSetor
	String t707xNamaUser
	String t707xDivisi
	String t707StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false, unique:true
		t707Tanggal blank:false, nullable:false, unique:true
		t707SaldoAwal blank:false, nullable:false
		t707SaldoAkhir blank:false, nullable:false
		t707TanggalSetorBank blank:false, nullable:false
		t707NamaBank blank:false, nullable:false, maxSize:10
		t707AtasNama blank:false, nullable:false, maxSize:10
		t707NoRekening  blank:false, nullable:false, maxSize:10
		t707JmlSetor blank:false, nullable:false
		t707SaldoAkhirMinSetor blank:false, nullable:false
		t707xNamaUser blank:false, nullable:false, maxSize:20
		t707xDivisi blank:false, nullable:false, maxSize:20
		t707StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T707_KasKasir'
		companyDealer column:'T707_M011_ID'
		t707Tanggal column:'T707_Tanggal', sqlType : 'TIMESTAMP'    //sqlType: 'date'
		t707SaldoAwal column:'T707_SaldoAwal'
		t707SaldoAkhir column:'T707_SaldoAkhir'
		t707TanggalSetorBank column:'T707_TanggalSetorBank', sqlType : 'TIMESTAMP'  //sqlType:'char(10)'
		t707NamaBank column:'T707_NamaBank', sqlType:'char(10)'
		t707AtasNama column:'T707_AtasNama', sqlType:'char(10)'
		t707NoRekening  column:'T707_NoRekening', sqlType:'char(10)'
		t707JmlSetor column:'T707_JmlSetor'
		t707SaldoAkhirMinSetor column:'T707_SaldoAkhirMinSetor'
		t707xNamaUser column:'T707_xNamaUser', sqlType:'varchar(20)'
		t707xDivisi  column:'T707_xDivisi', sqlType:'varchar(20)'
		t707StaDel column:'T707_StaDel', sqlType:'char(1)'
	}
}
