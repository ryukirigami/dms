package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.UserRole

class TaskListRole {
	TaskList taskList
	CompanyDealer userRoleCompanyDealer
	UserRole userRole
	String m034LevelUserRole
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        taskList nullable: false, blank:false
        userRoleCompanyDealer nullable: false, blank:false
        userRole nullable: false, blank:false
        m034LevelUserRole nullable: false, blank:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping ={
        autoTimestamp false
        taskList column: "M034_M033_ID", unique: true
        userRoleCompanyDealer column: "M034_T003_M011_ID", unique: true
        userRole column: "M034_T003_ID", unique: true
        m034LevelUserRole column: "M034_LevelUserRole", sqlType : "char(1)"
        table "M034_TASKLISTROLE"
    }

    String toString(){
        return m034LevelUserRole;
    }
}
