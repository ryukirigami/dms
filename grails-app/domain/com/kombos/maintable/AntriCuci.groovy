package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.Reception

class AntriCuci {

    CompanyDealer companyDealer
	Date t600Tanggal = new Date()
//	Integer t600ID
	Integer t600NomorAntrian
	Reception reception
	String t600alasanUbahUrutan
	String t600staOkCancelSelesai
	String t600StaCuciSebelumProd
	String t600xKet
	String t600xNamaUser
	String t600xDivisi
	String t600StaDel
	String t600StaPrioritas

    ActualCuci actualCuci

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess //Baris ini, terakhir prosess apa ?? insert, update, delete


//    Integer getT600NomorAntrian(){
//        if (t600NomorAntrian == null){
//            t600NomorAntrian = generateNomorAntrian()
//        }
//        return t600NomorAntrian
//    }

//    static Integer generateNomorAntrian() {
//        Integer noUrut = AntriCuci.countByT600Tanggal(new Date()) + 1
//        println noUrut
//        return noUrut
//    }

    static transients = ['customField']

    Date getCustomField() {
        if(reception?.t401TglJamPenyerahanReSchedule){
            if(reception?.t401TglJamPenyerahanReSchedule < reception?.t401TglJamJanjiPenyerahan){
                return reception?.t401TglJamPenyerahanReSchedule
            }else{
                return reception?.t401TglJamJanjiPenyerahan
            }
        }else{
            return reception?.t401TglJamJanjiPenyerahan
        }

    }

    static constraints = {
		companyDealer blank:true, nullable:true, maxSize:4
		t600Tanggal blank:true, nullable:true
//		t600ID blank:true, nullable:true, maxSize:4, unique:true
		t600NomorAntrian blank:true, nullable:true, maxSize:4
		reception blank:true, nullable:true, maxSize:20
        t600alasanUbahUrutan blank:true, nullable:true, maxSize:250
		t600staOkCancelSelesai blank:true, nullable:true, maxSize:1
		t600StaCuciSebelumProd blank:true, nullable:true, maxSize:1
		t600xKet blank:true, nullable:true, maxSize:50
		t600xNamaUser blank:true, nullable:true, maxSize:20
		t600xDivisi blank:true, nullable:true, maxSize:20
		t600StaDel blank:true, nullable:true, maxSize:1
		t600StaPrioritas blank:true, nullable:true, maxSize:1
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
        actualCuci nullable: true
    }
	
	static mapping = {
        autoTimestamp false
        table 'T600_ANTRICUCI'
		companyDealer column:'T600_M011_ID'
		t600Tanggal column:'T600_Tanggal'//, sqlType: 'date', type: "date"
		id column:'T600_ID'
		t600NomorAntrian column:'T600_NomorAntrian', sqlType:'int'
		reception column:'T600_T401_NoWO'
        t600alasanUbahUrutan column:'T600_AlasanUbahUrutan'
		t600staOkCancelSelesai column:'T600_staOkCancelSelesai', sqlType:'char(1)'
		t600StaCuciSebelumProd column:'T600_StaCuciSebelumProd', sqlType:'char(1)'
		t600xKet column:'T600_xKet', sqlType:'varchar(50)'
		t600xNamaUser column:'T600_xNamaUser', sqlType:'varchar(20)'
		t600xDivisi column:'T600_xDivisi', sqlType:'varchar(20)'
		t600StaDel column:'T600_StaDel', sqlType:'char(1)'
		t600StaPrioritas column:'T600_StaPrioritas', sqlType:'char(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "update"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
//        t600NomorAntrian = generateNomorAntrian();

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()

            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if (!lastUpdProcess)
            lastUpdProcess = "insert"
        if (!lastUpdated)
            lastUpdated = new Date()
        if (!createdBy) {
            createdBy = "_SYSTEM_"
        }
    }
}