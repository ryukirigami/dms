package com.kombos.maintable

import com.kombos.administrasi.KegiatanApproval

class KondisiApproval {
	
	KegiatanApproval kegiatanApproval
	Integer m772Id
	String m772FieldPembanding1
	String m772TabelPembanding1
	String m772NamaPembanding
	String m772Operator1
	String m772Operator2
	Double m772Nilai1
	Double m772Nilai2
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M772_KONDISIAPPROVAL'
		kegiatanApproval column : 'M772_M770_IDApproval'
		m772Id column : 'M772_ID', sqlType : 'int'
		m772FieldPembanding1 column : 'M772_FieldPembanding1', sqlType : 'varchar(50)'
		m772TabelPembanding1 column : 'M772_FieldPembanding2', sqlType : 'varchar(50)'
		m772NamaPembanding column : 'M772_NamaPembanding', sqlType : 'varchar(50)' 
		m772Operator1 column : 'M772_Operator1', sqlType : 'varchar(50)'
		m772Operator2 column : 'M772_Operator2', sqlType : 'varchar(50)'
		m772Nilai1 column : 'M772_Nilai1', sqlType : 'float'
		m772Nilai2 column : 'M772_Nilai2', sqlType : 'float'
		
	}
	
	
    static constraints = {
		kegiatanApproval (nullable : false, blank : false)
		m772Id (nullable : false, blank : false, unique : true, maxSize : 4)
		m772FieldPembanding1 (nullable : false, blank : false, maxSize : 50)
		m772TabelPembanding1(nullable : false, blank : false, maxSize : 50)
		m772NamaPembanding (nullable : false, blank : false, maxSize : 50)
		m772Operator1 (nullable : false, blank : false, maxSize : 50)
		m772Operator2 (nullable : false, blank : false, maxSize : 50)
		m772Nilai1 (nullable : false, blank : false, maxSize : 8)
		m772Nilai2 (nullable : false, blank : false, maxSize : 8)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}


