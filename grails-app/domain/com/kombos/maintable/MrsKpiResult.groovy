package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class MrsKpiResult {

    int id
    int version
    int r002_ID
    float custDtgDiundang
    float custDiundang
    float appMrs
    float totalApp
    float sbeMrs
    float totalUnitSbe
    float sbeGrMrs
    float totalUnitEntry
    float sbiMrs
    float jumUnitTerkirim
    Date tglLaporan
    CompanyDealer companyDealer
    float sbe1k_10k
    float sbe10k_20k
    float sbe20k_30k
    float sbe30k_40k
    float sbe40k_50k
    float sbe50k_60k
    float sbe60k_70k
    float sbe70k_80k
    float sbe80k_90k
    float sbe90k_100k

    static constraints = {

    }

    static mapping = {
        table "R002_KPI_RESULT"
        id column : "ID"
        version column : "VERSION"
        r002_ID column : "R002_ID"
        custDtgDiundang column : "CUST_DTG_DIUNDANG"
        custDiundang column : "CUST_DIUNDANG"
        appMrs column : "APP_MRS"
        totalApp column : "TOTAL_APP"
        sbeMrs column : "SBE_MRS"
        totalUnitSbe column : "TOTAL_UNIT_SBE"
        sbeGrMrs column : "SBE_GR_MRS"
        totalUnitEntry column : "TOTAL_UNIT_ENTRY"
        sbiMrs column : "SBI"
        jumUnitTerkirim column : "JUMLAH_UNIT_TERKIRIM"
        tglLaporan column : "TGL_LAPORAN"
        companyDealer column : "R002_M011_ID"
        sbe1k_10k column : "SBE_1K_10K"
        sbe10k_20k column : "SBE_10K_20K"
        sbe20k_30k column : "SBE_20K_30K"
        sbe30k_40k column : "SBE_30K_40K"
        sbe40k_50k column : "SBE_40K_50K"
        sbe50k_60k column : "SBE_50K_60K"
        sbe60k_70k column : "SBE_60K_70K"
        sbe70k_80k column : "SBE_70K_80K"
        sbe80k_90k column : "SBE_80K_90K"
        sbe90k_100k column : "SBE_90K_100K"
    }
}
