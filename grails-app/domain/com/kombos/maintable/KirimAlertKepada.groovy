package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.UserRole
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.board.JPB
import com.kombos.production.Masalah
import com.kombos.woinformation.Actual

class KirimAlertKepada {
	CompanyDealer companyDealer
	JPB jPB
	Actual actual
	Masalah masalah
	Role userRole
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false
		jPB blank:false, nullable:false
		actual blank:false, nullable:false
		masalah blank:false, nullable:false
		userRole blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table "T502_KIRIMALERTKEPADA"
		companyDealer column:"T502_M011_ID"
		jPB column:"T502_T451_IDJPB"
		actual column:"T502_452_ID"
		masalah column:"T502_T501_ID"
		userRole column:"T502_T003_ID"
	}
}
