	package com.kombos.maintable

    import com.kombos.administrasi.CompanyDealer
    import com.kombos.administrasi.Operation

    class JobInvitation {

	CompanyDealer companyDealer
	Operation operation
	String m203Urutan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M204_JOBINVITATION'
		
		companyDealer column : 'M203_M011_ID'
		operation column : 'M0203_M053_JOBID'
		m203Urutan column : 'M203_Urutan', sqlType : 'char', length : 4
	}
	
	String toString(){
		return m203Urutan
	}
    static constraints = {
		companyDealer (nullable : false, blank : false)
		operation (nullable : false, blank : false)
		m203Urutan (nullable : false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
