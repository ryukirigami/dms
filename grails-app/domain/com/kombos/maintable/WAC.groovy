package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.MasterWac
import com.kombos.reception.Reception

class WAC {
    CompanyDealer companyDealer
    Reception reception
    MasterWac masterWACID
    String t410StaItem
    String t410Keterangan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        reception blank:true, nullable:false
        masterWACID  blank:true, nullable:true, maxSize:4
        t410StaItem blank:true, nullable:true, maxSize:1
        t410Keterangan blank:true, nullable:true, maxSize:50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T410_WAC'
        reception column:'T410_T401_NoWO'
        masterWACID column:'T410_M403_ID'
        t410StaItem column:'T410_StaItem', sqlType:'char(1)'
        t410Keterangan column:'T410_Keterangan', sqlType:'varchar(50)'
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}