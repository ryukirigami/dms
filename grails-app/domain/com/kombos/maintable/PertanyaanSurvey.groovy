package com.kombos.maintable

import com.kombos.customerprofile.JenisSurvey

class PertanyaanSurvey {

	JenisSurvey jenisSurvey
	Date m132TanggalBerlaku
	Integer m132ID
	String m132Nomor
	String m132Pertanyaan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		m132ID unique : true,blank : false, nullable : false, maxSize : 4
		jenisSurvey blank : false, nullable : false,unique : true
		m132TanggalBerlaku blank : false , nullable : false,unique : true
		m132Nomor blank : false , nullable : false, maxSize : 5
		m132Pertanyaan blank : false , nullable : false, maxSize : 50
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'M132_PERTANYAANSURVEY'
		m132ID column : 'M132_ID', sqlType: 'int'
		jenisSurvey column : 'M132_M131_ID'
		m132TanggalBerlaku column : 'M132_TanggalBerlaku', sqlType : 'date'
		m132Nomor column : 'M132_Nomor', sqlType : 'varchar(5)'
		m132Pertanyaan column : 'M132_Pertanyaan', sqlType : 'varchar(50)'
	}
	
	String toString(){
		return m132Pertanyaan
	}
}
