package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaProsesBP
import com.kombos.administrasi.Operation
import com.kombos.parts.Goods
import com.kombos.reception.Reception

class PartsApp {
    CompanyDealer companyDealer
	Reception reception
	Goods goods
	Operation operation
	Double t303Jumlah1
	Double t303Jumlah1P
	Double t303Jumlah2
	Double t303Jumlah2P
	NamaProsesBP namaProsesBP
	String t303staTgaih
	Double t303HargaRp
	Double t303HargaRpp
	Double t303DiscRp
	Double t302DiscRpP
	Double t303DiscPersen
	Double t303DiscPersenp
	Double t303TotalRp
	Double t303TotalRpP
	String t303TotalTerbilang
	String t303TotalTerbilangP
	String t303NoDP
	Double t303RencanaDP
	Double t303RencanaDPP
	Double t303DPRp
	Double t303DPRpP
	String t303DPTerbilang
	String t303DPTerbilangp
	Double t303SisaRp
	Double t303SisaRpP
	String t303SisaTerbilang
	String t303SisaTerbilangP
	String t303StaHarusBookingFee = "0"
	String t303StaOkcancel = "0"
	String t303StaBayarDp = "0"
	String t303StaRequestPart = "0"
	Date t303TglCanncel
	Double t303DPCancelRp
	String t303DPCancelTerbilang
	String t303xKet
	String t303xNamaUser
	String t303xDivisi
	String t303StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:true, maxSize:20
		goods blank:false, nullable:true, maxSize:14
		operation blank:false, nullable:true, maxSize:7
		t303Jumlah1 blank:false, nullable:true, maxSize:8
		t303Jumlah1P blank:false, nullable:true, maxSize:8
		t303Jumlah2 blank:false, nullable:true, maxSize:8
		t303Jumlah2P blank:false, nullable:true, maxSize:8
		namaProsesBP blank:false, nullable:true, maxSize:1
		t303staTgaih blank:false, nullable:true, maxSize:1
		t303HargaRp blank:false, nullable:true, maxSize:8
		t303HargaRpp blank:false, nullable:true, maxSize:8
		t303DiscRp blank:false, nullable:true, maxSize:8
		t302DiscRpP blank:false, nullable:true, maxSize:8
		t303DiscPersen blank:false, nullable:true, maxSize:8
		t303DiscPersenp blank:false, nullable:true, maxSize:8
		t303TotalRp blank:false, nullable:true, maxSize:8
		t303TotalRpP blank:false, nullable:true, maxSize:8
		t303TotalTerbilang blank:false, nullable:true, maxSize:150
		t303TotalTerbilangP blank:false, nullable:true, maxSize:150
		t303NoDP blank:false, nullable:true, maxSize:20
		t303RencanaDP blank:false, nullable:true, maxSize:8
		t303RencanaDPP blank:false, nullable:true, maxSize:8
		t303DPRp blank:false, nullable:true, maxSize:8
		t303DPRpP blank:false, nullable:true, maxSize:8
		t303DPTerbilang blank:false, nullable:true, maxSize:150
		t303DPTerbilangp blank:false, nullable:true, maxSize:150
		t303SisaRp blank:false, nullable:true, maxSize:8
		t303SisaRpP blank:false, nullable:true, maxSize:8
		t303SisaTerbilang blank:false, nullable:true, maxSize:50
		t303SisaTerbilangP blank:false, nullable:true, maxSize:10
		t303StaHarusBookingFee blank:false, nullable:true, maxSize:1
		t303StaOkcancel blank:false, nullable:true, maxSize:1
		t303StaBayarDp blank:false, nullable:true, maxSize:1
		t303StaRequestPart blank:false, nullable:true, maxSize:1
		t303TglCanncel blank:false, nullable:true, maxSize:4
		t303DPCancelRp blank:false, nullable:true, maxSize:8
		t303DPCancelTerbilang blank:false, nullable:true, maxSize:100
		t303xKet blank:true, nullable:true, maxSize:50
		t303xNamaUser blank:false, nullable:true, maxSize:20
		t303xDivisi blank:false, nullable:true, maxSize:20
		t303StaDel blank:false, nullable:true, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T303_PARTSAPP'
		reception column:'T303_T401_NoWO' 
		goods column:'T303_M111_ID'
		operation column:'T303_M053_JobID'
		t303Jumlah1 column:'T303_Jumlah1'//, sqlType:'double'
		t303Jumlah1P column:'T303_Jumlah1P'//, sqlType:'double'
		t303Jumlah2 column:'T303_Jumlah2'//, sqlType:'double'
		t303Jumlah2P column:'T303_Jumlah2P'//, sqlType:'double'
		namaProsesBP column:'T303_M190_ID'
		t303staTgaih column:'T303_satTagih', sqlType:'char(1)'
		t303HargaRp column:'T303_HargaRp'//, sqlType:'double'
		t303HargaRpp column:'T303_HargaRpP'//, sqlType:'double'
		t303DiscRp column:'T303_DiscRp'//, sqlType:'double'
		t302DiscRpP column:'T303_DiscRpP'//, sqlType:'double'
		t303DiscPersen column:'T303_DiscPersen'//, sqlType:'double'
		t303DiscPersenp column:'T303_DiscPersenP'//, sqlType:'double'
		t303TotalRp column:'T303_TotalRp'//, sqlType:'double'
		t303TotalRpP column:'T303_TotalRpP'//, sqlType:'double'
		t303TotalTerbilang column:'T303_TotalTerbilang', sqlType:'varchar(150)'
		t303TotalTerbilangP column:'T303_TotalTerbilangP', sqlType:'varchar(150)'
		t303NoDP column:'T303_NoDP', sqlType:'char(20)'
		t303RencanaDP column:'T303_RencanaDP'//, sqlType:'double'
		t303RencanaDPP column:'T303_RencanaDPP'//, sqlType:'double'
		t303DPRp column:'T303_DPRp'//, sqlType:'double'
		t303DPRpP column:'T303_DPRpP'//, sqlType:'double'
		t303DPTerbilang column:'T303_DPTerbilang', sqlType:'varchar(150)'
		t303DPTerbilangp column:'T303_DPTerbilangP', sqlType:'varchar(150)'
		t303SisaRp column:'T303_SisaRp'//, sqlType:'double'
		t303SisaRpP column:'T303_SisaRpP'//, sqlType:'double'
		t303SisaTerbilang column:'T303_SisaTerbilang', sqlType:'varchar(150)'
		t303SisaTerbilangP column:'T303_SisaTerbilangP', sqlType:'char(150)'
		t303StaHarusBookingFee column:'T303_StaHarusBookingFee', sqlType:'char(1)'
		t303StaOkcancel column:'T303_StaOkCancel', sqlType:'char(1)'
		t303StaBayarDp column:'T303_StaBayarDP', sqlType:'char(1)'
		t303StaRequestPart column:'T303_StaRequestPart', sqlType:'char(1)'
		t303TglCanncel column:'T303_TglCancel'//, sqlType: 'date'
		t303DPCancelRp column:'T303_DPCancelRp'//, sqlType:'double'
		t303DPCancelTerbilang column:'T303_DPCancelTerbilang', sqlType:'varchar(100)'
		t303xKet column:'T303_xKet', sqlType:'varchar(50)'
		t303xNamaUser column:'T303_xNamaUser', sqlType:'varchar(20)'
		t303xDivisi column:'T303_xDivisi', sqlType:'varchar(20)'
		t303StaDel column:'T303_StaDel', sqlType:'char(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}