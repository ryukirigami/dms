package com.kombos.maintable

class JenisInvitation {

	String m204Id
	String m204JenisInvitation
    String m204StaDel = "0";
    Date dateCreated = new Date(); // Menunjukkan kapan baris isian ini dibuat.
    String createdBy; //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated = new Date(); //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "insert" //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		m204Id column : 'M204_ID', sqlType : 'char(2)'
		table 'M204_JENISINVITATION'
		
		m204JenisInvitation column : 'M204_JenisInvitation', sqlType : 'varchar(20)'
		m204StaDel colum : 'M204_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m204JenisInvitation
	}
    static constraints = {
		m204Id (nullable : false, blank : false, unique : false, maxSize : 2)
		m204JenisInvitation (nullable : false, blank : false, maxSize : 20)
		m204StaDel (nullable : true, blank : true, maxSize : 1)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
