package com.kombos.maintable

import com.kombos.administrasi.Bank
import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.MesinEdc
import com.kombos.administrasi.RateBank
import com.kombos.parts.Invoice
import com.kombos.reception.Reception

class Settlement {
    CompanyDealer companyDealer
//	Invoice invoice
    InvoiceT701 invoice
	MetodeBayar metodeBayar
	Integer t704ID
	Date t704TglJamSettlement
	String t704NoWBS
	String t704NoKartu
	String t704NoReff
	Bank bank
	MesinEdc mesinEdc
	RateBank rateBank
	String t704StaPPh23
	Double t704JmlPPh23
	String t704StaPPN
	Double t704JmlPPN
	Double t704JmlBayar
	String t704NoBuktiTransfer
	String t704xNamaUser
	String t704xDivisi
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	Reception reception
	String t704keterangan
	
    static constraints = {
        companyDealer (blank:true, nullable:true)
		/*
		invoice blank:false, nullable:false, unique:true
		metodeBayar blank:false, nullable:false, unique:true
		t704ID blank:false, nullable:false, maxSized:4, unique:true
		t704TglJamSettlement blank:false, nullable:false
		t704NoWBS blank:false, nullable:false, maxSized:50
		t704NoKartu blank:false, nullable:false, maxSized:50
		t704NoReff blank:false, nullable:false, maxSized:50
		bank blank:false, nullable:false
		mesinEdc blank:false, nullable:false
		rateBank blank:false, nullable:false
		t704StaPPh23 blank:false, nullable:false, maxSized:1
		t704JmlPPh23 blank:false, nullable:false
		t704StaPPN blank:false, nullable:false, maxSized:1
		t704JmlPPN blank:false, nullable:false
		t704JmlBayar blank:false, nullable:false
		t704NoBuktiTransfer blank:false, nullable:false, maxSized:50
		t704xNamaUser blank:false, nullable:false, maxSized:20
		t704xDivisi blank:false, nullable:false, maxSized:20
		t704StaDel blank:false, nullable:false, maxSized:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
		*/
		invoice blank:false, nullable:true
		metodeBayar blank:false, nullable:true
		t704ID nullable:true, maxSized:4
		t704TglJamSettlement nullable:true
		t704NoWBS nullable:true, maxSized:50
		t704NoKartu nullable:true, maxSized:50
		t704NoReff nullable:true, maxSized:50
		bank nullable:true
		mesinEdc nullable:true
		rateBank nullable:true
		t704StaPPh23 nullable:true, maxSized:1
		t704JmlPPh23 nullable:true
		t704StaPPN nullable:true, maxSized:1
		t704JmlPPN nullable:true
		t704JmlBayar nullable:true
		t704NoBuktiTransfer nullable:true, maxSized:50
		t704xNamaUser nullable:true, maxSized:20
		t704xDivisi nullable:true, maxSized:20
		staDel blank:false, nullable:false, maxSized:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
		reception nullable : true
		t704keterangan nullable : true
    }
	
	static mapping = {
        autoTimestamp false
        table 'T704_SETTLEMENT'
		invoice column:'T704_T701_NoInv'
		metodeBayar column:'T704_M701_ID'
		t704ID column:'T704_ID', sqlType:'int'
		t704TglJamSettlement column:'T704_TglJamSettlement'//, sqlType: 'date'
		t704NoWBS  column:'T704_NoWBS', sqlType:'varchar(50)'
		t704NoKartu column:'T704_NoKartu', sqlType:'varchar(50)'
		t704NoReff column:'T704_NoReff', sqlType:'varchar(50)'
		bank column:'T704_M702_ID'
		mesinEdc column:'M704_M704_ID'
		rateBank column:'T704_M703_BankCharge'
		t704StaPPh23 column:'T704_StaPPh23', sqlType:'char(1)'
		t704JmlPPh23 column:'T704_JmlPPh23'
		t704StaPPN column:'T704_StaPPN', sqlType:'char(1)'
		t704JmlPPN column:'T704_JmlPPN'
		t704JmlBayar column:'T704_JmlBayar'
		t704NoBuktiTransfer column:'T704_NoBuktiTransfer', sqlType:'varchar(50)'
		t704xNamaUser column:'T704_xNamaUser', sqlType:'varchar(20)'
		t704xDivisi column:'T704_xDivisi', sqlType:'varchar(20)'
		staDel column:'T704_StaDel', sqlType:'char(1)'
		t704keterangan column:'T704_Keterangan'
	}
}
