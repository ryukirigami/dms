package com.kombos.maintable

class Nikah {

    String m062ID
    String m062StaNikah
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m062ID blank:false, nullable : false , maxSize : 2, matches : "[0-9]{2}"//validasi untuk format 00
        m062StaNikah blank:false, nullable : false , maxSize : 20
        staDel blank:false, nullable : false , maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        m062ID column : 'M062_ID', sqlType : 'char(2)'
		m062StaNikah column : 'M062_staNikah', sqlType : 'varchar(20)'
		staDel column : 'M062_staDel', sqlType : 'char(1)'
		table ' M062_NIKAH'
	}
	
	String toString(){
		return m062StaNikah
	}
}
