package com.kombos.maintable

class StaHadir {

    String m017ID
    String m017NamaStaHadir
    String m017StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m017ID maxSize : 2, nullable : false, blank : false
        m017NamaStaHadir maxSize : 20, nullable : false, blank : false
        m017StaDel maxSize : 1, nullable : false, blank : false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table ' M017_STAHADIR'
        m017ID column : 'M017_ID', sqlType : 'char(2)', unique: true
        m017NamaStaHadir column : 'M017_NamaStaHadir', sqlType : 'varchar(20)'
        m017StaDel column : 'M017_StaDel', sqlType : 'char(1)'
    }

    String toString() {
		return m017NamaStaHadir
	}
}
