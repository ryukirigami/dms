package com.kombos.maintable

class MetodeBayar {

	String m701Id
	String m701MetodeBayar
	String m701StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

//    m701Id | m701MetodeBayar
//    -------------------------
//    0      | CASH
//    1      | DEBIT
//    2      | TRANSFER
//    3      | WBS
//    4      | KARTU KREDIT

	static mapping = {
		m701Id column : 'M701_ID', sqlType : 'char(2)'
		m701MetodeBayar column : 'M701_MetodeBayar', sqlType : 'varchar(50)'
		m701StaDel column : 'M701_StaDel', sqlType : 'char(1)'
		
		table 'M701_METODEBAYAR'
	}
    static constraints = {
		m701Id (nullable : false, unique : true, maxSize : 2, blank : false)
		m701MetodeBayar (nullable : false, blank : false, maxSize : 50) 
		m701StaDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m701MetodeBayar
	}
}
