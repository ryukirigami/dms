package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.parts.Goods

class Part {
    CompanyDealer companyDealer
	Customer customer
	HistoryCustomer historyCustomer
	CustLamaWeb custLamaWeb
	Goods goods
	Integer t189Qty
	String t189StaUtama
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
        companyDealer (blank:true, nullable:true)
		customer nullable : false, blank : false, unique : true
		historyCustomer nullable : false, blank : false, unique : true
		custLamaWeb nullable : false, blank : false, unique : true
		goods nullable : false, blank : false, unique : true
		t189Qty nullable : false, blank : false
		t189StaUtama nullable : false, blank : false, maxSize : 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

	static mapping = {
        autoTimestamp false
        table 'T189_PART'
		customer column : 'T189_T102_ID'
		historyCustomer column : 'T189_T182_ID'
		custLamaWeb column : 'T189_T188_ID'
		goods column : 'T189_M111_ID'
		t189Qty column : 'T189_Qty', sqlType : 'int'
		t189StaUtama column : 'T189_StaUtama', sqlType : 'char(1)'
	}
}
