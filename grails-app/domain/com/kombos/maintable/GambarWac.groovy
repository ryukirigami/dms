package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class GambarWac {

	CompanyDealer companyDealer
	byte[] m406GambarWac
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'M406_GAMBARWAC'
		
		companyDealer column : 'M406_4011_ID'
		m406GambarWac column : 'M406_GambarWac', sqlType : 'blob', length : 16
	}
	
	String toString(){
		return m406GambarWac
	}
    static constraints = {
		companyDealer (nullable : false, blank : false)
		m406GambarWac (nullable : false, blank : false, maxSize : 16)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
