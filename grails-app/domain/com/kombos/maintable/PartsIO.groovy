package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Goods

class PartsIO {

	CompanyDealer companyDealer
	Goods goods
	Date t121Tgl
	String t121JenisIO
	Integer t121ID
	String t121NoReff
	String t121TglReff
	Double t121Qty1
	Double t121Qty2
	Double t121LandedCost
	String t121Ket
	String t121xNamaUser
	String t121xNamaDivisi
	String t121StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static constraints = {
		companyDealer nullable : false, blank:false, unique:true
		goods nullable : false, blank:false, unique:true
		t121Tgl nullable : false, blank:false, unique:true
		t121JenisIO nullable : false, blank:false, unique:true, maxSize : 1
		t121ID nullable : false, blank:false, unique:true, maxSize : 4
		t121NoReff nullable : false, blank:false, maxSize :20
		t121TglReff nullable : false, blank:false, maxSize :50
		t121Qty1 nullable : false, blank:false, maxSize :8
		t121Qty2 nullable : false, blank:false, maxSize :8
		t121LandedCost nullable : false, blank:false, maxSize :8
		t121Ket nullable : false, blank:false, maxSize :50
		t121xNamaUser nullable : false, blank:false, maxSize :20
		t121xNamaDivisi nullable : false, blank:false, maxSize :20
		t121StaDel nullable : false, maxSize :1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T121_PARTSIO'
		companyDealer column : 'T121_M011_ID'
		goods column : 'T121_M111_ID'
		t121Tgl column : 'T121_Tgl', sqlType : 'date'
		t121JenisIO column : 'T121_JenisIO', sqlType : 'char(1)'
		t121ID column : 'T121_ID', sqlType : 'int'
		t121NoReff column : 'T121_NoReff', sqlType : 'varchar(20)'
		t121TglReff column : 'T121_TglReff', sqlType : 'varchar(50)'
		t121Qty1 column : 'T121_Qty1'
		t121Qty2 column : 'T121_Qty2'
		t121LandedCost column : 'T121_LandedCost'
		t121Ket column : 'T121_Ket', sqlType : 'varchar(50)'
		t121xNamaUser column : 'T121_xNamaUser', sqlType : 'varchar(20)'
		t121xNamaDivisi column : 'T121_xNamaDivisi', sqlType : 'varchar(20)'
		t121StaDel column : 'T121_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return t121JenisIO
	}
}
