package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.Invoice

class DiscInformation {
    CompanyDealer companyDealer
	Invoice invoice
	Integer t705ID
	String t705NamaJobPart
	String t705StaJobPar
	Double t705Harga
	Double t705KategoriJob
	String t705CustVehicleDiscPersen
	Double t705CustVehicleDiscRp
	Double t705StaticDiscPersen
	Double t705StaticDiscRp
	Double t705DinamicDiscPersen
	Double t705DinamicDiscRp
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		invoice blank:false, nullable:false, unique:true
		t705ID blank:false, nullable:false, maxSize:4, unique:true
		t705NamaJobPart blank:false, nullable:false, maxSize:10
		t705StaJobPar blank:false, nullable:false, maxSize:1
		t705Harga blank:false, nullable:false
		t705KategoriJob blank:false, nullable:false, maxSize:50
		t705CustVehicleDiscPersen blank:false, nullable:false
		t705CustVehicleDiscRp blank:false, nullable:false
		t705StaticDiscPersen blank:false, nullable:false
		t705StaticDiscRp blank:false, nullable:false
		t705DinamicDiscPersen blank:false, nullable:false
		t705DinamicDiscRp blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T705_DISCINFORMATION'
		invoice column:'T705_T701_NoInv'
		t705ID column:'T705_ID', sqlType:'int'
		t705NamaJobPart column:'T705_NamaJobPart', sqlType:'char(10)'
		t705StaJobPar column:'T705_StaJobPart', sqlType:'char(1)'
		t705Harga column:'T705_Harga'
		t705KategoriJob column:'T705_KategoriJob', sqlType:'varchar(50)'
		t705CustVehicleDiscPersen column:'T705_CustVehicleDiscPersen'
		t705CustVehicleDiscRp column:'T705_CustVehicleDiscRp'
		t705StaticDiscPersen column:'T705_StaticDiscPersen'
		t705StaticDiscRp column:'T705_StaticDiscRp'
		t705DinamicDiscPersen column:'T705_DinamicDiscPersen'
		t705DinamicDiscRp column:'T705_DinamicDiscRp'
	}
}
