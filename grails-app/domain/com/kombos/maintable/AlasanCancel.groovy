package com.kombos.maintable

class AlasanCancel {

	//int m402Id
	String m402NamaAlasanCancel
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static mapping = {
		//m402Id column : 'M402_ID', sqlType : 'int'
        autoTimestamp false
        table 'M402_ALASANCANCEL'
		m402NamaAlasanCancel column : 'M402_NamaAlasanCancel', sqlType : 'varchar(50)'
		staDel column : 'M402_StaDel', sqlType : 'char(1)'
	}
	
	String toString(){
		return m402NamaAlasanCancel
	}

    static constraints = {
		//m402Id (nullable : false, unique : true, blank : false)
		m402NamaAlasanCancel (nullable : false, maxSize : 50, blank : false)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
}
