package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.board.JPB
import com.kombos.parts.Goods
import com.kombos.production.Masalah
import com.kombos.woinformation.Actual

class KebutuhanPart {
	CompanyDealer companyDealer
	JPB jpb
	Actual actual
	Masalah masalah
	Goods goods
	Double t509Qty1
	Double t509Qty2
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false
		jpb blank:false, nullable:false
		actual blank:false, nullable:false
		masalah blank:false, nullable:false
		goods blank:false, nullable:false
		t509Qty1 blank:false, nullable:false
		t509Qty2 blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
		table 'T509_KEBUTUHANPART'
		companyDealer column:'T509_M011_ID'
		jpb column:'T509_T451_IDJPB'
		actual column:'T509_T452_ID'
		masalah column:'T509_T501_ID'
		goods column:'T509_M111_ID'
		t509Qty1 column:'T509_Qty1'
		t509Qty2 column:'T509_Qty2'
	}
}
