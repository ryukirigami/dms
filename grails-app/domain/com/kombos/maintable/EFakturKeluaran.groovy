package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class EFakturKeluaran {

    CompanyDealer companyDealer
    String docNumber // SESUAKAN DENGAN JENIS INPUTAN EFAKTUR
    Date tanggal

    String MASA_PAJAK
    String TAHUN_PAJAK
    String REFERENSI
    String NO_SI
    Date TGL_SI
    String NAMA_CUSTOMER
    String NPWP
    String ALAMAT
    BigDecimal BIAYA_JASA
    BigDecimal BIAYA_PARTS
    BigDecimal BIAYA_OLI
    BigDecimal BIAYA_MATERIAL
    BigDecimal BIAYA_SUBLET
    BigDecimal BIAYA_ADM
    BigDecimal DISC_JASA
    BigDecimal DISC_PARTS
    BigDecimal DISC_BAHAN
    BigDecimal DISC_OLI
    BigDecimal DPP
    BigDecimal PPN
    BigDecimal TOTAL

    String NO_FAKTUR

    String staDel
    String staOriRev
    String staAprove   // 1 = APPROVED, 2 = REJECTED, 0 = WAIT_APPROVE

    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        companyDealer (blank:true, nullable:true)
        docNumber (blank: true, nullable: true)
        tanggal (blank: true, nullable: true)

        MASA_PAJAK (blank: true, nullable: true)
        TAHUN_PAJAK (blank: true, nullable: true)
        REFERENSI (blank: true, nullable: true)
        NO_SI (blank: true, nullable: true)
        TGL_SI (blank: true, nullable: true)
        NAMA_CUSTOMER (blank: true, nullable: true)
        NPWP (blank: true, nullable: true)
        ALAMAT (blank: true, nullable: true)
        BIAYA_JASA (blank: true, nullable: true)
        BIAYA_PARTS (blank: true, nullable: true)
        BIAYA_OLI (blank: true, nullable: true)
        BIAYA_MATERIAL (blank: true, nullable: true)
        BIAYA_SUBLET (blank: true, nullable: true)
        BIAYA_ADM (blank: true, nullable: true)
        DISC_JASA (blank: true, nullable: true)
        DISC_PARTS (blank: true, nullable: true)
        DISC_BAHAN (blank: true, nullable: true)
        DISC_OLI(blank: true, nullable: true)
        DPP (blank: true, nullable: true)
        PPN (blank: true, nullable: true)
        TOTAL (blank: true, nullable: true)

        NO_FAKTUR (blank: true, nullable: true)

        staDel (nullable : true, blank : true, maxSize : 1)
        staAprove (nullable : true, blank : true, maxSize :1)
        staOriRev (nullable : true, blank : true, maxSize : 1)
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }


    static mapping = {
        autoTimestamp false
        table 'TBL_EFAKTUR_KELUARAN'
    }

}

