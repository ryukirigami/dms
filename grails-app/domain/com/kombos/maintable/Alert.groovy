package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.JenisAlert

class Alert {
	CompanyDealer companyDealer
	JenisAlert jenisAlert
	Date tanggal
	TipeAlert tipeAlert
	String namaTabel
	String namaFK
	String noDokumen
	Date tglJamAlert
	String staBaca
	Date tglJamBaca
	String ket1
	String xNamaUserBaca
	String staTindakLanjut
	Date tglJamTinjakLanjut
	String ket2
	String xNamaUserTindakLanjut
	String staDel // beforeInsert
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
	
	String statusAksi
	Date tglAksi
	String userAksi

    static constraints = {
		companyDealer blank:false, nullable:false
		jenisAlert blank:false, nullable:false
		tanggal blank:false, nullable:false
		tipeAlert blank:true, nullable:true
		namaTabel blank:true, nullable:true, maxSize: 50
		namaFK blank:true, nullable:true, maxSize: 250
		noDokumen blank:false, nullable:false, maxSize: 50
		tglJamAlert blank:false, nullable:false
		staBaca blank:true, nullable:true, maxSize: 1
		tglJamBaca blank:true, nullable:true
		ket1 blank:true, nullable:true, maxSize: 50
		xNamaUserBaca blank:true, nullable:true, maxSize: 50
		staTindakLanjut blank:true, nullable:true, maxSize: 1
		tglJamTinjakLanjut blank:true, nullable:true
		ket2 blank:true, nullable:true, maxSize: 50
		xNamaUserTindakLanjut blank:true, nullable:true, maxSize: 50
		staDel blank:false, nullable:false, maxSize: 1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T901_Alert'
		id column:'T901_ID', sqlType:'int'
		companyDealer column:'T901_companyDealer'
		jenisAlert column:'T901_jenisAlert'
		tanggal column:'T901_tanggal'//, sqlType: 'date'
		tipeAlert column:'T901_tipeAlert'
		namaTabel column:'T901_namaTabel', sqlType:'varchar(50)'
		namaFK column:'T901_namaFK', sqlType:'varchar(250)'
		noDokumen column:'T901_noDokumen', sqlType:'varchar(50)'
		tglJamAlert column:'T901_tglJamAlert'//, sqlType: 'date'
		staBaca column:'T901_staBaca', sqlType:'char(1)'
		tglJamBaca column:'T901_tglJamBaca'//, sqlType: 'date'
		ket1 column:'T901_ket1', sqlType:'varchar(50)'
		xNamaUserBaca column:'T901_xNamaUserBaca', sqlType:'varchar(50)'
		staTindakLanjut column:'T901_staTindakLanjut', sqlType:'char(1)'
		tglJamTinjakLanjut column:'T901_tglJamTinjakLanjut'//, sqlType: 'date'
		ket2 column:'T901_ket2', sqlType:'varchar(50)'
		xNamaUserTindakLanjut column:'T901_xNamaUserTindakLanjut', sqlType:'varchar(50)'
		staDel column:'T770_Stadel', sqlType:'char(1)'
		statusAksi formula: "case when T901_staTindakLanjut = '1' then 'Tindak Lanjut' when T901_staBaca = '1' then 'Baca' else 'Baru' end"
		tglAksi formula: "case when T901_staTindakLanjut = '1' then T901_tglJamTinjakLanjut when T901_staBaca = '1' then T901_tglJamBaca else null end"
		userAksi formula: "case when T901_staTindakLanjut = '1' then T901_xNamaUserTindakLanjut when T901_staBaca = '1' then T901_xNamaUserBaca else null end"
	}
	
	def beforeUpdate() {
		lastUpdated = new Date();
		lastUpdProcess = "UPDATE"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			}
		} catch (Exception e) {
		}
	}

	def beforeInsert() {
//		dateCreated = new Date()
		lastUpdProcess = "INSERT"
//		lastUpdated = new Date()
		staDel = "0"
		staBaca = "0"
		staTindakLanjut = "0"
		try {
			if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
				createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
				updatedBy = createdBy
			}
		} catch (Exception e) {
		}

	}

	def beforeValidate() {
		//createdDate = new Date()
		if(!lastUpdProcess)
			lastUpdProcess = "INSERT"
		if(!lastUpdated)
			lastUpdated = new Date()
		if(!staDel)
			staDel = "0"
		if(!createdBy){
			createdBy = "SYSTEM"
			updatedBy = createdBy
		}
	}
}