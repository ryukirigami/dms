package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KabKota
import com.kombos.administrasi.Kecamatan
import com.kombos.administrasi.Kelurahan
import com.kombos.administrasi.Provinsi
import com.kombos.customerprofile.Agama
import com.kombos.customerprofile.Hobby
import com.kombos.customerprofile.JenisIdCard

class BillTo {
    CompanyDealer companyDealer
	String t107Id
	PeranCustomer peranCustomer
	String t107GelarD
	String t107NamaDepan
	String t107NamaBelakang
	String t107GelarB
	String t107NomorIDCard
	String t107JenisKelamin
	Date t107TglLahir
	JenisIdCard jenisIDCard
	Agama agama
	Nikah nikah
	Hobby hobby
	String t107Alamat
	String t107Rt
	String t107Rw
	Provinsi provinsi
	KabKota kabKota
	Kecamatan kecamatan
	Kelurahan kelurahan
	String t107NoTelpKantor
	String t107NoTelpRumah
	String t107NoHp
	String t107Email
	String t107NPWP
	String t107NoFakturPajakStd
	String t107NoFax
	String t107AlamatNPWP
	Provinsi provinsiIDNPWP
	KabKota kabKotaIDNPWP
	Kecamatan kecamatanIDNPWP
	Kelurahan kelurahanIDNPWP
	String t107KodePosNPWP
	String t107Ket
	String t107KodePos
	String t107xNamaUser
	String t107xNamaDivisi
	String t107StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
		table 'T107_BILLTO'
		
		t107Id column : 'T107_ID', sqlType : 'char(12)'
		peranCustomer column : 'T107_M115_ID'
		provinsi column : 'T107_M001_ID'
		kabKota column : 'T107_M002_ID'
		kecamatan column : 'T107_M003_ID'
		kelurahan column : 'T107_M004_ID'
		t107GelarD column : 'T107_GelarD', sqlType : 'varchar(50)'
		t107NamaDepan column : 'T107_NamaDepan', sqlType : 'varchar(50)'
		t107NamaBelakang column : 'T107_NamaBelakang', sqlType : 'varchar(50)'
		t107GelarB column : 'T107_GelarB', sqlType : 'varchar(50)'
		t107JenisKelamin column : 'T01_JenisKelamin', sqlType : 'char(1)'
		t107TglLahir column : 'T107_TglLahir', sqlType : 'date'
		t107Alamat column : 'T107_Alamat', sqlType : 'varchar(20)'
		t107NoFakturPajakStd column : 'T107_NoFakturPajakStd', sqlType : 'varchar(20)'
		t107Rt column : 'T107_RT1', sqlType : 'varchar(20)'
		t107Rw column : 'T107_RTW2', sqlType : 'varchar(20)'
		t107NoFax column : 'T107_NoFax', sqlType : 'varchar(50)'
		t107NomorIDCard column : 'T107_NomorIDCard', sqlType : 'varchar(20)'
		jenisIDCard column : 'T107_M060_ID'
		agama column : 'T107_M061_ID'
		nikah column : 'T107_M062_ID'
		hobby column : 'T107_M063_ID'
		t107NoTelpKantor column : 'T107_NoTelp', sqlType : 'varchar(20)'
		t107NoHp column : 'T107_HP', sqlType : 'varchar(20)'
		t107Email column : 'T107_Email', sqlType : 'varchar(20)'
		t107NPWP column : 'T107_NoNPWP', sqlType : 'varchar(20)'
		t107AlamatNPWP column : 'T107_AlamatNPWP', sqlType : 'varchar(20)'
		provinsiIDNPWP column : 'T107_M001_IDNPWP'
		kabKotaIDNPWP column : 'T107_M002_IDNPWP'
		kecamatanIDNPWP column : 'T107_M003_IDNPWP'
		kelurahanIDNPWP column : 'T107_M004_IDNPWP'
		t107KodePosNPWP column : 'T107_KodePosNPWP', sqlType : 'varchar(5)'
		t107KodePos column : 'T107_KodePos', sqlType : 'varchar(5)'
		t107Ket column : 'T107_Ket', sqlType : 'varchar(50)'
		t107xNamaUser column : 'T107_xNamaUser', sqlType : 'varchar(20)'
		t107xNamaDivisi column : 'T107_xNamaDivisi', sqlType : 'varchar(20)'
		t107StaDel column : 'T107_StaDel', sqlType : 'char(1)'
		
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t107Id (nullable:  false, blank : false,unique : true)
		peranCustomer (nullable:  false, blank : false)
		provinsi (nullable:  false, blank : false)
		kabKota (nullable:  false, blank : false)
		kecamatan (nullable:  false, blank : false)
		kelurahan (nullable:  false, blank : false)
		t107GelarD (nullable:  false, blank : false)
		t107NamaDepan (nullable:  false, blank : false)
		t107NamaBelakang (nullable:  false, blank : false)
		t107GelarB (nullable:  false, blank : false)
		t107JenisKelamin (nullable:  false, blank : false)
		t107TglLahir (nullable:  false, blank : false)
		t107Alamat (nullable:  false, blank : false)
		t107NoFakturPajakStd (nullable:  false, blank : false)
		t107Rt (nullable:  false, blank : false)
		t107Rw (nullable:  false, blank : false)
		t107NoFax (nullable:  false, blank : false)
		t107NomorIDCard (nullable:  false, blank : false)
		jenisIDCard (nullable:  false, blank : false)
		agama (nullable:  false, blank : false)
		nikah (nullable:  false, blank : false)
		hobby (nullable:  false, blank : false)
		t107NoTelpKantor (nullable:  false, blank : false)
		t107NoHp (nullable:  false, blank : false)
		t107Email (nullable:  false, blank : false)
		t107NPWP (nullable:  false, blank : false)
		t107AlamatNPWP (nullable:  false, blank : false)
		provinsiIDNPWP (nullable:  false, blank : false)
		kabKotaIDNPWP (nullable:  false, blank : false)
		kecamatanIDNPWP (nullable:  false, blank : false)
		kelurahanIDNPWP (nullable:  false, blank : false)
		t107KodePosNPWP (nullable:  false, blank : false)
		t107KodePos (nullable:  false, blank : false)
		t107Ket (nullable:  false, blank : false)
		t107xNamaUser (nullable:  false, blank : false)
		t107xNamaDivisi (nullable:  false, blank : false)
		t107StaDel (nullable:  false, blank : false)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t107NamaDepan + " " + t107NamaBelakang
	}
}
