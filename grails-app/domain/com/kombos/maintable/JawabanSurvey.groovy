package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.CustomerSurvey
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.JenisSurvey

class JawabanSurvey {
    CompanyDealer companyDealer
	CustomerVehicle customerVehicle
	CustomerSurvey customerSurvey
	Integer t108ID
	JenisSurvey jenisSurvey
	Date t108TglBerlakuPertanyaanSurvey
	PertanyaanSurvey pertanyaanSurvey
	String t108Nomor
	String t108Pertanyaan
	String t108Jawaban
	String t108Nilai
	String t108Keterangan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	static mapping = {
        autoTimestamp false
        table 'T108_JAWABANSURVEY'
		customerVehicle column : 'T108_T108_VinCode'
		customerSurvey column : 'T108_T104_ID'
		t108ID column : 'T108_ID', sqlType : 'int'
		jenisSurvey column : 'T108_M131_ID'
		t108TglBerlakuPertanyaanSurvey column : 'T108_T132_TanggalBerlaku', sqlType : 'date'
		pertanyaanSurvey column : 'T108_T132_ID'
		t108Nomor column : 'T108_Nomor', sqlType : 'varchar(5)'
		t108Pertanyaan column : 'T108_Pertanyaan', sqlType : 'varchar(250)'
		t108Jawaban column : 'T108_Jawaban', sqlType : 'varchar(250)'
		t108Nilai column : 'T108_Nilai', sqlType : 'varchar(50)'
		t108Keterangan column : 'T108_Keterangan', sqlType : 'varchar(50)'
		
	}

    static constraints = {
        companyDealer (blank:true, nullable:true)
		customerVehicle (nullable : false, blank : false)
		customerSurvey (nullable : false, blank : false)
		t108ID (nullable : false, blank : false, unique : true, maxSize : 4)
		jenisSurvey (nullable : false, blank : false)
		t108TglBerlakuPertanyaanSurvey (nullable : false, blank : false)
		pertanyaanSurvey (nullable : false, blank : false)
		t108Nomor (nullable : false, blank : false, maxSize : 5)
		t108Pertanyaan (nullable : false, blank : false, maxSize : 250)
		t108Jawaban (nullable : false, blank : false, maxSize : 250)
		t108Nilai (nullable : false, blank : false, maxSize : 50)
		t108Keterangan (nullable : false, blank : false, maxSize : 50)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return t108ID
	}
}
