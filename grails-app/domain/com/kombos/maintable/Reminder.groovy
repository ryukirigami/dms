package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.customerprofile.CustomerVehicle

class Reminder {
    CompanyDealer companyDealer
    CustomerVehicle customerVehicle
//	Integer t201ID

    JenisReminder m201ID
    StatusReminder m202ID
    Operation operation
    Integer t201LastKM
    Date t201LastServiceDate
    Date t201TglJatuhTempo
    Date t201TglDM
    Date t201TglSMS
    Date t201TglEmail
    Date t201TglCall
    String t201Ket
    String t201xNamaUser
    String t201xNamaDivisi
    String staDel
    String staKirim
    Date dateCreated = new Date() // Menunjukkan kapan baris isian ini dibuat.
    String createdBy//Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated = new Date()  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "insert" //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        m201ID(nullable: true)
        companyDealer blank: true, nullable: true, maxSize: 4
        customerVehicle blank: true, nullable: true, maxSize: 17
        m202ID blank: true, nullable: true, maxSize: 2
        operation blank: true, nullable: true, maxSize: 7
        t201LastKM blank: true, nullable: true, maxSize: 4
        t201LastServiceDate blank: true, nullable: true, maxSize: 4
        t201TglJatuhTempo blank: true, nullable: true, maxSize: 4
        t201TglDM blank: true, nullable: true, maxSize: 4
        t201TglSMS blank: true, nullable: true, maxSize: 4
        t201TglEmail blank: true, nullable: true, maxSize: 4
        t201TglCall blank: true, nullable: true, maxSize: 4
        t201Ket blank: true, nullable: true, maxSize: 50
        t201xNamaUser blank: true, nullable: true, maxSize: 20
        t201xNamaDivisi blank: true, nullable: true, maxSize: 20
        staDel blank: true, nullable: true, maxSize: 1
        staKirim blank: true, nullable: true, maxSize: 1
        createdBy nullable: true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T201_REMINDER'
        companyDealer column: 'T201_M011_ID'
        customerVehicle column: 'T201_T103_VinCode'
        id column: "t201ID"
//		t201ID column:'T201_ID', sqlType:'int'
        m201ID column: 'T201_M201_ID'
        m202ID column: 'T201_M202_ID'
        operation column: 'T201_M053_JobID'
        t201LastKM column: 'T201_LastKM', sqlType: 'int'
        t201LastServiceDate column: 'T201_lastServiceDate'//, sqlType: 'date'
        t201TglJatuhTempo column: 'T201_TglJatuhTempo'//, sqlType: 'date'
        t201TglDM column: 'T201_TglDM'
        t201TglSMS column: 'T201_TglSMS'
        t201TglEmail column: 'T201_TglEmail'
        t201TglCall column: 'T201_TglCall'
        t201Ket column: 'T201_Ket', sqlType: 'varchar(50)'
        t201xNamaUser column: 'T201_xNamaUser', sqlType: 'varchar(20)'
        t201xNamaDivisi column: 'T201_xNamaDivisi', sqlType: 'varchar(20)'
        staDel column: 'T201_StaDel', sqlType: 'char(1)'
        staKirim column: 'T201_staKirim', sqlType: 'char(1)'
    }

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "update"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()


        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if (!lastUpdProcess)
            lastUpdProcess = "insert"
        if (!lastUpdated)
            lastUpdated = new Date()
        if (!createdBy) {
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}