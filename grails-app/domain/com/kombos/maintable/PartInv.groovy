package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.parts.Goods
//import com.kombos.parts.Invoice
import com.kombos.maintable.InvoiceT701

class PartInv {
    CompanyDealer companyDealer
    String t703NoPartInv
    //Invoice invoice
    InvoiceT701 invoice
    Goods goods
    Operation operation
    Double t703Jumlah1
    Double t703Jumlah2
    String t703staTampilDiInv
    String t703staTagih
    Double t703LandedCost
    Double t703HargaRp
    Double t703DiscRp
    Double t703DiscPersen
    Double t703TotalRp
    Double t703DPRp
    Double t703SisaTagihanRp
    String t703Remark
    String t703xKet
    String t703xNamaUser
    String t703xDivisi
    String t703StaDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        t703NoPartInv blank:true, nullable:true, maxSize:20
        invoice blank:true, nullable:true
        goods blank:true, nullable:true
        operation blank:true, nullable:true
        t703Jumlah1 blank:true, nullable:true
        t703Jumlah2 blank:true, nullable:true
        t703staTampilDiInv blank:true, nullable:true, maxSize:1
        t703staTagih blank:true, nullable:true, maxSize:1
        t703LandedCost blank:true, nullable:true
        t703HargaRp blank:true, nullable:true
        t703DiscRp blank:true, nullable:true
        t703DiscPersen blank:true, nullable:true
        t703TotalRp blank:true, nullable:true
        t703DPRp blank:true, nullable:true
        t703SisaTagihanRp blank:true, nullable:true
        t703Remark blank:true, nullable:true, maxSize:50
        t703xKet blank:true, nullable:true, maxSize:50
        t703xNamaUser blank:true, nullable:true, maxSize:20
        t703xDivisi blank:true, nullable:true, maxSize:20
        t703StaDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table 'T703_PARTINV'
        t703NoPartInv column:'T703_NoPartInv', sqlType:'char(20)'
        invoice column:'T703_T701_NoInv'
        goods column:'T703_M111_ID'
        operation column:'T703_M053_JobID'
        t703Jumlah1 column:'T703_Jumlah1'
        t703Jumlah2 column:'T703_Jumlah2'
        t703staTampilDiInv column:'T703_staTampilDiInv', sqlType:'char(1)'
        t703staTagih column:'T703_staTagih', sqlType:'char(1)'
        t703LandedCost column:'T703_LandedCost'
        t703HargaRp column:'T703_HargaRp'
        t703DiscRp column:'T703_DiscRp'
        t703DiscPersen column:'T703_DiscPersen'
        t703TotalRp column:'T703_TotalRp'
        t703DPRp column:'T703_DPRp'
        t703SisaTagihanRp column:'T703_SisaTagihanRp'
        t703Remark column:'T703_Remark', sqlType:'varchar(50)'
        t703xKet column:'T703_xKet', sqlType:'varchar(50)'
        t703xNamaUser column:'T703_xNamaUser', sqlType:'varchar(20)'
        t703xDivisi column:'T703_xDivisi', sqlType:'varchar(20)'
        t703StaDel column:'T703_StaDel', sqlType:'char(1)'
    }
}
