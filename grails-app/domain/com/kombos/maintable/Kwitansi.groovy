package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.reception.Reception

class Kwitansi {
    CompanyDealer companyDealer
	String t722NoKwitansi
	String t722StaBookingOnRisk
	Reception reception
	Customer customer
	HistoryCustomer historyCustomer
	String t722Nama
	String t722Nopol
	Double t722JmlUang
	String t722Ket
	String t722Terbilang
	String t722xNamaUser
	String t722xDivisi
	String t722StaDel
    String t722StaBFPartService
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		t722NoKwitansi blank:false, nullable:false, unique:false, maxSize:20
		t722StaBookingOnRisk blank:false, nullable:false, maxSize:1
		reception blank:false, nullable:false
		customer blank:false, nullable:false
		historyCustomer blank:false, nullable:false
		t722Nama blank:false, nullable:false
		t722Nopol blank:false, nullable:false
		t722JmlUang blank:false, nullable:false
		t722Ket blank:false, nullable:false
		t722Terbilang blank:false, nullable:false
		t722xNamaUser blank:true, nullable:true
		t722xDivisi blank:true, nullable:true
		t722StaDel blank:false, nullable:false
        t722StaBFPartService nullable: true, blank: true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {table 'T722_KWITANSI'
        autoTimestamp false
        t722NoKwitansi column:'T722_NoKwitansi', sqlType:'char(20)'
		t722StaBookingOnRisk column:'T722_staBookingOnRisk', sqlType:'char(1)'
		reception column:'T722_T401_NoWO'
		customer column:'T722_T182_T102_ID'
		historyCustomer column:'T722_T182_ID'
		t722Nama column:'T722_Nama', sqlType:'varchar(250)'
		t722Nopol column:'T722_Nopol', sqlType:'varchar(50)'
		t722JmlUang column:'T722_JmlUang'
		t722Ket column:'T722_Ket', sqlType:'varchar(250)'
		t722Terbilang column:'T722_Terbilang', sqlType:'varchar(250)'
		t722xNamaUser column:'T722_xNamaUser', sqlType:'varchar(20)'
		t722xDivisi column:'T722_xDivisi', sqlType:'varchar(20)'
		t722StaDel column:'T722_StaDel', sqlType:'char(1)'
        t722StaBFPartService column: 'T722_BPPartsService', sqlType: 'varchar(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        t722StaDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        //createdDate = new Date()
        if(!lastUpdProcess)
            lastUpdProcess = "INSERT"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!t722StaDel)
            t722StaDel = "0"
        if(!createdBy){
            createdBy = "SYSTEM"
            updatedBy = createdBy
        }
    }
}