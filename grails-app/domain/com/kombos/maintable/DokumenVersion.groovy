package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer

class DokumenVersion {
    CompanyDealer companyDealer
    String t952KodeDokumen
    String t952NoDokumen
    Integer t952VersionCounter
    Date t952Tanggal
    String t952XNamaUser
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        t952KodeDokumen (nullable : false, blank : false, maxSize : 10)
        t952NoDokumen (nullable : false, blank : false, maxSize : 10)
        t952VersionCounter (nullable : false, blank : false)
        t952Tanggal (nullable : false, blank : false)
        t952XNamaUser (nullable : true, blank : true, maxSize : 50)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping={
        t952KodeDokumen column: "T952_KodeDokumen", sqlType: "varchar(10)"
        t952NoDokumen column: "T952_NoDokumen", sqlType: "varchar(10)"
        t952VersionCounter column: "T952_VersionCounter"
        t952Tanggal column: "T952_Tanggal"//, sqlType: 'date'
        t952XNamaUser column: "T952_xNamaUser", sqlType: "varchar(50)"
        table "T952_DOKUMENVERSION"
    }

}
