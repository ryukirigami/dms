package com.kombos.woinformation

import com.kombos.administrasi.CompanyDealer
import com.kombos.reception.Appointment
import com.kombos.reception.Reception

class Prediagnosis {
    CompanyDealer companyDealer
	Reception reception
	Integer t406ID
	Integer t406T305_M302_ID
	String t406KeluhanCust
	String t406StaAppRcp
	String t406StaButuhDiagnose
	String t406StaSudahDiagnoseApp
	String t406StaSudahDiagnoseRcp
	Appointment appointment
	Integer t406Km
	Date t406TglDiagnosis
	String t406StaGejalaHariIni
	String t406StaGejalaMingguLalu
	String t406StaGejalaLainnya
	String t406GejalaLainnya
	String t406StaFrekSekali
	String t406StaFrekKadang
	String t406StaFrekSelalu
	String t406StaFrekLainnya
	String t406FrekLainnya
	String t406StaMILOn
	String t406StaMILKedip
	String t406StaMILOff
	String t406MILLainnya
	String t406StaMILIdling
	String t406StaMILStarting
	String t406StaMILKonstan
	String t406MILKecepatanLainnya
	String t406StaMesinPanas
	String t406StaMesinDingin
	String t406StaPanasMesinLainnya
	String t406PanasMesinLainnya
	String t406Suhu
	String t406StaGigi1
	String t406StaGigi2
	String t406StaGigi3
	String t406StaGigi4
	String t406StaGigi5
	String t406StaGigiP
	String t406StaGigiR
	String t406StaGigiN
	String t406StaGigiD
	String t406StaGigiS
	String t406StaGigiLainnya
	String t406GigiLainnya
	Integer t406Kecepatan
	Integer t406RPM
	Integer t406Beban
	Integer t406JmlPenumpang
	String t406StaDalamKota
	String t406StaJalanLurus
	String t406StaAkselerasi
	String t406StaLuarKota
	String t406StaDatar
	String t406StaRem
	String t406StaTol
	String t406StaTanjakan
	String t406StaBelok
	String t406StaTurunan
	String t406StaKondisiLainnya
	String t406KondisiLainnya
	String t406StaMacet
	String t406StaLancar
	String t406StaLalinLainnya
	String t406LalinLainnya
	String t406StaCerah
	String t406StaBerawan
	String t406StaHujan
	String t406StaPanas
	String t406StaLembab
	Integer t406Temperatur
	String t406StaEG
	String t406StaSuspensi
	String t406StaKlasifikasiRem
	String t406StaNVH
	String t406StaLainnya
	String t406KlasifikasiLainnya
	Integer t406BlowerSpeed
	Integer t406TempSetting
	String t406StaReci1
	String t406StaReci2
	String t406StaReci3
	String t406StaReci4
	String t406StaReci5
	String t406StaReci6
	String t406StaPerluDTR
	String t406StaTidaKPerluDTR
	String t406StaKonfirmasiSelalu
	String t406StaKonfirmasiKadang
	String t406StaKonfirmasiTidakMuncul
	String t406StaKonfirmasiLainnya
	String t406KonfirmasiLainnya
	String t406KeteranganKonfirmasi
	String t406StaCustomer
	String t406StaPerluTS
	String t406StaTidakPerluTS
	String t406DiagnosaAwal
	Date t406JamMulai
	Date t406JamSelesai
	String t406PICDiagnosis
	String t406DetailPekerjaan
	String t406KonfirmasiAkhir
	Date t406TglJamMulai
	Date t406TglJamSelesai
	String t406Foreman
	String t406Teknisi
	String t406StaOK
	String t406StaNG
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:true, maxSize:20, unique:true
		t406ID blank:false, nullable:true, maxSize:4, unique:true
		t406T305_M302_ID blank:true, nullable:true, maxSize:4
		t406KeluhanCust blank:true, nullable:true, maxSize:250
		t406StaAppRcp blank:true, nullable:true, maxSize:1
		t406StaButuhDiagnose blank:true, nullable:true, maxSize:1
		t406StaSudahDiagnoseApp blank:true, nullable:true, maxSize:1
		t406StaSudahDiagnoseRcp blank:true, nullable:true, maxSize:1
		appointment blank:true, nullable:true, maxSize:4
		t406Km blank:true, nullable:true, maxSize:4
		t406TglDiagnosis blank:true, nullable:true, maxSize:4
		t406StaGejalaHariIni blank:false, nullable:true, maxSize:1
		t406StaGejalaMingguLalu blank:false, nullable:true, maxSize:1
		t406StaGejalaLainnya blank:true, nullable:true, maxSize:1
		t406GejalaLainnya blank:true, nullable:true, maxSize:50
		t406StaFrekSekali blank:false, nullable:true, maxSize:1
		t406StaFrekKadang blank:false, nullable:true, maxSize:1
		t406StaFrekSelalu blank:false, nullable:true, maxSize:1
		t406StaFrekLainnya blank:false, nullable:true, maxSize:1
		t406FrekLainnya blank:false, nullable:true, maxSize:50
		t406StaMILOn blank:false, nullable:true, maxSize:1
		t406StaMILKedip blank:false, nullable:true, maxSize:1
		t406StaMILOff blank:false, nullable:true, maxSize:1
		t406MILLainnya blank:false, nullable:true, maxSize:50
		t406StaMILIdling blank:false, nullable:true, maxSize:1
		t406StaMILStarting blank:false, nullable:true, maxSize:1
		t406StaMILKonstan blank:false, nullable:true, maxSize:1
		t406MILKecepatanLainnya blank:false, nullable:true, maxSize:50
		t406StaMesinPanas blank:false, nullable:true, maxSize:1
		t406StaMesinDingin blank:false, nullable:true, maxSize:1
		t406StaPanasMesinLainnya blank:false, nullable:true, maxSize:1
		t406PanasMesinLainnya blank:true, nullable:true, maxSize:50
		t406Suhu blank:true, nullable:true, maxSize:50
		t406StaGigi1 blank:false, nullable:true, maxSize:1
		t406StaGigi2 blank:false, nullable:true, maxSize:1
		t406StaGigi3 blank:false, nullable:true, maxSize:1
		t406StaGigi4 blank:false, nullable:true, maxSize:1
		t406StaGigi5 blank:false, nullable:true, maxSize:1
		t406StaGigiP blank:false, nullable:true, maxSize:1
		t406StaGigiR blank:false, nullable:true, maxSize:1
		t406StaGigiN blank:false, nullable:true, maxSize:1
		t406StaGigiD blank:false, nullable:true, maxSize:1
		t406StaGigiS blank:false, nullable:true, maxSize:1
		t406StaGigiLainnya blank:false, nullable:true, maxSize:1
		t406GigiLainnya blank:false, nullable:true, maxSize:50
		t406Kecepatan blank:true, nullable:true, maxSize:4
		t406RPM blank:true, nullable:true, maxSize:4
		t406Beban blank:true, nullable:true, maxSize:4
		t406JmlPenumpang blank:true, nullable:true, maxSize:4
		t406StaDalamKota blank:false, nullable:true, maxSize:1
		t406StaJalanLurus blank:false, nullable:true, maxSize:1
		t406StaAkselerasi blank:false, nullable:true, maxSize:1
		t406StaLuarKota blank:false, nullable:true, maxSize:1
		t406StaDatar blank:false, nullable:true, maxSize:1
		t406StaRem blank:false, nullable:true, maxSize:1
		t406StaTol blank:false, nullable:true, maxSize:1
		t406StaTanjakan blank:false, nullable:true, maxSize:1
		t406StaBelok blank:false, nullable:true, maxSize:1
		t406StaTurunan blank:false, nullable:true, maxSize:1
		t406StaKondisiLainnya blank:false, nullable:true, maxSize:1
		t406KondisiLainnya blank:true, nullable:true, maxSize:50
		t406StaMacet blank:false, nullable:true, maxSize:1
		t406StaLancar blank:false, nullable:true, maxSize:1
		t406StaLalinLainnya blank:false, nullable:true, maxSize:1
		t406LalinLainnya blank:true, nullable:true, maxSize:50
		t406StaCerah blank:false, nullable:true, maxSize:1
		t406StaBerawan blank:false, nullable:true, maxSize:1
		t406StaHujan blank:false, nullable:true, maxSize:1
		t406StaPanas blank:false, nullable:true, maxSize:1
		t406StaLembab blank:false, nullable:true, maxSize:1
		t406Temperatur blank:true, nullable:true, maxSize:4
		t406StaEG blank:false, nullable:true, maxSize:1
		t406StaSuspensi blank:false, nullable:true, maxSize:1
		t406StaKlasifikasiRem blank:false, nullable:true, maxSize:1
		t406StaNVH blank:false, nullable:true, maxSize:1
		t406StaLainnya blank:false, nullable:true, maxSize:1
		t406KlasifikasiLainnya blank:true, nullable:true, maxSize:50
		t406BlowerSpeed blank:true, nullable:true, maxSize:4
		t406TempSetting blank:true, nullable:true, maxSize:4
		t406StaReci1 blank:false, nullable:true, maxSize:1
		t406StaReci2 blank:false, nullable:true, maxSize:1
		t406StaReci3 blank:false, nullable:true, maxSize:1
		t406StaReci4 blank:false, nullable:true, maxSize:1
		t406StaReci5 blank:false, nullable:true, maxSize:1
		t406StaReci6 blank:false, nullable:true, maxSize:1
		t406StaPerluDTR blank:false, nullable:true, maxSize:1
		t406StaTidaKPerluDTR blank:false, nullable:true, maxSize:1
		t406StaKonfirmasiSelalu blank:false, nullable:true, maxSize:1
		t406StaKonfirmasiKadang blank:false, nullable:true, maxSize:1
		t406StaKonfirmasiTidakMuncul blank:false, nullable:true, maxSize:1
		t406StaKonfirmasiLainnya blank:false, nullable:true, maxSize:1
		t406KonfirmasiLainnya blank:false, nullable:true, maxSize:50
		t406KeteranganKonfirmasi blank:false, nullable:true, maxSize:50
		t406StaCustomer blank:false, nullable:true, maxSize:1
		t406StaPerluTS blank:false, nullable:true, maxSize:1
		t406StaTidakPerluTS blank:false, nullable:true, maxSize:1
		t406DiagnosaAwal blank:false, nullable:true, maxSize:16
		t406JamMulai blank:false, nullable:true, maxSize:4
		t406JamSelesai blank:false, nullable:true, maxSize:4
		t406PICDiagnosis blank:false, nullable:true, maxSize:50
		t406DetailPekerjaan blank:true, nullable:true, maxSize:50
		t406KonfirmasiAkhir blank:true, nullable:true, maxSize:50
		t406TglJamMulai blank:false, nullable:true, maxSize:4
		t406TglJamSelesai blank:false, nullable:true, maxSize:4
		t406Foreman blank:false, nullable:true, maxSize:50
		t406Teknisi blank:false, nullable:true, maxSize:50
		t406StaOK blank:false, nullable:true, maxSize:1
		t406StaNG blank:false, nullable:true, maxSize:1
		staDel blank:false, nullable:true, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T406_PREDIAGNOSIS'
		reception column:'T406_T401_NoWO'
		t406ID column:'T406_ID', sqlType:'int'
		t406T305_M302_ID column:'T406_T305_M302_ID', sqlType:'int'
		t406KeluhanCust column:'T406_KeluhanCust', sqlType:'varchar(250)'
		t406StaAppRcp column:'T406_StaAppRcp', sqlType:'char(1)'
		t406StaButuhDiagnose column:'T406_StaButuhDiagnose', sqlType:'char(1)'
		t406StaSudahDiagnoseApp column:'T406_StaSudahDiagnoseApp', sqlType:'char(1)'
		t406StaSudahDiagnoseRcp column:'T406_StaSudahDiagnoseRcp', sqlType:'char(1)'
		appointment column:'T406_T301_ID'
		t406Km column:'T406_Km', sqlType:'int'
		t406TglDiagnosis column:'T406_TglDiagnosis'//, sqlType: 'date'
		t406StaGejalaHariIni column:'T406_StaGejalaHariIni', sqlType:'char(1)'
		t406StaGejalaMingguLalu column:'T406_StaGejalaMingguLalu', sqlType:'char(1)'
		t406StaGejalaLainnya column:'T406_StaGejalaLainnya', sqlType:'char(1)'
		t406GejalaLainnya column:'T406_GejalaLainnya', sqlType:'varchar(50)'
		t406StaFrekSekali column:'T406_StaFrekSekali', sqlType:'char(1)'
		t406StaFrekKadang column:'T406_StaFrekKadang', sqlType:'char(1)'
		t406StaFrekSelalu column:'T406_StaFrekSelalu', sqlType:'char(1)'
		t406StaFrekLainnya column:'T406_StaFrekLainnya', sqlType:'char(1)'
		t406FrekLainnya column:'T406_FrekLainnya', sqlType:'varchar(50)'
		t406StaMILOn column:'T406_StaMILOn', sqlType:'char(1)'
		t406StaMILKedip column:'T406_StaMILKedip', sqlType:'char(1)'
		t406StaMILOff column:'T406_StaMILOff', sqlType:'char(1)'
		t406MILLainnya column:'T406_MILLainnya', sqlType:'varchar(50)'
		t406StaMILIdling column:'T406_StaMILIdling', sqlType:'char(1)'
		t406StaMILStarting column:'T406_StaMILStarting', sqlType:'char(1)'
		t406StaMILKonstan column:'T406_StaMILKonStan', sqlType:'char(1)'
		t406MILKecepatanLainnya column:'T406_MILKecepatanLainnya', sqlType:'varchar(50)'
		t406StaMesinPanas column:'T406_StaMesinPanas', sqlType:'char(1)'
		t406StaMesinDingin column:'T406_StaMesinDingin', sqlType:'char(1)'
		t406StaPanasMesinLainnya column:'T406_StaPanasMersinLainnya', sqlType:'char(1)'
		t406PanasMesinLainnya column:'T406_PanasMesinLainnya', sqlType:'varchar(50)'
		t406Suhu column:'T406_Suhu', sqlType:'varchar(50)'
		t406StaGigi1 column:'T406_StaGigi1', sqlType:'char(1)'
		t406StaGigi2 column:'T406_StaGigi2', sqlType:'char(1)'
		t406StaGigi3 column:'T406_StaGigi3', sqlType:'char(1)'
		t406StaGigi4 column:'T406_StaGigi4', sqlType:'char(1)'
		t406StaGigi5 column:'T406_StaGigi5', sqlType:'char(1)'
		t406StaGigiP column:'T406_StaGigiP', sqlType:'char(1)'
		t406StaGigiR column:'T406_StaGigiR', sqlType:'char(1)'
		t406StaGigiN column:'T406_StaGigiN', sqlType:'char(1)'
		t406StaGigiD column:'T406_StaGigiD', sqlType:'char(1)'
		t406StaGigiS column:'T406_StaGigiS', sqlType:'char(1)'
		t406StaGigiLainnya column:'T406_StaGigiLainnya', sqlType:'char(1)'
		t406GigiLainnya column:'T406_GigiLainnya', sqlType:'varchar(50)'
		t406Kecepatan column:'T406_Kecepatan', sqlType:'int'
		t406RPM column:'T406_RPM', sqlType:'int'
		t406Beban column:'T406_Beban', sqlType:'int'
		t406JmlPenumpang column:'T406_JmlPenumpang', sqlType:'int'
		t406StaDalamKota column:'T406_StaDalamKota', sqlType:'char(1)'
		t406StaJalanLurus column:'T406_StaJalanLurus', sqlType:'char(1)'
		t406StaAkselerasi column:'T406_StaAkselerasi', sqlType:'char(1)'
		t406StaLuarKota column:'T406_StaLuarKota', sqlType:'char(1)'
		t406StaDatar column:'T406_StaDatar', sqlType:'char(1)'
		t406StaRem column:'T406_StaRem', sqlType:'char(1)'
		t406StaTol column:'T406_StaTol', sqlType:'char(1)'
		t406StaTanjakan column:'T406_StaTanjakan', sqlType:'char(1)'
		t406StaBelok column:'T406_StaBelok', sqlType:'char(1)'
		t406StaTurunan column:'T406_StaTurunan', sqlType:'char(1)'
		t406StaKondisiLainnya column:'T406_StaKondisiLainnya', sqlType:'char(1)'
		t406KondisiLainnya column:'T406_KondisiLainnya', sqlType:'varchar(50)'
		t406StaMacet column:'T406_StaMacet', sqlType:'char(1)'
		t406StaLancar column:'T406_StaLancar', sqlType:'char(1)'
		t406StaLalinLainnya column:'T406_StaLalinLainnya', sqlType:'char(1)'
		t406LalinLainnya column:'T406_LalinLainnya', sqlType:'varchar(50)'
		t406StaCerah column:'T406_StaCerah', sqlType:'char(1)'
		t406StaBerawan column:'T406_StaBerawan', sqlType:'char(1)'
		t406StaHujan column:'T406_StaHujan', sqlType:'char(1)'
		t406StaPanas column:'T406_StaPanas', sqlType:'char(1)'
		t406StaLembab column:'T406_StaLembab', sqlType:'char(1)'
		t406Temperatur column:'T406_Temperatur', sqlType:'int'
		t406StaEG column:'T406_StaEG', sqlType:'char(1)'
		t406StaSuspensi column:'T406_StaSuspensi', sqlType:'char(1)'
		t406StaKlasifikasiRem column:'T406_StaKlasifikasiRem', sqlType:'char(1)'
		t406StaNVH column:'T406_StaNVH', sqlType:'char(1)'
		t406StaLainnya column:'T406_StaLainnya', sqlType:'char(1)'
		t406KlasifikasiLainnya column:'T406_KlasifikasiLainnya', sqlType:'varchar(50)'
		t406BlowerSpeed column:'T406_BlowerSpeed', sqlType:'int'
		t406TempSetting column:'T406_TempSetting', sqlType:'int'
		t406StaReci1 column:'T406_StaReci1', sqlType:'char(1)'
		t406StaReci2 column:'T406_StaReci2', sqlType:'char(1)'
		t406StaReci3 column:'T406_StaReci3', sqlType:'char(1)'
		t406StaReci4 column:'T406_StaReci4', sqlType:'char(1)'
		t406StaReci5 column:'T406_StaReci5', sqlType:'char(1)'
		t406StaReci6 column:'T406_StaReci6', sqlType:'char(1)'
		t406StaPerluDTR column:'T406_StaPerluDTR', sqlType:'char(1)'
		t406StaTidaKPerluDTR column:'T406_StaTidaKPerluDTR', sqlType:'char(1)'
		t406StaKonfirmasiSelalu column:'T406_StaKonfirmasiSelalu', sqlType:'char(1)'
		t406StaKonfirmasiKadang column:'T406_StaKonfirmasiKadang', sqlType:'char(1)'
		t406StaKonfirmasiTidakMuncul column:'T406_StaKonfirmasiTidakMuncul', sqlType:'char(1)'
		t406StaKonfirmasiLainnya column:'T406_StaKonfirmasiLainnya', sqlType:'char(1)'
		t406KonfirmasiLainnya column:'T406_KonfirmasiLainnya', sqlType:'varchar(50)'
		t406KeteranganKonfirmasi column:'T406_KeteranganKonfirmasi', sqlType:'varchar(50)'
		t406StaCustomer column:'T406_StaCustomer', sqlType:'char(1)'
		t406StaPerluTS column:'T406_StaPerluTS', sqlType:'char(1)'
		t406StaTidakPerluTS column:'T406_StaTidakPerluTS', sqlType:'char(1)'
		t406DiagnosaAwal column:'T406_DiagnosaAwal'//, sqlType:'text'
		t406JamMulai column:'T406_JamMulai'//, sqlType: 'date'
		t406JamSelesai column:'T406_JamSelesai'//, sqlType: 'date'
		t406PICDiagnosis column:'T406_PICDiagnosis', sqlType:'varchar(50)'
		t406DetailPekerjaan column:'T406_DetailPekerjaan', sqlType:'varchar(50)'
		t406KonfirmasiAkhir column:'T406_KonfirmasiAkhir', sqlType:'varchar(50)'
		t406TglJamMulai column:'T406_TglJamMulai'//, sqlType: 'date'
		t406TglJamSelesai column:'T406_TglJamSelesai'//, sqlType: 'date'
		t406Foreman column:'T406_Foreman', sqlType:'varchar(50)'
		t406Teknisi column:'T406_Teknisi', sqlType:'varchar(50)'
		t406StaOK column:'T406_StaOK', sqlType:'char(1)'
		t406StaNG column:'T406_StaNG', sqlType:'char(1)'
		staDel column:'T406_StaDel', sqlType:'char(1)'
	}
}
