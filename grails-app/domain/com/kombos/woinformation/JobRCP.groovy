package com.kombos.woinformation

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.maintable.StatusWarranty
import com.kombos.parts.Vendor
import com.kombos.reception.Reception

class JobRCP {
    CompanyDealer companyDealer
	Reception reception
	Operation operation
	CompanyDealer companyDealerIDSumber
	CompanyDealer companyDealerIDTujuan
	StatusWarranty statusWarranty
	String t402StaTambahKurang
	String t402StaApproveTambahKurang
	Date t402TglTambahKurang
	String t402StaIntExt 
	String t402StaWarranty 
	String t402StaCashInsurance 
	String t402StaWarrantyByJOC 
	String t402StaPekerjaanTambahanCust // value 1 untuk ada job tambahan 0 untuk tidak
	String t402FA
	Double t402Rate 
	String t402AlasanUbahRate 
	Double t402HargaRp 
	Double t402DiscRp 
	Double t402DiscPersen
	Double t402TotalRp
	String t402StaOkCancel
	Date t402TglOkCancel
	String t402AlasanCancel
	String t402NoKeluhan
	Vendor vendor
	Double t402HargaBeliSublet
	String t402StaApprovalSublet
	String t402AlasanUnAproved 
	String t402JmlSublet
	String t402SatuanSublet
	String t402KeteranganSublet 
	String t402xKet 
	String t402xNamaUser 
	String t402xDivisi 
	String t402billTo
	String t402idBillTo
	String t402namaBillTo
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		reception blank:true, nullable:true
		operation blank:true, nullable:true
		companyDealerIDSumber blank:true, nullable: true
		companyDealerIDTujuan blank:true, nullable: true
		statusWarranty blank:true, nullable: true
		t402StaTambahKurang blank:true, nullable:true, maxSize:4
		t402StaApproveTambahKurang blank:true, nullable:true, maxSize:1
		t402TglTambahKurang blank:true, nullable:true
		t402StaIntExt blank:true, nullable:true, maxSize:1
		t402StaWarranty blank:true, nullable:true, maxSize:1
		t402StaCashInsurance blank:true, nullable:true, maxSize:1
		t402StaWarrantyByJOC blank:true, nullable:true, maxSize:1
		t402StaPekerjaanTambahanCust blank:true, nullable:true, maxSize:1
		t402FA blank:true, nullable:true, maxSize:50
		t402Rate blank:true, nullable:true
		t402AlasanUbahRate blank:true, nullable:true, maxSize:50
		t402HargaRp blank:true, nullable:true
		t402DiscRp blank:true, nullable:true
		t402DiscPersen blank:true, nullable:true
		t402TotalRp blank:true, nullable:true
		t402StaOkCancel blank:true, nullable:true, maxSize:1
		t402TglOkCancel blank:true, nullable:true
		t402AlasanCancel blank:true, nullable:true, maxSize:250
		t402NoKeluhan blank:true, nullable:true, maxSize:50
		vendor blank:true, nullable:true
		t402HargaBeliSublet blank:true, nullable:true
		t402StaApprovalSublet blank:true, nullable:true, maxSize:1
		t402AlasanUnAproved blank:true, nullable:true, maxSize:50
		t402JmlSublet blank:true, nullable:true, maxSize:50
		t402SatuanSublet blank:true, nullable:true, maxSize:50
		t402KeteranganSublet blank:true, nullable:true, maxSize:250
		t402xKet blank:true, nullable:true, maxSize:50
		t402xNamaUser blank:true, nullable:true, maxSize:20
		t402xDivisi blank:true, nullable:true, maxSize:20
		t402billTo blank:true, nullable:true, maxSize:30
		t402idBillTo blank:true, nullable:true, maxSize:10
		t402namaBillTo blank:true, nullable:true, maxSize:50
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T402_JOBRCP'
		reception column:'T402_T401_NoWO'
		operation column:'T402_M053_JobID'
		companyDealerIDSumber column:'T402_M011_IDSumber'
		companyDealerIDTujuan column:'T402_M011_IDTujuan'
		statusWarranty column:'T402_M058_ID'
		t402StaTambahKurang column:'T402_StaTambahKurang', sqlType:'char(1)'
		t402StaApproveTambahKurang column:'T402_StaApproveTambahKurang', sqlType:'char(1)'
		t402TglTambahKurang  column:'TglTambahKurang'//, sqlType: 'date'
		t402StaIntExt  column:'T402_StaIntExt', sqlType:'char(1)'
		t402StaWarranty  column:'T402_StaWarranty', sqlType:'char(1)'
		t402StaCashInsurance  column:'T402_StaCashInsurance', sqlType:'char(1)'
		t402StaWarrantyByJOC  column:'T402_staWarrantyByJOC', sqlType:'char(1)'
		t402StaPekerjaanTambahanCust  column:'T402_staPekerjaanTambahanCust', sqlType:'char(1)'
		t402FA  column:'T402_FA', sqlType:'char(1)'
		t402Rate  column:'T402_Rate'//, sqlType:'double'
		t402AlasanUbahRate  column:'T402_AlasanUbahRate', sqlType:'varchar(50)'
		t402HargaRp  column:'T402_HargaRp'//, sqlType:'double'
		t402DiscRp  column:'T402_DiscRp'//, sqlType:'double'
		t402DiscPersen column:'T402_DiscPersen'//, sqlType:'double'
		t402TotalRp column:'T402_TotalRp'//, sqlType:'double'
		t402StaOkCancel column:'T402_staOkCancel', sqlType:'char(1)'
		t402TglOkCancel column:'T402_TglOkCancel'//, sqlType: 'date'
		t402AlasanCancel column:'T402_AlasanCancel', sqlType:'varchar(250)'
		t402NoKeluhan column:'T402_NoKeluhan', sqlType:'varchar(50)'
		vendor column:'T402_M121_ID'
		t402HargaBeliSublet column:'T402_HargaBeliSublet'//, sqlType:'double'
		t402StaApprovalSublet column:'T402_StaApprovalSublet', sqlType:'char(1)'
		t402AlasanUnAproved  column:'T402_AlasanUnAproved', sqlType:'varchar(50)'
		t402JmlSublet column:'T402_JmlSublet', sqlType:'varchar(50)'
		t402SatuanSublet column:'T402_SatuanSublet', sqlType:'varchar(50)'
		t402KeteranganSublet  column:'T402_KeteranganSublet', sqlType:'varchar(250)'
		t402xKet  column:'T402_xKet', sqlType:'varchar(50)'
		t402xNamaUser  column:'T402_xNamaUser', sqlType:'varchar(20)'
		t402xDivisi  column:'T402_xDivisi', sqlType:'varchar(20)'
		t402billTo  column:'T402_BillTo', sqlType:'varchar(30)'
		t402idBillTo  column:'T402_IdBillTo', sqlType:'varchar(10)'
		t402namaBillTo  column:'T402_NamaBillTo', sqlType:'varchar(50)'
		staDel  column:'T402_StaDel', sqlType:'char(1)'
	}

    def beforeUpdate() {
        lastUpdated = new Date();
        lastUpdProcess = "UPDATE"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "INSERT"
//        lastUpdated = new Date()
        staDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    String toString(){
        return operation?.m053NamaOperation
    }
}