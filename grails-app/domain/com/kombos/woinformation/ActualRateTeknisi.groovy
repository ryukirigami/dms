package com.kombos.woinformation

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Operation
import com.kombos.reception.Reception

class ActualRateTeknisi {
	CompanyDealer companyDealer
	NamaManPower namaManPower
	Reception reception
	Operation operation
	Double t453ActualRate
	Double t453RevisiRate
	Date t453TglRevisi
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false, unique:true, maxSize:4
		namaManPower blank:false, nullable:false, unique:true, maxSize:10
		reception blank:false, nullable:false, unique:true, maxSize:20
		operation blank:false, nullable:false, unique:true, maxSize:7
		t453ActualRate blank:false, nullable:false
		t453RevisiRate blank:false, nullable:false
		t453TglRevisi blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T453_ACTUALRATETEKNISI'
		companyDealer column:'T453_M011_ID'
		namaManPower column:'T453_T015_IDManPower'
		reception column:'T453_T401_NoWO'
		operation column:'T453_M053_JobID'
		t453ActualRate column:'T453_ActualRate'
		t453RevisiRate column:'T453_RevisiRate'
		t453TglRevisi column:'T453_TglRevisi'
	}
}
