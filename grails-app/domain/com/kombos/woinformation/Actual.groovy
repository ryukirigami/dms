package com.kombos.woinformation

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.NamaProsesBP
import com.kombos.administrasi.Operation
import com.kombos.administrasi.Stall
import com.kombos.board.JPB
import com.kombos.maintable.StatusActual
import com.kombos.reception.Reception

class Actual {
	CompanyDealer companyDealer
	JPB jpb
	String t452ID
	NamaManPower namaManPower
	Stall stall
	Reception reception
	Operation operation
	StatusActual statusActual
	NamaProsesBP namaProsesBP
	Date t452TglJam
	String t452Ket
	String t452staProblemTeknis
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:false, nullable:false
        jpb blank:false, nullable:false
		t452ID blank:false, nullable:false, unique:false, maxSize:14
		namaManPower blank:false, nullable:false
		stall blank:false, nullable:false
		reception blank:false, nullable:false
		operation blank:false, nullable:false
		statusActual blank:false, nullable:false
		t452TglJam blank:false, nullable:false
        namaProsesBP blank:true, nullable:true
		t452Ket blank:true, nullable:true, maxSize:250
		t452staProblemTeknis blank:false, nullable:false, maxSize:1
		staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	
	static mapping = {
        autoTimestamp false
        companyDealer column:'T452_M011_ID'
        jpb column:'T452_T451_IDJPB'
		t452ID column:'T452_ID', sqlType:'char(14)'
		namaManPower column:'T452_T015_IDManPower'
		stall column:'T452_M022_StallID'
		reception column:'T452_T401_NoWO'
		operation column:'T452_M053_JobID'
		statusActual column:'T452_M452_ID'
		t452TglJam column:'T452_TglJam'//, sqlType: 'date'
		t452Ket column:'T452_Ket', sqlType:'varchar(250)'
		t452staProblemTeknis column:'T452_staProblemTeknis', sqlType:'char(1)'
		staDel column:'T452_StaDel', sqlType:'char(1)'
		table 'T452_ACTUAL'
	}
}
