package com.kombos.woinformation

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.StatusKendaraan
import com.kombos.reception.Reception

class Progress {
    CompanyDealer companyDealer
    Reception reception
    String t999ID
    Date t999TglJamStatus
    StatusKendaraan statusKendaraan
    String t999XKet
    String t999XNamaUser
    String t999XDivisi
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        companyDealer (blank:true, nullable:true)
        reception (nullable : false, blank : false)
        t999ID (nullable : true, blank : true)
        t999TglJamStatus (nullable : false, blank : false)
        staDel (nullable : false, blank : false)
        statusKendaraan (nullable : true, blank : true)
        t999XKet (nullable : true, blank : true)
        t999XNamaUser (nullable : true, blank : true)
        t999XDivisi (nullable : true, blank : true)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping={
        autoTimestamp false
        t999ID column: "T999_ID", sqlType: "varchar(14)"
        t999TglJamStatus column: "T999_TglJamStatus"//, sqlType: 'date'
        t999XKet column: "T999_xKet", sqlType: "varchar(50)"
        t999XNamaUser column: "T999_xNamaUser", sqlType: "varchar(20)"
        t999XDivisi column: "T999_xDivisi", sqlType: "varchar(20)"
        staDel column: "T999_StaDel", sqlType: "char(1)"
        table "T999_PROGRESS"
    }

}
