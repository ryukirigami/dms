package com.kombos.woinformation

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.reception.JobInv
import com.kombos.parts.Goods
import com.kombos.reception.Reception

class PartsRCP {
    CompanyDealer companyDealer
    Reception reception
    Goods goods
    Operation operation
    JobInv jobInv
    String t403StaTambahKurang
    String t403StaApproveTambahKurang
    Date t403TglJamTambahKurang
    String t403StaIntExt
    String t403StaWarranty
    String t403StaWarrantyByJOC
    Double t403Jumlah1
    Double t403Jumlah2
    Double t403HargaRp
    Double t403DiscRp
    Double t403DiscPersen
    Double t403TotalRp
    Double t403DPRp
    Double t403SisaRp
    String t403StaOkCancel
    Date t403TglCancel
    String t403AlasanCancel
    Date t403JanjiTglJamBayarDP
    Date t403TglJamBayarDP
    String t403StaRequestPart
    String t403xKet
    String t403xNamaUser
    String t403xDivisi
    String t403billTo
    String t403idBillTo
    String t403namaBillTo
    String isPending
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
        reception blank:true, nullable:true
        goods blank:true, nullable:true
        operation blank:true, nullable:true
        jobInv blank:true, nullable:true
        t403StaTambahKurang blank:true, nullable:true, maxSize:1
        t403StaApproveTambahKurang blank:true, nullable:true, maxSize:1
        t403TglJamTambahKurang blank:true, nullable:true, maxSize:4
        t403StaIntExt blank:true, nullable:true, maxSize:1
        t403StaWarranty blank:true, nullable:true, maxSize:1
        t403StaWarrantyByJOC blank:true, nullable:true, maxSize:1
        t403Jumlah1 blank:true, nullable:true
        t403Jumlah2 blank:true, nullable:true
        t403HargaRp blank:true, nullable:true
        t403DiscRp blank:true, nullable:true
        t403DiscPersen blank:true, nullable:true
        t403TotalRp blank:true, nullable:true
        t403DPRp blank:true, nullable:true
        t403SisaRp blank:true, nullable:true
        t403StaOkCancel blank:true, nullable:true, maxSize:1
        t403TglCancel blank:true, nullable:true, maxSize:4
        t403AlasanCancel blank:true, nullable:true, maxSize:50
        t403JanjiTglJamBayarDP blank:true, nullable:true, maxSize:4
        t403TglJamBayarDP blank:true, nullable:true, maxSize:4
        t403StaRequestPart blank:true, nullable:true, maxSize:1
        t403xKet blank:true, nullable:true, maxSize:50
        t403xNamaUser blank:true, nullable:true, maxSize:20
        t403xDivisi blank:true, nullable:true, maxSize:20
        t403billTo blank:true, nullable:true, maxSize:30
        t403idBillTo blank:true, nullable:true, maxSize:10
        t403namaBillTo blank:true, nullable:true, maxSize:50
        isPending blank:true, nullable:true, maxSize:1
        staDel blank:false, nullable:false, maxSize:1
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
    static mapping = {
        autoTimestamp false
        table 'T403_PARTSRCP'
        reception column:'T403_T401_NoWO'
        goods column:'T403_M111_ID'
        operation column:'T403_M053_JobID'
        jobInv column:'T403_T702_ID'
        t403StaTambahKurang column:'T403_StaTambahKurang', sqlType:'char(1)'
        t403StaApproveTambahKurang column:'T403_StaApproveTambahKurang', sqlType:'char(1)'
        t403TglJamTambahKurang column: 'T403_TglJamTambahKurang'//, sqlType: 'date'
        t403StaIntExt column: 'T403_StaIntExt' , sqlType:'char(1)'
        t403StaWarranty column: 'T403_StaWarranty', sqlType:'char(1)'
        t403StaWarrantyByJOC column: 'T403_staWarrantyByJOC', sqlType:'char(1)'
        t403Jumlah1 column: 'T403_Jumlah1'
        t403Jumlah2 column: 'T403_Jumlah2'
        t403HargaRp column: 'T403_HargaRp'
        t403DiscRp column: 'T403_DiscRp'
        t403DiscPersen column: 'T403_DiscPersen'
        t403TotalRp column: 'T403_TotalR'
        t403DPRp column: 'T403_DPRp'
        t403SisaRp column: 'T403_SisaRp'
        t403StaOkCancel column: 'T403_StaOkCancel' , sqlType:'char(1)'
        t403TglCancel column: 'T403_TglCancel' //, sqlType: 'date'
        t403AlasanCancel column: 'T403_AlasanCancel' , sqlType:'varchar(50)'
        t403JanjiTglJamBayarDP column: 'T403_JanjiTglJamBayarDP' //, sqlType: 'date'
        t403TglJamBayarDP column: 'T403_TglJamBayarDP' //, sqlType: 'date'
        t403StaRequestPart column: 'T403_StaRequestPart' , sqlType:'char(1)'
        t403xKet column: 'T403_xKet' , sqlType:'varchar(50)'
        t403xNamaUser column: 'T403_xNamaUser' , sqlType:'varchar(20)'
        t403xDivisi column: 'T403_xDivisi' , sqlType:'varchar(20)'
        t403billTo column: 'T403_BillTo' , sqlType:'varchar(30)'
        t403namaBillTo column: 'T403_NamaBillTo' , sqlType:'varchar(50)'
        t403idBillTo column: 'T403_IdBillTo' , sqlType:'varchar(10)'
        staDel column: 'T403_StaDel', sqlType:'char(1)'
    }

}
