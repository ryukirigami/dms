package com.kombos.hrd

import com.kombos.administrasi.KabKota

class PendidikanKaryawan {

    KabKota kota
    Karyawan karyawan
    Pendidikan pendidikan
    String namaSekolah
    int tahunMasuk
    int tahunLulus
    String jurusan
    String statusLulus
    float nemIpk
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        jurusan nullable: true, blank: true
        kota nullable: false, blank: false
        karyawan nullable: false, blank: false
        pendidikan nullable: false, blank: false
        namaSekolah nullable: false, blank: false
        tahunMasuk nullable: false, blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TH025_PENDIDIKANKARYAWAN"
        id column: "TH025_ID"
        kota column: "TH025_M002_ID_KOTA"
        karyawan column: "TH025_MH009_ID_KARYAWAN"
        pendidikan column: "TH025_MH001_ID_PENDIDIKAN"
        namaSekolah column: "TH025_NAMASEKOLAH"
        tahunMasuk column: "TH025_TAHUNMASUK"
        tahunLulus column: "TH025_TAHUNLULUS"
        jurusan column: "TH025_JURUSAN"
        statusLulus column: "TH025_STATUSLULUS"
        nemIpk column: "TH025_NEMIPK"
        staDel column: "TH025_STADEL"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}

