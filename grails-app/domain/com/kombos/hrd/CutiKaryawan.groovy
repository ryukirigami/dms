package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer

class CutiKaryawan {

    int tahunCuti
    CompanyDealer companyDealer
    int jumlahHariCuti
    Date tanggalMulaiCuti
    Date tanggalAkhirCuti
    String keterangan
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    /*Karyawan karyawan
    Cuti cutiKaryawan*/

    static hasOne = [
            karyawan: Karyawan,
            cutiKaryawan: Cuti
    ]

    static constraints = {
        karyawan nullable: false, blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TH006_CUTIKARYAWAN"
        id column: "TH006_ID", sqlType: "int"
        karyawan column: "TH006_MH009_ID_KARYAWAN", sqlType: "int"
        jumlahHariCuti column: "TH006_JUMLAHHARICUTI", sqlType: "int"
        tanggalMulaiCuti column: "TH006_TANGGALMULAICUTI"
        tanggalAkhirCuti column: "TH006_TANGGALAKHIRCUTI"
        tahunCuti column: "TH006_TAHUNCUTI", sqlType: "int"
        cutiKaryawan column: "TH006_MH006_ID_CUTIKARYAWAN", sqlType: "int"
        keterangan column: "TH006_KETERANGAN", sqlType: "varchar(128)"
        staDel column: "TH006_STADEL", sqlType: "char(1)"
        companyDealer column: "ID_COMPANY", sqlType: "int"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}