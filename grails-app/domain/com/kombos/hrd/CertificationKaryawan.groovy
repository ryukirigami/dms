package com.kombos.hrd

class CertificationKaryawan {

    CertificationType tipeSertifikasi
    TrainingClassRoom training
    Date tglSertifikasi
    String keterangan
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasOne = [
            karyawan: Karyawan
    ]

    static constraints = {
        staDel nullable: false, blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TH023_SERTIFIKASIKARYAWAN"
        id column: "TH023_ID", sqlType: "int"
        tglSertifikasi column: "TH023_TGLSERTIFIKASI"
        karyawan column: "TH023_MH009_ID_KARYAWAN", sqlType: "int"
        tipeSertifikasi column: "TH023_MH021_ID_TIPESERTIFIKASI", sqlType: "int"
        training column: "TH023_TH021_ID_TRAINING", sqlType: "int"
        keterangan column: "TH023_KETERANGAN", sqlType: "varchar(64)"
        staDel column: "TH023_STADEL", sqlType: "char(1)"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}