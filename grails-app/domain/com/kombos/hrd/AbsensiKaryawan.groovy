package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer

class AbsensiKaryawan {
    CompanyDealer companyDealer
    Karyawan karyawan
    String bulanTahun
    int attend
    int absent
    int permit
    int leave
    int overtime
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasOne = [karyawan: Karyawan]

    static constraints = {
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        companyDealer nullable: true
        karyawan nullable: false, blank: false
    }


    static mapping = {
        autoTimestamp false
        table "TH004_ABSENSIKARYAWAN"
        id column: "TH004_ID", sqlType: "int"
        karyawan column: "TH004_MH009_ID_KARYAWAN", sqlType: "int"
        bulanTahun column: "TH004_BULANTAHUN", sqlType: "varchar(64)"
        attend column: "TH004_ATTEND", sqlType: "int"
        absent column: "TH004_ABSENT", sqlType: "int"
        permit column: "TH004_PERMIT", sqlType: "int"
        leave  column: "TH004_LEAVE", sqlType: "int"
        overtime column: "TH004_OVERTIME", sqlType: "int"
        staDel column: "TH004_STADEL", sqlType: "char(1)"
        companyDealer column: "ID_COMPANY", sqlType: "int"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}