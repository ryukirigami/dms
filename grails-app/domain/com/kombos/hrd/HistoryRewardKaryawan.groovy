package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.parts.StatusApproval

class HistoryRewardKaryawan {

    CompanyDealer companyDealer
    Date tanggalReward
    String keterangan
    StatusApproval staApproval
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasOne = [
            karyawan: Karyawan,
            rewardKaryawan: RewardKaryawan
    ]

    static constraints = {
        tanggalReward nullable: false, blank: false
        staApproval nullable: true, blank: true
        staDel nullable: false, blank: false
        companyDealer nullable: true, blank: true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete

    }

    static mapping = {
        autoTimestamp false
        table "TH001_HISTORYREWARDKARYAWAN"
        id column: "TH001_ID", sqlType: "int"
        tanggalReward column: "TH001_TANGGALREWARD"
        keterangan column: "TH001_KETERANGAN", sqlType: "varchar(128)"
        karyawan column: "TH001_MH009_ID_KARYAWAN", sqlType: "int"
        rewardKaryawan column: "TH001_MH004_ID_REWARDKARYAWAN", sqlType: "int"
        staDel column: "TH001_STADEL", sqlType: "char(1)"

    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        staApproval = StatusApproval.WAIT_FOR_APPROVAL

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}