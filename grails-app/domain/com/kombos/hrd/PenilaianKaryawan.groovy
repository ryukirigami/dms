package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.ManPower

class PenilaianKaryawan {

    CompanyDealer companyDealer
    Karyawan karyawan
    String periode
    Karyawan penilai
    ManPower jabatan
    int teknis
    int logika
    int kecepatanKetelitian
    int loyalitas
    int drive
    int disiplin
    String stadel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer nullable: true, blank: true
        karyawan nullable: false, blank: false
        periode nullable: false, blank: false
        penilai nullable: false, blank: false
        jabatan nullable: false, blank: false
        teknis nullable: true
        logika nullable: true
        kecepatanKetelitian nullable: true
        loyalitas nullable: true
        drive nullable: true
        disiplin nullable: true
        stadel nullable: false, blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static  mapping = {
        autoTimestamp false
        table "TH026_PENILAIANKARYAWAN"
        id column: "TH026_ID"
        karyawan column: "TH026_MH009_ID_KARYAWAN"
        periode column: "TH026_PERIODE"
        penilai column: "TH026_MH009_ID_PENILAI"
        jabatan column: "TH026_JABATANPENILAI"
        teknis column: "TH026_TEKNIS"
        logika column: "TH026_LOGIKA"
        kecepatanKetelitian column: "TH026_KECEPATANKETELITIAN"
        loyalitas column: "TH026_LOYALITAS"
        drive column: "TH026_DRIVE"
        disiplin column: "TH026_DISIPLIN"
        stadel column: "TH026_STADEL"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(stadel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        stadel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!stadel)
            stadel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }

}