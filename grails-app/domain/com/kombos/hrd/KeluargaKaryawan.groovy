package com.kombos.hrd

class KeluargaKaryawan {

    Pendidikan pendidikan
    Karyawan karyawan
    HubunganKeluarga hubungan
    String nama
    String tempatLahir
    Date tanggalLahirKel
    String jenisKelamin
    String pekerjaan
    String jabatan
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        hubungan nullable: false, blank: false
        nama nullable: false, blank: false
        karyawan nullable: false, blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TH024_KELUARGAKARYAWAN"
        id column: "TH024_ID"
        pendidikan column: "TH024_PENDIDIKAN"
        hubungan column: "TH024_HUBUNGAN"
        karyawan column: "TH024_MH009_ID_KARYAWAN"
        nama column: "TH024_NAMA"
        tempatLahir column: "TH024_TEMPATLAHIR"
        tanggalLahirKel column: "TH024_TANGGALLAHIR"
        jenisKelamin column: "TH024_JENISKELAMIN"
        pekerjaan column: "TH024_PEKERJAAN"
        jabatan column: "TH024_JABATAN"
        staDel column: "TH024_STADEL"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }

}