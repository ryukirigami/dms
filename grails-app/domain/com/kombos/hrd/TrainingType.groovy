package com.kombos.hrd

import com.kombos.administrasi.Divisi

class TrainingType {

    String tipeTraining
    String intEks
    Divisi bagian
    String singkatan
    String tempatTraining
    String durasi
    String sertifikasi
    String keterangan
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        tipeTraining nullable: true, blank: true
        intEks nullable: true, blank: true
        bagian nullable: true, blank: true
        singkatan nullable: true, blank: true
        tempatTraining nullable: true, blank: true
        durasi nullable: true, blank: true
        sertifikasi nullable: true, blank: true
        keterangan nullable: true, blank: true
        staDel nullable: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MH020_TRAININGTYPE"
        id column: "MH020_ID"
        tipeTraining column: "MH020_TIPETRAINING", sqlType: "varchar(32)"
        intEks column: "MH020_INTEKS", sqlType: "varchar(32)"
        bagian column: "MH020_M012_ID"
        singkatan column: "MH020_SINGKATAN", sqlType: "varchar(16)"
        tempatTraining column: "MH020_TEMPATTRAINING", sqlType: "varchar(64)"
        durasi column: "MH020_DURASI", sqlType: "varchar(16)"
        sertifikasi column: "MH020_SERTIFIKASI", sqlType: "char(1)"
        staDel column: "MH020_STADEL", sqlType: "char(1)"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}