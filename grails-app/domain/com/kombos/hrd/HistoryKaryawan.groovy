package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Divisi
import com.kombos.administrasi.ManPower
import com.kombos.parts.StatusApproval

class HistoryKaryawan {

    CompanyDealer companyDealer
    Date tanggal
    CompanyDealer cabangBefore
    CompanyDealer cabangAfter
    ManPower jabatanBefore
    ManPower jabatanAfter
    Divisi divisi
    String letterNumber
    String reason
    String explanation
    String staDel
    StatusApproval staApproval
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete
    /*Karyawan karyawan
    History history*/

    static hasOne = [
            karyawan: Karyawan,
            history: History
    ]

    static constraints = {
        karyawan nullable: false, blank: false
        companyDealer nullable: true, blank: true
        cabangBefore nullable: true, blank: true
        cabangAfter nullable: true, blank: true
        jabatanBefore nullable: true, blank: true
        jabatanAfter nullable: true, blank: true
        letterNumber nullable: true, blank: true
        divisi nullable: true, blank: true
        staApproval nullable: true, blank: true
        reason nullable: true, blank: true
        explanation nullable: true, blank: true
        staDel nullable: false, blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TH003_HISTORYKARYAWAN"
        id column: "TH003_ID", sqlType: "int"
        karyawan column: "TH003_MH009_ID_KARYAWAN", sqlType: "int"
        tanggal column: "TH003_TANGGAL"
        history column: "TH003_MH008_ID_HISTORY", sqlType: "int"
        cabangBefore column: "TH003_M011_ID_CABANGBEFORE"
        cabangAfter column: "TH003_M011_ID_CABANGAFTER"
        letterNumber column: "TH003_LETTERNUMBER", sqlType: "varchar(32)"
        reason column: "TH003_REASON", sqlType: "varchar(64)"
        explanation column: "TH003_EXPLANATION", sqlType: "varchar(128)"
        staDel column: "TH003_STADEL", sqlType: "char(1)"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
        staApproval = StatusApproval.WAIT_FOR_APPROVAL
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }

}