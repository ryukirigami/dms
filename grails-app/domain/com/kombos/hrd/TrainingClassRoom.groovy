package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer

class TrainingClassRoom {

    TrainingType trainingType
    CompanyDealer branch
    String namaTraining
    TrainingInstructor firstInstructor
    TrainingInstructor secondInstructor
    TrainingInstructor thirdInstructor
    Date dateBegin
    Date dateFinish
    int totalTrainingDay
    int pointMin
    int pointMax
    int pointAverage
    String description
    int batch
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    CertificationType certificationType

    static hasMany = [trainingMember: TrainingMember]

    static constraints = {
        branch nullable: true, blank: true
        secondInstructor nullable: true, blank: true
        thirdInstructor nullable: true, blank: true
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TH021_TRAININGCLASSROOM"
        id column: "TH021_ID", sqlType: "int"
        namaTraining column: "TH021_NAMATRAINING", sqlType: "varchar(32)"
        branch column: "TH021_M011_ID_BRANCH", sqlType: "int"
        trainingType column: "TH021_MH020_ID_TRAININGTYPE", sqlType: "int"
        firstInstructor column: "TH021_TH020_ID_INSTRUCTOR1", sqlType: "int"
        secondInstructor column: "TH021_TH020_ID_INSTRUCTOR2", sqlType: "int"
        thirdInstructor column: "TH021_TH020_ID_INSTRUCTOR3", sqlType: "int"
        dateBegin column: "TH021_DATEBEGIN"
        dateFinish column: "TH021_DATEFINISH"
        totalTrainingDay column: "TH021_TOTALTRAININGDAY", sqlType: "int"
        pointMin column: "TH021_POINTMIN", sqlType: "int"
        pointMax column: "TH021_POINTMAX", sqlType: "int"
        pointAverage column: "TH021_POINTAVERAGE", sqlType: "int"
        description column: "TH021_DESCRIPTION", sqlType: "varchar(128)"
        batch column: "TH021_BATCH", sqlType: "int"
        staDel column: "TH021_STADEL", sqlType: "char(1)"
        certificationType column: "TH021_MH021_ID_SERTIFIKASI", sqlType: "int"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if (staDel == "1") {
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if (!lastUpdProcess)
            lastUpdProcess = "insert"
        if (!lastUpdated)
            lastUpdated = new Date()
        if (!staDel)
            staDel = "0"
        if (!createdBy) {
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }


    @Override
    public String toString() {
        return namaTraining
    }
}