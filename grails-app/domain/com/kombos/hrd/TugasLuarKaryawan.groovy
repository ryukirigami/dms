package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.CompanyController

class TugasLuarKaryawan {
 CompanyDealer companyDealer
    String assignmentReason
    String assignmentObjectives
    Date dateBegin
    Date dateFinishSchedule
    Date dateFinish
    String transportation
    String explanation
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasOne = [karyawan: Karyawan]

    static constraints = {
        dateFinish nullable: true, blank: true
        explanation nullable: true, blank: true
        dateFinishSchedule nullable: false, blank: false
        karyawan nullable: false, blank: false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "TH005_TUGASLUARKARYAWAN"
        id column: "TH005_ID", sqlType: "int"
        karyawan column: "TH005_MH009_ID_KARYAWAN", sqlType: "int"
        assignmentReason column: "TH005_ASSIGNMENTREASON", sqlType: "varchar(64)"
        assignmentObjectives column: "TH005_ASSIGNMENTOBJECTIVES", sqlType: "varchar(64)"
        dateBegin column: "TH005_DATEBEGIN"
        dateFinishSchedule column: "TH005_DATEFINISHSCHEDULE"
        dateFinish column: "TH005_DATEFINISH"
        transportation column: "TH005_TRANSPORTATION", sqlType: "varchar(32)"
        explanation column: "TH005_EXPLANATION", sqlType: "varchar(64)"
        staDel column: "TH005_STADEL", sqlType: "char(1)"
        companyDealer column: "ID_COMPANY", sqlType: "int"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}