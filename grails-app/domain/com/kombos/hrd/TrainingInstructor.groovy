package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.ManPower

class TrainingInstructor {

    Karyawan karyawan
    TrainingClassRoom trainingClassRoom
    CertificationType certificationType
    String th020isAktif
    String th020isTam
    CompanyDealer companyDealer
    ManPower manPower
    String jenisInstruktur
    String namaInstruktur
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasOne = [karyawan: Karyawan]

    static constraints = {
        th020isAktif nullable: false
        th020isTam nullable: false
        trainingClassRoom nullable: true, false : true
        jenisInstruktur nullable: false, blank: false
        namaInstruktur nullable: false, blank: false
        karyawan nullable: true, blank: true // Boleh null karena bisa saja trainer dari pihak luar
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
        staDel nullable: false
    }

    static mapping = {
        autoTimestamp false
        table "TH020_TRAININGINSTRUCTOR"
        id column: "TH020_ID", sqlType: "int"
        trainingClassRoom column: "TH020_TH021_KELASTRAINING", sqlType: "int"
        manPower column: "TH020_M014_ID_JABATANMANPOWER", sqlType: "int"
        certificationType column: "TH020_MH021_ID_TIPESERTIFIKASI", sqlType: "int"
        companyDealer column: "TH020_M011_ID_COMPANYDEALER", sqlType: "int"
        karyawan column: "TH020_MH009_ID_KARYAWAN", sqlType: "int"
        staDel column: "TH020_STADEL", sqlType: "char(1)"
        namaInstruktur column: "TH020_NAMAINSTRUKTUR", sqlType: "varchar(64)"
        jenisInstruktur column: "TH020_JENISINSTRUKTUR", sqlType: "varchar(255)"
        th020isAktif column: "TH020_ISAKTIF", sqlType: "char(1)"
        th020isTam column: "TH020_ISTAM", sqlType: "char(1)"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}