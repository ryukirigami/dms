package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Divisi
import com.kombos.administrasi.KabKota
import com.kombos.administrasi.ManPower
import com.kombos.customerprofile.Agama
import com.kombos.customerprofile.Hobby
import com.kombos.finance.AccountNumber
import com.kombos.maintable.Nikah
import com.kombos.parts.StatusApproval

class Karyawan {

    String nomorPokokKaryawan
    String nama
    String alamat
    KabKota kota
    KabKota kotaLahir
    Date tanggalLahir
    Date tanggalMasuk
    String emailAddress
    String telepon
    String jamsostekIDNumber
    int jenisKelamin

    Divisi divisi
    Agama agama
    Nikah statusNikah
    StatusKaryawan statusKaryawan
    ManPower jabatan
    CompanyDealer branch
    AccountNumber accountNumber

    String staAvailable
    StatusApproval staApproval
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [
            pendidikan: Pendidikan,
            hobby: Hobby,
            historyKaryawan: HistoryKaryawan,
            historyRewardKaryawan: HistoryRewardKaryawan,
            historyWarningKaryawan: HistoryWarningKaryawan,
            absensiKaryawan: AbsensiKaryawan,
            cutiKaryawan: CutiKaryawan,
            trainingInstructor: TrainingInstructor,
            trainingMember: TrainingMember,
            tugasLuarKaryawan: TugasLuarKaryawan,
            sisaCutiKaryawan: SisaCutiKaryawan,
            sertifikasiKaryawan: CertificationKaryawan
    ]


    static constraints = {
        nomorPokokKaryawan nullable: true, blank: true
        nama nullable: false, blank: false
        staApproval nullable: true, blank: true
        tanggalLahir nullable: false, blank: false
        tanggalMasuk nullable: true, blank: true
        emailAddress nullable: false, blank: false
        jenisKelamin nullable: false, blank: false
        staAvailable nullable: true, blank: true
        kotaLahir nullable: true, blank: true
        jamsostekIDNumber nullable: true, blank: true
        accountNumber nullable: true, blank: true

        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MH009_KRYWAN"
        id column: "MH009_ID"
        nomorPokokKaryawan column: "MH009_NOMORPOKOKKARYAWAN", sqlType: "varchar(16)"
        alamat column: "MH009_ALAMAT", sqlType: "varchar(128)"
        kota column: "MH009_M002_ID_KOTA"//, sqlType: "varchar(2)"
        kotaLahir column: "MH009_M002_ID_KOTALAHIR"//, sqlType: "varchar(2)"
        tanggalLahir column: "MH009_TANGGALLAHIR"
        tanggalMasuk column: "MH009_TANGGALMASUK"
        emailAddress column: "MH009_EMAILADDRESS", sqlType: "varchar(64)"
        telepon column: "MH009_TELEPON", sqlType: "varchar(15)"
        pendidikan column: "ID_PENDIDIKAN", sqlType: "int"
        jenisKelamin column: "MH009_JENISKELAMIN", sqlType: "int"
        agama column: "MH009_M061_ID_AGAMA", sqlType: "int"
        statusNikah column: "M009_M062_ID_NIKAH", sqlType: "int"
        hobby column: "M009_M063_ID_HOBBY", sqlType: "int"
        statusKaryawan column: "ID_STATUSKARYAWAN", sqlType: "int"
        divisi column: "ID_DIVISI", sqlType: "int"
        jabatan column: "ID_JABATAN", sqlType: "int"
        jamsostekIDNumber column: "MH009_JAMSOSTEKIDNUMBER", sqlType: "varchar(32)"
        branch column: "ID_BRANCH", sqlType: "int"
        accountNumber column: "ACCOUNTNUMBER", sqlType: "int"
        staAvailable column: "MH009_STAAVAILABLE", sqlType: "char(1)"
        staDel column: "MH009_STADEL", sqlType: "char(1)"
    }


    @Override
    public String toString() {
        return nama
    }


    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        staAvailable = KaryawanStatus.AVAILABLE

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }
}