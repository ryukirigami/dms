package com.kombos.hrd

class History {

    String history
    String keterangan
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static hasMany = [historyKaryawan: HistoryKaryawan]

    static constraints = {
        history nullable: false, blank: false
        staDel nullable: false, blank: false
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MH008_HISTORY"
        id column: "MH008_ID"
        history column: "MH008_HISTORY", sqlType: "varchar(16)"
        keterangan column: "MH008_KETERANGAN", sqlType: "varchar(64)"
        staDel column: "MH008_STADEL", sqlType: "char(1)"
    }

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"

        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }

    @Override
    public String toString() {
        return history
    }
}