package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer

class WarningKaryawan {
    String warningKaryawan
    CompanyDealer companyDealer
    String keterangan
    String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete


    static constraints = {
        warningKaryawan nullable: false, blank: false, validator: {val, obj ->
            def similar = WarningKaryawan.findByWarningKaryawanIlike(val)
            return !similar || obj.id == similar.id
        }
        createdBy nullable: false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy nullable: true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess nullable: false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }

    static mapping = {
        autoTimestamp false
        table "MH005_WARNINGKARYAWAN"
        id column: "MH005_ID"
        warningKaryawan column: "MH005_WARNINGKARYAWAN", sqlType: "varchar(16)"
        keterangan column: "MH005_KETERANGAN", sqlType: "varchar(64)"
        staDel column: "MH005_STADEL", sqlType: "char(1)"
        companyDealer column: "ID_COMPANY", sqlType: "int"
    }

    static hasMany = [historyWarningKaryawan: HistoryWarningKaryawan]

    def beforeUpdate() {
        lastUpdated = new Date();

        if(staDel == "1"){
            lastUpdProcess = "delete"
        } else {
            lastUpdProcess = "update"
        }
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }
        } catch (Exception e) {
        }
    }

    def beforeInsert() {
//        dateCreated = new Date()
        lastUpdProcess = "insert"
//        lastUpdated = new Date()
        staDel = "0"
        try {
            if (org.apache.shiro.SecurityUtils.subject.principal.toString()) {
                createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                updatedBy = createdBy
            }
        } catch (Exception e) {
        }

    }

    def beforeValidate() {
        if(!lastUpdProcess)
            lastUpdProcess = "insert"
        if(!lastUpdated)
            lastUpdated = new Date()
        if(!staDel)
            staDel = "0"
        if(!createdBy){
            createdBy = "_SYSTEM_"
            updatedBy = createdBy
        }
    }


    @Override
    public String toString() {
        return warningKaryawan
    }
}