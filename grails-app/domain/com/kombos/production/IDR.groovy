package com.kombos.production

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.administrasi.Stall
//import com.kombos.administrasi.UserProfile
import com.kombos.baseapp.sec.shiro.User
import com.kombos.reception.Reception

class IDR {
    CompanyDealer companyDealer
	Reception reception
	Integer t506ID
	Stall stall
    Date t506TglJamMulai
	Date t506TglJamSelesai
	User userProfile
	String t506StaIDR
	String t506StaRedo
	Operation t506M053_JobID_Redo
	Operation t506RedoKeProses_M190_ID
	String t506AlasanIDR
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false
		t506ID blank:true, nullable:true
		stall blank:false, nullable:false
		t506TglJamMulai blank:false, nullable:false
		t506TglJamSelesai blank:false, nullable:false
		userProfile blank:true, nullable:true
		t506StaIDR blank:false, nullable:false, maxSize:1
		t506StaRedo blank:false, nullable:false, maxSize:1
		t506M053_JobID_Redo blank:true, nullable:true
		t506RedoKeProses_M190_ID blank:true, nullable:true
		t506AlasanIDR blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T506_IDR'
		reception column:'T506_T401_NoWO'
		t506ID column:'T506_ID', sqlType:'int'
		stall column:'T506_M022_StallID'
		t506TglJamMulai column:'T506_TglJamMulai'//, sqlType: 'date'
		t506TglJamSelesai column:'T506_TglJamSelesai'//, sqlType: 'date'
		userProfile column:'T506_T001_IDUser'
		t506StaIDR column:'T506_StaIDR', sqlType:'char(1)'
		t506StaRedo column:'T506_StaRedo', sqlType:'char(1)'
		t506M053_JobID_Redo column:'T506_M053_JobID_Redo'//, sqlType:'varchar(50)'
		t506RedoKeProses_M190_ID column:'T506_RedoKeProses_M190_ID'
		t506AlasanIDR column:'T506_AlasanIDR'//, sqlType:'text'
	}
}
