package com.kombos.production

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorCat
import com.kombos.board.JPB
import com.kombos.maintable.StatusKendaraan
import com.kombos.woinformation.Actual

class Masalah {
	CompanyDealer companyDealer
	JPB jpb
	Actual actual
	Integer t501ID
	String t501staProblemTeknis
	String t501CatatanTambahan
	Date t501TglUpdate
	Date t501TanggalKirim
	String t501NamaKirim
	String t501StaButuhPause
	String t501StaButuhRSA
	String t501Sta_TL_ATL_NTL
	String t501StaBisaKrjSdri
	String t501StaBthTambahWkt
	String t501DurasiTambahWaktu
	String t501StaBthJob
	String t501KetBthJob
	String t501StaBthParts
	String t501HasilDSS
	String t501StaSolved
	Date t501TglSolved
	String t501StaButuhCat
	String t501StaTagihKepada
	VendorCat vendorCat
	StatusKendaraan statusKendaraan
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer blank:true, nullable:true
        jpb blank:true, nullable:true
        actual blank:true, nullable:true
        t501ID blank:true, nullable:true
        t501staProblemTeknis blank:true, nullable:true, maxSize:1
        t501CatatanTambahan blank:true, nullable:true
        t501TglUpdate blank:true, nullable:true
        t501TanggalKirim blank:true, nullable:true
        t501NamaKirim blank:true, nullable:true, maxSize:50
        t501StaButuhPause blank:true, nullable:true, maxSize:1
        t501StaButuhRSA blank:true, nullable:true, maxSize:1
        t501Sta_TL_ATL_NTL blank:true, nullable:true, maxSize:1
        t501StaBisaKrjSdri blank:true, nullable:true, maxSize:1
        t501StaBthTambahWkt blank:true, nullable:true
        t501DurasiTambahWaktu blank:true, nullable:true
        t501StaBthJob blank:true, nullable:true, maxSize:1
        t501KetBthJob blank:true, nullable:true, maxSize:50
        t501StaBthParts blank:true, nullable:true
        t501HasilDSS blank:true, nullable:true, maxSize:1
        t501StaSolved blank:true, nullable:true
        t501TglSolved blank:true, nullable:true
        t501StaButuhCat blank:true, nullable:true, maxSize:1
        t501StaTagihKepada blank:true, nullable:true, maxSize:1
        vendorCat blank:true, nullable:true, maxSize:4
        statusKendaraan blank:true, nullable:true, maxSize:2
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
        dateCreated nullable : true, blank : true
        lastUpdated nullable : true, blank : true
    }
	
	static mapping = {
        autoTimestamp false
        table 'T501_MASALAH'
		companyDealer column:'T501_M011_ID'
		jpb column:'T501_T451_IDJPB'
		actual column:'T501_T452_ID'
		t501ID column:'T501_ID', sqlType:'int'
		t501staProblemTeknis column:'T501_staProblemTeknis', sqlType:'char(1)'
		t501CatatanTambahan column:'T501_CatatanTambahan'//, sqlType:'text'
		t501TglUpdate column:'T501_TglUpdate'//, sqlType: 'date'
        t501TanggalKirim column:'T501_TanggalKirim'//, sqlType: 'date'
		t501NamaKirim column:'T501_NamaKirim', sqlType:'varchar(50)'
		t501StaButuhPause column:'T501_StaButuhPause', sqlType:'char(1)'
		t501StaButuhRSA column:'T501_StaButuhRSA', sqlType:'char(1)'
		t501Sta_TL_ATL_NTL column:'T501_Sta_TL_ATL_NTL', sqlType:'char(1)'
		t501StaBisaKrjSdri column:'T501_StaBisaKrjSdri', sqlType:'char(1)'
		t501StaBthTambahWkt column:'T501_StaBthTambahWkt', sqlType:'char(1)'
		t501DurasiTambahWaktu column:'T501_DurasiTambahWaktu'//, sqlType: 'date'
		t501StaBthJob column:'T501_StaBthJob', sqlType:'char(1)'
		t501KetBthJob column:'T501_KetBthJob', sqlType:'varchar(50)'
		t501StaBthParts column:'T501_StaBthParts', sqlType:'char(1)'
		t501HasilDSS column:'T501_HasilDSS'//, sqlType:'text'
		t501StaSolved column:'T501_StaSolved', sqlType:'char(1)'
		t501TglSolved column:'T501_TglSolved'//, sqlType: 'date'
		t501StaButuhCat column:'T501_StaButuhCat', sqlType:'char(1)'
		t501StaTagihKepada column:'T501_StaTagihKepada', sqlType:'char(1)'
		vendorCat column:'T501_M191_ID'
		statusKendaraan column:'T501_M999_ID'

	}
}
