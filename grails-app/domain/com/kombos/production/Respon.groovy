package com.kombos.production

import com.kombos.administrasi.CompanyDealer
//import com.kombos.administrasi.UserProfile
import com.kombos.baseapp.sec.shiro.User
import com.kombos.administrasi.UserRole
import com.kombos.board.JPB
import com.kombos.production.Masalah
import com.kombos.woinformation.Actual

class Respon {
	CompanyDealer companyDealer
	JPB jpb
	Actual actual
	Masalah masalah
	UserRole userRole
	User userProfile
	Integer t505ID
	String t505Respon
	String t505StaSolusi
	Date t505TglUpdate
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
		companyDealer blank:true, nullable:true, maxSize:4
		jpb blank:true, nullable:true, maxSize:12
		actual blank:true, nullable:true, maxSize:12
		masalah blank:true, nullable:true, maxSize:4
		userRole blank:true, nullable:true, maxSize:2
		userProfile blank:true, nullable:true, maxSize:9
		t505ID blank:true, nullable:true, maxSize:4
		t505Respon blank:true, nullable:true, maxSize:16
		t505StaSolusi blank:true, nullable:true, maxSize:1
		t505TglUpdate blank:true, nullable:true
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T505_RESPON'
		companyDealer column:'T505_M011_ID'
		jpb column:'T505_T451_IDJPB'
		actual column:'T505_T452_ID'
		masalah column:'T505_T501_ID'
		userRole column:'T505_T003_ID'
		userProfile column:'T505_T001_IDUser'
		t505ID column:'T505_ID', sqlType:'int'
		t505Respon column:'T505_Respon'//, sqlType:'text'
		t505StaSolusi column:'T505_StaSolusi', sqlType:'char(1)'
		t505TglUpdate column:'T505_TglUpdate'//, sqlType: 'date'
	}
}
