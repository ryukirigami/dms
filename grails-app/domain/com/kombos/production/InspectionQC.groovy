package com.kombos.production

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
import com.kombos.administrasi.Stall
//import com.kombos.administrasi.UserProfile
import com.kombos.baseapp.sec.shiro.User
import com.kombos.reception.Reception

class InspectionQC {
    CompanyDealer companyDealer
	Reception reception
	Stall stall
	Integer t507ID
	Date t507TglJamMulai
	Date t507TglJamSelesai
	User userProfile
	String t507StaLolosQC
	Operation t507RedoKeProses_M190_ID
    Operation t507M053_JobID_Redo
	String t507Catatan
	Date t507TglUpdate
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:false, nullable:false
		stall blank:false, nullable:false
		t507ID blank:true, nullable:true
		t507TglJamMulai blank:false, nullable:false
		t507TglJamSelesai blank:false, nullable:false
		userProfile blank:true, nullable:true
		t507StaLolosQC blank:false, nullable:false, maxSize:1
		t507RedoKeProses_M190_ID blank:true, nullable:true
		t507M053_JobID_Redo blank:true, nullable:true
		t507Catatan blank:false, nullable:false, maxSize:50
		t507TglUpdate blank:false, nullable:false
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T507_INSPECTIONQC'
		reception column:'T507_T401_NoWO'
		stall column:'T507_M022_StallID'
		t507ID column:'T507_ID', sqlType:'int'
		t507TglJamMulai column:'T507_TglJamMulai'//, sqlType: 'date'
		t507TglJamSelesai column:'T507_TglJamSelesai'//, sqlType: 'date'
		userProfile column:'T507_T001_IDUser'
		t507StaLolosQC column:'T507_StaLolosQC', sqlType:'char(1)'
		t507RedoKeProses_M190_ID column:'T507_RedoKeProses_M190_ID'//, sqlType:'char(1)'
		t507M053_JobID_Redo column:'T507_M053_JobID_Redo'//, sqlType:'varchar(50)'
		t507Catatan column:'T507_Catatan', sqlType:'varchar(50)'
		t507TglUpdate column:'T507_TglUpdate'//, sqlType: 'date'
	}
}
