package com.kombos.production

class Deffect {

	String m503Id
	String m503NamaDeffect
	String staDel
    Date dateCreated // Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess  //Baris ini, terakhir prosess apa ?? insert, update, delete

	
	static mapping = {
        autoTimestamp false
        m503Id column : 'M503_ID', sqlType : 'char(2)'
		m503NamaDeffect column : 'M503_NamaDeffect', sqlType : 'varchar(50)'
		staDel column : 'M503_StaDel', sqlType : 'char(1)'
		table 'M503_Deffect'
	}
	
    static constraints = {
		m503Id (nullable : false, blank : false, maxSize : 2)
		m503NamaDeffect (nullable : false, blank : false, maxSize : 60)
		staDel (nullable : false, blank : false, maxSize : 1)
        createdBy nullable : false//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : false//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	String toString(){
		return m503NamaDeffect
	}
}
