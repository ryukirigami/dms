package com.kombos.production

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Operation
//import com.kombos.administrasi.UserProfile
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.FA
import com.kombos.reception.Reception

class FinalInspection {
    CompanyDealer companyDealer
	Reception reception
	Date t508TglJamMulai = new Date()
	Date t508TglJamSelesai = new Date()
	String t508StaButuhRSA
	String t508StaKategori
	FA fa
	String t508Keterangan
	String t508StaInspection
	String t508StaRedo
    Operation t508RedoKeProses_M190_ID
	Operation operation
	User userProfile
    Date dateCreated  = new Date()// Menunjukkan kapan baris isian ini dibuat.
    String createdBy //Menunjukkan siapa yang buat isian di baris ini.
    Date lastUpdated  = new Date() //Menunjukkan kapan baris isian ini update terakhir.
    String updatedBy  //Menunjukkan siapa yang trakhir update isian di baris ini.
    String lastUpdProcess = "insert" //Baris ini, terakhir prosess apa ?? insert, update, delete

    static constraints = {
        companyDealer (blank:true, nullable:true)
		reception blank:true, nullable:true, maxSize:20
		t508TglJamMulai blank:true, nullable:true
		t508TglJamSelesai blank:true, nullable:true
		t508StaButuhRSA blank:true, nullable:true, maxSize:1
		t508StaKategori blank:true, nullable:true, maxSize:4
		fa blank:true, nullable:true, maxSize:4
		t508Keterangan blank:true, nullable:true
		t508StaInspection blank:true, nullable:true, maxSize:1
		t508StaRedo blank:true, nullable:true, maxSize:1
		t508RedoKeProses_M190_ID blank:true, nullable:true, maxSize:4
		operation blank:true, nullable:true
		userProfile blank:true, nullable:true
        createdBy nullable : true//Menunjukkan siapa yang buat isian di baris ini.
        updatedBy  nullable : true//Menunjukkan siapa yang trakhir update isian di baris ini.
        lastUpdProcess  nullable : true//Baris ini, terakhir prosess apa ?? insert, update, delete
    }
	
	static mapping = {
        autoTimestamp false
        table 'T508_FINALINSPECTION'
		reception column:'T508_T401_NoWO'
		id column:'T508_ID'
		t508TglJamMulai column:'T508_TglJamMulai'//, sqlType: 'date'
		t508TglJamSelesai column:'T508_TglJamSelesai'
		t508StaButuhRSA column:'T508_StaButuhRSA', sqlType:'char(1)'
		t508StaKategori column:'T508_StaKategori', sqlType:'char(4)'
		fa column:'T508_M185_ID'
		t508Keterangan column:'T508_Keterangan'//, sqlType:'text'
		t508StaInspection column:'T508_StaInspection', sqlType:'char(1)'
		t508StaRedo column:'T508_StaRedo', sqlType:'char(1)'
		t508RedoKeProses_M190_ID column:'T508_RedoKeProses_M190_ID'
		operation column:'T508_M053_JobID'
		userProfile column:'T508_T001_IDUser'
	}

}
