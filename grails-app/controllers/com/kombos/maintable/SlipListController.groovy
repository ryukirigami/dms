package com.kombos.maintable

import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import com.kombos.maintable.InvoicingGenerateInvoicesController

/**
 *
 * @author Febry Indra Setyawan
 */
class SlipListController {
  
  def deliveryService

  def invoicingGenerateInvoices = new InvoicingGenerateInvoicesController()

  def jasperService
    
  static viewPermissions = [
    'index', 
    
    'invoiceList', 
    'invoiceDataTablesList', 
    'invoiceSubList', 
    'invoiceDataTablesSubList', 
    'printInvoice', 
    'previewInvoice',
    
    'partsSlipList', 
    'partsSlipDataTablesList', 
    'partsSlipSubList', 
    'partsSlipDataTablesSubList', 
    'partsSlipSubSubList',
    'partsSlipDataTablesSubSubList',
    'printPartsSlip',
    'previewPartsSlip',
    
    'fakturPajakList',
    'fakturPajakDataTablesList',
    'printFakturPajak',
    'previewFakturPajak',
    
    'notaReturList',
    'notaReturDataTablesList',
    'printNotaRetur',
    'previewNotaRetur',
    
    'kuitansiList',
    'kuitansiDataTablesList',
    'kuitansiSubList',
    'kuitansiDataTablesSubList',
    'previewKuitansi',
    
    'refundFormList',
    'refundFormDataTablesList',
    'previewRefundForm'
  ]
  
  def index() {
    redirect(action: "invoiceList", params: params)
  }
  
  /* Start Invoice Action */
  def invoiceList() {
    def metodeBayar = deliveryService.getMetodeBayarList()
    def tipeInvoice = deliveryService.getTipeInvoiceList()
    def customerSPK = deliveryService.getCustomerSpkList()  
    [idTable: new Date().format("yyyyMMddhhmmss"), metodeBayar: metodeBayar, tipeInvoice: tipeInvoice, customerSPK: customerSPK]
  }
  
  def invoiceDataTablesList() {
    params.companyDealer = session.userCompanyDealerId
    def dataTables = deliveryService.getInvoiceList(params)    
    render dataTables as JSON
  }
  
  def invoiceSubList() {
    [id: params.noInvoice, idTable: new Date().format("yyyyMMddhhmmss")]
  }
  
  def invoiceDataTablesSubList() {
    params.companyDealer = session.userCompanyDealerId
    def dataTables = deliveryService.getInvoiceSubList(params)
    render dataTables as JSON
  }
  
  def printInvoice() {
      InvoiceT701 invoiceData = InvoiceT701.findByT701NoInv(params.id)
      if(invoiceData?.id){
          def file = null
          params.idInv = invoiceData?.id
          if(invoiceData?.t701JenisInv=="K"){
              def inv = InvoiceT701.get(invoiceData?.id)
              inv.t701PrintKe = inv.t701PrintKe? (inv.t701PrintKe + 1) : 1
              inv.save(flush: true)
              invoicingGenerateInvoices.doJournalIvoiceKredit(invoiceData)
          }
          def reportData = invoicingGenerateInvoices.invoiceReportData(params)

          def reportDef = new JasperReportDef(name:'Invoice1.jasper',
                  fileFormat:JasperExportFormat.PDF_FORMAT,
                  reportData: reportData
          )

          file = File.createTempFile("Slip_Invoice_",".pdf")

          file.deleteOnExit()
          FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())
          response.setHeader("Content-Type", "application/pdf")
          response.setHeader("Content-disposition", "attachment;filename=${file.name}")
          response.outputStream << file.newInputStream()
      }else{
          response.setStatus(404)
      }
  }
  
  def previewInvoice() {
      params.companyDealer = session.userCompanyDealerId
      params.companyDealers = session.userCompanyDealer
      def invoiceData = deliveryService.searchInvoiceByNoInvoice(params)
      if(invoiceData?.noInvoice){
          [data:invoiceData]
      }else{
          response.setStatus(404)
      }
  }
  /* End Invoice Action */
  
  
  /* Start Parts Slip Action */
  def partsSlipList() {
    [idTable: new Date().format("yyyyMMddhhmmss")]
  }
  
  def partsSlipDataTablesList() {
    params.companyDealer = session.userCompanyDealerId
      def dataTables = deliveryService.getPartSlipList(params)
      render dataTables as JSON
  }
  
  def partsSlipSubList() {
    [id: params.noPartsSlip.toString().trim(), noInvoice: params.noInvoice.toString().trim(), idTable: new Date().format("yyyyMMddhhmmss")]
  }
  
  def partsSlipDataTablesSubList() {
    params.companyDealer = session.userCompanyDealerId
    def dataTables = deliveryService.getPartSlipSubList(params)
    render dataTables as JSON
  }
  
  def partsSlipSubSubList() {
    [id: params.noPartsSlip.toString().trim(), group:params.goodId, invId: params.invId, idTable: new Date().format("yyyyMMddhhmmss")]
  }
  
  def partsSlipDataTablesSubSubList() {
    params.companyDealer = session.userCompanyDealerId
    def dataTables = deliveryService.getPartSlipSubSubList(params)
    render dataTables as JSON
  }
  
  def printPartsSlip() {
      def partsSlipData = deliveryService.searchPartsSlipByNomorPartsSlip(params)
      if(partsSlipData.nomorWO){
          def jasperParams = [:]
          jasperParams.logo = grailsApplication.mainContext.servletContext.getRealPath("/images/toyota_genuine_parts.jpg")
          jasperParams.put("noPartsSlip",partsSlipData?.slipNo)
          jasperParams.put("noPolisi",partsSlipData?.nomorPolisi)
          jasperParams.put("customerName",partsSlipData?.customer)
          jasperParams.put("customerAddress",partsSlipData?.addess)
          jasperParams.put("noWO",partsSlipData?.nomorWO)
          jasperParams.put("slipNo",partsSlipData?.slipNo)
          jasperParams.put("slipDate",partsSlipData?.tanggal)
          jasperParams.put("slipTime",partsSlipData?.tanggal)

          def reportData = calculateData(partsSlipData?.details)

          def jasperReport = new JasperReportDef(
                  name:'partsSlip.jasper',
                  fileFormat:JasperExportFormat.PDF_FORMAT,
                  reportData: reportData,
                  parameters: jasperParams
          )

          def file = File.createTempFile("PartsSlip_",".pdf")
          file.deleteOnExit()

          FileUtils.writeByteArrayToFile(file, jasperService.generateReport(jasperReport).toByteArray())

          response.setHeader("Content-Type", "application/pdf")
          response.setHeader("Content-disposition", "attachment;filename=${file.name}")
          response.outputStream << file.newInputStream()
      }else{
          response.status(404)
      }
  }

  def calculateData(details){
      def reportData = new ArrayList();
      def data = [:]
      def count = 0
      def subTotal = 0
      def grandTotal = 0
      details.each {
          count = count + 1
          data.put("noSeq", details?.no)
          data.put("partsNo", details?.partNo)
          data.put("partsName", details?.partName)
          data.put("partsQty", details?.qty)
          data.put("partsUnitPrice", details?.unitPrice)
          data.put("partsDisc", details?.discount)
          data.put("partsExtdAmount", details?.extAmount)
          data.put("partsRemark", details?.remark)

          subTotal = subTotal + (details?.qty * details?.unitPrice)
          data.put("subTotal", subTotal)

          grandTotal = grandTotal + ((details?.qty * details?.unitPrice) - details?.discount)
          data.put("grandTotal", grandTotal)

          reportData.add(data)
      }
      return reportData
  }
  
  def previewPartsSlip() {
      params.companyDealer = session.userCompanyDealer
      def partData = deliveryService.searchPartsSlipByNomorPartsSlip(params)
      if(partData?.slipNo){
          [data:partData]
      }else{
          response.setStatus(404)
      }
  }
  /* End Parts Slip Action */
  
  
  /* Start Faktur Pajak Action */
  def fakturPajakList(){
    [idTable: new Date().format("yyyyMMddhhmmss")]
  }
  
  def fakturPajakDataTablesList() {
    def dataTables = deliveryService.getFakturPajakList(params)
    render dataTables as JSON
  }
  
  def printFakturPajak() {
      params.put("nomorFaktur",params.nomorNota)
      params.put("jenisFaktur",['1'])
      def notaData = deliveryService.printFakturByNoFaktur(params)
      if(notaData.noFaktur){

          def jasperData = [:]
          jasperData.put("kodeFakturPajak",notaData?.kodeFakturPajak)
          jasperData.put("namaPenjual",notaData?.namaPenjual)
          jasperData.put("alamatPenjual",notaData?.alamatPenjual)
          jasperData.put("npwpPenjual",notaData?.npwpPenjual)
          jasperData.put("tanggalPKP",notaData?.tanggalPKP)
          jasperData.put("namaPembeli",notaData?.namaPembeli)
          jasperData.put("alamatPembeli",notaData?.alamatPembeli)
          jasperData.put("npwpPembeli",notaData?.npwpPembeli)
          jasperData.put("hargaJasa",notaData?.hargaJasa)
          jasperData.put("hargaOther",notaData?.hargaOther)
          jasperData.put("totalHargaJual",notaData?.totalHargaJual)
          jasperData.put("totalPotongan",notaData?.totalPotongan)
          jasperData.put("totalUangMuka",notaData?.totalUangMuka)
          jasperData.put("totalKenaPajak",notaData?.totalKenaPajak)
          jasperData.put("totalPPN",notaData?.totalPPN)
          jasperData.put("tanggalFaktur",notaData?.tanggalFaktur)

          def reportData = new ArrayList();
          reportData.add(jasperData)

          def jasperReport = new JasperReportDef(
                  name:'fakturPajakPrint.jasper',
                  fileFormat:JasperExportFormat.PDF_FORMAT,
                  reportData: reportData
          )

          def file = File.createTempFile("FakturPajak_",".pdf")
          file.deleteOnExit()

          FileUtils.writeByteArrayToFile(file, jasperService.generateReport(jasperReport).toByteArray())

          response.setHeader("Content-Type", "application/pdf")
          response.setHeader("Content-disposition", "attachment;filename=${file.name}")
          response.outputStream << file.newInputStream()
      }else{
          response.status(404)
      }
  }
  
  def previewFakturPajak() {
      params.put("nomorFaktur",params.nomorNota)
      params.put("jenisFaktur",['1'])
      def fakturData = deliveryService.searchFakturByNoFaktur(params)
      if(fakturData?.noFaktur){
          [data:fakturData]
      }else{
          response.setStatus(404)
      }
  }
  /* End Faktur Pajak Action */
  
  
  /* Start Nota Retur Action */
  def notaReturList() {
    [idTable: new Date().format("yyyyMMddhhmmss")]
  }
  
  def notaReturDataTablesList() {
    def dataTables = deliveryService.getNotaReturList(params)
    render dataTables as JSON
  }
  
  def printNotaRetur() {
      params.put("nomorFaktur",params.nomorNota)
      params.put("jenisFaktur",['2','3'])
      def notaData = deliveryService.printNotaReturByNoFaktur(params)
      if(notaData.noFaktur){

          def jasperData = [:]
          jasperData.put("namaPembeli",notaData?.namaPembeli)
          jasperData.put("alamatPembeli",notaData?.alamatPembeli)
          jasperData.put("kotaPembeli",notaData?.kotaPembeli)
          jasperData.put("npwpPembeli",notaData?.npwpPembeli)
          jasperData.put("noFakturPajak",notaData?.noFakturPajak)
          jasperData.put("tanggalFakturPajak",notaData?.tanggalFakturPajak)
          jasperData.put("invoiceNo",notaData?.invoiceNo)
          jasperData.put("tanggalInvoice",notaData?.tanggalInvoice)
          jasperData.put("namaPenjual",notaData?.namaPenjual)
          jasperData.put("alamatPenjual",notaData?.alamatPenjual)
          jasperData.put("npwpPenjual",notaData?.npwpPenjual)
          jasperData.put("tanggalPengukuhan",notaData?.tanggalPengukuhan)
          jasperData.put("kwantumJasa",notaData?.kwantumJasa)
          jasperData.put("hargaSatuanJasa",notaData?.hargaSatuanJasa)
          jasperData.put("hargaJualJasa",notaData?.hargaJualJasa)
          jasperData.put("kwantumOther",notaData?.kwantumOther)
          jasperData.put("hargaSatuanOther",notaData?.hargaSatuanOther)
          jasperData.put("hargaJualOther",notaData?.hargaJualOther)
          jasperData.put("total",notaData?.total)
          jasperData.put("ppn",notaData?.ppn)

          def reportData = new ArrayList();
          reportData.add(jasperData)

          def jasperReport = new JasperReportDef(
                  name:'notaReturPrint.jasper',
                  fileFormat:JasperExportFormat.PDF_FORMAT,
                  reportData: reportData
          )

          def file = File.createTempFile("NotaRetur_",".pdf")
          file.deleteOnExit()

          FileUtils.writeByteArrayToFile(file, jasperService.generateReport(jasperReport).toByteArray())

          response.setHeader("Content-Type", "application/pdf")
          response.setHeader("Content-disposition", "attachment;filename=${file.name}")
          response.outputStream << file.newInputStream()
      }else{
          response.status(404)
      }
  }
  
  def previewNotaRetur() {
      params.put("nomorFaktur",params.nomorNota)
      params.put("jenisFaktur",['2','3'])
      def notaData = deliveryService.searchFakturByNoFaktur(params)
      if(notaData?.noFaktur){
          [data:notaData]
      }else{
          response.setStatus(404)
      }
  }
  /* End Nota Retur Action */
  
  
  /* Start Kuitansi Action */
  def kuitansiList() {
    def metodeBayar = deliveryService.getMetodeBayarList()
    def jenisBayar = deliveryService.getJenisBayarList()
    [idTable: new Date().format("yyyyMMddhhmmss"), metodeBayar: metodeBayar, jenisBayar: jenisBayar]
  }
  
  def kuitansiDataTablesList() {
    params.companyDealer = session.userCompanyDealerId
    def dataTables = deliveryService.getKuitansiList(params)
    render dataTables as JSON
  }
  
  def kuitansiSubList() {
    [id: params.noKuitansi, tglKui: params.tanggalKuitansi, idTable: new Date().format("yyyyMMddhhmmss")]
  }
  
  def kuitansiDataTablesSubList() {
    params.companyDealer = session.userCompanyDealerId
    def dataTables = deliveryService.getKuitansiSubList(params)
    render dataTables as JSON
  }
  
  def previewKuitansi() {
      params.companyDealer = session.userCompanyDealer
      def kuitansiData = deliveryService.searchKwitansiByNomorKwitansi(params)
      if(kuitansiData?.nomorKuitansi){
          [data:kuitansiData]
      }else{
          response.setStatus(404)
      }
  }
  /* End Kuitansi Action */
  
  
  /* Start Refund Form Action */
  def refundFormList() {
    def metodeRefund = deliveryService.getMetodeRefundList()
    def jenisRefund = deliveryService.getJenisRefundList()
    [idTable: new Date().format("yyyyMMddhhmmss"), metodeRefund: metodeRefund, jenisRefund: jenisRefund]
  }
  
  def refundFormDataTablesList() {
    params.companyDealer = session.userCompanyDealerId
    def dataTables = deliveryService.getRefundFormList(params)
    render dataTables as JSON
  }
  
  def previewRefundForm() {
    def refundData = deliveryService.searchRefundByNomorRefund(params)
    if(refundData?.nomorRefund){
        [data:refundData]
    }else{
        response.setStatus(404)
    }
  }
  /* End Refund Form Action */
}