package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class AlertController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	def alertService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session.userCompanyDealer
		render alertService.datatablesList(params) as JSON
	}

	def create() {
		def result = alertService.create(params)
        if(!result.error)
            return [alertInstance: result.alertInstance]
        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
		 def result = alertService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Alert", result.alertInstance.id])
            redirect(action:'show', id: result.alertInstance.id)
            return
        }

        render(view:'create', model:[alertInstance: result.alertInstance])
	}

	def show(Long id) {
		def result = alertService.show(params)
		
		
		if(!result.error)
			return [ alertInstance: result.alertInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = alertService.show(params)
		def status =  result.alertInstance.statusAksi

		def nextStatus 
		if(status == 'Baru'){
			nextStatus = "BACA"
		} else if(status == 'Baca'){
			nextStatus = "TINDAK LANJUT"
		}

        if(!result.error)
			return [ alertInstance: result.alertInstance, nextStatus: nextStatus ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = alertService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Alert", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[alertInstance: result.alertInstance.attach()])
	}

	def delete() {
		def result = alertService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Alert", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(Alert, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
