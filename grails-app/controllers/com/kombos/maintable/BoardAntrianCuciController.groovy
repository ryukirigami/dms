package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.ManPowerDetail
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Stall
import com.kombos.production.FinalInspection
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class BoardAntrianCuciController {
    def datatablesUtilService

    def index() {

    }

    def doClockOff(){
        if(!params.customer){
            def data = [status: "Error", message:"Pilih salah satu Kendaraan"]
            render data as JSON
            return
        }

        def temp = ActualCuci.get(params.customer)
        temp.flagClockOff = true
        temp.save(flush: true)

        def actualCuci = new ActualCuci(temp.properties)
        actualCuci.flagClockOff = true
        actualCuci?.companyDealer=session.userCompanyDealer
        actualCuci.t602TglJam = new Date()
        actualCuci.statusActual = StatusActual.findByM452StatusActual('Clock Off')
        actualCuci.t602Ket = params.catatan
        actualCuci.save(flush: true)
        def antriCuci = temp.antriCuciID
        antriCuci.actualCuci = actualCuci
        antriCuci.save(flush: true)

        def data = [status: "OK"]
        render data as JSON
    }

    def doClockOn(){
        if(!params.customer){
            def data = [status: "Error", message:"Pilih salah satu Kendaraan"]
            render data as JSON
            return
        }

        def antriCuci = AntriCuci.get(params.customer)
        def actualCuci = new ActualCuci(
                companyDealer: antriCuci?.companyDealer,
                antriCuciID: antriCuci,
                antriCuciTanggal: antriCuci,
                namaManPower: NamaManPower.get(params.namaWasher),
                stall: Stall.get(params.stallCuci),
                t602TglJam: datatablesUtilService?.syncTime(),
                t602StaDel: '0',
                statusActual: StatusActual.findByM452StatusActual('Clock On'),
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()

        )
        actualCuci.save(flush: true)
        antriCuci.actualCuci = actualCuci
        antriCuci.lastUpdated = datatablesUtilService?.syncTime()
        antriCuci.save(flush: true)
        def data = [status: "OK"]
        render data as JSON
    }

    def kendaraanAntriCuci(){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = AntriCuci.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)
            isNull("actualCuci")
            eq("t600StaDel","0")
            switch (sortProperty) {
                default:
                    order("t600NomorAntrian", "asc")
                    break;
            }

        }
        results = results.sort{a,b -> a.equals(b)?0: a.customField<b.customField? -1:1}
        def rows = []
        results.each {
            def reception = it?.reception
            def currentCondition = reception?.historyCustomerVehicle
            rows << [
                    id: it.id,
                    noAntri: it.t600NomorAntrian,
                    nopol: currentCondition?.kodeKotaNoPol?.m116ID + ' ' + currentCondition?.t183NoPolTengah + ' ' + currentCondition?.t183NoPolBelakang,
                    stall: reception?.stall?.m022NamaStall,
                    waktuSerah: reception?.t401TglJamJanjiPenyerahan?.format("HH:mm"),
                    sa: reception?.t401NamaSA,

            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }
    def kendaraanSedangCuci(){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = ActualCuci.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)
            eq("flagClockOff", false)
            statusActual {
                eq("m452StatusActual", 'Clock On')
            }
            switch (sortProperty) {
                default:
                    order("id", "desc")
                    break;
            }
        }
        def rows = []
        results.each {
            def antriCuciID = it?.antriCuciID
            def reception = antriCuciID?.reception
            def currentCondition = reception?.historyCustomerVehicle
            rows << [
                    id: it.id,
                    noAntri: antriCuciID.t600NomorAntrian,
                    nopol: currentCondition?.fullNoPol,
                    stall: it?.stall?.m022NamaStall,
                    washer: it?.namaManPower?.t015NamaLengkap,
                    sa: reception?.t401NamaSA
            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

    def kendaraanSelesaiCuci(){
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = ActualCuci.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)
            eq("flagClockOff", true)
            statusActual {
                eq("m452StatusActual", 'Clock Off')
            }
            switch (sortProperty) {
                default:
                    order("id", "desc")
                    break;
            }
        }
//

        def rows = []
        results.each {
            def antriCuciID = it?.antriCuciID
            def reception = antriCuciID?.reception
            def currentCondition = reception?.historyCustomerVehicle?.customerVehicle?.currentCondition
            rows << [
                    id: it?.id,
                    noAntri: antriCuciID?.t600NomorAntrian,
                    nopol: currentCondition?.fullNoPol,
                    stall: it?.stall?.m022NamaStall,
                    tanggal: it?.t602TglJam? it?.t602TglJam.format("dd/MM/yyyy HH:mm"):"",
                    washer: it?.namaManPower?.t015NamaLengkap,
                    sa: reception?.t401NamaSA
            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON

    }


    def skipkendaraancuci(Long id){
        def skip=AntriCuci.get(id)

        try {
            skip?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            skip?.setLastUpdProcess("DELETE")
            skip?.setT600StaDel("1")
            skip?.lastUpdated = datatablesUtilService?.syncTime()
            skip.save(flush: true)
            redirect(action: "list")

        }
        catch(DataIntegrityViolationException e){
//            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "show", id: id)
        }
    }
}
