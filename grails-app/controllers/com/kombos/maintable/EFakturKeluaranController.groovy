package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import groovy.sql.Sql
import org.apache.commons.io.FileUtils
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat

class EFakturKeluaranController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def EFakturKeluaranService

//    @Procedure(actionName="E_FAKTUR")
    def koneksiToOracleService
    def dataSource
    def sessionFactory
    def jasperService

    def excelImportService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() { }

    def uploadData() {
        [tanggal : new Date()]
    }

    def list(Integer max) {
        [docNumber : params.docNumber]
    }

    def edit(Long id) {
        def EFakturInstance = EFakturKeluaran.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'TESTER'), id])
            redirect(action: "list")
            return
        }

        [EFakturInstance: EFakturInstance]
    }

    def update(Long id, Long version) {
        def EFakturInstance = EFakturKeluaran.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'TESTER2'), id])
            redirect(action: "list")
            return
        }

        EFakturInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        EFakturInstance?.setLastUpdProcess("UPDATE")
        EFakturInstance.properties = params
        EFakturInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if (!EFakturInstance.save(flush: true)) {
            render(view: "edit", model: [EFakturInstance: EFakturInstance])

            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'bank.label', default: 'Efaktur'), EFakturInstance.id])
        redirect(action: "show", id: EFakturInstance.id)
    }

    def show(Long id) {
        def EFakturInstance = EFakturKeluaran.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Efaktur'), id])
            redirect(action: "list")
            return
        }

        [EFakturInstance: EFakturInstance]
    }

    def delete(Long id) {
        def EFakturInstance = EFakturKeluaran.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "list")
            return
        }

        try {
            EFakturInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            EFakturInstance?.setLastUpdProcess("DELETE")
            EFakturInstance?.lastUpdated = datatablesUtilService?.syncTime()
            EFakturInstance?.setStaDel('1')
            EFakturInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
//            datatablesUtilService.massDeleteStaDelNew(EFaktur, params)
            def jsonArray = JSON.parse(params.ids)
            jsonArray.each { arr ->
                def efak = EFakturKeluaran.get(arr as Long)
                efak.staDel = '1'
                efak.save(flush: true)
            }
            res.message = "Mass Delete Success"

        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }



    def datatablesList() {
        session.exportParams = params
        params.userCompanyDealer = session?.userCompanyDealer
        render EFakturKeluaranService.datatablesList(params) as JSON
    }


    def datatablesList1() {
        session.exportParams = params
        params.userCompanyDealer = session?.userCompanyDealer
        render EFakturKeluaranService.datatablesList1(params) as JSON
    }


    def view() {
        def date = datatablesUtilService.syncTime()
        session["PPN_KELUARAN"] = "";
        def cek = EFakturKeluaran.createCriteria()
        def ppnMasukan = cek.list() {
            eq("companyDealer",session.userCompanyDealer)
            ge("dateCreated",date.clearTime())
            lt("dateCreated",date.clearTime() + 1)
            eq("staDel",'0')
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
            }
        }
        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'NO',
                        'B':'MASA_PAJAK',
                        'C':'TAHUN_PAJAK',
                        'D':'REFERENSI',
                        'E':'NO_SI',
                        'F':'TGL_SI',
                        'G':'NAMA_CUSTOMER',
                        'H':'NPWP',
                        'I':'ALAMAT',
                        'J':'BIAYA_JASA',
                        'K':'BIAYA_PARTS',
                        'L':'BIAYA_OLI',
                        'M':'BIAYA_MATERIAL',
                        'N':'BIAYA_SUBLET',
                        'O':'BIAYA_ADM',
                        'P':'DISC_JASA',
                        'Q':'DISC_PARTS',
                        'R':'DISC_OLI',
                        'S':'DISC_BAHAN',
                        'T':'DPP',
                        'U':'PPN',
                        'V':'TOTAL'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }

        def jsonData = []
        int jmlhDataError = 0;
        def isiEror= "";
        String htmlData = ""
        def arrMasaPajak = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        def arrThnPajak = ["2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2015","2026","2027"]
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/vnd-ms-excel','application/octet-stream','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]

            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "uploadData")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            String status = "0", style = "color:black;", flag = "black"
            int nomor = 0
            jobList.each {
                nomor++
                style = "color:black;"
                flag = "black"
                try {
                    it.MASA_PAJAK = it.MASA_PAJAK.toInteger().toString()
                    if(it.MASA_PAJAK.toInteger() < 10){
                        it.MASA_PAJAK = "0" + it.MASA_PAJAK
                    }
                    it.TAHUN_PAJAK = it.TAHUN_PAJAK.toInteger().toString()
                    if(it.NPWP == 0.0){
                        it.NPWP = "0"
                    }
                }catch(Exception e){

                }

                if((it.MASA_PAJAK=="" || it.MASA_PAJAK==null) || !arrMasaPajak.contains(it.MASA_PAJAK.toString())){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" :BULAN DIISI (01-12) !! <br>"
                    jmlhDataError++
                }

                if((it.TAHUN_PAJAK=="" || it.TAHUN_PAJAK==null) || !arrThnPajak.contains(it.TAHUN_PAJAK.toInteger().toString())){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : TAHUN DIISI (EX. 2016) !! <br>"
                    jmlhDataError++
                }
                if(it.NO_SI=="" || it.NO_SI==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : SI TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.NAMA_CUSTOMER=="" || it.NAMA_CUSTOMER==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : NAMA CUSTOMER TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.ALAMAT=="" || it.ALAMAT==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : ALAMAT CUSTOMER TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.NPWP=="" || it.NPWP==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : NPWP TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.REFERENSI=="" && it.REFERENSI==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : REFERENSI TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                //
                if(it.BIAYA_JASA=="" || it.BIAYA_JASA=="-" || it.BIAYA_JASA==null){
                    it.BIAYA_JASA = "0"
                }else{
                    try{
                        it.BIAYA_JASA = it.BIAYA_JASA.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA JASA HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_PARTS=="" ||it.BIAYA_PARTS=="-"|| it.BIAYA_PARTS==null){
                    it.BIAYA_PARTS = "0"
                }else{
                    try{
                        it.BIAYA_PARTS = it.BIAYA_PARTS.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA PARTS HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_OLI=="" ||it.BIAYA_OLI=="-" || it.BIAYA_OLI==null){
                    it.BIAYA_OLI = "0"
                }else{
                    try{
                        it.BIAYA_OLI = it.BIAYA_OLI.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA OLI HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_MATERIAL=="" || it.BIAYA_MATERIAL=="" || it.BIAYA_MATERIAL==null){
                    it.BIAYA_MATERIAL = "0"
                }else{
                    try{
                        it.BIAYA_MATERIAL = it.BIAYA_MATERIAL.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA MATERIAL HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_SUBLET=="" || it.BIAYA_SUBLET==""  || it.BIAYA_SUBLET==null){
                    it.BIAYA_SUBLET = "0"
                }else{
                    try{
                        it.BIAYA_SUBLET = it.BIAYA_SUBLET.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA SUBLET HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_ADM=="" || it.BIAYA_ADM==""  || it.BIAYA_ADM==null){
                    it.BIAYA_ADM = "0"
                }else{
                    try{
                        it.BIAYA_ADM = it.BIAYA_ADM.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA ADM HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DISC_JASA=="" || it.DISC_JASA==""  || it.DISC_JASA==null){
                    it.DISC_JASA = "0"
                }else{
                    try{
                        it.DISC_JASA = it.DISC_JASA.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_JASA HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DISC_PARTS=="" || it.DISC_PARTS==""  || it.DISC_PARTS==null){
                    it.DISC_PARTS = "0"
                }else{
                    try{
                        it.DISC_PARTS = it.DISC_PARTS.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_PARTS HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DISC_BAHAN=="" || it.DISC_BAHAN==""  || it.DISC_BAHAN==null){
                    it.DISC_BAHAN = "0"
                }else{
                    try{
                        it.DISC_BAHAN = it.DISC_BAHAN.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_BAHAN HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }

                if(it.DISC_OLI=="" || it.DISC_OLI==""  || it.DISC_OLI==null){
                    it.DISC_OLI = "0"
                }else{
                    try{
                        it.DISC_OLI = it.DISC_OLI.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_OLI HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DPP=="" || it.DPP==""  || it.DPP==null){
                    it.DPP = "0"
                }else{
                    try{
                        it.DPP = it.DPP.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DPP HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.PPN=="" || it.PPN==""  || it.PPN==null){
                    it.PPN = "0"
                }else{
                    try{
                        it.PPN = it.PPN.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : PPN  HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.TOTAL=="" || it.TOTAL==""  || it.TOTAL==null){
                    it.TOTAL = "0"
                }else{
                    try{
                        it.TOTAL = it.TOTAL.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : TOTAL HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                //
                try {
                    Date TGL_SI = Date.parse("yyyy-MM-dd",it.TGL_SI.toString())
                }catch (Exception e){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : Format Tanggal (31/12/2016)<br>"
                    jmlhDataError++
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+nomor+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.MASA_PAJAK.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.TAHUN_PAJAK.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.REFERENSI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.NO_SI.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.TGL_SI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.NAMA_CUSTOMER.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.NPWP.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.ALAMAT.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_JASA+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_PARTS+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_OLI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_MATERIAL+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_SUBLET+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_ADM+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_JASA+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_PARTS+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_OLI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_BAHAN+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DPP+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.PPN+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.TOTAL+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
                jsonData << [
                        MASA_PAJAK:it.MASA_PAJAK,
                        TAHUN_PAJAK:it.TAHUN_PAJAK,
                        REFERENSI:it.REFERENSI,
                        NO_SI:it.NO_SI,
                        TGL_SI:it.TGL_SI,
                        NAMA_CUSTOMER:it.NAMA_CUSTOMER,
                        NPWP:it.NPWP,
                        ALAMAT:it.ALAMAT,
                        BIAYA_JASA:it.BIAYA_JASA,
                        BIAYA_PARTS:it.BIAYA_PARTS,
                        BIAYA_OLI:it.BIAYA_OLI,
                        BIAYA_MATERIAL:it.BIAYA_MATERIAL,
                        BIAYA_SUBLET:it.BIAYA_SUBLET,
                        BIAYA_ADM:it.BIAYA_ADM,
                        DISC_JASA:it.DISC_JASA,
                        DISC_PARTS:it.DISC_PARTS,
                        DISC_OLI:it.DISC_OLI,
                        DISC_BAHAN:it.DISC_BAHAN,
                        DPP:it.DPP,
                        PPN:it.PPN,
                        TOTAL:it.TOTAL,
                        flag : flag
                ]
            }
        }
        if (jsonData.size()==0){
            flash.message = message(code: 'default.uploadGoods.message', default: "File Tidak dapat diproses")
        }else if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah")
        }
        else {
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
        }
        session["PPN_KELUARAN"] = jsonData
        render(view: "uploadData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError, tanggal : params.tanggalUpload, jumJson : jsonData.size(), isiEror : isiEror])
    }


    def save() {

    }

    def saveApproval(){
        def data = JSON.parse(params.arr)
        def pesan = [:]
        data.each { dataFaka ->
            def faktur = EFakturKeluaran.get(dataFaka.dataid as Long)
            faktur.setStaAprove(dataFaka.staApprove.toString())
            faktur.save(flush: true)
        }
        render pesan as JSON
    }

    def showDetail() {
        [dataFakturDetail : EFakturKeluaran.findAllByDocNumberAndStaDelAndStaAprove(params.docNumber,"0","0",[sort: 'id',order: 'asc'])]
    }

    def chekNoEfaktur() {
        [dataFakturDetail : EFakturKeluaran.findAllByDocNumberAndStaDelAndStaAprove(params.docNumber,"0","1",[sort: 'id',order: 'asc'])]
    }


    def printEfaktur(){
        List<JasperReportDef> reportDefList = []
        params.userCompanyDealer = session?.userCompanyDealer
        reportDefList = EFakturKeluaranService.printEfaktur(params)
        def file = File.createTempFile("DMS_" + params.docNumber + "_",".csv")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "text/csv")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

//  Simpan data yang telah diupload (uploadData)
    def upload(){

        def date = datatablesUtilService.syncTime()
        def requestBody = JSON.parse(params.sendData)
        def tanggal= Date.parse("dd/MM/yyyy",params.tanggal)

        def EFakturInstance = null
        def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
        def sdf = new SimpleDateFormat("yyyyMMdd")
        String docNumber = com+".OUT."+sdf.format(tanggal)

        requestBody.each{
            EFakturInstance = new EFakturKeluaran()
            EFakturInstance.companyDealer = session?.userCompanyDealer

            EFakturInstance.MASA_PAJAK = it.MASA_PAJAK
            EFakturInstance.TAHUN_PAJAK = it.TAHUN_PAJAK
            EFakturInstance.REFERENSI = it.REFERENSI
            EFakturInstance.NO_SI = it.NO_SI
            EFakturInstance.TGL_SI = new Date().parse("yyyy-MM-dd", it?.TGL_SI.toString())
            EFakturInstance.NAMA_CUSTOMER = it.NAMA_CUSTOMER
            EFakturInstance.NPWP = it.NPWP
            EFakturInstance.ALAMAT = it.ALAMAT
            EFakturInstance.BIAYA_JASA = it.BIAYA_JASA.toBigDecimal()
            EFakturInstance.BIAYA_PARTS = it.BIAYA_PARTS.toBigDecimal()
            EFakturInstance.BIAYA_OLI = it.BIAYA_OLI.toBigDecimal()
            EFakturInstance.BIAYA_MATERIAL = it.BIAYA_MATERIAL.toBigDecimal()
            EFakturInstance.BIAYA_SUBLET = it.BIAYA_SUBLET.toBigDecimal()
            EFakturInstance.BIAYA_ADM = it.BIAYA_ADM.toBigDecimal()
            EFakturInstance.DISC_JASA = it.DISC_JASA.toBigDecimal()
            EFakturInstance.DISC_PARTS = it.DISC_PARTS.toBigDecimal()
            EFakturInstance.DISC_OLI = it.DISC_OLI.toBigDecimal()
            EFakturInstance.DISC_BAHAN = it.DISC_BAHAN.toBigDecimal()
            EFakturInstance.DPP = it.DPP.toBigDecimal()
            EFakturInstance.PPN = it.PPN.toBigDecimal()
            EFakturInstance.TOTAL = it.TOTAL.toBigDecimal()

//            EFakturInstance.NO_FAKTUR = '0'

            EFakturInstance.staAprove = '0'
            EFakturInstance.staDel = '0'
            EFakturInstance.staOriRev = '0'
            EFakturInstance.docNumber = docNumber
            EFakturInstance.tanggal = tanggal
            EFakturInstance.dateCreated = datatablesUtilService?.syncTime()
            EFakturInstance.lastUpdated = datatablesUtilService?.syncTime()
            EFakturInstance.lastUpdProcess = 'INSERT'
            EFakturInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            EFakturInstance.save(flush: true)
            EFakturInstance.errors.each {
                println it
            }
            flash.message = message(code: 'default.uploadGoods.message', default: "Save Data Sukses")
        }

        render(view: "uploadData", model: [woTransactionInstance: EFakturInstance, tanggal : tanggal])

    }

    def exportToExcel(){
        List<JasperReportDef> reportDefList = []
        params.dataExcel = session["PPN_KELUARAN"]
        def reportData = exportToExcelDetail(params) //komen ini jangan dulu di hapus

        def reportDef = new JasperReportDef(name:'reportEfaktur.jasper',
                fileFormat:JasperExportFormat.XLS_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("EFaktur_ppnKeluaran",".xls")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }

    def exportToExcelDetail(def params){
        def jsonArray = params.dataExcel
        def reportData = new ArrayList();
        jsonArray.each {
            def data = [:]
            data.put("COLUMN","MASA_PAJAK")
            data.put("COLUMN_1","TAHUN_PAJAK")
            data.put("COLUMN_2","REFERENSI")
            data.put("COLUMN_3","NO_SI")
            data.put("COLUMN_4","TGL_SI")
            data.put("COLUMN_5","NAMA_CUSTOMER")
            data.put("COLUMN_6","NPWP")
            data.put("COLUMN_7","ALAMAT")
            data.put("COLUMN_8","BIAYA_JASA")
            data.put("COLUMN_9","BIAYA_PARTS")
            data.put("COLUMN_10","BIAYA_OLI")
            data.put("COLUMN_11","BIAYA_MATERIAL")
            data.put("COLUMN_12","BIAYA_SUBLET")
            data.put("COLUMN_13","BIAYA_ADM")
            data.put("COLUMN_14","DISC_JASA")
            data.put("COLUMN_15","DISC_PARTS")
            data.put("COLUMN_16","DISC_BAHAN")
            data.put("COLUMN_17","DPP")
            data.put("COLUMN_18","PPN")
            data.put("COLUMN_19","TOTAL")

            data.put("FLAG_COLOR",it.flag)
            data.put("F_COLUMN",it.MASA_PAJAK)
            data.put("F_COLUMN_1",it.TAHUN_PAJAK)
            data.put("F_COLUMN_2",it.REFERENSI)
            data.put("F_COLUMN_3",it.NO_SI)
            data.put("F_COLUMN_4",it.TGL_SI)
            data.put("F_COLUMN_5",it.NAMA_CUSTOMER)
            data.put("F_COLUMN_6",it.NPWP)
            data.put("F_COLUMN_7",it.ALAMAT)
            data.put("F_COLUMN_8",it.BIAYA_JASA)
            data.put("F_COLUMN_9",it.BIAYA_PARTS)
            data.put("F_COLUMN_10",it.BIAYA_OLI)
            data.put("F_COLUMN_11",it.BIAYA_MATERIAL)
            data.put("F_COLUMN_12",it.BIAYA_SUBLET)
            data.put("F_COLUMN_13",it.BIAYA_ADM)
            data.put("F_COLUMN_14",it.DISC_JASA)
            data.put("F_COLUMN_15",it.DISC_PARTS)
            data.put("F_COLUMN_16",it.DISC_BAHAN)
            data.put("F_COLUMN_17",it.DPP)
            data.put("F_COLUMN_18",it.PPN)
            data.put("F_COLUMN_19",it.TOTAL)


            reportData.add(data)
        }

        return reportData
    }

    def generateNofak (){
        def data = JSON.parse(params.arr)
        def pesan = [:]
        data.each {
            def idfaktur = EFakturKeluaran.get(it.dataid as Long)
            Sql sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
            def query = "call GENERATE_NOFAKTUR("+it.dataid+")";
            sql.call(query)
            sql.commit();
//            faktur.setNOMOR_FAKTUR(it?.noFaktur.toString())
            idfaktur.save(flush: true)
        }
        render pesan as JSON
    }

//    Fungsi disini masih absurt
    def generateData (){
        def startTgl = params.tgl1.toString()
        def endTgl = params.tgl2.toString()
        def comp = session?.userCompanyDealerId
        def jenisInputan = 'PPN_KELUARAN';
        def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID

        Date date1 = new Date().parse("dd-MM-yyyy",startTgl)
        Date date2 = new Date().parse("dd-MM-yyyy",endTgl)
        Date date3 = date1
        def selisih = date2.date - date1.date
        date2.date+=1

        (1..(selisih+1)).each {
            String docNumber = com+".OUT."+date3.format("yyyyMMdd")
            startTgl = date3.format("dd-MM-yyyy")
            date3.date+=1
            endTgl = date3.format("dd-MM-yyyy")
            Sql sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
            Sql sql2 = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
            def query = "call E_FAKTUR_KELUARAN('"+docNumber+"','"+jenisInputan+"','"+startTgl+"', '"+endTgl+"',"+comp+")";
            def query2 = "call EFAKTUR_KELUARAN_PJL_LGSG('"+docNumber+"','"+jenisInputan+"','"+startTgl+"', '"+endTgl+"',"+comp+")";
            sql.call(query)
            sql2.call(query2)
            sql.commit();
        }

    }

    def exportCSV={
        def jsonArray = JSON.parse(params.docNumber)

        Date today = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
        String date = DATE_FORMAT.format(today);

        response.setHeader("Content-disposition","attachment; filename=DMS_PPNKeluaran"+date+".csv")

        def results=[]
        jsonArray.each {
            results << it
        }
        def docNumber = null
        results.each {
            docNumber = it
        }

        params.userCompanyDealer = session?.userCompanyDealer
        def efaktur = EFakturKeluaran.createCriteria().list {
            eq("staDel","0")
            not{
                isNull("NO_FAKTUR")
            }
            eq("staAprove","1")
            if(!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("companyDealer",session?.userCompanyDealer)
            }
//            order("noUrutInvoice","ASC")
            inList("docNumber", results)
        }

        def result= 'FK, KD_JENIS_TRANSAKSI, FG_PENGGANTI, NOMOR_FAKTUR, MASA_PAJAK, TAHUN_PAJAK, TANGGAL_FAKTUR, NPWP, NAMA, ALAMAT_LENGKAP, JUMLAH_DPP, JUMLAH_PPN, JUMLAH_PPNBM, ID_KETERANGAN_TAMBAHAN, FG_UANG_MUKA, UANG_MUKA_DPP, UANG_MUKA_PPN, UANG_MUKA_PPNBM, REFERENSI,\n' +
                    'LT, NPWP2, NAMA2, JALAN, BLOK, NOMOR, RT, RW, KECAMATAN, KELURAHAN, KABUPATEN, PROPINSI, KODE_POS, NOMOR_TELEPON,\n' +
                    'OF, KODE_OBJEK, NAMA_OBJEK, HARGA_SATUAN, JUMLAH_BARANG, HARGA_TOTAL, DISKON, DPP, PPN, TARIF_PPNBM, PPNBM\n'

        def npwpEfaktur = NpwpEfaktur.list().get(0)

        efaktur.each {u ->

            def FK = "FK"
            def KD_JENIS_TRANSAKSI = '01'
            def FG_PENGGANTI = '0'
            def NOMOR_FAKTUR = u.NO_FAKTUR
            def MASA_PAJAK = u.MASA_PAJAK
            def TAHUN_PAJAK = u.TAHUN_PAJAK
            def TANGGAL_FAKTUR = u.TGL_SI.format("dd/MM/yyyy")
            def NPWP = u.NPWP
            if (NPWP==null){
                NPWP = "000000000000000"
            }
            if (NPWP=="0" || NPWP=="-"){
                NPWP = "000000000000000"
            }
            if(NPWP=="00.000.000.0-000.000"){
                NPWP = "000000000000000"
            }
            def NAMA = u.NAMA_CUSTOMER.replaceAll(",",".")
            def ALAMAT_LENGKAP = u.ALAMAT.replaceAll(",",".")
            def JUMLAH_DPP = u.DPP
            if (JUMLAH_DPP==null){
                JUMLAH_DPP=0
            }
            def JUMLAH_PPN = u.PPN
            if (JUMLAH_PPN==null){
                JUMLAH_PPN=0
            }
            def JUMLAH_PPNBM = "0"
            def ID_KETERANGAN_TAMBAHAN = ""
            def FG_UANG_MUKA = "0"
            def UANG_MUKA_DPP = "0"
            def UANG_MUKA_PPN = "0"
            def UANG_MUKA_PPNBM = "0"
            def REFERENSI = u.REFERENSI.replaceAll(",",".")

            def LT = "FAPR"

            def NPWP2 = npwpEfaktur.namaPerusahaan
            def NAMA2 = npwpEfaktur.alamatNpwp
            def JALAN = npwpEfaktur.penandaTangan
            def BLOK = "", NOMOR = "", RT = "", RW = "", KECAMATAN = "", KELURAHAN = "", KABUPATEN = "", PROPINSI = "", KODE_POS = "", NOMOR_TELEPON = ""

            def OF = "OF"
            def KODE_OBJEK
            def NAMA_OBJEK
            def HARGA_SATUAN
            def JUMLAH_BARANG = "1"
            def HARGA_TOTAL
            def DISKON
            def DPP
            def PPN
            def TARIF_PPNBM = "0"
            def PPNBM = "0"

            def d = EFakturKeluaran.findByDocNumber(u.docNumber)
            if (d){
                if(session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                    def ubha = EFakturKeluaran.get(u.id)
                    ubha.lastUpdProcess = 'DOWNLOAD'
                    ubha.save(flush: true)
                }
                result += FK+","+ KD_JENIS_TRANSAKSI+","+ FG_PENGGANTI+","+ NOMOR_FAKTUR+","+ MASA_PAJAK+","+ TAHUN_PAJAK+","+ TANGGAL_FAKTUR+","+ // baris pertama
                        NPWP+","+ NAMA+","+ ALAMAT_LENGKAP+","+ JUMLAH_DPP+","+ JUMLAH_PPN+","+ JUMLAH_PPNBM+","+ ID_KETERANGAN_TAMBAHAN+","+ FG_UANG_MUKA+","+
                        UANG_MUKA_DPP+","+ UANG_MUKA_PPN+","+ UANG_MUKA_PPNBM+","+ u.REFERENSI+"\n"

                result += LT+","+ NPWP2+","+ NAMA2+","+ JALAN+","+ BLOK+","+ NOMOR+","+ RT+","+ RW+","+ KECAMATAN+","+ KELURAHAN+","+ KABUPATEN+","+ PROPINSI+","+ KODE_POS+","+ NOMOR_TELEPON+"\n" //baris kedua

                if(u.BIAYA_JASA!=0)
                {
                    result += OF+",01,BIAYA JASA,"+u.BIAYA_JASA+",1,"+u.BIAYA_JASA+","+u.DISC_JASA+"," +
                            ""+(u.BIAYA_JASA - u.DISC_JASA)+","+((u.BIAYA_JASA - u.DISC_JASA) * 0.1)+",0,0\n"
                }

                if(u.BIAYA_PARTS!=0)
                {
                    result += OF+",02,BIAYA PARTS,"+u.BIAYA_PARTS+",1,"+u.BIAYA_PARTS+","+u.DISC_PARTS+"," +
                            ""+(u.BIAYA_PARTS - u.DISC_PARTS)+","+((u.BIAYA_PARTS - u.DISC_PARTS) * 0.1)+",0,0\n"
                }

                if(u.BIAYA_OLI!=0)
                {
                    result += OF+",03,BIAYA OLI,"+u.BIAYA_OLI+",1,"+u.BIAYA_OLI+","+u.DISC_OLI+"," +
                            ""+(u.BIAYA_OLI - u.DISC_OLI)+","+((u.BIAYA_OLI - u.DISC_OLI) * 0.1)+",0,0\n"
                }

                if(u.BIAYA_MATERIAL!=0)
                {
                    result += OF+",04,BIAYA MATERIAL,"+u.BIAYA_MATERIAL+",1,"+u.BIAYA_MATERIAL+","+u.DISC_BAHAN+"," +
                            ""+(u.BIAYA_MATERIAL - u.DISC_BAHAN)+","+((u.BIAYA_MATERIAL - u.DISC_BAHAN) * 0.1)+",0,0\n"
                }

                if(u.BIAYA_SUBLET!=0)
                {
                    result += OF+",05,BIAYA SUBLET,"+u.BIAYA_SUBLET+",1,"+u.BIAYA_SUBLET+",0," +
                            ""+u.BIAYA_SUBLET+","+(u.BIAYA_SUBLET * 0.1)+",0,0\n"
                }

                if(u.BIAYA_ADM!=0)
                {
                    result += OF+",06,BIAYA ADMINISTRASI,"+u.BIAYA_ADM+",1,"+u.BIAYA_ADM+",0," +
                            ""+u.BIAYA_ADM+","+(u.BIAYA_ADM * 0.1)+",0,0\n"
                }
            }
        }
        render(contentType: 'text/csv',text: result)
    }
}
