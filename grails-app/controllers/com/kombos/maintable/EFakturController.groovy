package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.GeneralParameter
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Reception
import grails.converters.JSON
import groovy.sql.Sql
import org.apache.commons.io.FileUtils
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.h2.engine.Procedure
import org.hibernate.criterion.CriteriaSpecification
import org.hsqldb.jdbc.jdbcDataSource
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import javax.sql.DataSource
import javax.xml.crypto.Data
import java.sql.CallableStatement
import java.sql.DatabaseMetaData
import java.sql.PreparedStatement
import java.text.SimpleDateFormat

class EFakturController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
//    @Procedure(actionName="E_FAKTUR")
    def koneksiToOracleService
    def dataSource
    def datatablesUtilService
    def sessionFactory
    def EFakturService
    def jasperService

    def excelImportService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {

    }

    def uploadData() {
        [tanggal : new Date()]
    }

    def list(Integer max) {
        [docNumber : params.docNumber]
    }

    def edit(Long id) {
        def EFakturInstance = EFakturMasukan.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'TESTER'), id])
            redirect(action: "list")
            return
        }

        [EFakturInstance: EFakturInstance]
    }

    def update(Long id, Long version) {
        def EFakturInstance = EFakturMasukan.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'TESTER2'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (EFakturInstance.version > version) {

                EFakturInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'bank.label', default: 'Bank')] as Object[],
                        "Another user has updated this Bank while you were editing")
                render(view: "edit", model: [bankInstance: EFakturInstance])
                return
            }
        }
        EFakturInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        EFakturInstance?.setLastUpdProcess("UPDATE")
        EFakturInstance.properties = params
        EFakturInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if (!EFakturInstance.save(flush: true)) {
            render(view: "edit", model: [EFakturInstance: EFakturInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'bank.label', default: 'Bank'), EFakturInstance.id])
        redirect(action: "show", id: EFakturInstance.id)
    }

    def show(Long id) {
        def EFakturInstance = EFakturMasukan.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "list")
            return
        }

        [EFakturInstance: EFakturInstance]
    }

    def delete(Long id) {
        def EFakturInstance = EFakturMasukan.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "list")
            return
        }

        try {
            EFakturInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            EFakturInstance?.setLastUpdProcess("DELETE")
            EFakturInstance?.lastUpdated = datatablesUtilService?.syncTime()
            EFakturInstance?.setStaDel('1')
            EFakturInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(EFakturMasukan, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }



    def datatablesList() {
        session.exportParams = params
        params.userCompanyDealer = session?.userCompanyDealer
        render EFakturService.datatablesList(params) as JSON
    }


    def datatablesList1() {
        session.exportParams = params
        params.userCompanyDealer = session?.userCompanyDealer
        render EFakturService.datatablesList1(params) as JSON
    }


    def view() {
        def date = datatablesUtilService.syncTime()
        session["ppnMasukan"] = "";
        def cek = EFakturMasukan.createCriteria()
        def ppnMasukan = cek.list() {
            eq("companyDealer",session.userCompanyDealer)
            ge("dateCreated",date.clearTime())
            lt("dateCreated",date.clearTime() + 1)
            eq("staDel",'0')
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
            }
        }
        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'FM',
                        'B':'KD_JENIS_TRANSAKSI',
                        'C':'FG_PENGGANTI',
                        'D':'NOMOR_FAKTUR',
                        'E':'MASA_PAJAK',
                        'F':'TAHUN_PAJAK',
                        'G':'TANGGAL_FAKTUR',
                        'H':'NPWP',
                        'I':'NAMA',
                        'J':'ALAMAT_LENGKAP',
                        'K':'JUMLAH_DPP',
                        'L':'JUMLAH_PPN',
                        'M':'JUMLAH_PPNBM',
                        'N':'IS_CREDITABLE'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }

        def jsonData = []
        def tanggal = ""
        int jmlhDataError = 0;
        String htmlData = "",status2 = "", statusReplace = ""
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/vnd-ms-excel','application/octet-stream','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]

            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "uploadData")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            String status = "0", style = "color:black;", flag = "black"
            int nomor = 0

            jobList.each {
                nomor++
                style = "color:black;"
                flag = "black"
                // Format dokumen yang TIDAK boleh kosong Di isi
                if (    it.FM ==""|| it.FM ==null ||
                        it.NOMOR_FAKTUR ==""|| it.NOMOR_FAKTUR ==null ||
                        it.JUMLAH_DPP ==""|| it.JUMLAH_DPP ==null ||
                        it.JUMLAH_PPN ==""|| it.JUMLAH_PPN ==null ||
                        it.JUMLAH_PPNBM ==""|| it.JUMLAH_PPNBM ==null ||
                        it.ALAMAT_LENGKAP ==""|| it.ALAMAT_LENGKAP ==null ||
                        it.KD_JENIS_TRANSAKSI ==""|| it.KD_JENIS_TRANSAKSI ==null ||
                        it.IS_CREDITABLE ==""|| it.IS_CREDITABLE ==null
                ){
                    style = "color:red;"
                    flag =  "red"
                    jmlhDataError++
                }

                try {
                    def datet  = new Date().parse("yyyy-MM-dd", (it.TANGGAL_FAKTUR).toString())
                }catch(Exception e){
                    style = "color:red;"
                    flag =  "red"
                    tanggal = ", Ada tanggal yang salah, harusnya (yyyy-MM-dd)"
                    jmlhDataError++

                }
                def id = "'adaFK'"
                if (it.FM ==""|| it.FM ==null){
                    it.FM = " "
                    id = "'noFK'"
                }
                htmlData+="<tr id="+id+" style='"+style+"'>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+nomor+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+">\n" +
                        "                                "+it.FM+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+it.KD_JENIS_TRANSAKSI +"\n" +
                        "                            </td>\n" +
                        "                           <td id="+id+" >\n" +
                        "                                "+it.FG_PENGGANTI+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+it.NOMOR_FAKTUR+"\n" +
                        "                            </td>\n" +
                        "                             <td id="+id+" >\n" +
                        "                                "+it.MASA_PAJAK+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+it.TAHUN_PAJAK+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+ it.TANGGAL_FAKTUR+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+it.NPWP+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+it.NAMA+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+it.ALAMAT_LENGKAP+"\n" +
                        "                            </td>\n" +
                        "                             <td id="+id+" >\n" +
                        "                                "+it.JUMLAH_DPP+"\n" +
                        "                            </td>\n" +
                        "                             <td id="+id+" >\n" +
                        "                                "+it.JUMLAH_PPN+"\n" +
                        "                            </td>\n" +
                        "                           <td id="+id+" >\n" +
                        "                                "+it.JUMLAH_PPNBM+"\n" +
                        "                            </td>\n" +
                        "                            <td id="+id+" >\n" +
                        "                                "+it.IS_CREDITABLE+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"


                status = "0"
                jsonData << [
                        FM : it?.FM ,
                        KD_JENIS_TRANSAKSI : it?.KD_JENIS_TRANSAKSI,
                        FG_PENGGANTI : it?.FG_PENGGANTI,
                        NOMOR_FAKTUR : it?.NOMOR_FAKTUR,
                        MASA_PAJAK : it?.MASA_PAJAK,
                        TAHUN_PAJAK : it?.TAHUN_PAJAK,
                        TANGGAL_FAKTUR : it.TANGGAL_FAKTUR,
                        NPWP : it?.NPWP,
                        NAMA : it?.NAMA,
                        ALAMAT_LENGKAP : it.ALAMAT_LENGKAP,
                        JUMLAH_DPP : it?.JUMLAH_DPP,
                        JUMLAH_PPN : it?.JUMLAH_PPN,
                        JUMLAH_PPNBM : it?.JUMLAH_PPNBM,
                        IS_CREDITABLE : it?.IS_CREDITABLE,
                        flag : flag
                ]

            }


        }
        if (jsonData.size()==0){
            flash.message = message(code: 'default.uploadGoods.message', default: "File Tidak dapat diproses, wajib Format File .xls")
        }else if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah " + tanggal)
        }
        else {
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
        }
        session["ppnMasukan"] = jsonData
        render(view: "uploadData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError, tanggal : params.tanggalUpload, jumJson : jsonData.size()])
    }


    def save() {

    }
    def saveApproval(){
        def data = JSON.parse(params.arr)
        def pesan = [:]
        data.each {
            def faktur = EFakturMasukan.get(it.dataid as Long)
            faktur.setStaAprove(it.staApprove.toString())
            faktur.save(flush: true)
        }
        render pesan as JSON
    }

    def showDetail() {
        [dataFakturDetail : EFakturMasukan.findAllByDocNumberAndStaDelAndJenisInputanAndStaAprove(params.docNumber,"0","PPN_MASUKAN","0",[sort: 'id',order: 'asc'])]
    }

    def printEfaktur(){
        List<JasperReportDef> reportDefList = []
        params.userCompanyDealer = session?.userCompanyDealer
        reportDefList = EFakturService.printEfaktur(params)
        def file = File.createTempFile("DMS_MASUKAN_" + params.docNumber + "_",".csv")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "text/csv")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def upload(){
        def date = datatablesUtilService.syncTime()

        def requestBody = JSON.parse(params.sendData)
        def tanggal= Date.parse("dd/MM/yyyy",params.tanggal)


        def EFakturInstance = null
        def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
        def sdf = new SimpleDateFormat("yyyyMMdd")
        String docNumber = com+".IN."+sdf.format(tanggal)
        requestBody.each{
            EFakturInstance = new EFakturMasukan()
            String FM = it?.FM.toString()
            String KD_JENIS_TRANSAKSI = it?.KD_JENIS_TRANSAKSI.toString()
            String FG_PENGGANTI = it?.FG_PENGGANTI.toString()
            String NOMOR_FAKTUR = it?.NOMOR_FAKTUR.toString()
            String MASA_PAJAK = it?.MASA_PAJAK.toString()
            String TAHUN_PAJAK = it?.TAHUN_PAJAK.toString()
            def TANGGAL_FAKTUR = new Date().parse("yyyy-MM-dd",it?.TANGGAL_FAKTUR)
            String NPWP = it?.NPWP.toString()
            String NAMA = it?.NAMA.toString()
            String NPWP2 = it?.NPWP2.toString()
            String NAMA2 = it?.NAMA2.toString()
            String ALAMAT_LENGKAP = it?.ALAMAT_LENGKAP.toString()
            def JUMLAH_DPP = it?.JUMLAH_DPP.toBigDecimal()
            def JUMLAH_PPN = it?.JUMLAH_PPN.toBigDecimal()
            def JUMLAH_PPNBM = it?.JUMLAH_PPNBM.toBigDecimal()
            String IS_CREDITABLE = it?.IS_CREDITABLE.toString()


            //save Data
            EFakturInstance.companyDealer = session?.userCompanyDealer
            EFakturInstance.FM = FM
            EFakturInstance.KD_JENIS_TRANSAKSI = KD_JENIS_TRANSAKSI
            EFakturInstance.FG_PENGGANTI = FG_PENGGANTI
            EFakturInstance.NOMOR_FAKTUR = NOMOR_FAKTUR
            EFakturInstance.MASA_PAJAK = MASA_PAJAK
            EFakturInstance.TAHUN_PAJAK = TAHUN_PAJAK
            EFakturInstance.TANGGAL_FAKTUR = TANGGAL_FAKTUR
            EFakturInstance.NPWP = NPWP
            EFakturInstance.NAMA = NAMA
            EFakturInstance.ALAMAT_LENGKAP = ALAMAT_LENGKAP
            EFakturInstance.JUMLAH_DPP = JUMLAH_DPP ? JUMLAH_DPP:0
            EFakturInstance.JUMLAH_PPN = JUMLAH_PPN ? JUMLAH_PPN:0
            EFakturInstance.JUMLAH_PPNBM = JUMLAH_PPNBM ? JUMLAH_PPNBM:0
            EFakturInstance.IS_CREDITABLE = IS_CREDITABLE
            EFakturInstance.staAprove = '0'
            EFakturInstance.staDel = '0'
            EFakturInstance.staOriRev = '0'
            EFakturInstance.docNumber = docNumber
            EFakturInstance.tanggal = tanggal
            EFakturInstance.dateCreated = datatablesUtilService?.syncTime()
            EFakturInstance.lastUpdated = datatablesUtilService?.syncTime()
            EFakturInstance.lastUpdProcess = 'INSERT'
            EFakturInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            EFakturInstance.jenisInputan = 'PPN_MASUKAN'
            EFakturInstance.save(flush: true)
            EFakturInstance.errors.each {
                println it
            }

            flash.message = message(code: 'default.uploadGoods.message', default: "Save Data Sukses")
        }

        render(view: "uploadData", model: [woTransactionInstance: EFakturInstance, tanggal : tanggal])

    }

    def exportToExcel(){

        List<JasperReportDef> reportDefList = []
        params.dataExcel = session["ppnMasukan"]
        def reportData = exportToExcelDetail(params) //komen ini jangan dulu di hapus

        def reportDef = new JasperReportDef(name:'reportEfaktur.jasper',
                fileFormat:JasperExportFormat.XLS_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("EFaktur_ppnMasukan",".xls")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }

    def exportToExcelDetail(def params){
        def jsonArray = params.dataExcel
        def reportData = new ArrayList();
        jsonArray.each {
            def data = [:]
            data.put("COLUMN","FK")
            data.put("COLUMN_1","KD_JENIS_TRANSAKSI")
            data.put("COLUMN_2","FG_PENGGANTI")
            data.put("COLUMN_3","NOMOR_FAKTUR")
            data.put("COLUMN_4","MASA_PAJAK")
            data.put("COLUMN_5","TAHUN_PAJAK")
            data.put("COLUMN_6","TANGGAL_FAKTUR")
            data.put("COLUMN_7","NPWP")
            data.put("COLUMN_8","NAMA")
            data.put("COLUMN_9","ALAMAT_LENGKAP")
            data.put("COLUMN_10","JUMLAH_DPP")
            data.put("COLUMN_11","JUMLAH_PPN")
            data.put("COLUMN_12","JUMLAH_PPNBM")
            data.put("COLUMN_13","ID_KETERANGAN_TAMBAHAN")
            data.put("COLUMN_14","FG_UANG_MUKA")
            data.put("COLUMN_15","UANG_MUKA_DPP")
            data.put("COLUMN_16","UANG_MUKA_PPN")
            data.put("COLUMN_17","UANG_MUKA_PPNBM")
            data.put("COLUMN_18","REFERENSI")
            data.put("COLUMN_19","LT")
            data.put("COLUMN_20","NPWP2") // MENUNJUKAN NPWP2
            data.put("COLUMN_21","NAMA2") // MENUNJUKKAN NAMA2
            data.put("COLUMN_22","JALAN")
            data.put("COLUMN_23","BLOK")
            data.put("COLUMN_24","NOMOR")
            data.put("COLUMN_25","RT")
            data.put("COLUMN_26","RW")
            data.put("COLUMN_27","KECAMATAN")
            data.put("COLUMN_28","KELURAHAN")
            data.put("COLUMN_29","KABUPATEN")
            data.put("COLUMN_30","PROPINSI")
            data.put("COLUMN_31","KODE_POS")
            data.put("COLUMN_32","NOMOR_TELEPON")
            data.put("COLUMN_33","OF")
            data.put("COLUMN_34","KODE_OBJEK")
            data.put("COLUMN_35","NAMA_OBJEK")
            data.put("COLUMN_36","HARGA_SATUAN")
            data.put("COLUMN_37","JUMLAH_BARANG")
            data.put("COLUMN_38","HARGA_TOTAL")
            data.put("COLUMN_39","DISKON")
            data.put("COLUMN_40","DPP")
            data.put("COLUMN_41","PPN")
            data.put("COLUMN_42","TARIF_PPNBM")
            data.put("COLUMN_43","PPNBM")

            data.put("FLAG_COLOR",it.flag)
            data.put("F_COLUMN",it.FK)
            data.put("F_COLUMN_1",it.KD_JENIS_TRANSAKSI)
            data.put("F_COLUMN_2",it.FG_PENGGANTI)
            data.put("F_COLUMN_3",it.NOMOR_FAKTUR)
            data.put("F_COLUMN_4",it.MASA_PAJAK)
            data.put("F_COLUMN_5",it.TAHUN_PAJAK)
            data.put("F_COLUMN_6",it.TANGGAL_FAKTUR)
            data.put("F_COLUMN_7",it.NPWP)
            data.put("F_COLUMN_8",it.NAMA_OBJEK)
            data.put("F_COLUMN_9",it.ALAMAT_LENGKAP)
            data.put("F_COLUMN_10",it.JUMLAH_DPP)
            data.put("F_COLUMN_11",it.JUMLAH_PPN)
            data.put("F_COLUMN_12",it.JUMLAH_PPNBM)
            data.put("F_COLUMN_13",it.ID_KETERANGAN_TAMBAHAN)
            data.put("F_COLUMN_14",it.FG_UANG_MUKA)
            data.put("F_COLUMN_15",it.UANG_MUKA_DPP)
            data.put("F_COLUMN_16",it.UANG_MUKA_PPN)
            data.put("F_COLUMN_17",it.UANG_MUKA_PPNBM)
            data.put("F_COLUMN_18",it.REFERENSI)
            data.put("F_COLUMN_19",it.LT)
            data.put("F_COLUMN_20",it.NPWP2)
            data.put("F_COLUMN_21",it.NAMA2)
            data.put("F_COLUMN_22",it.JALAN)
            data.put("F_COLUMN_23",it.BLOK)
            data.put("F_COLUMN_24",it.NOMOR)
            data.put("F_COLUMN_25",it.RT)
            data.put("F_COLUMN_26",it.RW)
            data.put("F_COLUMN_27",it.KECAMATAN)
            data.put("F_COLUMN_28",it.KELURAHAN)
            data.put("F_COLUMN_29",it.KABUPATEN)
            data.put("F_COLUMN_30",it.PROPINSI)
            data.put("F_COLUMN_31",it.KODE_POS)
            data.put("F_COLUMN_32",it.NOMOR_TELEPON)
            data.put("F_COLUMN_33",it.OF_)
            data.put("F_COLUMN_34",it.KODE_OBJEK)
            data.put("F_COLUMN_35",it.NAMA)
            data.put("F_COLUMN_36",it.HARGA_SATUAN)
            data.put("F_COLUMN_37",it.JUMLAH_BARANG)
            data.put("F_COLUMN_38",it.HARGA_TOTAL)
            data.put("F_COLUMN_39",it.DISKON)
            data.put("F_COLUMN_40",it.DPP)
            data.put("F_COLUMN_41",it.PPN)
            data.put("F_COLUMN_42",it.TARIF_PPNBM)
            data.put("F_COLUMN_43",it.PPNBM)

            reportData.add(data)
        }

        return reportData
    }

    def generateNofak (){
        def data = JSON.parse(params.arr)
        def pesan = [:]
        data.each {
            def idfaktur = EFaktur.get(it.dataid as Long)
            Sql sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
            def query = "call GENERATE_NOFAKTUR("+it.dataid+")";
            sql.call(query)
            sql.commit();
            idfaktur.save(flush: true)
        }
        render pesan as JSON
    }


    def generateData (){
        def startTgl = params.tgl1.toString()
        def endTgl = params.tgl2.toString()
        def comp = session?.userCompanyDealerId
        def jenisInputan = 'PPN_MASUKAN';
        def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID

        Date date1 = new Date().parse("dd-MM-yyyy",startTgl)
        Date date2 = new Date().parse("dd-MM-yyyy",endTgl)
        Date date3 = date1
        def selisih = date2.date - date1.date
        date2.date+=1

        (1..(selisih+1)).each {
            String docNumber = com+".OUT."+date3.format("yyyyMMdd")
            startTgl = date3.format("dd-MM-yyyy")
            date3.date+=1
            endTgl = date3.format("dd-MM-yyyy")
            Sql sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
            Sql sql2 = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
            def query = "call E_FAKTUR('"+docNumber+"','"+jenisInputan+"','"+startTgl+"', '"+endTgl+"',"+comp+")";
            def query2 = "call EFAKTUR_PJL_LGSG('"+docNumber+"','"+jenisInputan+"','"+startTgl+"', '"+endTgl+"',"+comp+")";
            sql.call(query)
            sql2.call(query2)
            sql.commit();
        }

    }

    def exportCSV={
        def jsonArray = JSON.parse(params.docNumber)
        Date today = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
        String date = DATE_FORMAT.format(today);
        def results=[]
        jsonArray.each {
            results << it
        }
        def docNumber = null
        results.each {
            docNumber = it
        }

        response.setHeader("Content-disposition","attachment; filename=DMS_PPNMasukan"+date+".csv")

        params.userCompanyDealer = session?.userCompanyDealer

        def efaktur = EFakturMasukan.createCriteria().list {
            eq("staDel","0")
            eq("jenisInputan","PPN_MASUKAN")
            eq("staAprove","1")
            inList("docNumber", results)
        }

        def result='FM, KD_JENIS_TRANSAKSI, FG_PENGGANTI, NOMOR_FAKTUR, MASA_PAJAK, TAHUN_PAJAK, TANGGAL_FAKTUR, NPWP, NAMA, ALAMAT_LENGKAP, JUMLAH_DPP, JUMLAH_PPN, JUMLAH_PPNBM, IS_CREDITABLE\n'
        efaktur.each {u ->

            def npwp = u.NPWP
            if (npwp==null || npwp==""){
                npwp = "000000000000000"
            }
            if (npwp=="00.000.000.0-000.000"){
                npwp = "000000000000000"
            }

            def NAMA = u.NAMA
            if (NAMA==null){
                NAMA = ""
            }
            def ALAMAT_LENGKAP = u.ALAMAT_LENGKAP
            if ((ALAMAT_LENGKAP=="" || ALAMAT_LENGKAP==null || !ALAMAT_LENGKAP)){
                ALAMAT_LENGKAP =" "
            }


            def TANGGAL_FAKTUR = u.TANGGAL_FAKTUR?.format("dd/MM/yyyy")
            result += u.FM+","+ u.KD_JENIS_TRANSAKSI+","+ u.FG_PENGGANTI+","+ u.NOMOR_FAKTUR+","+ u.MASA_PAJAK+","+ u.TAHUN_PAJAK+","+ TANGGAL_FAKTUR+","+ // baris pertama
                      npwp+","+ NAMA+","+ ALAMAT_LENGKAP+","+ u.JUMLAH_DPP+","+ u.JUMLAH_PPN+","+ u.JUMLAH_PPNBM+","+ u.IS_CREDITABLE+","+"\n"
        }
        render(contentType: 'text/csv',text: result)
    }

    def massDeleteNew() {
        def hasil = "ok"
        int jum = 0
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {

            EFakturMasukan.findAllByDocNumber(it as String)*.delete()

        }
        render hasil
    }
}

