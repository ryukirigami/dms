package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import grails.converters.JSON
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import org.apache.commons.io.FileUtils
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat

class PartsTransactionController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def excelImportService

	def jasperService

	def datatablesUtilService

	def sessionFactory;

	def partsTransactionService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = ['index', 'list', 'datatablesList']

	static addPermissions = ['create', 'save']

	static editPermissions = ['edit', 'update']

	static deletePermissions = ['delete']

	def index() {

	}

	def uploadData() {
        def jsonData = []
        [tanggal : new Date(),jsonData:jsonData as JSON,jumJson:0]
	}

	def generateData() {
        def jsonData = []
        [tanggal : new Date(),jsonData:jsonData as JSON,jumJson:0]
	}

	def datatablesList() {
		session.exportParams = params
		params.userCompanyDealer = session.userCompanyDealer
		render partsTransactionService.datatablesList(params) as JSON
	}

	def viewGenerate(){
        session["partTrx"] = "";
        def tanggal = Date.parse("dd/MM/yyyy",params.tanggal)
        def cek = PartsTransaction.createCriteria()
        def partTrx = cek.list() {
            eq("companyDealer",session.userCompanyDealer)
            ge("tglUpload",tanggal)
            lt("tglUpload",tanggal  + 1)
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
            }
        }
		def tanggal2 = tanggal
		String tgl1 = tanggal.format("dd-MM-yyyy")
		String tgl2 = (tanggal2 + 1).format("dd-MM-yyyy")
		def htmlData = ""
        def jmlhDataError = 0
		def jsonData = []

		final sessions = sessionFactory.currentSession
        String query=" SELECT  'HA','','"+session.userCompanyDealer.m011OutletCode+"',A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'YYYY-MM-DD') AS TGL_WO, C.M111_ID, C.M111_NAMA," +
                " C.M111_ID AS PART_NO_SUPPLIED, C.M111_NAMA AS PART_DESC_SUPPLIED," +
                " TO_CHAR(B.DATE_CREATED,'YYYY-MM-DD') AS REQUEST_DATE, B.T403_JUMLAH1 AS REQUEST_QTY," +
                " F.T142_QTY1,'0',M118_SATUAN1,'1','2'" +
                " FROM T401_RECEPTION A" +
                " INNER JOIN T403_PARTSRCP B ON A.ID = B.T403_T401_NOWO" +
                " INNER JOIN M111_GOODS C ON B.T403_M111_ID = C.ID" +
                " INNER JOIN M118_SATUAN D ON C.M111_M118_ID = D.ID" +
                " INNER JOIN T141_PICKINGSLIP E ON A.ID = E.T141_T401_NOWO" +
                " INNER JOIN T142_PICKINGSLIPDETAIL F ON E.ID = F.T142_T141_ID" +
                " WHERE A.COMPANY_DEALER_ID = "+session.userCompanyDealerId+" AND F.T142_M111_ID=B.T403_M111_ID" +
                " AND A.COMPANY_DEALER_ID = "+session.userCompanyDealerId+" AND A.T401_STADEL='0' AND B.T403_STADEL = '0'" +
                " AND E.T141_STADEL ='0' " +
                " AND A.T401_TANGGALWO BETWEEN  TO_DATE('"+tgl1+"','DD-MM-YYYY') AND TO_DATE('"+tgl2+"','DD-MM-YYYY') "+
                " ORDER BY A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'DD-MM-YYYY')";

        def results = null
		try {
			final sqlQuery = sessions.createSQLQuery(query)
			final queryResults = sqlQuery.with {
				list()
			}
			def nomor = 0


			def style = ""

			def dealerCode = ""
			def areaCode= ""
			def outletCode= ""
			def noWo= ""
			def tglWo= ""
			def partNoReq= ""
			def partDesReq= ""
			def partNoSup= ""
			def partDesSup= ""
			def nonToyotaOil= "0"
			def reqDate= ""
			def reqQty= ""
			def qty= ""
			def cancelQty= ""
			def satuan= ""

			def fillRate= ""
			def rate= ""
			results = queryResults.collect
					{ resultRow ->
						dealerCode = resultRow[0]
						areaCode= "0"
						outletCode= resultRow[2]
						noWo= resultRow[3]
						tglWo= resultRow[4]
						partNoReq= resultRow[5]
						partDesReq= resultRow[6]
						partNoSup= resultRow[7]
						partDesSup= resultRow[8]
						reqDate= resultRow[9]
						reqQty= resultRow[10]
						qty= resultRow[11]
						cancelQty= "0"
                        nonToyotaOil= "-"
						satuan= resultRow[13]
						fillRate= "100%"
						rate= "100%"
						nomor++

						htmlData+="<tr "+style+">\n" +
								"                            <td>\n" +
								"                                "+nomor+"\n" +
								"                             </td>\n" +
								"                            <td>\n" +
								"                                "+dealerCode+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+areaCode+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+outletCode+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+noWo+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+tglWo+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+partNoReq+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+partDesReq+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+partNoSup+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+partDesSup+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+nonToyotaOil+"\n" +
								"                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+reqDate+"\n" +
                                "                            </td>\n" +
								"                            <td>\n" +
								"                                "+reqQty+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+qty+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+cancelQty+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+satuan+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+fillRate+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+rate+"\n" +
								"                            </td>\n" +
								"                        </tr>"

						jsonData << [
								dealerCode : dealerCode,
								areaCode : areaCode,
								outletCode : outletCode,
								noWo : noWo,
								tglWo : tglWo,
								partNoReq : partNoReq,
								partDesReq : partDesReq,
								partNoSup : partNoSup,
								partDesSup : partDesSup,
								nonToyotaOil : nonToyotaOil,
								reqDate : reqDate,
								reqQty : reqQty,
								fileName : "Generate",
								qty : qty,
								cancelQty : cancelQty,
								satuan : satuan,
								fillRate : fillRate,
								rate : rate,
								flag : "BLACK"
						]
					}

		}catch(Exception e){
                println e
		}
        session["partTrx"] = jsonData
        def jumJson = jsonData.size()
        if (jsonData.size()==0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Data Tidak dapat diproses")
        }else if(partTrx.size()>0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Hari Ini Sudah Upload Sebanyak " + partTrx.size() + " Kali")
        }else{
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done.")
        }
		render(view: "generateData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError,tanggal : tanggal,jumJson : jsonData.size()])
	}
	def view() {
		def tanggal = Date.parse("dd/MM/yyyy",params.tanggalUpload_dp)
		session["partTrx"] = "";
		def cek = PartsTransaction.createCriteria()
		def partTrx = cek.list() {
			eq("companyDealer",session.userCompanyDealer)
			ge("tglUpload",tanggal)
			lt("tglUpload",tanggal  + 1)
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
				groupProperty("docNumber", "docNumber")
			}
		}
		//handle upload file
		Map CONFIG_JOB_COLUMN_MAP = [
				sheet:'Sheet1',
				startRow: 1,
				columnMap:  [
						//Col, Map-Key
						'A':'dealerCode',
						'B':'areaCode',
						'C':'outletCode',
						'D':'noWo',
						'E':'tglWo',
						'F':'partNoReq',
						'G':'partDesReq',
						'H':'partNoSup',
						'I':'partDesSup',
						'J':'nonToyotaOil',
						'K':'reqDate',
						'L':'reqQty',
						'M':'qty',
						'N':'cancelQty',
						'O':'satuan',
						'P':'fillRate',
						'Q':'rate'
				]
		]

		CommonsMultipartFile uploadExcel = null
		if(request instanceof MultipartHttpServletRequest){
			MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
			uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
		}
		def jsonData = []
		int jmlhDataError = 0;
        def isiEror = ""
		String htmlData = "",status2 = "", statusReplace = ""
		if(!uploadExcel?.empty){
			//validate content type
			def okcontents = [
					'application/excel','application/vnd.ms-excel','application/octet-stream','application/vnd-ms-excel'
			]
			if (!okcontents.contains(uploadExcel?.getContentType())) {
				flash.message = "Illegal Content Type"
				uploadExcel = null
				render(view: "uploadData")
				return
			}

			Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
			//Iterate through bookList and create/persists your domain instances
			def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

			String status = "0", style = "style='color:black'", flag = "black"
			int nomor = 0

			jobList?.each {
				nomor++
				style = "style='color:black'"
				flag = "black"
                if(it.nonToyotaOil == null || it.nonToyotaOil == ""){
                    it.nonToyotaOil = "-"
                }
                try {
                    it.outletCode = it.outletCode.toInteger().toString()
                    if(it.outletCode.toInteger()<10){
                        it.outletCode = "0" + it.outletCode.toInteger().toString()
                    }
                    it.cancelQty = it.cancelQty.toInteger().toString()
                    it.areaCode = it.areaCode.toInteger().toString()
                }catch (Exception e){
                }

                try {
                    it.fillRate = (it.fillRate.toInteger() * 100) + "%"
                    it.rate = (it.rate.toInteger() * 100) + "%"
                }catch (Exception e){
                }

                if(it.areaCode=="" || it.areaCode=="-" || it.areaCode==null){
                    it.areaCode = "0"
                }else{
                    try {
                        it.areaCode = it.areaCode.toInteger().toString()
                    }catch (Exception e){
                    }
                }

				if(it.dealerCode==null || it.dealerCode=="" || it.outletCode==null || it.outletCode=="" ||
						it.noWo==null || it.noWo=="" ||  it.tglWo==null || it.tglWo=="" || it.partNoReq==null || it.partNoReq=="" ||
						it.partDesReq==null || it.partDesReq=="" ||  it.partNoSup==null || it.partNoSup=="" || it.partDesSup==null || it.partDesSup=="" ||
						it.reqDate==null || it.reqDate=="" ||  it.reqQty==null || it.reqQty=="" ||
						it.qty==null || it.qty=="" ||  it.cancelQty==null || it.cancelQty=="" ||  it.satuan==null || it.satuan=="" ||
						it.fillRate==null || it.fillRate=="" ||  it.rate==null || it.rate==""
				){
					style = "style='color:red'"
					flag =  "red"
					jmlhDataError++
                    isiEror += "*BARIS Ke- " + nomor + " : Ada yang Null <br>"
				}

                if(it.dealerCode!="HA"){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS Ke- " + nomor + " : Dealer Code hanya boleh diisi 'HA'<br>"
                    jmlhDataError++
                }
                if(it.outletCode.toString() != session.userCompanyDealer.m011OutletCode){
					style = "style='color:red'"
					flag =  "red"
                    isiEror += "*BARIS Ke- " + nomor + " : outletCode harus diisi "+session.userCompanyDealer.m011OutletCode+" <br>"
					jmlhDataError++
				}
                if(it.areaCode!="0"){
                    style = "style='color:red'"
                    flag =  "red"
                    jmlhDataError++
                    isiEror += "*BARIS ke-"+nomor+" : AreaCode hanya boleh disi '0'<br>"
                }
				try {
					Date tglWO = Date.parse("yyyy-MM-dd",it.tglWo.toString())
					Date dateReq = Date.parse("yyyy-MM-dd",it.reqDate.toString())
				}catch (Exception e){
					style = "style='color:red'"
					flag =  "red"
                    isiEror += "*BARIS Ke- " + nomor + " : Format Tanggal (31/12/2016) <br>"
					jmlhDataError++
				}

				htmlData+="<tr "+style+">\n" +
						"                            <td>\n" +
						"                                "+nomor+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.dealerCode+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.areaCode+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.outletCode+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.noWo+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.tglWo+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.partNoReq+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.partDesReq+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.partNoSup+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.partDesSup+"\n" +
						"                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.nonToyotaOil+"\n" +
                        "                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.reqDate+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.reqQty+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.qty+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.cancelQty+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.satuan+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.fillRate+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.rate+"\n" +
						"                            </td>\n" +
						"                        </tr>"
				status = "0"
				jsonData << [
						dealerCode : it.dealerCode,
						areaCode : it.areaCode,
						outletCode : it.outletCode,
						noWo : it.noWo,
						tglWo : it.tglWo,
						partNoReq : it.partNoReq,
						partDesReq : it.partDesReq,
						partNoSup : it.partNoSup,
						partDesSup : it.partDesSup,
                        nonToyotaOil : it.nonToyotaOil,
						reqDate : it.reqDate,
						reqQty : it.reqQty,
						qty : it.qty,
						cancelQty : it.cancelQty,
						satuan : it.satuan,
						fillRate : it.fillRate,
						rate : it.rate,
						fileName : uploadExcel.getOriginalFilename(),
						flag : flag
				]

			}
		}

		 if (jsonData.size()==0){
			flash.message = message(code: 'default.uploadGoods.message', default: "File Tidak dapat diproses, Pastikan Ekstensi .xls & Nama Sheetnya 'Sheet1'")
		 }else if(jmlhDataError>0){
			flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah")
		 }else if(partTrx.size()>0){
			 flash.message = message(code: 'default.uploadGoods.message', default: "Hari Ini Sudah Upload Sebanyak " + partTrx.size() + " Kali")
		 }else {
			flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
		 }
		session["partTrx"] = jsonData
		render(view: "uploadData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError,tanggal : params.tanggalUpload, jumJson : jsonData.size(),isiEror:isiEror])
	}

	def upload() {
		def requestBody = JSON.parse(params.sendData)
		def tanggal= Date.parse("dd/MM/yyyy",params.tanggal)
		def partsTransactionInstance = null
		def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
		def sdf = new SimpleDateFormat("yyyyMMdd")
		String docNumber = "HA_"+com+".PART."+sdf.format(tanggal)
		def cek = PartsTransaction.createCriteria()
		def partTrx = cek.list() {
			eq("companyDealer",session.userCompanyDealer)
			ge("tglUpload",tanggal)
			lt("tglUpload",tanggal + 1)
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
				groupProperty("docNumber", "docNumber")
			}
		}
		String addText = ""
		if(partTrx.size()>0){
            def cekDelete = PartsTransaction.createCriteria()
            def pTrxDelete = cekDelete.list() {
                eq("companyDealer",session.userCompanyDealer)
                ge("tglUpload",tanggal)
                lt("tglUpload",tanggal + 1)
                eq("staDel",'0')
            }
            pTrxDelete.each {
                def aD = PartsTransaction.get(it.id as Long)
                aD.staDel = '1'
                aD.save(flush: true)
            }
			addText = "_" + partTrx.size()
		}
		requestBody.each{
			partsTransactionInstance = new PartsTransaction()
			String dealerCode = it?.dealerCode.toString()
			String areaCode = it.areaCode.toString()
			String outletCode = it.outletCode.toString()
			String noWo = it.noWo.toString()
			String tglWo = it.tglWo.toString()
			String partNoReq = it.partNoReq.toString()
			String partDesReq = it.partDesReq.toString()
			String partNoSup = it.partNoSup.toString()
			String partDesSup = it.partDesSup.toString()
			String nonToyotaOil = it.nonToyotaOil.toString()
			String reqDate = it.reqDate.toString()
			String reqQty = it.reqQty.toString()
			String qty = it.qty.toString()
			String cancelQty = it.cancelQty.toString()
			String satuan = it.satuan.toString()
			String fillRate = it.fillRate.toString()
			String rate = it.rate.toString()
			String fileName = it.fileName.toString()

			//save Data
			partsTransactionInstance.companyDealer = session.userCompanyDealer
			partsTransactionInstance.dealerCode = dealerCode
			partsTransactionInstance.areaCode = "0"
			partsTransactionInstance.outletCode = outletCode
			partsTransactionInstance.noWo = noWo
			partsTransactionInstance.tanggalWo = tglWo
			partsTransactionInstance.partNoRequest = partNoReq
			partsTransactionInstance.partDescRequest = partDesReq
			partsTransactionInstance.partNoSupplied = partNoSup
			partsTransactionInstance.partDescSupplied = partDesSup
			partsTransactionInstance.nonToyotaOil = nonToyotaOil
			partsTransactionInstance.requestDate = reqDate
			partsTransactionInstance.requestQty = reqQty
			partsTransactionInstance.qty = qty
			partsTransactionInstance.cancelQty = cancelQty
			partsTransactionInstance.satuan  = satuan
			partsTransactionInstance.fillRate = fillRate
			partsTransactionInstance.rate  = rate
			partsTransactionInstance.staDel = '0'
			partsTransactionInstance.tglUpload = tanggal
			partsTransactionInstance.fileName = fileName
			partsTransactionInstance.docNumber = docNumber+""+addText
			partsTransactionInstance.dateCreated  = datatablesUtilService?.syncTime()
			partsTransactionInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			partsTransactionInstance.lastUpdated  = datatablesUtilService?.syncTime()
			partsTransactionInstance.lastUpdProcess  = 'INSERT'
			partsTransactionInstance.save(flush: true)
			partsTransactionInstance.errors.each {println it}

			flash.message = message(code: 'default.uploadGoods.message', default: "Save Job Done")
		}

		render(view: "uploadData", model: [partsTransactionInstance: partsTransactionInstance])

	}

	def exportToExcel(){

		List<JasperReportDef> reportDefList = []
		params.dataExcel = session["partTrx"]
		def reportData = exportToExcelDetail(params)

		def reportDef = new JasperReportDef(name:'partsTransaction.jasper',
				fileFormat:JasperExportFormat.XLS_FORMAT,
				reportData: reportData

		)
		reportDefList.add(reportDef)


		def file = File.createTempFile("partTrx_",".xls")
		file.deleteOnExit()

		FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

		response.setHeader("Content-Type", "application/vnd.ms-excel")
		response.setHeader("Content-disposition", "attachment;filename=${file.name}")
		response.outputStream << file.newInputStream()


	}
	def exportToExcelDetail(def params){
		def jsonArray = params.dataExcel
		def reportData = new ArrayList();
		jsonArray.each {

			def data = [:]
			data.put("DEALER_CODE",it.dealerCode.toString())
			data.put("AREA_CODE",it.areaCode.toString())
			data.put("OUTLET_CODE",it.outletCode.toString())
			data.put("NOWO",it.noWo.toString())
			data.put("TGLWO",it.tglWo.toString())

			data.put("PART_NO_REQUEST",it.partNoReq.toString())
			data.put("PART_DESC_REQUEST",it.partDesReq.toString())
			data.put("PART_NO_SUPPLIED",it.partNoSup.toString())
			data.put("PART_DESC_SUPPLIED",it.partDesSup.toString())
			data.put("NON_TOYOTA_OIL",it.nonToyotaOil.toString())
			data.put("REQUEST_DATE",it.reqDate.toString())
			data.put("REQUEST_QUANTITY",it.reqQty.toString())
			data.put("QUANTITY",it.qty.toString())
			data.put("CANCEL_QUANTITY",it.cancelQty.toString())
			data.put("SATUAN",it.satuan.toString())
			data.put("FILL_RATE",it.fillRate.toString())
			data.put("RATE",it.rate.toString())
			data.put("FLAG_COLOR",it.flag)
			reportData.add(data)
		}

		return reportData
	}

	def printPartTrx(){
		List<JasperReportDef> reportDefList = []
        def periode = "ALL"
        try {
            periode = new Date().parse("dd/MM/yyyy",params.tanggal as String).format("yyyyMMdd")
        }catch (Exception e){}

		def reportData = partsTransactionService.printPartTrx(params)

		def reportDef = new JasperReportDef(name:'partsTransaction2.jasper',
				fileFormat:JasperExportFormat.CSV_FORMAT,
				reportData: reportData
		)
		reportDefList.add(reportDef)

		def file = File.createTempFile("HA_PART_"+periode+"_",".txt")
		file.deleteOnExit()

		def bosData = new StringBuilder();
		def dataToArray = jasperService.generateReport(reportDefList).toString().split('\n')
        int count = 0
        int sizeArr = dataToArray.size()
		dataToArray.each {
            count++
			def fixedData = it.replaceAll(",", ";")
            fixedData = fixedData.replaceAll("#", ",")
			bosData.append(fixedData)
            if(count != sizeArr){
                bosData.append(System.getProperty("line.separator"))
            }
		}
		def gaga = bosData.toString()
		FileUtils.writeStringToFile(file, gaga)
		response.setHeader("Content-Type", "text/plain")
		response.setHeader("Content-disposition", "attachment;filename=${file.name}")
		response.outputStream << file.newInputStream()

	}
}
