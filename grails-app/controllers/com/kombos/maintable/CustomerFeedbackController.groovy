package com.kombos.maintable

import com.kombos.customerprofile.HistoryCustomer
import grails.converters.JSON

class CustomerFeedbackController {


    def statusPencarianFeedback = [
            [id: 1, nama: "SMS – Dibalas dan setuju booking"],
            [id: 2, nama: "SMS – Dibalas dan tidak setuju booking"],
            [id: 3, nama: "SMS – Dibalas dengan format tidak jelas"],
            [id: 4, nama: "SMS – Belum dibalas"],
            [id: 5, nama: "Telepon – Tersambung dan setuju booking"],
            [id: 6, nama: "Telepon – Tersambung dan tidak setuju booking"]
    ]

    def index() {

        [statusPencarianFeedback: statusPencarianFeedback]
    }

    def updateInvitation() {
        def invitation = Invitation.get(params.invitationID)
        invitation.t202StaLangsungAppointment = params.t202StaLangsungAppointment
        invitation.lastUpdated = datatablesUtilService?.syncTime()
        invitation.save(flush: true)

        def result = [status: "OK"]
        render result as JSON
    }


    def loadInvitation() {
        def invitation = Invitation.get(params.id)
        def customer = MappingCustVehicle.findByCustomerVehicle(invitation.customerVehicle, [sort: "id", order: "desc"])?.customer
        def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "id", order: "desc"])
        def result = [
                NamaCustomer: historyCustomer?.t182NamaDepan + " " + historyCustomer?.t182NamaBelakang,
                TanggalInvite: invitation?.t202TglJamKirim ? invitation?.t202TglJamKirim?.format("dd-MM-yyyy HH:mm") : "",
                TanggalFeedback: invitation?.t202TglFeedBack?.format("dd-MM-yyyy HH:mm"),
                MetodeInvitation: invitation?.jenisInvitation?.m204JenisInvitation,
                t202StaFeedBack: invitation?.t202StaFeedBack,
                balasanCustomer: invitation?.t202TextFeedBack,
                t202StaLangsungAppointment: invitation.t202StaLangsungAppointment,
                invitationID: invitation?.id
        ]

        render result as JSON
    }

    def datatablesList() {

        Date tanggalInvitationStart = params.tanggalInvitationStart
        Date tanggalInvitationEnd = params.tanggalInvitationEnd

        Date tanggalFeedbackStart = params.tanggalFeedbackStart
        Date tanggalFeedbackEnd = params.tanggalFeedbackEnd


        def searchReminder = [-1000L]
        if (params."searchReminder") {
            params?."searchReminder"?.toString()?.split(",")?.each {
                searchReminder.add(it?.trim()?.toLong())
            }
        }


        def searchMethod = [-1000L]
        if (params."searchMethod") {
            params?."searchMethod"?.toString()?.split(",")?.each {
                searchMethod.add(it?.trim()?.toLong())
            }
        }



        def status = [-1000L]
        if (params."status") {
            params?."status"?.toString()?.split(",")?.each {
                status.add(it?.trim()?.toLong())
            }
        }


        String method = params.method

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0


        def c = Invitation.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            reminder {
                m201ID {
                    inList("id", searchReminder)
                }
            }


            jenisInvitation {
                inList("id", searchMethod)
            }



            isNotNull("t202TglJamKirim")

            if (tanggalInvitationStart) {
                ge("t202TglJamKirim", tanggalInvitationStart)
            }
            if (tanggalInvitationEnd) {
                le("t202TglJamKirim", tanggalInvitationEnd)
            }

            if (tanggalFeedbackStart) {
                ge("t202TglFeedBack", tanggalFeedbackStart)
            }
            if (tanggalFeedbackEnd) {
                le("t202TglFeedBack", tanggalFeedbackEnd)
            }

            switch (sortProperty) {
                default:
                    order("id", "desc")
                    break;
            }
        }

        def rows = []

        results.each {

            def currentCondition = it.customerVehicle?.currentCondition;
            def customer = MappingCustVehicle.findByCustomerVehicle(currentCondition.customerVehicle, [sort: "id", order: "desc"])?.customer
            def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "id", order: "desc"])

            rows << [
                    id: it.id,
                    t202TglJamKirim: it.t202TglJamKirim ? it.t202TglJamKirim?.format("dd-MM-yyyy") : "",
                    customer: historyCustomer?.t182NamaDepan + " " + historyCustomer?.t182NamaBelakang,
                    nopol: currentCondition?.fullNoPol,
                    telp: historyCustomer?.t182NoTelpRumah,
                    hp: historyCustomer?.t182NoHp,
                    tglFeedback: it.t202TglFeedBack?.format("dd-MM-yyyy"),
                    tipe: it.reminder?.m201ID?.m201NamaJenisReminder,
                    metode: it.jenisInvitation?.m204JenisInvitation,
                    status: it.t202StaFeedBack,
                    app: it.t202StaLangsungAppointment == "1" ? "Ya" : "Tidak"
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
}
