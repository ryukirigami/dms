package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class HistoryApprovalController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def historyApprovalService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render historyApprovalService.datatablesList(params) as JSON
	}

	def create() {
		def result = historyApprovalService.create(params)

        if(!result.error)
            return [historyApprovalInstance: result.historyApprovalInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		 def result = historyApprovalService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["HistoryApproval", result.historyApprovalInstance.id])
            redirect(action:'show', id: result.historyApprovalInstance.id)
            return
        }

        render(view:'create', model:[historyApprovalInstance: result.historyApprovalInstance])
	}

	def show(Long id) {
		def result = historyApprovalService.show(params)

		if(!result.error)
			return [ historyApprovalInstance: result.historyApprovalInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = historyApprovalService.show(params)

		if(!result.error)
			return [ historyApprovalInstance: result.historyApprovalInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = historyApprovalService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["HistoryApproval", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[historyApprovalInstance: result.historyApprovalInstance.attach()])
	}

	def delete() {
		def result = historyApprovalService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["HistoryApproval", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(HistoryApproval, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
