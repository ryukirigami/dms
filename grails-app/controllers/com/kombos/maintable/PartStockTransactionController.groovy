package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class PartStockTransactionController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def excelImportService

	def jasperService

	def datatablesUtilService

	def sessionFactory;

	def partStockTransactionService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = ['index', 'list', 'datatablesList']

	static addPermissions = ['create', 'save']

	static editPermissions = ['edit', 'update']

	static deletePermissions = ['delete']

	def index() {

	}

	def uploadData() {
		def jsonData = []
		[tanggal : new Date(), jsonData:jsonData as JSON,jumJson:0]
	}

	def generateData() {
		def jsonData = []
		[tanggal : new Date(), jsonData:jsonData as JSON,jumJson:0]
	}

	def datatablesList() {
		session.exportParams = params
		params.userCompanyDealer = session.userCompanyDealer
		render partStockTransactionService.datatablesList(params) as JSON
	}
	def viewGenerate(){
		session["partStockTrx"] = "";
		def cek = PartStockTransaction.createCriteria()
		def partStockTrx = cek.list() {
			eq("companyDealer",session.userCompanyDealer)
			ge("tglUpload",tanggal)
			lt("tglUpload",tanggal  + 1)
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
				groupProperty("docNumber", "docNumber")
			}
		}

		def tanggal = Date.parse("dd/MM/yyyy",params.tanggal)
		def tanggal2 = tanggal
		String tgl1 = tanggal.format("dd-MM-yyyy")
		String tgl2 = (tanggal2 + 1).format("dd-MM-yyyy")
		def jmlhDataError = 0
		def htmlData = ""
		def jsonData = []

		final sessions = sessionFactory.currentSession
		String query=" SELECT 'HA','-','"+session.userCompanyDealer.m011OutletCode+"', A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'DD-MM-YYYY') AS TGL_WO," +
				" C.M116_ID||''|| B.T183_NOPOLTENGAH||''|| B.T183_NOPOLBELAKANG AS NOPOL," +
				" D.T103_VINCODE, M102_BASEMODEL.M102_NAMABASEMODEL AS VEHICLE_TYPE," +
				" DECODE(SUBSTR(F.M053_NAMAOPERATION,1,2),'EM',1,0) AS Is_EM," +
				" DECODE(SUBSTR(G.T400_NOMORANTRIAN,1,1),'W','WALK IN','BOOKING') AS WO_TYPE," +
				"DECODE(A.T401_M401_ID,1,'BP',2,'BP',3,'BP',4,'BP','GR') AS GRBP," +
				"DECODE(A.T401_M401_ID,1,'LIGHT REPAIR',2,'MEDIUM REPAIR',3,'HEAVY REPAIR',4,'MINOR','-') AS DAMAGE_LEVEL," +
				" F.M053_NAMAOPERATION, E.T402_RATE, NVL(A.T401_STATWC,0)" +
				" FROM T401_RECEPTION A" +
				" INNER JOIN T183_HISTORYCUSTOMERVEHICLE B ON A.T401_T183_ID = B.ID" +
				" INNER JOIN M116_KODEKOTANOPOL C ON B.T183_M116_ID = C.ID" +
				" INNER JOIN T103_CUSTOMERVEHICLE D ON B.T183_T103_VINCODE = D.ID" +
				" LEFT JOIN T110_FULLMODELCODE ON B.T183_T110_FULLMODELCODE = T110_FULLMODELCODE.ID" +
				" LEFT JOIN M102_BASEMODEL ON T110_FULLMODELCODE.T110_M102_ID = M102_BASEMODEL.ID" +
				" INNER JOIN T402_JOBRCP E ON A.ID = E.T402_T401_NOWO" +
				" INNER JOIN M053_OPERATION F ON E.T402_M053_JOBID = F.ID" +
				" LEFT JOIN T400_CUSTOMERIN G ON T401_T400_ID = G.ID" +
				" WHERE A.COMPANY_DEALER_ID = "+session.userCompanyDealerId+"" +
				" AND A.T401_TANGGALWO BETWEEN  TO_DATE('"+tgl1+"','DD-MM-YYYY') AND TO_DATE('"+tgl2+"','DD-MM-YYYY') "+
				" ORDER BY A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'DD-MM-YYYY')";

		def results = null

		try {
			final sqlQuery = sessions.createSQLQuery(query)
			final queryResults = sqlQuery.with {
				list()
			}
			def nomor = 0
			def style = ""
			def dealerCode = ""
			def areaCode= ""
			def outletCode= ""

			def partNo= ""
			def partDesc = ""
			def stock= ""
			def madUpdate= ""

			results = queryResults.collect
					{ resultRow ->
						dealerCode = resultRow[0]
						areaCode= "-"
						outletCode= resultRow[2]
						partNo= resultRow[3]

						partDesc= resultRow[4]
						stock= resultRow[5]
						madUpdate= resultRow[6]
						nomor++

						htmlData+="<tr "+style+">\n" +
								"                            <td>\n" +
								"                                "+nomor+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+dealerCode+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+areaCode+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+outletCode+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+partNo+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+partDesc+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+stock+"\n" +
								"                            </td>\n" +
								"                            <td>\n" +
								"                                "+madUpdate+"\n" +
								"                            </td>\n" +
								"                        </tr>"

						jsonData << [
								dealerCode : dealerCode,
								areaCode : areaCode,
								outletCode : outletCode,
								partNo : partNo,
								partDesc : partDesc,
								stock : stock,
								madUpdate : madUpdate,
								fileName : "Generate",
								flag : "BLACK"
						]
					}

		}catch(Exception e){

		}
		session["partStockTrx"] = jsonData
		def jumJson = jsonData.size()
		if (jsonData.size()==0){
			flash.message = message(code: 'default.uploadGoods.message', default: "Data Tidak dapat diproses")
		}else if(partStockTrx.size()>0){
			flash.message = message(code: 'default.uploadGoods.message', default: "Hari Ini Sudah Upload Sebanyak " + partTrx.size() + " Kali")
		}else{
			flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done.")
		}
		render(view: "generateData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError,tanggal : tanggal], jumJson : jsonData.size())
	}
	def view() {
		def tanggal = Date.parse("dd/MM/yyyy",params.tanggalUpload_dp)
		session["partStockTrx"] = "";
		def cek = PartStockTransaction.createCriteria()
		def partStockTrx = cek.list() {
			eq("companyDealer",session.userCompanyDealer)
			ge("tglUpload",tanggal)
			lt("tglUpload",tanggal  + 1)
			eq("staDel",'0')
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
				groupProperty("docNumber", "docNumber")
			}
		}
		//handle upload file
		Map CONFIG_JOB_COLUMN_MAP = [
				sheet:'Sheet1',
				startRow: 1,
				columnMap:  [
						//Col, Map-Key
						'A':'dealerCode',
						'B':'areaCode',
						'C':'outletCode',
						'D':'partNo',
						'E':'partDesc',
						'F':'stock',
						'G':'madUpdate'
				]
		]

		CommonsMultipartFile uploadExcel = null
		if(request instanceof MultipartHttpServletRequest){
			MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
			uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
		}
		def jsonData = []
		int jmlhDataError = 0;
		String htmlData = "",status2 = "", statusReplace = ""
		if(!uploadExcel?.empty){
			//validate content type
			def okcontents = [
					'application/excel','application/vnd.ms-excel','application/octet-stream','application/vnd-ms-excel'
			]
			if (!okcontents.contains(uploadExcel?.getContentType())) {
				flash.message = "Illegal Content Type"
				uploadExcel = null
				render(view: "uploadData")
				return
			}

			Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
			//Iterate through bookList and create/persists your domain instances
			def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

			String status = "0", style = "style='color:black'", flag = "black"
			int nomor = 0
			jobList?.each {
				nomor++
				style = "style='color:black'"
				flag = "black"
				if(it.areaCode=="" || it.areaCode==null){
					it.areaCode = '-'
				}

				if(it.dealerCode==null || it.dealerCode=="" || it.outletCode==null || it.outletCode=="" ||
						it.partNo==null || it.partNo=="" ||  it.partDesc==null || it.partDesc=="" ||
						it.stock==null || it.stock=="" || it.madUpdate==null || it.madUpdate==""
				){
					style = "style='color:red'"
					flag =  "red"
				}
				if(it.areaCode!="-"){
					style = "style='color:red'"
					flag =  "red"
				}
				if(it.outletCode != session.userCompanyDealer.m011OutletCode){
					style = "style='color:red'"
					flag =  "red"
					jmlhDataError++
				}
				if(it.dealerCode!="HA"){
					style = "style='color:red'"
					flag =  "red"
					jmlhDataError++
				}

				htmlData+="<tr "+style+">\n" +
						"                            <td>\n" +
						"                                "+nomor+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.dealerCode+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.areaCode+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.outletCode+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.partNo+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.partDesc+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.stock+"\n" +
						"                            </td>\n" +
						"                            <td>\n" +
						"                                "+it.madUpdate+"\n" +
						"                            </td>\n" +
						"                        </tr>"
				status = "0"
				jsonData << [
						dealerCode : it.dealerCode,
						areaCode : it.areaCode,
						outletCode : it.outletCode,
						partNo : it.partNo,
						partDesc : it.partDesc,
						stock : it.stock,
						madUpdate : it.madUpdate,
						fileName : uploadExcel.getOriginalFilename(),
						flag : flag
				]

			}
		}
		if(partStockTrx.size()>0){
			flash.message = message(code: 'default.uploadGoods.message', default: "Hari Ini Sudah Upload Sebanyak " + partStockTrx.size() + " Kali")
		}else if(jmlhDataError>0){
			flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah")
		} else {
			flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
		}
		session["partStockTrx"] = jsonData
		render(view: "uploadData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError,tanggal : params.tanggalUpload, jumJson : jsonData.size()])
	}
	def upload() {
		def requestBody = JSON.parse(params.sendData)
		def tanggal= Date.parse("dd/MM/yyyy",params.tanggal)
		def partStockTransactionInstance = null
		def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
		def sdf = new SimpleDateFormat("yyyyMMdd")
		String docNumber = com+"."+sdf.format(tanggal)
		def cek = PartStockTransaction.createCriteria()
		def partStockTrx = cek.list() {
			eq("companyDealer",session.userCompanyDealer)
			ge("tglUpload",tanggal)
			lt("tglUpload",tanggal + 1)
			eq("staDel",'0')
			resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
			projections {
				groupProperty("docNumber", "docNumber")
			}
		}
		String addText = ""
		if(partStockTrx.size()>0){
			addText = "_" + partStockTrx.size()
		}
		requestBody.each{
			partStockTransactionInstance = new PartStockTransaction()
			String dealerCode = it?.dealerCode.toString()
			String areaCode = it.areaCode.toString()
			String outletCode = it.outletCode.toString()
			String partNo = it.partNo.toString()
			String partDesc = it.partDesc.toString()
			String stock = it.stock.toString()
			String madUpdate = it.madUpdate.toString()
			String fileName = it.fileName.toString()

			//save Data
			partStockTransactionInstance.companyDealer = session.userCompanyDealer
			partStockTransactionInstance.dealerCode = dealerCode
			partStockTransactionInstance.areaCode = areaCode
			partStockTransactionInstance.outletCode = outletCode
			partStockTransactionInstance.partNo = partNo
			partStockTransactionInstance.partDesc = partDesc
			partStockTransactionInstance.stock = stock
			partStockTransactionInstance.madUpdate = madUpdate
			partStockTransactionInstance.staDel = '0'
			partStockTransactionInstance.tglUpload = tanggal
			partStockTransactionInstance.fileName = fileName
			partStockTransactionInstance.docNumber = docNumber+""+addText
			partStockTransactionInstance.dateCreated  = datatablesUtilService?.syncTime()
			partStockTransactionInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			partStockTransactionInstance.lastUpdated  = datatablesUtilService?.syncTime()
			partStockTransactionInstance.lastUpdProcess  = 'INSERT'
			partStockTransactionInstance.save(flush: true)
			partStockTransactionInstance.errors.each {println it}

			flash.message = message(code: 'default.uploadGoods.message', default: "Save Job Done")
		}

		render(view: "uploadData", model: [partStockTransactionInstance: partStockTransactionInstance])

	}

	def exportToExcel(){

		List<JasperReportDef> reportDefList = []
		params.dataExcel = session["partStockTrx"]
		def reportData = exportToExcelDetail(params)

		def reportDef = new JasperReportDef(name:'partStockTransaction.jasper',
				fileFormat:JasperExportFormat.XLS_FORMAT,
				reportData: reportData

		)
		reportDefList.add(reportDef)


		def file = File.createTempFile("PARTSTOCKTRX_",".xls")
		file.deleteOnExit()

		FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

		response.setHeader("Content-Type", "application/vnd.ms-excel")
		response.setHeader("Content-disposition", "attachment;filename=${file.name}")
		response.outputStream << file.newInputStream()


	}
	def exportToExcelDetail(def params){
		def jsonArray = params.dataExcel
		def reportData = new ArrayList();
		jsonArray.each {

			def data = [:]
			data.put("DEALER_CODE",it.dealerCode.toString())
			data.put("AREA_CODE",it.areaCode.toString())
			data.put("OUTLET_CODE",it.outletCode.toString())
			data.put("PART_NOMOR",it.partNo.toString())
			data.put("PART_DESC",it.partDesc.toString())
			data.put("STOCK",it.stock.toString())
			data.put("MAD_UPDATE",it.madUpdate.toString())
			data.put("FLAG_COLOR",it.flag)
			reportData.add(data)
		}

		return reportData
	}

	def printPartStockTrx(){
		List<JasperReportDef> reportDefList = []

		def reportData = partStockTransactionService.printPartStockTrxDetail(params)

		def reportDef = new JasperReportDef(name:'partStockTransaction2.jasper',
				fileFormat:JasperExportFormat.CSV_FORMAT,
				reportData: reportData

		)
		reportDefList.add(reportDef)


		def file = File.createTempFile("HA_STOCK_",".txt")
		file.deleteOnExit()

		def bosData = new StringBuilder();
		def dataToArray = jasperService.generateReport(reportDefList).toString().split('\n')
		dataToArray.each {
			def fixedData = it.replaceAll(",", ";")
			bosData.append(fixedData)
			bosData.append(System.getProperty("line.separator"))
		}
		def gaga = bosData.toString()
		FileUtils.writeStringToFile(file, gaga)
		response.setHeader("Content-Type", "text/plain")
		response.setHeader("Content-disposition", "attachment;filename=${file.name}")
		response.outputStream << file.newInputStream()

	}
}
