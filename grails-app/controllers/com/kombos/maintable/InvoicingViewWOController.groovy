package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Reception
import grails.converters.JSON

class InvoicingViewWOController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    static viewPermissions = [
            'index',
            'viewWO',
            'datatablesList'

    ]

    /* Start ViewWO */
    def index() {
        redirect(action: "viewWO", params: params)
    }
    /* End ViewWO */

    /* Start ViewWO */
    def viewWO() {

    }
    /* End ViewWO */

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = JOCTECO.createCriteria()
        def today = new Date()
        def today2 = today+1
        def tglHariini = today.format("dd/MM/yyyy")
        def tglHariini2 = today2.format("dd/MM/yyyy")
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("companyDealer",session?.userCompanyDealer)
                if(params."sCriteria_Tanggal"){
                    ge("t401TanggalWO",new Date().parse('dd/MM/yyyy',params."sCriteria_Tanggal"))
                }else{
                    ge("t401TanggalWO", new Date().parse('dd/MM/yyyy', tglHariini.toString()))
                }
                if(params."sCriteria_Tanggalakhir"){
                    le("t401TanggalWO",new Date().parse('dd/MM/yyyy',params."sCriteria_Tanggalakhir"))
                }else{
                    lt("t401TanggalWO", new Date().parse('dd/MM/yyyy', tglHariini2.toString()))
                }

                String kategoriView = params.sCriteria_column
                String kataKunci = params.sCriteria_value


                Reception wo = null;
                if (kategoriView == "nomorWO") {
                    eq('t401NoWO', kataKunci,[ignoreCase: true])

                }else if(kategoriView == "noPol"){
                    historyCustomerVehicle{
                        eq('fullNoPol', kataKunci,[ignoreCase: true])
                    }
                }
            }
        }

        def rows = []

        results.each {
                rows << [
                        id: it?.receptionId,
                        tanggalWO: it?.reception?.t401TanggalWO ? it?.reception?.t401TanggalWO?.format("dd/MM/yyyy") : "",
                        noWo: it?.reception?.t401NoWO,
                        noPolisi: it?.reception?.historyCustomerVehicle?.fullNoPol,
                        namaCustomer: it?.reception?.historyCustomer?.fullNama,
                        kategoriJob: it?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan,
                        statusInv: it?.reception?.t401StaInvoice && it?.reception?.t401StaInvoice=="0" ? "OK" : "-",

                ]

        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
}
