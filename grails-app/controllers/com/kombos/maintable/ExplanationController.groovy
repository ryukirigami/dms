package com.kombos.maintable

import com.kombos.administrasi.Divisi
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerFollowUp.FollowUp
import com.kombos.customerFollowUp.MetodeFu
import com.kombos.customerFollowUp.RealisasiFU
import com.kombos.production.FinalInspection
import com.kombos.production.IDR
import com.kombos.production.Masalah
import com.kombos.production.Respon
import com.kombos.reception.KeluhanRcp
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import grails.converters.JSON

class ExplanationController {

    def generateCodeService
    def datatablesUtilService

    def index() {

    }

    def doSave() {
        def username = org.apache.shiro.SecurityUtils.subject.principal.toString()
        def user = User.findByUsername(username)

        def reception = Reception.get(params.receptionId)
        try {
            reception?.t401TglJamExplanation = datatablesUtilService.syncTime()
            reception?.t401NamaSAExplaination = org.apache.shiro.SecurityUtils.subject.principal.toString()
            reception?.t401TglJamExplanationSelesai = datatablesUtilService.syncTime()
            reception?.save(flush: true)
        }catch (Exception e){
        }
//        def companyDealer = reception?.historyCustomer?.companyDealer

        def kategoriWaktuFU = KategoriWaktuFu.get(params.waktuFollowUp.id)
        def metodeFU =null

        if(params.metodeFollowUp){
            if(params.metodeFollowUp==1){
                metodeFU = MetodeFu.findById(1)
            }else{
                metodeFU = MetodeFu.findById(2)
            }
        }


        def followUp = FollowUp.findByReception(reception)
        boolean isError = false
        String error = ""
        if(!followUp){
//            def seq = generateCodeService.codeGenerateSequence('T801_ID',null)
            followUp = new FollowUp()
            followUp.t801ID = FollowUp?.list()?.size()>0 ? (FollowUp?.last()?.id?.toInteger()+1): 1
            followUp.companyDealer = session.userCompanyDealer
            followUp.reception = reception
            followUp.kategoriWaktuFu = kategoriWaktuFU
            followUp.metodeFu = metodeFU
            followUp.t801TglJamFU = params.tanggalFollowUp
            followUp.t801StaBerhasilFU = '0' //belum di follow up
            followUp.t801Ket = params.catatan
            followUp.t801xNamaUser = user
            followUp.t801xDivisi = (user.divisi)?user.divisi:Divisi.first()
            followUp.staDel = '0'
            followUp.lastUpdProcess = 'INSERT'
            followUp.createdBy = username
            followUp.dateCreated = datatablesUtilService?.syncTime()
            followUp.lastUpdated = datatablesUtilService?.syncTime()

            if(!followUp.save(flush: true, insert: true)){
                isError = true
                error = "Data tidak berhasil disimpan !"
                followUp.errors.allErrors.each {
                    println it
                }
            }
        }else{
            followUp = FollowUp.get(followUp.id)
            followUp.kategoriWaktuFu = kategoriWaktuFU
            followUp.metodeFu = metodeFU
            followUp.t801TglJamFU = params.tanggalFollowUp
            followUp.t801StaBerhasilFU = '0' //belum di follow up
            followUp.t801Ket = params.catatan
            followUp.t801xNamaUser = user
            followUp.t801xDivisi = (user.divisi)?user.divisi:Divisi.first()
            followUp.staDel = '0'
            followUp.lastUpdProcess = 'UPDATE'
            followUp.updatedBy = username
            followUp.lastUpdated = datatablesUtilService?.syncTime()

            if(!followUp.save(flush: true, insert: false)){
                isError = true
                error = "Data tidak berhasil disimpan !"
                followUp.errors.allErrors.each {
                    println it
                }
            }
        }

        if (isError) {
            def result = [status: "FAIL", error: error]
            render result as JSON
        } else {
            def rFU = new RealisasiFU()
            rFU.followUp = followUp
            rFU.companyDealer = followUp?.companyDealer
            rFU.metodeFu = followUp?.metodeFu
            rFU.createdBy = username
            rFU.lastUpdProcess = 'INSERT'
            rFU.t802NamaSA_FU = username
            rFU.t802CatatanCounterMeasure = "-"
            rFU.t802StaBalasanSMS = '0'
            rFU.t802StaCounterMeasure = '0'
            rFU.t802StaNomorValid = '0'
            rFU.t802StaPotensiRTJ = '0'
            rFU.t802StaTerhubung = '0'
            rFU.dateCreated = datatablesUtilService?.syncTime()
            rFU.lastUpdated = datatablesUtilService?.syncTime()
            rFU.t802ID = RealisasiFU?.list()?.size() > 0 ? (RealisasiFU?.last()?.id?.toInteger()+1) : 1
            rFU.t802TglJamFU = followUp?.t801TglJamFU
            rFU.save(flush: true)

            def rcp = Reception.findById(params.receptionId)
            rcp.t401TglJamExplanationSelesai = datatablesUtilService?.syncTime()
            rcp?.t401NamaSAExplaination = org.apache.shiro.SecurityUtils.subject.principal.toString()
            rcp.save(flush: true)

            def result = [status: "OK"]
            render result as JSON
        }
    }


    def loadDataSemua() {
        String kategoriView = params.kategoriView
        String kataKunci = params.kataKunci

        def wo = null;
        if (kategoriView.trim().equalsIgnoreCase("NOMOR WO")) {
            wo = Reception.findByT401NoWOIlikeAndStaDelAndStaSave("%"+kataKunci?.trim()+"%","0","0", [sort: "dateCreated", order: "desc"])
        } else {
            def temp = Reception.withCriteria {
                historyCustomerVehicle {
                    eq("fullNoPol", kataKunci?.trim(),[ignoreCase:true])
                }
                eq("staDel","0");
                eq("staSave","0");
                order("id", "desc");
            }
            wo = temp.size() > 0 ? temp.get(0) : null
        }

        if (wo == null) {
            def result = [status: "Error", message: "Data tidak ditemukan"]
            render result as JSON
            return
        }

        def InformasiUmum = [
                noWo: wo?.t401NoWO,
                nopol: wo?.historyCustomerVehicle?.fullNoPol,
                model: wo?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName,
                namaCustomer: wo?.historyCustomer?.fullNama
        ]


        def keluhans = []
        def keluhanRcps = KeluhanRcp.findAllByReception(wo)
        keluhanRcps.each { keluhanRcp ->
            keluhans << [nama: keluhanRcp?.t411NamaKeluhan ? keluhanRcp?.t411NamaKeluhan : "-"]
        }

        def actuals = Actual.findAllByReception(wo)
        def DiagnoseCustomers = []
        actuals.each { actual ->
            def masalahs = Masalah.findAllByActual(actual)
            masalahs.each { masalah ->
                def respons = Respon.findAllByActualAndMasalah(actual, masalah)
                respons.each { respon ->
                    def DiagnoseCustomer = [
                            Masalah: masalah?.t501CatatanTambahan ? masalah?.t501CatatanTambahan : "-",
                            Solusi: respon?.t505Respon ? respon?.t505Respon : "-"
                    ]
                    DiagnoseCustomers << DiagnoseCustomer
                }
            }
        }

        def jobRCPs = JobRCP.findAllByReceptionAndStaDel(wo, "0")
        def StatusPekerjaans = []
        jobRCPs.each { jobRCP ->
            StatusPekerjaans << [
                    nama: jobRCP?.operation?.m053NamaOperation,
                    status: jobRCP?.t402StaTambahKurang?jobRCP?.t402StaTambahKurang:"-",
                    alasan: jobRCP?.t402AlasanCancel?jobRCP?.t402AlasanCancel:"-"
            ];
        }

        def IDRs = IDR.findAllByReception(wo)
        def InspectionDuringRepairs = []
        IDRs.each { idr ->
            InspectionDuringRepairs << [status: idr?.t506StaIDR ? idr?.t506StaIDR : "-", alasan: idr?.t506AlasanIDR ? idr?.t506AlasanIDR : "-"]
        }

        def finalInspections = FinalInspection.findAllByReception(wo)
        def jobSugest = JobSuggestion.findByCustomerVehicle(wo?.historyCustomerVehicle?.customerVehicle)?.t503KetJobSuggest
        def t201TglJatuhTempo = Reminder.findByCustomerVehicle(wo?.historyCustomerVehicle?.customerVehicle)?.t201TglJatuhTempo
        def FinalInspections = []
        finalInspections.each { finalInspection ->
            FinalInspections << [alasan: finalInspection?.t508Keterangan ? finalInspection?.t508Keterangan : "-", hasil: (finalInspection?.t508StaKategori+" "+finalInspection?.t508StaInspection+" "+jobSugest+" "+(t201TglJatuhTempo?.format("dd/MM/yyyy")))]
        }

        def cekJoc = JOCTECO.findByReceptionAndT601staOkCancelJOC(wo,"0")
        def result = [:]
        result = [status: "OK",
                InformasiUmum: InformasiUmum,
                keluhans: keluhans,
                DiagnoseCustomers: DiagnoseCustomers,
                StatusPekerjaans: StatusPekerjaans,
                InspectionDuringRepairs:InspectionDuringRepairs,
                FinalInspections: FinalInspections,
                receptionId:wo.id]
        wo?.t401TglJamExplanation = datatablesUtilService.syncTime()
        wo?.t401NamaSAExplaination = org.apache.shiro.SecurityUtils.subject.principal.toString()
        wo.save(flush: true)

        render result as JSON
    }
}