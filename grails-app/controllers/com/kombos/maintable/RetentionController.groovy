package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class RetentionController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = Retention.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m207NamaRetention"){
				ilike("m207NamaRetention","%" + (params."sCriteria_m207NamaRetention" as String) + "%")
			}
	
			if(params."sCriteria_m207FormatSms"){
				ilike("m207FormatSms","%" + (params."sCriteria_m207FormatSms" as String) + "%")
			}
	
			if(params."sCriteria_m207StaOtomatis"){
				ilike("m207StaOtomatis","%" + (params."sCriteria_m207StaOtomatis" as String) + "%")
			}
	
			if(params."sCriteria_m207Ket"){
				ilike("m207Ket","%" + (params."sCriteria_m207Ket" as String) + "%")
			}
	
				ilike("staDel","0")

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m207NamaRetention: it.m207NamaRetention,
			
						m207FormatSms: it.m207FormatSms,
			
						m207StaOtomatis: it?.m207StaOtomatis?.equalsIgnoreCase("1") ? 'Otomatis' : it?.m207StaOtomatis?.equalsIgnoreCase("2") ?  'Manual' : 'Manual & Otomatis'

            ]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[retentionInstance: new Retention(params)]
	}

	def save() {
		params?.dateCreated = datatablesUtilService?.syncTime()
		params?.lastUpdated = datatablesUtilService?.syncTime()
        def retentionInstance = new Retention(params)
        def cek = retentionInstance.createCriteria()
        def result = cek.list() {
            or{
                eq("m207NamaRetention",retentionInstance.m207NamaRetention?.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }
        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "create", model: [retentionInstance: retentionInstance])
            return
        }
		if (!retentionInstance.save(flush: true)) {
			render(view: "create", model: [retentionInstance: retentionInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'retention.label', default: 'Retention'), retentionInstance.id])
		redirect(action: "show", id: retentionInstance.id)
	}

	def show(Long id) {
		def retentionInstance = Retention.get(id)
		if (!retentionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'retention.label', default: 'Retention'), id])
			redirect(action: "list")
			return
		}

		[retentionInstance: retentionInstance]
	}

	def edit(Long id) {
		def retentionInstance = Retention.get(id)
		if (!retentionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'retention.label', default: 'Retention'), id])
			redirect(action: "list")
			return
		}

		[retentionInstance: retentionInstance]
	}

	def update(Long id, Long version) {
		def retentionInstance = Retention.get(id)
		if (!retentionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'retention.label', default: 'Retention'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (retentionInstance.version > version) {
				
				retentionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'retention.label', default: 'Retention')] as Object[],
				"Another user has updated this Retention while you were editing")
				render(view: "edit", model: [retentionInstance: retentionInstance])
				return
			}
		}
        def cek2 = retentionInstance.createCriteria()
        def result2 = cek2.list() {
            eq("m207NamaRetention",params.m207NamaRetention, [ignoreCase: true])
            eq("staDel",'0')
        }
        if(result2 &&  Retention.findByM207NamaRetentionIlikeAndStaDel(params.m207NamaRetention,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "edit", model: [retentionInstance: retentionInstance])
            return
        }
        params?.lastUpdated = datatablesUtilService?.syncTime()
		retentionInstance.properties = params

		if (!retentionInstance.save(flush: true)) {
			render(view: "edit", model: [retentionInstance: retentionInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'retention.label', default: 'Retention'), retentionInstance.id])
		redirect(action: "show", id: retentionInstance.id)
	}

	def delete(Long id) {
		def retentionInstance = Retention.get(id)
		if (!retentionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'retention.label', default: 'Retention'), id])
			redirect(action: "list")
			return
		}

		try {
            retentionInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            retentionInstance?.lastUpdProcess = "DELETE"
            retentionInstance?.setStaDel('1')
            retentionInstance?.lastUpdated = datatablesUtilService?.syncTime()
            retentionInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'retention.label', default: 'Retention'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'retention.label', default: 'Retention'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Retention, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Retention, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
