package com.kombos.maintable

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class NoEfakturController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def noEfakturService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render noEfakturService.datatablesList(params) as JSON
    }

    def create() {
        def result = noEfakturService.create(params)

        if (!result.error)
            return [noEfakturInstance: result.noEfakturInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = noEfakturService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["NoEfaktur", result.noEfakturInstance.id])
            redirect(action: 'show', id: result.noEfakturInstance.id)
            return
        }

        render(view: 'create', model: [noEfakturInstance: result.noEfakturInstance])
    }

    def show(Long id) {
        def result = noEfakturService.show(params)

        if (!result.error)
            return [noEfakturInstance: result.noEfakturInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = noEfakturService.show(params)

        if (!result.error)
            return [noEfakturInstance: result.noEfakturInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = noEfakturService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["NoEfaktur", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [noEfakturInstance: result.noEfakturInstance.attach()])
    }

    def delete() {
        def result = noEfakturService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["NoEfaktur", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(NoEfaktur, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
