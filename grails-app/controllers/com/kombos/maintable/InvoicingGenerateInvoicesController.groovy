package com.kombos.maintable

import com.kombos.administrasi.*
import com.kombos.board.JPB
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.customerprofile.JenisPerusahaan
import com.kombos.finance.Collection
import com.kombos.finance.Journal
import com.kombos.finance.JournalDetail
import com.kombos.finance.SubType
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.GoodsHargaBeli
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.Konversi
import com.kombos.parts.PickingSlip
import com.kombos.reception.BillToReception
import com.kombos.reception.CustomerIn
import com.kombos.reception.JobInv
import com.kombos.reception.Reception
import com.kombos.utils.MoneyUtil
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification

class InvoicingGenerateInvoicesController {

    def smsService
    def jasperService
    def HPPSubletJournalService
    def pemakaianPartsJournalService
    def journalPendingKeluarService
    def invoiceKreditJournalService
    def datatablesUtilService
    def conversi = new Konversi()
    def staApproveJob = ["Approved","UnApproved","Waiting For Approval"]
    private MoneyUtil moneyUtil = new MoneyUtil()

    static viewPermissions = [
            'index',
            'generateInvoices',
            'discountTable'
    ]

    def generateInvoices() {
        def reception = new Reception()
        if(params.idUbah){
            reception = Reception.get(params.idUbah.toLong())
        }
        [receptionInstance : reception]

    }
    def cekPickingSlip(){
        def result = [:]
        result.picking = false
        result.teknisi = false
        result.approvalSO = true
        def part = PartsRCP.createCriteria().list {
            reception{
                eq("t401NoWO", params.noWo)
            }
            eq("staDel","0");
            or{
                ilike("t403StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t403StaTambahKurang","%1%")
                        ilike("t403StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t403StaTambahKurang")
                    isNull("t403StaApproveTambahKurang")
                }
            }
        }
        if(part.size()>0){
            def picking = PickingSlip.findByReceptionAndStaDel(Reception.findByT401NoWO(params.noWo),"0")
            if(picking){
                result.picking  = true
            }
        }else{
            result.picking  = true
        }
        if(JPB.findByReceptionAndStaDel(Reception.findByT401NoWO(params.noWo),"0")?.namaManPower){
            result.teknisi  = true
        }
        def ar = new ArrayList();
        ar.add(KegiatanApproval.PENGURANGAN_JOB_ATAU_PART)
        ar.add(KegiatanApproval.REQUEST_SPECIAL_DISCOUNT)
        ar.add(KegiatanApproval.EDIT_RATE)
        ar.add(KegiatanApproval.PO_SUBLET)
        def kegiataLis = KegiatanApproval.findAllByM770KegiatanApprovalInList(ar)
        def aap = ApprovalT770.findAllByKegiatanApprovalInListAndT770NoDokumen(kegiataLis,params.noWo)
        if(aap.t770Status.toString().contains('0')){
            result.approvalSO  = false
        }
        render result as JSON
    }
    def loadDataInv() {
        String kategoriView = params.kategoriView
        String kataKunci = params.kataKunci
        def company = session?.userCompanyDealer
        def wo = null;
        def skrg = new Date().clearTime()
        if (kategoriView.trim().equalsIgnoreCase("NOMOR WO")) {
            wo = Reception.findByT401NoWOIlikeAndStaDelAndStaSaveAndCompanyDealer("%"+kataKunci?.trim(),"0","0", company, [sort: "id", order: "desc"])
        } else {
            def temp = Reception.withCriteria {
                historyCustomerVehicle {
                    or{
                        eq("fullNoPol", kataKunci?.trim(),[ignoreCase : true])
                        ilike("fullNoPol","%"+kataKunci?.trim()+"%")
                    }
                }
                eq("companyDealer",company)
                eq("staDel","0");
                eq("staSave","0");
                order("id", "desc")
            }
            wo = temp.size() > 0 ? temp.get(0) : null
        }

        if (wo == null) {
            def result = [status: "Error", message: "Data tidak ditemukan"]
            render result as JSON
            return
        }

        def InfoInvoicing = [
                tglWo: wo.t401TglJamCetakWO ? wo.t401TglJamCetakWO.format("dd-MM-yyyy HH:mm") : "",
                noWo: wo.t401NoWO? wo.t401NoWO: "",
                nopol: wo.historyCustomerVehicle?.fullNoPol,
                noMesin: wo.historyCustomerVehicle?.t183NoMesin,
                noRangka: wo.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                modelNm: wo.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                namaCustomer: wo.historyCustomer?.fullNama,
                t182Alamat : wo.historyCustomer?.t182Alamat,
                serviceAdv: wo.t401NamaSA
        ]

        def joc = JOCTECO.findByReceptionAndT601staOkCancelJOC(wo,"0")
        def result = [:]
        result = [status: "OK", InfoInvoicing: InfoInvoicing]
        render result as JSON
    }

    def datatablesList(){
        session.exportParams = params

        def service = new InvoicingGenerateInvoicesService()
        render service.datatablesList(params) as JSON
    }

    def dataTablePart(){
        session.exportParams = params

        def service = new InvoicingGenerateInvoicesService()
        render service.datatablesPart(params) as JSON
    }

    def dataTableJasa(){
        session.exportParams = params

        def service = new InvoicingGenerateInvoicesService()
        render service.datatablesJasa(params) as JSON
    }

    def dataTableDiscount(){
        session.exportParams = params

        def service = new InvoicingGenerateInvoicesService()
        render service.datatablesDiscount(params) as JSON
    }

    def partDatatablesList() {
        def service = new InvoicingGenerateInvoicesService()
        render service.partDatatablesList(params) as JSON
    }

    def partList(){
        Random rand = new Random();
        int  n = rand.nextInt(50) + 1;
        String idTable = new Date().format("yyyyMMddhhmmss")+params.idJob+n.toString()
        [idInv : params.idInv, idJob : params.idJob, idTable: idTable]
    }

    def SendApproval(){
        def invoice = InvoiceT701.get(params.id.toLong())
        [invoiceInstance : invoice]
    }

    def doGenerate(){
        String hasil = ""

        Reception reception = Reception.findByT401NoWOAndStaDel(params.noWo,"0")
        if(reception?.t401StaInvoice && reception?.t401StaInvoice=="0"){
            hasil = "sudah"
        }else if(reception?.t401StaInvoice && reception?.t401StaInvoice=="2"){
            hasil = "tunggu"
        }else{

            def partsCek = PartsRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",reception)
                or{
                    ilike("t403StaTambahKurang","%0%")
                    and{
                        not{
                            ilike("t403StaTambahKurang","%1%")
                            ilike("t403StaApproveTambahKurang","%1%")
                        }
                    }
                    and{
                        isNull("t403StaTambahKurang")
                        isNull("t403StaApproveTambahKurang")
                    }
                }
            }
            def mssg = "Data Goods dengan kode "
            def backToHome = false
            def hasUsed = false;
            partsCek.each {
                def klas = KlasifikasiGoods.findByGoods(it?.goods)
                if(!klas){
                    backToHome = true
                    mssg+=it?.goods?.m111ID+" , "
                    backToHome = true
                }
            }

            if(backToHome){
                def result = [:]
                mssg+="\nBelum memiliki klasifikasi goods\nMohon dilengkapi"
                result.error = mssg
                render result as JSON
            }else {
                def subtype = "";
                def subledger = "";
                def staError = false
                hasil = "oke"
                def cekApprovalJobs = JobRCP.findByReceptionAndStaDelAndT402StaTambahKurangAndT402StaApproveTambahKurang(reception,"0","1","2");
                def cekApprovalParts = PartsRCP.findByReceptionAndStaDelAndT403StaTambahKurangAndT403StaApproveTambahKurang(reception,"0","1","2");

                if(cekApprovalJobs || cekApprovalParts){
                    hasil = "jobpart"
                }else {
                    def billToJob= JobRCP.createCriteria().listDistinct {
                        eq("staDel","0")
                        eq("reception",reception)
                        or{
                            ilike("t402StaTambahKurang","0")
                            and{
                                not{
                                    ilike("t402StaTambahKurang","%1%")
                                    ilike("t402StaApproveTambahKurang","%1%")
                                }
                            }
                            and {
                                isNull("t402StaTambahKurang")
                                isNull("t402StaApproveTambahKurang")
                            }
                        }
                        resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                        projections {
                            groupProperty("t402namaBillTo", "namaBillTo")
                            groupProperty("t402billTo", "billTo")
                        }
                    }

                    def billToParts= PartsRCP.createCriteria().listDistinct {
                        eq("staDel","0")
                        eq("reception",reception)
                        or{
                            ilike("t403StaTambahKurang","%0%")
                            and{
                                not{
                                    ilike("t403StaTambahKurang","%1%")
                                    ilike("t403StaApproveTambahKurang","%1%")
                                }
                            }
                            and{
                                isNull("t403StaTambahKurang")
                                isNull("t403StaApproveTambahKurang")
                            }
                        }
                        resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                        projections {
                            groupProperty("t403namaBillTo", "namaBillTo")
                            groupProperty("t403billTo", "billTo")
                        }
                    }
                    int ind = 0
                    def listPart = []
                    billToJob.each {
                        subtype = "";
                        subledger = "";
                        if(billToParts.indexOf(it)>=0){
                            listPart << it
                        }
                        ind++
                        def namaBillTo = it.namaBillTo
                        def billTo = it.billTo
                        def rpSublet = 0
                        def rpJasa = 0
                        def rpPart = 0
                        def wo=""
                        def parts = []
                        String namaCustomer = reception?.historyCustomer?.fullNama
                        String alamat = reception?.historyCustomer?.t182Alamat
                        String npwp = reception?.historyCustomer?.t182NPWP
                        def rpPartCampuran = 0
                        def rpPartToyota = 0
                        def rpOli = 0
                        def rpMaterial = 0
                        def rpDiscount = 0
                        def jobs = JobRCP.createCriteria().list {
                            eq("staDel","0")
                            eq("reception",reception)
                            or{
                                ilike("t402StaTambahKurang","%0%")
                                and{
                                    not{
                                        ilike("t402StaTambahKurang","%1%")
                                        ilike("t402StaApproveTambahKurang","%1%")
                                    }
                                }
                                and {
                                    isNull("t402StaTambahKurang")
                                    isNull("t402StaApproveTambahKurang")
                                }
                            }
                            if(namaBillTo=="null" || namaBillTo==null || namaBillTo==""){
                                isNull("t402namaBillTo")
                            }else{
                                eq("t402namaBillTo",namaBillTo,[ignoreCase: true])
                                eq("t402billTo",billTo,[ignoreCase: true])
                            }
                        }
                        def jenis = BillToReception.createCriteria().list {
                            eq("reception",reception)
                            if(jobs?.size()>0 && namaBillTo){
                                eq("namaBillTo",jobs?.last()?.t402namaBillTo,[ignoreCase: true])
                                eq("billTo",jobs?.last()?.t402billTo,[ignoreCase: true])
                            }else{
                                eq("id",-1.toLong())
                            }
                            order("id");
                        }

                        jobs.each {
                            if(billTo){
                                namaCustomer = namaBillTo
                                if(billTo.toString().equalsIgnoreCase("customer")){
                                    def temp = HistoryCustomer.findByFullNama(namaBillTo)
                                    if(temp){
                                        alamat = temp?.t182Alamat
                                        npwp = temp?.t182NPWP
                                        subtype = "642218"
                                        subledger = temp.id.toString()
                                    }
                                }else if(billTo.toString().equalsIgnoreCase("dealer")){
                                    def temp = DealerPenjual.findByM091NamaDealer(namaBillTo)
                                    def idD = "";
                                    if(temp){
                                        alamat = temp?.m091AlamatDealer
                                        npwp = temp?.m091Npwp
                                        if(!Company.findByNamaPerusahaan(namaBillTo)){
                                            def comp = new Company()
                                            comp.companyDealer = session.userCompanyDealer
                                            comp.kodePerusahaan = new GenerateCodeService().codeGenerateSequence("T101_ID",null)
                                            comp.jenisPerusahaan  = JenisPerusahaan.findByNamaJenisPerusahaanAndStaDel("PT","0");
                                            comp.namaPerusahaan = namaBillTo
                                            comp.alamatPerusahaan = "INI DARI SISTEM YG INVOICE BILLTO"
                                            comp.telp = "0"
                                            comp.namaDepanPIC = "INI DARI SISTEM YG INVOICE "
                                            comp.alamatPIC = "INI DARI SISTEM YG INVOICE "
                                            comp.telpPIC = "0"
                                            comp.hpPIC = "0"
                                            comp.noNPWP = npwp
                                            comp.alamatNPWP = "INI DARI SISTEM YG INVOICE BILLTO"
                                            comp.staDel = "0"
                                            comp.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                            comp.lastUpdProcess = "INSERT"
                                            comp.dateCreated = datatablesUtilService?.syncTime()
                                            comp.lastUpdated = datatablesUtilService?.syncTime()
                                            comp.save(flush: true)
                                            comp.errors.each {
                                                println "compBaru : " + it
                                            }
                                        }
                                        idD = Company.findByNamaPerusahaan(namaBillTo).id

                                        subtype = "1753355"
                                        subledger = idD
                                    }
                                }else if(billTo.toString().equalsIgnoreCase("asuransi")){
                                    def temp = VendorAsuransi.findByM193Nama(namaBillTo)
                                    if(temp){
                                        alamat = temp?.m193Alamat
                                        npwp = temp?.m193Npwp
                                        subtype = "642217"
                                        subledger =temp.id.toString()
                                    }
                                }else if(billTo.toString().equalsIgnoreCase("cabang")){
                                    def temp = CompanyDealer.findByM011NamaWorkshop(namaBillTo)
                                    if(temp){
                                        alamat = temp?.m011Alamat
                                        npwp = temp?.m011NPWP
                                        subtype = "642219"
                                        subledger =temp.id.toString()
                                    }
                                }else{
                                    if(!Company.findByNamaPerusahaan(namaBillTo)){
                                        def comp = new Company()
                                        comp.companyDealer = session.userCompanyDealer
                                        comp.kodePerusahaan = new GenerateCodeService().codeGenerateSequence("T101_ID",null)
                                        comp.jenisPerusahaan  = JenisPerusahaan.findByNamaJenisPerusahaanAndStaDel("PT","0");
                                        comp.namaPerusahaan = namaBillTo
                                        comp.alamatPerusahaan = "INI DARI SISTEM YG INVOICE BILLTO"
                                        comp.telp = "0"
                                        comp.namaDepanPIC = "INI DARI SISTEM YG INVOICE "
                                        comp.alamatPIC = "INI DARI SISTEM YG INVOICE "
                                        comp.telpPIC = "0"
                                        comp.hpPIC = "0"
                                        comp.alamatNPWP = "INI DARI SISTEM YG INVOICE BILLTO"
                                        comp.staDel = "0"
                                        comp.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                        comp.lastUpdProcess = "INSERT"
                                        comp.dateCreated = datatablesUtilService?.syncTime()
                                        comp.lastUpdated = datatablesUtilService?.syncTime()
                                        comp.save(flush: true)
                                        comp.errors.each {
                                            println "compBaru : " + it
                                        }
                                    }
                                    def temp = Company.findByNamaPerusahaan(namaBillTo)
                                    if(temp){
                                        alamat = temp?.alamatPerusahaan
                                        npwp = temp?.noNPWP

                                    }

                                    subtype = "1753355"
                                    subledger = temp.id.toString()
                                }
                            }
                            rpJasa+=it.t402HargaRp && !it?.t402JmlSublet ? it.t402HargaRp : 0
                            rpSublet+=it.t402JmlSublet ? it?.t402JmlSublet?.toDouble() : 0
                            rpDiscount += it.t402DiscRp ? it.t402DiscRp : 0
                        }

                        parts = PartsRCP.createCriteria().list {
                            eq("staDel","0");
                            eq("reception",reception);
                            if(billTo){
                                eq("t403billTo",billTo)
                                eq("t403namaBillTo",namaBillTo)
                            }else{
                                isNull("t403billTo");
                                isNull("t403namaBillTo");
                            }
                            or{
                                ilike("t403StaTambahKurang","%0%")
                                and{
                                    not{
                                        ilike("t403StaTambahKurang","%1%")
                                        ilike("t403StaApproveTambahKurang","%1%")
                                    }
                                }
                                and{
                                    isNull("t403StaTambahKurang")
                                    isNull("t403StaApproveTambahKurang")
                                }
                            }
                        }

                        parts.each {
                            def hargaSDiskon = it?.t403Jumlah1 && it?.t403HargaRp ? (it?.t403Jumlah1 * it?.t403HargaRp) : 0
                            def klas = KlasifikasiGoods.findByGoods(it.goods)
                            if(klas){
                                if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL")){
                                    rpMaterial+= hargaSDiskon
                                }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                                    rpOli+= hargaSDiskon
                                }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("TOYOTA")){
                                    rpPartToyota+= hargaSDiskon
                                }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN")){
                                    rpPartCampuran+= hargaSDiskon
                                }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                                    rpPartCampuran+= hargaSDiskon
                                }else {
                                    rpMaterial+= hargaSDiskon
                                }
                                rpDiscount += it.t403DiscRp ? it.t403DiscRp : 0
                            }else {
                                rpMaterial+= hargaSDiskon
                            }
                        }
                        def rpBF = 0
                        def rpOR = 0
                        if(namaCustomer==reception?.historyCustomer?.fullNama && !hasUsed){
                            hasUsed = true;
                            def kwitansi = Kwitansi.findByReceptionAndT722StaDel(reception,"0")
                            if(kwitansi){
                                def bookingFee = BookingFee.findByKwitansiAndT721StaDel(kwitansi,"0")
                                if(bookingFee){
                                    if(kwitansi?.t722StaBookingOnRisk?.equalsIgnoreCase("0")){
                                        rpBF = bookingFee?.t721JmlhBayar
                                    }else{
                                        rpOR = bookingFee?.t721JmlhBayar
                                    }
                                }
                            }
                        }
                        def invoice = new InvoiceT701()
                        invoice?.reception = reception
                        invoice?.historyCustomer = reception?.historyCustomer
                        invoice?.companyDealer = session.userCompanyDealer
                        invoice.t701NoInv = generateNoInv('invoice')
                        invoice.t701JenisInv = namaBillTo ? (jenis?.size()>0 ? jenis?.last()?.jenisBayar?.substring(0,1) : "T") : "T"
                        def jpbTek = JPB.findByReceptionAndStaDel(reception, "0")
                        def teeeknisi = "";
                        if(jpbTek){
                            teeeknisi = jpbTek?.namaManPower?.t015NamaLengkap?.toString()
                        }else{
                            try {
                                def p = PickingSlip.findByReceptionAndStaDel(reception,"0").t141NamaTeknisi.trim()
                                teeeknisi = NamaManPower.findByT015NamaLengkapLike("%"+p+"%").t015NamaLengkap.toString()
                            }catch (Exception e){

                            }
                        }
                        invoice?.t701Teknisi = teeeknisi
                        invoice?.t701AdmBilling = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        invoice?.t701JasaRp = Math.round(rpJasa)
                        invoice?.t701SubletRp = Math.round(rpSublet)
                        invoice?.t701PartsRp = (Math.round(rpPartCampuran) + Math.round(rpPartToyota))
                        invoice?.t701PartsRpCampuran = Math.round(rpPartCampuran)
                        invoice?.t701PartsRpToyota = Math.round(rpPartToyota)
                        invoice?.t701OliRp = Math.round(rpOli)
                        invoice?.t701MaterialRp = Math.round(rpMaterial)
                        try {
                            invoice?.t701NPWP = npwp
                            invoice?.t701Subtype = subtype
                            invoice?.t701Subledger = subledger
                        }catch (Exception e){}
                        def adm = BiayaAdministrasi.findAllByTglBerlakuLessThanEquals(new Date())
                        def nilaiAdm = 0
                        if(adm){
                            nilaiAdm = adm?.last()?.biaya
                        }
                        invoice?.t701AdmRp = Math.round(nilaiAdm)
                        invoice?.t701SubTotalRp = Math.round(rpJasa) + Math.round(rpSublet) + (Math.round(rpPartCampuran) + Math.round(rpPartToyota)) + Math.round(rpOli) + Math.round(rpMaterial) + Math.round(invoice?.t701AdmRp)
                        invoice?.t701TotalDiscRp = Math.round(rpDiscount)
                        invoice?.t701TotalRp = Math.round(invoice?.t701SubTotalRp) - Math.round(invoice?.t701TotalDiscRp)
                        invoice?.t701PPnRp = Math.round(10 / 100 * invoice?.t701TotalRp)
                        def nilaiMaterai = 0
                        def materai = Materai.findAllByM705TglBerlakuLessThanEqualsAndM705Nilai1LessThanEqualsAndM705Nilai2GreaterThanEquals(new Date(),invoice?.t701TotalRp,invoice?.t701TotalRp)
                        if(materai){
                            nilaiMaterai = materai?.last()?.m705NilaiMaterai
                        }
                        invoice?.t701MateraiRp = Math.round(nilaiMaterai)
                        invoice?.t701TotalInv = Math.round(invoice?.t701TotalRp) + Math.round(invoice?.t701MateraiRp) + Math.round(invoice?.t701PPnRp)
                        invoice?.t701BookingFee = Math.round(rpBF)
                        invoice?.t701OnRisk = Math.round(rpOR)
                        invoice?.t701TotalBayarRp = Math.round(invoice.t701TotalInv) - Math.round(rpBF) - Math.round(rpOR)
                        invoice?.t701Nopol = reception?.historyCustomerVehicle?.fullNoPol
                        invoice?.t701TglJamInvoice = datatablesUtilService?.syncTime()
                        invoice?.t701Customer = namaCustomer
                        invoice?.t701Alamat = alamat
                        invoice?.t701NPWP = npwp
                        invoice?.t701IntExt = "1"
                        invoice?.t701Tipe = reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.toUpperCase()?.contains("G") ? "0" : "1"
                        invoice?.t701StaSettlement = "0"
                        invoice?.t701StaDel = "0"

                        int bilTotal = invoice.t701TotalBayarRp
                        invoice?.t701TotalInvTerbilang = moneyUtil.convertMoneyToWords(bilTotal)
                        invoice?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
                        invoice?.lastUpdProcess="INSERT"
                        invoice?.dateCreated= datatablesUtilService?.syncTime()
                        invoice?.lastUpdated= datatablesUtilService?.syncTime()
                        def cek = cekIsExist(invoice?.t701NoInv);
                        if(cek){
                            invoice.t701NoInv = generateNoInv('invoice')
                        }
                        invoice.save(flush: true)
                        invoice.errors.each {}
                        if(invoice.hasErrors()){
                            staError = true
                        }
                        String noPartSlip = generateNoInv("partslip")
                        int batas = 0, temp = 0
                        jobs.each {
                            def harga = it?.t402JmlSublet?it?.t402JmlSublet?.toDouble():(it?.t402HargaRp?it?.t402HargaRp:0)
                            def total = it?.t402DiscRp ? (harga-it?.t402DiscRp) : harga
                            def nJob = new JobInv()
                            nJob?.companyDealer = session.userCompanyDealer
                            nJob?.invoice = invoice
                            nJob?.reception = reception
                            nJob?.operation = it.operation
                            nJob?.t702HargaRp = Math.round(harga)
                            nJob?.t702DiscRp = (it?.t402DiscRp?it?.t402DiscRp:0)
                            nJob?.t702DiscPersen = it?.t402DiscPersen
                            nJob?.t702TotalRp = Math.round(total)
                            nJob?.staDel = "0"
                            nJob?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
                            nJob?.lastUpdProcess="INSERT"
                            nJob?.t702xKet= it.t402JmlSublet ? "SUBLET" : ""
                            nJob?.dateCreated= datatablesUtilService?.syncTime()
                            nJob?.lastUpdated= datatablesUtilService?.syncTime()
                            nJob?.save(flush: true)
                            nJob?.errors?.each {

                            }
                            batas = temp
                            def klas = ["PARTS", "BAHAN"]
                            klas.each { kl ->
                                parts.each {
                                    def klasifikasi = null
                                    def tampil = false
                                    if (kl=="PARTS"){
                                        klasifikasi = KlasifikasiGoods.findByGoods(it?.goods)?.franc?.m117NamaFranc
                                        if(klasifikasi.toUpperCase().contains(kl.toUpperCase()) ||
                                                klasifikasi.toUpperCase().contains("OLI")){
                                            tampil = true
                                        }
                                    }else{
                                        klasifikasi = KlasifikasiGoods.findByGoods(it?.goods)?.franc?.m117NamaFranc
                                        if(!klasifikasi.toUpperCase().contains("PARTS") &&
                                                !klasifikasi.toUpperCase().contains("OLI")){
                                            tampil = true
                                        }
                                    }
                                    if (tampil == true) {
                                        def cari = PartInv.findByInvoiceAndGoodsAndT703StaDelAndOperation(invoice, it.goods, "0",it?.operation)
                                        if(cari == null){
                                            if(batas == 7 ){
                                                noPartSlip = generateNoInv("partslip")
                                                batas = 0
                                                temp = 0
                                            }
                                            def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.goods,session?.userCompanyDealer,"0")
                                            def hargaBeli = 0
                                            if(goodsHargaBeli){
                                                hargaBeli = goodsHargaBeli?.t150Harga
                                            }
                                            def nPartInv = new PartInv()
                                            nPartInv.companyDealer = session.userCompanyDealer
                                            nPartInv.invoice = invoice
                                            nPartInv.t703NoPartInv = noPartSlip
                                            nPartInv.goods = it.goods
                                            nPartInv.operation = it.operation
                                            nPartInv.t703HargaRp = Math.round(it?.t403HargaRp)
                                            nPartInv.t703LandedCost = hargaBeli
                                            nPartInv.t703Jumlah1 = it?.t403Jumlah1
                                            nPartInv.t703Jumlah2 = it.t403Jumlah2
                                            nPartInv.t703DiscRp = (it?.t403DiscRp?it?.t403DiscRp:0)
                                            nPartInv.t703DiscPersen = it.t403DiscPersen
                                            nPartInv.t703TotalRp = Math.round(it.t403Jumlah1 * it?.t403HargaRp - (it.t403DiscRp?it.t403DiscRp : 0))
                                            nPartInv?.t703StaDel = "0"
                                            nPartInv?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
                                            nPartInv?.lastUpdProcess="INSERT"
                                            nPartInv?.dateCreated = datatablesUtilService?.syncTime()
                                            nPartInv?.lastUpdated = datatablesUtilService?.syncTime()
                                            nPartInv.save(flush: true)
                                            nPartInv.errors.each {

                                            }
                                            batas+=1
                                        }

                                    }
                                }
                            }
                            temp = batas
                        }

                        def noWo = ""
                        parts.each {
                            if(!it.isPending || it.isPending.isEmpty() || it.isPending=="0"){
                                if(!noWo.contains(reception.t401NoWO)){
                                    params.noWo = reception.t401NoWO
                                    try {
                                        def journalId = Journal.findByDocNumberAndDescriptionIlikeAndStaDel(params.noWo,"%Journal Pending Keluar%","0")
                                        if(journalId){
                                            def journalDel = Journal.get(journalId.id as Long)
                                            journalDel.staDel = '1'
                                            journalDel.lastUpdProcess = 'DELETE'
                                            journalDel.save(flush: true)

                                            def journalDetail = JournalDetail.findAllByJournal(journalDel)
                                            journalDetail.each {
                                                def jDel = JournalDetail.get(it.id as Long)
                                                jDel.staDel = '1'
                                                jDel.lastUpdProcess = 'DELETE'
                                                jDel.save(flush: true)
                                                jDel.each {

                                                }
                                            }
                                        }
                                        journalPendingKeluarService.createJournal(params)
                                    }catch (Exception e){

                                    }
                                }
                                try {
                                    noWo = reception.t401NoWO
                                    def partRcp = PartsRCP.get(it.id as Long)
                                    partRcp.isPending = "1"
                                    partRcp.save(flush: true)
                                }catch (Exception e){

                                }

                            }
                        }



                        if(ind==billToJob.size()){
                            namaBillTo = "";
                            billToParts.each {
                                if(listPart.indexOf(it)<0){
                                    namaBillTo = it.namaBillTo
                                    billTo = it.billTo
                                    rpSublet = 0
                                    rpJasa = 0
                                    rpPart = 0
                                    wo=""
                                    rpPartCampuran = 0
                                    rpPartToyota = 0
                                    rpOli = 0
                                    rpMaterial = 0
                                    rpDiscount = 0

                                    parts = []
                                    namaCustomer = reception?.historyCustomer?.fullNama
                                    alamat = reception?.historyCustomer?.t182Alamat
                                    npwp = reception?.historyCustomer?.t182NPWP

                                    if(billTo){
                                        parts = PartsRCP.createCriteria().list {
                                            eq("staDel","0");
                                            eq("reception",reception);
                                            eq("t403billTo",billTo)
                                            eq("t403namaBillTo",namaBillTo)
                                            or{
                                                ilike("t403StaTambahKurang","%0%")
                                                and{
                                                    not{
                                                        ilike("t403StaTambahKurang","%1%")
                                                        ilike("t403StaApproveTambahKurang","%1%")
                                                    }
                                                }
                                                and{
                                                    isNull("t403StaTambahKurang")
                                                    isNull("t403StaApproveTambahKurang")
                                                }
                                            }
                                        }
                                    }else{
                                        parts = PartsRCP.createCriteria().list {
                                            eq("staDel","0");
                                            eq("reception",reception);
                                            isNull("t403billTo");
                                            isNull("t403namaBillTo");
                                            or{
                                                ilike("t403StaTambahKurang","%0%")
                                                and{
                                                    not{
                                                        ilike("t403StaTambahKurang","%1%")
                                                        ilike("t403StaApproveTambahKurang","%1%")
                                                    }
                                                }
                                                and{
                                                    isNull("t403StaTambahKurang")
                                                    isNull("t403StaApproveTambahKurang")
                                                }
                                            }
                                        }
                                    }

                                    subtype = "";
                                    subledger = "";

                                    jenis = BillToReception.createCriteria().list {
                                        eq("reception",reception)
                                        if(parts?.size()>0 && namaBillTo){
                                            eq("namaBillTo",parts?.last()?.t403namaBillTo,[ignoreCase: true])
                                            eq("billTo",parts?.last()?.t403billTo,[ignoreCase: true])
                                        }else{
                                            eq("id",-1.toLong())
                                        }
                                        order("id");
                                    }

                                    if(billTo){
                                        namaCustomer = namaBillTo
                                        if(billTo.toString().equalsIgnoreCase("customer")){
                                            def tempData = HistoryCustomer.findByFullNama(namaBillTo)
                                            if(tempData){
                                                alamat = tempData?.t182Alamat
                                                npwp = tempData?.t182NPWP
                                                subtype = "642218"
                                                subledger =tempData.id.toString()
                                            }
                                        }else if(billTo.toString().equalsIgnoreCase("dealer")){
                                            def tempData = DealerPenjual.findByM091NamaDealer(namaBillTo)
                                            def idD = "";
                                            if(tempData){
                                                alamat = tempData?.m091AlamatDealer
                                                npwp = tempData.m091Npwp
                                                if(!Company.findByNamaPerusahaan(namaBillTo)){
                                                    def comp = new Company()
                                                    comp.companyDealer = session.userCompanyDealer
                                                    comp.kodePerusahaan = new GenerateCodeService().codeGenerateSequence("T101_ID",null)
                                                    comp.jenisPerusahaan  = JenisPerusahaan.findByNamaJenisPerusahaanAndStaDel("PT","0");
                                                    comp.namaPerusahaan = namaBillTo
                                                    comp.alamatPerusahaan = "INI DARI SISTEM YG INVOICE BILLTO"
                                                    comp.telp = "0"
                                                    comp.namaDepanPIC = "INI DARI SISTEM YG INVOICE "
                                                    comp.alamatPIC = "INI DARI SISTEM YG INVOICE "
                                                    comp.telpPIC = "0"
                                                    comp.hpPIC = "0"
                                                    comp.noNPWP = npwp
                                                    comp.alamatNPWP = "INI DARI SISTEM YG INVOICE BILLTO"
                                                    comp.staDel = "0"
                                                    comp.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                                    comp.lastUpdProcess = "INSERT"
                                                    comp.dateCreated = datatablesUtilService?.syncTime()
                                                    comp.lastUpdated = datatablesUtilService?.syncTime()
                                                    comp.save(flush: true)
                                                    comp.errors.each {
                                                        println "compBaru : " + it
                                                    }
                                                }
                                                idD = Company.findByNamaPerusahaan(namaBillTo).id
                                                subtype = "1753355"
                                                subledger = idD.toString()
                                            }
                                        }else if(billTo.toString().equalsIgnoreCase("asuransi")){
                                            def tempData = VendorAsuransi.findByM193Nama(namaBillTo)
                                            if(tempData){
                                                alamat = tempData?.m193Alamat
                                                npwp = tempData?.m193Npwp
                                                subtype = "642217"
                                                subledger =tempData.id.toString()
                                            }
                                        }else{
                                            if(!Company.findByNamaPerusahaan(namaBillTo)){
                                                def comp = new Company()
                                                comp.companyDealer = session.userCompanyDealer
                                                comp.kodePerusahaan = new GenerateCodeService().codeGenerateSequence("T101_ID",null)
                                                comp.jenisPerusahaan  = JenisPerusahaan.findByNamaJenisPerusahaanAndStaDel("PT","0");
                                                comp.namaPerusahaan = namaBillTo
                                                comp.alamatPerusahaan = "INI DARI SISTEM YG INVOICE BILLTO"
                                                comp.telp = "0"
                                                comp.namaDepanPIC = "INI DARI SISTEM YG INVOICE "
                                                comp.alamatPIC = "INI DARI SISTEM YG INVOICE "
                                                comp.telpPIC = "0"
                                                comp.hpPIC = "0"
                                                comp.noNPWP = "0"
                                                comp.alamatNPWP = "INI DARI SISTEM YG INVOICE BILLTO"
                                                comp.staDel = "0"
                                                comp.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                                comp.lastUpdProcess = "INSERT"
                                                comp.dateCreated = datatablesUtilService?.syncTime()
                                                comp.lastUpdated = datatablesUtilService?.syncTime()
                                                comp.save(flush: true)
                                                comp.errors.each {
                                                    println "compBaru : " + it
                                                }
                                            }
                                            def tempData = Company.findByNamaPerusahaan(namaBillTo)
                                            if(tempData){
                                                alamat = tempData?.alamatPerusahaan
                                                npwp = tempData?.noNPWP
                                                subtype = "1753355"
                                                subledger =tempData.id.toString()
                                            }
                                        }
                                    }

                                    parts.each {
                                        def hargaSDiskon = it?.t403Jumlah1 && it?.t403HargaRp ? (it?.t403Jumlah1 * it?.t403HargaRp) : 0
                                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                                        if(klas){
                                            if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("MATERIAL")){
                                                rpMaterial+= hargaSDiskon
                                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                                                rpOli+= hargaSDiskon
                                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("TOYOTA")){
                                                rpPartToyota+= hargaSDiskon
                                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN")){
                                                rpPartCampuran+= hargaSDiskon
                                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                                                rpPartCampuran+= hargaSDiskon
                                            }else {
                                                rpMaterial+= hargaSDiskon
                                            }
                                            rpDiscount += it.t403DiscRp ? it.t403DiscRp : 0
                                        }else {
                                            rpMaterial+= hargaSDiskon
                                        }
                                    }

                                    if(namaCustomer==reception?.historyCustomer?.fullNama && !hasUsed){
                                        rpBF = 0
                                        rpOR = 0
                                        hasUsed = true;
                                        def kwitansi = Kwitansi.findByReceptionAndT722StaDel(reception,"0")
                                        if(kwitansi){
                                            def bookingFee = BookingFee.findByKwitansiAndT721StaDel(kwitansi,"0")
                                            if(bookingFee){
                                                if(kwitansi?.t722StaBookingOnRisk?.equalsIgnoreCase("0")){
                                                    rpBF = bookingFee?.t721JmlhBayar
                                                }else{
                                                    rpOR = bookingFee?.t721JmlhBayar
                                                }
                                            }
                                        }
                                    }

                                    invoice = new InvoiceT701()
                                    invoice?.reception = reception
                                    invoice?.historyCustomer = reception?.historyCustomer
                                    invoice?.companyDealer = session.userCompanyDealer
                                    invoice.t701NoInv = generateNoInv('invoice')
                                    invoice.t701JenisInv = namaBillTo ? (jenis?.size()>0 ? jenis?.last()?.jenisBayar?.substring(0,1) : "T") : "T"
                                    jpbTek = JPB.findByReceptionAndStaDel(reception, "0")
                                    if(jpbTek){
                                        teeeknisi = jpbTek?.namaManPower?.t015NamaLengkap?.toString()
                                    }else{
                                        try {
                                            def p = PickingSlip.findByReceptionAndStaDel(reception,"0").t141NamaTeknisi.trim()
                                            teeeknisi = NamaManPower.findByT015NamaLengkapLike("%"+p+"%").t015NamaLengkap.toString()
                                        }catch (Exception e){

                                        }
                                    }
                                    invoice?.t701Teknisi = teeeknisi
                                    invoice?.t701AdmBilling = org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    invoice?.t701JasaRp = Math.round(rpJasa)
                                    invoice?.t701SubletRp = Math.round(rpSublet)
                                    invoice?.t701PartsRp = Math.round(rpPartCampuran) + Math.round(rpPartToyota)
                                    invoice?.t701PartsRpCampuran = Math.round(rpPartCampuran)
                                    invoice?.t701PartsRpToyota = Math.round(rpPartToyota)
                                    invoice?.t701OliRp = Math.round(rpOli)
                                    invoice?.t701MaterialRp = Math.round(rpMaterial)
                                    try {
                                        invoice?.t701NPWP = npwp
                                        invoice?.t701Subtype = subtype
                                        invoice?.t701Subledger= subledger
                                    }catch (Exception e){}
                                    adm = BiayaAdministrasi.findAllByTglBerlakuLessThanEquals(new Date())
                                    nilaiAdm = 0
                                    if(adm){
                                        nilaiAdm = adm?.last()?.biaya
                                    }
                                    invoice?.t701AdmRp = Math.round(nilaiAdm)
                                    invoice?.t701SubTotalRp = Math.round(rpJasa) + Math.round(rpSublet) + (Math.round(rpPartCampuran) + Math.round(rpPartToyota)) + Math.round(rpOli) + Math.round(rpMaterial) + Math.round(invoice?.t701AdmRp)
                                    invoice?.t701TotalDiscRp = Math.round(rpDiscount)
                                    invoice?.t701TotalRp = Math.round(invoice?.t701SubTotalRp - invoice?.t701TotalDiscRp)
                                    invoice?.t701PPnRp = Math.round(10 / 100 * invoice?.t701TotalRp)
                                    nilaiMaterai = 0
                                    materai = Materai.findAllByM705TglBerlakuLessThanEqualsAndM705Nilai1LessThanEqualsAndM705Nilai2GreaterThanEquals(new Date(),invoice?.t701TotalRp,invoice?.t701TotalRp)
                                    if(materai){
                                        nilaiMaterai = materai?.last()?.m705NilaiMaterai
                                    }
                                    invoice?.t701MateraiRp = Math.round(nilaiMaterai)
                                    invoice?.t701TotalInv = Math.round(invoice?.t701TotalRp + invoice?.t701MateraiRp + invoice?.t701PPnRp)
                                    invoice?.t701BookingFee = Math.round(rpBF)
                                    invoice?.t701OnRisk = Math.round(rpOR)
                                    invoice?.t701TotalBayarRp = Math.round(invoice.t701TotalInv - rpBF - rpOR)
                                    invoice?.t701Nopol = reception?.historyCustomerVehicle?.fullNoPol
                                    invoice?.t701TglJamInvoice = datatablesUtilService?.syncTime()
                                    invoice?.t701Customer = namaCustomer
                                    invoice?.t701Alamat = alamat
                                    invoice?.t701NPWP = npwp
                                    invoice?.t701IntExt = "1"
                                    invoice?.t701Tipe = reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.toUpperCase()?.contains("G") ? "0" : "1"
                                    invoice?.t701StaSettlement = "0"
                                    invoice?.t701StaDel = "0"

                                    bilTotal = invoice.t701TotalBayarRp
                                    invoice?.t701TotalInvTerbilang = moneyUtil.convertMoneyToWords(bilTotal)
                                    invoice?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    invoice?.lastUpdProcess="INSERT"
                                    invoice?.dateCreated= datatablesUtilService?.syncTime()
                                    invoice?.lastUpdated= datatablesUtilService?.syncTime()
                                    def ceks = cekIsExist(invoice?.t701NoInv);
                                    if(ceks){
                                        invoice.t701NoInv = generateNoInv('invoice')
                                    }
                                    invoice.save(flush: true)
                                    invoice.errors.each { }
                                    if(invoice?.hasErrors()){
                                        staError=true
                                    }
                                    noPartSlip = generateNoInv("partslip")
                                    batas = 0;
                                    def klas = ["PARTS", "BAHAN"]
                                    klas.each { kl ->
                                        parts.each {
                                            def klasifikasi = null
                                            def tampil = false
                                            if (kl=="PARTS"){
                                                klasifikasi = KlasifikasiGoods.findByGoods(it?.goods)?.franc?.m117NamaFranc
                                                if(klasifikasi.toUpperCase().contains(kl.toUpperCase()) ||
                                                        klasifikasi.toUpperCase().contains("OLI")){
                                                    tampil = true
                                                }
                                            }else{
                                                klasifikasi = KlasifikasiGoods.findByGoods(it?.goods)?.franc?.m117NamaFranc
                                                if(!klasifikasi.toUpperCase().contains("PARTS") &&
                                                        !klasifikasi.toUpperCase().contains("OLI")){
                                                    tampil = true
                                                }
                                            }
                                            if (tampil == true) {
                                                if(batas == 7 ){
                                                    noPartSlip = generateNoInv("partslip")
                                                    batas = 0
                                                }
                                                def goodsHargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.goods,session?.userCompanyDealer,"0")
                                                def hargaBeli = 0
                                                if(goodsHargaBeli){
                                                    hargaBeli = goodsHargaBeli?.t150Harga
                                                }
                                                def nPartInv = new PartInv()
                                                nPartInv.companyDealer = session.userCompanyDealer
                                                nPartInv.invoice = invoice
                                                nPartInv.t703NoPartInv = noPartSlip
                                                nPartInv.goods = it.goods
                                                nPartInv.operation = it.operation
                                                nPartInv.t703HargaRp = Math.round(it?.t403HargaRp)
                                                nPartInv.t703LandedCost = hargaBeli
                                                nPartInv.t703Jumlah1 = it.t403Jumlah1
                                                nPartInv.t703Jumlah2 = it.t403Jumlah2
                                                nPartInv.t703DiscRp = (it?.t403DiscRp?it?.t403DiscRp:0)
                                                nPartInv.t703DiscPersen = it.t403DiscPersen
                                                nPartInv.t703TotalRp = Math.round(it.t403Jumlah1 * it?.t403HargaRp - (it.t403DiscRp ? it.t403DiscRp : 0))
                                                nPartInv?.t703StaDel = "0"
                                                nPartInv?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
                                                nPartInv?.lastUpdProcess="INSERT"
                                                nPartInv?.dateCreated = datatablesUtilService?.syncTime()
                                                nPartInv?.lastUpdated= datatablesUtilService?.syncTime()
                                                nPartInv.save(flush: true)
                                                nPartInv.errors.each { }
                                                batas+=1
                                            }
                                        }
                                    }

                                    noWo = ""
                                    parts.each {
                                        if(!it.isPending || it.isPending.isEmpty() || it.isPending=="0"){
                                            if(!noWo.contains(reception.t401NoWO)){
                                                params.noWo = reception.t401NoWO
                                                try {
                                                    def journalId = Journal.findByDocNumberAndDescriptionIlikeAndStaDel(params.noWo,"%Journal Pending Keluar%","0")
                                                    if(journalId){
                                                        def journalDel = Journal.get(journalId.id as Long)
                                                        journalDel.staDel = '1'
                                                        journalDel.lastUpdProcess = 'DELETE'
                                                        journalDel.save(flush: true)

                                                        def journalDetail = JournalDetail.findAllByJournal(journalDel)
                                                        journalDetail.each {
                                                            def jDel = JournalDetail.get(it.id as Long)
                                                            jDel.staDel = '1'
                                                            jDel.lastUpdProcess = 'DELETE'
                                                            jDel.save(flush: true)
                                                            jDel.each {

                                                            }
                                                        }
                                                    }
                                                    journalPendingKeluarService.createJournal(params)
                                                }catch (Exception e){

                                                }
                                            }
                                            try {
                                                noWo = reception.t401NoWO
                                                def partRcp = PartsRCP.get(it.id as Long)
                                                partRcp.isPending = "1"
                                                partRcp.save(flush: true)
                                            }catch (Exception e){

                                            }
                                        }
                                    }
                                }
                            }
                            if(!hasUsed){
                                hasUsed = true;
                                rpBF = 0;rpOR = 0;
                                def kwitansi = Kwitansi.findByReceptionAndT722StaDel(reception,"0")
                                if(kwitansi){
                                    def bookingFee = BookingFee.findByKwitansiAndT721StaDel(kwitansi,"0")
                                    if(bookingFee){
                                        if(kwitansi?.t722StaBookingOnRisk?.equalsIgnoreCase("0")){
                                            rpBF = bookingFee?.t721JmlhBayar
                                        }else{
                                            rpOR = bookingFee?.t721JmlhBayar
                                        }
                                    }
                                }
                                doChangeBFOR(reception,rpOR,rpBF);
                            }
                        }
                    }
                    if(staError){
                        reception?.t401StaInvoice = "0"
                        reception?.lastUpdated = datatablesUtilService?.syncTime()
                        reception?.save(flush: true)
                        def invDelete = InvoiceT701.findAllByReceptionAndT701StaDel(reception,"0")
                        invDelete.each {
                            def jobsDelete = JobInv.findAllByInvoiceAndStaDel(it,"0")
                            jobsDelete.each {
                                it.staDel = "1"
                                it.lastUpdated = datatablesUtilService?.syncTime()
                                it.save()
                            }
                            def partsDelete = PartInv.findAllByInvoiceAndT703StaDel(it,"0")
                            partsDelete.each {
                                it.lastUpdated = datatablesUtilService?.syncTime()
                                it.t703StaDel = "1"
                                it.save()
                            }
                            it.t701StaDel = "1"
                            it.lastUpdated = datatablesUtilService?.syncTime()
                            it.save()
                        }
                    }else {
                        reception?.t401StaInvoice = "0"
                        reception?.lastUpdated = datatablesUtilService?.syncTime()
                        reception?.save(flush: true)
                    }
                }
            }
        }
        render hasil
    }

    def doChangeBFOR(Reception reception,def rpOR,def rpBF){
        def invoice = InvoiceT701.createCriteria().get {
            eq("reception",reception)
            eq("t701StaDel","0")
            eq("companyDealer",session?.userCompanyDealer)
//            eq("t701JenisInv","T",[ignoreCase:true])
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            order("t701TglJamInvoice")
            maxResults(1);
        }
        if(!invoice){
            invoice = InvoiceT701.createCriteria().get {
                eq("reception",reception)
                eq("t701StaDel","0")
                eq("companyDealer",session?.userCompanyDealer)
                isNull("t701NoInv_Reff");
                or{
                    isNull("t701StaApprovedReversal")
                    eq("t701StaApprovedReversal","1")
                }
                gt("t701TotalBayarRp",0.toDouble())
                order("t701TglJamInvoice")
                maxResults(1);
            }
        }
        if(invoice){
            invoice.t701BookingFee = Math.round(rpBF)
            invoice.t701OnRisk = Math.round(rpOR)
            invoice.t701TotalBayarRp = Math.round(invoice.t701TotalInv - (rpBF+rpOR))
            int terbilang = invoice.t701TotalInv
            invoice.t701TotalInvTerbilang = moneyUtil.convertMoneyToWords(terbilang)
            invoice.save(flush: true)
        }
    }

    def doGenerateCreditNote(){
        InvoiceT701 invoiceT701 = InvoiceT701.get(params?.idInv)
        if(invoiceT701?.t701StaApprovedReversal && invoiceT701?.t701StaApprovedReversal=="2"){
            render "waiting"
            return
        }
        if(invoiceT701?.t701StaApprovedReversal && invoiceT701?.t701StaApprovedReversal=="1"){
            render "unapproved"
            return
        }
        if(invoiceT701?.t701StaApprovedReversal && invoiceT701?.t701StaApprovedReversal=="0"){
            def invReff = InvoiceT701?.findByT701NoInv_ReffIlikeAndT701StaDel("%"+invoiceT701?.t701NoInv+"%","0")
            if(invReff){
                render "exist"
                return
            }
        }
        if(!invoiceT701?.t701StaApprovedReversal){
            render "noapproval"
            return
        }
        InvoiceT701 nInv = new InvoiceT701()
        nInv?.reception = invoiceT701?.reception
        nInv?.t701NoInv_Reff = invoiceT701?.t701NoInv
        nInv?.historyCustomer = invoiceT701?.historyCustomer
        nInv?.companyDealer = invoiceT701?.companyDealer
        nInv?.t701StaApprovedReversal = "2"
        nInv.t701NoInv = generateNoInv('invoice')
        nInv.t701JenisInv = invoiceT701?.t701JenisInv
        nInv?.t701JasaRp = -invoiceT701?.t701JasaRp
        nInv?.t701PartsRp = -invoiceT701?.t701PartsRp
        nInv?.t701OliRp = -invoiceT701?.t701OliRp
        nInv?.t701MaterialRp = -invoiceT701?.t701MaterialRp
        nInv?.t701SubTotalRp = -invoiceT701?.t701SubTotalRp
        nInv?.t701SubletRp = -invoiceT701?.t701SubletRp
        nInv?.t701TotalDiscRp = -invoiceT701?.t701TotalDiscRp
        nInv?.t701TotalRp = -invoiceT701?.t701TotalRp
        nInv?.t701MateraiRp = -invoiceT701?.t701MateraiRp
        nInv?.t701AdmRp = -invoiceT701?.t701AdmRp
        nInv?.t701TotalInv = -invoiceT701?.t701TotalInv
        nInv?.t701BookingFee = -invoiceT701?.t701BookingFee
        nInv?.t701OnRisk = -invoiceT701?.t701OnRisk
        nInv?.t701TotalBayarRp = -invoiceT701?.t701TotalBayarRp
        nInv?.t701PPnRp = -invoiceT701?.t701PPnRp
        nInv?.t701Nopol = invoiceT701?.t701Nopol
        nInv?.t701TglJamInvoice = datatablesUtilService?.syncTime()
        nInv?.t701Customer = invoiceT701?.t701Customer
        nInv?.t701Alamat = invoiceT701?.t701Alamat
        nInv?.t701NPWP = invoiceT701?.t701NPWP
        nInv?.t701IntExt = invoiceT701?.t701IntExt
        nInv?.t701Tipe = invoiceT701?.t701Tipe
        nInv?.t701StaSettlement = invoiceT701?.t701StaSettlement
        nInv?.t701StaDel = "0"
        nInv?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
        nInv?.lastUpdProcess="INSERT"
        nInv?.dateCreated= datatablesUtilService?.syncTime()
        nInv?.lastUpdated= datatablesUtilService?.syncTime()
        def cek = cekIsExist(nInv?.t701NoInv);
        if(cek){
            nInv.t701NoInv = generateNoInv('invoice')
        }
        nInv.save(flush: true)
        nInv.errors.each { }

        def jobInv = JobInv.createCriteria().list {
            eq("staDel","0")
            invoice{
                eq("id", params?.idInv as long)
            }
        }

        jobInv.each {
            def nJob = new JobInv()
            nJob?.companyDealer = session.userCompanyDealer
            nJob?.invoice = nInv
            nJob?.reception = it?.reception
            nJob?.operation = it?.operation
            nJob?.t702HargaRp = it?.t702HargaRp ? -it?.t702HargaRp : 0
            nJob?.t702DiscRp = it?.t702DiscRp ? -it?.t702DiscRp : 0
            nJob?.t702DiscPersen = it?.t702DiscPersen
            nJob?.t702TotalRp = it?.t702TotalRp ? -it?.t702TotalRp : 0
            nJob?.staDel = "0"
            nJob?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            nJob?.lastUpdProcess="INSERT"
            nJob?.dateCreated= datatablesUtilService?.syncTime()
            nJob?.lastUpdated= datatablesUtilService?.syncTime()
            nJob?.save(flush: true)
            nJob?.errors?.each { }

        }

        def partInv = PartInv.createCriteria().list {
            eq("t703StaDel","0")
            invoice{
                eq("id", params?.idInv as long)
            }
        }

        partInv.each {
            def nPartInv = new PartInv()
            nPartInv.companyDealer = session.userCompanyDealer
            nPartInv.invoice = nInv
            nPartInv.t703NoPartInv = it?.t703NoPartInv
            nPartInv.goods = it?.goods
            nPartInv.operation = it?.operation
            nPartInv.t703HargaRp = it?.t703HargaRp ? -it?.t703HargaRp : 0
            nPartInv.t703LandedCost = it?.t703LandedCost ? -it?.t703LandedCost : 0
            nPartInv.t703Jumlah1 = it?.t703Jumlah1
            nPartInv.t703Jumlah2 = it?.t703Jumlah2
            nPartInv.t703DiscRp = it?.t703DiscRp ? -it?.t703DiscRp : 0
            nPartInv.t703DiscPersen = it?.t703DiscPersen
            nPartInv.t703TotalRp = it?.t703TotalRp ? -it?.t703TotalRp : 0
            nPartInv?.t703StaDel = "0"
            nPartInv?.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            nPartInv?.lastUpdProcess="INSERT"
            nPartInv?.dateCreated= datatablesUtilService?.syncTime()
            nPartInv?.lastUpdated= datatablesUtilService?.syncTime()
            nPartInv.save(flush: true)
            nPartInv.errors.each { }
        }
        try{
            def journalAllId = Journal.findAllByDocNumber(invoiceT701.t701NoInv as String)
            journalAllId.each {
                def jId = Journal.get(it.id)
                jId.staDel = '1'
                jId.save(flush: true)
                def jd = JournalDetail.findAllByJournal(jId)*.delete()
            }
        }catch(Exception e){

        }
        def recept = invoiceT701?.reception
        recept?.t401StaInvoice = null
        recept?.save(flush: true)
        render "ok"
    }

    def cekWarrantyKM(Reception reception){
        def hasil = false
        if(reception.t401KmSaatIni<=10000){
            hasil=true
        }
        return hasil
    }

    def cekWarrantyBulan(HistoryCustomerVehicle historyCustomerVehicle){
        def hasil = false
        def vehicle = historyCustomerVehicle
        if(vehicle?.t183TglDEC?.format("dd")?.toInteger()<=new Date().format("dd").toInteger() && vehicle?.t183TglDEC?.format("MM")?.toInteger()+6<=new Date().format("MM").toInteger()){
            hasil = true
        }else{
            if(vehicle?.t183TglDEC?.format("MM")?.toInteger()+6<new Date().format("MM").toInteger()){
                hasil = true
            }
        }
        return hasil
    }

    def cekIsExist(def noInv){
        def exist = false;
        def inv = InvoiceT701.findByCompanyDealerAndT701NoInvIlike(session?.userCompanyDealer,"%"+noInv+"%");
        if(inv){
            exist = true;
        }
        return exist
    }

    def generateNoInv(String kelas){
        CompanyDealer companyDealer = session.userCompanyDealer
        def thn = new Date().format("yy")
        def noInv = companyDealer?.m011ID+"."+thn+"."
        if(kelas.equalsIgnoreCase("invoice")){
            def inv = InvoiceT701.createCriteria().list {
                eq("t701StaDel","0")
                eq("companyDealer",companyDealer)
                ilike("t701NoInv","%"+noInv+"%")
                order("t701NoInv","asc")
            }
            if(inv.size()<1){
                noInv += "0000000001"
            }else{
                def noTemp = inv?.last()?.t701NoInv?.substring(inv?.last()?.t701NoInv?.lastIndexOf(".")+1)?.toLong()
                noTemp = noTemp + 1
                for(int a=noTemp.toString().length();a<=9;a++){
                    noInv+="0"
                }
                noInv+=noTemp.toString()
            }
        }else{

            def partInv = PartInv.findAllByCompanyDealerAndT703NoPartInvIlikeAndT703StaDel(companyDealer,"%."+thn+".%","0", [sort: "dateCreated"])
            if(partInv.size()<1){
                noInv += "00001"
            }else{
                def noLagi = partInv?.last()?.t703NoPartInv?.substring(partInv?.last()?.t703NoPartInv?.lastIndexOf(".")+1)

                def noTemp = noLagi.toInteger() + 1
                for(int a=noTemp.toString().length();a<=4;a++){
                    noInv+="0"
                }
                noInv+=noTemp.toString()
            }
        }
        return noInv
    }

    def sendApprovalReversal(){
        def rec = Reception.findByT401NoWO(params.noWo)
        def invoice = InvoiceT701.findByT701NoInvAndT701StaDel(params.noInvoice,"0")
        rec?.t401StaInvoice = "2"
        rec?.save(flush: true)
        invoice.t701StaApprovedReversal = "2"
        invoice.save(flush: true)
        def approval = new ApprovalT770(
                t770FK:invoice.id as String,
                kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.INVOICE_REVERSAL),
                t770NoDokumen: invoice?.t701NoInv,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                companyDealer: session?.userCompanyDealer,
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{  }
        render "ok"
    }

    def doJournalIvoiceKredit(InvoiceT701 invoice){
        def subType = null
        def subLedger = null
        def nama = invoice?.t701Customer
        def cabang = false
        if(nama?.toUpperCase()?.contains("HASJRAT") || nama?.toUpperCase()?.contains("KOMBOS")){
            cabang = true
        }
        if(invoice.t701JenisInv=="K" && (invoice?.t701PrintKe<=1 || invoice?.t701PrintKe==null)){
            params.noInvoice = invoice?.t701NoInv?.trim()
            def duedate = new Date()
            duedate.setMonth(duedate.getMonth()+1)
            try {
                if(invoice.t701Subtype){
                    subType = SubType?.findById(invoice.t701Subtype as Long)
                    subLedger =  invoice.t701Subledger
                }else {
                    def cari = VendorAsuransi.findByM193NamaIlikeAndM193AlamatAndStaDel("%"+invoice?.t701Customer+"%",invoice?.t701Alamat,"0")
                    def found = false;
                    if(cari && !found){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("%ASURANSI","0")
                        subLedger = cari?.id
                        found = true;
                    }
                    def cari4 = Company.findByNamaPerusahaanIlikeAndStaDel("%"+invoice?.t701Customer+"%","0")
                    if(cari4 && !found){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER COMPANY","0")
                        subLedger = cari4?.id
                        found = true;
                    }
                    def cari2 = CompanyDealer.findByM011NamaWorkshopIlikeAndStaDel(invoice?.t701Customer,"0")
                    if(cari2 && !found){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("%CABANG","0")
                        subLedger = cari2?.id
                        found = true;
                    }
                    def cari3 = HistoryCustomer.findByFullNamaIlikeAndT182AlamatAndStaDel("%"+invoice?.t701Customer+"%",invoice?.t701Alamat,"0")
                    if(cari3 && !found){
                        subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER","0")
                        subLedger = cari3?.id
                        found = true;
                    }
                    if(found == false){
                        def comp = new Company()
                        comp.companyDealer = invoice?.companyDealer
                        comp.kodePerusahaan = new GenerateCodeService().codeGenerateSequence("T101_ID",null)
                        comp.jenisPerusahaan  = JenisPerusahaan.findByNamaJenisPerusahaanAndStaDel("PT","0");
                        comp.namaPerusahaan = invoice?.t701Customer.toString()
                        comp.alamatPerusahaan = "INI DARI SISTEM YG INVOICE"
                        comp.telp = "0"
                        comp.namaDepanPIC = "INI DARI SISTEM YG INVOICE"
                        comp.alamatPIC = "INI DARI SISTEM YG INVOICE"
                        comp.telpPIC = "0"
                        comp.hpPIC = "0"
                        comp.alamatNPWP = "INI DARI SISTEM YG INVOICE"
                        comp.staDel = "0"
                        comp.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        comp.lastUpdProcess = "INSERT"
                        comp.dateCreated = datatablesUtilService?.syncTime()
                        comp.lastUpdated = datatablesUtilService?.syncTime()
                        comp.save(flush: true)
                        comp.errors.each {
                            println "compBaru : " + it
                        }
                        if(!comp?.hasErrors()){
                            subType = SubType?.findByDescriptionIlikeAndStaDel("%CUSTOMER COMPANY","0")
                            subLedger = comp?.id
                        }
                    }
                    try {
                        InvoiceT701 invUbh = InvoiceT701.get(invoice.getId())
                        invUbh.t701Subtype = subType.id.toString()
                        invUbh.t701Subledger = subLedger
                        invUbh.save(flush: true)
                    }catch (Exception e){

                    }
                }

            }catch (Exception e){

            }

            try {
                def j = Journal.findAllByDocNumber(invoice.t701NoInv)
                j*.staDel = "1"
                j*.save(flush: true)

                def jd = JournalDetail.findAllByJournalInList(j)
                jd*.staDel = "1"
                jd*.save(flush: true)

            }catch (Exception e){

            }

            if(Collection.findByInvoiceT071(invoice)){
                def colD = Collection.get(Collection.findByInvoiceT071(invoice).id);
                colD.delete();
            }
            def koleksi = new com.kombos.finance.Collection()
            koleksi.companyDealer = session.userCompanyDealer
            koleksi.collectionDate = invoice.t701TglJamInvoice
            koleksi.amount = invoice?.t701TotalBayarRp
            koleksi.dueDate  = duedate
            koleksi.paidAmount = 0
            koleksi.paymentStatus = "BELUM"
            koleksi.staDel = "0"
            koleksi.subType = subType
            koleksi.subLedger = subLedger
            koleksi.createdBy = "USER"
            koleksi.lastUpdProcess  = "INSERT"
            koleksi.invoiceT071 = invoice
            koleksi.dateCreated = invoice.t701TglJamInvoice
            koleksi.lastUpdated =  datatablesUtilService?.syncTime()
            koleksi.save(flush: true)

            params.cabang = cabang
            invoiceKreditJournalService.createJournal(params);

            params?.noWo = invoice?.reception?.t401NoWO
            params?.noInv = invoice?.t701NoInv
            params?.companyDealer = session?.userCompanyDealer
            def cekPart = PartInv.findAllByInvoiceAndT703StaDel(invoice,"0");

            if(cekPart.size()>0){
                pemakaianPartsJournalService.createJournal(params)
            }
            if(invoice?.t701SubletRp && invoice?.t701SubletRp>0){
                params.totalBiaya = invoice?.t701SubletRp
                params.docNumber = invoice?.reception?.t401NoWO
                HPPSubletJournalService.createJournal(params)
            }
        }
    }

    def previewInv(){
        def file = null
        def namaPdf = ""

        InvoiceT701 invoiceT701 = InvoiceT701.get(params.idInv)
        try {
            def not = Reception.get(invoiceT701?.reception?.id)
            not?.t401TglJamNotifikasi = datatablesUtilService.syncTime()
            not?.save(flush: true)
        }catch (Exception e){

        }
        if(invoiceT701.id == params.idInv as long && (!invoiceT701.t701StaApprovedReversal || (invoiceT701?.t701StaApprovedReversal && (invoiceT701?.t701StaApprovedReversal=="2" || invoiceT701?.t701StaApprovedReversal=="0")))){
            namaPdf = "Preview_Invoice_"
            if(params?.staPrint && params?.staPrint!="undefined"){
                namaPdf = "Invoice_"+invoiceT701.t701NoInv+"_"

                try {
                    println("printke" + invoiceT701?.t701PrintKe)
                    invoiceT701?.t701TglJamCetak = datatablesUtilService.syncTime()
                    invoiceT701?.t701PrintKe = invoiceT701?.t701PrintKe != null ? invoiceT701?.t701PrintKe + 1 : 1
                    invoiceT701.save(flush: true)
                }catch (Exception e){}

                if(!invoiceT701?.t701NoInv_Reff || invoiceT701?.t701TotalBayarRp>0.toDouble()){
                    doJournalIvoiceKredit(invoiceT701)
                }

//                HistoryCustomer hc = HistoryCustomer.findByFullNamaIlikeAndT182AlamatIlike("%"+invoiceT701?.t701Customer?.trim()+"%","%"+invoiceT701?.t701Alamat?.trim()+"%")
//                if(hc && invoiceT701?.t701PrintKe<=1){
//                    // Ucapan selamat
//                    Date today = new Date()
//                    def waktu =  today.format("HH:mm")
//                    def pisah = waktu.split(":")
//                    def hour = pisah[0].toInteger()
//                    def minute = pisah[1].toInteger()
//
//                    def greeting = ""
//
//                    if (hour > 0 && hour < 10 ){
//                        if (minute >0 && minute<60){
//                            greeting="Selamat Pagi";
//                        }
//                    }else if (hour >= 10 && hour < 15 ){
//                        if (minute > 0 && minute<60){
//                            greeting="Selamat Siang";
//                        }
//                    }else if (hour >= 15 && hour < 18 ){
//                        if (minute > 0 && minute<60){
//                            greeting="Selamat Sore";
//                        }
//                    }else if (hour >= 18 && hour <= 24 ){
//                        if (minute > 0 && minute < 60){
//                            greeting="Selamat Malam";
//                        }
//                    }else {
//                        greeting="Error";
//                    }
//
//                    def recept = Reception.get(invoiceT701?.reception?.id)
//                    def isSP = CustomerIn.findByReceptionAndStaDel(recept, "0")
//
//                    if (isSP.t400StaShow == "0"){
//                        def tplSms = Retention.findByM207NamaRetentionIlikeAndStaDel("%Pemberitahuan Selesai Invoice%", "0")
//                        //Ucapan Selamat
//                        def dest = hc.t182NoHp
//                        def message = tplSms?.m207FormatSms?.replaceAll("<Greeting>", greeting)
//                        message = message.replaceAll("<WorkshopTAM>", session.userCompanyDealer.m011NamaWorkshop)
//                        message = message.replaceAll("<Nopol>", invoiceT701.t701Nopol)
//
//                        if(dest){
//                            def antriSms = new AntrianSms()
//                            antriSms.noTujuan = dest
//                            antriSms.isiPesan = message
//                            antriSms.status = 0.toLong()
//                            antriSms.waktuAntri = new Date()
//                            antriSms.errors.each {
//                            }
//                            antriSms.save(flush: true)
//                            try {
//                                smsService.performSendSms(dest, message)
//                            }catch (Exception e){
//                                println "tidak kriim SMS"
//                            }
//                        }
//                        //Kirim Sms
//                    }else {
//                        println("WO ini adalah Service Point, tidak menerima SMS")
//                    }
//                }

            }
        }else{
            namaPdf = "Credit_Note_"
        }

        def reportData = invoiceReportData(params)

        def reportDef = new JasperReportDef(name:'Invoice1.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        file = File.createTempFile(namaPdf,".pdf")

        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }

    def invoiceReportData(def params){
        def reportData = new ArrayList();

        InvoiceT701 invoiceT701 = InvoiceT701.get(params.idInv)

        def data = [:]

//        data.put("logo", invoiceT701?.companyDealer?.m011Logo ? invoiceT701?.companyDealer?.m011Logo : "")
        data.put("creditNotes", invoiceT701?.t701JenisInv == "C" ? "CREDIT NOTES" : "")
        data.put("noInvoice", invoiceT701?.t701NoInv)
        data.put("tanggalInvoice", invoiceT701?.t701TglJamInvoice ? invoiceT701?.t701TglJamInvoice?.format("dd MMM yyyy") : "-")
        data.put("npwp", invoiceT701?.companyDealer?.m011NPWP ? invoiceT701?.companyDealer?.m011NPWP : "-")
        data.put("pkp", invoiceT701?.companyDealer?.m011PKP ? invoiceT701?.companyDealer?.m011PKP : "-")
        data.put("customerName", invoiceT701?.t701Customer)
        data.put("customerAddress", invoiceT701?.t701Alamat)
        data.put("customerNpwp", invoiceT701?.t701NPWP)
        data.put("noPolisi", invoiceT701?.t701Nopol)

        HistoryCustomerVehicle hcv = HistoryCustomerVehicle.findByFullNoPolAndStaDel(invoiceT701.t701Nopol, "0")

        data.put("model",  invoiceT701?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel)
        data.put("noMesin", invoiceT701?.reception?.historyCustomerVehicle?.t183NoMesin)
        data.put("noRangka", invoiceT701?.reception?.historyCustomerVehicle?.customerVehicle?.t103VinCode)
        data.put("noOrder", invoiceT701?.reception?.t401NoWO)
        data.put("serviceAdvisor", invoiceT701?.reception?.t401NamaSA)

        data.put("intExt", invoiceT701?.t701IntExt == "1" ? "EXT" : "INT")
        data.put("technician", invoiceT701?.t701Teknisi)
        data.put("printedDate", invoiceT701?.t701TglJamCetak ? invoiceT701?.t701TglJamCetak?.format("dd/MM/yyyy HH:mm:ss") : new Date().format("dd/MM/yyyy HH:mm"))
        data.put("billingAdmin", invoiceT701?.t701AdmBilling)

        data.put("jasaLabor", conversi.toRupiahInvoice(invoiceT701?.t701JasaRp))
        data.put("parts", conversi.toRupiahInvoice(invoiceT701?.t701PartsRp))
        data.put("oil", conversi.toRupiahInvoice(invoiceT701?.t701OliRp))
        data.put("material", conversi.toRupiahInvoice(invoiceT701?.t701MaterialRp))
        data.put("sublet", conversi.toRupiahInvoice(invoiceT701?.t701SubletRp))
        data.put("administrasi", conversi.toRupiahInvoice(invoiceT701?.t701AdmRp))
        data.put("subTotal", conversi.toRupiahInvoice(invoiceT701?.t701SubTotalRp))

        def c = JobInv.createCriteria()
        def results = c.list{
            eq("staDel","0")
            invoice{
                eq("id",params.idInv.toLong())
            }
        }

        def baris = []
        def discJasa = 0
        def staFreeJasa = false
        def staFree = false
        def daftarJob = ""
        results.each {
            def jb = JobRCP.findByReceptionAndOperationAndStaDel(it?.reception,it?.operation,"0");
            if(!jb?.t402namaBillTo || it.invoice.t701JenisInv.contains("T")){
                staFreeJasa = true
                daftarJob += it?.operation?.m053NamaOperation + ","
            }
            discJasa += it?.t702DiscRp ? it?.t702DiscRp : 0

            baris << [
                    desc : it?.operation?.m053NamaOperation,
                    rate : jb?.t402Rate,
                    rupiah : conversi.toRupiahInvoice(it?.t702HargaRp)
            ]
        }
        if(results.size()<1){
            def jbs = JobRCP.createCriteria().list {
                eq("reception", invoiceT701.reception)
                eq("staDel","0")
                or{
                    ilike("t402StaTambahKurang","0")
                    and{
                        not{
                            ilike("t402StaTambahKurang","%1%")
                            ilike("t402StaApproveTambahKurang","%1%")
                        }
                    }
                    and {
                        isNull("t402StaTambahKurang")
                        isNull("t402StaApproveTambahKurang")
                    }
                }
            }
            jbs.each {
                    staFreeJasa = true
                    staFree = true
                    daftarJob += it?.operation?.m053NamaOperation + ","
            }
        }
        try {
            if(staFreeJasa == true){
                def jbs = JobRCP.createCriteria().list {
                    eq("reception", invoiceT701.reception)
                    eq("staDel","0")
                    or{
                        ilike("t402StaTambahKurang","0")
                        and{
                            not{
                                ilike("t402StaTambahKurang","%1%")
                                ilike("t402StaApproveTambahKurang","%1%")
                            }
                        }
                        and {
                            isNull("t402StaTambahKurang")
                            isNull("t402StaApproveTambahKurang")
                        }
                    }
                }

                jbs.each {
                    if(staFree == true){
                        if( it.operation.m053NamaOperation.toUpperCase().contains("SB")){
                            baris << [
                                    desc : it?.operation?.m053NamaOperation + " (Free Jasa)",
                                    rate : it?.t402Rate,
                                    rupiah : conversi.toRupiahInvoice(0)
                            ]
                        }
                    }else{
                        if(!daftarJob.toUpperCase().contains(it.operation.m053NamaOperation.toUpperCase())
                                && it.operation.m053NamaOperation.toUpperCase().contains("SB")){
                            baris << [
                                    desc : it?.operation?.m053NamaOperation + " (Free Jasa)",
                                    rate : it?.t402Rate,
                                    rupiah : conversi.toRupiahInvoice(0)
                            ]
                        }
                    }
                }
            }
        }catch (Exception e){}

        data.put("jasaLaborTabel", baris)
        data.put("discJasa", conversi.toRupiahInvoice(discJasa))

        def nPart = ""
        def partSlip = PartInv.createCriteria().list {
            invoice{
                eq("id", params.idInv.toLong())
            }
            eq("t703StaDel", "0")

            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t703NoPartInv", "t703NoPartInv")
            }
            order("t703NoPartInv")
        }

        int ind=1

        partSlip.each {
            if(ind==partSlip.size()){
                nPart+=it?.t703NoPartInv
            }else{
                nPart+=it?.t703NoPartInv+", "
            }
            ind++;
        }

        def p = PartInv.createCriteria().list {
            eq("t703StaDel","0")
            invoice{
                eq("id",params.idInv.toLong())
            }
        }

        def discPart = 0
        p.each {
            discPart += it?.t703DiscRp ? it?.t703DiscRp : 0
        }

        data.put("discPart", conversi.toRupiahInvoice(discPart))
        data.put("total", conversi.toRupiahInvoice(invoiceT701?.t701TotalRp))
        data.put("ppn", conversi.toRupiahInvoice(invoiceT701?.t701PPnRp))
        data.put("materai", conversi.toRupiahInvoice(invoiceT701?.t701MateraiRp))
        data.put("totalJasaLabor", conversi.toRupiahInvoice(invoiceT701?.t701JasaRp))
        data.put("terbilang", invoiceT701?.t701TotalInvTerbilang)
        data.put("totalInvoice", conversi.toRupiahInvoice(invoiceT701?.t701TotalInv))
        data.put("bookingFee", conversi.toRupiahInvoice((invoiceT701?.t701BookingFee+invoiceT701?.t701OnRisk)))
//        data.put("onRisk", conversi.toRupiah(invoiceT701?.t701OnRisk))
        data.put("totalBayar", conversi.toRupiahInvoice(invoiceT701?.t701TotalBayarRp))
        data.put("supplySlip", nPart)
        data.put("catatan", invoiceT701?.t701Catatan)
        def KB = NamaManPower.createCriteria().get {
            eq("staDel","0");
            eq("companyDealer",session?.userCompanyDealer)
            manPowerDetail{
                manPower{
                    or{
                        eq("m014JabatanManPower","KEPALA BENGKEL",[ignoreCase: true])
                        eq("m014Inisial","KBNG",[ignoreCase: true])
                    }
                }
            }
            maxResults(1);
        }
        data.put("kaBeng", KB?.t015NamaLengkap ? KB?.t015NamaLengkap : "-")
        data.put("saran", invoiceT701?.t701Catatan)
        data.put("keluhan", "")

        reportData.add(data)

        return  reportData
    }

    def previewPart(){

        def returnList = []
        List<JasperReportDef> reportDefList = []
        def file = null

        def partslip = PartInv.createCriteria().list {
            eq("t703StaDel", "0")
            invoice{
                eq("id", params.idInv as long)
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("t703NoPartInv", "t703NoPartInv")
            }
            order("t703NoPartInv")
        }

        partslip.each {
            returnList << it
        }

        returnList.each {
            def reportData = partSlipReportData(it)
            def reportDef = new JasperReportDef(name:'supplySlip2.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )

            reportDefList.add(reportDef)
        }

        file = File.createTempFile("Parts_Slip_",".pdf")

        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }

    def partSlipReportData(def noPart){
        def reportData = new ArrayList()

        def detPart = InvoiceT701.findByIdAndT701StaDel(params.idInv as long, "0")

        def partslip = PartInv.createCriteria().list {
            eq("t703StaDel", "0")
            ilike("t703NoPartInv", "%"+noPart.t703NoPartInv.toString()+"%")
            invoice{
                eq("id", params.idInv as long)
            }
        }

        def isi = [], isi2 = []
        def totalBahan = 0
        def totalpart = 0
        def grandTo = 0, nop = 0, noB = 0
        def data = [:]

        partslip.each {
            def klas = KlasifikasiGoods.findByGoods(it?.goods)

            if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("TOYOTA") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN") || klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                nop++
                totalpart += it.t703TotalRp
                isi << [
                        no : nop,
                        partsNo : it?.goods?.m111ID,
                        partsName: it?.goods?.m111Nama,
                        partsQty : it?.t703Jumlah1 ? it?.t703Jumlah1 : 0,
                        partsUnitPrice : it?.t703HargaRp ? conversi.toRupiahInvoice(it?.t703HargaRp) : 0,
                        partsDisc : it?.t703DiscPersen ? it?.t703DiscPersen : 0,
                        partsExtdAmount : it?.t703TotalRp ? conversi.toRupiahInvoice(it?.t703TotalRp) : 0,
                        partsRemark : it?.t703Remark ? it?.t703Remark : ""
                ]
            }else{
                noB++
                totalBahan+=it.t703TotalRp
                isi2 << [
                        no : noB,
                        partsNo : it?.goods?.m111ID,
                        partsName: it?.goods?.m111Nama,
                        partsQty : it?.t703Jumlah1 ? it?.t703Jumlah1 : 0,
                        partsUnitPrice : it?.t703HargaRp ? conversi.toRupiahInvoice(it?.t703HargaRp) : 0,
                        partsDisc : it?.t703DiscPersen ? it?.t703DiscPersen : 0,
                        partsExtdAmount : it?.t703TotalRp ? conversi.toRupiahInvoice(it?.t703TotalRp) : 0,
                        partsRemark : it?.t703Remark ? it?.t703Remark : ""
                ]
            }

            grandTo = totalpart + totalBahan
        }

        if (totalpart > 0){
            isi << [
                    no: "",
                    partsNo : "",
                    partsName: "",
                    partsQty : "",
                    partsUnitPrice : "SUBTOTAL",
                    partsDisc : "",
                    partsExtdAmount : conversi.toRupiahInvoice(totalpart),
                    partsRemark : ""

            ]
        }

        if (totalBahan > 0){
            isi2 << [
                    no: "",
                    partsNo : "",
                    partsName: "",
                    partsQty : "",
                    partsUnitPrice : "SUBTOTAL",
                    partsDisc : "",
                    partsExtdAmount : conversi.toRupiahInvoice(totalBahan),
                    partsRemark : ""

            ]
        }

        data.put("slipNo", noPart.t703NoPartInv.toString())
        data.put("noPartsSlip", noPart.t703NoPartInv.toString())
        data.put("noWO", detPart?.reception?.t401NoWO)
        data.put("slipDate", detPart?.t701TglJamInvoice?.format("dd/MM/yyyy"))
        data.put("slipTime", detPart?.t701TglJamInvoice?.format("HH:mm"))
        data.put("noPolisi", detPart?.t701Nopol)
        data.put("customerName", detPart?.t701Customer)
        data.put("customerAddress", detPart?.t701Alamat)

        data.put("partsItem", isi + isi2)
        data.put("grandTot", conversi.toRupiahInvoice(grandTo))
        reportData.add(data)

        return reportData
    }

    def discountTable(){
        def invoice = InvoiceT701.get(params.idInv.toLong())
        def invPart = PartInv.findByInvoiceAndT703StaDel(invoice, "0")

        [invoiceInstance : invPart]
    }

    def cekApproval(){
        def result = [:]
        def inv = InvoiceT701.findByT701NoInvIlikeAndT701StaDel("%"+params.noInv+"%","0")
        if(inv){
            result.hasil = "ada"
            String staApp = ""
            String alasanApp = ""
            if(inv?.t701StaApprovedReversal){
                staApp = staApproveJob[inv?.t701StaApprovedReversal?.toInteger()]
                if(inv?.t701AlasanUnApproved){
                    alasanApp = inv?.t701AlasanUnApproved
                }
            }
            result.staApp = staApp
            result.alasan = alasanApp
        }else{
            result.hasil = "tidak"
        }
        render result as JSON
    }

    def doSaveAM(){
        def inv = InvoiceT701.get(params.idInv.toLong())
        if(inv){
            if(params.from=="adm"){
                inv?.t701AdmRp = Math.round(params?.nilai?.toDouble())
                inv?.t701SubTotalRp = Math.round(inv?.t701JasaRp) + Math.round(inv?.t701SubletRp) + Math.round(inv?.t701PartsRp) +  Math.round(inv?.t701MaterialRp) + Math.round(inv?.t701OliRp) + Math.round(params?.nilai?.toDouble())
                inv?.t701TotalRp = Math.round(inv?.t701SubTotalRp) - Math.round(inv?.t701TotalDiscRp)
                inv?.t701PPnRp = Math.round(10/100 * inv?.t701TotalRp)
                inv?.t701TotalInv = (Math.round(inv?.t701TotalRp) + Math.round(inv?.t701PPnRp) + Math.round(inv?.t701MateraiRp))
                inv?.t701TotalBayarRp = (Math.round(inv?.t701TotalInv) - Math.round(inv?.t701BookingFee) - Math.round(inv?.t701OnRisk))
            }else if(params.from=="jasa"){
                def selisih = inv?.t701JasaRp - params?.nilai?.toDouble()
                inv?.t701SubTotalRp = (Math.round(inv?.t701SubTotalRp) - Math.round(inv?.t701JasaRp) + Math.round(params?.nilai?.toDouble()))
                inv?.t701TotalRp = (Math.round(inv?.t701TotalRp) - Math.round(inv?.t701JasaRp) + Math.round(params?.nilai?.toDouble()))
                inv?.t701PPnRp = Math.round(10/100 * inv?.t701TotalRp)
                inv?.t701TotalInv = (Math.round(inv?.t701TotalRp) + Math.round(inv?.t701PPnRp) + Math.round(inv?.t701MateraiRp))
                inv?.t701TotalBayarRp = Math.round(inv?.t701TotalInv) - Math.round(inv?.t701BookingFee) - Math.round(inv?.t701OnRisk)
                inv?.t701JasaRp = Math.round(params?.nilai?.toDouble())
                def jobInvs = JobInv.findAllByInvoiceAndStaDel(inv,"0")
                def sisa = selisih%jobInvs?.size()
                def perJobs = selisih/jobInvs?.size()
                int jum =  0
                jobInvs?.each {
                    jum++;
                    if(jum==jobInvs?.size()){
                        it?.t702HargaRp = Math.round(it?.t702HargaRp-perJobs?.toInteger()-sisa)
                    }else {
                        it?.t702HargaRp = Math.round(it?.t702HargaRp-perJobs?.toInteger())
                    }
                    it?.t702TotalRp = it?.t702DiscRp ? it?.t702HargaRp-it?.t702DiscRp : it?.t702HargaRp
                    it?.save(flush: true)
                }
            }else if(params.from=="alamat"){
                inv?.t701Alamat = (params?.nilai as String)
            }else if(params.from=="npwp"){
                inv?.t701NPWP = (params?.nilai as String)
            }
            else{
                inv?.t701MateraiRp = Math.round(params?.nilai?.toDouble())
                inv?.t701SubTotalRp = Math.round(inv?.t701JasaRp) + Math.round(inv?.t701SubletRp) + Math.round(inv?.t701PartsRp) +  Math.round(inv?.t701MaterialRp) + Math.round(inv?.t701OliRp) + Math.round(inv?.t701AdmRp)
                inv?.t701TotalRp = Math.round(inv?.t701SubTotalRp) - Math.round(inv?.t701TotalDiscRp)
                inv?.t701PPnRp = Math.round(10/100 * inv?.t701TotalRp)
                inv?.t701TotalInv = (Math.round(inv?.t701TotalRp) + Math.round(inv?.t701PPnRp) + Math.round(params?.nilai?.toDouble()))
                inv?.t701TotalBayarRp = (Math.round(inv?.t701TotalInv) - Math.round(inv?.t701BookingFee) - Math.round(inv?.t701OnRisk))
            }
            inv?.t701TotalInvTerbilang = moneyUtil.convertMoneyToWords(inv?.t701TotalBayarRp)
            inv.save(flush: true)
            inv.errors.each { print(it)}
        }
        render "ok"
    }

    def doSaveCatatan(){
        def inv = InvoiceT701.get(params?.idInv?.toLong())
        if(inv){
            inv.t701Catatan = params?.catatan
            inv?.save(flush: true)
        }
        render "ok"
    }
}