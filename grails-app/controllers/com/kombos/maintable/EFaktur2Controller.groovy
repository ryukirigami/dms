package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import groovy.sql.Sql
import org.apache.commons.io.FileUtils
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat

class EFaktur2Controller {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def EFaktur2Service

//    @Procedure(actionName="E_FAKTUR")
    def koneksiToOracleService
    def dataSource
    def sessionFactory
    def jasperService

    def excelImportService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {

    }

    def uploadData() {
        [tanggal : new Date()]
    }

    def list(Integer max) {
        def m = EFaktur.createCriteria().list {
            if(params.docNumber){
                eq("docNumber",params.docNumber)
            }
            isNotNull("DPP")
            isNotNull("PPN")
            eq("staDel","0")
            eq("jenisInputan","PPN_KELUARAN")
            eq("staOriRev","0")
        }
        def DPP = ""
        def PPN = ""
        if(m.DPP){
            DPP = new Konversi().toRupiah2(m?.DPP.sum());
        }
        if(m.PPN){
            PPN = new Konversi().toRupiah2(m?.PPN.sum());
        }
        [docNumber : params.docNumber,DPP:DPP,PPN: PPN]
    }

    def edit(Long id) {
        def EFakturInstance = EFaktur.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'TESTER'), id])
            redirect(action: "list")
            return
        }

        [EFakturInstance: EFakturInstance]
    }

    def update(Long id, Long version) {
        def EFakturInstance = EFaktur.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'TESTER2'), id])
            redirect(action: "list")
            return
        }

        EFakturInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        EFakturInstance?.setLastUpdProcess("UPDATE")
        EFakturInstance.properties = params
        EFakturInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if (!EFakturInstance.save(flush: true)) {
            render(view: "edit", model: [EFakturInstance: EFakturInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'bank.label', default: 'Efaktur'), EFakturInstance.id])
        redirect(action: "show", id: EFakturInstance.id)
    }

    def show(Long id) {
        def EFakturInstance = EFaktur.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Efaktur'), id])
            redirect(action: "list")
            return
        }

        [EFakturInstance: EFakturInstance]
    }

    def delete(Long id) {
        def EFakturInstance = EFaktur.get(id)
        if (!EFakturInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "list")
            return
        }

        try {
            EFakturInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            EFakturInstance?.setLastUpdProcess("DELETE")
            EFakturInstance?.lastUpdated = datatablesUtilService?.syncTime()
            EFakturInstance?.setStaDel('1')
            EFakturInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'EFaktur.label', default: 'EFaktur'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
//            datatablesUtilService.massDeleteStaDelNew(EFaktur, params)
            def jsonArray = JSON.parse(params.ids)
            jsonArray.each { arr ->
                def efak = EFaktur.findById(arr as Long)
                def noInv = efak.getNoUrutInvoice().split("-")
                def allef = EFaktur.findAllByCompanyDealerAndJenisInputanAndNoUrutInvoiceLike(efak.companyDealer,'PPN_KELUARAN',noInv[0]+"-%")
                allef.each {
                    def ubh = EFaktur.get(it.id as Long)
                    ubh.staDel = '1'
                    ubh.save(flush: true)
                }
            }
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }



    def datatablesList() {
        session.exportParams = params
        params.userCompanyDealer = session?.userCompanyDealer
        render EFaktur2Service.datatablesList(params) as JSON
    }


    def datatablesList1() {
        session.exportParams = params
        params.userCompanyDealer = session?.userCompanyDealer
        render EFaktur2Service.datatablesList1(params) as JSON
    }


    def view() {
        def date = datatablesUtilService.syncTime()
        session["PPN_KELUARAN"] = "";
        def cek = EFaktur.createCriteria()
        def ppnMasukan = cek.list() {
            eq("companyDealer",session.userCompanyDealer)
            ge("dateCreated",date.clearTime())
            lt("dateCreated",date.clearTime() + 1)
            eq("staDel",'0')
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
            }
        }
        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'NO',
                        'B':'MASA_PAJAK',
                        'C':'TAHUN_PAJAK',
                        'D':'REFERENSI',
                        'E':'NO_SI',
                        'F':'TGL_SI',
                        'G':'NAMA_CUSTOMER',
                        'H':'NPWP',
                        'I':'ALAMAT',
                        'J':'BIAYA_JASA',
                        'K':'BIAYA_PARTS',
                        'L':'BIAYA_OLI',
                        'M':'BIAYA_MATERIAL',
                        'N':'BIAYA_SUBLET',
                        'O':'BIAYA_ADM',
                        'P':'DISC_JASA',
                        'Q':'DISC_PARTS',
                        'R':'DISC_OLI',
                        'S':'DISC_BAHAN',
                        'T':'DPP',
                        'U':'PPN',
                        'V':'TOTAL'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }

        def jsonData = []
        int jmlhDataError = 0;
        def isiEror= "";
        String htmlData = ""
        def arrMasaPajak = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        def arrThnPajak = ["2014","2015","2016","2017","2018","2019","2020","2021","2022","2023","2024","2015","2026","2027"]
        if(!uploadExcel?.empty){
            //validate content type
            NumberFormat formatter = new DecimalFormat("#0")
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/vnd-ms-excel','application/octet-stream','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]

            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "uploadData")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            String status = "0", style = "color:black;", flag = "black"
            int nomor = 0
            jobList.each {
                nomor++
                style = "color:black;"
                flag = "black"
                try {
                    it.MASA_PAJAK = it.MASA_PAJAK.toInteger().toString()
                    if(it.MASA_PAJAK.toInteger() < 10){
                        it.MASA_PAJAK = "0" + it.MASA_PAJAK
                    }
                    it.TAHUN_PAJAK = it.TAHUN_PAJAK.toInteger().toString()
                    if(it.NPWP == 0.0){
                        it.NPWP = "0"
                    }
                }catch(Exception e){

                }

                if((it.MASA_PAJAK=="" || it.MASA_PAJAK==null) || !arrMasaPajak.contains(it.MASA_PAJAK.toString())){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" :BULAN DIISI (01-12) !! <br>"
                    jmlhDataError++
                }

                if((it.TAHUN_PAJAK=="" || it.TAHUN_PAJAK==null) || !arrThnPajak.contains(it.TAHUN_PAJAK.toInteger().toString())){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : TAHUN DIISI (EX. 2016) !! <br>"
                    jmlhDataError++
                }
                if(it.NO_SI=="" || it.NO_SI==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : SI TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.NAMA_CUSTOMER=="" || it.NAMA_CUSTOMER==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : NAMA CUSTOMER TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.ALAMAT=="" || it.ALAMAT==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : ALAMAT CUSTOMER TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.NPWP=="" || it.NPWP==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : NPWP TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                if(it.REFERENSI=="" && it.REFERENSI==null){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : REFERENSI TIDAK BOLEH KOSONG !! <br>"
                    jmlhDataError++
                }
                //
                if(it.BIAYA_JASA=="" || it.BIAYA_JASA=="-" || it.BIAYA_JASA==null){
                    it.BIAYA_JASA = "0"
                }else{
                        try{
                            it.BIAYA_JASA = it.BIAYA_JASA.toDouble().toString()
                        }catch(Exception e){
                            style = "style='color:red'"
                            flag =  "red"
                            isiEror += "*BARIS ke-"+nomor+" : BIAYA JASA HARUS ANGKA !! <br>"
                            jmlhDataError++
                        }
                }
                if(it.BIAYA_PARTS=="" ||it.BIAYA_PARTS=="-"|| it.BIAYA_PARTS==null){
                    it.BIAYA_PARTS = "0"
                }else{
                    try{
                        it.BIAYA_PARTS = it.BIAYA_PARTS.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA PARTS HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_OLI=="" ||it.BIAYA_OLI=="-" || it.BIAYA_OLI==null){
                    it.BIAYA_OLI = "0"
                }else{
                    try{
                        it.BIAYA_OLI = it.BIAYA_OLI.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA OLI HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_MATERIAL=="" || it.BIAYA_MATERIAL=="" || it.BIAYA_MATERIAL==null){
                    it.BIAYA_MATERIAL = "0"
                }else{
                    try{
                        it.BIAYA_MATERIAL = it.BIAYA_MATERIAL.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA MATERIAL HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_SUBLET=="" || it.BIAYA_SUBLET==""  || it.BIAYA_SUBLET==null){
                    it.BIAYA_SUBLET = "0"
                }else{
                    try{
                        it.BIAYA_SUBLET = formatter.format(it.BIAYA_SUBLET.toDouble()).toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA SUBLET HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.BIAYA_ADM=="" || it.BIAYA_ADM==""  || it.BIAYA_ADM==null){
                    it.BIAYA_ADM = "0"
                }else{
                    try{
                        it.BIAYA_ADM = it.BIAYA_ADM.toDouble().toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : BIAYA ADM HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DISC_JASA=="" || it.DISC_JASA==""  || it.DISC_JASA==null){
                    it.DISC_JASA = "0"
                }else{
                    try{
                        it.DISC_JASA = formatter.format(it.DISC_JASA.toDouble()).toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_JASA HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DISC_PARTS=="" || it.DISC_PARTS==""  || it.DISC_PARTS==null){
                    it.DISC_PARTS = "0"
                }else{
                    try{
                        it.DISC_PARTS = formatter.format(it.DISC_PARTS.toDouble()).toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_PARTS HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DISC_BAHAN=="" || it.DISC_BAHAN==""  || it.DISC_BAHAN==null){
                    it.DISC_BAHAN = "0"
                }else{
                    try{
                        it.DISC_BAHAN = formatter.format(it.DISC_BAHAN.toDouble()).toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_BAHAN HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }

                if(it.DISC_OLI=="" || it.DISC_OLI==""  || it.DISC_OLI==null){
                    it.DISC_OLI = "0"
                }else{
                    try{
                        it.DISC_OLI = formatter.format(it.DISC_OLI.toDouble()).toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DISC_OLI HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.DPP=="" || it.DPP==""  || it.DPP==null){
                    it.DPP = "0"
                }else{
                    try{
                        it.DPP = formatter.format(it.DPP.toDouble()).toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : DPP HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.PPN=="" || it.PPN==""  || it.PPN==null){
                    it.PPN = "0"
                }else{
                    try{
                        it.PPN = formatter.format(it.PPN.toDouble()).toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : PPN  HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                if(it.TOTAL=="" || it.TOTAL==""  || it.TOTAL==null){
                    it.TOTAL = "0"
                }else{
                    try{
                        it.TOTAL = formatter.format(it.TOTAL.toDouble())toString()
                    }catch(Exception e){
                        style = "style='color:red'"
                        flag =  "red"
                        isiEror += "*BARIS ke-"+nomor+" : TOTAL HARUS ANGKA !! <br>"
                        jmlhDataError++
                    }
                }
                //
                try {
                    it.NO_SI =it.NO_SI.toString().replaceAll("-","_")
                    Date TGL_SI = Date.parse("yyyy-MM-dd",it.TGL_SI.toString())
                }catch (Exception e){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : Format Tanggal (31/12/2016)<br>"
                    jmlhDataError++
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+nomor+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.MASA_PAJAK.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.TAHUN_PAJAK.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.REFERENSI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.NO_SI.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.TGL_SI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.NAMA_CUSTOMER.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.NPWP.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.ALAMAT.toString()+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_JASA+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_PARTS+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_OLI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_MATERIAL+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_SUBLET+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.BIAYA_ADM+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_JASA+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_PARTS+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_OLI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DISC_BAHAN+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.DPP+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.PPN+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.TOTAL+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
                jsonData << [
                        MASA_PAJAK:it.MASA_PAJAK,
                        TAHUN_PAJAK:it.TAHUN_PAJAK,
                        REFERENSI:it.REFERENSI,
                        NO_SI:it.NO_SI,
                        TGL_SI:it.TGL_SI,
                        NAMA_CUSTOMER:it.NAMA_CUSTOMER,
                        NPWP:it.NPWP,
                        ALAMAT:it.ALAMAT,
                        BIAYA_JASA:it.BIAYA_JASA,
                        BIAYA_PARTS:it.BIAYA_PARTS,
                        BIAYA_OLI:it.BIAYA_OLI,
                        BIAYA_MATERIAL:it.BIAYA_MATERIAL,
                        BIAYA_SUBLET:it.BIAYA_SUBLET,
                        BIAYA_ADM:it.BIAYA_ADM,
                        DISC_JASA:it.DISC_JASA,
                        DISC_PARTS:it.DISC_PARTS,
                        DISC_BAHAN:it.DISC_BAHAN,
                        DISC_OLI:it.DISC_OLI,
                        DPP:it.DPP,
                        PPN:it.PPN,
                        TOTAL:it.TOTAL,
                        flag : flag
                ]
            }
        }
        if (jsonData.size()==0){
            flash.message = message(code: 'default.uploadGoods.message', default: "File Tidak dapat diproses")
        }else if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah")
        }
        else {
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
        }
        session["PPN_KELUARAN"] = jsonData
        render(view: "uploadData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError, tanggal : params.tanggalUpload, jumJson : jsonData.size(), isiEror : isiEror])
    }


    def save() {

    }
    def saveApproval(){
        def data = JSON.parse(params.arr)
        def pesan = [:]
        data.each { dataFaka ->
            def faktur = EFaktur.get(dataFaka.dataid as Long)
            def dataInv = faktur.noUrutInvoice.toString().split("-")
            def listFak  = EFaktur.findAllByNoUrutInvoiceIlikeAndDocNumber(dataInv[0]+"%",faktur.docNumber)
            listFak.each {
                def fakUbah = EFaktur.get(it.id as Long)
                fakUbah.setStaAprove(dataFaka.staApprove.toString())
                fakUbah.save(flush: true)
            }
        }
        render pesan as JSON
    }

    def showDetail() {
        [dataFakturDetail : EFaktur.findAllByDocNumberAndStaDelAndJenisInputanAndStaAprove(params.docNumber,"0","PPN_KELUARAN","0",[sort: 'id',order: 'asc'])]
    }

    def chekNoEfaktur() {
        [dataFakturDetail : EFaktur.findAllByDocNumberAndStaDelAndJenisInputanAndStaAprove(params.docNumber,"0","PPN_KELUARAN","1",[sort: 'id',order: 'asc'])]
    }


    def printEfaktur(){
        List<JasperReportDef> reportDefList = []
        params.userCompanyDealer = session?.userCompanyDealer
        reportDefList = EFaktur2Service.printEfaktur(params)
        def file = File.createTempFile("DMS_" + params.docNumber + "_",".csv")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "text/csv")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def upload(){
        def date = datatablesUtilService.syncTime()
        def requestBody = JSON.parse(params.sendData)
        def tanggal= Date.parse("dd/MM/yyyy",params.tanggal)

        def EFakturInstance = null
        def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
        def sdf = new SimpleDateFormat("yyyyMMdd")
        String docNumber = com+".OUT."+sdf.format(tanggal)
        def masterNPWP = NpwpEfaktur.list().get(0)
        requestBody.each{
            def depepe = (it.BIAYA_JASA.toBigDecimal() - it.DISC_JASA.toBigDecimal()) +
                    (it.BIAYA_PARTS.toBigDecimal() - it.DISC_PARTS.toBigDecimal()) +
                    (it.BIAYA_OLI.toBigDecimal() - it.DISC_OLI.toBigDecimal()) +
                    (it.BIAYA_MATERIAL.toBigDecimal() - it.DISC_BAHAN.toBigDecimal()) +
                    (it.BIAYA_ADM.toBigDecimal()) + (it.BIAYA_SUBLET.toBigDecimal())
            EFakturInstance = new EFaktur()
            EFakturInstance.companyDealer = session?.userCompanyDealer
            EFakturInstance.FK = 'FK'
            EFakturInstance.KD_JENIS_TRANSAKSI = '01'
            EFakturInstance.FG_PENGGANTI = '0'
            EFakturInstance.NOMOR_FAKTUR = ""
            EFakturInstance.MASA_PAJAK = it.MASA_PAJAK
            EFakturInstance.TAHUN_PAJAK = it.TAHUN_PAJAK
            EFakturInstance.TANGGAL_FAKTUR = new Date().parse("yyyy-MM-dd", it?.TGL_SI.toString())
            EFakturInstance.NPWP = it.NPWP
            EFakturInstance.NAMA = it.NAMA_CUSTOMER
            EFakturInstance.ALAMAT_LENGKAP = it.ALAMAT
            EFakturInstance.JUMLAH_DPP = depepe.toBigDecimal() //DIISI
            EFakturInstance.JUMLAH_PPN = (depepe * 0.1).toBigDecimal() //DIISI
            EFakturInstance.REFERENSI = it.REFERENSI
            EFakturInstance.LT = 'FAPR'
            EFakturInstance.NPWP2 = masterNPWP.namaPerusahaan
            EFakturInstance.NAMA2 = masterNPWP.penandaTangan
            EFakturInstance.JALAN = masterNPWP.alamatNpwp
            EFakturInstance.DISKON = 0
            EFakturInstance.noUrutInvoice = it.NO_SI + "-0"
            EFakturInstance.staAprove = '0'
            EFakturInstance.staDel = '0'
            EFakturInstance.staOriRev = '0'
            EFakturInstance.docNumber = docNumber
            EFakturInstance.tanggal = tanggal
            EFakturInstance.dateCreated = datatablesUtilService?.syncTime()
            EFakturInstance.lastUpdated = datatablesUtilService?.syncTime()
            EFakturInstance.lastUpdProcess = 'INSERT'
            EFakturInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            EFakturInstance.jenisInputan = 'PPN_KELUARAN'
            EFakturInstance.save(flush: true)
            EFakturInstance.errors.each {
                println it
            }

            //objek1
            if(it.BIAYA_JASA!="0"){
                def EFakturObjek = new EFaktur()
                EFakturObjek.companyDealer = session?.userCompanyDealer
                EFakturObjek.OF_ = 'OF'
                EFakturObjek.KODE_OBJEK = "01"
                EFakturObjek.NAMA_OBJEK = "BIAYA JASA"
                EFakturObjek.HARGA_SATUAN = it.BIAYA_JASA.toBigDecimal()
                EFakturObjek.JUMLAH_BARANG = 1
                EFakturObjek.HARGA_TOTAL = it.BIAYA_JASA.toBigDecimal()
                EFakturObjek.DISKON = it.DISC_JASA.toBigDecimal()
                EFakturObjek.DPP = (it.BIAYA_JASA.toBigDecimal() - it.DISC_JASA.toBigDecimal())
                EFakturObjek.PPN = ((it.BIAYA_JASA.toBigDecimal() - it.DISC_JASA.toBigDecimal()) * 0.1)
                EFakturObjek.noUrutInvoice = it.NO_SI + "-01"
                EFakturObjek.staAprove = '0'
                EFakturObjek.staDel = '0'
                EFakturObjek.staOriRev = '0'
                EFakturObjek.docNumber = docNumber
                EFakturObjek.tanggal = tanggal
                EFakturObjek.dateCreated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdProcess = 'INSERT'
                EFakturObjek.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                EFakturObjek.jenisInputan = 'PPN_KELUARAN'
                EFakturObjek.save(flush: true)
                EFakturObjek.errors.each {
                    println it
                }
            }

            //objek2
            if(it.BIAYA_PARTS!="0"){
                def EFakturObjek = new EFaktur()
                EFakturObjek.companyDealer = session?.userCompanyDealer
                EFakturObjek.OF_ = 'OF'
                EFakturObjek.KODE_OBJEK = "02"
                EFakturObjek.NAMA_OBJEK = "BIAYA PARTS"
                EFakturObjek.HARGA_SATUAN = it.BIAYA_PARTS.toBigDecimal()
                EFakturObjek.JUMLAH_BARANG = 1
                EFakturObjek.HARGA_TOTAL = it.BIAYA_PARTS.toBigDecimal()
                EFakturObjek.DISKON = it.DISC_PARTS.toBigDecimal()
                EFakturObjek.DPP = (it.BIAYA_PARTS.toBigDecimal() - it.DISC_PARTS.toBigDecimal())
                EFakturObjek.PPN = ((it.BIAYA_PARTS.toBigDecimal() - it.DISC_PARTS.toBigDecimal()) * 0.1)
                EFakturObjek.noUrutInvoice = it.NO_SI + "-02"
                EFakturObjek.staAprove = '0'
                EFakturObjek.staDel = '0'
                EFakturObjek.staOriRev = '0'
                EFakturObjek.docNumber = docNumber
                EFakturObjek.tanggal = tanggal
                EFakturObjek.dateCreated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdProcess = 'INSERT'
                EFakturObjek.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                EFakturObjek.jenisInputan = 'PPN_KELUARAN'
                EFakturObjek.save(flush: true)
                EFakturObjek.errors.each {
                    println it
                }
            }

            //OLI OBJEK 3
            if(it.BIAYA_OLI!="0"){
                def EFakturObjek = new EFaktur()
                EFakturObjek.companyDealer = session?.userCompanyDealer
                EFakturObjek.OF_ = 'OF'
                EFakturObjek.KODE_OBJEK = "03"
                EFakturObjek.NAMA_OBJEK = "BIAYA OLI"
                EFakturObjek.HARGA_SATUAN = it.BIAYA_OLI.toBigDecimal()
                EFakturObjek.JUMLAH_BARANG = 1
                EFakturObjek.HARGA_TOTAL = it.BIAYA_OLI.toBigDecimal()
                EFakturObjek.DISKON = it.DISC_OLI.toBigDecimal()
                EFakturObjek.DPP = (it.BIAYA_OLI.toBigDecimal() - it.DISC_OLI.toBigDecimal())
                EFakturObjek.PPN = ((it.BIAYA_OLI.toBigDecimal() - it.DISC_OLI.toBigDecimal()) * 0.1)
                EFakturObjek.noUrutInvoice = it.NO_SI + "-03"
                EFakturObjek.staAprove = '0'
                EFakturObjek.staDel = '0'
                EFakturObjek.staOriRev = '0'
                EFakturObjek.docNumber = docNumber
                EFakturObjek.tanggal = tanggal
                EFakturObjek.dateCreated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdProcess = 'INSERT'
                EFakturObjek.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                EFakturObjek.jenisInputan = 'PPN_KELUARAN'
                EFakturObjek.save(flush: true)
                EFakturObjek.errors.each {
                    println it
                }
            }

            //MATERIAL OBJEK 4
            if(it.BIAYA_MATERIAL!="0"){
                def EFakturObjek = new EFaktur()
                EFakturObjek.companyDealer = session?.userCompanyDealer
                EFakturObjek.OF_ = 'OF'
                EFakturObjek.KODE_OBJEK = "04"
                EFakturObjek.NAMA_OBJEK = "BIAYA MATERIAL"
                EFakturObjek.HARGA_SATUAN = it.BIAYA_MATERIAL.toBigDecimal()
                EFakturObjek.JUMLAH_BARANG = 1
                EFakturObjek.HARGA_TOTAL = it.BIAYA_MATERIAL.toBigDecimal()
                EFakturObjek.DISKON = it.DISC_BAHAN.toBigDecimal()
                EFakturObjek.DPP = (it.BIAYA_MATERIAL.toBigDecimal() - it.DISC_BAHAN.toBigDecimal())
                EFakturObjek.PPN = ((it.BIAYA_MATERIAL.toBigDecimal() - it.DISC_BAHAN.toBigDecimal()) * 0.1)
                EFakturObjek.noUrutInvoice = it.NO_SI + "-04"
                EFakturObjek.staAprove = '0'
                EFakturObjek.staDel = '0'
                EFakturObjek.staOriRev = '0'
                EFakturObjek.docNumber = docNumber
                EFakturObjek.tanggal = tanggal
                EFakturObjek.dateCreated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdProcess = 'INSERT'
                EFakturObjek.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                EFakturObjek.jenisInputan = 'PPN_KELUARAN'
                EFakturObjek.save(flush: true)
                EFakturObjek.errors.each {
                    println it
                }
            }

            //ADM OBJEK 6
            if(it.BIAYA_ADM!="0"){
                def EFakturObjek = new EFaktur()
                EFakturObjek.companyDealer = session?.userCompanyDealer
                EFakturObjek.OF_ = 'OF'
                EFakturObjek.KODE_OBJEK = "06"
                EFakturObjek.NAMA_OBJEK = "BIAYA ADMINISTRASI"
                EFakturObjek.HARGA_SATUAN = it.BIAYA_ADM.toBigDecimal()
                EFakturObjek.JUMLAH_BARANG = 1
                EFakturObjek.HARGA_TOTAL = it.BIAYA_ADM.toBigDecimal()
                EFakturObjek.DISKON = 0
                EFakturObjek.DPP = (it.BIAYA_ADM.toBigDecimal())
                EFakturObjek.PPN = ((it.BIAYA_ADM.toBigDecimal()) * 0.1)
                EFakturObjek.noUrutInvoice = it.NO_SI + "-06"
                EFakturObjek.staAprove = '0'
                EFakturObjek.staDel = '0'
                EFakturObjek.staOriRev = '0'
                EFakturObjek.docNumber = docNumber
                EFakturObjek.tanggal = tanggal
                EFakturObjek.dateCreated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdProcess = 'INSERT'
                EFakturObjek.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                EFakturObjek.jenisInputan = 'PPN_KELUARAN'
                EFakturObjek.save(flush: true)
                EFakturObjek.errors.each {
                    println it
                }
            }
            //SUBLET OBJEK 5
            if(it.BIAYA_SUBLET!="0"){
                def EFakturObjek = new EFaktur()
                EFakturObjek.companyDealer = session?.userCompanyDealer
                EFakturObjek.OF_ = 'OF'
                EFakturObjek.KODE_OBJEK = "05"
                EFakturObjek.NAMA_OBJEK = "BIAYA SUBLET"
                EFakturObjek.HARGA_SATUAN = it.BIAYA_SUBLET.toBigDecimal()
                EFakturObjek.JUMLAH_BARANG = 1
                EFakturObjek.HARGA_TOTAL = it.BIAYA_SUBLET.toBigDecimal()
                EFakturObjek.DISKON = 0
                EFakturObjek.DPP = (it.BIAYA_SUBLET.toBigDecimal())
                EFakturObjek.PPN = ((it.BIAYA_SUBLET.toBigDecimal()) * 0.1)
                EFakturObjek.noUrutInvoice = it.NO_SI + "-05"
                EFakturObjek.staAprove = '0'
                EFakturObjek.staDel = '0'
                EFakturObjek.staOriRev = '0'
                EFakturObjek.docNumber = docNumber
                EFakturObjek.tanggal = tanggal
                EFakturObjek.dateCreated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdated = datatablesUtilService?.syncTime()
                EFakturObjek.lastUpdProcess = 'INSERT'
                EFakturObjek.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                EFakturObjek.jenisInputan = 'PPN_KELUARAN'
                EFakturObjek.save(flush: true)
                EFakturObjek.errors.each {
                    println it
                }
            }
            flash.message = message(code: 'default.uploadGoods.message', default: "Save Data Sukses")
        }

        render(view: "uploadData", model: [woTransactionInstance: EFakturInstance, tanggal : tanggal])

    }

    def exportToExcel(){
        List<JasperReportDef> reportDefList = []
        params.dataExcel = session["PPN_KELUARAN"]
        def reportData = exportToExcelDetail(params) //komen ini jangan dulu di hapus

        def reportDef = new JasperReportDef(name:'reportEfaktur.jasper',
                fileFormat:JasperExportFormat.XLS_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("EFaktur_ppnKeluaran",".xls")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }

    def exportToExcelDetail(def params){
        def jsonArray = params.dataExcel
        def reportData = new ArrayList();
        jsonArray.each {
            def data = [:]
            data.put("COLUMN","FK")
            data.put("COLUMN_1","KD_JENIS_TRANSAKSI")
            data.put("COLUMN_2","FG_PENGGANTI")
            data.put("COLUMN_3","NOMOR_FAKTUR")
            data.put("COLUMN_4","MASA_PAJAK")
            data.put("COLUMN_5","TAHUN_PAJAK")
            data.put("COLUMN_6","TANGGAL_FAKTUR")
            data.put("COLUMN_7","NPWP")
            data.put("COLUMN_8","NAMA")
            data.put("COLUMN_9","ALAMAT_LENGKAP")
            data.put("COLUMN_10","JUMLAH_DPP")
            data.put("COLUMN_11","JUMLAH_PPN")
            data.put("COLUMN_12","JUMLAH_PPNBM")
            data.put("COLUMN_13","ID_KETERANGAN_TAMBAHAN")
            data.put("COLUMN_14","FG_UANG_MUKA")
            data.put("COLUMN_15","UANG_MUKA_DPP")
            data.put("COLUMN_16","UANG_MUKA_PPN")
            data.put("COLUMN_17","UANG_MUKA_PPNBM")
            data.put("COLUMN_18","REFERENSI")
            data.put("COLUMN_19","LT")
            data.put("COLUMN_20","NPWP2") // MENUNJUKAN NPWP2
            data.put("COLUMN_21","NAMA2") // MENUNJUKKAN NAMA2
            data.put("COLUMN_22","JALAN")
            data.put("COLUMN_23","BLOK")
            data.put("COLUMN_24","NOMOR")
            data.put("COLUMN_25","RT")
            data.put("COLUMN_26","RW")
            data.put("COLUMN_27","KECAMATAN")
            data.put("COLUMN_28","KELURAHAN")
            data.put("COLUMN_29","KABUPATEN")
            data.put("COLUMN_30","PROPINSI")
            data.put("COLUMN_31","KODE_POS")
            data.put("COLUMN_32","NOMOR_TELEPON")
            data.put("COLUMN_33","OF")
            data.put("COLUMN_34","KODE_OBJEK")
            data.put("COLUMN_35","NAMA_OBJEK")
            data.put("COLUMN_36","HARGA_SATUAN")
            data.put("COLUMN_37","JUMLAH_BARANG")
            data.put("COLUMN_38","HARGA_TOTAL")
            data.put("COLUMN_39","DISKON")
            data.put("COLUMN_40","DPP")
            data.put("COLUMN_41","PPN")
            data.put("COLUMN_42","TARIF_PPNBM")
            data.put("COLUMN_43","PPNBM")

            data.put("FLAG_COLOR",it.flag)
            data.put("F_COLUMN",it.FK)
            data.put("F_COLUMN_1",it.KD_JENIS_TRANSAKSI)
            data.put("F_COLUMN_2",it.FG_PENGGANTI)
            data.put("F_COLUMN_3",it.NOMOR_FAKTUR)
            data.put("F_COLUMN_4",it.MASA_PAJAK)
            data.put("F_COLUMN_5",it.TAHUN_PAJAK)
            data.put("F_COLUMN_6",it.TANGGAL_FAKTUR)
            data.put("F_COLUMN_7",it.NPWP)
            data.put("F_COLUMN_8",it.NAMA_OBJEK)
            data.put("F_COLUMN_9",it.ALAMAT_LENGKAP)
            data.put("F_COLUMN_10",it.JUMLAH_DPP)
            data.put("F_COLUMN_11",it.JUMLAH_PPN)
            data.put("F_COLUMN_12",it.JUMLAH_PPNBM)
            data.put("F_COLUMN_13",it.ID_KETERANGAN_TAMBAHAN)
            data.put("F_COLUMN_14",it.FG_UANG_MUKA)
            data.put("F_COLUMN_15",it.UANG_MUKA_DPP)
            data.put("F_COLUMN_16",it.UANG_MUKA_PPN)
            data.put("F_COLUMN_17",it.UANG_MUKA_PPNBM)
            data.put("F_COLUMN_18",it.REFERENSI)
            data.put("F_COLUMN_19",it.LT)
            data.put("F_COLUMN_20",it.NPWP2)
            data.put("F_COLUMN_21",it.NAMA2)
            data.put("F_COLUMN_22",it.JALAN)
            data.put("F_COLUMN_23",it.BLOK)
            data.put("F_COLUMN_24",it.NOMOR)
            data.put("F_COLUMN_25",it.RT)
            data.put("F_COLUMN_26",it.RW)
            data.put("F_COLUMN_27",it.KECAMATAN)
            data.put("F_COLUMN_28",it.KELURAHAN)
            data.put("F_COLUMN_29",it.KABUPATEN)
            data.put("F_COLUMN_30",it.PROPINSI)
            data.put("F_COLUMN_31",it.KODE_POS)
            data.put("F_COLUMN_32",it.NOMOR_TELEPON)
            data.put("F_COLUMN_33",it.OF_)
            data.put("F_COLUMN_34",it.KODE_OBJEK)
            data.put("F_COLUMN_35",it.NAMA)
            data.put("F_COLUMN_36",it.HARGA_SATUAN)
            data.put("F_COLUMN_37",it.JUMLAH_BARANG)
            data.put("F_COLUMN_38",it.HARGA_TOTAL)
            data.put("F_COLUMN_39",it.DISKON)
            data.put("F_COLUMN_40",it.DPP)
            data.put("F_COLUMN_41",it.PPN)
            data.put("F_COLUMN_42",it.TARIF_PPNBM)
            data.put("F_COLUMN_43",it.PPNBM)

            reportData.add(data)
        }

        return reportData
    }

    def generateNofak (){
        def data = JSON.parse(params.arr)
        def pesan = [:]
        data.each {
            def idfaktur = EFaktur.get(it.dataid as Long)
            Sql sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
            def query = "call GENERATE_NOFAKTUR("+it.dataid+")";
            sql.call(query)
            sql.commit();
//            faktur.setNOMOR_FAKTUR(it?.noFaktur.toString())
            idfaktur.save(flush: true)
        }
        render pesan as JSON
    }


    def generateData (){
        def startTgl = params.tgl1.toString()
        def endTgl = params.tgl2.toString()
        def comp = session?.userCompanyDealerId
        def jenisInputan = 'PPN_KELUARAN';
        def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
        Date date1 = new Date().parse("dd-MM-yyyy",startTgl)
        Date date2 = new Date().parse("dd-MM-yyyy",endTgl)
        Date date3 = date1
        def selisih = date2.date - date1.date
        date2.date+=1
        String pesan = ""
        (1..(selisih+1)).each {
            String docNumber = com+".OUT."+date3.format("yyyyMMdd")
            startTgl = date3.format("dd-MM-yyyy")
            date3.date+=1
            endTgl = date3.format("dd-MM-yyyy")
            if(!EFaktur.findByDocNumber(docNumber)){
                Sql sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
                Sql sql2 = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
                def query = "call E_FAKTUR('"+docNumber+"','"+jenisInputan+"','"+startTgl+"', '"+endTgl+"',"+comp+")";
                def query2 = "call EFAKTUR_PJL_LGSG('"+docNumber+"','"+jenisInputan+"','"+startTgl+"', '"+endTgl+"',"+comp+")";
                sql.call(query)
                sql2.call(query2)
                sql.commit();
                sql2.commit();
            }else{
                pesan += "Data tanggal " + startTgl  + " GAGAL SIMPAN  (Data Sudah Ada) \n"
            }
        }
        render pesan
    }

    def exportCSV={
        def jsonArray = JSON.parse(params.docNumber)
        int count = 0
        int count2 = 0
        Date today = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
        String date = DATE_FORMAT.format(today);

        response.setHeader("Content-disposition","attachment; filename=PPN_OUT_"+date+".csv")

        def results=[]
        jsonArray.each {
            results << it
        }
        def docNumber = null
        results.each {
            docNumber = it
        }

        params.userCompanyDealer = session?.userCompanyDealer
        def efaktur = EFaktur.createCriteria().list {
            eq("staDel","0")
            eq("jenisInputan","PPN_KELUARAN")
            eq("staAprove","1")
            if(!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("companyDealer",session?.userCompanyDealer)
            }
            order("noUrutInvoice","ASC")
            inList("docNumber", results)
        }

        def result='FK, KD_JENIS_TRANSAKSI, FG_PENGGANTI, NOMOR_FAKTUR, MASA_PAJAK, TAHUN_PAJAK, TANGGAL_FAKTUR, NPWP, NAMA, ALAMAT_LENGKAP, JUMLAH_DPP, JUMLAH_PPN, JUMLAH_PPNBM, ID_KETERANGAN_TAMBAHAN, FG_UANG_MUKA, UANG_MUKA_DPP, UANG_MUKA_PPN, UANG_MUKA_PPNBM, REFERENSI,\n' +
                'LT, NPWP2, NAMA2, JALAN, BLOK, NOMOR, RT, RW, KECAMATAN, KELURAHAN, KABUPATEN, PROPINSI, KODE_POS, NOMOR_TELEPON,\n' +
                'OF, KODE_OBJEK, NAMA_OBJEK, HARGA_SATUAN, JUMLAH_BARANG, HARGA_TOTAL, DISKON, DPP, PPN, TARIF_PPNBM, PPNBM\n'

        efaktur.each {u ->
            count2++;

            def npwp = u.NPWP
            if (npwp==null){
                npwp = "000000000000000"
            }
            if (npwp=="0" || npwp=="-"){
                npwp = "000000000000000"
            }
            if(npwp=="00.000.000.0-000.000"){
                npwp = "000000000000000"
            }

            def HARGA_TOTAL = u.HARGA_TOTAL
            if (HARGA_TOTAL==null){
                HARGA_TOTAL= u.HARGA_SATUAN
            }

            def JUMLAH_DPP = u.JUMLAH_DPP
            def JUMLAH_PPN = u.JUMLAH_PPN
            def JUMLAH_PPNBM = u.JUMLAH_PPNBM
            if (JUMLAH_DPP==null){
                JUMLAH_DPP=0
            }
            if (JUMLAH_PPN==null){
                JUMLAH_PPN=0
            }
            if (JUMLAH_PPNBM==null){
                JUMLAH_PPNBM = 0
            }

            def ID_KETERANGAN_TAMBAHAN = u.ID_KETERANGAN_TAMBAHAN
            if (ID_KETERANGAN_TAMBAHAN==null){
                ID_KETERANGAN_TAMBAHAN =""
            }

            def FG_UANG_MUKA = u.FG_UANG_MUKA
            if (FG_UANG_MUKA==null){
                FG_UANG_MUKA=0
            }
            def UANG_MUKA_DPP = u.UANG_MUKA_DPP
            if (UANG_MUKA_DPP==null){
                UANG_MUKA_DPP=0
            }
            def UANG_MUKA_PPN = u.UANG_MUKA_PPN
            if (UANG_MUKA_PPN==null){
                UANG_MUKA_PPN=0
            }

            def UANG_MUKA_PPNBM = u.UANG_MUKA_PPN
            if (UANG_MUKA_PPNBM==null){
                UANG_MUKA_PPNBM=0
            }

            def BLOK = u.BLOK
            if (BLOK==null){
                BLOK=""
            }

            def NOMOR = u.NOMOR
            if (NOMOR==null){
                NOMOR=""
            }
            def RT = u.RT
            if (RT==null){
                RT=""
            }
            def RW = u.RW
            if (RW==null){
                RW=""
            }
            def KECAMATAN = u.KECAMATAN
            if (KECAMATAN==null){
                KECAMATAN=""
            }
            def KELURAHAN = u.KELURAHAN
            if (KELURAHAN==null){
                KELURAHAN=""
            }
            def KABUPATEN = u.KABUPATEN
            if (KABUPATEN==null){
                KABUPATEN=""
            }
            def PROPINSI = u.PROPINSI
            if (PROPINSI==null){
                PROPINSI=""
            }
            def KODE_POS = u.KODE_POS
            if (KODE_POS==null){
                KODE_POS=""
            }
            def NOMOR_TELEPON = u.NOMOR_TELEPON
            if (NOMOR_TELEPON==null){
                NOMOR_TELEPON=""
            }

            def NOMOR_FAKTUR = u.NOMOR_FAKTUR

            def DISKON = u.DISKON
            if (DISKON==null){
                DISKON = 0
            }

            def TARIF_PPNBM = u.TARIF_PPNBM
            if (TARIF_PPNBM==null){
                TARIF_PPNBM = 0
            }

            def PPNBM = u.PPNBM
            if (PPNBM==null){
                PPNBM = 0
            }

            def NAMA = u.NAMA
            if (NAMA==null){
                NAMA = ""
            }
            def ALAMAT_LENGKAP = u.ALAMAT_LENGKAP
            if (ALAMAT_LENGKAP==null){
                ALAMAT_LENGKAP =""
            }
            def TANGGAL_FAKTUR = u.TANGGAL_FAKTUR?.format("dd/MM/yyyy")
            def KD_JENIS_TRANSAKSI = u.KD_JENIS_TRANSAKSI
            def dataInv = u.noUrutInvoice.toString().split("-")
            def d = EFaktur.findByNoUrutInvoiceAndCompanyDealerAndStaAproveAndStaDelAndJenisInputanAndDocNumber(dataInv[0]+"-0",u.companyDealer,'1','0','PPN_KELUARAN',u.docNumber)?.NOMOR_FAKTUR
            if (d){
                if(session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                    def ubha = EFaktur.get(u.id)
                    ubha.lastUpdProcess = 'DOWNLOAD'
                    ubha.save(flush: true)
                }
                if (u.FK!=null){
                    count++;
                    result += u.FK+","+ KD_JENIS_TRANSAKSI+","+ u.FG_PENGGANTI+","+ NOMOR_FAKTUR+","+ u.MASA_PAJAK+","+ u.TAHUN_PAJAK+","+ TANGGAL_FAKTUR+","+ // baris pertama
                            npwp+","+ NAMA+","+ ALAMAT_LENGKAP+","+ JUMLAH_DPP+","+ JUMLAH_PPN+","+ JUMLAH_PPNBM+","+ ID_KETERANGAN_TAMBAHAN+","+ FG_UANG_MUKA+","+
                            UANG_MUKA_DPP+","+ UANG_MUKA_PPN+","+ UANG_MUKA_PPNBM+","+ u.REFERENSI+"\n"
                }
                if (u.LT!=null){
                    result += u.LT+","+ u.NPWP2+","+ u.JALAN+","+ u.NAMA2+","+ BLOK+","+ NOMOR+","+ RT+","+ RW+","+ KECAMATAN+","+ KELURAHAN+","+ KABUPATEN+","+ PROPINSI+","+ KODE_POS+","+ NOMOR_TELEPON+"\n" //baris kedua
                }
                if (u.OF_!=null){
                    count++;
                    result += u.OF_+","+ u.KODE_OBJEK+","+ u.NAMA_OBJEK+","+ u.HARGA_SATUAN+","+ u.JUMLAH_BARANG+","+ HARGA_TOTAL+","+ DISKON+","+ u.DPP+","+ u.PPN+","+ TARIF_PPNBM+","+ PPNBM + "\n"
                }
            }

        }
        render(contentType: 'text/csv',text: result)
    }
    def massDeleteNew() {
        def hasil = "not"
        int jum = 0
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def d = EFaktur.findAllByDocNumber(it as String)
            d.each { da->
                if(da.getNOMOR_FAKTUR()){
                    jum++
                }
            }
            if(jum==0){
                hasil = "ok"
                EFaktur.findAllByDocNumber(it as String)*.delete()
            }
        }

        render hasil
    }
}
