package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.dao.DataIntegrityViolationException

class FakturPajakController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams=params

        def c = FakturPajak.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            eq("companyDealer",session?.userCompanyDealer);
            if(params."sCriteria_t711NoFakturPajak"){
                ilike("t711NoFakturPajak","%" + (params."sCriteria_t711NoFakturPajak" as String) + "%")
            }

            if(params."sCriteria_t711nama"){
                ilike("t711nama","%" + (params."sCriteria_t711nama" as String) + "%")
            }

            if(params."sCriteria_t711alamat"){
                ilike("t711alamat","%" + (params."sCriteria_t711alamat" as String) + "%")
            }

            if(params."sCriteria_t711npwp"){
                ilike("t711npwp","%" + (params."sCriteria_t711npwp" as String) + "%")
            }

            if(params."sCriteria_t711xNamaUser"){
                ilike("t711xNamaUser","%" + (params."sCriteria_t711xNamaUser" as String) + "%")
            }

            if(params."sCriteria_t711deskripsi"){
                ilike("t711deskripsi","%" + (params."sCriteria_t711deskripsi" as String) + "%")
            }

            if(params."sCriteria_invoiceT701"){
                invoiceT701{
                    ilike("t701NoInv","%" + (params."sCriteria_invoiceT701" as String) + "%")
                }
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t711NoFakturPajak: it.t711NoFakturPajak,

                    invoiceT701: it?.invoiceT701?.t701NoInv,

                    t711nama: it.t711nama,

                    t711alamat: it.t711alamat,

                    t711npwp: it.t711npwp,

                    t711deskripsi: it.t711deskripsi,

                    t711xNamaUser: it.t711xNamaUser,
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [fakturPajakInstance: new FakturPajak(params)]
    }

    def save() {
        def fakturPajakInstance = new FakturPajak(params)
        def inv = InvoiceT701.findByT701NoInvIlikeAndCompanyDealerAndT701StaDel(params?.no_invoiceT701,session?.userCompanyDealer,"0")
        if(!inv){
            flash.message = "Nomor Invoice Tidak ditemukan"
            render(view: "create", model: [fakturPajakInstance: fakturPajakInstance])
            return
        }

        def fp1 = FakturPajak.findByCompanyDealerAndT711NoFakturPajakIlikeAndStaDel(session?.userCompanyDealer,fakturPajakInstance?.t711NoFakturPajak?.trim(),"0");
        def fakturPajak2 = FakturPajak.createCriteria().get {
            eq("staDel","0")
            eq("invoiceT701",inv)
            eq("companyDealer",session?.userCompanyDealer)
        }

        if(fp1 || fakturPajak2){
            flash.message = "Nomor Faktur Pajak/Nomor Invoice Sudah Ada"
            render(view: "create", model: [fakturPajakInstance: fakturPajakInstance])
            return
        }
        fakturPajakInstance.companyDealer=session?.userCompanyDealer
        fakturPajakInstance.dateCreated=datatablesUtilService?.syncTime()
        fakturPajakInstance.invoiceT701=inv
        fakturPajakInstance.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
        fakturPajakInstance.lastUpdated=datatablesUtilService?.syncTime()
        fakturPajakInstance.lastUpdProcess= "INSERT"
        fakturPajakInstance.t711BiayaGoods=(inv?.t701PartsRp+inv?.t701OliRp+inv?.t701MaterialRp+inv?.t701SubletRp)
        fakturPajakInstance.t711BiayaJasa=inv?.t701JasaRp
        fakturPajakInstance.t711DasarKenaPajak=inv?.t701SubTotalRp
        fakturPajakInstance.t711HargaJual=inv?.t701TotalRp
        fakturPajakInstance.t711JenisFP=inv?.t701JenisInv
        fakturPajakInstance.t711KodeFP=fakturPajakInstance?.t711NoFakturPajak
        fakturPajakInstance.t711NoFakturPajak=fakturPajakInstance?.t711NoFakturPajak
        fakturPajakInstance.t711NoFakturPajakReff=inv?.t701NoInv_Reff
        fakturPajakInstance.t711PPN=inv.t701PPnRp
        fakturPajakInstance.t711PotHarga=inv.t701TotalDiscRp
        fakturPajakInstance.t711SeriFP="SF"
        fakturPajakInstance.t711Tahun=new Date().format("YYYY")
        fakturPajakInstance.t711TglJamFP=datatablesUtilService?.syncTime()
        fakturPajakInstance.t711TglKukuh=datatablesUtilService?.syncTime()
        fakturPajakInstance.t711UangMuka=inv?.t701BookingFee
        fakturPajakInstance.t711xDivisi=inv?.t701xDivisi
        fakturPajakInstance.t711xKet=inv.t701xKet
        fakturPajakInstance.staDel="0"
        fakturPajakInstance.t711deskripsi=fakturPajakInstance?.t711DeskripsiAkhirTahun
        fakturPajakInstance.dateCreated = datatablesUtilService?.syncTime()
        fakturPajakInstance.lastUpdated = datatablesUtilService?.syncTime()

        if (!fakturPajakInstance.save(flsush: true)) {
            render(view: "create", model: [fakturPajakInstance: fakturPajakInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), fakturPajakInstance.id])
        redirect(action: "show", id: fakturPajakInstance.id)
    }

    def show(Long id) {
        def fakturPajakInstance = FakturPajak.get(id)
        if (!fakturPajakInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), id])
            redirect(action: "list")
            return
        }

        [fakturPajakInstance: fakturPajakInstance]
    }

    def edit(Long id) {
        def fakturPajakInstance = FakturPajak.get(id)
        if (!fakturPajakInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), id])
            redirect(action: "list")
            return
        }

        [fakturPajakInstance: fakturPajakInstance]
    }

    def update(Long id, Long version) {
        def fakturPajakInstance = FakturPajak.get(id)
        if (!fakturPajakInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), id])
            redirect(action: "list")
            return
        }
        def fakturPajak = FakturPajak.createCriteria().get {
            eq("staDel","0")
            ilike("t711NoFakturPajak",params.t711NoFakturPajak)
            eq("companyDealer",session?.userCompanyDealer)
        }

        if(fakturPajak && fakturPajak?.id!=id){
            flash.message = "Nomor Faktur Pajak Sudah digunakan"
            render(view: "edit", model: [fakturPajakInstance: fakturPajakInstance])
            return
        }
        def inv = InvoiceT701.findByT701NoInvIlikeAndCompanyDealerAndT701StaDel(params?.no_invoiceT701,session?.userCompanyDealer,"0")
        if(!inv){
            flash.message = "Nomor Invoice Tidak ditemukan"
            render(view: "edit", model: [fakturPajakInstance: fakturPajakInstance])
            return
        }
        def fakturPajak2 = FakturPajak.createCriteria().get {
            eq("staDel","0")
            eq("invoiceT701",inv)
            eq("companyDealer",session?.userCompanyDealer)
        }

        if(fakturPajak2 && fakturPajak2?.id!=id){
            flash.message = "Nomor Invoice Sudah digunakan"
            render(view: "edit", model: [fakturPajakInstance: fakturPajakInstance])
            return
        }

        if (version != null) {
            if (fakturPajakInstance.version > version) {

                fakturPajakInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'fakturPajak.label', default: 'FakturPajak')] as Object[],
                        "Another user has updated this FakturPajak while you were editing")
                render(view: "edit", model: [fakturPajakInstance: fakturPajakInstance])
                return
            }
        }
        fakturPajakInstance.properties = params
        fakturPajakInstance.invoiceT701=inv
        fakturPajakInstance.lastUpdated=datatablesUtilService?.syncTime()
        fakturPajakInstance.lastUpdProcess= "UPDATE"
        fakturPajakInstance.t711BiayaGoods=(inv?.t701PartsRp+inv?.t701OliRp+inv?.t701MaterialRp+inv?.t701SubletRp)
        fakturPajakInstance.t711BiayaJasa=inv?.t701JasaRp
        fakturPajakInstance.t711DasarKenaPajak=inv?.t701SubTotalRp
        fakturPajakInstance.t711HargaJual=inv?.t701TotalRp
        fakturPajakInstance.t711JenisFP=inv?.t701JenisInv
        fakturPajakInstance.t711KodeFP=fakturPajakInstance?.t711NoFakturPajak
        fakturPajakInstance.t711NoFakturPajak=fakturPajakInstance?.t711NoFakturPajak
        fakturPajakInstance.t711NoFakturPajakReff=inv?.t701NoInv_Reff
        fakturPajakInstance.t711PPN=inv.t701PPnRp
        fakturPajakInstance.t711PotHarga=inv.t701TotalDiscRp
        fakturPajakInstance.t711SeriFP="SF"
        fakturPajakInstance.t711Tahun=new Date().format("YYYY")
        fakturPajakInstance.t711TglJamFP=datatablesUtilService?.syncTime()
        fakturPajakInstance.t711TglKukuh=datatablesUtilService?.syncTime()
        fakturPajakInstance.t711UangMuka=inv?.t701BookingFee
        fakturPajakInstance.t711xDivisi=inv?.t701xDivisi
        fakturPajakInstance.t711xKet=inv.t701xKet
        fakturPajakInstance.t711deskripsi=fakturPajakInstance?.t711DeskripsiAkhirTahun
        fakturPajakInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())

        if (!fakturPajakInstance.save(flush: true)) {
            render(view: "edit", model: [fakturPajakInstance: fakturPajakInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), fakturPajakInstance.id])
        redirect(action: "show", id: fakturPajakInstance.id)
    }

    def delete(Long id) {
        def fakturPajakInstance = FakturPajak.get(id)
        if (!fakturPajakInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), id])
            redirect(action: "list")
            return
        }

        try {
            fakturPajakInstance?.delete()
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'fakturPajak.label', default: 'FakturPajak'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(FakturPajak, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def previewFaktur(){

        List<JasperReportDef> reportDefList = []
        def FakturReportData = FakturReportData(params)
        def reportDef = new JasperReportDef(name:'fakturPajakPrint.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: FakturReportData)
        reportDefList.add(reportDef)

        def file = File.createTempFile("Faktur_Pajak_",".pdf")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def  FakturReportData(def params){
        def conversi = new Konversi()
        def reportData = new ArrayList();
        def data=[:]
        def faktur=FakturPajak.get(params?.id?.toLong())
        def totHrgJual = 0, dsrKenaPpn = 0
        totHrgJual = faktur?.t711BiayaJasa + faktur?.t711BiayaGoods + faktur?.invoiceT701?.t701AdmRp
        dsrKenaPpn = totHrgJual - faktur?.t711PotHarga
        if(faktur){
            data.put("kodeFakturPajak",faktur?.t711NoFakturPajak)
            data.put("namaPenjual",faktur?.invoiceT701?.companyDealer?.m011NamaNpwp)
            data.put("alamatPenjual",faktur?.invoiceT701?.companyDealer?.m011AlamatNpwp)
            data.put("npwpPenjual",faktur?.invoiceT701?.companyDealer?.m011NPWP)
            data.put("namaPembeli",faktur?.t711nama)
            data.put("alamatPembeli",faktur?.t711alamat)
            data.put("npwpPembeli",faktur?.t711npwp)
            data.put("hargaJasa",conversi.toRupiah(faktur?.t711BiayaJasa))
            data.put("hargaOther",conversi.toRupiah(faktur?.t711BiayaGoods))
            data.put("biayaAdm", conversi.toRupiah(faktur?.invoiceT701?.t701AdmRp))
            data.put("invoiceNo",faktur?.invoiceT701?.t701NoInv)
            data.put("totalHargaJual", conversi.toRupiah(totHrgJual))
            data.put("totalPotongan",conversi.toRupiah(faktur?.t711PotHarga))
            data.put("totalUangMuka",conversi.toRupiah(faktur?.t711UangMuka))
            data.put("totalKenaPajak",conversi.toRupiah(dsrKenaPpn))
            data.put("totalPPN",conversi.toRupiah(faktur?.t711PPN))
            data.put("tertanda",faktur?.t711xNamaUser)
            data.put("tanggalFaktur",faktur?.companyDealer?.kabKota?.m002NamaKabKota+", "+faktur?.invoiceT701?.t701TglJamInvoice?.format("dd/MM/YYYY"))
            data.put("biayagoods",Double.valueOf(faktur.t711BiayaGoods).longValue())
            reportData.add(data)
        }

        return reportData
    }

    def getInvoice(){
        def res = [:]
        def opts = []
        def noinvoice = InvoiceT701.createCriteria().list {
            eq("t701StaDel","0")
            eq("companyDealer",session?.userCompanyDealer)
            ilike("t701NoInv","%"+params.query+"%")
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            order("t701NoInv")
            maxResults(10);
        }
        noinvoice.each {
            opts<<it.t701NoInv
        }
        res."options" = opts
        render res as JSON
    }

    def loadDataInv(){
        String kataKunci=params.kataKunci
        InvoiceT701 Inv = null
        if(kataKunci){
            Inv=InvoiceT701.findByT701NoInvIlikeAndT701StaDelAndCompanyDealer("%"+kataKunci.trim()+"%","0",session?.userCompanyDealer)
        }
        def dataInv = [:]
        if (Inv==null){
            dataInv = [status: "Error", message: "Data tidak ditemukan"]
        }else{
            dataInv=[
                    nama    :Inv?.t701Customer,
                    alamat  :Inv?.t701Alamat,
                    npwp    :Inv?.t701NPWP
            ]

            dataInv = [status: "OK", InfoInvoicing: dataInv]
        }
        render dataInv as JSON
    }

}
