package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomerVehicle
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class VehicleFAController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = VehicleFA.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_customerVehicle") {
                eq("customerVehicle", params."sCriteria_customerVehicle")
            }

            if (params."sCriteria_historyCustomerVehicle") {
                eq("historyCustomerVehicle", params."sCriteria_historyCustomerVehicle")
            }

            if (params."sCriteria_fa") {
                eq("fa", params."sCriteria_fa")
            }

            if (params."sCriteria_t185StaReminderFA") {
                ilike("t185StaReminderFA", "%" + (params."sCriteria_t185StaReminderFA" as String) + "%")
            }

            if (params."sCriteria_t185StaSudahDiperiksa") {
                ilike("t185StaSudahDiperiksa", "%" + (params."sCriteria_t185StaSudahDiperiksa" as String) + "%")
            }

            if (params."sCriteria_t185StaSudahDikerjakan") {
                ilike("t185StaSudahDikerjakan", "%" + (params."sCriteria_t185StaSudahDikerjakan" as String) + "%")
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_t185TanggalDikerjakan") {
                ge("t185TanggalDikerjakan", params."sCriteria_t185TanggalDikerjakan")
                lt("t185TanggalDikerjakan", params."sCriteria_t185TanggalDikerjakan" + 1)
            }

            if (params."sCriteria_t185Lokasi") {
                ilike("t185Lokasi", "%" + (params."sCriteria_t185Lokasi" as String) + "%")
            }

            if (params."sCriteria_t185MainDealer") {
                ilike("t185MainDealer", "%" + (params."sCriteria_t185MainDealer" as String) + "%")
            }

            if (params."sCriteria_t185Dealer") {
                ilike("t185Dealer", "%" + (params."sCriteria_t185Dealer" as String) + "%")
            }

            if (params."sCriteria_t185Ket") {
                ilike("t185Ket", "%" + (params."sCriteria_t185Ket" as String) + "%")
            }

            if (params."sCriteria_t185xNamaUser") {
                ilike("t185xNamaUser", "%" + (params."sCriteria_t185xNamaUser" as String) + "%")
            }

            if (params."sCriteria_t185xNamaDivisi") {
                ilike("t185xNamaDivisi", "%" + (params."sCriteria_t185xNamaDivisi" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    customerVehicle: it.customerVehicle?.toString(),

                    historyCustomerVehicle: it.historyCustomerVehicle?.toString(),

                    fa: it.fa?.toString(),

                    t185StaReminderFA: it.t185StaReminderFA?.toString(),

                    t185StaSudahDiperiksa: it.t185StaSudahDiperiksa?.toString(),

                    t185StaSudahDikerjakan: it.t185StaSudahDikerjakan?.toString(),

                    companyDealer: it.companyDealer?.toString(),

                    t185TanggalDikerjakan: it.t185TanggalDikerjakan ? it.t185TanggalDikerjakan.format(dateFormat) : ""?.toString(),

                    t185Lokasi: it.t185Lokasi?.toString(),

                    t185MainDealer: it.t185MainDealer?.toString(),

                    t185Dealer: it.t185Dealer?.toString(),

                    t185Ket: it.t185Ket?.toString(),

                    t185xNamaUser: it.t185xNamaUser?.toString(),

                    t185xNamaDivisi: it.t185xNamaDivisi?.toString(),

                    createdBy: it.createdBy?.toString(),

                    updatedBy: it.updatedBy?.toString(),

                    lastUpdProcess: it.lastUpdProcess?.toString(),

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
    String dateFormat = "dd/MM/yyyy"


    def vehicleListDatatablesList(){
//        def vehicleFA = VehicleFA.read(params.vehicleFAInstanceId)
//        def ret



        def c = HistoryCustomerVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    nopol: it.kodeKotaNoPol?.m116ID + " " + it.t183NoPolTengah + " " + it.t183NoPolBelakang,
                    vinCode: it.customerVehicle?.t103VinCode,
                    fullModel: it.fullModelCode?.t110FullModelCode,
                    model: it.fullModelCode?.baseModel?.m102NamaBaseModel,
                    tanggalDEC: it.t183TglDEC ? it.t183TglDEC.format(dateFormat) : "",
                    nomorMesin: it.t183NoMesin,
                    warna: it.warna?.m092NamaWarna,
                    nomorKunci: it.t183NoKunci,
                    tanggalSTNK: it.t183TglSTNK ? it.t183TglSTNK.format(dateFormat) : "",
                    tahunDanBulanRakit: it.t183ThnBlnRakit
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }


    def create() {
        [vehicleFAInstance: new VehicleFA(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def vehicleFAInstance = new VehicleFA(params)
        vehicleFAInstance.companyDealer = session.userCompanyDealer
        vehicleFAInstance.lastUpdProcess = "INSERT"
        if (!vehicleFAInstance.save(flush: true)) {
            render(view: "create", model: [vehicleFAInstance: vehicleFAInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), vehicleFAInstance.id])
        redirect(action: "show", id: vehicleFAInstance.id)
    }

    def show(Long id) {
        def vehicleFAInstance = VehicleFA.get(id)
        if (!vehicleFAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), id])
            redirect(action: "list")
            return
        }

        [vehicleFAInstance: vehicleFAInstance]
    }

    def edit(Long id) {
        def vehicleFAInstance = VehicleFA.get(id)
        if (!vehicleFAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), id])
            redirect(action: "list")
            return
        }

        [vehicleFAInstance: vehicleFAInstance]
    }

    def update(Long id, Long version) {
        def vehicleFAInstance = VehicleFA.get(id)
        if (!vehicleFAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (vehicleFAInstance.version > version) {

                vehicleFAInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'vehicleFA.label', default: 'VehicleFA')] as Object[],
                        "Another user has updated this VehicleFA while you were editing")
                render(view: "edit", model: [vehicleFAInstance: vehicleFAInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        vehicleFAInstance.properties = params
        vehicleFAInstance.lastUpdProcess = "UPDATE"

        if (!vehicleFAInstance.save(flush: true)) {
            render(view: "edit", model: [vehicleFAInstance: vehicleFAInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), vehicleFAInstance.id])
        redirect(action: "show", id: vehicleFAInstance.id)
    }

    def delete(Long id) {
        def vehicleFAInstance = VehicleFA.get(id)
        if (!vehicleFAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), id])
            redirect(action: "list")
            return
        }

        try {
            vehicleFAInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'vehicleFA.label', default: 'VehicleFA'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(VehicleFA, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(VehicleFA, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
