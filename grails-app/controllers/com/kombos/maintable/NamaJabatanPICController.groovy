package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class NamaJabatanPICController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def namaJabatanPICService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render namaJabatanPICService.datatablesList(params) as JSON
	}

	def create() {
		def result = namaJabatanPICService.create(params)

        if(!result.error)
            return [namaJabatanPICInstance: result.namaJabatanPICInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		params?.dateCreated = datatablesUtilService?.syncTime()
		params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = namaJabatanPICService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["NamaJabatanPIC", result.namaJabatanPICInstance.id])
            redirect(action:'show', id: result.namaJabatanPICInstance.id)
            return
        }

        render(view:'create', model:[namaJabatanPICInstance: result.namaJabatanPICInstance])
	}

	def show(Long id) {
		def result = namaJabatanPICService.show(params.id)

		if(!result.error)
			return [ namaJabatanPICInstance: result.namaJabatanPICInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = namaJabatanPICService.show(params.id)

		if(!result.error)
			return [ namaJabatanPICInstance: result.namaJabatanPICInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params?.lastUpdated = datatablesUtilService?.syncTime()
         def result = namaJabatanPICService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["NamaJabatanPIC", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[namaJabatanPICInstance: result.namaJabatanPICInstance.attach()])
	}

	def delete() {
		def result = namaJabatanPICService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["NamaJabatanPIC", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(NamaJabatanPIC, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
