package com.kombos.maintable

import com.kombos.administrasi.FlatRate
import com.kombos.administrasi.KegiatanApproval
import com.kombos.parts.Konversi
import com.kombos.parts.PickingSlip
import com.kombos.parts.PickingSlipDetail
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON

class JobOrderCompletionController {

    def datatablesUtilService

    def index() {
    }

    def approvalFinish() {
    }

    def sendApprovalJOC()
    {
        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def reception = Reception.findByT401NoWOAndStaDel(params.nowo,"0")

        def joc = new JOCTECO()
        joc?.reception = reception
        joc?.t601staSPK = params.staPks
        if(params.staBawaPks){
            joc?.t601BawaSPK = params.staBawaPks
        }
        joc?.t601staAsuransi = params.staAsurans
        if(params.staBawaAsuransi){
            joc?.t601BawaAsuransi = params.staBawaAsuransi
        }
        String waktu = params.waktuJOC
        joc?.t601staOkCancelJOC = "2"
        joc?.t601NamaSAJOC = org.apache.shiro.SecurityUtils.subject.principal.toString()
        joc?.t601TglJamJOC = datatablesUtilService?.syncTime()
        joc?.t601TglJamJOCSelesai = datatablesUtilService?.syncTime()
        joc?.t601staOkCancelExp="2"
        joc?.t601xKet = params.pesan
        joc?.t601xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
        joc?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        joc?.lastUpdProcess = "INSERT"
        joc?.lastUpdated = datatablesUtilService?.syncTime()
        joc?.dateCreated = datatablesUtilService?.syncTime()
        joc?.staDel = "0"
        joc?.save(flush: true)
        joc?.errors?.each {println it}

        def approval = new ApprovalT770(
                t770FK:joc?.id,
                kegiatanApproval: KegiatanApproval.findByM770KegiatanApprovalAndStaDel(KegiatanApproval.FINISH_JOC,"0"),
                t770NoDokumen: reception?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                companyDealer: session?.userCompanyDealer,
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{ println it }
        render "ok"
    }

    def loadWorkOrder() {
        String kategoriView = params.kategoriView
        String kataKunci = params.kataKunci

        Reception wo = null;
        if (kategoriView.trim().equalsIgnoreCase("NOMOR WO")) {
            wo = Reception.findByT401NoWOIlikeAndStaDelAndStaSave("%"+kataKunci?.trim()+"%","0","0", [sort: "dateCreated", order: "desc"])
        } else {
            def temp = Reception.withCriteria {
                historyCustomerVehicle {
                    eq("fullNoPol", kataKunci?.trim(),[ignoreCase:true])
                }
                eq("staDel","0");
                eq("staSave","0");
                order("dateCreated", "desc");
            }
            wo = temp.size() > 0 ? temp.get(0) : null
        }

        if (wo == null) {
            def result = [status: "Error", message: "Data tidak ditemukan"]
            render result as JSON
            return
        }


        def jobRCPs = JobRCP.findAllByReceptionAndStaDel(wo,"0")
        def hasils = []
        jobRCPs.each { jobRCP ->
                def partsRCPs = PartsRCP.findAllByOperationAndReceptionAndStaDel(jobRCP?.operation,jobRCP?.reception,"0")
                def hasil = [
                        col1: jobRCP?.operation?.m053JobsId,
                        col2: jobRCP?.operation?.m053NamaOperation,
                        col3: jobRCP?.t402Rate,
                        col4: "",
                        col5: true
                ]
                hasils << hasil

                partsRCPs.each { partsRCP ->
                    hasil = [
                            col1: "",
                            col2: partsRCP?.goods?.m111ID,
                            col3: partsRCP?.goods?.m111Nama,
                            col4: partsRCP?.t403Jumlah1 ? partsRCP?.t403Jumlah1+" "+ (partsRCP?.goods?.satuan?.m118Satuan1 ? partsRCP?.goods?.satuan?.m118Satuan1:"") : "",
                            col5: false
                    ]
                    hasils << hasil
                }
        }



        def result = [status: "OK", hasils: hasils]
        render result as JSON
    }


    def loadActualPengerjaanJob() {
        String kategoriView = params.kategoriView
        String kataKunci = params.kataKunci

        Reception wo = null;
        if (kategoriView.trim().equalsIgnoreCase("NOMOR WO")) {
            wo = Reception.findByT401NoWOIlikeAndStaDelAndStaSave("%"+kataKunci?.trim()+"%","0","0", [sort: "dateCreated", order: "desc"])
        } else {
            def temp = Reception.withCriteria {
                eq("companyDealer",session.userCompanyDealer)
                historyCustomerVehicle {
                    eq("fullNoPol", kataKunci?.trim(),[ignoreCase:true])
                }
                eq("staDel","0");
                eq("staSave","0");
                order("dateCreated", "desc");
            }
            wo = temp.size() > 0 ? temp.get(0) : null
        }

        if (wo == null) {
            def result = [status: "Error", message: "Data tidak ditemukan"]
            render result as JSON
            return
        }


        def actuals = Actual.findAllByReception(wo)
        def hasils = []
        def cekJob = [];
        actuals.each { actual ->
            if(cekJob?.indexOf(actual?.operation?.id)<0){
                def flatRate = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(actual?.operation,wo?.historyCustomerVehicle?.fullModelCode?.baseModel,"0",new Date(),[sort: 'id',order: 'desc'])

                def hasil = [
                        col1: actual?.operation?.m053JobsId,
                        col2: actual?.operation?.m053NamaOperation,
                        col3: flatRate?.t113FlatRate,
                        col4: "",
                        col5: true
                ]
                hasils << hasil

                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(wo,actual?.operation,"0")
                parts.each { goods ->
                    def pickingSlips = PickingSlip.findAllByReception(wo)
                    pickingSlips.each { pickingSlip ->

                        def pickingSlipDetails = PickingSlipDetail.findAllByPickingSlipAndGoods(pickingSlip,goods?.goods);
                        pickingSlipDetails.each { pickingSlipDetail ->
                            hasil = [
                                    col1: "",
                                    col2: pickingSlipDetail?.goods?.m111ID,
                                    col3: pickingSlipDetail?.goods?.m111Nama,
                                    col4: (pickingSlipDetail?.t142Qty1+" "+pickingSlipDetail?.goods?.satuan?.m118Satuan1),
                                    col5: false
                            ]
                            hasils << hasil
                        }
                    }
                }
                cekJob << actual?.operation?.id
            }
        }



        def result = [status: "OK", hasils: hasils]
        render result as JSON
    }


    def loadPenambahanPenguranganJob() {
        String kategoriView = params.kategoriView
        String kataKunci = params.kataKunci

        Reception wo = null;
        if (kategoriView.trim().equalsIgnoreCase("NOMOR WO")) {
            wo = Reception.findByT401NoWOIlikeAndStaDelAndStaSave("%"+kataKunci?.trim()+"%","0","0", [sort: "dateCreated", order: "desc"])
        } else {
            def temp = Reception.withCriteria {
                historyCustomerVehicle {
                    eq("fullNoPol", kataKunci?.trim(),[ignoreCase:true])
                }
                eq("staDel","0");
                eq("staSave","0");
                order("dateCreated", "desc");
            }
            wo = temp.size() > 0 ? temp.get(0) : null
        }

        if (wo == null) {
            def result = [status: "Error", message: "Data tidak ditemukan"]
            render result as JSON
            return
        }


        def jobRCPs = JobRCP.findAllByReceptionAndStaDel(wo,"0")
        def hasils = []
        jobRCPs.each { jobRCP ->
            if(jobRCP?.t402StaTambahKurang){
                def partsRCPs = PartsRCP.findAllByOperationAndReceptionAndStaDel(jobRCP?.operation,wo,"0")

                def hasil = [
                        col1: jobRCP?.operation?.m053JobsId,
                        col2: jobRCP?.operation?.m053NamaOperation,
                        col3: jobRCP?.t402Rate,
                        col4: jobRCP?.t402StaTambahKurang ?  (jobRCP?.t402StaTambahKurang?.equals("0") ? "Tambah" : "Kurang") : "",
                        col5: true,
                        col6: jobRCP?.t402xKet
                ]
                hasils << hasil

                partsRCPs.each { partsRCP ->
                    hasil = [
                            col1: "",
                            col2: partsRCP?.goods?.m111ID,
                            col3: partsRCP?.goods?.m111Nama,
                            col4: partsRCP?.t403Jumlah1,
                            col5: false,
                            col6: partsRCP?.goods?.satuan?.m118Satuan1,
                            col7: partsRCP?.t403StaTambahKurang ? (partsRCP?.t403StaTambahKurang?.equals("0") ? "Tambah" : "Kurang") : "",
                            col8: partsRCP?.t403xKet
                    ]
                    hasils << hasil
                }
            }
        }



        def result = [status: "OK", hasils: hasils]
        render result as JSON
    }


    def loadInformasiUmum() {
        String kategoriView = params.kategoriView
        String kataKunci = params.kataKunci

        def wo = null;
        if (kategoriView.trim().equalsIgnoreCase("NOMOR WO")) {
            wo = Reception.findByT401NoWOIlikeAndStaDelAndStaSave("%"+kataKunci?.trim()+"%","0","0", [sort: "dateCreated", order: "desc"])
        } else {
            def temp = Reception.withCriteria {
                historyCustomerVehicle {
                    eq("fullNoPol", kataKunci?.trim(),[ignoreCase:true])
                }
                eq("staDel","0");
                eq("staSave","0");
                order("dateCreated", "desc");
            }
            wo = temp.size() > 0 ? temp.get(0) : null
        }
        if (wo == null) {
            def result = [status: "Error", message: "Data tidak ditemukan"]
            render result as JSON
            return
        }
        def totJasa = 0;
        def totParts = 0;
        def discJasa = 0;
        def discPart = 0;
        def bookingFee = (wo?.t401DPRp ? wo?.t401DPRp : 0);
        if(wo){
            def jobs= JobRCP.createCriteria().listDistinct {
                eq("staDel","0")
                eq("reception",wo)
                or{
                    ilike("t402StaTambahKurang","0")
                    and{
                        not{
                            ilike("t402StaTambahKurang","%1%")
                            ilike("t402StaApproveTambahKurang","%1%")
                        }
                    }
                    and {
                        isNull("t402StaTambahKurang")
                        isNull("t402StaApproveTambahKurang")
                    }
                }
            }
            jobs.each {
                totJasa+= it?.t402HargaRp
                discJasa+= (it?.t402DiscRp ? it?.t402DiscRp : 0)
            }
//            def parts = PartsRCP.findAllByReceptionAndStaDel(wo,"0");
            def parts= PartsRCP.createCriteria().listDistinct {
                eq("staDel","0")
                eq("reception",wo)
                or{
                    ilike("t403StaTambahKurang","%0%")
                    and{
                        not{
                            ilike("t403StaTambahKurang","%1%")
                            ilike("t403StaApproveTambahKurang","%1%")
                        }
                    }
                    and{
                        isNull("t403StaTambahKurang")
                        isNull("t403StaApproveTambahKurang")
                    }
                }
            }
            parts.each {
                totParts+= it?.t403HargaRp
                discPart+= (it?.t403DiscRp ? it?.t403DiscRp : 0)
                bookingFee += (it?.t403DPRp ? it?.t403DPRp : 0)
            }
        }
        def staJoc = JOCTECO.findByReceptionAndStaDel(wo,"0")
        def konversi = new Konversi()
        def hasil = [
                noWo: wo?.t401NoWO,
                nopol: wo?.historyCustomerVehicle?.fullNoPol,
                model: wo?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                tanggalWO: wo?.t401TglJamCetakWO ? wo?.t401TglJamCetakWO?.format("dd/MM/yyyy HH:mm") : "",
                waktuPenyerahan: wo.t401TglJamPenyerahan ? wo.t401TglJamPenyerahan?.format("dd/MM/yyyy HH:mm") : "",
                SPKAss: wo?.sPkAsuransi ? "Ya" : "Tidak",
                SPKAsuransi: wo?.sPkAsuransi?.t193NomorPolis?wo?.sPkAsuransi?.t193NomorPolis:"",
                pks:wo?.sPK ? "Ya" : "Tidak",
                saldoPKS: wo?.sPK ? wo?.sPK?.t191JmlSPK : "",
                staJoc : staJoc ? "FINISH JOC" : "START JOC",
                subTotal : konversi.toRupiah(totJasa+totParts),
                discJasa : konversi.toRupiah(discJasa),
                discParts : konversi.toRupiah(discPart),
                bookingFee : konversi.toRupiah(bookingFee),
                sisaBayar : konversi.toRupiah(totJasa+totParts-discJasa-discPart-bookingFee)
        ]

        def result = [status: "OK", hasil: hasil]
        render result as JSON
    }

}
