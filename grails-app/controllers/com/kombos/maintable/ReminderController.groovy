package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ReminderController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {

        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def searchReminder = [-1000L]
        if(params."searchReminder"){
            params?."searchReminder"?.toString()?.split(",")?.each {
                searchReminder.add(it?.trim()?.toLong())
            }
        }

        def c = Reminder.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)
            m201ID {
                inList("id", searchReminder)
            }

            if (params."km" && params."km".toString()?.isNumber()) {
                if (params."oprKm"?.toString()?.equalsIgnoreCase("=")) {
                    eq("t201LastKM", params."km".toString()?.toDouble()?.intValue())
                } else if (params."oprKm"?.toString()?.equalsIgnoreCase(">")) {
                    gt("t201LastKM", params."km".toString()?.toDouble()?.intValue())
                } else if (params."oprKm"?.toString()?.equalsIgnoreCase("<")) {
                    lt("t201LastKM", params."km".toString()?.toDouble()?.intValue())
                } else if (params."oprKm"?.toString()?.equalsIgnoreCase(">=")) {
                    ge("t201LastKM", params."km".toString()?.toDouble()?.intValue())
                } else if (params."oprKm"?.toString()?.equalsIgnoreCase("<=")) {
                    le("t201LastKM", params."km".toString()?.toDouble()?.intValue())
                } else if (params."oprKm"?.toString()?.equalsIgnoreCase("!=")) {
                    ne("t201LastKM", params."km".toString()?.toDouble()?.intValue())
                }
            }

            if (params."nextKm" && params."nextKm".toString()?.isNumber()) {
                operation {
                    if (params."oprNextKm"?.toString()?.equalsIgnoreCase("=")) {
                        eq("m053Km", params."nextKm".toString()?.toDouble())
                    } else if (params."oprNextKm"?.toString()?.equalsIgnoreCase("<")) {
                        gt("m053Km", params."nextKm".toString()?.toDouble())
                    } else if (params."oprNextKm"?.toString()?.equalsIgnoreCase(">")) {
                        lt("m053Km", params."nextKm".toString()?.toDouble())
                    } else if (params."oprNextKm"?.toString()?.equalsIgnoreCase("<=")) {
                        ge("m053Km", params."nextKm".toString()?.toDouble())
                    } else if (params."oprNextKm"?.toString()?.equalsIgnoreCase(">=")) {
                        le("m053Km", params."nextKm".toString()?.toDouble())
                    } else if (params."oprNextKm"?.toString()?.equalsIgnoreCase("!=")) {
                        ne("m053Km", params."nextKm".toString()?.toDouble())
                    }
                }
            }

            if (params."startJatuhTempo") {
                ge("t201TglJatuhTempo", params."startJatuhTempo")
            }
            if (params."endJatuhTempo") {
                le("t201TglJatuhTempo", params."endJatuhTempo" + 1)
            }

//            if (params."startDM") {
//                ge("t201TglDM", params."startDM")
//            }
//            if (params."endDM") {
//                le("t201TglDM", params."endDM" + 1)
//            }

            if (params."startCall") {
                ge("t201TglCall", params."startCall")
            }
            if (params."endCall") {
                le("t201TglCall", params."endCall" + 1)
            }

            if (params."startSMS") {
                ge("t201TglSMS", params."startSMS")
            }

            if (params."endSMS") {
                le("t201TglSMS", params."endSMS" + 1)
            }

            if (params."startEmail") {
                ge("t201TglEmail", params."startEmail")
            }

            if (params."endEmail") {
                le("t201TglEmail", params."endEmail" + 1)
            }

            if (params."startSTSKirim") {
                ge("staKirim", params."startSTSKirim")
            }
            if (params."endSTSKirim") {
                le("staKirim", params."endSTSKirim" + 1)
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {

            def currentCondition = it?.customerVehicle?.currentCondition
            def custVehicle = currentCondition?.customerVehicle
            def mappingCV = MappingCustVehicle.findByCustomerVehicle(custVehicle, [sort: "id", order: "desc"])
            def customer = mappingCV?.customer
            def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "id", order: "desc"])
            def nextKm = 0
            def lastKm = it?.t201LastKM ? it?.t201LastKM :0
            if(lastKm < 1000 && lastKm > 0){
                nextKm = 1000
            }else {
                nextKm = lastKm + 10000
            }

            rows << [

                    id: it?.id,

                    m201ID: it?.m201ID?.m201NamaJenisReminder,

                    nopol: currentCondition?.fullNoPol,

                    nama: currentCondition?.t183NamaSTNK,

                    alamat: currentCondition?.t183AlamatSTNK,

                    telp: historyCustomer?.t182NoTelpRumah ? historyCustomer?.t182NoTelpRumah:"-",

                    hp: historyCustomer?.t182NoHp ? historyCustomer?.t182NoHp:"-",

                    t201LastKM: it?.t201LastKM,

                    nextKM: nextKm,

                    t201TglJatuhTempo: it?.t201TglJatuhTempo ? it?.t201TglJatuhTempo.format(dateFormat) : "",

//                    t201TglDM: it?.t201TglDM ? it?.t201TglDM.format(dateFormat) : "",

                    t201TglSMS: it?.t201TglSMS ? it?.t201TglSMS.format(dateFormat) : "",

                    t201TglEmail: it?.t201TglEmail ? it?.t201TglEmail.format(dateFormat) : "",

                    t201STSkirim: it?.staKirim==0 ? "Tidak Terkirim" : "Terkirim"

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [reminderInstance: new Reminder(params)]
    }

    def save() {
        params.companyDealer = session.userCompanyDealer
        params.staDel = "0"
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def custVehicle = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("t103VinCode",params.customerVehicles.trim(),[ignoreCase:true])
        }

        if(custVehicle.size()<1){
            flash.message = "Vincode tidak ditemukan/salah"
            render(view: "create", model: [reminderInstance: reminderInstance])
            return
        }

        params.customerVehicle = custVehicle.last()
        def reminderInstance = new Reminder(params)
        def cek = Reminder.createCriteria().list {
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)
            customerVehicle{
                eq("t103VinCode",params.customerVehicles,[ignoreCase:true])
            }

            m201ID{
                eq("id",params.m201ID.id.toLong())
            }
            if(params.t201LastKM){
                eq("t201LastKM",params.t201LastKM.toInteger())
            }
            if(params.t201LastServiceDate){
                eq("t201LastServiceDate",params.t201LastServiceDate)
            }
            if(params.t201TglJatuhTempo){
                ge("t201TglJatuhTempo",params."t201TglJatuhTempo")
                lt("t201TglJatuhTempo",params."t201TglJatuhTempo" + 1)
            }
//            if(params.t201TglDM){
//                ge("t201TglDM",params."t201TglDM")
//                lt("t201TglDM",params."t201TglDM" + 1)
//            }
            if(params.t201TglSMS){
                ge("t201TglSMS",params."t201TglSMS")
                lt("t201TglSMS",params."t201TglSMS" + 1)
            }
            if(params.t201TglEmail){
                ge("t201TglEmail",params."t201TglEmail")
                lt("t201TglEmail",params."t201TglEmail" + 1)
            }
            if(params.t201TglCall){
                ge("t201TglCall",params."t201TglCall")
                lt("t201TglCall",params."t201TglCall" + 1)
            }
            if(params.staKirim){
                ge("staKirim",params."staKirim")
                lt("staKirim",params."staKirim" + 1)
            }
        }

        if(cek){
            flash.message = "Data Sudah Ada"
            render(view: "create", model: [reminderInstance: reminderInstance])
            return
        }else{
            reminderInstance.save(flush: true)

            if(params."m201ID".id.toLong() == 1){

                def hcv = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(params.customerVehicle, "0")

                hcv.t183StaReminderSBI = "1"
                hcv.lastUpdated = datatablesUtilService?.syncTime()
                hcv.save(flush: true)

                def jenisInvitation = JenisInvitation.findByM204JenisInvitation("SMS")
                def invitation = new Invitation()

                invitation.reminder = reminderInstance
                invitation.companyDealer = reminderInstance?.companyDealer
                invitation.jenisInvitation = jenisInvitation
                invitation.customerVehicle = hcv.customerVehicle
                invitation.t202TglNextReContact = new Date()
                invitation.t202KetReContact = "-"
                invitation.dateCreated = datatablesUtilService?.syncTime()
                invitation.lastUpdated = datatablesUtilService?.syncTime()

                invitation.save(flush: true)
                invitation.errors.each {println "INVITATION SBI "+it}
            }

            if(params."m201ID".id.toLong() == 2 || params."m201ID".id.toLong() == 3 || params."m201ID".id.toLong() == 4 || params."m201ID".id.toLong() == 5){

                def hcv = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(params.customerVehicle, "0")
                def jenisInvitation = JenisInvitation.findByM204JenisInvitation("SMS")
                def invitation = new Invitation()

                invitation.reminder = reminderInstance
                invitation.companyDealer = reminderInstance?.companyDealer
                invitation.jenisInvitation = jenisInvitation
                invitation.customerVehicle = hcv.customerVehicle
                invitation.t202TglNextReContact = new Date()
                invitation.t202KetReContact = "-"
                invitation.dateCreated = datatablesUtilService?.syncTime()
                invitation.lastUpdated = datatablesUtilService?.syncTime()

                invitation.save(flush: true)
                invitation.errors.each {println "INVITATION SBE "+it}
            }
        }

        if (!reminderInstance.save(flush: true)) {
            render(view: "create", model: [reminderInstance: reminderInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'reminder.label', default: 'Reminder'), reminderInstance.id])
        redirect(action: "show", id: reminderInstance.id)
    }

    def show(Long id) {
        def reminderInstance = Reminder.get(id)
        if (!reminderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reminder.label', default: 'Reminder'), id])
            redirect(action: "list")
            return
        }

        [reminderInstance: reminderInstance]
    }

    def edit(Long id) {
        def reminderInstance = Reminder.get(id)
        if (!reminderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reminder.label', default: 'Reminder'), id])
            redirect(action: "list")
            return
        }

        [reminderInstance: reminderInstance]
    }

    def update(Long id, Long version) {
        def reminderInstance = Reminder.get(id)

        if (!reminderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reminder.label', default: 'Reminder'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (reminderInstance.version > version) {

                reminderInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'reminder.label', default: 'Reminder')] as Object[],
                        "Another user has updated this Reminder while you were editing")
                render(view: "edit", model: [reminderInstance: reminderInstance])
                return
            }
        }

        params.companyDealer = session.userCompanyDealer
        def custVehicle = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("t103VinCode",params.customerVehicles.trim(),[ignoreCase:true])
        }

        if(custVehicle.size()<1){
            flash.message = "Vincode tidak ditemukan/salah"
            render(view: "edit", model: [reminderInstance: reminderInstance])
            return
        }
        def cek = Reminder.createCriteria().list {
            eq("staDel","0")
            customerVehicle{
                eq("t103VinCode",params.customerVehicles.trim(),[ignoreCase:true])
            }
            m201ID{
                eq("id",params.m201ID.id as Long)
            }
            if(params.t201LastKM){
                eq("t201LastKM",params.t201LastKM as int)
            }
            if(params.t201LastServiceDate){
                ge("t201LastServiceDate",params."t201LastServiceDate")
                lt("t201LastServiceDate",params."t201LastServiceDate" + 1)
            }
            if(params.t201TglJatuhTempo){
                ge("t201TglJatuhTempo",params."t201TglJatuhTempo")
                lt("t201TglJatuhTempo",params."t201TglJatuhTempo" + 1)
            }
//            if(params.t201TglDM){
//                ge("t201TglDM",params."t201TglDM")
//                lt("t201TglDM",params."t201TglDM" + 1)
//            }
            if(params.t201TglSMS){
                ge("t201TglSMS",params."t201TglSMS")
                lt("t201TglSMS",params."t201TglSMS" + 1)
            }
            if(params.t201TglEmail){
                ge("t201TglEmail",params."t201TglEmail")
                lt("t201TglEmail",params."t201TglEmail" + 1)
            }
            if(params.t201TglCall){
                ge("t201TglCall",params."t201TglCall")
                lt("t201TglCall",params."t201TglCall" + 1)
            }
            if(params.staKirim){
                ge("staKirim",params."staKirim")
                1t("staKirim",params."staKirim" + 1)
            }
        }
        if(cek){
            for(cari in cek){
                if(id!=cek.id){
                    flash.message = "Data Sudah Ada"
                    render(view: "create", model: [reminderInstance: reminderInstance])
                    return
                    break
                }
            }
        }
        params?.lastUpdated = datatablesUtilService?.syncTime()
        params?.customerVehicle = custVehicle?.last()
        reminderInstance.properties = params

        if (!reminderInstance.save(flush: true)) {
            render(view: "edit", model: [reminderInstance: reminderInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'reminder.label', default: 'Reminder'), reminderInstance.id])
        redirect(action: "show", id: reminderInstance.id)
    }

    def delete(Long id) {
        def reminderInstance = Reminder.get(id)
        def invitIns = Invitation.findByReminderAndT202StaDel(reminderInstance, "0")
        if (!reminderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reminder.label', default: 'Reminder'), id])
            redirect(action: "list")
            return
        }

        try {
            reminderInstance.lastUpdProcess = "DELETE"
            reminderInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            reminderInstance.staDel = "1"
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'reminder.label', default: 'Reminder'), id])

            invitIns.t202StaDel = "1"
            invitIns.lastUpdProcess = "delete"
            invitIns.lastUpdated = datatablesUtilService?.syncTime()
            invitIns.save(flush: true)
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'reminder.label', default: 'Reminder'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Reminder, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }

        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Reminder, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
