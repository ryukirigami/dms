package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.reception.Reception
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification
import org.hibernate.criterion.Criterion
import org.hibernate.criterion.MatchMode
import org.hibernate.criterion.Restrictions

import java.text.SimpleDateFormat

class SendSMSRetentionController {

    def laporanService

    def smsService

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def index() {

    }


    def addParameterSearch() {
        def data = session.searchCriteriaRetention
        if (data == null) {
            data = new HashMap()
        }

        def nilai = [params.andOr, params.kolom, params.kondisi, params.nilai, params.nilai1]
        data[params.id] = nilai

        session.searchCriteriaRetention = data;

        def result = [status: "OK"]
        render result as JSON
    }

    def hapusParameterSearch() {
        def data = session.searchCriteriaRetention
        if (data == null) {
            data = new HashMap()
        }
        data.remove(params.id)

        session.searchCriteriaRetention = data;

        def result = [status: "OK"]
        render result as JSON
    }

    def clearParameterSearch() {
        def data = new HashMap()
        session.searchCriteriaRetention = data;

        def result = [status: "OK"]
        render result as JSON
    }

    def datatablesList() {

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        HashMap data = session.searchCriteriaRetention
        if (data == null) {
            data = new HashMap()
        }

        def historyCustomerId = []
        Criterion restrictions = Restrictions.sqlRestriction("1=1")
        boolean ada = false;
        for (def it : data.values()) {
            String andOr = it[0];
            String kolom = it[1];
            String kondisi = it[2];
            def nilai = it[3];
            def nilai1 = it[4];

            if (kolom.equalsIgnoreCase("t401NoWO") || kolom.equalsIgnoreCase("t401TanggalWO")) {

                if (kolom?.equalsIgnoreCase("t401TanggalWO")) {
                    if (nilai && !nilai?.equals("")) {
                        nilai = new SimpleDateFormat("dd/MM/yyyy").parse(nilai)
                    }
                    if (nilai1 && !nilai1?.equals("")) {
                        nilai1 = new SimpleDateFormat("dd/MM/yyyy").parse(nilai1)
                    }
                }

                if (kondisi?.equalsIgnoreCase("LIKE")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.ilike(kolom, nilai?.toString(), MatchMode.ANYWHERE))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.ilike(kolom, nilai?.toString(), MatchMode.ANYWHERE))
                    }
                } else if (kondisi?.equalsIgnoreCase("=")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.eq(kolom, nilai))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.eq(kolom, nilai))
                    }
                } else if (kondisi?.equalsIgnoreCase("<")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.lt(kolom, nilai))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.lt(kolom, nilai))
                    }
                } else if (kondisi?.equalsIgnoreCase(">")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.gt(kolom, nilai))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.gt(kolom, nilai))
                    }
                } else if (kondisi?.equalsIgnoreCase("<=")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.le(kolom, nilai))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.le(kolom, nilai))
                    }
                } else if (kondisi?.equalsIgnoreCase(">=")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.ge(kolom, nilai))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.ge(kolom, nilai))
                    }
                } else if (kondisi?.equalsIgnoreCase("!=")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.ne(kolom, nilai))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.ne(kolom, nilai))
                    }
                } else if (kondisi?.equalsIgnoreCase("BETWEEN")) {
                    if (andOr?.equalsIgnoreCase("DAN")) {
                        restrictions = Restrictions.and(restrictions, Restrictions.between(kolom, nilai, nilai1))
                    } else {
                        restrictions = Restrictions.or(restrictions, Restrictions.between(kolom, nilai, nilai1))
                    }
                }
                ada = true
            }
        }

        if (ada) {
            historyCustomerId = Reception.withCriteria {
                eq("staDel","0");
                eq("staSave","0");
                projections {
                    groupProperty("historyCustomerVehicle.id")
                }
                add(restrictions)
            }
        }

        restrictions = Restrictions.sqlRestriction("1=1")

        for (def it : data.values()) {
            String andOr = it[0];
            String kolom = it[1];
            String kondisi = it[2];
            def nilai = it[3];
            def nilai1 = it[4];

            if (kolom.equalsIgnoreCase("t401NoWO")) {
                continue;
            }
            if (kolom.equalsIgnoreCase("t401TbanggalWO")) {
                continue;
            }

            if (kolom?.equalsIgnoreCase("t182TglLahir")) {
                if (nilai && !nilai?.equals("")) {
                    nilai = new SimpleDateFormat("dd/MM/yyyy").parse(nilai)
                }
                if (nilai1 && !nilai1?.equals("")) {
                    nilai1 = new SimpleDateFormat("dd/MM/yyyy").parse(nilai1)
                }
            }
            if (kolom?.equalsIgnoreCase("t183TglSTNK")) {
                kolom = "historyCustomerVehicle?.t183TglSTNK"
                if (nilai && !nilai?.equals("")) {
                    nilai = new SimpleDateFormat("dd/MM/yyyy").parse(nilai)
                }
                if (nilai1 && !nilai1?.equals("")) {
                    nilai1 = new SimpleDateFormat("dd/MM/yyyy").parse(nilai1)
                }
            }

            if (kolom?.equalsIgnoreCase("t183TglDEC")) {
                kolom = "historyCustomerVehicle?.t183TglDEC"
                if (nilai && !nilai?.equals("")) {
                    nilai = new SimpleDateFormat("dd/MM/yyyy").parse(nilai)
                }
                if (nilai1 && !nilai1?.equals("")) {
                    nilai1 = new SimpleDateFormat("dd/MM/yyyy").parse(nilai1)
                }
            }

            if (kolom?.equalsIgnoreCase("fullNoPol")) {
                kolom = "historyCustomerVehicle.fullNoPol"
            }

            if (kolom?.equalsIgnoreCase("modelName")) {
                kolom = "historyCustomerVehicle.fullModelCode.t110FullModelCode" //fullModelCode.modelName.id
                if (nilai && !nilai?.equals("")) {
                    nilai = Long.parseLong(nilai)
                }
            }

            if (kolom?.equalsIgnoreCase("t182NoHp")) {
                kolom = "historyCustomer.t182NoHp"
                if (nilai && !nilai?.equals("")) {
                    nilai = Long.parseLong(nilai)
                }
            }

            if (kolom?.equalsIgnoreCase("company")) {
                kolom = "company.id"
                if (nilai && !nilai?.equals("")) {
                    nilai = Long.parseLong(nilai)
                }
            }

            if (kolom?.equalsIgnoreCase("nikah")) {
                kolom = "nikah.id"
                if (nilai && !nilai?.equals("")) {
                    nilai = Long.parseLong(nilai)
                }
            }

            if (kolom?.equalsIgnoreCase("t103VinCode")) {
                kolom = "customerVehicle.t103VinCode"
            }

            if (kondisi?.equalsIgnoreCase("LIKE")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.ilike(kolom, nilai?.toString(), MatchMode.ANYWHERE))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.ilike(kolom, nilai?.toString(), MatchMode.ANYWHERE))
                }
            } else if (kondisi?.equalsIgnoreCase("=")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.eq(kolom, nilai))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.eq(kolom, nilai))
                }
            } else if (kondisi?.equalsIgnoreCase("<")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.lt(kolom, nilai))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.lt(kolom, nilai))
                }
            } else if (kondisi?.equalsIgnoreCase(">")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.gt(kolom, nilai))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.gt(kolom, nilai))
                }
            } else if (kondisi?.equalsIgnoreCase("<=")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.le(kolom, nilai))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.le(kolom, nilai))
                }
            } else if (kondisi?.equalsIgnoreCase(">=")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.ge(kolom, nilai))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.ge(kolom, nilai))
                }
            } else if (kondisi?.equalsIgnoreCase("!=")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.ne(kolom, nilai))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.ne(kolom, nilai))
                }
            } else if (kondisi?.equalsIgnoreCase("BETWEEN")) {
                if (andOr?.equalsIgnoreCase("DAN")) {
                    restrictions = Restrictions.and(restrictions, Restrictions.between(kolom, nilai, nilai1))
                } else {
                    restrictions = Restrictions.or(restrictions, Restrictions.between(kolom, nilai, nilai1))
                }
            }
        }

        def c = HistoryCustomer.createCriteria()

        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            createAlias("company", "company", CriteriaSpecification.LEFT_JOIN)
            createAlias("customer", "customer", CriteriaSpecification.LEFT_JOIN)
            createAlias("customer.mappingCustVehicles", "mappingCustVehicles", CriteriaSpecification.LEFT_JOIN)
//            createAlias("mappingCustVehicles.customerVehicle", "customerVehicle", CriteriaSpecification.LEFT_JOIN)
//            createAlias("customerVehicle.histories", "historyCustomerVehicle", CriteriaSpecification.LEFT_JOIN)
//            createAlias("historyCustomerVehicle.fullModelCode.t110FullModelCode", "fullModelCode", CriteriaSpecification.LEFT_JOIN)

            add(restrictions)

            if (ada) {
                historyCustomerId.size() > 0 ? inList("historyCustomerVehicle?.id", historyCustomerId) : sqlRestriction("1!=1")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            HistoryCustomer hc -> //mengubah it menjadi hc

            def customerDlmMapping = MappingCustVehicle.findByCustomer(hc.customer)//dapetin data customer di MapingCustVehicle
            def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(customerDlmMapping?.customerVehicle,0)

            def mapCustVhcl = hc.customer?.mappingCustVehicles;

            def tglSTNK = ""
            if(mapCustVhcl) {
                for (MappingCustVehicle mcv : mapCustVhcl) {
                    CustomerVehicle cv = mcv.customerVehicle
                    tglSTNK = cv?.currentCondition?.t183TglSTNK?.format("dd-MM-yyyy")
                }
            }

            rows << [

                    id: hc?.id,

                    t182NamaDepan: hc?.t182NamaDepan?.toString(),

                    t182NamaBelakang: hc?.t182NamaBelakang?.toString(),

                    t182TglLahir: hc?.t182TglLahir?.format("dd-MM-yyyy"),

                    t183TglSTNK: tglSTNK,

                    fullModelCode: historyCustomerVehicle?.fullModelCode?.t110FullModelCode,

                    hobby: hc?.hobbies?.collect { it?.m063NamaHobby + ", " },

                    t182NoHp: hc?.t182NoHp?.toString(),

                    nikah: hc?.nikah?.m062StaNikah?.toString(),

                    company: hc?.company?.toString()

            ]
        }

        session.smsRetention = rows;

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def doSave() {

        def customers = []
        customers = params?.customer && params?.customer?.toString()?.contains("[") ? params?.customer : (!params?.customer ? [] : [params?.customer])
        String typeSave = params.typeSave
        if (!customers || customers.size() == 0) {
            def result = [status: "Error", typeSave: typeSave, error: "Minimal salah satu customer harus dipilih"]
            render result as JSON
            return
        }

        Retention retention = Retention.get(params.retention)
        if (!retention) {
            def result = [status: "Error", typeSave: typeSave, error: "Pilih salah satu Jenis SMS Retention"]
            render result as JSON
            return
        }

        if (typeSave?.equalsIgnoreCase("Send SMS")) {
            Date today = new Date()
//            def message = []
            def message = params.formatSMS.replaceAll("<WorkshopTAM>", session.userCompanyDealer?.m011NamaWorkshop)//.replaceAll("<NamaCustomer>", hc.t182NamaDepan + " " + t182NamaBelakang)
            def hari = "", sta = ""
            def result
            switch(today.day){
                case 0:
                    hari = "Minggu"
                    break
                case 1:
                    hari = "Senin"
                    break
                case 2:
                    hari = "Selasa"
                    break
                case 3:
                    hari = "Rabu"
                    break
                case 4:
                    hari = "Kamis"
                    break
                case 5:
                    hari = "Jumat"
                    break
                case 6:
                    hari = "Sabtu"
                    break
                default:
                    hari = ""
                    break
            }
            message = message.replaceAll("<NamaHariIni>", hari)
            String dateFormat = appSettingParamDateFormat.value? appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

            message = message.replaceAll("<TanggalHariIni>", today.format(dateFormat))
            Customer cst
            customers.each{
                def dataTemp = message
                HistoryCustomer hc = HistoryCustomer.get(it.toLong())

                def dest = hc?.t182NoHp
                if(hc?.t182TglLahir){
                dataTemp = dataTemp.replaceAll("<NamaCustomer>", hc?.fullNama)
                dataTemp = dataTemp.replaceAll("<UsiaCustomer>", new Integer(today.year - hc?.t182TglLahir?.year).toString())

                    if(dest){
                        smsService.queueSms(dest, dataTemp)
                    }
                    sta += "SIP"
                }else{
                    sta += "NOK"
                }
            }
            result = [typeSave: typeSave, status: sta]

            render result as JSON
        } else {
            def rows = session.smsRetention;
            if (rows) {
                def list = []
                rows.each {
                    def id = it.id?.toString()
                    if (customers.contains(id)) {
                        list.add([it?.t182NamaDepan,
                                it?.t182NamaBelakang,
                                it?.t182TglLahir,
                                it?.t183TglSTNK,
                                it?.hobby,
                                it?.t182NoHp,
                                it?.nikah,
                                it?.company
                        ])
                    }

                }
                File file = laporanService.createReportTanpaUrutan(["Nama Depan", "Nama Belakang", "Tgl. Lahir", "Tgl. STNK", "Hobby", "Nomor Hanphone", "Nikah", "Perusahaan"], list)
                def result = [typeSave: typeSave, status: "SIP", file: file?.absolutePath?.encodeAsURL()]
                render result as JSON
            }
        }
    }

    def downloadFile() {
        File file = new File(params.file)
        response.setHeader("Content-Type", "application/xls")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
