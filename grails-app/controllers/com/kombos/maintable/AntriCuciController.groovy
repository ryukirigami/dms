package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.production.FinalInspection
import com.kombos.reception.CustomerIn
import com.kombos.reception.Reception
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class AntriCuciController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        String statusCuci = params.statusCuci
        String statusProduction = params.statusProduction
        String statusKategori = params.statusKategori
        String kataKunci = params.kataKunci
        Date tanggalWOMulai = params.tanggalWOMulai
        Date tanggalWOSelesai = params.tanggalWOSelesai

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = AntriCuci.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)
            ge("t600Tanggal",df.parse(currentDate))
            lt("t600Tanggal",df.parse(currentDate) + 1)
            if (statusKategori && statusKategori.equalsIgnoreCase("0")) {
                reception {
                    ilike("t401NoWO", "%" + kataKunci + "%")
                }
            } else if (statusKategori && statusKategori.equalsIgnoreCase("1")) {
                reception {
                    historyCustomerVehicle {
                        ilike("fullNoPol", "%" + kataKunci + "%")
                    }
                }
            }

            if (tanggalWOMulai || tanggalWOSelesai) {
                reception {
                    if (tanggalWOMulai) {
                        ge("t401TglJamCetakWO", tanggalWOMulai)
                    }
                    if (tanggalWOSelesai) {
                        le("t401TglJamCetakWO", tanggalWOSelesai)
                    }
                }
            }

            if (statusCuci && !statusCuci.equals("0")) {
                if (statusCuci.equals("1")) {
                    isNull("actualCuci")
                } else if (statusCuci.equals("2")) {
                    actualCuci {
                        eq("flagClockOff", false)
                        statusActual {
                            eq("m452StatusActual", 'Clock On')
                        }
                    }
                } else if (statusCuci.equals("3")) {
                    actualCuci {
                        eq("flagClockOff", true)
                        statusActual {
                            eq("m452StatusActual", 'Clock Off')
                        }
                    }
                }
            }


            switch (sortProperty) {
                default:
                    //order("t600NomorAntrian", "asc")
                    break;
            }
        }

        results = results.sort{a,b -> a.equals(b)?0: a.customField<b.customField? -1:1}

        def rows = []

        def clockOn = StatusActual.findByM452StatusActual('Clock On')
        def clockOff = StatusActual.findByM452StatusActual('Clock Off')

        results.each {

            def reception = it.reception
            def history = reception?.historyCustomerVehicle

            def start = ActualCuci.findByAntriCuciIDAndStatusActual(it, clockOn)
            def end = ActualCuci.findByAntriCuciIDAndStatusActual(it, clockOff)

            def jpb = JPB.findByReception(reception)

            def finalInspection = FinalInspection.findByReception(reception)

            def customerIn = CustomerIn.findByReception(reception)

            rows << [

                    id: it.id,

                    no: it?.t600NomorAntrian,

                    noWo: reception?.t401NoWO,

                    nopol: history?.fullNoPol,

                    tanggalWo: reception?.t401TglJamCetakWO ? reception?.t401TglJamCetakWO?.format("dd/MM/yyyy HH:mm:ss") : "",

                    start: start?.t602TglJam?.format("dd/MM/yyyy HH:mm:ss"),

                    end: end?.t602TglJam?.format("dd/MM/yyyy HH:mm:ss"),

                    waktuSerah: reception?.t401TglJamPenyerahan?.format("dd/MM/yyyy HH:mm:ss"),

                    waktuProduction: jpb?.t451TglJamPlan?.format("dd/MM/yyyy HH:mm:ss"),

                    finalInspection: finalInspection?.t508TglJamSelesai?.format("dd/MM/yyyy HH:mm:ss"),

                    wktDtg: customerIn?.t400TglCetakNoAntrian?.format("dd/MM/yyyy HH:mm:ss"),

                    keterangan: it?.t600alasanUbahUrutan,

                    SA: reception?.t401NamaSA


            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {

        [antriCuciInstance: new AntriCuci(params)]
    }

    def save() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = AntriCuci.createCriteria()
        def results = c.list() {
            ge("t600Tanggal",df.parse(currentDate))
            lt("t600Tanggal",df.parse(currentDate) + 1)
        }
        params?.dateCreated  = datatablesUtilService?.syncTime()
        params?.lastUpdated  = datatablesUtilService?.syncTime()
        def antriCuciInstance = new AntriCuci(params)
        antriCuciInstance.companyDealer = session?.userCompanyDealer
        antriCuciInstance.t600NomorAntrian = results.size() + 1
        //def reception = Reception.findByHistoryCustomerVehicle(HistoryCustomerVehicle.findByFullNoPol(params.noPol))
        antriCuciInstance?.reception = Reception.last()
        def reception = Reception.findById(antriCuciInstance?.reception?.id)
        reception.t401TglJamNotifikasi = datatablesUtilService?.syncTime()
        reception.save(flush: true)
        if(!antriCuciInstance?.reception){
            flash.message = "* Data Belum Lengkap"
            render(view: "create", model: [antriCuciInstance: antriCuciInstance])
            return
        }
        if (!antriCuciInstance.save(flush: true)) {
            render(view: "create", model: [antriCuciInstance: antriCuciInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), antriCuciInstance.id])
        redirect(action: "show", id: antriCuciInstance.id)
    }

    def show(Long id) {
        def antriCuciInstance = AntriCuci.get(id)
        if (!antriCuciInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), id])
            redirect(action: "list")
            return
        }

        [antriCuciInstance: antriCuciInstance]
    }

    def edit(Long id) {
        def antriCuciInstance = AntriCuci.get(id)
        if (!antriCuciInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), id])
            redirect(action: "list")
            return
        }
        def antri = params.antri


        [antriCuciInstance: antriCuciInstance, antri:antri]
    }

    def update(Long id, Long version) {
        def antriCuciInstance = AntriCuci.get(id)
        if (!antriCuciInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (antriCuciInstance.version > version) {

                antriCuciInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'antriCuci.label', default: 'AntriCuci')] as Object[],
                        "Another user has updated this AntriCuci while you were editing")
                render(view: "edit", model: [antriCuciInstance: antriCuciInstance])
                return
            }
        }
        params?.lastUpdated = datatablesUtilService?.syncTime()
        antriCuciInstance.properties = params
        if(params.antrian=='true'){
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
            def currentDate = new Date().format("dd-MM-yyyy")
            def c = AntriCuci.createCriteria()
            def results = c.list() {
                isNull("actualCuci")
                isNull("t600StaPrioritas")
                ge("t600Tanggal",df.parse(currentDate))
                lt("t600Tanggal",df.parse(currentDate) + 1)

                lt("t600NomorAntrian",antriCuciInstance.t600NomorAntrian + 1)

                order("t600NomorAntrian","asc")
            }
            def rows=[]
            results.each {
                rows<<[
                        id:it.id,
                        no:it.t600NomorAntrian
                ]
            }
            int nomor =  rows[0].no as Integer

            for(int i=0; i< rows.size();i++){
                if(i==(rows.size()-1)){
                    def antri = AntriCuci.get(rows[i].id as Long)
                    antri?.t600NomorAntrian = nomor
                    antri?.t600StaPrioritas = '1'
                    antri?.lastUpdated = datatablesUtilService?.syncTime()
                    antri.save(flush: true)
                }else{
                    def antri = AntriCuci.get(rows[i].id as Long)
                    antri?.t600NomorAntrian = rows[i+1].no as Integer
                    antri?.lastUpdated = datatablesUtilService?.syncTime()
                    antri.save(flush: true)
                }
            }
        }

        if(!antriCuciInstance?.reception){
            flash.message = "* Data Belum Lengkap"
            render(view: "edit", model: [antriCuciInstance: antriCuciInstance])
            return
        }
        antriCuciInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        antriCuciInstance?.lastUpdProcess = "UPDATE"

        if (!antriCuciInstance.save(flush: true)) {
            render(view: "edit", model: [antriCuciInstance: antriCuciInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), antriCuciInstance.id])
        redirect(action: "show", id: antriCuciInstance.id)
    }

    def delete(Long id) {
        def antriCuciInstance = AntriCuci.get(id)
        if (!antriCuciInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), id])
            redirect(action: "list")
            return
        }

        try {
            antriCuciInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'antriCuci.label', default: 'AntriCuci'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(AntriCuci, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(AntriCuci, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
    def cek(){
        def hcv = HistoryCustomerVehicle.findByFullNoPol(params.noPol)
        def rec = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(hcv,"0","0")
        if(rec){
            render rec?.t401NoWO
        }else{
            render ""
        }

    }
    def dataNopol() {
        def res = [:]
        def exist = []
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = AntriCuci.createCriteria()
        def results = c.list() {
            ge("t600Tanggal",df.parse(currentDate))
            lt("t600Tanggal",df.parse(currentDate) + 1)
        }
        results.each {
            exist << it.reception.historyCustomerVehicle.fullNoPol
        }

        def d = HistoryCustomerVehicle.createCriteria()
        def result = d.list {
            eq("staDel","0")
            exist.each {
                ne("fullNoPol",it)
            }
        }
        def opts = []
        result.each {
            opts << it.fullNoPol
        }

        res."options" = opts
        render res as JSON
    }
}
