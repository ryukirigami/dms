package com.kombos.maintable

import com.kombos.administrasi.KabKota
import com.kombos.administrasi.Kecamatan
import com.kombos.administrasi.Kelurahan
import com.kombos.administrasi.Provinsi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.customerprofile.KomposisiKendaraan
import com.kombos.customerprofile.SPK
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class CompanyController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def generateCodeService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
        Company.findAllByNamaPerusahaanLike("%HASJRAT%").each {
            println it?.namaPerusahaan
        }
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = Company.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_jenisPerusahaan"){
                jenisPerusahaan{
                    ilike("namaJenisPerusahaan","%" + (params."sCriteria_jenisPerusahaan" as String) + "%")
                }
			}
	
			if(params."sCriteria_namaPerusahaan"){
				ilike("namaPerusahaan","%" + (params."sCriteria_namaPerusahaan" as String) + "%")
			}
	
			if(params."sCriteria_alamatPerusahaan"){
				ilike("alamatPerusahaan","%" + (params."sCriteria_alamatPerusahaan" as String) + "%")
			}

			if(params."sCriteria_provinsi"){
				provinsi{
					ilike("m001NamaProvinsi","%" + (params."sCriteria_provinsi" as String) + "%" )
				}
			}

			if(params."sCriteria_kabKota"){
				kabKota{
					ilike("m002NamaKabKota","%" + (params."sCriteria_kabKota" as String) + "%" )
				}
			}

			if(params."sCriteria_kecamatan"){
				or{
					kecamatan{
						ilike("m003NamaKecamatan","%" + (params."sCriteria_kecamatan" as String) + "%")
					}
					ilike("kodePos","%" + (params."sCriteria_kecamatan" as String) + "%")
				}
			}

			if(params."sCriteria_kelurahan"){
				kelurahan{
					ilike("m004NamaKelurahan","%" + (params."sCriteria_kelurahan" as String) + "%" )
				}
			}
	
			if(params."sCriteria_telp"){
				ilike("telp","%" + (params."sCriteria_telp" as String) + "%")
			}
	
			if(params."sCriteria_fax"){
				ilike("fax","%" + (params."sCriteria_fax" as String) + "%")
			}
	
			if(params."sCriteria_namaPIC"){
				or{
					ilike("namaDepanPIC","%" + (params."sCriteria_namaPIC" as String) + "%")
					ilike("namaBelakangPIC","%" + (params."sCriteria_namaPIC" as String) + "%")
				}
			}
			
			eq("staDel","0")
			
	
			switch(sortProperty){
				case "provinsi":
					provinsi{
						order("m001NamaProvinsi",sortDir)
					}
					break
				case "kabKota":
					kabKota{
						order("m002NamaKabKota",sortDir)
					}
					break
				case "kecamatan":
					kecamatan{
						order("m003NamaKecamatan",sortDir)
					}
					break
				case "kelurahan":
					kelurahan{
						order("m004NamaKelurahan",sortDir)
					}
					break
				case "namaPIC":
					order("namaDepanPIC",sortDir)
					break
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						jenisPerusahaan: it.jenisPerusahaan?.namaJenisPerusahaan,
			
						namaPerusahaan: it.namaPerusahaan,
			
						alamatPerusahaan: it.alamatPerusahaan,
			
						provinsi: it.provinsi?.m001NamaProvinsi,
			
						kabKota: it.kabKota?.m002NamaKabKota,
			
						kecamatan: it.kecamatan?.m003NamaKecamatan ? it.kecamatan?.m003NamaKecamatan : "" + "" + (it?.kodePos ? (", " + it?.kodePos) : ""),
			
						kelurahan: it.kelurahan?.m004NamaKelurahan,
			
						telp: it.telp,
			
						fax: it.fax,
			
						namaPIC: it.namaDepanPIC ? it.namaDepanPIC : "" + " " + it.namaBelakangPIC ? it.namaBelakangPIC : "",
			
						
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
        def company = new Company(params)
        company.setKodePerusahaan(generateCodeService.codeGenerateSequence("T101_ID",null))

		[companyInstance: company]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def companyInstance = new Company(params)

        companyInstance.setCompanyDealer(session.userCompanyDealer)

		if (!companyInstance.save(flush: true)) {
			render(view: "create", model: [companyInstance: companyInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'company.label', default: 'Company'), companyInstance.id])
		redirect(action: "edit", id: companyInstance.id)
	}

	def show(Long id) {
		def companyInstance = Company.get(id)
		if (!companyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'company.label', default: 'Company'), id])
			redirect(action: "list")
			return
		}

		[companyInstance: companyInstance]
	}

	def edit(Long id) {
		def companyInstance = Company.get(id)
		if (!companyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'company.label', default: 'Company'), id])
			redirect(action: "list")
			return
		}

		[companyInstance: companyInstance]
	}

	def update(Long id, Long version) {
		params?.lastUpdated = datatablesUtilService?.syncTime()
        def companyInstance = Company.get(id)
		if (!companyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'company.label', default: 'Company'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (companyInstance.version > version) {
				
				companyInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'company.label', default: 'Company')] as Object[],
				"Another user has updated this Company while you were editing")
				render(view: "edit", model: [companyInstance: companyInstance])
				return
			}
		}

		companyInstance.properties = params

		if (!companyInstance.save(flush: true)) {
			render(view: "edit", model: [companyInstance: companyInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'company.label', default: 'Company'), companyInstance.id])
		redirect(action: "edit", id: companyInstance.id)
	}

	def delete(Long id) {
		def companyInstance = Company.get(id)
		if (!companyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'company.label', default: 'Company'), id])
			redirect(action: "list")
			return
		}

		try {
			companyInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'company.label', default: 'Company'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'company.label', default: 'Company'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(Company, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Company, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	def kkstDatatablesList() {
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def companyInstance = Company.get(params.companyId)
		
		def c = KomposisiKendaraan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			if(companyInstance){
				eq("company", companyInstance)
			}
			switch(sortProperty){
				case "merk":
					merk{
						order("m064NamaMerk",sortDir)
					}
					break;
				case "model":
					model{
						order("m065NamaModel",sortDir)
					}
					break;
				case "tahun":
						order("t105Tahun",sortDir)
					break;
				case "jumlah":
						order("t105Jumlah",sortDir)
					break;
				
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
				merk: it.merk?.m064NamaMerk,
				model: it.model?.m065NamaModel,
				tahun: it.t105Tahun,
				jumlah: it.t105Jumlah			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}
	
	def companyVehicleDatatablesList() {
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		def companyInstance = Company.get(params.companyId)
		
        def historyCustomer = HistoryCustomer.findAllByCompany(companyInstance)
        def customerList = new ArrayList()
        historyCustomer.each {
            customerList.add(it.customer)
        }

        def mappingCustomerVehicle = MappingCustVehicle.findAllByCustomerInList(customerList)
        def customerVehicleList = new ArrayList()
        def mapData = [:]
        mappingCustomerVehicle.each {
            customerVehicleList.add(it.customerVehicle)
            mapData.put(it.customerVehicle, it.customer)
        }

        def historyCustomerVehicle = HistoryCustomerVehicle.findAllByCustomerVehicleInList(customerVehicleList)
        def results = historyCustomerVehicle

		def rows = []
		
		results.each {
			rows << [
				noPol: it.kodeKotaNoPol?.m116ID + ' ' + it.t183NoPolTengah + ' ' + it.t183NoPolBelakang,
				model: it.fullModelCode?.modelName?.m104NamaModelName,
				namaCustomer: it.t183NamaSTNK,
				vinCode: it.customerVehicle?.t103VinCode
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
		
		render ret as JSON
	}
	
	def spkDatatablesList() {
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		def companyInstance = Company.get(params.companyId)
		
		def c = SPK.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			if(companyInstance){
				eq("company", companyInstance)
			}
			switch(sortProperty){
				case "namaPerusahaan":
					company{
						order("namaPerusahaan",sortDir)
					}
					break;
				case "noSPK":
					order("t191NoSPK",sortDir)
					break;
				case "tanggalSPK":
					order("t191TanggalSPK",sortDir)
					break;
				case "tanggalAwal":
					order("t191TglAwal",sortDir)
					break;
				case "tanggalAkhir":
					order("t191TglAkhir",sortDir)
					break;
				case "jumlahTotal":
					order("t191JmlSPK",sortDir)
					break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
				namaPerusahaan: it.company?.namaPerusahaan,
                jenisPerusahaan: it.company?.jenisPerusahaan?.namaJenisPerusahaan,
				noSPK: it.t191NoSPK,
				tanggalSPK: it.t191TanggalSPK,
				tanggalAwal: it.t191TglAwal,
				tanggalAkhir: it.t191TglAkhir,
				jumlahTotal: it.t191JmlSPK
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

    def getProvinsiList(){
        def res = [:]

        def result = Provinsi.findAllByStaDelAndM001NamaProvinsiIlike('0','%'+params.query+'%')
        def opts = []
        result.each { Provinsi p ->
            def prov = [:]
            prov."id" = p.id
            prov."nama" = p.m001NamaProvinsi
            opts << prov
        }

        res."options" = opts
        render res as JSON
    }

    def getKabKotaList(){
        def res = [:]

        def provinsi = Provinsi.get(params.provinsi)
        def opts = []
        if(provinsi) {
            def result = KabKota.findAllByStaDelAndM002NamaKabKotaIlikeAndProvinsi('0','%'+params.query+'%', provinsi)

            result.each { KabKota kk ->
                def kabKota = [:]
                kabKota."id" = kk.id
                kabKota."nama" = kk.m002NamaKabKota
                opts << kabKota
            }
        }

        res."options" = opts
        render res as JSON
    }

    def getKecamatanList(){
        def res = [:]

        def kabKota = KabKota.get(params.kabKota)
        def opts = []
        if(kabKota) {
            def result = Kecamatan.findAllByStaDelAndM003NamaKecamatanIlikeAndKabKota('0','%'+params.query+'%', kabKota)

            result.each { Kecamatan k ->
                def kecamatan = [:]
                kecamatan."id" = k.id
                kecamatan."nama" = k.m003NamaKecamatan
                opts << kecamatan
            }
        }

        res."options" = opts
        render res as JSON
    }

    def getKelurahanList(){
        def res = [:]

        def kecamatan = Kecamatan.get(params.kecamatan)
        def opts = []
        if(kecamatan) {
            def result = Kelurahan.findAllByStaDelAndM004NamaKelurahanIlikeAndKecamatan('0','%'+params.query+'%', kecamatan)

            result.each { Kelurahan k ->
                def kelurahan = [:]
                kelurahan."id" = k.id
                kelurahan."nama" = k.m004NamaKelurahan
                opts << kelurahan
            }
        }

        res."options" = opts
        render res as JSON
    }

    def insertNamaJabatanPIC(){
        def namaJabatanPIC = params.namaJabatanPIC

        def namaJabatanPICInstance = new NamaJabatanPIC()
        def check = NamaJabatanPIC.findByM093NamaJabatanAndStaDel("%"+namaJabatanPIC+"%","0")
        def res = [:]

        if(check){
            println "ERROR"
            res.error = "duplicate"
            render res as JSON
        }else{
            namaJabatanPICInstance?.m093ID = Integer.parseInt(generateCodeService.codeGenerateSequence("M093_ID",null))
            namaJabatanPICInstance?.m093NamaJabatan = namaJabatanPIC
            namaJabatanPICInstance?.dateCreated = datatablesUtilService?.syncTime()
            namaJabatanPICInstance?.lastUpdated = datatablesUtilService?.syncTime()

            if(namaJabatanPICInstance.hasErrors() || !namaJabatanPICInstance.save(flush: true)){
                res.error = "gagal simpan"
                render res as JSON
            }else{
                def namaJabatanPICData = NamaJabatanPIC.last()
                res.put("id", namaJabatanPICData.id)
                res.put("namaJabatan", namaJabatanPICData.m093NamaJabatan)
                render res as JSON
            }
        }
    }

    def getKodePos(){
        def hasil = ""
        def kelurahan = Kelurahan.createCriteria().list {
            eq("staDel","0")
            eq("m004NamaKelurahan",params?.kelurahan?.trim(),[ignoreCase:true])
            kecamatan{
                eq("m003NamaKecamatan",params?.kecamatan?.trim(),[ignoreCase:true])
            }
            kabkota{
                eq("m002NamaKabKota",params?.kabKota?.trim(),[ignoreCase:true])
            }
        }
        if(kelurahan?.size()>0){
            hasil = kelurahan?.last()?.m004KodePos
        }
        render hasil
    }
}
