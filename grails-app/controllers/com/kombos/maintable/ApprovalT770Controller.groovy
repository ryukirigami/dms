package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.BillToReception
import com.kombos.reception.EditJobPartsService
import com.kombos.reception.Reception
import grails.converters.JSON

class ApprovalT770Controller {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def approvalT770Service

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
        [kegiatan : params?.jenis]
	}

	def detailApproval() {
        def approval = ApprovalT770.get(params?.idApproval?.toLong())
        def receptionInstance = null
        def billToInstance = null
        String bilDisc = "Bill To";
        if(approval){
            def paramApp = approval?.t770FK?.split("#");
            receptionInstance = Reception.get(paramApp[0].toLong())
            billToInstance = BillToReception.get(paramApp[1].toLong())
            if(paramApp?.size()>2){
                bilDisc = paramApp[2] == 'd' ? "Discount" : "Bill To"
            }
        }
        [receptionInstance : receptionInstance,staBD : bilDisc,billToInstance : billToInstance]
    }

    def datatablesGoodsList() {
        def servis = new EditJobPartsService()
        params.dari = "APPROVAL"
        render servis.datatablesGoodsList(params,session) as JSON
    }

    def datatablesJobsList() {
        def servis = new EditJobPartsService()
        params.companyDealer = session?.userCompanyDealer
        params.dari = "APPROVAL"
        render servis.datatablesJobsList(params,session) as JSON
    }
    def datatablesList() {
        params.user = org.apache.shiro.SecurityUtils.subject.principal
        params.companyDealer = session?.userCompanyDealer
		render approvalT770Service.datatablesList(params) as JSON
	}

	def historyDatatablesList() {
        params.companyDealer = session?.userCompanyDealer
		render approvalT770Service.historyDatatablesList(params) as JSON
	}

	def create() {
		def result = approvalT770Service.create(params)

        if(!result.error)
            return [approvalT770Instance: result.approvalT770Instance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.companyDealer = session?.userCompanyDealer
		def result = approvalT770Service.save(params)
        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["ApprovalT770", result.approvalT770Instance.id])
            redirect(action:'show', id: result.approvalT770Instance.id)
            return
        }

        render(view:'create', model:[approvalT770Instance: result.approvalT770Instance])
	}

	def show(Long id) {
		def result = approvalT770Service.show(params)

		if(!result.error)
			return [ approvalT770Instance: result.approvalT770Instance, totalLevel:  result.approvalT770Instance.kegiatanApproval.namaApprovals?.size(), currentApprover: result.currentApprover, user:org.apache.shiro.SecurityUtils.subject.principal]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}
	
	def showHistory(Long id) {
		def result = approvalT770Service.showHistory(params)

		render result as JSON
	}

	def edit(Long id) {
		def result = approvalT770Service.show(params)

		if(!result.error)
			return [ approvalT770Instance: result.approvalT770Instance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = approvalT770Service.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["ApprovalT770", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[approvalT770Instance: result.approvalT770Instance.attach()])
	}
   	def delete() {
		def result = approvalT770Service.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["ApprovalT770", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(ApprovalT770, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	def respondApproval() {
		def res = [:]
		try {
			res = approvalT770Service.respondApproval(params)
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
}
