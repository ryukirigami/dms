package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class NoPajakController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def noPajakService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        render noPajakService.datatablesList(params) as JSON
    }

    def create() {
        def result = noPajakService.create(params)
        if (!result.error)
            return [noPajakInstance: result.noPajakInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = noPajakService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["NoPajak", result.noPajakInstance.id])
            redirect(action: 'show', id: result.noPajakInstance.id)
            return
        }

        render(view: 'create', model: [noPajakInstance: result.noPajakInstance])
    }

    def show(Long id) {
        def result = noPajakService.show(params)

        if (!result.error)
            return [noPajakInstance: result.noPajakInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = noPajakService.show(params)

        if (!result.error)
            return [noPajakInstance: result.noPajakInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = noPajakService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["NoPajak", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [noPajakInstance: result.noPajakInstance.attach()])
    }

    def delete() {
        def result = noPajakService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["NoPajak", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(NoPajak, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
