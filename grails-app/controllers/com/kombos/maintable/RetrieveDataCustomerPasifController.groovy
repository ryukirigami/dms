package com.kombos.maintable

import com.kombos.administrasi.GeneralParameter
import com.kombos.reception.Reception
import grails.converters.JSON

class RetrieveDataCustomerPasifController {

    def invitationService
    def sessionFactory

    def index() {

    }

    def doSave() {

        GeneralParameter generalParameter = invitationService.currentGeneralParameter(session?.userCompanyDealer)


        Date start = params.start
        Date end = params.end
        if (start && end) {
            def query = sessionFactory.currentSession.createSQLQuery("select max(id) from T401_RECEPTION where T401_TanggalWO >= :start and T401_TanggalWO <= :end and T401_STADEL = '0' and t401_STASAVE = '0' group by T401_T183_ID");
            query.setDate("start", start);
            query.setDate("end", end);
            def ids = query.list();
            def history = []
            if (!ids.empty) {
                def idsLong = []
                ids.each {
                    idsLong << it?.toString()?.toLong()
                }
                history = Reception.findAllByT401TanggalWOGreaterThanEqualsAndT401TanggalWOLessThanEqualsAndIdInListAndStaSaveAndStaDelAndFlagInvitationIsNull(start, end, idsLong,"0","0")
                def cp = JenisReminder.findByM201NamaJenisReminder("CP")
                def aktif = StatusReminder.findByM202NamaStatusReminder("ACCURATE")
                history.each { reception ->
                    if (generalParameter?.m000StaAktifEmail?.trim()?.equalsIgnoreCase("1")) {
                        invitationService.editOrAddReminder(
                                JenisInvitation.findByM204JenisInvitation("Email"),
                                reception,
                                cp,
                                aktif,
                                generalParameter
                        )
                    }
                    if (generalParameter?.m000StaAktifSMS?.trim()?.equalsIgnoreCase("1")) {
                        invitationService.editOrAddReminder(
                                JenisInvitation.findByM204JenisInvitation("SMS"),
                                reception,
                                cp,
                                aktif,
                                generalParameter
                        )
                    }
                    if (generalParameter?.m000StaAktifDM?.trim()?.equalsIgnoreCase("1")) {
                        invitationService.editOrAddReminder(
                                JenisInvitation.findByM204JenisInvitation("Mail"),
                                reception,
                                cp,
                                aktif,
                                generalParameter
                        )
                    }
                    if (generalParameter?.m000StaAktifCall?.trim()?.equalsIgnoreCase("1")) {
                        invitationService.editOrAddReminder(
                                JenisInvitation.findByM204JenisInvitation("Phone Call"),
                                reception,
                                cp,
                                aktif,
                                generalParameter
                        )
                    }
                }

            }
            String status = history.size() == 0 ? "Data record tidak ditemukan" : history.size() + " record berhasil ditemukan"

            def result = [status: "OK", hasil: status]
            render result as JSON
        } else {
            def result = [status: "FAIL", error: "Tanggal Terakhir Service harus diisi"]
            render result as JSON
        }
    }
}
