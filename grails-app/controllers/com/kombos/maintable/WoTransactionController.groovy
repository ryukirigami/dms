package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat

class WoTransactionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def jasperService

    def datatablesUtilService

    def sessionFactory;

    def woTransactionService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {

    }

    def uploadData() {
        def jsonData = []
        [tanggal : new Date(),jsonData:jsonData as JSON]
    }
    def viewData() {
        def htmlData = ""
        def style = ""
        def nomor = 0
        def jsonData = []
        def wotrx = WoTransaction.findAllByDocNumber(params.doc)
        wotrx.each {
            nomor++
            htmlData+="<tr "+style+">\n" +
                    "                            <td>\n" +
                    "                                "+nomor+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.dealerCode+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.areaCode+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.outletCode+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.noWo+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.tglWo+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.noPol+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.noVehicle+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.typeVehicle+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.isEm+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.woType+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.operation+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.operationType+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.flatRate+"\n" +
                    "                            </td>\n" +
                    "                            <td>\n" +
                    "                                "+it.warranty+"\n" +
                    "                            </td>\n" +
                    "                        </tr>"
            jsonData << [
                    dealerCode : it.dealerCode,
                    areaCode : it.areaCode,
                    outletCode : it.outletCode,
                    noWo : it.noWo,
                    tglWo : it.tglWo,
                    noPol : it.noPol,
                    noVehicle : it.noVehicle,
                    typeVehicle : it.typeVehicle,
                    isEm : it.isEm,
                    woType : it.woType,
                    operation : it.operation,
                    damageLevel : it.damageLevel,
                    operationType : it.operationType,
                    flatRate : it.flatRate,
                    warranty : it.warranty,
            ]
        }
        session["woTrx"] = jsonData
        [doc : params.doc, htmlData: htmlData]
    }

    def generateData() {
        def jsonData = []
        [tanggal : new Date(),jsonData:jsonData as JSON,jumJson:0]
    }

    def datatablesList() {
        session.exportParams = params
        params.userCompanyDealer = session.userCompanyDealer
        render woTransactionService.datatablesList(params) as JSON
    }
    def viewGenerate(){
        session["woTrx"] = "";
        def tanggal = Date.parse("dd/MM/yyyy",params.tanggal)
        def cek = WoTransaction.createCriteria()
        def woTrx = cek.list() {
            eq("companyDealer",session.userCompanyDealer)
            ge("tglUpload",tanggal)
            lt("tglUpload",tanggal  + 1)
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
            }
        }
        def tanggal2 = tanggal
        String tgl1 = tanggal.format("dd-MM-yyyy")
        String tgl2 = (tanggal2 + 1).format("dd-MM-yyyy")
        def jmlhDataError = 0
        def htmlData = ""
        def jsonData = []
        final sessions = sessionFactory.currentSession
        String query="SELECT 'HA','0',OUTLET_CODE,NO_WO, TGL_WO, NOPOL,VINCODE, VEHICLE_TYPE, Is_EM, WO_TYPE,OPERATION_DESC,OPR_TYPE, RATE, TWC "+
                " FROM "+
                "(SELECT H.M011OUTLET_CODE OUTLET_CODE, A.T401_NOWO NO_WO, TO_CHAR(A.T401_TANGGALWO,'YYYY/MM/DD') AS TGL_WO,"+
                " C.M116_ID|| B.T183_NOPOLTENGAH|| B.T183_NOPOLBELAKANG AS NOPOL, "+
                " D.T103_VINCODE VINCODE, M102_BASEMODEL.M102_NAMABASEMODEL AS VEHICLE_TYPE, "+
                " DECODE(SUBSTR(F.M053_NAMAOPERATION,1,2),'EM',1,0) AS Is_EM, "+
                " DECODE(A.T401_STATWC,1,'SP', DECODE(SUBSTR(G.T400_NOMORANTRIAN,1,1),'W','WALK IN','BOOKING')) AS WO_TYPE, "+
                " F.M053_NAMAOPERATION OPERATION_DESC,DECODE(SUBSTR(F.M053_KET,1,3),'SBI','SBI','SBE','SBE','SPO','SPOORING', 'PDS','PDS', 'PDI','PDI','GR') OPR_TYPE,"+
                " E.T402_RATE RATE, "+
                " DECODE(F.M053_M055_ID,4085,'1',4086,'1','0') TWC "+
                "FROM T401_RECEPTION A "+
                "LEFT JOIN T183_HISTORYCUSTOMERVEHICLE B ON A.T401_T183_ID = B.ID "+
                "INNER JOIN M116_KODEKOTANOPOL C ON B.T183_M116_ID = C.ID "+
                "INNER JOIN T103_CUSTOMERVEHICLE D ON B.T183_T103_VINCODE = D.ID  "+
                "LEFT JOIN T110_FULLMODELCODE ON B.T183_T110_FULLMODELCODE = T110_FULLMODELCODE.ID "+
                "LEFT JOIN M102_BASEMODEL ON T110_FULLMODELCODE.T110_M102_ID = M102_BASEMODEL.ID "+
                "LEFT JOIN T402_JOBRCP E ON E.T402_T401_NOWO = A.ID "+
                "INNER JOIN M053_OPERATION F ON E.T402_M053_JOBID = F.ID "+
                "LEFT JOIN T400_CUSTOMERIN G ON T401_T400_ID = G.ID "+
                "LEFT JOIN M011_COMPANYTAM H ON H.ID = A.COMPANY_DEALER_ID "+
                "WHERE A.COMPANY_DEALER_ID = "+session.userCompanyDealerId+" AND A.T401_TANGGALWO BETWEEN  TO_DATE('"+tgl1+"','DD-MM-YYYY') AND TO_DATE('"+tgl2+"','DD-MM-YYYY')"+
                "AND A.T401_T183_ID = B.ID AND A.T401_M401_ID IS NULL "+
                "AND A.T401_STADEL ='0' AND E.T402_STADEL='0' AND A.T401_STARECESTSLSQUOT = 'reception' "+
                "AND A.ID = E.T402_T401_NOWO "+
                "ORDER BY A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'YYYY/MM/DD')) "+
                "GROUP BY NO_WO, TGL_WO, NOPOL,VINCODE, VEHICLE_TYPE, Is_EM, WO_TYPE,OPERATION_DESC, RATE, TWC,OPR_TYPE,OUTLET_CODE "+
                "ORDER BY NO_WO, TGL_WO ";

        def results = null
        try {
            final sqlQuery = sessions.createSQLQuery(query)
            final queryResults = sqlQuery.with {
                list()
            }
            def nomor = 0

            def style = ""
            def dealerCode = ""
            def areaCode= ""
            def outletCode= ""
            def noWo= ""
            def tglWo= ""
            def noPol= ""
            def noVehicle= ""
            def typeVehicle= ""
            def isEm= ""
            def woType= ""
            def operation= ""
            def damageLevel= ""
            def operationType= ""
            def flatRate= ""
            def warranty= ""
            results = queryResults.collect
                    { resultRow ->
                        dealerCode = resultRow[0]
                        areaCode= "0"
                        outletCode= resultRow[2]
                        noWo= resultRow[3]
                        tglWo= resultRow[4]
                        noPol= resultRow[5]
                        noVehicle= resultRow[6]
                        typeVehicle= resultRow[7]
                        isEm= resultRow[8]
                        woType= resultRow[9]
                        operation = resultRow[10]
                        operationType= resultRow[11]
                        if(isEm.toString().equalsIgnoreCase("1")){
                            operationType= "SBE"
                        }
                        flatRate= resultRow[12]
                        warranty= resultRow[13]
                        nomor++

                        htmlData+="<tr "+style+">\n" +
                                "                            <td>\n" +
                                "                                "+nomor+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+dealerCode+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+areaCode+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+outletCode+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+noWo+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+tglWo+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+noPol+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+noVehicle+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+typeVehicle+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+isEm+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+woType+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+operation+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+operationType+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+flatRate+"\n" +
                                "                            </td>\n" +
                                "                            <td>\n" +
                                "                                "+warranty+"\n" +
                                "                            </td>\n" +
                                "                        </tr>"

                        jsonData << [
                                dealerCode : dealerCode,
                                areaCode : areaCode,
                                outletCode : outletCode,
                                noWo : noWo,
                                tglWo : tglWo,
                                noPol : noPol,
                                noVehicle : noVehicle,
                                typeVehicle : typeVehicle,
                                isEm : isEm,
                                fileName : "GENERATE",
                                woType : woType,
                                operation : operation,
                                damageLevel : damageLevel,
                                operationType : operationType,
                                flatRate : flatRate,
                                warranty : warranty,
                                flag : "BLACK"
                        ]
                    }

        }catch(Exception e){

        }
        session["woTrx"] = jsonData
        def jumJson = jsonData.size()
        if (jsonData.size()==0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Tidak Ada Data")
        }else if(woTrx.size()>0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Untuk Tanggal Ini, Sudah Upload Sebanyak " + woTrx.size() + " Kali")
        }else{
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
        }
        render(view: "generateData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError,tanggal : tanggal,jumJson:jumJson])
    }
    def view() {
        def tanggal = Date.parse("dd/MM/yyyy",params.tanggalUpload_dp)
        session["woTrx"] = "";
        def cek = WoTransaction.createCriteria()
        def woTrx = cek.list() {
            eq("companyDealer",session.userCompanyDealer)
            ge("tglUpload",tanggal)
            lt("tglUpload",tanggal  + 1)
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("docNumber", "docNumber")
            }
        }
        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'dealerCode',
                        'B':'areaCode',
                        'C':'outletCode',
                        'D':'noWo',
                        'E':'tglWo',
                        'F':'noPol',
                        'G':'noVehicle',
                        'H':'typeVehicle',
                        'I':'isEm',
                        'J':'woType',
                        'K':'operation',
                        'L':'operationType',
                        'M':'flatRate',
                        'N':'warranty'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        def jsonData = []
        int jmlhDataError = 0;
        def isiEror= "";
        String htmlData = "",status2 = "", statusReplace = ""
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream','application/vnd-ms-excel'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "uploadData")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)
            String status = "0", style = "style='color:black'", flag = "black"
            int nomor = 0
            jobList?.each {
                nomor++
                style = "style='color:black'"
                flag = "black"
                try {
                    it.outletCode = it.outletCode.toInteger().toString()
                    if(it.outletCode.toInteger()<10){
                        it.outletCode = "0" + it.outletCode.toInteger().toString()
                    }
                    it.warranty = it.warranty.toInteger().toString()
                    it.isEm = it.isEm.toInteger().toString()
                }catch (Exception e){
                }

                if(it.areaCode=="" || it.areaCode=="-" || it.areaCode==null){
                    it.areaCode = "0"
                }else{
                    try {
                        it.areaCode = it.areaCode.toInteger().toString()
                    }catch (Exception e){
                    }
                }
                if(it.dealerCode==null || it.dealerCode=="" || it.outletCode==null || it.outletCode=="" ||
                it.noWo==null || it.noWo=="" ||  it.tglWo==null || it.tglWo=="" || it.noPol==null || it.noPol=="" ||
                it.noVehicle==null || it.noVehicle=="" ||  it.typeVehicle==null || it.typeVehicle=="" || it.isEm==null || it.isEm=="" ||
                it.operation==null || it.operation=="" || it.operationType==null || it.operationType=="" ||
                it.woType==null || it.woType=="" ||  it.flatRate==null || it.flatRate=="" ||  it.warranty==null || it.warranty==""
                ){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : ada yang Null / Kosong <br>"
                }
                if(it.areaCode!="0"){
                    style = "style='color:red'"
                    flag =  "red"
                    jmlhDataError++
                    isiEror += "*BARIS ke-"+nomor+" : AreaCode harus disi '0' <br>"
                }
                if(it.outletCode != session.userCompanyDealer.m011OutletCode){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : OutletCode harus diisi "+session.userCompanyDealer.m011OutletCode+" <br>"
                    jmlhDataError++
                }
                if(it.dealerCode!="HA"){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : DealerCode harus diisi HA <br>"
                    jmlhDataError++
                }
                if(it.woType.toString().toUpperCase() !="RTJ" && it.woType.toString().toUpperCase()!="BOOKING"
                        && it.woType.toString().toUpperCase() !="WALK IN" && it.woType.toString().toUpperCase() !="PDS"
                        && it.woType.toString().toUpperCase() !="PDI" && it.woType.toString().toUpperCase() !="SP"
                        && it.woType.toString().toUpperCase() !="TOYOTA MOBILE SERVICE MOBIL" && it.woType.toString().toUpperCase() !="TOYOTA MOBILE SERVICE MOTOR"){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : Wo Type hanya boleh diisi (RTJ,BOOKING,WALK IN, PDS, PDI, SP,TOYOTA MOBILE SERVICE MOBIL, dan TOYOTA MOBILE SERVICE MOTOR) <br>"
                    jmlhDataError++
                }
                if(it.operationType.toString().toUpperCase() !="SBI" && it.operationType.toString().toUpperCase()!="SBE"
                        && it.operationType.toString().toUpperCase()!="GR" && it.operationType.toString().toUpperCase()!="PDS"){
                    style = "style='color:red'"
                    flag =  "red"
                    jmlhDataError++
                    isiEror += "*BARIS ke-"+nomor+" : Operation Type hanya boleh diisi (SBI,SBE,GR,PDS)<br>"
                }
                if(it.noPol.toString().contains(" ")){
                    style = "style='color:red'"
                    flag =  "red"
                    jmlhDataError++
                    isiEror += "*BARIS ke-"+nomor+" : Nomor Polisi jangan ada spasi <br>"
                }
                if(it.warranty!="0" && it.warranty!="1"){
                    style = "style='color:red'"
                    flag =  "red"
                    jmlhDataError++
                    isiEror += "*BARIS ke-"+nomor+" : Warranty hanya boleh diisi 0 dan 1<br>"
                }
                if(it.isEm!="0" && it.isEm!="1"){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : IS EM hanya boleh diisi 0 dan 1<br>"
                    jmlhDataError++
                }

                try {
                    Date tglWO = Date.parse("yyyy-MM-dd",it.tglWo.toString())
                }catch (Exception e){
                    style = "style='color:red'"
                    flag =  "red"
                    isiEror += "*BARIS ke-"+nomor+" : Format Tanggal (31/12/2016)<br>"
                    jmlhDataError++
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+nomor+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.dealerCode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.areaCode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.outletCode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.noWo+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.tglWo+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.noPol+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.noVehicle+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.typeVehicle+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.isEm+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.woType+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.operation+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.operationType+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.flatRate+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.warranty+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
                jsonData << [
                        dealerCode : it.dealerCode,
                        areaCode : it.areaCode,
                        outletCode : it.outletCode,
                        noWo : it.noWo,
                        tglWo : it.tglWo,
                        noPol : it.noPol,
                        noVehicle : it.noVehicle,
                        typeVehicle : it.typeVehicle,
                        isEm : it.isEm,
                        fileName : uploadExcel.getOriginalFilename(),
                        woType : it.woType,
                        operation : it.operation,
                        damageLevel : it.damageLevel,
                        operationType : it.operationType,
                        flatRate : it.flatRate,
                        warranty : it.warranty,
                        flag : flag
                ]

            }
        }
        if (jsonData.size()==0){
            flash.message = message(code: 'default.uploadGoods.message', default: "File Tidak dapat diproses, Format File *.xls dan Nama SHEET harus 'Sheet1'")
        }else if(woTrx.size()>0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Untuk Tanggal Ini, Sudah Upload Sebanyak " + woTrx.size() + " Kali")
        }else if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
        }
        session["woTrx"] = jsonData
        render(view: "uploadData", model: [htmlData:htmlData, jsonData:jsonData as JSON, jmlhDataError:jmlhDataError,tanggal : params.tanggalUpload, jumJson : jsonData.size(), isiEror : isiEror])
    }
    def upload() {
        def requestBody = JSON.parse(params.sendData)
        def sukses = true
        def res = []
        def tanggal= Date.parse("dd/MM/yyyy",params.tanggal)
        if(requestBody.size()>0){
            def woTransactionInstance = null
            def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
            def sdf = new SimpleDateFormat("yyyyMMdd")
            String docNumber = "HA_"+com+".WO.GR."+sdf.format(tanggal)
            def cek = WoTransaction.createCriteria()
            def woTrx = cek.list() {
                eq("companyDealer",session.userCompanyDealer)
                ge("tglUpload",tanggal)
                lt("tglUpload",tanggal + 1)
                resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                projections {
                    groupProperty("docNumber", "docNumber")
                }
            }
            String addText = ""
            if(woTrx.size()>0){
                def cekDelete = WoTransaction.createCriteria()
                def woTrxDelete = cekDelete.list() {
                    eq("companyDealer",session.userCompanyDealer)
                    ge("tglUpload",tanggal)
                    lt("tglUpload",tanggal + 1)
                    eq("staDel",'0')
                }
                woTrxDelete.each {
                    def aD = WoTransaction.get(it.id as Long)
                    aD.staDel = '1'
                    aD.save(flush: true)
                }
                addText = "_" + woTrx.size()
            }
            requestBody.each{
                woTransactionInstance = new WoTransaction()
                String dealerCode = it?.dealerCode.toString()
                String areaCode = it.areaCode.toString()
                String outletCode = it.outletCode.toString()
                String noWo = it.noWo.toString()
                String tglWo = it.tglWo.toString()
                String noPol = it.noPol.toString()
                String noVehicle = it.noVehicle.toString()
                String typeVehicle = it.typeVehicle.toString()
                String isEm = it.isEm.toString()
                String woType = it.woType.toString()
                String operation = it.operation.toString()
                String damageLevel = it.damageLevel.toString()
                String operationType = it.operationType.toString()
                String flatRate = it.flatRate.toString()
                String warranty = it.warranty.toString()
                String fileName = it.fileName.toString()

                //save Data
                woTransactionInstance.companyDealer = session.userCompanyDealer
                woTransactionInstance.dealerCode = dealerCode
                woTransactionInstance.areaCode = "0"
                woTransactionInstance.outletCode = outletCode
                woTransactionInstance.noWo = noWo
                woTransactionInstance.tglWo = tglWo
                woTransactionInstance.noPol = noPol
                woTransactionInstance.noVehicle = noVehicle
                woTransactionInstance.typeVehicle = typeVehicle
                woTransactionInstance.isEm = isEm
                woTransactionInstance.woType = woType
                woTransactionInstance.operation = operation
                woTransactionInstance.damageLevel = damageLevel
                woTransactionInstance.operationType = operationType
                woTransactionInstance.flatRate = flatRate
                woTransactionInstance.warranty  = warranty
                woTransactionInstance.staDel = '0'
                woTransactionInstance.tglUpload = tanggal
                woTransactionInstance.fileName = fileName
                woTransactionInstance.docNumber = docNumber+""+addText
                woTransactionInstance.dateCreated  = datatablesUtilService?.syncTime()
                woTransactionInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                woTransactionInstance.lastUpdated  = datatablesUtilService?.syncTime()
                woTransactionInstance.lastUpdProcess  = 'INSERT'
                woTransactionInstance.save(flush: true)
                woTransactionInstance.errors.each {println it}
                sukses = true
            }
        }else{
            sukses = false
            flash.message = message(code: 'default.uploadGoods.message', default: "Data Kosong")
        }
        res << [
                sukses : sukses
        ]
        render res as JSON

    }

    def exportToExcel(){
        List<JasperReportDef> reportDefList = []
        params.dataExcel = session["woTrx"]
        def reportData = exportToExcelDetail(params)

        def reportDef = new JasperReportDef(name:'woTransaction.jasper',
                fileFormat:JasperExportFormat.XLS_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)


        def file = File.createTempFile("WOTRX_",".xls")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def exportToExcelDetail(def params){
        def jsonArray = params.dataExcel
        def reportData = new ArrayList();
        jsonArray.each {

            def data = [:]
            data.put("DEALER_CODE",it.dealerCode.toString())
            data.put("AREA_CODE",it.areaCode.toString())
            data.put("OUTLET_CODE",it.outletCode.toString())
            data.put("NOWO",it.noWo.toString())
            data.put("TGLWO",it.tglWo.toString())
            data.put("NOPOL",it.noPol.toString())
            data.put("NO_VEHICLE",it.noVehicle.toString())
            data.put("TIPE_VEHICLE",it.typeVehicle.toString())
            data.put("IS_EM",it.isEm.toString())
            data.put("TIPE_WO",it.woType.toString())
            data.put("OPERATION_DESC",it.operation.toString())
            data.put("DAMAGE_LEVEL",it.damageLevel.toString())
            data.put("OPERATION_TYPE",it.operationType.toString())
            data.put("FLAT_RATE",it.flatRate.toString())
            data.put("WARRANTY",it.warranty.toString())
            data.put("FLAG_COLOR",it.flag)
            reportData.add(data)
        }

        return reportData
    }

    def printWoTrx(){
        List<JasperReportDef> reportDefList = []
        def periode = "ALL"
        try {
            periode = new Date().parse("dd/MM/yyyy",params.tanggal as String).format("yyyyMMdd")
        }catch (Exception e){}
        def reportData = woTransactionService.printWoTrxDetail(params)

        def reportDef = new JasperReportDef(name:'woTransaction2.jasper',
                fileFormat:JasperExportFormat.CSV_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("HA_WO_"+periode+"_",".txt")
        file.deleteOnExit()

        def bosData = new StringBuilder();
        def dataToArray = jasperService.generateReport(reportDefList).toString().split('\n')
        int count = 0
        int sizeArr = dataToArray.size()
        dataToArray.each {
            count++
            def fixedData = it.replaceAll(",", ";")
            fixedData = fixedData.replaceAll("#", ",")
            bosData.append(fixedData)

            if(count != sizeArr){
                bosData.append(System.getProperty("line.separator"))
            }
        }

        def gaga = bosData.toString()
        FileUtils.writeStringToFile(file, gaga)
        response.setHeader("Content-Type", "text/plain")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
