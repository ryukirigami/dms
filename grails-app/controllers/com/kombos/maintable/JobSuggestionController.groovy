package com.kombos.maintable

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class JobSuggestionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jobSuggestionService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render jobSuggestionService.datatablesList(params) as JSON
    }

    def create() {
        def result = jobSuggestionService.create(params)

        if (!result.error)
            return [jobSuggestionInstance: result.jobSuggestionInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = jobSuggestionService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["JobSuggestion", result.jobSuggestionInstance.id])
            redirect(action: 'show', id: result.jobSuggestionInstance.id)
            return
        }

        render(view: 'create', model: [jobSuggestionInstance: result.jobSuggestionInstance])
    }

    def show(Long id) {
        def result = jobSuggestionService.show(params)

        if (!result.error)
            return [jobSuggestionInstance: result.jobSuggestionInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = jobSuggestionService.show(params)

        if (!result.error)
            return [jobSuggestionInstance: result.jobSuggestionInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = jobSuggestionService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["JobSuggestion", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [jobSuggestionInstance: result.jobSuggestionInstance.attach()])
    }

    def delete() {
        def result = jobSuggestionService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["JobSuggestion", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(JobSuggestion, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
