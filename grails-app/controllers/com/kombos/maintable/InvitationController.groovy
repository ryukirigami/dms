package com.kombos.maintable

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Provinsi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.customerprofile.CustomerSurveyDetail
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.production.FinalInspection
import com.kombos.reception.Reception
import grails.converters.JSON
import groovy.sql.Sql
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.Order

import java.text.DateFormat
import java.text.SimpleDateFormat

class InvitationController {

    def koneksiToOracleService
    def invitationService
    def jasperService
    def BatchProcessMenginsertDataReminderService
    def datatablesUtilService
    private java.lang.String companyDealer

    def index() {}

    def sendMail() {

        Invitation get = Invitation.get(params.invitation)

        Invitation invitation = new Invitation(get.properties)
        invitation.t202StaAction = "2"
        invitation.t202TglJamKirim = new Date()
        invitation.jenisInvitation = JenisInvitation.findByM204JenisInvitation("Email")
        invitation.dateCreated = datatablesUtilService?.syncTime()
        invitation.lastUpdated = datatablesUtilService?.syncTime()
        invitation.save(flush: true)

        Reminder reminder = new Reminder(get?.reminder?.properties)
        reminder.t201TglEmail = new Date()
        reminder.dateCreated = datatablesUtilService?.syncTime()
        reminder.lastUpdated = datatablesUtilService?.syncTime()
        reminder.save(flush: true)

        def result = [status: "OK", smsContent: invitationService.formatContentEmail(invitation)]
        render result as JSON
    }

    def sendSMS() {

        Invitation get = Invitation.get(params.invitation)

        Invitation invitation = new Invitation(get.properties)
        invitation.t202StaAction = "3"
        invitation.jenisInvitation = JenisInvitation.findByM204JenisInvitation("SMS")
        invitation.t202TglJamKirim = new Date()
        invitation.dateCreated = datatablesUtilService?.syncTime()
        invitation.lastUpdated = datatablesUtilService?.syncTime()
        invitation.save(flush: true)

        Reminder reminder = new Reminder(get?.reminder?.properties)
        reminder.t201TglSMS = new Date()
        reminder.dateCreated = datatablesUtilService?.syncTime()
        reminder.lastUpdated = datatablesUtilService?.syncTime()
        reminder.save(flush: true)
        def mapping = MappingCustVehicle.findByCustomerVehicle(invitation?.customerVehicle,[sort: "id", order: "desc"])
        def historyCustomer = HistoryCustomer.findByCustomer(mapping?.customer, [sort: "id", order: "desc"])


        def result = [status: "OK", smsContent: invitationService.formatContentSMS(invitation)]
        render result as JSON
    }

    def printMail() {

        Invitation invitation = Invitation.get(params.invitation)
        invitation.t202StaAction = "1"
        invitation.t202TglJamKirim = datatablesUtilService?.syncTime()
        invitation.dateCreated = datatablesUtilService?.syncTime()
        invitation.lastUpdated = datatablesUtilService?.syncTime()
        invitation.save(flush: true)

        String m000TextCatatanDM = invitationService.formatContentDM(invitation)

        def parameters = [logo: request.getRealPath("/reports/LOGOnew.gif")];
        parameters.isi = m000TextCatatanDM

        String tipe = params.tipe ? params.tipe : "PDF"
        def file = File.createTempFile("printMail", "." + tipe);

        def reportDef = new JasperReportDef(name: 'printMail.jasper', fileFormat: tipe.equals('PDF') ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT, parameters: parameters, reportData: new ArrayList())
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def doSave() {

        //get single data of invitation record.
        Invitation invitation = Invitation.get(params.invitation)
        def currentCondition = invitation?.customerVehicle?.currentCondition

        def customer = MappingCustVehicle.findByCustomerVehicle(invitation?.customerVehicle, [sort: "id", order: "desc"])?.customer
        def historyCustomer = HistoryCustomer.findByCustomer(customer, [sort: "id", order: "desc"])

        if (historyCustomer) {
            params.lastUpdated = datatablesUtilService?.syncTime()
            historyCustomer.properties = params
            //historyCustomer.save(flush: true)
        }

        //save appoinment date
        if (invitation) {
            invitation.properties = params
            invitation.jenisInvitation = params.chageMethod ? JenisInvitation.findByM204JenisInvitation(params.chageMethod) : null;

            String t202StaInvitation = params.t202StaInvitationxx1
            if (t202StaInvitation?.equalsIgnoreCase("1")) {
                t202StaInvitation =
                        params.t202StaInvitationxx1 +
                                params.t202StaInvitationxx2 +
                                params.t202StaInvitationxx3 +
                                params.t202StaInvitationxx4 +
                                params.t202StaInvitationxx5 +
                                params.t202StaInvitationxx6
            } else {
                t202StaInvitation =
                        params.t202StaInvitationxx1 +
                                params.t202StaInvitationxx8 +
                                params.t202StaInvitationxx9 +
                                params.t202StaInvitationxx10 +
                                params.t202StaInvitationxx11 +
                                params.t202StaInvitationxx12
            }

            invitation.lastUpdated = params.lastUpdated
            invitation.t202StaInvitation = t202StaInvitation

            //approve re-contact
            invitation.t202StaReContact = params.t202StaReContact
            invitation.t202TglNextReContact = params.t202TglNextReContact
            invitation.t202KetReContact = params.t202KetReContact

            //approve appointment
            invitation.t202StaLangsungAppointment = params.t202StaLangsungAppointment
            invitation.t202TglJamApp = params.t202TglJamApp
            invitation.t202CatAppointment = params.t202CatAppointment

            def operation = invitation?.reminder?.operation

            //Operation here

            def reminder = invitation.reminder
            def lastSdate = reminder?.t201LastServiceDate?.format("dd-MMM-yyyy")

            reminder.m202ID = StatusReminder.get(params.m202ID)
            reminder.lastUpdProcess = "update"
            reminder.lastUpdated = params.lastUpdated

            reminder.t201LastKM = params.t201LastKM?.toString()?.toDouble()?.intValue()

        }

        def result = [status: "OK"]
        render result as JSON
    }

    def vehiclesDatatablesList() {
        //Invitation invitation = Invitation.get(params.invitation)
        Date tgl1 = datatablesUtilService.syncTime()
        if(params.estJatuhTempo){
            tgl1 = new Date().parse('dd-MM-yyyy',params.estJatuhTempo)
        }

        //Set<String> nopols = new HashSet<String>()
        def searchReminder = []
        if (params."searchReminder") {
            params?."searchReminder"?.toString()?.split(",")?.each {
                searchReminder.add(it?.trim()?.toLong())
            }
        }

        def results2 = Invitation.createCriteria().list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t202StaDel", "0")
            eq("companyDealer", session.userCompanyDealer)

            jenisInvitation {
                eq("m204StaDel", "0")
                eq("m204JenisInvitation", params."method")
            }
            //put reminder
            reminder {
                eq("staDel", "0")
                m201ID {
                    inList("id", searchReminder)
                }
                if (params."estJatuhTempo") {
                    print("ADA TANGGAL method")

//                    ge("t201TglJatuhTempo", tgl1)
//                    lt("t201TglJatuhTempo", tgl1 + 1)

                } else {
                    print("TIDAK ADA TANGGAL method")
                    //set data 0
                    eq("id", -1000.toLong())
                }
            }
            addOrder(Order.desc("id"))
        }

        //Start rearrange script:)
        def c = Invitation.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            eq("t202StaDel", "0")
            eq("companyDealer", session.userCompanyDealer)

            jenisInvitation {
                eq("m204StaDel", "0")
                eq("m204JenisInvitation", params."method")
            }

            reminder {
                eq("staDel", "0")
                m201ID {
                    inList("id", searchReminder)
                }
            }

            if(params."estJatuhTempo"){
                ge("dateCreated", tgl1)
                lt("dateCreated", tgl1 + 1)
            }

            addOrder(Order.desc("id"))
        }

        def rows = []

        results.each {
            def mobil = HistoryCustomerVehicle.findByCustomerVehicleAndT183NoPolTengahIsNotNull(it?.customerVehicle)
            def current = it?.customerVehicle?.currentCondition
            def settang = it?.reminder?.t201TglJatuhTempo
                rows << [
                        id     : it.id,
                        nopol  : mobil?.fullNoPol,
                        vehicle: it?.customerVehicle?.id
                ]
        }

        def result = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render result as JSON
    }

    def retrieveInvitationHistory() {
        Invitation invitation = Invitation.get(params.invitation)

        def Mail = Invitation.withCriteria {
            projections {
                max("t202TglJamKirim")
            }
            eq("customerVehicle", invitation?.customerVehicle)
            jenisInvitation {
                eq("m204JenisInvitation", "Mail")
            }
        }

        def Email = Invitation.withCriteria {
            projections {
                max("t202TglJamKirim")
            }
            eq("customerVehicle", invitation?.customerVehicle)
            jenisInvitation {
                eq("m204JenisInvitation", "Email")
            }
        }

        def SMS = Invitation.withCriteria {
            projections {
                max("t202TglJamKirim")
            }
            eq("customerVehicle", invitation?.customerVehicle)
            jenisInvitation {
                eq("m204JenisInvitation", "SMS")
            }
        }

        def PhoneCall = Invitation.withCriteria {
            projections {
                max("t202TglJamKirim")
            }
            eq("customerVehicle", invitation?.customerVehicle)
            jenisInvitation {
                eq("m204JenisInvitation", "Phone Call")
            }
        }

        def lastDM = Mail ? Mail[0]?.format("dd/MM/yyyy HH:mm:ss") : "-"
        def lastSMS = SMS ? SMS[0]?.format("dd/MM/yyyy HH:mm:ss") : "-"
        def lastEmail = Email ? Email[0]?.format("dd/MM/yyyy HH:mm:ss") : "-"
        def lastPhoneCall = PhoneCall ? PhoneCall[0]?.format("dd/MM/yyyy HH:mm:ss") : "-"

        def result = [lastDM: lastDM, lastSMS: lastSMS, lastEmail: lastEmail, lastPhoneCall: lastPhoneCall]
        render result as JSON
    }

    def loadInvitation() {
        def invitation = Invitation.get(params.id)
        def currentCondition = invitation?.customerVehicle?.currentCondition
        String nopol = currentCondition?.kodeKotaNoPol?.m116ID + " " + currentCondition?.t183NoPolTengah + " " + currentCondition?.t183NoPolBelakang;
        String baseModel = currentCondition?.fullModelCode?.baseModel?.m102NamaBaseModel
        String vinCode = currentCondition?.customerVehicle?.t103VinCode
        def customer = MappingCustVehicle.findByCustomerVehicle(currentCondition.customerVehicle)?.customer
        def historyCustomer = customer?.histories?.size()>0 ? customer?.histories?.last() : null
        def customerSurvey = CustomerSurveyDetail.findByCustomerVehicle(currentCondition.customerVehicle, [sort: "id", order: "desc"])?.customerSurvey
        def jenisSurvey = customerSurvey?.jenisSurvey
        def reception = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(currentCondition,"0","0", [sort: "id", order: "desc"])
        def finalInspection = FinalInspection.findByReception(reception, [sort: "id", order: "desc"])
        def sPK = reception?.sPK
        def sPkAsuransi = reception?.sPkAsuransi
        def getProvinsi = historyCustomer?.provinsi!=null ? historyCustomer?.provinsi : ""
        def getKota = historyCustomer?.kabKota!=null ? historyCustomer?.kabKota : ""
        def getKec = historyCustomer?.kecamatan!=null ? historyCustomer?.kecamatan : ""
        def getKel = historyCustomer?.kelurahan!=null ? historyCustomer?.kelurahan : ""
        def getPos = historyCustomer?.kelurahan!=null ? historyCustomer?.kelurahan.m004KodePos.toString() : ""


        String t202StaInvitation = invitation?.t202StaInvitation ? invitation?.t202StaInvitation : "0000000"
        String t202StaInvitationxx1 = t202StaInvitation.length() > 0 ? t202StaInvitation.substring(0, 1) : "0"
        String t202StaInvitationxx2 = t202StaInvitation.length() > 1 ? t202StaInvitation.substring(1, 2) : "0"
        String t202StaInvitationxx3 = t202StaInvitation.length() > 2 ? t202StaInvitation.substring(2, 3) : "0"
        String t202StaInvitationxx4 = t202StaInvitation.length() > 3 ? t202StaInvitation.substring(3, 4) : "0"
        String t202StaInvitationxx5 = t202StaInvitation.length() > 4 ? t202StaInvitation.substring(4, 5) : "0"
        String t202StaInvitationxx6 = t202StaInvitation.length() > 5 ? t202StaInvitation.substring(5, 6) : "0"
        String t202StaInvitationxx7 = t202StaInvitation.length() > 6 ? t202StaInvitation.substring(6, 7) : "0"
        def statusReminder = invitation?.reminder?.m202ID
        Date t202TglJamApp = invitation.t202TglJamApp

        def data = [invitation: invitation,
                provinsi: getProvinsi,
                kota: getKota,
                kecamatan: getKec,
                kelurahan: getKel,
                kodepos: getPos,
                historyCustomer: historyCustomer,
                currentCondition: currentCondition,
                nopol: nopol,
                baseModel: baseModel,
                vinCode: vinCode,
                warna: currentCondition?.warna?.m092NamaWarna,
                reminder: invitation?.reminder,
                customerSurvey: customerSurvey,
                jenisSurvey: jenisSurvey,
                fa: finalInspection?.fa,
                sPK: sPK,
                sPkAsuransi: sPkAsuransi,
                jenisInvitation: invitation?.jenisInvitation,
                t202StaInvitation: t202StaInvitation,
                t202StaInvitationxx1: t202StaInvitationxx1,
                t202StaInvitationxx2: t202StaInvitationxx2,
                t202StaInvitationxx3: t202StaInvitationxx3,
                t202StaInvitationxx4: t202StaInvitationxx4,
                t202StaInvitationxx5: t202StaInvitationxx5,
                t202StaInvitationxx6: t202StaInvitationxx6,
                t202StaInvitationxx7: t202StaInvitationxx7,
                statusReminder: statusReminder,
                t202TglJamApp_hour: t202TglJamApp?.hours,
                t202TglJamApp_minute: t202TglJamApp?.minutes
        ]
        render data as JSON
    }

    def viewServiceHistory() {
        def invitation = Invitation.get(params.invitation)
        [invitation: invitation]
    }

    def historyServiceDatatablesList() {

        def invitation = Invitation.get(params.invitation)
        HistoryCustomerVehicle hcv = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(invitation?.customerVehicle, "0")

        session.exportParams = params
        def c = Reception.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0");
            eq("staSave","0");
            eq("historyCustomerVehicle", hcv)
            addOrder(Order.desc("id"))
        }
        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    tanggalService: it?.t401TanggalWO ? it?.t401TanggalWO.format("dd/MM/yyyy") : "-",
                    nopol: it.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " + it.historyCustomerVehicle?.t183NoPolTengah + " " + it.historyCustomerVehicle?.t183NoPolBelakang,
                    nomorWO: it.t401NoWO,
                    jenisService: it?.operation?.kategoriJob?.m055KategoriJob,
                    serviceAdvisor: it?.namaManPower?.t015NamaBoard,
                    teknisi: it.namaManPower?.t015NamaLengkap,
            ]
        }
        def ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

    def reCalculate(){
        def sql
//        params.nopol = "B 7474 NG"
//        params.nopol = "AA 84 GUS"
        def tglService
        def hcv = HistoryCustomerVehicle.findByFullNoPol(params.nopol)
        def dataRec = Reception.findAllByHistoryCustomerVehicleAndStaDelAndStaSave(hcv,'0','0')
        dataRec.sort{
            it.dateCreated
        }
        def ret = []
        def lastServiceDate = dataRec.size() > 0 ? dataRec.last()?.t401TanggalWO.format("dd-MMM-yyyy").toUpperCase():""
        def kmSaatIni = dataRec.size() > 0 ? dataRec?.last()?.t401KmSaatIni : "0"
        sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)

        try {
            sql.call("{? = call NEXT_SERVICE(?)}", [Sql.CHAR, params.nopol]){
                cursorResults ->

                    if (cursorResults.next()) {
                        tglService = cursorResults
                    }
            }

        }catch (Exception e){
            println("error "+e.message)
        }

        def nextServiceDate = dataRec.size() > 0 ? (dataRec?.last()?.t401TanggalWO + 180).format("dd-MMM-yyyy").toUpperCase() : tglService
        ret = [nextServiceDate:nextServiceDate, lastServiceDate : lastServiceDate, nextServiceKm : tglService, kmSaatIni : kmSaatIni]
        render ret as JSON
    }
}
