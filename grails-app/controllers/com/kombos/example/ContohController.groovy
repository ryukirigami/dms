package com.kombos.example

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.activiti.engine.runtime.ProcessInstance
import org.grails.activiti.ActivitiConstants

class ContohController {
    // XXX activiti
    static activiti = true

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def contohService

    // XXX activiti
    def taskService
    def activitiService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render contohService.datatablesList(params) as JSON
    }

    def create() {
        def result = contohService.create(params)

        if (!result.error)
            return [contohInstance: result.contohInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {

        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = contohService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Contoh", result.contohInstance.id])

            // XXX activiti
            String sessionUsernameKey = grailsApplication.config.activiti.sessionUsernameKey ?: ActivitiConstants.DEFAULT_SESSION_USERNAME_KEY
            params[sessionUsernameKey] = session[sessionUsernameKey]
            params["id"] = result.contohInstance.id
            ProcessInstance pi = activitiService.startProcess(params)

            redirect(action: 'show', id: result.contohInstance.id)
            return
        }

        render(view: 'create', model: [contohInstance: result.contohInstance])
        render "ok"
    }

    def show(Long id) {
        def result = contohService.show(params.id)

        if (!result.error)
            return [contohInstance: result.contohInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    // XXX activiti
    def showApprove() {
        if (params.taskId) {
            def variables = taskService.getVariables(params.taskId)
            def result = contohService.show(variables.id)

            if (!result.error)
                return [contohInstance: result.contohInstance, taskId: params.taskId]
        }


        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }
    // XXX activiti
    def showImplement() {
        if (params.taskId) {
            def variables = taskService.getVariables(params.taskId)
            def result = contohService.show(variables.id)

            if (!result.error)
                return [contohInstance: result.contohInstance, taskId: params.taskId]
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = contohService.show(params)
        if (!result.error)
            return [contohInstance: result.contohInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    // XXX activiti
    def approve(Long id, Long version) {
        def result = contohService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Contoh", params.id])
            completeTask(params)
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [contohInstance: result.contohInstance.attach()])
    }
    // XXX activiti
    def finishImplement(Long id, Long version) {
        def result = contohService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Contoh", params.id])
            completeTask(params)
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [contohInstance: result.contohInstance.attach()])
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = contohService.update(params)
        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Contoh", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [contohInstance: result.contohInstance.attach()])
    }

    def delete() {
        def result = contohService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Contoh", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Contoh, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}