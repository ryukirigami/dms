package com.kombos.woinformation

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON


class WoInformationController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def woInformationService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", findNoPol: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
        params.companyDealer = session.userCompanyDealer
    }

    def grailsApplication

    def list(Integer max) {
        String noWO = "";
        if(params?.nowo){
            noWO = params?.nowo;
        }
        [noWO : noWO]
    }

    def datatablesList() {
        session.exportParams=params

        render woInformationService.datatablesList(params) as JSON
    }

    def findData(){
        params.companyDealer = session.userCompanyDealer
        def result = woInformationService.findData(params);
        if(result!=null){
            render result as JSON
        }

    }

    def getTableBiayaData(){
        def result = woInformationService.getTableBiayaData(params);
        if(result!=null){
            render result as JSON
        }
    }

    def getTableRateData(){
        def result = woInformationService.getTableRateData(params);
        if(result!=null){
            render result as JSON
        }
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = woInformationService.save(params)
        render result
    }
}
