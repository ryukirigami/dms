package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class SmsTerkirimController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def smsTerkirimService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def c = RealisasiFU.createCriteria()
        def result = c.listDistinct {
            projections {
                property('t802NamaSA_FU')
            }
        }
        [idTable: new Date().format("yyyyMMddhhmmss"),realisasiFu:result]
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session.userCompanyDealer
        render smsTerkirimService.datatablesList(params) as JSON
    }

    def getSmsTerkirimHistory(){
        def result = smsTerkirimService.getSmsTerkirimHistory(params);
        if(result!=null){
            render result as JSON
        }
    }
}
