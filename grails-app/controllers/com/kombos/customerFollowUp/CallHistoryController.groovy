package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class CallHistoryController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def callHistoryService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def c = RealisasiFU.createCriteria()
        def result = c.listDistinct {
            projections {
                property('t802NamaSA_FU')
            }
        }
        [idTable: new Date().format("yyyyMMddhhmmss"),realisasiFu:result]
    }

    def datatablesList() {
        session.exportParams=params
        params?.companyDealer = session?.userCompanyDealer
        render callHistoryService.datatablesList(params) as JSON
    }

    def cekStatusFollowUp(){
        def realisasi = RealisasiFU.findById(params.id)?.followUp?.t801StaBerhasilFU
        render realisasi
    }
}
