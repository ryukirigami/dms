package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON

class CounterMeasureController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def counterMeasureService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss"),namaSA : listNamaSA()]
    }

    def datatablesList() {
        session.exportParams=params
        params?.companyDealer = session?.userCompanyDealer
        render counterMeasureService.datatablesList(params) as JSON
    }

    def datatablesListEdit(){
        session.exportParams=params
        render counterMeasureService.datatablesListEdit(params) as JSON
    }

    def cariData(){
        def realisasiFU = new RealisasiFU()
        def row = []
        if(params.idUbah){
            realisasiFU = RealisasiFU.get(params.idUbah.toLong())
            row << realisasiFU.id
        }
        render realisasiFU as JSON
    }

    def saveData(){
        def realisasi = RealisasiFU.get(params.idUbah.toLong())
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        realisasi.properties = params
        realisasi.lastUpdProcess = "UPDATE"
        realisasi.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        if(realisasi.save(flush: true)){
            render "ok"
        }
    }

    def listNamaSA(){
        def namaSA = User.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",session?.userCompanyDealer);
            roles{
                ilike("name","%SA & LSA%")
            }
            order("username");
        }
        return namaSA
    }
}
