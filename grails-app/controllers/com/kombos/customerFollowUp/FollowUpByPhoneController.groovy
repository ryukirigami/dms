package com.kombos.customerFollowUp

import com.kombos.administrasi.Operation
import com.kombos.administrasi.Section
import com.kombos.administrasi.Serial
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class FollowUpByPhoneController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def followUpByPhoneService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render followUpByPhoneService.datatablesList(params) as JSON
    }

    def create() {
        def result = followUpByPhoneService.create(params)

        if (!result.error)
            return [followUpByPhoneInstance: result.followUpByPhoneInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def followUpByPhoneInstance = new FollowUpByPhone()
        followUpByPhoneInstance.staDel = '0'
        followUpByPhoneInstance.lastUpdProcess = "INSERT"
        followUpByPhoneInstance?.dateCreated = getDatatablesUtilService()?.syncTime()
        followUpByPhoneInstance?.lastUpdated = getDatatablesUtilService()?.syncTime()
        followUpByPhoneInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        followUpByPhoneInstance.operation = Operation.get(params.operation.id.toLong());
        def cek = FollowUpByPhone.createCriteria().list {
            eq("staDel","0")
            operation{
                eq("id",params?.operation?.id.toLong())
            }
        }

        if(cek){
            flash.message = "Data sudah ada"
            render(view:'create', model:[followUpByPhoneInstance: followUpByPhoneInstance])
            return
        }

        if (!followUpByPhoneInstance.save(flush: true)) {
            render(view: "create", model: [followUpByPhoneInstance: followUpByPhoneInstance])
            return
        }

        flash.message = g.message(code: "default.created.message", args: ["Jobs Follow Up By Phone", ""])
        redirect(action: "show", id: followUpByPhoneInstance.id)
    }

    def show(Long id) {
        def result = followUpByPhoneService.show(params)

        if (!result.error)
            return [followUpByPhoneInstance: result.followUpByPhoneInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = followUpByPhoneService.show(params)

        if (!result.error)
            return [followUpByPhoneInstance: result.followUpByPhoneInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def followUpByPhoneInstance = FollowUpByPhone.get(id)
        if (!followUpByPhoneInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'followUpByPhone.label', default: 'Jobs Follow Up By Phone'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (followUpByPhoneInstance.version > version) {

                followUpByPhoneInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'followUpByPhone.label', default: 'Jobs Follow Up By Phone')] as Object[],
                        "Another user has updated this FollowUpByPhone while you were editing")
                render(view: "edit", model: [followUpByPhoneInstance: followUpByPhoneInstance])
                return
            }
        }
        def cek = FollowUpByPhone.createCriteria().list {
            eq("staDel","0")
            operation{
                eq("id",params.operation.id.toLong())
            }
        }

        if(cek){
            for(find in cek){
                if(find?.id != id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [followUpByPhoneInstance: followUpByPhoneInstance])
                    return
                    break
                }
            }
        }

        followUpByPhoneInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        followUpByPhoneInstance?.lastUpdProcess = "UPDATE"
        followUpByPhoneInstance?.lastUpdated = datatablesUtilService?.syncTime()
        followUpByPhoneInstance.operation = Operation.get(params.operation.id)

        if (!followUpByPhoneInstance.save(flush: true)) {
            render(view: "edit", model: [followUpByPhoneInstance: followUpByPhoneInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'followUpByPhone.label', default: 'Jobs Follow Up By Phone'), ""])
        redirect(action: "show", id: followUpByPhoneInstance.id)
    }

    def delete() {
        def result = followUpByPhoneService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["FollowUpByPhone", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(FollowUpByPhone, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def findSerialBySection(){
        def html="<option value=\'\'> Pilih Serial</option>"
        def serial = null
        def section = Section.findById(params.section.id.toLong())
        if(section) {
            serial = Serial.findAllBySectionAndStaDel(section,"0")
            serial.each {
                html=html + "<option value=\'${it.id}\'> ${it.m052NamaSerial}</option>"
            }
        }
        render html
    }

    def findJobBySerial(){
        def html="<option value=\'\'> Pilih Job</option>"
        if(params.serial.id){
            def operation = null
            def serial = Serial.findById(params.serial.id.toLong())
            if(serial){
                operation = Operation.createCriteria().list {
                    eq("staDel","0")
                    eq("serial",serial)
                    order("m053NamaOperation")
                }
                operation.each {
                    html=html + "<option value=\'${it.id}\'> ${it.m053NamaOperation}</option>"
                }
            }
        }
        render html
    }
}