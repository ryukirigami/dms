package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.maintable.KategoriWaktuFu
import com.kombos.maintable.Pertanyaan
import com.kombos.reception.Reception
import grails.converters.JSON
import org.hsqldb.jdbc.jdbcResultSet

class FollowUpController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def followUpService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def c = User.createCriteria()
        def result = c.list {
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)
            roles {
                ilike("name","%SA & LSA%");
            }
        }

        def c2= Reception.createCriteria()
        def result2 = c2.listDistinct {
            eq("staDel","0");
            eq("staSave","0");
            projections {
                property('t401NamaSA')
            }
        }

        [idTable: new Date().format("yyyyMMddhhmmss"),saAction:result,saPlan:result2]
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session.userCompanyDealer
        render followUpService.datatablesList(params) as JSON
    }
    def getData(){
        session.exportParams = params
        params."companyDealer" = session.userCompanyDealer
        def result = followUpService.getData(params);
        if(result!=null){
            render result as JSON
        }
    }
    def addFollow(){
        def realisasi = RealisasiFU.findByFollowUp(FollowUp.get(params.id as Long))
        def metodeSms = realisasi.followUp.metodeFu.id
        def nomorValid = realisasi.t802StaNomorValid
        def statusFollowUp = realisasi.followUp.t801StaBerhasilFU
        [id: params.id, metodeSms:metodeSms,nomorValid:nomorValid,statusFollowUp:statusFollowUp]
    }

    def editFollow(){
        def realisasi = RealisasiFU.findByFollowUp(FollowUp.get(params.id as Long))
        realisasi?.metodeFu = MetodeFu.findById(params.metodeSms as Long)
        realisasi?.t802StaNomorValid = params.nomorValid
        realisasi?.dateCreated = datatablesUtilService?.syncTime()
        realisasi?.lastUpdated = datatablesUtilService?.syncTime()
        realisasi.save(flush: true)
        def followUp = FollowUp.get(params.id as long)
        followUp?.metodeFu = MetodeFu.findById(params.metodeSms as Long)
        followUp?.dateCreated = datatablesUtilService?.syncTime()
        followUp?.lastUpdated = datatablesUtilService?.syncTime()
        followUp?.t801StaBerhasilFU = params.statusFollowUp
        followUp.save(flush: true)
        render "ok"
    }

    def addTerhubung(){
        def realisasi = RealisasiFU.findByFollowUp(FollowUp.get(params.id as Long))
        def staTerhubung = realisasi?.t802StaTerhubung
        [id: params.id,staTerhubung:staTerhubung]
    }

    def editTerhubung(){
        def realisasi = RealisasiFU.findByFollowUp(FollowUp.get(params.id as Long))

        realisasi?.t802StaTerhubung = params.terhubung
        realisasi.save(flush: true)
        def jsonArray = JSON.parse(params.alasan)
        jsonArray.each {
            def pert = Pertanyaan.get(it[0] as Long)

            if(pert){
                pert?.t803StaPuasTdkPuas = it[2]
                pert?.t803AlasanTdkPuas = it[3]
                pert.save(flush: true)
            }else{
                def pert2 = new Pertanyaan()
                pert2.companyDealer = session.userCompanyDealer
                pert2.t803ID = Pertanyaan?.list()?.size()>0 ? (Pertanyaan?.last()?.t803ID?.toInteger()+1): 1
                pert2.lastUpdProcess = "INSERT"
                pert2.realisasiFU = realisasi
                pert2.pertanyaanFu = PertanyaanFu.get(it[1] as long)
                pert2.t803TglRespon = new Date()
                pert2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                pert2.t803StaPuasTdkPuas = it[2] as String
                pert2.t803AlasanTdkPuas = it[3] ? it[3] : "-"
                pert2.dateCreated = datatablesUtilService?.syncTime()
                pert2.lastUpdated = datatablesUtilService?.syncTime()
                pert2.save(flush: true)
                pert2.errors.each {
                    println(it)
                }
            }
        }
        render "ok"
    }

    def addReFollow(){
        def realisasi = RealisasiFU.findByFollowUp(FollowUp.get(params.id as long))
        def metodeSms = realisasi.metodeFu.id
        def kategoriWaktuFu = realisasi.followUp.kategoriWaktuFu.id
        [id: params.id,metodeSms:metodeSms,kategoriWaktuFu:kategoriWaktuFu]
    }

    def editReFollow(){
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        String mul = params.tanggal
        if(params.ket){
            params.ket = "-"
        }
        Date tanggal = new Date().parse('yyyy-MM-d', mul.toString())
        def followUp = FollowUp.get(params.id as long)
        followUp?.kategoriWaktuFu = KategoriWaktuFu.findById(params.kategoriWaktuFu)
        followUp?.metodeFu = MetodeFu.findById(params.metodeFu)
        followUp?.t801Ket = params.ket
        followUp?.t801TglJamFU = tanggal
        followUp?.dateCreated = datatablesUtilService?.syncTime()
        followUp?.lastUpdated = datatablesUtilService?.syncTime()
        followUp.save(flush: true)
        render "ok"
    }
}
