package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.maintable.JobSuggestion
import com.kombos.production.FinalInspection
import com.kombos.reception.Reception
import grails.converters.JSON

class ServiceHistoryController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def serviceHistoryService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sublist() {
        [noWo : params.noWo,idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsublist() {
        [idTable: new Date().format("yyyyMMddhhmmss"),noWo : params.noWo,job : params.job]
    }

    def datatablesList() {
        params.companyDealer = session?.userCompanyDealer
        render serviceHistoryService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        render serviceHistoryService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {
        render serviceHistoryService.datatablesSubSubList(params) as JSON
    }
    def finalIns(){
        def finalInspection = FinalInspection.findByReception(Reception.findByT401NoWO(params.noWo))
        def jobS = JobSuggestion.findByCustomerVehicle(Reception.findByT401NoWO(params.noWo).historyCustomerVehicle.customerVehicle)
        String kategori = ""
        String fi = finalInspection?.t508StaKategori
        kategori =  fi?.substring(0,1)=="1"?"Safety, ":""
        kategori +=  fi?.substring(1,2)=="1"?"Regulation, ":""
        kategori +=  fi?.substring(2,3)=="1"?"Comfortable, ":""
        kategori +=  fi?.substring(3,4)=="1"?"Efficiency":""

        def res = [:]
        res.catatan = finalInspection?.t508Keterangan
        res.job     = jobS?.t503KetJobSuggest?jobS?.t503KetJobSuggest:"-"
        res.kategori = ": " + kategori
        res.tanggal  = jobS?.t503TglJobSuggest?": "+jobS?.t503TglJobSuggest.format("dd MMMM yyyy"):": -"
        res.staFI    = finalInspection?.t508StaInspection=="1"?": Fix":": Non Fix"
        render res as JSON
    }



    def getVincodes(){
        def res = [:]
        def opts = []
        def vincodes = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            ilike("t103VinCode","%"+params.vincode+"%")
            order("t103VinCode")
            maxResults(10);
        }
        vincodes.each {
            opts<<it.t103VinCode
        }
        res."options" = opts
        render res as JSON
    }




}
