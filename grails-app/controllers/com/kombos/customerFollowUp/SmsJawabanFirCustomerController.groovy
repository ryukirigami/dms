package com.kombos.customerFollowUp

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.Pertanyaan
import grails.converters.JSON

class SmsJawabanFirCustomerController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def smsJawabanFirCustomerService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def c = RealisasiFU.createCriteria()
        def result = c.listDistinct {
            projections {
                property('t802NamaSA_FU')
            }
        }
        def c2 = RealisasiFU.createCriteria()
        def results2 = c2.list () {
            metodeFu{
                eq("m801NamaMetodeFu","SMS")
            }
        }
        int valid = 0, invalid = 0, validate = 0, all = 0
        results2.each {
            def tampil = false
            def pert1,pert2,pert3,pert4,pert5
            pert1 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(1))
            pert2 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(2))
            pert3 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(3))
            pert4 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(4))
            pert5 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(5))
            if(pert1 || pert2 || pert3){
                tampil = true
            }
            if(tampil == true){
                if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="1"){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="0"){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="0" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="0" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="0" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="0" && pert2?.t803StaPuasTdkPuas=="" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if((pert1?.t803StaPuasTdkPuas!="1" && pert1?.t803StaPuasTdkPuas!="0") || (pert2?.t803StaPuasTdkPuas!="1" && pert2?.t803StaPuasTdkPuas!="0") || (pert3?.t803StaPuasTdkPuas!="1" && pert3?.t803StaPuasTdkPuas!="0") || (pert4?.t803StaPuasTdkPuas!="1" && pert4?.t803StaPuasTdkPuas!="0") || (pert5?.t803StaPuasTdkPuas!="1" && pert5?.t803StaPuasTdkPuas!="0")){
                    validate++
                }else{
                    invalid++
                }
                all++
            }
        }
        [idTable: new Date().format("yyyyMMddhhmmss"), realisasiFu : result, cValid : valid, cInvalid:invalid, cValidate : validate, cAll :all]
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session.userCompanyDealer
        render smsJawabanFirCustomerService.datatablesList(params) as JSON
    }

    def getData(){
        def result = smsJawabanFirCustomerService.getData(params);
        if(result!=null){
            render result as JSON
        }
    }

    def editSms(){
        def c = RealisasiFU.findById(params.id)
        def isi = params.isi
        def tanyaId = null
        if(params.isi=="-"){
            isi = ""
        }

        if(params.pertanyaan=="Q1"){
            tanyaId = 1
        }else if(params.pertanyaan=="Q2"){
            tanyaId = 2
        }else if(params.pertanyaan=="Q3"){
            tanyaId = 3
        }else if(params.pertanyaan=="Q4"){
            tanyaId = 4
        }else if(params.pertanyaan=="Q5"){
            tanyaId = 5
        }

        def noWo = c.followUp?.reception?.t401NoWO
        def nopol = c.followUp.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+c.followUp.reception.historyCustomerVehicle.t183NoPolTengah + " " + c.followUp.reception.historyCustomerVehicle.t183NoPolBelakang
        [nilai : isi, pertanyaan : params.pertanyaan, noWo : noWo,nopol:nopol, id:params.id,tanyaId:tanyaId]
    }

    def save(){
        params.lastUpdated = datatablesUtilService?.syncTime()
        def jawaban = ""
        jawaban = params.jawaban
        if(params.jawaban=="" || params.jawaban==null){
            jawaban = "-"

        }
        def pert = Pertanyaan.findByRealisasiFUAndPertanyaanFu(RealisasiFU.findById(params.id),PertanyaanFu.findById(params.pertanyaan as Long))
        def pertanyaan = Pertanyaan.get(pert.id)
        pertanyaan?.t803StaPuasTdkPuas = jawaban
        pertanyaan?.save(flush: true)

        def c2 = RealisasiFU.createCriteria()
        def results2 = c2.list () {
            metodeFu{
                eq("m801NamaMetodeFu","SMS")
            }
        }
        int valid = 0, invalid = 0, validate = 0, all = 0
        results2.each {
            def tampil = false
            def pert1,pert2,pert3,pert4,pert5
            pert1 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(1))
            pert2 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(2))
            pert3 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(3))
            pert4 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(4))
            pert5 = Pertanyaan.findByRealisasiFUAndPertanyaanFu(it,PertanyaanFu.findById(5))
            if(pert1 || pert2 || pert3 || pert4 || pert5){
                tampil = true
            }
            if(tampil == true){
                if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="1"){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="1" && pert5?.t803StaPuasTdkPuas=="0"){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="1" && pert4?.t803StaPuasTdkPuas=="0" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="1" && pert3?.t803StaPuasTdkPuas=="0" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="1" && pert2?.t803StaPuasTdkPuas=="0" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if(pert1?.t803StaPuasTdkPuas=="0" && pert2?.t803StaPuasTdkPuas=="" && pert3?.t803StaPuasTdkPuas=="" && pert4?.t803StaPuasTdkPuas=="" && pert5?.t803StaPuasTdkPuas==""){
                    valid++
                }else if((pert1?.t803StaPuasTdkPuas!="1" && pert1?.t803StaPuasTdkPuas!="0") || (pert2?.t803StaPuasTdkPuas!="1" && pert2?.t803StaPuasTdkPuas!="0") || (pert3?.t803StaPuasTdkPuas!="1" && pert3?.t803StaPuasTdkPuas!="0") || (pert4?.t803StaPuasTdkPuas!="1" && pert4?.t803StaPuasTdkPuas!="0") || (pert5?.t803StaPuasTdkPuas!="1" && pert5?.t803StaPuasTdkPuas!="0")){
                    validate++
                }else{
                    invalid++
                }
                all++
            }

        }

        def res = [:]
        res.valid = valid
        res.validate = validate
        res.invalid = invalid
        res.all = all
        render res as JSON
    }
}