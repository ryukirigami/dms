package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ViewReceptionBpCenterController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
		redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Reception.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0");
            eq("staSave","0");
            eq("companyDealer",session?.userCompanyDealer)
            if (params."sCriteria_t401NoWO") {
                ilike("t401NoWO", "%" + (params."sCriteria_t401NoWO" as String) + "%")
            }

            if (params."sCriteria_t401TanggalWO") {
                ge("t401TanggalWO", params."sCriteria_t401TanggalWO")
                lt("t401TanggalWO", params."sCriteria_t401TanggalWO" + 1)
            }

            if (params."sCriteria_t401TglJamCetakWO") {
                ge("t401TglJamCetakWO", params."sCriteria_t401TglJamCetakWO")
                lt("t401TglJamCetakWO", params."sCriteria_t401TglJamCetakWO" + 1)
            }

            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan", "BP")
                }
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it?.id,

                    t401NoWO: it?.t401NoWO,

                    t401TanggalWO: it?.t401TanggalWO ? it?.t401TanggalWO?.format(dateFormat) : "",

                    customerIn: it?.customerIn,

                    customerVehicle: it?.customerIn?.customerVehicle,

                    historyCustomerVehicle: it?.historyCustomerVehicle,

                    customer: it?.customerIn?.reception,

                    historyCustomer: it?.historyCustomer,

                    t401Area: it?.t401Area,
					
					namaCustomer: it?.historyCustomer?.fullNama,
					
					alamatCustomer: it?.historyCustomer?.t182Alamat,
					
					nomorHp: it?.historyCustomer?.t182NoHp,
					
					kendaraan: it?.customerIn?.customerVehicle?.currentCondition?.fullModelCode?.baseModel?.m102NamaBaseModel,
					
					noPolKendaraan: it?.historyCustomerVehicle?.toString()
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [receptionInstance: new Reception(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def receptionInstance = new Reception(params)
        if (!receptionInstance.save(flush: true)) {
            render(view: "create", model: [receptionInstance: receptionInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'reception.label', default: 'Reception'), receptionInstance.id])
        redirect(action: "show", id: receptionInstance.id)
    }

    def show(Long id) {
        def receptionInstance = Reception.get(id)
        if (!receptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reception.label', default: 'Reception'), id])
            redirect(action: "list")
            return
        }

        [receptionInstance: receptionInstance]
    }

    def edit(Long id) {
        def receptionInstance = Reception.get(id)
        if (!receptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reception.label', default: 'Reception'), id])
            redirect(action: "list")
            return
        }

        [receptionInstance: receptionInstance]
    }

    def update(Long id, Long version) {
        def receptionInstance = Reception.get(id)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        if (!receptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reception.label', default: 'Reception'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (receptionInstance.version > version) {

                receptionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'reception.label', default: 'Reception')] as Object[],
                        "Another user has updated this Reception while you were editing")
                render(view: "edit", model: [receptionInstance: receptionInstance])
                return
            }
        }

        receptionInstance.properties = params

        if (!receptionInstance.save(flush: true)) {
            render(view: "edit", model: [receptionInstance: receptionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'reception.label', default: 'Reception'), receptionInstance.id])
        redirect(action: "show", id: receptionInstance.id)
    }

    def delete(Long id) {
        def receptionInstance = Reception.get(id)
        if (!receptionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'reception.label', default: 'Reception'), id])
            redirect(action: "list")
            return
        }

        try {
            receptionInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'reception.label', default: 'Reception'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'reception.label', default: 'Reception'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Reception, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Reception, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
