package com.kombos.reception

import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.administrasi.MasterWac
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.WAC
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import javax.imageio.ImageIO
import java.awt.*
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.text.DateFormat
import java.text.SimpleDateFormat

class ReceptionServiceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
//        redirect(action: "testPageWAC", params: params)
    }

    def testService(){
        def ret = [sEcho: "testService"]
        render ret as JSON
    }

    def getKodeKotaNoPol(){
        def result = KodeKotaNoPol.createCriteria().list {
            eq("staDel","0")
            order("m116ID")
        }
        def rows = []
        result.each {
            rows << [

                    id: it.id,

                    m116ID: it.m116ID.toUpperCase(),

                    m116NamaKota: it.m116NamaKota

            ]
        }
        render rows as JSON
    }

    def getDataAwal(){
        def result = [:]
        def nopols = KodeKotaNoPol.createCriteria().list {
            eq("staDel","0")
            order("m116ID")
        }
        def rows = []
        nopols.each {
            rows << [
                    id: it.id,

                    m116ID: it.m116ID.toUpperCase(),

                    m116NamaKota: it.m116NamaKota
            ]
        }
        def items = MasterWac.createCriteria().list {
            order("m403NamaPerlengkapan")
        }
        def rows2 = []
        items.each {
            rows2 << [
                    id: it.id,

                    nama: it.m403NamaPerlengkapan
            ]
        }

        result.nopol = rows
        result.items = rows2
        render result as JSON
    }

    def getReceptionByNoPol(){
        def kodeKotaNoPol = params.kodeKotaNoPol
        def noPolTengah =  params.noPolTengah
        def noPolBelakang =  params.noPolBelakang

        def result = [:]
        String fullNoPol = kodeKotaNoPol+" "+noPolTengah+" "+noPolBelakang
        def historyCustomerVehicle = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0");
            eq("fullNoPol",fullNoPol,[ignoreCase : true])
        }
        if(historyCustomerVehicle){
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            def waktuSekarang = df.format(new Date())
            Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
            Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
            def receptions = Reception.findAllByHistoryCustomerVehicleAndT401TanggalWOBetweenAndStaDelAndStaSave(historyCustomerVehicle?.last(),dateAwal,dateAkhir,"0","0");
            if(receptions){
                result.id = receptions.last().id
            }else{
                result.id = null
            }
        }
        render result as JSON
    }

    def printWAC(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        def historyCustomerVehicleInstance = receptionInstance.historyCustomerVehicle
        def items = WAC.findAllByReception(receptionInstance)
        def wacChecklistItems = items.collect {
            [itemName: it?.masterWACID?.m403NamaPerlengkapan, countOk: it?.t410StaItem=="0"?"V":"", countNotOk: it?.t410StaItem=="1"?"V":"", statusNone: it?.t410StaItem=="2"?"V":"", note: it?.t410Keterangan?it?.t410Keterangan:""]
        }
        String imagePath = g.resource(dir: 'images', file: 'Hasjrat_logo.png', absolute: true)
        def rows1 = []
        def c = WAC.createCriteria()
        def no = 1;
        def allWac = c.list {
            eq("reception",receptionInstance)
        }
        allWac.each {
            rows1 << [
                    itemName : it?.masterWACID?.m403NamaPerlengkapan,
                    countOk : it?.t410StaItem=="0" ? "V" : "",
                    countNotOk : it?.t410StaItem=="1" ? "V" : "",
                    statusNone : it?.t410StaItem=="2" ? "V" : "",
                    note : it?.t410Keterangan
            ]
        }
        if(receptionInstance?.wacItem){
            def wac = receptionInstance?.wacItem
            wac.each {
                rows1 << [
                        itemName : it?.itemName,
                        countOk : it?.statusItem=="0" ? "V" : "",
                        countNotOk : it?.statusItem=="1" ? "V" : "",
                        statusNone : it?.statusItem=="2" ? "V" : "",
                        note : it?.note
                ]
            }
        }

        if(wacChecklistItems?.size()<1){
            rows1 <<
                [
                        itemName: "-",
                        countOk: "-",
                        countNotOk: "-",
                        statusNone: "-",
                        note: "-"
                ]
        }

        def reportData = []
        Image gambar = null
        if(receptionInstance?.t401GambarWAC){
            println "GAMBAR SEBENARNYAAA"
            gambar = ImageIO.read(new ByteArrayInputStream(receptionInstance.t401GambarWAC));
        }

        User user = User.findByUsernameAndStaDel(receptionInstance?.t401NamaSA,"0");
        NamaManPower manPower = NamaManPower.findByUserProfileAndStaDel(user,"0");
        reportData << [
                namaCompany : receptionInstance?.companyDealer?.m011NamaWorkshop,
                alamat : "Alamat : ${receptionInstance?.companyDealer?.m011Alamat}",
                telp : "Telp.\t: ${receptionInstance?.companyDealer?.m011Telp}",
                fax : "Fax.\t: ${receptionInstance?.companyDealer?.m011Fax}",
                itemName: "name",
                countOk: "1",
                countNotOk: "0",
                statusNone: "N",
                note: "none",
                wacChecklistItems : rows1,
                imageWac : gambar,
                namaSA : user ? user?.fullname : "",
                hpSA : manPower ? manPower?.t015NoTelp : ""
        ]

        def parameters = [
                NoPol: historyCustomerVehicleInstance.fullNoPol,
                BookingStatus: receptionInstance?.customerIn?.t400NomorAntrian?.substring(0,1)?.equalsIgnoreCase("B") ? "Booking" : "Walk in",
                Catatan : receptionInstance?.t401CatatanWAC,
                Fuel : (receptionInstance?.t401Bensin ? (receptionInstance?.t401Bensin/10*100)+"%" : "-"),
                Model : historyCustomerVehicleInstance?.fullModelCode?.baseModel?.m102NamaBaseModel,
                Warna : historyCustomerVehicleInstance?.warna?.m092NamaWarna,
                NoWo : receptionInstance?.t401NoWO,
                GrOrBp : receptionInstance?.customerIn?.tujuanKedatangan,
                TglMasuk : receptionInstance?.customerIn?.dateCreated?.format("dd-MM-yyyy"),
                JamMasuk : receptionInstance?.customerIn?.dateCreated?.format("HH:mm:ss"),
                TglPenyerahan : receptionInstance?.t401TglJamJanjiPenyerahan ? receptionInstance?.t401TglJamJanjiPenyerahan?.format("dd-MM-yyyy"): "",
                JamPenyerahan : receptionInstance?.t401TglJamJanjiPenyerahan ? receptionInstance?.t401TglJamJanjiPenyerahan?.format("HH:mm:ss"): "",
                NamaPelanggan : receptionInstance?.historyCustomer?.fullNama,
                AlamatPelanggan : receptionInstance?.historyCustomer?.t182Alamat,
                NoTelpPelanggan : receptionInstance?.historyCustomer?.t182NoHp,
                imagePath : imagePath
        ]

        def reportDef = new JasperReportDef(name:'wac.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )

        def file = File.createTempFile("WAC_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }

    def saveWAC(){
        String sResult = "1";
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def cekError = false;
        if(params.id=="" || params.id==null){
            sResult += " | id reception null "
            cekError = true;
        }
        if(params.bensin && !params.bensin.toString().isNumber()){
            sResult += " | bensin must be number "
            cekError = true;
        }
        if(params.countRow && !params.countRow.toString().isNumber()){
            sResult += " | countRow must be number"
            cekError = true;
        }

        if(!cekError){
            def id = params.id.toLong()
            def wacChecklistItems = new ArrayList()
            def receptionInstance = Reception.get(id);
            def countRow = 0;
            if(params.countRow){
                countRow = params.countRow.toLong();
            }
            def indexRow = 1;

            def wacChecklistItem

            while(indexRow <= countRow){
                def wac = new WAC()
                String itemName = "itemName"+indexRow
                String staItem = "staItem"+indexRow
                String note = "note"+indexRow
                wacChecklistItem = new WacItem()
                def cek = new MasterWac()
                if(params?."${itemName}"){
                    cek = MasterWac.findByM403NamaPerlengkapanIlike(params?."${itemName}")
                    if(cek){
                        wac = WAC.findByReceptionAndMasterWACID(receptionInstance,cek)
                        if(!wac){
                            wac = new WAC()
                            wac.dateCreated = datatablesUtilService?.syncTime()
                        }
                        wac.reception = receptionInstance
                        wac.masterWACID = cek
                    }else{
                        wacChecklistItem.itemName = params?."${itemName}"
                    }

                    if(params?."${staItem}"){
                        if(cek){
                            wac.t410StaItem = params?."${staItem}"
                        }else{
                            wacChecklistItem.statusItem = params?."${staItem}"
                        }
                    }
                    if(params?."${note}"){
                        if(cek){
                            wac.t410Keterangan = params?."${note}"
                        }else{
                            wacChecklistItem.note = params?."${note}"
                        }
                    }

                    if(cek){
                        wac.lastUpdated = datatablesUtilService?.syncTime()
                        wac.save(flush: true)
                        wac.errors.each {println it}
                    }else{
                        wacChecklistItems.add(wacChecklistItem)
                    }

                }

                indexRow++
            }

            if(!cekError){

                if(params.gambarWAC){
                    String namaFile = params.gambarWAC.originalFilename
                    if(namaFile!=""){
                        def f = request.getFile('gambarWAC')
                        receptionInstance.t401GambarWAC = f.getBytes()
                    }
                }
                if(params.catatan){
                    receptionInstance.t401CatatanWAC = params.catatan
                }
                if(params.bensin){
                    receptionInstance.t401Bensin = params.bensin.toInteger()
                }
                if(wacChecklistItems.size()>0){
                    receptionInstance.wacItem = wacChecklistItems
                }
                receptionInstance?.lastUpdated = datatablesUtilService?.syncTime()
                if (!receptionInstance.save(flush: true)) {
                    println receptionInstance.errors
                    sResult="0";
                }
            }
        }
        def result = [success: sResult]
        render result as JSON
    }

    def testPageWAC(){
    }
}
