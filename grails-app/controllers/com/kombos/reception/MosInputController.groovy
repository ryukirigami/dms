package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Claim
import com.kombos.parts.Goods
import com.kombos.parts.Request
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class MosInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def addModal(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def list(Integer max) {
        String staUbah = "create"
        def cari = new MOS()
        def u_tanggal = ""
        if(params.noMos!=null){
            staUbah="ubah"
            cari = MOS.findByReception(Reception?.findByT401NoWO(params.noMos))
            u_tanggal = cari?.t417TglMOS?cari?.t417TglMOS.format("dd-MM-yyyy"):""
        }
        [noMos : "009" + new Date().format("yyyyMMddhhmmss"),u_tanggal : u_tanggal, u_noMos : cari?.t417NoMOS,u_noWo : params.noMos ,aksi:staUbah]
    }
    def cek(){
        def rec = Reception.findByT401NoWO(params.noWo)
        if(rec){
            def model = rec.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
            def warna = rec.historyCustomerVehicle?.warna?.m092NamaWarna
            def nopol = rec.historyCustomerVehicle?.kodeKotaNoPol?.m116ID +" "+rec.historyCustomerVehicle?.t183NoPolTengah + "" + rec.historyCustomerVehicle?.t183NoPolBelakang
            render model + " | " + warna +" | "+ nopol
        }else{
            render ""
        }
    }
    def req(){
        DateFormat df = new SimpleDateFormat("yyyy-M-d")
        def reception =  Reception.findByT401NoWO(params.noWo)
        def hasil = null
        if(reception){
            def jsonArray = JSON.parse(params.ids)
            def mos = null;
            def nomos = ""
            if(params.noMosBefore==null || params.noMosBefore==""){
                nomos = params.noMos
            }
            jsonArray.each {
                def goods = Goods.get(it)
                def tanggal = df.format(params.tanggal)
                if(goods){
                    mos = new MOS()
                    mos?.reception = reception
                    if(params.noMosBefore==null || params.noMosBefore==""){
                        mos?.t417NoMOS = nomos
                    }else{
                        mos?.t417NoMOS = params.noMosBefore
                    }
                    mos?.goods = goods
                    mos?.t417TglMOS = new Date().parse('yyyy-M-d', tanggal)
                    mos?.t417Qty1 = params."qty-${it}" as Double
                    mos?.t417Qty2 = params."qty-${it}" as Double
                    mos?.t417xDivisi = "DIVISI"
                    mos?.t417xKet = "-"
                    mos?.t417xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()

                    mos?.staDel = '0'
                    mos?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    mos?.dateCreated = datatablesUtilService?.syncTime()
                    mos?.lastUpdated = datatablesUtilService?.syncTime()
                    mos?.lastUpdProcess  ="INSERT"
                    mos?.save(flush: true)
                    mos?.errors.each{
                        println it
                    }
                }
            }
            hasil = nomos
        }else {
            hasil = "fail"
        }
        render hasil
    }

    def ubahData(){
        DateFormat df = new SimpleDateFormat("yyyy-M-d")
        def reception = Reception.findByT401NoWO(params.u_nowo)
        def jsonArray = JSON.parse(params.ids)
        def mos = null;
        jsonArray.each {
            def goods = Goods.get(it)
            String tanggal = params.u_tanggal
            def cekData = MOS.findByReceptionAndGoods(reception,goods)
            if(cekData){
                def mosEdit = MOS.get(cekData.id)
                mosEdit?.t417Qty1 = params."qty-${it}" as Double
                mosEdit?.t417Qty2 = params."qty-${it}" as Double
                mosEdit?.lastUpdProcess  ="UPDATE"
                mosEdit?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mosEdit?.lastUpdated = datatablesUtilService?.syncTime()
                mosEdit.save(flush: true)
            }else{
                mos = new MOS()
                mos?.reception = reception
                mos?.t417NoMOS = params.u_nomos
                mos?.goods = goods
                mos?.t417TglMOS = new Date().parse('yyyy-M-d', tanggal.toString())
                mos?.t417Qty1 = params."qty-${it}" as Double
                mos?.t417Qty2 = params."qty-${it}" as Double
                mos?.t417xDivisi = "DIVISI"
                mos?.t417xKet = "-"
                mos?.t417xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mos?.staDel = '0'
                mos?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mos?.dateCreated = datatablesUtilService?.syncTime()
                mos?.lastUpdated = datatablesUtilService?.syncTime()
                mos?.lastUpdProcess  ="INSERT"
                mos.save(flush: true)
            }
        }
        render "kosong"
    }
    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Claim, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        try {
            datatablesUtilService.updateField(Request, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def kodeList() {
        def res = [:]
        def result = Reception.findAllWhere(staDel : '0',staSave: '0')
        def opts = []
        result.each {
            opts << it.t401NoWO
        }

        res."options" = opts
        render res as JSON
    }

    def getTableData(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def result = []
        def hasil = MOS.createCriteria().list() {
            reception{
                eq("t401NoWO",params.id)
            }
        }
        hasil.each {
            result << [
                    id : it.goods.id,
                    goods : it.goods.m111ID,
                    goods2 : it.goods.m111Nama,
                    qty : it.t417Qty1,
                    satuan : it.goods.satuan?.m118Satuan1

            ]
        }
        render result as JSON
    }
    def addModalDatatablesList(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params
        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
            if(params.sCriteria_goods){
                ilike('m111ID',"%"+ params.sCriteria_goods + "%" as String)
            }
            if(params.sCriteria_goods2){
                ilike('m111Nama',"%"+ params.sCriteria_goods2 + "%" as String)
            }
            switch (sortProperty) {
                case "goods":

                        order("m111ID",sortDir)

                    break;
                case "goods2":

                        order("m111Nama",sortDir)

                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def nos = 0
        results.each {
            nos = nos + 1
            rows << [

                    id: it?.id,

                    goods: it?.m111ID,

                    goods2: it?.m111Nama,

                    qty : '0',

                    satuan: it?.satuan.m118Satuan1,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

}
