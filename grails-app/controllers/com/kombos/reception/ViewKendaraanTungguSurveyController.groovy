package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.SPKAsuransi
import grails.converters.JSON

class ViewKendaraanTungguSurveyController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = SPKAsuransi.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_tanggalSPK") {
                ge("t193TanggalSPK", params."sCriteria_tanggalSPK")
                lt("t193TanggalSPK", params."sCriteria_tanggalSPK" + 1)
            }



            ilike("staDel", "0")

            order("id","desc")
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    namaAsuransi: it.t193IDAsuransi,

                    nomorPolisi: it.customerVehicle.t103VinCode,

                    nomorPolisAsuransi: it.t193NomorPolis,

                    status: it.t193StaNomorSPK

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }



}
