package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.maintable.Retention
import grails.converters.JSON

class ViewAppointmentController {
	def viewAppointmentService

    def smsService

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def belumBayarBookingFee() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px', type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string'],
			[field: 'janjiBayar', label: 'Janji Bayar', width: '200px',type:'date'],
			[field: 'jmlBookingFee', label: 'Jml Booking Fee', width: '200px',type:'numeric']
			]
		render(view:"list",model:[id:'belumBayarBookingFee', label: 'View Customer Appointment Belum Bayar Booking Fee', columns: columns])		
	}
	
	def belumBayarBookingFeeDatatablesList() {
		render viewAppointmentService.belumBayarBookingFeeDatatablesList(params) as JSON
	}
	
	def sudahBayarBookingFeeBelumOrderParts() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'alamatCustomer', label: 'Alamat Customer', width: '300px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string']
			]
		render(view:"list",model:[id:'sudahBayarBookingFeeBelumOrderParts', label: 'View Customer Appointment Sudah Bayar Booking Fee Belum Order Parts', columns: columns])		
	}
	
	def sudahBayarBookingFeeBelumOrderPartsDatatablesList() {
		render viewAppointmentService.sudahBayarBookingFeeBelumOrderPartsDatatablesList(params) as JSON
	}
	
	def tungguETAParts() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'namaSA', label: 'Nama SA', width: '300px',type:'string'],
			[field: 'statusAmbil', label: 'Status Ambil', width: '200px',type:'string']
		]
		render(view:"listETAParts",model:[id:'tungguETAParts', label: 'View Customer Appointment Tunggu ETA Parts', columns: columns])
	}

    def tungguETAPartsSub() {
        def columns = [
                [field: 'spacer', label: '&nbsp', width: '28px',type:'nested'],
                [field: 'eta', label: 'ETA', width: '680px',type:'nested'],
                [field: 'parts', label: 'Parts', width: '680px',type:'nested']
        ]
        render(view:"sublist",model:[id:'tungguETAPartsSub', label: 'View Customer Appointment Tunggu ETA Parts', columns: columns, noWO: params.noWo])
    }
	
	def tungguETAPartsDatatablesList() {
		render viewAppointmentService.tungguETAPartsDatatablesList(params) as JSON
	}

    def tungguETAPartsSubDatatablesList() {
        render viewAppointmentService.tungguETAPartsDatatablesSubList(params) as JSON
    }
	
	def registrasiAccountWeb() {
		def columns = [
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'alamatCustomer', label: 'Alamat', width: '300px',type:'string'],
			[field: 'noHP', label: 'No HP', width: '200px',type:'string'],
			[field: 'noTelpRumah', label: 'No Telp Rumah', width: '200px',type:'string'],
			[field: 'jenisKelamin', label: 'Jenis Kelamin', width: '200px',type:'sex'],
			[field: 'tglLahir', label: 'Tanggal Lahir', width: '200px',type:'date'],
			]
		render(view:"list",model:[id:'registrasiAccountWeb', label: 'View Customer Registrasi Account Web', columns: columns])		
	}
	
	def registrasiAccountWebDatatablesList() {
		render viewAppointmentService.registrasiAccountWebDatatablesList(params) as JSON
	}
	
	def viaWebBelumPlotASBJPB() {
		def columns = [
			[field: 'noUrut', label: 'NO', width: '50px',type:'seq'],
			[field: 'namaCustomerCustLama', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'alamatCustomer', label: 'Alamat', width: '300px',type:'string'],
			[field: 'noHP', label: 'No HP', width: '150px',type:'string'],
			[field: 'tglDaftar', label: 'Tanggal Daftar', width: '200px',type:'date'],
			[field: 'pekerjaan', label: 'Nama Job', width: '200px',type:'string'],
			[field: 'tglService', label: 'Waktu Service', width: '200px',type:'date'],
			]
		render(view:"list",model:[id:'viaWebBelumPlotASBJPB', label: 'View Customer Appointment Via Web Belum Plot ASB/JPB', columns: columns])
	}
	
	def viaWebBelumPlotASBJPBDatatablesList() {
		render viewAppointmentService.viaWebBelumPlotASBJPBDatatablesList(params) as JSON
	}
	
	def sudahBayarBookingFeeBelumPlotJPB() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'alamatCustomer', label: 'Alamat Customer', width: '300px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string']
			]
		render(view:"list",model:[id:'sudahBayarBookingFeeBelumPlotJPB', label: 'View Customer Appointment Sudah Bayar Booking Fee Belum Plot JPB', columns: columns])		
	}
	
	def sudahBayarBookingFeeBelumPlotJPBDatatablesList() {
		render viewAppointmentService.sudahBayarBookingFeeBelumPlotJPBDatatablesList(params) as JSON
	}
	
	def menungguKelengkapanDokumenAsuransi() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'alamatCustomer', label: 'Alamat Customer', width: '300px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string']
			]
		render(view:"list",model:[id:'menungguKelengkapanDokumenAsuransi', label: 'View Customer Appointment Menunggu Kelengkapan Dokumen Asuransi', columns: columns])		
	}
	
	def menungguKelengkapanDokumenAsuransiDatatablesList() {
		render viewAppointmentService.menungguKelengkapanDokumenAsuransiDatatablesList(params) as JSON
	}
	
	def menungguSurvey() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'alamatCustomer', label: 'Alamat Customer', width: '300px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string']
			]
		render(view:"list",model:[id:'menungguSurvey', label: 'View Customer Appointment Menunggu Survey', columns: columns])		
	}
	
	def menungguSurveyDatatablesList() {
		render viewAppointmentService.menungguSurveyDatatablesList(params) as JSON
	}
	
	def menungguSPK() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'alamatCustomer', label: 'Alamat Customer', width: '300px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string']
			]
		render(view:"list",model:[id:'menungguSPK', label: 'View Customer Appointment Menunggu SPK', columns: columns])		
	}
	
	def menungguSPKDatatablesList() {
		render viewAppointmentService.menungguSPKDatatablesList(params) as JSON
	}
	
	def followUpAppointment() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '40px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
                [field: 'jamAppointment', label: 'Jam Appointment', width: '120px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string'],
			[field: 'tipeCustomer', label: 'Tipe Customer', width: '200px',type:'string'],
			[field: 'bookingFee', label: 'Booking Fee', width: '200px',type:'numeric'],
			[field: 'parts', label: 'Parts', width: '200px',type:'string']
			]

		render(view:"listFollowUp",model:[id:'followUpAppointment', label: 'View Customer Follow Up Appointment', columns: columns])		
	}
	
	def sendSMSFollowUp() {
        def res = [:]
        Date today = new Date()
        def message = params.formatSMS.replaceAll("<WorkshopTAM>", session.userCompanyDealer.m011NamaWorkshop)//.replaceAll("<NamaCustomer>", hc.t182NamaDepan + " " + t182NamaBelakang)
        def hari = ""
        switch(today.day){
            case 0:
                hari = "Minggu"
                break
            case 1:
                hari = "Senin"
                break
            case 2:
                hari = "Selasa"
                break
            case 3:
                hari = "Rabu"
                break
            case 4:
                hari = "Kamis"
                break
            case 5:
                hari = "Jumat"
                break
            case 6:
                hari = "Sabtu"
                break
            default:
                hari = ""
                break
        }
        message = message.replaceAll("<NamaHariIni>", hari)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        message = message.replaceAll("<TanggalHariIni>", today.format(dateFormat))
        String[] customers = (params.customers as String).split(",")
        customers.each {
            String cleanId = it.replaceAll("[^a-zA-Z0-9\\s]", "")
            HistoryCustomer hc = HistoryCustomer.get(cleanId as long)
            def dest = hc.t182NoHp
            message = message.replaceAll("<NamaCustomer>", hc.t182NamaDepan + " " + hc.t182NamaBelakang)
            message = message.replaceAll("<UsiaCustomer>", new Integer(today.year - hc.t182TglLahir.year).toString())
            try {
                smsService.queueSms(dest, message)
            } catch (e) {
                res.status = e.message
            }
        }
        res.status = 'ok'

        render res as JSON
	}
	
	def formatSMS() {
		def res = [:]
		def ret = Retention.get(params.id)
		if(ret){
			res.status = 'ok'
			res.formatSMS = ret.m207FormatSms
		} else {
			res.status = 'nok'
		}
       render res as JSON
	}
	
	def followUpAppointmentDatatablesList() {
		render viewAppointmentService.followUpAppointmentDatatablesList(params) as JSON
	}
	
	def perluReschedule() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string'],
			[field: 'tipeCustomer', label: 'Tipe Customer', width: '200px',type:'string'],
			[field: 'bookingFee', label: 'Booking Fee', width: '200px',type:'numeric'],
			[field: 'parts', label: 'Parts', width: '200px',type:'string']
			]
		render(view:"list",model:[id:'perluReschedule', label: 'View Customer Perlu Reschedule', columns: columns])		
	}
	
	def perluRescheduleDatatablesList() {
		render viewAppointmentService.perluRescheduleDatatablesList(params) as JSON
	}
	
	def revisiJanjiPenyerahan() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px',type:'seq'],
			[field: 'noAppointment', label: 'No Appointment', width: '100px',type:'string'],
			[field: 'tglAppointment', label: 'Tanggal Appointment', width: '200px',type:'date'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px',type:'string'],
			[field: 'namaBaseModel', label: 'Nama Base Model', width: '200px',type:'string'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px',type:'string'],
			[field: 'telpCustomer', label: 'Telp Customer', width: '200px',type:'string'],
			[field: 'tipeCustomer', label: 'Tipe Customer', width: '200px',type:'string'],
			[field: 'bookingFee', label: 'Booking Fee', width: '200px',type:'numeric'],
			[field: 'parts', label: 'Parts', width: '200px',type:'string']
			]
		render(view:"list",model:[id:'revisiJanjiPenyerahan', label: 'View Customer Revisi Janji Penyerahan', columns: columns])		
	}
	
	def revisiJanjiPenyerahanDatatablesList() {
		render viewAppointmentService.revisiJanjiPenyerahanDatatablesList(params) as JSON
	}
}
