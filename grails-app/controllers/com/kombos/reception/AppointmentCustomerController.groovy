package com.kombos.reception

import com.kombos.administrasi.FullModelVinCode
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerSurvey
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.MappingCustVehicle
import com.kombos.maintable.PeranCustomer
import grails.converters.JSON
import org.apache.commons.codec.binary.Base64
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.commons.CommonsMultipartFile

class AppointmentCustomerController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    def historyCustomerService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        [noPolisiDepan: params.noPolisiDepan, noPolisiTengah: params.noPolisiTengah, noPolisiBelakang: params.noPolisiBelakang]
    }

    def uploadImage() {
        if (params.myFile) {
            def fileName
            def inputStream
            if (params.myFile instanceof CommonsMultipartFile) {
                fileName = params.myFile?.originalFilename
                inputStream = params.myFile.getInputStream()
            } else {
                fileName = params.myFile
                inputStream = request.getInputStream()
            }

            fileName = fileName.replaceAll(" ", "_")

            File storedFile = new File("/opt/${fileName}")
            storedFile.parentFile.mkdirs()
            storedFile.append(inputStream)

            session.customerFoto = storedFile?.absolutePath

            render '<img width=\"200px\" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(storedFile.bytes), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }


    def lihatGambar() {
        def historyCustomer = HistoryCustomer.read(params.id)
        if (params.id && historyCustomer) {
            render '<img width=\"200px\" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(historyCustomer.t182Foto), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }

    def surveyListDatatablesList() {
        def historyCustomer = HistoryCustomer.read(params.historyCustomerId)
        def ret

        def c = CustomerSurvey.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("historyCustomer", historyCustomer)
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    tanggal: it?.t104TglAwal?.format("dd-MM-yyyy"),
                    feedback: it?.t104Text
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def customerCarsDatatablesList() {

        def historyCustomer = HistoryCustomer.read(params.historyCustomerId)
        def ret

        def c = MappingCustVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("customer", historyCustomer?.customer)
            order("dateCreated","desc");
        }

        def rows = []

        results.each {
            def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicle(it.customerVehicle)
            rows << [
                    id: it.id,
                    pemilik: ((historyCustomer?.t182NamaBelakang) + " " + (historyCustomer?.t182NamaBelakang)),
                    nopol: ((historyCustomerVehicle?.kodeKotaNoPol?.m116NamaKota) + " " + (historyCustomerVehicle?.t183NoPolTengah) + " " + (historyCustomerVehicle?.t183NoPolBelakang)),
                    model: (historyCustomerVehicle?.fullModelCode?.modelName)
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }


    def customerListDatatablesList() {

        def historyCustomer = HistoryCustomer.read(params.historyCustomerId)
        def customerVehicles = MappingCustVehicle.withCriteria {
            projections {
                groupProperty("customerVehicle")
            }
            eq("customer", historyCustomer?.customer)
        }

        def ret

        def c = MappingCustVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            customerVehicles.size() == 0 ? sqlRestriction("1!=1") : inList("customerVehicle", customerVehicles)
        }

        def rows = []

        results.each { mapping ->
            def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicle(mapping.customerVehicle)
            def historyCustomers = HistoryCustomer.findAllByCustomer(mapping.customer)

            historyCustomers.each { customer ->
                rows << [
                        id: mapping.id + customer.id,
                        nopol: ((historyCustomerVehicle?.kodeKotaNoPol?.m116NamaKota) + " " + (historyCustomerVehicle?.t183NoPolTengah) + " " + (historyCustomerVehicle?.t183NoPolBelakang)),
                        customer: ((customer?.t182NamaBelakang) + " " + (customer?.t182NamaBelakang)),
                        peran: (customer?.peranCustomer?.m115NamaPeranCustomer)
                ]
            }


        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }


    def historyKepemilikanMobilDatatablesList() {
        def historyCustomer = HistoryCustomer.read(params.historyCustomerId)
        def customerVehicles = MappingCustVehicle.withCriteria {
            projections {
                groupProperty("customerVehicle")
            }
            eq("customer", historyCustomer?.customer)
        }

        def ret

        def c = MappingCustVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            customerVehicles.size() == 0 ? sqlRestriction("1!=1") : inList("customerVehicle", customerVehicles)
        }

        def rows = []

        def perans = [PeranCustomer.findByM115NamaPeranCustomerIlike("Pemilik"), PeranCustomer.findByM115NamaPeranCustomerIlike("Pemilik dan Penanggung Jawab")]

        results.each { mapping ->
            def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicle(mapping.customerVehicle)
            def historyCustomers = HistoryCustomer.findAllByCustomerAndPeranCustomerInList(mapping.customer, perans)

            historyCustomers.each { customer ->
                rows << [
                        id: mapping.id + customer.id,
                        nopol: ((historyCustomerVehicle?.kodeKotaNoPol?.m116NamaKota) + " " + (historyCustomerVehicle?.t183NoPolTengah) + " " + (historyCustomerVehicle?.t183NoPolBelakang)),
                        customer: ((customer?.t182NamaBelakang) + " " + (customer?.t182NamaBelakang)),
                        company: (customer?.company?.namaPerusahaan)
                ]
            }


        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesPeranCustList() {
   		render historyCustomerService.datatablesPeranCustList(params) as JSON
   	}

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params


            def c = HistoryCustomer.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

                createAlias("company", "company", CriteriaSpecification.LEFT_JOIN)
                createAlias("agama", "agama", CriteriaSpecification.LEFT_JOIN)

                if (params."sCriteria_customer") {
                    eq("customer", params."sCriteria_customer")
                }

                if (params."sCriteria_t182ID") {
                    eq("t182ID", params."sCriteria_t182ID")
                }

                if (params."sCriteria_company") {
                    ilike("company.namaPerusahaan", "%" + params."sCriteria_company" + "%")
                }

                if (params."sCriteria_peranCustomer") {
                    eq("peranCustomer", params."sCriteria_peranCustomer")
                }

                if (params."sCriteria_t182GelarD") {
                    ilike("t182GelarD", "%" + (params."sCriteria_t182GelarD" as String) + "%")
                }

                if (params."sCriteria_t182NamaDepan") {
                    ilike("t182NamaDepan", "%" + (params."sCriteria_t182NamaDepan" as String) + "%")
                }

                if (params."sCriteria_t182NamaBelakang") {
                    ilike("t182NamaBelakang", "%" + (params."sCriteria_t182NamaBelakang" as String) + "%")
                }

                if (params."sCriteria_t182GelarB") {
                    ilike("t182GelarB", "%" + (params."sCriteria_t182GelarB" as String) + "%")
                }

                if (params."sCriteria_t182JenisKelamin") {
                    ilike("t182JenisKelamin", "%" + (params."sCriteria_t182JenisKelamin" as String) + "%")
                }

                if (params."sCriteria_t182TglLahir") {
                    ge("t182TglLahir", params."sCriteria_t182TglLahir")
                    lt("t182TglLahir", params."sCriteria_t182TglLahir" + 1)
                }

                if (params."sCriteria_jenisCustomer") {
                    ilike("jenisCustomer", "%" + (params."sCriteria_jenisCustomer" as String) + "%")
                }

                if (params."sCriteria_t182NPWP") {
                    ilike("t182NPWP", "%" + (params."sCriteria_t182NPWP" as String) + "%")
                }

                if (params."sCriteria_t182NoFakturPajakStd") {
                    ilike("t182NoFakturPajakStd", "%" + (params."sCriteria_t182NoFakturPajakStd" as String) + "%")
                }

                if (params."sCriteria_jenisIdCard") {
                    eq("jenisIdCard", params."sCriteria_jenisIdCard")
                }

                if (params."sCriteria_t182NomorIDCard") {
                    ilike("t182NomorIDCard", "%" + (params."sCriteria_t182NomorIDCard" as String) + "%")
                }

                if (params."sCriteria_agama") {
                    ilike("agama.m061NamaAgama", "%" + params."sCriteria_agama" + "%")
                }

                if (params."sCriteria_nikah") {
                    eq("nikah", params."sCriteria_nikah")
                }

                if (params."sCriteria_t182NoTelpRumah") {
                    ilike("t182NoTelpRumah", "%" + (params."sCriteria_t182NoTelpRumah" as String) + "%")
                }

                if (params."sCriteria_t182NoTelpKantor") {
                    ilike("t182NoTelpKantor", "%" + (params."sCriteria_t182NoTelpKantor" as String) + "%")
                }

                if (params."sCriteria_t182NoFax") {
                    ilike("t182NoFax", "%" + (params."sCriteria_t182NoFax" as String) + "%")
                }

                if (params."sCriteria_t182NoHp") {
                    ilike("t182NoHp", "%" + (params."sCriteria_t182NoHp" as String) + "%")
                }

                if (params."sCriteria_t182Email") {
                    ilike("t182Email", "%" + (params."sCriteria_t182Email" as String) + "%")
                }

                if (params."sCriteria_t182StaTerimaMRS") {
                    eq("t182StaTerimaMRS", params."sCriteria_t182StaTerimaMRS")
                }

                if (params."sCriteria_t182Alamat") {
                    ilike("t182Alamat", "%" + (params."sCriteria_t182Alamat" as String) + "%")
                }

                if (params."sCriteria_t182RT") {
                    ilike("t182RT", "%" + (params."sCriteria_t182RT" as String) + "%")
                }

                if (params."sCriteria_t182RW") {
                    ilike("t182RW", "%" + (params."sCriteria_t182RW" as String) + "%")
                }

                if (params."sCriteria_kelurahan") {
                    eq("kelurahan", params."sCriteria_kelurahan")
                }

                if (params."sCriteria_kecamatan") {
                    eq("kecamatan", params."sCriteria_kecamatan")
                }

                if (params."sCriteria_kabKota") {
                    eq("kabKota", params."sCriteria_kabKota")
                }

                if (params."sCriteria_provinsi") {
                    eq("provinsi", params."sCriteria_provinsi")
                }

                if (params."sCriteria_t182KodePos") {
                    ilike("t182KodePos", "%" + (params."sCriteria_t182KodePos" as String) + "%")
                }

                if (params."sCriteria_t182Foto") {
                    eq("t182Foto", params."sCriteria_t182Foto")
                }

                if (params."sCriteria_t182AlamatNPWP") {
                    ilike("t182AlamatNPWP", "%" + (params."sCriteria_t182AlamatNPWP" as String) + "%")
                }

                if (params."sCriteria_t182RTNPWP") {
                    ilike("t182RTNPWP", "%" + (params."sCriteria_t182RTNPWP" as String) + "%")
                }

                if (params."sCriteria_t182RWNPWP") {
                    ilike("t182RWNPWP", "%" + (params."sCriteria_t182RWNPWP" as String) + "%")
                }

                if (params."sCriteria_kelurahan2") {
                    eq("kelurahan2", params."sCriteria_kelurahan2")
                }

                if (params."sCriteria_kecamatan2") {
                    eq("kecamatan2", params."sCriteria_kecamatan2")
                }

                if (params."sCriteria_kabKota2") {
                    eq("kabKota2", params."sCriteria_kabKota2")
                }

                if (params."sCriteria_provinsi2") {
                    eq("provinsi2", params."sCriteria_provinsi2")
                }

                if (params."sCriteria_t182KodePosNPWP") {
                    ilike("t182KodePosNPWP", "%" + (params."sCriteria_t182KodePosNPWP" as String) + "%")
                }

                if (params."sCriteria_t182StaCompany") {
                    ilike("t182StaCompany", "%" + (params."sCriteria_t182StaCompany" as String) + "%")
                }

                if (params."sCriteria_t182HMinusKirimSMS") {
                    eq("t182HMinusKirimSMS", params."sCriteria_t182HMinusKirimSMS")
                }

                if (params."sCriteria_t182WebUserName") {
                    ilike("t182WebUserName", "%" + (params."sCriteria_t182WebUserName" as String) + "%")
                }

                if (params."sCriteria_t182WebPassword") {
                    ilike("t182WebPassword", "%" + (params."sCriteria_t182WebPassword" as String) + "%")
                }

                if (params."sCriteria_t182Ket") {
                    ilike("t182Ket", "%" + (params."sCriteria_t182Ket" as String) + "%")
                }

                if (params."sCriteria_t182xNamaUser") {
                    ilike("t182xNamaUser", "%" + (params."sCriteria_t182xNamaUser" as String) + "%")
                }

                if (params."sCriteria_t182xNamaDivisi") {
                    ilike("t182xNamaDivisi", "%" + (params."sCriteria_t182xNamaDivisi" as String) + "%")
                }

                if (params."sCriteria_t182TglTransaksi") {
                    ge("t182TglTransaksi", params."sCriteria_t182TglTransaksi")
                    lt("t182TglTransaksi", params."sCriteria_t182TglTransaksi" + 1)
                }


                if (params."sNoPolisiDepan" && params."sNoPolisiTengah" && params."sNoPolisiBelakang")  {
                    customer{
                        mappingCustVehicles{
                            customerVehicle{
                                histories{
                                    kodeKotaNoPol{
                                        ilike("m116ID", params."sNoPolisiDepan")
                                    }
                                    ilike("t183NoPolTengah", params."sNoPolisiTengah")
                                    ilike("t183NoPolBelakang", params."sNoPolisiBelakang")
                                }
                            }
                        }
                    }
//                    customer{
//                        'in'("id", cs)
//                    }
                }

                switch (sortProperty) {
                    default:
                        order(sortProperty, sortDir)
                        break;
                }
            }

            def rows = []

            results.each {
                rows << [

                        id              : it.id,


                        t182NamaDepan   : it.t182NamaDepan?.toString(),

                        t182NamaBelakang: it.t182NamaBelakang?.toString(),

                        t182Alamat      : it.t182Alamat?.toString(),

                        t182NoTelpRumah : it.t182NoTelpRumah?.toString(),

                        t182NoTelpKantor: it.t182NoTelpKantor?.toString(),


                        t182NoHp        : it.t182NoHp?.toString(),


                        company         : it.company?.toString()

                ]
            }

            ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

            render ret as JSON
//        }
    }

    def create() {
        def historyCustomer = new HistoryCustomer(params)
        historyCustomer.t182ID = HistoryCustomer.generateCode()
        [historyCustomerInstance: historyCustomer]
    }

    def save() {
        params.dateCreted = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
         def historyCustomerInstance = new HistoryCustomer(params)

        if (session.customerFoto) {
            File fileImage = new File(session?.customerFoto?.toString())
            historyCustomerInstance.t182Foto = fileImage.bytes
            session.customerFoto = null
        }

        historyCustomerInstance.t182ID = generateCodeService.codeGenerateSequence("T182_T102_ID",session.userCompanyDealer)//HistoryCustomer.generateCode()

        if (!historyCustomerInstance.save(flush: true)) {
            render(view: "create", model: [historyCustomerInstance: historyCustomerInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), historyCustomerInstance.id])
        redirect(action: "show", id: historyCustomerInstance.id)
    }

    def show(Long id) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }

        [historyCustomerInstance: historyCustomerInstance]
    }

    def edit(Long id) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        println("historyCustomerInstance=" + historyCustomerInstance)
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }

        def c = HistoryCustomerVehicle.createCriteria()
        def historyCustomerVehicles = c.list() {
//            customerVehicle{
//                mappingCustVehicles{
//                    eq("customer", historyCustomerInstance?.customer)
//                }
//            }
//            kodeKotaNoPol{
//                ilike("m116ID", params."noPolisiDepan")
//            }
//            ilike("t183NoPolTengah", params."noPolisiTengah")
//            ilike("t183NoPolBelakang", params."noPolisiBelakang")
        }
        def  HistoryCustomerVehicle historyCustomerVehicleInstance = new HistoryCustomerVehicle()
        def fullModelVinCode = new FullModelVinCode();
        if(historyCustomerVehicles.size() > 0) {
            historyCustomerVehicleInstance = historyCustomerVehicles.last()
            fullModelVinCode = FullModelVinCode.findByFullModelCodeAndStaDel(historyCustomerVehicleInstance?.fullModelCode, "0")
        }
        println("historyCustomerVehicleInstance=" + historyCustomerVehicleInstance)
        println("fullModelVinCode=" + fullModelVinCode)
        def appointment
        if (historyCustomerInstance.id && historyCustomerVehicleInstance.id) {
            appointment = Appointment.findByHistoryCustomerAndHistoryCustomerVehicleAndStaDel(historyCustomerInstance, historyCustomerVehicleInstance, "0");
        }
        println("appointment=" + appointment)
        [appointmentInstance: appointment, historyCustomerVehicleInstance: historyCustomerVehicleInstance, fullModelVinCode:fullModelVinCode, historyCustomerInstance: historyCustomerInstance, noPolisiDepan: params.noPolisiDepan, noPolisiTengah: params.noPolisiTengah, noPolisiBelakang: params.noPolisiBelakang]
    }

    def update(Long id, Long version) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        println("historyCustomerInstance=" + historyCustomerInstance)
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (historyCustomerInstance.version > version) {

                historyCustomerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'historyCustomer.label', default: 'HistoryCustomer')] as Object[],
                        "Another user has updated this HistoryCustomer while you were editing")
                render(view: "edit", model: [historyCustomerInstance: historyCustomerInstance])
                return
            }
        }

        historyCustomerInstance.properties = params


        if (session.customerFoto) {
            File fileImage = new File(session?.customerFoto?.toString())
            historyCustomerInstance.t182Foto = fileImage.bytes
            session.customerFoto = null
        }

        historyCustomerInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString();
        historyCustomerInstance.lastUpdated = datatablesUtilService?.syncTime()
        historyCustomerInstance.lastUpdProcess = "UPDATE"
        if (!historyCustomerInstance.save(flush: true)) {
            println("inside if")
            render(view: "edit", model: [historyCustomerInstance: historyCustomerInstance])
            return
        }
        println("will goes to show")
        flash.message = message(code: 'default.updated.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), historyCustomerInstance.t182NamaDepan + " " + historyCustomerInstance.t182NamaBelakang])
        //redirect(action: "show", id: historyCustomerInstance.id)
        redirect(action: "list")
    }

    def delete(Long id) {
        def historyCustomerInstance = HistoryCustomer.get(id)
        if (!historyCustomerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
            return
        }

        try {
            historyCustomerInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'historyCustomer.label', default: 'HistoryCustomer'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(HistoryCustomer, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        try {
            datatablesUtilService.updateField(HistoryCustomer, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
