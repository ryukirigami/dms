package com.kombos.reception

import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class BoardAntrianController {

    def index() {
    }

    def datatablesList() {
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def angka = params?.indMulai?.toInteger();
        session.exportParams=params
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def c = CustomerIn.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: angka) {
            eq("companyDealer",session?.userCompanyDealer);
            eq("staDel","0");
            lt("dateCreated",dateAkhir);
            ge("dateCreated",dateAwal);
            tujuanKedatangan{
                or{
                    ilike("m400Tujuan","%GR%")
                    ilike("m400Tujuan","%BP%")
                }
            }
            or{
                isNull("t400StaReception");
                not {
                    or{
                        eq("t400StaReception","2");
                        eq("t400StaReception","3");
                    }
                }
            }
            not{
                or{
                    ilike("t400NomorAntrian","-");
                    ilike("t400NomorAntrian","% %");
                }
            }
            order("t400NomorAntrian");
        }

        if(results?.size()<1){
            angka = 0
            results = CustomerIn.createCriteria().list (max: params.iDisplayLength as int, offset: angka) {
                eq("companyDealer",session?.userCompanyDealer);
                eq("staDel","0");
                lt("dateCreated",dateAkhir);
                ge("dateCreated",dateAwal);
                tujuanKedatangan{
                    or{
                        ilike("m400Tujuan","%GR%")
                        ilike("m400Tujuan","%BP%")
                    }
                }
                or{
                    isNull("t400StaReception");
                    not {
                        or{
                            eq("t400StaReception","2");
                            eq("t400StaReception","3");
                        }
                    }
                }
                not{
                    or{
                        ilike("t400NomorAntrian","-");
                        ilike("t400NomorAntrian","% %");
                    }
                }
                order("t400NomorAntrian");
            }
        }

        angka+=15

        def rows = []
        results.each {
            rows << [

                    id: it.id,

                    noAntri: it.t400NomorAntrian,

                    noPol: it?.customerVehicle ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol : it?.t400noPol,

                    noloket: it?.loket?.m404NoLoket,

                    ket: it?.tujuanKedatangan?.m400Tujuan ? (it?.tujuanKedatangan?.m400Tujuan + (it?.t400Keterangan? " - " + it?.t400Keterangan : "") ) :"-"
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows,indMulai : angka]

        render ret as JSON
    }

}
