package com.kombos.reception

import com.kombos.administrasi.*
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.customerprofile.SPK
import com.kombos.maintable.*
import com.kombos.parts.*
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import com.kombos.woinformation.Prediagnosis
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification

import java.text.DecimalFormat

class EditJobPartsController {
    def editJobPartsService
    def datatablesUtilService

    def index() {

        redirect(action: "list", params: params)
    }

    def partList() {
        String idTable = new Date().format("yyyyMMddhhmmss")+params.idJob
        [idReception : params.idReception,idJob : params.idJob, idTable: idTable]
    }

    def billtoanddiscount() {
        def recep = new Reception()
        if(params.noWo){
            recep = Reception.findByT401NoWOAndStaDel(params.noWo,"0")
        }
        [receptionInstance : recep]
    }

    def list(){
        String noWo = "";
        if(params.nowo){
            noWo = params.nowo;
        }
        [noWO : noWo]
    }

    def deleteJobPart(){
        def hasil = "not"
        def status = "0"
        def listPartDelete = JSON.parse(params.listPartDelete)
        def listJobDelete = JSON.parse(params.listJobDelete)
        listPartDelete.each {
            status = "0"
            def partI = PartsRCP.get(it as Long)
            def pickinglist = PickingSlip.findByReceptionAndStaDel(partI.reception,'0')
            if(pickinglist){
                if(PickingSlipDetail.findByGoodsAndStaDelAndPickingSlip(partI.goods,'0',pickinglist)){
                    status = "1"
                }
            }
            if(status=="0"){
                partI.staDel = "1"
                partI.setLastUpdProcess("DELETE");
                partI.t403TglJamTambahKurang = null
                partI.t403StaApproveTambahKurang = null
                partI.t403StaTambahKurang = null
                partI.save(flush: true)
                hasil = "ok"
            }
        }

        listJobDelete.each {
            status = "0"
            def job = JobRCP.get(it as Long)
            def parts= PartsRCP.findAllByReceptionAndOperationAndStaDel(job.reception,job.operation,'0')
            if(parts.size()>0){
                parts.each {
                    status = "0"
                    def pickinglist = PickingSlip?.findByReceptionAndStaDel(job.reception,'0')
                    if(pickinglist){
                        if(PickingSlipDetail.findByGoodsAndStaDelAndPickingSlip(it.goods,'0',pickinglist)){
                            status = "1"
                        }
                    }
                    if(status=="0"){
                        def partI = PartsRCP.get(it as Long)
                        partI.staDel = "1"
                        partI.setLastUpdProcess("DELETE");
                        partI.t403TglJamTambahKurang = null
                        partI.t403StaApproveTambahKurang = null
                        partI.t403StaTambahKurang = null
                        partI.save(flush: true)

                        job.staDel="1";
                        job.setLastUpdProcess("DELETE");
                        job.save(flush: true)
                        hasil = "ok"
                    }
                }
            }else{
                if(status=="0"){
                    job.staDel="1";
                    job.setLastUpdProcess("DELETE");
                    job.save(flush: true)
                    hasil = "ok"
                }
            }
        }
        render hasil
    }

    def getReception(){
        params.companyDealer = session?.userCompanyDealer
        def result = editJobPartsService.getReception(params);
        render result as JSON
    }

    def ganti(){
        def part = PartsRCP.get(params.idChange.toLong())
        part?.t403StaIntExt = params.value
        part.save(flush: true)
        render "oke"
    }

    def datatablesGoodsList() {
        render editJobPartsService.datatablesGoodsList(params,session) as JSON
    }

    def datatablesJobsList() {
        render editJobPartsService.datatablesJobsList(params,session) as JSON
    }

    def jobnPartsDatatablesList() {
        render editJobPartsService.jobnPartsDatatablesList(params,session) as JSON
    }

    def jobnPartsOrderDatatablesList() {
        render editJobPartsService.jobnPartsOrderDatatablesList(params,session) as JSON
    }

    def partDatatablesList() {
        render editJobPartsService.partDatatablesList(params) as JSON
    }
    def workDatatablesList() {
        render editJobPartsService.workDatatablesList(params) as JSON
    }

    def workList() {
        [idJob : params.idJob, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def orderPart() {
        render editJobPartsService.orderPart(params, session.userCompanyDealer) as JSON
    }

    def getNewReception(){
        params?.companyDealer = session?.userCompanyDealer
        def result = editJobPartsService.getNewReception(params);
        render result as JSON
    }

    def savePreDiagnose(def params){
        def result = [:]
        def prediagnosis = editJobPartsService.savePreDiagnose(params)

        if(prediagnosis != null){
            result = prediagnosis
        }

        render result as JSON
    }

    def printPrediagnosis(){
        def id = params.id

        def prediagnosisInstance = Prediagnosis.get(id)
        def receptionInstance = Reception.get(prediagnosisInstance.reception.id)

        def parameters = [
                SUBREPORT_DIR : servletContext.getRealPath('/reports') + '/',
                receptionId : receptionInstance.id,
                t406KeluhanCust : prediagnosisInstance.t406KeluhanCust,
                t406StaGejalaHariIni : prediagnosisInstance.t406StaGejalaHariIni
        ]

        def idx = 1
        def pemeriksaanAwalItems = []
        def pemeriksaanAwalList = DiagnosisDetail.findAllByReceptionAndPrediagnosisAndT416staAwalUlang(receptionInstance,prediagnosisInstance,'A')
        pemeriksaanAwalList.each {
            pemeriksaanAwalItems << [
                    no : idx,
                    system : it.t416System,
                    dtc : it.t416DTC,
                    desc : it.t416Desc,
                    status : it.t416StatusPCH,
                    freeze : it.t416Freeze
            ]
            idx++
        }

        idx = 1
        def cekUlangItems = []
        def cekUlangList = DiagnosisDetail.findAllByReceptionAndPrediagnosisAndT416staAwalUlang(receptionInstance,prediagnosisInstance,'U')
        cekUlangList.each {
            cekUlangItems << [
                    no : idx,
                    system : it.t416System,
                    dtc : it.t416DTC,
                    desc : it.t416Desc,
                    status : it.t416StatusPCH,
                    freeze : it.t416Freeze
            ]
            idx++
        }

        def subReportData1 = []
        subReportData1 << [
                cekUlangItems : cekUlangItems
        ]

        def subReportData = []
        subReportData << [
                pemeriksaanAwalItems : pemeriksaanAwalItems,
                subReportData1 : subReportData1
        ]

        def reportData = []
        reportData << [
                subReportData : subReportData
        ]

        def reportDef = new JasperReportDef(name:'prediagnose.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )

        def file = File.createTempFile("prediagnose_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }


    def saveJobInput(){
        def result = [success: "1"]

        def id = params.idReception
        def countRow = new Integer(params.countRow)
        def indexRow = 1
        def keluhanRcp = null

        while(indexRow < countRow){
            keluhanRcp = KeluhanRcp.findByReceptionAndT411NamaKeluhan(Reception.get(id),params.get("t411NamaKeluhan" + indexRow))
            if(!keluhanRcp){
                println("INI APA "+params.get("t411NamaKeluhan" + indexRow))
                keluhanRcp = new KeluhanRcp()
                keluhanRcp.reception = Reception.get(id)
                keluhanRcp.companyDealer = session.userCompanyDealer
                keluhanRcp.t411NoUrut = new Double(params.get("t411NoUrut" + indexRow))
                keluhanRcp.t411NamaKeluhan = params.get("t411NamaKeluhan" + indexRow)
                keluhanRcp.t411StaButuhDiagnose = params.get("t411StaButuhDiagnose" + indexRow)
                keluhanRcp.staDel = 0
                keluhanRcp.createdBy = "system"
                keluhanRcp.lastUpdProcess = "INSERT"
                keluhanRcp.dateCreated = datatablesUtilService?.syncTime()
                keluhanRcp.lastUpdated = datatablesUtilService?.syncTime()
                keluhanRcp.save(flush:true)
            }
            indexRow++
        }

        def countRowJob = new Integer(params.countRowJob)
        indexRow = 1
        def jobRCP
        def receptionInstance = Reception.get(id)
        if(params?.kmSekarang){
            receptionInstance?.t401KmSaatIni = params.kmSekarang.toLong()
        }
        receptionInstance?.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance?.save(flush: true)
        def histCV = receptionInstance?.historyCustomerVehicle?.customerVehicle

        while(indexRow < countRowJob){
            def operationInstance = Operation.get(params.get("operation" + indexRow))
            jobRCP = JobRCP.findByOperationAndReceptionAndStaDel(operationInstance,receptionInstance,"0");
            if(jobRCP){
                jobRCP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                jobRCP.lastUpdProcess = "UPDATE"
            }else {
                jobRCP = new JobRCP()
                jobRCP.dateCreated = datatablesUtilService?.syncTime()
                jobRCP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                jobRCP.lastUpdProcess = "INSERT"
            }
            if(params.staTambah){
                jobRCP.t402StaTambahKurang = params.staTambah
                jobRCP.t402TglTambahKurang = datatablesUtilService.syncTime()
            }
            jobRCP.lastUpdated = datatablesUtilService?.syncTime()
            if(!jobRCP?.t402Rate || (jobRCP?.t402Rate && jobRCP?.t402Rate==0)){
                def rate = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(operationInstance,histCV?.getCurrentCondition()?.fullModelCode?.baseModel,"0",datatablesUtilService.syncTime(),[sort: 'id',order: 'desc'])
                jobRCP?.t402Rate = (rate?.t113FlatRate?rate?.t113FlatRate:0)
            }

            if(!jobRCP?.t402HargaRp || (jobRCP?.t402HargaRp && jobRCP?.t402HargaRp==0)){
                def nominalValue = 0
                def rate = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(operationInstance,histCV?.getCurrentCondition()?.fullModelCode?.baseModel,"0",datatablesUtilService.syncTime(),[sort: 'id',order: 'desc'])

                    def harga = TarifPerJam.findByKategoriAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(histCV?.getCurrentCondition()?.fullModelCode?.baseModel?.kategoriKendaraan,"0",session?.userCompanyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
                    def nominal1 = harga && rate ? harga?.t152TarifPerjamBP * rate?.t113FlatRate : 0
                    def nominal2 = harga && rate ? harga?.t152TarifPerjamSB * rate?.t113FlatRate : 0
                    def nominal3 = harga && rate ? harga?.t152TarifPerjamGR * rate?.t113FlatRate : 0
                    def nominalTam = harga && rate ? harga?.t152TarifPerjamDealer * rate?.t113FlatRate : 0
                    def nominal4 = harga?.t152TarifPerjamPDS && rate ? harga?.t152TarifPerjamPDS * rate?.t113FlatRate : 0
                    def nominal5 = harga?.t152TarifPerjamSBI && rate ? harga?.t152TarifPerjamSBI * rate?.t113FlatRate : 0
                    def nominal6 = harga?.t152TarifPerjamSPO && rate ? harga?.t152TarifPerjamSPO * rate?.t113FlatRate : 0
                    if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("TWC")
                            || operationInstance?.kategoriJob?.m055KategoriJob?.toString().toUpperCase().contains("WARRANTY")){
                        def hargaTwc = TarifTWC.createCriteria().get {
                            eq("staDel","0")
                            eq("companyDealer",session?.userCompanyDealer)
                            eq("kategoriKendaraan",receptionInstance?.historyCustomerVehicle?.fullModelCode?.kategoriKendaraan)
                            order("t113aTMT","desc")
                            maxResults(1);
                        }
                        if(hargaTwc){
                            def nominalTemp = hargaTwc?.t113aTarifPerjamTWC
                            nominalValue = rate ? (nominalTemp * rate?.t113FlatRate) : 0
                        }
                    } else if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("BP")){
                            nominalValue = nominal1
                    }else if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("SBE")){
                        nominalValue = nominal2
                        String jobTam = "SB10K,SB20K,SB30K,SB40K,SB50K"
                        if(jobTam.toString().contains(operationInstance.m053Id)){
                            nominalValue = nominalTam
                        }
                    }else if(operationInstance?.kategoriJob?.m055KategoriJob?.toString().toUpperCase().contains("PDS")
                            && operationInstance?.m053Id?.toUpperCase().contains("CUCI")){
                        nominalValue = nominal4
                    }else if(operationInstance?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SBI")){
                        nominalValue = nominal5
                    }
                    else{
                        if(operationInstance?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")){
                            if(harga?.t152TarifPerjamSPO != null && harga?.t152TarifPerjamSPO != 0){
                                nominalValue = nominal6
                            }else{
                                nominalValue = nominal3
                            }
                        }else{
                            nominalValue = nominal3
                        }
                    }
                jobRCP?.t402HargaRp = (nominalValue?nominalValue:0)
                jobRCP?.t402TotalRp = (nominalValue?nominalValue:0)
                jobRCP?.t402Rate = (rate?.t113FlatRate?rate?.t113FlatRate:0)
            }else{
                def nominalValue = 0
                def rate = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(operationInstance,histCV?.getCurrentCondition()?.fullModelCode?.baseModel,"0",datatablesUtilService.syncTime(),[sort: 'id',order: 'desc'])
                if(receptionInstance?.staTwc && receptionInstance?.staTwc=="1"){
                    def hargaTwc = TarifTWC.createCriteria().get {
                        eq("staDel","0")
                        eq("companyDealer",session?.userCompanyDealer)
                        eq("kategoriKendaraan",receptionInstance?.historyCustomerVehicle?.fullModelCode?.kategoriKendaraan)
                        order("t113aTMT","desc")
                        maxResults(1);
                    }
                    if(hargaTwc){
                        def nominalTemp = hargaTwc?.t113aTarifPerjamTWC
                        nominalValue = rate ? (nominalTemp * rate?.t113FlatRate) : 0
                    }
                }else{
                    def harga = TarifPerJam.findByKategoriAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(histCV?.getCurrentCondition()?.fullModelCode?.baseModel?.kategoriKendaraan,"0",session?.userCompanyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
                    def nominal1 = harga && rate ? harga?.t152TarifPerjamBP * rate?.t113FlatRate : 0
                    def nominal2 = harga && rate ? harga?.t152TarifPerjamSB * rate?.t113FlatRate : 0
                    def nominal3 = harga && rate ? harga?.t152TarifPerjamGR * rate?.t113FlatRate : 0
                    def nominal4 = harga?.t152TarifPerjamPDS && rate ? harga?.t152TarifPerjamPDS * rate?.t113FlatRate : 0
                    def nominal5 = harga?.t152TarifPerjamSBI && rate ? harga?.t152TarifPerjamSBI * rate?.t113FlatRate : 0
                    def nominal6 = harga?.t152TarifPerjamSPO && rate ? harga?.t152TarifPerjamSPO * rate?.t113FlatRate : 0
                    if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("BP")){
                        nominalValue = nominal1
                    }else if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("SBE")){
                        nominalValue = nominal2
                    }else if(operationInstance?.m053Id?.equalsIgnoreCase("CuciPDS")){
                        nominalValue = nominal4
                    }else if(operationInstance?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SBI")){
                        nominalValue = nominal5
                    }
                    else{
                        if(operationInstance?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")){
                            if(harga?.t152TarifPerjamSPO != null && harga?.t152TarifPerjamSPO != 0){
                                nominalValue = nominal6
                            }else{
                                nominalValue = nominal3
                            }
                        }else{
                            nominalValue = nominal3
                        }
                    }

                }
                jobRCP?.t402HargaRp = (nominalValue?nominalValue:0)
                jobRCP?.t402TotalRp = (nominalValue?nominalValue:0)
                jobRCP?.t402Rate = (rate?.t113FlatRate?rate?.t113FlatRate:0)
            }
            jobRCP.companyDealer = session.userCompanyDealer
            jobRCP.reception = receptionInstance
            jobRCP.t402StaIntExt = "i"
            jobRCP.operation = operationInstance
            jobRCP.t402NoKeluhan = params.get("t702NoUrutKeluhan" + indexRow)
            jobRCP.statusWarranty = StatusWarranty.get(params.statusWarranty.toLong())
            if(params.workshopSumber){
                jobRCP.companyDealerIDSumber = CompanyDealer.get(params.workshopSumber.toLong())
            }
            if(params.workshopTujuan){
                jobRCP.companyDealerIDTujuan = CompanyDealer.get(params.workshopTujuan.toLong())
            }
            jobRCP.staDel = 0

            jobRCP.save(flush:true)
            jobRCP.errors.each {println "ADD JOB - EDIT JOB AND PARTS - "+it}
            indexRow++
        }

        render result as JSON
    }

    def savePartInput(){
        def result = [success: "1"]
        def id = params.idReception
        def job = JobRCP.get(params.idJob.toLong())
        def countRow = new Integer(params.countRowPart)
        int indexRow = 1;
        def partRCP;
        DecimalFormat df = new DecimalFormat("#.##")

        while(indexRow < countRow){
            def goodsInstance = Goods.get(params.get("good" + indexRow))
            def qty = params.get("qty" + indexRow).toDouble();
            def receptionInstance = Reception.get(id)
            partRCP = PartsRCP.findByReceptionAndOperationAndGoodsAndStaDel(receptionInstance,job.operation,goodsInstance,"0");
            try {
                def parttt = PartsRCP.findByReceptionAndGoodsAndStaDel(receptionInstance,goodsInstance,"1");
                parttt.reception = null
                parttt.save(flush: true)
            }catch (Exception e){

            }
            if(!partRCP){
                partRCP = new PartsRCP()
                partRCP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                partRCP.lastUpdProcess = "INSERT"
                partRCP.dateCreated = datatablesUtilService?.syncTime()
            }else{
                partRCP.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                partRCP.lastUpdProcess = "UPDATE"
            }
            partRCP.lastUpdated = datatablesUtilService?.syncTime()
            if(params.staTambah){
                partRCP.t403StaTambahKurang = params.staTambah
                partRCP.t403TglJamTambahKurang = datatablesUtilService.syncTime()
            }
            def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(goodsInstance, "0",new Date(),[sort: "t151TMT",order: "desc"])
            def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                eq("staDel","0");
                eq("companyDealer",session?.userCompanyDealer);
            }
            def mappingRegion = MappingPartRegion.createCriteria().get {
                if(mappingCompRegion?.size()>0){
                    inList("region",mappingCompRegion?.region)
                }else{
                    eq("id",-10000.toLong())
                }
                maxResults(1);
            }
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
            if(mappingRegion){
                try {
                    if(KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("TOYOTA") ||
                            KlasifikasiGoods.findByGoods(ghj.goods).franc?.m117NamaFranc.toUpperCase().contains("OLI")){
                        hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                    }
                }catch (Exception e){

                }
            }
            partRCP.reception = receptionInstance
            partRCP.operation = job.operation
            partRCP.goods = goodsInstance
            partRCP.t403StaIntExt = "i"
            partRCP.t403HargaRp = hargaTanpaPPN
            partRCP.t403Jumlah1 = qty
            partRCP.t403Jumlah2 = qty
            partRCP.t403TotalRp = qty * hargaTanpaPPN
            partRCP.staDel = 0


            partRCP.save(flush: true)
            partRCP.errors.each {println "ADD PARTS - EDIT JOB AND PARTS - "+it}


            indexRow++
        }

        render result as JSON
    }

    def getDataKeluhan(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        String htmlData = ""

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " "+it.t411NoUrut+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t411NamaKeluhan+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t411StaButuhDiagnose+"\n" +
                        " </td>\n" +
                        "</tr>"
            }
        }

        render htmlData as String
    }

    def getDataJob(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        def jobItemsDb = JobInv.findAllByReception(receptionInstance)
        String htmlData = ""

        if(jobItemsDb){
            jobItemsDb.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " "+it.operation.m053Id+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.operation.m053NamaOperation+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t702HargaRp+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t702NoUrutKeluhan+"\n" +
                        " </td>\n" +
                        "</tr>"
            }
        }
        render htmlData as String
    }

    def addPartDatatablesList() {
        render editJobPartsService.addPartDatatablesList(params) as JSON
    }

    def savePart() {
        def result = editJobPartsService.savePart(params)
        render result as JSON
    }

    def saveSummary() {
        def result = editJobPartsService.saveSummary(params)

        render result as JSON
    }

    def getDataJobSummary(){
        def conversi = new Konversi()
        def noWo = params.noWo
        def receptionInstance = Reception.findByT401NoWO(noWo)
        def jobItemsDb = JobRCP.findAllByReceptionAndStaDel(receptionInstance, "0")
        String htmlData = ""

        def subTotal = 0
        def hargaParts = 0
        def no = 1
        def ppn = 0
        def grandTotal = 0
        def bookingFee = 0
        def hargaJob = 0
        def tot = 0
        if(jobItemsDb){
            jobItemsDb.each{
                hargaParts = 0
                def partRcp = PartsRCP.findAllByReceptionAndOperationAndStaDel(receptionInstance,it.operation,"0")
                partRcp.each {
                    hargaParts += it.t403HargaRp?it.t403HargaRp:0
                }
                hargaJob = it.t402HargaRp?it.t402HargaRp:0
                tot = hargaParts + hargaJob
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " "+ (no++) +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ it.operation?.m053NamaOperation +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ conversi.toRupiah(hargaJob) +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ conversi.toRupiah(hargaParts) +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ conversi.toRupiah(tot) +"\n" +
                        " </td>\n" +
                        "</tr>"

                subTotal+= tot
            }

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " \n"+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    "</tr>"

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " SUB TOTAL "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+conversi.toRupiah(subTotal)+"\n" +
                    " </td>\n" +
                    "</tr>"

            ppn = subTotal * (10/100)
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " PPN "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " 10 %\n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+conversi.toRupiah(ppn)+"\n" +
                    " </td>\n" +
                    "</tr>"

            grandTotal = ppn + subTotal
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " GRAND TOTAL "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+conversi.toRupiah(grandTotal)+"\n" +
                    " </td>\n" +
                    "</tr>"

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " \n"+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    "</tr>"

            bookingFee = grandTotal * (25/100)
            String addBF = "",addOR = "";
            if(receptionInstance?.t401StaInvoice){
                addBF = "readonly";
                addOR = "readonly";
            }else{
                addBF = "onblur=\"saveChangeData('bookingfee-"+receptionInstance?.id+"');\" ";
                addOR = "onblur=\"saveChangeData('onrisk-"+receptionInstance?.id+"');\" ";
            }
            def totBF = receptionInstance?.t401DPRp ? receptionInstance?.t401DPRp : 0
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " BOOKING FEE "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " Min. 25 %\n" +
                    " </td>\n" +
                    " <td>\n" +
                    " <input id=\"bookingfee-"+receptionInstance?.id+"\" type=\"text\" "+addBF+" value=\""+totBF+"\">\n" +
                    " </td>\n" +
                    "</tr>";
//            if(receptionInstance?.customerIn?.tujuanKedatangan?.m400Tujuan?.equalsIgnoreCase("BP")){
            def totOR = receptionInstance?.t401OnRisk ? receptionInstance?.t401OnRisk : 0
            htmlData+="<tr>\n" +
                    " <td>\n" +
                    " ON RISK "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " <input id=\"onrisk-"+receptionInstance?.id+"\" type=\"text\" "+addOR+" value=\""+totOR+"\">\n" +
                    " </td>\n" +
                    "</tr>"
//            }
        }
        render htmlData as String
    }

    def getDataKeluhanSummary(){
        def id = params.noWo
        def receptionInstance = Reception.findByT401NoWO(id)
        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        String htmlData = ""

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                htmlData+=	it.t411NoUrut+". "+it.t411NamaKeluhan+"\n"
            }
        }

        render htmlData as String
    }

    def getDataPembayaran(){
        def id = params.noWo
        def receptionInstance = Reception.findByT401NoWO(id)
        def settlementItemsDb = Settlement.findAllByReception(receptionInstance)
        String htmlData = ""

        int idx = 1
        if(settlementItemsDb){
            settlementItemsDb.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " " +(idx++)+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.metodeBayar.toString()+
                        " </td>\n" +
                        " <td style='text-align:right;'>\n" +
                        " " +it.t704JmlBayar+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.t704keterangan+
                        " </td>\n" +
                        "</tr>"
            }

        }

        render htmlData as String
    }

    def datatablesListParts(){
        def rows = []
        def ret = [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]

        render ret as JSON
    }

    def getInputParts(){
        def receptionInstance = Reception.get(params.id)
        def htmlData = ""
        def partsApps = PartsApp.findByReception(receptionInstance)
        if(partsApps){
            partsApps.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " " +it.goods.m111ID+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.goods.m111Nama+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.namaProsesBP+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.t303Jumlah1+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.goods.satuan+
                        " </td>\n" +
                        "</tr>"
            }
        }

        render htmlData as String
    }

    def requestPart() {

    }

    def cancelEditBookingFee() {

    }

    def requestPartDatatablesList() {
        render editJobPartsService.requestPartDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def estimasiDatatablesList() {
        render editJobPartsService.estimasiDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def cancelEditBookingFeeDatatablesList(){
        render editJobPartsService.cancelEditBookingFeeDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def jobKeluhan(){
        render editJobPartsService.jobKeluhan(params) as JSON
    }

    def showEstimasi(){
        def nopol = KodeKotaNoPol.findById(params.kode as Long).m116ID + " " + params.tengah + " " + params.belakang
        def model = HistoryCustomerVehicle.findByFullNoPol(nopol)?.fullModelCode?.baseModel?.m102NamaBaseModel
        def tglEstimasi = new Date().format("dd MMMM yyyy")
        render(view: "_receptionEstimasi", model: [nopol: nopol, tglEstimasi : tglEstimasi, models : model, noWo : params.noWo])
    }
    def approval(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWO(params.noWo)
        def recEdit = Reception.get(rec.id)
        recEdit.alasanCancel = AlasanCancel.findById(params.alasanCancel)
        recEdit.save(flush: true)
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:rec.id as String,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService.syncTime(),
                lastUpdated: datatablesUtilService.syncTime()
        )

        approval.save(flush:true)

        render "ok"
    }

    def approvalRate(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWOAndStaDel(params.noWo,"0")
        def jobRcp = JobRCP.get(params.idJob.toLong())
        def pesan = params.pesan + '\n'
        pesan += " - Job " + jobRcp.operation.m053NamaOperation
        pesan += "  : Rate Menjadi " + params.rate
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:jobRcp?.id?.toString()+"#"+params.rate,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: pesan,
                dateCreated: datatablesUtilService.syncTime(),
                lastUpdated: datatablesUtilService.syncTime()
        )

        approval.save(flush:true)

        render "ok"
    }

    def approvalDiscount(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWO(params.noWo)
        rec.staApprovalDisc = "2"
        rec.save(flush: true)
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:rec.id as String,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService.syncTime(),
                lastUpdated: datatablesUtilService.syncTime()
        )

        approval.save(flush:true)

        render "ok"
    }

    def approvalSPK(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWO(params.noWo)
        rec.t401StaButuhSPKSebelumProd = "1"
        rec.save(flush: true)
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:rec.id as String,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService.syncTime(),
                lastUpdated: datatablesUtilService.syncTime()
        )

        approval.save(flush:true)

        render "ok"
    }

    def approvalKurangiJob(){
        String hasil = "ok";
        def JOB = "\nJOB : "
        def PARTS = "\nPARTS : "
        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWO(params.noWo)
        PickingSlip pickingSlip = PickingSlip.findByReceptionAndStaDel(rec,'0')
        if(pickingSlip){
            hasil = PickingSlipDetail.findAllByPickingSlip(pickingSlip).goods.toArray().toString()
        }else{
            def jsonArray = JSON.parse(params.jobs)
            def jsonArray2 = JSON.parse(params.parts)
            String idJob="";
            def ind=1;
            def lengArray = jsonArray.size()

            jsonArray.each{
                def jobRcp = JobRCP.get(it.toLong());
                jobRcp.t402StaTambahKurang = "1"
                jobRcp.t402StaApproveTambahKurang = "2"
                jobRcp.lastUpdated = datatablesUtilService?.syncTime()
                jobRcp.save(flush: true)
                idJob+=it

                try {
                    JOB += "\n  - " + jobRcp?.operation?.m053NamaOperation
                }catch(Exception e){}

                if(ind<=lengArray){
                    idJob+="-"
                }
            }
            def ind2=1

            if(idJob != ""){
                idJob = idJob.toString().substring(0, idJob.length() - 1)
            }

            jsonArray2.each {
                def id = it.toString().substring(0,it.toString().indexOf("*"))
                def partRCP = PartsRCP.get(id.toLong());
                if(partRCP){
                        partRCP.t403StaTambahKurang = "1"
                        partRCP.t403StaApproveTambahKurang = "2"
                        partRCP.lastUpdated = datatablesUtilService?.syncTime()
                        partRCP.save(flush: true)
                        try {
                            PARTS += "\n  - " + partRCP?.goods?.m111ID + " | " + partRCP?.goods?.m111Nama
                        }catch(Exception e){}
                        if(idJob.length()>0){
                            if(ind2<=jsonArray2.size() ){
                                idJob+="-"
                            }
                        }
                        idJob+=it
                        ind2++
                }
            }
            params.pesan = params.pesan +" "+ JOB +" "+ PARTS

            def approval = new ApprovalT770(
                    companyDealer: session?.userCompanyDealer,
                    t770FK:rec.id.toString()+"#"+idJob,
                    kegiatanApproval: kegiatanApproval,
                    t770NoDokumen: rec?.t401NoWO,
                    t770TglJamSend: datatablesUtilService?.syncTime(),
                    t770Pesan: params.pesan,
                    dateCreated: datatablesUtilService.syncTime(),
                    lastUpdated: datatablesUtilService.syncTime()
            )

            approval.save(flush:true)
            approval.errors.each {println(it)}
        }
        render hasil
    }

    def addPembayaran(){
        def result = [:]
        def setlement = new Settlement()
        setlement.metodeBayar = MetodeBayar.findById(params.metodeBayar as Long)
        setlement.t704keterangan = params.ket
        setlement.t704JmlBayar = params.jumlah as Double
        setlement.staDel = '0'
        setlement.lastUpdProcess = 'insert'
        setlement.t704ID = 2
        setlement.t704TglJamSettlement = datatablesUtilService?.syncTime()
        setlement.reception = Reception.findByT401NoWO(params.noWo)
        setlement.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        setlement.dateCreated = datatablesUtilService.syncTime()
        setlement.lastUpdated = datatablesUtilService.syncTime()
        setlement.save(flush: true)
        setlement.errors.each {
            println it
        }
        render result  as JSON
    }

    def hitungEstimasi(){
        def res = [:]
        def stdWork = StdTimeWorkItems.findAll()
        def waktu = 0, body = 0, asembly = 0, polishing = 0, painting = 0, preparation = 0
        if(params.kerusakan=='Mudah'){
            stdWork.each{
                waktu += it.m041RdanR
            }
        }else if(params.kerusakan=='Medium'){
            stdWork.each{
                waktu += it.m041Replace
            }
        }else{
            stdWork.each{
                waktu += it.m041Overhaul
            }
        }

        //body & assembly
        def gm = GeneralParameter.first()
        body = (waktu * (gm.m000PersenPelepasanPartBP / 100)).intValue()
        asembly = (waktu * (gm.m000PersenPemasanganPartBP / 100)).intValue()

        //perhitungan Waktu
        def pat = PaintingApplicationTime.findById(1).m044NewMultiple //waktu 1
        def cm = ColorMatching.findById(params.colorKlasifikasi as Long).m046StdTime //waktu 2
        def bpt = BumperPaintingTime.findById(1).m0452Def //waktu 3
        def sub = 0
        sub = pat + cm + sub

        polishing = (sub * (gm.m000PersenPoleshing / 100)).intValue()
        preparation = (sub * (gm.m000PersenPreparation / 100)).intValue()
        painting = (sub * (gm.m000PersenPainting / 100)).intValue()

        res.bodi = body
        res.asembly = asembly
        res.polishing = polishing
        res.preparation = preparation
        res.painting = painting

        render res as JSON
    }

    def saveEstimasi(){
        def rec = Reception.findByT401NoWO(params.noWo)
        def reception = Reception.get(rec?.id)
        reception.t401Proses1 = params.body as Double
        reception.t401Proses2 = params.preparation as Double
        reception.t401Proses3 = params.painting as Double
        reception.t401Proses4 = params.polishing as Double
        reception.t401Proses5 = params.asembly as Double
        reception.t401TotalProses = params.total as Double
        reception.colorMatchingJmlPanel = ColorMatching.findById(params.colorPanel as Long)
        reception.colorMatchingKlasifikasi = ColorMatching.findById(params.colorKlasifikasi as Long)
        reception.tipeKerusakan = TipeKerusakan.findByM401NamaTipeKerusakan(params.kerusakan)
        reception.lastUpdated = datatablesUtilService?.syncTime()
        reception?.save(flush: true)
        reception.errors.each {
            println it
        }
        render "ok"
    }

    def getBillTo(){
        def res = [:]
        def opts = []
        def cari = [];
        if(params.from.toString().equalsIgnoreCase("Customer")){
            cari = HistoryCustomer.createCriteria().list {
                eq("staDel","0")
                ilike("fullNama","%"+params.query+"%")
                order("fullNama")
                maxResults(10);
            }
            cari.each {
                opts<<it.fullNama
            }
        }else if(params.from.toString().equalsIgnoreCase("Company")){
            cari = Company.createCriteria().list {
                eq("staDel","0")
                ilike("namaPerusahaan","%"+params.query+"%")
                order("namaPerusahaan")
                maxResults(10);
            }
            cari.each {
                opts<<it.namaPerusahaan
            }
        }else if(params.from.toString().equalsIgnoreCase("Asuransi")){
            cari = VendorAsuransi.createCriteria().list {
                eq("staDel","0")
                ilike("m193Nama","%"+params.query+"%")
                order("m193Nama")
                maxResults(10);
            }
            cari.each {
                opts<<it.m193Nama
            }
        }else if(params.from.toString().equalsIgnoreCase("Dealer")){
            cari = DealerPenjual.createCriteria().list {
                eq("staDel","0")
                ilike("m091NamaDealer","%"+params.query+"%")
                order("m091NamaDealer")
                maxResults(10);
            }
            cari.each {
                opts<<it.m091NamaDealer
            }
        }else if(params.from.toString().equalsIgnoreCase("Cabang")){
            cari = CompanyDealer.createCriteria().list {
                eq("staDel","0")
                ilike("m011NamaWorkshop","%"+params.query+"%")
                order("m011NamaWorkshop")
                maxResults(10);
            }
            cari.each {
                opts<<it.m011NamaWorkshop
            }
        }else if(params.from.toString().equalsIgnoreCase("Partner Service")){
            def spk = SPK.createCriteria().list {
                eq("staDel","0")
                resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                projections {
                    company{
                        groupProperty("id", "idCompany")
                    }
                }
            }
            cari = Company.createCriteria().list {
                eq("staDel","0")
                ilike("namaPerusahaan","%"+params.query+"%")
                if(spk.size()>0){
                    inList("id",spk.idCompany)
                }else {
                    eq("staDel","0")
                }
                order("namaPerusahaan")
                maxResults(10);
            }
            cari.each {
                opts<<it.namaPerusahaan
            }
        }
        res."options" = opts
        render res as JSON
    }

    def doBillTo(){
        def kegiatan = KegiatanApproval.findByM770KegiatanApprovalAndStaDel(KegiatanApproval.REQUEST_SPECIAL_DISCOUNT,"0")
        def reception = Reception.findByT401NoWOAndStaDel(params.noWo,"0")
        def jsonJ = JSON.parse(params.jobs)
        def jsonG = JSON.parse(params.goods)
        def jsonDJ = JSON.parse(params.discJobs)
        def jsonDG = JSON.parse(params.discParts)
        def billTo = new BillToReception()
        String jenisBillto = "d"
        if(params?.jenisAksi && params?.jenisAksi=="billto"){
            billTo = new BillToReception()
            billTo?.billTo = params?.billTo
            billTo?.namaBillTo = params?.namaBillTo
            billTo?.jenisBayar = params?.jenisBayar
            jenisBillto = "b"
        }
        int a = 0
        String disc = reception?.id+"#"
        jsonJ.each {
            def job = JobRCP.get(it.toLong())
            if(params?.jenisAksi && params?.jenisAksi=="billto"){
                job?.t402billTo = params.billTo
                job?.t402namaBillTo = params.namaBillTo
                billTo?.addToJobs(job)
            }
            job?.t402DiscPersen = jsonDJ[a].toDouble()
            job?.t402DiscRp = Math.round(jsonDJ[a].toDouble()/100*job?.t402HargaRp)
            job?.lastUpdated = datatablesUtilService?.syncTime()
            job?.save(flush: true)
            a++
        }

        a=0;

        jsonG.each {
            def part = PartsRCP.get(it.toLong())
            if(params?.jenisAksi && params?.jenisAksi=="billto"){
                billTo?.addToParts(part)
                part?.t403billTo = params.billTo
                part?.t403namaBillTo = params.namaBillTo
            }
            part?.t403TotalRp = (part?.t403Jumlah1 * part?.t403HargaRp)
            part?.t403DiscPersen = jsonDG[a].toDouble()
            part?.t403DiscRp = Math.round(jsonDG[a].toDouble()/100 * ((part?.t403Jumlah1 * part?.t403HargaRp)))
            part?.lastUpdated = datatablesUtilService?.syncTime()
            part?.save(flush: true)
            a++
        }
        Long ax = -1.toLong()
        if(params?.jenisAksi && params?.jenisAksi=="billto"){
            billTo.reception = reception
            billTo.save(flush: true)
            ax = billTo?.id
        }
        disc+=ax+"#"+jenisBillto

        reception.staApprovalDisc = "2"
        reception.lastUpdated = datatablesUtilService?.syncTime()
        reception.save(flush: true)
        def approval = new ApprovalT770(
                t770FK:disc,
                companyDealer: session?.userCompanyDealer,
                kegiatanApproval: kegiatan,
                t770NoDokumen: reception?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService.syncTime(),
                lastUpdated: datatablesUtilService.syncTime()
        )
        approval.save(flush:true)
        approval.errors.each{ println it }

        render "OK"
    }

    def editBillTo(){
        def jsonJ = JSON.parse(params.jobs)
        def jsonG = JSON.parse(params.goods)
        def jsonDJ = JSON.parse(params.discJobs)
        def jsonDG = JSON.parse(params.discParts)
        int a=0
        jsonJ.each {
            def job = JobRCP.get(it.toLong())
            job?.t402DiscPersen = jsonDJ[a].toDouble()
            job?.t402DiscRp = jsonDJ[a].toDouble()/100*job?.t402TotalRp
            job?.lastUpdated = datatablesUtilService?.syncTime()
            job?.save(flush: true)
            a++
        }
        a=0;
        jsonG.each {
            def part = PartsRCP.get(it.toLong())
            part?.t403DiscPersen = jsonDG[a].toDouble()
            part?.t403DiscRp = jsonDG[a].toDouble()/100*part?.t403TotalRp
            part?.lastUpdated = datatablesUtilService?.syncTime()
            part?.save(flush: true)
            a++
        }

        render "OK"
    }

}
