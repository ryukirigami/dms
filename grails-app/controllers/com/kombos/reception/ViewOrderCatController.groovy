package com.kombos.reception

import com.kombos.administrasi.MappingJobPanel
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class ViewOrderCatController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def viewOrderCatService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def cekNoSupply(){
        String hasil = ""
        def nos = POS.findByT418NoPOS(params.id)?.t418NoSupply
        if(nos){
            hasil = "ada"
        }else{
            hasil = ""
        }
        render hasil
    }
    def sublist() {
        [sCriteria_Tanggalakhir : params.sCriteria_Tanggalakhir, sCriteria_Tanggal: params.sCriteria_Tanggal,sCriteria_vendorCat: params.sCriteria_vendorCat, t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsublist() {
        [noPos:params.noPos, t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsubsublist() {
        [noPos:params.noPos, kodeWarna:params.kodeWarna, t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        session.exportParams=params
        render viewOrderCatService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        render viewOrderCatService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {
        render viewOrderCatService.datatablesSubSubList(params) as JSON
    }
    def datatablesSubSubSubList() {
        render viewOrderCatService.datatablesSubSubSubList(params) as JSON
    }

    def massdelete() {
        def hasil= viewOrderCatService.massDelete(params)
        render "ok"
    }

    def printPos(){
        def jsonArray = JSON.parse(params.idReception)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'orderCat.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )

            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("orderCat_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();

        def results = SupplyWarna.findAllByReception(Reception.findByT401NoWO(id))
        results.sort {
            it.pos.t418NoPOS
        }

        int count = 0

        results.each {
            def posJob = PosJob.findByReception(Reception.findByT401NoWO(id))
            def mjp = MappingJobPanel.findAllByOperation(posJob.operation)
            def dataMjp = ""
            mjp.each {
                dataMjp += it.masterpanel?.m094NamaPanel
                dataMjp += ","
            }
            def nopol = it.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.reception.historyCustomerVehicle.t183NoPolBelakang
            def data = [:]

            count = count + 1

            data.put("noPos",it.pos?.t418NoPOS)
            data.put("tglPos", it.pos.t418TglPOS.format("dd MMMM yyyy"))
            data.put("noPol", nopol)
            data.put("model",  it.reception.historyCustomerVehicle.fullModelCode.baseModel.m102NamaBaseModel)
            data.put("noWo",  it.reception?.t401NoWO)
            data.put("vendor",  it.vendorcat?.m191Nama)

            data.put("no", count as String)
            data.put("warna",it.pos.warna)
            data.put("panel",dataMjp)

            reportData.add(data)
        }

        return reportData

    }
}
