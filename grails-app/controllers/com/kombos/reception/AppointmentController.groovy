package com.kombos.reception

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.ASB
import com.kombos.board.JPB
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.*
import com.kombos.parts.GoodsHargaJual
import com.kombos.parts.Konversi
import com.kombos.parts.MappingPartRegion
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

class AppointmentController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def appointmentService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def jasperService
    def conversi = new Konversi()

    def index() {
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['jobSuggest'] = null
        KegiatanApproval kegiatanApprovalEditBookingFee = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.REQUEST_EDIT_BOOKING_FEE)
        def approverEditBookingFee = ""
        for(NamaApproval namaApproval : kegiatanApprovalEditBookingFee.namaApprovals){
            approverEditBookingFee += namaApproval.userProfile.fullname + "\r\n"
        }

        KegiatanApproval kegiatanApprovalCancelBookingFee = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.CANCEL_BOOKING_FEE)
        def approverCancelBookingFee = ""
        for(NamaApproval namaApproval : kegiatanApprovalCancelBookingFee.namaApprovals){
            approverCancelBookingFee += namaApproval.userProfile.fullname + "\r\n"
        }
        KegiatanApproval kegiatanApprovalCancelAppointment = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.CANCEL_APPOINTMENT)
        def approverCancelApp = ""
        for(NamaApproval namaApproval : kegiatanApprovalCancelAppointment.namaApprovals){
            approverCancelApp += namaApproval.userProfile.fullname + "\r\n"
        }

        def custLamaWeb = new CustLamaWeb()
        def vehicleWeb = new HistoryCustomerVehicle()
        if(params?.idCustWeb){
            custLamaWeb = CustLamaWeb?.get(params?.idCustWeb?.toLong())
            vehicleWeb = custLamaWeb?.customerVehicle?.getCurrentCondition()
        }
        params.companyDealer = session.userCompanyDealer

        def result = appointmentService.create(params)

        return [
                kategoriJobs: KategoriJob.list(),
                statusWarranties: StatusWarranty.list(),
                kegiatanApprovalEditBookingFee: kegiatanApprovalEditBookingFee,
                approverEditBookingFee: approverEditBookingFee,
                kegiatanApprovalCancelBookingFee: kegiatanApprovalCancelBookingFee,
                approverCancelBookingFee: approverCancelBookingFee,
                kegiatanApprovalCancelAppointment: kegiatanApprovalCancelAppointment,
                approverCancelBookingApp: approverCancelApp,
                custLamaWeb : custLamaWeb,
                vehicleWeb : vehicleWeb
        ]
    }

    def keluhanDatatablesList() {
        session.exportParams=params

        render appointmentService.keluhanDatatablesList(params) as JSON
    }

    def addJobDatatablesList() {
        session.exportParams=params

        render appointmentService.addJobDatatablesList(params) as JSON
    }

    def jobnPartsDatatablesList() {
        params.companyDealer = session?.userCompanyDealer
        render appointmentService.jobnPartsDatatablesList(params,session) as JSON
    }

    def partDatatablesList() {
        render appointmentService.partDatatablesList(params,session) as JSON
    }

    def addPartDatatablesList() {
        render appointmentService.addPartDatatablesList(params) as JSON
    }

    def historyServiceDatatablesList() {
        render appointmentService.historyServiceDatatablesList(params) as JSON
    }

    def partList() {
        Random rand = new Random();
        int  n = rand.nextInt(50) + 1;
        String idTable = new Date().format("yyyyMMddhhmmss")+params.idJob+n.toString()
        [idJobApp: params.idJobApp,idJob : params.idJob, idTable: idTable]
    }

    def workList() {
        [idJob : params.idJob, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def JobKeluhan(){
        render appointmentService.JobKeluhan(params ) as JSON
    }

    def saveJobKeluhan() {
        def result = appointmentService.saveJobKeluhan(params)

        render result as JSON
    }

    def saveJobInput(){
        def result = [success: "1"]

        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def currentRequest = RequestContextHolder.requestAttributes
        def id = params.id
        def countRow = new Integer(params.countRow)
        println("Param: "+params)
        def indexRow = 1
        def keluhanRcp
        def permintaanRcp
        def app = Appointment.findById(params.id)

        while(indexRow < countRow){
            if(params.get("t411StaKP" + indexRow).toString()=="K"){
                keluhanRcp = KeluhanRcp.findByReceptionAndT411NamaKeluhanAndStaDel(app?.reception,params.get("t411NamaKeluhan" + indexRow),"0")

                //println("Keluhan: "+params.get("t411NamaKeluhan" + indexRow))

                if(!keluhanRcp){
                    keluhanRcp = new KeluhanRcp()
                    keluhanRcp.reception = app?.reception
                    keluhanRcp.companyDealer = session.userCompanyDealer
                    keluhanRcp.t411NoUrut = new Double(params.get("t411NoUrut" + indexRow))
                    keluhanRcp.t411NamaKeluhan = params.get("t411NamaKeluhan" + indexRow)
                    keluhanRcp.t411StaButuhDiagnose = params.get("t411StaButuhDiagnose" + indexRow)
                    keluhanRcp.staDel = 0
                    keluhanRcp.createdBy = "system"
                    keluhanRcp.lastUpdProcess = "INSERT"
                    keluhanRcp.setDateCreated(datatablesUtilService?.syncTime())
                    keluhanRcp.setLastUpdated(datatablesUtilService?.syncTime())
                    keluhanRcp.save(flush:true)
                }
            }else{
                permintaanRcp = PermintaanCustomer.findByReceptionAndPermintaanAndStaDel(app?.reception,params.get("t411NamaKeluhan" + indexRow),"0")
                println("ada permintaan")
                if(!permintaanRcp){
                    permintaanRcp = new PermintaanCustomer()
                    permintaanRcp.reception = app?.reception
                    permintaanRcp.permintaan = params.get("t411NamaKeluhan" + indexRow)
                    permintaanRcp.staDiagnose = params.get("t411StaButuhDiagnose" + indexRow)
                    permintaanRcp.staDel = 0
                    permintaanRcp.createdBy = "system"
                    permintaanRcp.lastUpdProcess = "INSERT"
                    permintaanRcp?.dateCreated = datatablesUtilService?.syncTime()
                    permintaanRcp?.lastUpdated = datatablesUtilService?.syncTime()
                    permintaanRcp.save(flush:true)
                }
            }
            indexRow++
        }

        def countRowJob = new Integer(params.countRowJob)
        indexRow = 1
        def jobInv
        def histCV = CustomerVehicle.findByT103VinCodeAndStaDel(params.idVincode,"0");

        Appointment appointmentInstance = Appointment.get(params.id as Long)
        appointmentInstance?.t301KmSaatIni = params.kmSekarang.toLong()
        appointmentInstance?.reception?.t401KmSaatIni = params.kmSekarang.toLong()
        appointmentInstance?.reception?.historyCustomerVehicle = histCV.getCurrentCondition()
        appointmentInstance?.reception?.lastUpdated = datatablesUtilService?.syncTime()

        result.appointmentInstance = appointmentInstance
        Reception reception = appointmentInstance.reception

        //Job Suggest
        def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(histCV,"0");
        if(jobSuggest){
            if(params.jobSuggest){
                def jobSNew = new JobSuggestion()
                jobSNew.properties = jobSuggest.properties
                jobSNew.t503KetJobSuggest = params.jobSuggest
                jobSNew.dateCreated = datatablesUtilService?.syncTime()
                jobSNew.lastUpdated = datatablesUtilService?.syncTime()
                jobSNew.save(flush: true)
            }
            jobSuggest.lastUpdated = datatablesUtilService?.syncTime()
            jobSuggest?.staDel = "1"
            jobSuggest?.save(flush: true);
        }

        def allJobs = "";
        while(indexRow < countRowJob){
            Operation operation = Operation.get(params.get("operation" + indexRow))
            JobApp ja =JobApp.findByReceptionAndOperationAndT302StaDel(reception,Operation.get(params."operation${indexRow}" as Long),"0")

            if(ja){
                ja.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ja.lastUpdProcess = "UPDATE"
            }else {
                ja = new JobApp()
                ja.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                ja.lastUpdProcess = "INSERT"
                ja.dateCreated = datatablesUtilService?.syncTime()
            }
            ja.lastUpdated = datatablesUtilService?.syncTime()

            def nominalValue = 0
            def rate = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(operation,histCV?.getCurrentCondition()?.fullModelCode?.baseModel,"0",new Date(),[sort: 'id',order: 'desc'])
            def harga = TarifPerJam.findByKategoriAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(histCV?.getCurrentCondition()?.fullModelCode?.baseModel?.kategoriKendaraan,"0",session?.userCompanyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])

            def nominal1 = harga && rate ? harga?.t152TarifPerjamBP * rate?.t113FlatRate : 0
            def nominal2 = harga && rate ? harga?.t152TarifPerjamSB * rate?.t113FlatRate : 0
            def nominalTam = harga && rate ? harga?.t152TarifPerjamDealer * rate?.t113FlatRate : 0
            def nominal3 = harga && rate ? harga?.t152TarifPerjamGR * rate?.t113FlatRate : 0
            def nominal5 = harga?.t152TarifPerjamSBI && rate ? harga?.t152TarifPerjamSBI * rate?.t113FlatRate : 0

            if(operation?.kategoriJob?.m055KategoriJob?.contains("BP")){
                nominalValue = nominal1
            }else if(operation?.kategoriJob?.m055KategoriJob?.contains("SBE")){
                nominalValue = nominal2
                String jobTam = "SB10K,SB20K,SB30K,SB40K,SB50K"
                if(jobTam.toString().contains(operation.m053Id)){
                    nominalValue = nominalTam
                }
            }else if(operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SBI")){
                nominalValue = nominal5
            }else if(operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("GR")){
                nominalValue = nominal3
            }

            println("Harga: "+nominalValue)

            ja?.t302HargaRp = (nominalValue?nominalValue:0)
            ja?.t302TotalRp = (nominalValue?nominalValue:0)
            ja.reception = reception
            ja.operation = operation
            ja.statusWarranty = StatusWarranty.get(params."staWarranty" as Long)
            ja.t302NoKeluhan = params.get("t702NoUrutKeluhan" + indexRow)
            ja?.companyDealer = session?.userCompanyDealer
            ja.t302StaDel = "0"
            allJobs += ja?.operation?.m053NamaOperation

            if(indexRow+1!=countRowJob){
                allJobs+=","
            }

            ja.save(flush: true)
            ja.errors.each { println it }

            def kategori = KategoriJob.get(params?.kategoriJob?.toLong())
            def pjs = PartJob.findAllByFullModelCodeAndOperation(histCV?.getCurrentCondition()?.fullModelCode,operation)
            println("pjs "+pjs)
            pjs.each {PartJob pj ->
                println("masuk pjs")
                PartsApp pa = new PartsApp()
                def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(pj?.goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                    eq("staDel","0");
                    eq("companyDealer",session?.userCompanyDealer);
                }
                def mappingRegion = MappingPartRegion.createCriteria().get {
                    if(mappingCompRegion?.size()>0){
                        inList("region",mappingCompRegion?.region)
                    }else{
                        eq("id",-10000.toLong())
                    }
                    maxResults(1);
                }
                def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                if(mappingRegion){
                    hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                }

                pa.setGoods(pj.goods)
                pa.setOperation(pj.operation)
                pa.reception = reception
                pa.namaProsesBP = pj.namaProsesBP
                pa.t303HargaRp = hargaTanpaPPN
                pa.t303Jumlah1 = pj.t112Jumlah
                pa.t303Jumlah2 = pj.t112Jumlah
                pa.t303TotalRp = pj.t112Jumlah * hargaTanpaPPN
                pa.t303StaDel = "0"
                pa.dateCreated = datatablesUtilService?.syncTime()
                pa.lastUpdated = datatablesUtilService?.syncTime()
                pa.save(flush: true)
                pa.errors.each { println it }

            }

            indexRow++
        }
        render result as JSON
    }

    def savePart() {
        params.userCompanyDealer = session.userCompanyDealer
        params.dateCreated = datatablesUtilService.syncTime()
        params.lastUpdated = datatablesUtilService.syncTime()
        def result = appointmentService.savePart(params)

        render result as JSON
    }

    def showAppointment() {
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['jobSuggest'] = null
        def result = appointmentService.showAppointment(params)

        render result as JSON
    }

    def saveApp() {
        def currentRequest = RequestContextHolder.requestAttributes
        params.jobTaken = currentRequest.session['jobSuggest']
        def result = appointmentService.saveApp(params)

        render result as JSON
    }

    def requestPart() {
    }

    def cancelEditBookingFee() {
    }

    def cancelEditBookingFeeDatatablesList(){
        render appointmentService.cancelEditBookingFeeDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def requestPartDatatablesList() {
        render appointmentService.requestPartDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def orderPart() {
        render appointmentService.orderPart(params, session.userCompanyDealer) as JSON
    }

    def massdelete() {
        println(params)
        def res = [:]
        def jsonArray = JSON.parse(params.jobs)
        def jsonArray2 = JSON.parse(params.parts)
        try {
            jsonArray.each {
                def jobApp = JobApp.get(it.toLong())
                def parts = PartsApp.findAllByReceptionAndOperationAndT303StaDel(jobApp?.reception,jobApp?.operation,"0");
                parts.each {
                    it.lastUpdated = datatablesUtilService?.syncTime()
                    it.t303StaDel = "1"
                    it.save(flush: true)
                }
                jobApp.lastUpdated = datatablesUtilService?.syncTime()
                jobApp.t302StaDel = "1"
                jobApp.save(flush: true)
            }
            jsonArray2.each {
                def partsApp = PartsApp.get(it.toLong())
                partsApp.lastUpdated = datatablesUtilService?.syncTime()
                partsApp.t303StaDel = "1"
                partsApp.save(flush: true)
                partsApp.errors.each {println "DOWN "+it}

            }
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


    def generateNomorDokumenApprovalEditBookingFee() {
        params.companyDealer = session?.userCompanyDealer
        def result = appointmentService.generateNomorDokumenApprovalEditBookingFee()
        render result as JSON
    }
    def generateNomorDokumenApprovalCancelBookingFee() {
        params.companyDealer = session?.userCompanyDealer
        def result = appointmentService.generateNomorDokumenApprovalCancelBookingFee()
        render result as JSON
    }
    def generateNomorDokumenApprovalCancelApp() {
        params.companyDealer = session?.userCompanyDealer
        def result = appointmentService.generateNomorDokumenApprovalCancelApp()
        render result as JSON
    }

    def requestApprovalEditBookingFee(){
        params.companyDealer = session?.userCompanyDealer
        def result = appointmentService.requestApprovalEditBookingFee(params)

        if(!result.error) {
            render result as JSON
            return
        }

        render result as JSON
    }
    def requestApprovalCancelBookingFee(){
        params.companyDealer = session?.userCompanyDealer
        def result = appointmentService.requestApprovalCancelBookingFee(params)

        if(!result.error) {
            render result as JSON
            return
        }

        render result as JSON
    }

    def requestApprovalCancelAppointment(){
        params?.companyDealer = session.userCompanyDealer
        def result = appointmentService.requestApprovalCancelAppointment(params)
        if(!result.error) {
            render result as JSON
            return
        }

        render result as JSON
    }

    def printAppointmentGR(){
        def id = params.id
        Date awal = datatablesUtilService?.syncTime()
        SimpleDateFormat sdf = new SimpleDateFormat()
        Date beres = datatablesUtilService?.syncTime()
        String jenisApp = params?.jenisApp

        Appointment appointment = Appointment.get(id)

        def receptionInstance = appointment.reception

        HistoryCustomerVehicle historyCustomerVehicle = appointment?.historyCustomerVehicle
        def noPol = historyCustomerVehicle?.fullNoPol
        def vincode = historyCustomerVehicle?.customerVehicle?.t103VinCode
        def model = historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        def warna = historyCustomerVehicle?.warna?.m092NamaWarna
        CompanyDealer companyDealer = (CompanyDealer)session.userCompanyDealer
        def gp = GeneralParameter.findByCompanyDealerAndM000StaDel(companyDealer,"0")

        HistoryCustomer historyCustomer = appointment?.historyCustomer
        def asb = ASB.createCriteria().list {
            eq("staDel","0")
            eq("appointment",appointment)
            order("t351TglJamApp")
        }
        int ind = 0
        asb.each {
            ind++;
            if(ind==1){
                awal = it?.t351TglJamApp
            }
            if(ind==asb.size()){
                beres = it?.t351TglJamFinished
            }
        }
        def namaCustomer = historyCustomer?.fullNama?:""
        def alamatCustomer = historyCustomer?.t182Alamat?:""
        def noHP = historyCustomer?.t182NoHp?historyCustomer?.t182NoHp:""
        def namaSA = "...";
        def hpSA = "...";
        if(appointment?.t301NamaSA){
            User user = User.findByUsernameAndStaDel(appointment?.t301NamaSA,"0")
            namaSA = user?.fullname
            def manPower = NamaManPower.findByUserProfileAndStaDel(user,"0")
            if(manPower){
                hpSA = manPower?.t015NoTelp
            }

        }
        def JA = "GR & "+conversi.toKM(appointment?.t301KmSaatIni)
        def parameters = [
                janjiDatang : appointment?.t301TglJamRencana ? appointment?.t301TglJamRencana?.format("dd-MM-yyyy HH:mm"):"-",
                jenisApp: JA,
                noPol: noPol,
                model: model,
                warna: warna,
                namaCustomer: namaCustomer,
                alamatCustomer: alamatCustomer,
                noTelp: noHP,
                namaSA: namaSA,
                nmPetugas : namaSA,
                hpSA: hpSA,
                tglMasuk: awal?.format("dd-MM-yyyy"),
                jamMasuk: awal?.format("HH:mm"),
                ubahTgl : "",
                ubahJam : "",
                tglPenyerahan: "",
                jamPenyerahan: "",
                janjiUbahTgl : "-",
                janjiUbahJam : "-",
                noAppointment: receptionInstance?.t401NoAppointment,
                companyDealer: appointment?.historyCustomerVehicle?.dealerPenjual?.m091NamaDealer ? appointment?.historyCustomerVehicle?.dealerPenjual?.m091NamaDealer:"-",
                dd : appointment?.historyCustomerVehicle?.t183TglDEC?appointment?.historyCustomerVehicle?.t183TglDEC?.format("dd/MM/yyyy"):"-",
                mdl : appointment?.historyCustomerVehicle?.fullModelCode?.t110FullModelCode ? appointment?.historyCustomerVehicle?.fullModelCode?.t110FullModelCode:"-",
                frm : appointment?.historyCustomerVehicle?.customerVehicle?.t103VinCode ? appointment?.historyCustomerVehicle?.customerVehicle?.t103VinCode:"-",
                eg : appointment?.historyCustomerVehicle?.t183NoMesin ? appointment?.historyCustomerVehicle?.t183NoMesin:"-",
                thnProd : appointment?.historyCustomerVehicle?.t183ThnBlnRakit?.substring(0, 4) ? appointment?.historyCustomerVehicle?.t183ThnBlnRakit?.substring(0, 4):"-"
        ]

        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        def keluhanItems = []

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                keluhanItems << [
                        keluhan : it.t411NamaKeluhan
                ]
            }
        }else{
            keluhanItems << [
                    keluhan : "TIDAK ADA KELUHAN"
            ]
        }

        //Kolom Permintaan
        def permintaanItemsDb = PermintaanCustomer.findAllByReceptionAndStaDel(receptionInstance, "0");
        def permintaanItems = []

        if(permintaanItemsDb){
            permintaanItemsDb.each{
                permintaanItems << [
                        permintaan : it.permintaan
                ]
            }
        }else{
            permintaanItems << [
                    permintaan : "-"
            ]
        }
        ////////end kolom permintaan

        def jobItems = []
        def noUrut = 1
        double totRate = 0
        double totJumlah = 0
        def jobApps = JobApp.findAllByReceptionAndT302StaDel(appointment.reception,"0")
        jobApps.each { JobApp job ->
            double rate = 0

            if (job.operation && appointment.historyCustomerVehicle?.fullModelCode?.baseModel) {
                FlatRate r = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(job.operation,appointment.historyCustomerVehicle?.fullModelCode?.baseModel,"0",new Date(),[sort: 'id',order: 'desc'])
                if(r)
                    rate = r.t113FlatRate
            }
            totRate=totRate+rate
            totJumlah=totJumlah+job.t302TotalRp
            jobItems << [
                    uraian : job?.operation?.m053NamaOperation,
                    rate : rate,
                    total : job?.t302TotalRp ? conversi.toRupiah(job.t302TotalRp):"0",
            ]

//            permintaanItems << [
//                    permintaan: job?.operation?.m053NamaOperation
//            ]

            noUrut++
        }
        if(jobApps.size()<1){
            jobItems << [
                    uraian : "-",
                    rate : "-",
                    total : "-",
            ]
        }

        def jobItemsDb = JobApp.findAllByReceptionAndT302StaDel(receptionInstance,"0")
        def partApps = PartsApp.findAllByReception(appointment.reception)
        def totPart = 0
        def totalBiaya= 0
        if(jobItemsDb){
            jobItemsDb.each{
                partApps.each {
                    totPart = it?.t303TotalRp
                }
                totalBiaya = it?.t302TotalRp
            }
        }

        def riwayatjobItems = []
        if(vincode) {
            def crit = Reception.createCriteria()
            def results = crit.list() {
                eq("staDel","0")
                eq("staSave","0")
                eq("historyCustomerVehicle", historyCustomerVehicle)
                order("dateCreated")
            }
            def rows = []
            results.each {
                Reception reception ->
                    def arrJob = ""
                    def arrPart = []
                    int  indD = 1
                    def jobRCP = JobRCP.findAllByReceptionAndStaDel(reception, "0");
                    jobRCP.each {
                        if(indD==jobRCP.size()){
                            arrJob+=it?.operation?.m053NamaOperation
                        }else{
                            arrJob+=it?.operation?.m053NamaOperation+", "
                        }
                        indD++;
                    }
                    riwayatjobItems << [

                            tanggal: reception?.t401TglJamCetakWO ? reception?.t401TglJamCetakWO?.format("dd-MM-yyyy") : reception?.dateCreated?.format("dd-MM-yyyy"),
                            job    : arrJob,
                            sa     : reception?.t401NamaSA,
                            km     : conversi.toKM(appointment?.t301KmSaatIni)
                    ]
            }
            if(results.size()<1){
                riwayatjobItems << [
                        tanggal: "-",
                        job    : "-",
                        sa     : "-",
                        km     : "-"
                ]
            }
        }

        def reportData = []
        reportData << [
                totalBiaya : conversi.toRupiah(totalBiaya),
                ubahBiaya : "",
                permintaanItems : permintaanItems,
                keluhanItems : keluhanItems,
                jobItems : jobItems,
                riwayatjobItems : riwayatjobItems
        ]

        def reportDef = new JasperReportDef(name:'ServiceApp.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )

        def file = File.createTempFile("ServiceAppointment${jenisApp}_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }

    def printAppointmentBP(){
        Date awal = datatablesUtilService?.syncTime()
        Date beres = datatablesUtilService?.syncTime()
        String jenisApp = params?.jenisApp
        def id = params.id

        Appointment appointment = Appointment.get(id)

        def receptionInstance = appointment.reception

        HistoryCustomerVehicle historyCustomerVehicle = appointment.historyCustomerVehicle
        def noPol = historyCustomerVehicle?.fullNoPol
        def vincode = historyCustomerVehicle?.customerVehicle?.t103VinCode
        def model = historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        def warna = historyCustomerVehicle?.warna?.m092NamaWarna
        CompanyDealer companyDealer = (CompanyDealer)session.userCompanyDealer

        HistoryCustomer historyCustomer = appointment.historyCustomer
        def jpb = JPB.createCriteria().list {
            eq("staDel", "0")
            eq("appointments",appointment)
            order("t451TglJamPlan")
        }

        int ind = 0
        jpb.each {
            ind++;
            if(ind==1){
                awal = it?.t451TglJamPlan
            }
            if(ind==jpb.size()){
                beres = it?.t451TglJamPlanFinished
            }
        }
        def namaCustomer = historyCustomer?.fullNama?:""
        def alamatCustomer = historyCustomer?.t182Alamat?:""
        def noHP = historyCustomer?.t182NoHp?historyCustomer?.t182NoHp:""
        def namaSA = "...";
        def hpSA = "...";
        if(appointment?.t301NamaSA){
            User user = User.findByUsernameAndStaDel(appointment?.t301NamaSA,"0")
            namaSA = user?.fullname
            def manPower = NamaManPower.findByUserProfileAndStaDel(user,"0")
            if(manPower){
                hpSA = manPower?.t015NoTelp
            }

        }

        def parameters = [
                janjiDatang : appointment?.t301TglJamRencana ? appointment?.t301TglJamRencana.format("dd-MM-yyyy"):"-",
                jenisApp: appointment?.t301KmSaatIni ? "BP & "+conversi.toRupiah(appointment?.t301KmSaatIni):"-",
                noPol: noPol,
                model: model,
                warna: warna,
                namaCustomer: namaCustomer,
                alamatCustomer: alamatCustomer,
                noTelp: noHP,
                namaSA: namaSA,
                nmPetugas : namaSA,
                hpSA: hpSA,
                tglMasuk: awal?.format("dd-MM-yyyy"),
                jamMasuk: awal?.format("HH:mm"),
                ubahTgl : "",
                ubahJam : "",
                tglPenyerahan: beres?.format("dd-MM-yyyy"),
                jamPenyerahan: beres?.format("HH:mm"),
                janjiUbahTgl : appointment?.t301TglJamPenyerahanSchedule ? appointment?.t301TglJamPenyerahanSchedule.format("dd-MM-yyyy"):"-",
                janjiUbahJam : appointment?.t301TglJamPenyerahanSchedule ? appointment?.t301TglJamPenyerahanSchedule.format("HH:mm"):"-",
                noAppointment: receptionInstance?.t401NoAppointment,
                companyDealer: appointment?.historyCustomerVehicle?.companyDealer?.m011NamaWorkshop,
                dd : appointment?.historyCustomerVehicle?.t183TglDEC?appointment?.historyCustomerVehicle?.t183TglDEC?.format("dd/MM/yyyy"):"-",
                mdl : appointment?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102KodeBaseModel,
                frm : appointment?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                eg : appointment?.historyCustomerVehicle?.t183NoMesin,
                thnProd : appointment?.historyCustomerVehicle?.t183ThnBlnRakit.substring(0, 4)
        ]

        def permintaanItems = []
        (0..4).each {
            permintaanItems << [
                    permintaan : "belum diproses...."
            ]
        }

        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        def keluhanItems = []

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                keluhanItems << [
                        keluhan : it.t411NamaKeluhan
                ]
            }
        }else{
            keluhanItems << [
                    keluhan : "Tidak Ada Keluhan"
            ]
        }


        def jobItems = []
        def noUrut = 1
        double totRate = 0
        double totJumlah = 0
        def jobApps = JobApp.findAllByReception(appointment.reception)
        jobApps.each { JobApp job ->
            double rate = 0

            if (job.operation && appointment.historyCustomerVehicle?.fullModelCode?.baseModel) {
                FlatRate r = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(job.operation,appointment.historyCustomerVehicle?.fullModelCode?.baseModel,"0",new Date(),[sort: 'id',order: 'desc'])
                if(r)
                    rate = r.t113FlatRate
            }
            totRate=totRate+rate
            totJumlah=totJumlah+job.t302TotalRp
            jobItems << [
                    uraian : job.operation.m053NamaOperation,
                    rate : rate,
                    total : job.t302TotalRp ? conversi.toRupiah(job.t302TotalRp):"0",
            ]
            noUrut++
        }
        if(jobApps.size()<1){
            jobItems << [
                    uraian : "-",
                    rate : "-",
                    total : "-",
            ]
        }

        def partApps = PartsApp.findAllByReception(appointment.reception)
        def totPart = 0
        partApps.each {
            totPart = totPart + it.t303TotalRp
        }
        def totalBiaya = totJumlah + totPart

        def riwayatjobItems = []
        if(vincode) {
            def crit = Reception.createCriteria()
            def results = crit.list() {
                eq("staDel","0")
                eq("staSave","0")
                eq("historyCustomerVehicle", historyCustomerVehicle)
                order("dateCreated")
            }
            def rows = []
            results.each { Reception reception ->
                def arrJob = []
                def arrPart = []
                def jobRCP = JobRCP.findAllByReceptionAndStaDel(reception, "0");
                jobRCP.each {
                    arrJob << jobRCP?.operation?.m053NamaOperation
                }
                def partRCP = PartsRCP.findAllByReceptionAndStaDel(reception, "0");
                partRCP.each {
                    arrPart << partRCP?.goods?.m111Nama
                }

                riwayatjobItems << [

                        tanggal: reception?.t401TglJamCetakWO ? reception?.t401TglJamCetakWO?.format("dd MMMM yyyy") : reception?.dateCreated?.format("dd MMMM yyyy"),
                        job    : arrJob,
                        sa     : reception?.t401NamaSA,
                        km     : reception?.t401KmSaatIni
                ]
            }
            if(results.size()<1){
                riwayatjobItems << [
                        tanggal: "-",
                        job    : "-",
                        sa     : "-",
                        km     : "-"
                ]
            }
        }

        def reportData = []
        reportData << [
                totalBiaya : conversi.toRupiah(totalBiaya),
                ubahBiaya : "",
                permintaanItems : permintaanItems,
                keluhanItems : keluhanItems,
                jobItems : jobItems,
                riwayatjobItems : riwayatjobItems
        ]

        def reportDef = new JasperReportDef(name:'ServiceApp.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )

        def file = File.createTempFile("ServiceAppointment${jenisApp}_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }

    def getDataJobSummary(){
        def id = params.id
        def app =Appointment.get(id)
        def receptionInstance = app?.reception
        def jobItemsDb = JobApp.findAllByReceptionAndT302StaDel(receptionInstance,'0')
        String htmlData = ""

        def subTotal = 0
        def ppn = 0
        def grandTotal = 0
        def bookingFee = 0
        if(jobItemsDb){
            def no = 0;
            jobItemsDb.each{
                no++;
                Double hParts = 0
                def parts = PartsApp.findAllByReceptionAndOperationAndT303StaDel(app?.reception,it?.operation,"0");
                parts.each {
                    hParts += it?.t303TotalRp ? it?.t303TotalRp : 0
                }
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " "+no+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.operation.m053NamaOperation+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t302TotalRp+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+hParts+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+(hParts+it.t302TotalRp)+"\n" +
                        " </td>\n" +
                        "</tr>"

                subTotal+= (it.t302TotalRp==null?0:(it.t302TotalRp+hParts))
            }

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " \n"+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    "</tr>"

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " SUB TOTAL "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+subTotal+"\n" +
                    " </td>\n" +
                    "</tr>"

            ppn = subTotal * (10/100)
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " PPN "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " 10 %\n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+ppn+"\n" +
                    " </td>\n" +
                    "</tr>"

            grandTotal = ppn + subTotal
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " GRAND TOTAL "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+grandTotal+"\n" +
                    " </td>\n" +
                    "</tr>"

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " \n"+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    "</tr>"

            bookingFee = grandTotal * (25/100)
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " BOOKING FEE "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " 25 %\n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+bookingFee+"\n" +
                    " </td>\n" +
                    "</tr>"
        }
        render htmlData as String
    }

    def getDataKeluhanSummary(){
        def id = params.id
        def appointmentInstance = Appointment.get(id)
        def receptionInstance = appointmentInstance.getReception()
        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        String htmlData = ""

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                htmlData+=	it.t411NoUrut+". "+it.t411NamaKeluhan+"\n"
            }
        }

        render htmlData as String
    }

    def getSummaryDetail(){
        def res = [:]
        def id = params.id
        def appointmentInstance = Appointment.get(id)
        render res as JSON
    }

    def getRate(){
        def res = [:]
        def id = params.id
        Operation op = Operation.get(id)
        if(op) {
            FlatRate r = FlatRate.findByOperation(op)
            def rate = r?.t113FlatRate
            res.rate = rate
            res.status = "ok"
        } else {
            res.status = "nok"
        }
        render res as JSON
    }

    def addPembayaran(){
        def result = [:]
        def setlement = new BookingFee()
        setlement.metodeBayar = MetodeBayar.findById(params.metodeBayar as Long)
        setlement.t721TglJamBookingFee = datatablesUtilService?.syncTime()
        setlement.t721JmlhBayar = params.jumlah as Double
        setlement.t721StaDel = '0'
        setlement.lastUpdProcess = 'INSERT'
        setlement.companyDealer = session.userCompanyDealer
        setlement.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        setlement.dateCreated = datatablesUtilService.syncTime()
        setlement.lastUpdated = datatablesUtilService.syncTime()
        setlement.save(flush: true)
        setlement.errors.each {
            println it
        }
        def kwitansi = new Kwitansi()
        kwitansi?.companyDealer = session.userCompanyDealer
        kwitansi?.t722StaBookingOnRisk = "B"
        kwitansi?.t722JmlUang = params.jumlah as Double
        kwitansi?.t722Ket = params.ket
        kwitansi?.t722StaDel = "0"
        kwitansi?.dateCreated = datatablesUtilService.syncTime()
        kwitansi?.lastUpdated = datatablesUtilService.syncTime()
        kwitansi?.save(flush: true)
        render result  as JSON
    }

    def getNewAppointment(){
        params.companyDealer = session.userCompanyDealer
        def result = appointmentService.getNewAppointment(params)
        render result as JSON
    }

    def getAppointment(){
        params.companyDealer = session.userCompanyDealer
        def result = appointmentService.getAppointment(params)
        render result as JSON
    }

    def doReschedule(){
        def appointment = Appointment.get(params.idApp.toLong())
        def asb = ASB.findAllByAppointmentAndStaDel(appointment,"0");
        asb.each {
            it.staDel = "1"
            it.lastUpdated = datatablesUtilService.syncTime()
            it.save(flush: true)
        }
        appointment.lastUpdated= datatablesUtilService.syncTime()
        appointment.t301StaOkCancelReSchedule = "2"
        appointment.save(flush: true)
        try {
            def jpbEdit = JPB.findAllByReception(appointment.reception)
            if(jpbEdit.size() > 0 ){
                jpbEdit.each {
                    def ubhS = JPB.findById(it.id)
                    ubhS.staDel = '1'
                    ubhS.save(flush: true)
                }
            }
        }catch(Exception e){

        }
        render "ok"
    }

    def savePreDiagnose(def params){
        def result = [:]
        def prediagnosis = appointmentService.savePreDiagnose(params)

        if(prediagnosis != null){
            result = prediagnosis
        }

        render result as JSON
    }

    def jobnPartsOrderDatatablesList() {
        render appointmentService.jobnPartsOrderDatatablesList(params,session) as JSON
    }

    def findNoPol(){
        def balik=[]
        def idPemilik = -1
        if(params.kode && params.tengah && params.belakang){
            def cari = HistoryCustomerVehicle.createCriteria().list {
                eq("staDel","0")
//                kodeKotaNoPol{
//                    eq("id",params.kode.toLong())
//                }
//                eq("t183NoPolTengah",params.tengah)
//                eq("t183NoPolBelakang",params.belakang,ignoreCase : true)
                kodeKotaNoPol {
                    eq('m116ID', params.kode,[ignoreCase: true])
                }
                eq('t183NoPolTengah', params.tengah,[ignoreCase: true])
                eq('t183NoPolBelakang', params.belakang, [ignoreCase: true])
            }
            if(cari){
                balik << [
                        id : cari?.last()?.id
                ]
                def mappCust = MappingCustVehicle.createCriteria().list {
                    eq("customerVehicle",cari?.last()?.customerVehicle);
                    order("dateCreated","desc");
                }
                if(mappCust){
                    for (find in mappCust){
                        balik << [
                                id : find?.customer?.id
                        ]
                        if(find?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase("Pemilik") || find?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase("Pemilik dan Penanggung Jawab")){
                            idPemilik = find?.customer?.id;
                            break;
                        }
                    }
                    balik << [
                            id : idPemilik
                    ]
                }
            }
        }
        render balik as JSON
    }

}