package com.kombos.reception

import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomerVehicle
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class TowingMalamController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def towingMalamService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render towingMalamService.datatablesList(params) as JSON
    }

    def create() {
        [towingMalamInstance: new TowingMalam(params)]
    }

    def save() {
        HistoryCustomerVehicle historyCustomerVehicle = HistoryCustomerVehicle?.createCriteria()?.get {
            eq("staDel","0");
            kodeKotaNoPol{
                eq("id",Long.parseLong(params.noPol.id))
            }
            ilike("t183NoPolTengah","%"+params?.t183NoPolTengah+"%")
            ilike("t183NoPolBelakang","%"+params?.t183NoPolBelakang+"%")
            maxResults(1);
        }
        params.customerVehicle = historyCustomerVehicle?.customerVehicle
        params.t421NamaPenerima = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.staDel = '0'
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.lastUpdProcess = "INSERT"
        params.t421TglJamDatang = new Date()
        params?.companyDealer = session?.userCompanyDealer

        def towingMalamInstance = new TowingMalam(params)
        if (!towingMalamInstance.save(flush: true)) {
            render(view: "create", model: [towingMalamInstance: towingMalamInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), towingMalamInstance.id])
        redirect(action: "show", id: towingMalamInstance.id)
    }

    def show(Long id) {
        def towingMalamInstance = TowingMalam.get(id)
        if (!towingMalamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), id])
            redirect(action: "list")
            return
        }

        [towingMalamInstance: towingMalamInstance]
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def towingMalamInstance = TowingMalam.get(id)
        if (!towingMalamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), id])
            redirect(action: "list")
            return
        }

        [towingMalamInstance: towingMalamInstance]
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def towingMalamInstance = TowingMalam.get(id)
        if (!towingMalamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (towingMalamInstance.version > version) {

                towingMalamInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'towingMalam.label', default: 'TowingMalam')] as Object[],
                        "Another user has updated this TowingMalam while you were editing")
                render(view: "edit", model: [towingMalamInstance: towingMalamInstance])
                return
            }
        }

        towingMalamInstance.properties = params

        if (!towingMalamInstance.save(flush: true)) {
            render(view: "edit", model: [towingMalamInstance: towingMalamInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), towingMalamInstance.id])
        redirect(action: "show", id: towingMalamInstance.id)
    }

    def delete(Long id) {
        def towingMalamInstance = TowingMalam.get(id)
        if (!towingMalamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), id])
            redirect(action: "list")
            return
        }

        try {
            towingMalamInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'towingMalam.label', default: 'TowingMalam'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(TowingMalam, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        try {
            datatablesUtilService.updateField(TowingMalam, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def findNoPol(){
        def result = towingMalamService.findNoPol(params)

        if(result!=null){
            render result as JSON
        }
    }
}