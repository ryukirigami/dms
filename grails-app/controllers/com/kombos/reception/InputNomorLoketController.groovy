package com.kombos.reception

import com.kombos.administrasi.Audio
import com.kombos.administrasi.Loket
import com.kombos.board.JPB
import com.kombos.maintable.MappingCustVehicle
import com.kombos.maintable.TujuanKedatangan
import grails.converters.JSON
import javazoom.jl.decoder.JavaLayerException
import javazoom.jl.player.Player
import java.text.DateFormat
import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class InputNomorLoketController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.TIME_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions =['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    def waktuSekarang = df.format(new Date())
    String awal = waktuSekarang+" 00:00"
    String akhir = waktuSekarang+" 24:00"
    Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
    Date dateAkhir= new Date().parse("dd/MM/yyyy HH:mm",akhir)


    def index() {
    }

    def list(Integer max) {
        def currentNoAntrian = callNextCustomerIn()
        def loket = Loket.get(params.loketId.toLong())
        [idLoket : loket?.id, loketId: loket?.m404NoLoket, kategori: params.kategori, currentNoAntrian: currentNoAntrian]
    }

    def callNextCustomerIn() {
        def c=CustomerIn.findByStaDelAndT400StaReceptionAndDateCreatedBetweenAndCompanyDealer("0","1",dateAwal,dateAkhir,session?.userCompanyDealer);
        return c?.t400NomorAntrian
    }

    def reCall(){
        def result = [:]
        result.ada = false
        def custIn = CustomerIn.findByT400NomorAntrianIlikeAndCompanyDealerAndDateCreatedGreaterThanEquals("%"+params?.idLoket+"%",session?.userCompanyDealer,new Date()?.clearTime())
        def urlz = params?.urlz?.toString()?.substring(0,params?.urlz?.toString()?.indexOf('dms'))
        println urlz
        String noAntri = ""
        if(custIn){
            noAntri = custIn?.t400NomorAntrian
            if(!noAntri?.toUpperCase()?.contains("TIDAK ADA ANTRIAN") || noAntri==null){
                result.ada = true
                def listUrl = []
                listUrl << urlz+"dms/audio/Next.mp3"
                noAntri.each {
                    def judul = Audio.findByM405TextIlike("%"+it+"%")
                    if(judul){
                        listUrl << urlz+"dms/audio/"+judul?.audioPath
                    }
                }
                listUrl << urlz+"dms/audio/Loket.mp3"
                def juduls = Audio.findByM405TextIlike("%"+custIn?.loket?.m404NoLoket+"%");
                if(juduls){
                    listUrl << urlz+"dms/audio/"+juduls?.audioPath
                }
                result.urls = listUrl
            }
        }
        result.noAntri = noAntri
        render result as JSON
    }

    def callCustomer() {
        def urlz = params?.urlz?.toString()?.substring(0,params?.urlz?.toString()?.indexOf('dms'))
        println urlz
        def result = [:]
        result.ada = false
        def c = CustomerIn.createCriteria()
        def results = c.list {
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
            inList("t400StaReception",["0","1"])
            eq("companyDealer",session?.userCompanyDealer)
            order("t400NomorAntrian")
        }
//
        def noAntri = "W002"
        def custIn = results?.size()>0 ? results?.first() : new CustomerIn()
        def tujuan = ""
        tujuan = custIn?.tujuanKedatangan?.m400Tujuan
        def loket = Loket.get(params.id);
        if(custIn){
            if(tujuan=="BP" && params.idK=="BP"){
                custIn?.t400StaReception = "2"
                noAntri = custIn?.t400NomorAntrian ? custIn?.t400NomorAntrian:"Tidak Ada Antrian BP"
                if(params?.id){
                    def noLoket = Loket.get(params.id);
                    custIn?.loket = noLoket
                }

            }else if (tujuan=="GR" && params.idK=="GR"){
                custIn?.t400StaReception = "2"
                noAntri = custIn?.t400NomorAntrian ? custIn?.t400NomorAntrian:"Tidak Ada Antrian GR"
                if(params?.id){
                    def noLoket = Loket.get(params.id);
                    custIn?.loket = noLoket
                }
            }else if (tujuan=="BP" && params.idK=="GR"){
                noAntri = "Tidak Ada Antrian"
            }else if (tujuan=="GR" && params.idK=="BP"){
                noAntri = "Tidak Ada Antrian"
            }
            else{
                noAntri = custIn?.t400NomorAntrian ? null:"Tidak Ada Antrian"
            }
            custIn?.lastUpdated = datatablesUtilService?.syncTime()
            custIn?.save(flush: true)
            if(!noAntri?.toUpperCase()?.contains("TIDAK ADA ANTRIAN") || noAntri==null){
                result.ada = true
                def listUrl = []
                listUrl << urlz+"dms/audio/Next.mp3"
                noAntri.each {
                    def judul = Audio.findByM405TextIlike("%"+it+"%")
                    if(judul){
                        listUrl << urlz+"dms/audio/"+judul?.audioPath
                    }
                }
                listUrl << urlz+"dms/audio/Loket.mp3"
                def juduls = Audio.findByM405TextIlike("%"+custIn?.loket?.m404NoLoket+"%");
                if(juduls){
                    listUrl << urlz+"dms/audio/"+juduls?.audioPath
                }
                result.urls = listUrl
            }
        }
        result.noAntri = noAntri
        render result as JSON
    }

    def callSpecialCustomer() {
        def urlz = params?.urlz?.toString()?.substring(0,params?.urlz?.toString()?.indexOf('dms'))
        println urlz
        def result = [:]
        def custIn = CustomerIn.get(params.id.toLong())
        def loket = Loket.get(params?.idLoket?.toLong())
        custIn.t400StaReception = "2"
        custIn.loket= loket
        custIn.lastUpdated = datatablesUtilService?.syncTime()
        custIn?.save(flush : true)
        def noAntri = custIn?.t400NomorAntrian
        if(!noAntri?.toUpperCase()?.contains("Tidak Ada Antrian") || noAntri!=null){
            def listUrl = []
            listUrl << urlz+"dms/audio/Next.mp3"
            noAntri.each {
                def judul = Audio.findByM405TextIlike("%"+it+"%")
                if(judul){
                    listUrl << urlz+"dms/audio/"+judul?.audioPath
                }
            }
            listUrl << urlz+"dms/audio/Loket.mp3"
            def juduls = Audio.findByM405TextIlike("%"+custIn?.loket?.m404NoLoket+"%");
            if(juduls){
                listUrl << urlz+"dms/audio/"+juduls?.audioPath
            }
            result.urls = listUrl
        }
        result.noAntri = noAntri
        render result as JSON
    }

    def datatablesList() {

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret

        session.exportParams=params
        def c = CustomerIn.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
            inList("t400StaReception",["0","1"])
            tujuanKedatangan{
                eq("m400StaDel","0");
                ilike("m400Tujuan","%"+params?.kategori+"%");
            }
            eq("companyDealer",session?.userCompanyDealer)
            order("t400NomorAntrian")
        }

        def rows = []

        JPB jpb
        Appointment appointment

        results.each {
            CustomerIn customerIn = it

            jpb = JPB.findByReception(customerIn?.reception)
            appointment = Appointment.findByCustomerVehicle(customerIn.customerVehicle)
            rows << [

                    id: customerIn.id,

                    t400NomorAntrian: customerIn?.t400NomorAntrian,

                    fullNoPol: customerIn?.customerVehicle ? customerIn?.customerVehicle?.getCurrentCondition()?.fullNoPol : customerIn?.t400noPol,

                    t400TglCetakNoAntrian: customerIn?.dateCreated?.format("HH:mm:yy"),

                    t301TglJamRencana: appointment?appointment?.t301TglJamRencana?.format(dateFormat):"", //janjiDatang

                    m404NoLoket: customerIn?.loket?.m404NoLoket ? customerIn?.loket?.m404NoLoket : "-",

                    t400TglJamReception: customerIn?.t400TglJamReception ? customerIn?.t400TglJamReception?.format(dateFormat) : "-",

                    t451TglJamPlan: jpb?jpb?.t451TglJamPlan?.format(dateFormat):"-"

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

    def datatablesCustomerList() {
        def ret
        session.exportParams=params
        def c = CustomerIn.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
            inList("t400StaReception",["0","1"])
            tujuanKedatangan{
                eq("m400StaDel","0");
                ilike("m400Tujuan","%"+params?.kategori+"%");
            }
            eq("companyDealer",session?.userCompanyDealer)
            order("t400NomorAntrian")
            if(params."sCriteria_noAntri"){
                ilike("t400NomorAntrian","%" + (params."sCriteria_noAntri" as String) + "%")
            }
            if(params."sCriteria_noPol"){
                reception{
                    historyCustomerVehicle{
                        ilike("fullNoPol","%" + (params."sCriteria_noPol" as String) + "%")
                    }
                }
            }
            if(params."sCriteria_customer"){
                reception{
                    historyCustomer{
                        ilike("fullNama","%" + (params."sCriteria_customer" as String) + "%")
                    }
                }
            }

        }

        def rows = []

        results.each {
            CustomerIn customerIn = it
            def cust = ""
            if(customerIn?.customerVehicle){
                def mapping = MappingCustVehicle.createCriteria().list {
                    eq("customerVehicle",customerIn?.customerVehicle);
                    order("dateCreated","desc");
                }
                for(find in mapping){
                    if(find?.customer?.histories?.last()?.peranCustomer?.m115NamaPeranCustomer?.toLowerCase()?.contains("pemilik") || find?.customer?.histories?.last()?.peranCustomer?.m115NamaPeranCustomer?.toLowerCase()?.contains("penanggung jawab")){
                        cust = find?.customer?.histories?.last()?.fullNama
                        break
                    }else{
                        cust = find?.customer?.histories?.last()?.fullNama
                    }
                }
            }

            rows << [

                    id: customerIn.id,

                    t400NomorAntrian: customerIn?.t400NomorAntrian,

                    fullNoPol: customerIn?.customerVehicle ? customerIn?.customerVehicle?.getCurrentCondition()?.fullNoPol : customerIn?.t400noPol,

                    namaCustomer: cust

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

    def listCustomer(){

    }
}
