package com.kombos.reception

import com.kombos.administrasi.KodeKotaNoPol
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.StatusActual
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class ViewProgressKendaraanController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [identity : new Random().nextBytes()]
    }
    def editRevisi(){
        DateFormat df = new SimpleDateFormat("yyyy-M-d")
        def data = Reception.findByT401NoWO(params.noWo)
        String tanggal = params.tglRevisi + " " + params.jamRevisi + ":" + params.menitRevisi
        def rec = Reception.get(data.id)
        rec?.t401TglJamJanjiPenyerahan = new Date().parse('dd-MM-yyyy HH:mm', tanggal)
        rec?.lastUpdated = datatablesUtilService?.syncTime()
        rec?.save(flush: true)

        render "ok"
    }
    def revisiJanji(){
        def data = Reception.findByT401NoWO(params.noWo)
        [namaCustomer : data?.historyCustomer.fullNama, alamat : data.historyCustomer.t182Alamat,noWo:params.noWo,
                telp : data?.historyCustomer?.t182NoTelpRumah, tglPenyerahan : data?.t401TglJamJanjiPenyerahan,
                mobil : data?.historyCustomerVehicle.fullModelCode?.baseModel?.m102NamaBaseModel]
    }

    def datatablesList() {
        def data = "", dana = "", alasan=""
        boolean JD = false
        boolean AD = false
        Date janjiDate = null
        Date aktualDate = null
        Date actualClockOn = null
        Date actualPanggil = null
        Date actualClockOff = null
        Date datangDate = null
        if(params.kata_kunci!=""){
            if(params.kriteria=='noPol'){
                String nopol = params.kata_kunci
                data = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(HistoryCustomerVehicle.findByFullNoPol(params.kata_kunci),"0","0");
            }else{
                data = Reception.findByT401NoWO(params.kata_kunci)
                dana = JPB.findByReception(data)?.id
            }

            if(data){
                janjiDate =  data?.t401TglJamJanjiPenyerahan
                aktualDate =  new Date()
                datangDate =  data?.customerIn?.t400TglJamReception
                actualClockOn = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(1))?.t452TglJam
                actualPanggil = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452TglJam
                actualClockOff = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(4))?.t452TglJam
                alasan = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452Ket
                if(aktualDate.getDay()==1){
                    AD = true
                }
                if(janjiDate.getDay()==1){
                    JD = true
                }
            }
        }

        def ret
        def rows = []
        if(AD==true && JD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7 && janjiDate.getHours()==7?'E':(aktualDate?.getHours()==7?'Y':(janjiDate?.getHours()==7?'J':'')),
                    jam8: aktualDate?.getHours()==8 && janjiDate.getHours()==8?'E':(aktualDate?.getHours()==8?'Y':(janjiDate?.getHours()==8?'J':'')),
                    jam9: aktualDate?.getHours()==9 && janjiDate.getHours()==9?'E':(aktualDate?.getHours()==9?'Y':(janjiDate?.getHours()==9?'J':'')),
                    jam10: aktualDate?.getHours()==10 && janjiDate.getHours()==10?'E':(aktualDate?.getHours()==10?'Y':(janjiDate?.getHours()==10?'J':'')),
                    jam11: aktualDate?.getHours()==11 && janjiDate.getHours()==11?'E':(aktualDate?.getHours()==11?'Y':(janjiDate?.getHours()==11?'J':'')),
                    jam12: aktualDate?.getHours()==12 && janjiDate.getHours()==12?'E':(aktualDate?.getHours()==12?'Y':(janjiDate?.getHours()==12?'J':'')),
                    jam13: aktualDate?.getHours()==13 && janjiDate.getHours()==13?'E':(aktualDate?.getHours()==13?'Y':(janjiDate?.getHours()==13?'J':'')),
                    jam14: aktualDate?.getHours()==14 && janjiDate.getHours()==14?'E':(aktualDate?.getHours()==14?'Y':(janjiDate?.getHours()==14?'J':'')),
                    jam15: aktualDate?.getHours()==15 && janjiDate.getHours()==15?'E':(aktualDate?.getHours()==15?'Y':(janjiDate?.getHours()==15?'J':'')),
                    jam16: aktualDate?.getHours()==16 && janjiDate.getHours()==16?'E':(aktualDate?.getHours()==16?'Y':(janjiDate?.getHours()==16?'J':'')),
                    menitAktual : aktualDate?.getMinutes(),
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 1,
                    jamJanji : janjiDate?.getHours(),

                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan

            ]
        }else if(AD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7?'Y':'',
                    jam8: aktualDate?.getHours()==8?'Y':'',
                    jam9: aktualDate?.getHours()==9?'Y':'',
                    jam10: aktualDate?.getHours()==10?'Y':'',
                    jam11: aktualDate?.getHours()==11?'Y':'',
                    jam12: aktualDate?.getHours()==12?'Y':'',
                    jam13: aktualDate?.getHours()==13?'Y':'',
                    jam14: aktualDate?.getHours()==14?'Y':'',
                    jam15: aktualDate?.getHours()==15?'Y':'',
                    jam16: aktualDate?.getHours()==16?'Y':'',
                    menitAktual : aktualDate?.getMinutes(),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 1,
                    jamJanji : janjiDate?.getHours(),

                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(JD==true){
            rows << [
                    jam7: janjiDate?.getHours()==7?'J':'',
                    jam8: janjiDate?.getHours()==8?'J':'',
                    jam9: janjiDate?.getHours()==9?'J':'',
                    jam10: janjiDate?.getHours()==10?'J':'',
                    jam11: janjiDate?.getHours()==11?'J':'',
                    jam12: janjiDate?.getHours()==12?'J':'',
                    jam13: janjiDate?.getHours()==13?'J':'',
                    jam14: janjiDate?.getHours()==14?'J':'',
                    jam15: janjiDate?.getHours()==15?'J':'',
                    jam16: janjiDate?.getHours()==16?'J':'',
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 1,
                    jamJanji : janjiDate?.getHours(),

                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else{
            rows << [
                    jam7: '-',
                    jam8: ' ',
                    jam9: ' ',
                    jam10: ' ',
                    jam11: ' ',
                    jam12: ' ',
                    jam13: ' ',
                    jam14: ' ',
                    jam15: ' ',
                    jam16: ' ',

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 1,
                    jamJanji : janjiDate?.getHours(),

                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }
    def datatablesList2() {
        def data = "", dana = "", alasan = ""
        boolean JD = false
        boolean AD = false
        Date janjiDate = null
        Date aktualDate = null
        Date datangDate = null
        Date actualClockOn = null
        Date actualPanggil = null
        Date actualClockOff = null
        if(params.kata_kunci!=""){
            if(params.kriteria=='noPol'){
                String nopol = params.kata_kunci
                data = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(HistoryCustomerVehicle.findByFullNoPol(params.kata_kunci),"0","0")
            }else{
                data = Reception.findByT401NoWO(params.kata_kunci)
                dana = JPB.findByReception(data)?.id
            }

            if(data){
                janjiDate =  data?.t401TglJamJanjiPenyerahan
                aktualDate =  new Date()
                datangDate =  data?.customerIn?.t400TglJamReception
                actualClockOn = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(1))?.t452TglJam
                actualPanggil = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452TglJam
                actualClockOff = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(4))?.t452TglJam
                alasan = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452Ket
                if(aktualDate.getDay()==2){
                    AD = true
                }
                if(janjiDate.getDay()==2){
                    JD = true
                }
            }
        }

        def ret
        def rows = []
        if(AD==true && JD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7 && janjiDate.getHours()==7?'E':(aktualDate?.getHours()==7?'Y':(janjiDate?.getHours()==7?'J':'')),
                    jam8: aktualDate?.getHours()==8 && janjiDate.getHours()==8?'E':(aktualDate?.getHours()==8?'Y':(janjiDate?.getHours()==8?'J':'')),
                    jam9: aktualDate?.getHours()==9 && janjiDate.getHours()==9?'E':(aktualDate?.getHours()==9?'Y':(janjiDate?.getHours()==9?'J':'')),
                    jam10: aktualDate?.getHours()==10 && janjiDate.getHours()==10?'E':(aktualDate?.getHours()==10?'Y':(janjiDate?.getHours()==10?'J':'')),
                    jam11: aktualDate?.getHours()==11 && janjiDate.getHours()==11?'E':(aktualDate?.getHours()==11?'Y':(janjiDate?.getHours()==11?'J':'')),
                    jam12: aktualDate?.getHours()==12 && janjiDate.getHours()==12?'E':(aktualDate?.getHours()==12?'Y':(janjiDate?.getHours()==12?'J':'')),
                    jam13: aktualDate?.getHours()==13 && janjiDate.getHours()==13?'E':(aktualDate?.getHours()==13?'Y':(janjiDate?.getHours()==13?'J':'')),
                    jam14: aktualDate?.getHours()==14 && janjiDate.getHours()==14?'E':(aktualDate?.getHours()==14?'Y':(janjiDate?.getHours()==14?'J':'')),
                    jam15: aktualDate?.getHours()==15 && janjiDate.getHours()==15?'E':(aktualDate?.getHours()==15?'Y':(janjiDate?.getHours()==15?'J':'')),
                    jam16: aktualDate?.getHours()==16 && janjiDate.getHours()==16?'E':(aktualDate?.getHours()==16?'Y':(janjiDate?.getHours()==16?'J':'')),
                    menitAktual : aktualDate?.getMinutes(),
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 2,
                    jamJanji : janjiDate?.getHours(),

                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(AD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7?'Y':'',
                    jam8: aktualDate?.getHours()==8?'Y':'',
                    jam9: aktualDate?.getHours()==9?'Y':'',
                    jam10: aktualDate?.getHours()==10?'Y':'',
                    jam11: aktualDate?.getHours()==11?'Y':'',
                    jam12: aktualDate?.getHours()==12?'Y':'',
                    jam13: aktualDate?.getHours()==13?'Y':'',
                    jam14: aktualDate?.getHours()==14?'Y':'',
                    jam15: aktualDate?.getHours()==15?'Y':'',
                    jam16: aktualDate?.getHours()==16?'Y':'',
                    menitAktual : aktualDate?.getMinutes(),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 2,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(JD==true){
            rows << [
                    jam7: janjiDate?.getHours()==7?'J':'',
                    jam8: janjiDate?.getHours()==8?'J':'',
                    jam9: janjiDate?.getHours()==9?'J':'',
                    jam10: janjiDate?.getHours()==10?'J':'',
                    jam11: janjiDate?.getHours()==11?'J':'',
                    jam12: janjiDate?.getHours()==12?'J':'',
                    jam13: janjiDate?.getHours()==13?'J':'',
                    jam14: janjiDate?.getHours()==14?'J':'',
                    jam15: janjiDate?.getHours()==15?'J':'',
                    jam16: janjiDate?.getHours()==16?'J':'',
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 2,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else{
            rows << [
                    jam7: '-',
                    jam8: ' ',
                    jam9: ' ',
                    jam10: ' ',
                    jam11: ' ',
                    jam12: ' ',
                    jam13: ' ',
                    jam14: ' ',
                    jam15: ' ',
                    jam16: ' ',

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 2,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }
    def datatablesList3() {
        def data = "", dana = "", alasan=""
        boolean JD = false
        boolean AD = false
        Date janjiDate = null
        Date aktualDate = null
        Date datangDate = null
        Date actualClockOn = null
        Date actualPanggil = null
        Date actualClockOff = null
        if(params.kata_kunci!=""){
            if(params.kriteria=='noPol'){
                String nopol = params.kata_kunci
                data = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(HistoryCustomerVehicle.findByFullNoPol(params.kata_kunci),"0","0");
            }else{
                data = Reception.findByT401NoWO(params.kata_kunci)
                dana = JPB.findByReception(data)?.id
            }

            if(data){
                janjiDate =  data?.t401TglJamJanjiPenyerahan
                aktualDate =  new Date()
                datangDate =  data?.customerIn?.t400TglJamReception
                actualClockOn = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(1))?.t452TglJam
                actualPanggil = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452TglJam
                actualClockOff = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(4))?.t452TglJam
                alasan = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452Ket
                if(aktualDate.getDay()==3){
                    AD = true
                }
                if(janjiDate.getDay()==3){
                    JD = true
                }
            }
        }

        def ret
        def rows = []
        if(AD==true && JD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7 && janjiDate.getHours()==7?'E':(aktualDate?.getHours()==7?'Y':(janjiDate?.getHours()==7?'J':'')),
                    jam8: aktualDate?.getHours()==8 && janjiDate.getHours()==8?'E':(aktualDate?.getHours()==8?'Y':(janjiDate?.getHours()==8?'J':'')),
                    jam9: aktualDate?.getHours()==9 && janjiDate.getHours()==9?'E':(aktualDate?.getHours()==9?'Y':(janjiDate?.getHours()==9?'J':'')),
                    jam10: aktualDate?.getHours()==10 && janjiDate.getHours()==10?'E':(aktualDate?.getHours()==10?'Y':(janjiDate?.getHours()==10?'J':'')),
                    jam11: aktualDate?.getHours()==11 && janjiDate.getHours()==11?'E':(aktualDate?.getHours()==11?'Y':(janjiDate?.getHours()==11?'J':'')),
                    jam12: aktualDate?.getHours()==12 && janjiDate.getHours()==12?'E':(aktualDate?.getHours()==12?'Y':(janjiDate?.getHours()==12?'J':'')),
                    jam13: aktualDate?.getHours()==13 && janjiDate.getHours()==13?'E':(aktualDate?.getHours()==13?'Y':(janjiDate?.getHours()==13?'J':'')),
                    jam14: aktualDate?.getHours()==14 && janjiDate.getHours()==14?'E':(aktualDate?.getHours()==14?'Y':(janjiDate?.getHours()==14?'J':'')),
                    jam15: aktualDate?.getHours()==15 && janjiDate.getHours()==15?'E':(aktualDate?.getHours()==15?'Y':(janjiDate?.getHours()==15?'J':'')),
                    jam16: aktualDate?.getHours()==16 && janjiDate.getHours()==16?'E':(aktualDate?.getHours()==16?'Y':(janjiDate?.getHours()==16?'J':'')),
                    menitAktual : aktualDate?.getMinutes(),
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 3,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(AD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7?'Y':'',
                    jam8: aktualDate?.getHours()==8?'Y':'',
                    jam9: aktualDate?.getHours()==9?'Y':'',
                    jam10: aktualDate?.getHours()==10?'Y':'',
                    jam11: aktualDate?.getHours()==11?'Y':'',
                    jam12: aktualDate?.getHours()==12?'Y':'',
                    jam13: aktualDate?.getHours()==13?'Y':'',
                    jam14: aktualDate?.getHours()==14?'Y':'',
                    jam15: aktualDate?.getHours()==15?'Y':'',
                    jam16: aktualDate?.getHours()==16?'Y':'-',
                    menitAktual : aktualDate?.getMinutes(),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 3,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(JD==true){
            rows << [
                    jam7: janjiDate?.getHours()==7?'J':'',
                    jam8: janjiDate?.getHours()==8?'J':'',
                    jam9: janjiDate?.getHours()==9?'J':'',
                    jam10: janjiDate?.getHours()==10?'J':'',
                    jam11: janjiDate?.getHours()==11?'J':'',
                    jam12: janjiDate?.getHours()==12?'J':'',
                    jam13: janjiDate?.getHours()==13?'J':'',
                    jam14: janjiDate?.getHours()==14?'J':'',
                    jam15: janjiDate?.getHours()==15?'J':'',
                    jam16: janjiDate?.getHours()==16?'J':'-',
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 3,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else{
            rows << [
                    jam7: '-',
                    jam8: ' ',
                    jam9: ' ',
                    jam10: ' ',
                    jam11: ' ',
                    jam12: ' ',
                    jam13: ' ',
                    jam14: ' ',
                    jam15: ' ',
                    jam16: ' ',

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 3,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }
    def datatablesList4() {
        def data = "", dana = "",alasan=""
        boolean JD = false
        boolean AD = false
        Date janjiDate = null
        Date aktualDate = null
        Date datangDate = null
        Date actualClockOn = null
        Date actualPanggil = null
        Date actualClockOff = null
        if(params.kata_kunci!=""){
            if(params.kriteria=='noPol'){
                String nopol = params.kata_kunci
                data = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(HistoryCustomerVehicle.findByFullNoPol(params.kata_kunci),"0","0")
            }else{
                data = Reception.findByT401NoWO(params.kata_kunci)
                dana = JPB.findByReception(data)?.id
            }

            if(data){
                janjiDate =  data?.t401TglJamJanjiPenyerahan
                aktualDate =  new Date()
                datangDate =  data?.customerIn?.t400TglJamReception
                actualClockOn = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(1))?.t452TglJam
                actualPanggil = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452TglJam
                actualClockOff = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(4))?.t452TglJam
                alasan = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452Ket
                if(aktualDate.getDay()==4){
                    AD = true
                }
                if(janjiDate.getDay()==4){
                    JD = true
                }
            }
        }

        def ret
        def rows = []
        if(AD==true && JD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7 && janjiDate.getHours()==7?'E':(aktualDate?.getHours()==7?'Y':(janjiDate?.getHours()==7?'J':'')),
                    jam8: aktualDate?.getHours()==8 && janjiDate.getHours()==8?'E':(aktualDate?.getHours()==8?'Y':(janjiDate?.getHours()==8?'J':'')),
                    jam9: aktualDate?.getHours()==9 && janjiDate.getHours()==9?'E':(aktualDate?.getHours()==9?'Y':(janjiDate?.getHours()==9?'J':'')),
                    jam10: aktualDate?.getHours()==10 && janjiDate.getHours()==10?'E':(aktualDate?.getHours()==10?'Y':(janjiDate?.getHours()==10?'J':'')),
                    jam11: aktualDate?.getHours()==11 && janjiDate.getHours()==11?'E':(aktualDate?.getHours()==11?'Y':(janjiDate?.getHours()==11?'J':'')),
                    jam12: aktualDate?.getHours()==12 && janjiDate.getHours()==12?'E':(aktualDate?.getHours()==12?'Y':(janjiDate?.getHours()==12?'J':'')),
                    jam13: aktualDate?.getHours()==13 && janjiDate.getHours()==13?'E':(aktualDate?.getHours()==13?'Y':(janjiDate?.getHours()==13?'J':'')),
                    jam14: aktualDate?.getHours()==14 && janjiDate.getHours()==14?'E':(aktualDate?.getHours()==14?'Y':(janjiDate?.getHours()==14?'J':'')),
                    jam15: aktualDate?.getHours()==15 && janjiDate.getHours()==15?'E':(aktualDate?.getHours()==15?'Y':(janjiDate?.getHours()==15?'J':'')),
                    jam16: aktualDate?.getHours()==16 && janjiDate.getHours()==16?'E':(aktualDate?.getHours()==16?'Y':(janjiDate?.getHours()==16?'J':'')),
                    menitAktual : aktualDate?.getMinutes(),
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 4,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(AD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7?'Y':'',
                    jam8: aktualDate?.getHours()==8?'Y':'',
                    jam9: aktualDate?.getHours()==9?'Y':'',
                    jam10: aktualDate?.getHours()==10?'Y':'',
                    jam11: aktualDate?.getHours()==11?'Y':'',
                    jam12: aktualDate?.getHours()==12?'Y':'',
                    jam13: aktualDate?.getHours()==13?'Y':'',
                    jam14: aktualDate?.getHours()==14?'Y':'',
                    jam15: aktualDate?.getHours()==15?'Y':'',
                    jam16: aktualDate?.getHours()==16?'Y':'-',
                    menitAktual : aktualDate?.getMinutes(),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 4,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(JD==true){
            rows << [
                    jam7: janjiDate?.getHours()==7?'J':'',
                    jam8: janjiDate?.getHours()==8?'J':'',
                    jam9: janjiDate?.getHours()==9?'J':'',
                    jam10: janjiDate?.getHours()==10?'J':'',
                    jam11: janjiDate?.getHours()==11?'J':'',
                    jam12: janjiDate?.getHours()==12?'J':'',
                    jam13: janjiDate?.getHours()==13?'J':'',
                    jam14: janjiDate?.getHours()==14?'J':'',
                    jam15: janjiDate?.getHours()==15?'J':'',
                    jam16: janjiDate?.getHours()==16?'J':'-',
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 4,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else{
            rows << [
                    jam7: '-',
                    jam8: ' ',
                    jam9: ' ',
                    jam10: ' ',
                    jam11: ' ',
                    jam12: ' ',
                    jam13: ' ',
                    jam14: ' ',
                    jam15: ' ',
                    jam16: ' ',

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 4,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }
    def datatablesList5() {
        def data = "", dana = "",alasan=""
        boolean JD = false
        boolean AD = false
        Date janjiDate = null
        Date aktualDate = null
        Date datangDate = null
        Date actualClockOn = null
        Date actualPanggil = null
        Date actualClockOff = null
        if(params.kata_kunci!=""){
            if(params.kriteria=='noPol'){
                String nopol = params.kata_kunci
                data = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(HistoryCustomerVehicle.findByFullNoPol(params.kata_kunci),"0","0")
            }else{
                data = Reception.findByT401NoWO(params.kata_kunci)
                dana = JPB.findByReception(data)?.id
            }

            if(data){
                janjiDate =  data?.t401TglJamJanjiPenyerahan
                aktualDate =  new Date()
                datangDate =  data?.customerIn?.t400TglJamReception
                actualClockOn = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(1))?.t452TglJam
                actualPanggil = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452TglJam
                actualClockOff = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(4))?.t452TglJam
                alasan = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452Ket
                if(aktualDate.getDay()==5){
                    AD = true
                }
                if(janjiDate.getDay()==5){
                    JD = true
                }
            }
        }

        def ret
        def rows = []
        if(AD==true && JD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7 && janjiDate.getHours()==7?'E':(aktualDate?.getHours()==7?'Y':(janjiDate?.getHours()==7?'J':'')),
                    jam8: aktualDate?.getHours()==8 && janjiDate.getHours()==8?'E':(aktualDate?.getHours()==8?'Y':(janjiDate?.getHours()==8?'J':'')),
                    jam9: aktualDate?.getHours()==9 && janjiDate.getHours()==9?'E':(aktualDate?.getHours()==9?'Y':(janjiDate?.getHours()==9?'J':'')),
                    jam10: aktualDate?.getHours()==10 && janjiDate.getHours()==10?'E':(aktualDate?.getHours()==10?'Y':(janjiDate?.getHours()==10?'J':'')),
                    jam11: aktualDate?.getHours()==11 && janjiDate.getHours()==11?'E':(aktualDate?.getHours()==11?'Y':(janjiDate?.getHours()==11?'J':'')),
                    jam12: aktualDate?.getHours()==12 && janjiDate.getHours()==12?'E':(aktualDate?.getHours()==12?'Y':(janjiDate?.getHours()==12?'J':'')),
                    jam13: aktualDate?.getHours()==13 && janjiDate.getHours()==13?'E':(aktualDate?.getHours()==13?'Y':(janjiDate?.getHours()==13?'J':'')),
                    jam14: aktualDate?.getHours()==14 && janjiDate.getHours()==14?'E':(aktualDate?.getHours()==14?'Y':(janjiDate?.getHours()==14?'J':'')),
                    jam15: aktualDate?.getHours()==15 && janjiDate.getHours()==15?'E':(aktualDate?.getHours()==15?'Y':(janjiDate?.getHours()==15?'J':'')),
                    jam16: aktualDate?.getHours()==16 && janjiDate.getHours()==16?'E':(aktualDate?.getHours()==16?'Y':(janjiDate?.getHours()==16?'J':'')),
                    menitAktual : aktualDate?.getMinutes(),
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),
                    hariJanji : janjiDate?.getDay(),
                    hariIni : 5,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(AD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7?'Y':'',
                    jam8: aktualDate?.getHours()==8?'Y':'',
                    jam9: aktualDate?.getHours()==9?'Y':'',
                    jam10: aktualDate?.getHours()==10?'Y':'',
                    jam11: aktualDate?.getHours()==11?'Y':'',
                    jam12: aktualDate?.getHours()==12?'Y':'',
                    jam13: aktualDate?.getHours()==13?'Y':'',
                    jam14: aktualDate?.getHours()==14?'Y':'',
                    jam15: aktualDate?.getHours()==15?'Y':'',
                    jam16: aktualDate?.getHours()==16?'Y':'-',
                    menitAktual : aktualDate?.getMinutes(),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),
                    hariJanji : janjiDate?.getDay(),
                    hariIni : 5,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(JD==true){
            rows << [
                    jam7: janjiDate?.getHours()==7?'J':'',
                    jam8: janjiDate?.getHours()==8?'J':'',
                    jam9: janjiDate?.getHours()==9?'J':'',
                    jam10: janjiDate?.getHours()==10?'J':'',
                    jam11: janjiDate?.getHours()==11?'J':'',
                    jam12: janjiDate?.getHours()==12?'J':'',
                    jam13: janjiDate?.getHours()==13?'J':'',
                    jam14: janjiDate?.getHours()==14?'J':'',
                    jam15: janjiDate?.getHours()==15?'J':'',
                    jam16: janjiDate?.getHours()==16?'J':'-',
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),
                    hariJanji : janjiDate?.getDay(),
                    hariIni : 5,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else{
            rows << [
                    jam7: '-',
                    jam8: ' ',
                    jam9: ' ',
                    jam10: ' ',
                    jam11: ' ',
                    jam12: ' ',
                    jam13: ' ',
                    jam14: ' ',
                    jam15: ' ',
                    jam16: ' ',
                    hariJanji : janjiDate?.getDay(),
                    hariIni : 5,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }
    def datatablesList6() {
        def data = "", dana = "", alasan = ""
        boolean JD = false
        boolean AD = false
        Date janjiDate = null
        Date aktualDate = null
        Date datangDate = null
        Date actualClockOn = null
        Date actualPanggil = null
        Date actualClockOff = null
        if(params.kata_kunci!=""){
            if(params.kriteria=='noPol'){
                String nopol = params.kata_kunci
                data = Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(HistoryCustomerVehicle.findByFullNoPol(params.kata_kunci),"0","0")
            }else{
                data = Reception.findByT401NoWO(params.kata_kunci)
                dana = JPB.findByReception(data)?.id
            }

            if(data){
                janjiDate =  data?.t401TglJamJanjiPenyerahan
                aktualDate =  new Date()
                datangDate =  data?.customerIn?.t400TglJamReception
                actualClockOn = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(1))?.t452TglJam
                actualPanggil = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452TglJam
                actualClockOff = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(4))?.t452TglJam
                alasan = Actual.findByReceptionAndStatusActual(data,StatusActual.findById(5))?.t452Ket
                if(aktualDate.getDay()==6){
                    AD = true
                }
                if(janjiDate.getDay()==6){
                    JD = true
                }
            }
        }

        def ret
        def rows = []
        if(AD==true && JD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7 && janjiDate.getHours()==7?'E':(aktualDate?.getHours()==7?'Y':(janjiDate?.getHours()==7?'J':'')),
                    jam8: aktualDate?.getHours()==8 && janjiDate.getHours()==8?'E':(aktualDate?.getHours()==8?'Y':(janjiDate?.getHours()==8?'J':'')),
                    jam9: aktualDate?.getHours()==9 && janjiDate.getHours()==9?'E':(aktualDate?.getHours()==9?'Y':(janjiDate?.getHours()==9?'J':'')),
                    jam10: aktualDate?.getHours()==10 && janjiDate.getHours()==10?'E':(aktualDate?.getHours()==10?'Y':(janjiDate?.getHours()==10?'J':'')),
                    jam11: aktualDate?.getHours()==11 && janjiDate.getHours()==11?'E':(aktualDate?.getHours()==11?'Y':(janjiDate?.getHours()==11?'J':'')),
                    jam12: aktualDate?.getHours()==12 && janjiDate.getHours()==12?'E':(aktualDate?.getHours()==12?'Y':(janjiDate?.getHours()==12?'J':'')),
                    jam13: aktualDate?.getHours()==13 && janjiDate.getHours()==13?'E':(aktualDate?.getHours()==13?'Y':(janjiDate?.getHours()==13?'J':'')),
                    jam14: aktualDate?.getHours()==14 && janjiDate.getHours()==14?'E':(aktualDate?.getHours()==14?'Y':(janjiDate?.getHours()==14?'J':'')),
                    jam15: aktualDate?.getHours()==15 && janjiDate.getHours()==15?'E':(aktualDate?.getHours()==15?'Y':(janjiDate?.getHours()==15?'J':'')),
                    jam16: aktualDate?.getHours()==16 && janjiDate.getHours()==16?'E':(aktualDate?.getHours()==16?'Y':(janjiDate?.getHours()==16?'J':'')),
                    menitAktual : aktualDate?.getMinutes(),
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 6,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan

            ]
        }else if(AD==true){
            rows << [
                    jam7: aktualDate?.getHours()==7?'Y':'',
                    jam8: aktualDate?.getHours()==8?'Y':'',
                    jam9: aktualDate?.getHours()==9?'Y':'',
                    jam10: aktualDate?.getHours()==10?'Y':'',
                    jam11: aktualDate?.getHours()==11?'Y':'',
                    jam12: aktualDate?.getHours()==12?'Y':'',
                    jam13: aktualDate?.getHours()==13?'Y':'',
                    jam14: aktualDate?.getHours()==14?'Y':'',
                    jam15: aktualDate?.getHours()==15?'Y':'',
                    jam16: aktualDate?.getHours()==16?'Y':'',
                    menitAktual : aktualDate?.getMinutes(),
                    jamAktual :  aktualDate.format('dd-MM-yyyy HH:mm'),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 6,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else if(JD==true){
            rows << [
                    jam7: janjiDate?.getHours()==7?'J':'',
                    jam8: janjiDate?.getHours()==8?'J':'',
                    jam9: janjiDate?.getHours()==9?'J':'',
                    jam10: janjiDate?.getHours()==10?'J':'',
                    jam11: janjiDate?.getHours()==11?'J':'',
                    jam12: janjiDate?.getHours()==12?'J':'',
                    jam13: janjiDate?.getHours()==13?'J':'',
                    jam14: janjiDate?.getHours()==14?'J':'',
                    jam15: janjiDate?.getHours()==15?'J':'',
                    jam16: janjiDate?.getHours()==16?'J':'',
                    janjiPenyerahan :  janjiDate?.format('dd-MM-yyyy HH:mm'),
                    menitJanji : janjiDate?.getMinutes(),

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 6,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan
            ]
        }else{
            rows << [
                    jam7: '-',
                    jam8: ' ',
                    jam9: ' ',
                    jam10: ' ',
                    jam11: ' ',
                    jam12: ' ',
                    jam13: ' ',
                    jam14: ' ',
                    jam15: ' ',
                    jam16: ' ',

                    hariJanji : janjiDate?.getDay(),
                    hariIni : 6,
                    jamJanji : janjiDate?.getHours(),
                    hariDatang : datangDate?.getDay(),
                    jamDatang : datangDate?.getHours(),
                    data:dana,

                    hariClockOn : actualClockOn?.getDay(),
                    hariClockOff : actualClockOff?.getDay(),
                    hariPanggil : actualPanggil?.getDay(),

                    jamClockOn : actualClockOn?.getHours(),
                    jamClockOff : actualClockOff?.getHours(),
                    jamPanggil : actualPanggil?.getHours(),
                    alasan:alasan

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }
    def viewData(){
        def data = ""
        if(params.kriteria=='noPol'){
            String nopol = params.kataKunci
            def datas = nopol.split("\\s+")
            def hcv = null
            if(datas.size()==3){
               hcv =  HistoryCustomerVehicle.findByKodeKotaNoPolAndT183NoPolTengahAndT183NoPolBelakangIlike(KodeKotaNoPol.findByM116ID(datas[0].trim()),datas[1].trim(),datas[2].trim())
            }
            data = JPB.findByReception(Reception.findByHistoryCustomerVehicleAndStaSaveAndStaDel(hcv,"0","0"))
        }else{
            data = JPB.findByReception(Reception.findByT401NoWO(params.kataKunci))
        }
        def res = [:]
        if(data){
            Date tgl = data?.reception?.t401TglJamJanjiPenyerahan
            if(tgl.getDay()==1){
                res.seninDate =  data?.reception?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy")
                res.selasaDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 1).format("dd/MM/yyyy")
                res.rabuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 2).format("dd/MM/yyyy")
                res.kamisDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 3).format("dd/MM/yyyy")
                res.jumatDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 4).format("dd/MM/yyyy")
                res.sabtuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 5).format("dd/MM/yyyy")
            }else  if(tgl.getDay()==2){
                res.seninDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 1).format("dd/MM/yyyy")
                res.selasaDate = (data?.reception?.t401TglJamJanjiPenyerahan + 0).format("dd/MM/yyyy")
                res.rabuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 1).format("dd/MM/yyyy")
                res.kamisDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 2).format("dd/MM/yyyy")
                res.jumatDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 3).format("dd/MM/yyyy")
                res.sabtuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 4).format("dd/MM/yyyy")
            }else  if(tgl.getDay()==3){
                res.seninDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 2).format("dd/MM/yyyy")
                res.selasaDate = (data?.reception?.t401TglJamJanjiPenyerahan - 1).format("dd/MM/yyyy")
                res.rabuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 0).format("dd/MM/yyyy")
                res.kamisDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 1).format("dd/MM/yyyy")
                res.jumatDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 2).format("dd/MM/yyyy")
                res.sabtuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 3).format("dd/MM/yyyy")
            }else  if(tgl.getDay()==4){
                res.seninDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 3).format("dd/MM/yyyy")
                res.selasaDate = (data?.reception?.t401TglJamJanjiPenyerahan - 2).format("dd/MM/yyyy")
                res.rabuDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 1).format("dd/MM/yyyy")
                res.kamisDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 0).format("dd/MM/yyyy")
                res.jumatDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 1).format("dd/MM/yyyy")
                res.sabtuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 2).format("dd/MM/yyyy")
            }else  if(tgl.getDay()==5){
                res.seninDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 4).format("dd/MM/yyyy")
                res.selasaDate = (data?.reception?.t401TglJamJanjiPenyerahan - 3).format("dd/MM/yyyy")
                res.rabuDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 2).format("dd/MM/yyyy")
                res.kamisDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 1).format("dd/MM/yyyy")
                res.jumatDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 0).format("dd/MM/yyyy")
                res.sabtuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 1).format("dd/MM/yyyy")
            }else  if(tgl.getDay()==6){
                res.seninDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 5).format("dd/MM/yyyy")
                res.selasaDate = (data?.reception?.t401TglJamJanjiPenyerahan - 4).format("dd/MM/yyyy")
                res.rabuDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 3).format("dd/MM/yyyy")
                res.kamisDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 2).format("dd/MM/yyyy")
                res.jumatDate =  (data?.reception?.t401TglJamJanjiPenyerahan - 1).format("dd/MM/yyyy")
                res.sabtuDate =  (data?.reception?.t401TglJamJanjiPenyerahan + 0).format("dd/MM/yyyy")
            }
            res.noWo = data?.reception?.t401NoWO
            res.noPol = data?.reception?.historyCustomerVehicle?.fullNoPol
            res.customerIn = data?.reception.customerIn?.t400TglJamReception.format("dd/MM/yyyy HH:mm")
            res.deliveryTime = data?.reception?.t401TglJamJanjiPenyerahan.format("dd/MM/yyyy HH:mm")
            def op = JobRCP.findAllByReception(data.reception)
            String operation = '<ul style="padding-left:20px">'
            op.each {
                operation += '<li>' + it.operation?.m053NamaOperation + '</li>'
            }
            operation += '</ul>'
            res.operation = operation
        }else{
            res.noWo = '-'
            res.noPol = '-'
            res.customerIn = '-'
            res.deliveryTime = '-'
            res.operation = '-'

            res.seninDate =  '-'
            res.selasaDate =  '-'
            res.rabuDate =  '-'
            res.kamisDate =  '-'
            res.jumatDate =  '-'
            res.sabtuDate =  '-'
        }
        render res as JSON
    }
    def daftar() {
        def res = [:]
        def opts = []
        if(params.kriteria == 'noPol'){
            def result = HistoryCustomerVehicle.findAllWhere(staDel : '0')
            result.each {
          //      opts << it?.fullNoPol
            }
        }else{
            def result = Reception.findAllWhere(staDel : '0',staSave: '0')
            result.each {
            //    opts << it.t401NoWO
            }
        }

        res."options" = opts
        render res as JSON
    }
}
