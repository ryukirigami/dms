package com.kombos.reception

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.*
import com.kombos.maintable.Company
import com.kombos.maintable.Customer
import com.kombos.maintable.MappingCustVehicle
import com.kombos.maintable.Nikah
import grails.converters.JSON
import org.apache.commons.codec.binary.Base64
import org.springframework.web.multipart.commons.CommonsMultipartFile

class CustomerController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def menu = ""
        if(params.menu){
            menu = params.menu
        }
        def customerInstance = new HistoryCustomer()
        def historyCustomerVehicle = new HistoryCustomerVehicle()
        def idHistVehicle = null, fullModelVinCode = new FullModelVinCode(), reception = new Reception()
        if (params.idCustomer) {
            customerInstance = HistoryCustomer.findByCustomerAndStaDel(Customer.get(params.idCustomer.toLong()), "0")
        }
        if (params.idVehicle) {
            historyCustomerVehicle = HistoryCustomerVehicle.get(params.idVehicle.toLong())
            idHistVehicle = historyCustomerVehicle?.id
            fullModelVinCode = FullModelVinCode.findByFullModelCodeAndStaDel(historyCustomerVehicle?.fullModelCode, "0")
        }

        if (params.idCustomer && params.idVehicle) {
            reception = Reception.findByHistoryCustomerAndHistoryCustomerVehicleAndStaDelAndStaSave(customerInstance, historyCustomerVehicle, "0","0");
        }


        [historyCustomerInstance: customerInstance, historyCustomerVehicleInstance: historyCustomerVehicle, idHistVehicle: idHistVehicle,
                fullModelVinCode: fullModelVinCode, receptionInstance: reception,menu:menu]
    }

    def uploadImage() {
        if (params.myFile) {
            def fileName
            def inputStream
            if (params.myFile instanceof CommonsMultipartFile) {
                fileName = params.myFile?.originalFilename
                inputStream = params.myFile.getInputStream()
            } else {
                fileName = params.myFile
                inputStream = request.getInputStream()
            }

            fileName = fileName.replaceAll(" ", "_")

            File storedFile = new File("/temp/${fileName}")
            storedFile.parentFile.mkdirs()
            storedFile.append(inputStream)
            session.customerFoto = storedFile?.absolutePath
            render '<img width=\"200px\" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(storedFile.bytes), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }

    def uploadImagePopup() {
        if (params.t195Foto) {
            def fileName
            def inputStream
            if (params.t195Foto instanceof CommonsMultipartFile) {
                fileName = params.t195Foto?.originalFilename
                inputStream = params.t195Foto.getInputStream()
            } else {
                fileName = params.t195Foto
                inputStream = request.getInputStream()
            }

            fileName = fileName.replaceAll(" ", "_")

            File storedFile = new File("/temp/${fileName}")
            storedFile.parentFile.mkdirs()
            storedFile.append(inputStream)

            session.fotoPopup = storedFile?.absolutePath

            render '<img width=\"200px\" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(storedFile.bytes), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }

    def uploadDokumen() {
        if (params.myDocument) {
            def dokumenName
            def inputStream
            if (params.myDocument instanceof CommonsMultipartFile) {
                dokumenName = params.myDocument?.originalFilename
                inputStream = params.myDocument.getInputStream()
            } else {
                dokumenName = params.myDocument
                inputStream = request.getInputStream()
            }

            dokumenName = dokumenName.replaceAll(" ", "_")

            File storedFile = new File("/temp/${dokumenName}")
            storedFile.parentFile.mkdirs()
            storedFile.append(inputStream)

            session.dokumen = storedFile?.absolutePath

            render 'Upload Success'
        } else {
            render "No File"
        }
    }

    def lihatGambar() {
        def historyCustomer = HistoryCustomer.read(params.id)
        if (params.id && historyCustomer) {
            render '<img width=\"200px\" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(historyCustomer.t182Foto), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }

    def customerCarsDatatablesList() {

        def custVehicle = HistoryCustomerVehicle.get(params.idVehicle.toLong())

        def ret

        def c = MappingCustVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("customerVehicle", custVehicle?.customerVehicle)
        }

        def rows = []

        results.each {
            def histCustomer = HistoryCustomer.findByCustomerAndStaDel(it.customer, "0")
            rows << [
                    id: it.id,
                    peran: it?.customer?.peranCustomer?.m115NamaPeranCustomer,
                    t182NamaDepan: histCustomer ? histCustomer?.t182NamaDepan : "",
                    t182NamaBelakang: histCustomer ? histCustomer?.t182NamaBelakang : "",
                    t182TglTransaksi: histCustomer ? (histCustomer?.t182TglTransaksi ? histCustomer?.t182TglTransaksi?.format("dd/MM/yyyy") : "") : "",
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def save() {
        def historyCustomerInstance = new HistoryCustomer()
        def historyCustVehicle = new HistoryCustomerVehicle()
        def custVehicle = new CustomerVehicle()
        custVehicle.dateCreated = datatablesUtilService.syncTime()
        custVehicle.lastUpdated = datatablesUtilService.syncTime()
        if (params.idVehicle) {
            historyCustVehicle = HistoryCustomerVehicle.get(params.idVehicle.toLong())
            custVehicle = historyCustVehicle?.customerVehicle
        }
        if (params.t182ID) {
            historyCustomerInstance = HistoryCustomer.findByT182IDAndStaDel(params.t182ID, "0")
            historyCustomerInstance.properties = params
            historyCustomerInstance.lastUpdated = datatablesUtilService?.syncTime()
            historyCustomerInstance.lastUpdProcess = "UPDATE"
            historyCustomerInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        } else {
            historyCustomerInstance = new HistoryCustomer(params)
            historyCustomerInstance.t182ID = generateCodeService.codeGenerateSequence("T182_T102_ID", session.userCompanyDealer)
            historyCustomerInstance.staDel = "0"
            historyCustomerInstance.dateCreated = datatablesUtilService?.syncTime()
            historyCustomerInstance.lastUpdated = datatablesUtilService?.syncTime()
            historyCustomerInstance.lastUpdProcess = "INSERT"
            historyCustomerInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        }
        if (session.customerFoto) {
            File fileImage = new File(session?.customerFoto?.toString())
            historyCustomerInstance.t182Foto = fileImage.bytes
            session.customerFoto = null
        }

        if (session.dokumen) {
            File fileDokumen = new File(session?.dokumen?.toString())
            custVehicle.t103BukuService = fileDokumen.bytes
            session.dokumen = null
            custVehicle.save(flush: true)
        }
        def tempCustomer = new Customer()
        tempCustomer.company = historyCustomerInstance.company
        tempCustomer.dateCreated = datatablesUtilService?.syncTime()
        tempCustomer.lastUpdated = datatablesUtilService?.syncTime()
        //tempCustomer.dateCreated = new Date()
        //        tempCustomer.lastUpdated = new Date()
        tempCustomer.peranCustomer = historyCustomerInstance.peranCustomer
        tempCustomer.t102ID = historyCustomerInstance.t182ID
        tempCustomer.staDel = '0'
        tempCustomer.lastUpdProcess = "INSERT"
        tempCustomer.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()

        tempCustomer.save(flush: true)
        tempCustomer.errors.each {println it}
        historyCustomerInstance.customer = tempCustomer
        def cek = HistoryCustomer.find(historyCustomerInstance)
        if(cek){
            render "ada"
        }else{
            if (!historyCustomerInstance.save(flush: true)) {

            } else {
                historyCustomerInstance.errors.each {println it}
                if (params.t182ID == "" || params.t182ID == null) {
                    def mappCust = new MappingCustVehicle()
                    mappCust.customer = historyCustomerInstance?.customer
                    mappCust.customerVehicle = historyCustVehicle?.customerVehicle
                    mappCust.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    mappCust.lastUpdProcess = "INSERT"
                    mappCust.dateCreated = datatablesUtilService.syncTime()
                    mappCust.lastUpdated = datatablesUtilService.syncTime()
                    mappCust.save(flush: true);
                    mappCust.errors.each {println it}
                }
                render "sukses"
            }
        }
    }

    def saveAsuransi(){
        def history = HistoryCustomerVehicle.get(params.idVehicle.toLong())
        def spkAsuransi = SPKAsuransi.findByCustomerVehicleAndStaDel(history?.customerVehicle,"0")
        def tempSPK = null
        if(spkAsuransi){
            tempSPK = spkAsuransi
            spkAsuransi?.properties = params
            spkAsuransi?.lastUpdated = datatablesUtilService?.syncTime()
            spkAsuransi?.lastUpdProcess = "UPDATE"
            spkAsuransi?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        }else{
            spkAsuransi = new SPKAsuransi(params)
            spkAsuransi?.customerVehicle = history?.customerVehicle
            spkAsuransi?.staDel = "0"
            spkAsuransi?.dateCreated = datatablesUtilService?.syncTime()
            spkAsuransi?.lastUpdated = datatablesUtilService?.syncTime()
            spkAsuransi?.lastUpdProcess = "INSERT"
            spkAsuransi?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        }

        if(!spkAsuransi.save(flush: true)){

        }else{
            def dokSPK = DokumenSPK.findBySpkAsuransiAndStaDel(tempSPK,"0")
            if(dokSPK){
                dokSPK.properties = params
                dokSPK.spkAsuransi = spkAsuransi
                dokSPK.t195TglJamUpload = new Date()
                dokSPK?.lastUpdated = datatablesUtilService?.syncTime()
                dokSPK.lastUpdProcess = "UPDATE"
                dokSPK.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }else{
                dokSPK = new DokumenSPK(params)
                dokSPK?.spkAsuransi = spkAsuransi
                dokSPK?.t195TglJamUpload = new Date()
                dokSPK?.staDel = "0"
                dokSPK?.dateCreated = datatablesUtilService?.syncTime()
                dokSPK?.lastUpdated = datatablesUtilService?.syncTime()
                dokSPK?.lastUpdProcess = "INSERT"
                dokSPK?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }

            if (session.fotoPopup) {
                File fileImage = new File(session?.fotoPopup?.toString())
                dokSPK?.t195Foto = fileImage.bytes
                session.fotoPopup = null
            }
            if(!dokSPK?.save(flush: true)){
            }else{
                render "OK"
            }
        }
    }

    def findKab() {
        def html = "<option value=\'\'> Pilih Kab. / Kota</option>"
        if (params?.provinsi.id) {
            def provinsi = Provinsi.get(params?.provinsi.id?.toLong())
            if (provinsi) {
                def kabKota = KabKota.createCriteria().list {
                    eq("staDel", "0")
                    eq("provinsi", provinsi);
                    order("m002NamaKabKota")
                }
                kabKota.each {
                    html = html + "<option value=\'${it.id}\'> ${it.m002NamaKabKota}</option>"
                }
            }
        }
        render html
    }

    def findKecamatan() {
        def html = "<option value=\'\'> Pilih Kecamatan</option>"
        if (params?.kabKota.id) {
            def kabKota = KabKota.get(params?.kabKota.id?.toLong())
            if (kabKota) {
                def kecamatan = Kecamatan.createCriteria().list {
                    eq("staDel", "0")
                    eq("kabKota", kabKota);
                    order("m003NamaKecamatan")
                }
                kecamatan.each {
                    html = html + "<option value=\'${it.id}\'> ${it.m003NamaKecamatan}</option>"
                }
            }
        }
        render html
    }

    def findKelurahan() {
        def html = "<option value=\'\'> Pilih Kelurahan</option>"
        if (params?.kecamatan.id) {
            def kecamatan = Kecamatan.get(params?.kecamatan.id?.toLong())
            if (kecamatan) {
                def kelurahan = Kelurahan.createCriteria().list {
                    eq("staDel", "0")
                    eq("kecamatan", kecamatan);
                    order("m004NamaKelurahan")
                }
                kelurahan.each {
                    html = html + "<option value=\'${it.id}\'> ${it.m004NamaKelurahan}</option>"
                }
            }
        }
        render html
    }

    def findCustomerTampil() {
        def html = "<option value=\'\'> Tambah Customer</option>"
        def getHistoryVehicle = HistoryCustomerVehicle.get(params.idVehicle.toLong())
        def customers = MappingCustVehicle.findAllByCustomerVehicle(getHistoryVehicle?.customerVehicle)
        if (customers) {
            customers.each {
                def history = HistoryCustomer.findByCustomerAndPeranCustomerAndStaDel(it.customer, it.customer?.peranCustomer, "0")
                if (history) {
                    def nama = history?.t182NamaBelakang ? history?.t182NamaDepan + " " + history?.t182NamaBelakang : history?.t182NamaDepan
                    if (params.kodeCust) {
                        if (history?.t182ID?.equalsIgnoreCase(params.kodeCust)) {
                            html += "<option value=\'" + it.customer.id + "\' selected=\'selected\'> " + nama + " " + history?.t182NamaBelakang + " - " + history?.peranCustomer?.m115NamaPeranCustomer + "</option>"
                        } else {
                            html += "<option value=\'" + it.customer.id + "\'> " + nama + " - " + history?.peranCustomer?.m115NamaPeranCustomer + "</option>"
                        }
                    } else {
                        html += "<option value=\'" + it.customer.id + "\'> " + nama + " - " + history?.peranCustomer?.m115NamaPeranCustomer + "</option>"
                    }
                }
            }
        }
        render html
    }

    def validate(){
        def row = []
        def fullVinCode = FullModelVinCode.createCriteria().list {
            eq("staDel","0")
            eq("t109WMI",params.t109WMI.trim(),[ignoreCase : true])
            eq("t109VDS",params.t109VDS.trim(),[ignoreCase : true])
            eq("t109CekDigit",params.t109CekDigit.trim(),[ignoreCase : true])
            eq("t109VIS",params.t109VIS.trim(),[ignoreCase : true])
        }

        def bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
        if(fullVinCode){
            (1..7).each {
                switch (it){
                    case 1: row << [
                                data : "<option value=\'"+fullVinCode?.first()?.fullModelCode?.baseModel?.id+"\'> "+fullVinCode?.first()?.fullModelCode?.baseModel?.m102NamaBaseModel+"</option>"
                            ];
                        break;
                    case 2 : row << [
                                data : "<option value=\'"+fullVinCode?.first()?.fullModelCode?.grade?.id+"\'> "+fullVinCode?.first()?.fullModelCode?.grade?.m107NamaGrade+"</option>"
                            ];
                        break;
                    case 3 : row << [
                                data : "<option value=\'"+fullVinCode?.first()?.fullModelCode?.engine?.id+"\'> "+fullVinCode?.first()?.fullModelCode?.engine?.m108NamaEngine+"</option>"
                            ];
                        break;
                    case 4 : row << [
                                data :" <option value=\'"+fullVinCode?.first()?.fullModelCode?.gear?.id+"\'> "+fullVinCode?.first()?.fullModelCode?.gear?.m106NamaGear+"</option>"
                            ];
                        break;
                    case 5 : row << [
                                data : "<option value=\'"+fullVinCode?.first()?.fullModelCode?.id+"\'> "+fullVinCode?.first()?.fullModelCode?.t110FullModelCode+"</option>"
                            ];
                        break;
                    case 6 : row << [
                                data : fullVinCode?.first()?.t109ThnBlnPembuatan?.toString()?.substring(0,4)
                            ];
                        break;
                    case 7 :
                        String blnTrim = fullVinCode?.first()?.t109ThnBlnPembuatan?.toString()?.substring(4,6);
                        String hasil = "";
                        for(int a=1;a<=12;a++){
                            if(blnTrim.toInteger()==a){
                                hasil = bulan[a-1]
                            }
                        }
                        row << [
                                data : "<option value=\'"+blnTrim+"\'> "+hasil+"</option>"
                        ];
                        break;
                }
            }
        }
        else {
            row << [data : "<option value=\'\'> </option>"];
        }

        render row as JSON
    }

    def findList(){
        String keyword = params.cari
        String html = ""
        def instance = null
        if(keyword.equalsIgnoreCase('company')){
            html+="<option value=\'\'> Pilih Perusahaan</option>"
            instance = Company.createCriteria().list {
                eq("staDel","0");
                order("namaPerusahaan");
            }
            instance.each {
                html+="<option value=\'"+it.id+"\'> "+it.namaPerusahaan+"</option>"
            }
        }else if(keyword.equalsIgnoreCase('jenisIdCard')){
            html+="<option value=\'\'> Pilih Jenis ID Card</option>"
            instance = JenisIdCard.createCriteria().list {
                eq("staDel","0");
                order("m060JenisIDCard");
            }
            instance.each {
                html+="<option value=\'"+it.id+"\'> "+it.m060JenisIDCard+"</option>"
            }
        }
        else if(keyword.equalsIgnoreCase('agama')){
            html+="<option value=\'\'> Pilih Agama</option>"
            instance = Agama.createCriteria().list {
                eq("staDel","0");
                order("m061NamaAgama");
            }
            instance.each {
                html+="<option value=\'"+it.id+"\'> "+it.m061NamaAgama+"</option>"
            }
        }
        else if(keyword.equalsIgnoreCase('nikah')){
            html+="<option value=\'\'> Pilih nikah</option>"
            instance = Nikah.createCriteria().list {
                eq("staDel","0");
                order("m062StaNikah");
            }
            instance.each {
                html+="<option value=\'"+it.id+"\'> "+it.m062StaNikah+"</option>"
            }
        }
        else if(keyword.equalsIgnoreCase('hobby')){
            html+="<option value=\'\'> Pilih hobby</option>"
            instance = Hobby.createCriteria().list {
                eq("staDel","0");
                order("m063NamaHobby");
            }
            instance.each {
                html+="<option value=\'"+it.id+"\'> "+it.m063NamaHobby+"</option>"
            }
        }
        render html
    }

    def insertNikah(){
        String html = ""
        def cariNikah = Nikah.createCriteria().list {
            eq("staDel","0")
            eq("m062StaNikah",params.status.trim(),[ignoreCase: true])
        }
        if(cariNikah){
            html="duplicate"
        }else{
            def cekID = Nikah?.last()?.id
            def nikah = new Nikah()
            nikah?.m062ID = cekID ? (cekID+1).toString() : "1"
            nikah?.m062StaNikah = params.status.trim().toUpperCase()
            nikah?.staDel = "0"
            nikah?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            nikah?.lastUpdProcess = "INSERT"
            nikah?.dateCreated = datatablesUtilService.syncTime()
            nikah?.lastUpdated = datatablesUtilService.syncTime()
            if(!nikah?.save(flush: true)){
                nikah.errors.each {println(it)}
                println "GAGAL"
                html="gagal"
            }else{
                println "BEHASIL"
                html+="<option value=\'\'> Pilih Status</option>"
                def c = Nikah.createCriteria().list {eq("staDel","0");order("m062StaNikah")}
                c.each {
                    html+="<option value=\'"+it.id+"\'> "+it.m062StaNikah+"</option>"
                }
            }
        }
        render html
    }

    def changeVendor(){
        def vendor = VendorAsuransi.get(params.id.toLong())
        render vendor.m193JmlToleransiAsuransiBP
    }

    def getDokumen(){
        def html = ""
        def row = []
        def cari = KelengkapanDokumenAsuransi.list()
        if(cari){
            row << cari.size()
            cari.each {
                html+="<input type=\"checkbox\" name=\"cekbok"+it.id+"\" id=\"cekbok"+it.id+"\" checked=\"false\" readonly=\"\" disabled=\"\" /> "+it.m195NamaDokumenAsuransi+" <br/>"
            }
            row << html
        }
        render row as JSON
    }


}
