package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.maintable.TujuanKedatangan
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class CustomerInController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def customerInService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']

	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']

	static deletePermissions = ['delete']
	
	def index() {
//		redirect(action: "list", params: params)
		redirect(action: "create", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = CustomerIn.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer);
			if(params."sCriteria_t400ID"){
				ilike("t400ID","%" + (params."sCriteria_t400ID" as String) + "%")
			}

			if(params."sCriteria_customerVehicle"){
				eq("customerVehicle",params."sCriteria_customerVehicle")
			}

			if(params."sCriteria_tujuanKedatangan"){
				eq("tujuanKedatangan",params."sCriteria_tujuanKedatangan")
			}

			if(params."sCriteria_t400StaComplain"){
				ilike("t400StaComplain","%" + (params."sCriteria_t400StaComplain" as String) + "%")
			}

			if(params."sCriteria_t400NomorAntrian"){
				eq("t400NomorAntrian",params."sCriteria_t400NomorAntrian")
			}

			if(params."sCriteria_t400TglCetakNoAntrian"){
				ge("t400TglCetakNoAntrian",params."sCriteria_t400TglCetakNoAntrian")
				lt("t400TglCetakNoAntrian",params."sCriteria_t400TglCetakNoAntrian" + 1)
			}

			if(params."sCriteria_t400xNamaUserIn"){
				ilike("t400xNamaUserIn","%" + (params."sCriteria_t400xNamaUserIn" as String) + "%")
			}

			if(params."sCriteria_t400StaApp"){
				ilike("t400StaApp","%" + (params."sCriteria_t400StaApp" as String) + "%")
			}

			if(params."sCriteria_reception"){
				eq("reception",params."sCriteria_reception")
			}

			if(params."sCriteria_t400StaReception"){
				ilike("t400StaReception","%" + (params."sCriteria_t400StaReception" as String) + "%")
			}

			if(params."sCriteria_t400TglJamReception"){
				ge("t400TglJamReception",params."sCriteria_t400TglJamReception")
				lt("t400TglJamReception",params."sCriteria_t400TglJamReception" + 1)
			}

			if(params."sCriteria_companyDealer"){
				eq("companyDealer",params."sCriteria_companyDealer")
			}

			if(params."sCriteria_loket"){
				eq("loket",params."sCriteria_loket")
			}

			if(params."sCriteria_t400StaSelesaiReception"){
				ilike("t400StaSelesaiReception","%" + (params."sCriteria_t400StaSelesaiReception" as String) + "%")
			}

			if(params."sCriteria_t400TglJamSelesaiReception"){
				ge("t400TglJamSelesaiReception",params."sCriteria_t400TglJamSelesaiReception")
				lt("t400TglJamSelesaiReception",params."sCriteria_t400TglJamSelesaiReception" + 1)
			}

			if(params."sCriteria_t400StaOut"){
				ilike("t400StaOut","%" + (params."sCriteria_t400StaOut" as String) + "%")
			}

			if(params."sCriteria_t400TglJamOut"){
				ge("t400TglJamOut",params."sCriteria_t400TglJamOut")
				lt("t400TglJamOut",params."sCriteria_t400TglJamOut" + 1)
			}

			if(params."sCriteria_t400xNamaUserOut"){
				ilike("t400xNamaUserOut","%" + (params."sCriteria_t400xNamaUserOut" as String) + "%")
			}

			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}

			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}

			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}


			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						t400ID: it.t400ID,
			
						customerVehicle: it.customerVehicle,
			
						tujuanKedatangan: it.tujuanKedatangan,
			
						t400StaComplain: it.t400StaComplain,
			
						t400NomorAntrian: it.t400NomorAntrian,
			
						t400TglCetakNoAntrian: it.t400TglCetakNoAntrian?it.t400TglCetakNoAntrian.format(dateFormat):"",
			
						t400xNamaUserIn: it.t400xNamaUserIn,
			
						t400StaApp: it.t400StaApp,
			
						reception: it.reception,
			
						t400StaReception: it.t400StaReception,
			
						t400TglJamReception: it.t400TglJamReception?it.t400TglJamReception.format(dateFormat):"",
			
						companyDealer: it.companyDealer,
			
						loket: it.loket,
			
						t400StaSelesaiReception: it.t400StaSelesaiReception,
			
						t400TglJamSelesaiReception: it.t400TglJamSelesaiReception?it.t400TglJamSelesaiReception.format(dateFormat):"",
			
						t400StaOut: it.t400StaOut,
			
						t400TglJamOut: it.t400TglJamOut?it.t400TglJamOut.format(dateFormat):"",
			
						t400xNamaUserOut: it.t400xNamaUserOut,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
        [customerInInstance: new CustomerIn(params)]
	}

	def save() {
        def noPol = params?.noPol?.id?.toString()+" "+params?.t183NoPolTengah?.toString()+" "+params?.t183NoPolBelakang
        if(params.t400StaComplain == null){
            params.t400StaComplain = '0'
        }
        if(params.t400StaShow == null){
            params.t400StaShow = '1'
        }

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def noAntri = params.noPol.id.toString()+" "+params.t183NoPolTengah.toString()+" "+params.t183NoPolBelakang
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)
        def tujuans = TujuanKedatangan.get(params?.tujuanKedatangan?.id?.toLong())
        def c = CustomerIn.createCriteria().list {
            eq("companyDealer",session?.userCompanyDealer);
            eq("staDel","0")
            if(params.t400StaComplain=='1'){
                ilike("t400NomorAntrian",'B'+"%");
            }else{
                ilike("t400NomorAntrian",params.t400StaApp.toString().substring(0,1)+"%")
            }
            not{
                or{
                    ilike("t400NomorAntrian","% %");
                    eq("t400NomorAntrian","-");
                }
            }
            isNull("t400TglJamSelesaiReception")
            ge("dateCreated",dateAwal)
            le("dateCreated",dateAkhir)
            order("t400NomorAntrian")
        }
        def cek = CustomerIn.createCriteria().list {
            eq("companyDealer",session?.userCompanyDealer);
            eq("staDel","0")
//            if(params?.idCustomerVehicle){
//                customerVehicle{
//                    eq("id",params?.idCustomerVehicle?.toLong())
//                }
//            }else{
                eq("t400noPol",params?.noPol?.id?.toString()+" "+params?.t183NoPolTengah?.toString()+" "+params?.t183NoPolBelakang,ignoreCase : true)
//            }
            isNull("t400TglJamSelesaiReception")
            ge("dateCreated",dateAwal)
            le("dateCreated",dateAkhir)
        }
        if(tujuans && (tujuans?.m400Tujuan?.contains("GR") || tujuans?.m400Tujuan?.contains("BP"))){
            if(params.t400StaApp.toString().toUpperCase()?.contains("WALK IN") || params.t400StaApp.toString().toUpperCase()?.contains("BOOKING")){
                if(c){
                    def belakang = c.last()?.t400NomorAntrian?.substring(1,c.last()?.t400NomorAntrian?.length())?.toLong() + 1
                    def belakangS = belakang?.toString()?.length()==1 || belakang?.toString()?.length()==2 ? (belakang?.toString()?.length()==1 ?  "00"+belakang.toString() :  "0"+belakang.toString()) : belakang.toString()
                    if(params?.idCustomerVehicle){
                        def custV = CustomerVehicle?.get(params.idCustomerVehicle.toLong())
                        def appointments = Appointment.createCriteria().list{
                            eq("companyDealer",session?.companyDealer)
                            eq("staDel","0");
                            ge("t301TglJamRencana",dateAwal)
                            lt("t301TglJamRencana",dateAkhir)
                            order("t301TglJamRencana","asc")
                        }
                        int urutApp = 1;
                        for(cari in appointments) {
                            if(cari?.historyCustomerVehicle==custV?.getCurrentCondition()){
                                belakangS = urutApp.toString().length()==1 ||  urutApp.toString().length()==2 ? (urutApp?.toString()?.length()==1 ? "00"+urutApp : "0"+urutApp) : urutApp.toString()
                            }
                        }
                    }
                    def depan = params?.t400StaComplain=='1 ' ? "B" : params.t400StaApp.toString().substring(0,1)
                    noAntri=depan+belakangS
                }else{
                    def depan = params?.t400StaComplain?.toString()?.equalsIgnoreCase('1') ? "B" : params.t400StaApp.toString().substring(0,1)
                    noAntri=depan+"001"
                }
            }
        }
        if(params?.idCustomerVehicle){
            params.customerVehicle = CustomerVehicle.get(params.idCustomerVehicle.toLong())
        }
        params.t400noPol = noPol
        params.staDel = '0'
        params.companyDealer = session.userCompanyDealer
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.t400StaApp = params.t400StaApp.toString().equalsIgnoreCase("Walk In") || params.t400StaApp.toString().equalsIgnoreCase("Booking") ? params.t400StaApp.toString().substring(0,1) : "-"
        params?.t400StaReception = "0"
        def customerInInstance = new CustomerIn(params)
        if(cek.size()<1){
            if(params.t400StaShow=='1'){
                noAntri = "-"
            }
            if(params?.idCustomerVehicle){
                customerInInstance.customerVehicle = CustomerVehicle.get(params.idCustomerVehicle.toLong())
            }
            customerInInstance.t400NomorAntrian = noAntri
            customerInInstance.t400noPol = noPol
            customerInInstance.dateCreated = datatablesUtilService?.syncTime()
            customerInInstance.t400TglCetakNoAntrian = datatablesUtilService?.syncTime()
            if(!customerInInstance?.save(flush: true)){
                customerInInstance.errors.each {println it}
            }
            else{
                render "ok"
            }
        }else{
            def temp = cek.first()
            temp.properties = params
            if(temp.t400NomorAntrian=="-" && params.t400StaShow=='0'){
                temp.t400NomorAntrian = noAntri
            }

            if(params.t400StaShow=='1'){
                temp.t400NomorAntrian  = "-"
            }
            temp.t400noPol = noPol
            temp?.save(flush: true)
            temp.errors.each {println it}
            render "ok"
        }
	}
	
	def printNomorAntrian(){
		def customerInInstanceId = params.customerInInstanceId
		redirect(action: "show", id: customerInInstanceId)	
	}

	def show(Long id) {
		def customerInInstance = CustomerIn.get(id)
		if (!customerInInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerIn.label', default: 'CustomerIn'), id])
			redirect(action: "list")
			return
		}

		[customerInInstance: customerInInstance]
	}

	def edit(Long id) {
		def customerInInstance = CustomerIn.get(id)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		if (!customerInInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerIn.label', default: 'CustomerIn'), id])
			redirect(action: "list")
			return
		}

		[customerInInstance: customerInInstance]
	}

    def findStatusBooking(){
        params.companyDealer = session.userCompanyDealer
        def result = customerInService.findStatusBooking(params);

        if(result!=null){
            render result[0] as JSON
        }
    }

    def validasiStatusBook(){
        def result = '4'
        if(customerInService.checkAppointmentBooked(params)){
            result = '1' //booked valid
        }else if(customerInService.checkAppointmentBoookedComplaint(params)){
            result = '2'
        }else if(customerInService.checkGatePass(params)){
            result = '3'
        }

        render result
    }
    def findNoPol(){
        params.companyDealer = session.userCompanyDealer
        def hasil = customerInService.findNoPol(params,session)
        render hasil as JSON
    }



    def printNoAntrian(){
        def hari = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
        def tujuan = TujuanKedatangan.get(params.tujuan.toLong())
        Date sekarang = datatablesUtilService?.syncTime()
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)
        def noAntri = CustomerIn?.createCriteria()?.get {
            eq("companyDealer",session?.userCompanyDealer);
            if(params.idCustomerVehicle){
                eq("customerVehicle",CustomerVehicle.get(params.idCustomerVehicle.toLong()));
            }else{
                eq("t400noPol",params.kode+" "+params.tengah+" "+params.belakang,[ignoreCase: true]);
            }
            ge("dateCreated",dateAwal)
            lt("dateCreated",dateAkhir)
            maxResults(1);
        }
        noAntri.t400TglCetakNoAntrian = datatablesUtilService?.syncTime()
        noAntri?.save(flush : true)
        noAntri?.errors?.each {println it}
        def waktu = hari[sekarang?.getDay()]+", "+sekarang.format("dd MMMM yyyy | HH:mm")
        def noPol = params.kode+" "+params.tengah+" "+params.belakang
        def html = ""
        def realTujuan = tujuan?.m400Tujuan?.equalsIgnoreCase("BP") || tujuan?.m400Tujuan?.equalsIgnoreCase("GR") ? (tujuan?.m400Tujuan?.equalsIgnoreCase("BP") ? "BODY PAINT" : "GENERAL REPAIR") : tujuan?.m400Tujuan
        if(tujuan && (tujuan?.m400Tujuan?.contains("GR") || tujuan?.m400Tujuan?.contains("BP"))){
            html = "<table class=\"table table-bordered table-hover\">\n" +
                    "                        <tr>\n" +
                    "                            <td style=\"text-align: center;font-size: 50px\">\n" +
                    "                                <b>"+noAntri?.t400NomorAntrian+"</b>\n" +
                    "                            </td>\n" +
                    "                        </tr>\n" +
                    "                        <tr>\n" +
                    "                            <td style=\"text-align: center;font-size: 18px\">\n" +
                    "                                "+noPol+"\n" +
                    "                            </td>\n" +
                    "                        </tr>\n" +
                    "                        <tr>\n" +
                    "                            <td style=\"text-align: center\">\n" +
                    "                                "+waktu+"\n" +
                    "                            </td>\n" +
                    "                        </tr>\n" +
                    "                        <tr>\n" +
                    "                            <td style=\"text-align: center; font-size: 20px\">\n" +
                    "                                "+realTujuan+"\n" +
                    "                            </td>\n" +
                    "                        </tr>\n" +
                    "</table>"
        }else{
            html = "<table class=\"table table-bordered table-hover\">\n" +
                    "                        <tr>\n" +
                    "                            <td style=\"text-align: center;font-size: 50px\">\n" +
                    "                                <b>"+noPol+"</b>\n" +
                    "                            </td>\n" +
                    "                        </tr>\n" +
                    "                        <tr>\n" +
                    "                            <td style=\"text-align: center;font-size: 18px\">\n" +
                    "                                "+waktu+"\n" +
                    "                            </td>\n" +
                    "                        </tr>\n" +
                    "</table>"
        }
        render html
    }
}
