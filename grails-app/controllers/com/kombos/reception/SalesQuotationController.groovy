package com.kombos.reception

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.maintable.*
import com.kombos.parts.*
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.PartsRCP
import com.kombos.woinformation.Prediagnosis
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class SalesQuotationController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def salesQuotationService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def conversi=new Konversi()

    def index() {
        params.companyDealer = session.userCompanyDealer
        def result = salesQuotationService.create(params)
    }

    def list(Integer max) {
    }

    def datatablesList(){
        session.exportParams = params
        render salesQuotationService.datatablesList(params) as JSON
    }

    def cekNoSpk(){
        def noSpk = params.noSpk
        Date tglSkr = datatablesUtilService?.syncTime()
        String hasil = ""
        if(noSpk != ""){
            def kodeKotaNoPol = KodeKotaNoPol.get(params.kodeKotaNoPol.toLong())
            def histCV = HistoryCustomerVehicle.createCriteria().list {
                eq("staDel","0");
                eq("fullNoPol",kodeKotaNoPol?.m116ID+" "+params.noPolTengah+" "+params.noPolBelakang,[ignoreCase : true]);
            }
            def CariNo = SPKAsuransi.findByT193NomorSPKAndCustomerVehicleAndStaDel(noSpk as String, histCV.customerVehicle.last(), "0")

            if (tglSkr > CariNo?.t193TglAwal && tglSkr < CariNo?.t193TglAkhir){
                hasil = "OK"
            }else{
                hasil = "NOK"
            }
        }
        render hasil
    }

    def saveReception(){
        String hasil = ""
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        def kodeKotaNoPol = KodeKotaNoPol.get(params.kodeKotaNoPol.toLong())
        def histCV = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0");
            eq("fullNoPol",kodeKotaNoPol?.m116ID+" "+params.noPolTengah+" "+params.noPolBelakang,[ignoreCase : true]);
        }
        if(histCV.size()>0){
            def custTemp = new HistoryCustomer()
            def cariPakai = false
            def custVehicle = histCV.last().customerVehicle
            def mappingCV = MappingCustVehicle.createCriteria().list {
                eq("customerVehicle",custVehicle);
                order("dateCreated","desc")
            }
            for(cari in mappingCV){
                if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik') || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase('Pemilik dan Penanggung Jawab')){
                    custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                    cariPakai=true;
                }
                if(cariPakai){
                    break
                }else{
                    custTemp = HistoryCustomer.findByCustomerAndStaDel(cari.customer,"0");
                }
            }
            def reception = Reception.get(params.idReception.toLong())
            reception.historyCustomerVehicle = histCV.last()
            reception.historyCustomer = custTemp
            reception.staSave = "2"
            reception.t401StaReceptionEstimasiSalesQuotation = "estimasi"
            reception.t401TanggalWO = datatablesUtilService?.syncTime()
            reception.t401NamaSA = org.apache.shiro.SecurityUtils.subject.principal.toString()
            reception?.lastUpdated = datatablesUtilService?.syncTime()
            reception.save(flush: true)

            hasil="ok"
        }else{
            hasil = "no"
        }

        render hasil
    }

    def massdelete() {
        def res = [:]
        def jsonArray = JSON.parse(params.jobs)
        def jsonArray2 = JSON.parse(params.parts)
        try {
            jsonArray.each {
                def jobRcp = JobRCP.get(it.toLong())
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(jobRcp?.reception,jobRcp?.operation,"0");
                parts.each {
                    it.lastUpdated = datatablesUtilService?.syncTime()
                    it.staDel = "1"
                    it.save(flush: true)
                }
                jobRcp.lastUpdated = datatablesUtilService?.syncTime()
                jobRcp.staDel = "1"
                jobRcp.save(flush: true)
            }
            jsonArray2.each {
                def partsRcp = PartsRCP.findByIdAndStaDel(it.toLong(),"0")
                if(partsRcp){
                    partsRcp.lastUpdated = datatablesUtilService?.syncTime()
                    partsRcp.staDel = "1"
                    partsRcp.save(flush: true)

                }

            }
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def jobnPartsDatatablesList() {
        render salesQuotationService.jobnPartsDatatablesList(params,session) as JSON
    }

    def jobnPartsOrderDatatablesList() {
        render salesQuotationService.jobnPartsOrderDatatablesList(params,session) as JSON
    }

    def partDatatablesList() {
        render salesQuotationService.partDatatablesList(params) as JSON
    }

    def workDatatablesList() {
        render salesQuotationService.workDatatablesList(params) as JSON
    }

    def partList() {
        Random rand = new Random();
        int  n = rand.nextInt(50) + 1;
        String idTable = new Date().format("yyyyMMddhhmmss")+params.idJob+n.toString()
        [idReception : params.idReception,idJob : params.idJob, idTable: idTable]
    }

    def workList() {
        [idJob : params.idJob, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def orderPart() {
        render salesQuotationService.orderPart(params, session.userCompanyDealer) as JSON
    }

    def findNoPol(){
        def balik=[]
        def idPemilik = -1
        if(params.kode && params.tengah && params.belakang){
            def cari = HistoryCustomerVehicle.createCriteria().list {
                eq("staDel","0")
                kodeKotaNoPol{
                    eq("id",params.kode.toLong())
                }
                eq("t183NoPolTengah",params.tengah)
                eq("t183NoPolBelakang",params.belakang,ignoreCase : true)
            }
            if(cari){
                balik << [
                        id : cari?.last()?.id
                ]
                def mappCust = MappingCustVehicle.createCriteria().list {
                    eq("customerVehicle",cari?.last()?.customerVehicle);
                    order("dateCreated","desc");
                }
                if(mappCust){
                    for (find in mappCust){
                        balik << [
                                id : find?.customer?.id
                        ]
                        if(find?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase("Pemilik") || find?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase("Pemilik dan Penanggung Jawab")){
                            idPemilik = find?.customer?.id;
                            break;
                        }
                    }
                    balik << [
                            id : idPemilik
                    ]
                }
            }
        }
        render balik as JSON
    }

    def getNewReception(){
        params.companyDealer = session.userCompanyDealer
        def result = salesQuotationService.getNewReception(params);
        render result as JSON
    }

    def getReception(){
        params.companyDealer = session.userCompanyDealer
        def result = salesQuotationService.getReception(params);
        render result as JSON
    }

    def savePreDiagnose(def params){
        def result = [:]
        def prediagnosis = salesQuotationService.savePreDiagnose(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if(prediagnosis != null){
            result = prediagnosis
        }

        render result as JSON
    }

    def printPrediagnosis(){
        def id = params.id

        def prediagnosisInstance = Prediagnosis.get(id)
        def receptionInstance = Reception.get(prediagnosisInstance.reception.id)

        def parameters = [
                SUBREPORT_DIR : servletContext.getRealPath('/reports') + '/',
                receptionId : receptionInstance.id,
                t406KeluhanCust : prediagnosisInstance.t406KeluhanCust,
                t406StaGejalaHariIni : prediagnosisInstance.t406StaGejalaHariIni
        ]

        def idx = 1
        def pemeriksaanAwalItems = []
        def pemeriksaanAwalList = DiagnosisDetail.findAllByReceptionAndPrediagnosisAndT416staAwalUlang(receptionInstance,prediagnosisInstance,'A')
        pemeriksaanAwalList.each {
            pemeriksaanAwalItems << [
                    no : idx,
                    system : it.t416System,
                    dtc : it.t416DTC,
                    desc : it.t416Desc,
                    status : it.t416StatusPCH,
                    freeze : it.t416Freeze
            ]
            idx++
        }

        idx = 1
        def cekUlangItems = []
        def cekUlangList = DiagnosisDetail.findAllByReceptionAndPrediagnosisAndT416staAwalUlang(receptionInstance,prediagnosisInstance,'U')
        cekUlangList.each {
            cekUlangItems << [
                    no : idx,
                    system : it.t416System,
                    dtc : it.t416DTC,
                    desc : it.t416Desc,
                    status : it.t416StatusPCH,
                    freeze : it.t416Freeze
            ]
            idx++
        }

        def subReportData1 = []
        subReportData1 << [
                cekUlangItems : cekUlangItems
        ]

        def subReportData = []
        subReportData << [
                pemeriksaanAwalItems : pemeriksaanAwalItems,
                subReportData1 : subReportData1
        ]

        def reportData = []
        reportData << [
                subReportData : subReportData
        ]

        def reportDef = new JasperReportDef(name:'prediagnose.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )

        def file = File.createTempFile("prediagnose_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }

    def printWoGr(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        receptionInstance.t401TglJamCetakWO = datatablesUtilService?.syncTime()
        receptionInstance.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance.save(flush: true)
        if(receptionInstance?.t401GambarWAC){
            byte[] bb = receptionInstance?.t401GambarWAC
            FileOutputStream output = new FileOutputStream("d:\\picWAC.jpg");
            output.write(bb);
        }
        def sa = User.findByUsernameAndStaDel(receptionInstance?.t401NamaSA,"0")
        def manPower = NamaManPower.findByUserProfileAndStaDel(sa,"0")

        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        def keluhanItems = []

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                keluhanItems << [
                    keluhan : it.t411NamaKeluhan
                ]
            }
        }else{
            keluhanItems << [
                keluhan : "-"
            ]
        }

        def jobItemsDb = JobRCP.findAllByReceptionAndStaDel(receptionInstance,"0")
        def jobItems = []
        def totalPart = 0
        def allTotal = 0
        def waktuPengerjaan = 0
        if(jobItemsDb){
            jobItemsDb.each{
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(receptionInstance,it.operation,"0")
                parts.each {
                    totalPart+=it.t403TotalRp
                }
                allTotal+=it?.t402TotalRp
                waktuPengerjaan += it.t402Rate
                jobItems << [
                    JOB : it.operation.m053NamaOperation,
                    RATE : it.t402Rate,
                    HARGA : conversi.toRupiah(it.t402TotalRp),
                ]
            }
        }
        receptionInstance?.t401TotalRp = allTotal + totalPart
        receptionInstance?.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance?.save(flush: true)

        def reportData = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def appointment = Appointment.findAllByReceptionAndStaDelAndT301TglJamRencanaBetween(receptionInstance,"0",dateAwal,dateAkhir)

        def recepPartList = PartsRCP.findAllByReceptionAndStaDel(receptionInstance,"0")
        def lParts = []
        def lMaterial = []
        if(recepPartList.size()>0){
            recepPartList.each {
                def franc = KlasifikasiGoods.findByGoods(it.goods) ? KlasifikasiGoods.findByGoods(it.goods)?.franc.m117NamaFranc:"Parts Toyota"
                    if(franc.toUpperCase().contains("PARTS")){
                        lParts << [
                                NAMA_PARTS : it?.goods?.m111Nama,
                                KODE_PARTS : it?.goods?.m111ID,
                                HARGA_PARTS : conversi.toRupiah2(it?.t403TotalRp),
                                QTY : it?.t403Jumlah1
                        ]
                    }else{
                        lMaterial << [
                                NAMA_PARTS : it?.goods?.m111Nama,
                                KODE_PARTS : it?.goods?.m111ID,
                                HARGA_PARTS : conversi.toRupiah2(it?.t403TotalRp),
                                QTY : it?.t403Jumlah1
                        ]
                    }
            }
        }else{
            lParts << [
                    NAMA_PARTS : "-",
                    KODE_PARTS : "-",
                    HARGA_PARTS : "-",
                    QTY : "-"
            ]
            lMaterial << [
                    NAMA_PARTS : "-",
                    KODE_PARTS : "-",
                    HARGA_PARTS : "-",
                    QTY : "-"
            ]
        }
        def thnProd =  FullModelVinCode.findByCustomerVehicleAndStaDel(receptionInstance?.historyCustomerVehicle?.customerVehicle, "0")
        def  tanggal = receptionInstance.companyDealer.kabKota.m002NamaKabKota
        def total = allTotal + totalPart
        def ppn = 0.1 * total
        def grand = total + ppn
        reportData << [
                NAMA_CUSTOMER : receptionInstance?.historyCustomer?.fullNama?.toUpperCase(),
                ALAMAT_CUSTOMER : receptionInstance?.historyCustomer?.t182Alamat,
                NOPOL : receptionInstance?.historyCustomerVehicle.fullNoPol,
                NO_ESTIMASI : receptionInstance?.getT401NoWO(),
                TANGGAL : tanggal + ", " + receptionInstance.dateCreated.format("dd/MM/yyyy"),
                RATE : waktuPengerjaan,
                ALLJOBITEM : jobItems,
                ALLPARTSITEM : lParts,
                ALLMATERIALITEM : lMaterial,
                TOTAL : conversi.toRupiah(total),
                PPN : conversi.toRupiah(ppn),
                GRAND_TOTAL : conversi.toRupiah(grand),
        ]
        List<JasperReportDef> reportDefList = []
        def reportDef = new JasperReportDef(name:'salesQuation.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )


        reportDefList.add(reportDef);

        def file = File.createTempFile("estimasi_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }
    def printWoGr_lama(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        receptionInstance.t401TglJamCetakWO = datatablesUtilService?.syncTime()
        receptionInstance.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance.save(flush: true)
        if(receptionInstance?.t401GambarWAC){
            byte[] bb = receptionInstance?.t401GambarWAC
            FileOutputStream output = new FileOutputStream("d:\\picWAC.jpg");
            output.write(bb);
        }
        def sa = User.findByUsernameAndStaDel(receptionInstance?.t401NamaSA,"0")
        def manPower = NamaManPower.findByUserProfileAndStaDel(sa,"0")
        def parameters = [
                namaSA : sa?.fullname ? sa?.fullname : ".....",
                noHPSA : manPower?.t015NoTelp ? manPower?.t015NoTelp : ".....",
                catatanWAC : receptionInstance?.t401CatatanWAC?receptionInstance?.t401CatatanWAC:"",
        ]

        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        def keluhanItems = []
        def permintaanItems = []

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                keluhanItems << [
                        keluhan : it.t411NamaKeluhan
                ]
            }
        }else{
            keluhanItems << [
                    keluhan : "-"
            ]
        }

        def jobItemsDb = JobRCP.findAllByReceptionAndStaDel(receptionInstance,"0")
        def jobItems = []
        def totalPart = 0
        def allTotal = 0

        if(jobItemsDb){
            jobItemsDb.each{
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(receptionInstance,it.operation,"0")
                parts.each {
                    totalPart+=it.t403TotalRp
                }
                allTotal+=it?.t402TotalRp
                jobItems << [
                        uraian : it.operation.m053NamaOperation,
                        rate : it.t402Rate,
                        total : conversi.toRupiah(it.t402TotalRp),
                ]
                permintaanItems << [
                        permintaan: it.operation.m053NamaOperation
                ]
            }
        }
        receptionInstance?.t401TotalRp = allTotal + totalPart
        receptionInstance?.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance?.save(flush: true)

        def listHistory = Reception.createCriteria().list {
            eq("staDel","0");
            eq("staSave","0");
            eq("historyCustomerVehicle",receptionInstance?.historyCustomerVehicle)
            not {
                ge("t401TanggalWO", new Date().clearTime())
                lt("t401TanggalWO", new Date().clearTime() + 1)
            }

            order("t401TanggalWO","desc")
            setMaxResults(10)
        }

        def riwayatjobItems = []

        listHistory.each {
            def sJob = ""
            def jobs = JobRCP.findAllByReceptionAndStaDel(it,"0")
            int ind=1
            jobs.each {
                if(ind==jobs.size()){
                    sJob+=it?.operation?.m053NamaOperation
                }else{
                    sJob+=it?.operation?.m053NamaOperation+", "
                }
                ind++;
            }

            def sal = User.findByUsernameAndStaDel(it?.t401NamaSA,"0")

            riwayatjobItems << [
                    tanggal : it?.t401TanggalWO ? it?.t401TanggalWO?.format("dd/MM/yyyy"):"-",
                    km : it?.t401KmSaatIni ? conversi.toKM(it?.t401KmSaatIni) : "-",
                    sa : sal?.t001Inisial ? sal?.t001Inisial : "-",
                    job : sJob
            ]

        }

        def reportData = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def appointment = Appointment.findAllByReceptionAndStaDelAndT301TglJamRencanaBetween(receptionInstance,"0",dateAwal,dateAkhir)

        def recepPartList = PartsRCP.findAllByReceptionAndStaDel(receptionInstance,"0")
        def lParts = []
        if(recepPartList.size()>0){
            recepPartList.each {
                lParts << [
                        item : it?.goods?.m111Nama,
                        noParts : it?.goods?.m111ID,
                        estParts : it?.t403TotalRp
                ]
            }
        }else{
            lParts << [
                    item : "-",
                    noParts : "-",
                    estParts : "-"
            ]
        }
        def thnProd =  FullModelVinCode.findByCustomerVehicleAndStaDel(receptionInstance?.historyCustomerVehicle?.customerVehicle, "0")

        reportData << [
                jenisWo : "GR & "+conversi.toKM(receptionInstance?.t401KmSaatIni),
                janjiDatang : receptionInstance?.t401NoAppointment != null ? receptionInstance?.t401TglJamRencana?.format("dd/MM/yyyy HH:mm") : "-",
                noWo : receptionInstance?.t401NoWO,
                noPol : receptionInstance?.historyCustomerVehicle?.fullNoPol,
                namaPetugas : appointment?.t301NamaSA ? appointment?.t301NamaSA:"-",
                staSO : "-",
                kdSA : sa?.t001Inisial ? sa?.t001Inisial:"-",
                model : receptionInstance?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                warna : receptionInstance?.historyCustomerVehicle?.warna?.m092NamaWarna,
                tglPenyerahan : receptionInstance?.t401TglJamJanjiPenyerahan?receptionInstance?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy"):"-",
                jamPenyerahan : receptionInstance?.t401TglJamJanjiPenyerahan?receptionInstance?.t401TglJamJanjiPenyerahan?.format("HH:mm"):"-",
                dlr : receptionInstance?.historyCustomerVehicle?.dealerPenjual?.m091NamaDealer,
                dd : receptionInstance?.historyCustomerVehicle?.t183TglDEC?receptionInstance?.historyCustomerVehicle?.t183TglDEC?.format("dd/MM/yyyy"):"-",
                mdl : receptionInstance?.historyCustomerVehicle?.fullModelCode,
                frm : receptionInstance?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                eg : receptionInstance?.historyCustomerVehicle?.t183NoMesin,
                thnProd : thnProd?.t109ThnBlnPembuatan?.substring(0,4),
                tglMasuk : receptionInstance?.customerIn?.t400TglCetakNoAntrian ? receptionInstance?.customerIn?.t400TglCetakNoAntrian?.format("dd/MM/yyyy"):"-",
                jamMasuk : receptionInstance?.customerIn?.t400TglCetakNoAntrian ? receptionInstance?.customerIn?.t400TglCetakNoAntrian?.format("HH:mm"):"-",
                rescTgl : "-",
                rescJam : "-",
                biayaPart : totalPart ? conversi.toRupiah(totalPart):"0",
                totalBiaya : allTotal ? conversi.toRupiah(allTotal + totalPart):"0",
                changeCost : "0",
                jenisBayar : "-",
                gantiPart : "-",
                nama : receptionInstance?.historyCustomer?.fullNama?.toUpperCase(),
                alamat : receptionInstance?.historyCustomer?.t182Alamat,
                noTelp : receptionInstance?.historyCustomer?.t182NoHp,
                keluhanItems : keluhanItems,
                jobItems : jobItems,
                riwayatjobItems : riwayatjobItems,
                permintaanItems : permintaanItems
        ]
        List<JasperReportDef> reportDefList = []
        def reportDef = new JasperReportDef(name:'WorkOrderGr.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )


        reportDefList.add(reportDef);

        def file = File.createTempFile("estimasi_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }

    def printWoBp(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        receptionInstance.t401TglJamCetakWO = datatablesUtilService?.syncTime()
        receptionInstance.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance.save(flush: true)
        if(receptionInstance?.t401GambarWAC){
            byte[] bb = receptionInstance?.t401GambarWAC
            FileOutputStream output = new FileOutputStream("d:\\picWAC.jpg");
            output.write(bb);
        }
        def sa = User.findByUsernameAndStaDel(receptionInstance?.t401NamaSA,"0")
        def manPower = NamaManPower.findByUserProfileAndStaDel(sa,"0")
        def parameters = [
                namaSA : sa?.fullname ? sa?.fullname : ".....",
                noHPSA : manPower?.t015NoTelp ? manPower?.t015NoTelp : ".....",
                catatanWAC : receptionInstance?.t401CatatanWAC?receptionInstance?.t401CatatanWAC:"",
        ]

        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        def keluhanItems = []
        def permintaanItems = []

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                keluhanItems << [
                        keluhan : it.t411NamaKeluhan
                ]
            }
        }else{
            keluhanItems << [
                    keluhan : "-"
            ]
        }

        def jobItemsDb = JobRCP.findAllByReceptionAndStaDel(receptionInstance,"0")
        def jobItems = []
        def totalPart = 0
        def allTotal = 0

        if(jobItemsDb){
            jobItemsDb.each{
                def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(receptionInstance,it.operation,"0")
                parts.each {
                    totalPart+=it.t403TotalRp
                }
                allTotal+=it?.t402TotalRp
                jobItems << [
                        uraian : it.operation.m053NamaOperation,
                        rate : it.t402Rate,
                        total : conversi.toRupiah(it.t402TotalRp),
                ]

                permintaanItems << [
                        permintaan: it.operation.m053NamaOperation
                ]
            }
        }
        receptionInstance?.t401TotalRp = allTotal + totalPart
        receptionInstance?.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance?.save(flush: true)

        def listHistory = Reception.createCriteria().list {
            eq("staDel","0");
            eq("staSave","0");
            eq("historyCustomerVehicle",receptionInstance?.historyCustomerVehicle);
            not {
                ge("t401TanggalWO", new Date().clearTime())
                lt("t401TanggalWO", new Date().clearTime() + 1)
            }

            order("t401TanggalWO","desc")
            setMaxResults(10)
        }

        def riwayatjobItems = []

        listHistory.each {
            def sJob = ""
            def jobs = JobRCP.findAllByReceptionAndStaDel(it,"0")
            int ind=1
            jobs.each {
                if(ind==jobs.size()){
                    sJob+=it?.operation?.m053NamaOperation
                }else{
                    sJob+=it?.operation?.m053NamaOperation+", "
                }
                ind++;
            }

            def sal = User.findByUsernameAndStaDel(it?.t401NamaSA,"0")

            riwayatjobItems << [
                    tanggal : it?.t401TanggalWO ? it?.t401TanggalWO?.format("dd/MM/yyyy"):"-",
                    km : it?.t401KmSaatIni ? conversi.toKM(it?.t401KmSaatIni) : "-",
                    sa : sal?.t001Inisial ? sal?.t001Inisial : "-",
                    job : sJob
            ]

        }

        def reportData = []
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def appointment = Appointment.findAllByReceptionAndStaDelAndT301TglJamRencanaBetween(receptionInstance,"0",dateAwal,dateAkhir)
        def recepPartList = PartsRCP.findAllByReceptionAndStaDel(receptionInstance,"0")
        def lParts = []
        if(recepPartList.size()>0){
            recepPartList.each {
                lParts << [
                        item : it?.goods?.m111Nama,
                        noParts : it?.goods?.m111ID,
                        estParts : it?.t403TotalRp
                ]
            }
        }else{
            lParts << [
                    item : "-",
                    noParts : "-",
                    estParts : "-"
            ]
        }
        def thnProd =  FullModelVinCode.findByCustomerVehicleAndStaDel(receptionInstance?.historyCustomerVehicle?.customerVehicle, "0")

        reportData << [
                jenisWo : "BP & "+conversi.toKM(receptionInstance?.t401KmSaatIni),
                janjiDatang : receptionInstance?.t401NoAppointment != null ? receptionInstance?.t401TglJamRencana?.format("dd/MM/yyyy HH:mm") : "-",
                noWo : receptionInstance?.t401NoWO,
                noPol : receptionInstance?.historyCustomerVehicle?.fullNoPol,
                namaPetugas : appointment?.t301NamaSA ? appointment?.t301NamaSA:"-",
                staSO : "-",
                kdSA : sa?.t001Inisial ? sa?.t001Inisial:"-",
                model : receptionInstance?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                warna : receptionInstance?.historyCustomerVehicle?.warna?.m092NamaWarna,
                tglPenyerahan : receptionInstance?.t401TglJamJanjiPenyerahan?receptionInstance?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy"):"-",
                jamPenyerahan : receptionInstance?.t401TglJamJanjiPenyerahan?receptionInstance?.t401TglJamJanjiPenyerahan?.format("HH:mm"):"-",
                dlr : receptionInstance?.historyCustomerVehicle?.dealerPenjual?.m091NamaDealer,
                dd : receptionInstance?.historyCustomerVehicle?.t183TglDEC?receptionInstance?.historyCustomerVehicle?.t183TglDEC?.format("dd/MM/yyyy"):"-",
                mdl : receptionInstance?.historyCustomerVehicle?.fullModelCode,
                frm : receptionInstance?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                eg : receptionInstance?.historyCustomerVehicle?.t183NoMesin,
                thnProd : thnProd?.t109ThnBlnPembuatan?.substring(0,4),
                tglMasuk : receptionInstance?.customerIn?.t400TglCetakNoAntrian ? receptionInstance?.customerIn?.t400TglCetakNoAntrian?.format("dd/MM/yyyy"):"-",
                jamMasuk : receptionInstance?.customerIn?.t400TglCetakNoAntrian ? receptionInstance?.customerIn?.t400TglCetakNoAntrian?.format("HH:mm"):"-",
                rescTgl : "-",
                rescJam : "-",
                biayaPart : totalPart ? conversi.toRupiah(totalPart):"0",
                totalBiaya : allTotal ? conversi.toRupiah(allTotal + totalPart):"0",
                changeCost : "0",
                jenisBayar : "-",
                gantiPart : "-",
                nama : receptionInstance?.historyCustomer?.fullNama?.toUpperCase(),
                alamat : receptionInstance?.historyCustomer?.t182Alamat,
                noTelp : receptionInstance?.historyCustomer?.t182NoHp,
                keluhanItems : keluhanItems,
                jobItems : jobItems,
                riwayatjobItems : riwayatjobItems,
                permintaanItems : permintaanItems
        ]
        List<JasperReportDef> reportDefList = []
        def reportDef = new JasperReportDef(name:'WorkOrderGr.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )


        reportDefList.add(reportDef);

        def file = File.createTempFile("wobp_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }

    def saveJobInput(){
        def result = [success: "1"]

        def id = params.idReception
        def countRow = new Integer(params.countRow)
        def indexRow = 1
        def keluhanRcp
        def totalRp = 0
        while(indexRow < countRow){
            keluhanRcp = new KeluhanRcp()
            keluhanRcp.reception = Reception.get(id)
            keluhanRcp.companyDealer = session.userCompanyDealer
            keluhanRcp.t411NoUrut = new Double(params.get("t411NoUrut" + indexRow))
            keluhanRcp.t411NamaKeluhan = params.get("t411NamaKeluhan" + indexRow)
            keluhanRcp.t411StaButuhDiagnose = params.get("t411StaButuhDiagnose" + indexRow)
            keluhanRcp.staDel = 0
            keluhanRcp.createdBy = "system"
            keluhanRcp.lastUpdProcess = "INSERT"
            keluhanRcp?.dateCreated = datatablesUtilService?.syncTime()
            keluhanRcp?.lastUpdated = datatablesUtilService?.syncTime()

            keluhanRcp.save(flush:true)
            indexRow++
        }

        def countRowJob = new Integer(params.countRowJob)
        indexRow = 1
        def jobRCP = null
        def receptionInstance = Reception.get(id)
        def histCV = CustomerVehicle.findByT103VinCodeAndStaDel(params.idVincode,"0");
        receptionInstance?.t401KmSaatIni = params.kmSekarang.toLong()
        receptionInstance?.historyCustomerVehicle = histCV.getCurrentCondition()
        receptionInstance?.lastUpdated = datatablesUtilService?.syncTime()
        receptionInstance?.save(flush: true)
        while(indexRow < countRowJob){
            def operationInstance = Operation.get(params.get("operation" + indexRow))
            def jobSuggest = JobSuggestion.findByCustomerVehicleAndStaDel(histCV,"0")
            if(jobSuggest){
                if(params.jobSuggest){
                    def jobSNew = new JobSuggestion()
                    jobSNew.properties = jobSuggest.properties
                    jobSNew.t503KetJobSuggest = params.jobSuggest
                    jobSNew.dateCreated = datatablesUtilService?.syncTime()
                    jobSNew.lastUpdated = datatablesUtilService?.syncTime()
                    jobSNew.save(flush: true)
                }
                jobSuggest?.lastUpdated = datatablesUtilService?.syncTime()
                jobSuggest?.staDel = "1"
                jobSuggest?.save(flush: true);
            }
            jobRCP = JobRCP.findByOperationAndReceptionAndStaDel(operationInstance,receptionInstance,"0");
            if(jobRCP){
                jobRCP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                jobRCP.lastUpdProcess = "UPDATE"
            }else {
                jobRCP = new JobRCP()
                jobRCP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                jobRCP.lastUpdProcess = "INSERT"
                jobRCP?.dateCreated = datatablesUtilService?.syncTime()
            }
            jobRCP?.lastUpdated = datatablesUtilService?.syncTime()
            if(params.staTambah){
                jobRCP.t402StaTambahKurang = params.staTambah
                jobRCP.t402TglTambahKurang = datatablesUtilService?.syncTime()
            }
            if(!jobRCP?.t402Rate || (jobRCP?.t402Rate && jobRCP?.t402Rate==0)){
                def rate = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(operationInstance,histCV?.getCurrentCondition()?.fullModelCode?.baseModel,"0",new Date(),[sort: 'id',order: 'desc'])
                jobRCP?.t402Rate = (rate?.t113FlatRate?rate?.t113FlatRate:0)            }
            if(!jobRCP?.t402HargaRp || (jobRCP?.t402HargaRp && jobRCP?.t402HargaRp==0)){
                def nominalValue = 0
                def rate = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(operationInstance,histCV?.getCurrentCondition()?.fullModelCode?.baseModel,"0",new Date(),[sort: 'id',order: 'desc'])
                def harga = TarifPerJam.findByKategoriAndStaDelAndCompanyDealerAndT152TMTLessThanEquals(histCV?.getCurrentCondition()?.fullModelCode?.baseModel?.kategoriKendaraan,"0",session?.userCompanyDealer,(new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
                def nominal1 = harga && rate ? harga?.t152TarifPerjamBP * rate?.t113FlatRate : 0
                def nominal2 = harga && rate ? harga?.t152TarifPerjamSB * rate?.t113FlatRate : 0
                def nominal3 = harga && rate ? harga?.t152TarifPerjamGR * rate?.t113FlatRate : 0
                def nominal5 = harga?.t152TarifPerjamSBI && rate ? harga?.t152TarifPerjamSBI * rate?.t113FlatRate : 0
                def nominal6 = harga?.t152TarifPerjamSPO && rate ? harga?.t152TarifPerjamSPO * rate?.t113FlatRate : 0
                if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("BP")){
                    nominalValue = nominal1
                }else if(operationInstance?.kategoriJob?.m055KategoriJob?.contains("SB")){
                    nominalValue = nominal2
                }else if(operationInstance?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SBI")){
                    nominalValue = nominal5
                }else{
                    if(operationInstance?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")){
                        if(harga?.t152TarifPerjamSPO != null){
                            nominalValue = nominal6
                        }else{
                            nominalValue = nominal3
                        }
                    }else{
                        nominalValue = nominal3
                    }
                }
                jobRCP?.t402HargaRp = (nominalValue?nominalValue:0)
                jobRCP?.t402TotalRp = (nominalValue?nominalValue:0)
                jobRCP?.t402Rate = (rate?.t113FlatRate?rate?.t113FlatRate:0)
            }
            jobRCP.companyDealer = session.userCompanyDealer
            jobRCP.reception = receptionInstance
            jobRCP.t402StaIntExt = "i"
            jobRCP.operation = operationInstance
            jobRCP.t402NoKeluhan = params.get("t702NoUrutKeluhan" + indexRow)
            jobRCP.statusWarranty = StatusWarranty.get(params.statusWarranty.toLong())
            if(params.workshopSumber){
                jobRCP.companyDealerIDSumber = CompanyDealer.get(params.workshopSumber.toLong())
            }
            if(params.workshopTujuan){
                jobRCP.companyDealerIDTujuan = CompanyDealer.get(params.workshopTujuan.toLong())
            }
            jobRCP.staDel = 0

            jobRCP.save(flush:true)
            jobRCP.errors.each {println it}
            def kategori = KategoriJob.get(params?.kategoriJob?.toLong())
            def parts = PartJob.findAllByFullModelCodeAndOperation(histCV?.getCurrentCondition()?.fullModelCode,operationInstance)
            parts.each{
                def partRCP = PartsRCP.findByReceptionAndOperationAndGoodsAndStaDel(receptionInstance,it?.operation,it?.goods,"0");
                if(!partRCP){
                    partRCP = new PartsRCP()
                    partRCP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    partRCP.lastUpdProcess = "INSERT"
                    partRCP?.dateCreated = datatablesUtilService?.syncTime()
                }else{
                    partRCP.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    partRCP.lastUpdProcess = "UPDATE"
                }
                if(params.staTambah){
                    partRCP.t403StaTambahKurang = params.staTambah
                    partRCP.t403TglJamTambahKurang = datatablesUtilService?.syncTime()
                }
                partRCP?.lastUpdated = datatablesUtilService?.syncTime()
                def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it?.goods, "0",new Date(),[sort: "t151TMT",order: "desc"])
                def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                    eq("staDel","0");
                    eq("companyDealer",session?.userCompanyDealer);
                }
                def mappingRegion = MappingPartRegion.createCriteria().get {
                    if(mappingCompRegion?.size()>0){
                        inList("region",mappingCompRegion?.region)
                    }else{
                        eq("id",-10000.toLong())
                    }
                    maxResults(1);
                }
                def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
                if(mappingRegion){
                    hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
                }
                partRCP.reception = receptionInstance
                partRCP.operation = it?.operation
                partRCP.goods = it?.goods
                partRCP.t403StaIntExt = "i"
                partRCP.t403HargaRp = hargaTanpaPPN
                partRCP.t403Jumlah1 = it.t112Jumlah
                partRCP.t403Jumlah2 = it.t112Jumlah
                partRCP.t403TotalRp = it.t112Jumlah * hargaTanpaPPN
                partRCP.staDel = 0


                partRCP.save(flush: true)
                partRCP.errors.each {println it}

            }
            indexRow++
        }

        render result as JSON
    }

    def savePartInput(){
        def result = [success: "1"]

        def id = params.idReception
        def job = JobRCP.get(params.idJob.toLong())
        def countRow = new Integer(params.countRowPart)
        int indexRow = 1;
        def partRCP;
        while(indexRow < countRow){
            def goodsInstance = Goods.get(params.get("good" + indexRow))
            def qty = params.get("qty" + indexRow).toDouble()
            def receptionInstance = Reception.get(id)
            partRCP = PartsRCP.findByReceptionAndOperationAndGoodsAndStaDel(receptionInstance,job.operation,goodsInstance,"0");
            if(!partRCP){
                partRCP = new PartsRCP()
                partRCP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                partRCP.lastUpdProcess = "INSERT"
                partRCP?.dateCreated = datatablesUtilService?.syncTime()
            }else{
                partRCP.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                partRCP.lastUpdProcess = "UPDATE"
            }
            partRCP?.lastUpdated = datatablesUtilService?.syncTime()
            if(params.staTambah){
                partRCP.t403StaTambahKurang = params.staTambah
                partRCP.t403TglJamTambahKurang = datatablesUtilService?.syncTime()
            }
            def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(goodsInstance,  "0",new Date(),[sort: "t151TMT",order: "desc"])
            def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                eq("staDel","0");
                eq("companyDealer",session?.userCompanyDealer);
            }
            def mappingRegion = MappingPartRegion.createCriteria().get {
                if(mappingCompRegion?.size()>0){
                    inList("region",mappingCompRegion?.region)
                }else{
                    eq("id",-10000.toLong())
                }
                maxResults(1);
            }
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
            if(mappingRegion){
                hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
            }
            partRCP.reception = receptionInstance
            partRCP.operation = job.operation
            partRCP.goods = goodsInstance
            partRCP.t403StaIntExt = "i"
            partRCP.t403HargaRp = hargaTanpaPPN
            partRCP.t403Jumlah1 = qty
            partRCP.t403Jumlah2 = qty
            partRCP.t403TotalRp = qty * hargaTanpaPPN
            partRCP.staDel = 0


            partRCP.save(flush: true)
            partRCP.errors.each {println it}
            indexRow++
        }

        render result as JSON
    }

    def getDataKeluhan(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        String htmlData = ""

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " "+it.t411NoUrut+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t411NamaKeluhan+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t411StaButuhDiagnose+"\n" +
                        " </td>\n" +
                        "</tr>"
            }
        }

        render htmlData as String
    }

    def getDataJob(){
        def id = params.id
        def receptionInstance = Reception.get(id)
        def jobItemsDb = JobInv.findAllByReception(receptionInstance)
        String htmlData = ""

        if(jobItemsDb){
            jobItemsDb.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " "+it.operation.m053Id+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.operation.m053NamaOperation+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+conversi.toRupiah(it.t702HargaRp)+"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+it.t702NoUrutKeluhan+"\n" +
                        " </td>\n" +
                        "</tr>"
            }
        }
        render htmlData as String
    }

    def addPartDatatablesList() {
        render salesQuotationService.addPartDatatablesList(params) as JSON
    }

    def savePart() {
        params.userCompanyDealer = session.userCompanyDealer
        def result = salesQuotationService.savePart(params)
        render result as JSON
    }

    def saveSummary() {
        def result = salesQuotationService.saveSummary(params)
        render result as JSON
    }

    def getDataJobSummary(){
        def noWo = params.noWo
        def receptionInstance = Reception.findByT401NoWO(noWo)
        def jobItemsDb = JobRCP.findAllByReceptionAndStaDel(receptionInstance, "0")
        String htmlData = ""

        def subTotal = 0
        def hargaParts = 0
        def no = 1
        def ppn = 0
        def grandTotal = 0
        def bookingFee = 0
        def hargaJob = 0
        def tot = 0
        if(jobItemsDb){
            jobItemsDb.each{
                hargaParts = 0
                def partRcp = PartsRCP.findAllByReceptionAndOperation(receptionInstance,it.operation)
                partRcp.each {
                    hargaParts += it.t403HargaRp?it.t403HargaRp:0
                }
                hargaJob = it.t402HargaRp?it.t402HargaRp:0
                tot = hargaParts + hargaJob
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " "+ (no++) +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ it.operation?.m053NamaOperation +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ conversi.toRupiah(hargaJob) +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ conversi.toRupiah(hargaParts) +"\n" +
                        " </td>\n" +
                        " <td>\n" +
                        " "+ conversi.toRupiah(tot) +"\n" +
                        " </td>\n" +
                        "</tr>"

                subTotal+= tot
            }

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " \n"+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    "</tr>"

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " SUB TOTAL "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+conversi.toRupiah(subTotal)+"\n" +
                    " </td>\n" +
                    "</tr>"

            ppn = subTotal * (10/100)
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " PPN "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " 10 %\n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+conversi.toRupiah(ppn)+"\n" +
                    " </td>\n" +
                    "</tr>"

            grandTotal = ppn + subTotal
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " GRAND TOTAL "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " "+conversi.toRupiah(grandTotal)+"\n" +
                    " </td>\n" +
                    "</tr>"

            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " \n"+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    "</tr>"

            bookingFee = grandTotal * (25/100)
            def totBF = receptionInstance?.t401DPRp ? receptionInstance?.t401DPRp : 0
            htmlData+=	"<tr>\n" +
                    " <td>\n" +
                    " BOOKING FEE "+
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " \n" +
                    " </td>\n" +
                    " <td>\n" +
                    " Min. 25 %\n" +
                    " </td>\n" +
                    " <td>\n" +
                    " <input id=\"bookingfee-"+receptionInstance?.id+"\" onblur=\"saveChangeData('bookingfee-"+receptionInstance?.id+"');\" type=\"text\" value=\""+totBF+"\">\n" +
                    " </td>\n" +
                    "</tr>";
            if(receptionInstance?.customerIn?.tujuanKedatangan?.m400Tujuan?.equalsIgnoreCase("BP")){
                def totOR = receptionInstance?.t401OnRisk ? receptionInstance?.t401OnRisk : 0
                htmlData+="<tr>\n" +
                        " <td>\n" +
                        " ON RISK "+
                        " </td>\n" +
                        " <td>\n" +
                        " \n" +
                        " </td>\n" +
                        " <td>\n" +
                        " \n" +
                        " </td>\n" +
                        " <td>\n" +
                        " \n" +
                        " </td>\n" +
                        " <td>\n" +
                        " <input id=\"onrisk-"+receptionInstance?.id+"\" onblur=\"saveChangeData('onrisk-"+receptionInstance?.id+"');\" type=\"text\" value=\""+totOR+"\">\n" +
                        " </td>\n" +
                        "</tr>"
            }
        }
        render htmlData as String
    }

    def getDataKeluhanSummary(){
        def id = params.noWo
        def receptionInstance = Reception.findByT401NoWO(id)
        def keluhanItemsDb = KeluhanRcp.findAllByReception(receptionInstance)
        String htmlData = ""

        if(keluhanItemsDb){
            keluhanItemsDb.each{
                htmlData+=	it.t411NoUrut+". "+it.t411NamaKeluhan+"\n"
            }
        }

        render htmlData as String
    }

    def getDataPembayaran(){
        def id = params.noWo
        def receptionInstance = Reception.findByT401NoWO(id)
        def settlementItemsDb = Settlement.findAllByReception(receptionInstance)
        String htmlData = ""

        int idx = 1
        if(settlementItemsDb){
            settlementItemsDb.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " " +(idx++)+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.metodeBayar.toString()+
                        " </td>\n" +
                        " <td style='text-align:right;'>\n" +
                        " " +conversi.toRupiah(it.t704JmlBayar)+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.t704keterangan+
                        " </td>\n" +
                        "</tr>"
            }

        }

        render htmlData as String
    }

    def showReception() {
        def result = salesQuotationService.showReception(params)

        render result as JSON
    }

    def datatablesListParts(){
        def rows = []
        def ret = [sEcho: params.sEcho, iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: rows]

        render ret as JSON
    }

    def changeValue(){
        def parameter = params.id.split("-")
        def recept = Reception.get(parameter[1].toLong())
        if(parameter[0].toString().equalsIgnoreCase("bookingfee")){
            recept?.t401DPRp = params?.nilai?.toDouble()
        }else{
            recept?.t401OnRisk = params?.nilai?.toDouble()
        }
        recept?.save(flush: true)
        render "ok"
    }

    def getInputParts(){
        def receptionInstance = Reception.get(params.id)
        def htmlData = ""
        def partsApps = PartsApp.findByReception(receptionInstance)
        if(partsApps){
            partsApps.each{
                htmlData+=	"<tr>\n" +
                        " <td>\n" +
                        " " +it.goods.m111ID+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.goods.m111Nama+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.namaProsesBP+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.t303Jumlah1+
                        " </td>\n" +
                        " <td>\n" +
                        " " +it.goods.satuan+
                        " </td>\n" +
                        "</tr>"
            }
        }

        render htmlData as String
    }

    def requestPart() {

    }

    def cancelEditBookingFee() {

    }

    def requestPartDatatablesList() {
        render salesQuotationService.requestPartDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def estimasiDatatablesList() {
        render salesQuotationService.estimasiDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def cancelEditBookingFeeDatatablesList(){
        render salesQuotationService.cancelEditBookingFeeDatatablesList(params, session.userCompanyDealer) as JSON
    }

    def jobKeluhan(){
        render salesQuotationService.jobKeluhan(params) as JSON
    }

    def showEstimasi(){
        def nopol = KodeKotaNoPol.findById(params.kode as Long).m116ID + " " + params.tengah + " " + params.belakang
        def model = HistoryCustomerVehicle.findByFullNoPol(nopol)?.fullModelCode?.baseModel?.m102NamaBaseModel
        def tglEstimasi = new Date().format("dd MMMM yyyy")
        render(view: "_receptionEstimasi", model: [nopol: nopol, tglEstimasi : tglEstimasi, models : model, noWo : params.noWo])
    }

    def approval(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWO(params.noWo)
        def recEdit = Reception.get(rec.id)
        recEdit.alasanCancel = AlasanCancel.findById(params.alasanCancel)
        recEdit.save(flush: true)
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:rec.id as String,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{ println it }

        render "ok"
    }

    def approvalRate(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWOAndStaDel(params.noWo,"0")
        def jobRcp = JobRCP.get(params.idJob.toLong())
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:jobRcp?.id?.toString()+"#"+params.rate,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO+"-"+jobRcp?.operation?.m053JobsId,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{ println it }

        render "ok"
    }

    def approvalDiscount(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWO(params.noWo)
        rec.staApprovalDisc = "2"
        rec.save(flush: true)
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:rec.id as String,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{ println it }

        render "ok"
    }

    def approvalSPK(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def rec = Reception.findByT401NoWO(params.noWo)
        rec.t401StaButuhSPKSebelumProd = "1"
        rec.save(flush: true)
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:rec.id as String,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{ println it }

        render "ok"
    }

    def approvalPiutang(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def mobil = HistoryCustomerVehicle.createCriteria().get {
            customerVehicle{
                eq("t103VinCode",params.idVincode,[ignoreCase: true])
            }
            order("dateCreated","desc")
            maxResults(1)
        }
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:mobil?.id?.toString(),
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: mobil?.fullNoPol,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{ println it }

        render "ok"
    }

    def approvalKurangiJob(){

        def kegiatanApproval = KegiatanApproval.get(params.kegiatanApproval)
        def jsonArray = JSON.parse(params.jobs)
        def jsonArray2 = JSON.parse(params.parts)
        def rec = Reception.findByT401NoWO(params.noWo)
        String idJob="";
        def ind=1;
        def lengArray = jsonArray.size()
        jsonArray.each{
            def jobRcp = JobRCP.get(it.toLong());
            jobRcp.t402StaTambahKurang = "1"
            jobRcp.t402StaApproveTambahKurang = "2"
            jobRcp.lastUpdated = datatablesUtilService?.syncTime()
            jobRcp.save(flush: true)
            idJob+=it
            if(ind<lengArray){
                idJob+="-"
            }
        }
        def ind2=1
        jsonArray2.each {
            def id = it.toString().substring(0,it.toString().indexOf("*"))
            def partRCP = PartsRCP.get(id.toLong());
            if(partRCP){
                if(!partRCP?.t403StaApproveTambahKurang?.equalsIgnoreCase("0") || partRCP?.t403StaApproveTambahKurang==""){
                    partRCP.t403StaTambahKurang = "1"
                    partRCP.t403StaApproveTambahKurang = "2"
                    partRCP.lastUpdated = datatablesUtilService?.syncTime()
                    partRCP.save(flush: true)
                    if(idJob.length()>0){
                        if(ind2<jsonArray2.size()){
                            idJob+="-"
                        }
                    }
                    idJob+=it
                    ind2++
                }
            }
        }

        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:rec.id.toString()+"#"+idJob,
                kegiatanApproval: kegiatanApproval,
                t770NoDokumen: rec?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )

        approval.save(flush:true)
        approval.errors.each{ println it }

        render "ok"
    }

    def addPembayaran(){
        def result = [:]
        def setlement = new Settlement()
        setlement.metodeBayar = MetodeBayar.findById(params.metodeBayar as Long)
        setlement.t704keterangan = params.ket
        setlement.t704JmlBayar = params.jumlah as Double
        setlement.staDel = '0'
        setlement.lastUpdProcess = 'insert'
        setlement.t704ID = 2
        setlement.t704TglJamSettlement = datatablesUtilService?.syncTime()
        setlement.reception = Reception.findByT401NoWO(params.noWo)
        setlement.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        setlement.dateCreated = datatablesUtilService?.syncTime()
        setlement.lastUpdated = datatablesUtilService?.syncTime()
        setlement.save(flush: true)
        setlement.errors.each {
            println it
        }
        println result.countPembayaran = Settlement.findAllByReception(Reception.findByT401NoWO(params.noWo)).size()
        render result  as JSON
    }

    def hitungEstimasi(){
        def res = [:]
        def stdWork = StdTimeWorkItems.findAll()
        def waktu = 0, body = 0, asembly = 0, polishing = 0, painting = 0, preparation = 0
        if(params.kerusakan=='Mudah'){
            stdWork.each{
                waktu += it.m041RdanR
            }
        }else if(params.kerusakan=='Medium'){
            stdWork.each{
                waktu += it.m041Replace
            }
        }else{
            stdWork.each{
                waktu += it.m041Overhaul
            }
        }

        //body & assembly
        def gm = GeneralParameter.first()
        body = (waktu * (gm.m000PersenPelepasanPartBP / 100)).intValue()
        asembly = (waktu * (gm.m000PersenPemasanganPartBP / 100)).intValue()

        //perhitungan Waktu
        def pat = PaintingApplicationTime.findById(1).m044NewMultiple //waktu 1
        def cm = ColorMatching.findById(params.colorKlasifikasi as Long).m046StdTime //waktu 2
        def bpt = BumperPaintingTime.findById(1).m0452Def //waktu 3
        def sub = 0
        sub = pat + cm + sub

        polishing = (sub * (gm.m000PersenPoleshing / 100)).intValue()
        preparation = (sub * (gm.m000PersenPreparation / 100)).intValue()
        painting = (sub * (gm.m000PersenPainting / 100)).intValue()
        res.bodi = body
        res.asembly = asembly
        res.polishing = polishing
        res.preparation = preparation
        res.painting = painting

        render res as JSON
    }

    def saveEstimasi(){
        def rec = Reception.findByT401NoWO(params.noWo)
        def reception = Reception.get(rec?.id)
        reception.t401Proses1 = params.body as Double
        reception.t401Proses2 = params.preparation as Double
        reception.t401Proses3 = params.painting as Double
        reception.t401Proses4 = params.polishing as Double
        reception.t401Proses5 = params.asembly as Double
        reception.t401TotalProses = params.total as Double
        reception.colorMatchingJmlPanel = ColorMatching.findById(params.colorPanel as Long)
        reception.colorMatchingKlasifikasi = ColorMatching.findById(params.colorKlasifikasi as Long)
        reception.tipeKerusakan = TipeKerusakan.findByM401NamaTipeKerusakan(params.kerusakan)
        reception?.save(flush: true)
        reception.errors.each {
            println it
        }
        render "ok"
    }

    def ganti(){
        def job = JobRCP.get(params.idChange.toLong())
        job?.t402StaIntExt = params.value
        job.save(flush: true)
        render "oke"
    }

    def confirm(){
        def reception = Reception.get(params.id.toLong())
        reception?.t401StaButuhSPKSebelumProd="0"
        reception?.save(flush: true)
        render "oke"
    }

    def previewEstimasi(){
        List<JasperReportDef> reportDefList = []
        def file = null
        def reportData = reportEstimasi(params)
        def reportDef = new JasperReportDef(name:'ServiceEstimation.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )
        reportDefList.add(reportDef)
        file = File.createTempFile("ESTIMASI_",".pdf")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }

    def reportEstimasi(def params){
        def reportData = new ArrayList();
        def data = [:]
        def reception = Reception.get(params?.id?.toLong())
        def jasa = 0, part =0, rate =0
        def jobs = JobRCP?.findAllByReceptionAndStaDel(reception,"0")
        jobs.each {
            jasa+= it?.t402TotalRp
            rate+= it?.t402Rate
            def parts = PartsRCP.findAllByReceptionAndOperationAndStaDel(reception,it?.operation,"0")
            parts?.each {
                part+= it?.t403TotalRp
            }
        }
        data.put("nowo",reception?.t401NoWO)
        data.put("nopol", reception?.historyCustomerVehicle?.fullNoPol)
        data.put("model", reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel)
        data.put("pemilik", reception?.historyCustomer?.fullNama)
        data.put("jasa", conversi.toRupiah(jasa))
        data.put("part", conversi.toRupiah(part))
        data.put("total", conversi.toRupiah(jasa+part))
        data.put("waktu", rate)
        reportData.add(data)
        return reportData
    }

    def jobDetails(){
        def kategori = KategoriJob.get(params?.kategori?.toLong())
        def result = []
        def jobs = JobPackage.createCriteria().list {
            eq("kategori",kategori)
            if(kategori && (kategori?.m055KategoriJob=="GR" || kategori?.m055KategoriJob=="BP")){
                eq("id",-1000.toLong())
            }
        }

        jobs?.each {
            def rate = FlatRate?.findByOperationAndT113TMTLessThanEquals(it?.job , new Date())
            result << [
                    idJob : it?.job?.id,
                    kodeJob : it?.job?.m053Id,
                    rate : rate ? rate?.t113FlatRate : 0,
                    namaJob : it?.job?.m053NamaOperation
            ]
        }
        render result as JSON
    }

    def updateHarga(){
        if(params?.dari=="jasa"){
            def jobs = JobRCP.get(params?.id?.toLong())
            if(params?.harga){
                def hargaBaru = params?.harga?.toString()?.replaceAll(",","")
                if(hargaBaru?.isNumber()){
                    jobs?.t402HargaRp = hargaBaru?.toDouble()
                    jobs?.t402TotalRp = hargaBaru?.toDouble()
                    jobs?.save(flush: true)
                }
            }
        }else{
            def part = PartsRCP.get(params?.id?.toLong())
            if(params?.harga){
                def hargaBaru = params?.harga?.toString()?.replaceAll(",","")
                if(hargaBaru?.isNumber()){
                    part?.t403HargaRp = hargaBaru?.toDouble()
                    part?.t403TotalRp = (part?.t403HargaRp * part?.t403Jumlah1)
                    part?.save(flush: true)
                    part?.errors?.each {println it}
                }
            }
        }
        render "ok"
    }
}
