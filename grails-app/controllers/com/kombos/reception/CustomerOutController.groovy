package com.kombos.reception

import com.kombos.administrasi.FullModelVinCode
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerSurvey
import com.kombos.customerprofile.CustomerSurveyDetail
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.MappingCustVehicle
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class CustomerOutController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def customerInService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
//		redirect(action: "list", params: params)
		redirect(action: "create", params: params)
	}

	def create() {
        [customerInInstance: new CustomerIn(params)]
	}


    def backHome(){
        if(params?.idReception){
            def recept = Reception.get(params?.idReception?.toLong())
            def custIn = recept?.customerIn
            if(custIn){
                custIn?.t400StaOut = "0"
                custIn?.t400TglJamOut  = datatablesUtilService?.syncTime()
                custIn.save(flush: true)
            }
        }
        render "ok"
    }

    def findNoPol(){
        def hasil = []
        params.companyDealer = session.userCompanyDealer
        String noPol = params.kode+" "+params.tengah+" "+params.belakang
        def results = HistoryCustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("fullNoPol",noPol, [ignoreCase : true])
        }
        if(results.size()>0){
            def mapTemp = new HistoryCustomer()
            def gatePasTemp = new GatePassT800()
            def custSurvey = new CustomerSurvey()
            def maping = MappingCustVehicle.createCriteria().list {
                eq("customerVehicle",results?.last()?.customerVehicle);
                order("dateCreated","desc");
            }
            if(maping){
                for(cari in maping) {
                    mapTemp = HistoryCustomer.findByCustomerAndStaDel(cari?.customer,"0")
                    if(cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.equalsIgnoreCase("Pemilik dan Penanggung Jawab") || cari?.customer?.peranCustomer?.m115NamaPeranCustomer?.toLowerCase()?.contains("pemilik")){
                        break
                    }
                }
            }
            def receptions = Reception.createCriteria().list {
                eq("staDel","0");
                eq("staSave","0");
                eq("historyCustomerVehicle",results.last());
                order("dateCreated")
            }
            if(receptions){
                gatePasTemp = GatePassT800.createCriteria().get {
                    eq("staDel","0")
                    reception{
                        historyCustomerVehicle{
                            ilike("fullNoPol",results?.last()?.fullNoPol)
                        }
                    }
                    ge("dateCreated",new Date().clearTime());
                    lt("dateCreated",(new Date().clearTime()+1))
                    order("dateCreated","desc");
                    maxResults(1);
                }
            }
            def mappingSurvey = CustomerSurveyDetail.findAllByCustomerVehicleAndStaDel(results?.last()?.customerVehicle,"0")
            if(mappingSurvey){
                custSurvey = mappingSurvey?.last()?.customerSurvey
            }
            def fullMVCTemp = FullModelVinCode.findByFullModelCodeAndStaDel(results?.last()?.fullModelCode,"0")
            hasil << [
                    idCV : results?.last()?.id,
                    mobil : results?.last()?.fullModelCode?.baseModel?.m102NamaBaseModel ? results?.last()?.fullModelCode?.baseModel?.m102NamaBaseModel:"-",
                    tahun : fullMVCTemp?.t109ThnBlnPembuatan?.substring(0,4),
                    warna : results?.last()?.warna?.m092NamaWarna ? results?.last()?.warna?.m092NamaWarna:"-",
                    namaCustomer : mapTemp?.fullNama ? mapTemp?.fullNama:"-",
                    staGatePas : gatePasTemp?.gatePassId ? gatePasTemp?.gatePassId : -1,
                    custSurvey : custSurvey ? custSurvey?.t104Text : "-",
                    idReception : gatePasTemp ? gatePasTemp?.reception?.id : -1
            ]
        }
        else {
            def hari = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            def waktuSekarang = df.format(new Date())
            String awal = waktuSekarang+" 00:00"
            String akhir = waktuSekarang+" 24:00"
            Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
            Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)
            Date hariIni = datatablesUtilService?.syncTime()
            def  staBooking = "Walk In"
            def tglJam = hari[hariIni?.getDay()]+" "+hariIni.format("dd MMMM yyyy | HH:mm");
            def no=0
            def findcustomerin =CustomerIn.findAllByDateCreatedBetweenAndStaDel(dateAwal,dateAkhir,"0")

            hasil << [
                    idCV : "",
                    mobil : "-",
                    tahun : "-",
                    warna : "-",
                    namaCustomer : "-",
                    staGatePas : "-",
            ]
        }
        render hasil as JSON
    }
}