package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class ViewMaterialPerWoController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def viewMaterialPerWoService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }


    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [noWo: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsublist() {

        [noMos: params.t417NoMOS, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        render viewMaterialPerWoService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        render viewMaterialPerWoService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {

        render viewMaterialPerWoService.datatablesSubSubList(params) as JSON
    }

    def delete() {
        def result = viewMaterialPerWoService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Claim", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def hasil= viewMaterialPerWoService.massDelete(params)
        render "ok"
    }

    def printMos(){
        def jsonArray = JSON.parse(params.idMos)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'mos.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )

            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("mos_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();

        def results = MOS.findAllByReception(Reception.findByT401NoWO(id))
        results.sort {
            it.goods.m111ID
        }

        int count = 0

        results.each {
            def nopol = it.reception.historyCustomerVehicle.kodeKotaNoPol.m116ID+" "+it.reception.historyCustomerVehicle.t183NoPolTengah + " " + it.reception.historyCustomerVehicle.t183NoPolBelakang
            def data = [:]

            count = count + 1

            data.put("noMos",it.t417NoMOS)
            data.put("tanggalMos", it.t417TglMOS.format("dd MMMM yyyy"))
            data.put("noPol", nopol)
            data.put("model",  it.reception.historyCustomerVehicle.fullModelCode.baseModel.m102NamaBaseModel)
            data.put("noWo",  it.reception.t401NoWO)
            data.put("SA",  it.reception.t401NamaSA)
            data.put("teknisi",  it.t417xNamaUser)

            data.put("nomor", count as String)
            data.put("namaMaterial",it.goods?.m111Nama)
            data.put("qty",it.t417Qty1)
            data.put("satuan",it.goods.satuan.m118Satuan1)

            reportData.add(data)
        }

        return reportData

    }
}
