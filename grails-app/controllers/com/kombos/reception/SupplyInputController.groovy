package com.kombos.reception

import com.kombos.administrasi.WarnaVendor
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Claim
import com.kombos.parts.Request
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class SupplyInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def addModal(Integer max) {
        [idTable: datatablesUtilService.syncTime().format("yyyyMMddhhmmss")]
    }
    def list(Integer max) {
        String staUbah = "create"
        def posJob = null
        if(params.noPos){
            posJob = PosJob.findByPos(POS.findByT418NoPOS(params.noPos))
            staUbah = "ubah"
        }else{
            posJob = PosJob.findByPos(POS.findByT418NoPOS(params.id))
        }
        [noSupply : "80-" + datatablesUtilService.syncTime().format("yyMMddhhmmss"),posJob:posJob, aksi : staUbah]
    }

    def req(){
        DateFormat df = new SimpleDateFormat("yyyy-M-d")
        def poss =  POS.findByT418NoPOS(params.nopos)
        def reception =  Reception.findByT401NoWO(params.noWo)
        def supply = null
        if(poss){
            def jsonArray = JSON.parse(params.ids)
            def posId = POS.findByT418NoPOSAndReception(params.nopos,reception)
            def pos = POS.get(posId.id)
            pos?.t418NoSupply = params.noSupply
            pos?.t418TglSupply = params.tanggal
            pos?.t418KetSupply = params.ket
            pos?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            pos?.lastUpdProcess = "UPDATE"
            pos?.lastUpdated = datatablesUtilService?.syncTime()
            pos?.save(flush: true)

            jsonArray.each {
                def warnaVendor = WarnaVendor.get(it)
                if(warnaVendor){
                    supply = new SupplyWarna()
                    supply?.reception = reception
                    supply?.pos = pos
                    supply?.vendorcat = poss?.vendorCat
                    supply?.warna = warnaVendor
                    supply?.t420Qty = params."qty-${it}" as Double
                    supply?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    supply?.lastUpdProcess = "INSERT"
                    supply?.dateCreated = datatablesUtilService?.syncTime()
                    supply?.lastUpdated = datatablesUtilService?.syncTime()
                    supply?.save(flush: true)
                }
            }

        }else {
            render "not"
        }
        render "Ok"
    }

    def ubah(){
        DateFormat df = new SimpleDateFormat("yyyy-M-d")
        def poss =  POS.findByT418NoPOS(params.nopos)
        def reception =  Reception.findByT401NoWO(params.noWo)
        if(poss){
            def jsonArray = JSON.parse(params.ids)
            def posId = POS.findByT418NoPOSAndReception(params.nopos,reception)
            def pos = POS.get(posId.id)
            pos?.t418NoSupply = params.noSupply
            pos?.t418TglSupply = params.tanggal
            pos?.t418KetSupply = params.ket
            pos?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            pos?.lastUpdProcess = "UPDATE"
            pos?.lastUpdated = datatablesUtilService?.syncTime()
            pos?.save(flush: true)
            def hapus = SupplyWarna.findAllByPosAndReception(pos,reception)
            hapus.each{
                it.delete()
            }

            jsonArray.each {
                def warnaVendor = WarnaVendor.get(it)
                if(warnaVendor){
                    def supply = new SupplyWarna()
                    supply?.reception = reception
                    supply?.pos = pos
                    supply?.vendorcat = poss?.vendorCat
                    supply?.warna = warnaVendor
                    supply?.t420Qty = params."qty-${it}" as Double
                    supply?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    supply?.lastUpdProcess = "INSERT"
                    supply?.dateCreated = datatablesUtilService?.syncTime()
                    supply?.lastUpdated = datatablesUtilService?.syncTime()
                    supply?.save()
                }
            }

        }else {
            render "not"
        }
        render "Ok"
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Claim, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        try {
            datatablesUtilService.updateField(Request, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def kodeList() {
        def res = [:]

        def result = Reception.findAllWhere(staDel : '0',staSave: '0')
        def opts = []
        result.each {
            opts << it.t401NoWO
        }

        res."options" = opts
        render res as JSON
    }

    def getTableData(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def result = []
        def hasil = SupplyWarna.createCriteria().list() {
            pos{
                eq("t418NoPOS",params.nopos)
            }
        }
        hasil.each {
            result << [
                    id : it.warna.id,
                    kodeWarna : it.warna.m192IDWarna,
                    namaWarna : it.warna.m192NamaWarna,
                    qty : it.t420Qty
            ]
        }
        render result as JSON
    }

    def addModalDatatablesList(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params
        def c = WarnaVendor.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
            if(params.sCriteria_kode){
                ilike('m192IDWarna',"%"+ params.sCriteria_kode+ "%" as String)
            }
            if(params.sCriteria_nama){
                ilike('m192NamaWarna',"%"+ params.sCriteria_nama + "%" as String)
            }
            switch (sortProperty) {
                case "kodeWarna":

                    order("m192IDWarna",sortDir)

                    break;
                case "namaWarna":

                    order("m192IDWarna",sortDir)

                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def nos = 0
        results.each {
            nos = nos + 1
            rows << [

                    id: it?.id,

                    kodeWarna: it?.m192IDWarna,

                    namaWarna: it?.m192NamaWarna,

                    qty : '0',

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

}
