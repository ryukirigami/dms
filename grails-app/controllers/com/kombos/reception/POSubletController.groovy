package com.kombos.reception

import com.kombos.administrasi.HargaJobSublet
import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.NamaApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.ApprovalT770
import com.kombos.parts.Vendor
import com.kombos.woinformation.JobRCP
import grails.converters.JSON

class POSubletController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def POSubletService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render POSubletService.datatablesList(params) as JSON
    }

    def create() {
        def result = POSubletService.create(params)

        if (!result.error)
            return [POSubletInstance: result.POSubletInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.createdBy  = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = POSubletService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["POSublet", result.POSubletInstance.id])
            redirect(action: 'show', id: result.POSubletInstance.id)
            return
        }

        render(view: 'create', model: [POSubletInstance: result.POSubletInstance])
    }

    def show(Long id) {
        def result = POSubletService.show(params)

        if (!result.error)
            return [POSubletInstance: result.POSubletInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = POSubletService.show(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error)
            return [POSubletInstance: result.POSubletInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = POSubletService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["POSublet", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [POSubletInstance: result.POSubletInstance.attach()])
    }

    def delete() {
        def result = POSubletService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["POSublet", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(POSublet, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


    def sublist() {
        [  idTable: new Date().format("yyyyMMddhhmmss")+params.id, idReception: params.id]
    }

    def datatablesSubList(){
      //  println "Ok"
        render POSubletService.datatablesSubList(params) as JSON
    }

    def inputPOSublet(){
     //   POSubletService.inputPOSublet(params) as JSON
        def jobRCP = JobRCP.get(params.idJobRCP)
        def nomorWO = jobRCP.reception?.t401NoWO
        def namaJob = jobRCP.operation?.m053NamaOperation
        def vendor = jobRCP.vendor
        def hargaBeli = HargaJobSublet.findByOperation(jobRCP.operation)?.t116Harga

        [idJobRCP : jobRCP.id, nomorWO : nomorWO, namaJob : namaJob, vendor : vendor, hargaBeli: hargaBeli]

    }

    def saveApproval(){
        def jobRCP = JobRCP.get(params.idJobRCP as Long)
        def vendor = Vendor.get(params.vendor as Long)
        def jumlahSublet = params.jumlahSublet
        def hargaBeli = params.hargaBeli
        def catatan = params.catatan

        jobRCP.vendor = vendor
        jobRCP.t402HargaBeliSublet = Double.valueOf(hargaBeli as String)
        jobRCP.t402JmlSublet = (jumlahSublet as String)
        jobRCP.t402KeteranganSublet = catatan
        jobRCP.t402HargaRp = Double.valueOf(jumlahSublet  as String)
        jobRCP.t402TotalRp = Double.valueOf(jumlahSublet  as String)
        jobRCP.t402StaApprovalSublet = "0"
        jobRCP.lastUpdated = datatablesUtilService?.syncTime()
        jobRCP.save(flush : true)


        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.PO_SUBLET)
        def approver = ""
        if(kegiatanApproval){

        }else{
            def kegiatanApprovalAdd = new KegiatanApproval()
            kegiatanApprovalAdd?.afterApprovalService = "POSubletService"
            kegiatanApprovalAdd?.createdBy= "SYSTEM"
            kegiatanApprovalAdd?.lastUpdProcess= "INSERT"
            kegiatanApprovalAdd?.m770IdApproval= ((KegiatanApproval.last().m770IdApproval as int)+ 1).toString()
            kegiatanApprovalAdd?.m770KegiatanApproval= KegiatanApproval.PO_SUBLET
            kegiatanApprovalAdd?.m770NamaDomain= "com.kombos.reception.reception"
            kegiatanApprovalAdd?.m770NamaFieldDiupdate= "T402_STADEL"
            kegiatanApprovalAdd?.m770NamaFk = "T402_STADEL"
            kegiatanApprovalAdd?.m770NamaFormDetail = ""
            kegiatanApprovalAdd?.m770NamaTabel = "T402_JOBRCP"
            kegiatanApprovalAdd?.staDel= "0"
            kegiatanApprovalAdd?.m770StaButuhNilai = "0"
            kegiatanApprovalAdd?.dateCreated = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.lastUpdated = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.save(flush: true)
            kegiatanApprovalAdd.errors.each{ println it }
        }
        def approval = new ApprovalT770(
                companyDealer: session?.userCompanyDealer,
                t770FK:jobRCP.id as String,
                kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.PO_SUBLET),
                t770NoDokumen: jobRCP?.reception?.t401NoWO,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: catatan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush:true)
        approval.errors.each{ println it }
        render "Succesful save"
    }

    def sendApproval(){
        def kegiatan = new KegiatanApproval(
                m770IdApproval: (KegiatanApproval.last().m770IdApproval.toInteger() + 1).toString(),
                m770KegiatanApproval: "Adjustment",
                m770NamaDomain: "partAdjustment",
                m770NamaTabel: "T145_PartsAdjust",
                m770NamaFk: "T145_ID",
                namaApprovals: NamaApproval.first(),
                m770NamaFieldDiupdate: "T145_StatusApprove",
                m770NamaFormDetail: "-",
                m770StaButuhNilai: 1,
                staDel: '0',
                afterApprovalService: 'Kegiatan AprroveService',
                createdBy: 'SYSTEM',
                lastUpdProcess: 'INSERT',
        ).save(flush: true)
    }
}
