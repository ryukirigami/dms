package com.kombos.reception

import grails.converters.JSON

import java.text.SimpleDateFormat

class BoardAntrianBPController {

    def customerInService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        String datetimeDesc = new SimpleDateFormat("EEEE, dd MMMM yyyy / HH:mm:ss").format(new Date());
        [datetimeDesc: datetimeDesc]
   	}

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render customerInService.boardAntrianBPList(params) as JSON
    }

}
