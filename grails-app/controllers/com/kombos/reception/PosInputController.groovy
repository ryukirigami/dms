package com.kombos.reception

import com.kombos.administrasi.Operation
import com.kombos.administrasi.VendorCat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.Warna
import com.kombos.parts.Claim
import com.kombos.parts.Request
import grails.converters.JSON
import org.activiti.engine.impl.util.json.JSONArray

class PosInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def addModal(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def list(Integer max) {
        String staUbah = "create"
        def rec = null
        def posJob = new PosJob()
        if(params.noPos){
            staUbah="ubah"
            def pos = POS.findByT418NoPOS(params.noPos)
            posJob = PosJob.findByPos(pos)
            rec = pos.reception
        }else{
            rec = Reception.findByT401NoWO(params.id)
        }
        def recc = Reception.createCriteria().list {
            eq("staDel","0");
            eq("staSave","0");
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","BP")
                }
            }
            order("t401NoWO","asc")
        }
        [noPos : "501-" + new Date().format("yyMMddhhmmss"),reception:rec,recc:recc,posJob:posJob, aksi:staUbah]
    }
    def cek(){
        def rec = Reception.findByT401NoWO(params.noWo)
        if(rec){
            def model = rec.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
            def warna = rec.historyCustomerVehicle?.warna?.m092NamaWarna
            def nopol = rec.historyCustomerVehicle?.kodeKotaNoPol?.m116ID +" "+rec.historyCustomerVehicle?.t183NoPolTengah + "" + rec.historyCustomerVehicle?.t183NoPolBelakang
            render model + " | " + warna +" | "+ nopol
        }else{
            render ""
        }
    }
    def addJob(){
        def hasil = Operation.createCriteria().list() {

                eq("id",params.idJob as Long)

        }
        def result = []
        hasil.each {
            result << [
                    id : it.id,
                    job : it.m053NamaOperation
            ]
        }
        render result as JSON
    }
    def allJob(){
        def hasil = Operation.createCriteria().list() {
        }
        def result = []

        hasil.each {
            result << [
                    id : it.id,
                    job : it.m053NamaOperation
            ]
        }
        render result as JSON
    }
    def req(){
        def tagih = ""
        if(params.tagih=="bengkel"){
            tagih = "1"
        }else{
            tagih = "0"
        }
        def reception =  Reception.findByT401NoWO(params.noWo)
        def posJob = null
        if(reception){
            def jsonArray = JSON.parse(params.ids)
            def pos = new POS()
            pos?.reception = reception
            pos?.t418NoPOS = params.noPos
            pos?.t418TglPOS = params.tanggal
            pos?.warna = Warna.findById(params.warna as Long)
            pos?.vendorCat = VendorCat.findById(params.vendorCat as Long)
            pos?.t418KetOrder = params.ket
            pos?.t418StaTagihKe = tagih
            pos?.t418NoSupply = ""
//            pos?.t418TglSupply = new Date()
            pos?.t418KetSupply = ''
            pos?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            pos?.lastUpdProcess = "INSERT"
            pos?.dateCreated = datatablesUtilService?.syncTime()
            pos?.lastUpdated = datatablesUtilService?.syncTime()
            pos?.save(flush: true)

            jsonArray.each {
                def op = Operation.get(it)
                if(op){
                    posJob = new PosJob()
                    posJob?.reception = reception
                    posJob?.pos = pos
                    posJob?.operation = op
                    posJob?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    posJob?.lastUpdProcess = "INSERT"
                    posJob?.dateCreated = datatablesUtilService?.syncTime()
                    posJob?.lastUpdated = datatablesUtilService?.syncTime()
                    posJob?.save(flush: true)
                }
            }

        }else {
            render "not"
        }
        render "Ok"
    }

    def ubah(){
        def tagih = ""
        if(params.tagih=="bengkel"){
            tagih = "1"
        }else{
            tagih = "0"
        }
        def reception =  Reception.findByT401NoWO(params.noWo)
        def posJob = null
        if(reception){
            def jsonArray = JSON.parse(params.ids)
            def pos = POS.get(params.noPosId as Long)
            pos?.t418TglPOS = params.tanggal
            pos?.warna = Warna.findById(params.warna as Long)
            pos?.vendorCat = VendorCat.findById(params.vendorCat as Long)
            pos?.t418KetOrder = params.ket
            pos?.t418StaTagihKe = tagih
            pos?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            pos?.lastUpdProcess = "INSERT"
            pos?.lastUpdated = datatablesUtilService?.syncTime()
            pos?.save(flush: true)
            jsonArray.each {
                def op = Operation.get(it)
                if(!PosJob.findByPosAndOperationAndReception(pos,op,reception)){
                    posJob = new PosJob()
                    posJob?.reception = reception
                    posJob?.pos = pos
                    posJob?.operation = op
                    posJob?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    posJob?.lastUpdProcess = "INSERT"
                    posJob?.dateCreated = datatablesUtilService?.syncTime()
                    posJob?.lastUpdated = datatablesUtilService?.syncTime()
                    posJob?.save(flush: true)
                }
            }

        }else {
            render "not"
        }
        render "Ok"
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Claim, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        try {
            datatablesUtilService.updateField(Request, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def kodeList() {
        def res = [:]

        def result = Reception.findAllWhere(staDel : '0',staSave: '0')
        def opts = []
        result.each {
            opts << it.t401NoWO
        }

        res."options" = opts
        render res as JSON
    }

    def getTableData(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def result = []
        def hasil = PosJob.createCriteria().list() {
            pos{
                eq("t418NoPOS",params.id)
            }
        }
        hasil.each {
            result << [
                    id : it.operation.id,
                    job : it.operation.m053NamaOperation,
            ]
        }
        render result as JSON
    }

}
