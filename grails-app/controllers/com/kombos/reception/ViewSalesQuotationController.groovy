package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON

class ViewSalesQuotationController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params
        def c = Reception.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0");
            eq("staSave","2");
            eq("t401StaReceptionEstimasiSalesQuotation","SQ")
            if(params.sCriteria_Tanggal && params.sCriteria_Tanggalakhir){
                ge("t401TglJamCetakWO", params."sCriteria_Tanggal")
                lt("t401TglJamCetakWO", params."sCriteria_Tanggalakhir" + 1)
            }
            if(params.sCriteria_kriteria=="s_customer"){
                String nopol = params.sCriteria_kunci
                if(params.sCriteria_kunci==""){

                }else{
                    def data = nopol.split("\\s+")
                    if(data.size()>=2){
                        historyCustomer{
                            eq("t182NamaDepan",data[0].trim())
                            eq("t182NamaBelakang",data[1].trim())
                        }
                    }else{
                        historyCustomer{
                            ilike("t182NamaDepan","%" + params.sCriteria_kunci + "%")
                        }
                    }
                }
            }

            if(params.sCriteria_kriteria=="s_alamat"){
                historyCustomer{
                     ilike("t182Alamat","%"+ params.sCriteria_kunci +"%")
                }
            }

            if(params.sCriteria_kriteria=="s_noHp"){
                historyCustomer{
                    ilike("t182NoHp","%"+ params.sCriteria_kunci +"%")
                }
            }

            if(params.sCriteria_kriteria=="s_kendaraan"){
                historyCustomerVehicle{
                    fullModelCode{
                        baseModel{
                            ilike("m102NamaBaseModel","%"+  params.sCriteria_kunci +"%")
                        }
                    }
                }
            }

            if(params.sCriteria_kriteria=="s_nopol"){
                String nopol = params.sCriteria_kunci
                if(params.sCriteria_kunci){
                    def data = nopol.split("\\s+")
                    if(data.size()==3){
                        historyCustomerVehicle{
                            kodeKotaNoPol{
                                eq("m116ID",data[0].trim())
                            }
                        }
                        historyCustomerVehicle{
                            eq("t183NoPolTengah",data[1].trim())
                        }
                        historyCustomerVehicle{
                            eq("t183NoPolBelakang",data[2].trim())
                        }
                    }else{
                        historyCustomerVehicle{
                            ilike("fullNoPol","%" +params.sCriteria_kunci+ "%")
                        }
                    }
                }

            }

            switch (sortProperty) {
                case "namaCustomer" :
                    historyCustomer{
                    order("t182NamaDepan",sortDir)}; break;
                case "alamat" :
                    historyCustomer{
                    order("t182Alamat",sortDir)};break;
                case "nomorHp" :
                    historyCustomer{
                    order("t182NoHp",sortDir)}; break;
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []
        def bm = null
        def konversi = new Konversi()
        results.each {

            rows << [

                    id: it.id,
                    namaCustomer: it?.historyCustomer?.fullNama,
                    alamat : it?.historyCustomer?.t182Alamat,
                    nomorHp : it?.historyCustomer?.t182NoHp,
                    kendaraan : it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                    noPol : it?.historyCustomerVehicle?.fullNoPol,
                    tglEstimasi : it?.t401TglJamCetakWO ? it?.t401TglJamCetakWO?.format("dd/MM/yyyy") : it?.dateCreated?.format("dd/MM/yyyy"),
                    totalEstimasi : it?.t401TotalRp ? konversi?.toRupiah(it?.t401TotalRp) : 0
            ]

        }

        ret = [sEcho: params.sEcho, iTotalRecords: rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }
}
