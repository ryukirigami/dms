package com.kombos.reception

import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import java.text.DateFormat
import java.text.SimpleDateFormat

class GatePassGraphicController {

    def index() {
        redirect(action: "GatePassGrahic", params: params)
    }
    def jasperService

def GatePassGrahic()
{
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    def waktuSekarang = df.format(new Date())
    Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
    Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
    def companyid=session?.userCompanyDealer;
    def findcustomerin =CustomerIn.findAllByDateCreatedBetweenAndStaDelAndCompanyDealer(dateAwal,dateAkhir,"0",companyid)
    //def findcustomerin =CustomerIn.findAllByDateCreatedBetweenAndStaDel(dateAwal,dateAkhir,"0")
    def jmlrcustomerin= findcustomerin.size()
    def c=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(1),companyid)
    def d=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(2),companyid)
    def e=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(3),companyid)
    def f=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(4),companyid)
    def satu=c.size()
    def dua=d.size()
    def tiga=e.size()
    def empat=f.size()

    if(jmlrcustomerin==0)
    {
        jmlrcustomerin=0
        satu=0
        tiga=0
        dua=0
        empat=0
    }
         else
    {
        def totalgp=satu+dua+tiga+empat
        def sisa= jmlrcustomerin-totalgp
        def presentasesatu=satu/jmlrcustomerin*100
        def presentasedua=dua/jmlrcustomerin*100
        def presentasisisa=(jmlrcustomerin-sisa)/jmlrcustomerin*100
        def panjanggrafiksatu
        def panjanggrafikdua
        def panjanggrafiksisa

    if(presentasisisa==0)
      panjanggrafiksisa=5
       else
       panjanggrafiksisa=(5+presentasisisa)*30/100

   if (presentasesatu == 0)
       panjanggrafiksatu =5;

    else
       panjanggrafiksatu = presentasesatu * 30/ 100;
   if (presentasedua ==0)
       panjanggrafikdua = 5;
    else
     panjanggrafikdua = presentasedua *30/ 100;

    [satu:satu,
     dua:dua,
     tiga:tiga,
     empat:empat,
     gp:presentasesatu,
     tes:presentasedua,
     grafik1:panjanggrafiksatu,
     grafik2:panjanggrafikdua,
     custsisa:sisa,
     totalgate:totalgp,
     psisa:panjanggrafiksisa,
     jmlcustomerin:jmlrcustomerin]
}

}
    def preview(){
        List<JasperReportDef> reportDefList = []
        def Reportcustomerin= Reportcustomerin(params)

        def reportDef2 = new JasperReportDef(name:'GatePass.jasper',
        fileFormat:JasperExportFormat.PDF_FORMAT, reportData: Reportcustomerin)
        reportDefList.add(reportDef2)
        def file = File.createTempFile("GatePass_",".pdf")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
    def previewdetail(){

        List<JasperReportDef> reportDefList = []
        def Reportcustomerin= Reportcustomerin(params)
        def reportgatepassdetail = reportgatepassdetail(params)

                  def reportDef2 = new JasperReportDef(name:'GatePass.jasper',
                  fileFormat:JasperExportFormat.PDF_FORMAT, reportData: Reportcustomerin)
                  reportDefList.add(reportDef2)

                def reportDef = new JasperReportDef(name:'GatePassDetail.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportgatepassdetail)
                reportDefList.add(reportDef)

        def file = File.createTempFile("GatePass_Detail_",".pdf")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def Reportcustomerin(def params){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")
        def companyid=session?.userCompanyDealer;
       def findcustomerin =CustomerIn.findAllByDateCreatedBetweenAndStaDelAndCompanyDealer(dateAwal,dateAkhir,"0",companyid)
//        def findcustomerin =CustomerIn.findAllByDateCreatedBetweenAndStaDel(dateAwal,dateAkhir,"0")
        def jmlrcustomerin=findcustomerin.size()
        def c=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(1),companyid)
        def d=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(2),companyid)
        def e=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(3),companyid)
        def f=GatePassT800.findAllByDateCreatedBetweenAndGatePassAndCompanyDealer(dateAwal,dateAkhir,GatePass.get(4),companyid)
        def satu=c.size()
        def dua=d.size()
        def tiga=e.size()
        def empat=f.size()
        def totalgp=satu+dua+tiga
        def sisa= jmlrcustomerin-totalgp
        def reportData = new ArrayList();


        def no=1;
       if (jmlrcustomerin>0){
        findcustomerin.each {
            def data = [:]
            data.put("JmlCustomerIn", jmlrcustomerin)
            data.put("KendaraanProsesService", sisa)
            data.put("KendaraanTestJalan", dua)
            data.put("KendaraanRawatJalan", tiga)
            data.put("sublet", empat)
            data.put("GatePass", satu)
            data.put("no",no)
            data.put("nopol",it?.customerVehicle?.getCurrentCondition()?.fullNoPol ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol  : it.t400noPol)
           // data.put("nopol",it.t400noPol)
            data.put("jammasuk",it.t400TglCetakNoAntrian?it.t400TglCetakNoAntrian.format('HH:mm'):"")
            data.put("jamkeluar",it?.t400TglJamOut ? it.t400TglJamOut.format("HH:mm") :"-")
            data.put("status","Customer In")
            no++
            reportData.add(data)
        }
           return reportData
       }
            else{
           def data = [:]
            data.put("JmlCustomerIn","-")
            data.put("KendaraanProsesService", "-")
            data.put("KendaraanTestJalan", "-")
            data.put("KendaraanRawatJalan", "-")
            data.put("GatePass", "-")
            data.put("sublet", "-")
            data.put("no","-")
            data.put("nopol","-")
            data.put("jammasuk","-")
            data.put("jamkeluar","-")
            data.put("status","-")
            reportData.add(data)
        }
        return reportData
    }

    def reportgatepassdetail(def params){

        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)

         def c=GatePassT800.createCriteria().list(){
             eq("companyDealer",session?.userCompanyDealer)
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
        }
        def ctotal=c.size();
        def nourut=1
        def reportData = new ArrayList();

        if(ctotal>0)
        {
        c.each {
            def data = [:]
            data.put("no",nourut)
            data.put("nopol",it.reception.historyCustomerVehicle.fullNoPol)
            data.put("jammasuk",it.reception?.customerIn?it.reception.customerIn.dateCreated.format('HH:mm'):"")
            data.put("jamkeluar",it.t800TglJamGatePass?it.t800TglJamGatePass.format('HH:mm'):"")
            data.put("status",it.gatePass?.m800NamaGatePass)
            reportData.add(data)
            nourut++
        }
        return reportData
        }
        else
        {
            def data = [:]
            data.put("no","-")
            data.put("nopol","-")
            data.put("jammasuk","-")
            data.put("jamkeluar","-")
            data.put("status","-")
            reportData.add(data)
        }
        return reportData
    }

    def datatablesList() {

        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)

        //def c=GatePassT800.findAllByDateCreatedBetweenAndGatePass(dateAwal,dateAkhir,GatePass.get(1))
        def ret
        def c =CustomerIn.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            eq("staDel","0")

            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
            order("t400TglCetakNoAntrian","asc")

        }
        def nourut=1
        def rows = []
        results.each {
            rows << [
                    id:nourut,
//                    nopol:it?.customerVehicle?.getCurrentCondition()?.fullNoPol ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol  : "",
                    nopol :it?.customerVehicle?.getCurrentCondition()?.fullNoPol ? it?.customerVehicle?.getCurrentCondition()?.fullNoPol  : it.t400noPol,
                    jammasuk:it.t400TglCetakNoAntrian?it.t400TglCetakNoAntrian.format('HH:mm'):"",
                    jamkeluar: it?.t400TglJamOut ? it?.t400TglJamOut.format("HH:mm") :"-"
           ]
            nourut++
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
     }

    def datatablesList2() {

        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)

      def ret
        def c=GatePassT800.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
          eq("gatePass",GatePass.get(1))
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
        }
        def nourut=1
        def rows = []
        results.each {
            def tes=GatePassT800.findByReceptionAndStaDel(it.reception,"0")
            rows << [
                    id:nourut,
                    nopol:it.reception.historyCustomerVehicle.fullNoPol,
                    jammasuk:it.reception?.customerIn?it.reception.customerIn.dateCreated.format('HH:mm'):"",
                    jamkeluar:tes?.t800TglJamGatePass?it.t800TglJamGatePass.format('HH:mm'):"",
            ]
            nourut++
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

    def datatablesList3() {


        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)

        //def c=GatePassT800.findAllByDateCreatedBetweenAndGatePass(dateAwal,dateAkhir,GatePass.get(1))
        def ret
        def c=GatePassT800.createCriteria()
//       def c = CustomerIn.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
           eq("gatePass",GatePass.get(2))
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)


        }
        def nourut=1
        def rows = []
        results.each {
            def tes=GatePassT800.findByReceptionAndStaDel(it.reception,"0")
            //ambil yg paling akhir

            rows << [
                    id:nourut,
                    nopol:it.reception.historyCustomerVehicle.fullNoPol,
                    jammasuk:it.reception?.customerIn?it.reception.customerIn.dateCreated.format('HH:mm'):"",
                    jamkeluar:tes?.t800TglJamGatePass?it.t800TglJamGatePass.format('HH:mm'):"",
//                    T800Gatepass:(it.t800TglJamGatePass==null)?"":hhmm.format(it.t800TglJamGatePass)

            ]
            nourut++
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }
    //rawat jalan
    def datatablesList4() {


        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)


        def ret
        def c=GatePassT800.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            eq("gatePass",GatePass.get(3))
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
        }
        def nourut=1
        def rows = []
        results.each {
            def tes=GatePassT800.findByReceptionAndStaDel(it.reception,"0")
            rows << [
                    id:nourut,
                    nopol:it.reception.historyCustomerVehicle.fullNoPol,
                    jammasuk:it.reception?.customerIn?it.reception.customerIn.dateCreated.format('HH:mm'):"",
                    jamkeluar:tes?.t800TglJamGatePass?it.t800TglJamGatePass.format('HH:mm'):"",
                    ]
            nourut++
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }


    def TableSublet(){
        DateFormat hhmm = new SimpleDateFormat("HH:mm")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        String awal = waktuSekarang+" 00:00"
        String akhir = waktuSekarang+" 24:00"
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",awal)
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir)


        def ret
        def c=GatePassT800.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            eq("gatePass",GatePass.get(4))
            eq("staDel","0")
            ge("dateCreated",dateAwal)
            le("dateCreated" ,dateAkhir)
        }
        def nourut=1
        def rows = []  
        results.each {
            def tes=GatePassT800.findByReceptionAndStaDel(it.reception,"0")
            rows << [
                    id:nourut,
                    nopol:it.reception.historyCustomerVehicle.fullNoPol,
                    jammasuk:it.reception?.customerIn?it.reception.customerIn.dateCreated.format('HH:mm'):"",
                    jamkeluar:tes?.t800TglJamGatePass?it.t800TglJamGatePass.format('HH:mm'):"",
            ]
            nourut++
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON

    }
}
