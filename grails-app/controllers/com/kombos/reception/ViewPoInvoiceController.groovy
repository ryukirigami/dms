package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class ViewPoInvoiceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def viewPoInvoiceService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        render viewPoInvoiceService.datatablesList(params) as JSON
    }

    def sublist(){
        [idPo: params.idPo,idTable:new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubList() {
        render viewPoInvoiceService.datatablesSubList(params) as JSON
    }

}
