package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Vendor
import com.kombos.woinformation.JobRCP
import grails.converters.JSON

class InputInvoiceReceptionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def invoiceSublet = new InvoiceSublet()
        def vendor = new Vendor()
        String status = 'create'
        if(params.idUbah){
            invoiceSublet = InvoiceSublet.findByT413NoInv(params.idUbah)
            def jobRCP = JobRCP.findByReceptionAndOperationAndStaDel(invoiceSublet.reception, invoiceSublet.operation, '0')
            if(jobRCP){
                vendor = jobRCP.vendor
            }
            status = 'update'
        }
        [idTable: new Date().format("yyyyMMddhhmmss") , invoiceInstance: invoiceSublet, vendor: vendor, status : status]
    }

    def datatablesModalList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params
        def c = JobRCP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            reception{
                if(params.nomorWO){
                    ilike("t401NoWO","%"+params.nomorWO+"%");
                }
                if(params.nomorPolisi){
                    historyCustomerVehicle{
                        String nopol = params.nomorPolisi.replace(" ","")
                        String awal="",tengah="",akhir=""
                        def angka = false
                        for(int a=0;a<nopol.length();a++){
                            if(nopol.charAt(a).toString().isNumber()){
                                angka = true
                                tengah+=nopol.charAt(a).toString()
                            }else{
                                if(angka){
                                    akhir+=nopol.charAt(a).toString()
                                }else{
                                    awal+=nopol.charAt(a).toString()
                                }
                            }
                        }
                        ilike("fullNoPol","%"+awal+tengah+akhir+"%")
                    }
                }
                order("t401NoWO")
            }
            vendor{
                eq("id",params.vendorId.toLong())
            }
            if(params.kodeJob){
                operation{
                    ilike("m053Id","%"+params.kodeJob+"%")
                }
            }
            if(params.namaJob){
                operation{
                    ilike("m053NamaOperation","%"+params.namaJob+"%")
                }
            }
            if(exist.size()>0){
                exist.each {
                    ne("id", it as long)
                }
            }
        }

        def rows = []
        results.each {
            rows << [
                    id: it?.id,
                    idJob : it?.operation?.id,
                    noWO_modal: it?.reception?.t401NoWO,
                    noPolisi_modal: it?.reception?.historyCustomerVehicle ? it?.reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID+" "+it?.reception?.historyCustomerVehicle?.t183NoPolTengah
                            +" "+it?.reception?.historyCustomerVehicle?.t183NoPolBelakang: "-",
                    kodeJob_modal: it?.operation?.m053JobsId,
                    namaJob_modal: it?.operation?.m053NamaOperation
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [invoiceInstance: new InvoiceSublet(params)]
    }

   def save() {
       params?.dateCreated = datatablesUtilService?.syncTime()
       params?.lastUpdated = datatablesUtilService?.syncTime()
       def html = "ada"
       def jsonArray = JSON.parse(params.idsReception)
       def jsonArray2 = JSON.parse(params.idsJob)
       int a=0;
       def find = InvoiceSublet.createCriteria().list {
           eq("staDel","0")
           eq("t413NoInv",params.t413NoInv.trim(),[ignoreCase : true])
       }
       def cari = []
       if(find){
           cari = JobRCP.createCriteria().list {
               eq("staDel","0")
               eq("reception",find?.first()?.reception)
               eq("operation",find?.first()?.operation)
               vendor{
                   eq("id",params.vendorId.toLong())
               }
           }
       }

       if(find.size()==0 || cari.size()==0){
           html="belum"
           jsonArray.each {
               def jobRCP = JobRCP.get(it.toString().toLong())
               params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
               params.lastUpdProcess = 'INSERT'
               params.dateCreated = datatablesUtilService?.syncTime()
               params.lastUpdated = datatablesUtilService?.syncTime()
               params.companyDealer = session?.userCompanyDealer
               params.staDel = '0'
               params.reception = jobRCP?.reception
               params.operation = jobRCP?.operation
               params.poSublet = POSublet.findByReceptionAndOperationAndStaDel(params.reception,params.operation , '0')
               params.t413JmlSublet = Double.parseDouble(params."harga${it}" as String)
               params.t413JmlBayar = 0
               def invSublet = new InvoiceSublet(params)
               invSublet.save(flush: true)
               invSublet.errors.each {println it}

               def jobInv = new JobInv()
               jobInv.invoiceSublet = invSublet
               jobInv.reception = params.reception
               jobInv.operation = params.operation
               jobInv.dateCreated = datatablesUtilService?.syncTime()
               jobInv.lastUpdated = datatablesUtilService?.syncTime()
               jobInv.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
               jobInv.lastUpdProcess = 'INSERT'
               jobInv.companyDealer = session?.userCompanyDealer
               jobInv.staDel = '0'
               jobInv.t702HargaRp = params."harga${it}" as Double
               jobInv.save(flush: true)
               jobInv.errors.each {println it}
               a++;
           }
       }
       render html
   }

   def update(){
       def jsonArray = JSON.parse(params.idsReception)
       def jsonArray2 = JSON.parse(params.idsJob)
       int a=0;
       jsonArray.each {
           def jobRCP = JobRCP.get(it.toString().toLong())
           params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
           params.lastUpdProcess = 'INSERT'
           params.staDel = '0'
           params.dateCreated = datatablesUtilService?.syncTime()
           params.lastUpdated = datatablesUtilService?.syncTime()
           params.reception = jobRCP?.reception
           params.operation = jobRCP?.operation
           params.poSublet = POSublet.findByReceptionAndStaDel(params.reception , '0')
           params.t413JmlSublet = 0
           def cari = InvoiceSublet.findByT413NoInvAndReceptionAndOperationAndStaDel(params.idsInvoice,params.reception,params.operation,'0')
           def invTemp = new InvoiceSublet()
           if(cari){
               def inv = cari
                   inv.t413TanggalInv =params.t413TanggalInv
                   inv.t413NoInv = params.t413NoInv
                   inv.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                   inv.lastUpdProcess = 'UPDATE'
                   inv?.lastUpdated = datatablesUtilService?.syncTime()
                   inv.save(flush: true)
                   invTemp = inv

               def jobIn = JobInv.findByReceptionAndOperationAndStaDelAndInvoiceSublet(params.reception, params.operation,'0',cari)
               if(jobIn){
                   jobIn.invoiceSublet = inv
                   jobIn.lastUpdProcess = 'UPDATE'
                   jobIn?.lastUpdated = datatablesUtilService?.syncTime()
                   jobIn.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                   jobIn.t702HargaRp = params."harga${it}" as Double
                   jobIn.save(flush: true)

               }else{
                   def jobInv = new JobInv()
                   jobInv.invoiceSublet = invTemp
                   jobInv.reception = params.reception
                   jobInv.operation = params.operation
                   jobInv.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                   jobInv.lastUpdProcess = 'INSERT'
                   jobInv?.dateCreated = datatablesUtilService?.syncTime()
                   jobInv?.lastUpdated = datatablesUtilService?.syncTime()
                   jobInv.staDel = '0'
                   jobInv.t702HargaRp = params."harga${it}" as Double
                   jobInv.save(flush: true)
                   jobInv.errors.each {println it}
               }
           }
           else{
               def invSublet = new InvoiceSublet(params)
               invSublet.save(flush: true)
               invTemp = invSublet

               def jobInv = new JobInv()
               jobInv.invoiceSublet = invTemp
               jobInv.reception = params.reception
               jobInv.operation = params.operation
               jobInv.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
               jobInv.lastUpdProcess = 'INSERT'
               jobInv.dateCreated = datatablesUtilService?.syncTime()
               jobInv.lastUpdated = datatablesUtilService?.syncTime()
               jobInv.staDel = '0'
               jobInv.t702HargaRp = params."harga${it}" as Double
               jobInv.save(flush: true)
               jobInv.errors.each {println it}
           }
           a++;
       }
       render "ok"
   }

    def getTableData(){
        def c = JobInv.createCriteria()
        def results = c.list {
            eq("staDel","0")
            invoiceSublet{
                eq('t413NoInv',params.noInvo)
            }
            reception{
                order("t401NoWO")
            }
        }

        def rows = []
        results.each {
            def cari = JobRCP.findByReceptionAndOperationAndStaDel(it.reception,it.operation,"0")
            rows << [
                    id: cari?.id,
                    noWO: it.reception.t401NoWO,
                    noPolisi : it.reception.historyCustomerVehicle.fullNoPol,
                    kodeJob : it.operation.m053JobsId,
                    namaJob : it.operation.m053NamaOperation,
                    harga : it.t702HargaRp
            ]
        }

        render rows as JSON
    }

}
