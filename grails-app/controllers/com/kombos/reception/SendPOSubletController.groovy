package com.kombos.reception

import com.kombos.administrasi.GeneralParameter
import com.kombos.administrasi.GroupManPower
import com.kombos.administrasi.ManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.hrd.Karyawan
import com.kombos.parts.GroovyEmailer
import com.kombos.parts.Konversi
import com.kombos.parts.PO
import com.kombos.parts.PartTipeOrder
import com.kombos.woinformation.ActualRateTeknisi
import com.kombos.woinformation.JobRCP
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class SendPOSubletController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def sendPOSubletService

    def jasperService

    def mailService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: datatablesUtilService?.syncTime().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        //  session.exportParams=params
        println "data table"

        render sendPOSubletService.datatablesList(params) as JSON
    }


    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
//        Date tglId = Date.parse(dateFormat,params.tanggalRequest)

        [ idTable: datatablesUtilService?.syncTime().format("yyyyMMddhhmmss"), nomorPO: params.noPO, namaVendor : params.vendor]
    }
    def subsublist() {
        println "kode goods : "+params.kodePart
        println "no PO : "+params.nomorPO
        [goods: params.kodePart, idTable: datatablesUtilService?.syncTime().format("yyyyMMddhhmmss"), nomorPO: params.nomorPO]
    }

    def datatablesSubList() {
        render sendPOSubletService.datatablesSubList(params) as JSON
    }


    def create() {
        def result = SendPOSubletService.create(params)

        if(!result.error)
            return [POInstance: result.POInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = SendPOSubletService.save(params)
        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PO", result.POInstance.id])
            redirect(action:'show', id: result.POInstance.id)
            return
        }

        render(view:'create', model:[POInstance: result.POInstance])
    }

    def show(Long id) {
        def result = SendPOSubletService.show(params)

        if(!result.error)
            return [ POInstance: result.POInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = SendPOSubletService.show(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if(!result.error)
            return [ POInstance: result.POInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = SendPOSubletService.update(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PO", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[POInstance: result.POInstance.attach()])
    }

    def delete() {
        def result = SendPOSubletService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PO", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PO, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }



    def sendPOSubletSPLD(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def idPO = params.idPO
        PO po = PO.findByT164NoPO(idPO)
        def reportData = calculateReportData(po)
        def reportDef = new JasperReportDef(name:'sendPOSublet.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        def file = File.createTempFile("po_",".pdf")
        //    file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        byte[] bytes = jasperService.generateReport(reportDef).toByteArray()

        def emailTujuan = po.vendor.m121eMail;
        def subject = "PO : "+po.t164NoPO


        def companyDealerId =  session.userCompanyDealer.id
        def generalParameter = GeneralParameter.findById(companyDealerId)
        def username = generalParameter.m000UserName
        def password = generalParameter.m000Password
        def port = generalParameter.m000Port
        def messageBody = generalParameter.m000FormatEmail
        def attachment = file

        GroovyEmailer emailer = new GroovyEmailer(username,password,"smtp.gmail.com")
        emailer.sendMail(emailTujuan,subject,messageBody,file)

        po.staKirimPrint = "1"
        po.lastUpdated = datatablesUtilService?.syncTime()
        po.save(flush:true)

        render "ok"

    }

    def exportPO(){
        println "MASUK EXPORT GAN"
        println params.format
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def jsonArray = JSON.parse(params.idPOs)
        def poList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            println it
            poList << POSublet.findByT412NoPOSubletAndStaDel(it,"0")
        }

        poList.each {
            def reportData = calculateReportData(it?.t412NoPOSublet)
            def reportDef = new JasperReportDef(name:'sendPOSublet.jasper',
                    fileFormat:params.format=='pdf'?JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT,
                    reportData: reportData
            )

            reportDefList.add(reportDef)

        }
        def buntut = ".pdf"
        if(params.format=='xls'){
            buntut = ".xls"
        }
        def file = File.createTempFile("SEND_PO_SUBLET_",buntut)

        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="xls"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }

    def calculateReportData(def po){
        def kabeng = ""
        try {
            kabeng = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KEPALA BENGKEL%"),'0',session.userCompanyDealer)?.nama
        }catch (Exception e){

        }
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def results = POSublet.createCriteria().list {
            eq("staDel","0")
            eq("t412NoPOSublet", po)
        }
        int count = 0
        def total = 0
        if(results.size()>0){
            String tek = ""
            results.each { poSubletTemp ->

                def job = JobRCP?.findByReceptionAndOperationAndStaDel(poSubletTemp?.reception, poSubletTemp?.operation,'0')
                def data = [:]
                def nomorPolisi = job.reception.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " +job.reception.historyCustomerVehicle?.t183NoPolTengah+ " "+job.reception.historyCustomerVehicle?.t183NoPolBelakang
                count = count + 1
                def jpb = JPB.findAllByReceptionAndStaDel(poSubletTemp?.reception, "0")*.namaManPower?.t015NamaBoard?.unique()
                def cFr = JPB.findByReceptionAndStaDel(poSubletTemp?.reception, "0")
                def fr = GroupManPower.findById(cFr?.namaManPower?.groupManPower?.id)
                total += job.t402HargaBeliSublet
                jpb.each {
                    tek = jpb?.toString()?.substring(1,jpb?.toString()?.length() - 1).toUpperCase()
                }
                data.put("count", count)
                data.put("noWo", poSubletTemp?.reception?.t401NoWO)
                data.put("nama" , poSubletTemp?.reception?.historyCustomer?.t182NamaDepan)
                data.put("model" , poSubletTemp?.reception.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel)
                data.put("warna", poSubletTemp?.reception?.historyCustomerVehicle?.warna?.m092NamaWarna?.toUpperCase())
                data.put("nopol", nomorPolisi)
                data.put("sa", User.findByUsername(poSubletTemp?.reception.t401NamaSA).t001NamaPegawai.toUpperCase())
                data.put("teknisi",tek)
                data.put("foreman",  fr?.namaManPowerForeman?.t015NamaLengkap ? fr?.namaManPowerForeman?.t015NamaLengkap.toUpperCase() : "-")
                data.put("namaVendor" , job?.vendor?.m121Nama.toUpperCase())
                data.put("alamatVendor" , job?.vendor?.m121Alamat.toUpperCase())
                data.put("namaJob", poSubletTemp?.operation?.m053NamaOperation.toUpperCase())
                data.put("harga",new Konversi().toRupiah(job.t402HargaBeliSublet))
                data.put("total",new Konversi().toRupiah(total))
                data.put("catatan",job?.t402KeteranganSublet)
                data.put("kabeng",kabeng)
                data.put("companyDealer",poSubletTemp.companyDealer.m011NamaWorkshop)
                data.put("kota",poSubletTemp.companyDealer.kabKota.m002NamaKabKota + ", " + new Date().format('dd MMMM yyyy'))
                reportData.add(data)
            }
        }else{
            println "teu aya kang"

            def data = [:]
            data.put("noWo", "")
            reportData.add(data)
        }

        return reportData

    }

}
