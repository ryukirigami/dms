package com.kombos.reception

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.parts.Invoice
import com.kombos.parts.Vendor
import com.kombos.woinformation.JobRCP
import grails.converters.JSON

class ViewInvoiceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def invoiceService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = InvoiceSublet.createCriteria()
        def results = c.list {
            if (params."tanggalStart" && params."tanggalEnd") {
                ge("t413TanggalInv", params."tanggalStart")
                lt("t413TanggalInv", params."tanggalEnd" + 1)
            }
            eq("staDel","0")
            order("t413NoInv")
        }
        def rows = []
        def cekVendor = []
        results.each {
            def jobRCP = JobRCP.findByOperationAndReceptionAndStaDel(it.poSublet.operation, it.poSublet.reception, "0")
            if(jobRCP){
                String kriteria = jobRCP.vendor.id+";"+it.t413NoInv+";"+it.t413TanggalInv.format('dd/MM/yyyy')
                if(!cekVendor.contains(kriteria)){
                    rows << [
                        id: it.id,
                        idVendor: jobRCP?.vendor?.id,
                        vendor: jobRCP?.vendor?.m121Nama,
                        noInvoice : it.t413NoInv,
                        tglInvoice : it.t413TanggalInv
                    ]
                    cekVendor << jobRCP.vendor.id+";"+it.t413NoInv+";"+it.t413TanggalInv.format('dd/MM/yyyy')
                }
            }
        }

        def hasil = []
        int mulai = params.iDisplayStart.toLong()
        for(int a = mulai;a< (mulai + 10 <= rows.size() ? mulai+10 : rows.size()) ;a++){
            hasil << rows[a]
        }


        ret = [sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: hasil]

        render ret as JSON
    }

    def datatablesSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params
        def c = JobInv.createCriteria()
        def results = c.list {
            eq("staDel","0")
            invoiceSublet{
                eq('t413NoInv',params.noInvoice,[ignoreCase : true])
            }
            reception{
                order("t401NoWO")
            }
        }
        def rows = []
        results.each {
            rows << [
                    idInv: params.idInv,
                    id: it?.id,
                    noWO: it?.reception?.t401NoWO,
                    noPolisi : it?.reception?.historyCustomerVehicle?.fullNoPol,
                    kodeJob : it?.operation?.m053JobsId,
                    namaJob : it?.operation?.m053NamaOperation,
                    harga : it?.t702HargaRp
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  rows.size(), iTotalDisplayRecords: rows.size(), aaData: rows]

        render ret as JSON
    }


    def massdelete() {
        def res = [:]
        try {

            log.info(params.idsInvoice)
            def jsonArray = JSON.parse(params.idsInvoice)

            jsonArray.each {
                String idInv = it.toString().substring(0,it.toString().indexOf(';'))
                String idVendor = it.toString().substring(it.toString().indexOf(';')+1,it.toString().length())
                def inv = InvoiceSublet.get(idInv.toLong())
                def invSubAll = InvoiceSublet.findAllByT413NoInvAndStaDel(inv.t413NoInv,'0');
                def vendor = Vendor.get(idVendor.toLong())
                invSubAll.each {
                    def cek = JobRCP.findByVendorAndOperationAndReceptionAndStaDel(vendor,it.operation,it.reception,'0')
                    if(cek){

                        def jobInv = JobInv.findByReceptionAndOperationAndStaDelAndInvoiceSublet(it.reception,it.operation, '0' , it)
                        jobInv.staDel = '1'
                        jobInv.lastUpdProcess = 'DELETE'
                        jobInv.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        jobInv.lastUpdated = datatablesUtilService?.syncTime()
                        jobInv.save(flush: true)

                        def invTemp = it
                        invTemp.staDel = '1'
                        invTemp.lastUpdProcess = 'DELETE'
                        invTemp.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        invTemp.lastUpdated = datatablesUtilService?.syncTime()
                        invTemp.save(flush: true)
                    }

                }
            }

            log.info(params.idsJobInv)
            def jsonArray2 = JSON.parse(params.idsJobInv)
            jsonArray2.each {
                def jobInv = JobInv.get(it.toLong())
                jobInv.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                jobInv.lastUpdProcess = 'DELETE'
                jobInv.staDel = '1'
                jobInv.lastUpdated = datatablesUtilService?.syncTime()
                jobInv.save(flush: true)

                def invSub = jobInv.invoiceSublet
                invSub.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                invSub.lastUpdProcess = 'DELETE'
                invSub.staDel = '1'
                invSub.lastUpdated = datatablesUtilService?.syncTime()
                invSub.save(flush: true)

            }

            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def addParts(){
        def res = [:]
        Invoice invoiceInstance

        try {
            User user = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
            invoiceInstance = invoiceService.addParts(user, params)
            res.message = "Add Parts Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }

//        redirect(action: "edit", id: invoiceInstance.id)
        def ret = [id:invoiceInstance.id]
        render ret as JSON
    }

    def sublist(){
        String staSelect = params?.isSelected ? params?.isSelected : "false"
        [noInvoice: params.noInvoice,idInv:params.id,idTable:new Date().format("yyyyMMddhhmmss"),isSelected : staSelect]
    }

}
