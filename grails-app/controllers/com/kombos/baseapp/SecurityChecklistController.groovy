package com.kombos.baseapp

import grails.converters.JSON

class SecurityChecklistController {

    def securityChecklistService

    def index() { }

    def getList = {
        def res = securityChecklistService.getList(params)
        def ret = [rows: res.rows, records: res.totalRecords, page: params.page, total: res.totalPage]
        render ret as JSON
    }

    def edit = {
        def res = [:]
        switch (params.oper) {
            case "add":
                try {
                    securityChecklistService.add(params)
                    res.message = "Add Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "edit":
                try {
                    securityChecklistService.edit(params)
                    res.message = "Edit Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
            case "del":
                try {
                    securityChecklistService.delete(params)
                    res.message = "Delete Success"
                } catch (e) {
                    log.error(e.message, e)
                    res.message = e.message ?: e.cause?.message
                }
                break
        }

        render res as JSON
    }

}
