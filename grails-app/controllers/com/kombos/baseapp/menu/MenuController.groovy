package com.kombos.baseapp.menu

import com.kombos.baseapp.sec.shiro.Role
import com.kombos.production.Masalah
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class MenuController {

    def menuService

	def dashboardService

    def index() {
        def rol = RoleMenu.createCriteria().list {}
        def men = Menu.createCriteria().list {}
        if(rol.size()==0){
            men.each{
                def rm = new RoleMenu()
                rm?.menu = it
                rm?.role = Role.findById("ROLE_ADMIN")
                rm?.save(flush: true)

                def ubhMenu = Menu.get(it.id as Long)
                ubhMenu?.stockOpnameEnable = "0"
                ubhMenu?.save(flush: true)
            }
        }
        render(view:"/index", model:[adminMenus: menuService.loadAdminMenu(), menus: menuService.loadMenu(), runningText: dashboardService.runningText()]);
    }

	def home() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")

        def results = Masalah.createCriteria().list {
            ge("dateCreated",dateAwal)
            le("dateCreated",dateAkhir)
            eq("t501StaSolved","0")
        }
        def count=results.size()
		[taskCount: count,
                infos: dashboardService.informasiPenting(),
                masalah:results
//                results:count
        ]
	}

    def masalah(){
        def c=Masalah.findAll()
          def count=c.size()

        def row=[]
          c.each {
              row <<[
                 id: it.id,
                  wo: it.jpb?.reception?.t401NoWO
              ]

             def ret = [aaData: row]

              render ret as JSON
          }
    }


	def loadMenu() {
		def menus = menuService.loadPermittedMenu()
		render menus as JSON
	}
	
	def infoTeknisi() {
		def columns = [
			[field: 'namaTeknisi', label: 'Nama Teknisi', width: '200px'],
			[field: 'inisial', label: 'Inisial', width: '200px'],
			[field: 'status', label: 'Status', width: '200px'],
			[field: 'kehadiran', label: 'Kehadiran', width: '200px']
			]
		render(view:"list",model:[id:'infoTeknisi', label: 'Info Teknisi', columns: columns])		
	}
	def todayWaitingForDelivery() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noWO', label: 'Nomor WO', width: '200px'],
			[field: 'jamProduction', label: 'Jam Production', width: '200px'],
			[field: 'jamJanjiPenyerahan', label: 'Jam Janji Penyerahan', width: '200px']
			]
		render(view:"list",model:[id:'todayWaitingForDelivery', label: 'Today Waiting For Delivery', columns: columns])		
	}
	def todayUnitEntry() {
        def companyDealer = session.userCompanyDealer
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noWO', label: 'Nomor WO', width: '200px'],
			[field: 'namaSA', label: 'Nama SA', width: '200px'],
			[field: 'jamProduction', label: 'Jam Production', width: '200px'],
			[field: 'jamJanjiPenyerahan', label: 'Jam Janji Penyerahan', width: '200px'],
			[field: 'tujuanKedatangan', label: 'Tipe', width: '200px']
			]
		render(view:"list",model:[id:'todayUnitEntry', label: 'Today Unit Entry', columns: columns, companyDealer: companyDealer])
	}

	def todayNonOntimeDelivery() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noWO', label: 'Nomor WO', width: '200px'],
			[field: 'jamNotifikasi', label: 'Jam Notifikasi', width: '200px'],
			[field: 'janjiPenyerahan', label: 'Janji Penyerahan', width: '200px']
			]
		render(view:"list",model:[id:'todayNonOntimeDelivery', label: 'Today Non-Ontime Delivery', columns: columns])		
	}
	def todayAppointment() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noAppointment', label: 'Nomor Appointment', width: '200px'],
			[field: 'jamProduction', label: 'Jam Production', width: '200px'],
			[field: 'jamJanjiPenyerahan', label: 'Jam Janji Penyerahan', width: '200px']
			]
		render(view:"list",model:[id:'todayAppointment', label: 'Today Appointment', columns: columns])		
	}
	def todayUnitInvoice() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noWO', label: 'Nomor WO', width: '200px'],
			[field: 'jamProduction', label: 'Jam Production', width: '200px'],
			[field: 'jamJanjiPenyerahan', label: 'Jam Janji Penyerahan', width: '200px']
			]
		render(view:"list",model:[id:'todayUnitInvoice', label: 'Today Unit Invoice', columns: columns])		
	}
	def todayWalkIn() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noWO', label: 'Nomor WO', width: '200px'],
			[field: 'namaSA', label: 'Nama SA', width: '200px'],
			[field: 'jamProduction', label: 'Jam Production', width: '200px'],
			[field: 'jamJanjiPenyerahan', label: 'Jam Janji Penyerahan', width: '200px']
			]
		render(view:"list",model:[id:'todayWalkIn', label: 'Today Walk In', columns: columns])		
	}
	def todaySettlement() {
	
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noWO', label: 'Nomor WO', width: '200px'],
			[field: 'jamProduction', label: 'Jam Production', width: '200px'],
			[field: 'jamJanjiPenyerahan', label: 'Jam Janji Penyerahan', width: '200px']
			]
		render(view:"list",model:[id:'todaySettlement', label: 'Today Settlement', columns: columns])		
	}
	def todayNoShowAppointment() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noAppointment', label: 'Nomor Appointment', width: '200px'],
			[field: 'jamJanjiDatang', label: 'Tanggal Jam Janji Datang', width: '200px']
			]
		render(view:"list",model:[id:'todayNoShowAppointment', label: 'Today No Show Appointment', columns: columns])		
	}
	def tommorowAppointment() {
		def columns = [
			[field: 'noUrut', label: 'No Urut', width: '10px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '200px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '200px'],
			[field: 'noAppointment', label: 'No. Appointment', width: '200px'],
			[field: 'jamJanjiDatang', label: 'Jam Janji Datang', width: '200px']
			]
		render(view:"list",model:[id:'tommorowAppointment', label: 'Tommorow Appointment', columns: columns])		
	}
	
	def todayUnpaidInvoice() {
		def columns = [
			[field: 'no', label: 'No.', width: '10px'],
			[field: 'noInvoice', label: 'Nomor Invoice', width: '140px'],
			[field: 'tgglInv', label: 'Tgl Invoice', width: '100px'],
			[field: 'noWo', label: 'Nomor WO', width: '150px'],
			[field: 'tgglWo', label: 'Tgl WO', width: '100px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '100px'],
			[field: 'model', label: 'Model', width: '120px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '100px'],
			[field: 'sa', label: 'SA', width: '100px']
			]
		render(view:"list",model:[id:'todayUnpaidInvoice', label: 'Invoice Tunai Yang Belum Bayar', columns: columns])
	}

	def todayUnpaidOnRisk() {
		def columns = [
			[field: 'no', label: 'No.', width: '10px'],
			[field: 'noWo', label: 'Nomor WO', width: '180px'],
			[field: 'tanggal', label: 'Tanggal WO', width: '100px'],
			[field: 'noPolisi', label: 'Nomor Polisi', width: '100px'],
			[field: 'model', label: 'Model', width: '200px'],
			[field: 'namaCustomer', label: 'Nama Customer', width: '200px'],
			[field: 'noHp', label: 'Nomor HP', width: '130px']
			]
		render(view:"list",model:[id:'todayUnpaidOnRisk', label: 'WO(BP) Yang Belum Bayar On Risk', columns: columns])
	}

	def approvalAndAlertDatatablesList() {
		render dashboardService.approvalAndAlertDatatablesList(params) as JSON
	}
	def todayWaitingForDeliveryDatatablesList() {
		render dashboardService.todayWaitingForDeliveryDatatablesList(params) as JSON
	}
	def todayUnitEntryDatatablesList() {
		render dashboardService.todayUnitEntryDatatablesList(params) as JSON
	}
	def todayNonOntimeDeliveryDatatablesList() {
		render dashboardService.todayNonOntimeDeliveryDatatablesList(params) as JSON
	}
	def todayAppointmentDatatablesList() {
		render dashboardService.todayAppointmentDatatablesList(params) as JSON
	}
	def todayUnitInvoiceDatatablesList() {
		render dashboardService.todayUnitInvoiceDatatablesList(params) as JSON
	}
	def todayWalkInDatatablesList() {
		render dashboardService.todayWalkInDatatablesList(params) as JSON
	}
	def todaySettlementDatatablesList() {
		render dashboardService.todaySettlementDatatablesList(params) as JSON
	}
	def todayNoShowAppointmentDatatablesList() {
		render dashboardService.todayNoShowAppointmentDatatablesList(params) as JSON
	}
	def tommorowAppointmentDatatablesList() {
		render dashboardService.tommorowAppointmentDatatablesList(params) as JSON
	}	
	def infoTeknisiDatatablesList() {
		render dashboardService.infoTeknisiDatatablesList(params) as JSON
	}
	
	def todayUnpaidInvoiceDatatablesList() {
		render dashboardService.todayUnpaidInvoiceDatatablesList(params) as JSON
	}

	def todayUnpaidOnRiskDatatablesList() {
		render dashboardService.todayUnpaidOnRiskDatatablesList(params) as JSON
	}

	def todaySummary() {
		session.exportParams=params
		params.userCompanyDealer = session.userCompanyDealer
		render dashboardService.todaySummary(params) as JSON
	}
	
	/*def loadAdminMenu() {
		def menus = menuService.loadAdminMenu()
		def results = []
		def children
		menus?.each{ menu ->
			children = []
			def menuResult =
			[
				id:menu.id,
				details:menu.details,
				label:menu.label,
				linkAction:menu.linkAction,
				linkController:menu.linkController,
				menuCode:menu.menuCode,
				parent:menu.parent,
				divider:menu.divider,
				iconClass:menu.iconClass,
				seq:menu.seq,
				targetMode:menu.targetMode
			]
			menu?.children?.each { menuChild ->
				children << [
					details:menuChild.details,
					label:menuChild.label,
					linkAction:menuChild.linkAction,
					linkController:menuChild.linkController,
					menuCode:menuChild.menuCode,
					parent:menuChild.parent,
					divider:menu.divider,
					iconClass:menu.iconClass,
					seq:menuChild.seq,
					targetMode:menuChild.targetMode,
					children:[]
				]
			}
			menuResult.put('children', children)
			results << menuResult
		}
		render results as JSON
	}

    */

}
