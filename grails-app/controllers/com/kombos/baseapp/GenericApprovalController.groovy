package com.kombos.baseapp

import java.text.SimpleDateFormat

import org.apache.shiro.SecurityUtils

class GenericApprovalController {
	static activiti = true
	
	def activitiService
	def approvalService
	def taskService
	
	def formulaManagementService
	
//    def requestApproval(){
//		println("requestApproval params: " + params)
//		
//		def module = params.originController.toUpperCase() + '_' + params.originAction.toUpperCase()
//		def approval = approvalService.createNewApproval(module,params)
//		params.approvalId = approval.id
//		println ("activitiService startProcess")
//		ProcessInstance pi = activitiService.startProcess(params)
//		println ("activitiService ok")
//		render "hoho"
//	}
	
	def show(){
		def variables = [:]
		if(params.taskId) {
			variables = taskService.getVariables(params.taskId)
			variables.taskId = params.taskId
		}
		
		if(variables.originAction == 'delete'){
			def instance = variables
			def model = [:]
			model.instance = instance
			render(view: "showdelete", model: model)
		} else {
			def instance = variables
			def model = [:]
			if(variables.originController.equalsIgnoreCase("formulaManagement")){
				if(variables.originAction.equalsIgnoreCase("save"))
					instance.approvalLabel = "Formula Management Save Approval Request"
				else {
					instance.approvalLabel = "Formula Management Update Approval Request"
				}
				model = formulaManagementService.getEditModel(instance)	
				render(view: "show", model: model)
			} else if(variables.originController.equalsIgnoreCase("commonCode")) {
				instance.commoncode = variables.id
				instance.approvalLabel = variables.originController + " " + variables.originAction+" Approval Request"
				//model.instance = instance
				model."${variables.originController}Instance" = instance
				model.instance = instance
				render(view: "showCommonCode", model: model)
			} else if(variables.originController.equalsIgnoreCase("groupLocalBranch")) {
				instance.approvalLabel = variables.originController + " " + variables.originAction+" Approval Request"
				model."${variables.originController}Instance" = instance
				model.instance = instance
				render(view: "showGroupLocalBranch", model: model)
			} else if(variables.originController.equalsIgnoreCase("groupFormDetail")) {
				instance.approvalLabel = variables.originController + " " + variables.originAction+" Approval Request"
				def groupFormInstance = GroupForm.findByPkid(variables.pkid)
				model.groupFormInstance = groupFormInstance
				model.instance = instance
				render(view: "showGroupFormDetail", model: model)
			} else if(variables.originController.equalsIgnoreCase("rolePermissions")) {			
				instance.approvalLabel = variables.originController + " " + variables.originAction+" Approval Request"
				model."${variables.originController}Instance" = instance
				model.instance = instance
			//	println("Model: " + model)
				render(view: "showRolePermissions", model: model)
			}  else if(variables.originController.equalsIgnoreCase("user")) {
				instance.approvalLabel = variables.originController + " " + variables.originAction+" Approval Request"
				
				if(variables.roles){
					if(variables.roles instanceof String){
						instance.roless = variables.roles
					}
					else {
						instance.roless = []
						variables.roles.each { it
							instance.roless << it
						}						
					}
				}
				if(variables.reportTo){
					if(variables.reportTo instanceof String){
						instance.reportToes = variables.reportTo
					}
					else {
						instance.reportToes = []
						variables.reportTo.each { it
							instance.reportToes << it
						}						
					}
				}
					
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				if(instance.effectiveStartDate instanceof String){
					instance.effectiveStartDate = sdf.parse(instance.effectiveStartDate)
				}
				if(instance.effectiveEndDate instanceof String){
					instance.effectiveEndDate = sdf.parse(instance.effectiveEndDate)
				}
				model."${variables.originController}Instance" = instance
				
				
				model.instance = instance
				
				render(view: "showUser", model: model)
			}  else {			
				instance.approvalLabel = variables.originController + " " + variables.originAction+" Approval Request"
				model."${variables.originController}Instance" = instance
				model.instance = instance
			//	println("Model: " + model)
				render(view: "show", model: model)
			}
		}
	}
	
	def approve(Long id){
		//log.info("approve task params" + params )
		def variables = [:]
		
		variables = taskService.getVariables(id as String)
		variables.taskId = id as String
				
		//println("approve task variables" + variables )
		
		Approval approval = Approval.get(variables.approvalId)
		approval.approvalStatus = ApprovalStatus.APPROVED
		approval.approvedBy=SecurityUtils.subject.principal
		approval.approvedOn=new Date()
		log.info("perform Approve, approvalId: "+variables.approvalId+", approval: " + approval)
		
		completeTask(variables)
		render "approved"
	}
	
	def reject(Long id){
		def variables = [:]
		
		variables = taskService.getVariables(id as String)
		variables.taskId = id as String
				
		log.info("reject task variables" + variables )
		
		Approval approval = Approval.get(variables.approvalId)
		approval.approvalStatus = ApprovalStatus.REJECTED
		approval.approvedBy=SecurityUtils.subject.principal
		approval.approvedOn=new Date()
		
		completeTask(variables)
		render "rejected"
	}
	
//	def save(){
//		println("save task params: " + params )
//		render "hoho"
//	}
}
