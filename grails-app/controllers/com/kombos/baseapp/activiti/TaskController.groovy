package com.kombos.baseapp.activiti

//import com.kombos.administrasi.CompanyDealer;

import grails.converters.JSON

class TaskController {

	static activiti = true
	
	def approvalTaskService
	
	def taskService

	def tasksCount = {
		def ret = [unassignedTasksCount: unassignedTasksCount,
			myTasksCount: assignedTasksCount,
			allTasksCount: allTasksCount]
		render ret as JSON
	}

	def tasksNotification = {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def nowTimestamp = new Date().getTime()
		def myTasks = findAssignedTasks(params)
		
		def unassignedTasks = findUnassignedTasks(params)
		
		def taskDetail = [:]
		
		myTasks.each{
			def variables = taskService.getVariables(it.id)	
			if(it.name == 'review Approval Request'){
				if(variables.originAction == 'save')
					taskDetail."${it.id}" = "Approval add " + variables.originController.toLowerCase() 
				else if(variables.originAction == 'update')
					taskDetail."${it.id}" = "Approval edit " + variables.originController.toLowerCase()
				else if(variables.originAction == 'edit')
					taskDetail."${it.id}" = "Approval edit " + variables.originController.toLowerCase()
				else 
					taskDetail."${it.id}" = "Approval delete " + variables.originController.toLowerCase()
			} else {
				taskDetail."${it.id}" = it.name
			}
		}
		
		unassignedTasks.each{			
			def variables = taskService.getVariables(it.id)	
			if(it.name == 'review Approval Request'){
				if(variables.originAction == 'save')
					taskDetail."${it.id}" = "Approval add " + variables.originController.toLowerCase() 
				else if(variables.originAction == 'update')
					taskDetail."${it.id}" = "Approval edit " + variables.originController.toLowerCase()
				else if(variables.originAction == 'edit')
					taskDetail."${it.id}" = "Approval edit " + variables.originController.toLowerCase()
				else 
					taskDetail."${it.id}" = "Approval delete " + variables.originController.toLowerCase()
			} else {
				taskDetail."${it.id}" = it.name
			}
		}
		
		[now: nowTimestamp,
			myTasks: myTasks,
			unassignedTasks: unassignedTasks,
			unassignedTasksCount: unassignedTasksCount,
			myTasksCount: assignedTasksCount,
			allTasksCount: allTasksCount,
			taskDetail: taskDetail]
	}
	
	def myTaskList = {
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		[myTasks: findAssignedTasks(params),
		unassignedTasksCount: unassignedTasksCount,
		myTasksCount: assignedTasksCount,
		allTasksCount: allTasksCount]
		
	}
}
