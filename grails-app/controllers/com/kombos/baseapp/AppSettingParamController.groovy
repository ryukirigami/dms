package com.kombos.baseapp

import grails.converters.JSON

class AppSettingParamController {

    def appSettingParamService

    def securityChecklistService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
    }

    def saveAppSettingParam() {
        def res = [:]

        try{
            def valueInArrayLength = ((String) params.valueInArrayLength).toInteger()
            def i = 0
            def codeKey
            def valueKey
            def valueInList = new HashMap()
            while(i < valueInArrayLength){
                codeKey = "valueInArray["+i+"][code]"
                valueKey =  "valueInArray["+i+"][value]"
                valueInList.put(params.get(codeKey),params.get(valueKey))
                i++
            }

            appSettingParamService.editBatch(valueInList, params, request)

            res.message = "Success Editing Application Setting"
        }catch(Exception e){
            e.printStackTrace()
            res.message = e.message
        }
        render res as JSON
    }

    def saveSecurityChecklist() {
        def res = [:]

        try{
            def valueInArrayLength = ((String) params.valueInArrayLength).toInteger()
            def i = 0
            def codeKey
            def valueKey
            def valueInList = new HashMap()
            while(i < valueInArrayLength){
                codeKey = "valueInArray["+i+"][code]"
                valueKey =  "valueInArray["+i+"][value]"
                valueInList.put(params.get(codeKey),params.get(valueKey))
                i++
            }

            securityChecklistService.editBatch(valueInList, params, request)

            res.message = "Success Editing Application Setting"
        }catch(Exception e){
            e.printStackTrace()
            res.message = e.message
        }
        render res as JSON
    }

}
