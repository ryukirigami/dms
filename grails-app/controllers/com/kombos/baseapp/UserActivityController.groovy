package com.kombos.baseapp

import grails.converters.JSON

//import org.springframework.dao.DataIntegrityViolationException

//import com.kombos.baseapp.UserActivity;

//import java.text.SimpleDateFormat



class UserActivityController {
	
	def dateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

	def userActivityService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list() {
	}

	def getList() {
		def ret = userActivityService.getList(params)
		//		println ("Res = " + ret)
		render ret as JSON
	}

	def show(Long id) {
		def userActivityInstance = UserActivity.get(id)
		if (!userActivityInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'userActivity.label', default: 'UserActivity'), id])
			redirect(action: "list")
			return
		}

		[userActivityInstance: userActivityInstance]
	}
	
}
