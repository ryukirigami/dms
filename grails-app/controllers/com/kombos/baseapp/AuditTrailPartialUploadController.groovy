package com.kombos.baseapp

import grails.converters.JSON

class AuditTrailPartialUploadController {

    def auditTrailPartialUploadService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def getList = {
        params.queryparamsdate = params.sSearch_3
        def ret = auditTrailPartialUploadService.getList(params)
        render ret as JSON
    }

    def index() {

        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

}
