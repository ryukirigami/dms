package com.kombos.baseapp.sec.shiro

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.Divisi
import com.kombos.administrasi.GeneralParameter
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.Approval
import com.kombos.baseapp.ApprovalStatus
import com.kombos.baseapp.AuditTrailLog
import edu.vt.middleware.password.*
import grails.converters.JSON
import groovy.sql.Sql
import org.apache.commons.codec.binary.Base64
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.lang.reflect.UndeclaredThrowableException
import java.sql.SQLException
import java.text.SimpleDateFormat

class UserController {
  //  static activiti = true

    def userMaintenanceService
    def approvalService
    def taskService
    def auditTrailLogUtilService
    def exportService
    def menuService
    def userService
    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def getList = {
        params.initsearch = true
        params.initquery = "enabled = true"
        def ret = userMaintenanceService.getList(params)
        if(params.sColumns)
        {
            session.setAttribute('_params.sColumns',params.sColumns)
            def propertiesToRender = params.sColumns.split(",")
            def x = 0

            propertiesToRender.each { prop->
                session.setAttribute('_params.sSearch_'+x,params."sSearch_${x}")
                x++
            }
        }
        if(params.iSortCol_0)
            session.setAttribute('_params.iSortCol_0',params.iSortCol_0)
//        session.setAttribute("_userTable",ret)
        render ret as JSON
    }

    def index() {
        if(params._menucode){
            session.setAttribute('_menucode',params._menucode)
        }
        redirect(action: "list", params: params)
    }

    def list(){

    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session.userCompanyDealer
        params.companyDealerId = session.userCompanyDealerId
        def ret = userService.datatablesList(params)
        render ret as JSON
    }

    def lists(Integer max) {
        if(params._menucode){
            session.setAttribute('_menucode',params._menucode)
        }

//        def listUser=session.getAttribute("_userTable")
        //userMaintenanceService.getList(params)
//            session.getAttribute("_userTable")
        if(!params.sColumns)
            params.sColumns=session.getAttribute('_params.sColumns')
        if(!params.iSortCol_0)
            params.iSortCol_0=session.getAttribute('_params.iSortCol_0')
        if(params.sColumns)
        {
            def propertiesToRender = params.sColumns.split(",")
            def x = 0

            propertiesToRender.each { prop->
                params."sSearch_${x}" = session.getAttribute('_params.sSearch_'+x)
//                println "PARAMS.SSEARCH: ${params."sSearch_${x}"}"
                x++
            }
        }
//        println "PARAMS.SCOLUMNS: ${params.sColumns}"
        def dExport= userMaintenanceService.getListExport(params)
//        println "DEXPORT: ${dExport}"
        if(!params.max)
        {
//            println "PARAMS.MAX: "+session.getAttribute("_userTable").iTotalRecords
            params.max =dExport.iTotalRecords
            //session.getAttribute("_userTable").iTotalRecords
        }

        if(params?.format && params.format != "html"){
//            println "AADATA: ${session.getAttribute("_userTable").aaData}"
//            def dExport=userMaintenanceService.getListExport(params)
//            println "DEXPORT: ${dExport}"
            //session.getAttribute("_userTable").aaData
            response.contentType = grailsApplication.config.grails.mime.types[params.format]
            response.setHeader("Content-disposition", "attachment; filename=UserList.${params.extension}")

            List fields=
                [
                        "username"
                        , "staffno"
                        , "firstname"
                        , "lastname"
                        , "effectivestartdate"
                        , "effectiveenddate"
                        , "roles"
                        , "localgroup"
                        , "groupform"
                        , "reportto"
                        , "lastloggedin"
                        , "canpasswordexpire"
                        , "accountlocked"
                        , "isactive"
                        , "country"
                        , "createdon"
                ]

            Map labels=
                [
                        "username":"Username"
                        , "staffno":"Staff No"
                        , "firstname":"First Name"
                        , "lastname":"Last Name"
                        , "effectivestartdate":"Effective Start Date"
                        , "effectiveenddate":"Effective End Date"
                        , "roles":"Roles"
                        , "localgroup":"Group Local Branch"
                        , "groupform":"Group Form"
                        , "reportto":"Report To"
                        , "lastloggedin":"Last Logged In On"
                        , "canpasswordexpire":"Password can expire"
                        , "accountlocked":"Lock Out"
                        , "isactive":"Active"
                        , "country":"Country"
                        , "createdon":"User Creation Date"
                ]

            exportService.export(params.format, response.outputStream,dExport.aaData,fields,labels, [:], [:])

        }
//            exportService.export(params.format, response.outputStream,dExport.aaData,[:], [:]) }

        [ userInstance: dExport.aaData ]
    }

    def create() {
//        [userInstance: new User(params)]
        def variables = [:]
        if(params.taskId) {
            variables = taskService.getVariables(params.taskId)
            variables.taskId = params.taskId
        }
        User user=new User(variables)
        user.canPasswordExpire=true
//        user.isActive=true
//        user.accountLocked=false
        [userInstance: user]
    }

    def save() {
        if(!params.passwordHash){
            String passwordHash = this.generateNewPassword()
            params.passwordHash = passwordHash
        }
        User userInstance
        try{
            Date strDate = params.effectiveStartDate
            def general = GeneralParameter.findByCompanyDealerAndM000StaDel(CompanyDealer.get(params?.companyDealer.id?.toLong()),"0")
            Calendar c = Calendar.getInstance();
            c.setTime(strDate);
            if(general){
                c.add(Calendar.YEAR, general.m000DefaultMasaBerlakuAccount);
                strDate = c.getTime();
            }else{
                c.add(Calendar.YEAR, 10);
                strDate = c.getTime();
            }
            params.t001TglRegistered =  datatablesUtilService?.syncTime()
            params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            params.t001ExpiredAccountDate = strDate
            params.effectiveEndDate = strDate
            params.dateCreated = datatablesUtilService?.syncTime()
            params.lastUpdated = datatablesUtilService?.syncTime()
            userInstance = userMaintenanceService.add(params)
            String email = params.t001Email
            def atpos = email.indexOf("@");
            def dotpos = email.lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length()) {
                flash.message = "Format Email Salah"
                render(view: "create", model: [userInstance: userInstance])
                return
            }
            if (!userInstance.save(flush: true)) {
                render(view: "create", model: [userInstance: userInstance])
                return
            } else {
//                menuService.updateUserMenu(userInstance)
                auditTrailLogUtilService.auditTrailDomainInsert(userInstance, params, request, 'MENU_USER')
            }

        }catch (Exception e){
            println("Exception " + e)
            flash.message = e.message
            render(view: "create", model: [userInstance: userInstance])
            return
        }

        flash.message = message(code: 'user.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.username, params.passwordHash])
        redirect(action: "show", id: userInstance.username)

    }

    def check(String id) {
        def userInstance = User.findByUsername(id)
        if (!userInstance) {
            render "notfound"
        } else {
            render "found"
        }
    }

    def show(String id) {
        def userInstance = User.findByUsername(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        def appSettingParamDateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue


        def effectiveStartDate = ""
        def effectiveEndDate = ""
        if(userInstance.effectiveStartDate)
            effectiveStartDate = new SimpleDateFormat(dateFormat).format(userInstance.effectiveStartDate)
        if(userInstance.effectiveEndDate)
            effectiveEndDate = new SimpleDateFormat(dateFormat).format(userInstance.effectiveEndDate)

        [userInstance: userInstance, effectiveStartDate: effectiveStartDate, effectiveEndDate: effectiveEndDate]
    }

    def showPending() {
        def userInstance = User.get(params.username)
        if (!userInstance) {
            userInstance = new User(params)
        }

        render(view: "show", model: [userInstance: userInstance])
    }

    def edit(String id) {
        def userInstance = User.findByUsername(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }
        [userInstance: userInstance]// , coba : coba]
    }

    def update(String id, Long version) {
        def userInstance = User.findByUsername(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userInstance.version > version) {

                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'user.label', default: 'User')] as Object[],
                        "Another user has updated this User while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }

//        userInstance.properties = params
        def oldUserString = auditTrailLogUtilService.readObjectDomain(userInstance).toString()
//        println("OLD USER: " + oldUserString)
        if(params.unlock){
            try{
                userMaintenanceService.unlock(params)
            }catch(Exception e){
                User userInstanceParams = new User(params)
                if (e instanceof UndeclaredThrowableException){
                    flash.message = e.getCause().message
                }else{
                    flash.message = e.message
                }
                render(view: "edit", model: [userInstance: userInstanceParams])
                return
            }
            def newUser = User.findByUsername(id)
            def newUserString = auditTrailLogUtilService.readObjectDomain(newUser).toString()

            auditTrailLogUtilService.saveToAuditTrail(
                    AuditTrailLog.UPDATE
                    ,id
                    ,oldUserString
                    ,newUserString
                    ,auditTrailLogUtilService.setParamRequest(params, request, 'MENU_USER')
            )

            flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.username])
            redirect(action: "show", id: userInstance.username)
        } else {
            try{
                params.imageProfile = session.imageProfile==null?null:session.imageProfile
                params.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                Date strDate = params.effectiveStartDate
                def general = GeneralParameter.findByCompanyDealerAndM000StaDel(CompanyDealer.get(params?.companyDealer.id?.toLong()),"0")
                Calendar c = Calendar.getInstance();
                c.setTime(strDate);
                if(general){
                    c.add(Calendar.YEAR, general.m000DefaultMasaBerlakuAccount);
                    strDate = c.getTime();
                }else{
                    c.add(Calendar.YEAR, 10);
                    strDate = c.getTime();
                }
                params.t001ExpiredAccountDate = strDate
                params.effectiveEndDate = strDate
                String email = params.t001Email
                def atpos = email.indexOf("@");
                def dotpos = email.lastIndexOf(".");
                if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length()) {
                    flash.message = e.message
                    render(view: "create", model: [userInstance: userInstance])
                    return
                }

                userMaintenanceService.edit(params)
                session.imageProfile=null
            }catch(Exception e){
//                e.printStackTrace()
                User userInstanceParams = new User(params)
                if (e instanceof UndeclaredThrowableException){
                    flash.message = e.getCause().message
                }else{
                    flash.message = e.message
                }
                render(view: "edit", model: [userInstance: userInstanceParams])
                return
            }


            def newUser = User.findByUsername(id)
            def newUserString = auditTrailLogUtilService.readObjectDomain(newUser).toString()
            auditTrailLogUtilService.saveToAuditTrail(
                    AuditTrailLog.UPDATE
                    ,id
                    ,oldUserString
                    ,newUserString
                    ,auditTrailLogUtilService.setParamRequest(params, request, 'MENU_USER')
            )
            flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.username])
            redirect(action: "show", id: userInstance.username)
        }
    }

    def dataSource

    def delete(String id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        } else {
            auditTrailLogUtilService.auditTrailDomainDelete(userInstance, params, request, 'MENU_USER')
        }

        //userInstance.username = userInstance.username + "_del_" + new SimpleDateFormat("yyyyMd").format(new Date())
        userInstance.enabled = false
        userInstance.lastUpdated = datatablesUtilService?.syncTime()
        userInstance.save(flush:true)
        userInstance.discard();

        def sql = new Sql(dataSource)
        try
        {
            sql.execute("UPDATE DOM_SHIROUSER set username = :newusername where username = :oldusername", [newusername: userInstance.username + "_del_" + new SimpleDateFormat("yyyyMd").format(new Date()), oldusername: userInstance.username])
        }
        catch (SQLException e)
        {
            println(e.getMessage())
        }

        redirect(action: "list")

//		try {
//			userInstance.delete(flush: true)
//			flash.message = message(code: 'default.deleted.message', args: [
//				message(code: 'user.label', default: 'User'),
//				id
//			])
//			redirect(action: "list")
//		}
//		catch (DataIntegrityViolationException e) {
//			flash.message = message(code: 'default.not.deleted.message', args: [
//				message(code: 'user.label', default: 'User'),
//				id
//			])
//			redirect(action: "show", id: id)
//		}
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(User, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(User, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    //redirect to changePassword.gsp view
    def changePassword(){
        if(params._menucode){
            session.setAttribute('_menucode',params._menucode)
        }
    }

    def editChangePassword = {
        def res = [:]
        try {
            def oldUser = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
            userMaintenanceService.changePassword(params)
            def newUser = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
            auditTrailLogUtilService.auditTrailDomainUpdate(oldUser, newUser, params, request, session.getAttribute('_menucode'))
            res.message = "Password has been changed, please login with the new password"
            res.statusRelogin = userMaintenanceService.getReloginAfterChangePassword()
        } catch (e) {
            e.printStackTrace()
            res.message = e.message?:e.cause?.message
            res.statusRelogin = false
        }
        render res as JSON
    }

    // User task: approve
    def approveSave() {
        def variables = [:]
        if(params.taskId) {
            variables = taskService.getVariables(params.taskId)
            variables.taskId = params.taskId
        }

        redirect(action: "showPending", params: variables)
    }

    def generateNewPassword(){
        // create a password generator
        PasswordGenerator generator = new PasswordGenerator()

        // create character rules to generate passwords with
        List<CharacterRule> rules = new ArrayList<CharacterRule>()
        rules.add(new DigitCharacterRule(1));
        rules.add(new UppercaseCharacterRule(1));
        rules.add(new LowercaseCharacterRule(1));

        return generator.generatePassword(8, rules)
    }

    def generatePassword() {
//		// create a password generator
//		PasswordGenerator generator = new PasswordGenerator()
//
//		// create character rules to generate passwords with
//		List<CharacterRule> rules = new ArrayList<CharacterRule>()
//		rules.add(new DigitCharacterRule(1));
//		rules.add(new UppercaseCharacterRule(1));
//		rules.add(new LowercaseCharacterRule(1));
//
//		render generator.generatePassword(8, rules)

        render this.generateNewPassword()
    }

    def resetPassword(){
        def res = [:]
        try {
            def newPassword = this.generateNewPassword()
            params.newPassword = newPassword
            def oldUser = User.findByUsername(params.username)
            userMaintenanceService.resetPassword(params)
            def newUser = User.findByUsername(params.username)
            auditTrailLogUtilService.auditTrailDomainUpdate(oldUser, newUser, params, request, session.getAttribute('_menucode'))
            res.message = "Password has been changed for user : ${params.username}, please login with the new password : ${newPassword}"
        } catch (e) {
            e.printStackTrace()
            res.message = e.message?:e.cause?.message
        }
        render res as JSON
    }

    def performApprove() {
        def variables = [:]
        if(params.taskId) {
            variables = taskService.getVariables(params.taskId)
            variables.taskId = params.taskId
        }

        def userInstance = User.get(variables.username)
        if (!userInstance) {
            userInstance = userMaintenanceService.add(variables)
        }
        Approval approval = Approval.get(params.approvalId)
        approval.approvalStatus = ApprovalStatus.APPROVED
        userInstance.approval = approval
        userInstance.addToApprovalHistory(approval)
        userInstance.approvalStatus = ApprovalStatus.APPROVED
        userInstance.lastUpdated = datatablesUtilService?.syncTime()

        if (!userInstance.save(flush: true)) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.username])
        completeTask(params)
        redirect(action: "show", id: userInstance.username)
    }

    def uploadImage() {
        if (params.myFile) {
            String fileName
            def inputStream
            def byteFile
            if (params.myFile instanceof CommonsMultipartFile) {
                fileName = params.myFile?.originalFilename
                inputStream = params.myFile.getInputStream()
            } else {
                fileName = params.myFile
                inputStream = request.getInputStream()
            }

//            fileName = org.apache.shiro.SecurityUtils.subject.principal.toString()+"_temp."+fileName.tokenize(".").last()
            fileName = fileName.replaceAll(" ", "_")

            File storedFile
            String dirImage = "C:/image/profile"
            boolean fileSuccessfullyDeleted =  new File("${dirImage}/${fileName}").delete()

            try {
                storedFile = new File("${dirImage}/${fileName}")
                storedFile.append(inputStream)
            }catch(Exception e){
                try{
                    new File("${dirImage}").mkdirs()
                    storedFile = new File("${dirImage}/${fileName}")
                    storedFile.append(inputStream)
                }catch (Exception ex){
                    println(ex)
                    render "Directory ${dirImage} is write protected, please change the directory's permission"
                }
            }

            session.imageProfile = storedFile.path

            render '<img width="250" id="uploadedImage" name="uploadedImage" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(storedFile.bytes), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }

    def findDivisionByCompanyDealer(){
        def html="<option value=\'-1\'> Pilih Divisi</option>"
        def divisi
        CompanyDealer companyDealer
        def divisiUser = null
        if(params.username){
            def cekUser = User.findByUsernameAndStaDel(params.username,"0")
            if(cekUser?.divisi){
                divisiUser = cekUser?.divisi
            }
        }

        if(params?.companyDealer.id!='null' || params?.companyDealer.id!='-1') {
            companyDealer = CompanyDealer.findById(params?.companyDealer.id?.toLong())
            divisi = Divisi.findAllByCompanyDealerAndStaDel(companyDealer,"0")

            divisi.each {
                if(divisiUser){
                    html=html + "<option ${it.id==divisiUser.id?"selected=\'selected\'":""} value=\'${it.id}\'> ${it.m012NamaDivisi}</option>"
                }else{
                    html=html + "<option value=\'${it.id}\'> ${it.m012NamaDivisi}</option>"
                }
            }
            render html
        }
    }

    def findRoleByCompanyDealer(){
        def html=""
        def role
        def boolean empityString = true
        CompanyDealer companyDealer
        def roleUser = null
        if(params.username){
            def users = User.findByUsernameAndStaDel(params.username,"0")
            if(users?.roles){
                roleUser = users?.roles
            }
        }

        if(params?."companyDealer.id"!='null' || params?."companyDealer.id"!='-1') {

            //companyDealer = CompanyDealer.findById(params?.companyDealer.id?.toLong())
            role = Role.createCriteria().list{eq("staDel","0")}//findAllByCompanyDealer(companyDealer)

            role.each {
                if(roleUser){
                    html=html + "<option ${roleUser.contains(it)?"selected=\'selected\'":""} value=\'${it.id}\'> ${it.name}</option>"
                }else{
                    html=html + "<option value=\'${it.id}\'> ${it.name}</option>"
                }
            } //if end upper root

            render html
        }
    }

    def listManPower(){
        def res = [:]
        def opts = []
        def ids = []
        def manPowers = NamaManPower.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",session.userCompanyDealer);
            ilike("t015NamaLengkap","%"+params.query+"%")
            order("t015NamaLengkap");
            maxResults(10);
        }
        manPowers.each {
            opts<<it.t015NamaLengkap
            ids << it.id
        }
        res."options" = opts
        res."ids" = ids
        render res as JSON
    }
}
