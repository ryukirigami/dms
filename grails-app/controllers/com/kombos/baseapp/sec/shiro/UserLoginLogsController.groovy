package com.kombos.baseapp.sec.shiro

import grails.converters.JSON

import java.text.SimpleDateFormat

class UserLoginLogsController {
    def userLoginLogsService
    def exportService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def getList = {
        def username = params.username
        def loginStatusLookupCode = params.loginStatusLookupCode
        String from_date = params.from_date
        String to_date = params.to_date
        def initquery = ''
        def query = ''

        if (username){
            initquery = "username = '" + username +"'"
            params.initsearch = true
        }

        if (loginStatusLookupCode){
            query = "loginStatusLookupCode = '" + loginStatusLookupCode +"'"
            initquery = params.initsearch?initquery+' and '+query:query
            params.initsearch = true
        }

        if (from_date && to_date){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
            Date from = sdf.parse(from_date)
            Date to = sdf.parse(to_date).next()

            query = "loginDatetime >= :from and loginDatetime <= :to"
            params.queryparamsdate = [from:from,to:to]

            initquery = params.initsearch?initquery+' and '+query:query
            params.initsearch = true
        }

        params.initquery = initquery
		params.initoperator = true
        def ret = userLoginLogsService.getList(params)

//        if(params.sColumns)
//        {
//            session.setAttribute('_params.sColumns',params.sColumns)
//            def propertiesToRender = params.sColumns.split(",")
//            def x = 0
//
//            propertiesToRender.each { prop->
//                session.setAttribute('_params.sSearch_'+x,params."sSearch_${x}")
//                x++
//            }
//        }
//        if(params.iSortCol_0)
//            session.setAttribute('_params.iSortCol_0',params.iSortCol_0)

        session.setAttribute('_params',params)

        render ret as JSON
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
//        if(!params.sColumns)
//            params.sColumns=session.getAttribute('_params.sColumns')
//        if(!params.iSortCol_0)
//            params.iSortCol_0=session.getAttribute('_params.iSortCol_0')
//        if(params.sColumns)
//        {
//            def propertiesToRender = params.sColumns.split(",")
//            def x = 0
//
//            propertiesToRender.each { prop->
//                params."sSearch_${x}" = session.getAttribute('_params.sSearch_'+x)
//                x++
//            }
//        }
//
//        def ret=userLoginLogsService.getListExport(params)
//        if(!params.max)
//        {
//            if(ret)
//                params.max =ret.iTotalRecords
//            else
//                params.max=0
//        }
//
//        if(params?.format && params.format != "html"){
//            response.contentType = grailsApplication.config.grails.mime.types[params.format]
//            response.setHeader("Content-disposition", "attachment; filename=LogonReports.${params.extension}")
//
//            List fields=
//                [
//                        "username"
//                        , "loginDatetime"
//                        , "loginStatusLookupCode"
//                ]
//
//            Map labels=
//                [
//                        "username":"User Name"
//                        , "loginDatetime":"Timestamp"
//                        , "loginStatusLookupCode":"Action Type"
//                ]
//
//            exportService.export(params.format, response.outputStream,ret.aaData,fields,labels, [:], [:]) }
    }

    def export=
        {
            def paramSession = session.getAttribute('_params')

            paramSession.iDisplayLength = UserLoginLogs.count

            paramSession.iDisplayStart = 0

            def ret = userLoginLogsService.getList(paramSession)

            if(params?.format && params.format != "html"){
                response.contentType = grailsApplication.config.grails.mime.types[params.format]
                response.setHeader("Content-disposition", "attachment; filename=LogonReports.${params.extension}")

                List fields=
                    [
                            "username"
                            , "loginDatetime"
                            , "loginStatusLookupCode"
                    ]

                Map labels=
                    [
                            "username":"User Name"
                            , "loginDatetime":"Timestamp"
                            , "loginStatusLookupCode":"Action Type"
                    ]

                exportService.export(params.format, response.outputStream,ret.export,fields,labels, [:], [:]) }
        }
}
