package com.kombos.baseapp.sec.shiro

import org.grails.plugin.easygrid.Easygrid
import org.springframework.dao.DataIntegrityViolationException

//import grails.converters.JSON
//import com.kombos.baseapp.sec.shiro.UserAdditionalColumnInfo;

@Easygrid

class UserAdditionalColumnInfoController {

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static grids = {
		userAdditionalColumnInfoDatatables {
			dataSourceType 'gorm'
			domainClass UserAdditionalColumnInfo
			gridImpl 'dataTables'
			fixedColumns false
			columns {

				id { type 'id'
				}
				columnName { 
					label 'app.userAdditionalColumnInfo.columnName.label'
					enableFilter true
					hideOnShrink false
				}
				columnType { 
					label 'app.userAdditionalColumnInfo.columnType.label'
					enableFilter true
					hideOnShrink false
				}
				label { 
					label 'app.userAdditionalColumnInfo.label.label'
					enableFilter true
					hideOnShrink false
				}
			}
		}
	}

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def create() {
		[userAdditionalColumnInfoInstance: new UserAdditionalColumnInfo(params)]
	}

	def save() {
		def userAdditionalColumnInfoInstance = new UserAdditionalColumnInfo(params)
		if (!userAdditionalColumnInfoInstance.save(flush: true)) {
			render(view: "create", model: [userAdditionalColumnInfoInstance: userAdditionalColumnInfoInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), userAdditionalColumnInfoInstance.id])
		redirect(action: "show", id: userAdditionalColumnInfoInstance.id)
	}

	def show(Long id) {
		def userAdditionalColumnInfoInstance = UserAdditionalColumnInfo.get(id)
		if (!userAdditionalColumnInfoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), id])
			redirect(action: "list")
			return
		}

		[userAdditionalColumnInfoInstance: userAdditionalColumnInfoInstance]
	}

	def edit(Long id) {
		def userAdditionalColumnInfoInstance = UserAdditionalColumnInfo.get(id)
		if (!userAdditionalColumnInfoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), id])
			redirect(action: "list")
			return
		}

		[userAdditionalColumnInfoInstance: userAdditionalColumnInfoInstance]
	}

	def update(Long id, Long version) {
		def userAdditionalColumnInfoInstance = UserAdditionalColumnInfo.get(id)
		if (!userAdditionalColumnInfoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (userAdditionalColumnInfoInstance.version > version) {
				
				userAdditionalColumnInfoInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo')] as Object[],
				"Another user has updated this UserAdditionalColumnInfo while you were editing")
				render(view: "edit", model: [userAdditionalColumnInfoInstance: userAdditionalColumnInfoInstance])
				return
			}
		}

		userAdditionalColumnInfoInstance.properties = params

		if (!userAdditionalColumnInfoInstance.save(flush: true)) {
			render(view: "edit", model: [userAdditionalColumnInfoInstance: userAdditionalColumnInfoInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), userAdditionalColumnInfoInstance.id])
		redirect(action: "show", id: userAdditionalColumnInfoInstance.id)
	}

	def delete(Long id) {
		def userAdditionalColumnInfoInstance = UserAdditionalColumnInfo.get(id)
		if (!userAdditionalColumnInfoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), id])
			redirect(action: "list")
			return
		}

		try {
			userAdditionalColumnInfoInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'userAdditionalColumnInfo.label', default: 'UserAdditionalColumnInfo'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(UserAdditionalColumnInfo, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(UserAdditionalColumnInfo, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
