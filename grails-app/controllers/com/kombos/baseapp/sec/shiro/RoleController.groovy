package com.kombos.baseapp.sec.shiro

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class RoleController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def roleService

    //String msgDelete =""

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
//        flash.message = session.msgDelete
        session.msgDelete=""
	}

	def datatablesList() {
		session.exportParams=params

		render roleService.datatablesList(params) as JSON
	}

	def create() {
		def result = roleService.create(params)

        if(!result.error)
            return [roleInstance: result.roleInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		params?.dateCreated = datatablesUtilService?.syncTime()
		params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = roleService.save(params)
        if(result.ada=="ada"){
            flash.message = "Data sudah ada"
            render(view:'create', model:[roleInstance: result.instance])
            return
        }
        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Role", result.roleInstance.id])
            redirect(action:'show', id: result.roleInstance.id)
            return
        }

        render(view:'create', model:[roleInstance: result.roleInstance])
	}

	def show(Long id) {
		def result = roleService.show(params)

		if(!result.error)
			return [ roleInstance: result.roleInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = roleService.show(params)

		if(!result.error)
			return [ roleInstance: result.roleInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = roleService.update(params)

        if(result.ada=="ada"){
            flash.message = "Data sudah ada"
            render(view:'edit', model:[roleInstance: result.instance])
            return
        }

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Role", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[roleInstance: result.roleInstance.attach()])
	}

	def delete() {
        def roleInstance = Role.findByIdAndStaDel(params.id,"0")
        if (!roleInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'role.label', default: 'Role'), params.id])
            redirect(action: "list")
            return
        }

        def user = User.findAllByStaDel("0")
        def hasil=false
        for(find in user){
            def cek=false;
            if(find?.roles){
                for(cari in find?.roles){
                    if(cari.equals(roleInstance)){
                        cek=true;
                        break;
                    }
                }
            }
            if(cek){
                hasil=true
                break;
            }
        }
        if(hasil){
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'role.label', default: 'Role'), params.id])
            redirect(action: "show", id: params.id)
        }else{
            try {
                roleInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
                roleInstance?.setLastUpdProcess("DELETE")
                roleInstance?.setStaDel('1')
                roleInstance?.lastUpdated = datatablesUtilService?.syncTime()
                roleInstance.save(flush: true)
            }
            catch (DataIntegrityViolationException e) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'role.label', default: 'Role'), params.id])
                redirect(action: "show", id: params.id)
            }
        }
    }

	def massdelete() {
		def res = [:]
        String cekRole=""
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Role.findByIdAndStaDel(it,"0")}
        int a=0
        for(cari in oList){
            a++
            def ada=[]
            def user=User.findAllByStaDel("0")
            for(users in user) {
                for(find in users?.roles){
                        if(find==cari){
                            ada << "1"
                            break;
                        }
                }
            }
            if(ada.contains("1")){
                cekRole+=cari.name
                if(a<oList.size()){
                    cekRole+=", "
                }
            }else{
                cari?.staDel = "1";
                cari?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                cari?.lastUpdProcess = "DELETE"
                cari?.lastUpdated = datatablesUtilService?.syncTime()
                cari.save(flush:true)
            }

        }

        try {
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
        if(cekRole==""){
            render "ok"
        }else{
            session.msgDelete = "Data role "+cekRole+" sedang digunakan"
            render "ok"
        }
	}
	
}
