package com.kombos.baseapp.sec.shiro

import com.kombos.baseapp.menu.Menu
import com.kombos.baseapp.menu.RoleMenu
import grails.converters.JSON

class RolePermissionsController {

    def rolePermissionsService

    def list() { }

    def addMenu() { }

    def getList = {
        def ret = rolePermissionsService.getList(params)
        render ret as JSON
    }

    def datatablesList() {
        session.exportParams=params

        render rolePermissionsService.datatablesList(params) as JSON
    }


    def edit = {
        def res = [:]
        try {
            rolePermissionsService.edit(params)
            res.message = "Edit Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render res as JSON
    }

    def save(){
        def jsonArray = JSON.parse(params.ids)
        def role = Role.findById(params.role)
        jsonArray.each {
            def menu = Menu.findById(it.toLong())
            def nMenu = RoleMenu.findByRoleAndMenu(role,menu)
            if(!nMenu){
                nMenu = new RoleMenu()
                nMenu.menu = menu
                nMenu.role = role
                nMenu.save(flush: true)
                role.addToPermissions(menu.linkController+":*")
                role.save(flush: true)
            }
        }
        render "ok"
    }
}
