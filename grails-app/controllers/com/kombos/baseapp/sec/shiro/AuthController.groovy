package com.kombos.baseapp.sec.shiro

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.GeneralParameter
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AccountException
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.IncorrectCredentialsException
import org.apache.shiro.authc.UnknownAccountException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.grails.ConfigUtils
import org.apache.shiro.web.util.WebUtils

import java.text.SimpleDateFormat

class AuthController {
    def shiroSecurityManager
    def securityService
	def menuService
	def shiroActivitiSessionService
//
    def index = { redirect(action: "login", params: params) }

    def login = {
		if('XMLHttpRequest'.equals(request.getHeader('X-Requested-With'))) { 
			response.setHeader 'REQUIRES_AUTH', '1'
		}
        if (params.automaticlogout){
            try{
                new UserLoginLogs(username: params.username, loginStatusLookupCode:UserLoginLogs.AUTOMATIC_LOGOUT_STATUS, loginDatetime:new Date()).save(flush: true)
            }catch (Exception e){
                log.info(e.printStackTrace())
            }
        }
        String appVersion
        AppSettingParam appSettingParam = AppSettingParam.findByCode(AppSettingParamService.APPLICATION_VERSION)
        if(appSettingParam.value == null){
            appVersion = appSettingParam.defaultValue
        }else{
            if(appSettingParam.value.length()==0){
                appVersion = appSettingParam.defaultValue
            }else{
                appVersion = appSettingParam.value
            }
        }
        return [ username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri, appVersion:appVersion ]
    }

    def signIn = {
		// Check session timeout
        //SecurityChecklistUtilService securityChecklistUtilService = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_AUTOMATIC_LOG_OUT)

		byte[] decodedPass = (params.password as String).decodeBase64()
		
        def authToken = new UsernamePasswordToken(params.username, new String(decodedPass))

        // Support for "remember me"
        if (params.rememberMe) {
            authToken.rememberMe = true
        }
        
        // If a controller redirected to this page, redirect back
        // to it. Otherwise redirect to the root URI.
        def targetUri = params.targetUri ?: "/"
        
        // Handle requests saved by Shiro filters.
        def savedRequest = WebUtils.getSavedRequest(request)
        if (savedRequest) {
            targetUri = savedRequest.requestURI - request.contextPath
            if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
        }

        try{
            // Perform the actual login. An AuthenticationException
            // will be thrown if the username is unrecognised or the
            // password is incorrect.

            //Checking the password length
//            SecurityChecklistUtilService securityChecklistUtil = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_MINIMUM_PASSWORD_LENGTH)
//            def passwordLength = securityChecklistUtil.getMinimumPasswordLength()
//            if(!securityChecklistUtil.validateMinimumPasswordLength(params.password)) {
//                throw new AuthenticationException(grailsApplication?.getMainContext()?.getMessage('login.failed.minimum_password_length', [passwordLength] as Object[], "Minimum password length is "+passwordLength, request.getLocale()))
//            }
			
			User user = User.findByUsername(authToken.username)
			if(user == null){
				throw new UnknownAccountException("username or password is invalid")
			}

            if(user && user.effectiveStartDate){
                if(user.effectiveStartDate.after(new Date())){
                    throw new UnknownAccountException("Your account is not active yet")
                }
            }
			
			//Perform security checklist
			securityService.doAuthenticationChecklist(User.findByUsername(authToken.username), request)
            
			SecurityUtils.subject.login(authToken)
			
			shiroActivitiSessionService.attachUsername2Session()

            Date zonaWaktu = new Date()
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss")

            Calendar cal = Calendar.getInstance()
            Calendar cal2 = Calendar.getInstance()
            cal.add(Calendar.HOUR,1)
            cal2.add(Calendar.HOUR,2)
            Date sejam = cal.getTime()
            Date duojam = cal2.getTime()

            if(user.companyDealer){
                    session.userCompanyDealer = user.companyDealer
                    session.userAuthority = user.getRoles().toString()
                    session.userCompanyDealerId = user.companyDealer.id
                    session.selisihWaktu = (user?.companyDealer?.m011SelisihWaktu?user?.companyDealer?.m011SelisihWaktu:0)
                    CompanyDealer companyDealer = session.userCompanyDealer
                    GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
                    int h = generalParameterInstance ? generalParameterInstance?.m000WaktuSessionLog?.getHours() : 100;
                    int m = generalParameterInstance ? generalParameterInstance?.m000WaktuSessionLog?.getMinutes() : 100;
                    session.setMaxInactiveInterval((h*3600) + (m*60))
                    log.info("Session timeout: " + ((h*3600) + (m*60)));
            }else{
                session.userCompanyDealer = null
                session.userCompanyDealerId = null
                session.selisihWaktu = null
            }
			log.info "Redirecting to '${targetUri}'."
            redirect(uri: targetUri)
        }
		catch (AccountException ex){
			ex.printStackTrace()
			//update failed login date if user is exist
			User user = User.findByUsername(params.username)
			if(user){
				user.lastUnsuccessfullLoggedIn = new Date()
				if(user.save()){
					new UserLoginLogs(username: user.username, loginStatusLookupCode:UserLoginLogs.FAILED_LOGIN_STATUS, loginDatetime:new Date(), reason: ex.message?:ex.cause.message).save(flush: true)
				}
			}

			log.info "Authentication error for user '${params.username}', message : ${ex.message?:ex.cause.message}"

		   // flash.message = "Authentication error, ${ex.message?:ex.cause.message}"
			flash.message = "Authentication error, username or password is invalid"

			def m = [ username: params.username ]
			if (params.rememberMe) {
				m["rememberMe"] = true
			}

			// Remember the target URI too.
			if (params.targetUri) {
				m["targetUri"] = params.targetUri
			}

			// Now redirect back to the login page.
			redirect(action: "login", params: m)
		}
		catch (UnknownAccountException ex){
			ex.printStackTrace()
			//update failed login date if user is exist
			User user = User.findByUsername(params.username)
			if(user){
				user.lastUnsuccessfullLoggedIn = new Date()
				if(user.save()){
					new UserLoginLogs(username: user.username, loginStatusLookupCode:UserLoginLogs.FAILED_LOGIN_STATUS, loginDatetime:new Date(), reason: ex.message?:ex.cause.message).save(flush: true)
				}
			}

			log.info "Authentication error for user '${params.username}', message : ${ex.message?:ex.cause.message}"

		   // flash.message = "Authentication error, ${ex.message?:ex.cause.message}"
			flash.message = "Authentication error, username or password is invalid"

			def m = [ username: params.username ]
			if (params.rememberMe) {
				m["rememberMe"] = true
			}

			// Remember the target URI too.
			if (params.targetUri) {
				m["targetUri"] = params.targetUri
			}

			// Now redirect back to the login page.
			redirect(action: "login", params: m)
		}
		catch (NotActiveException ex){
			ex.printStackTrace()
			//update failed login date if user is exist
			User user = User.findByUsername(params.username)
			if(user){
				user.lastUnsuccessfullLoggedIn = new Date()
				if(user.save()){
					new UserLoginLogs(username: user.username, loginStatusLookupCode:UserLoginLogs.FAILED_LOGIN_STATUS, loginDatetime:new Date(), reason: ex.message?:ex.cause.message).save(flush: true)
				}
			}

			log.info "Authentication error for user '${params.username}', message : ${ex.message?:ex.cause.message}"

		   // flash.message = "Authentication error, ${ex.message?:ex.cause.message}"
			flash.message = "Authentication error, Your account is not active yet"

			def m = [ username: params.username ]
			if (params.rememberMe) {
				m["rememberMe"] = true
			}

			// Remember the target URI too.
			if (params.targetUri) {
				m["targetUri"] = params.targetUri
			}

			// Now redirect back to the login page.
			redirect(action: "login", params: m)
		}
		catch (IncorrectCredentialsException ex){
			ex.printStackTrace()
			//update failed login date if user is exist
			User user = User.findByUsername(params.username)
			if(user){
				user.lastUnsuccessfullLoggedIn = new Date()
				if(user.save()){
					new UserLoginLogs(username: user.username, loginStatusLookupCode:UserLoginLogs.FAILED_LOGIN_STATUS, loginDatetime:new Date(), reason: ex.message?:ex.cause.message).save(flush: true)
				}
			}

			log.info "Authentication error for user '${params.username}', message : ${ex.message?:ex.cause.message}"

		    flash.message = "Authentication error, ${ex.message?:ex.cause.message}"
			//flash.message = "Authentication error, username or password is invalid"

			def m = [ username: params.username ]
			if (params.rememberMe) {
				m["rememberMe"] = true
			}

			// Remember the target URI too.
			if (params.targetUri) {
				m["targetUri"] = params.targetUri
			}

			// Now redirect back to the login page.
			redirect(action: "login", params: m)
		}
        catch (AuthenticationException ex){
            ex.printStackTrace()
            // Authentication failed, so display the appropriate message
            // on the login page.

            //update failed login date if user is exist
            User user = User.findByUsername(params.username)
            if(user){
                user.lastUnsuccessfullLoggedIn = new Date()
                if(user.save()){
                    new UserLoginLogs(username: user.username, loginStatusLookupCode:UserLoginLogs.FAILED_LOGIN_STATUS, loginDatetime:new Date(), reason: ex.message?:ex.cause.message).save(flush: true)
                }
            }

            log.info "Authentication failure for user '${params.username}', message : ${ex.message?:ex.cause.message}"
            //flash.message = message(code: "login.failed")
            flash.message = "${message(code: "login.failed", default: "Authentication failed")}, ${ex.message?:ex.cause.message}"
			//flash.message = "${message(code: "login.failed", default: "Authentication failed")}, username or password is invalid"
            // Keep the username and "remember me" setting so that the
            // user doesn't have to enter them again.
            def m = [ username: params.username ]
            if (params.rememberMe) {
                m["rememberMe"] = true
            }

            // Remember the target URI too.
            if (params.targetUri) {
                m["targetUri"] = params.targetUri
            }

            // Now redirect back to the login page.
            redirect(action: "login", params: m)
        }catch(Exception ex){
            ex.printStackTrace()
            //update failed login date if user is exist
            User user = User.findByUsername(params.username)
            if(user){
                user.lastUnsuccessfullLoggedIn = new Date()
                if(user.save()){
                    new UserLoginLogs(username: user.username, loginStatusLookupCode:UserLoginLogs.FAILED_LOGIN_STATUS, loginDatetime:new Date(), reason: ex.message?:ex.cause.message).save(flush: true)
                }
            }

            log.info "Authentication error for user '${params.username}', message : ${ex.message?:ex.cause.message}"

           // flash.message = "Authentication error, ${ex.message?:ex.cause.message}"
			flash.message = "Authentication error, username or password is invalid"

            def m = [ username: params.username ]
            if (params.rememberMe) {
                m["rememberMe"] = true
            }

            // Remember the target URI too.
            if (params.targetUri) {
                m["targetUri"] = params.targetUri
            }

            // Now redirect back to the login page.
            redirect(action: "login", params: m)
        }
    }

    def signOut = {
        // Log the user out of the application.
        def principal = SecurityUtils.subject?.principal
		new UserLoginLogs(username: principal, loginStatusLookupCode:UserLoginLogs.NORMAL_LOGOUT_STATUS, loginDatetime:new Date(), reason: "").save(flush: true)
        SecurityUtils.subject?.logout()
        // For now, redirect back to the home page.
        if (ConfigUtils.getCasEnable() && ConfigUtils.isFromCas(principal)) {
            redirect(uri:ConfigUtils.getLogoutUrl())
        }else {
            redirect(uri: "/")
        }
        ConfigUtils.removePrincipal(principal)
    }

    def unauthorized = {
        render message(code: 'security.denied.message', default: "Sorry, you're not authorized to access this page.")
    }

    def checkSession = {
        if (SecurityUtils.subject?.principal == null) {
            def ret = [username:SecurityUtils.subject?.principal,loggedin:false]
            render ret as JSON
        }else{
            def ret = [username:SecurityUtils.subject?.principal,loggedin:true]
            render ret as JSON
        }
    }

    def getPermission = {
        def linkController = ((String)params.linkController).tokenize("/")[1]
        def allowAdd = false
        def allowEdit = false
        def allowDelete = false

        def permission
        def role
        def permissionDetail
        /* getting permission from User object (implicit role)*/
        permission = User.findByUsername(SecurityUtils.subject.principal).permissions
        permission?.each{
            permissionDetail = it.tokenize(":")
            permissionDetail[0] = !permissionDetail[0]?"":permissionDetail[0]
            permissionDetail[1] = !permissionDetail[1]?"":permissionDetail[1]
            permissionDetail[2] = !permissionDetail[2]?"":permissionDetail[2]

            if(permissionDetail[0].equals(linkController) || permissionDetail[0].equals("*")){
                if(((String)permissionDetail[1]).contains("add") || ((String)permissionDetail[1]).contains("*")){
                    allowAdd = true
                }
                if(((String)permissionDetail[1]).contains("edit") || ((String)permissionDetail[1]).contains("*")){
                    allowEdit = true
                }
                if(((String)permissionDetail[1]).contains("delete") || ((String)permissionDetail[1]).contains("*")){
                    allowDelete = true
                }
            }
        }

        /* getting permission from Rule object (explicit role)*/
        role = User.findByUsername(SecurityUtils.subject.principal).roles
        role?.each{
            permission = it.permissions
            permission?.each{
                permissionDetail = it.tokenize(":")
                permissionDetail[0] = !permissionDetail[0]?"":permissionDetail[0]
                permissionDetail[1] = !permissionDetail[1]?"":permissionDetail[1]
                permissionDetail[2] = !permissionDetail[2]?"":permissionDetail[2]

                if(permissionDetail[0].equals(linkController) || permissionDetail[0].equals("*")){
                    if(((String)permissionDetail[1]).contains("add") || ((String)permissionDetail[1]).contains("*")){
                        allowAdd = true
                    }
                    if(((String)permissionDetail[1]).contains("edit") || ((String)permissionDetail[1]).contains("*")){
                        allowEdit = true
                    }
                    if(((String)permissionDetail[1]).contains("delete") || ((String)permissionDetail[1]).contains("*")){
                        allowDelete = true
                    }
                }
            }
        }

        def ret = [allow_add:allowAdd,allow_edit:allowEdit,allow_delete:allowDelete]
        render ret as JSON
    }
}
