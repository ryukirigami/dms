package com.kombos.baseapp

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class CommonCodeController {
	def auditTrailLogUtilService
    def datatablesUtilService

	def dateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

//	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
    def commonCodeService

    def getList = {
        if (params.codetype){
            params.initsearch = true
            params.initquery = "codetype = '" + params.codetype +"'"
        }
		params.initoperator = true
        def ret = commonCodeService.getList(params)
        render ret as JSON
    }

	def index() {
        if(params._menucode){
            session.setAttribute('_menucode',params._menucode)
        }
        redirect(action: "list", params: params)
	}

	def list(Integer max) {
        if(params._menucode){
            session.setAttribute('_menucode',params._menucode)
        }
    }

	def create() {
		[commonCodeInstance: new CommonCode(params)]
	}

	def save() {
		def commonCodeInstance = new CommonCode(params)
		if (!commonCodeInstance.save(flush: true)) {
			render(view: "create", model: [commonCodeInstance: commonCodeInstance])
			return
		}else{
            auditTrailLogUtilService.auditTrailDomainInsert(commonCodeInstance, params, request, session.getAttribute('_menucode'))
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), commonCodeInstance.commoncode])
		redirect(action: "show", id: commonCodeInstance.commoncode)
	}

	def show(String id) {
		def commonCodeInstance = CommonCode.findByCommoncode(id)
		if (!commonCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), id])
			redirect(action: "list")
			return
		}

		[commonCodeInstance: commonCodeInstance]
	}

	def edit(String id) {
		def commonCodeInstance = CommonCode.findByCommoncode(id)
		if (!commonCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), id])
			redirect(action: "list")
			return
		}

		[commonCodeInstance: commonCodeInstance]
	}

	def update(String id, Long version) {
//        def oldCommonCodeInstance =  CommonCode.findByCommoncode(id)
//        oldCommonCodeInstance.discard()
        def commonCodeInstance = CommonCode.findByCommoncode(id)
		if (!commonCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (commonCodeInstance.version > version) {
				
				commonCodeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'commonCode.label', default: 'CommonCode')] as Object[],
				"Another user has updated this CommonCode while you were editing")
				render(view: "edit", model: [commonCodeInstance: commonCodeInstance])
				return
			}
		}

        def oldUserString = auditTrailLogUtilService.readObjectDomain(commonCodeInstance).toString()

		commonCodeInstance.properties = params

		if (!commonCodeInstance.save(flush: true)) {
			render(view: "edit", model: [commonCodeInstance: commonCodeInstance])
			return
		}else{
            def newUser =  CommonCode.findByCommoncode(id)
            def newUserString = auditTrailLogUtilService.readObjectDomain(newUser).toString()

            auditTrailLogUtilService.saveToAuditTrail(
                    AuditTrailLog.UPDATE
                    ,id
                    ,oldUserString
                    ,newUserString
                    ,auditTrailLogUtilService.setParamRequest(params, request, session.getAttribute('_menucode'))
            )

//            auditTrailLogUtilService.auditTrailDomainUpdate(oldCommonCodeInstance, commonCodeInstance, params, request, session.getAttribute('_menucode'))
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), commonCodeInstance.commoncode])
		redirect(action: "show", id: commonCodeInstance.commoncode)
	}

	def delete(String id) {
        def oldCommonCodeInstance = CommonCode.findByCommoncode(id)
        oldCommonCodeInstance.discard()
        def commonCodeInstance = CommonCode.findByCommoncode(id)
		if (!commonCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), id])
			redirect(action: "list")
			return
		}

		try {
			commonCodeInstance.delete(flush: true)
            auditTrailLogUtilService.auditTrailDomainDelete(oldCommonCodeInstance, params, request, session.getAttribute('_menucode'))
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'commonCode.label', default: 'CommonCode'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			//datatablesUtilService.massDelete(CommonCode, params)

            log.info(params.ids)
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            jsonArray.each
                    {
                        oList << CommonCode.findByCommoncode(it as String)
                    }

            oList*.discard() //detach all the objects from session
            oList.each{ it.delete() }

			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(CommonCode, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
