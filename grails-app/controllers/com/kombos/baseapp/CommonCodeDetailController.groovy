package com.kombos.baseapp

import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import org.springframework.dao.DataIntegrityViolationException

class CommonCodeDetailController {

    def dateFormat = com.kombos.baseapp.AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def commonCodeDetailService

    def auditTrailLogUtilService

    def getList = {
        if (params.parentcode){
            params.initsearch = true
            params.initquery = "parentbicode = '" + params.parentcode + "'"
        }

        if(params.commoncode){
            params.initsearch = true
//            params.initquery = "commoncode = '" + params.commoncode + "' and parentbicode = null"
            params.initquery = "commoncode = '" + params.commoncode + "'"
        }

        def ret = commonCodeDetailService.getList(params)
        render ret as JSON
    }

    def index() {
        if(params._menucode){
            session.setAttribute('_menucode',params._menucode)
        }
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        if(params._menucode){
            session.setAttribute('_menucode',params._menucode)
        }
    }

    def create() {
        [commonCodeDetailInstance: new CommonCodeDetail(params)]
    }

    def save() {
        def commonCodeDetailInstance = new CommonCodeDetail(params)
        commonCodeDetailInstance.createdhost="${request.getRemoteAddr()}"
        commonCodeDetailInstance.lastedithost="${request.getRemoteAddr()}"
        commonCodeDetailInstance.lasteditby=SecurityUtils.subject.principal

        commonCodeDetailInstance.pkid=commonCodeDetailService.getPkid()

        if (!commonCodeDetailInstance.save(flush: true)) {
            render(view: "create", model: [commonCodeDetailInstance: commonCodeDetailInstance])
            return
        }else{
            auditTrailLogUtilService.auditTrailDomainInsert(commonCodeDetailInstance, params, request, session.getAttribute('_menucode'))
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), commonCodeDetailInstance.pkid])
        redirect(action: "show", id: commonCodeDetailInstance.pkid)
    }

    def show(Long id) {
        def commonCodeDetailInstance = CommonCodeDetail.findByPkid(id)
        if (!commonCodeDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), id])
            redirect(action: "list")
            return
        }

        [commonCodeDetailInstance: commonCodeDetailInstance]
    }

    def edit(Long id) {
        def commonCodeDetailInstance = CommonCodeDetail.findByPkid(id)
        if (!commonCodeDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), id])
            redirect(action: "list")
            return
        }

        [commonCodeDetailInstance: commonCodeDetailInstance]
    }

    def update(Long id, Long version) {
//        def oldCommonCodeDetailInstance =  CommonCodeDetail.findByPkid(id)
//        oldCommonCodeDetailInstance.discard()
        def commonCodeDetailInstance = CommonCodeDetail.findByPkid(id)
        if (!commonCodeDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (commonCodeDetailInstance.version > version) {

                commonCodeDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail')] as Object[],
                        "Another user has updated this CommonCodeDetail while you were editing")
                render(view: "edit", model: [commonCodeDetailInstance: commonCodeDetailInstance])
                return
            }
        }

        def oldUserString = auditTrailLogUtilService.readObjectDomain(commonCodeDetailInstance).toString()

        commonCodeDetailInstance.properties = params
        commonCodeDetailInstance.lasteditdate=new Date()
        commonCodeDetailInstance.lasteditby=SecurityUtils.subject.principal
        commonCodeDetailInstance.lastedithost="${request.getRemoteAddr()}"

        if (!commonCodeDetailInstance.save(flush: true)) {
            render(view: "edit", model: [commonCodeDetailInstance: commonCodeDetailInstance])
            return
        }else{
            def newUser =  CommonCodeDetail.findByPkid(id)
            def newUserString = auditTrailLogUtilService.readObjectDomain(newUser).toString()

            auditTrailLogUtilService.saveToAuditTrail(
                    AuditTrailLog.UPDATE
                    ,id
                    ,oldUserString
                    ,newUserString
                    ,auditTrailLogUtilService.setParamRequest(params, request, session.getAttribute('_menucode'))
            )

//            auditTrailLogUtilService.auditTrailDomainUpdate(oldCommonCodeDetailInstance, commonCodeDetailInstance, params, request, session.getAttribute('_menucode'))
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), commonCodeDetailInstance.pkid])
        redirect(action: "show", id: commonCodeDetailInstance.pkid)
    }

    def delete(Long id) {
        def oldCommonCodeDetailInstance =  CommonCodeDetail.get(id)
        oldCommonCodeDetailInstance.discard()
        def commonCodeDetailInstance = CommonCodeDetail.get(id)
        if (!commonCodeDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), id])
            redirect(action: "list")
            return
        }

        try {
            commonCodeDetailInstance.delete(flush: true)
            auditTrailLogUtilService.auditTrailDomainDelete(oldCommonCodeDetailInstance, params, request, session.getAttribute('_menucode'))
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'commonCodeDetail.label', default: 'CommonCodeDetail'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(CommonCodeDetail, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(CommonCodeDetail, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
