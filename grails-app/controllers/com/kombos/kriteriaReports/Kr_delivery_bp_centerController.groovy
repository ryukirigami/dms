package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import javax.swing.table.DefaultTableModel

class Kr_delivery_bp_centerController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def deliveryBPCenterService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
	}

	def datatablesSAList(){
		session.exportParams=params;
		render deliveryBPCenterService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render deliveryBPCenterService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render deliveryBPCenterService.datatablesJenisPekerjaan(params) as JSON;			
	}

}