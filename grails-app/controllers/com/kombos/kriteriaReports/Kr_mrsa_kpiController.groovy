package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.finance.MonthlyBalanceController
import com.kombos.maintable.Invitation
import com.kombos.maintable.JenisInvitation
import com.kombos.maintable.MappingCustVehicle
import com.kombos.maintable.MrsKpiResult
import com.kombos.maintable.R001_MRS_KPI_PROSES
import com.kombos.maintable.R003_CONTROL_BOARD
import com.kombos.maintable.Reminder
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.text.DecimalFormat
import java.text.NumberFormat

class Kr_mrsa_kpiController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index','list','datatablesList']

    def sessionFactory

    def index() {
        def cd = CompanyDealer.findById(session.userCompanyDealerId)?.id
        [cd : cd]
    }

    def previewData(){
        def formatFile = "",formatBuntut = "";
        if(params.format=="x"){
            formatFile = ".xls";
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }

        List<JasperReportDef> reportDefList = []
        List<JasperReportDef> reportDefList2 = []
        def file = null
        def file2=null
        def reportData = calculateReportData(params)
        def reportData2 = calculateReportDataDetail(params)

        if(params.namaReport=="01") {
            def reportDef = new JasperReportDef(name:'KPIProccess.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("KPI_Proccess_",formatFile)
        }
        else if(params.namaReport=="02"){
            def reportDef = new JasperReportDef(name:'KPISummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KPIDetail.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("KPI_Direct_Mail_",formatFile)
        }
        else if(params.namaReport=="03"){
            def reportDef = new JasperReportDef(name:'KPISMSSummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KPIDetail.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("KPI_SMS_",formatFile)
        }
        else if(params.namaReport=="04"){
            def reportDef = new JasperReportDef(name:'KPISummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KPIDetail.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("KPI_CALL_",formatFile)
        }
        else if(params.namaReport=="05"){
            def reportDef = new JasperReportDef(name:'KPIAppointmentSummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KPIAppointmentDetail.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("KPI_APPOINTMENT_",formatFile)
        }
        else if(params.namaReport=="06"){
            def reportDef = new JasperReportDef(name:'KPIResult.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("KPI_Result_",formatFile)
        }
        else if(params.namaReport=="07"){
            def reportDef = new JasperReportDef(name:'KPIAppointmentContributionSummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KPIAppointmentContributionDetail.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("APPOINTMENT_CONTRIBUTION_",formatFile)
        }
        else if(params.namaReport=="08"){
            def reportDef = new JasperReportDef(name:'KPISBEContributionSummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KPISBEContributionDetail.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("SBE_CONTRIBUTION_",formatFile)
        }
        else if(params.namaReport=="09"){
            def reportDef = new JasperReportDef(name:'KPIAppointmentContributionSummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KPISBEContributionDetail.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("UNIT_ENTRY_CONTRIBUTION_",formatFile)
        }
        else if(params.namaReport=="10"){
            def reportDef = new JasperReportDef(name:'KPIProccessDetail.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("KPI_Proccess_Detail_",formatFile)
        }
        else if(params.namaReport=="11"){
            def reportDef = new JasperReportDef(name:'KPITotal.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("KPI_TOTAL_",formatFile)
        }
        else if(params.namaReport=="12"){
            def reportDef = new JasperReportDef(name:'MRSSBSummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)

            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'MRSSBDetil.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("SB_RETENTION_FIRST_SERVICE_",formatFile)
        }
        else if(params.namaReport=="13"){
            def reportDef = new JasperReportDef(name:'MRSSBSummary.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData:reportData
            )
            reportDefList.add(reportDef)
            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'MRSSBPeriod.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2
                )
                reportDefList.add(reportDef2)
            }
            file = File.createTempFile("SB_RETENTION_PERIOD_",formatFile)
        }
        else if(params.namaReport=="14"){
            def reportDef = new JasperReportDef(name:'Customize Report.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Customize_Report_",formatFile)
        }
        else if(params.namaReport=="15"){
            def reportDef = new JasperReportDef(name:'PARTSPERFORMANCE.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("FillRate_",formatFile)
        }

        file.deleteOnExit() //Aktifkan dulu
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", formatBuntut)
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def cekSama(def params, int x){
        if(x>=params.bulan1.toInteger() && x<=params.bulan2.toInteger()){
            return 1
        }
        return 0
    }

    def calculateReportData(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
        String tanggal = params.tanggal //kebo
        String tanggalEnd = params.tanggal2
        def strDate = new Date().parse("d-MM-yyyy",tanggal.toString())
        def strEndDate = new Date().parse("d-MM-yyyy",tanggalEnd.toString())

        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy")
        Calendar cal1 = Calendar.getInstance()
        if(params.namaReport=="01"){
            def title = ["Customer Yang Diundang","Direct Mail Dikirim","Akurasi Alamat Customer","SMS Dikirim","Akurasi SMS Yang Dikirim","Customer Yang Ditelepon",
                    "Akurasi Nomor Telepon Customer","Customer Yang Appointment Saat Ditelepon","Customer Yang Appointment Setelah Ditelepon","Customer Yang Appointment Karena Aktivitas MRS"]
            def child1 = [ "- Jumlah Customer Yang Diundang (Actual)","- Jumlah Direct Mail Dikirim (Actual)","- Jumlah Direct Mail Diterima Customer (Receive)",
                    "- Jumlah SMS Yang Dikirim (Actual)","- Jumlah SMS Yang Berhasil Diterima Customer (Receive)","- Jumlah Customer Yang Ditelepon (Actual)",
                    "- Jumlah Telepon Terhubung (Connected)","- Jumlah Customer Yang Appointment Saat Ditelepon (At Call)","- Jumlah Customer Yang Appointment Setelah Ditelepon (After Call)",
                    "- Jumlah Customer Yang Appointment ( At Call + After Call)"]
            def child2 = [ "- Jumlah Customer Yang Harus Diundang (Plan)","- Jumlah Customer Yang Diundang Melalui Direct Mail (Plan)","- Jumlah Direct Mail Dikirim (Actual)",
                    "- Jumlah Customer Yang Diundang Melalui SMS (Plan)","- Jumlah SMS Yang Dikirim (Actual)","- Jumlah Customer Yang Diundang Melalui Telepon (Plan)","- Jumlah Customer Yang Ditelepon (Actual)",
                    "- Jumlah Telepon Terhubung (Connected)","- Jumlah Telepon Terhubung (Connected)","- Jumlah Telepon Yang Terhubung (Connected)"]

            def dataTitle=    ["jan1", "feb1", "mar1", "apr1", "mei1", "jun1", "jul1", "agu1", "sep1", "okt1", "nov1", "des1"]
            def dataSubtitle1=["jan2", "feb2", "mar2", "apr2", "mei2", "jun2", "jul2", "agu2", "sep2", "okt2", "nov2", "des2"]
            def dataSubtitle2=["jan3", "feb3", "mar3", "apr3", "mei3", "jun3", "jul3", "agu3", "sep3", "okt3", "nov3", "des3"]

            int[] isiTitle=    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle1=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle2=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle3=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle4=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle5=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle6=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle7=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle8=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle9=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle10=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle11=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle12=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle13=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle14=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle15=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle16=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle17=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle18=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            int[] isiSubtitle19=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            int[] isiSubtitle20=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]



            def hari = new MonthlyBalanceController().tanggalAkhir((Integer.parseInt(params.bulan2)+1),params.tahun.toInteger());

            Date tglmulai=new Date().parse("dd-MM-yyyy","01-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglahir=new Date().parse("dd-MM-yyyy",hari+"-"+(Integer.parseInt(params.bulan2)+1)+"-"+params.tahun)

            final session = sessionFactory.currentSession
            String query =
                    "select CUST_DIUNDANG_ACT," +
                            "CUST_DIUNDANG_PLAN, " +
                            "DM_DIKIRIM, " +
                            "DM_PLAN, " +
                            "AKURASI_DM_KIRIM, " +
                            "AKURASI_DM_ACT, " +
                            "SMS_KIRIM, " +
                            "SMS_PLAN, " +
                            "AKURASI_SMS_DITERIMA, " +
                            "JML_SMS_DIKIRIM, " +
                            "CUST_DITELP_ACT, " +
                            "CUST_DITELP_PLAN, " +
                            "CUST_TERHUBUNG, " +
                            "AKURASI_CUST_DITELP, " +
                            "APPOINTMENT_ATCALL, " +
                            "APPOINTMENT_AFTER, " +
                            "TANGGAL_REPORT from R001_MRS_KPI_PROSES  " +
                            "WHERE TANGGAL_REPORT >= ? " +
                            "AND TANGGAL_REPORT < ? "+
                            "AND R001_M011_ID < ? ";
            try {
                final sqlQuery = session.createSQLQuery(query);
                sqlQuery.setParameter(0,tglmulai);
                sqlQuery.setParameter(1,(tglahir+1).clearTime());
                sqlQuery.setParameter(2,CompanyDealer?.findById(params.workshop as Long)?.id);
                final queryResults = sqlQuery.with {
                    list()
                }

                queryResults.collect { resultRow ->
                    def val1 = 0,val2 = 0,val3 = 0,val4 = 0,val5 = 0,val6 = 0,val7 = 0,val8 = 0,val9 = 0,val10 = 0,val11 = 0,val12 = 0,val13 = 0,val14 = 0,val15 = 0,val16 = 0;
                    def val17 = 0, val18 = 0, val19 = 0, val20 = 0;
                    Date tgglReport = resultRow[16]
                    val1=(resultRow[0]?resultRow[0].toInteger():0)
                    val2=(resultRow[1]?resultRow[1].toInteger():0)
                    val3=(resultRow[2]?resultRow[2].toInteger():0)
                    val4=(resultRow[3]?resultRow[3].toInteger():0)
                    val5=(resultRow[4]?resultRow[4].toInteger():0)
                    val6=(resultRow[5]?resultRow[5].toInteger():0)
                    val7=(resultRow[6]?resultRow[6].toInteger():0)
                    val8=(resultRow[7]?resultRow[7].toInteger():0)
                    val9=(resultRow[8]?resultRow[8].toInteger():0)
                    val10=(resultRow[9]?resultRow[9].toInteger():0)
                    val11=(resultRow[10]?resultRow[10].toInteger():0)
                    val12=(resultRow[11]?resultRow[11].toInteger():0)
                    val13=(resultRow[12]?resultRow[12].toInteger():0)
                    val14=(resultRow[13]?resultRow[13].toInteger():0)
                    val15=(resultRow[14]?resultRow[14].toInteger():0)
                    val16=(resultRow[12]?resultRow[12].toInteger():0)
                    val17=(resultRow[15]?resultRow[15].toInteger():0)
                    val18=(resultRow[12]?resultRow[12].toInteger():0)
                    val19=((resultRow[14]?resultRow[14].toInteger():0)+(resultRow[15]?resultRow[15].toInteger():0))
                    val20=(resultRow[12]?resultRow[12].toInteger():0)

                    if(tgglReport?.format("M")=="1"){
                        isiSubtitle1[0]+=val1
                        isiSubtitle2[0]+=val2
                        isiSubtitle3[0]+=val3
                        isiSubtitle4[0]+=val4
                        isiSubtitle5[0]+=val5
                        isiSubtitle6[0]+=val6
                        isiSubtitle7[0]+=val7
                        isiSubtitle8[0]+=val8
                        isiSubtitle9[0]+=val9
                        isiSubtitle10[0]+=val10
                        isiSubtitle11[0]+=val11
                        isiSubtitle12[0]+=val12
                        isiSubtitle13[0]+=val13
                        isiSubtitle14[0]+=val14
                        isiSubtitle15[0]+=val15
                        isiSubtitle16[0]+=val16
                        isiSubtitle17[0]+=val17
                        isiSubtitle18[0]+=val18
                        isiSubtitle19[0]+=val19
                        isiSubtitle20[0]+=val20
                    }else if(tgglReport?.format("M")=="2"){
                        isiSubtitle1[1]+=val1
                        isiSubtitle2[1]+=val2
                        isiSubtitle3[1]+=val3
                        isiSubtitle4[1]+=val4
                        isiSubtitle5[1]+=val5
                        isiSubtitle6[1]+=val6
                        isiSubtitle7[1]+=val7
                        isiSubtitle8[1]+=val8
                        isiSubtitle9[1]+=val9
                        isiSubtitle10[1]+=val10
                        isiSubtitle11[1]+=val11
                        isiSubtitle12[1]+=val12
                        isiSubtitle13[1]+=val13
                        isiSubtitle14[1]+=val14
                        isiSubtitle15[1]+=val15
                        isiSubtitle16[1]+=val16
                        isiSubtitle17[1]+=val17
                        isiSubtitle18[1]+=val18
                        isiSubtitle19[1]+=val19
                        isiSubtitle20[1]+=val20
                    }else if(tgglReport?.format("M")=="3"){
                        isiSubtitle1[2]+=val1
                        isiSubtitle2[2]+=val2
                        isiSubtitle3[2]+=val3
                        isiSubtitle4[2]+=val4
                        isiSubtitle5[2]+=val5
                        isiSubtitle6[2]+=val6
                        isiSubtitle7[2]+=val7
                        isiSubtitle8[2]+=val8
                        isiSubtitle9[2]+=val9
                        isiSubtitle10[2]+=val10
                        isiSubtitle11[2]+=val11
                        isiSubtitle12[2]+=val12
                        isiSubtitle13[2]+=val13
                        isiSubtitle14[2]+=val14
                        isiSubtitle15[2]+=val15
                        isiSubtitle16[2]+=val16
                        isiSubtitle17[2]+=val17
                        isiSubtitle18[2]+=val18
                        isiSubtitle19[2]+=val19
                        isiSubtitle20[2]+=val20
                    }else if(tgglReport?.format("M")=="4"){
                        isiSubtitle1[3]+=val1
                        isiSubtitle2[3]+=val2
                        isiSubtitle3[3]+=val3
                        isiSubtitle4[3]+=val4
                        isiSubtitle5[3]+=val5
                        isiSubtitle6[3]+=val6
                        isiSubtitle7[3]+=val7
                        isiSubtitle8[3]+=val8
                        isiSubtitle9[3]+=val9
                        isiSubtitle10[3]+=val10
                        isiSubtitle11[3]+=val11
                        isiSubtitle12[3]+=val12
                        isiSubtitle13[3]+=val13
                        isiSubtitle14[3]+=val14
                        isiSubtitle15[3]+=val15
                        isiSubtitle16[3]+=val16
                        isiSubtitle17[3]+=val17
                        isiSubtitle18[3]+=val18
                        isiSubtitle19[3]+=val19
                        isiSubtitle20[3]+=val20
                    }else if(tgglReport?.format("M")=="5"){
                        isiSubtitle1[4]+=val1
                        isiSubtitle2[4]+=val2
                        isiSubtitle3[4]+=val3
                        isiSubtitle4[4]+=val4
                        isiSubtitle5[4]+=val5
                        isiSubtitle6[4]+=val6
                        isiSubtitle7[4]+=val7
                        isiSubtitle8[4]+=val8
                        isiSubtitle9[4]+=val9
                        isiSubtitle10[4]+=val10
                        isiSubtitle11[4]+=val11
                        isiSubtitle12[4]+=val12
                        isiSubtitle13[4]+=val13
                        isiSubtitle14[4]+=val14
                        isiSubtitle15[4]+=val15
                        isiSubtitle16[4]+=val16
                        isiSubtitle17[4]+=val17
                        isiSubtitle18[4]+=val18
                        isiSubtitle19[4]+=val19
                        isiSubtitle20[4]+=val20
                    }else if(tgglReport?.format("M")=="6"){
                        isiSubtitle1[5]+=val1
                        isiSubtitle2[5]+=val2
                        isiSubtitle3[5]+=val3
                        isiSubtitle4[5]+=val4
                        isiSubtitle5[5]+=val5
                        isiSubtitle6[5]+=val6
                        isiSubtitle7[5]+=val7
                        isiSubtitle8[5]+=val8
                        isiSubtitle9[5]+=val9
                        isiSubtitle10[5]+=val10
                        isiSubtitle11[5]+=val11
                        isiSubtitle12[5]+=val12
                        isiSubtitle13[5]+=val13
                        isiSubtitle14[5]+=val14
                        isiSubtitle15[5]+=val15
                        isiSubtitle16[5]+=val16
                        isiSubtitle17[5]+=val17
                        isiSubtitle18[5]+=val18
                        isiSubtitle19[5]+=val19
                        isiSubtitle20[5]+=val20
                    }else if(tgglReport?.format("M")=="7"){
                        isiSubtitle1[6]+=val1
                        isiSubtitle2[6]+=val2
                        isiSubtitle3[6]+=val3
                        isiSubtitle4[6]+=val4
                        isiSubtitle5[6]+=val5
                        isiSubtitle6[6]+=val6
                        isiSubtitle7[6]+=val7
                        isiSubtitle8[6]+=val8
                        isiSubtitle9[6]+=val9
                        isiSubtitle10[6]+=val10
                        isiSubtitle11[6]+=val11
                        isiSubtitle12[6]+=val12
                        isiSubtitle13[6]+=val13
                        isiSubtitle14[6]+=val14
                        isiSubtitle15[6]+=val15
                        isiSubtitle16[6]+=val16
                        isiSubtitle17[6]+=val17
                        isiSubtitle18[6]+=val18
                        isiSubtitle19[6]+=val19
                        isiSubtitle20[6]+=val20
                    }else if(tgglReport?.format("M")=="8"){
                        isiSubtitle1[7]+=val1
                        isiSubtitle2[7]+=val2
                        isiSubtitle3[7]+=val3
                        isiSubtitle4[7]+=val4
                        isiSubtitle5[7]+=val5
                        isiSubtitle6[7]+=val6
                        isiSubtitle7[7]+=val7
                        isiSubtitle8[7]+=val8
                        isiSubtitle9[7]+=val9
                        isiSubtitle10[7]+=val10
                        isiSubtitle11[7]+=val11
                        isiSubtitle12[7]+=val12
                        isiSubtitle13[7]+=val13
                        isiSubtitle14[7]+=val14
                        isiSubtitle15[7]+=val15
                        isiSubtitle16[7]+=val16
                        isiSubtitle17[7]+=val17
                        isiSubtitle18[7]+=val18
                        isiSubtitle19[7]+=val19
                        isiSubtitle20[7]+=val20
                    }else if(tgglReport?.format("M")=="9"){
                        isiSubtitle1[8]+=val1
                        isiSubtitle2[8]+=val2
                        isiSubtitle3[8]+=val3
                        isiSubtitle4[8]+=val4
                        isiSubtitle5[8]+=val5
                        isiSubtitle6[8]+=val6
                        isiSubtitle7[8]+=val7
                        isiSubtitle8[8]+=val8
                        isiSubtitle9[8]+=val9
                        isiSubtitle10[8]+=val10
                        isiSubtitle11[8]+=val11
                        isiSubtitle12[8]+=val12
                        isiSubtitle13[8]+=val13
                        isiSubtitle14[8]+=val14
                        isiSubtitle15[8]+=val15
                        isiSubtitle16[8]+=val16
                        isiSubtitle17[8]+=val17
                        isiSubtitle18[8]+=val18
                        isiSubtitle19[8]+=val19
                        isiSubtitle20[8]+=val20
                    }else if(tgglReport?.format("M")=="10"){
                        isiSubtitle1[9]+=val1
                        isiSubtitle2[9]+=val2
                        isiSubtitle3[9]+=val3
                        isiSubtitle4[9]+=val4
                        isiSubtitle5[9]+=val5
                        isiSubtitle6[9]+=val6
                        isiSubtitle7[9]+=val7
                        isiSubtitle8[9]+=val8
                        isiSubtitle9[9]+=val9
                        isiSubtitle10[9]+=val10
                        isiSubtitle11[9]+=val11
                        isiSubtitle12[9]+=val12
                        isiSubtitle13[9]+=val13
                        isiSubtitle14[9]+=val14
                        isiSubtitle15[9]+=val15
                        isiSubtitle16[9]+=val16
                        isiSubtitle17[9]+=val17
                        isiSubtitle18[9]+=val18
                        isiSubtitle19[9]+=val19
                        isiSubtitle20[9]+=val20
                    }else if(tgglReport?.format("M")=="11"){
                        isiSubtitle1[10]+=val1
                        isiSubtitle2[10]+=val2
                        isiSubtitle3[10]+=val3
                        isiSubtitle4[10]+=val4
                        isiSubtitle5[10]+=val5
                        isiSubtitle6[10]+=val6
                        isiSubtitle7[10]+=val7
                        isiSubtitle8[10]+=val8
                        isiSubtitle9[10]+=val9
                        isiSubtitle10[10]+=val10
                        isiSubtitle11[10]+=val11
                        isiSubtitle12[10]+=val12
                        isiSubtitle13[10]+=val13
                        isiSubtitle14[10]+=val14
                        isiSubtitle15[10]+=val15
                        isiSubtitle16[10]+=val16
                        isiSubtitle17[10]+=val17
                        isiSubtitle18[10]+=val18
                        isiSubtitle19[10]+=val19
                        isiSubtitle20[10]+=val20
                    }else if(tgglReport?.format("M")=="12"){
                        isiSubtitle1[11]+=val1
                        isiSubtitle2[11]+=val2
                        isiSubtitle3[11]+=val3
                        isiSubtitle4[11]+=val4
                        isiSubtitle5[11]+=val5
                        isiSubtitle6[11]+=val6
                        isiSubtitle7[11]+=val7
                        isiSubtitle8[11]+=val8
                        isiSubtitle9[11]+=val9
                        isiSubtitle10[11]+=val10
                        isiSubtitle11[11]+=val11
                        isiSubtitle12[11]+=val12
                        isiSubtitle13[11]+=val13
                        isiSubtitle14[11]+=val14
                        isiSubtitle15[11]+=val15
                        isiSubtitle16[11]+=val16
                        isiSubtitle17[11]+=val17
                        isiSubtitle18[11]+=val18
                        isiSubtitle19[11]+=val19
                        isiSubtitle20[11]+=val20
                    }
                }

            }catch(Exception e){
                throw new java.lang.Exception(e.getMessage());
            }

            int jmlLooping9=0

            int count = 1
            (0..9).each{
                def data = [:]
                def periode = listBulan.get(Integer.parseInt(params.bulan1))+" "+ params.tahun  +" - "+ listBulan.get(Integer.parseInt(params.bulan2))+" "+ params.tahun

                data.put("workshop", CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)//.toString()
                data.put("periode", periode)
                data.put("no", count++)
                data.put("title", title.get(it))
                data.put("subtitle1", child1.get(it))
                data.put("subtitle2", child2.get(it))

                double total2=0.0;
                double total3=0.0;

                for(int i=0; i<isiTitle.size(); i++){//jan - des

                    if(isiSubtitle1[i]==0){
                        data.put(dataSubtitle1[i], "-")
                    }else{
                        data.put(dataSubtitle1[i], isiSubtitle1[i])
                    }
                    if(isiSubtitle2[i]==0){
                        data.put(dataSubtitle2[i], "-")
                    }
                    else{
                        data.put(dataSubtitle2[i], isiSubtitle2[i])
                    }

                    try{
                        total2+=isiSubtitle1[i];
                        total3+=isiSubtitle2[i];
                        if(isiSubtitle1[i]/isiSubtitle2[i]!=0){
                            data.put(dataTitle[i], ""+(Math.round((isiSubtitle1[i]/isiSubtitle2[i])*100.doubleValue()))+"%")
                        } else{ data.put(dataTitle[i], "-") }
                    }catch (ArithmeticException aritmatik1){
                        data.put(dataTitle[i], "-")//jan1
                    }

                    try{
                        data.put("tot2", total2)
                    }catch (ArithmeticException aritmatik2){
                        data.put("tot2", "-")
                    }
                    try{
                        data.put("tot3", total3)
                    }catch (ArithmeticException aritmatik3){
                        data.put("tot3", "-")
                    }
                    try{
                        data.put("tot1", ""+(Math.round((total2/total3)*100.doubleValue())+"%"))
                    }catch (ArithmeticException aritmatik4){
                        data.put("tot1", "-")
                    }
                }//end loop date
                jmlLooping9++
                if(jmlLooping9==1){
                    isiSubtitle1=isiSubtitle3
                    isiSubtitle2=isiSubtitle4
                }else if(jmlLooping9==2){
                    isiSubtitle1=isiSubtitle5
                    isiSubtitle2=isiSubtitle6
                }else if(jmlLooping9==3){
                    isiSubtitle1=isiSubtitle7
                    isiSubtitle2=isiSubtitle8
                }else if(jmlLooping9==4){
                    isiSubtitle1=isiSubtitle9
                    isiSubtitle2=isiSubtitle10
                }else if(jmlLooping9==5){
                    isiSubtitle1=isiSubtitle11
                    isiSubtitle2=isiSubtitle12
                }else if(jmlLooping9==6){
                    isiSubtitle1=isiSubtitle13
                    isiSubtitle2=isiSubtitle14
                }else if(jmlLooping9==7){
                    isiSubtitle1=isiSubtitle15
                    isiSubtitle2=isiSubtitle16
                }else if(jmlLooping9==8){
                    isiSubtitle1=isiSubtitle17
                    isiSubtitle2=isiSubtitle18
                }else if(jmlLooping9==9){
                    isiSubtitle1=isiSubtitle19
                    isiSubtitle2=isiSubtitle20
                }
                reportData.add(data)
            }//9 data
        }//end report 1
        else if(params.namaReport=="02"){
            int count = 0
            int totPlan = 0, totSent = 0, totRecv = 0
            int tPers1 = 0, tPers2 = 0
            Date tgl = strDate - 1

            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglDM",tgl)
                    lt("t201TglDM",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }


                def jmlSent = 0
                listPlan.each {
                    def cariSent =
                            Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminder(tgl, tgl + 1,"0",
                                    JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","Mail"),it?.companyDealer,it?.customerVehicle,Reminder.findByT201TglDMAndStaDel(tgl,"0"))

                    if(cariSent){
                        jmlSent +=1
                    }
                }

                def jmlReceived = 0
                listPlan.each {
                    def cariReceive =
                            Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndCompanyDealerAndCustomerVehicleAndReminder(tgl , tgl + 1, "0", it?.companyDealer, it?.customerVehicle, it)
                    if(cariReceive?.t202StaInvitation?.charAt(5) == "1"){
                        jmlReceived +=1
                    }
                }

                def plan = listPlan.size() ? listPlan.size() : 0
                def persSent = jmlSent!=0 ? Math.round(jmlSent/plan*100.doubleValue()) : 0;
                def persReceive = jmlReceived!=0 ? Math.round(jmlReceived/jmlSent*100.doubleValue()) : 0;
                if(plan>0){
                    count++
                    def data = [:]
                    data.put("title","MRS - KPI DIRECT MAIL")
                    data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("header", "DIRECT MAIL")
                    data.put("subheader1", "Plan")
                    data.put("subheader2", "Sent")
                    data.put("subheader3", "Received")
                    data.put("subheader4", "% Sent")
                    data.put("subheader5", "% Receive")
                    data.put("no", count)
                    data.put("date", tgl.format("dd-MMM-yyyy"))
                    try {
                        data.put("value1", plan)
                        data.put("value2", jmlSent)
                        data.put("value3", jmlReceived)
                        data.put("value4", persSent+"%")
                        data.put("value5", persReceive+"%")
                    }catch (Exception e){
                        data.put("value1", "-")
                        data.put("value2", "-")
                        data.put("value3", "-")
                        data.put("value4", "- %")
                        data.put("value5", "- %")
                    }
                    totPlan+=plan
                    totSent+=jmlSent
                    totRecv+=jmlReceived

                    tPers1=totSent !=0 ? Math.round(totSent/totPlan*100) : 0
                    tPers2=totSent !=0 ? Math.round(totRecv/totSent*100) : 0

                    try{
                        data.put("total1", totPlan)
                        data.put("total2", totSent)
                        data.put("total3", totRecv)
                        data.put("total4", tPers1+"%")
                        data.put("total5", tPers2+"%")
                    }catch (Exception e){
                        data.put("total1", "-")
                        data.put("total2", "-")
                        data.put("total3", "-")
                        data.put("total4", "- %")
                        data.put("total5", "- %")
                    }
                    reportData.add(data)
                }
            }
            if(reportData.size()<1){
                count++;
                def data = [:]
                data.put("title","MRS - KPI DIRECT MAIL")
                data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                data.put("periode", periode)
                data.put("header", "DIRECT MAIL")
                data.put("subheader1", "Plan")
                data.put("subheader2", "Sent")
                data.put("subheader3", "Received")
                data.put("subheader4", "% Sent")
                data.put("subheader5", "% Receive")
                data.put("no", count)
                data.put("date", tgl.format("dd-MMM-yyyy"))
                data.put("value1", "-")
                data.put("value2", "-")
                data.put("value3", "-")
                data.put("value4", "- %")
                data.put("value5", "- %")
                data.put("total1", "-")
                data.put("total2", "-")
                data.put("total3", "-")
                data.put("total4", "- %")
                data.put("total5", "- %")
                reportData.add(data)
            }
        }

        else if(params.namaReport=="03"){
            int count = 0
            int totPlan = 0, totSent = 0, totRecv = 0, totSms = 0
            int tPers1 = 0, tPers2 = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglSMS",tgl)
                    lt("t201TglSMS",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params?.workshop as Long))
                }

                def jmlSent = 0
                def jmlSms = 0

                listPlan.each {
                    if(it?.t201TglSMS > it?.t201TglDM){
                        jmlSms +=1
                    }

                    def cariSent =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminder(tgl, tgl + 1,"0",
                            JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","SMS"),it?.companyDealer,it?.customerVehicle,it)
                    if(cariSent){
                        jmlSent +=1
                    }
                }


                def jmlReceived = 0
                listPlan.each {
                    def cariReceive =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndCompanyDealerAndCustomerVehicleAndReminder(tgl , tgl + 1, "0", it?.companyDealer, it?.customerVehicle, it)

                    if(cariReceive?.t202StaInvitation?.charAt(5) == "1"){
                        jmlReceived +=1
                    }
                }

                def plan = listPlan?.size() ? listPlan?.size() : 0
//                def persSent = jmlSent!=0 ? Math.round((jmlSent+jmlSms)/plan*100.doubleValue()) : 0;
                def persSent = jmlSent!=0 ? Math.round((jmlSent/jmlSms)*100.doubleValue()) : 0;
                def persReceive = jmlSent!=0 || jmlSms!=0 ? Math.round(jmlReceived/(jmlSent+jmlSms)*100.doubleValue()) : 0;
                if(plan>0){
                    count++
                    def data = [:]
                    data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("no", count)
                    data.put("date", tgl.format("dd-MMM-yyyy"))
                    data.put("plan", plan)
                    data.put("sent", jmlSent)
                    data.put("sms", jmlSms)
                    data.put("receive", jmlReceived)
                    data.put("persensent", persSent+"%")
                    data.put("persenreceive", persReceive+"%")

                    totPlan+=plan
                    totSent+=jmlSent
                    totSms+=jmlSms
                    totRecv+=jmlReceived
                    tPers1=totPlan !=0 ? Math.round((totSent+totSms)/totPlan*100.doubleValue()) : 0
                    tPers2=totSent !=0 || totSms!=0 ? Math.round(totRecv/(totSent+totSms)*100.doubleValue()) : 0

                    data.put("totalplan", totPlan)
                    data.put("totalsent", totSent)
                    data.put("totalsms", totSms)
                    data.put("totalreceive", totRecv)
                    data.put("totalpersensent", tPers1+" %")
                    data.put("totalpersenreceive", tPers2+" %")
                    reportData.add(data)
                }
            }
            if(reportData?.size()<1){
                count++
                def data = [:]
                data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                data.put("periode", periode)
                data.put("no", count)
                data.put("date", tgl.format("dd-MMM-yyyy"))
                data.put("plan", "-")
                data.put("sent", "-")
                data.put("sms", "-")
                data.put("receive", "-")
                data.put("persensent", "-")
                data.put("persenreceive", "-")
                data.put("totalplan", "-")
                data.put("totalsent", "-")
                data.put("totalsms", "-")
                data.put("totalreceive", "-")
                data.put("totalpersensent", "-")
                data.put("totalpersenreceive", "-")
                reportData.add(data)
            }
        }
        else if(params.namaReport=="04"){
            int count = 0
            int totPlan = 0, totAct = 0, totConn = 0
            double tPers1 = 0, tPers2 = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglCall",tgl)
                    lt("t201TglCall",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                def jmlAct = 0
                listPlan.each {
                    def cariAct =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminder(tgl, tgl + 1,"0",
                            JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","Phone Call"),it?.companyDealer,it?.customerVehicle,it)

                    if(cariAct){
                        jmlAct +=1
                    }
                }

                def jmlConnected = 0
                listPlan.each {
                    def cariConn =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndCompanyDealerAndCustomerVehicleAndReminder(tgl , tgl + 1, "0", it?.companyDealer, it?.customerVehicle, it)

                    if(cariConn?.t202StaInvitation?.charAt(0) == "1"){
                        jmlConnected +=1
                    }
                }


                def plan = listPlan.size() ? listPlan.size() : 0
                def persSent = jmlAct!=0 ? Math.round(jmlAct/plan*100.doubleValue()) : 0;
                def persReceive = jmlConnected!=0 ? Math.round(jmlConnected/jmlAct*100.doubleValue()) : 0;

                if(plan>0){
                    count++
                    def data = [:]
                    data.put("title","MRS - KPI CALL")
                    data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("header", "CALL")
                    data.put("subheader1", "Plan")
                    data.put("subheader2", "Actual")
                    data.put("subheader3", "Connected")
                    data.put("subheader4", "% Call")
                    data.put("subheader5", "% Connected")
                    data.put("no", count)
                    data.put("date", tgl.format("dd-MMM-yyyy"))
                    data.put("value1", plan)
                    data.put("value2", jmlAct)
                    data.put("value3", jmlConnected)
                    data.put("value4", persSent+"%")
                    data.put("value5", persReceive+"%")

                    totPlan+=plan
                    totAct+=jmlAct
                    totConn+=jmlConnected

                    tPers1=totAct !=0 ? Math.round(totAct/totPlan*100.doubleValue()) : 0
                    tPers2=totAct !=0 ? Math.round(totConn/totAct*100.doubleValue()) : 0

                    data.put("total1", totPlan)
                    data.put("total2", totAct)
                    data.put("total3", totConn)
                    data.put("total4", tPers1+"%")
                    data.put("total5", tPers2+"%")

                    reportData.add(data)
                }
            }
            if(reportData.size()<1){
                count++;
                def data = [:]
                data.put("title","MRS - KPI CALL")
                data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                data.put("periode", periode)
                data.put("header", "CALL")
                data.put("subheader1", "Plan")
                data.put("subheader2", "Actual")
                data.put("subheader3", "Connected")
                data.put("subheader4", "% Call")
                data.put("subheader5", "% Connected")
                data.put("no", count)
                data.put("date", tgl.format("dd-MMM-yyyy"))
                data.put("value1", "-")
                data.put("value2", "-")
                data.put("value3", "-")
                data.put("value4", "-")
                data.put("value5", "-")

                data.put("total1", "-")
                data.put("total2", "-")
                data.put("total3", "-")
                data.put("total4", "-")
                data.put("total5", "-")

                reportData.add(data)
            }
        }
        else if(params.namaReport=="05"){
            int count = 0
            int totCon = 0, totAf = 0, totAt = 0, totT = 0
            double tPers1 = 0, tPers2 = 0, tPers3 = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglCall",tgl)
                    lt("t201TglCall",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }
                def jmlConnected = 0
                listPlan.each {
                    def cariConn =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndCompanyDealerAndCustomerVehicleAndReminderAndJenisInvitation(tgl , tgl + 1, "0", CompanyDealer.findById(params.workshop as Long), it?.customerVehicle, it ,JenisInvitation.findById(4) )

                    if(cariConn?.t202StaInvitation?.charAt(0) == "1"){
                        jmlConnected +=1
                    }
                }

                def jmlAtcall = 0
                def jmlAfcall = 0
                listPlan.each {
                    def cariAt =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminderAndT202StaLangsungAppointment(tgl, tgl + 1,"0",
                            JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","Phone Call"),it?.companyDealer,it?.customerVehicle,it, "1")

                    if(cariAt){
                        jmlAtcall +=1
                    }else{
                        def cariAf =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminderAndT202StaLangsungAppointment(tgl, tgl + 1,"0",
                                JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","Phone Call"),it?.companyDealer,it?.customerVehicle,it, "0")
                        if(cariAf){
                            def App = Appointment.findByT301TglJamAppGreaterThanEqualsAndStaDel(tgl, "0")
                            if(App?.t301TglJamApp > cariAf?.t202TglJamKirim){
                                jmlAfcall +=1
                            }
                        }
                    }
                }

                def total = jmlAtcall + jmlAfcall
                def persAtcall = jmlConnected!=0 ? Math.round(jmlAtcall/jmlConnected*100.doubleValue()) : 0
                def persAfcall = jmlConnected!=0 ? Math.round(jmlAfcall/jmlConnected*100.doubleValue()) : 0
                def persTot = jmlConnected!=0 ? Math.round(total/jmlConnected*100.doubleValue()) : 0

                if(jmlConnected!=0 || jmlAtcall!=0 || jmlAfcall!=0 || total!=0){
                    count++
                    def data = [:]
                    data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("no", count)
                    data.put("date", tgl.format("dd/MM/yyyy"))
                    data.put("conncall", jmlConnected ? jmlConnected:0)
                    data.put("atcall", jmlAtcall)
                    data.put("afcall", jmlAfcall)
                    data.put("tot", total)
                    data.put("persenatcall", persAtcall+" %")
                    data.put("persenafcall", persAfcall+" %")
                    data.put("persentot", persTot+" %")

                    totCon+=jmlConnected
                    totAt+=jmlAtcall
                    totAf+=jmlAfcall
                    totT+=total

                    tPers1=totCon !=0 ? Math.round(totAt/totCon*100.doubleValue()) : 0
                    tPers2=totCon !=0 ? Math.round(totAf/totCon*100.doubleValue()) : 0
                    tPers3=totCon !=0 ? Math.round(totT/totCon*100.doubleValue()) : 0

                    data.put("totalconcall", totCon)
                    data.put("totalatcall", totAt)
                    data.put("totalafcall", totAf)
                    data.put("totaltot", totT)
                    data.put("totalpersenatcall", tPers1+" %")
                    data.put("totalpersenafcall", tPers2+" %")
                    data.put("totalpersentot", tPers3+" %")

                    reportData.add(data)
                }
            }
            if(reportData.size()<1){
                count++
                def data = [:]
                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("no", count)
                data.put("date", tgl.format("dd/MM/yyyy"))
                data.put("conncall", "-")
                data.put("atcall", "-")
                data.put("afcall", "-")
                data.put("tot", "-")
                data.put("persenatcall", "-")
                data.put("persenafcall", "-")
                data.put("persentot", "-")
                data.put("totalconcall", "-")
                data.put("totalatcall", "-")
                data.put("totalafcall", "-")
                data.put("totaltot", "-")
                data.put("totalpersenatcall", "-")
                data.put("totalpersenafcall", "-")
                data.put("totalpersentot", "-")
                reportData.add(data)
            }
        }
        else if(params.namaReport=="06"){

            def data = [:]
            def listBulann = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
            def arr = Integer.parseInt(params.bulan1)
            def bln1 = Integer.parseInt(params.bulan1)
            def bln2 = Integer.parseInt(params.bulan2)

            def periode = listBulann.get(Integer.parseInt(params.bulan1)) +" - "+ listBulann.get(Integer.parseInt(params.bulan2))+" "+ params.tahun
            int selisih =bln2-bln1

            int PerCusDtg=0,PerKontMRS=0,PerKontSBEMrs=0,PerMRSEntry=0,PerSBIRet=0
            def T110k=0, T1020k=0, T2030k=0, T3040k=0, T4050k=0,T5060k=0,T6070k=0,T7080k=0,T8090k=0,T90100k=0

            def mount = ['jan', 'feb', 'mar', 'apr', 'mei', 'jun', 'jul', 'agu', 'sep', 'okt', 'nov', 'des']
            int t1=0,t2=0,t3=0,t4=0,t5=0,t6=0,t7=0,t8=0,t9=0,t10=0,t11=0,t12=0,t13=0,t14=0,t15=0

            (0..selisih).each {
                int JumCusDtg=0,JumCusUdg=0,JumAppMrs=0,JumTotApp=0,JumSBEMrs=0,JumTotSBE=0,JumSBEGRMrs=0,JumTotEntry=0,
                    JumCusService=0,JumUnitKirim=0

                bln1+=1
                String sdate1 = "01-"+bln1+"-"+params.tahun
                Date date1 = df2.parse(sdate1)
                cal1.setTime(date1)
                def maxD = cal1.getActualMaximum(cal1.DAY_OF_MONTH)
                String tgl2 = maxD+"-"+bln1+"-"+params.tahun+" 23:58:58"
                def dateawal = new Date().parse("dd-MM-yyyy",sdate1.toString())
                def dateakhir = new Date().parse("dd-MM-yyyy HH:mm:ss",tgl2.toString())


                final session = sessionFactory.currentSession
                String query =
                        "SELECT " +
                                "CUST_DTG_DIUNDANG , " +
                                "CUST_DIUNDANG , " +
                                "APP_MRS, " +
                                "TOTAL_APP, " +
                                "SBE_MRS," +
                                "TOTAL_UNIT_SBE, " +
                                "SBE_GR_MRS, " +
                                "TOTAL_UNIT_ENTRY," +
                                "SBI, " +
                                "JUMLAH_UNIT_TERKIRIM, " +
                                "SBE_1K_10K, " +
                                "SBE_10K_20K, " +
                                "SBE_20K_30K, " +
                                "SBE_30K_40K, " +
                                "SBE_40K_50K, " +
                                "SBE_50K_60K, " +
                                "SBE_60K_70K, " +
                                "SBE_70K_80K, " +
                                "SBE_80K_90K, " +
                                "SBE_90K_100K " +
                                "FROM " +
                                "R002_KPI_RESULT " +
                                "WHERE TGL_LAPORAN >= ? " +
                                "AND TGL_LAPORAN < ? " +
                                "AND R002_M011_ID = ? ";

                try {
                    final sqlQuery = session.createSQLQuery(query);
                    sqlQuery.setParameter(0,dateawal);
                    sqlQuery.setParameter(1,dateakhir);
                    sqlQuery.setParameter(2,CompanyDealer?.findById(params.workshop as Long)?.id);
                    final queryResults = sqlQuery.with {
                        list()
                    }

                    queryResults.collect { resultRow ->
                        JumCusDtg+=(resultRow[0]?resultRow[0].toInteger():0)
                        JumCusUdg+=(resultRow[1]?resultRow[1].toInteger():0)
                        JumAppMrs+=(resultRow[2]?resultRow[2].toInteger():0)
                        JumTotApp+=(resultRow[3]?resultRow[3].toInteger():0)
                        JumSBEMrs+=(resultRow[4]?resultRow[4].toInteger():0)
                        JumTotSBE+=(resultRow[5]?resultRow[5].toInteger():0)
                        JumSBEGRMrs+=(resultRow[6]?resultRow[6].toInteger():0)
                        JumTotEntry+=(resultRow[7]?resultRow[7].toInteger():0)
                        JumCusService+=(resultRow[8]?resultRow[8].toInteger():0)
                        JumUnitKirim+=(resultRow[9]?resultRow[9].toInteger():0)
                        T110k+=(resultRow[10]?resultRow[10].toInteger():0)
                        T1020k+=(resultRow[11]?resultRow[11].toInteger():0)
                        T2030k+=(resultRow[12]?resultRow[12].toInteger():0)
                        T3040k+=(resultRow[13]?resultRow[13].toInteger():0)
                        T4050k+=(resultRow[14]?resultRow[14].toInteger():0)
                        T5060k+=(resultRow[15]?resultRow[15].toInteger():0)
                        T6070k+=(resultRow[16]?resultRow[16].toInteger():0)
                        T7080k+=(resultRow[17]?resultRow[17].toInteger():0)
                        T8090k+=(resultRow[18]?resultRow[18].toInteger():0)
                        T90100k+=(resultRow[19]?resultRow[19].toInteger():0)
                    }

                }catch(Exception e){
                    throw new java.lang.Exception(e.getMessage());
                }

                PerCusDtg    = JumCusDtg!=0 && JumCusUdg!=0 ? Math.round(JumCusDtg/JumCusUdg*100.doubleValue()):0
                PerKontMRS   = JumAppMrs!=0 && JumTotApp!=0 ? Math.round(JumAppMrs/JumTotApp*100.doubleValue()):0
                PerKontSBEMrs= JumSBEMrs!=0 && JumTotSBE!=0 ? Math.round(JumSBEMrs/JumTotSBE*100.doubleValue()):0
                PerMRSEntry  = JumSBEGRMrs!=0 && JumTotEntry!=0 ? Math.round(JumSBEMrs/JumTotEntry*100.doubleValue()):0
                PerSBIRet    = JumCusService!=0 && JumUnitKirim!=0 ? Math.round(JumCusService/JumUnitKirim*100.doubleValue()):0

                data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                data.put("periode", periode)

                data.put(mount[arr]+"1",PerCusDtg+"%");t1+=PerCusDtg
                data.put(mount[arr]+"2",JumCusDtg);t2+=JumCusDtg
                data.put(mount[arr]+"3",JumCusUdg);t3+=JumCusUdg
                data.put(mount[arr]+"4",PerKontMRS+" %");t4+=PerKontMRS
                data.put(mount[arr]+"5",JumAppMrs);t5+=JumAppMrs
                data.put(mount[arr]+"6",JumTotApp);t6+=JumTotApp
                data.put(mount[arr]+"7",PerKontSBEMrs+"%");t7+=PerKontSBEMrs
                data.put(mount[arr]+"8",JumSBEMrs);t8+=JumSBEMrs
                data.put(mount[arr]+"9",JumTotSBE);t9+=JumTotSBE
                data.put(mount[arr]+"10",PerMRSEntry+"%");t10+=PerMRSEntry
                data.put(mount[arr]+"11",JumSBEGRMrs);t11+=JumSBEGRMrs
                data.put(mount[arr]+"12",JumTotEntry);t12+=JumTotEntry
                data.put(mount[arr]+"13",PerSBIRet+"%");t13+=PerSBIRet
                data.put(mount[arr]+"14",JumCusService);t14+=JumCusService
                data.put(mount[arr]+"15",JumUnitKirim);t15+=JumUnitKirim
                arr++
            }

            def T1  = t1 !=0 ? Math.round(t1/12.doubleValue()) : 0
            def T4  = t4 !=0 ? Math.round(t4/12.doubleValue()) : 0
            def T7  = t7 !=0 ? Math.round(t7/12.doubleValue()) : 0
            def T10 = t10 !=0 ? Math.round(t10/12.doubleValue()) : 0
            def T13 = t13 !=0 ? Math.round(t13/12.doubleValue()) : 0

            data.put("tot1", T1+"%")
            data.put("tot2", t2)
            data.put("tot3", t3)
            data.put("tot4", T4+"%")
            data.put("tot5", t5)
            data.put("tot6", t6)
            data.put("tot7", T7+"%")
            data.put("tot8", t8)
            data.put("tot9", t9)
            data.put("tot10", T10+"%")
            data.put("tot11", t11)
            data.put("tot12", t12)
            data.put("tot13", T13+"%")
            data.put("tot14", t14)
            data.put("tot15", t15)

            data.put("tot1k",Math.round(T110k.doubleValue())+"%")
            data.put("tot10k",Math.round(T1020k.doubleValue())+"%")
            data.put("tot20k",Math.round(T2030k.doubleValue())+"%")
            data.put("tot30k",Math.round(T3040k.doubleValue())+"%")
            data.put("tot40k",Math.round(T4050k.doubleValue())+"%")
            data.put("tot50k",Math.round(T5060k.doubleValue())+"%")
            data.put("tot60k",Math.round(T6070k.doubleValue())+"%")
            data.put("tot70k",Math.round(T7080k.doubleValue())+"%")
            data.put("tot80k",Math.round(T8090k.doubleValue())+"%")
            data.put("tot90k",Math.round(T90100k.doubleValue())+"%")

            reportData.add(data)
        }
        else if(params.namaReport=="07"){//stadel, divisi, nama user
            int count = 0
            int totApp = 0, totAf = 0, totAt = 0, totT = 0
            double tPers1 = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")

            (0..selisih).each {
                tgl+=1
                def listApp = Appointment.createCriteria().list {
                    eq("staSave","0");
                    eq("staDel","0");
                    ge("t301TglJamApp",tgl)
                    lt("t301TglJamApp",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                def jmlAtcall = 0
                def jmlAfcall = 0
                listApp.each {
                    def cariAt =  Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndT202StaLangsungAppointment(tgl, tgl + 1,"0",
                            JenisInvitation.findByM204StaDelAndM204JenisInvitationIlike("0","%CALL%"),it?.companyDealer,it?.customerVehicle, "1")

                    if(cariAt){
                        jmlAtcall +=1
                    }else{
                        Date tgglCari = it?.t301TglJamApp
                        def cariAf =  Invitation.findByT202TglJamKirimLessThanAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndT202StaLangsungAppointment(tgglCari?.clearTime(),"0",
                                JenisInvitation.findByM204StaDelAndM204JenisInvitationIlike("0","%CALL%"),it?.companyDealer,it?.customerVehicle, "0")
                        if(cariAf){
                            jmlAfcall +=1
                        }
                    }
                }

                def tApp = listApp?.size() ? listApp?.size() : 0
                def total = jmlAtcall + jmlAfcall
                def persTot = tApp!=0 ? Math.round(total/tApp*100.doubleValue()) : 0

                if(tApp>0){
                    def data = [:]

                    count++
                    data.put("judul", "MRS - APPOINTMENT CONTRIBUTION")
                    data.put("workshop", CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("title1", "TOTAL APPOINTMENT")
                    data.put("title2", "At Call")
                    data.put("title3", "After Call")
                    data.put("title4", "Total")
                    data.put("no", count)
                    data.put("date", tgl.format("dd-MMM-yyyy"))
                    data.put("val1", tApp)
                    data.put("val2", jmlAtcall)
                    data.put("val3", jmlAfcall)
                    data.put("val4", total)
                    data.put("persen", persTot+" %")

                    totApp+=tApp
                    totAt+=jmlAtcall
                    totAf+=jmlAfcall
                    totT+=total

                    tPers1=totT !=0 ? Math.round(totT/totApp*100.doubleValue()) : 0

                    data.put("totalval1", totApp)
                    data.put("totalval2", totAt)
                    data.put("totalval3", totAf)
                    data.put("totalval4", totT)
                    data.put("totalpersen", tPers1+" %")

                    reportData.add(data)
                }
            }
        }

        else if(params.namaReport=="08"){
            int count = 0
            int totSB = 0, totMRSC = 0
            double tPers1 = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")

            (0..selisih).each {
                tgl+=1
                def listRec = Reception.createCriteria().list {
                    ge("t401TanggalWO",tgl)
                    lt("t401TanggalWO",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    eq("staSave","0")
                    eq("staDel","0")
                }

                def unit=0
                def unit2=0
                listRec.each {
                    def Job=JobRCP.findByReceptionAndStaDel(it,"0")
                    if(Job?.operation?.kategoriJob?.m055KategoriJob == "SBE"){
                        unit+=1
                    }
                    def invitation = Invitation.findByT202TglJamKirimGreaterThanEqualsAndT202TglJamKirimLessThanAndCustomerVehicleAndT202StaDel(it.t401TanggalWO,it.t401TanggalWO + 1,it?.historyCustomerVehicle?.customerVehicle,"0")
                    if(invitation){
                        unit2+=1
                    }
                }
                if(listRec.size()>0){
                    def data = [:]
                    count++
                    def persTot = unit!=0 ? Math.round(unit2/unit*100.doubleValue()) : 0
                    data.put("judul","MRS - SBE CONTRIBUTION")
                    data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("no", count)
                    data.put("date", tgl.format("dd/MM/yyyy"))
                    try {
                        data.put("sbunit", unit)
                        data.put("mrscontribution",unit2)
                        data.put("persen", persTot)
                    }catch (Exception e){
                        data.put("sbunit","-")
                        data.put("mrscontribution","-")
                        data.put("persen", "-")
                    }
                    totSB+=unit
                    totMRSC+=unit2

                    tPers1=totSB !=0 ? Math.round(totMRSC/totSB*100.doubleValue()) : 0

                    try{
                        data.put("totalsbunit", totSB)
                        data.put("totalmrscontribution", totMRSC)
                        data.put("totalpersen", tPers1)
                    }catch (Exception e){
                        data.put("totalsbunit","-")
                        data.put("totalmrscontribution","-")
                        data.put("totalpersen", "-")
                    }

                    reportData.add(data)
                }
            }
            if(reportData.size()==0){
                def data = [:]
                data.put("judul","MRS - SBE CONTRIBUTION")
                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("no", count)
                data.put("date", tgl.format("dd/MM/yyyy"))
                data.put("sbunit","-")
                data.put("mrscontribution","-")
                data.put("persen", "-")
                data.put("totalsbunit","-")
                data.put("totalmrscontribution","-")
                data.put("totalpersen", "-")
                reportData.add(data)
            }
        }
        else if(params.namaReport=="09"){
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")
            def no = 0
            double tPers1 = 0
            int totSB = 0, totGR = 0, totUnit = 0
            (0..selisih).each {
                no+=1
                tgl+=1

                def unitSB=0
                def unitGR=0
                def listRec = Reception.createCriteria().list {
                    ge("t401TanggalWO",tgl)
                    lt("t401TanggalWO",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    eq("staSave","0")
                    eq("staDel","0")
                }
                listRec.each {
                    def invitation = Invitation.findByT202TglJamKirimGreaterThanEqualsAndT202TglJamKirimLessThanAndCustomerVehicleAndT202StaDel(it.t401TanggalWO,it.t401TanggalWO + 1,it?.historyCustomerVehicle?.customerVehicle,"0")
                    if(invitation){
                        def Job=JobRCP.findByReceptionAndStaDel(it,"0")
                        if(Job?.operation?.kategoriJob?.m055KategoriJob.contains("SB")){
                            unitSB+=1
                        }
                        if(Job?.operation?.kategoriJob?.m055KategoriJob.contains("GR")){
                            unitGR+=1
                        }
                    }
                }
                if(listRec.size()>0){
                    def data = [:]
                    data.put("judul","MRS - UNIT ENTRY CONTRIBUTION")
                    data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("title1", "TOTAL UNIT ENTRY")
                    data.put("title2", "SB")
                    data.put("title3", "GR")
                    data.put("title4", "Total")
                    data.put("no", no)
                    data.put("date",tgl.format("dd/MM/yyyy"))
                    data.put("val1",listRec.size())
                    data.put("val2", unitSB)
                    data.put("val3", unitGR)
                    data.put("val4", unitSB+unitGR)
                    def persTot = (unitSB+unitGR)!=0 ? Math.round((unitSB+unitGR)/listRec.size()*100.doubleValue()) : 0
                    data.put("persen", tPers1 + "%")
                    totUnit += listRec.size()
                    data.put("totalval1", totUnit)
                    totSB += unitSB
                    totGR += unitGR
                    data.put("totalval2", totSB)
                    data.put("totalval3", totGR)
                    data.put("totalval4", (totSB+totGR))
                    tPers1 = (totSB+totGR) !=0 ? Math.round((totGR+totSB)/ totUnit * 100.doubleValue() ) : 0
                    data.put("totalpersen", tPers1+"%")
                    reportData.add(data)
                }
            }
            if(reportData.size()==0){
                def data = [:]
                data.put("date", tgl.format("dd/MM/yyyy"))
                data.put("judul","MRS - UNIT ENTRY CONTRIBUTION")
                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("title1", "TOTAL UNIT ENTRY")
                data.put("title2", "SB")
                data.put("title3", "GR")
                data.put("title4", "Total")
                data.put("no", "1")
                data.put("val1","-")
                data.put("val2", "-")
                data.put("val3", "-")
                data.put("val4", "-")
                data.put("persen", "0%")
                data.put("totalval1", "-")
                data.put("totalval2", "-")
                data.put("totalval3", "-")
                data.put("totalval4", "-")
                data.put("totalpersen", "0%")
                reportData.add(data)
            }
        }
        else if(params.namaReport=="10"){

            int count = 0, tDM = 0, tSMS = 0, tCALL = 0, tTotP = 0, tAdm = 0, tAsms = 0, tAcall = 0, tTotA = 0
            int tSdm = 0, tSsms = 0, tScall = 0, tApat = 0, tApaf = 0, tTotap = 0
            int tPers1 = 0, tPers2 = 0, tPers3 = 0, tPers4 = 0, tPers5 = 0, tPers6 = 0, tPers7 = 0, tPers8 = 0, tPers9 = 0, tPers10 = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")

            (0..selisih).each {
                tgl+=1
                def jumData = 0
                def val1 = 0,val2 = 0,val3 = 0,val4 = 0,val5 = 0,val6 = 0,val7 = 0,val8 = 0,val9 = 0,val10 = 0,val11 = 0,val12 = 0,val13 = 0,val14 = 0,val15 = 0,val16 = 0;
                def val17 = 0, val18 = 0, val19 = 0, val20 = 0;
//                def listKpi = R001_MRS_KPI_PROSES.findByTanggal_reportBetweenAndCompanyDealer(tgl, tgl + 1, CompanyDealer.findById(params.workshop as Long))
                final session = sessionFactory.currentSession
                String query =
                        "select " +
                                "CUST_DIUNDANG_ACT," + //1
                                "CUST_DIUNDANG_PLAN, " +//2
                                "DM_DIKIRIM, " +//3
                                "DM_PLAN, " +//4
                                "AKURASI_DM_KIRIM, " +//5
                                "AKURASI_DM_ACT, " +//6
                                "SMS_KIRIM, " +//7
                                "SMS_PLAN, " +//8
                                "AKURASI_SMS_DITERIMA, " +//9
                                "JML_SMS_DIKIRIM, " +//10
                                "CUST_DITELP_ACT, " +//11
                                "CUST_DITELP_PLAN, " +//12
                                "CUST_TERHUBUNG, " +//13
                                "AKURASI_CUST_DITELP, " +//14
                                "APPOINTMENT_ATCALL, " +//15
                                "APPOINTMENT_AFTER, " +//16
                                "TANGGAL_REPORT " +//17
                                "from R001_MRS_KPI_PROSES  " +
                                "WHERE TANGGAL_REPORT >= ? " +
                                "AND TANGGAL_REPORT < ? "+
                                "AND R001_M011_ID < ? ";
                try {
                    final sqlQuery = session.createSQLQuery(query);
                    sqlQuery.setParameter(0,tgl);
                    sqlQuery.setParameter(1,(tgl+1).clearTime());
                    sqlQuery.setParameter(2,CompanyDealer?.findById(params.workshop as Long)?.id);
                    final queryResults = sqlQuery.with {
                        list()
                    }
                    jumData = queryResults?.size()
                    queryResults.collect { resultRow ->
                        val1+=(resultRow[0]?resultRow[0].toInteger():0)
                        val2+=(resultRow[1]?resultRow[1].toInteger():0)
                        val3+=(resultRow[2]?resultRow[2].toInteger():0)
                        val4+=(resultRow[3]?resultRow[3].toInteger():0)
                        val5+=(resultRow[4]?resultRow[4].toInteger():0)
                        val6+=(resultRow[5]?resultRow[5].toInteger():0)
                        val7+=(resultRow[6]?resultRow[6].toInteger():0)
                        val8+=(resultRow[7]?resultRow[7].toInteger():0)
                        val9+=(resultRow[8]?resultRow[8].toInteger():0)
                        val10+=(resultRow[9]?resultRow[9].toInteger():0)
                        val11+=(resultRow[10]?resultRow[10].toInteger():0)
                        val12+=(resultRow[11]?resultRow[11].toInteger():0)
                        val13+=(resultRow[12]?resultRow[12].toInteger():0)
                        val14+=(resultRow[13]?resultRow[13].toInteger():0)
                        val15+=(resultRow[14]?resultRow[14].toInteger():0)
                        val16+=(resultRow[15]?resultRow[15].toInteger():0)
                    }

                }catch(Exception e){
                    throw new java.lang.Exception(e.getMessage());
                }

                if(jumData>0){
                    def data = [:]

                    count++
                    def totPlan = val4 + val8 + val12
                    def totAct = val3 + val7 + val11
                    def totApp = val15 + val16

                    def persDM = val4!=0 ? Math.round(val3/val4*100.doubleValue()) : 0
                    def persSMS = val8!=0 ? Math.round(val7/val8*100.doubleValue()) : 0
                    def persCALL = val12!=0 ? Math.round(val11/val12*100.doubleValue()) : 0
                    def persTot = totPlan!=0 ? Math.round(totAct/totPlan*100.doubleValue()) : 0

                    def persAdm = val3!=0 ? Math.round(val5/val3*100.doubleValue()) : 0
                    def persAsms = val7!=0 ? Math.round(val9/val7*100.doubleValue()) : 0
                    def persAcall = val11!=0 ? Math.round(val13/val11*100.doubleValue()) : 0

                    def persAppat = val13!=0 ? Math.round(val15/val13*100.doubleValue()) : 0
                    def persAppaf = val13!=0 ? Math.round(val16/val13*100.doubleValue()) : 0
                    def persAppt = val13!=0 ? Math.round(totApp/val13*100.doubleValue()) : 0

                    data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("no", count)
                    data.put("date", tgl.format("dd-MMM-yyyy"))
                    data.put("dm1", val4)
                    data.put("sms1", val8)
                    data.put("call1", val12)
                    data.put("tot1", totPlan)
                    data.put("dm2", val3)
                    data.put("sms2", val7)
                    data.put("call2", val11)
                    data.put("tot2", totAct)
                    data.put("dmrec", val5)
                    data.put("smsrec", val9)
                    data.put("concall", val13)
                    data.put("atcall1", val15)
                    data.put("afcall1", val16)
                    data.put("totcall1", totApp)
                    data.put("dm3", persDM+" %")
                    data.put("sms3", persSMS+" %")
                    data.put("call3", persCALL+" %")
                    data.put("tot3", persTot)
                    data.put("akurasialamat", persAdm+" %")
                    data.put("akurasisms", persAsms+" %")
                    data.put("akurasinotelp", persAcall+" %")
                    data.put("atcall2", persAppat+" %")
                    data.put("afcall2", persAppaf+" %")
                    data.put("totcall2", persAppt+" %")

                    tDM+=val4
                    tSMS+=val8
                    tCALL+=val12
                    tTotP+=totPlan

                    tAdm+=val3
                    tAsms+=val7
                    tAcall+=val11
                    tTotA+=totAct

                    tSdm+=val5
                    tSsms+=val9
                    tScall+=val13

                    tApat+=val15
                    tApaf+=val16
                    tTotap+=totApp

                    tPers1=tDM !=0 ? Math.round(tAdm/tDM*100.doubleValue()) : 0
                    tPers2=tSMS !=0 ? Math.round(tAsms/tSMS*100.doubleValue()) : 0
                    tPers3=tCALL !=0 ? Math.round(tAcall/tCALL*100.doubleValue()) : 0
                    tPers4=tTotP !=0 ? Math.round(tTotA/tTotP*100.doubleValue()) : 0

                    tPers5=tAdm !=0 ? Math.round(tSdm/tAdm*100.doubleValue()) : 0
                    tPers6=tAsms !=0 ? Math.round(tSsms/tAsms*100.doubleValue()) : 0
                    tPers7=tAcall !=0 ? Math.round(tScall/tAcall*100.doubleValue()) : 0

                    tPers8=tScall !=0 ? Math.round(tApat/tScall*100.doubleValue()) : 0
                    tPers9=tScall !=0 ? Math.round(tApaf/tScall*100.doubleValue()) : 0
                    tPers10=tScall !=0 ? Math.round(tTotap/tScall*100.doubleValue()) : 0

                    data.put("alltot1", tDM)
                    data.put("alltot2", tSMS)
                    data.put("alltot3", tCALL)
                    data.put("alltot4", tTotP)
                    data.put("alltot5", tAdm)
                    data.put("alltot6", tAsms)
                    data.put("alltot7", tAcall)
                    data.put("alltot8", tTotA)
                    data.put("alltot9", tSdm)
                    data.put("alltot10", tSsms)
                    data.put("alltot11", tScall)
                    data.put("alltot12", tApat)
                    data.put("alltot13", tApaf)
                    data.put("alltot14", tTotap)
                    data.put("alltot15", tPers1+" %")
                    data.put("alltot16", tPers2+" %")
                    data.put("alltot17", tPers3+" %")
                    data.put("alltot18", tPers4+" %")
                    data.put("alltot19", tPers5+" %")
                    data.put("alltot20", tPers6+" %")
                    data.put("alltot21", tPers7+" %")
                    data.put("alltot22", tPers8+" %")
                    data.put("alltot23", tPers9+" %")
                    data.put("alltot24", tPers10+" %")

                    reportData.add(data)
                }
            }
            if(reportData?.size()<1){
                def data = [:]
                count++
                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("no", count)
                data.put("date", tgl.format("dd-MMM-yyyy"))

                data.put("dm1", "-")
                data.put("sms1", "-")
                data.put("call1", "-")
                data.put("tot1", "-")
                data.put("dm2", "-")
                data.put("sms2", "-")
                data.put("call2", "-")
                data.put("tot2", "-")
                data.put("dmrec", "-")
                data.put("smsrec", "-")
                data.put("concall", "-")
                data.put("atcall1", "-")
                data.put("afcall1", "-")
                data.put("totcall1", "-")
                data.put("dm3", "-")
                data.put("sms3", "-")
                data.put("call3", "-")
                data.put("tot3", "-")
                data.put("akurasialamat", "-")
                data.put("akurasisms", "-")
                data.put("akurasinotelp", "-")
                data.put("atcall2", "-")
                data.put("afcall2", "-")
                data.put("totcall2", "-")

                data.put("alltot1", "-")
                data.put("alltot2", "-")
                data.put("alltot3", "-")
                data.put("alltot4", "-")
                data.put("alltot5", "-")
                data.put("alltot6", "-")
                data.put("alltot7", "-")
                data.put("alltot8", "-")
                data.put("alltot9", "-")
                data.put("alltot10", "-")
                data.put("alltot11", "-")
                data.put("alltot12", "-")
                data.put("alltot13", "-")
                data.put("alltot14", "-")
                data.put("alltot15", "-")
                data.put("alltot16", "-")
                data.put("alltot17", "-")
                data.put("alltot18", "-")
                data.put("alltot19", "-")
                data.put("alltot20", "-")
                data.put("alltot21", "-")
                data.put("alltot22", "-")
                data.put("alltot23", "-")
                data.put("alltot24", "-")

                reportData.add(data)
            }
        }

        else if(params.namaReport=="11"){
            def arr = Integer.parseInt(params.bulan1)

            def bln1 = Integer.parseInt(params.bulan1)
            def bln2 = Integer.parseInt(params.bulan2)
            def periode = listBulan.get(Integer.parseInt(params.bulan1)) + " " +params.tahun +" - "+ listBulan.get(Integer.parseInt(params.bulan2))+" "+ params.tahun
            int selisih = bln2 - bln1
            def s1k = 0, s10k = 0, s20k = 0, s30k = 0, s40k = 0, s50k = 0, s60k = 0, s70k = 0, s80k = 0, s90k = 0
            def t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0 , t8 = 0, t9 = 0, t10 = 0, t11 = 0, t12 = 0, t13 = 0, t14 = 0, t15 = 0
            def t16 = 0, t17 = 0, t18 = 0, t19 = 0, t20 = 0, t21 = 0, t22 = 0, t23 = 0 , t24 = 0, t25 = 0, t26 = 0, t27 = 0, t28 = 0, t29 = 0, t30 = 0
            def t31 = 0, t32 = 0, t33 = 0, t34 = 0, t35 = 0, t36 = 0, t37 = 0, t38 = 0, t39 = 0, t40 = 0, t41 = 0, t42 = 0, t43 = 0, t44 = 0, t45 = 0
            def data = [:]

            (0..selisih).each {
                bln1+=1
                String sdate1 = "01-"+bln1+"-"+params.tahun
                Date date1 = df2.parse(sdate1)
                cal1.setTime(date1)
                def maxD = cal1.getActualMaximum(cal1.DAY_OF_MONTH)
                String tgl2 = maxD+"-"+bln1+"-"+params.tahun+" 23:58:58"

                def strDate1 = new Date().parse("dd-MM-yyyy",sdate1.toString())
                def strEndDate2 = new Date().parse("dd-MM-yyyy HH:mm:ss",tgl2.toString())

                def listData = R001_MRS_KPI_PROSES.createCriteria().list {
                    ge("tanggal_report", strDate1)
                    le("tanggal_report", strEndDate2)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                int jumCusAct = 0, jumCusPlan = 0, jumDmA = 0, jumDmP = 0, jumDmRec = 0
                int jumSmsA = 0, jumSmsP = 0, jumSmsRec = 0, jumCallA = 0, jumCallP = 0, jumCallCon = 0, jAppAt = 0, jAppAf = 0, jAppAtAf = 0
                int persJudg = 0, persJdm = 0, persJAdm = 0, persJsms = 0, persJAsms = 0
                int persJcall = 0, persJAcall = 0, persJApat = 0, persJApaf = 0, persJApmrs = 0

                listData.each {
                    jumCusAct+= it?.cust_diundang_act; jumCusPlan+= it?.cust_diundang_plan; jumDmA+=it?.akurasi_dm_act
                    jumDmP+=it?.dm_plan; jumDmRec+=it?.akurasi_dm_kirim; jumSmsA+=it?.sms_kirim; jumSmsP+=it?.sms_plan
                    jumSmsRec+=it?.akurasi_sms_diterima; jumCallA+=it?.cust_ditelp_act; jumCallP+=it?.cust_ditelp_plan
                    jumCallCon+=it?.cust_terhubung; jAppAt+=it?.appointment_atcall; jAppAf+=it?.appointment_after
                    jAppAtAf+=it?.appointment_atcall + it?.appointment_after
                }


                def mount = ['jan', 'feb', 'mar', 'apr', 'mei', 'jun', 'jul', 'agu', 'sep', 'okt', 'nov', 'des']
                persJudg = jumCusPlan !=0 ? Math.round(jumCusAct/jumCusPlan*100.doubleValue()) : 0
                persJdm = jumDmP !=0 ? Math.round(jumDmA/jumDmP*100.doubleValue()) : 0
                persJAdm = jumDmA !=0 ? Math.round(jumDmRec/jumDmA*100.doubleValue()) : 0
                persJsms = jumSmsP !=0 ? Math.round(jumSmsA/jumSmsP*100.doubleValue()) : 0
                persJAsms = jumSmsA !=0 ? Math.round(jumSmsRec/jumSmsA*100.doubleValue()) : 0
                persJcall = jumCallP !=0 ? Math.round(jumCallA/jumCallP*100.doubleValue()) : 0
                persJAcall = jumCallA !=0 ? Math.round(jumCallCon/jumCallA*100.doubleValue()) : 0
                persJApat = jumCallCon !=0 ? Math.round(jAppAt/jumCallCon*100.doubleValue()) : 0
                persJApaf = jumCallCon !=0 ? Math.round(jAppAf/jumCallCon*100.doubleValue()) : 0
                persJApmrs = jumCallCon !=0 ? Math.round(jAppAtAf/jumCallCon*100.doubleValue()) : 0

                data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
                data.put("periode", periode)

                data.put(mount[arr]+"1",persJudg+" %"); t1+=persJudg
                data.put(mount[arr]+"2",jumCusAct); t2+=jumCusAct
                data.put(mount[arr]+"3",jumCusPlan); t3+=jumCusPlan
                data.put(mount[arr]+"4",persJdm+" %"); t4+=persJdm
                data.put(mount[arr]+"5",jumDmA); t5+=jumDmA
                data.put(mount[arr]+"6",jumDmP); t6+=jumDmP
                data.put(mount[arr]+"7",persJAdm+" %"); t7+=persJAdm
                data.put(mount[arr]+"8",jumDmRec); t8+=jumDmRec
                data.put(mount[arr]+"9",jumDmA); t9+=jumDmA
                data.put(mount[arr]+"10",persJsms+" %"); t10+=persJsms
                data.put(mount[arr]+"11",jumSmsA); t11+=jumSmsA
                data.put(mount[arr]+"12",jumSmsP); t12+=jumSmsP
                data.put(mount[arr]+"13",persJAsms+" %"); t13+=persJAsms
                data.put(mount[arr]+"14",jumSmsRec); t14+=jumSmsRec
                data.put(mount[arr]+"15",jumSmsA); t15+=jumSmsA
                data.put(mount[arr]+"16",persJcall+" %"); t16+=persJcall
                data.put(mount[arr]+"17",jumCallA); t17+=jumCallA
                data.put(mount[arr]+"18",jumCallP); t18+=jumCallP
                data.put(mount[arr]+"19",persJAcall+" %"); t19+=persJAcall
                data.put(mount[arr]+"20",jumCallCon); t20+=jumCallCon
                data.put(mount[arr]+"21",jumCallA); t21+=jumCallA
                data.put(mount[arr]+"22",persJApat+" %"); t22+=persJApat
                data.put(mount[arr]+"23",jAppAt); t23+=jAppAt
                data.put(mount[arr]+"24",jumCallCon); t24+=jumCallCon
                data.put(mount[arr]+"25",persJApaf+" %"); t25+=persJApaf
                data.put(mount[arr]+"26",jAppAf); t26+=jAppAf
                data.put(mount[arr]+"27",jumCallCon); t27+=jumCallCon
                data.put(mount[arr]+"28",persJApmrs+" %"); t28+=persJApmrs
                data.put(mount[arr]+"29",jAppAtAf); t29+=jAppAtAf
                data.put(mount[arr]+"30",jumCallCon); t30+=jumCallCon

                def listMrsRes = MrsKpiResult.createCriteria().list {
                    ge("tglLaporan", strDate1)
                    le("tglLaporan", strEndDate2)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                int persJCdg = 0, jCdu = 0, jCu = 0, persKMA = 0, jAmrs = 0, jTotA = 0
                int persKMSbe = 0, jUSbe = 0, jTotSbe = 0, persCKMue = 0, jUsgm = 0, jTotUe = 0
                int persSbi = 0, jCS = 0, jUks = 0

                listMrsRes.each {
                    jCdu+=it.custDtgDiundang
                    jCu+=it.custDiundang
                    jAmrs+=it.appMrs
                    jTotA+=it.totalApp
                    jUSbe+=it.sbeMrs
                    jTotSbe+=it.totalUnitSbe
                    jUsgm+=it.sbeGrMrs
                    jTotUe+=it.totalUnitEntry
                    jCS+=it.sbiMrs
                    jUks+=it.jumUnitTerkirim
                    s1k+=it.sbe1k_10k
                    s10k+=it.sbe10k_20k
                    s20k+=it.sbe20k_30k
                    s30k+=it.sbe30k_40k
                    s40k+=it.sbe40k_50k
                    s50k+=it.sbe50k_60k
                    s60k+=it.sbe60k_70k
                    s70k+=it.sbe70k_80k
                    s80k+=it.sbe80k_90k
                    s90k+=it.sbe90k_100k
                }

                persJCdg = jCdu !=0 ? Math.round(jCdu/jCu*100.doubleValue()) : 0
                persKMA = jTotA !=0 ? Math.round(jAmrs/jTotA*100.doubleValue()) : 0
                persKMSbe = jTotSbe !=0 ? Math.round(jUSbe/jTotSbe*100.doubleValue()) : 0
                persCKMue = jTotUe !=0 ? Math.round(jUsgm/jTotUe*100.doubleValue()) : 0
                persSbi = jUks !=0 ? Math.round(jUks/jCS*100.doubleValue()) : 0

                data.put(mount[arr]+"31",persJCdg+" %"); t31+=persJCdg
                data.put(mount[arr]+"32",jCdu); t32+=jCdu
                data.put(mount[arr]+"33",jCu); t33+=jCu
                data.put(mount[arr]+"34",persKMA+" %"); t34+=persKMA
                data.put(mount[arr]+"35",jAmrs); t35+=jAmrs
                data.put(mount[arr]+"36",jTotA); t36+=jTotA
                data.put(mount[arr]+"37",persKMSbe+" %"); t37+=persKMSbe
                data.put(mount[arr]+"38",jUSbe); t38+=jUSbe
                data.put(mount[arr]+"39",jTotSbe); t39+=jTotSbe
                data.put(mount[arr]+"40",persCKMue+" %"); t40+=persCKMue
                data.put(mount[arr]+"41",jUsgm); t41+=jUsgm
                data.put(mount[arr]+"42",jTotUe); t42+=jTotUe
                data.put(mount[arr]+"43",persSbi+" %"); t43+=persSbi
                data.put(mount[arr]+"44",jCS); t44+=jCS
                data.put(mount[arr]+"45",jUks); t45+=jUks

                arr++

            }

            def T1 = t1 !=0 ? Math.round(t1/12.doubleValue()) : 0
            def T4 = t4 !=0 ? Math.round(t4/12.doubleValue()) : 0
            def T7 = t7 !=0 ? Math.round(t7/12.doubleValue()) : 0
            def T10 = t10 !=0 ? Math.round(t10/12.doubleValue()) : 0
            def T13 = t13 !=0 ? Math.round(t13/12.doubleValue()) : 0
            def T16 = t16 !=0 ? Math.round(t16/12.doubleValue()) : 0
            def T19 = t19 !=0 ? Math.round(t19/12.doubleValue()) : 0
            def T22 = t22 !=0 ? Math.round(t22/12.doubleValue()) : 0
            def T25 = t25 !=0 ? Math.round(t25/12.doubleValue()) : 0
            def T28 = t28 !=0 ? Math.round(t28/12.doubleValue()) : 0
            def T31 = t31 !=0 ? Math.round(t31/12.doubleValue()) : 0
            def T34 = t34 !=0 ? Math.round(t34/12.doubleValue()) : 0
            def T37 = t37 !=0 ? Math.round(t37/12.doubleValue()) : 0
            def T40 = t40 !=0 ? Math.round(t40/12.doubleValue()) : 0
            def T43 = t43 !=0 ? Math.round(t43/12.doubleValue()) : 0

            data.put("tot1", T1+" %")
            data.put("tot2", t2)
            data.put("tot3", t3)
            data.put("tot4", T4+" %")
            data.put("tot5", t5)
            data.put("tot6", t6)
            data.put("tot7", T7+" %")
            data.put("tot8", t8)
            data.put("tot9", t9)
            data.put("tot10", T10+" %")
            data.put("tot11", t11)
            data.put("tot12", t12)
            data.put("tot13", T13+" %")
            data.put("tot14", t14)
            data.put("tot15", t15)
            data.put("tot16", T16+" %")
            data.put("tot17", t17)
            data.put("tot18", t18)
            data.put("tot19", T19+" %")
            data.put("tot20", t20)
            data.put("tot21", t21)
            data.put("tot22", T22+" %")
            data.put("tot23", t23)
            data.put("tot24", t24)
            data.put("tot25", T25+" %")
            data.put("tot26", t26)
            data.put("tot27", t27)
            data.put("tot28", T28+" %")
            data.put("tot29", t29)
            data.put("tot30", t30)
            data.put("tot31", T31+" %")
            data.put("tot32", t32)
            data.put("tot33", t33)
            data.put("tot34", T34+" %")
            data.put("tot35", t35)
            data.put("tot36", t36)
            data.put("tot37", T37+" %")
            data.put("tot38", t38)
            data.put("tot39", t39)
            data.put("tot40", T40+" %")
            data.put("tot41", t41)
            data.put("tot42", t42)
            data.put("tot43", T43+" %")
            data.put("tot44", t44)
            data.put("tot45", t45)

            data.put("tot1k",Math.round(s1k.doubleValue())+" %")
            data.put("tot10k",Math.round(s10k.doubleValue())+" %")
            data.put("tot20k",Math.round(s20k.doubleValue())+" %")
            data.put("tot30k",Math.round(s30k.doubleValue())+" %")
            data.put("tot40k",Math.round(s40k.doubleValue())+" %")
            data.put("tot50k",Math.round(s50k.doubleValue())+" %")
            data.put("tot60k",Math.round(s60k.doubleValue())+" %")
            data.put("tot70k",Math.round(s70k.doubleValue())+" %")
            data.put("tot80k",Math.round(s80k.doubleValue())+" %")
            data.put("tot90k",Math.round(s90k.doubleValue())+" %")

            reportData.add(data)

        }
        else if(params.namaReport=="12"){
            String bulanAwal = listBulan.get(Integer.parseInt(params?.bulan1.toString()))+" "+params.tahun
            String bulanAkhir = listBulan.get(Integer.parseInt(params?.bulan2.toString()))+" "+params.tahun

            int hari=0
            if((Integer.parseInt(params.bulan2)+1)==1){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==2){
                hari=28
            }else if((Integer.parseInt(params.bulan2)+1)==3){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==4){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==5){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==6){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==7){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==8){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==9){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==10){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==11){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==12){
                hari=31
            }

            Calendar cal = Calendar.getInstance()
            cal.set(params.tahun as int,(params.bulan1 as int),1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

            Date tglmulai=new Date().parse("dd-MM-yyyy","01-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglAkhirBulan =new Date().parse("dd-MM-yyyy",cal.getActualMaximum(Calendar.DAY_OF_MONTH)+"-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglahir=new Date().parse("dd-MM-yyyy",hari+"-"+(Integer.parseInt(params.bulan2)+1)+"-"+params.tahun)

            def arrayreception=[]
            int a=0
            int b=0
            (1..10).each{//looping dari 1k-100K
                int no=1
                def receptionAwal
                def receptionAkhir
                def cSD1 = 0, cSD2 = 0
                receptionAwal = Reception.createCriteria().list {
                    ge("t401TanggalWO",tglmulai)
                    le("t401TanggalWO",tglAkhirBulan)
                    eq("staSave","0")
                    eq("staDel","0")
                    historyCustomer{
                        eq("t182StaTerimaMRS","1")
                    }
                }
                receptionAwal.each { rec ->
                    def job = JobRCP.findByReception(rec)
                    if(it==1 ){
                        a=1000
                        b=10000
                    }else{
                        a=(it-1)*10000
                        b=it*10000
                    }

                    String km  = (a/1000).toString() + "K"
                    String km2  = (b/1000).toString() + "K"
                    if(job?.operation?.m053JobsId?.contains(km)){
                        cSD1 += 1
                        receptionAkhir = Reception.createCriteria().list {
                            ge("t401TanggalWO",tglmulai)
                            le("t401TanggalWO",tglahir)
                            eq("staSave","0")
                            eq("staDel","0")
                            historyCustomer{
                                eq("t182StaTerimaMRS","1")
                                eq("id", rec?.historyCustomer?.id)
                            }
                        }
                        receptionAkhir.each{ recA ->
//                            def job2 = JobRCP.findByReception(reccHEK)
                            def job2 = JobRCP.findByReception(recA)
                            if(job2?.operation?.m053JobsId?.contains(km2)){
                                cSD2 += 1
                            }
                        }
                    }
                }
                def tPers1 = (cSD2) !=0 ? Math.round((cSD2)/ cSD1 * 100.doubleValue() ) : 0
                arrayreception << tPers1+"%"
            }

            def data = [:]
            data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
            data.put("title", "MRS - SB RETENTION (FIRST SERVICE)")
            data.put("from", "First Service")
            data.put("valfrom", bulanAwal)
            data.put("upto", bulanAkhir)
            data.put("10k",arrayreception[0])
            data.put("20k",arrayreception[1])
            data.put("30k",arrayreception[2])
            data.put("40k",arrayreception[3])
            data.put("50k",arrayreception[4])
            data.put("60k",arrayreception[5])
            data.put("70k",arrayreception[6])
            data.put("80k",arrayreception[7])
            data.put("90k",arrayreception[8])
            data.put("100k",arrayreception[9])
            reportData.add(data)
        }

        else if(params.namaReport=="13"){
            String bulanAwal = listBulan.get(Integer.parseInt(params.bulan1))+" "+params.tahun
            String bulanAkhir = listBulan.get(Integer.parseInt(params.bulan2))+" "+params.tahun
            int hari=0

            if((Integer.parseInt(params.bulan2)+1)==1){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==2){
                hari=28
            }else if((Integer.parseInt(params.bulan2)+1)==3){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==4){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==5){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==6){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==7){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==8){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==9){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==10){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==11){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==12){
                hari=31
            }

            Calendar cal = Calendar.getInstance()
            cal.set(params.tahun as int,(params.bulan1 as int),1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

            Date tglmulai=new Date().parse("dd-MM-yyyy","01-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglAkhirBulan =new Date().parse("dd-MM-yyyy",cal.getActualMaximum(Calendar.DAY_OF_MONTH)+"-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglahir=new Date().parse("dd-MM-yyyy",hari+"-"+(Integer.parseInt(params.bulan2)+1)+"-"+params.tahun)

            def historyCustomersnya=HistoryCustomer.createCriteria().list {
                eq("staDel","0")
            }

            int angka=0
            int no=1
            def km1 = [],km10 = [],km20 = [],km30 = [],km40 = [],km50 = [],km60 = [],km70 = [],km80 = [],km90 = [],km100 = []
            def ur1 = [],ur10 = [],ur20 = [],ur30 = [],ur40 = [],ur50 = [],ur60 = [],ur70 = [],ur80 = [],ur90 = [],ur100 = []
            def dataKm1000 = 0,dataKm10000 = 0,dataKm20000 = 0,dataKm30000 = 0,dataKm40000 = 0,dataKm50000 = 0,
                dataKm60000 = 0,dataKm70000 = 0,dataKm80000 = 0,dataKm90000 = 0,dataKm100000 = 0
            def dataUr1000 = 0,dataUr10000 = 0,dataUr20000 = 0,dataUr30000 = 0,dataUr40000 = 0,dataUr50000 = 0,
                dataUr60000 = 0,dataUr70000 = 0,dataUr80000 = 0,dataUr90000 = 0, dataUr100000 = 0
            historyCustomersnya.each { hc ->
                def receptionAwal
                receptionAwal = Reception.createCriteria().list {
                    ge("t401TanggalWO",tglmulai)
                    le("t401TanggalWO",tglahir)
                    eq("staSave","0");
                    eq("staDel","0");
                    historyCustomer{
                        eq("t182StaTerimaMRS","1")
                        eq("id",hc?.id as Long)
                    }
                }
                def masuk = false
                ur1[angka] = false
                ur10[angka] = false
                ur20[angka] = false
                ur30[angka] = false
                ur40[angka] = false
                ur50[angka] = false
                ur60[angka] = false
                ur70[angka] = false
                ur80[angka] = false
                ur90[angka] = false
                ur100[angka] = false
                receptionAwal.each {
                    def job = JobRCP.findByReception(it)
                    if(job){
                        masuk = false
                        if(job?.operation?.m053JobsId?.contains("1K")){
                            km1[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm1000++
                            ur1[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("10K")){
                            km10[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm10000++
                            ur10[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("20K")){
                            km20[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm20000++
                            ur20[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("30K")){
                            km30[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm30000++
                            ur30[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("40K")){
                            km40[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm40000++
                            ur40[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("50K")){
                            km50[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm50000++
                            ur50[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("60K")){
                            km60[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm60000++
                            ur60[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("70K")){
                            km70[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm70000++
                            ur70[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("80K")){
                            km80[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm80000++
                            ur80[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("90K")){
                            km90[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm90000++
                            ur90[angka] = true
                        }
                        if(job?.operation?.m053JobsId?.contains("100K")){
                            km100[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm100000++
                            ur100[angka] = true
                        }
                    }
                }
                if(masuk==true){
                    if(ur1[angka]==true && ur10[angka]==true){
                        dataUr1000++
                    }else if(ur10[angka]==true && ur20[angka]==true){
                        dataUr10000++
                    }else if(ur20[angka]==true && ur30[angka]==true){
                        dataUr20000++
                    }else if(ur30[angka]==true && ur40[angka]==true){
                        dataUr30000++
                    }else if(ur40[angka]==true && ur50[angka]==true){
                        dataUr40000++
                    }else if(ur50[angka]==true && ur60[angka]==true){
                        dataUr50000++
                    }else if(ur60[angka]==true && ur70[angka]==true){
                        dataUr60000++
                    }else if(ur70[angka]==true && ur80[angka]==true){
                        dataUr70000++
                    }else if(ur80[angka]==true && ur90[angka]==true){
                        dataUr80000++
                    }else if(ur90[angka]==true && ur100[angka]==true){
                        dataUr90000++
                    }
                }
                angka++
            }


            def data = [:]
            data.put("workshop",CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop)
            data.put("title", "MRS - SB RETENTION PERIOD")
            data.put("from", "First Service")
            data.put("valfrom", bulanAwal)
            data.put("upto", bulanAkhir)
            data.put("10k",(dataUr1000) !=0 ? Math.round((dataUr1000)/ dataKm1000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("20k",(dataUr10000) !=0 ? Math.round((dataUr20000)/ dataKm10000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("30k",(dataUr20000) !=0 ? Math.round((dataUr30000)/ dataKm20000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("40k",(dataUr30000) !=0 ? Math.round((dataUr40000)/ dataKm30000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("50k",(dataUr40000) !=0 ? Math.round((dataUr50000)/ dataKm40000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("60k",(dataUr50000) !=0 ? Math.round((dataUr60000)/ dataKm50000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("70k",(dataUr60000) !=0 ? Math.round((dataUr70000)/ dataKm60000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("80k",(dataUr70000) !=0 ? Math.round((dataUr80000)/ dataKm70000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("90k",(dataUr80000) !=0 ? Math.round((dataUr90000)/ dataKm80000 * 100.doubleValue() ) + "%" : 0 + "%")
            data.put("100k",(dataUr90000) !=0 ? Math.round((dataUr100000)/ dataKm90000 * 100.doubleValue() ) + "%" : 0 + "%")
            reportData.add(data)
        }
        else if(params.namaReport=="14"){
            Calendar cal = Calendar.getInstance()
            cal.set(params.tahun as int,(params.bulan1 as int),1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

            Date tglmulai=new Date().parse("dd-MM-yyyy","01-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglahir =new Date().parse("dd-MM-yyyy",cal.getActualMaximum(Calendar.DAY_OF_MONTH)+"-"+(Integer.parseInt(params.bulan2)+1)+"-"+params.tahun)

            def bulan1=Integer.parseInt(params.bulan1)+1
            def bulan2=Integer.parseInt(params.bulan2)+1

            def bln=new Date().format("MM")
//            def historyCustomerVehicle = HistoryCustomerVehicle.createCriteria().list {
//                eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
//                ge("t183TglSTNK",tglmulai)
//                le("t183TglSTNK",tglahir)
//            }

            int no=1
            def custamer = HistoryCustomer.createCriteria().list {
                eq("companyDealer",CompanyDealer.findById(params.workshop as Long))
                ge("t182TglLahirMonth",bulan1)
                le("t182TglLahirMonth",bulan2)
            }

            if(custamer.size() >0){
                custamer.each {//
                    def mapping=MappingCustVehicle.findAllByCustomer(it.customer)
                    def vhicle=HistoryCustomerVehicle.findByCustomerVehicleInListAndStaDel(mapping.customerVehicle,"0" )
                    if(mapping){
                        def data=[:]
                        data.put("no",no)
                        data.put("vincode", vhicle?.fullModelCode? vhicle?.fullModelCode:"-")
                        data.put("nopol", vhicle?.fullNoPol? vhicle?.fullNoPol:"-")
                        data.put("namastnk", vhicle?.t183NamaSTNK ? vhicle?.t183NamaSTNK:"-")
                        data.put("tanggalstnk", vhicle?.t183TglSTNK ? vhicle?.t183TglSTNK?.format("dd/MM/yyyy") :"-")
                        data.put("alamatstnk", vhicle?.t183AlamatSTNK ? vhicle?.t183AlamatSTNK:"-")
                        data.put("jenismobil", vhicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? vhicle.fullModelCode?.baseModel?.m102NamaBaseModel:"-")
                        data.put("jeniscustomer", it?.jenisCustomer ? it?.jenisCustomer :"-")
                        data.put("nama", it?.t182NamaDepan? it?.fullNama:"-")
                        data.put("alamat", it?.t182Alamat ? it?.t182Alamat:"-")
                        data.put("kontak1", it?.t182NoHp ? it?.t182NoHp :"-")
                        data.put("kontak2", it?.t182NoTelpRumah ? it?.t182NoTelpRumah:"-")
                        data.put("tanggallahir", it?.t182TglLahir ? it?.t182TglLahir?.format("dd/MM/yyyy"):"-")
                        data.put("Hobby", it?.hobbies ? it?.hobbies.toString()?.substring(1, it?.hobbies?.toString()?.length() - 1) :"-")
                        no++
                        reportData.add(data)
                    }

                }
            }


            else{
                def data=[:]
                data.put("no", "-")
                data.put("vincode", "-")
                data.put("nopol", "-")
                data.put("namastnk", "-")
                data.put("tanggalstnk","-")
                data.put("alamatstnk", "-")
                data.put("jenismobil","-")
                data.put("jeniscustomer", "-")
                data.put("nama", "-")
                data.put("alamat", "-")
                data.put("kontak1", "-")
                data.put("kontak2","-")
                data.put("tanggallahir","-")
                data.put("Hobby", "-")

                reportData.add(data)
            }
        }
        else if(params.namaReport=="15"){
            def data = [:]
            def arr = Integer.parseInt(params.bulan1)
            def bln1 = Integer.parseInt(params.bulan1)
            def bln2 = Integer.parseInt(params.bulan2)
            int selisih =bln2-bln1
            def mount = ['jan', 'feb', 'mar', 'apr', 'mei', 'jun', 'jul', 'agu', 'sep', 'okt', 'nov', 'des']
            int TotalA=0, TotalB=0, TotalC=0, TotalD=0, TotalE=0, TotalF=0, TotalG=0, TotalH=0, TotalI=0, TotalJ=0

            (0..selisih).each {

                int JumA=0,  JumB=0,  JumC=0, JumD=0, JumE=0,JumF=0,JumG=0, JumH=0,JumI=0,JumJ=0
                int JumAA=0, JumAB=0, JumAC=0, JumAD=0, JumAE=0, JumAF=0, JumAG=0,JumAH=0, JumAI=0, JumAJ=0
                bln1+=1
                String sdate1 = "01-"+bln1+"-"+params.tahun
                Date date1 = df2.parse(sdate1)
                cal1.setTime(date1)
                def maxD = cal1.getActualMaximum(cal1.DAY_OF_MONTH)
                String tgl2 = maxD+"-"+bln1+"-"+params.tahun+" 23:58:58"
                def dateawal = new Date().parse("dd-MM-yyyy",sdate1.toString())
                def dateakhir = new Date().parse("dd-MM-yyyy HH:mm:ss",tgl2.toString())

                def ListData=R003_CONTROL_BOARD.createCriteria().list {
                    ge("TANGGAL_REPORT",dateawal)
                    le("TANGGAL_REPORT",dateakhir)
                }

                ListData.each {
                    JumA+=it.LINE_ITEM_PARTS_BO
                    JumB+=it.LINE_ITEM_SUPPLY_PARTS_BO
                    JumC+=it.RATIO_AKURASI_SUPPLY_PARTS_BO
                    JumD+=it.PENJUALAN
                    JumE+=it.RATA_RATA_PENJUALAN_3_BULAN
                    JumF+=it.STOK_AKHIR_BULAN
                    JumG+=it.STOK_DAY
                    JumH+=it.OVER_STOCK_PARTS
                    JumI+=it.DEAD_STOCK_PARTS
                    JumJ+=it.STOCK_EFESIENSI_PERSEN
                    JumAA+=it.SERVICE_RATE
                    JumAB+=it.SERVICE_RATE_TARGET
                    JumAC+=it.SERVICE_ORDER_FILL_RATE
                    JumAD+=it.SERVICE_ORDER_FILL_RATE_TARGET
                    JumAE+=it.TYPE_VOR
                    JumAF+=it.TYPE_SATU
                    JumAG+=it.TYPE_DUA
                }

                data.put(mount[arr]+"11",Math.round(JumAC.doubleValue())+"%")
                data.put(mount[arr]+"12",Math.round(JumAD.doubleValue())+"%")
                data.put(mount[arr]+"1111",Math.round(JumAE.doubleValue())+"%")
                data.put(mount[arr]+"1112",Math.round(JumAF.doubleValue())+"%")
                data.put(mount[arr]+"1113",Math.round(JumAG.doubleValue())+"%")
                data.put(mount[arr]+"111",Math.round(JumAA.doubleValue())+"%")
                data.put(mount[arr]+"112",Math.round(JumAB.doubleValue())+"%")

                data.put(mount[arr]+"1",JumA);TotalA+=JumA
                data.put(mount[arr]+"2",JumB);TotalB+=JumB
                data.put(mount[arr]+"3",Math.round(JumC.doubleValue())+"%");TotalC+=JumC
                data.put(mount[arr]+"4",JumD);TotalD+=JumD
                data.put(mount[arr]+"5",JumE);TotalE+=JumE
                data.put(mount[arr]+"6",JumF);TotalF+=JumF
                data.put(mount[arr]+"7",JumG);TotalG+=JumG
                data.put(mount[arr]+"8",JumH);TotalH+=JumH
                data.put(mount[arr]+"9",JumI);TotalI+=JumI
                data.put(mount[arr]+"10",JumJ);TotalJ+=JumJ
                arr++
            }

            def TPERS  = TotalC !=0 ? Math.round(TotalC/selisih.doubleValue()) : 0
            data.put("tot1", TotalA)
            data.put("tot2", TotalB)
            data.put("tot3", TPERS+"%")
            data.put("tot4", TotalD)
            data.put("tot5", TotalE)
            data.put("tot6", TotalF)
            data.put("tot7", TotalG)
            data.put("tot8", TotalH)
            data.put("tot9", TotalI)
            data.put("tot10", TotalJ)
            reportData.add(data)
        }
        return reportData
    }

    def calculateReportDataDetail(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
        String tanggal=params.tanggal
        String tanggalEnd=params.tanggal2
        def strDate = new Date().parse("d-MM-yyyy",tanggal.toString())
        def strEndDate = new Date().parse("d-MM-yyyy",tanggalEnd.toString())
        if (params.namaReport == "02"){
            int count = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglDM",tgl)
                    lt("t201TglDM",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                if(listPlan.size() > 0){
                    listPlan.each {
                        def jumlah = Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminder(tgl, tgl + 1,"0",
                                JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","Mail"),it?.companyDealer,it?.customerVehicle,it)

                        def data = [:]
                        count++

                        def maping = MappingCustVehicle.findByCustomerVehicle(it?.customerVehicle)
                        def histCV = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(it?.customerVehicle,"0")
                        def histCus = HistoryCustomer.findByCustomerAndStaDel(maping?.customer,"0")

                        String nama = histCus?.t182NamaDepan+" "+histCus?.t182NamaBelakang
                        String alamat = histCus?.t182Alamat+" "+histCus?.t182RT+" "+histCus?.t182RW+" "+histCus?.kelurahan+" "+histCus?.kecamatan+" "+histCus?.kabKota+" "+histCus?.provinsi+" "+histCus?.t182KodePos
                        data.put("mrsactivity", "DM")
                        data.put("activity1", "Actual Date")
                        data.put("activity2", "Sent")
                        data.put("activity3", "Status")
                        data.put("nodetail", count)
                        data.put("datedetail", it?.t201TglDM ? it?.t201TglDM.format("dd-MMM-yyyy") : "")
                        data.put("nama", nama)
                        data.put("alamat", alamat)
                        data.put("nohp", histCus?.t182NoHp ? histCus?.t182NoHp : "-")
                        data.put("notelp", histCus?.t182NoTelpRumah ? histCus?.t182NoTelpRumah : "-")
                        data.put("nopol", histCV?.fullNoPol ? histCV?.fullNoPol:"-")
                        data.put("model", histCV?.fullModelCode?.baseModel?.m102NamaBaseModel ? histCV?.fullModelCode?.baseModel?.m102NamaBaseModel:"-")
                        data.put("year", histCV?.t183ThnBlnRakit ? histCV?.t183ThnBlnRakit:"-")
                        data.put("kminvitation", it?.t201LastKM)
                        data.put("valact1", jumlah?.t202TglJamKirim ? jumlah?.t202TglJamKirim?.format("dd/MM/yyyy") : "-")
                        data.put("valact2", jumlah?.t202TglJamKirim ? "Yes" : "No")
                        data.put("valact3", jumlah?.t202StaInvitation && jumlah?.t202StaInvitation?.charAt(0)=="1" && jumlah?.t202StaInvitation?.charAt(5)=="1" ? "Yes" : "No")
                        reportData.add(data)
                    }
                }
            }
            if(reportData?.size()<1){
                def data = [:]
                count++
                data.put("mrsactivity", "DM")
                data.put("activity1", "Actual Date")
                data.put("activity2", "Sent")
                data.put("activity3", "Status")
                data.put("nodetail", count)
                data.put("datedetail", tgl.format("dd-MMM-yyyy"))
                data.put("nama", "-")
                data.put("alamat", "-")
                data.put("nohp", "-")
                data.put("notelp", "-")
                data.put("nopol", "-")
                data.put("model", "-")
                data.put("year", "-")
                data.put("kminvitation", "-")
                data.put("valact1", "-")
                data.put("valact2", "-")
                data.put("valact3", "-")
                reportData.add(data)
            }
        }
        if (params.namaReport == "03"){
            int count = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglSMS",tgl)
                    lt("t201TglSMS",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                if(listPlan.size() > 0){
                    listPlan.each {
                        def jumlah = Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminder(tgl, tgl + 1,"0",
                                JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","SMS"),it?.companyDealer,it?.customerVehicle,it)

                        def data = [:]
                        count++

                        def maping = MappingCustVehicle.findByCustomerVehicle(it?.customerVehicle)
                        def histCV = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(it?.customerVehicle,"0")
                        def histCus = HistoryCustomer.findByCustomerAndStaDel(maping?.customer,"0")
                        String nama = histCus?.t182NamaDepan+" "+histCus?.t182NamaBelakang
                        String alamat = histCus?.t182Alamat+" "+histCus?.t182RT+" "+histCus?.t182RW+" "+histCus?.kelurahan+" "+histCus?.kecamatan+" "+histCus?.kabKota+" "+histCus?.provinsi+" "+histCus?.t182KodePos
                        def modelMobil = histCV?.fullModelCode?.baseModel?.m102NamaBaseModel
                        if (modelMobil==null){
                            modelMobil="-"
                        }
                        def tahunRakitMobil = histCV?.t183ThnBlnRakit
                        if(tahunRakitMobil==null){
                            tahunRakitMobil="-"
                        }
                        data.put("mrsactivity", "SMS")
                        data.put("activity1", "Actual Date")
                        data.put("activity2", "Sent")
                        data.put("activity3", "Status")
                        data.put("nodetail", count)
                        data.put("datedetail", it?.t201TglSMS ? it?.t201TglSMS.format("dd-MMM-yyyy") : "-")
                        data.put("nama", histCus?.fullNama ? histCus?.fullNama:"-")
                        data.put("alamat", alamat ? alamat:"-")
                        data.put("nohp", histCus?.t182NoHp ? histCus?.t182NoHp : "-")
                        data.put("notelp", histCus?.t182NoTelpRumah ? histCus?.t182NoTelpRumah : "-")
                        data.put("nopol", histCV?.fullNoPol ? histCV?.fullNoPol : "-")
                        data.put("model", modelMobil)
                        data.put("year", tahunRakitMobil)
                        data.put("kminvitation", it?.t201LastKM)
                        data.put("valact1", jumlah?.t202TglJamKirim ? jumlah?.t202TglJamKirim?.format("dd/MM/yyyy") : "-")
                        data.put("valact2", "-")
                        data.put("valact3", "-")
                        reportData.add(data)
                    }

                }
            }
            if(reportData?.size()<1){
                def data = [:]
                count++
                data.put("mrsactivity", "SMS")
                data.put("activity1", "Actual Date")
                data.put("activity2", "Sent")
                data.put("activity3", "Status")
                data.put("nodetail", count)
                data.put("datedetail", tgl.format("dd-MMM-yyyy"))
                data.put("nama", "-")
                data.put("alamat", "-")
                data.put("nohp", "-")
                data.put("notelp", "-")
                data.put("nopol", "-")
                data.put("model", "-")
                data.put("year", "-")
                data.put("kminvitation", "-")
                data.put("valact1", "-")
                data.put("valact2", "-")
                data.put("valact3", "-")
                reportData.add(data)
            }
        }

        if (params.namaReport == "04"){
            int count = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglCall",tgl)
                    lt("t201TglCall",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                if(listPlan.size() > 0){
                    listPlan.each {
                        def jumlah = Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminder(tgl, tgl + 1,"0",
                                JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","Phone Call"),it?.companyDealer,it?.customerVehicle,it)

                        def data = [:]
                        count++

                        def maping = MappingCustVehicle.findByCustomerVehicle(it?.customerVehicle)
                        def histCV = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(it?.customerVehicle,"0")
                        def histCus = HistoryCustomer.findByCustomerAndStaDel(maping?.customer,"0")

                        String nama = histCus?.t182NamaDepan+" "+histCus?.t182NamaBelakang ? histCus?.t182NamaDepan+"-"+histCus?.t182NamaBelakang:"-"
                        String alamat = histCus?.t182Alamat+" "+histCus?.t182RT+" "+histCus?.t182RW+" "+histCus?.kelurahan+" "+histCus?.kecamatan+" "+histCus?.kabKota+" "+histCus?.provinsi+" "+histCus?.t182KodePos
                        def fullModelMobil = histCV?.fullModelCode?.baseModel?.m102NamaBaseModel
                        if (fullModelMobil==null){
                            fullModelMobil="-"
                        }

                        def tahunRakitMobil = histCV?.t183ThnBlnRakit
                        if (tahunRakitMobil==null){
                            tahunRakitMobil="-"
                        }

                        data.put("mrsactivity", "Call")
                        data.put("activity1", "Actual Date")
                        data.put("activity2", "Status")
                        data.put("activity3", "Keterangan")
                        data.put("nodetail", count)
                        data.put("datedetail", it?.t201TglCall ? it?.t201TglCall.format("dd-MMM-yyyy") : "-")
                        data.put("nama", histCus?.fullNama)
                        data.put("alamat", alamat)
                        data.put("nohp", histCus?.t182NoHp ? histCus?.t182NoHp : "-")
                        data.put("notelp", histCus?.t182NoTelpRumah ? histCus?.t182NoTelpRumah : "-")
                        data.put("nopol", histCV?.fullNoPol ? histCV?.fullNoPol : "-")
                        data.put("model", fullModelMobil)
                        data.put("year", tahunRakitMobil)
                        data.put("kminvitation", it?.t201LastKM ? it?.t201LastKM:"-")
                        data.put("valact1", jumlah?.t202TglJamKirim ? jumlah?.t202TglJamKirim?.format("dd/MM/yyyy") : "-")
                        data.put("valact2", jumlah?.t202StaInvitation && jumlah?.t202StaInvitation?.charAt(0)=="1" ? "Connected" : "No")
                        data.put("valact3", histCus?.t182Ket ? histCus?.t182Ket : "-")
                        reportData.add(data)
                    }
                }
            }
            if(reportData?.size()<1){
                def data = [:]
                count++
                data.put("mrsactivity", "Call")
                data.put("activity1", "Actual Date")
                data.put("activity2", "Status")
                data.put("activity3", "Keterangan")
                data.put("nodetail", count)
                data.put("datedetail", tgl.format("dd-MMM-yyyy"))
                data.put("nama", "-")
                data.put("alamat", "-")
                data.put("nohp", "-")
                data.put("notelp", "-")
                data.put("nopol", "-")
                data.put("model", "-")
                data.put("year", "-")
                data.put("kminvitation", "-")
                data.put("valact1", "-")
                data.put("valact2", "-")
                data.put("valact3", "-")
                reportData.add(data)
            }
        }

        if (params.namaReport == "05"){
            int count = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate

            (0..selisih).each {
                tgl+=1
                def listPlan = Reminder.createCriteria().list {
                    eq("staDel","0")
                    ge("t201TglCall",tgl)
                    lt("t201TglCall",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                if(listPlan.size() > 0){
                    listPlan.each {
                        def jumlah = Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicleAndReminder(tgl, tgl + 1,"0",
                                JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","CALL"),it?.companyDealer,it?.customerVehicle,it)

                        def data = [:]
                        count++

                        def maping = MappingCustVehicle.findByCustomerVehicle(it?.customerVehicle)
                        def histCV = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(it?.customerVehicle,"0")
                        def histCus = HistoryCustomer.findByCustomerAndStaDel(maping?.customer,"0")

                        String nama = histCus?.t182NamaDepan+" "+histCus?.t182NamaBelakang ? histCus?.t182NamaDepan+"-"+histCus?.t182NamaBelakang:"-"
                        String alamat = histCus?.t182Alamat+" "+histCus?.t182RT+" "+histCus?.t182RW+" "+histCus?.kelurahan+" "+histCus?.kecamatan+" "+histCus?.kabKota+" "+histCus?.provinsi+" "+histCus?.t182KodePos
                        def kmInvitation = it?.t201LastKM ? it?.t201LastKM:"-"

                        data.put("nodetail", count)
                        data.put("datedetail", it?.t201TglCall ? it?.t201TglCall.format("dd-MMM-yyyy") : "-")
                        data.put("nama", histCus?.fullNama ? histCus?.fullNama:"-")
                        data.put("alamat", alamat ? alamat:"-")
                        data.put("nohp", histCus?.t182NoHp ? histCus?.t182NoHp : "-")
                        data.put("notelp", histCus?.t182NoTelpRumah ? histCus?.t182NoTelpRumah : "-")
                        data.put("nopol", histCV?.fullNoPol ? histCV?.fullNoPol : "-")
                        data.put("model", histCV?.fullModelCode?.baseModel?.m102NamaBaseModel ? histCV?.fullModelCode?.baseModel?.m102NamaBaseModel:"-")
                        data.put("year", histCV?.t183ThnBlnRakit ? histCV?.t183ThnBlnRakit:"-")
                        data.put("kminvitation", kmInvitation)
                        data.put("atcalldetail", jumlah?.t202StaInvitation && jumlah?.t202StaInvitation?.charAt(0)=="1" && jumlah?.t202StaLangsungAppointment=="1" ? "Yes" : "-")
                        data.put("afcalldetail", jumlah?.t202StaInvitation && jumlah?.t202StaInvitation?.charAt(0)=="1" && jumlah?.t202StaLangsungAppointment=="0" ? "No" : "-")
                        reportData.add(data)
                    }
                }
            }
            if(reportData.size()<1){
                def data = [:]
                data.put("nodetail",count)
                data.put("datedetail", tgl.format("dd-MMM-yyyy"))
                data.put("nama", "-")
                data.put("alamat", "-")
                data.put("nohp", "-")
                data.put("notelp", "-")
                data.put("nopol", "-")
                data.put("model", "-")
                data.put("year", "-")
                data.put("kminvitation", "-")
                data.put("atcalldetail", "-")
                data.put("afcalldetail", "-")
                reportData.add(data)
            }
        }

        if (params.namaReport == "07"){
            int count = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate

            (0..selisih).each {
                tgl+=1
                def listApp = Appointment.createCriteria().list {
                    eq("staDel","0")
                    eq("staSave","0")
                    ge("t301TglJamApp",tgl)
                    lt("t301TglJamApp",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }

                if(listApp.size() > 0){
                    listApp.each {
                        def jumlah = Invitation.findByT202TglJamKirimBetweenAndT202StaDelAndJenisInvitationAndCompanyDealerAndCustomerVehicle(tgl, tgl + 1,"0",
                                JenisInvitation.findByM204StaDelAndM204JenisInvitation("0","CALL"),it?.companyDealer,it?.customerVehicle)

                        def data = [:]
                        count++

                        def maping = MappingCustVehicle.findByCustomerVehicle(it?.customerVehicle)
                        def histCV = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(it?.customerVehicle,"0")
                        def histCus = HistoryCustomer.findByCustomerAndStaDel(maping?.customer,"0")

                        String nama = histCus?.t182NamaDepan+" "+histCus?.t182NamaBelakang ? histCus?.t182NamaDepan+"-"+histCus?.t182NamaBelakang:"-"
                        String alamat = histCus?.t182Alamat+" "+histCus?.t182RT+" "+histCus?.t182RW+" "+histCus?.kelurahan+" "+histCus?.kecamatan+" "+histCus?.kabKota+" "+histCus?.provinsi+" "+histCus?.t182KodePos ?
                                histCus?.t182Alamat+"-"+histCus?.t182RT+"-"+histCus?.t182RW+"-"+histCus?.kelurahan+"-"+histCus?.kecamatan+"-"+histCus?.kabKota+"-"+histCus?.provinsi+"-"+histCus?.t182KodePos:"-"

                        data.put("nodetail", count)
                        data.put("datedetail", it?.t301TglJamApp ? it?.t301TglJamApp.format("dd/MM/yyyy") : "-")
                        data.put("nama", histCus?.fullNama ? histCus?.fullNama:"-")
                        data.put("alamat", alamat ? alamat:"-")
                        data.put("nohp", histCus?.t182NoHp ? histCus?.t182NoHp : "-")
                        data.put("notelp", histCus?.t182NoTelpRumah ? histCus?.t182NoTelpRumah : "-")
                        data.put("nopol", histCV?.fullNoPol ? histCV?.fullNoPol : "-")
                        data.put("model", histCV?.fullModelCode?.baseModel?.m102NamaBaseModel ? histCV?.fullModelCode?.baseModel?.m102NamaBaseModel:"-")
                        data.put("year", histCV?.t183ThnBlnRakit ? histCV?.t183ThnBlnRakit:"-")
                        data.put("dataservice", it?.t301KmSaatIni ? it?.t301KmSaatIni : "-")
                        data.put("atcalldetail", jumlah?.t202StaInvitation && jumlah?.t202StaInvitation?.charAt(0)=="1" && jumlah?.t202StaLangsungAppointment=="1" ? "Yes" : "-")
                        data.put("afcalldetail", jumlah?.t202StaInvitation && jumlah?.t202StaInvitation?.charAt(0)=="1" && jumlah?.t202StaLangsungAppointment=="0" ? "Yes" : "-")
                        reportData.add(data)
                    }
                }

            }
            if(reportData?.size()<1){
                def data = [:]
                data.put("nodetail", "-")
                data.put("datedetail", "-")
                data.put("nama", "-")
                data.put("alamat", "-")
                data.put("nohp", "-")
                data.put("notelp", "-")
                data.put("nopol", "-")
                data.put("model", "-")
                data.put("year", "-")
                data.put("dataservice", "-")
                data.put("atcalldetail", "-")
                data.put("afcalldetail", "-")
                reportData.add(data)
            }
        }

        if (params.namaReport == "08"){
            int count = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate

            (0..selisih).each {
                tgl+=1
                def listRec = Reception.createCriteria().list {
                    eq("staDel","0");
                    eq("staSave","0");
                    ge("t401TanggalWO",tgl)
                    lt("t401TanggalWO",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                }
                if(listRec.size() > 0){
                    listRec.each {
                        def mrsContribution = "No"
                        def data = [:]
                        count++

                        def histCV = it.historyCustomerVehicle
                        def invitation = Invitation.findByT202TglJamKirimGreaterThanEqualsAndT202TglJamKirimLessThanAndCustomerVehicleAndT202StaDel(it.t401TanggalWO,it.t401TanggalWO + 1,it?.historyCustomerVehicle?.customerVehicle,"0")
                        if(invitation){
                            mrsContribution = "Yes"
                        }
                        def maping = MappingCustVehicle.findByCustomerVehicle(histCV?.customerVehicle)
                        def histCus = HistoryCustomer.findByCustomerAndStaDel(maping?.customer,"0")

//                        def jumlah = Reminder.findByT201TglCallBetweenAndCompanyDealerAndCustomerVehicleAndM201IDAndStaDel(tgl, tgl + 1, )
                        String nama = histCus?.t182NamaDepan+" "+histCus?.t182NamaBelakang ? histCus?.t182NamaDepan+"-"+histCus?.t182NamaBelakang:"-"
                        String alamat = histCus?.t182Alamat+" "+histCus?.t182RT+" "+histCus?.t182RW+" "+histCus?.kelurahan+" "+histCus?.kecamatan+" "+histCus?.kabKota+" "+histCus?.provinsi+" "+histCus?.t182KodePos

                        data.put("nodetail", count)
                        data.put("datedetail", it?.t401TanggalWO ? it?.t401TanggalWO.format("dd-MMM-yyyy") : "-")
                        data.put("nama", histCus?.fullNama ? histCus?.fullNama:"-")
                        data.put("alamat", alamat ? alamat:"-")
                        data.put("nohp", histCus?.t182NoHp ? histCus?.t182NoHp : "-")
                        data.put("notelp", histCus?.t182NoTelpRumah ? histCus?.t182NoTelpRumah : "-")
                        data.put("nopol", histCV?.fullNoPol ? histCV?.fullNoPol : "-")
                        data.put("model", histCV?.fullModelCode?.baseModel?.m102NamaBaseModel ? histCV?.fullModelCode?.baseModel?.m102NamaBaseModel:"-")
                        data.put("year", histCV?.t183ThnBlnRakit ? histCV?.t183ThnBlnRakit:"-")
                        data.put("dataservice", it?.t401KmSaatIni ? it?.t401KmSaatIni : "-")
                        data.put("mrscontributiondetail",mrsContribution)
                        reportData.add(data)
                    }
                }
            }
        }
        if (params.namaReport == "09"){
            int count = 0
            Date tgl = strDate - 1
            int selisih = strEndDate - strDate

            (0..selisih).each {
                tgl+=1
                def listRec = Reception.createCriteria().list {
                    ge("t401TanggalWO",tgl)
                    lt("t401TanggalWO",tgl + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    eq("staSave","0")
                    eq("staDel","0")
                }
                if(listRec.size() > 0){
                    listRec.each {
                        def mrsContribution = "No"
                        def data = [:]
                        count++

                        def histCV = it?.historyCustomerVehicle
                        def invitation = Invitation.findByT202TglJamKirimGreaterThanEqualsAndT202TglJamKirimLessThanAndCustomerVehicleAndT202StaDel(it?.t401TanggalWO,it?.t401TanggalWO + 1,it?.historyCustomerVehicle?.customerVehicle,"0")
                        if(invitation){
                            mrsContribution = "Yes"
                        }
                        def maping = MappingCustVehicle.findByCustomerVehicle(histCV?.customerVehicle)
                        def histCus = HistoryCustomer.findByCustomerAndStaDel(maping?.customer,"0")

//                        def jumlah = Reminder.findByT201TglCallBetweenAndCompanyDealerAndCustomerVehicleAndM201IDAndStaDel(tgl, tgl + 1, )
                        String nama = histCus?.t182NamaDepan+" "+histCus?.t182NamaBelakang ? histCus?.t182NamaDepan+"-"+histCus?.t182NamaBelakang:"-"
                        String alamat = histCus?.t182Alamat+" "+histCus?.t182RT+" "+histCus?.t182RW+" "+histCus?.kelurahan+" "+histCus?.kecamatan+" "+histCus?.kabKota+" "+histCus?.provinsi+" "+histCus?.t182KodePos ?
                                histCus?.t182Alamat+"-"+histCus?.t182RT+"-"+histCus?.t182RW+"-"+histCus?.kelurahan+"-"+histCus?.kecamatan+"-"+histCus?.kabKota+"-"+histCus?.provinsi+"-"+histCus?.t182KodePos:"-"

                        data.put("nodetail", count)
                        data.put("datedetail", it?.t401TanggalWO ? it?.t401TanggalWO.format("dd/MM/yyyy") : "-")
                        data.put("nama", histCus?.fullNama ? histCus?.fullNama:"-")
                        data.put("alamat", alamat ? alamat:"-")
                        data.put("nohp", histCus?.t182NoHp ? histCus?.t182NoHp : "-")
                        data.put("notelp", histCus?.t182NoTelpRumah ? histCus?.t182NoTelpRumah : "-")
                        data.put("nopol", histCV?.fullNoPol ? histCV?.fullNoPol : "-")
                        data.put("model", histCV?.fullModelCode?.baseModel?.m102NamaBaseModel ? histCV?.fullModelCode?.baseModel?.m102NamaBaseModel:"-")
                        data.put("year", histCV?.t183ThnBlnRakit ? histCV?.t183ThnBlnRakit:"-")
                        data.put("dataservice", it?.t401KmSaatIni ? it?.t401KmSaatIni : "-")
                        data.put("mrscontributiondetail",mrsContribution)
                        reportData.add(data)
                    }
                }
            }
        }
        if (params.namaReport == "12"){ //SBretentionfirst
            String bulanAwal = listBulan.get(Integer.parseInt(params.bulan1))+" "+params.tahun
            String bulanAkhir = listBulan.get(Integer.parseInt(params.bulan2))+" "+params.tahun
            int hari=0
            int hariBulan1=0

            if((Integer.parseInt(params.bulan2)+1)==1){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==2){
                hari=28
            }else if((Integer.parseInt(params.bulan2)+1)==3){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==4){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==5){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==6){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==7){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==8){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==9){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==10){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==11){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==12){
                hari=31
            }
            Calendar cal = Calendar.getInstance()
            cal.set(params.tahun as int,(params.bulan1 as int),1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

            Date tglmulai=new Date().parse("dd-MM-yyyy","01-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglAkhirBulan =new Date().parse("dd-MM-yyyy",cal.getActualMaximum(Calendar.DAY_OF_MONTH)+"-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglahir=new Date().parse("dd-MM-yyyy",hari+"-"+(Integer.parseInt(params.bulan2)+1)+"-"+params.tahun)

            int a=0
            int b=0

            (1..10).each{//looping dari 1k-100K
                int no=1
                def receptionAwal
                def receptionAkhir
                receptionAwal = Reception.createCriteria().list {
                    eq("staDel","0");
                    eq("staSave","0");
                    ge("t401TanggalWO",tglmulai)
                    le("t401TanggalWO",tglAkhirBulan)
                    eq("staSave","0")
                    historyCustomer{
                        eq("t182StaTerimaMRS","1")
                    }
                }

                def detil = []
                def masuk = false
                def cSD1 = 0, cSD2 = 0
                if(it==1 ){
                    a=1000
                    b=10000
                }else{
                    a=(it-1)*10000
                    b=it*10000
                }
                receptionAwal.each { rec ->
                    masuk = false
                    def job = JobRCP.findByReception(rec)
                    String km  = (a/1000).toString() + "K"
                    String km2  = (b/1000).toString() + "K"
                    def service2 = "-"
                    if(job?.operation?.m053JobsId?.contains(km)){
                        cSD1 += 1
                        masuk = true
                        receptionAkhir = Reception.createCriteria().list {
                            eq("staDel","0");
                            eq("staSave","0");
                            ge("t401TanggalWO",tglmulai)
                            le("t401TanggalWO",tglahir)
                            eq("staSave","0")
                            historyCustomer{
                                eq("t182StaTerimaMRS","1")
                                eq("id", rec?.historyCustomer?.id)
                            }
                        }
                        receptionAkhir.each{ recA ->
                            def job2 = JobRCP.findByReception(recA)
                            if(job2?.operation?.m053JobsId?.contains(km2)){
                                cSD2 += 1
                                service2 = recA?.t401TanggalWO.format("dd/MM/yyyy")
                            }
                        }
                        def customerDlmMapping = MappingCustVehicle.findByCustomer(rec.historyCustomer.customer)//dapetin data customer di MapingCustVehicle
                        def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(customerDlmMapping?.customerVehicle,0)
                        detil << [
                                no : no++,
                                nama : rec?.historyCustomer?.fullNama ? rec?.historyCustomer?.fullNama:"-",
                                telp : rec?.historyCustomer?.t182NoTelpRumah ? rec?.historyCustomer?.t182NoTelpRumah:"-",
                                jenisKendaraan : historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel:"-",
                                alamat : rec?.historyCustomer?.t182Alamat ? rec?.historyCustomer?.t182Alamat:"-",
                                tahun : historyCustomerVehicle?.t183TglDEC?.format("yyyy"),
                                noPol : historyCustomerVehicle?.fullNoPol ? historyCustomerVehicle?.fullNoPol:"-",
                                serviceDate1 : rec?.t401TanggalWO ? rec?.t401TanggalWO.format("dd/MM/yyyy"):"",
                                serviceDate2 : service2 ? service2:"-"
                        ]
                    }

                }
                if(receptionAwal.size()==0 || masuk == false){
                    detil << [
                            no : ""
                    ]
                }
                def tPers1 = (cSD2) !=0 ? Math.round((cSD2)/ cSD1 * 100.doubleValue() ) : 0
                reportData << [
                        detil : detil,
                        RetRate1 : (a/1000) + "K - " + (b/1000) + "K",
                        RetRate2 :  tPers1+"%"
                ]
            }
        }

        if (params.namaReport == "13"){
            String bulanAwal = listBulan.get(Integer.parseInt(params.bulan1))+" "+params.tahun
            String bulanAkhir = listBulan.get(Integer.parseInt(params.bulan2))+" "+params.tahun

            int hari=0

            if((Integer.parseInt(params.bulan2)+1)==1){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==2){
                hari=28
            }else if((Integer.parseInt(params.bulan2)+1)==3){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==4){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==5){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==6){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==7){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==8){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==9){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==10){
                hari=31
            }else if((Integer.parseInt(params.bulan2)+1)==11){
                hari=30
            }else if((Integer.parseInt(params.bulan2)+1)==12){
                hari=31
            }

            Date tglmulai=new Date().parse("dd-MM-yyyy","01-"+(Integer.parseInt(params.bulan1)+1)+"-"+params.tahun)
            Date tglahir=new Date().parse("dd-MM-yyyy",hari+"-"+(Integer.parseInt(params.bulan2)+1)+"-"+params.tahun)

            def historyCustomersss=HistoryCustomer.createCriteria().list {
                eq("staDel","0")
            }

            int angka=0
            int no=1
            def km1 = [],km10 = [],km20 = [],km30 = [],km40 = [],km50 = [],km60 = [],km70 = [],km80 = [],km90 = [],km100 = []
            def ur1 = [],ur10 = [],ur20 = [],ur30 = [],ur40 = [],ur50 = [],ur60 = [],ur70 = [],ur80 = [],ur90 = [],ur100 = []
            def dataKm1000 = 0,dataKm10000 = 0,dataKm20000 = 0,dataKm30000 = 0,dataKm40000 = 0,dataKm50000 = 0,
                dataKm60000 = 0,dataKm70000 = 0,dataKm80000 = 0,dataKm90000 = 0,dataKm100000 = 0
            def dataUr1000 = 0,dataUr10000 = 0,dataUr20000 = 0,dataUr30000 = 0,dataUr40000 = 0,dataUr50000 = 0,
                dataUr60000 = 0,dataUr70000 = 0,dataUr80000 = 0,dataUr90000 = 0
            historyCustomersss.each { hc ->
                def receptionAwal
                receptionAwal = Reception.createCriteria().list {
                    ge("t401TanggalWO",tglmulai)
                    le("t401TanggalWO",tglahir)
                    eq("staSave","0")
                    eq("staDel","0")
                    historyCustomer{
                        eq("t182StaTerimaMRS","1")
                        eq("id",hc?.id as Long)
                    }
                }
                def masuk = false
                ur1[angka] = false
                ur10[angka] = false
                ur20[angka] = false
                ur30[angka] = false
                ur40[angka] = false
                ur50[angka] = false
                ur60[angka] = false
                ur70[angka] = false
                ur80[angka] = false
                ur90[angka] = false
                ur100[angka] = false
                receptionAwal.each {
                    def job = JobRCP.findByReception(it)
                    if(job){
                        masuk = false
                        if(job?.operation?.m053JobsId?.contains("1K")){
                            km1[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm1000++
                            ur1[angka] = true
                        }

                        if(job?.operation?.m053JobsId?.contains("10K")){
                            km10[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm10000++
                            ur10[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("20K")){
                            km20[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm20000++
                            ur20[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("30K")){
                            km30[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm30000++
                            ur30[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("40K")){
                            km40[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm40000++
                            ur40[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("50K")){
                            km50[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm50000++
                            ur50[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("60K")){
                            km60[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm60000++
                            ur60[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("70K")){
                            km70[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm70000++
                            ur70[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("80K")){
                            km80[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm80000++
                            ur80[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("90K")){
                            km90[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm90000++
                            ur90[angka] = true

                        }
                        if(job?.operation?.m053JobsId?.contains("100K")){
                            km100[angka] = it?.t401TanggalWO.format("dd/MM/yyyy") ;masuk = true
                            dataKm100000++
                            ur100[angka] = true

                        }
                    }
                }
                if(masuk==true){
                    def customerDlmMapping = MappingCustVehicle.findByCustomer(hc?.customer)//dapetin data customer di MapingCustVehicle
                    def historyCustomerVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(customerDlmMapping?.customerVehicle,0)

                    def data = [:]

                    data.put("no", no++)
                    data.put("nama", hc?.fullNama ? hc?.fullNama:"-")
                    data.put("alamat", hc?.t182Alamat ? hc?.t182Alamat:"-")
                    data.put("notelepon", hc?.t182NoTelpRumah ? hc?.t182NoTelpRumah:"-")
                    data.put("nopol", historyCustomerVehicle?.fullNoPol ? historyCustomerVehicle?.fullNoPol:"-")
                    data.put("jenis", historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel:"-")
                    data.put("tahun", historyCustomerVehicle?.t183ThnBlnRakit ? historyCustomerVehicle?.t183ThnBlnRakit:"-")

                    data.put("1k", km1[angka])
                    data.put("10k", km10[angka])
                    data.put("20k", km20[angka])
                    data.put("30k", km30[angka])
                    data.put("40k", km40[angka])
                    data.put("50k", km50[angka])
                    data.put("60k", km60[angka])
                    data.put("70k", km70[angka])
                    data.put("80k", km80[angka])
                    data.put("90k", km90[angka])
                    data.put("100k", km100[angka])

                    data.put("110k", (ur1[angka]==true && ur10[angka]==true)?"Yes":"-" )
                    data.put("1020k", (ur10[angka]==true && ur20[angka]==true)?"Yes":"-")
                    data.put("2030k", (ur20[angka]==true && ur30[angka]==true)?"Yes":"-")
                    data.put("3040k", (ur30[angka]==true && ur40[angka]==true)?"Yes":"-")
                    data.put("4050k", (ur40[angka]==true && ur50[angka]==true)?"Yes":"-")
                    data.put("5060k", (ur50[angka]==true && ur60[angka]==true)?"Yes":"-")
                    data.put("6070k", (ur60[angka]==true && ur70[angka]==true)?"Yes":"-")
                    data.put("7080k", (ur70[angka]==true && ur80[angka]==true)?"Yes":"-")
                    data.put("8090k", (ur80[angka]==true && ur90[angka]==true)?"Yes":"-")
                    data.put("90100k", (ur90[angka]==true && ur100[angka]==true)?"Yes":"-")

                    data.put("1ktot" ,dataKm1000)
                    data.put("10ktot" ,dataKm10000)
                    data.put("20ktot" ,dataKm20000)
                    data.put("30ktot" ,dataKm30000)
                    data.put("40ktot" ,dataKm40000)
                    data.put("50ktot" ,dataKm50000)
                    data.put("60ktot" ,dataKm60000)
                    data.put("70ktot" ,dataKm70000)
                    data.put("80ktot" ,dataKm80000)
                    data.put("90ktot" ,dataKm90000)
                    data.put("100ktot" ,dataKm100000)
                    if(ur1[angka]==true && ur10[angka]==true){
                        dataUr1000++
                    }else if(ur10[angka]==true && ur20[angka]==true){
                        dataUr10000++
                    }else if(ur20[angka]==true && ur30[angka]==true){
                        dataUr20000++
                    }else if(ur30[angka]==true && ur40[angka]==true){
                        dataUr30000++
                    }else if(ur40[angka]==true && ur50[angka]==true){
                        dataUr40000++
                    }else if(ur50[angka]==true && ur60[angka]==true){
                        dataUr50000++
                    }else if(ur60[angka]==true && ur70[angka]==true){
                        dataUr60000++
                    }else if(ur70[angka]==true && ur80[angka]==true){
                        dataUr70000++
                    }else if(ur80[angka]==true && ur90[angka]==true){
                        dataUr80000++
                    }else if(ur90[angka]==true && ur100[angka]==true){
                        dataUr90000++
                    }
                    data.put("110ktot" ,dataUr1000)
                    data.put("1020ktot" ,dataUr10000)
                    data.put("2030ktot" ,dataUr20000)
                    data.put("3040ktot" ,dataUr30000)
                    data.put("4050ktot" ,dataUr40000)
                    data.put("5060ktot" ,dataUr50000)
                    data.put("6070ktot" ,dataUr60000)
                    data.put("7080ktot" ,dataUr70000)
                    data.put("8090ktot" ,dataUr80000)
                    data.put("90100ktot" ,dataUr90000)

                    reportData.add(data)
                }
                angka++
            }
        }
        return reportData
    }//end Calculate Detail
}
