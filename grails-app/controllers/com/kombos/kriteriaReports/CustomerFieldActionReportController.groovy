package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class CustomerFieldActionReportController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
	
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
	
	 def index() {}

    def previewData(){
        List<JasperReportDef> reportDefList = []
        def file = null
		def jasperParams = [:]      
		
        if(params.namaReport=="01"){			
			def reportData01 = customerFieldActionDaily(params, jasperParams)
            def reportDef = new JasperReportDef(name:'Part_Sales_Pernomor_WO.jasper',
					parameters: jasperParams,
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData01
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Part_Sales_Pernomor_WO_",".pdf")
        }	

    	file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    //Report customer field action daily
    def customerFieldActionDaily(def params, def jasperParams) {
		def reportData = new ArrayList();
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        def workshop = (params.workshop != null ? params.workshop : "")
		def listJenisFA = params.jenisFA as Set
		def faStatus = params.faStatus
		def listMainDealer = params.mainDealer as Set
		def statusBooking = params.statusBooking
		def noWo = (params.nomorWo != null ? params.nomorWo : "")
		
		jasperParams.put("WORKSHOP",CompanyDealer.findByM011ID(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd-MM-yyyy'))
		jasperParams.put("NOMORWO",noWo)
		
    	def listVehicleFA  = VehicleFA.createCriteria().list {
			if (faStatus == "All")
				'in' ("t185StaSudahDikerjakan",[0,1])  //dari parameter status All, 1(sudah), 0 Belum
			else if (faStatus != "All")
				eq ("t185StaSudahDikerjakan",faStatus) 
				
			'in' ("t185MainDealer",listMainDealer) 			
			fa {
				jenisFA {
					'in'("m184NamaJenisFA", listJenisFA)
				}
			}
		}
		
		listVehicleFA.each {
			def data = [:]
			def appointment = Appointment.findByCustomerVehicle(it?.customerVehicle)
			def customerIn = CustomerIn.findByCustomerVehicle(it?.customerVehicle)
			def modelVinCode = FullModelVinCode.findByFullModelCode(it?.historyCustomerVehicle.fullModelCode)
			if (statusBooking == "Yes") {
				if (appointment.t301TglJamRencana == null)
					return
			}else if (statusBooking == "No") {
				if (appointment.t301TglJamRencana != null)
					return
			}
			data.put("CUSTOMER_IN", customerIn.t400TglCetakNoAntrian)
			data.put("BOOKING_DATE", appointment.t301TglJamApp)
			if (appointment.t301TglJamRencana == null)
				data.put("STATUS_BOOKING", "No")
			if (appointment.t301TglJamRencana != null)	
				data.put("STATUS_BOOKING", "Yes")
			data.put("NO_POLISI", it?.historyCustomerVehicle.kodeKotaNoPol+ " "+it?.historyCustomerVehicle.t183NoPolTengah +" "+it?.historyCustomerVehicle.t183NoPolBelakang)
			data.put("NAMA_CUSTOMER", appointment.historyCustomer.t182NamaDepan+" "+appointment.historyCustomer.t182NamaBelakang)
			data.put("NO_WO", appointment.reception.t401NoWO)
			data.put("VIN_CODE",it?.historyCustomerVehicle.customerVehicle.t103VinCode)
			data.put("PRODUCTION YEAR", modelVinCode.t109ThnBlnPembuatan)
			data.put("FULL_MODEL_CODE",it?.historyCustomerVehicle.fullModelCode.t110FullModelCode )
			data.put("BASE_MODEL_NAME", it?.historyCustomerVehicle.fullModelCode.baseModel.m102NamaBaseModel)
			data.put("COLOUR", it?.historyCustomerVehicle.warna.m092NamaWarna)
			data.put("MAIN_DEALER", it?.t185MainDealer)
			data.put("DEALER", it?.t185Dealer)
			data.put("JENIS_FA",it?.fa.jenisFA.m184NamaJenisFA )
			data.put("JUDUL_FA", it?.fa.m185NamaFA)
			if (it?.t185StaSudahDikerjakan == 1)
				data.put("FA_STATUS", "Finish")
			else if (it?.t185StaSudahDikerjakan == 0)	
				data.put("FA_STATUS", "Not Yet")
			data.put("FA_ACTIVITY_INSPECTION", "") //??
			data.put("FA_ACTIVITY_REPAIR", "") //??
			data.put("PART_RELATED_PART NAME", "") //??
			data.put("PART_RELATED_PART NUMBER", "")//???
			
			reportData.add(data);
		}
		return reportData;
		
    }

}//end