package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Gate_passController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def gatePassService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01"){
			generateReportForGatePass(params);
		}		
 	}

 	def generateReportForGatePass(def params){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", "Sunter");
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", "All");		
		parameters.put("jenisGatePass", "All");					

		JRDataSource dataSource = new JRTableModelDataSource(jenisPekerjaanDataModel(params));
		parameters.put("subJenisPekerjaan", dataSource);
		dataSource = new JRTableModelDataSource(jenisGatePassDataModel(params));
		parameters.put("subJenisGatePass", dataSource);
		dataSource = new JRTableModelDataSource(totalDataModel(params));
		parameters.put("total", dataSource);
		dataSource = new JRTableModelDataSource(detailDataModel(params));
		parameters.put("detail", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Gate_Pass.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Gate_Pass", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
 	}

 	def jenisPekerjaanDataModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["GRP", "01:00", "00:11", "00:35", "00:40", "01:10", "01:10", "2"];
 		dataModel.addRow(data);
 		data = ["SBE", "01:00", "00:11", "00:35", "00:40", "01:10", "01:10", "1"];
 		dataModel.addRow(data);
 		data = ["SBI", "01:00", "00:11", "00:35", "00:40", "01:10", "01:10", "1"];
 		dataModel.addRow(data);
 		data = ["TWC", "01:00", "00:11", "00:35", "00:40", "01:10", "01:10", "3"];
 		dataModel.addRow(data);
 		data = ["RTJ", "01:00", "00:11", "00:35", "00:40", "01:10", "01:10", "3"];
 		dataModel.addRow(data);
 		data = ["PDI", "01:00", "00:11", "00:35", "00:40", "01:10", "01:10", "3"];
 		dataModel.addRow(data);	
 		return dataModel;
 	}

 	def jenisGatePassDataModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["Normal GR", "01:00", "00:09", "00:28", "-", "-", "-", "7"];
 		dataModel.addRow(data); 		
 		data = ["Diagnose GR", "01:00", "00:09", "00:28", "-", "-", "-", "7"];
 		dataModel.addRow(data); 		
 		data = ["Test Drive GR", "01:00", "00:09", "00:28", "-", "-", "-", "7"];
 		dataModel.addRow(data); 		
 		data = ["Test Drive BP", "01:00", "00:09", "00:28", "-", "-", "-", "7"];
 		dataModel.addRow(data); 		
 		return dataModel;
 	}

 	def totalDataModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["01:12", "00:05", "00:28", "01:10", "00:04", "00:32", "13"];
 		dataModel.addRow(data); 		 		
 		return dataModel;
 	}

 	def detailDataModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["B1234", "11111", "Avanza", "GRP", "Normal GR", "-", "-", "01-Jan-2011", "08:00 AM", "01-Jan-2011", "09:00 AM", "-", "-", "01:00", "-"];
 		dataModel.addRow(data); 		 		
 		data = ["B1234", "11111", "Avanza", "GRP", "Normal GR", "-", "-", "01-Jan-2011", "08:00 AM", "01-Jan-2011", "09:00 AM", "-", "-", "01:00", "-"];
 		dataModel.addRow(data); 		 		
 		data = ["B1234", "11111", "Avanza", "GRP", "Normal GR", "-", "-", "01-Jan-2011", "08:00 AM", "01-Jan-2011", "09:00 AM", "-", "-", "01:00", "-"];
 		dataModel.addRow(data); 		 		
 		data = ["B1234", "11111", "Avanza", "GRP", "Normal GR", "-", "-", "01-Jan-2011", "08:00 AM", "01-Jan-2011", "09:00 AM", "-", "-", "01:00", "-"];
 		dataModel.addRow(data); 		 		
 		data = ["B1234", "11111", "Avanza", "GRP", "Normal GR", "-", "-", "01-Jan-2011", "08:00 AM", "01-Jan-2011", "09:00 AM", "-", "-", "01:00", "-"];
 		dataModel.addRow(data); 		 		
 		return dataModel;
 	}

 	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

	def dataTablesJenisPekerjaan(){
		render gatePassService.datatablesJenisPekerjaan(params) as JSON;			
	}	
}
