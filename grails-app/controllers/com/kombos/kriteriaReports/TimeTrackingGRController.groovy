package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KategoriJob
import com.kombos.parts.Konversi
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class TimeTrackingGRController {

    def datatablesUtilService

    def timeTrackingGRService

    def jasperService
    def DefaultTableModel tableModel;
    def rootPath;
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }
    def previewData(){
        def jenisPekerjaan = "All", teknisi = "All", jenisKendaraan = "All", jobStopped = "All", sa = "All"
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
        params.file = ".xls"
        params.buntut = "application/vnd.ms-excel";
        rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";

        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        if(params.jenisPekerjaan){
            jenisPekerjaan = timeTrackingGRService.findJenisPekerjaan(params.jenisPekerjaan)
        }
        if(params.jenisKendaraan){
            jenisKendaraan = timeTrackingGRService.findJenisKendaraan(params.jenisKendaraan)
        }
        if(params.jobStopped){
            jobStopped = timeTrackingGRService.findJobStopped(params.jobStopped)
        }
        if(params.sa){
            sa = timeTrackingGRService.findTeknisiorSa(params.sa)
        }
        if(params.teknisi){
            teknisi = timeTrackingGRService.findTeknisiorSa(params.teknisi)
        }
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", CompanyDealer.findById(params.workshop as Long)?.m011NamaWorkshop);
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("jenisPekerjaan", jenisPekerjaan);
        parameters.put("jenisKendaraan", jenisKendaraan);
        parameters.put("jobStoppedStatus", jobStopped);
        parameters.put("serviceAdvisor", sa);
        parameters.put("technician", teknisi);

        JRDataSource dataSource = new JRTableModelDataSource(dataModelTimeTrackingGR(params));
        parameters.put("detailDataSource", dataSource);

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Time_Tracking_GR.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("Time_Tracking_GR", params.file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        response.setHeader("Content-Type", params.buntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        exporterXLS.exportReport();
    }
    def datatablesKendaraanList(){
        session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
        render timeTrackingGRService.datatablesKendaraanList(params) as JSON
    }

    def datatablesTechnisianList(){
        session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
        render timeTrackingGRService.datatablesTechnisianList(params) as JSON
    }

    def datatablesSAList(){
        session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
        render timeTrackingGRService.datatablesSAList(params) as JSON
    }

    def datatablesStoppedList(){
        session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
        render timeTrackingGRService.datatablesStoppedList(params) as JSON
    }
    def dataTablesJenisPekerjaan(){
        session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
        render timeTrackingGRService.datatablesJenisPekerjaan(params) as JSON;
    }

    def tableModelData(def params){
        def String[] columnNames = ["static_text"];
        def String[][] data = [["light"]];
        tableModel = new DefaultTableModel(data, columnNames);
    }
    def dataModelTimeTrackingGR(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
                "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
                "column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
                "column_31", "column_32", "column_33", "column_34", "column_35", "column_36", "column_37", "column_38", "column_39", "column_40",
                "column_41", "column_42", "column_43", "column_44", "column_45", "column_46", "column_47", "column_48", "column_49", "column_50",
                "column_51", "column_52", "column_53", "column_54", "column_55", "column_56", "column_57", "column_58", "column_59", "column_60",
                "column_61", "column_62", "column_63", "column_64", "column_65", "column_66", "column_67", "column_68", "column_69", "column_70",
                "column_71", "column_72", "column_73", "column_74", "column_75", "column_76"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = timeTrackingGRService.datatablesTimeTrackingGR(params);
            results.each {
                def Object[] object = [
                        String.valueOf(it.column_0?:""),
                        String.valueOf(it.column_1?:""),
                        String.valueOf(it.column_2?:""),
                        String.valueOf(it.column_3?:""),
                        String.valueOf(it.column_4?:""),
                        String.valueOf(it.column_5?:""),
                        String.valueOf(it.column_6?:""),
                        String.valueOf(it.column_7?:""),
                        String.valueOf(it.column_8?:""),
                        String.valueOf(it.column_9?:""),
                        String.valueOf(it.column_10?:""),
                        String.valueOf(it.column_11?:""),
                        String.valueOf(it.column_12?:""),
                        String.valueOf(it.column_13?:""),
                        String.valueOf(it.column_14?:""),
                        String.valueOf(it.column_15?:""),
                        String.valueOf(it.column_16?:""),
                        String.valueOf(it.column_17?:""),
                        String.valueOf(it.column_18?:""),
                        String.valueOf(it.column_19?:""),
                        String.valueOf(it.column_20?:""),
                        String.valueOf(it.column_21?:""),
                        String.valueOf(it.column_22?:""),
                        String.valueOf(it.column_23?:""),
                        String.valueOf(it.column_24?:""),
                        String.valueOf(it.column_25?:""),
                        String.valueOf(it.column_26?:""),
                        String.valueOf(it.column_27?:""),
                        String.valueOf(it.column_28?:""),
                        String.valueOf(it.column_29?:""),
                        String.valueOf(it.column_30?:""),
                        String.valueOf(it.column_31?:""),
                        String.valueOf(it.column_32?:""),
                        String.valueOf(it.column_33?:""),
                        String.valueOf(it.column_34?:""),
                        String.valueOf(it.column_35?:""),
                        String.valueOf(it.column_36?:""),
                        String.valueOf(it.column_37?:""),
                        String.valueOf(it.column_38?:""),
                        String.valueOf(it.column_39?:""),
                        String.valueOf(it.column_40?:""),
                        String.valueOf(it.column_41?:""),
                        String.valueOf(it.column_42?:""),
                        String.valueOf(it.column_43?:""),
                        String.valueOf(it.column_44?:""),
                        String.valueOf(it.column_45?:""),
                        String.valueOf(it.column_46?:""),
                        String.valueOf(it.column_47?:""),
                        String.valueOf(it.column_48?:""),
                        String.valueOf(it.column_49?:""),
                        String.valueOf(it.column_50?:""),
                        String.valueOf(it.column_51?:""),
                        String.valueOf(it.column_52?:""),
                        String.valueOf(it.column_53?:""),
                        String.valueOf(it.column_54?:""),
                        String.valueOf(it.column_55?:""),
                        String.valueOf(it.column_56?:""),
                        String.valueOf(it.column_57?:""),
                        String.valueOf(it.column_58?:""),
                        String.valueOf(it.column_59?:""),
                        String.valueOf(it.column_60?:""),
                        String.valueOf(it.column_61?:""),
                        String.valueOf(it.column_62?:""),
                        String.valueOf(it.column_63?:""),
                        String.valueOf(it.column_64?:""),
                        String.valueOf(it.column_65?:""),
                        String.valueOf(it.column_66?:""),
                        String.valueOf(it.column_67?:""),
                        String.valueOf(it.column_68?:""),
                        String.valueOf(it.column_69?:""),
                        String.valueOf(it.column_70?:""),
                        String.valueOf(it.column_71?:""),
                        String.valueOf(it.column_72?:""),
                        String.valueOf(it.column_73?:""),
                        String.valueOf(it.column_74?:""),
                        String.valueOf(it.column_75?:""),
                        String.valueOf(it.column_76?:"")
                ]
                model.addRow(object)
            }
        } catch(Exception e){
           println e
        }
        return model;
    }

}