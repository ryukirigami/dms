package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import javax.swing.table.DefaultTableModel

class Kr_mrs_kpiController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def mrsKpiService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";		
		if(params.namaReport == "01") {
			reportKpiProcess(params);
		} else
		if(params.namaReport == "02") {
			reportDirectMail(params);
		} else
		if(params.namaReport == "03") {
			reportKpiSms(params);
		} else
		if(params.namaReport == "04") {
			reportKpiCall(params);
		} else
		if(params.namaReport == "05") {
			reportKpiAppointment(params);
		} else
		if(params.namaReport == "06") {
			reportKpiResult(params);
		} else
		if(params.namaReport == "07") {
			reportAppointmentContribution(params);
		} else
		if(params.namaReport == "08") {
			reportSbeContribution(params);
		} else
		if(params.namaReport == "09") {
			reportUnitEntryContribution(params);
		} else
		if(params.namaReport == "10") {
			reportKpiProcessDetail(params);
		} else 
		if(params.namaReport == "11") {
			reportKpiTotal(params);
		} else 
		if(params.namaReport == "12") {
			reportSbRetention(params);
		} else 
		if(params.namaReport == "13") {
			reportSbRetentionPeriod(params);
		}
	}
	
	def reportKpiProcess(def params){
	}
	
	def reportDirectMail(def params){
	}
	
	def reportKpiSms(def params){
	}
	
	def reportKpiCall(def params){
	}
	
	def reportKpiAppointment(def params){
	}
	
	def reportKpiResult(def params){
	}
	
	def reportAppointmentContribution(def params){
	}
	
	def reportSbeContribution(def params){
	}
	
	def reportUnitEntryContribution(def params){
	}
	
	def reportKpiProcessDetail(def params){
	}
	
	def reportKpiTotal(def params){
	}
	
	def reportSbRetention(def params){
	}
	
	def reportSbRetentionPeriod(def params){
	}
	
	def dataModelKpiProcess(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", 
			"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", 
			"column_21", "column_22", "column_23", "column_24", "column_25"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
	}
	
	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}
}