package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_customer_followup_bpController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
    def datatablesUtilService;
    def customerFollowupBPService;
    def jasperService;
    def rootPath;
    def formatFile;
    def formatBuntut;
    def DefaultTableModel tableModel;
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
    static viewPermissions = ['index', 'list', 'datatablesList'];

    def index() {}

    def previewData(){
        rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
        params.companyDealer = session?.userCompanyDealer;
        if(params.format=="x"){
            formatFile = ".xls";
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }

        if(params?.workshop){
            params.companyDealer = CompanyDealer?.get(params?.workshop?.toLong())
        }
        if(params.namaReport == "01") {
            reportBPCustomerFollowupDaily(params);
        } else
        if(params.namaReport == "02") {
            reportBPCustomerFollowupMonthly(params);
        } else
        if(params.namaReport == "03") {
            reportBPCustomerFollowupPhone(params);
        } else
        if(params.namaReport == "04") {
            reportBPCustomerFollowupSMS(params);
        }

    }

    def reportBPCustomerFollowupDaily(def params){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        def result = customerFollowupBPService.getWorkshopByCode(params.workshop);
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : customerFollowupBPService.getServiceAdvisorById(params.sa)));
        parameters.put("kategoriWorkshop", "Body & Paint");
        parameters.put("report", "Daily");

        JRDataSource dataSource = new JRTableModelDataSource(dataModelBPCustomerFollowupSummaryDaily(params));
        parameters.put("customerFollowupSummary", dataSource);

        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(dataModelBPCustomerFollowupDetail(params,"daily"));
            parameters.put("customerFollowupDetail", dataSource);
        }
        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Customer_Followup_Daily.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("BP_Customer_Followup_Daily", formatFile);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", formatBuntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

    def reportBPCustomerFollowupMonthly(def params){
        DateFormat df = new SimpleDateFormat("M yyyy");
        DateFormat df1 = new SimpleDateFormat("MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def bulan1 = params.bulan1 + " " + params.tahun
        def bulan2 = params.bulan2 + " " + params.tahun
        def startDate = df.parse(bulan1);
        def endDate = df.parse(bulan2);

        def result = customerFollowupBPService.getWorkshopByCode(params.workshop);
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : customerFollowupBPService.getServiceAdvisorById(params.sa)));
        parameters.put("kategoriWorkshop", "Body & Paint");
        parameters.put("report", "Monthly");

        JRDataSource dataSource = new JRTableModelDataSource(dataModelBPCustomerFollowupGraph(params));
        parameters.put("customerFollowupGraph", dataSource);

        dataSource = new JRTableModelDataSource(dataModelBPCustomerFollowupSummaryMonthly(params));
        parameters.put("customerFollowupSummary", dataSource);

        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(dataModelBPCustomerFollowupDetail(params,"monthly"));
            parameters.put("customerFollowupDetail", dataSource);
        }
        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Customer_Followup_Monthly.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("BP_Customer_Followup_Monthly", formatFile);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", formatBuntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

    def reportBPCustomerFollowupPhone(def params){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        def result = customerFollowupBPService.getWorkshopByCode(params.workshop);
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : customerFollowupBPService.getServiceAdvisorById(params.sa)));
        parameters.put("kategoriWorkshop", "Body & Paint");

        JRDataSource dataSource = new JRTableModelDataSource(dataModelBPCustomerFollowupPhone(params));
        parameters.put("detailDataSource", dataSource);

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Customer_Followup_Phone_Detail.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("BP_Customer_Followup_Phone_Detail", formatFile);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", formatBuntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

    def reportBPCustomerFollowupSMS(def params){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        def result = customerFollowupBPService.getWorkshopByCode(params.workshop);
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : customerFollowupBPService.getServiceAdvisorById(params.sa)));
        parameters.put("kategoriWorkshop", "Body & Paint");

        JRDataSource dataSource = new JRTableModelDataSource(dataModelBPCustomerFollowupSMS(params));
        parameters.put("detailDataSource", dataSource);

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Customer_Followup_SMS_Detail.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("BP_Customer_Followup_SMS_Detail", formatFile);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", formatBuntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

    def dataModelBPCustomerFollowupSummaryDaily(def params){
        def String[] columnNames = ["column_0", "column_1" ,"column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
                "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
                "column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
                "column_31", "column_32"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = customerFollowupBPService.datatablesBPCustomerFollowupSummary(params, 0);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        Integer.parseInt(String.valueOf(result.get("column_1"))),
                        Integer.parseInt(String.valueOf(result.get("column_2"))),
                        Integer.parseInt(String.valueOf(result.get("column_3"))),
                        Integer.parseInt(String.valueOf(result.get("column_4"))),
                        Integer.parseInt(String.valueOf(result.get("column_5"))),
                        Integer.parseInt(String.valueOf(result.get("column_6"))),
                        Integer.parseInt(String.valueOf(result.get("column_7"))),
                        Integer.parseInt(String.valueOf(result.get("column_8"))),
                        Integer.parseInt(String.valueOf(result.get("column_9"))),
                        Integer.parseInt(String.valueOf(result.get("column_10"))),
                        Integer.parseInt(String.valueOf(result.get("column_11"))),
                        Integer.parseInt(String.valueOf(result.get("column_12"))),
                        Integer.parseInt(String.valueOf(result.get("column_13"))),
                        Integer.parseInt(String.valueOf(result.get("column_14"))),
                        Integer.parseInt(String.valueOf(result.get("column_15"))),
                        Integer.parseInt(String.valueOf(result.get("column_16"))),
                        Integer.parseInt(String.valueOf(result.get("column_17"))),
                        Integer.parseInt(String.valueOf(result.get("column_18")))
                ]
                model.addRow(object)
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelBPCustomerFollowupSummaryMonthly(def params){
        def String[] columnNames = ["column_0", "column_1" ,"column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
                "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
                "column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
                "column_31", "column_32", "column_33"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = customerFollowupBPService.datatablesBPCustomerFollowupSummaryMonth(params, 1);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
                        Integer.parseInt(String.valueOf(result.get("column_2"))),
                        Integer.parseInt(String.valueOf(result.get("column_3"))),
                        Integer.parseInt(String.valueOf(result.get("column_4"))),
                        Integer.parseInt(String.valueOf(result.get("column_5"))),
                        Integer.parseInt(String.valueOf(result.get("column_6"))),
                        Integer.parseInt(String.valueOf(result.get("column_7"))),
                        Integer.parseInt(String.valueOf(result.get("column_8"))),
                        Integer.parseInt(String.valueOf(result.get("column_9"))),
                        Integer.parseInt(String.valueOf(result.get("column_10"))),
                        Integer.parseInt(String.valueOf(result.get("column_11"))),
                        Integer.parseInt(String.valueOf(result.get("column_12"))),
                        Integer.parseInt(String.valueOf(result.get("column_13"))),
                        Integer.parseInt(String.valueOf(result.get("column_14"))),
                        Integer.parseInt(String.valueOf(result.get("column_15"))),
                        Integer.parseInt(String.valueOf(result.get("column_16"))),
                        Integer.parseInt(String.valueOf(result.get("column_17"))),
                        Integer.parseInt(String.valueOf(result.get("column_18"))),
                        Integer.parseInt(String.valueOf(result.get("column_19")))
                ]
                model.addRow(object)
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelBPCustomerFollowupDetail(def params,String dari){
        def String[] columnNames = ["column_0", "column_1" ,"column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
                "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
                "column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
                "column_31", "column_32"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = new ArrayList();
            if(dari=="daily"){
                results = customerFollowupBPService.datatablesBPCustomerFollowupDetail(params, 0);
                for(Map<String, Object> result : results) {
                    def Object[] object = [
                            String.valueOf(result.get("column_0")),
                            String.valueOf(result.get("column_1")),
                            String.valueOf(result.get("column_2")),
                            Integer.parseInt(String.valueOf(result.get("column_3"))),
                            Integer.parseInt(String.valueOf(result.get("column_4"))),
                            Integer.parseInt(String.valueOf(result.get("column_5"))),
                            Integer.parseInt(String.valueOf(result.get("column_6"))),
                            Integer.parseInt(String.valueOf(result.get("column_7"))),
                            Integer.parseInt(String.valueOf(result.get("column_8"))),
                            Integer.parseInt(String.valueOf(result.get("column_9"))),
                            Integer.parseInt(String.valueOf(result.get("column_10"))),
                            Integer.parseInt(String.valueOf(result.get("column_11"))),
                            Integer.parseInt(String.valueOf(result.get("column_12"))),
                            Integer.parseInt(String.valueOf(result.get("column_13"))),
                            Integer.parseInt(String.valueOf(result.get("column_14"))),
                            Integer.parseInt(String.valueOf(result.get("column_15"))),
                            Integer.parseInt(String.valueOf(result.get("column_16"))),
                            Integer.parseInt(String.valueOf(result.get("column_17"))),
                            Integer.parseInt(String.valueOf(result.get("column_18"))),
                            Integer.parseInt(String.valueOf(result.get("column_19"))),
                            Integer.parseInt(String.valueOf(result.get("column_20")))
                    ]
                    model.addRow(object)
                }
            }else{
                results = customerFollowupBPService.datatablesBPCustomerFollowupDetailMonthly(params, 0);
                for(Map<String, Object> result : results) {
                    def Object[] object = [
                            String.valueOf(result.get("column_0")),
                            String.valueOf(result.get("column_1")),
                            String.valueOf(result.get("column_2")),
                            Integer.parseInt(String.valueOf(result.get("column_3"))),
                            Integer.parseInt(String.valueOf(result.get("column_4"))),
                            Integer.parseInt(String.valueOf(result.get("column_5"))),
                            Integer.parseInt(String.valueOf(result.get("column_6"))),
                            Integer.parseInt(String.valueOf(result.get("column_7"))),
                            Integer.parseInt(String.valueOf(result.get("column_8"))),
                            Integer.parseInt(String.valueOf(result.get("column_9"))),
                            Integer.parseInt(String.valueOf(result.get("column_10"))),
                            Integer.parseInt(String.valueOf(result.get("column_11"))),
                            Integer.parseInt(String.valueOf(result.get("column_12"))),
                            Integer.parseInt(String.valueOf(result.get("column_13"))),
                            Integer.parseInt(String.valueOf(result.get("column_14"))),
                            Integer.parseInt(String.valueOf(result.get("column_15"))),
                            Integer.parseInt(String.valueOf(result.get("column_16"))),
                            Integer.parseInt(String.valueOf(result.get("column_17"))),
                            Integer.parseInt(String.valueOf(result.get("column_18"))),
                            Integer.parseInt(String.valueOf(result.get("column_19"))),
                            Integer.parseInt(String.valueOf(result.get("column_20")))
                    ]
                    model.addRow(object)
                }
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelBPCustomerFollowupGraph(def params) {
        def String[] columnNames = ["column_0", "column_1", "column_2"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = customerFollowupBPService.datatablesBPCustomerFollowupGraph(params);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        Integer.parseInt(String.valueOf(result.get("column_1"))),
                        String.valueOf("SUCCESS CALL RATE")
                ]
                model.addRow(object)
                object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        Integer.parseInt(String.valueOf(result.get("column_2"))),
                        String.valueOf("FOLLOWUP PHONE RATE")
                ]
                model.addRow(object)
                object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        Integer.parseInt(String.valueOf(result.get("column_3"))),
                        String.valueOf("SUCCESS DELIVERED RATE")
                ]
                model.addRow(object)
                object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        Integer.parseInt(String.valueOf(result.get("column_4"))),
                        String.valueOf("FOLLOWUP SMS RATE")
                ]
                model.addRow(object)
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelBPCustomerFollowupPhone(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = customerFollowupBPService.datatablesBPCustomerFollowupPhone(params, 0);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(result.get("column_0")),
                        String.valueOf(result.get("column_1")),
                        String.valueOf(result.get("column_2")),
                        String.valueOf(result.get("column_3")),
                        String.valueOf(result.get("column_4")),
                        String.valueOf(result.get("column_5")),
                        String.valueOf(result.get("column_6")),
                        String.valueOf(result.get("column_7"))
                ]
                model.addRow(object)
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelBPCustomerFollowupSMS(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = customerFollowupBPService.datatablesBPCustomerFollowupPhone(params, 1);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(result.get("column_0")),
                        String.valueOf(result.get("column_1")),
                        String.valueOf(result.get("column_2")),
                        String.valueOf(result.get("column_3")),
                        String.valueOf(result.get("column_4")),
                        String.valueOf(result.get("column_5")),
                        String.valueOf(result.get("column_6")),
                        String.valueOf(result.get("column_7"))
                ]
                model.addRow(object)
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def datatablesSAList(){
		session.exportParams=params;
		render customerFollowupBPService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render customerFollowupBPService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render customerFollowupBPService.datatablesJenisPekerjaan(params) as JSON;			
	}
    def tableModelData(def params){
        def String[] columnNames = ["static_text"];
        def String[][] data = [["light"]];
        tableModel = new DefaultTableModel(data, columnNames);
    }

}