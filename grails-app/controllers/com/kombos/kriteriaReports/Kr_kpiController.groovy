package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_kpiController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def kpiService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01") {
			reportTotalKPIGeneralRepair(params);
		} else
		if(params.namaReport == "02") {
			reportTotalKPIBodyPaint(params);
		}
	}
	
	def reportTotalKPIGeneralRepair(def params){
		def result = kpiService.getWorkshopByCode(params.workshop);

		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";

		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelTotalKPIGeneralRepair(params));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Total_KPI_General_Repair.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Total_KPI_General_Repair", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();	
	}
	
	def reportTotalKPIBodyPaint(def params){
		def result = kpiService.getWorkshopByCode(params.workshop);
	
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelTotalKPIBodyPaint(params));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Total_KPI_Body_Paint.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Total_KPI_Body_Paint", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();	
	}
	
	def dataModelTotalKPIGeneralRepair(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
					"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
					"column_31", "column_32", "column_33", "column_34", "column_35"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def result = kpiService.datatablesTotalKPIGeneralRepair(params);			
			model.addRow(result)			
		} catch(Exception e){
			e.printStackTrace();					
		}
		return model;		
	}
	
	def dataModelTotalKPIBodyPaint(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
					"column_21", "column_22", "column_23", "column_24", "column_25", "column_26"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def result = kpiService.datatablesTotalKPIBodyPaint(params);			
			model.addRow(result)				
		} catch(Exception e){
			e.printStackTrace();						
		}
		return model;		
	}
	
	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

}