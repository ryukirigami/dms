package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class BusinessTransactionController {


  def BusinessTransactionService;


    def rootPath;
    def DefaultTableModel tableModel;
    def sessionFactory;



    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index','list','datatablesList']



    def index() {}


   def previewData(){
       List<JasperReportDef> reportDefList = []
       List<JasperReportDef> reportDefList2 = []
       def file = null
       def file2=null
       def reportData = calculateReportData(params)
       def formatFile = ""
       if(params.format=="x"){
           formatFile = ".xls"
       }else{
           formatFile = ".pdf"
       }

       if(params.namaReport=="01") {
           def reportDef = new JasperReportDef(name:'Transaction_Part.jasper',
                   fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                   reportData: reportData
           )
           reportDefList.add(reportDef)
           file = File.createTempFile("Transaction_Part",formatFile)
       }

      else if(params.namaReport=="02"){
           def reportDef = new JasperReportDef(name:'Transaction_Business.jasper',
                   fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                   reportData: reportData
           )
           reportDefList.add(reportDef)
           file = File.createTempFile("Transaction_Business",formatFile)

       }

       file.deleteOnExit() //Aktifkan dulu
       FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
       if(params.format=="x"){
           response.setHeader("Content-Type", "application/vnd.ms-excel")
       }else {
           response.setHeader("Content-Type", "application/pdf")
       }
       response.setHeader("Content-disposition", "attachment;filename=${file.name}")
       response.outputStream << file.newInputStream()

    }



    def calculateReportData(def params){
        def reportData = new ArrayList();
        if(params.namaReport=="01"){
            def cd =CompanyDealer.findById(params.workshop)
            final session = sessionFactory.currentSession
            String query=" SELECT  A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'DD-MM-YYYY') AS TGL_WO, C.M111_ID, C.M111_NAMA," +
                    " C.M111_ID AS PART_NO_SUPPLIED, C.M111_NAMA AS PART_DESC_SUPPLIED," +
                    " TO_CHAR(B.DATE_CREATED,'DD-MM-YYYY') AS REQUEST_DATE, B.T403_JUMLAH1 AS REQUEST_QTY," +
                    " TO_CHAR(E.T141_TGLPICKING,'DD-MM-YYYY') AS SUPPLIED_DATE, F.T142_QTY1,M118_SATUAN1" +
                    " FROM T401_RECEPTION A" +
                    " INNER JOIN T403_PARTSRCP B ON A.ID = B.T403_T401_NOWO" +
                    " INNER JOIN M111_GOODS C ON B.T403_M111_ID = C.ID" +
                    " INNER JOIN M118_SATUAN D ON C.M111_M118_ID = D.ID" +
                    " INNER JOIN T141_PICKINGSLIP E ON A.ID = E.T141_T401_NOWO" +
                    " INNER JOIN T142_PICKINGSLIPDETAIL F ON E.ID = F.T142_T141_ID" +
                    " WHERE A.COMPANY_DEALER_ID = "+params.workshop+" AND F.T142_M111_ID=B.T403_M111_ID" +
                    " AND A.COMPANY_DEALER_ID = "+params.workshop+" AND A.T401_STADEL='0' AND B.T403_STADEL = '0'" +
                    " AND E.T141_STADEL ='0' " +
                    " AND A.T401_TANGGALWO BETWEEN  TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY') "+
                    " ORDER BY A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'DD-MM-YYYY')";

            def results = null

            try {
                final sqlQuery = session.createSQLQuery(query)
                final queryResults = sqlQuery.with {
                    list()
                }
              //def data=[:]
                results = queryResults.collect
                        { resultRow ->
                    [       workshop : cd.m011NamaWorkshop,
                            column_0: resultRow[0],
                            column_1: resultRow[1],
                            column_2: resultRow[2],
                            column_3: resultRow[3],
                            column_4: resultRow[4],
                            column_5: resultRow[5],
                            column_6: resultRow[6],
                            column_7: resultRow[7],
                            column_8: resultRow[8],
                            column_9: resultRow[9],
                            column_10: resultRow[10],
                            column_11: "",
                            column_12: "",
                            column_13: "",
                    ]

                }
             if(results.size()>0){
                 results.each {
                     def data=[:]
                     data.put("workshop",it.workshop)
                     data.put("tanggal",params.tanggal)
                     data.put("tanggal2",params.tanggal2)
                     data.put("column_1",it.column_0)
                     data.put("column_2",it.column_1)
                     data.put("column_3",it.column_2)
                     data.put("column_4",it.column_3)
                     data.put("column_5",it.column_4)
                     data.put("column_6",it.column_5)
                     data.put("column_7",it.column_6)
                     data.put("column_8",it.column_7)
                     data.put("column_9",it.column_8)
                     data.put("column_10",it.column_9)
                     data.put("column_11",it.column_10)
                     data.put("column_12",it.column_11)
                     data.put("column_13",it.column_12)
                     data.put("column_14",it.column_13)


                     reportData.add(data)
                 }
             }

               else{
                  def data=[:]
                   data.put("workshop",cd.m011NamaWorkshop)
                   data.put("tanggal",params.tanggal)
                 data.put("tanggal2",params.tanggal2)
                   data.put("column_1","-")
                   data.put("column_2","-")
                   data.put("column_3","-")
                   data.put("column_4","-")
                   data.put("column_5","-")
                   data.put("column_6","-")
                   data.put("column_7","-")
                   data.put("column_8","-")
                   data.put("column_8","-")
                   data.put("column_9","-")

                 reportData.add(data)

               }

            } catch(Exception e){
                throw new java.lang.Exception(e.getMessage());

            }

            if(results.size() <= 0) {
                println("Result size 0");
            }


            return reportData
        }
   else if(params.namaReport=="02"){
            def cd =CompanyDealer.findById(params.workshop)
            final session = sessionFactory.currentSession
            String query=" SELECT  A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'DD-MM-YYYY') AS TGL_WO," +
                    " C.M116_ID||''|| B.T183_NOPOLTENGAH||''|| B.T183_NOPOLBELAKANG AS NOPOL," +
                    " D.T103_VINCODE, M102_BASEMODEL.M102_NAMABASEMODEL AS VEHICLE_TYPE," +
                    " DECODE(SUBSTR(F.M053_NAMAOPERATION,1,2),'EM',1,0) AS Is_EM," +
                    " DECODE(SUBSTR(G.T400_NOMORANTRIAN,1,1),'W','WALK IN','BOOKING') AS WO_TYPE," +
                    " F.M053_NAMAOPERATION, E.T402_RATE, NVL(A.T401_STATWC,0)" +
                    " FROM T401_RECEPTION A" +
                    " INNER JOIN T183_HISTORYCUSTOMERVEHICLE B ON A.T401_T183_ID = B.ID" +
                    " INNER JOIN M116_KODEKOTANOPOL C ON B.T183_M116_ID = C.ID" +
                    " INNER JOIN T103_CUSTOMERVEHICLE D ON B.T183_T103_VINCODE = D.ID" +
                    " LEFT JOIN T110_FULLMODELCODE ON B.T183_T110_FULLMODELCODE = T110_FULLMODELCODE.ID" +
                    " LEFT JOIN M102_BASEMODEL ON T110_FULLMODELCODE.T110_M102_ID = M102_BASEMODEL.ID" +
                    " INNER JOIN T402_JOBRCP E ON A.ID = E.T402_T401_NOWO" +
                    " INNER JOIN M053_OPERATION F ON E.T402_M053_JOBID = F.ID" +
                    " LEFT JOIN T400_CUSTOMERIN G ON T401_T400_ID = G.ID" +
                    " WHERE A.COMPANY_DEALER_ID = "+params.workshop+"" +
                    " AND A.T401_TANGGALWO BETWEEN  TO_DATE('"+params.tanggal+"','DD-MM-YYYY') AND TO_DATE('"+params.tanggal2+"','DD-MM-YYYY') "+
                    " ORDER BY A.T401_NOWO, TO_CHAR(A.T401_TANGGALWO,'DD-MM-YYYY')";

            def results = null
            try {
                final sqlQuery = session.createSQLQuery(query)
                final queryResults = sqlQuery.with {
                    list()
                }

                results = queryResults.collect
                        { resultRow ->
                            [
//                                    workshop : cd.m011NamaWorkshop,
//                                    column_0: resultRow[0],
//                                    column_1: resultRow[1],
//                                    column_2: resultRow[2],
//                                    column_3: resultRow[3],
//                                    column_4: resultRow[4],
//                                    column_5: resultRow[5],
//                                    column_6: resultRow[6],
//                                    column_7: resultRow[7],
//                                    column_8: resultRow[8],
//                                    column_9: resultRow[9],
//                                    column_10:"",
//                                    column_11:"",
//                                    column_12:"",
//                                    column_13:"",
                                    workshop : cd.m011NamaWorkshop,
                                    column_0: resultRow[0],
                                    column_1: resultRow[1],
                                    column_2: resultRow[2],
                                    column_3: resultRow[3],
                                    column_4: resultRow[4],
                                    column_5: resultRow[5],
                                    column_6: resultRow[6],
                                    column_7: resultRow[7],
                                    column_8: "-",
                                    column_9: resultRow[8],
                                    column_10:"",
                                    column_11:"",
                                    column_12:"",
                                    column_13:resultRow[9],


                            ]



                        }
                if(results.size()>0){
                    results.each {
                        def data=[:]
                        data.put("workshop",it.workshop)
                        data.put("tanggal",params.tanggal)
                        data.put("tanggal2",params.tanggal2)
                        data.put("column_1",it.column_0)
                        data.put("column_2",it.column_1)
                        data.put("column_3",it.column_2)
                        data.put("column_4",it.column_3)
                        data.put("column_5",it.column_4)
                        data.put("column_6",it.column_5)
                        data.put("column_7",it.column_6)
                        data.put("column_8",it.column_7)
                        data.put("column_9",it.column_8)
                        data.put("column_10",it.column_9)
                        data.put("column_11",it.column_10)
                        data.put("column_12",it.column_11)
                        data.put("column_13",it.column_12)
                        data.put("column_14",it.column_13)
                        reportData.add(data)
                    }
                }

                else{
                    def data=[:]
                    data.put("workshop",cd.m011NamaWorkshop)
                    data.put("tanggal",params.tanggal)
                    data.put("tanggal2",params.tanggal2)
                    data.put("column_1","-")
                    data.put("column_2","-")
                    data.put("column_3","-")
                    data.put("column_4","-")
                    data.put("column_5","-")
                    data.put("column_6","-")
                    data.put("column_7","-")
                    data.put("column_8","-")
                    data.put("column_9","-")
                    data.put("column_10","-")
                    data.put("column_11","-")
                    data.put("column_12","-")
                    data.put("column_13","-")
                    data.put("column_14","-")
                    reportData.add(data)

                }

            } catch(Exception e){
                throw new java.lang.Exception(e.getMessage());

            }
            if(results.size() <= 0) {
                println("Result size 0");
            }


            return reportData

        }

    }



}
