package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import javax.swing.table.DefaultTableModel

class Kr_goods_bpController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def goodsBPService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
	}

	def datatablesSAList(){
		session.exportParams=params;
		render goodsBPService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render goodsBPService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render goodsBPService.datatablesJenisPekerjaan(params) as JSON;			
	}

}