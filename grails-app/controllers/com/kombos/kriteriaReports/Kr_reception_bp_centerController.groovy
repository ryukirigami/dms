package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import javax.swing.table.DefaultTableModel

class Kr_reception_bp_centerController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def receptionBPCenterService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";		
		if(params.namaReport == "01") {
			reportReceptionLeadTimeDaily(params);
		} else
		if(params.namaReport == "02") {
			reportReceptionLeadTimeMonthly(params);
		} else
		if(params.namaReport == "03") {
			reportReceptionLeadTimeYearly(params);
		} else
		if(params.namaReport == "04") {
			reportUnitEntryCompositionOnBPCenter(params);
		} else
		if(params.namaReport == "05") {
			reportUnitEntryCompositionOnJobType(params);
		} else
		if(params.namaReport == "06") {
			reportUnitEntryCompositionOnPaymentMethod(params);
		} else
		if(params.namaReport == "07") {
			reportUnitEntryCompositionOnDamageComposition(params);
		} else
		if(params.namaReport == "08") {
			reportUnitEntryCompositionOnSA(params);
		} else
		if(params.namaReport == "09") {
			reportInsuranceApprovalLeadTimeDaily(params);
		} else
		if(params.namaReport == "10") {
			reportInsuranceApprovalLeadTimeMonthly(params);
		} else
		if(params.namaReport == "11") {
			reportInsuranceApprovalLeadTimeYearly(params);
		} else
		if(params.namaReport == "12") {
			reportPotentialLostSalesDaily(params);
		} else
		if(params.namaReport == "13") {
			reportPotentialLostSalesMonthly(params);
		} else
		if(params.namaReport == "14") {
			reportAdditionalSPK(params);
		}
	}	 

	def reportReceptionLeadTimeDaily(def params){

	}

	def reportReceptionLeadTimeMonthly(def params){
		
	}

	def reportReceptionLeadTimeYearly(def params){
		
	}

	def reportUnitEntryCompositionOnBPCenter(def params){
		
	}

	def reportUnitEntryCompositionOnJobType(def params){
		
	}

	def reportUnitEntryCompositionOnPaymentMethod(def params){
		
	}

	def reportUnitEntryCompositionOnDamageComposition(def params){
		
	}

	def reportUnitEntryCompositionOnSA(def params){
		
	}

	def reportInsuranceApprovalLeadTimeDaily(def params){
		
	}

	def reportInsuranceApprovalLeadTimeMonthly(def params){
		
	}

	def reportInsuranceApprovalLeadTimeYearly(def params){
		
	}

	def reportPotentialLostSalesDaily(def params){
		
	}

	def reportPotentialLostSalesMonthly(def params){
		
	}

	def reportAdditionalSPK(def params){
		
	}

	

	def datatablesSAList(){
		session.exportParams=params;
		render receptionBPCenterService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render receptionBPCenterService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render receptionBPCenterService.datatablesJenisPekerjaan(params) as JSON;			
	}

	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

}