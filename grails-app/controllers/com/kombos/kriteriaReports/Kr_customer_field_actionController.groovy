package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.maintable.JenisFA
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter
import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_customer_field_actionController {

    def jasperService
    def customerFAService
    def rootPath;
    def formatFile;
    def formatBuntut;
    def DefaultTableModel tableModel;
    def staBooking = ['Yes','No','All'];
    def faStatus = ['Finish','Not Yet','All'];
    def index() {
        def blnSkrg = -1
        def skrg = new Date().clearTime()
        if(skrg.format("MM")=="01"){
            blnSkrg = 12
        }else{
            blnSkrg = skrg.format("MM").toInteger()-1
        }

        [bulanCari : blnSkrg]
    }

    def previewData(){

        rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
        params.companyDealer = CompanyDealer?.get(params?.companyDealer?.toLong());
        if(params.format=="x"){
            formatFile = ".xls";
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }

        if(params.namaReport == "FA_DAILY") {
            reportFADaily(params);
        } else {
            reportFAMonthly(params);
        } 

    }

    def listJenisFA(params){
        String hasil = ""
        def listID = []
        listID = params?.jenisFA?.toString()?.split(",")
        listID.each {
            def jenisFA = JenisFA.findById(it?.toLong());
            hasil+=jenisFA?.m184NamaJenisFA+","
        }
        hasil = hasil.substring(0,hasil.length()-1);
        return hasil
    }

    def reportFADaily(def params){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggalAkhir);
        String mainDealer = params?.mainDealer
        mainDealer = mainDealer.replaceAll("\'","");
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", params?.companyDealer?.m011NamaWorkshop);
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("report", "Daily");
        parameters.put("statusBooking", staBooking[params?.staBooking?.toInteger()]);
        parameters.put("faStatus", faStatus[params?.faStatus?.toInteger()]);
        parameters.put("jenisFA", params?.jenisFA=="" ? "All" : listJenisFA(params));
        parameters.put("mainDealer", params?.mainDealer=="" ? "All" : mainDealer);

        JRDataSource dataSource = new JRTableModelDataSource(dataModelCustomerFADaily(params));
        parameters.put("detailDataSource", dataSource);

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Customer_Field_Action.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("Customer_Field_Action_Daily_", formatFile);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", formatBuntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

    def reportFAMonthly(def params){
        DateFormat df = new SimpleDateFormat("M yyyy");
        DateFormat df1 = new SimpleDateFormat("MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def bulan1 = params.bulan + " " + params.tahun
        def bulan2 = params.bulan2 + " " + params.tahun
        def startDate = df.parse(bulan1);
        def endDate = df.parse(bulan2);
        String mainDealer = params?.mainDealer
        mainDealer = mainDealer.replaceAll("\'","");

        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", params?.companyDealer?.m011NamaWorkshop);
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("jenisFA", params?.jenisFA=="" ? "All" : listJenisFA(params));
        parameters.put("mainDealer", params?.mainDealer=="" ? "All" : mainDealer);
        parameters.put("report", "Monthly");

        JRDataSource dataSource = new JRTableModelDataSource(dataModelCustomerFAMonthly(params));
        parameters.put("detailDataSource", dataSource);

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Customer_Field_Action_Monthly.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("Customer_Field_Action_Monthly", formatFile);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", formatBuntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

    def dataModelCustomerFADaily(def params){
        def String[] columnNames = ["column_0", "column_1" ,"column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
                "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
                "column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
                "column_31", "column_32"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = customerFAService.datatablesCustomerFADaily(params, 0);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        String.valueOf(result.get("column_1")),
                        String.valueOf(result.get("column_2")),
                        String.valueOf(result.get("column_3")),
                        String.valueOf(result.get("column_4")),
                        String.valueOf(result.get("column_5")),
                        String.valueOf(result.get("column_6")),
                        String.valueOf(result.get("column_7")),
                        String.valueOf(result.get("column_8")),
                        String.valueOf(result.get("column_9")),
                        String.valueOf(result.get("column_10")),
                        String.valueOf(result.get("column_11")),
                        String.valueOf(result.get("column_12")),
                        String.valueOf(result.get("column_13")),
                        String.valueOf(result.get("column_14")),
                        String.valueOf(result.get("column_15")),
                        String.valueOf(result.get("column_16")),
                        String.valueOf(result.get("column_17")),
                        String.valueOf(result.get("column_18")),
                        String.valueOf(result.get("column_19"))
                ]
                model.addRow(object)
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelCustomerFAMonthly(def params){
        def String[] columnNames = ["column_0", "column_1" ,"column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = customerFAService.datatablesCustomerFAMonthly(params, 1);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
                        String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
                        String.valueOf(result.get("column_2")),
                        String.valueOf(result.get("column_3")),
                        String.valueOf(result.get("column_4")).toInteger(),
                        String.valueOf(result.get("column_5")).toInteger(),
                        String.valueOf(result.get("column_6")).toInteger(),
                        String.valueOf(result.get("column_7")).toInteger()
                ]
                model.addRow(object)
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def datatablesList() {

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams=params

        def c = JenisFA.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params."sCriteria_m184KodeJenisFA"){
                ilike("m184KodeJenisFA","%" + (params."sCriteria_m184KodeJenisFA" as String) + "%")
            }


            if(params."sCriteria_m184NamaJenisFA"){
                ilike("m184NamaJenisFA","%" + (params."sCriteria_m184NamaJenisFA" as String) + "%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m184KodeJenisFA: it?.m184KodeJenisFA,

                    m184NamaJenisFA: it.m184NamaJenisFA

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def tableModelData(def params){
        def String[] columnNames = ["static_text"];
        def String[][] data = [["light"]];
        tableModel = new DefaultTableModel(data, columnNames);
    }
}
