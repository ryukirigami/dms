package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_goods_grController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def goodsGRService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01") {
			reportGRGoodsWeekly(params);
		} else
		if(params.namaReport == "02") {
			reportGRGoodsMonthly(params);
		}
	}
	
	def reportGRGoodsWeekly(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = goodsGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
//		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : goodsGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Weekly");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelGRGoodsWeekly(params));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Goods_Weekly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Goods_Weekly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportGRGoodsMonthly(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = goodsGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Monthly");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelGRGoodsMonthly(params));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Goods_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Goods_Monthly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def dataModelGRGoodsWeekly(def params){
        		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = goodsGRService.datatablesGRGoodsWeekly(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;	
	}
	
	def dataModelGRGoodsMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5",
			"column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13",
			"column_14", "column_15"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = goodsGRService.datatablesGRGoodsMonthly(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ?  result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ?  result.get("column_1") : ""),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					String.valueOf(result.get("column_3") != null ?  result.get("column_3") : ""),
					Integer.parseInt(String.valueOf(result.get("column_4"))),					
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
	}

	def datatablesSAList(){
		render goodsGRService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render goodsGRService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render goodsGRService.datatablesJenisPekerjaan(params) as JSON;			
	}
	
	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

}