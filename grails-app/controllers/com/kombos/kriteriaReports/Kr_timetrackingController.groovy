package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_timetrackingController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def timeTrackingService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01") {
			reportTimeTrackingGR(params);
		} else 
		if(params.namaReport == "02") {
			reportTimeTrackingBP(params);
		} else
		if(params.namaReport == "03") {
			reportTimeTrackingBPCenter(params);
		}		
	}
	
	def reportTimeTrackingGR(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", "Sunter");
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", "All");
		parameters.put("jenisKendaraan", "All");
		parameters.put("jobStoppedStatus", "All");
		parameters.put("serviceAdvisor", "All");
		parameters.put("technician", "All");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelTimeTrackingGR(params));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Time_Tracking_GR.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Time_Tracking_GR", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportTimeTrackingBP(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", "Sunter");
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", "All");
		parameters.put("jenisPayment", "All");
		parameters.put("cariNoPolisi", "B12345");				
		
		JRDataSource dataSource = new JRTableModelDataSource(datamodelTimeTrackingBPDetail(params));
		parameters.put("detailDataSource", dataSource);
		dataSource = new JRTableModelDataSource(datamodelTimeTrackingBPDetailProduction(params));
		parameters.put("detailDataSourceProduction", dataSource);
		dataSource = new JRTableModelDataSource(datamodelTimeTrackingBPDetailDelivery(params));
		parameters.put("detailDataSourceDelivery", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Time_Tracking_BP.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Time_Tracking_BP", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportTimeTrackingBPCenter(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", "Sunter");
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", "All");
		parameters.put("jenisPayment", "All");
		parameters.put("cariNoPolisi", "B12345");				
		parameters.put("branch", "ALL");				
		parameters.put("unitIn", "ALL");				
		
		JRDataSource dataSource = new JRTableModelDataSource(datamodelTimeTrackingBPCenterDetail(params));
		parameters.put("detailDataSource", dataSource);
		dataSource = new JRTableModelDataSource(datamodelTimeTrackingBPCenterDetailReception(params));
		parameters.put("detailDataSourceReception", dataSource);
		dataSource = new JRTableModelDataSource(datamodelTimeTrackingBPCenterDetailProduction(params));
		parameters.put("detailDataSourceProduction", dataSource);		
		dataSource = new JRTableModelDataSource(datamodelTimeTrackingBPCenterDetailDelivery(params));
		parameters.put("detailDataSourceDelivery", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Time_Tracking_BP_Center.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Time_Tracking_BP_Center", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def dataModelTimeTrackingGR(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", 
						"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
						"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
						"column_31", "column_32", "column_33", "column_34", "column_35", "column_36", "column_37", "column_38", "column_39", "column_40",
						"column_41", "column_42", "column_43", "column_44", "column_45", "column_46", "column_47", "column_48", "column_49", "column_50",
						"column_51", "column_52", "column_53", "column_54", "column_55", "column_56", "column_57", "column_58", "column_59", "column_60",
						"column_61", "column_62", "column_63", "column_64", "column_65", "column_66", "column_67", "column_68"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingGR(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					String.valueOf(result.get("column_8")),
					String.valueOf(result.get("column_9")),
					String.valueOf(result.get("column_10")),
					String.valueOf(result.get("column_11")),
					String.valueOf(result.get("column_12")),
					String.valueOf(result.get("column_13")),
					String.valueOf(result.get("column_14")),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					String.valueOf(result.get("column_16")),
					String.valueOf(result.get("column_17")),
					String.valueOf(result.get("column_18")),
					String.valueOf(result.get("column_19")),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					String.valueOf(result.get("column_21")),
					String.valueOf(result.get("column_22")),
					String.valueOf(result.get("column_23")),
					String.valueOf(result.get("column_24")),
					String.valueOf(result.get("column_25")),
					Integer.parseInt(String.valueOf(result.get("column_26"))),
					String.valueOf(result.get("column_27")),
					String.valueOf(result.get("column_28")),
					String.valueOf(result.get("column_29")),
					String.valueOf(result.get("column_30")),
					String.valueOf(result.get("column_31")),
					String.valueOf(result.get("column_32")),
					Integer.parseInt(String.valueOf(result.get("column_33"))),
					Integer.parseInt(String.valueOf(result.get("column_34"))),
					String.valueOf(result.get("column_35")),
					String.valueOf(result.get("column_36")),
					String.valueOf(result.get("column_37")),
					String.valueOf(result.get("column_38")),
					String.valueOf(result.get("column_39")),
					String.valueOf(result.get("column_40")),
					Integer.parseInt(String.valueOf(result.get("column_41"))),
					Integer.parseInt(String.valueOf(result.get("column_42"))),
					String.valueOf(result.get("column_43")),
					String.valueOf(result.get("column_44")),
					String.valueOf(result.get("column_45")),
					String.valueOf(result.get("column_46")),
					Integer.parseInt(String.valueOf(result.get("column_47"))),
					Integer.parseInt(String.valueOf(result.get("column_48"))),
					String.valueOf(result.get("column_49")),
					String.valueOf(result.get("column_50")),
					String.valueOf(result.get("column_51")),
					Integer.parseInt(String.valueOf(result.get("column_52"))),
					Integer.parseInt(String.valueOf(result.get("column_53"))),
					String.valueOf(result.get("column_54")),
					String.valueOf(result.get("column_55")),
					String.valueOf(result.get("column_56")),
					Integer.parseInt(String.valueOf(result.get("column_57"))),
					Integer.parseInt(String.valueOf(result.get("column_58"))),
					String.valueOf(result.get("column_59")),
					Integer.parseInt(String.valueOf(result.get("column_60"))),
					String.valueOf(result.get("column_61")),
					String.valueOf(result.get("column_62")),
					String.valueOf(result.get("column_63")),
					String.valueOf(result.get("column_64")),
					Integer.parseInt(String.valueOf(result.get("column_65"))),
					String.valueOf(result.get("column_66")),
					String.valueOf(result.get("column_67")),
					String.valueOf(result.get("column_68"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["B-132-SFL","15516","SBE","INNOVA V","TGN 10","KARETA KACA DEPAN SOBEK","REM BLONG","MESIN RUSAK", "KNALPOT BERISIK","","BRI","TSA",
				"1","18-April-2011","06:05AM",new java.lang.Integer(4000),"18-April-2011","07:00AM","07:20AM","0",new java.lang.Integer(4000),"07:05AM",
				"07:15AM",new java.lang.Integer(4000),new java.lang.Integer(4000),"08:00AM",new java.lang.Integer(4000),"18-April-2011","08:49AM","09:00AM","09:05AM","18-April-2011",
				"10:00AM",new java.lang.Integer(4000),new java.lang.Integer(4000),"18-April-2011","09:00AM","2","Waiting For Approval","18-April-2011","09:15AM", new java.lang.Integer(4000),
				new java.lang.Integer(4000),"18-April-2011","10:15AM","19-April-2011","10:30AM", new java.lang.Integer(4000),new java.lang.Integer(4000),"10-April-2011","11:30AM","11:40AM",new java.lang.Integer(4000), 
				new java.lang.Integer(4000),"10-April-2011","10:30AM","11:50AM",new java.lang.Integer(4000), new java.lang.Integer(4000),"11:50AM",new java.lang.Integer(4000),"18-April-2011","01:00PM",
				"18-April-2011","01:10PM",new java.lang.Integer(4000),"18-April-2011","12:00PM","1"];
			model.addRow(object);			
		}
		return model;
	}
	
	def datamodelTimeTrackingBPDetail(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", 
						"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
						"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
						"column_31", "column_32", "column_33", "column_34", "column_35"];	
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingBPDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					String.valueOf(result.get("column_8")),
					String.valueOf(result.get("column_9")),
					String.valueOf(result.get("column_10")),
					String.valueOf(result.get("column_11")),
					String.valueOf(result.get("column_12")),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					String.valueOf(result.get("column_14")),
					String.valueOf(result.get("column_15")),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					String.valueOf(result.get("column_18")),
					String.valueOf(result.get("column_19")),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					String.valueOf(result.get("column_21")),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					String.valueOf(result.get("column_23")),
					String.valueOf(result.get("column_24")),
					Integer.parseInt(String.valueOf(result.get("column_25"))),
					String.valueOf(result.get("column_26")),
					String.valueOf(result.get("column_27")),
					String.valueOf(result.get("column_28")),
					String.valueOf(result.get("column_29")),
					String.valueOf(result.get("column_30")),
					String.valueOf(result.get("column_31")),
					String.valueOf(result.get("column_32")),
					String.valueOf(result.get("column_33")),
					String.valueOf(result.get("column_34")),
					String.valueOf(result.get("column_35"))
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["B4321","70456","Heavy Repair","Innova","1","Insurance","8","-","-","-","-",
				"-","01-Jan-2011",new java.lang.Integer(4000),"01-Jan-2011","01-Jan-2011",new java.lang.Integer(4000),new java.lang.Integer(4000),"01-Jan-2011","01-Jan-2011",new java.lang.Integer(4000),
				"01-Jan-2011",new java.lang.Integer(4000),"02-Jan-2011","17:00",new java.lang.Integer(4000),"01-Jan-2011","08:00AM","","08:10AM","08:30AM",
				"08:10AM","08:50AM","09:00AM","04:00PM","10:00AM"];
			model.addRow(object);
		}
		return model;
	}
	
	def datamodelTimeTrackingBPDetailProduction(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];	
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingBPDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7"))
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","START","01-Jan-2011","10:20PM"];
			model.addRow(object);
			object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","PAUSE","01-Jan-2011","10:20PM"];
			model.addRow(object);
			object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","RESUME","01-Jan-2011","10:20PM"];
			model.addRow(object);
			object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","FINISH","01-Jan-2011","10:20PM"];
			model.addRow(object);
		}
		return model;
	}
	
	def datamodelTimeTrackingBPDetailDelivery(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5"];	
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingBPDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5"))
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["WF JOC",new java.lang.Integer(4000),"JOC","START","02-Jan-2011","08:10AM"];
			model.addRow(object);			
			object = ["WF JOC",new java.lang.Integer(4000),"JOC","FINISH","02-Jan-2011","08:20AM"];
			model.addRow(object);			
		}
		return model;
	}
	
	def datamodelTimeTrackingBPCenterDetail(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];	
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingBPDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6"))
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["B4321","Sudirman","Light Repair","Innova","1","Insurance","B"];
			model.addRow(object);						
		}
		return model;
	}
	
	def datamodelTimeTrackingBPCenterDetailReception(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5"];	
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingBPDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5"))
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["WF Reception", new java.lang.Integer(4000),"Reception","START","01-Jan-2011","08:10AM"];
			model.addRow(object);									
		}
		return model;
	}
	
	def datamodelTimeTrackingBPCenterDetailProduction(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];	
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingBPDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7"))
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","START","01-Jan-2011","10:20AM"];
			model.addRow(object);									
			object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","PAUSE","01-Jan-2011","10:20AM"];
			model.addRow(object);									
			object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","RESUME","01-Jan-2011","10:20AM"];
			model.addRow(object);									
			object = ["BODY","WF Body", new java.lang.Integer(4000),"Body","AAA","FINISH","01-Jan-2011","10:20AM"];
			model.addRow(object);									
		}
		return model;
	}
	
	def datamodelTimeTrackingBPCenterDetailDelivery(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5"];	
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = timeTrackingService.datatablesTimeTrackingBPDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5"))
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			def Object[] object = ["WF JOC",new java.lang.Integer(4000),"JOC","START","02-Jan-2011","08:10AM"];
			model.addRow(object);												
			object = ["WF JOC",new java.lang.Integer(4000),"JOC","FINISH","02-Jan-2011","08:10AM"];
			model.addRow(object);												
		}
		return model;
	}
	
	def datatablesSAList(){
		render timeTrackingService.datatablesSAList(params) as JSON;
	}
	
	def datatablesTechnician(){
		render timeTrackingService.datatablesTechnician(params) as JSON;
	}

	def dataTablesJenisPekerjaan(){
		render timeTrackingService.datatablesJenisPekerjaan(params) as JSON;			
	}
	
	def dataTablesJenisKendaraan(){
		render timeTrackingService.datatablesJenisKendaraan(params) as JSON;			
	}
	
	def dataTablesJenisPayment(){
		render timeTrackingService.datatablesJenisPayment(params) as JSON;		
	}

	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

}