package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_delivery_grController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def deliveryGRService;
	def jasperService;
	def rootPath;
    def conversi = new Konversi()
    def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
        def formatFile = "";
        def formatBuntut = "";
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01") {
			reportDeliveryGRDeliveryLeadTimeDaily(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "02") {
			reportDeliveryGRDeliveryLeadTimeMonthly(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "03") {
			reportDeliveryGRDeliveryLeadTimeYearly(params);
		} else
		if(params.namaReport == "04") {
			reportDeliveryGRSADeliveryDaily(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "05") {
			reportDeliveryGRSADeliveryMonthly(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "06") {
			reportDeliveryGRSADeliveryYearly(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "07") {
			reportDeliveryGROTDRateDaily(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "08") {
			reportDeliveryGROTDRateMonthly(params,formatFile,formatBuntut);
		}
	}
	
	def reportDeliveryGRDeliveryLeadTimeDaily(def params ,String file,String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("waiting", (params.waitingStatus == "" ? "ALL" : params.waitingStatus));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Daily");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliveryLeadTimeDailySummary(params));
		parameters.put("reportSummaryDataSource", dataSource);

		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliveryLeadTimeDetail(params, 0));
			parameters.put("reportDetailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_Lead_Time_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Delivery_Lead_Time_Daily", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }
        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryGRDeliveryLeadTimeMonthly(def params,String file,String format){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = deliveryGRService.getWorkshopByCode(params.workshop);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("waiting", (params.waitingStatus == "" ? "ALL" : params.waitingStatus));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Monthly");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliveryLeadTimeMonthlySummary(params));
		parameters.put("reportSummaryDataSource", dataSource);
		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliveryLeadTimeDetail(params, 1));
			parameters.put("reportDetailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_Lead_Time_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Delivery_Lead_Time_Monthly", file);
		pdf.deleteOnExit();


        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }
        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryGRDeliveryLeadTimeYearly(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = deliveryGRService.getWorkshopByCode(params.workshop);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);
		parameters.put("waiting", (params.waitingStatus == "" ? "ALL" : params.waitingStatus));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Yearly");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliveryLeadTimeYearlySummary(params));
		parameters.put("reportSummaryDataSource", dataSource);
		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliveryLeadTimeDetail(params, 2));
			parameters.put("reportDetailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_Lead_Time_Yearly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Delivery_Lead_Time_Yearly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();		
	}
	
	def reportDeliveryGRSADeliveryDaily(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryGRService.getWorkshopByCode(params.workshop);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Daily");

		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliverySADailySummary(params));
		parameters.put("reportSummaryDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliverySADailySummaryResult(params, 0));
		parameters.put("reportSummaryResultDataSource", dataSource);
		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliverySADetail(params));
			parameters.put("reportDetailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_SA_Delivery_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Delivery_SA_Delivery_Daily", file);
		pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryGRSADeliveryMonthly(def params,String file,String format){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun		
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = deliveryGRService.getWorkshopByCode(params.workshop);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Monthly");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliverySAMonthlySummary(params));
		parameters.put("reportSummaryDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliverySADailySummaryResult(params, 1));
		parameters.put("reportSummaryResultDataSource", dataSource);

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_SA_Delivery_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Delivery_SA_Delivery_Monthly", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryGRSADeliveryYearly(def params,String file,String format) {
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = deliveryGRService.getWorkshopByCode(params.workshop);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Yearly");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliverySAYearlySummary(params));
		parameters.put("reportSummaryDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelDeliveryGRDeliverySADailySummaryResult(params, 2));
		parameters.put("reportSummaryResultDataSource", dataSource);

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_SA_Delivery_Yearly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Delivery_SA_Delivery_Yearly", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryGROTDRateDaily(def params,String file,String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		def result = deliveryGRService.getWorkshopByCode(params.workshop);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : deliveryGRService.getServiceAdvisorById(params.sa)));		
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Daily");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGROTDRateDailyServiceAdvisor(params));
		parameters.put("reportSummaryServiceAdvisorDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelDeliveryGROTDRateDailyJenisPekerjaan(params));
		parameters.put("reportSummaryJenisPekerjaanDataSource", dataSource);

		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDeliveryGROTDRateDetail(params, 0));
			parameters.put("reportDetailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_OTD_Rate_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("GR_Delivery_OTD_Rate_Daily", file);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryGROTDRateMonthly(def params,String file,String format){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = deliveryGRService.getWorkshopByCode(params.workshop);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : deliveryGRService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : deliveryGRService.getServiceAdvisorById(params.sa)));		
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Monthly");

		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryGROTDRateMonthlyServiceAdvisor(params));
		parameters.put("reportSummaryServiceAdvisorDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelDeliveryGROTDRateMonthlyJenisPekerjaan(params));
		parameters.put("reportSummaryJenisPekerjaanDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelDeliveryGROTDRateMonthlyTotalWorkshop(params));
		parameters.put("reportSummaryTotalDataSource", dataSource);

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Delivery_OTD_Rate_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Delivery_OTD_Rate_Monthly", file);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def dataModelDeliveryGRDeliveryLeadTimeDailySummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", 
									"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
									"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
									"column_31", "column_32", "column_33"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliveryLeadTimeSummary(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					Integer.parseInt(String.valueOf(result.get("column_3"))),					
					Integer.parseInt(String.valueOf(result.get("column_4"))),					
					Integer.parseInt(String.valueOf(result.get("column_5"))),					
					Integer.parseInt(String.valueOf(result.get("column_6"))),					
					Integer.parseInt(String.valueOf(result.get("column_7"))),					
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23"))),
					Integer.parseInt(String.valueOf(result.get("column_24"))),
					Integer.parseInt(String.valueOf(result.get("column_25"))),
					Integer.parseInt(String.valueOf(result.get("column_26"))),
					Integer.parseInt(String.valueOf(result.get("column_27"))),
					Integer.parseInt(String.valueOf(result.get("column_28"))),
					Integer.parseInt(String.valueOf(result.get("column_29"))),
					Integer.parseInt(String.valueOf(result.get("column_30"))),
					Integer.parseInt(String.valueOf(result.get("column_31"))),
					Integer.parseInt(String.valueOf(result.get("column_32"))),
					Integer.parseInt(String.valueOf(result.get("column_33"))),
					Integer.parseInt(String.valueOf(result.get("column_34"))),
					Integer.parseInt(String.valueOf(result.get("column_35")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelDeliveryGRDeliveryLeadTimeMonthlySummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", 
									"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
									"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
									"column_31", "column_32", "column_33", "column_34", "column_35"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliveryLeadTimeSummary(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),					
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					Integer.parseInt(String.valueOf(result.get("column_3"))),					
					Integer.parseInt(String.valueOf(result.get("column_4"))),					
					Integer.parseInt(String.valueOf(result.get("column_5"))),					
					Integer.parseInt(String.valueOf(result.get("column_6"))),					
					Integer.parseInt(String.valueOf(result.get("column_7"))),					
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23"))),
					Integer.parseInt(String.valueOf(result.get("column_24"))),
					Integer.parseInt(String.valueOf(result.get("column_25"))),
					Integer.parseInt(String.valueOf(result.get("column_26"))),
					Integer.parseInt(String.valueOf(result.get("column_27"))),
					Integer.parseInt(String.valueOf(result.get("column_28"))),
					Integer.parseInt(String.valueOf(result.get("column_29"))),
					Integer.parseInt(String.valueOf(result.get("column_30"))),
					Integer.parseInt(String.valueOf(result.get("column_31"))),
					Integer.parseInt(String.valueOf(result.get("column_32"))),
					Integer.parseInt(String.valueOf(result.get("column_33"))),
					Integer.parseInt(String.valueOf(result.get("column_34"))),
					Integer.parseInt(String.valueOf(result.get("column_35")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelDeliveryGRDeliveryLeadTimeYearlySummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", 
									"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
									"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
									"column_31", "column_32", "column_33", "column_34"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliveryLeadTimeSummary(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					Integer.parseInt(String.valueOf(result.get("column_3"))),					
					Integer.parseInt(String.valueOf(result.get("column_4"))),					
					Integer.parseInt(String.valueOf(result.get("column_5"))),					
					Integer.parseInt(String.valueOf(result.get("column_6"))),					
					Integer.parseInt(String.valueOf(result.get("column_7"))),					
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23"))),
					Integer.parseInt(String.valueOf(result.get("column_24"))),
					Integer.parseInt(String.valueOf(result.get("column_25"))),
					Integer.parseInt(String.valueOf(result.get("column_26"))),
					Integer.parseInt(String.valueOf(result.get("column_27"))),
					Integer.parseInt(String.valueOf(result.get("column_28"))),
					Integer.parseInt(String.valueOf(result.get("column_29"))),
					Integer.parseInt(String.valueOf(result.get("column_30"))),
					Integer.parseInt(String.valueOf(result.get("column_31"))),
					Integer.parseInt(String.valueOf(result.get("column_32"))),
					Integer.parseInt(String.valueOf(result.get("column_33"))),
					Integer.parseInt(String.valueOf(result.get("column_34"))),
					Integer.parseInt(String.valueOf(result.get("column_35")))
					]
				model.addRow(object)
			}
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}

	def dataModelDeliveryGRDeliveryLeadTimeDetail(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", 
									"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
									"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
									"column_31", "column_32", "column_33", "column_34", "column_35", "column_36", "column_37", "column_38"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliveryLeadTimeDetail(params, type);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),
					String.valueOf(result.get("column_10") != null ? result.get("column_10") : ""),
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : ""),
					String.valueOf(result.get("column_12") != null ? result.get("column_12") : ""),
					String.valueOf(result.get("column_13") != null ? result.get("column_13") : ""),
					String.valueOf(result.get("column_14") != null ? result.get("column_14") : ""),
					String.valueOf(result.get("column_15") != null ? result.get("column_15") : ""),
					String.valueOf(result.get("column_16") != null ? result.get("column_16") : ""),
					String.valueOf(result.get("column_17") != null ? result.get("column_17") : ""),
					String.valueOf(result.get("column_18") != null ? result.get("column_18") : ""),
					String.valueOf(result.get("column_19") != null ? result.get("column_19") : ""),
					String.valueOf(result.get("column_20") != null ? result.get("column_20") : ""),
					String.valueOf(result.get("column_21") != null ? result.get("column_21") : ""),
					String.valueOf(result.get("column_22") != null ? result.get("column_22") : ""),
					String.valueOf(result.get("column_23") != null ? result.get("column_23") : ""),
					String.valueOf(result.get("column_24") != null ? result.get("column_24") : ""),
					String.valueOf(result.get("column_25") != null ? result.get("column_25") : ""),
					String.valueOf(result.get("column_26") != null ? result.get("column_26") : ""),
					String.valueOf(result.get("column_27") != null ? result.get("column_27") : ""),					
					Integer.parseInt(String.valueOf(result.get("column_28"))),
					Integer.parseInt(String.valueOf(result.get("column_29"))),
					Integer.parseInt(String.valueOf(result.get("column_30"))),
					Integer.parseInt(String.valueOf(result.get("column_31"))),
					Integer.parseInt(String.valueOf(result.get("column_32"))),
					Integer.parseInt(String.valueOf(result.get("column_33"))),
					Integer.parseInt(String.valueOf(result.get("column_34"))),
					Integer.parseInt(String.valueOf(result.get("column_35"))),
					Integer.parseInt(String.valueOf(result.get("column_36"))),
					Integer.parseInt(String.valueOf(result.get("column_37"))),
					Integer.parseInt(String.valueOf(result.get("column_38")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;	
	}
	
	def dataModelDeliveryGRDeliverySADailySummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliverySADeliverySummary(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [															
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;	
	}
	
	def dataModelDeliveryGRDeliverySADailySummaryResult(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliverySADeliverySummaryResult(params, type);
			for(Map<String, Object> result : results) {
				def Object[] object = [															
					String.valueOf("xxx"),
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGRDeliverySAMonthlySummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliverySADeliverySummary(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [						
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;	
	}
	
	def dataModelDeliveryGRDeliverySAYearlySummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);

		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliverySADeliverySummary(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [						
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : "0"),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : "0"),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;	
	}
	
	def dataModelDeliveryGRDeliverySADetail(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrDeliverySADeliveryDetail(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [						
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
                    Integer.parseInt(String.valueOf(result.get("column_7"))),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : "")
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGROTDRateDailyServiceAdvisor(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrOTDRateSummaryServiceAdvisor(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [											
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    String.valueOf(result.get("column_5")).toBigDecimal()
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGROTDRateDailyJenisPekerjaan(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrOTDRateSummaryJenisPekerjaan(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [											
					String.valueOf(result.get("column_2") != null  ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    String.valueOf(result.get("column_5")).toBigDecimal(),
                    String.valueOf(result.get("column_6"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGROTDRateDailyTotalWorkshop(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrOTDRateSummaryTotalWorkshop(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [											
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
                    String.valueOf(result.get("column_2")).toBigDecimal(),
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGROTDRateMonthlyTotalWorkshop(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrOTDRateSummaryTotalWorkshop(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
                        String.valueOf(result.get("column_0")),
                        String.valueOf(result.get("column_1")),
					    Integer.parseInt(String.valueOf(result.get("column_2"))),
					    Integer.parseInt(String.valueOf(result.get("column_3"))),
                        String.valueOf(result.get("column_4")).toBigDecimal()
                ]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGROTDRateMonthlyServiceAdvisor(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrOTDRateSummaryServiceAdvisor(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    String.valueOf(result.get("column_5")).toBigDecimal()
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGROTDRateMonthlyJenisPekerjaan(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrOTDRateSummaryJenisPekerjaan(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    String.valueOf(result.get("column_5")).toBigDecimal()
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryGROTDRateDetail(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", 
									"column_6", "column_7", "column_8", "column_9", "column_10", "column_11"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryGRService.datatablesDeliveryGrOTDRateDetail(params, type);
			for(Map<String, Object> result : results) {

				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),
					String.valueOf(result.get("column_10") != null ? result.get("column_10") : ""),
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : "")
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}

	def datatablesSAList(){
        session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
		render deliveryGRService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render deliveryGRService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render deliveryGRService.datatablesJenisPekerjaan(params) as JSON;			
	}
	
	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

}