package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Production_bpController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def productionBPService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
        def formatFile = "";
        def formatBuntut = "";
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }
        rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01"){
			generateReportBpProductionLeadTime(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "02"){
			generateReportBpRedoLeadTime(params);
		} else
		if(params.namaReport == "03"){
			generateReportBpProductionLeadTimeDetailBody(params);
		} else
		if(params.namaReport == "04"){
			generateReportBpProductionLeadTimeDetailPreparation(params);
		} else
		if(params.namaReport == "05"){
			generateReportBpProductionLeadTimeDetailPainting(params);
		} else
		if(params.namaReport == "06"){
			generateReportBpProductionLeadTimeDetailPolishing(params);
		} else
		if(params.namaReport == "07"){
			generateReportBpProductionLeadTimeDetailAssembly(params);
		} else
		if(params.namaReport == "08"){
			generateReportBpProductionRedoBodyDailyUnit(params);
		} else
		if(params.namaReport == "09"){
			generateReportBpProductionRedoBodyDailyPanel(params);
		} else
		if(params.namaReport == "10"){
			generateReportBpProductionRedoBodyWeeklyUnit(params);
		} else
		if(params.namaReport == "11"){
			generateReportBpProductionRedoBodyWeeklyPanel(params);
		} else
		if(params.namaReport == "12"){
			generateReportBpProductionRedoBodyMonthlyUnit(params);
		} else
		if(params.namaReport == "13"){
			generateReportBpProductionRedoBodyMonthlyPanel(params);
		} else
		if(params.namaReport == "14"){
			generateReportBpProductionRedoBodyYearlyUnit(params);
		} else
		if(params.namaReport == "15"){
			generateReportBpProductionRedoBodyYearlyPanel(params);
		} else
		if(params.namaReport == "16"){
			generateReportBpProductionRedoPreparationDailyUnit(params);
		} else
		if(params.namaReport == "17"){
			generateReportBpProductionRedoPreparationDailyPanel(params);
		} else
		if(params.namaReport == "18"){
			generateReportBpProductionRedoPreparationWeeklyUnit(params);
		} else
		if(params.namaReport == "19"){
			generateReportBpProductionRedoPreparationWeeklyPanel(params);
		} else
		if(params.namaReport == "20"){
			generateReportBpProductionRedoPreparationMonthlyUnit(params);
		} else
		if(params.namaReport == "21"){
			generateReportBpProductionRedoPreparationMonthlyPanel(params);
		} else
		if(params.namaReport == "22"){
			generateReportBpProductionRedoPreparationYearlyUnit(params);
		} else 
		if(params.namaReport == "23"){
			generateReportBpProductionRedoPreparationYearlyPanel(params);
		} else
		if(params.namaReport == "24"){
			generateReportBpProductionRedoPaintingDailyUnit(params);
		} else
		if(params.namaReport == "25"){
			generateReportBpProductionRedoPaintingDailyPanel(params);
		} else
		if(params.namaReport == "26"){
			generateReportBpProductionRedoPaintingWeeklyUnit(params);
		} else
		if(params.namaReport == "27"){
			generateReportBpProductionRedoPaintingWeeklyPanel(params);
		} else 
		if(params.namaReport == "28"){
			generateReportBpProductionRedoPaintingMonthlyUnit(params);
		} else
		if(params.namaReport == "29"){
			generateReportBpProductionRedoPaintingMonthlyPanel(params);
		} else
		if(params.namaReport == "30"){
			generateReportBpProductionRedoPaintingYearlyUnit(params);
		} else
		if(params.namaReport == "31"){
			generateReportBpProductionRedoPaintingYearlyPanel(params);
		} else
		if(params.namaReport == "32"){
			generateReportBpProductionCombibothProductivityDaily(params);
		} else
		if(params.namaReport == "33"){
			generateReportBpProductionCombibothProductivityWeekly(params);
		} else
		if(params.namaReport == "34"){
			generateReportBpProductionCombibothProductivityMonthly(params);
		} else
		if(params.namaReport == "35"){
			generateReportBpProductionOneTimeInEachProcessDaily(params);
		} else
		if(params.namaReport == "36"){
			generateReportBpProductionOneTimeInEachProcessWeekly(params);
		} else
		if(params.namaReport == "37"){
			generateReportBpProductionOneTimeInEachProcessMonthly(params);
		} else
		if(params.namaReport == "38"){
			generateReportBpTechnicianProductivity(params);
		}
 	}

 	def generateReportBpProductionLeadTime(def params,String file,String format){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("proses", (params.jenisProses == "" ? "ALL" : productionBPService.getJenisProsesById(params.jenisProses)));		

		JRDataSource dataSource = new JRTableModelDataSource(productionLeadTimeDetailModel(params));
		parameters.put("detailDataSource", dataSource);
		dataSource = new JRTableModelDataSource(productionLeadTimeSummaryModel(params));
		parameters.put("summaryDataSource", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Production_Lead_Time.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Production_Lead_Time", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }

//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
//		response.setHeader("Content-Type", "application/pdf");
//		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
//		response.outputStream << pdf.newInputStream();
 	}

 	def generateReportBpRedoLeadTime(def params){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("proses", (params.jenisProses == "" ? "ALL" : productionBPService.getJenisProsesById(params.jenisProses)));		

		JRDataSource dataSource = new JRTableModelDataSource(redoLeadTimeDetailModel(params));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Production_Redo_Lead_Time.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Production_Redo_Lead_Time", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
 	}
	
	def generateReportBpProductionLeadTimeDetailBody(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("proses", "Body");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionLeadTimeDetail(params,"03"));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Production_Lead_Time_Detail_Body.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Production_Lead_Time_Detail_Body", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionLeadTimeDetailPreparation(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("proses", "Preparation");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionLeadTimeDetail(params,"04"));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Production_Lead_Time_Detail_Preparation.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Production_Lead_Time_Detail_Preparation", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionLeadTimeDetailPainting(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("proses", "Painting");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionLeadTimeDetail(params,"05"));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Production_Lead_Time_Detail_Painting.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Production_Lead_Time_Detail_Painting", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionLeadTimeDetailPolishing(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("proses", "Polishing");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionLeadTimeDetail(params,"06"));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Production_Lead_Time_Detail_Polishing.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Production_Lead_Time_Detail_Polishing", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionLeadTimeDetailAssembly(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));		
		parameters.put("proses", "Assembly");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionLeadTimeDetail(params,"07"));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Production_Lead_Time_Detail_Assembly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Production_Lead_Time_Detail_Assembly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyDailyUnit(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 0));
		parameters.put("pieChartDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 0));
		parameters.put("pieChartTableDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 0));
		parameters.put("grafikDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafikTable(params, 0));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 0));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Daily", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyDailyPanel(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 1));
		parameters.put("pieChartDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 1));
		parameters.put("pieChartTableDataSource", dataSource);	
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 1));
		parameters.put("grafikDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafikTable(params, 1));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 1));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Daily_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Daily_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyWeeklyUnit(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 2));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 2));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 2));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyWeeklyUnitGrafikTable(params, 2));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 2));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Weekly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Weekly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyWeeklyPanel(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 3));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 3));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 3));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyWeeklyUnitGrafikTable(params, 3));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 3));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Weekly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Weekly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyMonthlyUnit(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 4));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 4));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 4));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyMonthlyUnitGrafikTable(params, 4));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 4));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Monthly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Monthly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyMonthlyPanel(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 5));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 5));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 5));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyMonthlyUnitGrafikTable(params, 5));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 5));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Monthly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Monthly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyYearlyUnit(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);		
		parameters.put("report", "Yearly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 6));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 6));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 6));
		parameters.put("grafikDataSource", dataSource);//
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyYearlyUnitGrafikTable(params, 6));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 6));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Yearly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Yearly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoBodyYearlyPanel(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);		
		parameters.put("report", "Yearly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChart(params, 7));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitPieChartTable(params, 7));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitGrafik(params, 7));
		parameters.put("grafikDataSource", dataSource);//
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyYearlyUnitGrafikTable(params, 7));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoBodyDailyUnitDetail(params, 7));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Body_Yearly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Body_Yearly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationDailyUnit(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 0));
		parameters.put("pieChartDataSource", dataSource);
		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 0));
		parameters.put("pieChartTableDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 0));
		parameters.put("grafikDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafikTable(params, 0));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 0));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Daily_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Daily_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationDailyPanel(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 1));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 1));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 1));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafikTable(params, 1));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 1));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Daily_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Daily_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationWeeklyUnit(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 2));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 2));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 2));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationWeeklyUnitGrafikTable(params, 2));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 2));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Weekly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Weekly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationWeeklyPanel(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 3));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 3));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 3));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationWeeklyUnitGrafikTable(params, 3));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 3));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Weekly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Weekly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationMonthlyUnit(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 4));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 4));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 4));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationMonthlyUnitGrafikTable(params, 4));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 4));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Monthly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Monthly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationMonthlyPanel(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 5));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 5));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 5));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationMonthlyUnitGrafikTable(params, 5));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 5));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Monthly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Monthly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationYearlyUnit(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);		
		parameters.put("report", "Yearly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 6));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 6));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 6));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationYearlyUnitGrafikTable(params, 6));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 6));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Yearly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Yearly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPreparationYearlyPanel(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);		
		parameters.put("report", "Yearly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChart(params, 15));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitPieChartTable(params, 15));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitGrafik(params, 15));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationYearlyUnitGrafikTable(params, 15));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPreparationDailyUnitDetail(params, 15));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Preparation_Yearly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Preparation_Yearly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	def generateReportBpProductionRedoPaintingDailyUnit(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 0));
		parameters.put("pieChartDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 0));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 0));
		parameters.put("grafikDataSource", dataSource);	
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafikTable(params, 0));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 0));
		parameters.put("detailDataSource", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Daily_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Daily_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPaintingDailyPanel(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 1));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 1));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 1));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafikTable(params, 1));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 1));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Daily_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Daily_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPaintingWeeklyUnit(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 2));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 2));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 2));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingWeeklyUnitGrafikTable(params, 2));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 18));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Weekly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Weekly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPaintingWeeklyPanel(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 3));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 3));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 3));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingWeeklyUnitGrafikTable(params, 3));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 19));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Weekly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Weekly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPaintingMonthlyUnit(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 4));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 4));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 4));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingMonthlyUnitGrafikTable(params, 4));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 4));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Monthly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Weekly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPaintingMonthlyPanel(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 5));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 5));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 5));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingMonthlyUnitGrafikTable(params, 5));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 5));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Monthly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Monthly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPaintingYearlyUnit(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);		
		parameters.put("report", "Yearly");		
		parameters.put("tipeDefect", "Unit");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 6));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 6));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 6));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingYearlyUnitGrafikTable(params, 6));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 6));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Yearly_Unit.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Yearly_Unit", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionRedoPaintingYearlyPanel(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);		
		parameters.put("report", "Yearly");		
		parameters.put("tipeDefect", "Panel");		

		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChart(params, 7));
		parameters.put("pieChartDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitPieChartTable(params, 7));
		parameters.put("pieChartTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitGrafik(params, 7));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingYearlyUnitGrafikTable(params, 7));
		parameters.put("grafikTableDataSource", dataSource);			
		dataSource = new JRTableModelDataSource(dataModelBpProductionRedoPaintingDailyUnitDetail(params, 7));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Redo_Painting_Yearly_Panel.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Redo_Painting_Yearly_Panel", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionCombibothProductivityDaily(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyGrafik(params, 1));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyGrafikTable(params, 1));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall1Grafik(params, 1));
		parameters.put("stall1GrafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall1GrafikTable(params, 1));
		parameters.put("stall1GrafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall2Grafik(params, 1));
		parameters.put("stall2GrafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall2GrafikTable(params, 1));
		parameters.put("stall2GrafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyDetail(params, 1));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Combiboth_Productivity_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Combiboth_Productivity_Daily", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionCombibothProductivityWeekly(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyGrafik(params, 2));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyGrafikTable(params, 2));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall1Grafik(params, 2));
		parameters.put("stall1GrafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall1GrafikTable(params, 2));
		parameters.put("stall1GrafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall2Grafik(params, 2));
		parameters.put("stall2GrafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall2GrafikTable(params, 2));
		parameters.put("stall2GrafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyDetail(params, 2));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Combiboth_Productivity_Weekly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Combiboth_Productivity_Weekly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionCombibothProductivityMonthly(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyGrafik(params, 3));
		parameters.put("grafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyGrafikTable(params, 3));
		parameters.put("grafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall1Grafik(params, 3));
		parameters.put("stall1GrafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall1GrafikTable(params, 3));
		parameters.put("stall1GrafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall2Grafik(params, 3));
		parameters.put("stall2GrafikDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyStall2GrafikTable(params, 3));
		parameters.put("stall2GrafikTableDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionCombibothDailyDetail(params, 3));
		parameters.put("detailDataSource", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Combiboth_Productivity_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Combiboth_Productivity_Monthly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionOneTimeInEachProcessDaily(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Daily");		
		parameters.put("proses", "Body");		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessGrafik(params, 1));
		parameters.put("grafikBodyDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessSummary(params, 1));
		parameters.put("summaryBodyDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessDetail(params, 1));
		parameters.put("detailBodyDataSource", dataSource);
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_On_Time_In_Each_Process_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_On_Time_In_Each_Process_Daily", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionOneTimeInEachProcessWeekly(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Weekly");		
		parameters.put("proses", "Body");		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessGrafik(params, 2));
		parameters.put("grafikBodyDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessSummaryWeekly(params, 2));
		parameters.put("summaryBodyDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessDetail(params, 1));
		parameters.put("detailBodyDataSource", dataSource);
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_On_Time_In_Each_Process_Weekly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_On_Time_In_Each_Process_Weekly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpProductionOneTimeInEachProcessMonthly(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionBPService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("report", "Monthly");		
		parameters.put("proses", "Body");		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessGrafik(params, 3));
		parameters.put("grafikBodyDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessSummaryWeekly(params, 3));
		parameters.put("summaryBodyDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelBpProductionOnTimeInEachProcessDetail(params, 1));
		parameters.put("detailBodyDataSource", dataSource);
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_On_Time_In_Each_Process_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_On_Time_In_Each_Process_Monthly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def generateReportBpTechnicianProductivity(def params){
	}

 	def productionLeadTimeDetailModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7",
 		   		"column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17",
 		   		"column_18", "column_19", "column_20", "column_21", "column_22", "column_23", "column_24", "column_25", "column_26",
 		   		"column_27", "column_28", "column_29", "column_30", "column_31", "column_32", "column_33", "column_34"];		
 		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 		
		try {
			def results = productionBPService.datatablesProductionLeadTimeDetail(params,0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),
					String.valueOf(null != result.get("column_4") ? result.get("column_4") : ""),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23"))),
					Integer.parseInt(String.valueOf(result.get("column_24"))),
					Integer.parseInt(String.valueOf(result.get("column_25"))),
					Integer.parseInt(String.valueOf(result.get("column_26"))),
					Integer.parseInt(String.valueOf(result.get("column_27"))),
					Integer.parseInt(String.valueOf(result.get("column_28"))),
					Integer.parseInt(String.valueOf(result.get("column_29"))),
					Integer.parseInt(String.valueOf(result.get("column_30"))),
					Integer.parseInt(String.valueOf(result.get("column_31"))),
					Integer.parseInt(String.valueOf(result.get("column_32"))),
					Integer.parseInt(String.valueOf(result.get("column_33"))),
					Integer.parseInt(String.valueOf(result.get("column_34")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;		
 	}

 	def productionLeadTimeSummaryModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];		
 		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		
		try {
			def results = productionBPService.datatablesProductionLeadTimeSummary(params,0);
			for(Map<String, Object> result : results) {
				def Object[] object = new Object[8];
				object[0] = String.valueOf(null != result.get("column_0") ?  result.get("column_0") : "");
				object[1] = "Average";
				object[2] = Integer.parseInt(String.valueOf(result.get("column_1")));
				object[3] = Integer.parseInt(String.valueOf(result.get("column_2")));
				object[4] = Integer.parseInt(String.valueOf(result.get("column_3")));
				object[5] = Integer.parseInt(String.valueOf(result.get("column_4")));
				object[6] = Integer.parseInt(String.valueOf(result.get("column_5")));
				object[7] = Integer.parseInt(String.valueOf(result.get("column_6")));
				model.addRow(object);
				
				object = new Object[8];
				object[0] = String.valueOf(null != result.get("column_0") ?  result.get("column_0") : "");
				object[1] = "Max";
				object[2] = Integer.parseInt(String.valueOf(result.get("column_7")));
				object[3] = Integer.parseInt(String.valueOf(result.get("column_8")));
				object[4] = Integer.parseInt(String.valueOf(result.get("column_9")));
				object[5] = Integer.parseInt(String.valueOf(result.get("column_10")));
				object[6] = Integer.parseInt(String.valueOf(result.get("column_11")));
				object[7] = Integer.parseInt(String.valueOf(result.get("column_12")));
				model.addRow(object);
				
				object = new Object[8];
				object[0] = String.valueOf(null != result.get("column_0") ?  result.get("column_0") : "");
				object[1] = "Min";
				object[2] = Integer.parseInt(String.valueOf(result.get("column_13")));
				object[3] = Integer.parseInt(String.valueOf(result.get("column_14")));
				object[4] = Integer.parseInt(String.valueOf(result.get("column_15")));
				object[5] = Integer.parseInt(String.valueOf(result.get("column_16")));
				object[6] = Integer.parseInt(String.valueOf(result.get("column_17")));
				object[7] = Integer.parseInt(String.valueOf(result.get("column_18")));
				model.addRow(object);
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;			
 	}

 	def redoLeadTimeDetailModel(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7",
 		   		"column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17",
 		   		"column_18", "column_19", "column_20", "column_21", "column_22", "column_23", "column_24", "column_25", "column_26",
 		   		"column_27", "column_28"];		
 		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def results = productionBPService.datatablesProductionRedoLeadTimeDetail(params,0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),
					String.valueOf(null != result.get("column_4") ? result.get("column_4") : ""),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23"))),
					Integer.parseInt(String.valueOf(result.get("column_24"))),
					Integer.parseInt(String.valueOf(result.get("column_25"))),
					Integer.parseInt(String.valueOf(result.get("column_26"))),
					Integer.parseInt(String.valueOf(result.get("column_27"))),
					Integer.parseInt(String.valueOf(result.get("column_28")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	 		
 	}
	
	def dataModelBpProductionLeadTimeDetail(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
				"column_11", "column_12", "column_13", "column_14","column_15", "column_16", "column_17", "column_18","column_19","column_20",
				"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesProductionLeadTimeDetail_2(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),					
					String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),					
					String.valueOf(null != result.get("column_4") ? result.get("column_4") : ""),					
					String.valueOf(null != result.get("column_5") ? result.get("column_5") : ""),					
					String.valueOf(null != result.get("column_6") ? result.get("column_6") : ""),					
					String.valueOf(null != result.get("column_7") ? result.get("column_7") : ""),					
					String.valueOf(null != result.get("column_8") ? result.get("column_8") : ""),					
					String.valueOf(null != result.get("column_9") ? result.get("column_9") : ""),					
					String.valueOf(null != result.get("column_10") ? result.get("column_10") : ""),					
					String.valueOf(null != result.get("column_11") ? result.get("column_11") : ""),					
					String.valueOf(null != result.get("column_12") ? result.get("column_12") : ""),					
					String.valueOf(null != result.get("column_13") ? result.get("column_13") : ""),					
					String.valueOf(null != result.get("column_14") ? result.get("column_14") : ""),					
					String.valueOf(null != result.get("column_15") ? result.get("column_15") : ""),					
					String.valueOf(null != result.get("column_16") ? result.get("column_16") : ""),					
					String.valueOf(null != result.get("column_17") ? result.get("column_17") : ""),					
					String.valueOf(null != result.get("column_18") ? result.get("column_18") : ""),					
					String.valueOf(null != result.get("column_19") ? result.get("column_19") : ""),					
					String.valueOf(null != result.get("column_20") ? result.get("column_20") : ""),					
					String.valueOf(null != result.get("column_21") ? result.get("column_21") : ""),					
					String.valueOf(null != result.get("column_22") ? result.get("column_22") : ""),										
					Integer.parseInt(String.valueOf(result.get("column_23"))),
					Integer.parseInt(String.valueOf(result.get("column_24"))),
					Integer.parseInt(String.valueOf(result.get("column_25"))),
					Integer.parseInt(String.valueOf(result.get("column_26"))),
					Integer.parseInt(String.valueOf(result.get("column_27"))),
					Integer.parseInt(String.valueOf(result.get("column_28")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;	
	}
 	
	def dataModelBpProductionRedoBodyDailyUnitPieChart(def params, def type){
		def String[] columnNames = ["column_0", "column_1"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoBodyDailyUnitPieChart(params, type);			
			for(Map<String, Object> result : results) {
				int redo = Integer.parseInt(null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "0");
				int noRedo = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				int total = redo + noRedo;
				int totalRedo =  0 == total ? 0 : (redo/total) * 100;
				int totalNoRedo = 0 == total ? 0 : (noRedo/total) * 100;
				def Object[] object = [
					String.valueOf("Redo"),					
					totalRedo
				]
				model.addRow(object)
				object = [
					String.valueOf("No Redo"),					
					totalNoRedo
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();			
		}
		return model;	
	}
	
	def dataModelBpProductionRedoBodyDailyUnitPieChartTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoBodyDailyUnitPieChartTable(params, type);			
			for(Map<String, Object> result : results) {
				int redo = Integer.parseInt(null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "0");
				int noRedo = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				int total = redo + noRedo;
				int totalRedo = 0 == total ? 0 : (redo/total) * 100;
				int totalNoRedo = 0 == total ? 0 : (noRedo/total) * 100;
				def Object[] object = [
					String.valueOf("Redo"),					
					redo,										
					totalRedo
					]
				model.addRow(object)
				object = [
					String.valueOf("No Redo"),					
					noRedo,										
					totalNoRedo
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();						
		}
		return model;	
	}
	
	def dataModelBpProductionRedoBodyDailyUnitGrafik(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoBodyDailyUnitGrafik(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),										
					Integer.parseInt(String.valueOf(null != result.get("column_1") ? result.get("column_1") : "0"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;	
	}
	
	
	def dataModelBpProductionRedoBodyDailyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoBodyDailyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_0")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;	
	}
	
	
	def dataModelBpProductionRedoBodyWeeklyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoBodyWeeklyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_0")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),										
					deffect,
					persentaseDeffect
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelBpProductionRedoBodyMonthlyUnitGrafikTable(def params, def type){	
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoBodyMonthlyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_0")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),										
					deffect,
					persentaseDeffect
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;
	}
	
	def dataModelBpProductionRedoBodyYearlyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoBodyYearlyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_0")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),										
					deffect,
					persentaseDeffect
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelBpProductionRedoBodyDailyUnitDetail(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10","column_11"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoBodyDailyUnitDetail(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),										
					Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0"),
					Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0"),
					Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0"),				
					Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0"),
					Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0"),
					Integer.parseInt(null != result.get("column_7") ? String.valueOf(result.get("column_7")) : "0"),
					Integer.parseInt(null != result.get("column_8") ? String.valueOf(result.get("column_8")) : "0"),
					Integer.parseInt(null != result.get("column_9") ? String.valueOf(result.get("column_9")) : "0"),
					Integer.parseInt(null != result.get("column_10") ? String.valueOf(result.get("column_10")) : "0"),
					Integer.parseInt(null != result.get("column_11") ? String.valueOf(result.get("column_11")) : "0")
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();									
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPreparationDailyUnitDetail(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoPreparationDailyUnitGrafikTableDetail(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0"),
					Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0"),
					Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0"),
					Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0"),
					Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0"),
					Integer.parseInt(null != result.get("column_7") ? String.valueOf(result.get("column_7")) : "0"),
					Integer.parseInt(null != result.get("column_8") ? String.valueOf(result.get("column_8")) : "0")
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;	
	}
	
	
	def dataModelBpProductionCombibothDailyGrafik(def params, def type){
		def String[] columnNames = ["column_0", "column_1","column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesCombibothDailyGrafik(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					Integer.parseInt(String.valueOf(result.get("column_2")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
			
		}
		return model;	
	}
	
	def dataModelBpProductionCombibothDailyGrafikTable(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2","column_3","column_4"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesCombibothDailyGrafikTable(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					String.valueOf(result.get("column_4"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();																									
		}
		return model;	
	}
	
	def dataModelBpProductionCombibothDailyStall1Grafik(def params, def type){
		def String[] columnNames = ["column_0", "column_1","column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesCombibothDailyStall1Grafik(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					Integer.parseInt(String.valueOf(result.get("column_2")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelBpProductionCombibothDailyStall1GrafikTable(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2","column_3","column_4"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesCombibothDailyStall1GrafikTable(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					String.valueOf(result.get("column_4"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();																						
		}
		return model;	
	}
	
	def dataModelBpProductionCombibothDailyStall2Grafik(def params, def type){
		def String[] columnNames = ["column_0", "column_1","column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesCombibothDailyStall2Grafik(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					Integer.parseInt(String.valueOf(result.get("column_2")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){		
			e.printStackTrace();
			
		}
		return model;	
	}
	
	def dataModelBpProductionCombibothDailyStall2GrafikTable(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2","column_3","column_4"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesCombibothDailyStall2GrafikTable(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					String.valueOf(result.get("column_4"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();																						
		}
		return model;	
	}
	
	def dataModelBpProductionCombibothDailyDetail(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2","column_3","column_4","column_5","column_6","column_7","column_8"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesCombibothDailyDetail(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					Integer.parseInt(String.valueOf(result.get("column_8")))					
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();																										
		}
		return model;	
	}
	
	def dataModelBpProductionOnTimeInEachProcessGrafik(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesOnTimeInEachProcessGrafik(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					Integer.parseInt(String.valueOf(result.get("column_2")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelBpProductionOnTimeInEachProcessSummary(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2","column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesOnTimeInEachProcessSummary(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelBpProductionOnTimeInEachProcessSummaryWeekly(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesOnTimeInEachProcessSummary(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2"))					
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelBpProductionOnTimeInEachProcessDetail(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2","column_3","column_4","column_5","column_6"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesOnTimeInEachProcessDetail(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					String.valueOf(result.get("column_3")),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					String.valueOf(result.get("column_6"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelBpTechnicianProductivityDetail(def params, def type){
		def String[] columnNames = ["column_0","column_1","column_2","column_3","column_4","column_5","column_6","column_7","column_8","column_9","column_10",
					"column_11","column_12","column_13","column_14","column_15","column_16","column_17"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesOnTimeInEachProcessDetail(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17")))
					]
				model.addRow(object)
			}			
		} catch(Exception e){	
			e.printStackTrace();	
		}
		return model;
	}
	
	def dataModelBpProductionRedoPreparationDailyUnitPieChart(def params, def type){
		def String[] columnNames = ["column_0", "column_1"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoPreparationDailyUnitPieChart(params, type);			
			for(Map<String, Object> result : results) {
				int redo = Integer.parseInt(null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "0");
				int noRedo = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				int total = redo + noRedo;
				int totalRedo =  0 == total ? 0 : (redo/total) * 100;
				int totalNoRedo = 0 == total ? 0 : (noRedo/total) * 100;
				def Object[] object = [
					String.valueOf("Redo"),					
					totalRedo
				]
				model.addRow(object)
				object = [
					String.valueOf("No Redo"),					
					totalNoRedo
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelBpProductionRedoPaintingDailyUnitPieChart(def params, def type){
		def String[] columnNames = ["column_0", "column_1"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoPaintingDailyUnitPieChart(params, type);			
			for(Map<String, Object> result : results) {
				int redo = Integer.parseInt(null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "0");
				int noRedo = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				int total = redo + noRedo;
				int totalRedo =  0 == total ? 0 : (redo/total) * 100;
				int totalNoRedo = 0 == total ? 0 : (noRedo/total) * 100;
				def Object[] object = [
					String.valueOf("Redo"),					
					totalRedo
				]
				model.addRow(object)
				object = [
					String.valueOf("No Redo"),					
					totalNoRedo
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelBpProductionRedoPreparationDailyUnitPieChartTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoPreparationDailyUnitPieChartTable(params, type);			
			for(Map<String, Object> result : results) {
				int redo = Integer.parseInt(null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "0");
				int noRedo = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				int total = redo + noRedo;
				int totalRedo = 0 == total ? 0 : (redo/total) * 100;
				int totalNoRedo = 0 == total ? 0 : (noRedo/total) * 100;
				def Object[] object = [
					String.valueOf("Redo"),					
					redo,										
					totalRedo
					]
				model.addRow(object)
				object = [
					String.valueOf("No Redo"),					
					noRedo,										
					totalNoRedo
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();						
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPreparationDailyUnitGrafik(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoPreparationDailyUnitGrafik(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),										
					Integer.parseInt(String.valueOf(null != result.get("column_1") ? result.get("column_1") : "0"))
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;
	}
	
	def dataModelBpProductionRedoPreparationDailyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPreparationDailyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_0")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPreparationWeeklyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPreparationWeeklyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;
	}
	
	def dataModelBpProductionRedoPreparationMonthlyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPreparationMonthlyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;
	}
	
	
	def dataModelBpProductionRedoPreparationYearlyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPreparationYearlyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;
	}
	
	def dataModelBpProductionRedoPaintingDailyUnitPieChartTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoPaintingDailyUnitPieChartTable(params, type);			
			for(Map<String, Object> result : results) {
				int redo = Integer.parseInt(null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "0");
				int noRedo = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				int total = redo + noRedo;
				int totalRedo = 0 == total ? 0 : (redo/total) * 100;
				int totalNoRedo = 0 == total ? 0 : (noRedo/total) * 100;
				def Object[] object = [
					String.valueOf("Redo"),					
					redo,										
					totalRedo
					]
				model.addRow(object)
				object = [
					String.valueOf("No Redo"),					
					noRedo,										
					totalNoRedo
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();						
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPaintingDailyUnitGrafik(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPaintingDailyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_0")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPaintingDailyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPaintingDailyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_0")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPaintingDailyUnitDetail(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
			"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			def	results = productionBPService.datatablesRedoPaintingDailyUnitGrafikTableDetail(params, type);			
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0"),
					Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0"),
					Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0"),
					Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0"),
					Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0"),
					Integer.parseInt(null != result.get("column_7") ? String.valueOf(result.get("column_7")) : "0"),
					Integer.parseInt(null != result.get("column_8") ? String.valueOf(result.get("column_8")) : "0"),
					Integer.parseInt(null != result.get("column_9") ? String.valueOf(result.get("column_9")) : "0"),
					Integer.parseInt(null != result.get("column_10") ? String.valueOf(result.get("column_10")) : "0"),
					Integer.parseInt(null != result.get("column_11") ? String.valueOf(result.get("column_11")) : "0"),
					Integer.parseInt(null != result.get("column_12") ? String.valueOf(result.get("column_12")) : "0"),
					Integer.parseInt(null != result.get("column_13") ? String.valueOf(result.get("column_13")) : "0"),
					Integer.parseInt(null != result.get("column_14") ? String.valueOf(result.get("column_14")) : "0"),
					Integer.parseInt(null != result.get("column_15") ? String.valueOf(result.get("column_15")) : "0"),
					Integer.parseInt(null != result.get("column_16") ? String.valueOf(result.get("column_16")) : "0"),
					Integer.parseInt(null != result.get("column_17") ? String.valueOf(result.get("column_17")) : "0"),
					Integer.parseInt(null != result.get("column_18") ? String.valueOf(result.get("column_18")) : "0"),
					Integer.parseInt(null != result.get("column_19") ? String.valueOf(result.get("column_19")) : "0"),
					Integer.parseInt(null != result.get("column_20") ? String.valueOf(result.get("column_20")) : "0"),
					Integer.parseInt(null != result.get("column_21") ? String.valueOf(result.get("column_21")) : "0")
					]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPaintingWeeklyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPaintingWeeklyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPaintingMonthlyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPaintingMonthlyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;	
	}
	
	def dataModelBpProductionRedoPaintingYearlyUnitGrafikTable(def params, def type){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0); 	
		try {
			int totalDeffect = 0;
			def	results = productionBPService.datatablesRedoPaintingYearlyUnitGrafikTable(params, type);			
			for(Map<String, Object> result : results){
				totalDeffect += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
			}
			
			for(Map<String, Object> result : results) {
				int deffect = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				int persentaseDeffect = 0 == totalDeffect ? 0 : (deffect / totalDeffect) * 100;
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					deffect,
					persentaseDeffect					
				]
				model.addRow(object)
			}			
		} catch(Exception e){			
			e.printStackTrace();					
		}
		return model;
	}

 	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

	def dataTablesJenisPekerjaan(){
		render productionBPService.datatablesJenisPekerjaan(params) as JSON;			
	}
	
	def dataTablesJenisProses(){
		render productionBPService.datatablesJenisProses(params) as JSON;			
	}
	
}
