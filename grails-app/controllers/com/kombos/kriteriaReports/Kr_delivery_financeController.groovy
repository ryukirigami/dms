package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_delivery_financeController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def deliveryFinanceService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
        def formatFile = "";
        def formatBuntut = "";
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01"){
			reportDeliveryFinanceBookingFee(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "02"){
			reportDeliveryFinanceOutstandingBookingFee(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "03"){
			reportDeliveryFinanceSalesReport(params);
		} else
		if(params.namaReport == "04"){
			reportDeliveryFinanceSettlementReport(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "05"){
			reportDeliveryFinanceDailyCashierReport(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "06"){
			reportDeliveryFinanceRefundReport(params);
		} else
		if(params.namaReport == "07"){
			reportDeliveryFinanceAgingAr(params);
		} else
		if(params.namaReport == "08"){
			reportDeliveryFinanceTandaTerimaInvoice(params);
		}
		
		
	}
	
	def reportDeliveryFinanceBookingFee(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("openperiode", df1.format(endDate));
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceBookingFee(params));
		parameters.put("sub_report", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Booking_Fee.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Booking_Fee", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryFinanceOutstandingBookingFee(def params,String file,String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMMM yyyy");
		DateFormat df2 = new SimpleDateFormat("MMMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("openPeriode", "s.d "+ df2.format(endDate));

		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceOutstandingBookingFee(params));
		parameters.put("deliveryFinanceAgingOutstanding", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Report_Aging_OutstandingBooking_Fee.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Report_Aging_OutstandingBooking_Fee", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryFinanceSalesReport(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceSalesReport(params));
		parameters.put("Delivery_Finance_Report_Sales_Report", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Report_Sales_Report.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Report_Sales_Report", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportDeliveryFinanceSettlementReport(def params, String file,String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceSettlementReport(params));
		parameters.put("DeliveryFinanceReport", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceSettlementSummaryReport(params));
		parameters.put("sub_total", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Settlement_Report.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Settlement_Report", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportDeliveryFinanceDailyCashierReport(def params,String file,String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceDailyCashierReport(params));
		parameters.put("sub_report", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Report_Daily_Cashier.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Report_Daily_Cashier", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
//		response.setHeader("Content-Type", "application/pdf");
//		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
//		response.outputStream << pdf.newInputStream();
	}
	
	def reportDeliveryFinanceRefundReport(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceRefundReport(params));
		parameters.put("Delivery_Finance_Report_RefundReport", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Report_RefundReport.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Report_RefundReport", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}	
	
	def reportDeliveryFinanceAgingAr(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		DateFormat df2 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("openPeriode", df2.format(endDate));
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceAgingAr(params));
		parameters.put("sub_report", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Report_AgingAr.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Report_AgingAr", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportDeliveryFinanceTandaTerimaInvoice(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = deliveryFinanceService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));				
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDeliveryFinanceTandaTerimaInvoice(params));
		parameters.put("Delivery_Finance_Report_TandaTerimaInvoice", dataSource);		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Delivery_Finance_Report_TandaTerimaInvoice.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Delivery_Finance_Report_TandaTerimaInvoice", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def dataModelDeliveryFinanceBookingFee(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
					"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column28", "column29"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def kwitansiId = "";
			def historyCustomerId = "";
			def Object[]  object = null;
			def results = deliveryFinanceService.datatablesDeliveryFinanceBookingFee1(params);			
			ArrayList list = new ArrayList();
			int nomor = 0;
			for(Map<String, Object> result : results) {
				object = new Object[30];
				String receptionId = result.get("column_0") != null ? result.get("column_0") : "";
				String invoiceId = result.get("column_1") != null ? result.get("column_1") : "";
				kwitansiId = result.get("column_12") != null ? result.get("column_12") : "";
				historyCustomerId = result.get("column_13") != null ? result.get("column_13") : "";
				
				String nowo = result.get("column_2") != null ? result.get("column_2") : "";
				String nopol = result.get("column_3") != null ? result.get("column_3") : "";
				String customerId = result.get("column_4") != null ? result.get("column_4") : "";
				String nama = result.get("column_5") != null ? result.get("column_5") : "";
				java.math.BigDecimal bookingFee = new java.math.BigDecimal(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0");
				String noInv = result.get("column_7") != null ? result.get("column_7") : "";
				java.math.BigDecimal totalInv = new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0");
				String tglSettlement = result.get("column_9") != null ? result.get("column_9") : "";
				java.math.BigDecimal jmlBayar = new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0");
				java.math.BigDecimal jmlRefund = new java.math.BigDecimal(result.get("column_11") != null ? String.valueOf(result.get("column_11")) : "0");				
				
				object[0] = nowo;
				object[1] = nopol;
				object[2] = customerId;
				object[3] = nama;
				object[4] = bookingFee;
				object[5] = noInv;
				object[6] = totalInv;
				object[7] = tglSettlement;
				object[8] = jmlBayar;
				object[9] = jmlRefund;
				object[10] = "";
				object[11] = "";
				object[12] = "";
				object[13] = "";
				object[14] = "";
				object[15] = "";
				object[16] = new java.math.BigDecimal("0");
				object[17] = new java.math.BigDecimal("0");
				object[18] = new java.math.BigDecimal("0");
				object[19] = "";
				object[20] = "";
				object[21] = ""; 
				object[22] = "";
				object[23] = "";
				object[24] = "";
				object[25] = "";
				object[26] = new java.math.BigDecimal("0");								
				nomor++;
				object[27] = String.valueOf(nomor);
				object[28] = kwitansiId;
				object[29] = historyCustomerId;
				list.add(object);
			}
			ArrayList list2 = new ArrayList();
			nomor = 0;
			for(Object[] objectt : list){	
				nomor++;
				def results2 = deliveryFinanceService.datatablesDeliveryFinanceBookingFee2(params, objectt[28]);									
				
				String tglBayar = "";
				String noKwitansi = "";
				String kasirPenerima = "";
				String metodeBayar = "";
				String namaBank = "";
				String nomorReff = "";
				java.math.BigDecimal charge = new java.math.BigDecimal("0");
				java.math.BigDecimal jumlahBf = new java.math.BigDecimal("0");
				java.math.BigDecimal totalBf = new java.math.BigDecimal("0");
				if(results2.size() > 0){					
					for(Map<String, Object> result2 : results2) {
						tglBayar = result2.get("column_0") != null ? result2.get("column_0") : "";
						noKwitansi = result2.get("column_1") != null ? result2.get("column_1") : "";
						kasirPenerima = result2.get("column_2") != null ? result2.get("column_2") : "";
						metodeBayar = result2.get("column_3") != null ? result2.get("column_3") : "";
						namaBank = result2.get("column_4") != null ? result2.get("column_4") : "";
						nomorReff = result2.get("column_5") != null ? result2.get("column_5") : "";
						charge = new java.math.BigDecimal(result2.get("column_6") != null ? String.valueOf(result2.get("column_6")) : "0");
						jumlahBf = new java.math.BigDecimal(result2.get("column_7") != null ? String.valueOf(result2.get("column_7")) : "0");
						totalBf = new java.math.BigDecimal(totalBf.doubleValue() + jumlahBf.doubleValue());
						object = new Object[30];
						object[0] = objectt[0];
						object[1] = objectt[1];
						object[2] = objectt[2];
						object[3] = objectt[3];
						object[4] = objectt[4];
						object[5] = objectt[5];
						object[6] = objectt[6];
						object[7] = objectt[7];
						object[8] = objectt[8];
						object[9] = objectt[9];						
						object[10] = tglBayar;
						object[11] = noKwitansi;
						object[12] = kasirPenerima;
						object[13] = metodeBayar;
						object[14] = namaBank;
						object[15] = nomorReff;
						object[16] = charge;
						object[17] = jumlahBf;
						object[18] = totalBf;
						object[19] = "";
						object[20] = "";
						object[21] = ""; 
						object[22] = "";
						object[23] = "";
						object[24] = "";
						object[25] = "";
						object[26] = new java.math.BigDecimal("0");																
						object[27] = String.valueOf(nomor);
						object[28] = objectt[28];
						object[29] = objectt[29];
						list2.add(object);
					}
				} else {
					objectt[27] = String.valueOf(nomor);
					list2.add(objectt);
				}		
			}
			ArrayList list3 = new ArrayList();
			for(Object[] objectt : list2){		
				def results3 = deliveryFinanceService.datatablesDeliveryFinanceBookingFee3(params, objectt[29]);				
				String tglRefund = "";
				String noRefund = "";
				String noKwitansiRefund = "";
				String kasirPenerimaRefund = "";
				String metodeBayarRefund = "";
				String namaBankRefund = "";
				String noAccountRefund = "";
				java.math.BigDecimal jmlRefund = new java.math.BigDecimal("0");				
				if(results3.size() > 0){					
					for(Map<String, Object> result3 : results3) {
						tglRefund = result3.get("column_0") != null ? result3.get("column_0") : "";
						noRefund = result3.get("column_1") != null ? result3.get("column_1") : "";
						noKwitansiRefund = result3.get("column_2") != null ? result3.get("column_2") : "";
						kasirPenerimaRefund = result3.get("column_3") != null ? result3.get("column_3") : "";
						metodeBayarRefund = result3.get("column_4") != null ? result3.get("column_4") : "";
						namaBankRefund = result3.get("column_5") != null ? result3.get("column_5") : "";
						noAccountRefund = result3.get("column_6") != null ? result3.get("column_6") : "";
						jmlRefund = new java.math.BigDecimal(result3.get("column_7") != null ? String.valueOf(result3.get("column_7")) : "0");
					
						object = new Object[30];
						object[0] = objectt[0];
						object[1] = objectt[1];
						object[2] = objectt[2];
						object[3] = objectt[3];
						object[4] = objectt[4];
						object[5] = objectt[5];
						object[6] = objectt[6];
						object[7] = objectt[7];
						object[8] = objectt[8];
						object[9] = objectt[9];						
						object[10] = objectt[10];						
						object[11] = objectt[11];						
						object[12] = objectt[12];						
						object[13] = objectt[13];						
						object[14] = objectt[14];						
						object[15] = objectt[15];						
						object[16] = objectt[16];						
						object[17] = objectt[17];						
						object[18] = objectt[18];						
						object[19] = tglRefund;
						object[20] = noRefund;
						object[21] = noKwitansiRefund; 
						object[22] = kasirPenerimaRefund;
						object[23] = metodeBayarRefund;
						object[24] = namaBankRefund;
						object[25] = noAccountRefund;
						object[26] = jmlRefund;
						object[27] = objectt[27];
						object[28] = objectt[28];
						object[29] = objectt[29];
						list3.add(object);
					}
				} else {
					list3.add(objectt);
				}
			}
			
			for(Object[] objectt : list3){				

				model.addRow(objectt);
			}		
			return model;
		} catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	def dataModelDeliveryFinanceOutstandingBookingFee(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryFinanceService.datatablesDeliveryFinanceOutstandingBookingFeeReport(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : "-"),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : "-"),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : "-"),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : "-"),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : "-"),
					new java.math.BigDecimal(result.get("column_5") != null ? String.valueOf(result.get("column_5")) : "0"),
					new java.math.BigDecimal(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0"),
					new java.math.BigDecimal(result.get("column_7") != null ? String.valueOf(result.get("column_7")) : "0"),
					new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0"),
					new java.math.BigDecimal(result.get("column_9") != null ? String.valueOf(result.get("column_9")) : "0"),
					new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0")
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryFinanceSalesReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
					"column_21", "column_22", "column_23", "column_24", "column_25", "column_26"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryFinanceService.datatablesDeliveryFinanceSalesReport(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),					
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),					
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),					
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),					
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),					
					new java.math.BigDecimal(result.get("column_7") != null ? String.valueOf(result.get("column_7")) : "0"),
					new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0"),
					new java.math.BigDecimal(result.get("column_9") != null ? String.valueOf(result.get("column_9")) : "0"),
					new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0"),
					new java.math.BigDecimal(result.get("column_11") != null ? String.valueOf(result.get("column_11")) : "0"),
					new java.math.BigDecimal(result.get("column_12") != null ? String.valueOf(result.get("column_12")) : "0"),
					new java.math.BigDecimal(result.get("column_13") != null ? String.valueOf(result.get("column_13")) : "0"),
					new java.math.BigDecimal(result.get("column_14") != null ? String.valueOf(result.get("column_14")) : "0"),
					new java.math.BigDecimal(result.get("column_15") != null ? String.valueOf(result.get("column_15")) : "0"),
					new java.math.BigDecimal(result.get("column_16") != null ? String.valueOf(result.get("column_16")) : "0"),
					new java.math.BigDecimal(result.get("column_17") != null ? String.valueOf(result.get("column_17")) : "0"),
					new java.math.BigDecimal(result.get("column_18") != null ? String.valueOf(result.get("column_18")) : "0"),
					new java.math.BigDecimal(result.get("column_19") != null ? String.valueOf(result.get("column_19")) : "0"),
					new java.math.BigDecimal(result.get("column_20") != null ? String.valueOf(result.get("column_20")) : "0"),					
					new java.math.BigDecimal(result.get("column_21") != null ? String.valueOf(result.get("column_21")) : "0"),					
					new java.math.BigDecimal(result.get("column_22") != null ? String.valueOf(result.get("column_22")) : "0"),					
					new java.math.BigDecimal(result.get("column_23") != null ? String.valueOf(result.get("column_23")) : "0"),					
					new java.math.BigDecimal(result.get("column_24") != null ? String.valueOf(result.get("column_24")) : "0"),					
					new java.math.BigDecimal(result.get("column_25") != null ? String.valueOf(result.get("column_25")) : "0"),					
					String.valueOf(result.get("column_26") != null ? result.get("column_26") : "")
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryFinanceSettlementReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
					"column_21", "column_22", "column_23", "column_24"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def Object[] object = null;
			def results = deliveryFinanceService.datatablesDeliveryFinanceSettlementReport1(params);
			for(Map<String, Object> result : results) {
				object = new Object[25];
				
				String tlgJamInvoice = result.get("column_0") != null ? result.get("column_0") : "";
				String noInvoice = result.get("column_1") != null ? result.get("column_1") : "";
				String tipe = result.get("column_2") != null ? result.get("column_2") : "";
				String nopol = result.get("column_3") != null ? result.get("column_3") : "";
				String customerId = result.get("column_4") != null ? result.get("column_4") : "";
				String customerName = result.get("column_5") != null ? result.get("column_5") : "";
				java.math.BigDecimal totalInvoice = new java.math.BigDecimal(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0");
				java.math.BigDecimal bookingFee = new java.math.BigDecimal(result.get("column_7") != null ? String.valueOf(result.get("column_7")) : "0");
				java.math.BigDecimal totalBayar = new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0");
				
				object[0] = tlgJamInvoice;
				object[1] = noInvoice;
				object[2] = tipe;
				object[3] = nopol;
				object[4] = customerId;
				object[5] = customerName;
				object[6] = totalInvoice;
				object[7] = bookingFee;
				object[8] = totalBayar;
				
				def results2 = deliveryFinanceService.datatablesDeliveryFinanceSettlementReport2(params, noInvoice);
				int j = 8;
				java.math.BigDecimal totalSettlement = new java.math.BigDecimal("0");
				for(int i = 0 ; i < results2.size() && i < 4; i++){
					Map<String, Object> result2 = results2.get(i);
					for(int x = 0; x < 4; x++){
						j++;						
						if(x == 3){
							totalSettlement += new java.math.BigDecimal(null == result2.get("column_3") ? "0" : String.valueOf(result2.get("column_3")));
							object[j]  = new java.math.BigDecimal(null == result2.get("column_3") ? "0" : String.valueOf(result2.get("column_3")));;
						} else {
							object[j] = result2.get("column_"+x) != null ? result2.get("column_"+x) : "";
						}
					}
				}
				
				for(int i = j+1; j <= 20; j++){
					object[i] = null;
				}				
				
				def results3 = deliveryFinanceService.datatablesDeliveryFinanceSettlementReport3(params, noInvoice);
				for(int i = 0 ; i < 1; i++){
					Map<String, Object> result2 = results3.get(i);
					object[21] = new java.math.BigDecimal(result2.get("column_0") != null ? String.valueOf(result2.get("column_0")) : "0");
					object[22] = new java.math.BigDecimal(result2.get("column_1") != null ? String.valueOf(result2.get("column_1")) : "0");
					object[23] = new java.math.BigDecimal(result2.get("column_2") != null ? String.valueOf(result2.get("column_2")) : "0");
					object[24] = totalSettlement;
				}				
				
				model.addRow(object);
			
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def dataModelDeliveryFinanceSettlementSummaryReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			Object[] object = new Object[12];
			
			def results = deliveryFinanceService.datatablesDeliveryFinanceSettlementReport2(params, 'NOP');	
			if(results.size() > 0){
				java.math.BigDecimal cash = new java.math.BigDecimal("0");
				java.math.BigDecimal transfer = new java.math.BigDecimal("0");
				java.math.BigDecimal transferBCA = new java.math.BigDecimal("0");
				java.math.BigDecimal transferMANDIRI = new java.math.BigDecimal("0");
				java.math.BigDecimal kartuKredit = new java.math.BigDecimal("0");
				java.math.BigDecimal kartuKreditBNI = new java.math.BigDecimal("0");
				java.math.BigDecimal debit = new java.math.BigDecimal("0");
				java.math.BigDecimal debitBNI = new java.math.BigDecimal("0");
				java.math.BigDecimal debitBCA = new java.math.BigDecimal("0");
				java.math.BigDecimal debitMANDIRI = new java.math.BigDecimal("0");
				java.math.BigDecimal wbs = new java.math.BigDecimal("0");
				for(Map<String, Object> result : results) {
					String metodeBayar = result.get("column_0") != null ? result.get("column_0") : "";
					String namaBank = result.get("column_1") != null ? result.get("column_1") : "";
					java.math.BigDecimal jumlahBayar = new java.math.BigDecimal(result.get("column_3") != null ? String.valueOf(result.get("column_3")) : "0");
					
					if(("CASH").equalsIgnoreCase(metodeBayar)){
						cash += jumlahBayar;
					}
					
					if(("TRANSFER").equalsIgnoreCase(metodeBayar)){
						transfer += jumlahBayar;
					}
					
					if(("TRANSFER").equalsIgnoreCase(metodeBayar) && ("BCA").equalsIgnoreCase(namaBank)){
						transferBCA += jumlahBayar;
					}
					
					if(("TRANSFER").equalsIgnoreCase(metodeBayar) && ("MANDIRI").equalsIgnoreCase(namaBank)){
						transferMANDIRI += jumlahBayar;
					}
					
					if(("KARTU KREDIT").equalsIgnoreCase(metodeBayar.trim())){
						kartuKredit += jumlahBayar;						
					}
					
					if(("KARTU KREDIT").equalsIgnoreCase(metodeBayar.trim()) && ("BNI").equalsIgnoreCase(namaBank)){						
						kartuKreditBNI += jumlahBayar;
					}
					
					if(("DEBIT").equalsIgnoreCase(metodeBayar)){
						debit += jumlahBayar;											
					}
					
					if(("DEBIT").equalsIgnoreCase(metodeBayar) && ("BNI").equalsIgnoreCase(namaBank)){
						debitBNI += jumlahBayar;					
					}
					
					if(("DEBIT").equalsIgnoreCase(metodeBayar) && ("BCA").equalsIgnoreCase(namaBank)){
						debitBCA += jumlahBayar;					
					}
					
					if(("DEBIT").equalsIgnoreCase(metodeBayar) && ("MANDIRI").equalsIgnoreCase(namaBank)){
						debitMANDIRI += jumlahBayar;					
					}
					
					if(("DEBIT").equalsIgnoreCase(metodeBayar) && ("MANDIRI").equalsIgnoreCase(namaBank)){
						wbs += jumlahBayar;					
					}								
				}
				
				object[0] = cash;
				object[1] = transfer;
				object[2] = transferBCA;
				object[3] = transferMANDIRI;
				object[4] = kartuKredit;
				object[5] = kartuKreditBNI;
				object[6] = debit;
				object[7] = debitBNI;
				object[8] = debitBCA;
				object[9] = debitMANDIRI;
				object[10] = wbs;
				object[11] = new java.math.BigDecimal(cash.doubleValue() + transfer.doubleValue() + kartuKredit.doubleValue() + debit.doubleValue() + wbs.doubleValue());
				model.addRow(object);	
			} else {
				object[0] = new java.math.BigDecimal("0");
				object[1] = new java.math.BigDecimal("0");
				object[2] = new java.math.BigDecimal("0");
				object[3] = new java.math.BigDecimal("0");
				object[4] = new java.math.BigDecimal("0");
				object[5] = new java.math.BigDecimal("0");
				object[6] = new java.math.BigDecimal("0");
				object[7] = new java.math.BigDecimal("0");
				object[8] = new java.math.BigDecimal("0");
				object[9] = new java.math.BigDecimal("0");
				object[10] = new java.math.BigDecimal("0");
				object[11] = new java.math.BigDecimal("0");				
				model.addRow(object);				
			}			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
	
	def dataModelDeliveryFinanceDailyCashierReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryFinanceService.datatablesDeliveryFinanceDailyCashierReport(params);
			for(Map<String, Object> result : results) {				
				java.math.BigDecimal balance = new java.math.BigDecimal(result.get("column_0") != null ? String.valueOf(result.get("column_0")) : "0");
				String date = String.valueOf(result.get("column_1") != null ? result.get("column_1") : "");
				String noreff = String.valueOf(result.get("column_2") != null ? result.get("column_2") : "");
				String nopol = String.valueOf(result.get("column_3") != null ? result.get("column_3") : "");
				String customerId = String.valueOf(result.get("column_4") != null ? result.get("column_4") : "");
				String customerName = String.valueOf(result.get("column_5") != null ? result.get("column_5") : "");
				java.math.BigDecimal cashIn = new java.math.BigDecimal(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0");
				java.math.BigDecimal cashOut = new java.math.BigDecimal(result.get("column_7") != null ? String.valueOf(result.get("column_7")) : "0");
				java.math.BigDecimal debit = new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0");
				
				String namaBank = deliveryFinanceService.dataTablesNamaBank(noreff);
				
				java.math.BigDecimal creditAmount = new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0");
				java.math.BigDecimal charge = new java.math.BigDecimal(result.get("column_11") != null ? String.valueOf(result.get("column_11")) : "0");
				java.math.BigDecimal transfer = new java.math.BigDecimal(result.get("column_12") != null ? String.valueOf(result.get("column_12")) : "0");
				
				def Object[] object = [balance, date, noreff, nopol, customerId, customerName, cashIn, cashOut, debit, namaBank, creditAmount, charge, transfer]				
				
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
	
	def dataModelDeliveryFinanceRefundReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryFinanceService.datatablesDeliveryFinanceRefundReport(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),					
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),					
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),					
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),					
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),					
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),					
					new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0"),	
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : "")					
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
	
	def dataModelDeliveryFinanceAgingAr(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryFinanceService.datatablesDeliveryFinanceAgingAr(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),					
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),					
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),					
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),					
					new java.math.BigDecimal(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0"),	
					new java.math.BigDecimal(result.get("column_7") != null ? String.valueOf(result.get("column_7")) : "0"),	
					new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0"),	
					new java.math.BigDecimal(result.get("column_9") != null ? String.valueOf(result.get("column_9")) : "0"),	
					new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0")	
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
	
	def dataModelDeliveryFinanceTandaTerimaInvoice(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = deliveryFinanceService.datatablesDeliveryFinanceTandaTerimaInvoice(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),					
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					new java.math.BigDecimal(result.get("column_3") != null ? String.valueOf(result.get("column_3")) : "0")										
				]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
    def datatablesCustomerList(){
        session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
        render deliveryFinanceService.datatablesCustomerList(params) as JSON;
    }
	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

}