package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.maintable.KrRFiDaily
import com.kombos.production.FinalInspection
import com.kombos.reception.Appointment
import com.kombos.woinformation.JobRCP
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Production_grController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def productionGRService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {}

	def previewData(){
        def formatFile = "";
        def formatBuntut = "";
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01"){
			generateReportTechnicianProductivity(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "02"){
			generateReportLeadTimeStagnation(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "03"){
			generateReportLeadTimeTotalStagnation(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "04"){
			generateReportLeadTimeStagnationMonthly(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "05"){
			generateReportLeadTimeStagnationTotalMonthly(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "06"){
			generateFinalInspectionDaily(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "07"){
//            Monthly(params);
			generateFinalInspectionMonthly(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "08"){
			generateFinalInspectionYearly(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "09"){
			generateReportDetail(params,formatFile,formatBuntut);
		}

 	}

 	def generateReportDetail(def params, String file, String format){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
 		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", "Sunter");
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", "All");		
		parameters.put("kategoriWorkshop", "General Repair");		
		parameters.put("report", "Detail");	
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Final_Inspection_Detail.jasper", parameters,
						new JRTableModelDataSource(detailReportModel(params)));
		File pdf = File.createTempFile("GR_Final_Inspection_Detail", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
 	}

 	def generateFinalInspectionDaily(def params, String file, String format){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

		def result = productionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");		
		parameters.put("report", "Daily");			
		JRDataSource dataSource = new JRTableModelDataSource(finalInspectionDailyModel(params));
		parameters.put("summaryDataSource", dataSource);		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(detailReportModel(params));
			parameters.put("detailDataSource", dataSource);
		}		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Final_Inspection_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Final_Inspection_Daily", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
 	}

 	def Monthly(def params){
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();

        def result = productionGRService.getWorkshopByCode(params.workshop);
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode",  params.bulan1+" - "+params.bulan2 +" - "+params.tahun);
        parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
        parameters.put("kategoriWorkshop", "General Repair");
        parameters.put("report", "Monthly");
        JRDataSource dataSource = new JRTableModelDataSource(monthly(params));
        parameters.put("summaryDataSource", dataSource);
        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(detailReportModel(params));
            parameters.put("detailDataSource", dataSource);
        }
        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Final_Inspection_Yearly.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("GR_Final_Inspection_Yearly", ".pdf");
        pdf.deleteOnExit();
        JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        response.setHeader("Content-Type", "application/pdf");
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        response.outputStream << pdf.newInputStream();
 	}

    def generateFinalInspectionMonthly(def params, String file, String format){
        DateFormat df = new SimpleDateFormat("M yyyy");
        DateFormat df1 = new SimpleDateFormat("MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def bulan1 = params.bulan1 + " " + params.tahun
        def bulan2 = params.bulan2 + " " + params.tahun
        def startDate = df.parse(bulan1);
        def endDate = df.parse(bulan2);
        def result = productionGRService.getWorkshopByCode(params.workshop);
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
        parameters.put("kategoriWorkshop", "General Repair");
        parameters.put("report", "Monthly");
        JRDataSource dataSource = new JRTableModelDataSource(finalInspectionMonthlyModel(params));
        parameters.put("summaryDataSource", dataSource);
        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(detailReportModel(params));
            parameters.put("detailDataSource", dataSource);
        }
        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Final_Inspection_Daily.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("GR_Final_Inspection_Monthly", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }


 	def generateFinalInspectionYearly(def params, String file, String format){
 		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = productionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode",  params.tahun2+" - "+params.tahun);
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");		
		parameters.put("report", "Yearly");	
		JRDataSource dataSource = new JRTableModelDataSource(finalInspectionYearlyModel(params));
		parameters.put("summaryDataSource", dataSource);		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(detailReportModel(params));
			parameters.put("detailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Final_Inspection_Yearly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Final_Inspection_Yearly", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

 	def generateReportTechnicianProductivity(def params, String file, String format){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		DateFormat df2 = new SimpleDateFormat("MMMM");
		DateFormat df3 = new SimpleDateFormat("yyyy");

		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

		def result = productionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df3.format(startDate));
		parameters.put("periodeBulan", df2.format(startDate));
        parameters.put("periodeTanggal", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("kategoriWorkshop", "General Repair");

		JRDataSource dataSource = new JRTableModelDataSource(detailGeneralRepairModel(params));
		parameters.put("detailGeneralRepair", dataSource);
        dataSource = new JRTableModelDataSource(reportSummaryModel(params));
        parameters.put("reportSummary", dataSource);
//      ===== Comment di bawah jgn dulu di hapus ====
//      ===== DetilServiceBerkala ====
//		dataSource = new JRTableModelDataSource(detailServiceBerkalaModel(params));
//		parameters.put("detailServiceBerkala", dataSource);
//        =-=-= grafikTechnicianProductivity =-=-=
//		dataSource = new JRTableModelDataSource(grafikGeneralRepairModel(params));
//		parameters.put("grafikGeneralRepair", dataSource);
//		dataSource = new JRTableModelDataSource(grafikServiceBerkalaModel(params));
//		parameters.put("grafikServiceBerkala", dataSource);
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Technician_Productivity.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Technician_Productivity", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
 	}

 	def generateReportLeadTimeStagnation(def params, String file, String format){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");

		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		def result = productionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Daily");
		JRDataSource dataSource
//        dataSource = new JRTableModelDataSource(processLeadTimeGrafikModel(params));
//		parameters.put("processLeadTimeGrafik", dataSource);
//		dataSource = new JRTableModelDataSource(stagnationLeadTimeGrafikModel(params));
//		parameters.put("stagnationLeadTimeGrafik", dataSource);
		dataSource = new JRTableModelDataSource(stagnationLeadTimeTableModel(params));
		parameters.put("stagnationLeadTimeTable", dataSource);

//		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Lead_Time_Stagnation.jasper", parameters,
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Lead_Time_PStagnation.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("ProductionGR_Lead_Time_Stagnation_", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
 	}

 	def generateReportLeadTimeStagnationMonthly(def params, String file, String format){
 		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");		
		parameters.put("report", "Monthly");		

		JRDataSource dataSource
//      dataSource = new JRTableModelDataSource(processLeadTimeGrafikMonthlyModel(params));
//		parameters.put("processLeadTimeGrafik", dataSource);
//		dataSource = new JRTableModelDataSource(stagnationLeadTimeGrafikMonthlyModel(params));
//		parameters.put("stagnationLeadTimeGrafik", dataSource);
		dataSource = new JRTableModelDataSource(stagnationLeadTimeTableMonthlyModel(params));
		parameters.put("stagnationLeadTimeTable", dataSource);
		
//		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Lead_Time_Stagnation_Monthly.jasper", parameters,
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Lead_Time_PStagnation_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Lead_Time_Stagnation_Monthly", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
 	}

 	def generateReportLeadTimeStagnationTotalMonthly(def params, String file, String format){
 		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = productionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");		
		parameters.put("report", "Monthly");		

		JRDataSource dataSource = new JRTableModelDataSource(processLeadTimeGrafikTotalMonthlyModel(params));
		parameters.put("totalProcessSummary", dataSource);
		dataSource = new JRTableModelDataSource(stagnationLeadTimeGrafikTotalMonthlyModel(params));
		parameters.put("totalStagnationSummary", dataSource);
		dataSource = new JRTableModelDataSource(stagnationLeadTimeTableTotalMonthlyModel(params));
		parameters.put("stagnationLeadTimeTable", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Lead_Time_Stagnation_Total_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Lead_Time_Stagnation_Total_Monthly", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
 	}

 	def generateReportLeadTimeTotalStagnation(def params, String file, String format){
 		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = productionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));		
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : productionGRService.ambilJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("kategoriWorkshop", "General Repair");		
		parameters.put("report", "Daily");		

		JRDataSource dataSource = new JRTableModelDataSource(processLeadTimeTotalGrafikModel(params));
		parameters.put("totalProcessSummary", dataSource);
		dataSource = new JRTableModelDataSource(stagnationLeadTimeTotalGrafikModel(params));
		parameters.put("totalStagnationSummary", dataSource);
		dataSource = new JRTableModelDataSource(stagnationLeadTimeTableModel(params));
		parameters.put("stagnationLeadTimeTable", dataSource);
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Lead_Time_Stagnation_Total.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Lead_Time_Stagnation_Total", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
 	}

 	def detailGeneralRepairModel(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7",
 		   		"column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesGrTechnicianProductivityDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Float.parseFloat(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
                    Float.parseFloat(String.valueOf(result.get("column_15"))),
                    Float.parseFloat(String.valueOf(result.get("column_16"))),
                    Float.parseFloat(String.valueOf(result.get("column_17")))
					]
				model.addRow(object)				
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
 	}

 	def detailServiceBerkalaModel(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7",
 		   		"column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17"];		
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesGrTechnicianProductivityDetailServiceBerkala(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					String.valueOf(result.get("column_1")),
					String.valueOf(result.get("column_2")),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17")))
					]
				model.addRow(object)				
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
 	}

 	def reportSummaryModel(def params){
		def kepalaBengkel = "";
		try {
			kepalaBengkel = productionGRService.getKepalaBengkel(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def jumlahSA =  "0";
		try {
			jumlahSA = productionGRService.getJumlahSA(params)
		} catch(Exception e){
			e.printStackTrace();
		}		
		def jumlahForeman = "0";
		try {
			jumlahForeman = productionGRService.getJumlahForeman(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def jumlahTeknisi = "0";
		try {
			jumlahTeknisi = productionGRService.getJumlahTeknisi(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def jumlahStallSBNP = "0";
		try {
			jumlahStallSBNP = productionGRService.getJumlahStallSBNP(params);
		} catch (Exception e){
			e.printStackTrace();
		}
		def jumlahStallGR = "0";
		try {
			jumlahStallGR = productionGRService.getJumlahStallGR(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def te = "0";
		try {
			te = productionGRService.getTE(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def lu = "0";
		try {
			lu = productionGRService.getLU(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def op = "0";
		try {
			op = productionGRService.getOP(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def serviceBerkalaRUM = "0";
		try {
			serviceBerkalaRUM = productionGRService.serviceBerkalaRUM(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def serviceBerkalaRPS = "0";
		try {
			serviceBerkalaRPS = productionGRService.serviceBerkalaRPS(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def generalRepairRUM = "0";
		try {
			generalRepairRUM = productionGRService.generalRepairRUM(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def generalRepairRPS = "0";
		try {
			generalRepairRPS = productionGRService.generalRepairRPS(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def grandTotalRUM = "0";
		try {
			grandTotalRUM = productionGRService.grandTotalRUM(params);
		} catch(Exception e){
			e.printStackTrace();
		}
		def grandTotalRPS = "0";
		try {
			grandTotalRPS = productionGRService.grandTotalRPS(params);
		} catch(Exception e){
			e.printStackTrace();
		}		
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14"]
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);
 		def String[] data = [kepalaBengkel, jumlahSA, jumlahForeman, jumlahTeknisi, jumlahStallSBNP, jumlahStallGR, te, lu, op, serviceBerkalaRUM, serviceBerkalaRPS, generalRepairRUM, generalRepairRPS, grandTotalRUM, grandTotalRPS];
 		dataModel.addRow(data);
 		return dataModel;
 	}

 	def grafikGeneralRepairModel(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesGrTechnicianProductivityDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf("JA/JT"),
					String.valueOf(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_15")))					
					]
				model.addRow(object)			
				object = [
					String.valueOf("JF/JT"),
					String.valueOf(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_16")))					
					]
				model.addRow(object)					
				object = [
					String.valueOf("JF/JA"),
					String.valueOf(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_17")))					
					]
				model.addRow(object)					
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model; 		
 	}

 	def grafikServiceBerkalaModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesGrTechnicianProductivityDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf("JA/JT"),
					String.valueOf(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_15")))					
					]
				model.addRow(object)
				object = [
					String.valueOf("JF/JT"),
					String.valueOf(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_16")))					
					]
				model.addRow(object)					
				object = [
					String.valueOf("JF/JA"),
					String.valueOf(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_17")))					
					]
				model.addRow(object)					
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model; 
 	}

 	def processLeadTimeGrafikModel(def params){
		def String[] columnNames = ["column_0","column_1","column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesProcessLeadTimeGrafik(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf("Reception"),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)							
				object = [
					String.valueOf("Pre Diagnose"),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					String.valueOf(result.get("column_0"))
					]
				model.addRow(object)				
				object = [
					String.valueOf("Production"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Inspection During Repair"),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Final Inspection"),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Process JOC"),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Invoicing"),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Washing"),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Delivery"),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	
 	}

 	def processLeadTimeGrafikMonthlyModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["Reception", 10, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Pre Diagnose", 20, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Production", 5, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Inspection During Repair", 10, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Jan-2011"];
 		dataModel.addRow(data);


 		data = ["Reception", 10, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Pre Diagnose", 20, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Production", 5, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Inspection During Repair", 10, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Feb-2011"];
 		dataModel.addRow(data);

 		data = ["Reception", 10, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Pre Diagnose", 20, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Production", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Inspection During Repair", 10, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		return dataModel;
 	}

 	def processLeadTimeGrafikTotalMonthlyModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["Reception", 10, "Reception"];
 		dataModel.addRow(data);
 		data = ["Pre Diagnose", 20, "Pre Diagnose"];
 		dataModel.addRow(data);
 		data = ["Production", 5, "Production"];
 		dataModel.addRow(data);
 		data = ["Inspection During Repair", 10, "Inspection During Repair"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Final Inspection"];
 		dataModel.addRow(data);
 		data = ["JOC", 8, "JOC"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Invoicing"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Washing"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Delivery"];
 		dataModel.addRow(data);
 		return dataModel;
 	}

 	def processLeadTimeTotalGrafikModel(def params){
		def String[] columnNames = ["column_0","column_1","column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesLeadTimeTotalGrafik(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf("Reception"),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					Integer.parseInt(String.valueOf(result.get("column_1")))					
					]
				model.addRow(object)							
				object = [
					String.valueOf("Pre Diagnose"),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_2"))),					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Production"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Inspection During Repair"),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_4")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Final Inspection"),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_5")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("JOC"),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_6")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Invoicing"),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_7")))					
					]
				model.addRow(object)								
				object = [
					String.valueOf("Delivery"),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_8")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Washing"),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_9")))					
					]
				model.addRow(object)								
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	 		
 	}

 	def stagnationLeadTimeGrafikModel(def params){
		def String[] columnNames = ["column_0","column_1","column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesStagnationLeadTimeGrafik(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf("Job Stopped"),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)							
				object = [
					String.valueOf("WF Reception"),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Job Dispatch"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					String.valueOf(result.get("column_0"))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Clock On"),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Final Inspection"),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("JOC"),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Invoicing"),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Washing"),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Notifikasi"),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("Settlement"),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Delivery"),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					String.valueOf(result.get("column_0"))										
					]
				model.addRow(object)				
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
 		
 	}

 	def stagnationLeadTimeGrafikMonthlyModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_1"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["Job Stopped", 10, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Reception", 20, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Job Dispatch", 5, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Clock On", 10, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["JOC", 8, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Notification", 5, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Settlement", 5, "Jan-2011"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Jan-2011"];
 		dataModel.addRow(data);

 		data = ["Job Stopped", 10, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Reception", 20, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Job Dispatch", 5, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Clock On", 10, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["JOC", 8, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Notification", 5, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Settlement", 5, "Feb-2011"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Feb-2011"];
 		dataModel.addRow(data);

 		data = ["Job Stopped", 10, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Reception", 20, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Job Dispatch", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Clock On", 10, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["JOC", 8, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Notification", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Settlement", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Mar-2011"];
 		dataModel.addRow(data);
 		return dataModel;
 	}

 	def stagnationLeadTimeGrafikTotalMonthlyModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_1"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data = ["Job Stopped", 10, "Job Stopped"];
 		dataModel.addRow(data);
 		data = ["Reception", 20, "Reception"];
 		dataModel.addRow(data);
 		data = ["Job Dispatch", 5, "Job Dispatch"];
 		dataModel.addRow(data);
 		data = ["Clock On", 10, "Clock On"];
 		dataModel.addRow(data);
 		data = ["Final Inspection", 8, "Final Inspection"];
 		dataModel.addRow(data);
 		data = ["JOC", 8, "JOC"];
 		dataModel.addRow(data);
 		data = ["Invoicing", 10, "Invoicing"];
 		dataModel.addRow(data);
 		data = ["Washing", 5, "Washing"];
 		dataModel.addRow(data);
 		data = ["Notification", 5, "Notification"];
 		dataModel.addRow(data);
 		data = ["Settlement", 5, "Settlement"];
 		dataModel.addRow(data);
 		data = ["Delivery", 5, "Delivery"];
 		dataModel.addRow(data); 		
 		return dataModel;
 	}

 	def stagnationLeadTimeTotalGrafikModel(def params){
 		def String[] columnNames = ["column_0","column_1","column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = productionGRService.datatablesStagnationLeadTimeTotalGrafik(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf("Job Stopped"),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					Integer.parseInt(String.valueOf(result.get("column_1")))					
					]
				model.addRow(object)							
				object = [
					String.valueOf("WF Reception"),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_2")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Job Dispatch"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_3")))
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Clock On"),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_4")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Final Inspection"),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_5")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("JOC"),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_6")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Invoicing"),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_7")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Washing"),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_8")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Notifikasi"),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_9")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("Settlement"),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_10")))					
					]
				model.addRow(object)				
				object = [
					String.valueOf("WF Delivery"),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_11")))					
					]
				model.addRow(object)				
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	 		
 	}

 	def stagnationLeadTimeTableModel(def params){
 		def String[] columnNames = ["column_0","column_1","column_2","column_3","column_4","column_5","column_6","column_7","column_8",
						"column_9","column_10","column_11","column_12","column_13","column_14","column_15","column_16","column_17","column_18","column_19",
						"column_20","column_21","column_22","column_23","column_24","column_25","column_26","column_27"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {			
			def results = productionGRService.datatablesStagnationLeadTimeTable(params);			
			for(Map<String, Object> result : results) {		
				def column_0 = null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "";
				def column_1 = String.valueOf(result.get("column_1") != null ? result.get("column_1") : "0");
                def column_2 = String.valueOf(result.get("column_2") != null ? result.get("column_2") : "0");
                def column_3 = String.valueOf(result.get("column_3") != null ? result.get("column_3") : "0");
                def column_4 = String.valueOf(result.get("column_4") != null ? result.get("column_4") : "0");
                def column_5 = String.valueOf(result.get("column_5") != null ? result.get("column_5") : "0");
                def column_6 = String.valueOf(result.get("column_6") != null ? result.get("column_6") : "0");
                def column_7 = String.valueOf(result.get("column_7") != null ? result.get("column_7") : "0");
                def column_8 = String.valueOf(result.get("column_8") != null ? result.get("column_8") : "0");
                def column_9 = String.valueOf(result.get("column_9") != null ? result.get("column_9") : "0");
                def column_10 = String.valueOf(result.get("column_10") != null ? result.get("column_10") : "0");
                def column_11 = String.valueOf(result.get("column_11") != null ? result.get("column_11") : "0");
                def column_12 = String.valueOf(result.get("column_12") != null ? result.get("column_12") : "0");
                def column_13 = String.valueOf(result.get("column_13") != null ? result.get("column_13") : "0");
                def column_14 = String.valueOf(result.get("column_14") != null ? result.get("column_14") : "0");
                def column_15 = String.valueOf(result.get("column_15") != null ? result.get("column_15") : "0");
                def column_16 = String.valueOf(result.get("column_16") != null ? result.get("column_16") : "0");
                def column_17 = String.valueOf(result.get("column_17") != null ? result.get("column_17") : "0");
                def column_18 = String.valueOf(result.get("column_18") != null ? result.get("column_18") : "0");
                def column_19 = String.valueOf(result.get("column_19") != null ? result.get("column_19") : "0");
                def column_20 = String.valueOf(result.get("column_20") != null ? result.get("column_20") : "0");
                def column_21 = String.valueOf(result.get("column_21") != null ? result.get("column_21") : "0");
                def column_22 = String.valueOf(result.get("column_22") != null ? result.get("column_22") : "0");
                def column_23 = String.valueOf(result.get("column_23") != null ? result.get("column_23") : "0");
                def column_24 = String.valueOf(result.get("column_24") != null ? result.get("column_24") : "0");
                def column_25 = String.valueOf(result.get("column_25") != null ? result.get("column_25") : "0");
                def column_26 = String.valueOf(result.get("column_26") != null ? result.get("column_26") : "0");
                def column_27 = String.valueOf(result.get("column_27") != null ? result.get("column_27") : "0");
                def Object[] object = [
					column_0,column_1,column_2,column_3,column_4,column_5,column_6,column_7,column_8,column_9,column_10,
					column_11,column_12,column_13,column_14,column_15,column_16,column_17,column_18,column_19,column_20,
                    column_21,column_22,column_23,column_24,column_25,column_26,column_27
				];
				model.addRow(object);
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
 	}

 	def stagnationLeadTimeTableMonthlyModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
 					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22",
 					"column_23", "column_24", "column_25", "column_26", "column_27", "column_27", "column_28", "column_29", "column_30", "column_31", "column_32"];
 		def DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);

        try {
            def results = productionGRService.datatablesStagnationLeadTimeTableMontly(params);
            for(Map<String, Object> result : results) {
                def column_0 = null != result.get("column_0") ? String.valueOf(result.get("column_0")) : "";
                def column_1 = null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "";
                def column_2 = String.valueOf(result.get("column_2") != null ? result.get("column_2") : "0");
                def column_3 = String.valueOf(result.get("column_3") != null ? result.get("column_3") : "0");
                def column_4 = String.valueOf(result.get("column_4") != null ? result.get("column_4") : "0");
                def column_5 = String.valueOf(result.get("column_5") != null ? result.get("column_5") : "0");
                def column_6 = String.valueOf(result.get("column_6") != null ? result.get("column_6") : "0");
                def column_7 = String.valueOf(result.get("column_7") != null ? result.get("column_7") : "0");
                def column_8 = String.valueOf(result.get("column_8") != null ? result.get("column_8") : "0");
                def column_9 = String.valueOf(result.get("column_9") != null ? result.get("column_9") : "0");
                def column_10 = String.valueOf(result.get("column_10") != null ? result.get("column_10") : "0");
                def column_11 = String.valueOf(result.get("column_11") != null ? result.get("column_11") : "0");
                def column_12 = String.valueOf(result.get("column_12") != null ? result.get("column_12") : "0");
                def column_13 = String.valueOf(result.get("column_13") != null ? result.get("column_13") : "0");
                def column_14 = String.valueOf(result.get("column_14") != null ? result.get("column_14") : "0");
                def column_15 = String.valueOf(result.get("column_15") != null ? result.get("column_15") : "0");
                def column_16 = String.valueOf(result.get("column_16") != null ? result.get("column_16") : "0");
                def column_17 = String.valueOf(result.get("column_17") != null ? result.get("column_17") : "0");
                def column_18 = String.valueOf(result.get("column_18") != null ? result.get("column_18") : "0");
                def column_19 = String.valueOf(result.get("column_19") != null ? result.get("column_19") : "0");
                def column_20 = String.valueOf(result.get("column_20") != null ? result.get("column_20") : "0");
                def column_21 = String.valueOf(result.get("column_21") != null ? result.get("column_21") : "0");
                def column_22 = String.valueOf(result.get("column_22") != null ? result.get("column_22") : "0");
                def column_23 = String.valueOf(result.get("column_23") != null ? result.get("column_23") : "0");
                def column_24 = String.valueOf(result.get("column_24") != null ? result.get("column_24") : "0");
                def column_25 = String.valueOf(result.get("column_25") != null ? result.get("column_25") : "0");
                def column_26 = String.valueOf(result.get("column_26") != null ? result.get("column_26") : "0");
                def column_27 = String.valueOf(result.get("column_27") != null ? result.get("column_27") : "0");
                def column_28 = String.valueOf(result.get("column_28") != null ? result.get("column_28") : "0");
                def column_29 = String.valueOf(result.get("column_29") != null ? result.get("column_29") : "0");
                def column_30 = String.valueOf(result.get("column_30") != null ? result.get("column_30") : "0");
                def column_31 = String.valueOf(result.get("column_31") != null ? result.get("column_31") : "0");
                def column_32 = String.valueOf(result.get("column_32") != null ? result.get("column_32") : "0");

                def Object[] object = [
                        column_0,column_1,column_2,column_3,column_4,column_5,column_6,column_7,column_8,column_9,column_10,
                        column_11,column_12,column_13,column_14,column_15,column_16,column_17,column_18,column_19,column_20,
                        column_21,column_22,column_23,column_24,column_25,column_26,column_27,column_28,column_29,column_30,
                        column_31,column_32
                ];
                dataModel.addRow(object);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return dataModel;
 	}

 	def stagnationLeadTimeTableTotalMonthlyModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
 					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22",
 					"column_23", "column_24", "column_25", "column_26", "column_27", "column_27"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def String[] data = ["2011", "Januari", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45",
 					"00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "01:20", "00:45"];			
 		dataModel.addRow(data);
 		data = ["2011", "Februari", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45",
 					"00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "01:20", "00:45"];
 		dataModel.addRow(data);
 		data = ["2011", "Maret", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45",
 					"00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "01:20", "00:45"];
 		dataModel.addRow(data);
 		data = ["2011", "April", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45",
 					"00:45", "00:41", "01:20", "01:20", "00:45", "00:45", "00:41", "01:20", "01:20", "00:45", "01:20", "00:45"];
 		dataModel.addRow(data);
 		return dataModel;
 	}

 	def finalInspectionDailyModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "TOT_COLUMN_2", "TOT_COLUMN_3", "TOT_COLUMN_4", "TOT_COLUMN_5", "TOT_COLUMN_6", "TOT_COLUMN_7"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data
        String bulanParam =  params.tanggal
        String bulanParam2 =   params.tanggal2
        Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
        Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
        Date tanggal = tgl-1
        int selisih = tgl2 - tgl
        int fiT = 0, fiA = 0, ofI = 0, sfO = 0, sR = 0, fiR = 0, fiTtot = 0, fiAtot = 0, ofItot = 0, sfOtot = 0, sRtot = 0, fiRtot = 0
        def nMforeman, tglFi

        (0..selisih).each {
            tanggal += 1
            def listFiDaily = KrRFiDaily.createCriteria().list {
                ge("tglFi", tanggal)
                lt("tglFi", tanggal + 1)
                eq("companyDealer", CompanyDealer.get(params.workshop as long))
                groupProperty("tglFi")
            }

            if(listFiDaily.size() > 0){
                fiTtot = 0
                fiAtot = 0
                ofItot = 0
                sfOtot = 0
                sRtot = 0
                fiRtot = 0

                listFiDaily.each {
                    tglFi = it?.tglFi != null ? it?.tglFi?.format("dd-MMM-yyyy") : "-"
                    nMforeman = it?.namaForeman?.t001Inisial != null ? it?.namaForeman?.t001Inisial : "-"
                    fiT = it?.fiTarget != null ? Math.round(it?.fiTarget) :0
                    fiA = it?.fiActual != null ? Math.round(it?.fiActual) : 0
                    ofI = it?.otherFi != null ? Math.round(it?.otherFi) : 0
                    sfO = it?.solvedFo != null ? Math.round(it?.solvedFo) : 0
                    sR = it?.solvedRate != null ? Math.round(it?.solvedRate) : 0
                    fiR = it?.fiRate != null ? Math.round(it.fiRate) : 0
                    fiTtot += fiT
                    fiAtot += fiA
                    ofItot += ofI
                    sfOtot += sfO
                    sRtot += sR
                    fiRtot += fiR

                    data = [
                            tglFi,
                            nMforeman,
                            Integer.parseInt(String.valueOf(fiT)),
                            Integer.parseInt(String.valueOf(fiA)),
                            Integer.parseInt(String.valueOf(ofI)),
                            Integer.parseInt(String.valueOf(sfO)),
                            Integer.parseInt(String.valueOf(sR)),
                            Integer.parseInt(String.valueOf(fiR)),
                            Integer.parseInt(String.valueOf(fiTtot)),
                            Integer.parseInt(String.valueOf(fiAtot)),
                            Integer.parseInt(String.valueOf(ofItot)),
                            Integer.parseInt(String.valueOf(sfOtot)),
                            Integer.parseInt(String.valueOf(sRtot)),
                            Integer.parseInt(String.valueOf(fiRtot))
                    ];
                    dataModel.addRow(data);
                }

            }else{
                data = [tanggal.format("dd-MMM-yyyy"), "-",
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0))]
                dataModel.addRow(data)
            }
        }
 		return dataModel;
 	}

    def finalInspectionMonthlyModel(def params){
        DateFormat df = new SimpleDateFormat("M yyyy");
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "TOT_COLUMN_2", "TOT_COLUMN_3", "TOT_COLUMN_4", "TOT_COLUMN_5", "TOT_COLUMN_6", "TOT_COLUMN_7"];
        DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);
        def Object[] data
        String bulanParam =  params.tanggal
        String bulanParam2 =   params.tanggal2
        Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
        Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
        Date tanggal = tgl-1
        int selisih = tgl2 - tgl
        int fiT = 0, fiA = 0, ofI = 0, sfO = 0, sR = 0, fiR = 0, fiTtot = 0, fiAtot = 0, ofItot = 0, sfOtot = 0, sRtot = 0, fiRtot = 0
        def nMforeman, tglFi
        (0..selisih).each {
            tanggal += 1
            def listFiDaily = KrRFiDaily.createCriteria().list {
                ge("tglFi", tanggal)
                lt("tglFi", tanggal + 1)
                eq("companyDealer", CompanyDealer.get(params.workshop as long))
                groupProperty("tglFi")
            }

            if(listFiDaily.size() > 0){
                fiTtot = 0
                fiAtot = 0
                ofItot = 0
                sfOtot = 0
                sRtot = 0
                fiRtot = 0

                listFiDaily.each {
                    tglFi = it?.tglFi != null ? it?.tglFi?.format("MMMM-yyyy") : "-"
                    nMforeman = it?.namaForeman?.t001Inisial != null ? it?.namaForeman?.t001Inisial : "-"
                    fiT = it?.fiTarget != null ? Math.round(it?.fiTarget) :0
                    fiA = it?.fiActual != null ? Math.round(it?.fiActual) : 0
                    ofI = it?.otherFi != null ? Math.round(it?.otherFi) : 0
                    sfO = it?.solvedFo != null ? Math.round(it?.solvedFo) : 0
                    sR = it?.solvedRate != null ? Math.round(it?.solvedRate) : 0
                    fiR = it?.fiRate != null ? Math.round(it.fiRate) : 0
                    fiTtot += fiT
                    fiAtot += fiA
                    ofItot += ofI
                    sfOtot += sfO
                    sRtot += sR
                    fiRtot += fiR

                    data = [
                            tglFi,
                            nMforeman,
                            Integer.parseInt(String.valueOf(fiT)),
                            Integer.parseInt(String.valueOf(fiA)),
                            Integer.parseInt(String.valueOf(ofI)),
                            Integer.parseInt(String.valueOf(sfO)),
                            Integer.parseInt(String.valueOf(sR)),
                            Integer.parseInt(String.valueOf(fiR)),
                            Integer.parseInt(String.valueOf(fiTtot)),
                            Integer.parseInt(String.valueOf(fiAtot)),
                            Integer.parseInt(String.valueOf(ofItot)),
                            Integer.parseInt(String.valueOf(sfOtot)),
                            Integer.parseInt(String.valueOf(sRtot)),
                            Integer.parseInt(String.valueOf(fiRtot))
                    ];
                    dataModel.addRow(data);
                }

            }else{
                data = [tanggal.format("MMMM-yyyy"), "-",
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0)),
                        Integer.parseInt(String.valueOf(0))]
                dataModel.addRow(data)
            }
        }
        return dataModel;

    }

 	def monthly(def params){

        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "TOT_COLUMN_2", "TOT_COLUMN_3", "TOT_COLUMN_4", "TOT_COLUMN_5", "TOT_COLUMN_6", "TOT_COLUMN_7"];
        DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);
        def Object[] data
        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy")
        Calendar cal1 = Calendar.getInstance()

        def bln1 = Integer.parseInt(params.bulan1)
        def bln2 = Integer.parseInt(params.bulan2)

        String sdate1 = "01-"+bln1+"-"+params.tahun
        Date date1 = df2.parse(sdate1)
        cal1.setTime(date1)
        def maxD = cal1.getActualMaximum(cal1.DAY_OF_MONTH)
        String tgl2 = maxD+"-"+bln2+"-"+params.tahun+" 23:58:58"

        def strDate1 = new Date().parse("dd-MM-yyyy",sdate1.toString())
        def strEndDate2 = new Date().parse("dd-MM-yyyy HH:mm:ss",tgl2.toString())

        def nMforeman, tglFi


        def selisih =bln2-bln1
        int fiT = 0, fiA = 0, ofI = 0, sfO = 0, sR = 0, fiR = 0, fiTtot = 0, fiAtot = 0, ofItot = 0, sfOtot = 0, sRtot = 0, fiRtot = 0

//        (0..selisih).each {
//            date1+=1
        def listFiDaily = KrRFiDaily.createCriteria().list {
            ge("tglFi", strDate1)
            lt("tglFi",  strEndDate2)
            eq("companyDealer", CompanyDealer.get(params.workshop as long))
            groupProperty("tglFi")
        }

        if(listFiDaily.size()>0){
            fiTtot = 0
            fiAtot = 0
            ofItot = 0
            sfOtot = 0
            sRtot = 0
            fiRtot = 0
            listFiDaily.each {


                tglFi = it?.tglFi != null ? it?.tglFi?.format("MMM") : "-"
                nMforeman = it?.namaForeman?.t001Inisial != null ? it?.namaForeman?.t001Inisial : "-"
                fiT = it?.fiTarget != null ? Math.round(it?.fiTarget) :0
                fiA = it?.fiActual != null ? Math.round(it?.fiActual) : 0
                ofI = it?.otherFi != null ? Math.round(it?.otherFi) : 0
                sfO = it?.solvedFo != null ? Math.round(it?.solvedFo) : 0
                sR = it?.solvedRate != null ? Math.round(it?.solvedRate) : 0
                fiR = it?.fiRate != null ? Math.round(it.fiRate) : 0
                fiTtot += fiT
                fiAtot += fiA
                ofItot += ofI
                sfOtot += sfO
                sRtot += sR
                fiRtot += fiR
      
                data = [
                        it.tglFi.format("MMM"),
                        it.namaForeman.t001Inisial,
                        Integer.parseInt(String.valueOf(fiT)),
                        Integer.parseInt(String.valueOf(fiA)),
                        Integer.parseInt(String.valueOf(ofI)),
                        Integer.parseInt(String.valueOf(sfO)),
                        Integer.parseInt(String.valueOf(sR)),
                        Integer.parseInt(String.valueOf(fiR)),
                        Integer.parseInt(String.valueOf(fiTtot)),
                        Integer.parseInt(String.valueOf(fiAtot)),
                        Integer.parseInt(String.valueOf(ofItot)),
                        Integer.parseInt(String.valueOf(sfOtot)),
                        Integer.parseInt(String.valueOf(sRtot)),
                        Integer.parseInt(String.valueOf(fiRtot))
                ];
                dataModel.addRow(data);
            }
        }
        else{
            data = [
                    "-",
                    "-",
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0))
            ];
            dataModel.addRow(data);
        }

//   }
        return dataModel;
    }

 	def finalInspectionYearlyModel(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "TOT_COLUMN_2", "TOT_COLUMN_3", "TOT_COLUMN_4", "TOT_COLUMN_5", "TOT_COLUMN_6", "TOT_COLUMN_7"];
        DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);
        def Object[] data

        String bulanParam =  params.tahun
      String bulanParam2 =   params.tahun2
//
////        String bulanParam =  "2014"
//        String bulanParam2 =   "2016"

        Date tgl = new Date().parse('yyyy',bulanParam2.toString())
        Date tgl2 = new Date().parse('yyyy',bulanParam.toString())
        Date tanggal = tgl-1
        int selisih =tgl2 - tgl
        int fiT = 0, fiA = 0, ofI = 0, sfO = 0, sR = 0, fiR = 0, fiTtot = 0, fiAtot = 0, ofItot = 0, sfOtot = 0, sRtot = 0, fiRtot = 0

//        (0..selisih).each {
            tanggal += 1
            def listFiDaily = KrRFiDaily.createCriteria().list {
                ge("tglFi", tgl)
                lt("tglFi",  tgl2 + 1)
                eq("companyDealer", CompanyDealer.get(params.workshop as long))
                groupProperty("tglFi")
            }

        if(listFiDaily.size()>0){
            fiTtot = 0
            fiAtot = 0
            ofItot = 0
            sfOtot = 0
            sRtot = 0
            fiRtot = 0
            listFiDaily.each {


                fiT = Math.round(it.fiTarget)
                fiA = Math.round(it.fiActual)
                ofI = Math.round(it.otherFi)
                sfO = Math.round(it.solvedFo)
                sR = Math.round(it.solvedRate)
                fiR = Math.round(it.fiRate)
                fiTtot += fiT
                fiAtot += fiA
                ofItot += ofI
                sfOtot += sfO
                sRtot += sR
                fiRtot += fiR

                data = [
                        it.tglFi.format("yyyy"),
                        it.namaForeman.t001Inisial,
                        Integer.parseInt(String.valueOf(fiT)),
                        Integer.parseInt(String.valueOf(fiA)),
                        Integer.parseInt(String.valueOf(ofI)),
                        Integer.parseInt(String.valueOf(sfO)),
                        Integer.parseInt(String.valueOf(sR)),
                        Integer.parseInt(String.valueOf(fiR)),
                        Integer.parseInt(String.valueOf(fiTtot)),
                        Integer.parseInt(String.valueOf(fiAtot)),
                        Integer.parseInt(String.valueOf(ofItot)),
                        Integer.parseInt(String.valueOf(sfOtot)),
                        Integer.parseInt(String.valueOf(sRtot)),
                        Integer.parseInt(String.valueOf(fiRtot))
                ];
                dataModel.addRow(data);
            }
        }
        else{
            data = [
                    tanggal.format("yyyy"),
                   "-",
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0)),
                    Integer.parseInt(String.valueOf(0))
            ];
            dataModel.addRow(data);
        }





//        }
        return dataModel;

 	}

 	def detailReportModel(def params){
 		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10"];
 		DefaultTableModel dataModel = new DefaultTableModel(columnNames, 0);		
 		def Object[] data

        String bulanParam =  params.tanggal
        String bulanParam2 =   params.tanggal2
        Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
        Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
        Date tanggal = tgl-1
        int selisih = tgl2 - tgl

        // Convert String To longArray
//        if(params.jenisPekerjaan == "" || params.jenisPekerjaan == null){
//            params.jenisPekerjaan = "-1000"
//        }
//        String[] stringArray = params.jenisPekerjaan.split(",");
//        long[] longArray = new long[stringArray.length];
//        for (int i = 0; i < stringArray.length; i++) {
//            String numberAsString = stringArray[i];
//            longArray[i] = Integer.parseInt(numberAsString);
//        }
        // Convert String To longArray

        (0..selisih).each {
            tanggal += 1
            def listDetail = FinalInspection.createCriteria().list {
                ge("t508TglJamMulai", tanggal)
                lt("t508TglJamMulai", tanggal + 1)
                eq("companyDealer", CompanyDealer.get(params.workshop as long))

                reception{
//                    operation{
//                        kategoriJob{
//                            inList("id", longArray)
//                        }
//                    }
                    eq("staDel","0")
                    eq("staSave","0")
                }
                groupProperty("t508TglJamMulai")
            }

            String tek = ""

            if(listDetail.size() > 0){
                listDetail.each {
                    def jobRcp = JobRCP.findByReceptionAndStaDel(it?.reception, "0")
                    def appFir = Appointment.findByReceptionAndStaDel(it?.reception, "0")
                    def teknisi = JPB.findAllByReceptionAndStaDel(it?.reception, "0")
                    teknisi.each {
                        tek = teknisi?.namaManPower?.userProfile?.t001Inisial?.toString()?.substring(1,teknisi?.namaManPower?.userProfile?.t001Inisial?.toString()?.length() - 1)
                    }

                    data = [it?.t508TglJamMulai ? it?.t508TglJamMulai?.format("dd MMM yyyy") : "-",
                            it?.userProfile?.t001Inisial ? it?.userProfile?.t001Inisial : "-",
                            it?.reception?.historyCustomerVehicle?.fullNoPol ? it?.reception?.historyCustomerVehicle?.fullNoPol : "-",
                            it?.reception?.t401NoWO ? it?.reception?.t401NoWO : "-",
                            tek,
                            it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel,
                            jobRcp?.operation?.kategoriJob?.m055KategoriJob ? jobRcp?.operation?.kategoriJob?.m055KategoriJob : "-",
                            appFir?.t301StaFir ? appFir?.t301StaFir : "0",
                            it?.t508StaInspection ? it?.t508StaInspection : "0",
                            it?.userProfile?.t001Inisial ? it?.userProfile?.t001Inisial : "-",
                            "-"
                    ];
                    dataModel.addRow(data);
                }
            }else{
                data = [tanggal.format("dd MMM yyyy"), "-", "-", "-", "-", "-", "-", "-", "-", "-", "-"];
                dataModel.addRow(data);
            }


        }

 		return dataModel;
 	}

 	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

	def datatablesSAList(){
		render productionGRService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render productionGRService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render productionGRService.datatablesJenisPekerjaan(params) as JSON;
	}

}
