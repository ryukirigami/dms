package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_reception_grController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def receptionGRService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {

    }

	def previewData(){
        if(params?.format=="x"){
            params.file = ".xls"
            params.buntut = "application/vnd.ms-excel";
        }else{
            params.file = ".pdf";
            params.buntut = "application/pdf";
        }

		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01") {
			reportReceptionLeadTimeDaily(params);
		} else 
		if(params.namaReport == "02") {
			reportReceptionLeadTimeMonthly(params);
		} else
		if(params.namaReport == "03") {
			reportReceptionLeadTimeYearly(params);
		} else 
		if(params.namaReport == "04") {
			reportReceptionCustomerIn(params);
		} else
		if(params.namaReport == "05") {
			reportReceptionPotentialLostSalesDaily(params);
		} else
		if(params.namaReport == "06") {
			reportReceptionPotentialLostSalesMonthly(params);
		}
	}

	def reportReceptionLeadTimeDaily(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = receptionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : "") );
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("customerIn", "-");
		parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : receptionGRService.getServiceAdvisorById(params.sa)));
		parameters.put("appointmentStatus", (params.appointmentStatus == "" ? "ALL" : params.appointmentStatus));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : receptionGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("report", "Daily");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelAppointmentStatusDaily(params));
		parameters.put("appointmentStatusDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelJenisPekerjaanStatusDaily(params));
		parameters.put("jenisPekerjaanDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelServiceAdvisorStatusDaily(params));
		parameters.put("serviceAdvisorDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelTotalStatusDaily(params));
		parameters.put("totalDataSource", dataSource);				
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDetailDaily(params));
			parameters.put("detailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Reception_Lead_Time_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Reception_Lead_Time_Daily", params.file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", params.buntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}

	def reportReceptionLeadTimeMonthly(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = receptionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("customerIn", "-");
		parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : receptionGRService.getServiceAdvisorById(params.sa)));
		parameters.put("appointmentStatus", (params.appointmentStatus == "" ? "ALL" : params.appointmentStatus));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : receptionGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("report", "Monthly");
		JRDataSource dataSource = new JRTableModelDataSource(dataModelAppointmentStatusMonthly(params));
		parameters.put("appointmentStatusDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelJenisPekerjaanStatusMonthly(params));
		parameters.put("jenisPekerjaanDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelServiceAdvisorStatusMonthly(params));
		parameters.put("serviceAdvisorDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelTotalStatusMonthly(params));
		parameters.put("totalDataSource", dataSource);		

		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDetailMonthly(params));
			parameters.put("detailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Reception_Lead_Time_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Reception_Lead_Time_Monthly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}

	def reportReceptionLeadTimeYearly(def params){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = receptionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("customerIn", "-");
		parameters.put("serviceAdvisor", (params.sa == "" ? "ALL" : receptionGRService.getServiceAdvisorById(params.sa)));
		parameters.put("appointmentStatus", (params.appointmentStatus == "" ? "ALL" : params.appointmentStatus));
		parameters.put("jenisPekerjaan", (params.jenisPekerjaan == "" ? "ALL" : receptionGRService.getJenisPekerjaanById(params.jenisPekerjaan)));
		parameters.put("report", "Yearly");
		JRDataSource dataSource = new JRTableModelDataSource(dataModelAppointmentStatusYearly(params));
		parameters.put("appointmentStatusDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelJenisPekerjaanStatusYearly(params));
		parameters.put("jenisPekerjaanDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelServiceAdvisorStatusYearly(params));
		parameters.put("serviceAdvisorDataSource", dataSource);
		dataSource = new JRTableModelDataSource(dataModelTotalStatusYearly(params));
		parameters.put("totalDataSource", dataSource);

		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelDetailYearly(params));
			parameters.put("detailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Reception_Lead_Time_Yearly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Reception_Lead_Time_Yearly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}

	def reportReceptionCustomerIn(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
	
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def result = receptionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("arrivalTime", params.arrivalStart + " - " + params.arrivalTo);
		parameters.put("jenisAppointment", (params.appointmentStatus == "" ? "ALL" : params.appointmentStatus));
		parameters.put("delayTime", params.delayStart + " - " + params.delayTo);
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelCustomerInSummary(params));
		parameters.put("summaryDataSource", dataSource);		

		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelCustomerInDetail(params));
			parameters.put("detailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Reception_Customer_In.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Reception_Customer_In_", params.file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", params.buntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }

	}

	def reportReceptionPotentialLostSalesDaily(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = receptionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Daily");
		JRDataSource dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesSummaryDaily(params));
		parameters.put("summaryDataSource", dataSource);		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesDetailDaily(params));
			parameters.put("detailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Reception_Potential_Lost_Sales_Daily.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Reception_Potential_Lost_Sales_Daily", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}

	def reportReceptionPotentialLostSalesMonthly(def params){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = receptionGRService.getWorkshopByCode(params.workshop);
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kategoriWorkshop", "General Repair");
		parameters.put("report", "Monthly");
		JRDataSource dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesSummaryMonthly(params));
		parameters.put("summaryDataSource", dataSource);		

		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesDetailMonthly(params));
			parameters.put("detailDataSource", dataSource);
		}
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "GR_Reception_Potential_Lost_Sales_Monthly.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("GR_Reception_Potential_Lost_Sales_Monthly", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}

	def dataModelAppointmentStatusDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeAppointmentStatus(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def dataModelJenisPekerjaanStatusDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeJenisPekerjaan(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def dataModelServiceAdvisorStatusDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeServiceAdvisor(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def dataModelTotalStatusDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeTotal(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}

	def dataModelDetailDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14", "column_15", "column_16"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeDetail(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),
					String.valueOf(result.get("column_10") != null ? result.get("column_10") : ""),
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : ""),
					String.valueOf(result.get("column_12") != null ? result.get("column_12") : ""),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
	}


	def dataModelAppointmentStatusMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeAppointmentStatus(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;		
	}

	def dataModelJenisPekerjaanStatusMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeJenisPekerjaan(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;		
	}

	def dataModelServiceAdvisorStatusMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeServiceAdvisor(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;		
	}

	def dataModelTotalStatusMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeTotal(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;	
	}

	def dataModelDetailMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14", "column_15", "column_16"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeDetail(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),
					String.valueOf(result.get("column_10") != null ? result.get("column_10") : ""),
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : ""),
					String.valueOf(result.get("column_12") != null ? result.get("column_12") : ""),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
	}

	def dataModelAppointmentStatusYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeAppointmentStatus(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;		
	}

	def dataModelJenisPekerjaanStatusYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeJenisPekerjaan(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def dataModelServiceAdvisorStatusYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeServiceAdvisor(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def dataModelTotalStatusYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeTotal(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
	}

	def dataModelDetailYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12", "column_13", "column_14", "column_15", "column_16"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionLeadTimeDetail(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),
					String.valueOf(result.get("column_10") != null ? result.get("column_10") : ""),
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : ""),
					String.valueOf(result.get("column_12") != null ? result.get("column_12") : ""),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16")))]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
	}

	def dataModelCustomerInSummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionCustomerInSummary(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					Integer.parseInt(String.valueOf(result.get("column_1"))),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_7")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_9")))).toString())
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;		
	}

	def dataModelCustomerInDetail(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionCustomerInDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
                    String.valueOf(result.get("column_4") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : "")
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;	
	}

	def dataModelPotentialLostSalesSummaryDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionPotentialLostSalesSummary(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0")),
					Integer.parseInt(String.valueOf(result.get("column_1"))),					
					Integer.parseInt(String.valueOf(result.get("column_2"))),					
					Integer.parseInt(String.valueOf(result.get("column_3"))),					
					Integer.parseInt(String.valueOf(result.get("column_4"))),					
					Integer.parseInt(String.valueOf(result.get("column_5"))),					
					Integer.parseInt(String.valueOf(result.get("column_6"))),					
					Integer.parseInt(String.valueOf(result.get("column_7")))					
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;		
	}

	def dataModelPotentialLostSalesDetailDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionPotentialLostSalesDetail(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : "")
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;			
	}

	def dataModelPotentialLostSalesSummaryMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionPotentialLostSalesSummary(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					Integer.parseInt(String.valueOf(result.get("column_1"))),					
					Integer.parseInt(String.valueOf(result.get("column_2"))),					
					Integer.parseInt(String.valueOf(result.get("column_3"))),					
					Integer.parseInt(String.valueOf(result.get("column_4"))),					
					Integer.parseInt(String.valueOf(result.get("column_5"))),					
					Integer.parseInt(String.valueOf(result.get("column_6"))),					
					Integer.parseInt(String.valueOf(result.get("column_7")))					
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;			
	}

	def dataModelPotentialLostSalesDetailMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
            params.companyDealerId = session.userCompanyDealerId
			def results = receptionGRService.datatablesGrReceptionPotentialLostSalesDetail(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : "")
					]
				model.addRow(object)
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;			
	}

	def datatablesSAList(){
		//session.exportParams=params;
        params.companyDealerId = session.userCompanyDealerId
		render receptionGRService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render receptionGRService.datatablesJenisPayment(params) as JSON;		
	}

	def dataTablesJenisPekerjaan(){
		render receptionGRService.datatablesJenisPekerjaan(params) as JSON;			
	}

	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

}