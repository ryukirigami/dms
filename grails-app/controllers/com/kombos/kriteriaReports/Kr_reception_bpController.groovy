package com.kombos.kriteriaReports

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class Kr_reception_bpController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT);
	def datatablesUtilService;
	def receptionBPService;
	def jasperService;
	def rootPath;
	def DefaultTableModel tableModel;
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"];
	static viewPermissions = ['index', 'list', 'datatablesList'];

	def index() {

    }

	def previewData(){
        def formatFile = "";
        def formatBuntut = "";
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }
		rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "01") {
			reportReceptionLeadTimeDaily(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "02") {
			reportReceptionLeadTimeMonthly(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "03") {
			reportReceptionLeadTimeYearly(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "04") {
			reportUnitEntryCompositionOnJobType(params);
		} else
		if(params.namaReport == "05") {
			reportUnitEntryCompositionOnPaymentMethod(params);
		} else
		if(params.namaReport == "06") {
			reportUnitEntryCompositionOnDamagedComposition(params);
		} else
		if(params.namaReport == "07") {
			reportUnitEntryCompositionOnSa(params);
		} else
		if(params.namaReport == "08") {
			reportInsuranceApprovalLeadTimeDaily(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "09") {
			reportInsuranceApprovalLeadTimeMonthly(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "10") {
			reportInsuranceApprovalLeadTimeYearly(params,formatFile,formatBuntut);
		} else 
		if(params.namaReport == "11") {
			reportPotentialLostSalesDaily(params, formatFile, formatBuntut);
		} else
		if(params.namaReport == "12") {
            reportPotentialLostSalesMonthly(params, formatFile, formatBuntut);
		} else
		if(params.namaReport == "13") {
			reportAdditionalSPK(params, formatFile, formatBuntut);
		}
	}
	
	def reportReceptionLeadTimeDaily(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kategoriWorkshop", "Body & Paint");
		parameters.put("serviceAdvisorParam", "All");/*(params.sa == "" ? "ALL" : receptionBPService.getServiceAdvisorById(params.sa)));*/
		parameters.put("jenisPaymentParam", "All");
		parameters.put("jenisPekerjaanParam", "All");/*(params.jenisPekerjaan == "" ? "ALL" : receptionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));*/
		parameters.put("report", "Daily");
		JRDataSource dataSource = new JRTableModelDataSource(datamodelLeadTimeJenisPekerjaanDaily(params));
		parameters.put("jenisPekerjaan", dataSource);		
		dataSource = new JRTableModelDataSource(datamodelLeadTimeJenisPaymentDaily(params));
		parameters.put("jenisPayment", dataSource);
		dataSource = new JRTableModelDataSource(datamodelLeadTimeServiceAdvisorDaily(params));
		parameters.put("serviceAdvisor", dataSource);
		dataSource = new JRTableModelDataSource(datamodelLeadTimeTotalDaily(params));
		parameters.put("total", dataSource);
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(datamodelLeadTimeDetail(params));
			parameters.put("detail", dataSource);
		}
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Reception_Lead_Time_Summary.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Reception_Lead_Time_Summary", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }

	}
	
	def reportReceptionLeadTimeMonthly(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kategoriWorkshop", "Body & Paint");
		parameters.put("serviceAdvisorParam", "All");/*(params.sa == "" ? "ALL" : receptionBPService.getServiceAdvisorById(params.sa)));*/
		parameters.put("jenisPaymentParam", "All");
		parameters.put("jenisPekerjaanParam", "All");/*(params.jenisPekerjaan == "" ? "ALL" : receptionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));*/
		parameters.put("report", "Monthly");
		
		JRDataSource dataSource = new JRTableModelDataSource(datamodelLeadTimeJenisPekerjaanMonthly(params));
		parameters.put("jenisPekerjaan", dataSource);		
		
		dataSource = new JRTableModelDataSource(datamodelLeadTimeJenisPaymentMonthly(params));
		parameters.put("jenisPayment", dataSource);		
		dataSource = new JRTableModelDataSource(datamodelLeadTimeServiceAdvisorMonthly(params));
		parameters.put("serviceAdvisor", dataSource);		
		dataSource = new JRTableModelDataSource(datamodelLeadTimeTotalMonthly(params));
		parameters.put("totalSummary", dataSource);
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(datamodelLeadTimeDetail(params));
			parameters.put("detail", dataSource);
		}		
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Reception_Lead_Time_Monthly_Summary.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Reception_Lead_Time_Monthly_Summary", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }

	}
	
	def reportReceptionLeadTimeYearly(def params, String file, String format){
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);
		parameters.put("kategoriWorkshop", "Body & Paint");
		parameters.put("serviceAdvisorParam", "All");/*(params.sa == "" ? "ALL" : receptionBPService.getServiceAdvisorById(params.sa)));*/
		parameters.put("jenisPaymentParam", "All");
		parameters.put("jenisPekerjaanParam", "All");/*(params.jenisPekerjaan == "" ? "ALL" : receptionBPService.getJenisPekerjaanById(params.jenisPekerjaan)));*/
		parameters.put("report", "Yearly");
		JRDataSource dataSource = new JRTableModelDataSource(datamodelLeadTimeJenisPekerjaanYearly(params));
		parameters.put("jenisPekerjaan", dataSource);		
		dataSource = new JRTableModelDataSource(datamodelLeadTimeJenisPaymentYearly(params));
		parameters.put("jenisPayment", dataSource);		
		dataSource = new JRTableModelDataSource(datamodelLeadTimeServiceAdvisorYearly(params));
		parameters.put("serviceAdvisor", dataSource);
		dataSource = new JRTableModelDataSource(datamodelLeadTimeTotalYearly(params));
		parameters.put("totalSummary", dataSource);
		
		if(params.detail == "1"){
			dataSource = new JRTableModelDataSource(datamodelLeadTimeDetail(params));
			parameters.put("detail", dataSource);
		}
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Reception_Lead_Time_Yearly_Summary.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Reception_Lead_Time_Yearly_Summary", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
		
	}
	
	def reportUnitEntryCompositionOnJobType(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		JRDataSource dataSource = new JRTableModelDataSource(datamodelCompositionOnJobTypeGraph(params));
		parameters.put("dataChart", dataSource);		
		dataSource = new JRTableModelDataSource(datamodelCompositionOnJobTypeTable(params));
		parameters.put("dataTable", dataSource);	
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Unit_Entry_Composition_On_Job_Type.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Unit_Entry_Composition_On_Job_Type", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportUnitEntryCompositionOnPaymentMethod(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		JRDataSource dataSource = new JRTableModelDataSource(datamodelCompositionOnPaymentMethodGraph(params));
		parameters.put("dataChart", dataSource);
		dataSource = new JRTableModelDataSource(datamodelCompositionOnPaymentMethodTable(params));
		parameters.put("dataTable", dataSource);	
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Unit_Entry_Composition_On_Payment_Method.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Unit_Entry_Composition_On_Payment_Method", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportUnitEntryCompositionOnDamagedComposition(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		JRDataSource dataSource = new JRTableModelDataSource(datamodelCompositionOnDamagedGraph(params));
		parameters.put("dataChart", dataSource);
		dataSource = new JRTableModelDataSource(datamodelCompositionOnDamagedTable(params));
		parameters.put("dataTable", dataSource);	
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Unit_Entry_Composition_On_Damaged.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Unit_Entry_Composition_On_Damaged", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}
	
	def reportUnitEntryCompositionOnSa(def params){
		//Setting Date Format
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");

		//Parse String into Date Format
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

		//Get Workshop Name
		def result = receptionBPService.getWorkshopByCode(params.workshop);

		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));

		if(params.chart=="1"){
			JRDataSource dataSourceChart = new JRTableModelDataSource(datamodelCompositionOnSa(params));
			parameters.put("dataChart", dataSourceChart);
		}

		JRDataSource dataSourceTable = new JRTableModelDataSource(datamodelCompositionOnSaTable(params));
		parameters.put("dataTable", dataSourceTable);

		//Get Jasper Template
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Unit_Entry_Composition_On_Service_Advistor2.jasper", parameters,
						new JRTableModelDataSource(tableModel));

		//Create Default File Name and Format
		File file = File.createTempFile("Unit_Entry_Composition_On_Service_Advisor", params.format=="x"?".xls":".pdf");
		file.deleteOnExit();

		if(params.format=="x"){
			JExcelApiExporter exporterXLS = new JExcelApiExporter();
			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
			exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
			response.setHeader("Content-Type", "vnd.ms-excel");
			response.setHeader("Content-disposition", "attachment;filename=${file.name}");
			exporterXLS.exportReport();
		}else{
			JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(file));
			response.setHeader("Content-Type", "application/pdf")
			response.setHeader("Content-disposition", "attachment;filename=${file.name}");
			response.outputStream << file.newInputStream();
		}
	}
	
	def reportInsuranceApprovalLeadTimeDaily(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("insurance", "All");
		parameters.put("jobDispatchStatus", "All");
		parameters.put("jenisSPK", "All");
		parameters.put("report", "Daily");
		JRDataSource dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalJenisSPKDaily(params));
		parameters.put("sub_insurance", dataSource);
		dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalSubInsuranceDaily(params));
		parameters.put("sub_jenisspk", dataSource);
		dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalTotal(params));
		parameters.put("sub_total", dataSource);
        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalDetail(params));
			parameters.put("detail", dataSource);
        }

		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Insurance_Approval_Lead_Time_Summary.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Insurance_Approval_Lead_Time_Summary", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportInsuranceApprovalLeadTimeMonthly(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("M yyyy");
		DateFormat df1 = new SimpleDateFormat("MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def bulan1 = params.bulan1 + " " + params.tahun
		def bulan2 = params.bulan2 + " " + params.tahun
		def startDate = df.parse(bulan1);
		def endDate = df.parse(bulan2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("insurance", "All");
		parameters.put("jobDispatchStatus", "All");
		parameters.put("jenisSPK", "All");
		parameters.put("report", "Daily");
		JRDataSource dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalJenisSPKMonthly(params));
		parameters.put("sub_insurance", dataSource);
		dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalSubInsuranceMonthly(params));
		parameters.put("sub_jenisspk", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalMonthlyTotal(params));
		parameters.put("sub_totalsummary", dataSource);
        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalDetail(params));
            parameters.put("detail", dataSource);
        }

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Insurance_Approval_Lead_Time_Monthly_Summary.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Insurance_Approval_Lead_Time_Monthly_Summary", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportInsuranceApprovalLeadTimeYearly(def params, String file, String format){
		
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();		
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", params.tahun);
		parameters.put("insurance", "All");
		parameters.put("jobDispatchStatus", "All");
		parameters.put("jenisSPK", "All");
		parameters.put("report", "Daily");
		JRDataSource dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalJenisSPKYearly(params));
		parameters.put("sub_insurance", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalSubInsuranceYearly(params));
		parameters.put("sub_jenisspk", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalYearlyTotal(params));
		parameters.put("sub_totalsummary", dataSource);
        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(dataModelInsuranceApprovalDetail(params));
            parameters.put("detail", dataSource);
        }
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Insurance_Approval_Lead_Time_Yearly_Summary.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Insurance_Approval_Lead_Time_Yearly_Summary", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportPotentialLostSalesDaily(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = receptionBPService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kategoriWorkshop", "Body & Paint");
		parameters.put("report", "Daily");
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesDailySummary(params));
		parameters.put("sub_summary", dataSource);
        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesDailyDetail(params));
            parameters.put("sub_detail", dataSource);
        }

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Potential_Lost_Sales_Summary.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("BP_Potential_Lost_Sales_Summary_", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportPotentialLostSalesMonthly(def params, String file, String format){
        DateFormat df = new SimpleDateFormat("M yyyy");
        DateFormat df1 = new SimpleDateFormat("MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def bulan1 = params.bulan1 + " " + params.tahun
        def bulan2 = params.bulan2 + " " + params.tahun
        def startDate = df.parse(bulan1);
        def endDate = df.parse(bulan2);

        def result = receptionBPService.getWorkshopByCode(params.workshop);

        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
        parameters.put("kategoriWorkshop", "Body & Paint");
        parameters.put("report", "Monthly");

        JRDataSource dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesMonthlySummary(params));
        parameters.put("sub_summary", dataSource);
        if(params.detail == "1"){
            dataSource = new JRTableModelDataSource(dataModelPotentialLostSalesMonthlyDetail(params));
            parameters.put("sub_detail", dataSource);
        }

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Potential_Lost_Sales_Month_Summary.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("BP_Potential_Lost_Sales_Month_Summary_", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def reportAdditionalSPK(def params, String file, String format){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        def result = receptionBPService.getWorkshopByCode(params.workshop);

        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));

//        JRDataSource dataSource = new JRTableModelDataSource(dataModelAdditionalSPK(params));

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "BP_Additional_SPK.jasper", parameters, new JRTableModelDataSource(dataModelAdditionalSPK(params)));
        File pdf = File.createTempFile("BP_Additional_SPK_", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}
	
	def datamodelLeadTimeJenisPekerjaanDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeJenisPekerjaan(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : "BODY PAINT (BP)"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_12")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	def datamodelLeadTimeJenisPekerjaanMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22", "column_23"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeJenisPekerjaan(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : "BODY & PAINT (BP)"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_12")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))
				]
				model.addRow(object);						
			}				
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def datamodelLeadTimeJenisPekerjaanYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeJenisPekerjaan(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
//					Integer.parseInt(String.valueOf(result.get("column_11"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_12")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))						
				]
				model.addRow(object);		
			}			
		} catch(Exception e){
			e.printStackTrace();	
		}
		return model;
	}
	
	def datamodelLeadTimeJenisPaymentDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeJenisPayment(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : "CASH"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelLeadTimeJenisPaymentMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22", "column_23"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeJenisPayment(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : "CASH"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def datamodelLeadTimeJenisPaymentYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeJenisPayment(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),					
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
//					Integer.parseInt(String.valueOf(result.get("column_14"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))						
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	def datamodelLeadTimeServiceAdvisorDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeServiceAdvisor(params, 0);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : "ATMOJO"),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))
				]
				model.addRow(object);			
			}				
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def datamodelLeadTimeServiceAdvisorMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22", "column_23"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeServiceAdvisor(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def datamodelLeadTimeServiceAdvisorYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeServiceAdvisor(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),					
					Integer.parseInt(String.valueOf(result.get("column_3"))),
					Integer.parseInt(String.valueOf(result.get("column_4"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_5")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
					Integer.parseInt(String.valueOf(result.get("column_7"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
					Integer.parseInt(String.valueOf(result.get("column_10"))),
//					Integer.parseInt(String.valueOf(result.get("column_11"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_11")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
					Integer.parseInt(String.valueOf(result.get("column_13"))),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_14")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
					Integer.parseInt(String.valueOf(result.get("column_23")))					
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	def datamodelLeadTimeTotalDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeTotal(params, 0);
			int max_wfp = 0, min_wfp = 0, avg_wfp = 0, max_rp = 0, min_rp = 0, avg_rp = 0, max_wfe = 0, min_wfe = 0, avg_wfe = 0, max_estimasi = 0, min_estimasi = 0, avg_estimasi = 0, max_wfi = 0, min_wfi = 0, avg_wfi = 0,  
				max_ip = 0, min_ip = 0, avg_ip = 0, max_wfj = 0, min_wfj = 0, avg_wfj = 0;
			for(Map<String, Object> result : results) {
				max_wfp += Integer.parseInt(String.valueOf(result.get("column_2")));
				min_wfp += Integer.parseInt(String.valueOf(result.get("column_3")));
				avg_wfp += Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_4")))).toString());
				max_rp += Integer.parseInt(String.valueOf(result.get("column_5")));
				min_rp += Integer.parseInt(String.valueOf(result.get("column_6")));
				avg_rp += Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_7")))).toString());
				max_wfe += Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_8")))).toString());
				min_wfe += Integer.parseInt(String.valueOf(result.get("column_9")));
				avg_wfe += Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_10")))).toString());
				max_estimasi += Integer.parseInt(String.valueOf(result.get("column_11")));
				min_estimasi += Integer.parseInt(String.valueOf(result.get("column_12")));
				avg_estimasi += Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_13")))).toString());
				max_wfi += Integer.parseInt(String.valueOf(result.get("column_14")));
				min_wfi += Integer.parseInt(String.valueOf(result.get("column_15")));
				avg_wfi += Integer.parseInt(String.valueOf(result.get("column_16")));
				max_ip += Integer.parseInt(String.valueOf(result.get("column_17")));
				min_ip += Integer.parseInt(String.valueOf(result.get("column_18")));
				avg_ip += Integer.parseInt(String.valueOf(result.get("column_19")));
				max_wfj += Integer.parseInt(String.valueOf(result.get("column_20")));
				min_wfj += Integer.parseInt(String.valueOf(result.get("column_21")));
				avg_wfj += Integer.parseInt(String.valueOf(result.get("column_22")));		
			}
			def Object[] object = [
					max_wfp,				
					min_wfp,
					avg_wfp,
					max_rp,				
					min_rp,
					avg_rp,
					max_wfe,				
					min_wfe,
					avg_wfe,
					max_estimasi,				
					min_estimasi,
					avg_estimasi,
					max_wfi,				
					min_wfi,
					avg_wfi,
					max_ip,				
					min_ip,
					avg_ip,
					max_wfj,				
					min_wfj,
					avg_wfj
				]
				model.addRow(object);			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}

	def datamodelLeadTimeTotalMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeTotal(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_4")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_7")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_10")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_13")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22")))					
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def datamodelLeadTimeTotalYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
//			def results = receptionBPService.datatablesJenisPekerjaan(params, 2);
			def results = receptionBPService.datatablesLeadTimeTotal(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : "BODY & PAINT (BP)"),
					Integer.parseInt(String.valueOf(result.get("column_2"))),
					Integer.parseInt(String.valueOf(result.get("column_3"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_4")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_5"))),
					Integer.parseInt(String.valueOf(result.get("column_6"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_7")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_8"))),
					Integer.parseInt(String.valueOf(result.get("column_9"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_10")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_11"))),
					Integer.parseInt(String.valueOf(result.get("column_12"))),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_13")))).toString()),
					Integer.parseInt(String.valueOf(result.get("column_14"))),
					Integer.parseInt(String.valueOf(result.get("column_15"))),
					Integer.parseInt(String.valueOf(result.get("column_16"))),
					Integer.parseInt(String.valueOf(result.get("column_17"))),
					Integer.parseInt(String.valueOf(result.get("column_18"))),
					Integer.parseInt(String.valueOf(result.get("column_19"))),
					Integer.parseInt(String.valueOf(result.get("column_20"))),
					Integer.parseInt(String.valueOf(result.get("column_21"))),
					Integer.parseInt(String.valueOf(result.get("column_22"))),
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();			
		}
		return model;
	}
	
	def datamodelLeadTimeDetail(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21", "column_22", "column_23",
					"column_24", "column_25"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesLeadTimeDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [					
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),					
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),					
					String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),
					String.valueOf(null != result.get("column_4") ? result.get("column_4") : ""),
					String.valueOf(null != result.get("column_5") ? result.get("column_5") : ""),
					String.valueOf(null != result.get("column_6") ? result.get("column_6") : ""),					
					String.valueOf(null != result.get("column_7") ? result.get("column_7") : ""),					
					String.valueOf(null != result.get("column_8") ? result.get("column_8") : ""),					
					String.valueOf(null != result.get("column_9") ? result.get("column_9") : ""),					
					String.valueOf(null != result.get("column_10") ? result.get("column_10") : ""),					
					String.valueOf(null != result.get("column_11") ? result.get("column_11") : ""),					
					String.valueOf(null != result.get("column_12") ? result.get("column_12") : ""),					
					String.valueOf(null != result.get("column_13") ? result.get("column_13") : ""),					
					String.valueOf(null != result.get("column_14") ? result.get("column_14") : ""),					
					String.valueOf(null != result.get("column_15") ? result.get("column_15") : ""),					
					String.valueOf(null != result.get("column_16") ? result.get("column_16") : ""),					
					String.valueOf(null != result.get("column_17") ? result.get("column_17") : ""),					
					String.valueOf(null != result.get("column_18") ? result.get("column_18") : ""),
					Integer.parseInt(null != result.get("column_19") ? String.valueOf(result.get("column_19")) : "0"),					
					Integer.parseInt(null != result.get("column_20") ? String.valueOf(result.get("column_20")) : "0"),					
					Integer.parseInt(null != result.get("column_21") ? String.valueOf(result.get("column_21")) : "0"),					
					Integer.parseInt(null != result.get("column_22") ? String.valueOf(result.get("column_22")) : "0"),					
					Integer.parseInt(null != result.get("column_23") ? String.valueOf(result.get("column_23")) : "0"),					
					Integer.parseInt(null != result.get("column_24") ? String.valueOf(result.get("column_24")) : "0"),					
					Integer.parseInt(null != result.get("column_25") ? String.valueOf(result.get("column_25")) : "0")					
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelCompositionOnJobTypeGraph(def params){
		def String[] columnNames = ["column_0", "column_1"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesCompositionOnJobType(params);
			def totalUnit = 0;
			def body = 0;
			def bp = 0;
			def twc = 0;
			def prt = 0;
			def rtj = 0;
			for(Map<String, Object> result : results) {
//				totalUnit += Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0" );
				body += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0" );
				bp += Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0" );
				twc += Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0" );
				prt += Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0" );
				rtj += Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0" );
			}
            totalUnit = body + bp + twc + prt + rtj
			
			def persenBody = java.lang.Math.ceil((body/totalUnit) * 100).intValue();
			def persenBp = java.lang.Math.ceil((bp/totalUnit) * 100).intValue();
			def persenTwc = java.lang.Math.ceil((twc/totalUnit) * 100).intValue();
			def persenPrt = java.lang.Math.ceil((prt/totalUnit) * 100).intValue();
			def persenRtj = java.lang.Math.ceil((rtj/totalUnit) * 100).intValue();
				
			def Object[] object = [					
				"Body", persenBody
			]
			model.addRow(object);			
			object = [					
				"BP", persenBp
			]
			model.addRow(object);
			object = [					
				"TWC", persenTwc
			]
			model.addRow(object);
			object = [					
				"PRT", persenPrt
			]
			model.addRow(object);
			object = [					
				"RTJ", persenRtj
			]
			model.addRow(object);						
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelCompositionOnJobTypeTable(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10","column_11"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesCompositionOnJobType(params);
			
			for(Map<String, Object> result : results) {
//				def totalUnit = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				def body = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				def bp = Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0");
				def twc = Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0");
				def prt = Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0");
				def rtj = Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0");
				def totalUnit = body + bp + twc + prt + rtj
				def persenBody = java.lang.Math.ceil((body/totalUnit) * 100).intValue();
				def persenBp = java.lang.Math.ceil((bp/totalUnit) * 100).intValue();
				def persenTwc = java.lang.Math.ceil((twc/totalUnit) * 100).intValue();
				def persenPrt = java.lang.Math.ceil((prt/totalUnit) * 100).intValue();
				def persenRtj = java.lang.Math.ceil((rtj/totalUnit) * 100).intValue();
				
				def Object[] object = [						
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : " "),
                    body,
					persenBody,
					bp,
					persenBp,
					twc,
					persenTwc,
					prt,
					persenPrt,
					rtj,
					persenRtj,
                    totalUnit
				]
				model.addRow(object);						
				
			}			
			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelCompositionOnPaymentMethodGraph(def params){
		def String[] columnNames = ["column_0", "column_1"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesCompositionOnPaymentMethod(params);
			def totalUnit = 0;
			def cash = 0;
			def insurance = 0;
			def twc = 0;
			def prt = 0;
			def rtj = 0;
			def project = 0;
			
			for(Map<String, Object> result : results) {
//				totalUnit += Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0" );
				cash += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0" );
				insurance += Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0" );
				twc += Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0" );
				prt += Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0" );
				rtj += Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0" );
				project += Integer.parseInt(null != result.get("column_7") ? String.valueOf(result.get("column_7")) : "0" );
			}

            totalUnit = cash + insurance + twc + prt + rtj + project
			def persenCash = java.lang.Math.ceil((cash/totalUnit) * 100).intValue();
			def persenInsurance = java.lang.Math.ceil((insurance/totalUnit) * 100).intValue();
			def persenTwc = java.lang.Math.ceil((twc/totalUnit) * 100).intValue();
			def persenPrt = java.lang.Math.ceil((prt/totalUnit) * 100).intValue();
			def persenRtj = java.lang.Math.ceil((rtj/totalUnit) * 100).intValue();
			def persenProject = java.lang.Math.ceil((project/totalUnit) * 100).intValue();
				
			def Object[] object = [					
				"Cash", persenCash
			]
			model.addRow(object);			
			object = [					
				"Insurance", persenInsurance
			]
			model.addRow(object);
			object = [					
				"TWC", persenTwc
			]
			model.addRow(object);
			object = [					
				"PRT", persenPrt
			]
			model.addRow(object);
			object = [					
				"RTJ", persenRtj
			]
			model.addRow(object);
			object = [					
				"Project", persenProject
			]
			model.addRow(object);								
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelCompositionOnPaymentMethodTable(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10", "column_11", "column_12"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesCompositionOnPaymentMethod(params);
			
			for(Map<String, Object> result : results) {
//				def totalUnit = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				def cash = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				def insurance = Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0");
				def twc = Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0");
				def prt = Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0");
				def rtj = Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0");
				def project = Integer.parseInt(null != result.get("column_7") ? String.valueOf(result.get("column_7")) : "0");

                def totalUnit = cash + insurance + twc + prt + rtj + project
                def persenCash = java.lang.Math.ceil((cash/totalUnit) * 100).intValue();
				def persenInsurance = java.lang.Math.ceil((insurance/totalUnit) * 100).intValue();
				def persenTwc = java.lang.Math.ceil((twc/totalUnit) * 100).intValue();
				def persenPrt = java.lang.Math.ceil((prt/totalUnit) * 100).intValue();
				def persenRtj = java.lang.Math.ceil((rtj/totalUnit) * 100).intValue();
				def persenProject = java.lang.Math.ceil((project/totalUnit) * 100).intValue();
				
				def Object[] object = [						
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					cash,
					persenCash,
					insurance,
					persenInsurance,
					twc,
					persenTwc,
					prt,
					persenPrt,
					rtj,
					persenRtj,
					project,
					persenProject
				]
				model.addRow(object);						
				
			}			
			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelCompositionOnDamagedGraph(def params){
		def String[] columnNames = ["column_0", "column_1"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesCompositionOnDamaged(params);
			def totalUnit = 0;
			def tpsline = 0;
			def light = 0;
			def medium = 0;
			def heavy = 0;
			def minor = 0;
			
			for(Map<String, Object> result : results) {
//				totalUnit += Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0" );
				tpsline += Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0" );
				light += Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0" );
				medium += Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0" );
				heavy += Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0" );
				minor += Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0" );				
			}
            totalUnit = tpsline + light + medium + heavy + minor
            def persenTpsline=0,persenLight = 0,persenMedium=0, persenHeavy = 0, persenMinor = 0
            if(totalUnit>0){
			 persenTpsline = java.lang.Math.ceil((tpsline/totalUnit) * 100).intValue();
			 persenLight = java.lang.Math.ceil((light/totalUnit) * 100).intValue();
			 persenMedium = java.lang.Math.ceil((medium/totalUnit) * 100).intValue();
			 persenHeavy = java.lang.Math.ceil((heavy/totalUnit) * 100).intValue();
			 persenMinor = java.lang.Math.ceil((minor/totalUnit) * 100).intValue();
            }

			def Object[] object = [					
				"TPS Line", persenTpsline
			]
			model.addRow(object);			
			object = [					
				"Light", persenLight
			]
			model.addRow(object);
			object = [					
				"Medium", persenMedium
			]
			model.addRow(object);
			object = [					
				"Heavy", persenHeavy
			]
			model.addRow(object);
			object = [					
				"Minor", persenMinor
			]
			model.addRow(object);										
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelCompositionOnDamagedTable(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesCompositionOnDamaged(params);
			
			for(Map<String, Object> result : results) {
//				def totalUnit = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");
				def tpsline = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				def light = Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0");
				def medium = Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0");
				def heavy = Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0");
				def minor = Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0");

                def totalUnit = tpsline + light + medium + heavy + minor
                def persenTpsline=0,persenLight = 0,persenMedium=0, persenHeavy = 0, persenMinor = 0
                if(totalUnit>0){
                    persenTpsline = java.lang.Math.ceil((tpsline/totalUnit) * 100).intValue();
                    persenLight = java.lang.Math.ceil((light/totalUnit) * 100).intValue();
                    persenMedium = java.lang.Math.ceil((medium/totalUnit) * 100).intValue();
                    persenHeavy = java.lang.Math.ceil((heavy/totalUnit) * 100).intValue();
                    persenMinor = java.lang.Math.ceil((minor/totalUnit) * 100).intValue();
                }

				def Object[] object = [						
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					tpsline,
					persenTpsline,
					light,
					persenLight,
					medium,
					persenMedium,
					heavy,
					persenHeavy,
					minor,
					persenMinor
				]
				model.addRow(object);						
				
			}			
			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def datamodelCompositionOnSa(def params){
		def String[] columnNames = ["column_0", "column_1"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);

		try {
			def results = receptionBPService.datatablesCompositionOnSaChart(params);

			for(Map<String, Object> result : results) {
				def unit = Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0");

				def Object[] object = [
						String.valueOf(null != result.get("column_0") ? result.get("column_0") : "-"),
						unit
				]
				model.addRow(object);
			}
		} catch(Exception e){
			e.printStackTrace();
		}

		return model;
	}	
	
	def datamodelCompositionOnSaTable(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);

		try {
			def results = receptionBPService.datatablesCompositionOnSaTable(params);

			for(Map<String, Object> result : results) {
				def unit = Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0");
				
//				def persenUser1 = java.lang.Math.ceil((user1/totalUnit) * 100).intValue();
//				def persenUser2 = java.lang.Math.ceil((user2/totalUnit) * 100).intValue();

				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : "-"),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : "-"),
					unit
				]
				model.addRow(object);				
			}
		} catch(Exception e){
			e.printStackTrace();		
		}

		return model;
	}
	
	def dataModelInsuranceApprovalJenisSPKDaily(def params) {
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesJenisSPK(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					String.valueOf(result.get("column_8")),
					String.valueOf(result.get("column_9")),
					String.valueOf(result.get("column_10")),
					String.valueOf(result.get("column_11")),
					String.valueOf(result.get("column_12")),
					String.valueOf(result.get("column_13")),
					String.valueOf(result.get("column_14")),
					String.valueOf(result.get("column_15")),
					String.valueOf(result.get("column_16")),
					String.valueOf(result.get("column_17"))
				]
				model.addRow(object);			
			}

		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalJenisSPKMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15","column_16", "column_17"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesJenisSPK(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					String.valueOf(result.get("column_8")),
					String.valueOf(result.get("column_9")),
					String.valueOf(result.get("column_10")),
					String.valueOf(result.get("column_11")),
					String.valueOf(result.get("column_12")),
					String.valueOf(result.get("column_13")),
					String.valueOf(result.get("column_14")),
					String.valueOf(result.get("column_15")),
					String.valueOf(result.get("column_16")),
					String.valueOf(result.get("column_17"))
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalJenisSPKYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15","column_16"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesJenisSPK(params, 3);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					String.valueOf(result.get("column_8")),
					String.valueOf(result.get("column_9")),
					String.valueOf(result.get("column_10")),
					String.valueOf(result.get("column_11")),
					String.valueOf(result.get("column_12")),
					String.valueOf(result.get("column_13")),
					String.valueOf(result.get("column_14")),
					String.valueOf(result.get("column_15")),
					String.valueOf(result.get("column_16")),
					String.valueOf(result.get("column_17"))
				]
				model.addRow(object);
            }

		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalSubInsuranceDaily(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesInsurance(params, 1);
                for(Map<String, Object> result : results) {
                    def Object[] object = [
                        String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
                        String.valueOf(result.get("column_3")),
                        String.valueOf(result.get("column_4")),
                        String.valueOf(result.get("column_5")),
                        String.valueOf(result.get("column_6")),
                        String.valueOf(result.get("column_7")),
                        String.valueOf(result.get("column_8")),
                        String.valueOf(result.get("column_9")),
                        String.valueOf(result.get("column_10")),
                        String.valueOf(result.get("column_11")),
                        String.valueOf(result.get("column_12")),
                        String.valueOf(result.get("column_13")),
                        String.valueOf(result.get("column_14")),
                        String.valueOf(result.get("column_15")),
                        String.valueOf(result.get("column_16")),
                        String.valueOf(result.get("column_17"))
                    ]
                    model.addRow(object);
                }

		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalSubInsuranceMonthly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesInsurance(params, 2);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					String.valueOf(result.get("column_8")),
					String.valueOf(result.get("column_9")),
					String.valueOf(result.get("column_10")),
					String.valueOf(result.get("column_11")),
					String.valueOf(result.get("column_12")),
					String.valueOf(result.get("column_13")),
					String.valueOf(result.get("column_14")),
					String.valueOf(result.get("column_15")),
					String.valueOf(result.get("column_16")),
					String.valueOf(result.get("column_17"))
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalSubInsuranceYearly(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesInsurance(params, 3);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3")),
					String.valueOf(result.get("column_4")),
					String.valueOf(result.get("column_5")),
					String.valueOf(result.get("column_6")),
					String.valueOf(result.get("column_7")),
					String.valueOf(result.get("column_8")),
					String.valueOf(result.get("column_9")),
					String.valueOf(result.get("column_10")),
					String.valueOf(result.get("column_11")),
					String.valueOf(result.get("column_12")),
					String.valueOf(result.get("column_13")),
					String.valueOf(result.get("column_14")),
					String.valueOf(result.get("column_15")),
					String.valueOf(result.get("column_16")),
					String.valueOf(result.get("column_17"))
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalTotal(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def resultsJenisSpk = receptionBPService.datatablesJenisSPK(params, 1);
			def resultsInsurance = receptionBPService.datatablesInsurance(params, 1);
			
			def column_0 = 0;
			def column_1 = 0;
			def column_2 = 0;
			def column_3 = 0;
			def column_4 = 0;
			def column_5 = 0;
			def column_6 = 0;
			def column_7 = 0;
			def column_8 = 0;
			def column_9 = 0;
			def column_10 = 0;
			def column_11 = 0;
			def column_12 = 0;
			def column_13 = 0;
			def column_14 = 0;
			def column_15 = 0;
			
			for(Map<String, Object> result : resultsJenisSpk) {
				column_0 += Integer.parseInt(String.valueOf(result.get("column_3")));
				column_1 += Integer.parseInt(String.valueOf(result.get("column_4")));
				column_2 += Integer.parseInt(String.valueOf(result.get("column_5")));
//				column_3 += String.valueOf(result.get("column_6"));
//				column_4 += String.valueOf(result.get("column_7"));
//				column_5 += String.valueOf(result.get("column_8"));
//				column_6 += String.valueOf(result.get("column_9"));
//				column_7 += String.valueOf(result.get("column_10"));
//				column_8 += String.valueOf(result.get("column_11"));
//				column_9 += String.valueOf(result.get("column_12"));
//				column_10 += String.valueOf(result.get("column_13"));
//				column_11 += String.valueOf(result.get("column_14"));
//				column_12 += String.valueOf(result.get("column_15"));
//				column_13 += String.valueOf(result.get("column_16"));
//				column_14 += String.valueOf(result.get("column_17"));
//				column_15 += String.valueOf(result.get("column_18"));
			}			
			for(Map<String, Object> result : resultsInsurance) {
				column_0 += Integer.parseInt(String.valueOf(result.get("column_3")));
				column_1 += Integer.parseInt(String.valueOf(result.get("column_4")));
				column_2 += Integer.parseInt(String.valueOf(result.get("column_5")));
//				column_3 += String.valueOf(result.get("column_6"));
//				column_4 += String.valueOf(result.get("column_7"));
//				column_5 += String.valueOf(result.get("column_8"));
//				column_6 += String.valueOf(result.get("column_9"));
//				column_7 += String.valueOf(result.get("column_10"));
//				column_8 += String.valueOf(result.get("column_11"));
//				column_9 += String.valueOf(result.get("column_12"));
//				column_10 += String.valueOf(result.get("column_13"));
//				column_11 += String.valueOf(result.get("column_14"));
//				column_12 += String.valueOf(result.get("column_15"));
//				column_13 += String.valueOf(result.get("column_16"));
//				column_14 += String.valueOf(result.get("column_17"));
//				column_15 += String.valueOf(result.get("column_18"));
			}	
			def Object[] object = [
					column_0.toString(),column_1.toString(),column_2.toString(),column_3.toString(),
                    column_4.toString(),column_5.toString(),column_6.toString(),column_7.toString(),
                    column_8.toString(),column_9.toString(),column_10.toString(),
					column_11.toString(),column_12.toString(),column_13.toString(),column_14.toString(),column_15.toString()
				]
				model.addRow(object);		
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalMonthlyTotal(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {			
			def column_0 = "";
			def column_1 = "";
			def column_2 = 0;
			def column_3 = 0;
			def column_4 = 0;
			def column_5 = 0;
			def column_6 = 0;
			def column_7 = 0;
			def column_8 = 0;
			def column_9 = 0;
			def column_10 = 0;
			def column_11 = 0;
			def column_12 = 0;
			def column_13 = 0;
			def column_14 = 0;
			def column_15 = 0;			
			def column_16 = 0;			
			
			def Object[] object = [
					column_0,column_1,column_2,column_3,column_4,column_5,column_6,column_7,column_8,column_9,column_10,
					column_11,column_12,column_13,column_14,column_15, column_16
				]
			model.addRow(object);		
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalYearlyTotal(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {			
			def column_0 = "";
			def column_1 = 0;
			def column_2 = 0;
			def column_3 = 0;
			def column_4 = 0;
			def column_5 = 0;
			def column_6 = 0;
			def column_7 = 0;
			def column_8 = 0;
			def column_9 = 0;
			def column_10 = 0;
			def column_11 = 0;
			def column_12 = 0;
			def column_13 = 0;
			def column_14 = 0;
			def column_15 = 0;									
			
			def Object[] object = [
					column_0,column_1,column_2,column_3,column_4,column_5,column_6,column_7,column_8,column_9,column_10,
					column_11,column_12,column_13,column_14,column_15
				]
			model.addRow(object);		
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelInsuranceApprovalDetail(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesInsuranceApprovalDetail(params);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
					String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
					String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
					String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),
					String.valueOf(null != result.get("column_4") ? result.get("column_4") : ""),
					String.valueOf(null != result.get("column_5") ? result.get("column_5") : ""),
					String.valueOf(null != result.get("column_6") ? result.get("column_6") : ""),
					String.valueOf(null != result.get("column_7") ? result.get("column_7") : ""),
					String.valueOf(null != result.get("column_8") ? result.get("column_8") : ""),
					String.valueOf(null != result.get("column_9") ? result.get("column_9") : ""),
					String.valueOf(null != result.get("column_10") ? result.get("column_10") : ""),
					String.valueOf(null != result.get("column_11") ? result.get("column_11") : ""),
					String.valueOf(null != result.get("column_12") ? result.get("column_12") : ""),
					String.valueOf(null != result.get("column_13") ? result.get("column_13") : ""),
					String.valueOf(null != result.get("column_14") ? result.get("column_14") : ""),
					String.valueOf(null != result.get("column_15") ? result.get("column_15") : ""),
					null != result.get("column_16") ? String.valueOf(result.get("column_16")) : "0",
					null != result.get("column_17") ? String.valueOf(result.get("column_17")) : "0",
					null != result.get("column_18") ? String.valueOf(result.get("column_18")) : "0",
					null != result.get("column_19") ? String.valueOf(result.get("column_19")) : "0",
					null != result.get("column_20") ? String.valueOf(result.get("column_20")) : "0"
				]
				model.addRow(object);			
			}			
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelPotentialLostSalesDailySummary(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = receptionBPService.datatablesPotentialLostSalesSummary(params, 1);
			for(Map<String, Object> result : results) {
				def Object[] object = [
					String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),					
					Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0"),
					Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0"),
					Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0"),
					Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0"),
					Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0"),
					Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0"),
                    Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_7")))).toString())
				]
				model.addRow(object);			
			}
		} catch(Exception e){
			e.printStackTrace();		
		}
		return model;
	}
	
	def dataModelPotentialLostSalesDailyDetail(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = receptionBPService.datatablesPotentialLostSalesDetail(params, 1);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
                        String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
                        String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
                        String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),
                        String.valueOf(null != result.get("column_4") ? result.get("column_4") : ""),
                        String.valueOf(null != result.get("column_5") ? result.get("column_5") : ""),
                        String.valueOf(null != result.get("column_6") ? result.get("column_6") : ""),
                        String.valueOf(null != result.get("column_7") ? result.get("column_7") : ""),

                ]
                model.addRow(object);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
	}

    def dataModelPotentialLostSalesMonthlySummary(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = receptionBPService.datatablesPotentialLostSalesSummary(params, 2);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
                        Integer.parseInt(null != result.get("column_1") ? String.valueOf(result.get("column_1")) : "0"),
                        Integer.parseInt(null != result.get("column_2") ? String.valueOf(result.get("column_2")) : "0"),
                        Integer.parseInt(null != result.get("column_3") ? String.valueOf(result.get("column_3")) : "0"),
                        Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0"),
                        Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0"),
                        Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0"),
                        Integer.parseInt(Math.round(Double.parseDouble(String.valueOf(result.get("column_7")))).toString())
                ]
                model.addRow(object);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelPotentialLostSalesMonthlyDetail(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = receptionBPService.datatablesPotentialLostSalesDetail(params, 2);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
                        String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
                        String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
                        String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),
                        String.valueOf(null != result.get("column_4") ? result.get("column_4") : ""),
                        String.valueOf(null != result.get("column_5") ? result.get("column_5") : ""),
                        String.valueOf(null != result.get("column_6") ? result.get("column_6") : ""),
                        String.valueOf(null != result.get("column_7") ? result.get("column_7") : ""),

                ]
                model.addRow(object);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

    def dataModelAdditionalSPK(def params){
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = receptionBPService.datatablesAdditionalSPK(params);
            for(Map<String, Object> result : results) {
                def Object[] object = [
                        String.valueOf(null != result.get("column_0") ? result.get("column_0") : ""),
                        String.valueOf(null != result.get("column_1") ? result.get("column_1") : ""),
                        String.valueOf(null != result.get("column_2") ? result.get("column_2") : ""),
                        String.valueOf(null != result.get("column_3") ? result.get("column_3") : ""),
                        Integer.parseInt(null != result.get("column_4") ? String.valueOf(result.get("column_4")) : "0"),
                        Integer.parseInt(null != result.get("column_5") ? String.valueOf(result.get("column_5")) : "0"),
                        Integer.parseInt(null != result.get("column_6") ? String.valueOf(result.get("column_6")) : "0")
                ]
                model.addRow(object);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }

	def datatablesSAList(){
        params.companyDealerId = session.userCompanyDealerId
		render receptionBPService.datatablesSAList(params) as JSON;
	}

	def dataTablesJenisPayment(){
		render receptionBPService.datatablesJenisPayment(params) as JSON;		
	}

    def dataTablesJenisInsurance(){
        render receptionBPService.dataTablesJenisInsurance(params) as JSON;
    }

	def dataTablesJenisPekerjaan(){
		render receptionBPService.datatablesJenisPekerjaan(params) as JSON;			
	}
	
	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}
	
}
