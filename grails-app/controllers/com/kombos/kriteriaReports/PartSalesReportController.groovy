package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.ManPower
import com.kombos.administrasi.ManPowerDetail
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.Warna
import com.kombos.hrd.Karyawan
import com.kombos.kriteriaReport.PartSalesService
import com.kombos.maintable.PartInv
import com.kombos.parts.*
import com.kombos.reception.Reception
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRPdfExporter
import net.sf.jasperreports.engine.export.JRPdfExporterParameter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.FetchMode
import org.hibernate.criterion.CriteriaSpecification

import javax.swing.table.DefaultTableModel
import java.text.DateFormat
import java.text.SimpleDateFormat

class PartSalesReportController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
	
	def partSalesService;

    def jasperService
	
	def rootPath;	
	
	def DefaultTableModel tableModel;
	
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
	
	 def index() {}
	 
	def previewDataManual(){
        def formatFile = "";
        def formatBuntut = "";
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }

        rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
		if(params.namaReport == "16"){
			reportTransaksiPartsPerSupplier(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "10"){
			reportDataOFPartTransaction(params,formatFile,formatBuntut);
		} else
		if(params.namaReport == "11"){
			reportDataStockMovementReport(params);
		} else
		if(params.namaReport == "12"){
			reportDataStockMovementPerCode(params);
		} else
		if(params.namaReport == "13"){
			reportDataWipUnitReport(params);
		} else
		if(params.namaReport == "14"){
			reportDataStockTaking(params, formatFile, formatBuntut);
		} else
		if(params.namaReport == "15"){
            reportDataBaStockTaking(params, formatFile, formatBuntut);
		}
		
	}

    def previewData(){
        def formatFile = "";
        def formatBuntut = "";
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
        }

        rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";
	
        List<JasperReportDef> reportDefList = []
        def file = null
		def jasperParams = [:]      
		
        if(params.namaReport=="01"){
            Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
            Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
            def workshop = (params.workshop != null ? params.workshop : "")
            jasperParams.put("WORKSHOP", (null != CompanyDealer.findById(workshop).m011NamaWorkshop ? CompanyDealer.findById(workshop).m011NamaWorkshop : "") )
            jasperParams.put("PERIODE",tgl.format('dd/MM/yyyy') + " S.D " + tgl2.format('dd/MM/yyyy'))

            (1..3).each {
                params.tgl = tgl
                params.tgl2 = tgl2
                params.nomor = it
                def reportData01 = partSalesPerParts(params)
                def reportDef = new JasperReportDef(name:'Part_Sales_PerParts.jasper',
                        parameters: jasperParams,
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData01
                )
                reportDefList.add(reportDef)
            }
            file = File.createTempFile("PartSales_PerParts_",formatFile)
        }		
		else if(params.namaReport=="02"){
			def reportData02 = trxPartsByKodeParts(params, jasperParams)
            def reportDef = new JasperReportDef(name:'Transaksi_By_Kode_Part.jasper',
					parameters: jasperParams,
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData02
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Transaksi_By_Kode_Part_",formatFile)
        }
		else if(params.namaReport=="03"){
            Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
            Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
            def workshop = (params.workshop != null ? params.workshop : "")
            jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
            jasperParams.put("PERIODE",tgl.format('dd/MM/yyyy')+" s.d. "+tgl2.format('dd/MM/yyyy'))

            (1..2).each {
                params.nomor = it
                def reportData03 = trxPartsSales(params)
                def reportDef = new JasperReportDef(name:'Part_Sales.jasper',
                        parameters: jasperParams,
                        fileFormat: params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData03
                )
                reportDefList.add(reportDef)
            }
            file = File.createTempFile("Part_Sales_("+workshop+")_"+tgl.format('ddMMyy')+"sd"+tgl2.format('ddMMyy')+"_", formatFile)
        }
        else if(params.namaReport=="035"){
            Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
            Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
            def workshop = (params.workshop != null ? params.workshop : "")
            jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
            jasperParams.put("PERIODE",tgl.format('dd/MM/yyyy')+" s.d. "+tgl2.format('dd/MM/yyyy'))
            def reportData03 = trxPartsSalesTunai(params)
            def reportDef = new JasperReportDef(name:'Part_Sales_Tunai.jasper',
                    parameters: jasperParams,
                    fileFormat: params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData03
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Part_Sales_("+workshop+")_"+tgl.format('ddMMyy')+"sd"+tgl2.format('ddMMyy')+"_", formatFile)
        }
		else if(params.namaReport=="04"){

            def workshop = (params.workshop != null ? params.workshop : "")

            Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
            Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)

            jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
            jasperParams.put("PERIODE",tgl.format('dd-MM-yyyy')+" s.d. "+tgl2?.format("dd-MM-yyyy"))

            def gr = GoodsReceive.createCriteria().list {
                eq("companyDealer",CompanyDealer.findById(workshop))
                ge("t167TglJamReceive",tgl)
                lt("t167TglJamReceive",tgl2 + 1)
                eq("staDel","0")
                resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                projections {
                    groupProperty("vendor", "vendor")
                }
            }
            gr.each {
                params.vendorId = (it?.vendor?.id)
                def reportData04 = partsReceiving(params)
                def reportDef = new JasperReportDef(name:'Parts_Receiving.jasper',
                        parameters: jasperParams,
                        fileFormat: params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData04
                )
                reportDefList.add(reportDef)
            }
            if(gr.size() == 0){
                def reportData04 = dataDummy3()
                def reportDef = new JasperReportDef(name:'Parts_Receiving.jasper',
                        parameters: jasperParams,
                        fileFormat: params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData04
                )
                reportDefList.add(reportDef)
            }

            file = File.createTempFile("Parts_Receiving_("+workshop+")_"+tgl.format('ddMMyy')+"sd"+tgl2.format('ddMMyy')+"_", formatFile)
        }
		else if(params.namaReport=="05"){
			def reportData05 = partsReturn(params, jasperParams)
            def reportDef = new JasperReportDef(name:'Part_Return.jasper',
					parameters: jasperParams,
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData05
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Part_Return_",".pdf")
        }
		else if(params.namaReport=="06"){
			def reportData06 = partsOnHandsAdjustment(params, jasperParams)
            def reportDef = new JasperReportDef(name:'Part_OH_Adjustment.jasper',
					parameters: jasperParams,
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData06
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Part_OH_Adjustment_",".pdf")
        }
		else if(params.namaReport=="07"){
			def reportData07 = partsDisposal(params, jasperParams)
            def reportDef = new JasperReportDef(name:'Part_Disposal.jasper',
					parameters: jasperParams,
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData07
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Part_Disposal_",".pdf")
        }
		else if(params.namaReport=="08"){
			def reportData08 = stockReport(params, jasperParams)
            def reportDef = new JasperReportDef(name:'Report_Stock.jasper',
					parameters: jasperParams,
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData08
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Report_Stock_",".pdf")
        } 

        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", formatBuntut)
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }
	
	def reportTransaksiPartsPerSupplier(def params,String file,String format){
        List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();
        def vendors = Vendor.createCriteria().list {
            if(params.kodeVendor){
                or{
                    eq("m121ID",params.kodeVendor,[ignoreCase : true])
                    ilike("m121ID","%"+params.kodeVendor+"%")
                    eq("m121Nama",params.kodeVendor,[ignoreCase : true])
                    ilike("m121Nama","%"+params.kodeVendor+"%");
                }
            }
            eq("staDel","0")
        }
        vendors.each{
            def vendorId = partSalesService.getVendorId(it.m121Nama);
            def jml = dataModelTransaksiPartsPerSupplier(params, vendorId).size()
            if(jml==0){
                return
            }
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
            tableModelData(params);
            HashMap<String, Object> parameters = new HashMap<String, Object>();
            def startDate = df.parse(params.tanggal);
            def endDate = df.parse(params.tanggal2);

            def result = partSalesService.getWorkshopByCode(params.workshop);

            parameters.put("SUBREPORT_DIR", rootPath);
            parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
            parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
            parameters.put("kodeVendor", partSalesService.getKodeVendor(vendorId));
            parameters.put("namaVendor", partSalesService.getNamaVendor(vendorId));
            JRDataSource dataSource = new JRTableModelDataSource(dataModelTransaksiPartsPerSupplier(params, vendorId));
            parameters.put("sub_report", dataSource);
            JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Parts_Per_Vendor.jasper", parameters,
                    new JRTableModelDataSource(tableModel));
            if(jml>0){
                jasperPrintList.add(jasperPrint)
            }
        }
        File pdf = File.createTempFile("Parts_Per_Vendor", file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        JRExporter exporter = new JRPdfExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());

        }else{
            exporter.setParameter(JRPdfExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
            exporter.setParameter(JRPdfExporterParameter.OUTPUT_STREAM, response?.getOutputStream());

        }
        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            exporter.exportReport();
        }
	}
	
	def reportDataOFPartTransaction(def params,String file,String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);
		
		def result = partSalesService.getWorkshopByCode(params.workshop);
		
		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("WORKSHOP", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("PERIODE", df1.format(startDate) + " - " + df1.format(endDate));		
		
		JRDataSource dataSource = new JRTableModelDataSource(dataModelDetail(params));
		parameters.put("detailDataSource", dataSource);		
		dataSource = new JRTableModelDataSource(dataModelCatatan(params));
		parameters.put("catatanDataSource", dataSource);

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Report_Data_Of_Part_Trx.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Report_Data_Of_Part_Trx", file);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}

	def reportDataStockMovementReport(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

		def result = partSalesService.getWorkshopByCode(params.workshop);

		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));

		JRDataSource dataSource = new JRTableModelDataSource(dataModelPartStockMovementReport(params));
		parameters.put("sub_report", dataSource);

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Parts_Stock_Movement_Report.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Parts_Stock_Movement_Report", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}

	def reportDataStockMovementPerCode(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

		def headerDatas = partSalesService.headerForReportStockMovement(params);
		def headerData = new HashMap();
		if(headerDatas.size() > 0){
			headerData = headerDatas.get(0);
		}

		def result = partSalesService.getWorkshopByCode(params.workshop);

		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));
		parameters.put("kodePart", (headerData != null ? String.valueOf(headerData.get("column_0")) : "" ));
		parameters.put("namaPart", (headerData != null ? String.valueOf(headerData.get("column_1")) : "" ));
		parameters.put("lc", (headerData != null ? String.valueOf(headerData.get("column_2")) : "" ));
		parameters.put("stokAwal", (headerData != null ? String.valueOf(headerData.get("column_3")) : "" ));
		parameters.put("stokAkhir", (headerData != null ? String.valueOf(headerData.get("column_4")) : "" ));

		JRDataSource dataSource = new JRTableModelDataSource(dataModelPartStockMovementPerCodeReport(params));
		parameters.put("sub_report", dataSource);

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Parts_Stock_Movement_Per_Code_Report.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Parts_Stock_Movement_Per_Code_Report", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}

	def reportDataWipUnitReport(def params){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

		def result = partSalesService.getWorkshopByCode(params.workshop);

		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));

		JRDataSource dataSource = new JRTableModelDataSource(dataModelWipUnitReport(params));
		parameters.put("sub_report", dataSource);
		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Parts_Wip_Unit_Report.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Parts_Wip_Unit_Report", ".pdf");
		pdf.deleteOnExit();
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
		response.outputStream << pdf.newInputStream();
	}

	def reportDataStockTaking(def params, String file, String format){
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
		tableModelData(params);
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		def startDate = df.parse(params.tanggal);
		def endDate = df.parse(params.tanggal2);

		def result = partSalesService.getWorkshopByCode(params.workshop);

		parameters.put("SUBREPORT_DIR", rootPath);
		parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
		parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));

		JRDataSource dataSource = new JRTableModelDataSource(dataModelStockTaking(params, (result != null ? String.valueOf(result.get("name")) : "")));
		parameters.put("sub_report", dataSource);

		JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Parts_Stock_Taking.jasper", parameters,
						new JRTableModelDataSource(tableModel));
		File pdf = File.createTempFile("Parts_Stock_Taking", file);
		pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
	}

    def reportDataBaStockTaking(def params, String file, String format){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat df1 = new SimpleDateFormat("dd MMM yyyy");
        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def startDate = df.parse(params.tanggal);
        def endDate = df.parse(params.tanggal2);

        def result = partSalesService.getWorkshopByCode(params.workshop);

        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", (result != null ? String.valueOf(result.get("name")) : ""));
        parameters.put("periode", df1.format(startDate) + " - " + df1.format(endDate));

        JRDataSource dataSource = new JRTableModelDataSource(dataModelBaStockTaking(params, (result != null ? String.valueOf(result.get("name")) : "")));
        parameters.put("sub_report", dataSource);

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "Parts_BA_Stock_Taking.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("Parts_BA_Stock_Taking_", file);
        pdf.deleteOnExit();

        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        if(params?.format=="x"){
            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
            exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        }else{
            JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(pdf));
        }

        response.setHeader("Content-Type", format);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        if(params?.format=="x"){
            exporterXLS.exportReport();
        }else{
            response.outputStream << pdf.newInputStream();
        }
    }

	def dataModelTransaksiPartsPerSupplier(def params, def vendorId){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = partSalesService.datatablesTransaksiPartsPerSupplier(params, vendorId);

			for(Map<String, Object> result : results) {
				Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					Integer.parseInt(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0"),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0"),
					new java.math.BigDecimal(result.get("column_9") != null ? String.valueOf(result.get("column_9")) : "0"),
					new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0"),
					new java.math.BigDecimal(result.get("column_11") != null ? String.valueOf(result.get("column_11")) : "0"),
					new java.math.BigDecimal(result.get("column_12") != null ? String.valueOf(result.get("column_12")) : "0")
				]
				model.addRow(object)
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def dataModelDetail(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = partSalesService.datatablesDetail(params);

			for(Map<String, Object> result : results) {
				Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),
					String.valueOf(result.get("column_10") != null ? result.get("column_10") : ""),
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : ""),
					String.valueOf(result.get("column_12") != null ? result.get("column_12") : ""),
					String.valueOf(result.get("column_13") != null ? result.get("column_13") : ""),
					String.valueOf(result.get("column_14") != null ? result.get("column_14") : ""),
					String.valueOf(result.get("column_15") != null ? result.get("column_15") : "")
				]
				model.addRow(object)
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}

	def dataModelCatatan(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = partSalesService.datatablesCatatan(params);

			for(Map<String, Object> result : results) {
				Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					result.get("column_8") != null ? new java.math.BigDecimal(String.valueOf(result.get("column_8"))) : null,
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),
					result.get("column_10") != null ? new java.math.BigDecimal(String.valueOf(result.get("column_10"))) : null,
					result.get("column_11") != null ? new java.math.BigDecimal(String.valueOf(result.get("column_11"))) : null,
					result.get("column_12") != null ? new java.math.BigDecimal(String.valueOf(result.get("column_12"))) : null,
					result.get("column_13") != null ? new java.math.BigDecimal(String.valueOf(result.get("column_13"))) : null
				]
				model.addRow(object)
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return model;
	}
	
	def dataModelPartStockMovementReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21",
					"column_22", "column_23", "column_24", "column_25"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = partSalesService.datatablesPartStockMovementReport(params);
			
			for(Map<String, Object> result : results) {				
				Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),
					new java.math.BigDecimal(result.get("column_3") != null ? String.valueOf(result.get("column_3")) : "0"),
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),
					new java.math.BigDecimal(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0"),					
					String.valueOf(result.get("column_7") != null ? result.get("column_7") : ""),
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),
					new java.math.BigDecimal(result.get("column_9") != null ? String.valueOf(result.get("column_9")) : "0"),					
					String.valueOf(result.get("column_10") != null ? result.get("column_10") : ""),
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : ""),
					new java.math.BigDecimal(result.get("column_12") != null ? String.valueOf(result.get("column_12")) : "0"),					
					String.valueOf(result.get("column_13") != null ? result.get("column_13") : ""),
					String.valueOf(result.get("column_14") != null ? result.get("column_14") : ""),
					new java.math.BigDecimal(result.get("column_15") != null ? String.valueOf(result.get("column_15")) : "0"),					
					String.valueOf(result.get("column_16") != null ? result.get("column_16") : ""),
					String.valueOf(result.get("column_17") != null ? result.get("column_17") : ""),
					new java.math.BigDecimal(result.get("column_18") != null ? String.valueOf(result.get("column_18")) : "0"),					
					String.valueOf(result.get("column_19") != null ? result.get("column_19") : ""),
					String.valueOf(result.get("column_20") != null ? result.get("column_20") : ""),
					new java.math.BigDecimal(result.get("column_21") != null ? String.valueOf(result.get("column_21")) : "0"),					
					String.valueOf(result.get("column_22") != null ? result.get("column_22") : ""),
					String.valueOf(result.get("column_23") != null ? result.get("column_23") : ""),
					new java.math.BigDecimal(result.get("column_24") != null ? String.valueOf(result.get("column_24")) : "0"),					
					String.valueOf(result.get("column_25") != null ? result.get("column_25") : ""),
				]
				model.addRow(object)
			} 			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
	
	def dataModelPartStockMovementPerCodeReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20", "column_21"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = partSalesService.datatablesPartStockMovementPerCodeReport(params);
			
			for(Map<String, Object> result : results) {				
				Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),										
					new java.math.BigDecimal(result.get("column_1") != null ? String.valueOf(result.get("column_1")) : "0"),					
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),					
					new java.math.BigDecimal(result.get("column_3") != null ? String.valueOf(result.get("column_3")) : "0"),					
					new java.math.BigDecimal(result.get("column_4") != null ? String.valueOf(result.get("column_4")) : "0"),					
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),					
					new java.math.BigDecimal(result.get("column_6") != null ? String.valueOf(result.get("column_6")) : "0"),					
					new java.math.BigDecimal(result.get("column_7") != null ? String.valueOf(result.get("column_7")) : "0"),					
					String.valueOf(result.get("column_8") != null ? result.get("column_8") : ""),					
					new java.math.BigDecimal(result.get("column_9") != null ? String.valueOf(result.get("column_9")) : "0"),					
					new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0"),					
					String.valueOf(result.get("column_11") != null ? result.get("column_11") : ""),					
					new java.math.BigDecimal(result.get("column_12") != null ? String.valueOf(result.get("column_12")) : "0"),					
					new java.math.BigDecimal(result.get("column_13") != null ? String.valueOf(result.get("column_13")) : "0"),					
					String.valueOf(result.get("column_14") != null ? result.get("column_14") : ""),					
					new java.math.BigDecimal(result.get("column_15") != null ? String.valueOf(result.get("column_15")) : "0"),					
					new java.math.BigDecimal(result.get("column_16") != null ? String.valueOf(result.get("column_16")) : "0"),					
					String.valueOf(result.get("column_17") != null ? result.get("column_17") : ""),					
					new java.math.BigDecimal(result.get("column_18") != null ? String.valueOf(result.get("column_18")) : "0"),					
					new java.math.BigDecimal(result.get("column_19") != null ? String.valueOf(result.get("column_19")) : "0"),					
					String.valueOf(result.get("column_20") != null ? result.get("column_20") : ""),					
					new java.math.BigDecimal(result.get("column_21") != null ? String.valueOf(result.get("column_21")) : "0"),					
				]
				
				model.addRow(object)
			} 			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
	
	def dataModelWipUnitReport(def params){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		try {
			def results = partSalesService.datatablesWipUnitReport(params);
			
			for(Map<String, Object> result : results) {				
				Object[] object = [
					String.valueOf(result.get("column_0") != null ? result.get("column_0") : ""),															
					String.valueOf(result.get("column_1") != null ? result.get("column_1") : ""),															
					String.valueOf(result.get("column_2") != null ? result.get("column_2") : ""),															
					String.valueOf(result.get("column_3") != null ? result.get("column_3") : ""),															
					String.valueOf(result.get("column_4") != null ? result.get("column_4") : ""),															
					String.valueOf(result.get("column_5") != null ? result.get("column_5") : ""),															
					String.valueOf(result.get("column_6") != null ? result.get("column_6") : ""),															
					new java.math.BigDecimal(result.get("column_7") != null ? String.valueOf(result.get("column_7")) : "0"),					
					new java.math.BigDecimal(result.get("column_8") != null ? String.valueOf(result.get("column_8")) : "0"),					
					String.valueOf(result.get("column_9") != null ? result.get("column_9") : ""),												
					new java.math.BigDecimal(result.get("column_10") != null ? String.valueOf(result.get("column_10")) : "0"),										
					new java.math.BigDecimal(result.get("column_11") != null ? String.valueOf(result.get("column_11")) : "0"),										
					new java.math.BigDecimal(result.get("column_12") != null ? String.valueOf(result.get("column_12")) : "0"),										
					new java.math.BigDecimal(result.get("column_13") != null ? String.valueOf(result.get("column_13")) : "0"),										
					new java.math.BigDecimal(result.get("column_14") != null ? String.valueOf(result.get("column_14")) : "0"),										
					new java.math.BigDecimal(result.get("column_15") != null ? String.valueOf(result.get("column_15")) : "0")										
				]
				
				model.addRow(object)
			} 			
		} catch(Exception e){
			e.printStackTrace();				
		}
		return model;
	}
	
	def dataModelStockTaking(def params, def workshop){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        def TglPriview = df.parse(params.tanggal);

        //BeforePart
        def jumlahPartBefore =  "0";
        try {
            jumlahPartBefore = partSalesService.getBeforeJumlahPart(params)
        } catch(Exception e){
            e.printStackTrace();
        }

        //AfterPart
        def jumlahPartAfter = "0";
        try {
            jumlahPartAfter = partSalesService.getAfterJumlahPart(params)
        } catch (Exception e){
            e.printStackTrace();
        }

        //DifferenPart
        def jumlahPartDifferen = "0";
        jumlahPartDifferen =   Integer.parseInt(String.valueOf(jumlahPartAfter - jumlahPartBefore))

        //BeforeOil
        def jumlahOilBefore = "0";
        try {
            jumlahOilBefore = partSalesService.getBeforeJumlahOil(params)
        } catch (Exception e){
            e.printStackTrace();
        }

        //AfterOil
        def jumlahOilAfter = "0";
        try {
            jumlahOilAfter = partSalesService.getAfterJumlahOil(params)
        }catch (Exception e){
            e.printStackTrace();
        }

        //DifferenOil
        def jumlahOilDifferen = "0";
            jumlahOilDifferen = Integer.parseInt(String.valueOf(jumlahOilAfter - jumlahOilBefore))

        //TotQtyBefore
        def totQtyBefore = "0";
            totQtyBefore =  Integer.parseInt(String.valueOf(jumlahPartBefore + jumlahOilBefore))

        //TotQtyAfter
        def totQtyAfter = "0";
        totQtyAfter = Integer.parseInt(String.valueOf(jumlahPartAfter + jumlahOilAfter))

        //TotQtyDifferen
        def totQtyDifferen = "0";
            totQtyDifferen = Integer.parseInt(String.valueOf(jumlahPartDifferen + jumlahOilDifferen))

        //AmountPartBefore
        def jumAmountPartBefore = "0";
//            jumAmountPartBefore = partSalesService.getAmountPartBefore(params)

        //AmountPartAfter
        def jumAmountPartAfter = "0";
//            jumAmountPartAfter = partSalesService.getAmountPartAfter(params)

        //AmmountPartDifferen
        def jumAmountPartDifferen = "0";
//            jumAmountPartDifferen = Integer.parseInt(String.valueOf(jumAmountPartAfter - jumAmountPartBefore))

        //AmmountOilBefore
        def jumAmountOilBefore = "0";
//            jumAmountOilBefore = partSalesService.getAmountOilBefore(params)

        //AmmountOilAfter
        def jumAmountOilAfter = "0";
//            jumAmountOilAfter = partSalesService.getAmountOilAfter(params)

        //AmmountOilDifferen
        def jumAmountOilDifferen = "0";
//            jumAmountOilDifferen = Integer.parseInt(String.valueOf(jumAmountOilAfter - jumAmountOilBefore))

        def totQtyAmountBefore = "0";
            totQtyAmountBefore = Integer.parseInt(String.valueOf(jumAmountPartBefore + jumAmountOilBefore))

        def totQtyAmountAfter = "0";
//            totQtyAmountAfter = Integer.parseInt(String.valueOf(jumAmountPartAfter + jumAmountOilAfter))

        def totQtyAmountDifferen = "0";
//            totQtyAmountDifferen = Integer.parseInt(String.valueOf(jumAmountPartDifferen + jumAmountOilDifferen))

        def jumAll ="0";
//            jumAll = totQtyAmountAfter

        def col19 = df.format(TglPriview)
        def col20 = workshop + " "+", " +df.format(TglPriview)
        def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        def String[] data = [jumlahPartBefore, jumlahOilBefore, jumlahPartAfter, jumlahOilAfter, jumlahPartDifferen
                            , jumlahOilDifferen, totQtyBefore, totQtyAfter, totQtyDifferen,
                            jumAmountPartBefore, jumAmountOilBefore, jumAmountPartAfter, jumAmountOilAfter,
                            jumAmountPartDifferen, jumAmountOilDifferen, totQtyAmountBefore, totQtyAmountAfter, totQtyAmountDifferen,
                            jumAll, col19, col20
        ];
        model.addRow(data);
        return model;

    }

	def dataModelBaStockTaking(def params, def workshop){
		def String[] columnNames = ["column_0", "column_1", "column_2", "column_3", "column_4", "column_5", "column_6", "column_7", "column_8", "column_9", "column_10",
					"column_11", "column_12", "column_13", "column_14", "column_15", "column_16", "column_17", "column_18", "column_19", "column_20",
					"column_21", "column_22", "column_23", "column_24", "column_25", "column_26", "column_27", "column_28", "column_29", "column_30",
					"column_31", "column_32", "column_33", "column_34", "column_35", "column_36", "column_37", "column_38", "column_39", "column_40",
					"column_41", "column_42", "column_43", "column_44", "column_45", "column_46", "column_47", "column_48", "column_49", "column_50",
					"column_51", "column_52", "column_53", "column_54", "column_55", "column_56", "column_57", "column_58", "column_59", "column_60",
					"column_61", "column_62", "column_63", "column_64", "column_65", "column_tgl"];
		def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = partSalesService.dataBaStockTackingList(params);
            results.each{
                def Object[] object = [
                        String.valueOf(it.column_0),
                        String.valueOf(it.column_1),
                        String.valueOf(it.column_2),
                        String.valueOf(it.column_3),
                        String.valueOf(it.column_4),
                        String.valueOf(it.column_5),
                        String.valueOf(it.column_6),
                        String.valueOf(it.column_7),
                        String.valueOf(it.column_8),
                        String.valueOf(it.column_9),
                        String.valueOf(it.column_10),
                        String.valueOf(it.column_11),
                        String.valueOf(it.column_12),
                        String.valueOf(it.column_13),
                        String.valueOf(it.column_14),
                        String.valueOf(it.column_15),
                        String.valueOf(it.column_16),
                        String.valueOf(it.column_17),
                        String.valueOf(it.column_18),
                        String.valueOf(it.column_19),
                        String.valueOf(it.column_20),
                        String.valueOf(it.column_21),
                        String.valueOf(it.column_22),
                        String.valueOf(it.column_23),
                        String.valueOf(it.column_24),
                        String.valueOf(it.column_25),
                        String.valueOf(it.column_26),
                        String.valueOf(it.column_27),
                        String.valueOf(it.column_28),
                        String.valueOf(it.column_29),
                        String.valueOf(it.column_30),
                        String.valueOf(it.column_31),
                        String.valueOf(it.column_32),
                        String.valueOf(it.column_33),
                        String.valueOf(it.column_34),
                        String.valueOf(it.column_35),
                        String.valueOf(it.column_36),
                        String.valueOf(it.column_37),
                        String.valueOf(it.column_38),
                        String.valueOf(it.column_39),
                        String.valueOf(it.column_40),
                        String.valueOf(it.column_41),
                        String.valueOf(it.column_42),
                        String.valueOf(it.column_43),
                        String.valueOf(it.column_44),
                        String.valueOf(it.column_45),
                        String.valueOf(it.column_46),
                        String.valueOf(it.column_47),
                        String.valueOf(it.column_48),
                        String.valueOf(it.column_49),
                        String.valueOf(it.column_50),
                        String.valueOf(it.column_51),
                        String.valueOf(it.column_52),
                        String.valueOf(it.column_53),
                        String.valueOf(it.column_54),
                        String.valueOf(it.column_55),
                        String.valueOf(it.column_56),
                        String.valueOf(it.column_57),
                        String.valueOf(it.column_58),
                        String.valueOf(it.column_59),
                        String.valueOf(it.column_60),
                        String.valueOf(it.column_61),
                        String.valueOf(it.column_62),
                        String.valueOf(it.column_63),
                        String.valueOf(it.column_64),
                        String.valueOf(it.column_65),
                        String.valueOf(it.column_tgl)
                ]
                model.addRow(object)
            }
        }catch (Exception e){
            println("Error BA "+e)
        }
		return model;
	}
	
	def dataDummy(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1;x++){
			def data = [:]
				data.put("FRANC", null)
				data.put("INVOIC_DATE", null)
				data.put("NO_INVOICE", null)
				data.put("KODE_PARTS", null)
				data.put("NAMA_PARTS", null)
				data.put("QTY", null)
				data.put("SATUAN", null)
				data.put("L_C", null)
				data.put("UNIT_PRICE", null)
				data.put("DISC", null)
				
				reportData.add(data)
		}		
		return reportData;
	}
	
	def dataDummy1(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1;x++){
			def data = [:]
				data.put("INVOIC_DATE", null)
				data.put("NO_INVOICE", null)
				data.put("QTY_IN", null)
				data.put("SATUAN", null)
				data.put("QTY_OUT", null)
				data.put("L_C", null)
				data.put("UNIT_PRICE", null)
				data.put("DISC", null)				
				reportData.add(data)
		}
		
		return reportData;
	}
	
	def dataDummy2(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1;x++){
			def data = [:]
			data.put("FRANC", null)
			data.put("INVOICE_DATE", null)
			data.put("NO_INVOICE", null)
			data.put("KODE_PARTS", null)
			data.put("NAMA_PARTS", null)
			data.put("QTY", null)
			data.put("SATUAN", null)
			data.put("L_C", null)
			data.put("UNIT_PRICE", null)
			data.put("DISC", null)				
			reportData.add(data)
		}				
		return reportData;
	}
	
	def dataDummy3(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1;x++){
			def data = [:]
			data.put("FRANC", null)
			data.put("RECEIVING_DATE", null)
			data.put("NO_INVOICE", null)
			data.put("NAMA_VENDOR", null)
			data.put("KODE_PART", null)
			data.put("NAMA_PART", null)
			data.put("QTY", null)
			data.put("SATUAN", null)
			data.put("L_C", null)
			data.put("HARGA_BELI", null)
			data.put("NILAI_INVOICE", null)  //???
				
				reportData.add(data)
		}		
		
		return reportData;
	}
	def dataDummy4(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1;x++){
			def data = [:]
			data.put("RETURN_DATE", null);
			data.put("NO_RETURN", null);
			data.put("FRANC", null);
			data.put("RECEIVING_DATE", null);
			data.put("NO_INVOICE", null);
			data.put("NAMA_VENDOR", null);
			data.put("KODE_PART",  null);
			data.put("NAMA_PART", null);
			data.put("QTY", null);
			data.put("SATUAN", null);
			data.put("L_C", null);
			reportData.add(data);
		}		
		return reportData;
	}
	
	def dataDummy5(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1;x++){
			def data = [:]
			data.put("OH_DATE", "-")
			data.put("NO_OH", "-")
			data.put("KODE_PART", "-")
			data.put("NAMA_PART", "-")
			data.put("FRANC", "-")
			data.put("L_C", "-")
			data.put("QTY_TERCATAT", "-")
			data.put("QTY_TERCATAT_SATUAN", "-")
			data.put("QTY_REAL", "-")
			data.put("QTY_REAL_SATUAN", "-")
            data.put("QTY_SELISIH", "-")
			data.put("QTY_SELISIH_SATUAN", "-")
            data.put("TOTAL_LC", "-")
			data.put("ALASAN", "-")
			reportData.add(data)
		}	
		return reportData;
	}
	
	def dataDummy7(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1 ;x++){
			def data = [:]
			data.put("KODE_PART",null)
			data.put("NAMA_PART",null)
			data.put("L_C",null)
			data.put("LOCATION",null)
			data.put("OH_QTY",null)
			data.put("OH_SATUAN",null)
			data.put("RESERVED_QTY",null)
			data.put("RESERVED_SATUAN",null)
			data.put("WIP_QTY",null)
			data.put("WIP_SATUAN",null)
			data.put("BLOCKED_STOCK_QTY",null)
			data.put("BLOCKED_STOCK_SATUAN",null)
			data.put("TOTAL_QTY",null)
			data.put("TOTAL_SATUAN","Buah")
			data.put("PRICE_UNIT",null)
			data.put("FRANC",null)
			reportData.add(data)
		}		
		
		return reportData;
	}
	
	def dataDummy8(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <1; x++){
			def data = [:]
			data.put("DATE",null)
			data.put("NO_REFF",null)
			data.put("JENIS_TRANSAKSI",null)
			data.put("KODE_PART",null)
			data.put("NAMA_PART",null)
			data.put("FRANC",null)
			data.put("QTY",null)
			data.put("SATUAN",null)
			data.put("L_C",null)
			data.put("HARGA_BELI",null)
			data.put("NILAI_INVOICE",null)
			
			reportData.add(data)
		}		
		return reportData;
	}
	
	def dataDummy9(){
		def reportData = new ArrayList();
		for (int x = 0 ; x <=5;x++){
			def data = [:]
			data.put("DATE",new Date().format('dd-MM-yyyy'))
			data.put("TIME",new Date().format('HH:mm:ss'))
			data.put("NAMA_PROCESS","Claim")
			data.put("NOREFF1","0001")
			data.put("NOREFF2","3232")
			data.put("TIPE_JOB","")
			data.put("VENDOR","vendor claim")
			data.put("NO_PART_SLIP","")
			data.put("KODE_PART","00x"+x)
			data.put("NAMA_PART","part"+x)
			data.put("QTY",new BigDecimal(x))
			data.put("SATUAN","Satuan")
			data.put("L_C",new BigDecimal(x))
			data.put("HARGA_JUAL",new BigDecimal(0))
			data.put("DISC_AMOUNT",new BigDecimal(0))
			reportData.add(data)
			
		}
		for (int x = 0 ; x <=5;x++){
			def data = [:]
			data.put("DATE",new Date().format('dd-MM-yyyy'))
			data.put("TIME",new Date().format('HH:mm:ss'))
			data.put("NAMA_PROCESS","Receiving")
			data.put("NOREFF1","0001")
			data.put("NOREFF2","3232")
			data.put("TIPE_JOB","")
			data.put("VENDOR","vendor claim")
			data.put("NO_PART_SLIP","")
			data.put("KODE_PART","00x"+x)
			data.put("NAMA_PART","part"+x)
			data.put("QTY",new BigDecimal(x))
			data.put("SATUAN","Satuan")
			data.put("L_C",new BigDecimal(x))
			data.put("HARGA_JUAL",new BigDecimal(0))
			data.put("DISC_AMOUNT",new BigDecimal(0))
			reportData.add(data)
		}
		
		return reportData;
	}
	
	//1. Report Part Sales Per Parts
	def partSalesPerParts(def params) {
		def reportData = new ArrayList();
		def listPart = PartInv.createCriteria().list{
            eq("companyDealer",CompanyDealer.get(params.workshop as long))
            eq("t703StaDel","0")
            invoice {
                eq("t701StaDel","0");
                isNull("t701NoInv_Reff");
                isNull("t701StaApprovedReversal")
                gt("t701TotalBayarRp",0.toDouble())
				ge("t701TglJamInvoice",params.tgl)
				lt("t701TglJamInvoice",params.tgl2 + 1)
			}
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("goods","goods")
                groupProperty("t703HargaRp","t703HargaRp")
                groupProperty("t703LandedCost","t703LandedCost")
                sum("t703Jumlah1","t703Jumlah1")
                order("goods","asc")
            }
		}
        def listPartLangsung= SalesOrderDetail.createCriteria().list{
            sorNumber {
                eq("companyDealer",CompanyDealer.get(params.workshop as long))
                ge("sorDate",params.tgl)
                lt("sorDate",params.tgl2 + 1)
                eq("staDel","0");
                eq("isApprove",Integer.parseInt("0"));
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("materialCode","materialCode")
                groupProperty("unitPrice","unitPrice")
                groupProperty("unitPriceBeli","unitPriceBeli")
                sum("quantity","quantity")
            }
            order("materialCode","asc")
        }

		def totalJual = 0;def totalBeli = 0; def total_qty = 0; int nomor = 0; def jumQty = 0;
        if((params.nomor as String).equals("1")){
            listPart.each { par ->
//                println par?.goods?.m111ID
//                println KlasifikasiGoods.findByGoods(par?.goods)?.franc?.m117NamaFranc
                if(!KlasifikasiGoods.findByGoods(par?.goods).franc.m117NamaFranc.toUpperCase().contains("PARTS")){
                    def unitprice = 0;
                    def buyingprice = 0;
                    def totunitprice = 0;
                    def totbuyingprice = 0;

                    try {
                        unitprice = par.t703HargaRp
                        buyingprice = par.t703LandedCost
                        jumQty = par?.t703Jumlah1
                        totunitprice = (unitprice * jumQty)
                        totbuyingprice = (buyingprice * jumQty)
                        //jumlah
                        total_qty += (jumQty)
                        totalJual += (totunitprice)
                        totalBeli += (totbuyingprice)
                    }catch (Exception){}
                    def data = [:]
                    nomor++
                    data.put("FRANC", "PEMAKAIAN BAHAN")
                    data.put("NO", String.valueOf(nomor))
                    data.put("KODE_PARTS", String.valueOf(null != par?.goods && null != par?.goods.m111ID ? par?.goods.m111ID : ""))
                    data.put("NAMA_PARTS", String.valueOf(null != par?.goods && null != par?.goods.m111Nama ? par?.goods.m111Nama : ""))
                    data.put("QTY",new Konversi().toRupiah(jumQty))
                    data.put("UNIT_PRICE", new Konversi().toRupiah2(unitprice))
                    data.put("TOTAL_PRICE", new Konversi().toRupiah2(totunitprice))
                    data.put("TOTAL_QTY", new Konversi().toRupiah(total_qty))
                    data.put("TOTAL", new Konversi().toRupiah2(totalJual))
                    data.put("HARGA_BELI", new Konversi().toRupiah2(buyingprice))
                    data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah2(totbuyingprice))
                    data.put("TOTAL_BELI", new Konversi().toRupiah2(totalBeli))
                    reportData.add(data)
                }
            }

            listPartLangsung.each { datt ->
                if(!KlasifikasiGoods.findByGoods(datt?.materialCode).franc.m117NamaFranc.toUpperCase().contains("PARTS")){
                    def unitprice = 0;
                    def buyingprice = 0;
                    def totunitprice = 0;
                    def totbuyingprice = 0;
                    try {
                        unitprice = datt.unitPrice
                        buyingprice = datt.unitPriceBeli
                        jumQty    = datt?.quantity
                        totunitprice = (unitprice * jumQty)
                        totbuyingprice = (buyingprice * jumQty)
                        //jumlah
                        total_qty += jumQty
                        totalJual += (totunitprice)
                        totalBeli += (totbuyingprice)
                    }catch (Exception){}
                    def data = [:]
                    nomor++
                    data.put("FRANC", "PEMAKAIAN BAHAN")
                    data.put("NO", String.valueOf(nomor))
                    data.put("KODE_PARTS", String.valueOf(null != datt?.materialCode && null != datt?.materialCode.m111ID ? datt?.materialCode.m111ID + " (T)": ""))
                    data.put("NAMA_PARTS", String.valueOf(null != datt?.materialCode && null != datt?.materialCode.m111Nama ? datt?.materialCode.m111Nama : ""))
                    data.put("QTY",new Konversi().toRupiah(jumQty))
                    data.put("UNIT_PRICE", new Konversi().toRupiah2(unitprice))
                    data.put("TOTAL_PRICE", new Konversi().toRupiah2(totunitprice))
                    data.put("TOTAL_QTY", new Konversi().toRupiah(total_qty))
                    data.put("TOTAL", new Konversi().toRupiah2(totalJual))
                    data.put("HARGA_BELI", new Konversi().toRupiah2(buyingprice))
                    data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah2(totbuyingprice))
                    data.put("TOTAL_BELI", new Konversi().toRupiah2(totalBeli))
                    reportData.add(data)
                }
            }
        }else if((params.nomor as String).equals("2")){
            listPart.each { par ->
                if(KlasifikasiGoods.findByGoods(par?.goods).franc.m117NamaFranc.toUpperCase().contains("CAMPURAN")){
                    def unitprice = 0;
                    def buyingprice = 0;
                    def totunitprice = 0;
                    def totbuyingprice = 0;

                    try {
                        unitprice = par.t703HargaRp
                        buyingprice = par.t703LandedCost
                        jumQty = par?.t703Jumlah1
                        totunitprice = (unitprice * jumQty)
                        totbuyingprice = (buyingprice * jumQty)
                        //jumlah
                        total_qty += (jumQty)
                        totalJual += (totunitprice)
                        totalBeli += (totbuyingprice)
                    }catch (Exception){}
                    def data = [:]
                    nomor++
                    data.put("FRANC", "PEMAKAIAN PARTS CAMPURAN")
                    data.put("NO", String.valueOf(nomor))
                    data.put("KODE_PARTS", String.valueOf(null != par?.goods && null != par?.goods.m111ID ? par?.goods.m111ID : ""))
                    data.put("NAMA_PARTS", String.valueOf(null != par?.goods && null != par?.goods.m111Nama ? par?.goods.m111Nama : ""))
                    data.put("QTY",new Konversi().toRupiah(jumQty))
                    data.put("UNIT_PRICE", new Konversi().toRupiah2(unitprice))
                    data.put("TOTAL_PRICE", new Konversi().toRupiah2(totunitprice))
                    data.put("TOTAL_QTY", new Konversi().toRupiah(total_qty))
                    data.put("TOTAL", new Konversi().toRupiah2(totalJual))
                    data.put("HARGA_BELI", new Konversi().toRupiah2(buyingprice))
                    data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah2(totbuyingprice))
                    data.put("TOTAL_BELI", new Konversi().toRupiah2(totalBeli))
                    reportData.add(data)
                }
            }

            listPartLangsung.each { datt ->
                if(KlasifikasiGoods.findByGoods(datt?.materialCode).franc.m117NamaFranc.toUpperCase().contains("CAMPURAN")){
                    def unitprice = 0;
                    def buyingprice = 0;
                    def totunitprice = 0;
                    def totbuyingprice = 0;
                    try {
                        unitprice = datt.unitPrice
                        buyingprice = datt.unitPriceBeli
                        jumQty    = datt?.quantity
                        totunitprice = (unitprice * jumQty)
                        totbuyingprice = (buyingprice * jumQty)
                        //jumlah
                        total_qty += jumQty
                        totalJual += (totunitprice)
                        totalBeli += (totbuyingprice)
                    }catch (Exception){}
                    def data = [:]
                    nomor++
                    data.put("FRANC", "PEMAKAIAN PARTS CAMPURAN")
                    data.put("NO", String.valueOf(nomor))
                    data.put("KODE_PARTS", String.valueOf(null != datt?.materialCode && null != datt?.materialCode.m111ID ? datt?.materialCode.m111ID + " (T)": ""))
                    data.put("NAMA_PARTS", String.valueOf(null != datt?.materialCode && null != datt?.materialCode.m111Nama ? datt?.materialCode.m111Nama : ""))
                    data.put("QTY",new Konversi().toRupiah(jumQty))
                    data.put("UNIT_PRICE", new Konversi().toRupiah2(unitprice))
                    data.put("TOTAL_PRICE", new Konversi().toRupiah2(totunitprice))
                    data.put("TOTAL_QTY", new Konversi().toRupiah(total_qty))
                    data.put("TOTAL", new Konversi().toRupiah2(totalJual))
                    data.put("HARGA_BELI", new Konversi().toRupiah2(buyingprice))
                    data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah2(totbuyingprice))
                    data.put("TOTAL_BELI", new Konversi().toRupiah2(totalBeli))
                    reportData.add(data)
                }
            }
        }else if((params.nomor as String).equals("3")){
            listPart.each { par ->
                if(KlasifikasiGoods.findByGoods(par?.goods).franc.m117NamaFranc.toUpperCase().contains("TOYOTA")){
                    def unitprice = 0;
                    def buyingprice = 0;
                    def totunitprice = 0;
                    def totbuyingprice = 0;

                    try {
                        unitprice = par.t703HargaRp
                        buyingprice = par.t703LandedCost
                        jumQty = par?.t703Jumlah1
                        totunitprice = (unitprice * jumQty)
                        totbuyingprice = (buyingprice * jumQty)
                        //jumlah
                        total_qty += (jumQty)
                        totalJual += (totunitprice)
                        totalBeli += (totbuyingprice)
                    }catch (Exception){}
                    def data = [:]
                    nomor++
                    data.put("FRANC", "PEMAKAIAN PARTS TOYOTA")
                    data.put("NO", String.valueOf(nomor))
                    data.put("KODE_PARTS", String.valueOf(null != par?.goods && null != par?.goods.m111ID ? par?.goods.m111ID : ""))
                    data.put("NAMA_PARTS", String.valueOf(null != par?.goods && null != par?.goods.m111Nama ? par?.goods.m111Nama : ""))
                    data.put("QTY",new Konversi().toRupiah(jumQty))
                    data.put("UNIT_PRICE", new Konversi().toRupiah2(unitprice))
                    data.put("TOTAL_PRICE", new Konversi().toRupiah2(totunitprice))
                    data.put("TOTAL_QTY", new Konversi().toRupiah(total_qty))
                    data.put("TOTAL", new Konversi().toRupiah2(totalJual))
                    data.put("HARGA_BELI", new Konversi().toRupiah2(buyingprice))
                    data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah2(totbuyingprice))
                    data.put("TOTAL_BELI", new Konversi().toRupiah2(totalBeli))
                    reportData.add(data)
                }
            }

            listPartLangsung.each { datt ->
                if(KlasifikasiGoods.findByGoods(datt?.materialCode).franc.m117NamaFranc.toUpperCase().contains("TOYOTA")){
                    def unitprice = 0;
                    def buyingprice = 0;
                    def totunitprice = 0;
                    def totbuyingprice = 0;
                    try {
                        unitprice = datt.unitPrice
                        buyingprice = datt.unitPriceBeli
                        jumQty    = datt?.quantity
                        totunitprice = (unitprice * jumQty)
                        totbuyingprice = (buyingprice * jumQty)
                        //jumlah
                        total_qty += jumQty
                        totalJual += (totunitprice)
                        totalBeli += (totbuyingprice)
                    }catch (Exception){}
                    def data = [:]
                    nomor++
                    data.put("FRANC", "PEMAKAIAN PARTS TOYOTA")
                    data.put("NO", String.valueOf(nomor))
                    data.put("KODE_PARTS", String.valueOf(null != datt?.materialCode && null != datt?.materialCode.m111ID ? datt?.materialCode.m111ID + " (T)": ""))
                    data.put("NAMA_PARTS", String.valueOf(null != datt?.materialCode && null != datt?.materialCode.m111Nama ? datt?.materialCode.m111Nama : ""))
                    data.put("QTY",new Konversi().toRupiah(jumQty))
                    data.put("UNIT_PRICE", new Konversi().toRupiah2(unitprice))
                    data.put("TOTAL_PRICE", new Konversi().toRupiah2(totunitprice))
                    data.put("TOTAL_QTY", new Konversi().toRupiah(total_qty))
                    data.put("TOTAL", new Konversi().toRupiah2(totalJual))
                    data.put("HARGA_BELI", new Konversi().toRupiah2(buyingprice))
                    data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah2(totbuyingprice))
                    data.put("TOTAL_BELI", new Konversi().toRupiah2(totalBeli))
                    reportData.add(data)
                }
            }
        }

//		println("listpart "+listPart.size())
		if (listPart.size() == 0){			
			reportData = dataDummy();			
		}		
		return reportData;
	}
	//=================================================================================
	//2. Report Per Kode Parts
	def trxPartsByKodeParts(def params, def jasperParams) {		
		def reportData = new ArrayList();		
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
		def workshop = (params.workshop != null ? params.workshop : "")
		def goods_id = (params.goods_id != null ? params.goods_id : "")
//		println params.goods_id;
        Goods pGoods = Goods.findById(goods_id as Long)
        def franc = KlasifikasiGoods.findByGoods(pGoods)
		def namaFranc = "";
		//iterate each goods

		jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd-MM-yyyy')+" s.d. "+tgl2.format("dd-MM-yyyy"))
		jasperParams.put("KODE_PART",pGoods.m111ID)
		jasperParams.put("NAMA_PART",(pGoods != null ? pGoods.m111Nama :""))

        /*Pembelian Barang T167*/
        def listPart1 = GoodsReceiveDetail.createCriteria().list {
            eq("staDel","0")
            eq("companyDealer",CompanyDealer.findById(workshop))
            invoice {
                ge("t166TglInv",tgl)
                lt("t166TglInv",tgl2 + 1)
                order("t166TglInv")
            }
           eq("goods", pGoods)
        }
        listPart1.each {
            franc = KlasifikasiGoods.findByGoods(it?.goods)
            if(franc){
                namaFranc = franc?.franc?.m117NamaFranc
            }
            def data = [:]
            data.put("INVOICE_DATE", (null != it?.invoice && null != it?.invoice.t166TglInv ? it?.invoice.t166TglInv.format('dd-MMM-yyyy') : "") )
            data.put("NO_INVOICE", (null != it?.invoice && null != it?.invoice.t166NoInv ? it?.invoice.t166NoInv : ""))
            data.put("QTY_IN", new BigDecimal(null != it?.invoice.t166Qty1 ? it?.invoice.t166Qty1 : 0))
            data.put("SATUAN", (null != it?.goods && null != it?.goods.satuan.m118Satuan1  ? it?.goods.satuan.m118Satuan1 : ""))
            data.put("QTY_OUT", new BigDecimal(0))
            data.put("L_C", new BigDecimal(null != it?.t167LandedCost ? it?.t167LandedCost : 0))
            data.put("UNIT_PRICE", new BigDecimal(null != it?.t167RetailPrice ? it?.t167RetailPrice : 0))
            data.put("DISC", new BigDecimal(null != it?.t167Disc ? it?.t167Disc : 0))
            reportData.add(data)
        }
        //data penjualan barang T703
		def listPart = PartInv.createCriteria().list{
            eq("t703StaDel","0")
			invoice {
                eq("t701StaDel","0")
                eq("companyDealer",CompanyDealer.findById(workshop))
				ge("t701TglJamInvoice",tgl)
				lt("t701TglJamInvoice",tgl2 + 1)
                isNull("t701NoInv_Reff")
                isNull("t701StaApprovedReversal")
                gt("t701TotalBayarRp",0.toDouble())
			}
			eq("goods",pGoods)
            invoice{
                order("t701TglJamInvoice")
            }
		}
		listPart.each {
            franc = KlasifikasiGoods.findByGoods(it?.goods)
            if(franc){
                namaFranc = franc?.franc?.m117NamaFranc
            }

            def data = [:]
			data.put("INVOICE_DATE", null != it?.invoice && null != it?.invoice.t701TglJamInvoice ?  it?.invoice.t701TglJamInvoice.format('dd-MMM-yyyy') : null)
			data.put("NO_INVOICE", (null != it?.t703NoPartInv ? it?.invoice.t701NoInv + " / " + it?.t703NoPartInv : ""))
			data.put("QTY_IN", new BigDecimal(0))
			data.put("SATUAN", String.valueOf(null != it?.goods && null != it?.goods.satuan.m118Satuan1 ? it?.goods.satuan.m118Satuan1 : ""))
			data.put("QTY_OUT", new BigDecimal(null != it?.t703Jumlah1 ? it?.t703Jumlah1 : 0))
			data.put("L_C", new BigDecimal(null != it?.t703LandedCost ? String.valueOf(it?.t703LandedCost) : "0"))
			data.put("UNIT_PRICE", new BigDecimal(null != it?.t703HargaRp ? String.valueOf(it?.t703HargaRp)   : "0"))
			data.put("DISC", new BigDecimal(null != it?.t703DiscPersen ? String.valueOf(it?.t703DiscPersen)  : "0"))
			
			reportData.add(data)
		}
        //DeliveryOrder
        def lisDO = SalesOrderDetail.createCriteria().list{
            eq("staDel","0")
            sorNumber{
                eq("companyDealer",CompanyDealer.findById(workshop))
                ge("sorDate",tgl)
                lt("sorDate",tgl2 + 1)
            }
            eq("materialCode",pGoods)
        }
        lisDO.each {
            if(DeliveryOrder.findBySorNumberAndStaDel(it.sorNumber,"0")){
                def data = [:]
                data.put("INVOICE_DATE", null != it?.sorNumber.sorDate && null != it?.sorNumber.sorDate ?  it?.sorNumber.sorDate.format('dd-MMM-yyyy') : null)
                data.put("NO_INVOICE", (null != it?.sorNumber.sorNumber ? it?.sorNumber.sorNumber : ""))
                data.put("QTY_IN", new BigDecimal(0))
                data.put("SATUAN", String.valueOf(null != it?.materialCode.satuan && null != it?.materialCode.satuan.m118Satuan1 ? it?.materialCode.satuan.m118Satuan1 : ""))
                data.put("QTY_OUT", new BigDecimal(null != it?.quantity ? it?.quantity : 0))
                data.put("L_C", new BigDecimal(null != it?.unitPrice ? String.valueOf(it?.unitPrice) : "0"))
                data.put("UNIT_PRICE", new BigDecimal(null != it?.unitPriceBeli ? String.valueOf(it?.unitPriceBeli)   : "0"))
                data.put("DISC", new BigDecimal(null != it?.discount ? String.valueOf(it?.discount)  : "0"))
                reportData.add(data)
            }
        }

        //slipBebas
        def lisBebas = SupplySlipOwnDetail.createCriteria().list{
            eq("staDel","0")
            supplySlipOwn{
                eq("staDel","0")
                eq("staApprove","1")
                eq("companyDealer",CompanyDealer.findById(workshop))
                ge("ssDate",tgl)
                lt("ssDate",tgl2 + 1)
            }
            eq("goods",pGoods)
        }
        lisBebas.each {
            def data = [:]
            data.put("INVOICE_DATE", null != it?.supplySlipOwn.ssDate && null != it.supplySlipOwn.dateCreated ?  it.supplySlipOwn.ssDate.format('dd-MMM-yyyy') : null)
            data.put("NO_INVOICE", (null != it?.supplySlipOwn.ssNumber ? it?.supplySlipOwn.ssNumber : ""))
            data.put("QTY_IN", new BigDecimal(0))
            data.put("SATUAN", String.valueOf(null != it?.goods.satuan && null != it?.goods.satuan.m118Satuan1 ? it?.goods.satuan.m118Satuan1 : ""))
            data.put("QTY_OUT", new BigDecimal(null != it?.qty ? it?.qty : 0))
            data.put("L_C", new BigDecimal(null != it?.hargaSatuan ? String.valueOf(it?.hargaSatuan) : "0"))
            data.put("UNIT_PRICE", new BigDecimal(null != it?.hargaSatuan ? String.valueOf(it?.hargaSatuan)   : "0"))
            data.put("DISC", new BigDecimal(null != it?.discount ? String.valueOf(it?.discount)  : "0"))
            reportData.add(data)
        }
        jasperParams.put("FRANC", namaFranc)
        if(reportData?.size()<1){
            def data = [:]
            data.put("INVOICE_DATE", "-")
            data.put("NO_INVOICE", "-")
            data.put("QTY_IN", new BigDecimal(0))
            data.put("SATUAN", "-")
            data.put("QTY_OUT", new BigDecimal(0))
            data.put("L_C", new BigDecimal(0))
            data.put("UNIT_PRICE", new BigDecimal(0))
            data.put("DISC", new BigDecimal(0))
            reportData.add(data)
        }
		/*
		if (listPart.size() == 0 && listPart1.size() == 0)
			reportData = dataDummy1();
		*/		
		return reportData;
	}
	
	
	
	//================================================================================
	//3. Report Parts Sales
	def trxPartsSales(def params) {
		def reportData = new ArrayList();
        Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
        Date tglAwal = new Date().parse('dd-MM-yyyy',params.tanggal)
        tglAwal.setDate(1)
        int nomor = 0
        def total_label1_beli = 0;def total_label1_jual  = 0
        def total_label2_beli = 0;def total_label2_jual  = 0

            //kemarin
            def listPartSalesKemarin = PartInv.createCriteria().list{
                eq("t703StaDel","0")
                eq("companyDealer",CompanyDealer.findById(params.workshop))
                invoice {
                    ge("t701TglJamInvoice",tglAwal)
                    lt("t701TglJamInvoice",tgl)
                    order("t701TglJamInvoice","asc")
                    eq("t701StaDel","0");
                    isNull("t701NoInv_Reff");
                    isNull("t701StaApprovedReversal")
                    gt("t701TotalBayarRp",0.toDouble())
                }
            }
            if((params.nomor as String).equals("1")){
                listPartSalesKemarin.each {
                    if(!KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc.toUpperCase().contains("PARTS")){
                        def hargaBeli = 0;
                        def tothargaBeli = 0;
                        try {
                            hargaBeli = it.t703LandedCost
                            tothargaBeli = hargaBeli * it.t703Jumlah1
                            total_label2_beli += tothargaBeli
                        }catch (Exception e){}

                        def hargaJual = 0;
                        def tothargaJual = 0;
                        try {
                            hargaJual = it.t703HargaRp
                            tothargaJual = it.t703TotalRp
                            total_label2_jual += tothargaJual
                        }catch (Exception){}

                    }
                }
            }else{
                listPartSalesKemarin.each {
                    if(KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc.toUpperCase().contains("PARTS")){
                        def hargaBeli = 0;
                        def tothargaBeli = 0;
                        try {
                            hargaBeli = it.t703LandedCost
                            tothargaBeli = hargaBeli * it.t703Jumlah1
                            total_label2_beli += tothargaBeli
                        }catch (Exception e){}

                        def hargaJual = 0;
                        def tothargaJual = 0;
                        try {

                            hargaJual = it.t703HargaRp
                            tothargaJual = it.t703TotalRp
                            total_label2_jual += tothargaJual
                        }catch (Exception){}

                    }
                }
            }

            //today
            def listPartSales = PartInv.createCriteria().list{
                eq("t703StaDel","0")
                eq("companyDealer",CompanyDealer.findById(params.workshop))
                invoice {
                    ge("t701TglJamInvoice",tgl)
                    lt("t701TglJamInvoice",tgl2 + 1)
                    order("t701TglJamInvoice","asc")
                    eq("t701StaDel","0");
                    isNull("t701NoInv_Reff");
                    isNull("t701StaApprovedReversal")
                    gt("t701TotalBayarRp",0.toDouble())
                }
            }
            if((params.nomor as String).equals("1")){

                listPartSales.each {
//                    println it?.goods.id
//                    println KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc.toUpperCase()
                    if(!KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc.toUpperCase().contains("PARTS")){
                        nomor++;
                        def hargaBeli = 0;
                        def tothargaBeli = 0;
                        try {
                            hargaBeli = it.t703LandedCost
                            tothargaBeli = hargaBeli * it.t703Jumlah1
                            total_label1_beli += tothargaBeli
                        }catch (Exception e){}

                        def hargaJual = 0;
                        def tothargaJual = 0;
                        try {
                            hargaJual = it.t703HargaRp
                            tothargaJual = it.t703TotalRp
                            total_label1_jual += tothargaJual
                        }catch (Exception){}
                        def data = [:]
                        data.put("NO", nomor)
                        data.put("FRANC", "PENJUALAN BAHAN")
                        data.put("INVOICE_DATE", null != it?.invoice && null != it?.invoice?.t701TglJamInvoice ?  it?.invoice?.t701TglJamInvoice?.format('dd-MMM-yyyy') : null)
                        data.put("NO_INVOICE", (null != it?.invoice?.t701NoInv ? it?.invoice?.t701NoInv : ""))
                        data.put("NOMOR_WO", null != it?.invoice && null != it?.invoice?.reception ?  it?.invoice?.reception?.t401NoWO : "")
                        data.put("KODE_PARTS", String.valueOf(null != it?.goods && null != it?.goods?.m111ID ? it?.goods?.m111ID : ""))
                        data.put("NAMA_PARTS", String.valueOf(null != it?.goods && null != it?.goods?.m111Nama ? it?.goods?.m111Nama : ""))
                        data.put("QTY", it?.t703Jumlah1 ? String.valueOf(it?.t703Jumlah1) : "0")
                        data.put("SATUAN", String.valueOf(null != it?.goods && null != it?.goods.satuan.m118Satuan1 ? it?.goods.satuan.m118Satuan1 : ""))
                        data.put("HARGA_BELI", new Konversi().toRupiah(hargaBeli))
                        data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah(tothargaBeli))
                        data.put("HARGA_JUAL", new Konversi().toRupiah(hargaJual))
                        data.put("TOTAL_HARGA_JUAL", new Konversi().toRupiah(tothargaJual))
                        data.put("KET", it?.t703xKet)
                        def tanggal = tgl.format("dd/MM/yyyy")+ " s.d " + tgl2.format("dd/MM/yyyy")
                        if(tgl.toString().equals(tgl2.toString())){
                            tanggal =  tgl.format("dd/MM/yyyy")
                        }
                        data.put("LABEL1", "JUMLAH PEMAKAIAN TGL " + tanggal)
                        data.put("TOTAL_LABEL1_BELI", new Konversi().toRupiah(total_label1_beli))
                        data.put("TOTAL_LABEL1_JUAL", new Konversi().toRupiah(total_label1_jual))
                        if(tgl.format("dd").toString().equals("01")){
                            data.put("LABEL2","")
                        }else{
                            data.put("LABEL2", "JUMLAH PEMAKAIAN S.D TGL " + (tgl-1).format("dd/MM/yyyy"))
                        }
                        data.put("TOTAL_LABEL2_BELI", new Konversi().toRupiah(total_label2_beli))
                        data.put("TOTAL_LABEL2_JUAL", new Konversi().toRupiah(total_label2_jual))
                        data.put("LABEL3", "JUMLAH PEMAKAIAN S.D TGL " + tgl2.format("dd/MM/yyyy"))
                        data.put("TOTAL_LABEL3_BELI", new Konversi().toRupiah(total_label2_beli+total_label1_beli))
                        data.put("TOTAL_LABEL3_JUAL", new Konversi().toRupiah(total_label2_jual+total_label1_jual))
                        data.put("NAMA_SM", this.getServiceManagerName())
                        data.put("NAMA_ADKU", this.getKaryawanNameByJabatan("KABAG ADKEU"))
                        data.put("NAMA_PEMBUKUAN", this.getKaryawanNameByJabatan("KASIE PEMBUKUAN")? this.getKaryawanNameByJabatan("KASIE PEMBUKUAN"):
                                (this.getKaryawanNameByJabatan("ADM PEMBUKUAN / PSGL")?this.getKaryawanNameByJabatan("ADM PEMBUKUAN / PSGL"):this.getKaryawanNameByJabatan("ADM KEUANGAN")))
                        reportData.add(data)
                    }
                }

            }else{

                listPartSales.each {
//                    println it?.goods.id
//                    println KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc.toUpperCase()
                    if(KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc.toUpperCase().contains("PARTS")){
                        nomor++;
                        def hargaBeli = 0;
                        def tothargaBeli = 0;
                        try {
                            hargaBeli = it.t703LandedCost
                            tothargaBeli = hargaBeli * it.t703Jumlah1
                            total_label1_beli += tothargaBeli
                        }catch (Exception e){}

                        def hargaJual = 0;
                        def tothargaJual = 0;
                        try {
                            hargaJual = it.t703HargaRp
                            tothargaJual = it.t703TotalRp
                            total_label1_jual += tothargaJual
                        }catch (Exception){}

                        def data = [:]
                        data.put("NO", nomor)
                        data.put("FRANC", "PENJUALAN PARTS")
                        data.put("INVOICE_DATE", null != it?.invoice && null != it?.invoice?.t701TglJamInvoice ?  it?.invoice?.t701TglJamInvoice?.format('dd-MMM-yyyy') : null)
                        data.put("NO_INVOICE", (null != it?.invoice?.t701NoInv ? it?.invoice?.t701NoInv : ""))
                        data.put("NOMOR_WO", null != it?.invoice && null != it?.invoice?.reception ?  it?.invoice?.reception?.t401NoWO : "")
                        data.put("KODE_PARTS", String.valueOf(null != it?.goods && null != it?.goods?.m111ID ? it?.goods?.m111ID : ""))
                        data.put("NAMA_PARTS", String.valueOf(null != it?.goods && null != it?.goods?.m111Nama ? it?.goods?.m111Nama : ""))
                        data.put("QTY", it?.t703Jumlah1 ? String.valueOf(it?.t703Jumlah1) : "0")
                        data.put("SATUAN", String.valueOf(null != it?.goods && null != it?.goods.satuan.m118Satuan1 ? it?.goods.satuan.m118Satuan1 : ""))
                        data.put("HARGA_BELI", new Konversi().toRupiah(hargaBeli))
                        data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah(tothargaBeli))
                        data.put("HARGA_JUAL", new Konversi().toRupiah(hargaJual))
                        data.put("TOTAL_HARGA_JUAL", new Konversi().toRupiah(tothargaJual))
                        data.put("KET", it?.t703xKet)
                        def tanggal = tgl.format("dd/MM/yyyy")+ " s.d " + tgl2.format("dd/MM/yyyy")
                        if(tgl.toString().equals(tgl2.toString())){
                            tanggal =  tgl.format("dd/MM/yyyy")
                        }
                        data.put("LABEL1", "JUMLAH PEMAKAIAN TGL " + tanggal)
                        data.put("TOTAL_LABEL1_BELI", new Konversi().toRupiah(total_label1_beli))
                        data.put("TOTAL_LABEL1_JUAL", new Konversi().toRupiah(total_label1_jual))
                        if(tgl.format("dd").toString().equals("01")){
                            data.put("LABEL2","")
                        }else{
                            data.put("LABEL2", "JUMLAH PEMAKAIAN S.D TGL " + (tgl-1).format("dd/MM/yyyy"))
                        }
                        data.put("TOTAL_LABEL2_BELI", new Konversi().toRupiah(total_label2_beli))
                        data.put("TOTAL_LABEL2_JUAL", new Konversi().toRupiah(total_label2_jual))
                        data.put("LABEL3", "JUMLAH PEMAKAIAN TGL S.D TGL " + tgl2.format("dd/MM/yyyy"))
                        data.put("TOTAL_LABEL3_BELI", new Konversi().toRupiah(total_label2_beli+total_label1_beli))
                        data.put("TOTAL_LABEL3_JUAL", new Konversi().toRupiah(total_label2_jual+total_label1_jual))
                        data.put("NAMA_SM", this.getServiceManagerName())
                        data.put("NAMA_ADKU", this.getKaryawanNameByJabatan("KABAG ADKEU"))
                        data.put("NAMA_PEMBUKUAN", this.getKaryawanNameByJabatan("ADM KEUANGAN"))
                        reportData.add(data)
                    }
                }


            if (listPartSales.size() == 0)
                reportData = dataDummy2();
        }
		return reportData;
	}
    //35. Report Parts Sales Tunai
    def trxPartsSalesTunai(def params) {
        def reportData = new ArrayList();
        Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
        Date tglAwal = new Date().parse('dd-MM-yyyy',params.tanggal)
        tglAwal.setDate(1)
        int nomor = 0
        def total_label1_beli = 0;def total_label1_jual  = 0;def total_label1_ppn = 0;def total_label1_total = 0;
        def total_label2_beli = 0;def total_label2_jual  = 0;def total_label2_ppn= 0;def total_label2_total = 0;
            //kemarin
            def listPartSales2kemarin = DeliveryOrder.createCriteria().list{
                eq("staDel","0")
                eq("companyDealer",CompanyDealer.findById(params.workshop))
                sorNumber {
                    eq("companyDealer",CompanyDealer.findById(params.workshop))
                    ge("deliveryDate",tglAwal)
                    lt("deliveryDate",tgl)
                    order("deliveryDate","asc")
                    eq("staDel","0");
                }
            }
            listPartSales2kemarin.each {
                def sorDetail = SalesOrderDetail.findAllBySorNumberAndStaDel(it?.sorNumber,"0")
                sorDetail.each {
                    def hargaBeli = 0;
                    def tothargaBeli = 0;
                    def ppn = 0;
                    def totalSetelahPPn = 0;
                    try {
                        hargaBeli = it.unitPriceBeli
                        if(hargaBeli==0 || hargaBeli==null){
                            hargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.materialCode ,session.userCompanyDealer,'0')?.t150Harga
                        }
                        tothargaBeli = hargaBeli * it.quantity
                        total_label2_beli += tothargaBeli
                    }catch (Exception){}

                    def hargaJual = 0;
                    def tothargaJual = 0;
                    try {
                        hargaJual = it.unitPrice
                        tothargaJual = hargaJual * it.quantity
                        def hargaDisc = it?.discount ? (tothargaJual * it.discount/100) : 0
                        tothargaJual = tothargaJual-hargaDisc
                        total_label2_jual += tothargaJual

                        ppn = tothargaJual * 0.1
                        totalSetelahPPn += (tothargaJual + ppn)
                        total_label2_ppn += ppn
                        total_label2_total += totalSetelahPPn
                    }catch (Exception e){}

                }
            }
            //today
            def listPartSales2 = DeliveryOrder.createCriteria().list{
                eq("staDel","0")
                eq("companyDealer",CompanyDealer.findById(params.workshop))
                sorNumber {
                    eq("companyDealer",CompanyDealer.findById(params.workshop))
                    ge("deliveryDate",tgl)
                    lt("deliveryDate",tgl2 + 1)
                    order("deliveryDate","asc")
                    eq("staDel","0");
                }
            }

            listPartSales2.each {
                def dO = it
                def sorDetail = SalesOrderDetail.findAllBySorNumberAndStaDel(it?.sorNumber,"0")
                sorDetail.each {
                    def franc = "";
                    try {
                        if(!KlasifikasiGoods.findByGoods(it?.materialCode).franc.m117NamaFranc.toUpperCase().contains("PARTS")){
                            franc = " (B)";
                        }
                    }catch (Exception e){

                    }
                    nomor++;
                    def hargaBeli = 0;
                    def tothargaBeli = 0;
                    def ppn = 0;
                    def totalSetelahPPn = 0;
                    try {
                        hargaBeli = it?.unitPriceBeli
                        if(hargaBeli==0 || hargaBeli==null){
                            hargaBeli = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it?.materialCode ,session.userCompanyDealer,'0')?.t150Harga
                        }
                        tothargaBeli = hargaBeli * it.quantity
                        total_label1_beli += tothargaBeli
                    }catch (Exception){}

                    def hargaJual = 0;
                    def tothargaJual = 0;
                    try {
                        hargaJual = it.unitPrice
                        tothargaJual = hargaJual * it.quantity
                        def hargaDisc = it?.discount ? (tothargaJual * it.discount/100) : 0
                        tothargaJual = tothargaJual-hargaDisc
                        total_label1_jual += tothargaJual

                        ppn = tothargaJual * 0.1
                        totalSetelahPPn += (tothargaJual + ppn)
                        total_label1_ppn += ppn
                        total_label1_total += totalSetelahPPn
                    }catch (Exception e){}

                    def data = [:]
                    data.put("NO", nomor)
                    data.put("FRANC", "PENJUALAN TUNAI/LANGSUNG")
                    data.put("INVOICE_DATE", null != it?.sorNumber?.deliveryDate ?  it?.sorNumber?.deliveryDate?.format('dd-MMM-yyyy') : null)
                    data.put("NO_INVOICE", null != dO?  dO?.doNumber : "")
                    data.put("NOMOR_WO", null != it?.sorNumber ?  it?.sorNumber?.sorNumber : "")
                    data.put("KODE_PARTS", String.valueOf(null != it?.materialCode && null != it?.materialCode?.m111ID ? it?.materialCode?.m111ID + " " + franc : ""))
                    data.put("NAMA_PARTS", String.valueOf(null != it?.materialCode && null != it?.materialCode?.m111Nama ? it?.materialCode?.m111Nama : ""))
                    data.put("QTY", it?.quantity ? String.valueOf(it?.quantity) : "0")
                    data.put("SATUAN", String.valueOf(null != it?.materialCode && null != it?.materialCode.satuan.m118Satuan1 ? it?.materialCode.satuan.m118Satuan1 : ""))
                    data.put("HARGA_BELI", new Konversi().toRupiah(hargaBeli))
                    data.put("TOTAL_HARGA_BELI", new Konversi().toRupiah(tothargaBeli))
                    data.put("HARGA_JUAL", new Konversi().toRupiah(hargaJual))
                    data.put("TOTAL_HARGA_JUAL", new Konversi().toRupiah(tothargaJual))
                    data.put("PPN", new Konversi().toRupiah(ppn))
                    data.put("TOTAL_SETELAH_PPN", new Konversi().toRupiah(totalSetelahPPn))
                    data.put("KET", it?.sorNumber?.customerName)
                    def tanggal = tgl.format("dd/MM/yyyy")+ " s.d " + tgl2.format("dd/MM/yyyy")
                    if(tgl.toString().equals(tgl2.toString())){
                        tanggal =  tgl.format("dd/MM/yyyy")
                    }
                    data.put("LABEL1", "JUMLAH PEMAKAIAN TGL " + tanggal)
                    data.put("TOTAL_LABEL1_BELI", new Konversi().toRupiah(total_label1_beli))
                    data.put("TOTAL_LABEL1_JUAL", new Konversi().toRupiah(total_label1_jual))
                    data.put("TOTAL_LABEL1_PPN", new Konversi().toRupiah(total_label1_ppn))
                    data.put("TOTAL_LABEL1_TOTAL", new Konversi().toRupiah(total_label1_total))
                    if(tgl.format("dd").toString().equals("01")){
                        data.put("LABEL2","")
                    }else{
                        data.put("LABEL2", "JUMLAH PEMAKAIAN S.D TGL " + (tgl-1).format("dd/MM/yyyy"))
                    }
                    data.put("TOTAL_LABEL2_BELI", new Konversi().toRupiah(total_label2_beli))
                    data.put("TOTAL_LABEL2_JUAL", new Konversi().toRupiah(total_label2_jual))
                    data.put("TOTAL_LABEL2_PPN", new Konversi().toRupiah(total_label2_ppn))
                    data.put("TOTAL_LABEL2_TOTAL", new Konversi().toRupiah(total_label2_total))
                    data.put("LABEL3", "JUMLAH PEMAKAIAN S.D TGL " + tgl2.format("dd/MM/yyyy"))
                    data.put("TOTAL_LABEL3_BELI", new Konversi().toRupiah(total_label2_beli+total_label1_beli))
                    data.put("TOTAL_LABEL3_JUAL", new Konversi().toRupiah(total_label2_jual+total_label1_jual))
                    data.put("TOTAL_LABEL3_PPN", new Konversi().toRupiah(total_label2_ppn+total_label1_ppn))
                    data.put("TOTAL_LABEL3_TOTAL", new Konversi().toRupiah(total_label2_total+total_label1_total))
                    reportData.add(data)
                }
            }

            if (listPartSales2.size() == 0)
                reportData = dataDummy2();

        return reportData;
    }
	//====================================================================
	//4. Report Parts Receiving
	def partsReceiving(def params) {
		def reportData = new ArrayList();
        Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)

		def listPartsReceiving = GoodsReceiveDetail.createCriteria().list{
            eq("staDel","0")
			goodsReceive {
                eq("companyDealer",CompanyDealer.findById(params.workshop))
				ge("t167TglJamReceive",tgl)
				lt("t167TglJamReceive",tgl2 + 1)
                order("t167TglJamReceive")
                eq("staDel","0")
			}
            goodsReceive{
                vendor{
                    eq("id",params.vendorId)
                }
            }

		}
        def hargaTotal = 0
        int no = 0
		listPartsReceiving.each {
            if(!KlasifikasiGoods?.findByGoods(it?.goods)?.franc?.m117NamaFranc?.toUpperCase()?.contains("PARTS")){
                no++

                def nilaiInv = 0
                def guds = it?.goods
                def data = [:]
                data.put("NO", no)
                data.put("NO_INVOICE", null != it?.goodsReceive && null != it?.goodsReceive?.t167NoReff? it?.goodsReceive?.t167NoReff + " (B)": "")
                data.put("TGL_INVOICE", null != it?.goodsReceive && null != it?.goodsReceive?.t167TglJamReceive ? it?.goodsReceive?.t167TglJamReceive?.format("dd/MM/yyyy") : "")
                data.put("NAMA_VENDOR", null != it?.poDetail?.po.vendor && null != it?.poDetail?.po?.vendor?.m121Nama ? it?.poDetail?.po?.vendor?.m121Nama : "")
                data.put("KODE_PART", null != it?.goods && null != it?.goods.m111ID ? it?.goods.m111ID : "")
                data.put("NAMA_PART", null != it?.goods && null != it?.goods.m111Nama ? it?.goods.m111Nama : "")
                data.put("QTY", new BigDecimal(null != it?.t167Qty1Issued ? it?.t167Qty1Issued : 0))
                def harga  = 0
                try {
                    harga = it?.t167RetailPrice -( it?.t167RetailPrice * (it?.t167Disc/100))
                }catch (Exception e){

                }
                data.put("HARGA_BELI", new Konversi()?.toRupiah(harga))
                data.put("TOTAL",new Konversi()?.toRupiah(harga*it?.t167Qty1Issued))
                hargaTotal += (harga*it?.t167Qty1Issued)
                data.put("TOTAL_BELI", new Konversi()?.toRupiah(hargaTotal))
                reportData.add(data)
            }
		}

        listPartsReceiving.each {
            if(KlasifikasiGoods?.findByGoods(it?.goods)?.franc?.m117NamaFranc?.toUpperCase()?.contains("PARTS")){
                no++
                def nilaiInv = 0
                def guds = it?.goods
                def data = [:]
                data.put("NO", no)
                data.put("NO_INVOICE", null != it?.goodsReceive && null != it?.goodsReceive?.t167NoReff? it?.goodsReceive?.t167NoReff : "")
                data.put("TGL_INVOICE", null != it?.goodsReceive && null != it?.goodsReceive?.t167TglJamReceive ? it?.goodsReceive?.t167TglJamReceive.format("dd/MM/yyyy") : "")
                data.put("NAMA_VENDOR", null != it?.poDetail?.po.vendor && null != it?.poDetail?.po?.vendor?.m121Nama ? it?.poDetail?.po?.vendor?.m121Nama : "")
                data.put("KODE_PART", null != it?.goods && null != it?.goods.m111ID ? it?.goods.m111ID : "")
                data.put("NAMA_PART", null != it?.goods && null != it?.goods.m111Nama ? it?.goods.m111Nama : "")
                data.put("QTY", new BigDecimal(null != it?.t167Qty1Issued ? it?.t167Qty1Issued : 0))
                def harga  = 0
                try {
                    harga = it?.t167RetailPrice -( it?.t167RetailPrice * (it?.t167Disc/100))
                }catch (Exception e){

                }
                data.put("HARGA_BELI", new Konversi()?.toRupiah(harga))
                data.put("TOTAL",new Konversi()?.toRupiah(harga*it?.t167Qty1Issued))
                hargaTotal += (harga*it?.t167Qty1Issued)
                data.put("TOTAL_BELI", new Konversi()?.toRupiah(hargaTotal))
                reportData.add(data)
            }
        }

		if(reportData.size() == 0)
			reportData = dataDummy3();
		return reportData;
	}
	
	//5. Report Parts Return
	def partsReturn(def params, def jasperParams) {
		def reportData = new ArrayList();
		def kodeVendor = (params.kodeVendor != null ? params.kodeVendor : "")
		def workshop = (params.workshop != null ? params.workshop : "")
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
		
		jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd-MM-yyyy'))

		
		def listReturn = Returns.createCriteria().list{
//            eq("companyDealer",CompanyDealer.findById(workshop))
			 ge("t172TglJamReturn",tgl)
			 lt("t172TglJamReturn",tgl2 + 1)
			vendor {
					ilike("m121ID","%"+kodeVendor+"%")
				}

            eq("staDel","0")
		}

//        println("cetak return"+listReturn.size())
//        println("cetak kode vendor"+kodeVendor)
		listReturn.each {
			def data = [:]
			
			data.put("RETURN_DATE", null != it?.t172TglJamReturn ? new SimpleDateFormat("dd-MMM-yyyy").format(it?.t172TglJamReturn) : "")
			data.put("NO_RETURN", null != it?.t172ID ? it?.t172ID : "")
			data.put("FRANC", null != it?.goods && null != KlasifikasiGoods.findByGoods(it?.goods) ?  KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc : "")
			data.put("RECEIVING_DATE", it?.goodsReceive.t167TglJamReceive)
			data.put("NO_INVOICE", it?.invoice.t166NoInv)
			data.put("NAMA_VENDOR", it?.vendor.m121Nama)
			data.put("KODE_PART", it?.goods.m111ID)
			data.put("NAMA_PART", it?.goods.m111Nama)
			data.put("QTY", new BigDecimal(it?.t172Qty1Return))
			data.put("SATUAN", it?.goods.satuan.m118Satuan1)
			data.put("L_C", new BigDecimal(0))//???
			
			reportData.add(data)
		}
		reportData.sort{it.NO_RETURN};
		if (listReturn.size() == 0)
			reportData = dataDummy4();
		return reportData;
	}
	
	//6. Report Parts On Hands Adjustment
	def partsOnHandsAdjustment(def param, def jasperParams) {
		def reportData = new ArrayList();
		def kodeOH = (params.kodeOH != null ? params.kodeOH : "")		
		def workshop = (params.workshop != null ? params.workshop : "")
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
		
		jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd MMMM yyyy')+" s/d "+tgl2.format('dd MMMM yyyy'))
		
		def listAdj = PartsAdjDetail.createCriteria().list{
			 partsAdjust {
				 ge("t145TglAdj",tgl)
				 lt("t145TglAdj",tgl2 + 1)
				 ilike("t145ID","%"+kodeOH+"%")
                 eq("companyDealer", CompanyDealer.findById(params.workshop as long))
			}
		}

        double totLc = 0, selisih = 0
		listAdj.each {
			def klasifikasiGoods =  KlasifikasiGoods.findByGoods(it?.goods);
            def lcost = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",session?.userCompanyDealer)

            selisih = (it?.t146JmlAkhir1 ? it?.t146JmlAkhir1 : 0) - (it?.t146JmlAwal1 ? it?.t146JmlAwal1 : 0)
            totLc = (lcost?.t131LandedCost ? lcost?.t131LandedCost : 0) * selisih
		
			def data = [:]			
			data.put("OH_DATE", it?.partsAdjust?.t145TglAdj ? it?.partsAdjust?.t145TglAdj?.format('dd-MMM-yyyy') : "-")
			data.put("NO_OH", it?.partsAdjust?.t145ID ? it?.partsAdjust?.t145ID : "-")
			data.put("KODE_PART", it?.goods?.m111ID ? it?.goods?.m111ID : "-")
			data.put("NAMA_PART", it?.goods?.m111Nama ? it?.goods?.m111Nama : "-")
			data.put("FRANC", klasifikasiGoods?.franc?.m117NamaFranc ? klasifikasiGoods?.franc?.m117NamaFranc : "-")
			data.put("L_C", lcost?.t131LandedCost ? lcost?.t131LandedCost : "-")
			data.put("QTY_TERCATAT", it?.t146JmlAwal1 ? it?.t146JmlAwal1 : 0)
			data.put("QTY_TERCATAT_SATUAN", it?.goods?.satuan?.m118Satuan1 ? it?.goods?.satuan?.m118Satuan1 : "-")
			data.put("QTY_REAL", it?.t146JmlAkhir1 ? it?.t146JmlAkhir1 : "-")
			data.put("QTY_REAL_SATUAN", it?.goods?.satuan?.m118Satuan1 ? it?.goods?.satuan?.m118Satuan1 : "-")
            data.put("QTY_SELISIH", selisih)
			data.put("QTY_SELISIH_SATUAN", it?.goods?.satuan?.m118Satuan1 ? it?.goods?.satuan?.m118Satuan1 : "-")
			data.put("ALASAN", it?.alasanAdjusment?.m165AlasanAdjusment ? it?.alasanAdjusment?.m165AlasanAdjusment : "-")
            data.put("TOTAL_LC", totLc)
			reportData.add(data)
		}
		
		if(listAdj.size() == 0){
            reportData = dataDummy5();
        }
		return reportData;
	}
	
	//7. Report Part Disposal
	def partsDisposal(def param, def jasperParams) {
		def reportData = new ArrayList();
		def noDisposal = (params.noDisposal != null ? params.noDisposal : "")
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
		def workshop = (params.workshop != null ? params.workshop : "")
		
		jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd MMMM yyyy')+" - "+tgl2.format('dd MMMM yyyy'))
		
		def listPartsDisposal = PartsDisposalDetail.createCriteria().list{
			partsDisposal {
				ge("t147TglDisp",tgl)
				lt("t147TglDisp",tgl2 + 1)
				ilike("t147ID","%"+noDisposal+"%")
                eq("companyDealer", CompanyDealer.findById(params.workshop as long))
			}
		}
        double totlc = 0
        if(listPartsDisposal.size() > 0){
            listPartsDisposal.each {
                def lcost = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",session?.userCompanyDealer)
                def namaPart = KlasifikasiGoods.findByGoods(it?.goods)
                totlc = (it?.t148Jumlah1 ? it?.t148Jumlah1 : 0) * (lcost?.t131LandedCost ? lcost?.t131LandedCost : 0)
                def data = [:]

                data.put("DISPOSAL_DATE", it?.partsDisposal?.t147TglDisp ? it?.partsDisposal?.t147TglDisp?.format('dd-MMM-yyyy') : "-")
                data.put("NO_DISPOSAL", it?.partsDisposal?.t147ID ? it?.partsDisposal?.t147ID  : "-")
                data.put("KODE_PART", it?.goods?.m111ID ? it?.goods?.m111ID : "-")
                data.put("NAMA_PART", it?.goods?.m111Nama ? it?.goods?.m111Nama : "-")
                data.put("FRANC", namaPart?.franc?.m117NamaFranc ? namaPart?.franc?.m117NamaFranc  : "-")
                data.put("QTY", it?.t148Jumlah1 ? it?.t148Jumlah1 : 0)
                data.put("SATUAN", it?.goods?.satuan?.m118Satuan1 ? it?.goods?.satuan?.m118Satuan1 : "-")
                data.put("L_C", lcost?.t131LandedCost ? lcost?.t131LandedCost : "-")
                data.put("TOTAL_LC", totlc)
                reportData.add(data)
            }
        }else{
            def data = [:]
            data.put("DISPOSAL_DATE", "-")
            data.put("NO_DISPOSAL", "-")
            data.put("KODE_PART", "-")
            data.put("NAMA_PART", "-")
            data.put("FRANC", "-")
            data.put("QTY", "-")
            data.put("SATUAN", "-")
            data.put("L_C", "-")
            data.put("TOTAL_LC", totlc)
            reportData.add(data)
        }
		return reportData;
	}
	
	//8. Report Stock Report
	def stockReport(def param, def jasperParams) {
		def reportData = new ArrayList();
		
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
		def workshop = (params.workshop != null ? params.workshop : "")
		
		jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd-MM-yyyy'))
		
		def listStock = PartsStok.createCriteria().list{
			ge("t131Tanggal",tgl)
			lt("t131Tanggal",tgl2 + 1)	
			eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)
		}
		
		listStock.each {
			def data = [:]
			data.put("KODE_PART", null != it?.goods.m111ID ? it?.goods.m111ID : "")
			data.put("NAMA_PART", null != it?.goods && null != it?.goods.m111Nama ? it?.goods.m111Nama: "")
			data.put("L_C",new BigDecimal(null != it?.t131LandedCost ? it?.t131LandedCost : 0))
			data.put("LOCATION", null != it?.goods && null != KlasifikasiGoods.findByGoods(it?.goods) ? KlasifikasiGoods.findByGoods(it?.goods).location.m120NamaLocation : "")
			data.put("OH_QTY",new BigDecimal(null != it?.t131Qty1Free ? it?.t131Qty1Free : 0))
			data.put("OH_SATUAN", null != it?.goods && null != it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("RESERVED_QTY",new BigDecimal(null != it?.t131Qty1Reserved ? it?.t131Qty1Reserved : 0))
			data.put("RESERVED_SATUAN", null != it?.goods && null != it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("WIP_QTY",new BigDecimal(null != it?.t131Qty1WIP ? it?.t131Qty1WIP : 0))
			data.put("WIP_SATUAN",null != it?.goods && null != it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("BLOCKED_STOCK_QTY",new BigDecimal(null != it?.t131Qty1BlockStock ? it?.t131Qty1BlockStock : 0))
			data.put("BLOCKED_STOCK_SATUAN", null != it?.goods && null != it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("TOTAL_QTY",new BigDecimal(null != it?.t131Qty1 ? it?.t131Qty1 : 0))
			data.put("TOTAL_SATUAN", null != it?.goods && null != it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("PRICE_UNIT",new BigDecimal(null != GoodsHargaJual.findByGoods(it?.goods) ? GoodsHargaJual.findByGoods(it?.goods).t151HargaDenganPPN : 0))
			data.put("FRANC", null != KlasifikasiGoods.findByGoods(it?.goods) ?  KlasifikasiGoods.findByGoods(it?.goods).franc.m117NamaFranc : "")
			reportData.add(data)
		}
		reportData.sort{it.FRANC};
		
		if (listStock.size() == 0)
			reportData = dataDummy7()
		return reportData;
	}
	
	//9. Report Transaksi Parts Per Vendor
	def transaksiPerVendor(def param, def jasperParams) {
		def reportData = new ArrayList();
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
		
		def workshop = (params.workshop != null ? params.workshop : "")
		def kodeVendor = (params.kodeVendor != null ? params.kodeVendor : "")

		jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd-MM-yyyy'))
		jasperParams.put("KODE_VENDOR",kodeVendor)
		
		def listClaim = Claim.createCriteria().list{
			ge("t171TglJamClaim",tgl)
			lt("t171TglJamClaim",tgl2 + 1)
			vendor {
				ilike("m121ID","%"+kodeVendor+"%")
			}
			
		}
		listClaim.each {
			def data = [:]
			data.put("DATE", null != it?.t171TglJamClaim ? it?.t171TglJamClaim.format('dd-MMM-yyyy') : "")
			data.put("NO_REFF", null != it?.goodsReceive ?  it?.goodsReceive.t167NoReff : "")
			data.put("JENIS_TRANSAKSI", "CLAIM")
			data.put("KODE_PART", null != it?.goods ? it?.goods.m111ID : "")		
			data.put("NAMA_PART", null != it?.goods ? it?.goods.m111Nama : "")			
			data.put("FRANC", null != KlasifikasiGoods.findByGoods(it?.goods) ? KlasifikasiGoods.findByGoods(it?.goods).m117NamaFranc : "")
			data.put("QTY", new BigDecimal(null != it?.t171Qty1Reff ? it?.t171Qty1Reff : 0))
			data.put("SATUAN", null != it?.goods && null != it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("L_C", new BigDecimal(null != GoodsReceiveDetail.findByGoods(it?.goods) ?  GoodsReceiveDetail.findByGoods(it?.goods).t167LandedCost : 0) )
			data.put("HARGA_BELI", new BigDecimal(null != GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",CompanyDealer.findById(workshop)) ?  GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",CompanyDealer.findById(workshop)).t150Harga : 0))
			data.put("NILAI_INVOICE", new BigDecimal(null != GoodsReceiveDetail.findByGoods(it?.goods) ?  GoodsReceiveDetail.findByGoods(it?.goods).t167RetailPrice : 0))
			
			reportData.add(data)
		}
		
		def listReceive = GoodsReceiveDetail.createCriteria().list {
				
			goodsReceive {
				ge("t167TglJamReceive",tgl)
				lt("t167TglJamReceive",tgl2 + 1)	
				vendor {
					eq("m121ID",kodeVendor)
				}
			}	
		}
		listReceive.each {
			def data = [:]
			data.put("DATE", null != it?.goodsReceive ? it?.goodsReceive.t167TglJamReceive.format('dd-MMM-yyyy') : "")
			data.put("NO_REFF", null != it?.goodsReceive ? it?.goodsReceive.t167NoReff : "")
			data.put("JENIS_TRANSAKSI","RECEIVE")
			data.put("KODE_PART", null != it?.goods ? it?.goods.m111ID : "")
			data.put("NAMA_PART", null != it?.goods ? it?.goods.m111Nama : "")
			data.put("FRANC", null != KlasifikasiGoods.findByGoods(it?.goods) ? KlasifikasiGoods.findByGoods(it?.goods).m117NamaFranc : "")
			data.put("QTY", new BigDecimal(null != it?.t167Qty1Reff ? it?.t167Qty1Reff : 0) )
			data.put("SATUAN", null != it?.goods && null != it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("L_C", new BigDecimal(null != it?.t167LandedCost ? it?.t167LandedCost : 0) )
			data.put("HARGA_BELI", new BigDecimal(null != GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",CompanyDealer.findById(workshop)) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",CompanyDealer.findById(workshop)).t150Harga : 0) )
			data.put("NILAI_INVOICE", new BigDecimal(null != it?.goodsReceive && null != it?.goodsReceive.goodsReceiveDetail ? it?.goodsReceive.goodsReceiveDetail.t167RetailPrice : 0) )
			
			reportData.add(data)
		}
		
		def listReturn = Returns.createCriteria().list{
			ge("t172TglJamReturn",tgl)
			lt("t172TglJamReturn",tgl2 + 1)		
			vendor {
				eq("m121ID",kodeVendor)
			}
			
		}
		listReturn.each {
			def data = [:]
			data.put("DATE", null != it?.t172TglJamReturn ? it?.t172TglJamReturn.format('dd-MMM-yyyy') : "")
			data.put("NO_REFF", null != it?.goodsReceive ? it?.goodsReceive.t167NoReff : "")
			data.put("JENIS_TRANSAKSI","RETURN")
			data.put("KODE_PART", null != it?.goods ? it?.goods.m111ID : "")
			data.put("NAMA_PART", null != it?.goods ? it?.goods.m111Nama : "")
			data.put("FRANC", null != KlasifikasiGoods.findByGoods(it?.goods) ? KlasifikasiGoods.findByGoods(it?.goods).m117NamaFranc : "")
			data.put("QTY",new BigDecimal( null != it?.goodsReceive && null != it?.goodsReceive.goodsReceiveDetail ? it?.goodsReceive.goodsReceiveDetail.t167Qty1Reff : 0))
			data.put("SATUAN", null !=it?.goods && null !=it?.goods.satuan ? it?.goods.satuan.m118Satuan1 : "")
			data.put("L_C",new BigDecimal( null !=it?.goodsReceive && null !=it?.goodsReceive.goodsReceiveDetail ? it?.goodsReceive.goodsReceiveDetail.t167LandedCost : 0))
			data.put("HARGA_BELI",new BigDecimal(null != GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",CompanyDealer.findById(workshop)) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it?.goods,"0",CompanyDealer.findById(workshop)).t150Harga : 0))
			data.put("NILAI_INVOICE",new BigDecimal(null != it?.goodsReceive && null != it?.goodsReceive.goodsReceiveDetail ? it?.goodsReceive.goodsReceiveDetail.t167RetailPrice : 0))
			
			reportData.add(data)
		}
		if ((listClaim.size() == 0) && (listReturn.size() == 0) && (listReceive.size() == 0)) 
			reportData = dataDummy8();
		return reportData;			
	}
	
	//10 Report Data of Parts Transaction
	def reportDataOfPartsTrx(def params, def jasperParams) {
		def reportData = new ArrayList();
		Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
        Date tgl2 = new Date().parse('dd-MM-yyyy',params.tanggal2)
		
		def workshop = (params.workshop != null ? params.workshop : "")
		
		jasperParams.put("WORKSHOP",CompanyDealer.findById(workshop).m011NamaWorkshop)
		jasperParams.put("PERIODE",tgl.format('dd-MM-yyyy'))
		
		def listGoodsReceiveDetail = GoodsReceiveDetail.createCriteria().list {
			goodsReceive {
				ge("t167TglJamReceive",tgl)
				lt("t167TglJamReceive",tgl2 + 1)	
			}
		}
		
		def listStockIn = StockINDetail.createCriteria().list {
			stockIN {
				ge("t169TglJamStockIn",tgl)
				lt("t169TglJamStockIn",tgl2 + 1)
			}
		}
		def listClaim = Claim.createCriteria().list{
			ge("t171TglJamClaim",tgl)
			lt("t171TglJamClaim",tgl2 + 1)
		}
		
		def listReturn = Returns.createCriteria().list{
			ge("t172TglJamReturn",tgl)
			lt("t172TglJamReturn",tgl2 + 1)
		}
		def listAdj = PartsAdjDetail.createCriteria().list{
			partsAdjust {
				ge("t145TglAdj",tgl)
				lt("t145TglAdj",tgl2 + 1)	
			}
		}
		
		def listPart = PartInv.createCriteria().list{
			invoice {
				ge("t701TglJamInvoice",tgl)
				lt("t701TglJamInvoice",tgl2 + 1)
                eq("t701StaDel","0");
                isNull("t701NoInv_Reff");
                isNull("t701StaApprovedReversal")
                gt("t701TotalBayarRp",0.toDouble())
            }
		}
		
		listGoodsReceiveDetail.each {
			def data = [:]
			data.put("DATE", null != it?.goodsReceive ? it?.goodsReceive.t167TglJamReceive.format('dd-MMM-yyyy') : "")
			data.put("TIME", null != it?.goodsReceive ? it?.goodsReceive.t167TglJamReceive.format('HH:mm:ss') : "")
			data.put("NAMA_PROCESS","Receiving")
			data.put("NOREFF1", null != it?.goodsReceive ? it?.goodsReceive.t167NoReff : "")
			data.put("NOREFF2","")
			data.put("TIPE_JOB","")
			data.put("VENDOR", null != it?.vendor ? it?.vendor.m121Nama : "")
			data.put("NO_PART_SLIP","")
			data.put("KODE_PART", null != it?.goods ? it?.goods.m111ID : "")
			data.put("NAMA_PART",it?.goods.m111_Nama)
			data.put("QTY",new BigDecimal(it?.t167Qty1Issued))
			data.put("SATUAN",it?.goods.satuan.m118Satuan1)
			data.put("L_C",new BigDecimal(it?.t167LandedCost))
			data.put("HARGA_JUAL",new BigDecimal(0))
			data.put("DISC_AMOUNT",new BigDecimal(0))
			reportData.add(data)
		}
		
		listStockIn.each {
			def data = [:]
			data.put("DATE",new Date(it?.stockIN.t169TglJamStockIn).format('dd-MMM-yyyy'))
			data.put("TIME",new Date(it?.stockIN.t169TglJamStockIn).format('HH:mm:ss'))
			data.put("NAMA_PROCESS","Stock In")
			data.put("NOREFF1",it?.stockIN.t169ID)
			data.put("NOREFF2","")
			data.put("TIPE_JOB","")
			data.put("VENDOR","")
			data.put("NO_PART_SLIP","")
			data.put("KODE_PART",it?.binningDetail.goodsReceiveDetail.goods.m111ID)
			data.put("NAMA_PART",it?.binningDetail.goodsReceiveDetail.goods.m111_Nama)
			data.put("QTY",new BigDecimal(it?.t169Qty1StockIn))
			data.put("SATUAN",it?.binningDetail.goodsReceiveDetail.goods.satuan.m118Satuan1)
			data.put("L_C",new BigDecimal(it?.binningDetail.goodsReceiveDetail.t167LandedCost))
			data.put("HARGA_JUAL",new BigDecimal(0))
			data.put("DISC_AMOUNT",new BigDecimal(0))
			reportData.add(data)
		}
		
		listClaim.each {
			def data = [:]
			data.put("DATE",new Date(it?.stockIN.t171TglJamClaim).format('dd-MMM-yyyy'))
			data.put("TIME",new Date(it?.stockIN.t171TglJamClaim).format('HH:mm:ss'))
			data.put("NAMA_PROCESS","Claim")
			data.put("NOREFF1",it?.t171ID)
			data.put("NOREFF2","")
			data.put("TIPE_JOB","")
			data.put("VENDOR",it?.vendor.m121Nama)
			data.put("NO_PART_SLIP","")
			data.put("KODE_PART",it?.goods.m111ID)
			data.put("NAMA_PART",it?.goods.m111_Nama)
			data.put("QTY",new BigDecimal(it?.t171Qty1Issued))
			data.put("SATUAN",it?.goods.satuan.m118Satuan1)
			data.put("L_C",new BigDecimal(GoodsReceiveDetail.findByGoodsReceive(it?.goodsReceive).t167LandedCost))
			data.put("HARGA_JUAL",new BigDecimal(0))
			data.put("DISC_AMOUNT",new BigDecimal(0))
			reportData.add(data)
		}
		
		listReturn.each {
			def data = [:]
			data.put("DATE",new Date(it?.t172TglJamReturn).format('dd-MMM-yyyy'))
			data.put("TIME",new Date(it?.t172TglJamReturn).format('HH:mm:ss'))
			data.put("NAMA_PROCESS","Return")
			data.put("NOREFF1",it?.t172ID)
			data.put("NOREFF2","")
			data.put("TIPE_JOB","")
			data.put("VENDOR",it?.vendor.m121Nama)
			data.put("NO_PART_SLIP","")
			data.put("KODE_PART",it?.goods.m111ID)
			data.put("NAMA_PART",it?.goods.m111_Nama)
			data.put("QTY",new BigDecimal(it?.t172Qty1Return))
			data.put("SATUAN",it?.goods.satuan.m118Satuan1)
			data.put("L_C",new BigDecimal(GoodsReceiveDetail.findByGoodsReceive(it?.goodsReceive).t167LandedCost))
			data.put("HARGA_JUAL",new BigDecimal(0))
			data.put("DISC_AMOUNT",new BigDecimal(0))
			reportData.add(data)
		}
		
		listAdj.each {
			def data = [:]
			data.put("DATE",new Date(it?.partsAdjust.t145TglAdj).format('dd-MMM-yyyy'))
			data.put("TIME",new Date(it?.partsAdjust.t145TglAdj).format('HH:mm:ss'))
			data.put("NAMA_PROCESS","Adjustment")
			data.put("NOREFF1",it?.t145ID)
			data.put("NOREFF2","")
			data.put("TIPE_JOB","")
			data.put("VENDOR","")
			data.put("NO_PART_SLIP","")
			data.put("KODE_PART",it?.goods.m111ID)
			data.put("NAMA_PART",it?.goods.m111_Nama)
			data.put("QTY",new BigDecimal(it?.t146JmlAwal1))
			data.put("SATUAN",it?.goods.satuan.m118Satuan1)
			data.put("L_C",new BigDecimal(0))
			data.put("HARGA_JUAL",new BigDecimal(0))
			data.put("DISC_AMOUNT",new BigDecimal(0))
			reportData.add(data)
		}
		
		listPart.each {
			def data = [:]
			data.put("DATE",new Date(it?.invoice.t701TglJamInvoice).format('dd-MMM-yy'))
			data.put("TIME",new Date(it?.invoice.t701TglJamInvoice).format('HH:mm:ss'))
			data.put("NAMA_PROCESS","Goods Issue")
			data.put("NOREFF1",it?.invoice.reception.t401NoWO)
			data.put("NOREFF2",it?.invoice.t701NoInv)
			data.put("TIPE_JOB",it?.invoice.reception.operation.kategoriJob.m055KategoriJob)
			data.put("VENDOR","")
			data.put("NO_PART_SLIP",it?.t703NoPartInv)
			data.put("KODE_PART",it?.goods.m111ID)
			data.put("NAMA_PART",it?.goods.m111_Nama)
			data.put("QTY",new BigDecimal(it?.t703Jumlah1))
			data.put("SATUAN",it?.goods.satuan.m118Satuan1)
			data.put("L_C",new BigDecimal(it?.t703_LandedCost))
			data.put("HARGA_JUAL",new BigDecimal(it?.t703HargaRp))
			data.put("DISC_AMOUNT",new BigDecimal(it?.t703DiscRp))
			reportData.add(data)
		}
		if (listPart.size() == 0 && listAdj.size() == 0)
		reportData = dataDummy9();
		return reportData;
	}
	
	//test
	def satuanTest(def param, def jasperParams) {
		def listSatuan = Goods.createCriteria().list{
			fetchMode 'satuan', FetchMode.JOIN
			
		}
		return listSatuan;
	}
	
	def tableModelData(def params){
		def String[] columnNames = ["static_text"];
		def String[][] data = [["light"]];
		tableModel = new DefaultTableModel(data, columnNames);
	}

    def getServiceManagerName(){
        def manPower = ManPower.findByStaDelAndM014JabatanManPowerIlikeOrM014InisialIlike("0","KEPALA BENGKEL","KBNG");
        def manPowerDetail = ManPowerDetail.findByManPowerAndStaDel(manPower, "0");
        def namaManPower = NamaManPower.findByCompanyDealerAndStaDelAndManPowerDetailAndT015StaAktifAndT015StatusManPower(session?.userCompanyDealer, "0", manPowerDetail, "1", "1");
        return namaManPower ? namaManPower?.t015NamaLengkap : "";
    }

    def getKaryawanNameByJabatan(String jabatan){
        def manPower = ManPower.findByStaDelAndM014JabatanManPowerIlike("0", jabatan);
        def karyawan = Karyawan.findByBranchAndJabatanAndStaDel(session?.userCompanyDealer, manPower, "0")
        return karyawan ? karyawan?.nama : "";
    }
}
