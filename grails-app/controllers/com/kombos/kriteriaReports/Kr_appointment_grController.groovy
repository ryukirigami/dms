package com.kombos.kriteriaReports

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FullModelVinCode
import com.kombos.administrasi.Operation
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.JobApp
import com.kombos.reception.Appointment
import com.kombos.reception.CustomerIn
import com.kombos.reception.Reception
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class Kr_appointment_grController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]


    def index() {}

    def previewData(){
        List<JasperReportDef> reportDefList = []
        def file = null
        def reportData = calculateReportData(params)
        def reportData2 = calculateReportDataDetail(params)

        if(params.namaReport=="01"){
            def reportDef = new JasperReportDef(name:'KR_appointmentGR.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("KPI_Appointment_Procces_",".pdf")
        }
        else if(params.namaReport=="03"){
            def reportDef = new JasperReportDef(name:'KR_appointmentGR3.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)

            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KR_appointmentGR2.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData2
                )
                reportDefList.add(reportDef2)

            }

            file = File.createTempFile("KPI_Appointment_NoShow_",".pdf")
        }
        else if(params.namaReport=="02" || params.namaReport=="04"){
            def reportDef = new JasperReportDef(name:'KR_appointmentGR1.jasper',
                     fileFormat:JasperExportFormat.PDF_FORMAT,
                     reportData: reportData
            )
            reportDefList.add(reportDef)

            if(params.detail=="1"){
                def reportDef2 = new JasperReportDef(name:'KR_appointmentGR2.jasper',
                     fileFormat:JasperExportFormat.PDF_FORMAT,
                     reportData: reportData2
               )
               reportDefList.add(reportDef2)
            }
            file = File.createTempFile("KPI_AppointmentGR_",".pdf")
        }

        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }

    def calculateReportData(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        if(params.namaReport=="01"){
            def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
            def parent = ["Customer Yang Appointment","Customer No Show","Customer Yang Reschedule"]
            def child1 = [ "- Jumlah Customer Appointment Show","- Jumlah Customer Yang Batal Appointment","- Jumlah Customer Appointment Yang Reschedule"]
            def child2 = [ "- Jumlah Total Unit Entry Bengkel","- Jumlah Customer Yg Appointment","- Jumlah Customer Yg Appointment"]
            int count = 0
            def jan = [[]]
            def feb = [[]]
            def mar = [[]]
            def apr = [[]]
            def may = [[]]
            def jun = [[]]
            def jul = [[]]
            def aug = [[]]
            def sept = [[]]
            def okt = [[]]
            def nov = [[]]
            def dec = [[]]
            def total = [[]]

            for(int i=1;i<=12;i++){
                if(i>=(params.bulan1 as int) && i <=(params.bulan2 as int)){
                    Calendar cal = Calendar.getInstance()
                    cal.set(params.tahun as int,(i as int) - 1,1 )
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

                    String bulanParam =  params.tahun + "-" + i + "-1"
                    String bulanParam2 =  params.tahun + "-" + i + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)

                    Date tgl = new Date().parse('yyyy-MM-d',bulanParam.toString())
                    Date tgl2 = new Date().parse('yyyy-MM-d',bulanParam2.toString())

                    def listCustomerAppShow = Appointment.createCriteria().list {
                        ge("t301TglJamApp",tgl)
                        lt("t301TglJamApp",tgl2 + 1)
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                        reception{
                            eq("t401StaApp","1")
                        }
                        jenisApp{
                            eq("m301NamaJenisApp","GR")
                        }
                    }

                    def listUnitEntry = Reception.createCriteria().list {
                        ge("t401TanggalWO",tgl)
                        lt("t401TanggalWO",tgl2 + 1)
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                        eq("staSave","0")
                        eq("staDel","0");
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","GR")
                            }
                        }
                    }

                    def listCustomerBatal = Appointment.createCriteria().list {
                        ge("t301TglJamApp",tgl)
                        lt("t301TglJamApp",tgl2 + 1)
                        eq("t301StaOkCancelReSchedule","1")
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))

                        jenisApp{
                            eq("m301NamaJenisApp","GR")
                        }
                    }

                    def listCustomerRes = Appointment.createCriteria().list {
                        ge("t301TglJamApp",tgl)
                        lt("t301TglJamApp",tgl2 + 1)
                        eq("t301StaOkCancelReSchedule","2")
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                        jenisApp{
                            eq("m301NamaJenisApp","GR")
                        }
                    }

                    def listCustomerApp = Appointment.createCriteria().list {
                        ge("t301TglJamApp",tgl)
                        lt("t301TglJamApp",tgl2 + 1)
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                        jenisApp{
                            eq("m301NamaJenisApp","GR")
                        }
                    }

                    if(i==1){
                        jan = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==2){
                        feb = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==3){
                        mar = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==4){
                        apr = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==5){
                        may = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==6){
                        jun = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==7){
                        jul = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==8){
                        aug = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==9){
                        sept = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==10){
                        okt = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==11){
                        nov = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }else if(i==12){
                        dec = [[listCustomerAppShow.size(),listUnitEntry.size()],[listCustomerBatal.size(),listCustomerApp.size()],[listCustomerRes.size(),listCustomerApp.size()]]
                    }
                }
            }


            (0..2).each{
                int a = 0, b = 0
                def data = [:]
                count += 1
                def periode = listBulan.get((params.bulan1 as int)  - 1 ) +" "+ params.tahun +" - "+ listBulan.get((params.bulan2 as int) -1)+" "+ params.tahun
                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("kategori", "GENERAL REPAIR")
                data.put("judul", "GR - CUSTOMER APPOINTMENT")
                data.put("count", count)
                data.put("parent", parent.get(it))
                data.put("child1", child1.get(it))
                data.put("child2", child2.get(it))
                if(1>=(params.bulan1 as int) && 1 <=(params.bulan2 as int)){
                    try {
                        data.put("jan1", Math.round(((jan.get(it).get(0)/jan.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("jan2", jan.get(it).get(0))
                        data.put("jan3", jan.get(it).get(1))
                        a += jan.get(it).get(0)
                        b += jan.get(it).get(1)
                    }catch (Exception e){
                        data.put("jan1", "-")
                        data.put("jan2", "-")
                        data.put("jan3", "-")
                    }

                }else{
                    data.put("jan1", "-")
                    data.put("jan2", "-")
                    data.put("jan3", "-")
                }
                if(2>=(params.bulan1 as int) && 2 <=(params.bulan2 as int)){
                    try {
                        data.put("feb1", Math.round(((feb.get(it).get(0)/feb.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("feb2", feb.get(it).get(0))
                        data.put("feb3", feb.get(it).get(1))
                        a += feb.get(it).get(0)
                        b += feb.get(it).get(1)
                    }catch (Exception e){
                        data.put("feb1", "-")
                        data.put("feb2", "-")
                        data.put("feb3", "-")
                    }
                }else{
                    data.put("feb1", "-")
                    data.put("feb2", "-")
                    data.put("feb3", "-")
                }
                if(3>=(params.bulan1 as int) && 3  <=(params.bulan2 as int)){
                    try {
                        data.put("mar1", Math.round(((mar.get(it).get(0)/mar.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("mar2", mar.get(it).get(0))
                        data.put("mar3", mar.get(it).get(1))
                        a += mar.get(it).get(0)
                        b += mar.get(it).get(1)
                    }catch (Exception e){
                        data.put("mar1", "-")
                        data.put("mar2", "-")
                        data.put("mar3", "-")
                    }
                }else{
                    data.put("mar1", "-")
                    data.put("mar2", "-")
                    data.put("mar3", "-")
                }
                if(4>=(params.bulan1 as int) && 4 <=(params.bulan2 as int)){
                    try {
                        data.put("apr1", Math.round(((apr.get(it).get(0)/apr.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("apr2", apr.get(it).get(0))
                        data.put("apr3", apr.get(it).get(1))
                        a += apr.get(it).get(0)
                        b += apr.get(it).get(1)
                    }catch (Exception e){
                        data.put("apr1", "-")
                        data.put("apr2", "-")
                        data.put("apr3", "-")
                    }
                }else{
                    data.put("apr1", "-")
                    data.put("apr2", "-")
                    data.put("apr3", "-")
                }
                if(5>=(params.bulan1 as int) && 5 <=(params.bulan2 as int)){
                    try {
                        data.put("may1", Math.round(((may.get(it).get(0)/may.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("may2", may.get(it).get(0))
                        data.put("may3", may.get(it).get(1))
                        a += may.get(it).get(0)
                        b += may.get(it).get(1)
                    }catch (Exception e){
                        data.put("may1", "-")
                        data.put("may2", "-")
                        data.put("may3", "-")
                    }
                }else{
                    data.put("may1", "-")
                    data.put("may2", "-")
                    data.put("may3", "-")
                }
                if(6>=(params.bulan1 as int) && 6 <=(params.bulan2 as int)){
                    try {
                        data.put("jun1", Math.round(((jun.get(it).get(0)/jun.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("jun2", jun.get(it).get(0))
                        data.put("jun3", jun.get(it).get(1))
                        a += jun.get(it).get(0)
                        b += jun.get(it).get(1)
                    }catch (Exception e){
                        data.put("jun1", "-")
                        data.put("jun2", "-")
                        data.put("jun3", "-")
                    }
                }else{
                    data.put("jun1", "-")
                    data.put("jun2", "-")
                    data.put("jun3", "-")
                }
                if(7>=(params.bulan1 as int) && 7 <=(params.bulan2 as int)){
                    try {
                        data.put("jul1", Math.round(((jul.get(it).get(0)/jul.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("jul2", jul.get(it).get(0))
                        data.put("jul3", jul.get(it).get(1))
                        a += jul.get(it).get(0)
                        b += jul.get(it).get(1)
                    }catch (Exception e){
                        data.put("jul1", "-")
                        data.put("jul2", "-")
                        data.put("jul3", "-")
                    }
                }else{
                    data.put("jul1", "-")
                    data.put("jul2", "-")
                    data.put("jul3", "-")
                }
                if(8>=(params.bulan1 as int) && 8 <=(params.bulan2 as int)){
                    try {
                        data.put("aug1", Math.round(((aug.get(it).get(0)/aug.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("aug2", aug.get(it).get(0))
                        data.put("aug3", aug.get(it).get(1))
                        a += aug.get(it).get(0)
                        b += aug.get(it).get(1)
                    }catch (Exception e){
                        data.put("aug1", "-")
                        data.put("aug2", "-")
                        data.put("aug3", "-")
                    }
                }else{
                    data.put("aug1", "-")
                    data.put("aug2", "-")
                    data.put("aug3", "-")
                }
                if(9>=(params.bulan1 as int) && 9 <=(params.bulan2 as int)){
                    try {
                        data.put("sept1", Math.round(((sept.get(it).get(0)/sept.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("sept2", sept.get(it).get(0))
                        data.put("sept3", sept.get(it).get(1))
                        a += sept.get(it).get(0)
                        b += sept.get(it).get(1)
                    }catch (Exception e){
                        data.put("sept1", "-")
                        data.put("sept2", "-")
                        data.put("sept3", "-")
                    }
                }else{
                    data.put("sept1", "-")
                    data.put("sept2", "-")
                    data.put("sept3", "-")
                }
                if(10>=(params.bulan1 as int) && 10 <=(params.bulan2 as int)){
                    try {
                        data.put("okt1", Math.round(((okt.get(it).get(0)/okt.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("okt2", okt.get(it).get(0))
                        data.put("okt3", okt.get(it).get(1))
                        a += okt.get(it).get(0)
                        b += okt.get(it).get(1)
                    }catch (Exception e){
                        data.put("okt1", "-")
                        data.put("okt2", "-")
                        data.put("okt3", "-")
                    }
                }else{
                    data.put("okt1", "-")
                    data.put("okt2", "-")
                    data.put("okt3", "-")
                }
                if(11>=(params.bulan1 as int) && 11 <=(params.bulan2 as int)){
                    try {
                        data.put("nov1", Math.round(((nov.get(it).get(0)/nov.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("nov2", nov.get(it).get(0))
                        data.put("nov3", nov.get(it).get(1))
                        a += nov.get(it).get(0)
                        b += nov.get(it).get(1)
                    }catch (Exception e){
                        data.put("nov1", "-")
                        data.put("nov2", "-")
                        data.put("nov3", "-")
                    }
                }else{
                    data.put("nov1", "-")
                    data.put("nov2", "-")
                    data.put("nov3", "-")
                }
                if(12>=(params.bulan1 as int) && 12 <=(params.bulan2 as int)){
                    try {
                        data.put("dec1", Math.round(((dec.get(it).get(0)/dec.get(it).get(1))*100).doubleValue()) + '%')
                        data.put("dec2", dec.get(it).get(0))
                        data.put("dec3", dec.get(it).get(1))
                        a += dec.get(it).get(0)
                        b += dec.get(it).get(1)
                    }catch (Exception e){
                        data.put("dec1", "-")
                        data.put("dec2", "-")
                        data.put("dec3", "-")
                    }
                }else{
                    data.put("dec1", "-")
                    data.put("dec2", "-")
                    data.put("dec3", "-")
                }

                total = [[a,b],[a,b],[a,b]]

                try {
                    data.put("total1", Math.round(((total.get(it).get(0)/total.get(it).get(1))*100).doubleValue()) + '%')
                    data.put("total2", total.get(it).get(0))
                    data.put("total3", total.get(it).get(1))
                }catch (Exception e){
                    data.put("total1", "-")
                    data.put("total2", "-")
                    data.put("total3", "-")
                }
                reportData.add(data)
            }
        }
        else if (params.namaReport == "02"){
            int count = 0
            int t_app = 0, t_custShow = 0, t_total= 0
            String bulanParam =  params.tanggal
            String bulanParam2 =   params.tanggal2
            Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
            Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
            Date tanggal = tgl-1
            int selisih = tgl2 - tgl
            def periode = tgl.format("d MMMM yyyy") + " - " + tgl2.format("d MMMM yyyy")
            (0..selisih).each{
                tanggal += 1
                def listAppShow = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
//                    reception{
//                        eq("t401StaApp","1")
//                    }
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                def cariCustIn = CustomerIn.createCriteria().list {
                    eq("companyDealer",CompanyDealer.findById(params.workshop as Long))
                    if(listAppShow?.size()>0){
                        inList("customerVehicle",listAppShow?.historyCustomerVehicle?.customerVehicle)
                    }else{
                        eq("id",-1000.toLong())
                    }
                    ge("dateCreated",tanggal)
                    lt("dateCreated",tanggal + 1)
                }

                def listCustShow = Reception.createCriteria().list {
                    ge("t401TanggalWO",tanggal)
                    lt("t401TanggalWO",tanggal + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    eq("staSave","0")
                    eq("staDel","0");
                    customerIn{
                        tujuanKedatangan{
                            eq("m400Tujuan","GR")
                        }
                    }
                }

                def listTotApp = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                def listCustBatal = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("t301StaOkCancelReSchedule","1")
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                def listCustRes = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("t301StaOkCancelReSchedule","2")
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                def listCustNoShow = listCustBatal.size() + listCustRes.size()

                def data = [:]
                count++

                data.put("judul", "GR – CUSTOMER APPOINTMENT")
                data.put("header", "APPOINTMENT SHOW")

                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("kategori", "GENERAL REPAIR")

                data.put("count",count)
                data.put("date",tanggal.format("dd-MMM-yyyy"))
                try {
                    data.put("appointment",cariCustIn.size())
                    data.put("customerShow",listCustNoShow)
                    data.put("total",listTotApp.size())
                    data.put("persen",Math.round(((cariCustIn.size()/listTotApp.size())*100).doubleValue()) + '%')
                    data.put("keterangan1",listCustBatal.size())
                    data.put("keterangan2",listCustRes.size())
                    data.put("keterangan3",listCustBatal.size() + listCustRes.size())
                }catch (Exception e){
                    data.put("appointment","-")
                    data.put("customerShow","-")
                    data.put("total","-")
                    data.put("persen","-")
                    data.put("keterangan1","-")
                    data.put("keterangan2","-")
                    data.put("keterangan3","-")
                }
                t_app += listAppShow.size()
                t_custShow += listCustNoShow
                t_total += listTotApp.size()
                try {
                    data.put("t_appointment",t_app)
                    data.put("t_customerShow",t_custShow)
                    data.put("t_total",t_total)
                    data.put("t_persen",Math.round(((t_app/t_total)*100).doubleValue()) + '%')
                }catch (Exception e){
                    data.put("t_appointment","-")
                    data.put("t_customerShow","-")
                    data.put("t_total","-")
                    data.put("t_persen","-")
                }

                reportData.add(data)
            }

        }
        else if (params.namaReport == "03"){
                int count = 0
                int t_app = 0, t_lapp = 0, t_total= 0
                String bulanParam =  params.tanggal
                String bulanParam2 =   params.tanggal2
                Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
                Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
                Date tanggal = tgl-1
                int selisih = tgl2 - tgl
                def periode = tgl.format("d MMMM yyyy") + " - " + tgl2.format("d MMMM yyyy")

                (0..selisih).each{
                    tanggal += 1

                    def listCustomerBatal = Appointment.createCriteria().list {
                        ge("t301TglJamApp",tanggal)
                        lt("t301TglJamApp",tanggal + 1)
                        eq("t301StaOkCancelReSchedule","1")
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                        jenisApp{
                            eq("m301NamaJenisApp","GR")
                        }
                    }

                    def listCustomerTelat = Appointment.createCriteria().list {
                        ge("t301TglJamRencana",tanggal)
                        lt("t301TglJamRencana",tanggal + 1)
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                        asb{
                            reception{
                                customerIn{
                                    tujuanKedatangan{
                                        eq("m400Tujuan","GR")
                                    }
                                }
                            }
                        }
                    }

                    def jmlTelat = 0

                    listCustomerTelat.each {

                        def cekCustIn = CustomerIn.findByReceptionAndDateCreatedBetween(it.reception, tanggal, tanggal + 1)

                        if(it?.t301TglJamRencana < cekCustIn?.dateCreated){
                            jmlTelat+=1
                        }

                    }

                    def listTotApp = Appointment.createCriteria().list {
                        ge("t301TglJamApp",tanggal)
                        lt("t301TglJamApp",tanggal + 1)
                        eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                        jenisApp{
                            eq("m301NamaJenisApp","GR")
                        }
                    }

                    def data = [:]
                    count++
                    def late = []
                    late = jmlTelat

                    def persn = listCustomerBatal.size()!=0 ? Math.round((listCustomerBatal.size()/listTotApp.size())*100.doubleValue()) : 0;

                    data.put("judul", "GR – CUSTOMER NO SHOW")

                    data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("kategori", "GENERAL REPAIR")

                    data.put("count",count)
                    data.put("date",tanggal.format("dd-MMM-yyyy"))
                    try {
                        data.put("c_appointment",listCustomerBatal.size())
                        data.put("l_appointment",late)
                        data.put("total",listTotApp.size())
                        data.put("persen",persn + '%')
                    }catch (Exception e){
                        data.put("c_appointment","-")
                        data.put("l_appointment","-")
                        data.put("total","-")
                        data.put("persen","-")
                    }
                    t_app += listCustomerBatal.size()
                    t_lapp += late
                    t_total += listTotApp.size()
                    def tpersn = t_app !=0 ? Math.round((t_app/t_total)*100.doubleValue()) : 0;
                    try {
                        data.put("t_appointment",t_app)
                        data.put("t_lateAppointment",t_lapp)
                        data.put("t_total",t_total)
                        data.put("t_persen",tpersn + '%')
                    }catch (Exception e){
                        data.put("t_appointment","-")
                        data.put("t_lateAppointment","-")
                        data.put("t_total","-")
                        data.put("t_persen","-")
                    }
                    reportData.add(data)
                }
        }
        else if (params.namaReport == "04"){
            int count = 0
            int t_app = 0, t_custShow = 0, t_total= 0
            String bulanParam =  params.tanggal
            String bulanParam2 =   params.tanggal2
            Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
            Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
            Date tanggal = tgl-1
            int selisih = tgl2 - tgl
            def periode = tgl.format("d MMMM yyyy") + " - " + tgl2.format("d MMMM yyyy")
            (0..selisih).each{
                tanggal += 1
                def listCustomerRes = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("t301StaOkCancelReSchedule","2")
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","GR")
                            }
                        }
                    }
                }

                def listCustShow = Reception.createCriteria().list {
                    ge("t401TanggalWO",tanggal)
                    lt("t401TanggalWO",tanggal + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    eq("staSave","0")
                    eq("staDel","0");
                    customerIn{
                        tujuanKedatangan{
                            eq("m400Tujuan","GR")
                        }
                    }
                }

                def listTotApp = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                def listCustBatal = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("t301StaOkCancelReSchedule","1")
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                def listCustRes = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("t301StaOkCancelReSchedule","2")
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                def data = [:]
                count++

                data.put("judul", "GR – CUSTOMER RESCHEDULE")
                data.put("header", "RESCHEDULE")

                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("kategori", "GENERAL REPAIR")

                data.put("count",count)
                data.put("date",tanggal.format("dd-MMM-yyyy"))
                try {
                    data.put("appointment",listCustomerRes.size())
                    data.put("customerShow",listCustShow.size())
                    data.put("total",listTotApp.size())
                    data.put("persen",Math.round(((listCustomerRes.size()/listTotApp.size())*100).doubleValue()) + '%')
                    data.put("keterangan1",listCustBatal.size())
                    data.put("keterangan2",listCustRes.size())
                    data.put("keterangan3",listCustBatal.size() + listCustRes.size())
                }catch (Exception e){
                    data.put("appointment","-")
                    data.put("customerShow","-")
                    data.put("total","-")
                    data.put("persen","-")
                    data.put("keterangan1","-")
                    data.put("keterangan2","-")
                    data.put("keterangan3","-")
                }
                t_app += listCustomerRes.size()
                t_custShow += listCustShow.size()
                t_total += listTotApp.size()
                try {
                    data.put("t_appointment",t_app)
                    data.put("t_customerShow",t_custShow)
                    data.put("t_total",t_total)
                    data.put("t_persen",Math.round(((t_app/t_total)*100).doubleValue()) + '%')
                }catch (Exception e){
                    data.put("t_appointment","-")
                    data.put("t_customerShow","-")
                    data.put("t_total","-")
                    data.put("t_persen","-")
                }
                reportData.add(data)
            }
        }
        return reportData
    }

    def calculateReportDataDetail(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        if (params.namaReport == "02"){
            int count = 0
            int t_app = 0, t_total= 0
            String bulanParam =  params.tanggal
            String bulanParam2 =   params.tanggal2
            Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
            Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
            Date tanggal = tgl-1
            int selisih = tgl2 - tgl

            (0..selisih).each{

                tanggal += 1

                def listUnitEntry = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("staDel","0")
                    eq("staSave","0")
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    jenisApp{
                        eq("m301NamaJenisApp","GR")
                    }
                }

                if(listUnitEntry.size() > 0){

                    listUnitEntry.each {
                        def data = [:]
                        def app = it
                        def cariCustIn = CustomerIn.createCriteria().list {
                            eq("companyDealer",CompanyDealer.findById(params.workshop as Long))
                            or{
                                eq("customerVehicle",app?.historyCustomerVehicle?.customerVehicle)
                                eq("t400noPol",app?.historyCustomerVehicle?.fullNoPol,[ignoreCase : true])
                            }
                            ge("dateCreated",tanggal)
                            lt("dateCreated",tanggal + 1)
                        }
                        //SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        //String shortTimeStr = sdf.format(date);

                        String staDatang = cariCustIn?.size() > 0 ? "Yes" : "No"
                        count++
                        data.put("nomor", count)
                        data.put("header", "APPOINTMENT SHOW")
                        data.put("date", tanggal.format("dd-MMM-yyyy"))
                        data.put("nama", it?.historyCustomer?.t182NamaDepan ? it?.historyCustomer?.t182NamaDepan : ""+" "+it?.historyCustomer?.t182NamaBelakang ? it?.historyCustomer?.t182NamaBelakang : "")
                        data.put("alamat", it?.historyCustomer?.t182Alamat ? it?.historyCustomer?.t182Alamat : "-")
                        data.put("noTelp", it?.historyCustomer?.t182NoTelpRumah ? it?.historyCustomer?.t182NoTelpRumah : "-")
                        data.put("noHp", it?.historyCustomer?.t182NoHp ? it?.historyCustomer?.t182NoHp : "-")
                        data.put("tahun", FullModelVinCode.findByFullModelCode(it?.historyCustomerVehicle?.fullModelCode)?.t109ThnBlnPembuatan)
                        data.put("model", it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel : "")
                        data.put("total", staDatang)
                        data.put("noPol", it?.historyCustomerVehicle?.fullNoPol)
                        data.put("jamBooking", it?.t301TglJamApp?.format("HH:mm"))
                        data.put("job", JobApp.findByReception(it?.reception)?.operation.m053NamaOperation)
                        reportData.add(data)
                    }
                }else{
                    def data = [:]
                    count++
                    data.put("nomor", count)
                    data.put("header", "APPOINTMENT SHOW")
                    data.put("date", tanggal.format("dd-MMM-yyyy"))
                    data.put("nama", "-")
                    data.put("alamat", "-")
                    data.put("noTelp", "-")
                    data.put("noHp", "-")
                    data.put("tahun", "-")
                    data.put("model", "-")
                    data.put("total", "-")
                    data.put("noPol", "-")
                    data.put("jamBooking", "-")
                    data.put("job", "-")
                    reportData.add(data)
                }
            }
        }

        if (params.namaReport == "03"){
            int count = 0
            int t_app = 0, t_total= 0
            String bulanParam =  params.tanggal
            String bulanParam2 =   params.tanggal2
            Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
            Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
            Date tanggal = tgl-1
            int selisih = tgl2 - tgl

            (0..selisih).each{

                tanggal += 1
                def listUnitEntry = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
//                    jenisApp{
//                        eq("m301NamaJenisApp","GR")
//                    }
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","GR")
                            }
                        }
                    }
                }

                if(listUnitEntry.size() > 0){
                 listUnitEntry.each {
                    def data = [:]
                    count++
                    data.put("nomor", count)
                    data.put("header", "CANCEL BOOKING")
                    data.put("date", tanggal.format("dd-MMM-yyyy"))
                    data.put("nama", it?.historyCustomer?.t182NamaDepan ? it?.historyCustomer?.t182NamaDepan : ""+" "+it?.historyCustomer?.t182NamaBelakang ? it?.historyCustomer?.t182NamaBelakang : "")
                    data.put("alamat", it?.historyCustomer?.t182Alamat ? it?.historyCustomer?.t182Alamat : "-")
                    data.put("noTelp", it?.historyCustomer?.t182NoTelpRumah ? it?.historyCustomer?.t182NoTelpRumah : "-")
                    data.put("noHp", it?.historyCustomer?.t182NoHp ? it.historyCustomer.t182NoHp : "-")
                    data.put("model", it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel : "")
                    data.put("tahun", FullModelVinCode.findByFullModelCode(it?.historyCustomerVehicle?.fullModelCode)?.t109ThnBlnPembuatan)
                    data.put("total", it?.t301StaOkCancelReSchedule=="1"?"Yes":"No")
                    data.put("noPol", it?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID+" "+it?.historyCustomerVehicle?.t183NoPolTengah+" "+it?.historyCustomerVehicle?.t183NoPolBelakang)
                    reportData.add(data)
                 }
                }else{
                    def data = [:]
                    count++
                    data.put("nomor", count)
                    data.put("header", "CANCEL BOOKING")
                    data.put("date", "-")
                    data.put("nama", "-")
                    data.put("alamat", "-")
                    data.put("noTelp", "-")
                    data.put("noHp", "-")
                    data.put("model", "-")
                    data.put("tahun", "-")
                    data.put("total", "-")
                    data.put("noPol", "-")
                    reportData.add(data)
                }
            }
        }

        if (params.namaReport == "04"){
            int count = 0
            int t_app = 0, t_total= 0
            String bulanParam =  params.tanggal
            String bulanParam2 =   params.tanggal2
            Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
            Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
            Date tanggal = tgl-1
            int selisih = tgl2 - tgl

            (0..selisih).each{

                tanggal += 1
                def listUnitEntry = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    eq("companyDealer", CompanyDealer.findById(params.workshop as Long))
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","GR")
                            }
                        }
                    }
                }

                if(listUnitEntry.size() > 0 ){

                    listUnitEntry.each {
                        def data = [:]
                        count++
                        data.put("nomor", count)
                        data.put("header", "RESCHEDULE")
                        data.put("date", tanggal.format("dd-MMM-yyyy"))
                        data.put("nama", it?.historyCustomer?.t182NamaDepan ? it?.historyCustomer?.t182NamaDepan : ""+" "+it?.historyCustomer?.t182NamaBelakang ? it?.historyCustomer?.t182NamaBelakang : "")
                        data.put("alamat", it?.historyCustomer?.t182Alamat ? it?.historyCustomer?.t182Alamat : "-")
                        data.put("noTelp", it?.historyCustomer?.t182NoTelpRumah ? it?.historyCustomer?.t182NoTelpRumah : "-")
                        data.put("noHp", it?.historyCustomer?.t182NoHp ? it?.historyCustomer?.t182NoHp : "-")
                        data.put("tahun", FullModelVinCode.findByFullModelCode(it?.historyCustomerVehicle?.fullModelCode)?.t109ThnBlnPembuatan)
                        data.put("model", it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel ? it?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel : "")
                        data.put("total", it?.t301StaOkCancelReSchedule=="2"?"Yes":"No")
                        data.put("noPol", it?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID+" "+it?.historyCustomerVehicle?.t183NoPolTengah+" "+it?.historyCustomerVehicle?.t183NoPolBelakang)
                        reportData.add(data)
                    }
                }else{
                    def data = [:]
                    count++
                    data.put("nomor", count)
                    data.put("header", "RESCHEDULE")
                    data.put("date", tanggal.format("dd-MMM-yyyy"))
                    data.put("nama", "-")
                    data.put("alamat", "-")
                    data.put("noTelp", "-")
                    data.put("noHp", "-")
                    data.put("tahun", "-")
                    data.put("model", "-")
                    data.put("total", "-")
                    data.put("noPol", "-")
                    reportData.add(data)
                }
            }
        }

        return reportData
    }

    def previewTest(){
        List<JasperReportDef> reportDefList = []
        def file = null

        def reportData = sumberData(params)
        def paramDetail = request.getRealPath('reports/')
        paramDetail += "\\"
        def parameter = [:]
        parameter.put("SUBREPORT_DIR", paramDetail)

            def reportDef = new JasperReportDef(name:'parentReport.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData,
                    parameters: parameter

            )
            reportDefList.add(reportDef)

            file = File.createTempFile("Dummy_",".pdf")

        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def sumberData(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        if (params.namaReport == "02"){
            int count = 0
            int t_app = 0, t_total= 0
            String bulanParam =  params.tanggal
            String bulanParam2 =   params.tanggal2
            Date tgl = new Date().parse('d-MM-yyyy',bulanParam.toString())
            Date tgl2 = new Date().parse('d-MM-yyyy',bulanParam2.toString())
            Date tanggal = tgl-1
            int selisih = tgl2 - tgl
            def periode = tgl.format("d MMMM yyyy") + " - " + tgl2.format("d MMMM yyyy")
            (0..selisih).each{
                tanggal += 1
                def listCustomerApp = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    reception{
                        eq("t401StaApp","1")
                    }
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","GR")
                            }
                        }
                    }
                }
                def listUnitEntry = Appointment.createCriteria().list {
                    ge("t301TglJamApp",tanggal)
                    lt("t301TglJamApp",tanggal + 1)
                    reception{
                        customerIn{
                            tujuanKedatangan{
                                eq("m400Tujuan","GR")
                            }
                        }
                    }
                }

                def data = [:]
                count++

                data.put("judul", "GR – CUSTOMER APPOINTMENT")
                data.put("header", "APPOINTMENT SHOW")

                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("kategori", "GENERAL REPAIR")

                data.put("count",count)
                data.put("date",tanggal.format("dd-MMM-yyyy"))
                try {
                    data.put("appointment",listCustomerApp.size())
                    data.put("total",listUnitEntry.size())
                    data.put("persen",Math.round(((listCustomerApp.size()/listUnitEntry.size())*100).doubleValue()) + '%')
                }catch (Exception e){
                    data.put("appointment","-")
                    data.put("total","-")
                    data.put("persen","-")
                }
                t_app += listCustomerApp.size()
                t_total += listUnitEntry.size()
                try {
                    data.put("t_appointment",t_app)
                    data.put("t_total",t_total)
                    data.put("t_persen",Math.round(((t_app/t_total)*100).doubleValue()) + '%')
                }catch (Exception e){
                    data.put("t_appointment","-")
                    data.put("t_total","-")
                    data.put("t_persen","-")
                }

                reportData.add(data)
            }
        }
        return reportData
    }
}
