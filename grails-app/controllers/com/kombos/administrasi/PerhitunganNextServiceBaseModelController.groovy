package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PerhitunganNextServiceBaseModelController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
    def conversi =new Konversi()
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = PerhitunganNextServiceBaseModel.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_t115TglBerlaku"){
				ge("t115TglBerlaku",params."sCriteria_t115TglBerlaku")
				lt("t115TglBerlaku",params."sCriteria_t115TglBerlaku" + 1)
			}
	
			if(params."sCriteria_t115KategoriService"){
				ilike("t115KategoriService","%" + (params."sCriteria_t115KategoriService" as String) + "%")
			}

			if(params."sCriteria_baseModel"){
                baseModel{
                    ilike("m102NamaBaseModel","%"+(params."sCriteria_baseModel")+"%")
                }
			}

			if(params."sCriteria_t115Km"){
                def km = params.sCriteria_t115Km.replace(",","")
				eq("t115Km",Double.parseDouble(km))
			}

			if(params."sCriteria_t115Bulan"){
                def bulan = params.sCriteria_t115Bulan.replace(",","")
                eq("t115Bulan",Double.parseDouble(bulan))
			}

			
			switch(sortProperty){
                case "baseModel":
                    baseModel{
                        order("m102NamaBaseModel",sortDir)
                    }
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						t115TglBerlaku: it.t115TglBerlaku?it.t115TglBerlaku.format(dateFormat):"",
			
						t115KategoriService: it.t115KategoriService,
			
						baseModel: it.baseModel?.m102NamaBaseModel,
			
						t115Km: conversi.toRupiah(it.t115Km),
			
						t115Bulan: it.t115Bulan,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[perhitunganNextServiceBaseModelInstance: new PerhitunganNextServiceBaseModel(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def perhitunganNextServiceBaseModelInstance = new PerhitunganNextServiceBaseModel(params)

        def cek = PerhitunganNextServiceBaseModel.createCriteria()
        def result = cek.list() {
            and{
                eq("t115TglBerlaku",perhitunganNextServiceBaseModelInstance.t115TglBerlaku)
                eq("t115KategoriService",perhitunganNextServiceBaseModelInstance.t115KategoriService?.trim(), [ignoreCase: true])
                eq("baseModel",perhitunganNextServiceBaseModelInstance.baseModel)
                eq("t115Km",perhitunganNextServiceBaseModelInstance.t115Km)
                eq("t115Bulan",perhitunganNextServiceBaseModelInstance.t115Bulan)
            }
        }

        //find(operationInstance)
        if(!result){
            perhitunganNextServiceBaseModelInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            perhitunganNextServiceBaseModelInstance?.lastUpdProcess = "INSERT"

            if (!perhitunganNextServiceBaseModelInstance.save(flush: true)) {
                render(view: "create", model: [perhitunganNextServiceBaseModelInstance: perhitunganNextServiceBaseModelInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.perhitunganNextServiceBaseModel.error.message', default: 'Data has been used.')
            render(view: "create", model: [perhitunganNextServiceBaseModelInstance: perhitunganNextServiceBaseModelInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), perhitunganNextServiceBaseModelInstance.id])
		redirect(action: "show", id: perhitunganNextServiceBaseModelInstance.id)
	}

	def show(Long id) {
		def perhitunganNextServiceBaseModelInstance = PerhitunganNextServiceBaseModel.get(id)
		if (!perhitunganNextServiceBaseModelInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), id])
			redirect(action: "list")
			return
		}

		[perhitunganNextServiceBaseModelInstance: perhitunganNextServiceBaseModelInstance]
	}

	def edit(Long id) {
		def perhitunganNextServiceBaseModelInstance = PerhitunganNextServiceBaseModel.get(id)
		if (!perhitunganNextServiceBaseModelInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), id])
			redirect(action: "list")
			return
		}

		[perhitunganNextServiceBaseModelInstance: perhitunganNextServiceBaseModelInstance]
	}

	def update(Long id, Long version) {
		def perhitunganNextServiceBaseModelInstance = PerhitunganNextServiceBaseModel.get(id)
		if (!perhitunganNextServiceBaseModelInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (perhitunganNextServiceBaseModelInstance.version > version) {
				
				perhitunganNextServiceBaseModelInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model')] as Object[],
				"Another user has updated this PerhitunganNextServiceBaseModel while you were editing")
				render(view: "edit", model: [perhitunganNextServiceBaseModelInstance: perhitunganNextServiceBaseModelInstance])
				return
			}
		}


        def cek = PerhitunganNextServiceBaseModel.createCriteria()
        String str = params.t115Km
        String str2 = params.t115Bulan

        def result = cek.list() {
            and{
                ne("id",id)
                eq("t115TglBerlaku",new Date().parse("dd-MM-yyyy", params.t115TglBerlaku_dp))
                eq("t115KategoriService",params.t115KategoriService?.trim(), [ignoreCase: true])
                eq("baseModel",BaseModel.findById(Long.parseLong(params."baseModel.id")))
                eq("t115Km",Double.parseDouble(str?.replace(",","")))
                eq("t115Bulan",Double.parseDouble(str2?.replace(",","")))
            }
        }

        if(!result){
            params?.lastUpdated = datatablesUtilService?.syncTime()
            perhitunganNextServiceBaseModelInstance.properties = params
            perhitunganNextServiceBaseModelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            perhitunganNextServiceBaseModelInstance?.lastUpdProcess = "UPDATE"
            if (!perhitunganNextServiceBaseModelInstance.save(flush: true)) {
                render(view: "edit", model: [perhitunganNextServiceBaseModelInstance: perhitunganNextServiceBaseModelInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.update.perhitunganNextServiceBaseModel.error.message', default: 'Duplicate Data Perhitungan Next Service Base Model')
            render(view: "edit", model: [perhitunganNextServiceBaseModelInstance: perhitunganNextServiceBaseModelInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), perhitunganNextServiceBaseModelInstance.id])
		redirect(action: "show", id: perhitunganNextServiceBaseModelInstance.id)
	}

	def delete(Long id) {
		def perhitunganNextServiceBaseModelInstance = PerhitunganNextServiceBaseModel.get(id)
        perhitunganNextServiceBaseModelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        perhitunganNextServiceBaseModelInstance?.lastUpdProcess = "DELETE"
		if (!perhitunganNextServiceBaseModelInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), id])
			redirect(action: "list")
			return
		}

		try {
			perhitunganNextServiceBaseModelInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'perhitunganNextServiceBaseModel.label', default: 'Perhitungan Next Service Base Model'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(PerhitunganNextServiceBaseModel, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(PerhitunganNextServiceBaseModel, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
