package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MasterDurasiPerjalananDriverController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = MasterDurasiPerjalananDriver.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_m009TglBerlaku"){
				ge("m009TglBerlaku",params."sCriteria_m009TglBerlaku")
				lt("m009TglBerlaku",params."sCriteria_m009TglBerlaku" + 1)
			}

			if(params."sCriteria_companyDealer1"){
				//eq("companyDealer1",params."sCriteria_companyDealer1")
				eq("companyDealer1",CompanyDealer.findByM011AlamatIlike("%"+params."sCriteria_companyDealer1"+"%"))
			}

			if(params."sCriteria_companyDealer2"){
			//	eq("companyDealer2",params."sCriteria_companyDealer2")
				eq("companyDealer2",CompanyDealer.findByM011AlamatIlike("%"+params."sCriteria_companyDealer2"+"%"))
			}

			if(params."sCriteria_m009Durasi"){
				//eq("m009Durasi",params."sCriteria_m009Durasi")
				eq("m009Durasi",Double.parseDouble(params."sCriteria_m009Durasi"))
			}

			
			switch(sortProperty){
                case "companyDealer1" :
                    companyDealer1{
                        order("m011Alamat",sortDir)
                    }
                    break;
                case "companyDealer2" :
                    companyDealer2{
                        order("m011Alamat",sortDir)
                    }
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m009TglBerlaku: it.m009TglBerlaku?it.m009TglBerlaku.format(dateFormat):"",
			
						companyDealer1: it.companyDealer1.m011Alamat,
			
						companyDealer2: it.companyDealer2.m011Alamat,
			
						m009Durasi: it.m009Durasi,
						
						createdBy: it.createdBy,
						
						updatedBy: it.updatedBy,
						
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[masterDurasiPerjalananDriverInstance: new MasterDurasiPerjalananDriver(params)]
	}

	def save() {
		def masterDurasiPerjalananDriverInstance = new MasterDurasiPerjalananDriver(params)
        def cek = MasterDurasiPerjalananDriver.findByM009TglBerlakuAndCompanyDealer1AndCompanyDealer2AndM009Durasi(masterDurasiPerjalananDriverInstance?.m009TglBerlaku, masterDurasiPerjalananDriverInstance?.companyDealer1,masterDurasiPerjalananDriverInstance?.companyDealer2,masterDurasiPerjalananDriverInstance?.m009Durasi)
        if( masterDurasiPerjalananDriverInstance?.companyDealer1 ==  masterDurasiPerjalananDriverInstance?.companyDealer2){
            flash.message = "* Lokasi 1 & 2 tidak boleh sama"
            render(view: "create", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
            return
        }
        if(masterDurasiPerjalananDriverInstance?.m009Durasi == 0){
            flash.message = "* Durasi tidak boleh bernilai NOL"
            render(view: "create", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
            return
        }
        if(cek){
            flash.message = "* Data Sudah Ada"
            render(view: "create", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
            return
        }
        masterDurasiPerjalananDriverInstance?.companyDealer = session.userCompanyDealer
		masterDurasiPerjalananDriverInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		masterDurasiPerjalananDriverInstance?.lastUpdProcess = "INSERT"
		if (!masterDurasiPerjalananDriverInstance.save(flush: true)) {
			render(view: "create", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'Master Durasi Perjalanan Driver'), masterDurasiPerjalananDriverInstance.id])
		redirect(action: "show", id: masterDurasiPerjalananDriverInstance.id)
	}

	def show(Long id) {
		def masterDurasiPerjalananDriverInstance = MasterDurasiPerjalananDriver.get(id)
		if (!masterDurasiPerjalananDriverInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'MasterDurasiPerjalananDriver'), id])
			redirect(action: "list")
			return
		}

		[masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance]
	}

	def edit(Long id) {
		def masterDurasiPerjalananDriverInstance = MasterDurasiPerjalananDriver.get(id)
		if (!masterDurasiPerjalananDriverInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'MasterDurasiPerjalananDriver'), id])
			redirect(action: "list")
			return
		}

		[masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance]
	}

	def update(Long id, Long version) {
        params.m009Durasi=params.m009Durasi.replace(",","")
		def masterDurasiPerjalananDriverInstance = MasterDurasiPerjalananDriver.get(id)
		if (!masterDurasiPerjalananDriverInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'MasterDurasiPerjalananDriver'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (masterDurasiPerjalananDriverInstance.version > version) {
				masterDurasiPerjalananDriverInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'masterDurasiPerjalananDriver.label', default: 'MasterDurasiPerjalananDriver')] as Object[],
				"Another user has updated this MasterDurasiPerjalananDriver while you were editing")
				render(view: "edit", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
				return
			}
		}
        if( params.companyDealer1 ==  params?.companyDealer2){
            flash.message = "* Lokasi 1 & 2 tidak boleh sama"
            render(view: "edit", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
            return
        }
        if(params.m009Durasi.equalsIgnoreCase("0.00")){
            flash.message = "* Durasi tidak boleh bernilai NOL"
            render(view: "edit", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
            return
        }
        def cek = MasterDurasiPerjalananDriver.findByM009TglBerlakuAndCompanyDealer1AndCompanyDealer2AndM009Durasi(params.m009TglBerlaku, CompanyDealer.findById(params.companyDealer1.id),CompanyDealer.findById(params.companyDealer2.id),params.m009Durasi)
        if(cek && cek.id != id){
            flash.message = "* Data Sudah Ada"
            render(view: "edit", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
            return
        }
		masterDurasiPerjalananDriverInstance.properties = params
		masterDurasiPerjalananDriverInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        masterDurasiPerjalananDriverInstance?.companyDealer = session.userCompanyDealer
		masterDurasiPerjalananDriverInstance?.lastUpdProcess = "UPDATE"
		if (!masterDurasiPerjalananDriverInstance.save(flush: true)) {
			render(view: "edit", model: [masterDurasiPerjalananDriverInstance: masterDurasiPerjalananDriverInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'Master Durasi Perjalanan Driver'), masterDurasiPerjalananDriverInstance.id])
		redirect(action: "show", id: masterDurasiPerjalananDriverInstance.id)
	}

	def delete(Long id) {
		def masterDurasiPerjalananDriverInstance = MasterDurasiPerjalananDriver.get(id)
		if (!masterDurasiPerjalananDriverInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'MasterDurasiPerjalananDriver'), id])
			redirect(action: "list")
			return
		}

		try {
			masterDurasiPerjalananDriverInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'MasterDurasiPerjalananDriver'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'masterDurasiPerjalananDriver.label', default: 'MasterDurasiPerjalananDriver'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(MasterDurasiPerjalananDriver, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(MasterDurasiPerjalananDriver, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
