package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class DefaultWarnaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = DefaultWarna.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_vendorCatID") {
                vendorCat{
                    ilike("m191ID", "%" + (params."sCriteria_vendorCatID" as String) + "%")
                }
//                eq("vendorCatID",VendorCat.findByM191ID(params."sCriteria_vendorCatID"))
            }

			if (params."sCriteria_vendorCatNama") {
                vendorCat{
                    ilike("m191Nama", "%" + (params."sCriteria_vendorCatNama" as String) + "%")
                }
//				eq("vendorCatNama",VendorCat.findByM191Nama (params."sCriteria_vendorCatNama"))
			}
			
            if (params."sCriteria_warnaVendor") {
                warnaVendor{
                    ilike("m192NamaWarna", "%" + (params."sCriteria_warnaVendor" as String) + "%")
                }
//                eq("warnaVendor",WarnaVendor.findByM192NamaWarna (params."sCriteria_warnaVendor"))
            }            

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                case "vendorCatID":
                    vendorCat{
                        order("m191ID",sortDir)
                    }
                    break;
                case "vendorCatNama":
                    vendorCat{
                        order("m191Nama",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    vendorCatID: it.vendorCat?.m191ID,
					
					vendorCatNama: it.vendorCat?.m191Nama,

                    warnaVendor: it.warnaVendor?.m192NamaWarna,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [defaultWarnaInstance: new DefaultWarna(params)]
    }

    def save() {
        def defaultWarnaInstance = new DefaultWarna(params)
        defaultWarnaInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        defaultWarnaInstance?.lastUpdProcess = "INSERT"
        if (DefaultWarna.findByVendorCatAndWarnaVendor(defaultWarnaInstance?.vendorCat,defaultWarnaInstance?.warnaVendor)) {
            flash.message = message(code: 'default.error.created.defaultWarna.message', default: 'Data Vendor dan Warna sudah ada.')
            render(view: "create", model: [defaultWarnaInstance: defaultWarnaInstance])
            return
        }
        if (!defaultWarnaInstance.save(flush: true)) {
            render(view: "create", model: [defaultWarnaInstance: defaultWarnaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), defaultWarnaInstance.id])
        redirect(action: "show", id: defaultWarnaInstance.id)
    }

    def show(Long id) {
        def defaultWarnaInstance = DefaultWarna.get(id)
        if (!defaultWarnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), id])
            redirect(action: "list")
            return
        }

        [defaultWarnaInstance: defaultWarnaInstance]
    }

    def edit(Long id) {
        def defaultWarnaInstance = DefaultWarna.get(id)
        if (!defaultWarnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), id])
            redirect(action: "list")
            return
        }

        [defaultWarnaInstance: defaultWarnaInstance]
    }

    def update(Long id, Long version) {
        def defaultWarnaInstance = DefaultWarna.get(id)
        if (!defaultWarnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (defaultWarnaInstance.version > version) {

                defaultWarnaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'defaultWarna.label', default: 'DefaultWarna')] as Object[],
                        "Another user has updated this DefaultWarna while you were editing")
                render(view: "edit", model: [defaultWarnaInstance: defaultWarnaInstance])
                return
            }
        }

        defaultWarnaInstance.properties = params
        defaultWarnaInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        defaultWarnaInstance?.lastUpdProcess = "UPDATE"

        if (DefaultWarna.findByVendorCatAndWarnaVendor(defaultWarnaInstance?.vendorCat,defaultWarnaInstance?.warnaVendor)) {
            flash.message = message(code: 'default.error.created.defaultWarna.message', default: 'Data Vendor dan Warna sudah ada.')
            render(view: "edit", model: [defaultWarnaInstance: defaultWarnaInstance])
            return
        }

        if (!defaultWarnaInstance.save(flush: true)) {
            render(view: "edit", model: [defaultWarnaInstance: defaultWarnaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), defaultWarnaInstance.id])
        redirect(action: "show", id: defaultWarnaInstance.id)
    }

    def delete(Long id) {
        def defaultWarnaInstance = DefaultWarna.get(id)
        if (!defaultWarnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), id])
            redirect(action: "list")
            return
        }

        try {
            defaultWarnaInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            defaultWarnaInstance?.lastUpdProcess = "DELETE"
            defaultWarnaInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'defaultWarna.label', default: 'DefaultWarna'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(DefaultWarna, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(DefaultWarna, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
