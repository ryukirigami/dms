package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ManPowerSertifikatController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = ManPowerSertifikat.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_namaManPower"){
				eq("namaManPower",NamaManPower.findByT015NamaLengkapIlike("%"+params."sCriteria_namaManPower"+"%"))
			}

			if(params."sCriteria_sertifikat"){
				sertifikat{
					ilike("m016NamaSertifikat","%"+(params."sCriteria_sertifikat")+"%")
				}
			}

			if(params."sCriteria_t016TglAwalSertifikat"){
				ge("t016TglAwalSertifikat",params."sCriteria_t016TglAwalSertifikat")
				lt("t016TglAwalSertifikat",params."sCriteria_t016TglAwalSertifikat" + 1)
			}

			if(params."sCriteria_t016TglAkhirSertifikat"){
				ge("t016TglAkhirSertifikat",params."sCriteria_t016TglAkhirSertifikat")
				lt("t016TglAkhirSertifikat",params."sCriteria_t016TglAkhirSertifikat" + 1)
			}
	
			if(params."sCriteria_t016Ket"){
				ilike("t016Ket","%" + (params."sCriteria_t016Ket" as String) + "%")
			}
	
			ilike("staDel",'0')

			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						namaManPower: it.namaManPower.t015NamaLengkap,
			
						sertifikat: it.sertifikat.m016NamaSertifikat,
			
						t016TglAwalSertifikat: it.t016TglAwalSertifikat?it.t016TglAwalSertifikat.format(dateFormat):"",
			
						t016TglAkhirSertifikat: it.t016TglAkhirSertifikat?it.t016TglAkhirSertifikat.format(dateFormat):"",
			
						t016Ket: it.t016Ket,
			
						staDel: it.staDel,
						
						createdBy: it.createdBy,
						
						updatedBy: it.updatedBy,
						
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[manPowerSertifikatInstance: new ManPowerSertifikat(params)]
	}

	def save() {
        params.companyDealer = session.userCompanyDealer
		def manPowerSertifikatInstance = new ManPowerSertifikat()
        def nama = NamaManPower.findByT015NamaLengkap(params.namaManPower)
        def sertifikat = Sertifikat.findById(params.sertifikat.id)
        def cek = ManPowerSertifikat.findByNamaManPowerAndSertifikatAndT016TglAwalSertifikatAndT016TglAkhirSertifikatAndStaDelAndCompanyDealer(nama,sertifikat,params.t016TglAwalSertifikat,params.t016TglAkhirSertifikat,'0',session.userCompanyDealer)
        if(cek){
            flash.message = '* Data Sudah Ada'
            render(view: "create", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
            return
        }
        if(!nama){
            flash.message = '* Nama Manpower Tidak Ada'
            render(view: "create", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
            return
        }
        if(params.t016TglAkhirSertifikat <  params.t016TglAwalSertifikat){
            flash.message = '* Tanggal Akhir Harus Lebih Besar dari tanggal awal'
            render(view: "create", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
            return
        }

        manPowerSertifikatInstance?.dateCreated = datatablesUtilService?.syncTime()
        manPowerSertifikatInstance?.lastUpdated = datatablesUtilService?.syncTime()
        manPowerSertifikatInstance?.companyDealer = session?.userCompanyDealer
        manPowerSertifikatInstance?.namaManPower =  NamaManPower.findByT015NamaLengkap(params.namaManPower)
        manPowerSertifikatInstance?.sertifikat = Sertifikat.findById(params.sertifikat.id)
        manPowerSertifikatInstance?.t016TglAwalSertifikat = params.t016TglAwalSertifikat
        manPowerSertifikatInstance?.t016TglAkhirSertifikat = params.t016TglAkhirSertifikat
        manPowerSertifikatInstance?.t016Ket = params.t016Ket
        manPowerSertifikatInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		manPowerSertifikatInstance?.lastUpdProcess = "INSERT"
		manPowerSertifikatInstance?.setStaDel('0');
		if (!manPowerSertifikatInstance.save(flush: true)) {
			render(view: "create", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'manPowerSertifikat.label', default: 'Man Power Skill Map'), manPowerSertifikatInstance.id])
		redirect(action: "show", id: manPowerSertifikatInstance.id)
	}

	def show(Long id) {
		def manPowerSertifikatInstance = ManPowerSertifikat.get(id)
		if (!manPowerSertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerSertifikat.label', default: 'ManPowerSertifikat'), id])
			redirect(action: "list")
			return
		}

		[manPowerSertifikatInstance: manPowerSertifikatInstance]
	}

	def edit(Long id) {
		def manPowerSertifikatInstance = ManPowerSertifikat.get(id)
		if (!manPowerSertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerSertifikat.label', default: 'ManPowerSertifikat'), id])
			redirect(action: "list")
			return
		}

		[manPowerSertifikatInstance: manPowerSertifikatInstance]
	}

	def update(Long id, Long version) {
		def manPowerSertifikatInstance = ManPowerSertifikat.get(id)
		if (!manPowerSertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerSertifikat.label', default: 'ManPowerSertifikat'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (manPowerSertifikatInstance.version > version) {
				
				manPowerSertifikatInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'manPowerSertifikat.label', default: 'ManPowerSertifikat')] as Object[],
				"Another user has updated this ManPowerSertifikat while you were editing")
				render(view: "edit", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
				return
			}
		}
        def nama = NamaManPower.findByT015NamaLengkap(params.namaManPower)
        def sertifikat = Sertifikat.findById(params.sertifikat.id)
        def cek = ManPowerSertifikat.findByNamaManPowerAndSertifikatAndT016TglAwalSertifikatAndT016TglAkhirSertifikatAndStaDelAndCompanyDealer(nama,sertifikat,params.t016TglAwalSertifikat,params.t016TglAkhirSertifikat,'0',session.userCompanyDealer)
        if(cek){
            if(cek.id != id){
                flash.message = '* Data Sudah Ada'
                render(view: "edit", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
                return
            }
        }
        if(!nama){
            flash.message = '* Nama Manpower Tidak Ada'
            render(view: "edit", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
            return
        }
        if(params.t016TglAkhirSertifikat <  params.t016TglAwalSertifikat){
            flash.message = '* Tanggal Akhir Harus Lebih Besar dari tanggal awal'
            render(view: "edit", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
            return
        }
		//manPowerSertifikatInstance.properties = params
        manPowerSertifikatInstance?.lastUpdated = datatablesUtilService?.syncTime()
        manPowerSertifikatInstance?.namaManPower =  NamaManPower.findByT015NamaLengkap(params.namaManPower)
        manPowerSertifikatInstance?.sertifikat = Sertifikat.findById(params.sertifikat.id)
        manPowerSertifikatInstance?.t016TglAwalSertifikat = params.t016TglAwalSertifikat
        manPowerSertifikatInstance?.t016TglAkhirSertifikat = params.t016TglAkhirSertifikat
        manPowerSertifikatInstance?.t016Ket = params.t016Ket
		manPowerSertifikatInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		manPowerSertifikatInstance?.lastUpdProcess = "UPDATE"
		if (!manPowerSertifikatInstance.save(flush: true)) {
			render(view: "edit", model: [manPowerSertifikatInstance: manPowerSertifikatInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'manPowerSertifikat.label', default: 'Man Power Skill Map'), manPowerSertifikatInstance.id])
		redirect(action: "show", id: manPowerSertifikatInstance.id)
	}

	def delete(Long id) {
		def manPowerSertifikatInstance = ManPowerSertifikat.get(id)
		if (!manPowerSertifikatInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerSertifikat.label', default: 'ManPowerSertifikat'), id])
			redirect(action: "list")
			return
		}

		try {
			//manPowerSertifikatInstance.delete(flush: true)
			manPowerSertifikatInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			manPowerSertifikatInstance?.lastUpdProcess = "DELETE"
			manPowerSertifikatInstance?.setStaDel('1')
            manPowerSertifikatInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerSertifikatInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'manPowerSertifikat.label', default: 'ManPowerSertifikat'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'manPowerSertifikat.label', default: 'ManPowerSertifikat'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(ManPowerSertifikat, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ManPowerSertifikat, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def kodeList() {
        def res = [:]

        def result = NamaManPower.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.t015NamaLengkap
        }

        res."options" = opts
        render res as JSON
    }
	
}
