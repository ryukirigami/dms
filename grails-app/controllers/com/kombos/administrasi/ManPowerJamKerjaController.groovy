package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat

class ManPowerJamKerjaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST",autoCompleteCode:"GET"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def cekJam(String jam){
        String jamTemp = jam;
        if(jamTemp.indexOf(':')<0){
            jamTemp+=':00'
        }else{
            if(jamTemp.indexOf(':')==0){
                String temp = jamTemp
                jamTemp = '-1'+temp
            }
            if(jamTemp.indexOf(':')==jamTemp.length()-1){
                jamTemp+='00'
            }
        }
        return jamTemp;
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = ManPowerJamKerja.createCriteria()
        DateFormat df=new SimpleDateFormat("HH:mm")
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_namaManPower") {
                namaManPower{
                    ilike("t015NamaLengkap","%"+params."sCriteria_namaManPower"+"%")
                }
            }

            if (params."sCriteria_t022Tanggal") {
                ge("t022Tanggal", params."sCriteria_t022Tanggal")
                lt("t022Tanggal", params."sCriteria_t022Tanggal" + 1)
            }

            if (params."sCriteria_t022TanggalAkhir") {
                ge("t022TanggalAkhir", params."sCriteria_t022TanggalAkhir")
                lt("t022TanggalAkhir", params."sCriteria_t022TanggalAkhir" + 1)
            }

            if (params."sCriteria_t022JamMulai1") {
                String waktu = cekJam(params."sCriteria_t022JamMulai1")
                Date date1=df.parse(waktu)
                eq("t022JamMulai1",date1)
            }

            if (params."sCriteria_t022JamSelesai1") {
                String waktu = cekJam(params."sCriteria_t022JamSelesai1")
                Date date2=df.parse(waktu)
                eq("t022JamSelesai1",date2)
            }

            if (params."sCriteria_t022JamMulai2") {
                String waktu = cekJam(params."sCriteria_t022JamMulai2")
                Date date3=df.parse(waktu)
                eq("t022JamMulai2",date3)
            }

            if (params."sCriteria_t022JamSelesai2") {
                String waktu = cekJam(params."sCriteria_t022JamSelesai2")
                Date date4=df.parse(waktu)
                eq("t022JamSelesai2",date4)
            }

            if (params."sCriteria_t022JamMulai3") {
                String waktu = cekJam(params."sCriteria_t022JamMulai3")
                Date date5=df.parse(waktu)
                eq("t022JamMulai3",date5)
            }

            if (params."sCriteria_t022JamSelesai3") {
                String waktu = cekJam(params."sCriteria_t022JamSelesai3")
                Date date6=df.parse(waktu)
                eq("t022JamSelesai3",date6)
            }

            if (params."sCriteria_t022JamMulai4") {
                String waktu = cekJam(params."sCriteria_t022JamMulai4")
                Date date7=df.parse(waktu)
                eq("t022JamMulai4",date7)
            }

            if (params."sCriteria_t022JamSelesai4") {
                String waktu = cekJam(params."sCriteria_t022JamSelesai4")
                Date date8=df.parse(waktu)
                eq("t022JamSelesai4",date8)
            }

            if (params."sCriteria_t022xNamaUser") {
                ilike("t022xNamaUser", "%" + (params."sCriteria_t022xNamaUser" as String) + "%")
            }

            if (params."sCriteria_t022xNamaDivisi") {
                ilike("t022xNamaDivisi", "%" + (params."sCriteria_t022xNamaDivisi" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                case "namaManPower":
                    namaManPower{
                        order("t015NamaLengkap",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    namaManPower: it.namaManPower?.t015NamaLengkap,

                    t022Tanggal: it.t022Tanggal ? it.t022Tanggal.format(dateFormat) : "",

                    t022TanggalAkhir: it.t022TanggalAkhir ? it.t022TanggalAkhir.format(dateFormat) : "",

                    t022JamMulai1: new SimpleDateFormat("HH:mm").format(new Date(it.t022JamMulai1?.getTime())),

                    t022JamSelesai1: new SimpleDateFormat("HH:mm").format(new Date(it.t022JamSelesai1?.getTime())),

                    t022JamMulai2: new SimpleDateFormat("HH:mm").format(new Date(it.t022JamMulai2?.getTime())),

                    t022JamSelesai2: new SimpleDateFormat("HH:mm").format(new Date(it.t022JamSelesai2?.getTime())),

                    t022JamMulai3: it.t022JamMulai3 ? new SimpleDateFormat("HH:mm").format(new Date(it.t022JamMulai3?.getTime())) : '-',

                    t022JamSelesai3: it.t022JamSelesai3 ? new SimpleDateFormat("HH:mm").format(new Date(it.t022JamSelesai3?.getTime())) : '-',

                    t022JamMulai4: it.t022JamMulai4 ? new SimpleDateFormat("HH:mm").format(new Date(it.t022JamMulai4?.getTime())):'-',

                    t022JamSelesai4: it.t022JamSelesai4 ? new SimpleDateFormat("HH:mm").format(new Date(it.t022JamSelesai4?.getTime())) : '-',

                    t022xNamaUser: it.t022xNamaUser,

                    t022xNamaDivisi: it.t022xNamaDivisi,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [manPowerJamKerjaInstance: new ManPowerJamKerja(params)]
    }

    def save() {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy")
        def manPowerJamKerjaInstance = new ManPowerJamKerja()
        def namaManPower = NamaManPower.findByT015NamaLengkapAndStaDel(params.namaManPower,"0")
        def jamKerja = ManPowerJamKerja.findAllByNamaManPowerAndStaDel(namaManPower,"0")
        manPowerJamKerjaInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        manPowerJamKerjaInstance?.setLastUpdProcess("INSERT")
        manPowerJamKerjaInstance?.dateCreated = datatablesUtilService?.syncTime()
        manPowerJamKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()
        manPowerJamKerjaInstance?.setStaDel("0")
        manPowerJamKerjaInstance?.setNamaManPower(namaManPower)
        manPowerJamKerjaInstance?.setT022Tanggal(params.t022Tanggal)
        manPowerJamKerjaInstance?.setT022TanggalAkhir(params.t022TanggalAkhir)
        Time time1 = new Time(Integer?.parseInt(params?.t022JamMulai1_hour), Integer?.parseInt(params?.t022JamMulai1_minute), 0)
        Time time11 = new Time(Integer?.parseInt(params?.t022JamSelesai1_hour), Integer?.parseInt(params?.t022JamSelesai1_minute), 0)
        Time time2 = new Time(Integer?.parseInt(params?.t022JamMulai2_hour), Integer?.parseInt(params?.t022JamMulai2_minute), 0)
        Time time21 = new Time(Integer?.parseInt(params?.t022JamSelesai2_hour), Integer?.parseInt(params?.t022JamSelesai2_minute), 0)
        manPowerJamKerjaInstance?.setT022JamMulai1(time1)
        manPowerJamKerjaInstance?.setT022JamSelesai1(time11)
        manPowerJamKerjaInstance?.setT022JamMulai2(time2)
        manPowerJamKerjaInstance?.setT022JamSelesai2(time21)
        if(params?.t022JamMulai3_hour!='0'){
            Time time3 = new Time(Integer?.parseInt(params?.t022JamMulai3_hour), Integer?.parseInt(params?.t022JamMulai3_minute), 0)
            Time time31 = new Time(Integer?.parseInt(params?.t022JamSelesai3_hour), Integer?.parseInt(params?.t022JamSelesai3_minute), 0)
            manPowerJamKerjaInstance?.setT022JamMulai3(time3)
            manPowerJamKerjaInstance?.setT022JamSelesai3(time31)
        }
        if(params?.t022JamMulai4_hour!='0'){
            Time time4 = new Time(Integer?.parseInt(params?.t022JamMulai4_hour), Integer?.parseInt(params?.t022JamMulai4_minute), 0)
            Time time41 = new Time(Integer?.parseInt(params?.t022JamSelesai4_hour), Integer?.parseInt(params?.t022JamSelesai4_minute), 0)
            manPowerJamKerjaInstance?.setT022JamMulai4(time4)
            manPowerJamKerjaInstance?.setT022JamSelesai4(time41)
        }

        if(params?.t022JamMulai1_hour=='0' && params?.t022JamMulai1_minute=='0' || params?.t022JamSelesai1_hour=='0' && params?.t022JamSelesai1_minute=='0'
        || params?.t022JamMulai2_hour=='0' && params?.t022JamMulai2_minute=='0' || params?.t022JamSelesai2_hour=='0' && params?.t022JamSelesai2_minute=='0'){
            flash.message = "Jam kerja 1 dan jam kerja 2, harus diisi dan tidak boleh = 0"
            render(view: "create", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
            return
        }

        for(int a=1;a<=3;a++) {
            Double bil1=params."t022JamSelesai${a}_hour" as Double
            Double bil2=params."t022JamMulai${a+1}_hour" as Double
            if (a>1){
                if(params?."t022JamMulai${a+1}_hour"=='0' && params?."t022JamMulai${a+1}_minute"=='0'){
                    break;
                }
            }
            if(bil1>bil2){
                flash.message = "Jam kerja ${a+1} harus setelah jam selesai dari jam kerja ${a}"
                render(view: "create", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
                return
            }
        }

        if(manPowerJamKerjaInstance?.t022TanggalAkhir.before(manPowerJamKerjaInstance?.t022Tanggal)){
            flash.message = "Tanggal akhir harus sesudah tangal awal"
            render(view: "create", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
            return
        }
        if(namaManPower==null){
            flash.message = "Tidak ada data nama man power dengan nama "+params.namaManPower
            render(view: "create", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
            return
        }

        if(jamKerja){
            for(find in jamKerja){
                if(find?.t022TanggalAkhir.after(params.t022Tanggal) || find?.t022TanggalAkhir.compareTo(params.t022Tanggal)==0){
                    flash.message = "Tanggal awal jam kerja harus setelah tanggal "+df.format(find?.t022TanggalAkhir)+". Karena sudah ada data jam kerja dengan Nama Man Power "+find?.namaManPower?.t015NamaLengkap+" yang berakhir pada tanggal "+df.format(find?.t022TanggalAkhir)+"."
                    render(view: "create", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
                    return
                    break
                }
            }
        }

        if (!manPowerJamKerjaInstance.save(flush: true)) {
            render(view: "create", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'manPowerJamKerja.label', default: 'Jam Kerja Man Power'), manPowerJamKerjaInstance.id])
        redirect(action: "show", id: manPowerJamKerjaInstance.id)
    }

    def show(Long id) {
        def manPowerJamKerjaInstance = ManPowerJamKerja.get(id)
        if (!manPowerJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJamKerja.label', default: 'ManPowerJamKerja'), id])
            redirect(action: "list")
            return
        }

        [manPowerJamKerjaInstance: manPowerJamKerjaInstance]
    }

    def edit(Long id) {
        def manPowerJamKerjaInstance = ManPowerJamKerja.get(id)
        if (!manPowerJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJamKerja.label', default: 'ManPowerJamKerja'), id])
            redirect(action: "list")
            return
        }

        [manPowerJamKerjaInstance: manPowerJamKerjaInstance]
    }

    def update(Long id, Long version) {
        def manPowerJamKerjaInstance = ManPowerJamKerja.get(id)
        if (!manPowerJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJamKerja.label', default: 'ManPowerJamKerja'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (manPowerJamKerjaInstance.version > version) {

                manPowerJamKerjaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'manPowerJamKerja.label', default: 'ManPowerJamKerja')] as Object[],
                        "Another user has updated this ManPowerJamKerja while you were editing")
                render(view: "edit", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
                return
            }
        }
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy")
        def namaManPower = new NamaManPower()
        namaManPower = NamaManPower.findByT015NamaLengkapAndStaDel(params.namaManPower,"0")
        def jamKerja = ManPowerJamKerja.findAllByNamaManPowerAndStaDel(namaManPower,"0")

        if(namaManPower==null){
            flash.message = "Tidak ada data nama man power dengan nama "+params.namaManPower
            render(view: "edit", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
            return
        }

        if(params?.t022TanggalAkhir.before(params?.t022Tanggal)){
            flash.message = "Tanggal akhir harus sesudah tangal awal"
            render(view: "edit", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
            return
        }

        if(jamKerja){
            for(find in jamKerja) {
                if(find.id<id){
                    if(find?.t022TanggalAkhir.after(params.t022Tanggal) || find?.t022TanggalAkhir.compareTo(params.t022Tanggal)==0){
                        flash.message = "Tanggal awal jam kerja harus setelah tanggal "+df.format(find?.t022TanggalAkhir)+". Karena sudah ada data jam kerja dengan Nama Man Power "+find?.namaManPower?.t015NamaLengkap+" yang berakhir pada tanggal "+df.format(find?.t022TanggalAkhir)+"."
                        render(view: "edit", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
                        return
                        break
                    }
                }
                if(find.id>id){
                    if(find?.t022Tanggal.before(params.t022Tanggal) || find?.t022Tanggal.compareTo(params.t022Tanggal)==0){
                        flash.message = "Tanggal awal jam kerja harus sebelum tanggal "+df.format(find?.t022Tanggal)+". Karena sudah ada data jam kerja dengan Nama Man Power "+find?.namaManPower?.t015NamaLengkap+" yang dimulai pada tanggal "+df.format(find?.t022Tanggal)+"."
                        render(view: "edit", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
                        return
                        break
                    }
                }
            }
        }


        manPowerJamKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()
        manPowerJamKerjaInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        manPowerJamKerjaInstance?.setLastUpdProcess("UPDATE")
        manPowerJamKerjaInstance?.setNamaManPower(namaManPower)
        manPowerJamKerjaInstance?.setT022Tanggal(params.t022Tanggal)
        manPowerJamKerjaInstance?.setT022TanggalAkhir(params.t022TanggalAkhir)
        Time time1 = new Time(Integer?.parseInt(params?.t022JamMulai1_hour), Integer?.parseInt(params?.t022JamMulai1_minute), 0)
        Time time11 = new Time(Integer?.parseInt(params?.t022JamSelesai1_hour), Integer?.parseInt(params?.t022JamSelesai1_minute), 0)
        Time time2 = new Time(Integer?.parseInt(params?.t022JamMulai2_hour), Integer?.parseInt(params?.t022JamMulai2_minute), 0)
        Time time21 = new Time(Integer?.parseInt(params?.t022JamSelesai2_hour), Integer?.parseInt(params?.t022JamSelesai2_minute), 0)
        Time time3 = new Time(Integer?.parseInt(params?.t022JamMulai3_hour), Integer?.parseInt(params?.t022JamMulai3_minute), 0)
        Time time31 = new Time(Integer?.parseInt(params?.t022JamSelesai3_hour), Integer?.parseInt(params?.t022JamSelesai3_minute), 0)
        Time time4 = new Time(Integer?.parseInt(params?.t022JamMulai4_hour), Integer?.parseInt(params?.t022JamMulai4_minute), 0)
        Time time41 = new Time(Integer?.parseInt(params?.t022JamSelesai4_hour), Integer?.parseInt(params?.t022JamSelesai4_minute), 0)
        manPowerJamKerjaInstance?.setT022JamMulai1(time1)
        manPowerJamKerjaInstance?.setT022JamSelesai1(time11)
        manPowerJamKerjaInstance?.setT022JamMulai2(time2)
        manPowerJamKerjaInstance?.setT022JamSelesai2(time21)
        manPowerJamKerjaInstance?.setT022JamMulai3(time3)
        manPowerJamKerjaInstance?.setT022JamSelesai3(time31)
        manPowerJamKerjaInstance?.setT022JamMulai4(time4)
        manPowerJamKerjaInstance?.setT022JamSelesai4(time41)

//        manPowerJamKerjaInstance.properties = params

        if (!manPowerJamKerjaInstance.save(flush: true)) {
            render(view: "edit", model: [manPowerJamKerjaInstance: manPowerJamKerjaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'manPowerJamKerja.label', default: 'Jam Kerja Man Power'), manPowerJamKerjaInstance.id])
        redirect(action: "show", id: manPowerJamKerjaInstance.id)
    }

    def delete(Long id) {
        def manPowerJamKerjaInstance = ManPowerJamKerja.get(id)
        if (!manPowerJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJamKerja.label', default: 'ManPowerJamKerja'), id])
            redirect(action: "list")
            return
        }

        try {
            manPowerJamKerjaInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            manPowerJamKerjaInstance?.setLastUpdProcess("DELETE")
            manPowerJamKerjaInstance?.staDel = "1"
            manPowerJamKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerJamKerjaInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'manPowerJamKerja.label', default: 'ManPowerJamKerja'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'manPowerJamKerja.label', default: 'ManPowerJamKerja'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(ManPowerJamKerja, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(ManPowerJamKerja, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def getNamaManPower(){
        def res = [:]
        def opts = []
        def c = NamaManPower.createCriteria()
        def result = c.list {
            eq("staDel","0");
            order("t015NamaLengkap",'asc');
        }
        result.each {
            opts << it.t015NamaLengkap
        }
        res."options" = opts
        render res as JSON
    }


}
