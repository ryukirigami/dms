package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class JenisAlertController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	def jenisAlertService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = ['index', 'list', 'datatablesList']

	static addPermissions = ['create', 'save']

	static editPermissions = ['edit', 'update']

	static deletePermissions = ['delete']

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render jenisAlertService.datatablesList(params) as JSON
	}

	def create() {
		def result = jenisAlertService.create(params)

        if(!result.error)
            return [jenisAlertInstance: result.jenisAlertInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def jenisAlertInstance = new JenisAlert(params)
        jenisAlertInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        jenisAlertInstance.setLastUpdProcess("INSERT")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
        jenisAlertInstance.setStaDel("0") // untuk data awal di set 0 karena bukan data yang dihapus


        def cekNomor = JenisAlert.createCriteria().list() {
            and{
                eq("m901Id",jenisAlertInstance.m901Id?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cekNomor){
            flash.message = "Nomor Alert Sudah Ada"
            render(view: "create", model: [jenisAlertInstance: jenisAlertInstance])
            return
        }
        def cekNama = JenisAlert.createCriteria().list() {
            and{
                eq("m901NamaAlert",jenisAlertInstance.m901NamaAlert?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cekNama){
            flash.message = "Nama Alert Sudah Ada"
            render(view: "create", model: [jenisAlertInstance: jenisAlertInstance])
            return
        }

        if (!jenisAlertInstance.save(flush: true)) {
            render(view: "create", model: [jenisAlertInstance: jenisAlertInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'jenisAlert.label', default: 'Jenis Alert'), jenisAlertInstance.id])
        redirect(action: "show", id: jenisAlertInstance.id)
    }

	def show(Long id) {
		def result = jenisAlertService.show(params)

		if(!result.error)
			return [ jenisAlertInstance: result.jenisAlertInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = jenisAlertService.show(params)

		if(!result.error)
			return [ jenisAlertInstance: result.jenisAlertInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

    def update(Long id, Long version) {
        def jenisAlertInstance = JenisAlert.get(id)
        if (!jenisAlertInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisAlert.label', default: 'JenisAlert'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (jenisAlertInstance.version > version) {

                jenisAlertInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'jenisAlert.label', default: 'JenisAlert')] as Object[],
                        "Another user has updated this Religion while you were editing")
                render(view: "edit", model: [jenisAlertInstance: jenisAlertInstance])
                return
            }
        }

        def cekNomor = JenisAlert.createCriteria()
        def resultNomor = cekNomor.list() {
            and{
                eq("m901Id",params.m901Id?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(resultNomor){
            for(find in resultNomor){
                if(id!=find.id){
                    flash.message = "Nomor Alert Sudah Ada"
                    render(view: "edit", model: [jenisAlertInstance: jenisAlertInstance])
                    return
                }
            }
        }


        def cekNama = JenisAlert.createCriteria()
        def resultNama = cekNama.list() {
            and{
                eq("m901NamaAlert",params.m901NamaAlert?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(resultNama){
            for(find in resultNama){
                if(id!=find.id){
                    flash.message = "Nama Alert Sudah Ada"
                    render(view: "edit", model: [jenisAlertInstance: jenisAlertInstance])
                    return
                }
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        jenisAlertInstance.properties = params
        jenisAlertInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        jenisAlertInstance.setLastUpdProcess("UPDATE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"

        if (!jenisAlertInstance.save(flush: true)) {
            render(view: "edit", model: [jenisAlertInstance: jenisAlertInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'jenisAlert.label', default: 'Jenis Alert'), jenisAlertInstance.id])
        redirect(action: "show", id: jenisAlertInstance.id)
    }

    def delete(Long id) {
        def jenisAlertInstance = JenisAlert.get(id)
        if (!jenisAlertInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisAlert.label', default: 'JenisAlert'), id])
            redirect(action: "list")
            return
        }

        try {
            jenisAlertInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
            jenisAlertInstance.setLastUpdProcess("DELETE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
            jenisAlertInstance.setStaDel("1") // untuk data awal di set 0 karena bukan data yang dihapus
            jenisAlertInstance?.lastUpdated = datatablesUtilService?.syncTime()
            jenisAlertInstance.save(flush: true)
            // 4 baris codingan di atas ini digunakan jika domain2 yang mempunyain field staDel
            // jika tidak ada stadel cukup dengan codingan 1 baris dibawah ini
            //jenisAlertInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'jenisAlert.label', default: 'JenisAlert'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'jenisAlert.label', default: 'JenisAlert'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(JenisAlert, params)
            // ada 2 function yang bisa digunakan
            // yaitu massDelete dan massDeleteStaDelNew
            // massDelete khusus untuk domain2 yang tidak ada field staDel
            // massDeleteStaDelNew khusus untuk domain2 yang ada field staDel
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }


}
