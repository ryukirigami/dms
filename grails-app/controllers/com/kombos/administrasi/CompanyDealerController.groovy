package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.finance.AccountNumber
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.util.regex.Matcher
import java.util.regex.Pattern

class CompanyDealerController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
        
        private Pattern pattern;
	private Matcher matcher;
 
	private static final String EMAIL_PATTERN = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/;
 
	public boolean validasiEmail(String email) {
		email ==~ EMAIL_PATTERN
	}
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		
		session.exportParams=params
		
		def c = CompanyDealer.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m011ID"){
				ilike("m011ID","%" + (params."sCriteria_m011ID" as String) + "%")
			}
	
			if(params."sCriteria_m011NamaWorkshop"){
				ilike("m011NamaWorkshop","%" + (params."sCriteria_m011NamaWorkshop" as String) + "%")
			}

			if(params."sCriteria_jenisDealer"){
				eq("jenisDealer",JenisDealer.findByM005JenisDealerIlike("%" + params."sCriteria_jenisDealer"+ "%"))
			}
	
			if(params."sCriteria_m011Alamat"){
				ilike("m011Alamat","%" + (params."sCriteria_m011Alamat" as String) + "%")
			}

			if(params."sCriteria_provinsi"){
				eq("provinsi",Provinsi.findByM001NamaProvinsiIlike("%" + params."sCriteria_provinsi"+ "%"))
			}

			if(params."sCriteria_kabKota"){
				eq("kabKota",KabKota.findByM002NamaKabKotaIlike("%" + params."sCriteria_kabKota"+ "%"))
			}

			if(params."sCriteria_kecamatan"){
				eq("kecamatan",Kecamatan.findByM003NamaKecamatanIlike("%" + params."sCriteria_kecamatan"+ "%"))
			}

			if(params."sCriteria_kelurahan"){
				eq("kelurahan",Kelurahan.findByM004NamaKelurahanIlike("%" + params."sCriteria_kelurahan"+ "%"))
			}
	
			if(params."sCriteria_m011Fax"){
				ilike("m011Fax","%" + (params."sCriteria_m011Fax" as String) + "%")
			}
	
			if(params."sCriteria_m011Telp"){
				ilike("m011Telp","%" + (params."sCriteria_m011Telp" as String) + "%")
			}
	
			if(params."sCriteria_m011Email"){
				ilike("m011Email","%" + (params."sCriteria_m011Email" as String) + "%")
			}
	
			if(params."sCriteria_m011NPWP"){
				ilike("m011NPWP","%" + (params."sCriteria_m011NPWP" as String) + "%")
			}

			if(params."sCriteria_m011Logo"){
				eq("m011Logo",params."sCriteria_m011Logo")
			}
	
			if(params."sCriteria_imageMime"){
				ilike("imageMime","%" + (params."sCriteria_imageMime" as String) + "%")
			}
	
			if(params."sCriteria_m011NomorRekening"){
				ilike("m011NomorRekening","%" + (params."sCriteria_m011NomorRekening" as String) + "%")
			}

			if(params."sCriteria_bank"){
//				eq("bank",Bank.findByM702NamaBankIlike(params."sCriteria_bank"))
                bank{
                    ilike("m702NamaBank","%" + (params."sCriteria_bank" as String) + "%")
                }

			}
	
			if(params."sCriteria_m011AtasNama"){
				ilike("m011AtasNama","%" + (params."sCriteria_m011AtasNama" as String) + "%")
			}
	
			if(params."sCriteria_m011Ket"){
				ilike("m011Ket","%" + (params."sCriteria_m011Ket" as String) + "%")
			}
	
			if(params."sCriteria_m011EmailComplainSerius"){
				ilike("m011EmailComplainSerius","%" + (params."sCriteria_m011EmailComplainSerius" as String) + "%")
			}
	
			if(params."sCriteria_m011EmailComplainTdkSerius"){
				ilike("m011EmailComplainTdkSerius","%" + (params."sCriteria_m011EmailComplainTdkSerius" as String) + "%")
			}
	
			if(params."sCriteria_m011PKP"){
				ilike("m011PKP","%" + (params."sCriteria_m011PKP" as String) + "%")
			}
	
			if(params."sCriteria_m011StaBPCenter"){
				ilike("m011StaBPCenter","%" + (params."sCriteria_m011StaBPCenter" as String) + "%")
			}
	

				eq("staDel","0")


			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m011ID: it.m011ID,
			
						m011NamaWorkshop: it.m011NamaWorkshop,
			
						jenisDealer: it.jenisDealer.m005JenisDealer,
			
						m011Alamat: it.m011Alamat,
			
						provinsi: it.provinsi?.m001NamaProvinsi,
			
						kabKota: it.kabKota?.m002NamaKabKota,
			
						kecamatan: it.kecamatan?.m003NamaKecamatan,
			
						kelurahan: it.kelurahan?.m004NamaKelurahan,
			
						m011Fax: it.m011Fax,
			
						m011Telp: it.m011Telp,
			
						m011Email: it.m011Email,
			
						m011NPWP: it.m011NPWP,
			
						m011Logo: it.m011Logo,
			
						imageMime: it.imageMime,
			
						m011NomorRekening: it.m011NomorRekening,
			
						bank: it.bank?.m702NamaBank,
			
						m011AtasNama: it.m011AtasNama,
			
						m011Ket: it.m011Ket,
			
						m011EmailComplainSerius: it.m011EmailComplainSerius,
			
						m011EmailComplainTdkSerius: it.m011EmailComplainTdkSerius,
			
						m011PKP: it.m011PKP,
			
						m011StaBPCenter: it.m011StaBPCenter,
			
						m011StaDel: it.staDel
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[companyDealerInstance: new CompanyDealer(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def companyDealerInstance = new CompanyDealer(params)
         log.info('data masuk')


        companyDealerInstance.provinsi = Provinsi.get(params.provinsi.id)
        companyDealerInstance.kecamatan = Kecamatan.get(params.kecamatan.id)
        companyDealerInstance.kabKota = KabKota.get(params.kabupaten.id)
        companyDealerInstance.kelurahan = Kelurahan.get(params.kelurahan.id)
        companyDealerInstance.staDel = '0'
        companyDealerInstance.m011NomorAkunTransfer = AccountNumber.findById(params.m011NomorAkunTransfer.id)
        companyDealerInstance.m011NomorAkunLainnya = AccountNumber.findById(params.m011NomorAkunLainnya.id)
        companyDealerInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        companyDealerInstance?.lastUpdProcess = "INSERT"
        def logo = request.getFile('m011Logo')
        def notAllowCont = ['application/octet-stream']
          if(logo !=null && !notAllowCont.contains(logo.getContentType()) ){

          def okcontents = ['image/png', 'image/jpeg', 'image/gif']

              if (! okcontents.contains(logo.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'create', model:[companyDealerInstance: companyDealerInstance])
                return;
              }

             companyDealerInstance.m011Logo = logo.getBytes()
             companyDealerInstance.imageMime = logo.getContentType()
        }


        if (!companyDealerInstance.save(flush: true)) {
            render(view: "create", model: [companyDealerInstance: companyDealerInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), companyDealerInstance.id])
        redirect(action: "show", id: companyDealerInstance.id)
	}

	def show(Long id) {
		def companyDealerInstance = CompanyDealer.get(id)
		if (!companyDealerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), id])
			redirect(action: "list")
			return
		}

	//	[companyDealerInstance: companyDealerInstance]
        [companyDealerInstance: companyDealerInstance, logostamp: new Date().format("yyyyMMddhhmmss")]
	}

	def edit(Long id) {
		def companyDealerInstance = CompanyDealer.get(id)
		if (!companyDealerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), id])
			redirect(action: "list")
			return
		}

		[companyDealerInstance: companyDealerInstance]
	}

	def update(Long id, Long version) {
        def companyDealerInstance = CompanyDealer.get(Double.parseDouble(params.id))
		if (!companyDealerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), id])
			redirect(action: "list")
			return
		}



	//	companyDealerInstance.properties = params
        companyDealerInstance.m011TelpExt = params.m011TelpExt
        companyDealerInstance.m011FaxExt = params.m011FaxExt
        companyDealerInstance.bank = Bank.get(params.bank.id)
        companyDealerInstance.jenisDealer = JenisDealer.get(params.jenisDealer.id)
        companyDealerInstance.kabKota = KabKota.get(params.kabupaten.id)
        companyDealerInstance.kecamatan = Kecamatan.get(params.kecamatan.id)
        companyDealerInstance.kelurahan = Kelurahan.get(params.kelurahan.id)
        companyDealerInstance.lastUpdated = datatablesUtilService?.syncTime()
        companyDealerInstance.m011Alamat = params.m011Alamat
        companyDealerInstance.m011AtasNama = params.m011AtasNama
        companyDealerInstance.m011Email = params.m011Email
        companyDealerInstance.m011EmailComplainSerius = params.m011EmailComplainSerius
        companyDealerInstance.m011Fax = params.m011Fax
        companyDealerInstance.m011ID = params.m011ID
        companyDealerInstance.m011Ket = params.m011Ket
        companyDealerInstance.m011NamaWorkshop = params.m011NamaWorkshop
        companyDealerInstance.m011NomorRekening = params.m011NomorRekening
        companyDealerInstance.m011NPWP = params.m011NPWP
        companyDealerInstance.m011PKP = params.m011PKP
        companyDealerInstance.m011StaBPCenter = params.m011StaBPCenter
        companyDealerInstance.m011Telp = params.m011Telp
        companyDealerInstance.m011NomorAkunTransfer = AccountNumber.findById(params.m011NomorAkunTransfer.id)
        companyDealerInstance.m011NomorAkunLainnya = AccountNumber.findById(params.m011NomorAkunLainnya.id)
        companyDealerInstance.provinsi = Provinsi.get(params.provinsi.id)
        companyDealerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        companyDealerInstance?.lastUpdProcess = "UPDATE"
        companyDealerInstance?.m011SelisihWaktu = params?.m011SelisihWaktu?.toInteger()
        companyDealerInstance?.m011OutletCode = params?.m011OutletCode
        companyDealerInstance.lastUpdated = datatablesUtilService?.syncTime()


        def logo = request.getFile('m011Logo')
        def notAllowCont = ['application/octet-stream']

        if(logo !=null && !notAllowCont.contains(logo.getContentType()) ){

            //ln "ini masuk logo edit"

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(logo.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[companyDealerInstance: companyDealerInstance])
                return;
            }

            companyDealerInstance.m011Logo = logo.getBytes()
            companyDealerInstance.imageMime = logo.getContentType()
        }

        if (version != null) {
            if (companyDealerInstance.version > version) {

                companyDealerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'companyDealer.label', default: 'CompanyDealer')] as Object[],
                        "Another user has updated this CompanyDealer while you were editing")
                render(view: "edit", model: [companyDealerInstance: companyDealerInstance])
                return
            }
        }

        if (!companyDealerInstance.save(flush: true)) {
			render(view: "edit", model: [companyDealerInstance: companyDealerInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), companyDealerInstance.id])
		redirect(action: "show", id: id)
	}

	def delete(Long id) {
		def companyDealerInstance = CompanyDealer.get(id)

        companyDealerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        companyDealerInstance?.lastUpdProcess = "DELETE"

        companyDealerInstance.staDel = '1'
		if (!companyDealerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), id])
			redirect(action: "list")
			return
		}

		try {
			companyDealerInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'companyDealer.label', default: 'CompanyDealer'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDel(CompanyDealer, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(CompanyDealer, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
        
    def showLogo = {
      def companyDealer = CompanyDealer.get(params.id)

      response.setContentLength(companyDealer.m011Logo.size())
      OutputStream out = response.getOutputStream();
      out.write(companyDealer.m011Logo);
      out.close();
    }


    def getAlamatBank = {
        def idBank = params.idBank
        def bank = Bank.findById(Double.parseDouble(idBank))

        def ret = [alamatBank : bank.m702Alamat]
        render ret as JSON
    }

    def getKabupatens (){
        def idProp = params.idProp
        def provinsi = Provinsi.get(idProp)

        def kabKotaList = KabKota.findAllByProvinsi(provinsi)

        def res = []
        kabKotaList.each {
            res << [
                    id : it.id,
                    namaKabupaten : it.m002NamaKabKota
            ]
        }
        render res as JSON
    }

    def getKecamatans(){
        def idKab = params.idKab
        def kabKota = KabKota.get(idKab)

        def kecamatanList = Kecamatan.findAllByKabKota(kabKota)

        def res = []
        kecamatanList.each {
            res << [
                    id : it.id,
                    namaKecamatan : it.m003NamaKecamatan
            ]
        }

        render res as JSON
    }

    def getKelurahans(){
        def idKec = params.idKec
        def kec = Kecamatan.get(idKec)

        def kelurahanList = Kelurahan.findAllByKecamatan(kec)
        def res = []

        kelurahanList.each {
            res << [
                    id : it.id,
                    namaKelurahan : it.m004NamaKelurahan
            ]
        }

        render res as JSON
    }
    def listAccount() {
        def res = [:]
        def opts = []
        def z = AccountNumber.createCriteria()
        def results=z.list {
            or{
                ilike("accountNumber","%" + params.query + "%")
                ilike("accountName","%" + params.query + "%")
            }
            eq("accountMutationType","MUTASI")
            eq('staDel', '0')
            order("accountNumber");
            setMaxResults(10)
        }
        results.each {
            opts<<it.accountNumber +" || "+ it.accountName
        }
        res."options" = opts
        render res as JSON
    }

    def detailAccount(){
        def result = [:]
        def data= params.noAccount as String
        if(data.contains("||")){
            def dataNoAccount = data.split(" ")
            def account = AccountNumber.createCriteria().list {
                eq("staDel","0")
                ilike("accountNumber",dataNoAccount[0].trim() + "%")
            }
            if(account.size()>0){
                def accountData = account.last()
                result."hasil" = "ada"
                result."id" = accountData?.id
                result."namaAccount" = accountData?.accountName
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }


}
