package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Goods
import com.kombos.parts.Satuan
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

class UploadPartsMappingController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def excelImportService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    
    def index() {
      //  redirect(action: "list", params: params)
    }

    
    def view(){
        def partsMappingInstance = new PartsMapping(params)
        //handle upload file
        Map CONFIG_PARTSMAPPING_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'fullModelCode',
                        'B':'kodeGoods',
                        'C':'namaGoods',
                        'D':'t111Jumlah1',
                        'E':'satuan'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcelPartsMapping");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                //println "asd"
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [partsMappingInstance: partsMappingInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            def fullModelPartsList = excelImportService.columns(workbook, CONFIG_PARTSMAPPING_COLUMN_MAP)

            jsonData = fullModelPartsList as JSON

            FullModelCode fullModelCode = null
            Goods kodeGoods = null
            Goods namaGoods = null
            Satuan satuan = null

            String status = "0", style = ""
            fullModelPartsList?.each {

                if((it.fullModelCode && it.fullModelCode!="")
                        && (it.kodeGoods && it.kodeGoods!="")
                        && (it.namaGoods && it.namaGoods!="")
                        && (it.t111Jumlah1 && it.t111Jumlah1!="")
                        && (it.satuan && it.satuan!="")){

                    fullModelCode = FullModelCode.findByT110FullModelCode(it.fullModelCode)
                    kodeGoods = Goods.findByM111ID(it.kodeGoods)
                    namaGoods = Goods.findByM111Nama(it.namaGoods)
//                    jumlah = PartsMapping.findByT111Jumlah1(it.jumlah)
                    satuan = Satuan.findByM118Satuan1(it.satuan)

//                    try{
//                        t111Jumlah1 = it.t111Jumlah1
//                    }catch (Exception ex){
//                        t111Jumlah1 = null
//                    }


//                    if(!fullModelCode || !kodeGoods || !namaGoods || !satuan){
//                        jmlhDataError++;
//                        status = "1";
//                    }

                } else {

                    jmlhDataError++;
                    status = "1";

                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.fullModelCode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.kodeGoods+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.namaGoods+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t111Jumlah1+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.satuan+"\n" +
                        "                            </td>\n"+
                        "                        </tr>"

                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadFullModeParts.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadFullModeParts.message', default: "Read File Done")
        }

        render(view: "index", model: [partsMappingInstance: partsMappingInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }


    def upload() {
        def partsMappingInstance = null
        def requestBody = request.JSON

        FullModelCode fullModelCode = null
        Goods kodeGoods = null
        Goods namaGoods = null
        Satuan satuan = null
        Double t111Jumlah1 = null

        requestBody.each{
            partsMappingInstance = new PartsMapping()
            fullModelCode = FullModelCode.findByT110FullModelCode(it.fullModelCode)
            kodeGoods = Goods.findByM111ID(it.kodeGoods)
            namaGoods = Goods.findByM111Nama(it.namaGoods)
            satuan = Satuan.findByM118Satuan1(it.satuan)
//            t111Jumlah1 = Double.parseDouble(it.t111Jumlah1)

            if(fullModelCode && kodeGoods && namaGoods && satuan && (it.t111Jumlah1 && it.t111Jumlah1!="")){

                partsMappingInstance.fullModelCode = fullModelCode
                partsMappingInstance.goods = kodeGoods
                partsMappingInstance.goods = namaGoods
                partsMappingInstance.satuan = satuan
                partsMappingInstance.t111Jumlah1 = it.t111Jumlah1
                partsMappingInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                partsMappingInstance.companyDealer = session.userCompanyDealer
                partsMappingInstance.lastUpdProcess = "INSERT"
                partsMappingInstance.staDel = '0'
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                def cek = PartsMapping.find(partsMappingInstance)
                if(!cek){
                    partsMappingInstance.save()
                }
            }
        }

        flash.message = message(code: 'default.uploadFullModeParts.message', default: "Save Full Model Parts Done")
        render(view: "index", model: [partsMappingInstance: partsMappingInstance])
    }
}
