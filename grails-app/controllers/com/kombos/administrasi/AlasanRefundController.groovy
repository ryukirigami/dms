package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class AlasanRefundController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {

        def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]

		session.exportParams=params
		
		def c = AlasanRefund.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m071ID"){
				ilike("m071ID","%" + (params."sCriteria_m071ID" as String) + "%")
			}
	
			if(params."sCriteria_m071AlasanRefund"){
				ilike("m071AlasanRefund","%" + (params."sCriteria_m071AlasanRefund" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			eq("staDel", "0")
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m071ID: it.m071ID,
			
						m071AlasanRefund: it.m071AlasanRefund,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[alasanRefundInstance: new AlasanRefund(params)]
	}

	def save() {
		def alasanRefundInstance = new AlasanRefund(params)
        AlasanRefund cek = AlasanRefund.findByM071IDAndStaDel(params.m071ID, '0')
        if(cek){
            flash.message = message(code: 'alasanRefund.m071ID.unique')
            render(view: "create", model: [alasanRefundInstance: alasanRefundInstance])
            return
        }
        AlasanRefund cek2 = AlasanRefund.findByM071AlasanRefundIlikeAndStaDel(params.m071AlasanRefund, '0')
        if(cek2){
            flash.message = message(code: 'alasanRefund.m071AlasanRefund.unique')
            render(view: "create", model: [alasanRefundInstance: alasanRefundInstance])
            return
        }
		if (!alasanRefundInstance.save(flush: true)) {
			render(view: "create", model: [alasanRefundInstance: alasanRefundInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), alasanRefundInstance.id])
		redirect(action: "show", id: alasanRefundInstance.id)
	}

	def show(Long id) {
		def alasanRefundInstance = AlasanRefund.get(id)
		if (!alasanRefundInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), id])
			redirect(action: "list")
			return
		}

		[alasanRefundInstance: alasanRefundInstance]
	}

	def edit(Long id) {
		def alasanRefundInstance = AlasanRefund.get(id)
		if (!alasanRefundInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), id])
			redirect(action: "list")
			return
		}

		[alasanRefundInstance: alasanRefundInstance]
	}

	def update(Long id, Long version) {
		def alasanRefundInstance = AlasanRefund.get(id)
		if (!alasanRefundInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (alasanRefundInstance.version > version) {
				
				alasanRefundInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'alasanRefund.label', default: 'AlasanRefund')] as Object[],
				"Another user has updated this AlasanRefund while you were editing")
				render(view: "edit", model: [alasanRefundInstance: alasanRefundInstance])
				return
			}
		}

		alasanRefundInstance.properties = params
        AlasanRefund cek = AlasanRefund.findByM071IDAndStaDelAndIdNotEqual(params.m071ID, '0', id)
        if(cek){
            flash.message = message(code: 'alasanRefund.m071ID.unique')
            render(view: "edit", model: [alasanRefundInstance: alasanRefundInstance])
            return
        }
        AlasanRefund cek2 = AlasanRefund.findByM071AlasanRefundIlikeAndStaDelAndIdNotEqual(params.m071AlasanRefund, '0', id)
        if(cek2){
            flash.message = message(code: 'alasanRefund.m071AlasanRefund.unique')
            render(view: "edit", model: [alasanRefundInstance: alasanRefundInstance])
            return
        }
		if (!alasanRefundInstance.save(flush: true)) {
			render(view: "edit", model: [alasanRefundInstance: alasanRefundInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), alasanRefundInstance.id])
		redirect(action: "show", id: alasanRefundInstance.id)
	}

	def delete(Long id) {
		def alasanRefundInstance = AlasanRefund.get(id)
		if (!alasanRefundInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), id])
			redirect(action: "list")
			return
		}

		try {
            alasanRefundInstance.staDel = "1"
            if (!alasanRefundInstance.save(flush: true)) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), id])
                redirect(action: "show", id: id)
                return
            }
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'alasanRefund.label', default: 'AlasanRefund'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
//			datatablesUtilService.massDelete(AlasanRefund, params)
            log.info(params.ids)
            def jsonArray = JSON.parse(params.ids)

            jsonArray.each {
                def alasanRefundInstance = AlasanRefund.get(it)

                alasanRefundInstance.staDel = "1"
                //namaDokumenInstance.delete(flush: true)
                alasanRefundInstance.save(flush: true)

            }

            res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(AlasanRefund, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
