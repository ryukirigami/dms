package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StdTimeWorkItemsController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = StdTimeWorkItems.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_workItems"){
			workItems{
				ilike("m039WorkItems","%" + (params."sCriteria_workItems") + "%")
				}
				//eq("workItems",WorkItems.findByM039WorkItems(params."sCriteria_workItems"))
			}

			if(params."sCriteria_m041TanggalBerlaku"){
				ge("m041TanggalBerlaku",params."sCriteria_m041TanggalBerlaku")
				lt("m041TanggalBerlaku",params."sCriteria_m041TanggalBerlaku" + 1)
			}

			if(params."sCriteria_m041RdanR"){
				eq("m041RdanR",Double.parseDouble(params."sCriteria_m041RdanR"))
			}

			if(params."sCriteria_m041Replace"){
				eq("m041Replace",Double.parseDouble(params."sCriteria_m041Replace"))
			}

			if(params."sCriteria_m041Overhaul"){
				eq("m041Overhaul",Double.parseDouble(params."sCriteria_m041Overhaul"))
			}

			if(params."sCriteria_companyDealer"){
				eq("companyDealer",CompanyDealer.findByM011NamaWorkshop(params."sCriteria_companyDealer"))
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}
            order("m041TanggalBerlaku")
            order("workItems")

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						workItems: it.workItems?.m039WorkItems,
			
						m041TanggalBerlaku: it.m041TanggalBerlaku?it.m041TanggalBerlaku.format(dateFormat):"",
			
						m041RdanR: it.m041RdanR,
			
						m041Replace: it.m041Replace,
			
						m041Overhaul: it.m041Overhaul,
			
						companyDealer: it.companyDealer?.m011NamaWorkshop,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
        [stdTimeWorkItemsInstance: new StdTimeWorkItems(params)]
	}

	def save() {
		def stdTimeWorkItemsInstance = new StdTimeWorkItems(params)
        stdTimeWorkItemsInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stdTimeWorkItemsInstance?.lastUpdProcess = "INSERT"
        stdTimeWorkItemsInstance?.companyDealer = session.userCompanyDealer
        def cek = StdTimeWorkItems.createCriteria().list() {
            and{

                eq("workItems",stdTimeWorkItemsInstance.workItems)

                eq("m041TanggalBerlaku",stdTimeWorkItemsInstance.m041TanggalBerlaku)
                eq("m041Overhaul",stdTimeWorkItemsInstance.m041Overhaul)
                eq("m041RdanR",stdTimeWorkItemsInstance.m041RdanR)
                eq("m041Replace",stdTimeWorkItemsInstance.m041Replace)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }
        if (stdTimeWorkItemsInstance.getM041RdanR()<=0) {
            flash.message = message(code: 'default.error.created.stdTimeWorkItems.message', default: 'Inputan Removal & Reinstall harus lebih besar dari 0.')
            render(view: "create", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }
        if (stdTimeWorkItemsInstance.getM041Replace()<=0) {
            flash.message = message(code: 'default.error.created.stdTimeWorkItems.message', default: 'Inputan Replace harus lebih besar dari 0.')
            render(view: "create", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }
        if (stdTimeWorkItemsInstance.getM041Overhaul()<=0) {
            flash.message = message(code: 'default.error.created.stdTimeWorkItems.message', default: 'Inputan Overhaul harus lebih besar dari 0.')
            render(view: "create", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }
        if (!stdTimeWorkItemsInstance.save(flush: true)) {
			render(view: "create", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), stdTimeWorkItemsInstance.id])
		redirect(action: "show", id: stdTimeWorkItemsInstance.id)
	}

	def show(Long id) {
		def stdTimeWorkItemsInstance = StdTimeWorkItems.get(id)
		if (!stdTimeWorkItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), id])
			redirect(action: "list")
			return
		}

		[stdTimeWorkItemsInstance: stdTimeWorkItemsInstance]
	}

	def edit(Long id) {
		def stdTimeWorkItemsInstance = StdTimeWorkItems.get(id)
		if (!stdTimeWorkItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), id])
			redirect(action: "list")
			return
		}

		[stdTimeWorkItemsInstance: stdTimeWorkItemsInstance]
	}

	def update(Long id, Long version) {
		def stdTimeWorkItemsInstance = StdTimeWorkItems.get(id)
		if (!stdTimeWorkItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (stdTimeWorkItemsInstance.version > version) {
				
				stdTimeWorkItemsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems')] as Object[],
				"Another user has updated this StdTimeWorkItems while you were editing")
				render(view: "edit", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
				return
			}
		}

        def cek = StdTimeWorkItems.createCriteria()
            def result = cek.list() {
            and{
                eq("workItems",WorkItems.findById(params.workItems?.id))
                eq("m041TanggalBerlaku",params.m041TanggalBerlaku)
                eq("m041Overhaul",Double.parseDouble(params.m041Overhaul))
                eq("m041RdanR",Double.parseDouble(params.m041RdanR))
                eq("m041Replace",Double.parseDouble(params.m041Replace))
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }

		stdTimeWorkItemsInstance.properties = params
        stdTimeWorkItemsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stdTimeWorkItemsInstance?.lastUpdProcess = "UPDATE"
        if (stdTimeWorkItemsInstance.getM041RdanR()<=0) {
            flash.message = message(code: 'default.error.created.stdTimeWorkItems.message', default: 'Inputan Removal & Reinstall harus lebih besar dari 0.')
            render(view: "edit", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }
        if (stdTimeWorkItemsInstance.getM041Replace()<=0) {
            flash.message = message(code: 'default.error.created.stdTimeWorkItems.message', default: 'Inputan Replace harus lebih besar dari 0.')
            render(view: "edit", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }
        if (stdTimeWorkItemsInstance.getM041Overhaul()<=0) {
            flash.message = message(code: 'default.error.created.stdTimeWorkItems.message', default: 'Inputan Overhaul harus lebih besar dari 0.')
            render(view: "edit", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
            return
        }
		if (!stdTimeWorkItemsInstance.save(flush: true)) {
			render(view: "edit", model: [stdTimeWorkItemsInstance: stdTimeWorkItemsInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), stdTimeWorkItemsInstance.id])
		redirect(action: "show", id: stdTimeWorkItemsInstance.id)
	}

	def delete(Long id) {
		def stdTimeWorkItemsInstance = StdTimeWorkItems.get(id)
		if (!stdTimeWorkItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), id])
			redirect(action: "list")
			return
		}

		try {
            stdTimeWorkItemsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stdTimeWorkItemsInstance?.lastUpdProcess = "DELETE"
			stdTimeWorkItemsInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'stdTimeWorkItems.label', default: 'StdTimeWorkItems'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(StdTimeWorkItems, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(StdTimeWorkItems, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
