package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SerialController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    def generateCodeService

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Serial.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            eq("staDel", "0")
            section{
                eq("staDel","0")
            }
            if (params."sCriteria_section") {
                section{
                    ilike("m051NamaSection", "%" + (params."sCriteria_section" as String) + "%")
                }
                //eq("section", params."sCriteria_section")
            }

            if (params."sCriteria_m052ID") {
                ilike("m052ID", "%" + (params."sCriteria_m052ID" as String) + "%")
            }

            if (params."sCriteria_m052NamaSerial") {
                ilike("m052NamaSerial", "%" + (params."sCriteria_m052NamaSerial" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    section: it.section?.m051NamaSection,

                    m052ID: it.m052ID,

                    m052NamaSerial: it.m052NamaSerial,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [serialInstance: new Serial(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def serialInstance = new Serial(params)
        def section = Section.findById(params.section.id)
        serialInstance?.m052ID = generateCodeService.codeGenerateSequence("M052_ID",null)
        serialInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        serialInstance?.lastUpdProcess = "INSERT"
        serialInstance?.setStaDel('0')
        if(Serial.findByM052NamaSerialAndSectionAndStaDel(params.m052NamaSerial,section,"0")!=null){
            flash.message = "Data sudah ada"
            render(view: "create", model: [serialInstance: serialInstance])
            return
        }

        if (!serialInstance.save(flush: true)) {
            render(view: "create", model: [serialInstance: serialInstance])
            return
        }
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("M052_ID",null,serialInstance?.m052ID)

        flash.message = message(code: 'default.created.message', args: [message(code: 'serial.label', default: 'Serial'), serialInstance.id])
        redirect(action: "show", id: serialInstance.id)
    }

    def show(Long id) {
        def serialInstance = Serial.get(id)
        if (!serialInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serial.label', default: 'Serial'), id])
            redirect(action: "list")
            return
        }

        [serialInstance: serialInstance]
    }

    def edit(Long id) {
        def serialInstance = Serial.get(id)
        if (!serialInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serial.label', default: 'Serial'), id])
            redirect(action: "list")
            return
        }

        [serialInstance: serialInstance]
    }

    def update(Long id, Long version) {
        def serialInstance = Serial.get(id)
        def section = Section.findById(params.section.id)
        if(Serial.findByM052NamaSerialAndSectionAndStaDel(params.m052NamaSerial,section,"0")!=null){
            flash.message = "Data sudah ada"
            render(view: "create", model: [serialInstance: serialInstance])
            return
        }
        if (!serialInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serial.label', default: 'Serial'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (serialInstance.version > version) {

                serialInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'serial.label', default: 'Serial')] as Object[],
                        "Another user has updated this Serial while you were editing")
                render(view: "edit", model: [serialInstance: serialInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        serialInstance.properties = params
        serialInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        serialInstance?.lastUpdProcess = "UPDATE"

        if (!serialInstance.save(flush: true)) {
            render(view: "edit", model: [serialInstance: serialInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'serial.label', default: 'Serial'), serialInstance.id])
        redirect(action: "show", id: serialInstance.id)
    }

    def delete(Long id) {
        def serialInstance = Serial.get(id)
        if (!serialInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serial.label', default: 'Serial'), id])
            redirect(action: "list")
            return
        }

        try {
            serialInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            serialInstance?.lastUpdProcess = "DELETE"
            serialInstance?.setStaDel('1')
            serialInstance?.lastUpdated = datatablesUtilService?.syncTime()
            serialInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'serial.label', default: 'Serial'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'serial.label', default: 'Serial'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Serial, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Serial, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
