package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class KodeKotaNoPolController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = KodeKotaNoPol.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m116ID"){
				ilike("m116ID","%" + (params."sCriteria_m116ID" as String) + "%")
			}
	
			if(params."sCriteria_m116NamaKota"){
				ilike("m116NamaKota","%" + (params."sCriteria_m116NamaKota" as String) + "%")
			}
	
			ilike("staDel",'0')

			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m116ID: it.m116ID.toUpperCase(),
			
						m116NamaKota: it.m116NamaKota,
			
						staDel: it.staDel,
						
						createdBy: it.createdBy,
						
						updatedBy: it.updatedBy,
						
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[kodeKotaNoPolInstance: new KodeKotaNoPol(params)]
	}

	def save() {
		def kodeKotaNoPolInstance = new KodeKotaNoPol(params)
//        def kota = KabKota.findById(params.m116NamaKota)?.m002NamaKabKota
//        kodeKotaNoPolInstance?.m116NamaKota = kota
        def cek = KodeKotaNoPol.createCriteria()
        def result = cek.list() {
            or{
                eq("m116ID",kodeKotaNoPolInstance.m116ID?.trim(), [ignoreCase: true])
                eq("m116NamaKota",kodeKotaNoPolInstance.m116NamaKota.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }

        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "create", model: [kodeKotaNoPolInstance: kodeKotaNoPolInstance])
            return
        }

        kodeKotaNoPolInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		kodeKotaNoPolInstance?.lastUpdProcess = "INSERT"
		kodeKotaNoPolInstance?.setStaDel('0')

		if (!kodeKotaNoPolInstance.save(flush: true)) {
			render(view: "create", model: [kodeKotaNoPolInstance: kodeKotaNoPolInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), kodeKotaNoPolInstance?.getM116ID()])
		redirect(action: "show", id: kodeKotaNoPolInstance.id)
	}

	def show(Long id) {
		def kodeKotaNoPolInstance = KodeKotaNoPol.get(id)
		if (!kodeKotaNoPolInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), id])
			redirect(action: "list")
			return
		}

		[kodeKotaNoPolInstance: kodeKotaNoPolInstance]
	}

	def edit(Long id) {
		def kodeKotaNoPolInstance = KodeKotaNoPol.get(id)
//        def kota = KabKota.findByM002NamaKabKota(kodeKotaNoPolInstance.m116NamaKota)?.id
		if (!kodeKotaNoPolInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), id])
			redirect(action: "list")
			return
		}

		[kodeKotaNoPolInstance: kodeKotaNoPolInstance]
	}

	def update(Long id, Long version) {
		def kodeKotaNoPolInstance = KodeKotaNoPol.get(id)
		if (!kodeKotaNoPolInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (kodeKotaNoPolInstance.version > version) {
				
				kodeKotaNoPolInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol')] as Object[],
				"Another user has updated this KodeKotaNoPol while you were editing")
				render(view: "edit", model: [kodeKotaNoPolInstance: kodeKotaNoPolInstance])
				return
			}
		}
//        def kota = KabKota.findById(params.m116NamaKota)?.m002NamaKabKota
        def cek = KodeKotaNoPol.createCriteria()
        def result = cek.list() {
            and{
                eq("m116ID",params.m116ID, [ignoreCase: true])
            }
            eq("staDel",'0')
        }
        def cek2 = KodeKotaNoPol.createCriteria()
        def result2 = cek2.list() {
            and{
                eq("m116NamaKota",params.m116NamaKota.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }
        if(result && KodeKotaNoPol.findByM116IDAndStaDel(params.m116ID,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Kode Sudah Ada')
            render(view: "edit", model: [kodeKotaNoPolInstance: kodeKotaNoPolInstance])
            return
        }
        if(result2 && KodeKotaNoPol.findByM116NamaKotaAndStaDel(params.m116NamaKota,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "edit", model: [kodeKotaNoPolInstance: kodeKotaNoPolInstance])
            return
        }
		kodeKotaNoPolInstance.properties = params
		kodeKotaNoPolInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		kodeKotaNoPolInstance?.lastUpdProcess = "UPDATE"
		if (!kodeKotaNoPolInstance.save(flush: true)) {
			render(view: "edit", model: [kodeKotaNoPolInstance: kodeKotaNoPolInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), kodeKotaNoPolInstance.id])
		redirect(action: "show", id: kodeKotaNoPolInstance.id)
	}

	def delete(Long id) {
		
		def kodeKotaNoPolInstance = KodeKotaNoPol.get(id)
		if (!kodeKotaNoPolInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), id])
			redirect(action: "list")
			return
		}

		try {
			//kodeKotaNoPolInstance.delete(flush: true)
			kodeKotaNoPolInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			kodeKotaNoPolInstance?.lastUpdProcess = "DELETE"
			kodeKotaNoPolInstance?.setStaDel('1')
			kodeKotaNoPolInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'kodeKotaNoPol.label', default: 'KodeKotaNoPol'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(KodeKotaNoPol, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(KodeKotaNoPol, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
