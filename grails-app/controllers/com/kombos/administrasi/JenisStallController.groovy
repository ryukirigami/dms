package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class JenisStallController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = JenisStall.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")

            if(params."sCriteria_m021NamaJenisStall"){
				ilike("m021NamaJenisStall","%" + (params."sCriteria_m021NamaJenisStall" as String) + "%")
			}
	
			if(params."sCriteria_m021ID"){
				ilike("m021ID","%" + (params."sCriteria_m021ID" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
                    id: it.id,

                    m021NamaJenisStall: it.m021NamaJenisStall,

                    m021ID: it.m021ID,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[jenisStallInstance: new JenisStall(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def jenisStallInstance = new JenisStall(params)
        jenisStallInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        jenisStallInstance?.lastUpdProcess = "INSERT"
        jenisStallInstance?.setStaDel('0')
        def cek = JenisStall.createCriteria().list() {
            and{
                eq("m021NamaJenisStall",jenisStallInstance.m021NamaJenisStall?.trim(), [ignoreCase: true])
                eq("staDel",jenisStallInstance.staDel)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [jenisStallInstance: jenisStallInstance])
            return
        }

		if (!jenisStallInstance.save(flush: true)) {
			render(view: "create", model: [jenisStallInstance: jenisStallInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), jenisStallInstance.id])
		redirect(action: "show", id: jenisStallInstance.id)
	}

	def show(Long id) {
		def jenisStallInstance = JenisStall.get(id)
		if (!jenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), id])
			redirect(action: "list")
			return
		}

		[jenisStallInstance: jenisStallInstance]
	}

	def edit(Long id) {
		def jenisStallInstance = JenisStall.get(id)
		if (!jenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), id])
			redirect(action: "list")
			return
		}

		[jenisStallInstance: jenisStallInstance]
	}

	def update(Long id, Long version) {
		def jenisStallInstance = JenisStall.get(id)
		if (!jenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (jenisStallInstance.version > version) {
				
				jenisStallInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'jenisStall.label', default: 'JenisStall')] as Object[],
				"Another user has updated this JenisStall while you were editing")
				render(view: "edit", model: [jenisStallInstance: jenisStallInstance])
				return
			}
		}

        def cek = JenisStall.createCriteria()
        def result = cek.list() {
            and{
            eq("m021NamaJenisStall",jenisStallInstance.m021NamaJenisStall?.trim(), [ignoreCase: true])
            eq("staDel",jenisStallInstance.staDel)
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [jenisStallInstance: jenisStallInstance])
            return
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        jenisStallInstance.properties = params
        jenisStallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        jenisStallInstance?.lastUpdProcess = "UPDATE"

        if (!jenisStallInstance.save(flush: true)) {
			render(view: "edit", model: [jenisStallInstance: jenisStallInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), jenisStallInstance.id])
		redirect(action: "show", id: jenisStallInstance.id)
	}

	def delete(Long id) {
		def jenisStallInstance = JenisStall.get(id)
		if (!jenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), id])
			redirect(action: "list")
			return
		}

		try {
            jenisStallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            jenisStallInstance?.lastUpdProcess = "DELETE"
            jenisStallInstance?.setStaDel('1')
            jenisStallInstance?.lastUpdated = datatablesUtilService?.syncTime()
            jenisStallInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'jenisStall.label', default: 'JenisStall'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(JenisStall, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(JenisStall, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
