package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MappingJobPanelController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = MappingJobPanel.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            operation{
                eq("staDel", "0")
            }
            if (params."sCriteria_operation") {
                operation{
                    ilike("m053NamaOperation", "%" + (params."sCriteria_operation" as String) + "%")
                }
                //eq("operation", params."sCriteria_operation")
            }

            if (params."sCriteria_masterpanel") {
                masterpanel{
                    ilike("m094NamaPanel", "%" + (params."sCriteria_masterpanel" as String) + "%")
                }
                //eq("masterpanel", params."sCriteria_masterpanel")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    operation: it.operation?.serial?.m052NamaSerial+' - '+it.operation?.m053NamaOperation,

                    masterpanel: it.masterpanel?.m094NamaPanel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [mappingJobPanelInstance: new MappingJobPanel(params)]
    }

    def save() {
        def mappingJobPanelInstance = new MappingJobPanel(params)
        mappingJobPanelInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingJobPanelInstance?.lastUpdProcess = "INSERT"
        if(MappingJobPanel.findByOperationAndMasterpanel(mappingJobPanelInstance?.operation,mappingJobPanelInstance?.masterpanel)!=null){
            flash.message = '* Data Sudah Ada'
            render(view: "create",model: [mappingJobPanelInstance: mappingJobPanelInstance])
            return
        }
        if (!mappingJobPanelInstance.save(flush: true)) {
            render(view: "create", model: [mappingJobPanelInstance: mappingJobPanelInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), mappingJobPanelInstance.id])
        redirect(action: "show", id: mappingJobPanelInstance.id)
    }

    def show(Long id) {
        def mappingJobPanelInstance = MappingJobPanel.get(id)
        if (!mappingJobPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), id])
            redirect(action: "list")
            return
        }

        [mappingJobPanelInstance: mappingJobPanelInstance]
    }

    def edit(Long id) {
        def mappingJobPanelInstance = MappingJobPanel.get(id)
        if (!mappingJobPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), id])
            redirect(action: "list")
            return
        }

        [mappingJobPanelInstance: mappingJobPanelInstance]
    }

    def update(Long id, Long version) {
        def mappingJobPanelInstance = MappingJobPanel.get(id)
        if (!mappingJobPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (mappingJobPanelInstance.version > version) {

                mappingJobPanelInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel')] as Object[],
                        "Another user has updated this MappingJobPanel while you were editing")
                render(view: "edit", model: [mappingJobPanelInstance: mappingJobPanelInstance])
                return
            }
        }
        if(MappingJobPanel.findByOperationAndMasterpanel(Operation.findById(params.operation.id),MasterPanel.findById(params.masterpanel.id))!=null){
            if(MappingJobPanel.findByOperationAndMasterpanel(Operation.findById(params.operation.id),MasterPanel.findById(params.masterpanel.id)).id != id){
                flash.message = '* Data Sudah Ada'
                render(view: "edit",model: [mappingJobPanelInstance: mappingJobPanelInstance])
                return
            }
        }

        mappingJobPanelInstance.properties = params
        mappingJobPanelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingJobPanelInstance?.lastUpdProcess = "UPDATE"

        if (!mappingJobPanelInstance.save(flush: true)) {
            render(view: "edit", model: [mappingJobPanelInstance: mappingJobPanelInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), mappingJobPanelInstance.id])
        redirect(action: "show", id: mappingJobPanelInstance.id)
    }

    def delete(Long id) {
        def mappingJobPanelInstance = MappingJobPanel.get(id)
        if (!mappingJobPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), id])
            redirect(action: "list")
            return
        }

        try {
            mappingJobPanelInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'mappingJobPanel.label', default: 'Mapping Job Panel'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MappingJobPanel, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(MappingJobPanel, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
