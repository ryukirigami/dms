package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class HargaJobSubletController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = HargaJobSublet.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_operation") {

                eq("operation",Operation.findByM053NamaOperationIlike("%"+params."sCriteria_operation"+"%"))
            }

            if (params."sCriteria_baseModel") {

                eq("baseModel",BaseModel.findByM102NamaBaseModelIlike("%"+params."sCriteria_baseModel"+"%"))
            }

            if (params."sCriteria_section") {

                eq("section",Section.findByM051NamaSectionIlike("%"+params."sCriteria_section"+"%"))
            }

            if (params."sCriteria_serial") {

                eq("serial",Serial.findByM052NamaSerialIlike("%"+params."sCriteria_serial"+"%"))
            }

            if (params."sCriteria_t116TMT") {
                ge("t116TMT", params."sCriteria_t116TMT")
                lt("t116TMT", params."sCriteria_t116TMT" + 1)
            }

            if (params."sCriteria_t116Harga") {
                eq("t116Harga", Double.parseDouble(params."sCriteria_t116Harga"))
            }


                ilike("staDel", "0")


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    operation: it.operation.m053NamaOperation,

                    baseModel: it.baseModel.m102NamaBaseModel,

                    section: it.section.m051NamaSection,

                    serial: it.serial.m052NamaSerial,

                    t116TMT: it.t116TMT ? it.t116TMT.format(dateFormat) : "",

                    t116Harga: it.t116Harga,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [hargaJobSubletInstance: new HargaJobSublet(params)]
    }

    def save() {

        def hargaJobSubletInstance = new HargaJobSublet(params)
        hargaJobSubletInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        hargaJobSubletInstance?.lastUpdProcess = "INSERT"
        hargaJobSubletInstance.staDel = '0'
        def cek = HargaJobSublet.createCriteria().list() {
            and{
                eq("section",hargaJobSubletInstance.section)
                eq("serial",hargaJobSubletInstance.serial)
                eq("operation",hargaJobSubletInstance.operation)
                eq("baseModel",hargaJobSubletInstance.baseModel)
                eq("t116TMT",hargaJobSubletInstance.t116TMT)
                eq("t116Harga",hargaJobSubletInstance.t116Harga)
                eq("staDel","0")

            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [hargaJobSubletInstance: hargaJobSubletInstance])
            return
        }
        if (!hargaJobSubletInstance.save(flush: true)) {
            render(view: "create", model: [hargaJobSubletInstance: hargaJobSubletInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet'), hargaJobSubletInstance.id])
        redirect(action: "show", id: hargaJobSubletInstance.id)
    }

    def show(Long id) {
        def hargaJobSubletInstance = HargaJobSublet.get(id)
        if (!hargaJobSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet'), id])
            redirect(action: "list")
            return
        }

        [hargaJobSubletInstance: hargaJobSubletInstance]
    }

    def edit(Long id) {
        def hargaJobSubletInstance = HargaJobSublet.get(id)
        if (!hargaJobSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet'), id])
            redirect(action: "list")
            return
        }

        [hargaJobSubletInstance: hargaJobSubletInstance]
    }

    def update(Long id, Long version) {
        params.t116Harga=params.t116Harga.replace(",","")
        def hargaJobSubletInstance = HargaJobSublet.get(id)
        if (!hargaJobSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (hargaJobSubletInstance.version > version) {

                hargaJobSubletInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet')] as Object[],
                        "Another user has updated this HargaJobSublet while you were editing")
                render(view: "edit", model: [hargaJobSubletInstance: hargaJobSubletInstance])
                return
            }
        }

        def cek = HargaJobSublet.createCriteria()
        def result = cek.list() {
            and{
                eq("section",Section.findById(params.section?.id))
                eq("serial",Serial.findById(params.serial?.id))
                eq("operation",Operation.findById(params.operation?.id))
                eq("baseModel",BaseModel.findById(params.baseModel?.id))
                eq("t116TMT",params.t116TMT)
                eq("t116Harga",Double.parseDouble(params.t116Harga))
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [hargaJobSubletInstance: hargaJobSubletInstance])
            return
        }

        hargaJobSubletInstance.properties = params
        hargaJobSubletInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        hargaJobSubletInstance?.lastUpdProcess = "UPDATE"
        if (!hargaJobSubletInstance.save(flush: true)) {
            render(view: "edit", model: [hargaJobSubletInstance: hargaJobSubletInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'hargaJobSublet.label', default: 'Harga Job Sublet'), hargaJobSubletInstance.id])
        redirect(action: "show", id: hargaJobSubletInstance.id)
    }

    def delete(Long id) {
        def hargaJobSubletInstance = HargaJobSublet.get(id)
        if (!hargaJobSubletInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet'), id])
            redirect(action: "list")
            return
        }

        try {
            hargaJobSubletInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            hargaJobSubletInstance?.lastUpdProcess = "DELETE"
            hargaJobSubletInstance.staDel = '1'
            hargaJobSubletInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'hargaJobSublet.label', default: 'HargaJobSublet'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(HargaJobSublet, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(HargaJobSublet, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
