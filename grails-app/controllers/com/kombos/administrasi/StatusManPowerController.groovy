package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StatusManPowerController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = StatusManPower.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_namaManPower"){
				eq("namaManPower",NamaManPower.findByT015NamaLengkapIlike("%"+params."sCriteria_namaManPower"+"%"))
			}

			if(params."sCriteria_t023Tmt"){
				ge("t023Tmt",params."sCriteria_t023Tmt")
				lt("t023Tmt",params."sCriteria_t023Tmt" + 1)
			}
	
			if(params."sCriteria_t023Sta"){
				ilike("t023Sta","%" + (params."sCriteria_t023Sta" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						namaManPower: it.namaManPower.t015NamaLengkap,
			
						t023Tmt: it.t023Tmt?it.t023Tmt.format(dateFormat):"",
			
						t023Sta: it.t023Sta,
						
						createdBy: it.createdBy,
						
						updatedBy: it.updatedBy,
						
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[statusManPowerInstance: new StatusManPower(params)]
	}

	def save() {
		def statusManPowerInstance = new StatusManPower(params)
        def cek = StatusManPower.findByNamaManPowerAndT023TmtAndT023Sta(NamaManPower.findById(params.namaManPower.id), statusManPowerInstance?.t023Tmt, statusManPowerInstance?.t023Sta)
        if(cek){
            flash.message = " Data Sudah Ada"
            render(view: "create", model: [statusManPowerInstance: statusManPowerInstance])
            return
        }
		statusManPowerInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		statusManPowerInstance?.lastUpdProcess = "INSERT"
		if (!statusManPowerInstance.save(flush: true)) {
			render(view: "create", model: [statusManPowerInstance: statusManPowerInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), statusManPowerInstance.id])
		redirect(action: "show", id: statusManPowerInstance.id)
	}

	def show(Long id) {
		def statusManPowerInstance = StatusManPower.get(id)
		if (!statusManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), id])
			redirect(action: "list")
			return
		}

		[statusManPowerInstance: statusManPowerInstance]
	}

	def edit(Long id) {
		def statusManPowerInstance = StatusManPower.get(id)
		if (!statusManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), id])
			redirect(action: "list")
			return
		}

		[statusManPowerInstance: statusManPowerInstance]
	}

	def update(Long id, Long version) {
		def statusManPowerInstance = StatusManPower.get(id)
		if (!statusManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (statusManPowerInstance.version > version) {
				
				statusManPowerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'statusManPower.label', default: 'StatusManPower')] as Object[],
				"Another user has updated this StatusManPower while you were editing")
				render(view: "edit", model: [statusManPowerInstance: statusManPowerInstance])
				return
			}
		}
        def cek = StatusManPower.findByNamaManPowerAndT023TmtAndT023Sta(NamaManPower.findById(params.namaManPower.id), params.t023Tmt, params.t023Sta)
        if(cek && cek.id != id){
            flash.message = " Data Sudah Ada"
            render(view: "edit", model: [statusManPowerInstance: statusManPowerInstance])
            return
        }
		statusManPowerInstance.properties = params
		statusManPowerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		statusManPowerInstance?.lastUpdProcess = "UPDATE"
		if (!statusManPowerInstance.save(flush: true)) {
			render(view: "edit", model: [statusManPowerInstance: statusManPowerInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), statusManPowerInstance.id])
		redirect(action: "show", id: statusManPowerInstance.id)
	}

	def delete(Long id) {
		def statusManPowerInstance = StatusManPower.get(id)
		if (!statusManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), id])
			redirect(action: "list")
			return
		}

		try {
			statusManPowerInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'statusManPower.label', default: 'StatusManPower'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(StatusManPower, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(StatusManPower, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
