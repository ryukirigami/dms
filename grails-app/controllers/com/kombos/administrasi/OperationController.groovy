package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.reception.Appointment
import com.kombos.woinformation.Actual
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class OperationController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", generateSerial: "GET"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Operation.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            section{
                eq("staDel","0")
            }
            serial{
                eq("staDel","0")
            }
            if (params."sCriteria_section") {
                section{
                    ilike("m051NamaSection", "%" + (params."sCriteria_section" as String) + "%")
                }
            }

            if (params."sCriteria_serial") {
                serial{
                    ilike("m052NamaSerial", "%" + (params."sCriteria_serial" as String) + "%")
                }
            }

            if (params."sCriteria_m053Id") {
                    ilike("m053Id", "%" + (params."sCriteria_m053Id" as String) + "%")
            }

            if (params."sCriteria_m053NamaOperation") {
                    ilike("m053NamaOperation", "%" + (params."sCriteria_m053NamaOperation" as String) + "%")
            }

            if (params."sCriteria_m053StaLift") {
                   ilike("m053StaLift", "%" + (params."sCriteria_m053StaLift" as String) + "%")
            }

            if (params."sCriteria_m053StaPaket") {
                ilike("m053StaPaket", "%" + (params."sCriteria_m053StaPaket" as String) + "%")
            }

            if (params."sCriteria_kategoriJob") {
                if(params.reception){
                    kategoriJob{
                        eq("id",params."sCriteria_kategoriJob".toLong())
                    }
                }else{
                    kategoriJob{
                        ilike("m055KategoriJob", "%" + (params."sCriteria_kategoriJob" as String) + "%")
                    }
                }
                //eq("kategoriJob", params."sCriteria_kategoriJob")
            }

            if (params."sCriteria_m053StaPaint") {
                ilike("m053StaPaint", "%" + (params."sCriteria_m053StaPaint" as String) + "%")
            }

            if (params."sCriteria_m053Ket") {
                ilike("m053Ket", "%" + (params."sCriteria_m053Ket" as String) + "%")
            }

            if (params."sCriteria_m053JobsId") {
                ilike("m053JobsId", "%" + (params."sCriteria_m053JobsId" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        HistoryCustomerVehicle histCV = new HistoryCustomerVehicle()
        if(params?.reception){
            def noDepan = "";
            if(params?.appointment){
                noDepan = params?.noDepan;
            }else {
                def koKT = KodeKotaNoPol.get(params?.noDepan?.toLong())
                noDepan = koKT?.m116ID
            }
            def fullNopols = noDepan+" "+params?.noTengah+" "+params?.noBelakang

            histCV = HistoryCustomerVehicle.createCriteria().get {eq("fullNoPol",fullNopols,[ignoreCase: true]);eq("staDel","0");maxResults(1)}
        }

        def rows = []
        results.each {
            def ops = it

            def Viewrate = 0
            if(params?.reception){
                if(histCV){

                    FlatRate r = FlatRate.findByOperationAndBaseModelAndStaDelAndT113TMTLessThan(ops,histCV?.fullModelCode?.baseModel,"0",new Date()+1,[sort: 'id',order: 'desc'])
                    if(r){
                        Viewrate = r?.t113FlatRate
                    }
                }
            }

            rows << [

                    id: it?.id,

                    section: it?.section?.m051NamaSection,

                    serial: it?.serial?.m052NamaSerial,

                    m053Id: it?.m053Id,

                    m053NamaOperation: it?.m053NamaOperation,

                    rate : Viewrate ? Viewrate:0,

                    m053StaLift: it?.m053StaLift=="1"?"Ya":"Tidak",

                    m053StaPaket: it?.m053StaPaket=="1"?"Ya":"Tidak",

                    kategoriJob: it?.kategoriJob?.m055KategoriJob,

                    m053StaPaint: it?.m053StaPaint=="1"?"Ya":"Tidak",

                    m053Ket: it?.m053Ket,

                    m053JobsId: it?.m053JobsId,

                    staDel: it?.staDel,

                    createdBy: it?.createdBy,

                    updatedBy: it?.updatedBy,

                    lastUpdProcess: it?.lastUpdProcess,
//                    rate:rate
// ian
            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }


    def create() {
        [operationInstance: new Operation(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def operationInstance = new Operation(params)
        operationInstance?.m053JobsId = operationInstance?.section?.m051ID+operationInstance?.serial?.m052ID+operationInstance?.m053Id
        operationInstance?.setStaDel('0')

        def cek = Operation.createCriteria()
        def result = cek.list() {
            and{
                eq("section",operationInstance.section)
                eq("serial",operationInstance.serial)
                eq("m053Id",operationInstance.m053Id?.trim(), [ignoreCase: true])
                eq("m053NamaOperation",operationInstance.m053NamaOperation?.trim(), [ignoreCase: true])
                eq("m053StaLift",operationInstance.m053StaLift?.trim(), [ignoreCase: true])
                eq("m053StaPaket",operationInstance.m053StaPaket?.trim(), [ignoreCase: true])
                eq("kategoriJob",operationInstance.kategoriJob)
                eq("m053StaPaint",operationInstance.m053StaPaint?.trim(), [ignoreCase: true])
                eq("m053Ket",operationInstance.m053Ket?.trim(), [ignoreCase: true])
            }
        }

        //find(operationInstance)
        if(!result){
            operationInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            operationInstance?.lastUpdProcess = "INSERT"

            if (!operationInstance.save(flush: true)) {
                render(view: "create", model: [operationInstance: operationInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data has been used.')
            render(view: "create", model: [operationInstance: operationInstance])
            return
        }



        flash.message = message(code: 'default.created.message', args: [message(code: 'operation.label', default: 'Repair'), operationInstance.id])
        redirect(action: "show", id: operationInstance.id)
    }

    def generateSerial(Long id){
        String result = ""
        def serialInstance
        if(id){
            serialInstance = Serial.findAllBySectionAndStaDel(Section.findById(id),"0")
            serialInstance?.each {
                result+='<option value="'+it.id+'">'+it.m052NamaSerial+'</option>'
            }
        } else {
            serialInstance = Serial.createCriteria().list {
                section{
                    eq("staDel","0")
                }

                eq("staDel","0")

            }
            serialInstance?.each {
                result+='<option value="'+it.id+'">'+it.m052NamaSerial+'</option>'
            }
        }

        render result
    }

    def show(Long id) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'Repair'), id])
            redirect(action: "list")
            return
        }

        [operationInstance: operationInstance]
    }

    def edit(Long id) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'Repair'), id])
            redirect(action: "list")
            return
        }

        [operationInstance: operationInstance]
    }

    def update(Long id, Long version) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'Repair'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (operationInstance.version > version) {

                operationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'operation.label', default: 'Repair')] as Object[],
                        "Another user has updated this Operation while you were editing")
                render(view: "edit", model: [operationInstance: operationInstance])
                return
            }
        }

        //operationInstance?.m053JobsId = operationInstance?.m053Id
        if(Operation.findByM053IdAndM053NamaOperationAndStaDel(params.m053Id,params.m053NamaOperation,'0')!=null){
            if(Operation.findByM053IdAndM053NamaOperationAndStaDel(params.m053Id,params.m053NamaOperation,'0').id != id){

                flash.message = '* Data sudah ada'
                render(view: "edit", model: [operationInstance: operationInstance])
                return
            }
        }
//        if(Operation.findByM053NamaOperationAndStaDel(params.m053NamaOperation,'0')!=null){
//            if(Operation.findByM053NamaOperationAndStaDel(params.m053NamaOperation,'0').id != id){
//
//                flash.message = '* Nama Repair sudah ada'
//                render(view: "edit", model: [operationInstance: operationInstance])
//                return
//            }
//        }
//        def cek1 = Operation.createCriteria()
//        def result1 = cek1.list() {
//            and{
//                eq("m053Id",params.m053Id?.trim(), [ignoreCase: true])
//                eq("staDel","0")
//            }
//        }
//        if(result1){
//            flash.message = "Kode Repair sudah ada"
//            render(view: "edit", model: [operationInstance: operationInstance])
//            return
//        }
//
//        def cek2 = Operation.createCriteria()
//        def result2 = cek2.list() {
//            and{
//                eq("m053NamaOperation",params.m053NamaOperation?.trim(), [ignoreCase: true])
//                eq("staDel","0")
//            }
//        }
//        if(result2){
//            flash.message = "Nama Repair sudah ada"
//            render(view: "edit", model: [operationInstance: operationInstance])
//            return
//        }

        def cek = Operation.createCriteria()
        def result = cek.list() {
            and{
                ne("id",id)
//                eq("m053Id",params.m053Id?.trim(), [ignoreCase: true])
                eq("section",Section.findById(Long.parseLong(params."section.id")))
                eq("serial",Serial.findById(Long.parseLong(params."serial.id")))
                eq("m053NamaOperation",params.m053NamaOperation?.trim(), [ignoreCase: true])
                eq("m053Id",params.m053Id?.trim(), [ignoreCase: true])
                eq("m053StaLift",params.m053StaLift?.trim(), [ignoreCase: true])
                eq("m053StaPaket",params.m053StaPaket?.trim(), [ignoreCase: true])
                eq("kategoriJob",KategoriJob.findById(Long.parseLong(params."kategoriJob.id")))
                eq("m053StaPaint",params.m053StaPaint?.trim(), [ignoreCase: true])
                eq("m053Ket",params.m053Ket?.trim(), [ignoreCase: true])
            }
        }

        //find(operationInstance)
        if(!result){
            params?.lastUpdated = datatablesUtilService?.syncTime()
            operationInstance.properties = params
            operationInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            operationInstance?.lastUpdProcess = "UPDATE"
            if (!operationInstance.save(flush: true)) {
                render(view: "edit", model: [operationInstance: operationInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.update.operation.error.message', default: 'Duplicate Data Repair')
            render(view: "edit", model: [operationInstance: operationInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'operation.label', default: 'Repair'), operationInstance.id])
        redirect(action: "show", id: operationInstance.id)
    }

    def delete(Long id) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'Repair'), id])
            redirect(action: "list")
            return
        }

        try {
            operationInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            operationInstance?.lastUpdProcess = "DELETE"
            operationInstance?.setStaDel('1')
            operationInstance?.lastUpdated = datatablesUtilService?.syncTime()
            operationInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'operation.label', default: 'Repair'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'operation.label', default: 'Repair'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Operation, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Operation, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def getOnProgressOperation(){
        def idJPB = params.idJPB
        def bPOrGR = params.bPOrGR // 0 = BP, 1 = GR
        String info = params.info

        /*if(info.equalsIgnoreCase("BREAK")){

        }else if(info.equalsIgnoreCase("V1")){

        }else if(info.equalsIgnoreCase("V")){

        }else if(info.equalsIgnoreCase("W1")){

        }else if(info.equalsIgnoreCase("W")){

        }else if(info.equalsIgnoreCase("X")){

        }else if(info.equalsIgnoreCase("X1")){

        }else if(info.equalsIgnoreCase("Y")){

        }else if(info.equalsIgnoreCase("Z")){

        }else if(info.equalsIgnoreCase("Z1")){

        }
*/

        def jpb = JPB.get(idJPB)
        def reception = jpb?.reception

        def noWO = reception?.t401NoWO
        def historyCustomer = reception?.historyCustomerVehicle

        def kodeNoPol = historyCustomer?.kodeKotaNoPol?.m116ID
        def nopolTengah = historyCustomer?.t183NoPolTengah
        def nopolBelakang = historyCustomer?.t183NoPolBelakang
        def nopolFull = kodeNoPol + " " + nopolTengah + " " + nopolBelakang
        def level = jpb?.reception?.repairDifficulty?.m042Tipe

        def namaJob = reception?.operation?.m053NamaOperation
        def stall = jpb?.stall?.m022NamaStall
        def okCancelReschedule = jpb?.t451staOkCancelReSchedule
        def tambahanWaktu = jpb?.t451StaTambahWaktu
        def alasanReSchedule = jpb?.t451AlasanReSchedule
        def actual = Actual.findByJpb(jpb)
        def start = actual?.t452TglJam?.format("dd/MM/yyyy HH:mm")


        def appointment = Appointment.findByJpb(jpb)
        def bufferTime = new Date()
        if(bPOrGR == "0"){
            bufferTime = GeneralParameter.findByCompanyDealer(session.userCompanyDealer)?.m000BufferDeliveryTimeBP
        }else{
            bufferTime = GeneralParameter.findByCompanyDealer(session.userCompanyDealer)?.m000BufferDeliveryTimeGR
        }

        def targetFinishDate = appointment?.t301TglJamPenyerahann
        def targetFinishTime =  targetFinishDate.getTime() - bufferTime.getTime()

        Date date = new Date(targetFinishTime);
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String target = formatter.format(date);


        def targetFinish = targetFinishDate.format("dd/MM/yyyy") + " - " + target
        def deliveryTime = targetFinishDate.format("dd/MM/yyyy HH:mm")

        def res = [:]

        res.put("noWO", noWO)
        res.put("noPol", nopolFull)
        res.put("namaJob", namaJob)
        res.put("level", level)
        res.put("currProgress", stall)
        res.put("okCancelReschedule", okCancelReschedule )
        res.put("alasan", alasanReSchedule)
        res.put("tambahWaktu", tambahanWaktu)
        res.put("start", start)
        res.put("targetFinish", targetFinish)
        res.put("deliveryTime", deliveryTime)

        render res as JSON
    }
}
