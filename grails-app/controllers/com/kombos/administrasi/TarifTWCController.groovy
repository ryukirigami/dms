package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class TarifTWCController {
    def conversi = new Konversi()
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		params.companyDealer = session.userCompanyDealer
		
		def c = TarifTWC.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel", "0")
			if(params."sCriteria_companyDealer"){
                companyDealer{
                    ilike("m011NamaWorkshop","%" + (params."sCriteria_companyDealer") + "%")
                }
			}

			if(params."sCriteria_kategoriKendaraan"){
                kategoriKendaraan{
                    ilike("m101NamaKategori","%" + (params."sCriteria_kategoriKendaraan") + "%")
                }
			}

			if(params."sCriteria_t113aTMT"){
				ge("t113aTMT",params."sCriteria_t113aTMT")
				lt("t113aTMT",params."sCriteria_t113aTMT" + 1)
			}
            String str = params."sCriteria_t113aTarifPerjamTWC"
			if(str){
				eq("t113aTarifPerjamTWC",Double.parseDouble(str?.replace(",","")))
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			if(!params.companyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
				eq("companyDealer", params.companyDealer)
			}

			switch(sortProperty){
                case "companyDealer":
                    companyDealer{
                        order("m011NamaWorkshop",sortDir)
                    }
                    break;
                case "kategoriKendaraan":
                    kategoriKendaraan{
                        order("m101NamaKategori",sortDir)
                    }
                    break;
                default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companyDealer: it.companyDealer?.m011NamaWorkshop,
			
						kategoriKendaraan: it.kategoriKendaraan?.m101NamaKategori,
			
						t113aTMT: it.t113aTMT?it.t113aTMT.format(dateFormat):"",
			
						t113aTarifPerjamTWC: conversi.toRupiah(it.t113aTarifPerjamTWC),
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[tarifTWCInstance: new TarifTWC(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def tarifTWCInstance = new TarifTWC(params)
        tarifTWCInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        tarifTWCInstance?.lastUpdProcess = "INSERT"
		tarifTWCInstance?.setStaDel('0')

        def cek = TarifTWC.createCriteria()
        def result = cek.list() {
            and{
                eq("companyDealer",tarifTWCInstance.companyDealer)
                eq("kategoriKendaraan",tarifTWCInstance.kategoriKendaraan)
                eq("t113aTMT",tarifTWCInstance.t113aTMT)
                eq("staDel","0")
                //eq("t113aTarifPerjamTWC",tarifTWCInstance.t113aTarifPerjamTWC)
            }
        }

        if(!result){
            tarifTWCInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            tarifTWCInstance?.lastUpdProcess = "INSERT"

            if (!tarifTWCInstance.save(flush: true)) {
                render(view: "create", model: [tarifTWCInstance: tarifTWCInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.tariftwc.error.message', default: 'Data sudah pernah dimasukkan.')
            render(view: "create", model: [tarifTWCInstance: tarifTWCInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), tarifTWCInstance.id])
		redirect(action: "show", id: tarifTWCInstance.id)
	}

	def show(Long id) {
		def tarifTWCInstance = TarifTWC.get(id)
		if (!tarifTWCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), id])
			redirect(action: "list")
			return
		}

		[tarifTWCInstance: tarifTWCInstance]
	}

	def edit(Long id) {
		def tarifTWCInstance = TarifTWC.get(id)
		if (!tarifTWCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), id])
			redirect(action: "list")
			return
		}

		[tarifTWCInstance: tarifTWCInstance]
	}

	def update(Long id, Long version) {
		def tarifTWCInstance = TarifTWC.get(id)
		if (!tarifTWCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (tarifTWCInstance.version > version) {
				
				tarifTWCInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'tarifTWC.label', default: 'Tarif TWC')] as Object[],
				"Another user has updated this Tarif TWC while you were editing")
				render(view: "edit", model: [tarifTWCInstance: tarifTWCInstance])
				return
			}
		}

//        def cek = TarifTWC.createCriteria()
//        def result = cek.list() {
//            eq("companyDealer",tarifTWCInstance.companyDealer)
//            eq("kategoriKendaraan",tarifTWCInstance.kategoriKendaraan)
//            eq("t113aTMT",tarifTWCInstance.t113aTMT)
//            eq("t113aTarifPerjamTWC",tarifTWCInstance.t113aTarifPerjamTWC)
//        }
//        if(result){
//            flash.message = "Data sudah ada"
//            render(view: "edit", model: [tarifTWCInstance: tarifTWCInstance])
//            return
//        }
//
//		tarifTWCInstance.properties = params
//        tarifTWCInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
//        tarifTWCInstance?.lastUpdProcess = "UPDATE"
//
//		if (!tarifTWCInstance.save(flush: true)) {
//			render(view: "edit", model: [tarifTWCInstance: tarifTWCInstance])
//			return
//		}

        def cek = TarifTWC.createCriteria()
        String str = params.t113aTarifPerjamTWC

        def result = cek.list() {
            and{
                ne("id",id)
                eq("companyDealer",CompanyDealer.findById(Long.parseLong(params."companyDealer.id")))
                eq("kategoriKendaraan",KategoriKendaraan.findById(Long.parseLong(params."kategoriKendaraan.id")))
                eq("t113aTMT",new Date().parse("dd-MM-yyyy", params.t113aTMT_dp))
                eq("staDel","0")
                //eq("t113aTarifPerjamTWC",Double.parseDouble(str?.replace(",","")))
            }
        }
        if(!result){
            params?.lastUpdated = datatablesUtilService?.syncTime()
            tarifTWCInstance.properties = params
            tarifTWCInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            tarifTWCInstance?.lastUpdProcess = "UPDATE"
            if (!tarifTWCInstance.save(flush: true)) {
                render(view: "edit", model: [tarifTWCInstance: tarifTWCInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.update.tariTwc.error.message', default: 'Duplikat data Tarif TWC')
            render(view: "edit", model: [tarifTWCInstance: tarifTWCInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), tarifTWCInstance.id])
		redirect(action: "show", id: tarifTWCInstance.id)
	}

	def delete(Long id) {
		def tarifTWCInstance = TarifTWC.get(id)
		if (!tarifTWCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), id])
			redirect(action: "list")
			return
		}

		try {
            tarifTWCInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            tarifTWCInstance?.lastUpdProcess = "DELETE"
            tarifTWCInstance?.lastUpdated = datatablesUtilService?.syncTime()
            tarifTWCInstance?.setStaDel('1')
			tarifTWCInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tarifTWC.label', default: 'Tarif TWC'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(TarifTWC, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(TarifTWC, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
