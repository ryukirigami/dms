package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MappingBeratBaseModelController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = MappingBeratBaseModel.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            baseModel{
                eq("staDel","0")
            }
            if (params."sCriteria_tipeBerat") {
                tipeBerat{
                    ilike("m026NamaTipeBerat", "%" + (params."sCriteria_tipeBerat" as String) + "%")
                }
                //eq("tipeBerat", params."sCriteria_tipeBerat")
            }

            if (params."sCriteria_baseModel") {
                baseModel{
                    or{
                        ilike("m102KodeBaseModel", "%" + (params."sCriteria_baseModel" as String) + "%")
                        ilike("m102NamaBaseModel", "%" + (params."sCriteria_baseModel" as String) + "%")
                    }

                }
                //eq("baseModel", params."sCriteria_baseModel")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    tipeBerat: it.tipeBerat?.m026NamaTipeBerat,

                    baseModel: it.baseModel?.m102NamaBaseModel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [mappingBeratBaseModelInstance: new MappingBeratBaseModel(params)]
    }

    def save() {
        def mappingBeratBaseModelInstance = new MappingBeratBaseModel(params)

        def baseModel = BaseModel.get(params.baseModel.id)
        def tipeBerat = TipeBerat.get(params.tipeBerat.id)
        
        def duplicate = MappingBeratBaseModel.findByBaseModelAndTipeBerat(baseModel,tipeBerat)

        if(duplicate){
            flash.message = "Data sudah tersedia"
            render(view: "create", model: [mappingBeratBaseModelInstance: mappingBeratBaseModelInstance])
            return
        }

        mappingBeratBaseModelInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingBeratBaseModelInstance?.lastUpdProcess = "INSERT"
        if (!mappingBeratBaseModelInstance.save(flush: true)) {
            render(view: "create", model: [mappingBeratBaseModelInstance: mappingBeratBaseModelInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), mappingBeratBaseModelInstance.id])
        redirect(action: "show", id: mappingBeratBaseModelInstance.id)
    }

    def show(Long id) {
        def mappingBeratBaseModelInstance = MappingBeratBaseModel.get(id)
        if (!mappingBeratBaseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), id])
            redirect(action: "list")
            return
        }

        [mappingBeratBaseModelInstance: mappingBeratBaseModelInstance]
    }

    def edit(Long id) {
        def mappingBeratBaseModelInstance = MappingBeratBaseModel.get(id)
        if (!mappingBeratBaseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), id])
            redirect(action: "list")
            return
        }

        [mappingBeratBaseModelInstance: mappingBeratBaseModelInstance]
    }

    def update(Long id, Long version) {
        def mappingBeratBaseModelInstance = MappingBeratBaseModel.get(id)
        if (!mappingBeratBaseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), id])
            redirect(action: "list")
            return
        }

        def baseModel = BaseModel.get(params.baseModel.id)
        def tipeBerat = TipeBerat.get(params.tipeBerat.id)

        def duplicate = MappingBeratBaseModel.findByBaseModelAndTipeBerat(baseModel,tipeBerat)

        if(duplicate){
            flash.message = "Data tidak terupdate karena sama dengan sebelumnya atau data sudah tersedia"
            render(view: "create", model: [mappingBeratBaseModelInstance: mappingBeratBaseModelInstance])
            return
        }

        if (version != null) {
            if (mappingBeratBaseModelInstance.version > version) {

                mappingBeratBaseModelInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model')] as Object[],
                        "Another user has updated this MappingBeratBaseModel while you were editing")
                render(view: "edit", model: [mappingBeratBaseModelInstance: mappingBeratBaseModelInstance])
                return
            }
        }

        mappingBeratBaseModelInstance.properties = params
        mappingBeratBaseModelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingBeratBaseModelInstance?.lastUpdProcess = "UPDATE"

        if (!mappingBeratBaseModelInstance.save(flush: true)) {
            render(view: "edit", model: [mappingBeratBaseModelInstance: mappingBeratBaseModelInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), mappingBeratBaseModelInstance.id])
        redirect(action: "show", id: mappingBeratBaseModelInstance.id)
    }

    def delete(Long id) {
        def mappingBeratBaseModelInstance = MappingBeratBaseModel.get(id)
        if (!mappingBeratBaseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), id])
            redirect(action: "list")
            return
        }

        try {
            mappingBeratBaseModelInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'mappingBeratBaseModel.label', default: 'Mapping Berat Base Model'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MappingBeratBaseModel, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(MappingBeratBaseModel, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
