package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MesinEdcController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def generateCodeService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = MesinEdc.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m704Id"){
				ilike("m704Id","%" + (params."sCriteria_m704Id" as String) + "%")
			}
	
			if(params."sCriteria_m704NamaMesinEdc"){
				ilike("m704NamaMesinEdc","%" + (params."sCriteria_m704NamaMesinEdc" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

				ilike("staDel",'0')
			

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m704Id: it.m704Id,
			
						m704NamaMesinEdc: it.m704NamaMesinEdc,
			
						staDel: it.staDel,
						
						createdBy: it.createdBy,
						
											updatedBy: it.updatedBy,
						
											lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[mesinEdcInstance: new MesinEdc(params)]
	}

	def save() {
		def mesinEdcInstance = new MesinEdc(params)
        def cek = MesinEdc.createCriteria()
        def result = cek.list() {
            or{
                eq("m704NamaMesinEdc",mesinEdcInstance.m704NamaMesinEdc?.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }

        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Sudah Ada')
            render(view: "create", model: [mesinEdcInstance: mesinEdcInstance])
            return
        }
        mesinEdcInstance?.m704Id = generateCodeService.codeGenerateSequence("M704_ID",null)
		mesinEdcInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		mesinEdcInstance?.lastUpdProcess = "INSERT"
		mesinEdcInstance?.setStaDel('0')
		

		if (!mesinEdcInstance.save(flush: true)) {
			render(view: "create", model: [mesinEdcInstance: mesinEdcInstance])
			return
		}
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("M704_ID",null,mesinEdcInstance?.m704Id)

		flash.message = message(code: 'default.created.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), mesinEdcInstance.id])
		redirect(action: "show", id: mesinEdcInstance.id)
	}

	def show(Long id) {
		def mesinEdcInstance = MesinEdc.get(id)
		if (!mesinEdcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), id])
			redirect(action: "list")
			return
		}

		[mesinEdcInstance: mesinEdcInstance]
	}

	def edit(Long id) {
		def mesinEdcInstance = MesinEdc.get(id)
		if (!mesinEdcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), id])
			redirect(action: "list")
			return
		}

		[mesinEdcInstance: mesinEdcInstance]
	}

	def update(Long id, Long version) {
		def mesinEdcInstance = MesinEdc.get(id)
		if (!mesinEdcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (mesinEdcInstance.version > version) {
				
				mesinEdcInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'mesinEdc.label', default: 'MesinEdc')] as Object[],
				"Another user has updated this MesinEdc while you were editing")
				render(view: "edit", model: [mesinEdcInstance: mesinEdcInstance])
				return
			}
		}

        def cek = MesinEdc.createCriteria()
        def result = cek.list() {
            and{
                eq("m704NamaMesinEdc",params.m704NamaMesinEdc, [ignoreCase: true])
            }
            ilike("staDel",'0')
        }

        if(result && MesinEdc.findByM704NamaMesinEdcAndStaDel(params.m704NamaMesinEdc,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Mesin Sudah Ada')
            render(view: "edit", model: [mesinEdcInstance: mesinEdcInstance])
            return
        }

		mesinEdcInstance.properties = params

		mesinEdcInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		mesinEdcInstance?.lastUpdProcess = "UPDATE"
		if (!mesinEdcInstance.save(flush: true)) {
			render(view: "edit", model: [mesinEdcInstance: mesinEdcInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), mesinEdcInstance.id])
		redirect(action: "show", id: mesinEdcInstance.id)
	}

	def delete(Long id) {
		def mesinEdcInstance = MesinEdc.get(id)
		if (!mesinEdcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), id])
			redirect(action: "list")
			return
		}

		try {
			//mesinEdcInstance.delete(flush: true)
			mesinEdcInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			mesinEdcInstance?.lastUpdProcess = "DELETE"
			mesinEdcInstance?.setStaDel('1')
			mesinEdcInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'mesinEdc.label', default: 'MesinEdc'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(MesinEdc, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(MesinEdc, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
