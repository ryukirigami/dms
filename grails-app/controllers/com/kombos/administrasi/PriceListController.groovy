package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.GoodsHargaJual
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.hibernate.criterion.Order

class PriceListController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
    def conversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def showLatestPrice() {
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList(){
        def namaJob = ["SB1K","SB10K","SB20K","SB30K","SB40K","SB50K","SB60K","SB70K","SB80K","SB90K","SB100K"]
        def bsModel = BaseModel.findAllByStaDel('0')
        def no = 0
        def ret
        def rows = []

        bsModel.each {
            def cariPrice = PriceList.findAllByBaseModelAndCompanyDealer(it,CompanyDealer.findById(session.userCompanyDealerId as long))
            def j1,j2,j3,j4,j5,j6,j7,j8,j9,j10,j11,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11 = 0

            if(cariPrice){
                no++
                cariPrice.each {
                    if(it?.operation?.m053Id == namaJob[0]){
                        j1 = it?.m095Jasa
                        p1 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[1]){
                        j2 = it?.m095Jasa
                        p2 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[2]){
                        j3 = it?.m095Jasa
                        p3 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[3]){
                        j4 = it?.m095Jasa
                        p4 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[4]){
                        j5 = it?.m095Jasa
                        p5 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[5]){
                        j6 = it?.m095Jasa
                        p6 = it?.m095Parts
                    }else if(it?.operation.m053Id == namaJob[6]){
                        j7 = it?.m095Jasa
                        p7 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[7]){
                        j8 = it?.m095Jasa
                        p8 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[8]){
                        j9 = it?.m095Jasa
                        p9 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[9]){
                        j10 = it?.m095Jasa
                        p10 = it?.m095Parts
                    }else if(it?.operation?.m053Id == namaJob[10]){
                        j11 = it?.m095Jasa
                        p11 = it?.m095Parts
                    }
                }
                    rows << [
                            no : no,
                            model : it?.m102NamaBaseModel,
                            jasasb1k : conversi.toRupiah(j1),
                            partsb1k : conversi.toRupiah(p1),
                            jasasb10k : conversi.toRupiah(j2),
                            partsb10k : conversi.toRupiah(p2),
                            jasasb20k : conversi.toRupiah(j3),
                            partsb20k : conversi.toRupiah(p3),
                            jasasb30k : conversi.toRupiah(j4),
                            partsb30k : conversi.toRupiah(p4),
                            jasasb40k : conversi.toRupiah(j5),
                            partsb40k : conversi.toRupiah(p5),
                            jasasb50k : conversi.toRupiah(j6),
                            partsb50k : conversi.toRupiah(p6),
                            jasasb60k : conversi.toRupiah(j7),
                            partsb60k : conversi.toRupiah(p7),
                            jasasb70k : conversi.toRupiah(j8),
                            partsb70k : conversi.toRupiah(p8),
                            jasasb80k : conversi.toRupiah(j9),
                            partsb80k : conversi.toRupiah(p9),
                            jasasb90k : conversi.toRupiah(j10),
                            partsb90k : conversi.toRupiah(p10),
                            jasasb100k : conversi.toRupiah(j11),
                            partsb100k : conversi.toRupiah(p11)
                    ]
            }
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  0, iTotalDisplayRecords: 0, aaData: rows]
        render ret as JSON

    }

    def update() {
        def namaJob = ["SB1K","SB10K","SB20K","SB30K","SB40K","SB50K","SB60K","SB70K","SB80K","SB90K","SB100K"]
        CompanyDealer companyDealer = session.userCompanyDealer

        def opr = Operation.findAllByStaDelAndM053IdInList('0', namaJob)

        PriceList priceList
        Operation operations
        TarifPerJam tarifPerJam
        GeneralParameter generalParameter = GeneralParameter.findByCompanyDealer(companyDealer)

        def partJobs
        GoodsHargaJual goodsHargaJual
        Double parts
        Double jasa
        Date tmt
//        def mappingJobPanel, flat
        def flat

        opr.each {
            operations = it
            def flatRates = FlatRate.createCriteria().list {
                operation{
                    eq("m053Id", operations?.m053Id)
                }
                eq("t113StaPriceMenu", "1")
                addOrder(Order.asc("operation"))
            }

//            mappingJobPanel = MappingJobPanel.findByOperation(operations)

                flatRates.each {
                    flat = it
                    parts = 0
                    jasa = 0
                    if(flat?.t113StaPriceMenu == "1"){
                        tarifPerJam = TarifPerJam.findByCompanyDealerAndKategoriAndStaDelAndT152TMTLessThanEquals(companyDealer, flat?.baseModel?.kategoriKendaraan,"0", (new Date()+1).clearTime(),[sort: 'id',order: 'desc'])
                        tmt = tarifPerJam?.t152TMT
                        jasa = it?.t113FlatRate * tarifPerJam?.t152TarifPerjamSB * generalParameter?.m000Tax
                        tmt = tmt?.before(it?.t113TMT) ? it?.t113TMT : tmt

                        partJobs = PartJob.findAllByOperation(it.operation)
                        partJobs.each {
                            goodsHargaJual = GoodsHargaJual.findByGoods(it.goods)
                            parts = parts + (goodsHargaJual == null ? 0 : goodsHargaJual?.t151HargaDenganPPN)
                            tmt = goodsHargaJual == null ? tmt : (tmt.before(goodsHargaJual?.t151TMT) ? goodsHargaJual?.t151TMT : tmt)
                        }

                        def cariJobBase = PriceList.findByBaseModelAndOperation(flat?.baseModel, operations)

                        if (cariJobBase){
                            priceList = PriceList.get(cariJobBase.id)
                            priceList.companyDealer = companyDealer
//                            priceList.operation = opr
                            priceList.baseModel = it?.baseModel
                            priceList.m095TMT = tmt
                            priceList.m095Jasa = jasa
                            priceList.m095Parts = parts
                            priceList.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            priceList.lastUpdProcess = "UPDATE"
                            priceList?.lastUpdated = datatablesUtilService?.syncTime()
                            priceList.save()
                        }else{
                            priceList = new PriceList()
                            priceList.companyDealer = companyDealer
//                            priceList.operation = opr
                            priceList.baseModel = it?.baseModel
                            priceList.m095TMT = tmt
                            priceList.m095Jasa = jasa
                            priceList.m095Parts = parts
                            priceList.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            priceList.lastUpdProcess = "INSERT"
                            priceList?.dateCreated = datatablesUtilService?.syncTime()
                            priceList?.lastUpdated = datatablesUtilService?.syncTime()
                            priceList.save()
                        }
                    }
                }
        }

        redirect(action: "list")
//        render true as JSON
    }
}