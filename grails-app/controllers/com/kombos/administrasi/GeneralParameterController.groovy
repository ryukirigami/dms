package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.generatecode.GenerateCodeService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GeneralParameterController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def generalParameterService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = [
		'index',
		'list',
		'datatablesList'
	]

	static addPermissions = ['create', 'save']

	static editPermissions = [
		'edit',
		'update',
		'editGeneral',
		'editManPower',
		'editAppointment',
		'editReception',
		'editProduciton',
		'editDelivery',
		'editSecurity',
		'editStaticDiscount',
		'editDinamicDiscount',
		'editDiscountCustomerVehicle',
		'editWeb',
		'editFollowUp',
		'editReport',
		'editRefund',
		'editMaterai'
	]

	static deletePermissions = ['delete']

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def create() {
		[generalParameterInstance: new GeneralParameter(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def generalParameterInstance = new GeneralParameter(params)
		if (!generalParameterInstance.save(flush: true)) {
			render(view: "create", model: [generalParameterInstance: generalParameterInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [
			message(code: 'generalParameter.label', default: 'GeneralParameter'),
			generalParameterInstance.id
		])
		redirect(action: "show", id: generalParameterInstance.id)
	}

	def show(Long id) {
		def generalParameterInstance = GeneralParameter.get(id)
		if (!generalParameterInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'generalParameter.label', default: 'GeneralParameter'),
				id
			])
			redirect(action: "list")
			return
		}

		[generalParameterInstance: generalParameterInstance]
	}

	def edit(Long id) {
		def generalParameterInstance = GeneralParameter.get(id)
		if (!generalParameterInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'generalParameter.label', default: 'GeneralParameter'),
				id
			])
			redirect(action: "list")
			return
		}

		[generalParameterInstance: generalParameterInstance]
	}

	def update(Long id, Long version) {
		def res = [:]
		def generalParameterInstance = new GeneralParameter()
        if(id){
            generalParameterInstance = GeneralParameter.get(id)
        }else {
            generalParameterInstance?.companyDealer = session?.userCompanyDealer
            generalParameterInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            generalParameterInstance.setLastUpdProcess("INSERT")
            generalParameterInstance.setM000StaDel("0")
        }
				
		if (!generalParameterInstance) {
			res.message = message(code: 'default.not.found.message', args: [
				message(code: 'generalParameter.label', default: 'GeneralParameter'),
				id
			])
			render res as JSON
			return
		}

		if (version != null) {
			if (generalParameterInstance.version > version) {

				generalParameterInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						[
							message(code: 'generalParameter.label', default: 'GeneralParameter')] as Object[],
						"Another user has updated this GeneralParameter while you were editing")
				res.message = "Another user has updated this GeneralParameter while you were editing"
				render res as JSON
				return
			}
		}
		
		// Create complete date.struct for timepicker		
		if(params.m000KonfirmasiJamSebelum_hour && params.m000KonfirmasiJamSebelum_minute){
			params.m000KonfirmasiJamSebelum_day = "1"
			params.m000KonfirmasiJamSebelum_month = "1"
			params.m000KonfirmasiJamSebelum_year = "2000"
		}
		
		if(params.m000KonfirmasiTerlambat_hour && params.m000KonfirmasiTerlambat_minute){
			params.m000KonfirmasiTerlambat_day = "1"
			params.m000KonfirmasiTerlambat_month = "1"
			params.m000KonfirmasiTerlambat_year = "2000"
		}
		
		if(params.m000JMinusSMSAppointment_hour && params.m000JMinusSMSAppointment_minute){
			params.m000JMinusSMSAppointment_day = "1"
			params.m000JMinusSMSAppointment_month = "1"
			params.m000JMinusSMSAppointment_year = "2000"
		}
		
		if(params.m000JPlusSMSTerlambat_hour && params.m000JPlusSMSTerlambat_minute){
			params.m000JPlusSMSTerlambat_day = "1"
			params.m000JPlusSMSTerlambat_month = "1"
			params.m000JPlusSMSTerlambat_year = "2000"
		}
		
		if(params.m000WaktuNotifikasiTargetClockOffJob_hour && params.m000WaktuNotifikasiTargetClockOffJob_minute){
			params.m000WaktuNotifikasiTargetClockOffJob_day = "1"
			params.m000WaktuNotifikasiTargetClockOffJob_month = "1"
			params.m000WaktuNotifikasiTargetClockOffJob_year = "2000"
		}
		
		if(params.m000ToleransiAmbilWO_hour && params.m000ToleransiAmbilWO_minute){
			params.m000ToleransiAmbilWO_day = "1"
			params.m000ToleransiAmbilWO_month = "1"
			params.m000ToleransiAmbilWO_year = "2000"
		}
		
		if(params.m000BufferDeliveryTimeGR_hour && params.m000BufferDeliveryTimeGR_minute){
			params.m000BufferDeliveryTimeGR_day = "1"
			params.m000BufferDeliveryTimeGR_month = "1"
			params.m000BufferDeliveryTimeGR_year = "2000"
		}
		
		
		if(params.m000BufferDeliveryTimeBP_hour && params.m000BufferDeliveryTimeBP_minute){
			params.m000BufferDeliveryTimeBP_day = "1"
			params.m000BufferDeliveryTimeBP_month = "1"
			params.m000BufferDeliveryTimeBP_year = "2000"
		}
		if(params.m000ToleransiDeliveryGR_hour && params.m000ToleransiDeliveryGR_minute){
			params.m000ToleransiDeliveryGR_day = "1"
			params.m000ToleransiDeliveryGR_month = "1"
			params.m000ToleransiDeliveryGR_year = "2000"
		}
		
		if(params.m000ToleransiDeliveryBP_hour && params.m000ToleransiDeliveryBP_minute){
			params.m000ToleransiDeliveryBP_day = "1"
			params.m000ToleransiDeliveryBP_month = "1"
			params.m000ToleransiDeliveryBP_year = "2000"
		}
		
		if(params.m000WaktuSessionLog_hour && params.m000WaktuSessionLog_minute){
			params.m000WaktuSessionLog_day = "1"
			params.m000WaktuSessionLog_month = "1"
			params.m000WaktuSessionLog_year = "2000"
		}
		if(params.m000PenguncianPassword_hour && params.m000PenguncianPassword_minute){
			params.m000PenguncianPassword_day = "1"
			params.m000PenguncianPassword_month = "1"
			params.m000PenguncianPassword_year = "2000"
		}

        if(params.m000StaWaktuAkhir && params.m000StaWaktuAkhir == 'on'){
            params.m000StaWaktuAkhir = '1'
        } else {
            params.m000StaWaktuAkhir = '0'
        }

        if(params.m000staJenisJamKerjaSenin == 'null'){
            params.m000staJenisJamKerjaSenin = null
        }
        if(params.m000staJenisJamKerjaSelasa == 'null'){
            params.m000staJenisJamKerjaSelasa = null
        }
        if(params.m000staJenisJamKerjaRabu == 'null'){
            params.m000staJenisJamKerjaRabu = null
        }
        if(params.m000staJenisJamKerjaKamis == 'null'){
            params.m000staJenisJamKerjaKamis = null
        }
        if(params.m000staJenisJamKerjaJumat == 'null'){
            params.m000staJenisJamKerjaJumat = null
        }
        if(params.m000staJenisJamKerjaSabtu == 'null'){
            params.m000staJenisJamKerjaSabtu = null
        }
        if(params.m000staJenisJamKerjaMinggu == 'null'){
            params.m000staJenisJamKerjaMinggu = null
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        generalParameterInstance.properties = params


	
		if (!generalParameterInstance.save(flush: true)) {
            generalParameterInstance.errors.each{
				//println it
			}
			res.message = "Error on updating GeneralParameter"
			render res as JSON
			return
		}
		
		
		if(params.margin){
			CompanyDealer companyDealer = session.userCompanyDealer
			
			def c = Margin.createCriteria()
			def marginList = c.list () {
					eq("companyDealer",companyDealer)
			}
			
			marginList.each{
                if(params."margin-${it.id}") {
                    it.m018MarginAtas = params."margin-${it.id}" as double
                    it.save()
                }
			}
		}

		res.message = message(code: 'default.updated.message', args: [
			message(code: 'generalParameter.label', default: 'GeneralParameter'),
			generalParameterInstance.id
		])
        generalParameterInstance = GeneralParameter.get(id)
        res.version = (generalParameterInstance?.version?generalParameterInstance.version:0)
		render res as JSON
		return
	}

	def delete(Long id) {
		def generalParameterInstance = GeneralParameter.get(id)
		if (!generalParameterInstance) {
			flash.message = message(code: 'default.not.found.message', args: [
				message(code: 'generalParameter.label', default: 'GeneralParameter'),
				id
			])
			redirect(action: "list")
			return
		}

		try {
			generalParameterInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [
				message(code: 'generalParameter.label', default: 'GeneralParameter'),
				id
			])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [
				message(code: 'generalParameter.label', default: 'GeneralParameter'),
				id
			])
			redirect(action: "show", id: id)
		}
	}


	def editGeneral(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
		if (!generalParameterInstance) {
			generalParameterInstance = new GeneralParameter()
		}
		
		def c = Margin.createCriteria()
		def marginList = c.list () {			
				eq("companyDealer",companyDealer)
                eq("m018StaDel", '0')
                namaDokumen{
                    eq("staDel", '0')
                }
		}		

		[generalParameterInstance: generalParameterInstance, marginList: marginList]
	}
	def editManPower(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
		if (!generalParameterInstance) {
			generalParameterInstance = new GeneralParameter()
		}
		
		def userList = []
		
		User.list().each { User u ->
			def user = [:]
			user.username = u.username
            def roles = u.roles.join(',')
			user.label = u.fullname  + " ("  + roles +")"

			userList << user			
		}

		[generalParameterInstance: generalParameterInstance, jenisJamKerjaList: JenisJamKerja.list(), userList: userList]
	}
	def editAppointment(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	def editReception(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	def editProduction(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	def editDelivery(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	def editSecurity(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	def editDiscountCustomerVehicle(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }
		
		def searchCriterias = [
			[field:"tanggalDec", label:"Tanggal DEC", type: "date"],
			[field:"model", label:"Model", type: "string"],
			[field:"tanggalStnk", label:"Tanggal STNK", type: "date"],
			[field:"tanggalServis", label:"Tanggal Servis", type: "date"],
			[field:"vinCode", label:"Vin Code", type: "string"],
			[field:"noPolisi", label:"Nomor Polisi", type: "string"],
			[field:"namaStnk", label:"Nama STNK", type: "string"],
			[field:"tanggalAwal", label:"Tanggal Awal", type: "date"],
			[field:"tanggalAkhir", label:"Tanggal Akhir", type: "date"]
			]

		[generalParameterInstance: generalParameterInstance, searchCriterias: searchCriterias]
	}
	def editWeb(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	def editFollowUp(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	def editReport(){
		CompanyDealer companyDealer = session.userCompanyDealer
		GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
        if (!generalParameterInstance) {
            generalParameterInstance = new GeneralParameter()
        }

		[generalParameterInstance: generalParameterInstance]
	}
	
	def dcvDatatablesList() {
		render generalParameterService.dcvDatatablesList(params) as JSON
	}
	
	def setDiscountCustVehicle() {
		render generalParameterService.setDiscountCustVehicle(params) as JSON
	}
	
	
}
