package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ManPowerStallController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = ManPowerStall.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("companyDealer",session?.userCompanyDealer);
			if(params."sCriteria_t019Tanggal"){
				ge("t019Tanggal",params."sCriteria_t019Tanggal")
				lt("t019Tanggal",params."sCriteria_t019Tanggal" + 1)
			}

			if(params."sCriteria_namaManPower"){
				//eq("namaManPower",params."sCriteria_namaManPower")
				namaManPower{
				 ilike("t015NamaLengkap","%"+(params."sCriteria_namaManPower")+"%")
				}
				//eq("namaManPower",NamaManPower.findByT015NamaLengkapAndT015NamaBoardIlike("%"+params."sCriteria_namaManPower"+"%"))
			}

			if(params."sCriteria_stall"){
				//eq("stall",params."sCriteria_stall")
				eq("stall",Stall.findByM022NamaStallIlike("%"+params."sCriteria_stall"+"%"))
			}


	
			ilike("staDel",'0')
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						t019Tanggal: it.t019Tanggal?it.t019Tanggal.format(dateFormat):"",
			
						namaManPower: it.namaManPower.t015NamaLengkap,
			
						stall: it.stall.m022NamaStall,
			
						staDel: it.staDel,
						
						createdBy: it.createdBy,
						
											updatedBy: it.updatedBy,
						
											lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[manPowerStallInstance: new ManPowerStall(params)]
	}

	def save() {

		def manPowerStallInstance = new ManPowerStall()
        def namaMan = NamaManPower?.findByT015NamaLengkap(params.namaManPower)
        if(!namaMan){
            flash.message = '* Nama ManPower Tidak Ada'
            render(view: "create", model: [manPowerStallInstance: manPowerStallInstance])
            return
        }
        def stall = Stall.findById(params.stall.id)
        def cek = ManPowerStall.findByNamaManPowerAndT019TanggalAndStallAndStaDel(namaMan,params.t019Tanggal,stall,'0')
        if(cek){
            flash.message = '* Data Sudah Ada'
            render(view: "create", model: [manPowerStallInstance: manPowerStallInstance])
            return
        }
            manPowerStallInstance?.dateCreated = datatablesUtilService?.syncTime()
            manPowerStallInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerStallInstance?.namaManPower = namaMan
            manPowerStallInstance?.stall = stall
            manPowerStallInstance?.t019Tanggal = params.t019Tanggal
            manPowerStallInstance?.setStaDel("0")
            manPowerStallInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            manPowerStallInstance?.lastUpdProcess = "INSERT"
            manPowerStallInstance?.dateCreated = new Date()
            manPowerStallInstance?.companyDealer = session.userCompanyDealer

		if (!manPowerStallInstance.save(flush: true)) {
			render(view: "create", model: [manPowerStallInstance: manPowerStallInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), manPowerStallInstance.id])
		redirect(action: "show", id: manPowerStallInstance.id)
	}

	def show(Long id) {
		def manPowerStallInstance = ManPowerStall.get(id)
		if (!manPowerStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), id])
			redirect(action: "list")
			return
		}

		[manPowerStallInstance: manPowerStallInstance]
	}

	def edit(Long id) {
		def manPowerStallInstance = ManPowerStall.get(id)
		if (!manPowerStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), id])
			redirect(action: "list")
			return
		}

		[manPowerStallInstance: manPowerStallInstance]
	}

	def update(Long id, Long version) {
		def manPowerStallInstance = ManPowerStall.get(id)
		if (!manPowerStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (manPowerStallInstance.version > version) {
				
				manPowerStallInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'manPowerStall.label', default: 'ManPowerStall')] as Object[],
				"Another user has updated this ManPowerStall while you were editing")
				render(view: "edit", model: [manPowerStallInstance: manPowerStallInstance])
				return
			}
		}

        def namaMan = NamaManPower?.findByT015NamaLengkap(params.namaManPower)
        if(!namaMan){
            flash.message = '* Nama ManPower Tidak Ada'
            render(view: "edit", model: [manPowerStallInstance: manPowerStallInstance])
            return
        }

        def stall = Stall.findById(params.stall.id)
        def cek = ManPowerStall.findByNamaManPowerAndT019TanggalAndStallAndStaDel(namaMan,params.t019Tanggal,stall,'0')
        if(cek){
            if(cek.id != id){
                flash.message = '* Data Sudah Ada'
                render(view: "edit", model: [manPowerStallInstance: manPowerStallInstance])
                return
            }
        }
            manPowerStallInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerStallInstance?.namaManPower = namaMan
            manPowerStallInstance?.stall = stall
            manPowerStallInstance?.t019Tanggal = params.t019Tanggal
            manPowerStallInstance?.setStaDel("0")
            manPowerStallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            manPowerStallInstance?.lastUpdProcess = "UPDATE"


		if (!manPowerStallInstance.save(flush: true)) {
			render(view: "edit", model: [manPowerStallInstance: manPowerStallInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), manPowerStallInstance.id])
		redirect(action: "show", id: manPowerStallInstance.id)
	}

	def delete(Long id) {
		def manPowerStallInstance = ManPowerStall.get(id)
		 
		if (!manPowerStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), id])
			redirect(action: "list")
			return
		}

		try {
			//manPowerStallInstance.delete(flush: true)
			manPowerStallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			manPowerStallInstance?.lastUpdProcess = "DELETE"
			manPowerStallInstance?.setStaDel('1')
            manPowerStallInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerStallInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'manPowerStall.label', default: 'ManPowerStall'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(ManPowerStall, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ManPowerStall, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def kodeList() {
        def res = [:]
		def c = NamaManPower.createCriteria()
        //def result = NamaManPower.findAllWhere(staDel : '0')

		def result = c.listDistinct {
			eq("staDel","0")
			eq("companyDealer",session?.userCompanyDealer);
			ilike("t015NamaLengkap","%"+params?.query+"%");
			manPowerDetail{
				manPower{
					ilike("m014JabatanManPower","%Teknisi%")
				}
			}
		}

        def opts = []
        result.each {
            opts << it.t015NamaLengkap
        }

        res."options" = opts
        render res as JSON
    }

}
