package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import javax.print.PrintService
import javax.print.PrintServiceLookup

class UserPrinterController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = UserPrinter.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_userProfile") {
                userProfile{
                    ilike("t001NamaPegawai","%"+params."sCriteria_userProfile"+"%");
                }
            }

            if (params."sCriteria_companyDealer") {
                userProfile{
                    companyDealer{
                        ilike("m011NamaWorkshop","%"+params."sCriteria_companyDealer"+"%")
                    }
                }
            }

            if (params."sCriteria_divisi") {
                userProfile{
                    divisi{
                        ilike("m012NamaDivisi","%"+params."sCriteria_divisi"+"%")
                    }
                }
            }

            if (params."sCriteria_t005PathPrinter") {
                ilike("t005PathPrinter", "%" + (params."sCriteria_t005PathPrinter" as String) + "%")
            }

            if (params."sCriteria_namaDokumen") {
                namaDokumen{
                    ilike("m007NamaDokumen","%"+params."sCriteria_namaDokumen"+"%")
                }
            }

            switch (sortProperty) {
                case "userProfile":
                    userProfile{
                        order("t001NamaPegawai",sortDir)
                    }
                    break;
                case "companyDealer":
                    userProfile{
                        companyDealer{
                            order("m011NamaWorkshop",sortDir)
                        }
                    }
                    break;
                case "divisi":
                    userProfile{
                        divisi{
                            order("m012NamaDivisi",sortDir)
                        }
                    }
                    break;
                case "namaDokumen":
                    namaDokumen{
                        order("m007NamaDokumen",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    userProfile: it?.userProfile?.t001NamaPegawai,

                    companyDealer: it?.userProfile?.companyDealer?.m011NamaWorkshop,

                    divisi: it?.userProfile?.divisi?.m012NamaDivisi,

                    t005PathPrinter: it.t005PathPrinter,

                    namaDokumen: it?.namaDokumen?.m007NamaDokumen

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [userPrinterInstance: new UserPrinter(params)]
    }

    def save() {
        def userPrinterInstance = new UserPrinter()
        userPrinterInstance.staDel = '0'
        userPrinterInstance.lastUpdProcess = "INSERT"
        userPrinterInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        userPrinterInstance.namaDokumen = NamaDokumen.findById(params.namaDokumen.id.toLong())
        userPrinterInstance.userProfile = User.findByUsernameAndStaDel(params?.userProfile?.id,"0")
        userPrinterInstance.t005PathPrinter = params.t005PathPrinter
        def user = UserPrinter.createCriteria().list {
            eq("staDel","0")
            eq("t005PathPrinter",params.t005PathPrinter)
            userProfile{
                eq("staDel","0")
                eq("username",params?.userProfile?.id)
            }
            namaDokumen{
                eq("id",params.namaDokumen.id.toLong())
            }
        }
        if(user){
            flash.message = "Data sudah ada"
            render(view: "create", model: [userPrinterInstance: userPrinterInstance])
            return
        }
        if (!userPrinterInstance.save(flush: true)) {
            render(view: "create", model: [userPrinterInstance: userPrinterInstance])
            return
        }

        flash.message = "Data created"
        redirect(action: "show", id: userPrinterInstance.id)
    }

    def show(Long id) {
        def userPrinterInstance = UserPrinter.get(id)
        if (!userPrinterInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userPrinter.label', default: 'UserPrinter'), id])
            redirect(action: "list")
            return
        }

        [userPrinterInstance: userPrinterInstance]
    }

    def edit(Long id) {
        def userPrinterInstance = UserPrinter.get(id)
        if (!userPrinterInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userPrinter.label', default: 'UserPrinter'), id])
            redirect(action: "list")
            return
        }
        [userPrinterInstance: userPrinterInstance]
    }

    def update(Long id, Long version) {
        def userPrinterInstance = UserPrinter.get(id)
        if (!userPrinterInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userPrinter.label', default: 'UserPrinter'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userPrinterInstance.version > version) {

                userPrinterInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'userPrinter.label', default: 'UserPrinter')] as Object[],
                        "Another user has updated this UserPrinter while you were editing")
                render(view: "edit", model: [userPrinterInstance: userPrinterInstance])
                return
            }
        }
        def user = UserPrinter.createCriteria().list {
            eq("staDel","0")
            eq("t005PathPrinter",params.t005PathPrinter)
            userProfile{
                eq("staDel","0")
                eq("username",params?.userProfile?.id)
            }
            namaDokumen{
                eq("id",params.namaDokumen.id.toLong())
            }
        }
        if(user){
            for(find in user){
                if(find.id!=id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [userPrinterInstance: userPrinterInstance])
                    return
                    break
                }
            }
        }

        userPrinterInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        userPrinterInstance?.lastUpdProcess = "UPDATE"
        userPrinterInstance.namaDokumen = NamaDokumen.findById(params.namaDokumen.id.toLong())
        userPrinterInstance.userProfile = User.findByUsernameAndStaDel(params?.userProfile?.id,"0")
        userPrinterInstance.t005PathPrinter = params.t005PathPrinter

        if (!userPrinterInstance.save(flush: true)) {
            render(view: "edit", model: [userPrinterInstance: userPrinterInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'userPrinter.label', default: 'UserPrinter'), userPrinterInstance.id])
        redirect(action: "show", id: userPrinterInstance.id)
    }

    def delete(Long id) {
        def userPrinterInstance = UserPrinter.get(id)
        if (!userPrinterInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'userPrinter.label', default: 'UserPrinter'), id])
            redirect(action: "list")
            return
        }

        try {
            userPrinterInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            userPrinterInstance?.lastUpdProcess = "DELETE"
            userPrinterInstance?.staDel = "1"
            userPrinterInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'userPrinter.label', default: 'UserPrinter'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'userPrinter.label', default: 'UserPrinter'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(UserPrinter, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(UserPrinter, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def findUserByDivisi(){
        def html="<option value=\'\'> Pilih Pegawai</option>"
        Divisi divisi
        def users
        if(params?.divisi.id) {
            divisi = Divisi.findById(params?.divisi.id?.toLong())
            if(divisi){
                users = User.findAllByDivisiAndStaDel(divisi,"0")
                users.each {
                    html=html + "<option value=\'${it.username}\'> ${it.t001NamaPegawai}</option>"
                }
            }
        }
        render html
    }

    def getPrinter(){
        def html="<option value=\'\'> Pilih Path Printer</option>"
        PrintService[] printServices= PrintServiceLookup.lookupPrintServices(null,null);
        if(printServices){
            if(params.id){
                def user = UserPrinter.get(params.id.toLong())
                printServices.each {
                    if(user.t005PathPrinter.equalsIgnoreCase(it.getName())){
                        html +="<option value=\'"+it.getName()+"\' selected=\'selected\'> "+it.getName()+"</option>"
                    }else{
                        html +="<option value=\'"+it.getName()+"\'> "+it.getName()+"</option>"
                    }
                }
            }else{
                printServices.each {
                    html +="<option value=\'"+it.getName()+"\'> "+it.getName()+"</option>"
                }

            }
        }
        render html
    }

    def findDivisionByCompanyDealer(){
        def html="<option value=\'-1\'> Pilih Divisi</option>"
        def divisi
        CompanyDealer companyDealer
        def divisiUser = null
        if(params.username){
            divisiUser = User.findByUsername(params.username).divisi
        }

        if(params?.companyDealer.id!='null' || params?.companyDealer.id!='-1') {
            companyDealer = CompanyDealer.findById(params?.companyDealer.id?.toLong())
            divisi = Divisi.findAllByCompanyDealerAndStaDel(companyDealer,"0")

            divisi.each {
                if(divisiUser){
                    html=html + "<option ${it.id==divisiUser.id?"selected=\'selected\'":""} value=\'${it.id}\'> ${it.m012NamaDivisi}</option>"
                }else{
                    html=html + "<option value=\'${it.id}\'> ${it.m012NamaDivisi}</option>"
                }
            }
        }
        render html
    }

}
