package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class BankController {
	
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
    static viewPermissions = ['index', 'list', 'datatablesList']
	
    static addPermissions = ['create', 'save']
	
    static editPermissions = ['edit', 'update']
	
    static deletePermissions = ['delete']
	
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }
	
    def datatablesList() {

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		
        session.exportParams=params
		
        def c = Bank.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")	
            if(params."sCriteria_m702ID"){
                ilike("m702ID","%" + (params."sCriteria_m702ID" as String) + "%")
            }
	
            if(params."sCriteria_m702NamaBank"){
                ilike("m702NamaBank","%" + (params."sCriteria_m702NamaBank" as String) + "%")
            }
	
            if(params."sCriteria_m702Cabang"){
                ilike("m702Cabang","%" + (params."sCriteria_m702Cabang" as String) + "%")
            }
	
            if(params."sCriteria_m702Alamat"){
                ilike("m702Alamat","%" + (params."sCriteria_m702Alamat" as String) + "%")
            }
	
            if(params."sCriteria_m702staDel"){
                ilike("staDel","%" + (params."sCriteria_m702staDel" as String) + "%")
            }

			
            switch(sortProperty){
            default:
                order(sortProperty,sortDir)
                break;
            }
        }
		
        def rows = []
		
        results.each {
            rows << [
			
                id: it.id,
			
                m702ID: it.m702ID,
			
                m702NamaBank: it.m702NamaBank,
			
                m702Cabang: it.m702Cabang,

                m702Alamat: it.m702Alamat,
			
                staDel: it.staDel,
			
            ]
        }
		
        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
        render ret as JSON
    }

    def create() {
        [bankInstance: new Bank(params)]
    }

    def save() {
        def bankInstance = new Bank(params)
        def bank = Bank.createCriteria().list {
            eq("staDel","0")
            eq("m702NamaBank",params.m702NamaBank,[ignoreCase : true])
            eq("m702Alamat",params.m702Alamat,[ignoreCase : true])
            eq("m702Cabang",params.m702Cabang,[ignoreCase : true])
        }
        if(bank){
            bankInstance?.m702ID=null
            flash.message = "Data Bank Sudah Ada"
            render(view: "create", model: [bankInstance: bankInstance])
            return
        }
        bankInstance?.setDateCreated(datatablesUtilService.syncTime())
        bankInstance?.setLastUpdated(datatablesUtilService.syncTime())
        bankInstance?.setStaDel('0')
        bankInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        bankInstance?.setLastUpdProcess("INSERT")
        bankInstance?.setM702ID(generateCodeService.codeGenerateSequence("M702_ID",null))
        if (!bankInstance.save(flush: true)) {
            render(view: "create", model: [bankInstance: bankInstance])
            return
        }
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("M702_ID",null,bankInstance?.m702ID)

        flash.message = message(code: 'default.created.message', args: [message(code: 'bank.label', default: 'Bank'), bankInstance.id])
        redirect(action: "show", id: bankInstance.id)
    }

    def show(Long id) {
        def bankInstance = Bank.get(id)
        if (!bankInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "list")
            return
        }

        [bankInstance: bankInstance]
    }

    def edit(Long id) {
        def bankInstance = Bank.get(id)
        if (!bankInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "list")
            return
        }

        [bankInstance: bankInstance]
    }

    def update(Long id, Long version) {
        def bankInstance = Bank.get(id)
        if (!bankInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "list")
            return
        }
        def bank = Bank.createCriteria().list {
            eq("staDel","0")
            eq("m702NamaBank",params.m702NamaBank,[ignoreCase : true])
            eq("m702Alamat",params.m702Alamat,[ignoreCase : true])
            eq("m702Cabang",params.m702Cabang,[ignoreCase : true])
        }
        if(bank){
            for(find in bank){
                if(find?.id!=id){
                    flash.message = "Data Bank Sudah Ada"
                    render(view: "edit", model: [bankInstance: bankInstance])
                    return
                    break
                }
            }
        }

        if (version != null) {
            if (bankInstance.version > version) {
				
                bankInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                    [message(code: 'bank.label', default: 'Bank')] as Object[],
				"Another user has updated this Bank while you were editing")
                render(view: "edit", model: [bankInstance: bankInstance])
                return
            }
        }
        bankInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        bankInstance?.setLastUpdProcess("UPDATE")
        bankInstance.properties = params
        bankInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if (!bankInstance.save(flush: true)) {
            render(view: "edit", model: [bankInstance: bankInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'bank.label', default: 'Bank'), bankInstance.id])
        redirect(action: "show", id: bankInstance.id)
    }

    def delete(Long id) {
        def bankInstance = Bank.get(id)
        if (!bankInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "list")
            return
        }

        try {
            bankInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            bankInstance?.setLastUpdProcess("DELETE")
            bankInstance?.lastUpdated = datatablesUtilService?.syncTime()
            bankInstance?.setStaDel('1')
            bankInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'bank.label', default: 'Bank'), id])
            redirect(action: "show", id: id)
        }
    }
	
    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Bank, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"		
    }
	
    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(Bank, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }
	
	
}
