package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat

class PerformanceLogController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params
        DateFormat df=new SimpleDateFormat("HH:mm")
        def c = PerformanceLog.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_m778Tanggal" && params."sCriteria_m778TanggalAkhir") {
                ge("m778Tanggal", params."sCriteria_m778Tanggal")
                lt("m778Tanggal", params."sCriteria_m778TanggalAkhir" + 1)
            }

            if (params."sCriteria_m778Id") {
                ilike("m778Id", "%" + (params."sCriteria_m778Id" as String) + "%")
            }

            if (params."sCriteria_userProfile") {
                eq("userProfile", User.findById(Long.parseLong(params."sCriteria_userProfile")))
            }

            if (params."sCriteria_m778NamaActivity") {
                ilike("m778NamaActivity", "%" + (params."sCriteria_m778NamaActivity" as String) + "%")
            }

            if (params."sCriteria_m778TglJamClick") {
                Date date1 = df.parse(params."sCriteria_m778TglJamClick")
                ilike("m778TglJamClick", "%"+date1+"%")

            }

            if (params."sCriteria_m778TglJamTampil") {
                ge("m778TglJamTampil", params."sCriteria_m778TglJamTampil")
                lt("m778TglJamTampil", params."sCriteria_m778TglJamTampil" + 1)
            }

            if (params."sCriteria_m778DurasiDetik") {
                eq("m778DurasiDetik", Integer.parseInt(params."sCriteria_m778DurasiDetik"))
            }

            if (params."sCriteria_m778xNamaUser") {
                ilike("m778xNamaUser", "%" + (params."sCriteria_m778xNamaUser" as String) + "%")
            }

            if (params."sCriteria_m778xNamaDivisi") {
                ilike("m778xNamaDivisi", "%" + (params."sCriteria_m778xNamaDivisi" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m778Tanggal: it.m778Tanggal ? it.m778Tanggal.format(dateFormat) : "",

                    m778Id: it.m778Id,

                    userProfile: it.userProfile,

                    m778NamaActivity: it.m778NamaActivity,

                    m778TglJamClick: new SimpleDateFormat("HH:mm:ss").format(new Date(it.m778TglJamClick?.getTime())),

                    m778TglJamTampil: it.m778TglJamTampil ? it.m778TglJamTampil.format(dateFormat) : "",

                    m778DurasiDetik: it.m778DurasiDetik,

                    m778xNamaUser: it.m778xNamaUser,

                    m778xNamaDivisi: it.m778xNamaDivisi,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [performanceLogInstance: new PerformanceLog(params)]
    }

    def save() {
        def performanceLogInstance = new PerformanceLog(params)
        performanceLogInstance.setStaDel("0")
        performanceLogInstance.setLastUpdProcess("INSERT")
        performanceLogInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int hours = c.get(Calendar.HOUR);
        int mins = c.get(Calendar.MINUTE);
        int seconds = c.get(Calendar.SECOND);
        performanceLogInstance.m778TglJamClick = new Time(hours,mins,seconds)
        if (!performanceLogInstance.save(flush: true)) {
            render(view: "create", model: [performanceLogInstance: performanceLogInstance])
            return
        }
        flash.message = message(code: 'default.created.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), performanceLogInstance.id])
        Calendar c2=Calendar.getInstance();
        c2.setTime(new Date());
        int hours2 = c2.get(Calendar.HOUR)
        int mins2=c2.get(Calendar.MINUTE)
        int seconds2=c2.get(Calendar.SECOND)
        performanceLogInstance.setM778TglJamTampil(new Time(hours2,mins2,seconds))
        performanceLogInstance.setM778DurasiDetik(performanceLogInstance.m778TglJamClick - performanceLogInstance.m778TglJamTampil)
        redirect(action: "show", id: performanceLogInstance.id)
    }

    def show(Long id) {
        def performanceLogInstance = PerformanceLog.get(id)
        if (!performanceLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), id])
            redirect(action: "list")
            return
        }

        [performanceLogInstance: performanceLogInstance]
    }

    def edit(Long id) {
        def performanceLogInstance = PerformanceLog.get(id)
        if (!performanceLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), id])
            redirect(action: "list")
            return
        }

        [performanceLogInstance: performanceLogInstance]
    }

    def update(Long id, Long version) {
        def performanceLogInstance = PerformanceLog.get(id)
        performanceLogInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        performanceLogInstance.setLastUpdProcess("UPDATE")
        if (!performanceLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (performanceLogInstance.version > version) {

                performanceLogInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'performanceLog.label', default: 'PerformanceLog')] as Object[],
                        "Another user has updated this PerformanceLog while you were editing")
                render(view: "edit", model: [performanceLogInstance: performanceLogInstance])
                return
            }
        }

        performanceLogInstance.properties = params

        if (!performanceLogInstance.save(flush: true)) {
            render(view: "edit", model: [performanceLogInstance: performanceLogInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), performanceLogInstance.id])
        redirect(action: "show", id: performanceLogInstance.id)
    }

    def delete(Long id) {
        def performanceLogInstance = PerformanceLog.get(id)
        if (!performanceLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), id])
            redirect(action: "list")
            return
        }

        try {
            performanceLogInstance.setStaDel("1")
            performanceLogInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            performanceLogInstance.setLastUpdProcess("DELETE")
            performanceLogInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'performanceLog.label', default: 'PerformanceLog'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(PerformanceLog, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PerformanceLog, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
