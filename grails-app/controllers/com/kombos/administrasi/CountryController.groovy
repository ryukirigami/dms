package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class CountryController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]

		session.exportParams=params

		def c = Country.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
		
			if(params."sCriteria_m109KodeNegara"){
				ilike("m109KodeNegara","%" + (params."sCriteria_m109KodeNegara" as String) + "%")
			}
	
			if(params."sCriteria_m109NamaNegara"){
				ilike("m109NamaNegara","%" + (params."sCriteria_m109NamaNegara" as String) + "%")
			}
	
			ilike("staDel",'0')

			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m109ID: it.m109ID,
			
						m109KodeNegara: it.m109KodeNegara,
			
						m109NamaNegara: it.m109NamaNegara,
			
						staDel: it.staDel,
						
						createdBy: it.createdBy,
						
                        updatedBy: it.updatedBy,

                        lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[countryInstance: new Country(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def countryInstance = new Country(params)
        def cek = Country.createCriteria()
        def result = cek.list() {
            or{
                eq("m109KodeNegara",countryInstance.m109KodeNegara?.trim(), [ignoreCase: true])
                eq("m109NamaNegara",countryInstance.m109NamaNegara?.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }

        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "create", model: [countryInstance: countryInstance])
            return
        }

		countryInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		countryInstance?.lastUpdProcess = "INSERT"
		countryInstance?.setStaDel('0')

		if (!countryInstance.save(flush: true)) {
			render(view: "create", model: [countryInstance: countryInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'country.label', default: 'Country'), countryInstance?.getM109NamaNegara()])
		redirect(action: "show", id: countryInstance.id)
	}

	def show(Long id) {
		def countryInstance = Country.get(id)
		if (!countryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'country.label', default: 'Country'), id])
			redirect(action: "list")
			return
		}

		[countryInstance: countryInstance]
	}

	def edit(Long id) {
		def countryInstance = Country.get(id)
		if (!countryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'country.label', default: 'Country'), id])
			redirect(action: "list")
			return
		}

		[countryInstance: countryInstance]
	}

	def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def countryInstance = Country.get(id)
		if (!countryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'country.label', default: 'Country'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (countryInstance.version > version) {
				
				countryInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'country.label', default: 'Country')] as Object[],
				"Another user has updated this Country while you were editing")
				render(view: "edit", model: [countryInstance: countryInstance])
				return
			}
		}
        def cek = Country.createCriteria()
        def result = cek.list() {
             eq("m109NamaNegara",params.m109NamaNegara, [ignoreCase: true])
            eq("staDel",'0')
        }
        def cek2 = Country.createCriteria()
        def result2 = cek2.list() {
            eq("m109KodeNegara",params.m109KodeNegara, [ignoreCase: true])
            eq("staDel",'0')
        }
        if(result && Country.findByM109NamaNegaraAndStaDel(params.m109NamaNegara,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Negara Sudah Ada')
            render(view: "edit", model: [countryInstance: countryInstance])
            return
        }
        if(result2 && Country.findByM109KodeNegaraAndStaDel(params.m109KodeNegara,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Kode Negara Sudah Ada')
            render(view: "edit", model: [countryInstance: countryInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        countryInstance.properties = params
		countryInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		countryInstance?.lastUpdProcess = "UPDATE"
		if (!countryInstance.save(flush: true)) {
			render(view: "edit", model: [countryInstance: countryInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'country.label', default: 'Country'), countryInstance?.getM109KodeNegara()])
		redirect(action: "show", id: countryInstance.id)
	}

	def delete(Long id) {
		def countryInstance = Country.get(id)
		if (!countryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'country.label', default: 'Country'), id])
			redirect(action: "list")
			return
		}

		try {
			//countryInstance.delete(flush: true)
			countryInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			countryInstance?.lastUpdProcess = "DELETE"
			countryInstance?.setStaDel('1')
            countryInstance.lastUpdated = datatablesUtilService?.syncTime()
            countryInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'country.label', default: 'Country'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'country.label', default: 'Country'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Country, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Country, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
}
