package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class LoketController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def companyDealer = session.userCompanyDealer

        session.exportParams = params

        def c = Loket.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m404NoLoket") {
                eq("m404NoLoket",Integer.parseInt(params."sCriteria_m404NoLoket"))
            }

            if (params."sCriteria_m404NamaLoket") {
                ilike("m404NamaLoket", "%" + (params."sCriteria_m404NamaLoket" as String) + "%")
            }

            if (params."sCriteria_m404Ket") {
                ilike("m404Ket", "%" + (params."sCriteria_m404Ket" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            eq("companyDealer", companyDealer)


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m404NoLoket: it.m404NoLoket,

                    m404NamaLoket: it.m404NamaLoket,

                    m404Ket: it.m404Ket,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [loketInstance: new Loket(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def loketInstance = new Loket(params)

        loketInstance?.companyDealer = session.userCompanyDealer
        loketInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        loketInstance?.lastUpdProcess = "INSERT"

        if(loketInstance.m404NoLoket == 0){
            flash.message = "Nomor loket tidak boleh 0"
            render(view: "create", model: [loketInstance: loketInstance])
            return
        }

        def cek = Loket.createCriteria().list {
            eq("companyDealer",session.userCompanyDealer)
            or{
                eq("m404NoLoket", loketInstance.m404NoLoket)
                eq("m404NamaLoket", loketInstance.m404NamaLoket,[ignoreCase : true])
            }
        }
        //println cek
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [loketInstance: loketInstance])
            return
        }


        if (!loketInstance.save(flush: true)) {
            render(view: "create", model: [loketInstance: loketInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'loket.label', default: 'Loket'), loketInstance.id])
        redirect(action: "show", id: loketInstance.id)
    }

    def show(Long id) {
        def loketInstance = Loket.get(id)
        if (!loketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loket.label', default: 'Loket'), id])
            redirect(action: "list")
            return
        }

        [loketInstance: loketInstance]
    }

    def edit(Long id) {
        def loketInstance = Loket.get(id)


        if (!loketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loket.label', default: 'Loket'), id])
            redirect(action: "list")
            return
        }

        [loketInstance: loketInstance]
    }

    def update(Long id, Long version) {
        def loketInstance = Loket.get(id)
        if (!loketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loket.label', default: 'Loket'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (loketInstance.version > version) {

                loketInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'loket.label', default: 'Loket')] as Object[],
                        "Another user has updated this Loket while you were editing")
                render(view: "edit", model: [loketInstance: loketInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        loketInstance.properties = params
        loketInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        loketInstance?.lastUpdProcess = "UPDATE"

        if(loketInstance.m404NoLoket <= 0){
            flash.message = "Nomor loket tidak boleh 0"
            render(view: "edit", model: [loketInstance: loketInstance])
            return
        }

        if (!loketInstance.save(flush: true)) {
            render(view: "edit", model: [loketInstance: loketInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'loket.label', default: 'Loket'), loketInstance.id])
        redirect(action: "show", id: loketInstance.id)
    }

    def delete(Long id) {
        def loketInstance = Loket.get(id)
        if (!loketInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'loket.label', default: 'Loket'), id])
            redirect(action: "list")
            return
        }

        try {
            loketInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'loket.label', default: 'Loket'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'loket.label', default: 'Loket'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Loket, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Loket, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
