package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class PilihJobsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", generateSerial: "GET"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def idOperation = ServiceGratis.createCriteria().list {
            projections {
                property("operation")
            }
        }
        def idJob = []
        idOperation.each {idJob << it.id}

        def c = Operation.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            not {
                'in'('id',idJob)
            }
            if (params."sCriteria_m053Id") {
                ilike("m053Id", "%" + (params."sCriteria_m053Id" as String) + "%")
            }

            if (params."sCriteria_m053NamaOperation") {
                ilike("m053NamaOperation", "%" + (params."sCriteria_m053NamaOperation" as String) + "%")
            }

            if(exist.size()>0){
                exist.each {
                    ne("id", it as long)
                }
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        results.each {
            rows << [

                    id: it.id,

                    m053Id: it.m053Id,

                    m053NamaOperation: it.m053NamaOperation,
            ]
        }


        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

}