package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadFlatRateController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def excelImportService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def conversi=new Konversi()

//    SimpleDateFormat formatter = new SimpleDateFormat("dd/MMM/yyyy");

    def index() {
    }

    def upload() {
        def flatRateInstance = null
        def requestBody = request.JSON

        Operation operation = null
        BaseModel baseModel = null
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd")
        requestBody.each{
            flatRateInstance = new FlatRate()
            operation = Operation?.findByM053NamaOperation(it?.operation)
            baseModel = BaseModel?.findByM102KodeBaseModel(it?.baseModel)
            if(operation && baseModel && (it.t113TMT && it.t113TMT!="") && (it.t113FlatRate && it.t113FlatRate!="") && (it.t113ProductionRate && it.t113ProductionRate!="")
                    && (it.t113StaPriceMenu && it.t113StaPriceMenu!="")){
                String salesRate = it.t113FlatRate
                String prodRate = it.t113ProductionRate
                flatRateInstance.operation = operation
                flatRateInstance.baseModel = baseModel
                flatRateInstance.t113TMT = df.parse(it.t113TMT)
                flatRateInstance.t113FlatRate = Double.parseDouble(salesRate)
                flatRateInstance.t113ProductionRate = Double.parseDouble(prodRate)
                if(it.t113StaPriceMenu=='Ya'){
                    flatRateInstance.t113StaPriceMenu = '1'
                }else{
                    flatRateInstance.t113StaPriceMenu = '0'
                }
                flatRateInstance?.dateCreated = datatablesUtilService?.syncTime()
                flatRateInstance?.lastUpdated = datatablesUtilService?.syncTime()
                flatRateInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                flatRateInstance.lastUpdProcess = "UPLOAD"
                flatRateInstance.setStaDel('0')
                flatRateInstance.companyDealer = session?.userCompanyDealer
                def cek = FlatRate.find(flatRateInstance)
                if(!cek){
                    flatRateInstance.save()
                    flatRateInstance.errors.each {
                        //println it
                    }
                }
            }
        }

        flash.message = message(code: 'default.uploadFlatRate.message', default: "Save Flat Rate Done")
        render(view: "index", model: [flatRateInstance: flatRateInstance])
    }

    def view() {
        def flatRateInstance = new FlatRate(params)
        DateFormat df=new SimpleDateFormat("yyyy-MM-dd")
        //handle upload file
        Map CONFIG_FLATRATE_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A': 't113TMT',
                        'B': 'operation',
                        'C': 'baseModel',
                        'D': 't113FlatRate',
                        'E': 't113ProductionRate',
                        'F': 't113StaPriceMenu'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcelFlatRate");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [flatRateInstance: flatRateInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def flatRateList = excelImportService.columns(workbook, CONFIG_FLATRATE_COLUMN_MAP)

            jsonData = flatRateList as JSON

//            FlatRate flatRate = null
            Operation operation = null
            BaseModel baseModel = null

            String status = "0", style = ""
            def styleList = [:]
            flatRateList?.each {
                if((it?.operation && it?.operation!="") && (it?.baseModel && it?.baseModel!="") && (it.t113TMT && it.t113TMT!="") && (it.t113FlatRate && it.t113FlatRate!="") && (it.t113ProductionRate && it.t113ProductionRate!="") && (it.t113StaPriceMenu && it.t113StaPriceMenu!="")){
                    operation = Operation?.findByM053NamaOperation(it?.operation)
                    baseModel = BaseModel?.findByM102KodeBaseModel(it?.baseModel)
                    Date date = null
                    try{
                        //       date = new Date().parse('yyyy-MM-dd', it.tanggalBerlaku)
                        String tanggal = it.t113TMT
                        date = df.parse(tanggal)
                    }catch(Exception ex){
                        //println "Exc "+ex
                    }
                    if(!operation){
                        jmlhDataError++;
                        styleList.operation = "style='color:red;'"
                        status = "1";
                        //println "eror operation"
                    }
                    if(!baseModel){
                        jmlhDataError++;
                        styleList.baseModel = "style='color:red;'"
                        status = "1";
                        //println "eror baseModel"
                    }else if(!date){
                        jmlhDataError++;
                        //println "eror date"
                        styleList.tanggal = "style='color:red;'"
                        status = "1"
                    }
//                    if(!flatRate){
//                        jmlhDataError++;
//                        status = "1";
//                    }

                } else {
                    jmlhDataError++;
                    status = "1";
                }


                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td "+styleList.tanggal+">\n" +
                        "                                "+it.t113TMT+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.operation+">\n" +
                        "                                 "+it.operation+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.boseModel+">\n" +
                        "                                 "+it.baseModel+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.t113FlatRate)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.t113ProductionRate)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t113StaPriceMenu+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadFlatRate.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadFlatRate.message', default: "Read File Done")
        }

        render(view: "index", model: [flatRateInstance: flatRateInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }

}
