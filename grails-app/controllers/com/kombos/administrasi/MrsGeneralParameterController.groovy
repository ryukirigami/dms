package com.kombos.administrasi

import grails.converters.JSON
import org.springframework.web.servlet.support.RequestContextUtils

class MrsGeneralParameterController {

    def invitationService

    def datatablesUtilService

    def index() {
        GeneralParameter generalParameter = invitationService.currentGeneralParameter(session?.userCompanyDealer)
        [generalParameter: generalParameter]
    }


    def doSave() {
        def generalParameter = GeneralParameter.get(params.id)
        if (!generalParameter) {
            generalParameter = new GeneralParameter()
            generalParameter.companyDealer = session?.userCompanyDealer
        }
        params?.lastUpdated = datatablesUtilService?.syncTime()
        generalParameter.properties = params
        generalParameter.save(flush: true)
        if (generalParameter.hasErrors()) {
            def messageSource = grailsAttributes.applicationContext.messageSource
            def locale = RequestContextUtils.getLocale(request)
            String error = ""
            generalParameter.errors.fieldError.each {
                error += messageSource.getMessage(it, locale) + "\n"
            }
            def result = [status: "FAIL", error: error]
            render result as JSON
        } else {
            def result = [status: "OK"]
            render result as JSON
        }
    }
}
