package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class KategoriJobController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = KategoriJob.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m055KategoriJob") {
                ilike("m055KategoriJob", "%" + (params."sCriteria_m055KategoriJob" as String) + "%")
            }

            if (params."sCriteria_m055staReadOnly") {
                ilike("m055staReadOnly", "%" + (params."sCriteria_m055staReadOnly" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_m055ID") {
                ilike("m055ID", "%" + (params."sCriteria_m055ID" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m055KategoriJob: it.m055KategoriJob,

                    m055staReadOnly: it.m055staReadOnly=="0"?"Read Only":"Editable",

                    m055WarnaBord: it?.m055WarnaBord,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    m055ID: it.m055ID,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [kategoriJobInstance: new KategoriJob(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def kategoriJobInstance = new KategoriJob(params)
        kategoriJobInstance?.m055staReadOnly = "1"
        kategoriJobInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        kategoriJobInstance?.lastUpdProcess = "INSERT"
        def cek = KategoriJob.createCriteria().list() {
            and{
                eq("m055KategoriJob",kategoriJobInstance.m055KategoriJob?.trim(), [ignoreCase: true])
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [kategoriJobInstance: kategoriJobInstance])
            return
        }

        if (!kategoriJobInstance.save(flush: true)) {
            render(view: "create", model: [kategoriJobInstance: kategoriJobInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), kategoriJobInstance.id])
        redirect(action: "show", id: kategoriJobInstance.id)
    }

    def show(Long id) {
        def kategoriJobInstance = KategoriJob.get(id)
        String keterangan = kategoriJobInstance?.m055staReadOnly=="0" ? "Read Only" : "Editable"
        if (!kategoriJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), id])
            redirect(action: "list")
            return
        }

        [kategoriJobInstance: kategoriJobInstance, keterangan: keterangan]
    }

    def edit(Long id) {
        def kategoriJobInstance = KategoriJob.get(id)
        if (!kategoriJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), id])
            redirect(action: "list")
            return
        }

        [kategoriJobInstance: kategoriJobInstance]
    }

    def update(Long id, Long version) {
        def kategoriJobInstance = KategoriJob.get(id)
        if (!kategoriJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (kategoriJobInstance.version > version) {

                kategoriJobInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'kategoriJob.label', default: 'Kategori Job')] as Object[],
                        "Another user has updated this KategoriJob while you were editing")
                render(view: "edit", model: [kategoriJobInstance: kategoriJobInstance])
                return
            }
        }

        def cek = KategoriJob.createCriteria()
        def result = cek.list() {
            and{
                eq("m055KategoriJob",params.m055KategoriJob?.trim(), [ignoreCase: true])
            }
        }
        if(result){
            result.each {
                if(id!=it.id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [kategoriJobInstance: kategoriJobInstance])
                    return
                }
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        kategoriJobInstance.properties = params
        kategoriJobInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        kategoriJobInstance?.lastUpdProcess = "UPDATE"

        if (!kategoriJobInstance.save(flush: true)) {
            render(view: "edit", model: [kategoriJobInstance: kategoriJobInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), kategoriJobInstance.id])
        redirect(action: "show", id: kategoriJobInstance.id)
    }

    def delete(Long id) {
        def kategoriJobInstance = KategoriJob.get(id)
        if (!kategoriJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), id])
            redirect(action: "list")
            return
        }

        try {
            kategoriJobInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'kategoriJob.label', default: 'Kategori Job'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(KategoriJob, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(KategoriJob, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
