package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StatusStallController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = StatusStall.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_stall"){
				//eq("stall",params."sCriteria_stall")
				eq("stall",Stall.findByM022NamaStallIlike("%"+params."sCriteria_stall"+"%"))
			}

			if(params."sCriteria_m025Tmt"){
				ge("m025Tmt",params."sCriteria_m025Tmt")
				lt("m025Tmt",params."sCriteria_m025Tmt" + 1)
			}
	
			if(params."sCriteria_m025Sta"){
				ilike("m025Sta","%" + (params."sCriteria_m025Sta" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						stall: it.stall.m022NamaStall,
			
						m025Tmt: it.m025Tmt?it.m025Tmt.format(dateFormat):"",
			
						m025Sta: it.m025Sta,
						
						createdBy: it.createdBy,
						
						updatedBy: it.updatedBy,
						
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[statusStallInstance: new StatusStall(params)]
	}

	def save() {
		def statusStallInstance = new StatusStall(params)
        def cek = StatusStall.findByStallAndM025TmtAndM025Sta(Stall.findById(params.stall.id), statusStallInstance?.m025Tmt,statusStallInstance?.m025Sta)
        if(cek){
            flash.message = " Data Sudah Ada"
            render(view: "create", model: [statusStallInstance: statusStallInstance])
            return
        }
		statusStallInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		statusStallInstance?.lastUpdProcess = "INSERT"
		if (!statusStallInstance.save(flush: true)) {
			render(view: "create", model: [statusStallInstance: statusStallInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), statusStallInstance.id])
		redirect(action: "show", id: statusStallInstance.id)
	}

	def show(Long id) {
		def statusStallInstance = StatusStall.get(id)
		if (!statusStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), id])
			redirect(action: "list")
			return
		}

		[statusStallInstance: statusStallInstance]
	}

	def edit(Long id) {
		def statusStallInstance = StatusStall.get(id)
		if (!statusStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), id])
			redirect(action: "list")
			return
		}

		[statusStallInstance: statusStallInstance]
	}

	def update(Long id, Long version) {
		def statusStallInstance = StatusStall.get(id)
		if (!statusStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (statusStallInstance.version > version) {
				
				statusStallInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'statusStall.label', default: 'StatusStall')] as Object[],
				"Another user has updated this StatusStall while you were editing")
				render(view: "edit", model: [statusStallInstance: statusStallInstance])
				return
			}
		}
        def cek = StatusStall.findByStallAndM025TmtAndM025Sta(Stall.findById(params.stall.id), params.m025Tmt,params.m025Sta)
        if(cek && cek.id != id){
            flash.message = " Data Sudah Ada"
            render(view: "edit", model: [statusStallInstance: statusStallInstance])
            return
        }
		statusStallInstance.properties = params
		statusStallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		statusStallInstance?.lastUpdProcess = "UPDATE"
		if (!statusStallInstance.save(flush: true)) {
			render(view: "edit", model: [statusStallInstance: statusStallInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), statusStallInstance.id])
		redirect(action: "show", id: statusStallInstance.id)
	}

	def delete(Long id) {
		def statusStallInstance = StatusStall.get(id)
		if (!statusStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), id])
			redirect(action: "list")
			return
		}

		try {
			statusStallInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'statusStall.label', default: 'StatusStall'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(StatusStall, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(StatusStall, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
