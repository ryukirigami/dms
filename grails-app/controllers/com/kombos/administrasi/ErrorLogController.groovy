package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat

class ErrorLogController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = ErrorLog.createCriteria()
        DateFormat df=new SimpleDateFormat("HH:mm")
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_m779Tanggal" && params."sCriteria_m779TanggalAkhir") {
                ge("m779Tanggal", params."sCriteria_m779Tanggal")
                lt("m779Tanggal", params."sCriteria_m779TanggalAkhir" + 1)
            }

            if (params."sCriteria_m779Jam") {
                Date date1 = df.parse(params."sCriteria_m779Jam")
                ilike("m779Jam", "%"+date1+"%")
            }

            if (params."sCriteria_userProfile") {
                userProfile{
                    eq("id", (params."sCriteria_userProfile" as Long))
                }
            }

            if (params."sCriteria_namaForm") {
                eq("namaForm", params."sCriteria_namaForm")
            }

            if (params."sCriteria_m779Event") {
                ilike("m779Event", "%" + (params."sCriteria_m779Event" as String) + "%")
            }

            if (params."sCriteria_m779ErrorMsg") {
                ilike("m779ErrorMsg", "%" + (params."sCriteria_m779ErrorMsg" as String) + "%")
            }

            if (params."sCriteria_m779Id") {
                eq("m779Id", params."sCriteria_m779Id")
            }

            if (params."sCriteria_m779xNamaUser") {
                ilike("m779xNamaUser", "%" + (params."sCriteria_m779xNamaUser" as String) + "%")
            }

            if (params."sCriteria_m779xNamaDivisi") {
                ilike("m779xNamaDivisi", "%" + (params."sCriteria_m779xNamaDivisi" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m779Tanggal: it.m779Tanggal ? it.m779Tanggal.format(dateFormat) : "",

                    m779Jam: new SimpleDateFormat("HH:mm:ss").format(new Date(it.m779Jam?.getTime())),

                    userProfile: it.userProfile.fullname,

                    namaForm: it.namaForm.t004NamaAlias,

                    m779Event: it.m779Event,

                    m779ErrorMsg: it.m779ErrorMsg,

                    m779Id: it.m779Id,

                    m779xNamaUser: it.m779xNamaUser,

                    m779xNamaDivisi: it.m779xNamaDivisi,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [errorLogInstance: new ErrorLog(params)]
    }

    def save() {
        def errorLogInstance = new ErrorLog(params)
        errorLogInstance.setStaDel("0")
        errorLogInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        errorLogInstance.setLastUpdProcess("INSERT")
        Calendar c2=Calendar.getInstance();
        c2.setTime(new Date());
        int hours2 = c2.get(Calendar.HOUR)
        int mins2=c2.get(Calendar.MINUTE)
        int seconds2=c2.get(Calendar.SECOND)
        errorLogInstance.setM779Jam(new Time(hours2,mins2,seconds2))
        if (!errorLogInstance.save(flush: true)) {
            render(view: "create", model: [errorLogInstance: errorLogInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), errorLogInstance.id])
        redirect(action: "show", id: errorLogInstance.id)
    }

    def show(Long id) {
        def errorLogInstance = ErrorLog.get(id)
        if (!errorLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), id])
            redirect(action: "list")
            return
        }

        [errorLogInstance: errorLogInstance]
    }

    def edit(Long id) {
        def errorLogInstance = ErrorLog.get(id)
        if (!errorLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), id])
            redirect(action: "list")
            return
        }

        [errorLogInstance: errorLogInstance]
    }

    def update(Long id, Long version) {
        def errorLogInstance = ErrorLog.get(id)
        errorLogInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        errorLogInstance.setLastUpdProcess("UPDATE")
        if (!errorLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (errorLogInstance.version > version) {

                errorLogInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'errorLog.label', default: 'ErrorLog')] as Object[],
                        "Another user has updated this ErrorLog while you were editing")
                render(view: "edit", model: [errorLogInstance: errorLogInstance])
                return
            }
        }

        errorLogInstance.properties = params

        if (!errorLogInstance.save(flush: true)) {
            render(view: "edit", model: [errorLogInstance: errorLogInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), errorLogInstance.id])
        redirect(action: "show", id: errorLogInstance.id)
    }

    def delete(Long id) {
        def errorLogInstance = ErrorLog.get(id)
        if (!errorLogInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), id])
            redirect(action: "list")
            return
        }

        try {
            errorLogInstance.setStaDel("1")
            errorLogInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            errorLogInstance.setLastUpdProcess("DELETE")
            errorLogInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'errorLog.label', default: 'ErrorLog'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(ErrorLog, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(ErrorLog, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
