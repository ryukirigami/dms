package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StallJobController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = StallJob.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            and{
                eq("staDel", "0")
                stall{
                    eq("staDel","0")
                }
                operation{
                    eq("staDel","0")
                }
            }
            if (params."sCriteria_stall") {
                stall{
                    ilike("m022NamaStall", "%" + (params."sCriteria_stall" as String) + "%")
                }
                //eq("stall", params."sCriteria_stall")
            }

            if (params."sCriteria_m024Tanggal") {
                ge("m024Tanggal", params."sCriteria_m024Tanggal")
                lt("m024Tanggal", params."sCriteria_m024Tanggal" + 1)
            }

            if (params."sCriteria_operation") {
                operation{
				or{
                    ilike("m053NamaOperation", "%" + (params."sCriteria_operation" as String) + "%")
					ilike("m053JobsId", "%" + (params."sCriteria_operation" as String) + "%")
					serial{
					ilike("m052NamaSerial", "%" + (params."sCriteria_operation" as String) + "%")
					}
					}
                }
                //eq("operation", params."sCriteria_operation")
            }
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						stall: it.stall?.m022NamaStall,
			
						m024Tanggal: it.m024Tanggal?it.m024Tanggal.format(dateFormat):"",
			
						operation: it.operation?.m053JobsId+"."+it.operation?.serial?.m052NamaSerial+"."+it.operation?.m053NamaOperation,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[stallJobInstance: new StallJob(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def stallJobInstance = new StallJob(params)

        if(StallJob.findByStallAndM024TanggalAndOperationAndStaDel(Stall.findById(params.stall.id),
                params.m024Tanggal,Operation.findById(params.operation.id),"0")){
            flash.message = "Data sudah ada"
            render(view: "create", model: [stallJobInstance: stallJobInstance])
            return
        }
        stallJobInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stallJobInstance?.lastUpdProcess = "INSERT"
        stallJobInstance?.setStaDel('0')

        if (!stallJobInstance.save(flush: true)) {
			render(view: "create", model: [stallJobInstance: stallJobInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), stallJobInstance.id])
		redirect(action: "show", id: stallJobInstance.id)
	}

	def show(Long id) {
		def stallJobInstance = StallJob.get(id)
		if (!stallJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), id])
			redirect(action: "list")
			return
		}

		[stallJobInstance: stallJobInstance]
	}

	def edit(Long id) {
		def stallJobInstance = StallJob.get(id)
		if (!stallJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), id])
			redirect(action: "list")
			return
		}

		[stallJobInstance: stallJobInstance]
	}

	def update(Long id, Long version) {
		def stallJobInstance = StallJob.get(id)
		if (!stallJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (stallJobInstance.version > version) {
				
				stallJobInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'stallJob.label', default: 'Stall Job')] as Object[],
				"Another user has updated this StallJob while you were editing")
				render(view: "edit", model: [stallJobInstance: stallJobInstance])
				return
			}
		}
        def stallJobCek = StallJob.findByStallAndM024TanggalAndOperationAndStaDel(Stall.findById(params.stall.id),
                params.m024Tanggal,Operation.findById(params.operation.id),"0")
        if(stallJobCek){
            if(id!=stallJobCek.id){
                flash.message = "Data sudah ada"
                render(view: "edit", model: [stallJobInstance: stallJobInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        stallJobInstance.properties = params
        stallJobInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stallJobInstance?.lastUpdProcess = "UPDATE"

		if (!stallJobInstance.save(flush: true)) {
			render(view: "edit", model: [stallJobInstance: stallJobInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), stallJobInstance.id])
		redirect(action: "show", id: stallJobInstance.id)
	}

	def delete(Long id) {
		def stallJobInstance = StallJob.get(id)
		if (!stallJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), id])
			redirect(action: "list")
			return
		}

		try {
            stallJobInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stallJobInstance?.lastUpdProcess = "DELETE"
            stallJobInstance?.setStaDel('1')
            stallJobInstance?.lastUpdated = datatablesUtilService?.syncTime()
            stallJobInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'stallJob.label', default: 'Stall Job'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(StallJob, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(StallJob, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
