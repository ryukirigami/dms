package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class ProvinsiController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def provinsiService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render provinsiService.datatablesList(params) as JSON
	}

	def create() {
		def result = provinsiService.create(params)

        if(!result.error)
            return [provinsiInstance: result.provinsiInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		 def result = provinsiService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Provinsi", result.provinsiInstance.id])
            redirect(action:'show', id: result.provinsiInstance.id)
            return
        }else if(result.error.code == "dataGanda"){
            flash.message = "Data Sudah Ada"
            render(view:'create', model:[provinsiInstance: result.provinsiInstance])
            return
        }

        render(view:'create', model:[provinsiInstance: result.provinsiInstance])
	}

	def show(Long id) {
		def result = provinsiService.show(params)

		if(!result.error)
			return [ provinsiInstance: result.provinsiInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = provinsiService.show(params)

		if(!result.error)
			return [ provinsiInstance: result.provinsiInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {

		 def result = provinsiService.update(params)
        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Provinsi", params.id])
            redirect(action:'show', id: params.id)
            return
        }else if(result.error.code == "dataGanda"){
            flash.message = "Data Sudah Ada"
            render(view:'edit', model:[provinsiInstance: result.provinsiInstance])
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[provinsiInstance: result.provinsiInstance.attach()])
	}

	def delete() {
		def result = provinsiService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Provinsi", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Provinsi, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
