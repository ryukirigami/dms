package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ContactPersonController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		
		session.exportParams=params
		
		def c = ContactPerson.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			if(params."sCriteria_companyDealer"){
                companyDealer{
                    ilike("m011NamaWorkshop","%"+params."sCriteria_companyDealer"+"%")
                }
            }
			if(params."sCriteria_m013Tanggal"){
				ge("m013Tanggal",params."sCriteria_m013Tanggal")
				lt("m013Tanggal",params."sCriteria_m013Tanggal" + 1)
			}
	
			if(params."sCriteria_m013NamaCP"){
				ilike("m013NamaCP","%" + (params."sCriteria_m013NamaCP" as String) + "%")
			}
	
			if(params."sCriteria_m013JabatanCP"){
                m013JabatanCP{
                    ilike("m014JabatanManPower","%" + (params."sCriteria_m013JabatanCP" as String) + "%")
                }
			}
	
			if(params."sCriteria_m013Telp1"){
				ilike("m013Telp1","%" + (params."sCriteria_m013Telp1" as String) + "%")
			}
	
			if(params."sCriteria_m013Telp2"){
				ilike("m013Telp2","%" + (params."sCriteria_m013Telp2" as String) + "%")
			}

			
			switch(sortProperty){
                case "companyDealer":
                    companyDealer{
                        order("m011NamaWorkshop",sortDir)
                    }
                    break;
                case "m013JabatanCP":
                    m013JabatanCP{
                        order("m014JabatanManPower",sortDir)
                    }
                    break;
                default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companyDealer: it.companyDealer.m011NamaWorkshop,
			
						m013Tanggal: it.m013Tanggal?it.m013Tanggal.format(dateFormat):"",
			
						m013NamaCP: it.m013NamaCP,
			
						m013JabatanCP: it.m013JabatanCP.m014JabatanManPower,
			
						m013Telp1: it.m013Telp1,
			
						m013Telp2: it.m013Telp2,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[contactPersonInstance: new ContactPerson(params)]
	}

	def save() {
        def manPower = ManPower.findByM014JabatanManPowerAndStaDel(params.m013JabatanCP,'0')
        def contactPersonInstance = new ContactPerson()
        contactPersonInstance.dateCreated = datatablesUtilService?.syncTime()
        contactPersonInstance.lastUpdated = datatablesUtilService?.syncTime()
        contactPersonInstance.companyDealer = CompanyDealer.get(params.companyDealer.id.toLong())
        contactPersonInstance.m013JabatanCP = manPower
        contactPersonInstance.m013Tanggal = params.m013Tanggal
        contactPersonInstance.m013NamaCP = params.m013NamaCP
        contactPersonInstance.m013Telp1 = params.m013Telp1
        contactPersonInstance.m013Telp2 = params.m013Telp2
        contactPersonInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        contactPersonInstance.setLastUpdProcess("INSERT")
        def cd = CompanyDealer.findById(params.companyDealer.id)
        def cPerson = ContactPerson.createCriteria().list {
            eq("companyDealer",cd)
        }
        def cek = ContactPerson.createCriteria().list {
            companyDealer{
                eq('id',params.companyDealer.id.toLong())
            }
            m013JabatanCP{
                eq('m014JabatanManPower',params.m013JabatanCP,[ignoreCase : true])
            }
            eq('m013NamaCP',params.m013NamaCP.trim(),[ignoreCase: true])
            eq('m013Telp1',params.m013Telp1)
            eq('m013Telp2',params.m013Telp2)
            ge("m013Tanggal",params.m013Tanggal)
            lt("m013Tanggal",params.m013Tanggal + 1)
        }
        if(cPerson){
            for(find in cPerson){
                if(find.m013Tanggal.after(params.m013Tanggal) || find.m013Tanggal.compareTo(params.m013Tanggal)==0){
                    flash.message = 'Contact Person untuk Company Dealer tersebut sudah ada, dan aktif s.d. '+find.m013Tanggal.format('dd/MM/yyyy');
                    render(view: "create", model: [contactPersonInstance: contactPersonInstance])
                    return
                }
            }
        }
        if(cek){
            flash.message = 'Data sudah ada'
            render(view: "create", model: [contactPersonInstance: contactPersonInstance])
            return
        }
        if (!contactPersonInstance.save(flush: true)) {
            flash.message = message(code: 'contactPerson.gagalSimpan.label', default: 'Data Tidak Bisa disimpan')
            render(view: "create", model: [contactPersonInstance: contactPersonInstance])
            return
		}
                
		flash.message = message(code: 'default.created.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), ''])
		redirect(action: "show", id: contactPersonInstance.id)
	}

	def show(Long id) {
		def contactPersonInstance = ContactPerson.get(id)
		if (!contactPersonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), id])
			redirect(action: "list")
			return
		}

		[contactPersonInstance: contactPersonInstance]
	}

	def edit(Long id) {
		def contactPersonInstance = ContactPerson.get(id)
		if (!contactPersonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), id])
			redirect(action: "list")
			return
		}

		[contactPersonInstance: contactPersonInstance]
	}

	def update(Long id, Long version) {
        def manPower = ManPower.findByM014JabatanManPowerAndStaDel(params.m013JabatanCP,'0')
        def contactPersonInstance = ContactPerson.get(id)
		if (!contactPersonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (contactPersonInstance.version > version) {
				
				contactPersonInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'contactPerson.label', default: 'ContactPerson')] as Object[],
				"Another user has updated this ContactPerson while you were editing")
				render(view: "edit", model: [contactPersonInstance: contactPersonInstance])
				return
			}
		}
        def cd = CompanyDealer.findById(params.companyDealer.id)
        def cPerson = ContactPerson.createCriteria().list {
            eq("companyDealer",cd)
        }
        def cek = ContactPerson.createCriteria().list {
            companyDealer{
                eq('id',params.companyDealer.id.toLong())
            }
            m013JabatanCP{
                eq('m014JabatanManPower',params.m013JabatanCP,[ignoreCase : true])
            }
            eq('m013NamaCP',params.m013NamaCP.trim(),[ignoreCase: true])
            eq('m013Telp1',params.m013Telp1)
            eq('m013Telp2',params.m013Telp2)
            ge("m013Tanggal",params.m013Tanggal)
            lt("m013Tanggal",params.m013Tanggal + 1)
        }
        if(cPerson){
            for(cari in cPerson){
                if(id<cari.id){
                    if(cari.m013Tanggal.before(params.m013Tanggal) || cari.m013Tanggal.compareTo(params.m013Tanggal)==0){
                        flash.message = 'Contact Person untuk Company Dealer tersebut sudah ada, dan aktif s.d. '+cari.m013Tanggal.format('dd/MM/yyyy');
                        render(view: "edit", model: [contactPersonInstance: contactPersonInstance])
                        return
                    }
                }
                if(id>cari.id){
                    if(cari.m013Tanggal.after(params.m013Tanggal) || cari.m013Tanggal.compareTo(params.m013Tanggal)==0){
                        flash.message = 'Contact Person untuk Company Dealer tersebut sudah ada, dan aktif s.d. '+cari.m013Tanggal.format('dd/MM/yyyy');
                        render(view: "edit", model: [contactPersonInstance: contactPersonInstance])
                        return
                    }
                }
            }
        }
        if(cek){
            for(find in cek){
                if(find.id!=id){
                    flash.message = 'Data sudah ada'
                    render(view: "edit", model: [contactPersonInstance: contactPersonInstance])
                    return
                }
            }
        }


        contactPersonInstance.lastUpdated = datatablesUtilService?.syncTime()
        contactPersonInstance.companyDealer = CompanyDealer.get(params.companyDealer.id.toLong())
        contactPersonInstance.m013JabatanCP = manPower
        contactPersonInstance.m013Tanggal = params.m013Tanggal
        contactPersonInstance.m013NamaCP = params.m013NamaCP
        contactPersonInstance.m013Telp1 = params.m013Telp1
        contactPersonInstance.m013Telp2 = params.m013Telp2
        contactPersonInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        contactPersonInstance.setLastUpdProcess("UPDATE")
		if (!contactPersonInstance.save(flush: true)) {
			render(view: "edit", model: [contactPersonInstance: contactPersonInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), ''])
		redirect(action: "show", id: contactPersonInstance.id)
	}

	def delete(Long id) {
		def contactPersonInstance = ContactPerson.get(id)
		if (!contactPersonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), id])
			redirect(action: "list")
			return
		}

		try {
			contactPersonInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'contactPerson.label', default: 'ContactPerson'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(ContactPerson, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ContactPerson, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
