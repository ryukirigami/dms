package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.SimpleDateFormat

class RateBankController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
    def conversi = new Konversi()
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = RateBank.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_bank"){
			//	eq("bank",Bank.findByM702NamaBankIlike("%"+ params."sCriteria_bank"+ "%"))

                bank{
                    ilike("m702NamaBank","%"+ params."sCriteria_bank"+ "%" )
                }
			}

			if(params."sCriteria_mesinEdc"){
			//	eq("mesinEdc",MesinEdc.findByM704NamaMesinEdcIlike("%"+params."sCriteria_mesinEdc"+"%"))
                mesinEdc{
                    ilike("m704NamaMesinEdc","%"+params."sCriteria_mesinEdc"+"%" )
                }
			}

			if(params."sCriteria_m703TglBerlaku"){
				ge("m703TglBerlaku",params."sCriteria_m703TglBerlaku")
				lt("m703TglBerlaku",params."sCriteria_m703TglBerlaku" + 1)
			}

			if(params."sCriteria_m703JmlMin"){
                def jmlMin = params.sCriteria_m703JmlMin.replace(",","")
				eq("m703JmlMin",Double.parseDouble(jmlMin))
			}

			if(params."sCriteria_m703JmlMax"){
                def jmlMax = params.sCriteria_m703JmlMax.replace(",","")
				eq("m703JmlMax",Double.parseDouble(jmlMax))
			}
	
			if(params."sCriteria_m703StaPersenRp"){
				ilike("m703StaPersenRp","%" + (params."sCriteria_m703StaPersenRp" as String) + "%")
			}

			if(params."sCriteria_m703RatePersen"){
				eq("m703RatePersen",Double.parseDouble(params."sCriteria_m703RatePersen"))
			}

			if(params."sCriteria_m703RateRp"){
                def rateRp = params.sCriteria_m703RateRp.replace(",","")
				eq("m703RateRp",Double.parseDouble(rateRp))
			}
	

				ilike("staDel","0")

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						bank: it.bank.m702NamaBank,
			
						mesinEdc: it.mesinEdc.m704NamaMesinEdc,
			
						m703TglBerlaku: it.m703TglBerlaku?it.m703TglBerlaku.format("dd/MM/YYYY"):"",
			
						m703JmlMin: conversi.toRupiah(it.m703JmlMin),
			
						m703JmlMax: conversi.toRupiah(it.m703JmlMax),
			
						m703StaPersenRp: it.m703StaPersenRp,
			
						m703RatePersen: it.m703RatePersen,
			
						m703RateRp: conversi.toRupiah(it.m703RateRp),
			
						m703StaDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[rateBankInstance: new RateBank(params)]
	}

	def save() {
		def rateBankInstance = new RateBank(params)
        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy")


        def cek = RateBank.createCriteria()
        def result = cek.list() {
            and{
                eq("mesinEdc", MesinEdc.get(params.mesinEdc.id))
                eq("bank", Bank.get(params.bank.id))
                eq("m703TglBerlaku",formater.parse(params.m703TglBerlaku_dp))
                eq("staDel","0")
            }
        }

        //find(operationInstance)
        if(result){
            flash.message = message(code: 'default.created.rateBank.error.message', default: 'Data has been used.')
            render(view: "create", model: [rateBankInstance: rateBankInstance])
            return
        }

        String staRpPersen = params.m703StaPersenRp
        rateBankInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        rateBankInstance?.lastUpdProcess = "INSERT"


        if(staRpPersen == 'p'){
                    rateBankInstance.m703RateRp = 0
                    
                }else if(staRpPersen == 'r'){
                   rateBankInstance.m703RatePersen = 0
                   
                }
        rateBankInstance.staDel = '0'
		if (!rateBankInstance.save(flush: true)) {
			render(view: "create", model: [rateBankInstance: rateBankInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'rateBank.label', default: 'RateBank'), rateBankInstance.id])
		redirect(action: "show", id: rateBankInstance.id)
	}

	def show(Long id) {
		def rateBankInstance = RateBank.get(id)
		if (!rateBankInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'rateBank.label', default: 'RateBank'), id])
			redirect(action: "list")
			return
		}

		[rateBankInstance: rateBankInstance]
	}

	def edit(Long id) {
		def rateBankInstance = RateBank.get(id)
		if (!rateBankInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'rateBank.label', default: 'RateBank'), id])
			redirect(action: "list")
			return
		}

		[rateBankInstance: rateBankInstance]
	}

	def update(Long id, Long version) {
		def rateBankInstance = RateBank.get(id)
		if (!rateBankInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'rateBank.label', default: 'RateBank'), id])
			redirect(action: "list")
			return
		}

        SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy")


        def cek = RateBank.createCriteria()
        def result = cek.list() {
            and{
                eq("mesinEdc", MesinEdc.get(params.mesinEdc.id))
                eq("bank", Bank.get(params.bank.id))
                eq("m703TglBerlaku",formater.parse(params.m703TglBerlaku_dp))
                notEqual("id", Long.parseLong(params.id))
                eq("staDel","0")
            }
        }

        //find(operationInstance)
        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data has been used.')
            render(view: "edit", model: [rateBankInstance: rateBankInstance])
            return
        }


        if (version != null) {
			if (rateBankInstance.version > version) {
				
				rateBankInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'rateBank.label', default: 'RateBank')] as Object[],
				"Another user has updated this RateBank while you were editing")
				render(view: "edit", model: [rateBankInstance: rateBankInstance])
				return
			}
		}
        String staRpPersen = params.m703StaPersenRp

        rateBankInstance.properties = params
        if(staRpPersen == 'p'){
            rateBankInstance.m703RateRp = 0

        }else if(staRpPersen == 'r'){
            rateBankInstance.m703RatePersen = 0

        }

        rateBankInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        rateBankInstance?.lastUpdProcess = "UPDATE"

		if (!rateBankInstance.save(flush: true)) {
			render(view: "edit", model: [rateBankInstance: rateBankInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'rateBank.label', default: 'RateBank'), rateBankInstance.id])
		redirect(action: "show", id: rateBankInstance.id)
	}

	def delete(Long id) {
		def rateBankInstance = RateBank.get(id)
		if (!rateBankInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'rateBank.label', default: 'RateBank'), id])
			redirect(action: "list")
			return
		}

		try {
            rateBankInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            rateBankInstance?.lastUpdProcess = "DELETE"
            rateBankInstance.staDel = '1'
			rateBankInstance.save()
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'rateBank.label', default: 'RateBank'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'rateBank.label', default: 'RateBank'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDel(RateBank, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(RateBank, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}


    def cekMinMax(){
        def max = Double.parseDouble(params.max)
        def min = Double.parseDouble(params.min)
        def status = "true"
        def res = [:]
        if(max<min)
            status = "false"

        res.put("status", status)

        render res as JSON
    }
	
}
