package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import groovy.sql.Sql
import org.springframework.dao.DataIntegrityViolationException

class HistoryPrintController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST" ,view:"GET",autoCompleteCode:"GET"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']

    def dataSource

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = HistoryPrint.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

			if(params."sCriteria_namaDokumen"){
                namaDokumen{
                    eq("id",(params."sCriteria_namaDokumen" as Long))
                }

			}
	
			if(params."sCriteria_t951NoDokumen"){
				ilike("t951NoDokumen","%" + (params."sCriteria_t951NoDokumen" as String) + "%")
			}

			if(params."sCriteria_t951PrintCounter"){
				eq("t951PrintCounter",Long.parseLong(params."sCriteria_t951PrintCounter"))
			}

			if(params."sCriteria_t951Tanggal" && params."sCriteria_t951TanggalAkhir"){
                ge("t951Tanggal",params."sCriteria_t951Tanggal")
				lt("t951Tanggal",params."sCriteria_t951TanggalAkhir" + 1)
			}

			if(params."sCriteria_userProfile"){
                def role = Role.get(params."sCriteria_role")
                def sql=new Sql(dataSource)
                def user2= User.findByUsernameIlike("%"+(params."sCriteria_userProfile")+"%")
                if(user2==null || (params."sCriteria_role"==null || params."sCriteria_role"=="")){
                    userProfile{
                        ilike("username","%"+(params."sCriteria_userProfile")+"%")
                    }
                }else{
                    if(role!=null){
                        sql.eachRow("""\
                        select user_id from dom_user_roles where role_id = '${role.id}'
                    """) {
                            def user = User.get(it.USER_ID)
                            if(user==user2){
                                userProfile{
                                    eq("username",user2.username)
                                }
                            }
                        }
                    }
                }

			}

            if (params."sCriteria_role") {
                def role = Role.get(params."sCriteria_role")
                def sql=new Sql(dataSource)
                if(role!=null && (params."sCriteria_userProfile"==null || params."sCriteria_userProfile"=="")){
                    sql.eachRow("""\
                        select user_id from dom_user_roles where role_id = '${role.id}'
                    """) {
                        def user = User.get(it.USER_ID)
                        userProfile{
                            eq("username",user.username)
                        }
                    }
                }
            }
	

			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						namaDokumen: it.namaDokumen.m007NamaDokumen,
			
						t951NoDokumen: it.t951NoDokumen,
			
						t951PrintCounter: it.t951PrintCounter,
			
						t951Tanggal: it.t951Tanggal?it.t951Tanggal.format(dateFormat):"",
			
						userProfile: it.userProfile.username+" ("+it.userProfile.t001Inisial+") - "+it.userProfile.getRoles().name,
			
						t951Alasan: it.t951Alasan,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[historyPrintInstance: new HistoryPrint(params)]
	}

    def view(){
        String result = "1"
        render result
    }

	def save() {
		def historyPrintInstance = new HistoryPrint(params)
        historyPrintInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        historyPrintInstance?.setLastUpdProcess("INSERT")
		if (!historyPrintInstance.save(flush: true)) {
			render(view: "create", model: [historyPrintInstance: historyPrintInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), historyPrintInstance.id])
		redirect(action: "show", id: historyPrintInstance.id)
	}

	def show(Long id) {
		def historyPrintInstance = HistoryPrint.get(id)
		if (!historyPrintInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), id])
			redirect(action: "list")
			return
		}

		[historyPrintInstance: historyPrintInstance]
	}

	def edit(Long id) {
		def historyPrintInstance = HistoryPrint.get(id)
		if (!historyPrintInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), id])
			redirect(action: "list")
			return
		}

		[historyPrintInstance: historyPrintInstance]
	}

	def update(Long id, Long version) {
		def historyPrintInstance = HistoryPrint.get(id)
        historyPrintInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        historyPrintInstance?.setLastUpdProcess("UPDATE")
		if (!historyPrintInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (historyPrintInstance.version > version) {
				
				historyPrintInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'historyPrint.label', default: 'HistoryPrint')] as Object[],
				"Another user has updated this HistoryPrint while you were editing")
				render(view: "edit", model: [historyPrintInstance: historyPrintInstance])
				return
			}
		}

		historyPrintInstance.properties = params

		if (!historyPrintInstance.save(flush: true)) {
			render(view: "edit", model: [historyPrintInstance: historyPrintInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), historyPrintInstance.id])
		redirect(action: "show", id: historyPrintInstance.id)
	}

	def delete(Long id) {
		def historyPrintInstance = HistoryPrint.get(id)
		if (!historyPrintInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), id])
			redirect(action: "list")
			return
		}

		try {
            historyPrintInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            historyPrintInstance.setLastUpdProcess("DELETE")
			historyPrintInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'historyPrint.label', default: 'HistoryPrint'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(HistoryPrint, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(HistoryPrint, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def getUser(){
        def res = [:]
        def role = Role.get(params.roles)
        def opts = []
        def sql=new Sql(dataSource)
        if(role!=null){
            sql.eachRow("""\
                select user_id from dom_user_roles where role_id = '${role.id}'
            """) {
                def user = User.get(it.USER_ID)
                opts<<user.fullname
            }
        }

        res."options" = opts
        render res as JSON
    }
	
	
}
