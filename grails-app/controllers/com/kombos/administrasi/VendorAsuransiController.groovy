package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class VendorAsuransiController {
	def conversi= new Konversi()
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	private static final String EMAIL_PATTERN = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/;
	
	public boolean validasiEmail(String email) {
		email ==~ EMAIL_PATTERN
		}
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = VendorAsuransi.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")	
			if(params."sCriteria_m193Nama"){
				ilike("m193Nama","%" + (params."sCriteria_m193Nama" as String) + "%")
			}
	
			if(params."sCriteria_m193Alamat"){
				ilike("m193Alamat","%" + (params."sCriteria_m193Alamat" as String) + "%")
			}
	
			if(params."sCriteria_m193Npwp"){
				ilike("m193Npwp","%" + (params."sCriteria_m193Npwp" as String) + "%")
			}
	
			if(params."sCriteria_m193NoTelp"){
				ilike("m193NoTelp","%" + (params."sCriteria_m193NoTelp" as String) + "%")
			}
	
			if(params."sCriteria_m193Email"){
				ilike("m193Email","%" + (params."sCriteria_m193Email" as String) + "%")
			}

			if(params."sCriteria_m193JmlToleransiAsuransiBP"){
				eq("m193JmlToleransiAsuransiBP",Double.parseDouble(params."sCriteria_m193JmlToleransiAsuransiBP"))
			}
	
			if(params."sCriteria_m193ID"){
				ilike("m193ID","%" + (params."sCriteria_m193ID" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m193Nama: it.m193Nama,
			
						m193Alamat: it.m193Alamat,
			
						m193Npwp: it.m193Npwp,
			
						m193NoTelp: it.m193NoTelp,
			
						m193Email: it.m193Email,
			
						m193JmlToleransiAsuransiBP: conversi.toRupiah(it.m193JmlToleransiAsuransiBP),
			
						m193ID: it.m193ID,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[vendorAsuransiInstance: new VendorAsuransi(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def vendorAsuransiInstance = new VendorAsuransi(params)
		
		String email = params.m193Email
		def checkEmail = validasiEmail(email)
		
		if(email != ''){
			if(!checkEmail){
			//    flash.message = message(code: 'default.companyDealer.email', args: [message(code: 'companyDealer.email.error', default: 'Masukan email dengan format xxx@xxx.xxx'),''])
				flash.message = 'Input Email dengan format : xxx@xxx.xxx'
				render(view: "create", model: [vendorAsuransiInstance: vendorAsuransiInstance])
				 return
			}
		}
        if(VendorAsuransi.findByM193NamaAndM193AlamatAndM193Email(params.m193Nama,params.m193Alamat,params.m193Email)!=null){
            flash.message = '* Data Vendor Asuransi Sudah Ada'
            render(view: "create", model: [vendorAsuransiInstance: vendorAsuransiInstance])
            return
        }
		
		
		vendorAsuransiInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		vendorAsuransiInstance?.lastUpdProcess = "INSERT"
		vendorAsuransiInstance?.setStaDel('0')
		if (!vendorAsuransiInstance.save(flush: true)) {
			render(view: "create", model: [vendorAsuransiInstance: vendorAsuransiInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), vendorAsuransiInstance.id])
		redirect(action: "show", id: vendorAsuransiInstance.id)
	}

	def show(Long id) {
		def vendorAsuransiInstance = VendorAsuransi.get(id)
		if (!vendorAsuransiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), id])
			redirect(action: "list")
			return
		}

		[vendorAsuransiInstance: vendorAsuransiInstance]
	}

	def edit(Long id) {
		def vendorAsuransiInstance = VendorAsuransi.get(id)
		if (!vendorAsuransiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), id])
			redirect(action: "list")
			return
		}

		[vendorAsuransiInstance: vendorAsuransiInstance]
	}

	def update(Long id, Long version) {
		def vendorAsuransiInstance = VendorAsuransi.get(id)
		if (!vendorAsuransiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (vendorAsuransiInstance.version > version) {
				
				vendorAsuransiInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'vendorAsuransi.label', default: 'VendorAsuransi')] as Object[],
				"Another user has updated this VendorAsuransi while you were editing")
				render(view: "edit", model: [vendorAsuransiInstance: vendorAsuransiInstance])
				return
			}
		}

        params?.lastUpdated = datatablesUtilService?.syncTime()
        vendorAsuransiInstance.properties = params
		vendorAsuransiInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		vendorAsuransiInstance?.lastUpdProcess = "UPDATE"
		
		String email = params.m193Email
		def checkEmail = validasiEmail(email)
		
		if(email != ''){
			if(!checkEmail){
			//    flash.message = message(code: 'default.companyDealer.email', args: [message(code: 'companyDealer.email.error', default: 'Masukan email dengan format xxx@xxx.xxx'),''])
				flash.message = 'Input Email dengan format : xxx@xxx.xxx'
				render(view: "create", model: [vendorCatInstance: vendorCatInstance])
				 return
			}
		}

        if(VendorAsuransi.findByM193NamaAndM193AlamatAndM193Email(params.m193Nama,params.m193Alamat,params.m193Email)!=null){
            if(VendorAsuransi.findByM193NamaAndM193AlamatAndM193Email(params.m193Nama,params.m193Alamat,params.m193Email).id!=id){
                flash.message = '* Data Vendor Asuransi Sudah Ada'
                render(view: "create", model: [vendorAsuransiInstance: vendorAsuransiInstance])
                return
            }
        }

		if (!vendorAsuransiInstance.save(flush: true)) {
			render(view: "edit", model: [vendorAsuransiInstance: vendorAsuransiInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), vendorAsuransiInstance.id])
		redirect(action: "show", id: vendorAsuransiInstance.id)
	}

	def delete(Long id) {
		def vendorAsuransiInstance = VendorAsuransi.get(id)
		if (!vendorAsuransiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), id])
			redirect(action: "list")
			return
		}

		try {
            vendorAsuransiInstance?.lastUpdated = datatablesUtilService?.syncTime()
            vendorAsuransiInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            vendorAsuransiInstance?.lastUpdProcess = "DELETE"
            vendorAsuransiInstance?.setStaDel('1')
            vendorAsuransiInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'vendorAsuransi.label', default: 'VendorAsuransi'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(VendorAsuransi, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(VendorAsuransi, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
