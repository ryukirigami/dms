package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat

class UploadCOGSController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
  def conversi =new Konversi()
    def index() {
    }

    def upload() {
        def operationInstance = null
        def requestBody = request.JSON

        String bpGr = params.bpGr
        CompanyDealer companyDealer = CompanyDealer.findById(new Long(params.companyDealer.id))
        String insertUpdate = params.insertUpdate

        TargetBP targetBP
        TargetGR targetGR
        TargetBP targetBPCek
        TargetGR targetGRCek

        requestBody.each{
            if(bpGr.equalsIgnoreCase("BP")){
                targetBP = new TargetBP()
                targetBP.m036TglBerlaku = new Date(new SimpleDateFormat("yyyy-mm-dd").parse(it.tglBerlaku).getTime())
                targetBP.companyDealer = companyDealer
                targetBP.m036COGSPart = it.parts
                targetBP.m036COGSOil = it.oil
                targetBP.m036COGSSublet = it.sublet
                targetBP.m036COGSLabor = it.labor
                targetBP.m036COGSOverHead= it.overhead
                targetBP.m036COGSDeprecation = it.depreciation
                targetBP.m036COGSTotalSGA = it.totalSGA

                targetBP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                targetBP.setStaDel('0')
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                targetBPCek = TargetBP.findByM036TglBerlakuAndCompanyDealer(targetBP.m036TglBerlaku,targetBP.companyDealer)
                if(!targetBPCek){
                    targetBP.lastUpdProcess = "INSERT"
                    if (!targetBP.save()) {
                        targetBP.errors.each {
                            //println it
                        }
                    }
                }else if(insertUpdate.equalsIgnoreCase("REPLACE")){
                    targetBPCek.companyDealer = companyDealer
                    targetBPCek.m036COGSPart = it.parts
                    targetBPCek.m036COGSOil = it.oil
                    targetBPCek.m036COGSSublet = it.sublet
                    targetBPCek.m036COGSLabor = it.labor
                    targetBPCek.m036COGSOverHead= it.overhead
                    targetBPCek.m036COGSDeprecation = it.depreciation
                    targetBPCek.m036COGSTotalSGA = it.totalSGA
                    targetBPCek.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    targetBPCek.lastUpdProcess = "UPDATE"
                    if (!targetBPCek.save()) {
                        targetBPCek.errors.each {
                            //println it
                        }
                    }
                }
            }else{
                targetGR = new TargetGR()
                targetGR.m037TglBerlaku = new Date(new SimpleDateFormat("yyyy-mm-dd").parse(it.tglBerlaku).getTime())
                targetGR.companyDealer = companyDealer
                targetGR.m037COGSPart = it.parts
                targetGR.m037COGSOil = it.oil
                targetGR.m037COGSSublet = it.sublet
                targetGR.m037COGSLabor = it.labor
                targetGR.m037COGSOverHead= it.overhead
                targetGR.m037COGSDeprecation = it.depreciation
                targetGR.m037COGSTotalSGA = it.totalSGA

                targetGR.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                targetGR.lastUpdProcess = "INSERT"
                targetGR.setStaDel('0')
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                targetGRCek = TargetGR.findByM037TglBerlakuAndCompanyDealer(targetGR.m037TglBerlaku,targetGR.companyDealer)
                if(!targetGRCek){
                    targetGR.lastUpdProcess = "INSERT"
                    if (!targetGR.save()) {
                        targetGR.errors.each {
                            //println it
                        }
                    }
                }else if(insertUpdate.equalsIgnoreCase("REPlACE")){
                    targetGRCek.m037COGSPart = it.parts
                    targetGRCek.m037COGSOil = it.oil
                    targetGRCek.m037COGSSublet = it.sublet
                    targetGRCek.m037COGSLabor = it.labor
                    targetGRCek.m037COGSOverHead= it.overhead
                    targetGRCek.m037COGSDeprecation = it.depreciation
                    targetGRCek.m037COGSTotalSGA = it.totalSGA
                    targetGRCek.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    targetGRCek.lastUpdProcess = "UPDATE"
                    if (!targetGRCek.save()) {
                        targetGRCek.errors.each {
                            //println it
                        }
                    }
                }
            }
        }

        flash.message = message(code: 'default.uploadCOGS.message', default: "Save Job Done")
//        render(view: "index", model: [operationInstance: operationInstance])
        render(view: "index")
    }

    def view() {
//        def operationInstance = new Operation(params)
        String bpGr = params.bpGr
        CompanyDealer companyDealer = CompanyDealer.findById(new Long(params.companyDealer.id))
        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1, //starts from 0
                columnMap:  [
                        //Col, Map-Key
                        'A':'tglBerlaku',
                        'B':'parts',
                        'C':'oil',
                        'D':'sublet',
                        'E':'labor',
                        'F':'overhead',
                        'G':'depreciation',
                        'H':'totalSGA'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
//                render(view: "index", model: [operationInstance: operationInstance])
                render(view: "index")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def targetList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = targetList as JSON

//            Serial serial = null
//            Section section = null
//            KategoriJob kategoriJob = null

            String status = "0", style = ""
            org.joda.time.LocalDate tglBerlaku

            targetList?.each {
                //operationInstance = new Operation();
                if(
                        (it.tglBerlaku && it.tglBerlaku!="") &&
                                (it.parts && it.parts!="") &&
                                (it.oil && it.oil!="") &&
                                (it.sublet && it.sublet!="") &&
                                (it.labor && it.labor!="") &&
                                (it.depreciation && it.depreciation!="") &&
                                (it.overhead && it.overhead!="") &&
                                (it.totalSGA && it.totalSGA!="")
                ){
                    tglBerlaku = it.tglBerlaku

                    if(bpGr.equalsIgnoreCase("BP")){
                        if(TargetBP.findByM036TglBerlakuAndCompanyDealer(new java.sql.Date(tglBerlaku.toDate().getTime()),companyDealer)){
                            jmlhDataError++;
                            status = "1";
                        }
                    }else{
                        if(TargetGR.findByM037TglBerlakuAndCompanyDealer(new java.sql.Date(tglBerlaku.toDate().getTime()),companyDealer)){
                            jmlhDataError++;
                            status = "1";
                        }
                    }
                } else {
                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+new java.sql.Date(tglBerlaku.toDate().getTime()).format("dd/MM/yyyy")+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.parts)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.oil)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.sublet)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.labor)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.overhead)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.depreciation)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.totalSGA)+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadCOGSError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadCOGS.message', default: "Read File Done")
        }

        render(view: "index", model: [htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError, companyDealer: params.companyDealer.id, bpGr: params.bpGr])
    }

}
