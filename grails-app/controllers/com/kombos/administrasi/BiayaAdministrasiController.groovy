package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class BiayaAdministrasiController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def biayaAdministrasiService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render biayaAdministrasiService.datatablesList(params) as JSON
    }

    def create() {
        def result = biayaAdministrasiService.create(params)

        if (!result.error)
            return [biayaAdministrasiInstance: result.biayaAdministrasiInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = biayaAdministrasiService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Biaya Administrasi", ""])
            redirect(action: 'show', id: result.biayaAdministrasiInstance.id)
            return
        }

        render(view: 'create', model: [biayaAdministrasiInstance: result.biayaAdministrasiInstance])
    }

    def show(Long id) {
        def result = biayaAdministrasiService.show(params)

        if (!result.error)
            return [biayaAdministrasiInstance: result.biayaAdministrasiInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = biayaAdministrasiService.show(params)

        if (!result.error)
            return [biayaAdministrasiInstance: result.biayaAdministrasiInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = biayaAdministrasiService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Biaya Administrasi", ""])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [biayaAdministrasiInstance: result.biayaAdministrasiInstance.attach()])
    }

    def delete() {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = biayaAdministrasiService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Biaya Administrasi", ""])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(BiayaAdministrasi, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
