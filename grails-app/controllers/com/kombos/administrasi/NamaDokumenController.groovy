package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class NamaDokumenController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def generalParameterService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = NamaDokumen.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m007NamaDokumen"){
				ilike("m007NamaDokumen","%" + (params."sCriteria_m007NamaDokumen" as String) + "%")
			}
	
			eq("staDel","0")
				
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m007NamaDokumen: it.m007NamaDokumen,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[namaDokumenInstance: new NamaDokumen(params)]
	}

	def save() {
        def namaDokumenInstance = new NamaDokumen(params)
        if(params.m007NamaDokumen){
            NamaDokumen cek = NamaDokumen.findByM007NamaDokumenIlikeAndStaDel(params.m007NamaDokumen, '0')
            if(cek){
                flash.message = message(code: 'namaDokumen.m007NamaDokumen.unique')
                render(view: "create", model: [namaDokumenInstance: namaDokumenInstance])
                return
            }
        }
		//def namaDokumenInstance = new NamaDokumen(params)
		if (!namaDokumenInstance.save(flush: true)) {
			render(view: "create", model: [namaDokumenInstance: namaDokumenInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), namaDokumenInstance.id])
		redirect(action: "show", id: namaDokumenInstance.id)
	}

	def show(Long id) {
		def namaDokumenInstance = NamaDokumen.get(id)
		if (!namaDokumenInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), id])
			redirect(action: "list")
			return
		}

		[namaDokumenInstance: namaDokumenInstance]
	}

	def edit(Long id) {
		def namaDokumenInstance = NamaDokumen.get(id)
		if (!namaDokumenInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), id])
			redirect(action: "list")
			return
		}

		[namaDokumenInstance: namaDokumenInstance]
	}

	def update(Long id, Long version) {
		def namaDokumenInstance = NamaDokumen.get(id)
		if (!namaDokumenInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (namaDokumenInstance.version > version) {
				
				namaDokumenInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'namaDokumen.label', default: 'NamaDokumen')] as Object[],
				"Another user has updated this NamaDokumen while you were editing")
				render(view: "edit", model: [namaDokumenInstance: namaDokumenInstance])
				return
			}
		}

        if(params.m007NamaDokumen){
            NamaDokumen cek = NamaDokumen.findByM007NamaDokumenIlikeAndIdNotEqualAndStaDel(params.m007NamaDokumen, id, '0')
            if(cek){
                flash.message = message(code: 'namaDokumen.m007NamaDokumen.unique')
                render(view: "edit", model: [namaDokumenInstance: namaDokumenInstance])
                return
            }
        }

		namaDokumenInstance.properties = params

		if (!namaDokumenInstance.save(flush: true)) {
			render(view: "edit", model: [namaDokumenInstance: namaDokumenInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), namaDokumenInstance.id])
		redirect(action: "show", id: namaDokumenInstance.id)
	}

	def delete(Long id) {
		def namaDokumenInstance = NamaDokumen.get(id)
		if (!namaDokumenInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), id])
			redirect(action: "list")
			return
		}

		try {
            namaDokumenInstance.staDel = "1"
			//namaDokumenInstance.delete(flush: true)
            if (!namaDokumenInstance.save(flush: true)) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), id])
                redirect(action: "show", id: id)
                return
            }

            flash.message = message(code: 'default.deleted.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'namaDokumen.label', default: 'NamaDokumen'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
            log.info(params.ids)
            def jsonArray = JSON.parse(params.ids)

            jsonArray.each {
                def namaDokumenInstance = NamaDokumen.get(it)

                namaDokumenInstance.staDel = "1"
                //namaDokumenInstance.delete(flush: true)
                namaDokumenInstance.save(flush: true)

            }

			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(NamaDokumen, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
