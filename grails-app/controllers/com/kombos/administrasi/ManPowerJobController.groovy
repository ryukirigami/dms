package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ManPowerJobController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", copy:"GET", autoCompleteCode:"GET"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = ManPowerJob.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_namaManPower") {
                eq("namaManPower",NamaManPower.findByT015NamaLengkapIlike("%"+params."sCriteria_namaManPower"+"%"))
            }

            if (params."sCriteria_operation") {
                eq("operation",Operation.findByM053NamaOperationIlike("%"+params."sCriteria_operation"+"%"))
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [


                    id: it.id,

                    namaManPower: it.namaManPower.t015NamaLengkap+" ("+it.namaManPower.t015NamaBoard+") ",//+it.namaManPower.manPowerDetail.m015LevelManPower,

                    operation: it.operation.m053JobsId+" - "+it.operation.m053NamaOperation,

                    sertifikat: it.sertifikat,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [manPowerJobInstance: new ManPowerJob(params)]
    }

    def copy(){
        String result = ""
        def namaSumber = new ManPowerJob()
        def namaTujuan = new ManPowerJob()
        def namaManPower = new NamaManPower()
        namaManPower = NamaManPower.findByT015NamaLengkap(params.namaManPowerTujuan)
        namaSumber=ManPowerJob.findByNamaManPower(NamaManPower.findByT015NamaLengkap(params.namaManPowerSumber))
        namaTujuan=ManPowerJob.findByNamaManPower(namaManPower)
        if(namaManPower!=null && namaTujuan==null){
            def manPowerJobAdd= new ManPowerJob()
            manPowerJobAdd?.namaManPower = namaManPower
            manPowerJobAdd?.staDel = "0"
            manPowerJobAdd?.operation = namaSumber?.operation
            manPowerJobAdd?.sertifikat = namaSumber?.sertifikat
            manPowerJobAdd?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            manPowerJobAdd?.lastUpdProcess = "INSERT"
            manPowerJobAdd?.save(flush : true)
        }
        if(namaSumber?.id==namaTujuan?.id){
            result = "-1";
        } else {
            //proses copy
            if(ManPowerJob.findByNamaManPowerAndOperationAndStaDel(namaManPower,namaSumber.operation,"0")){
                result = "2";
            }else{
                namaTujuan?.setOperation(namaSumber.operation)
                result = "1";
            }
        }
        render result
    }

    def save() {
        def manPowerJobInstance = new ManPowerJob()
        def nama = NamaManPower.findByT015NamaLengkap(params.namaManPower)
        def ops= Operation.findById(params.operation.id)
        if(nama==null){
            flash.message= "Tidak ada data nama man power dengan nama "+params.namaManPower
            render(view: "create", model: [manPowerJobInstance: manPowerJobInstance])
            return
        }
        manPowerJobInstance?.dateCreated = datatablesUtilService?.syncTime()
        manPowerJobInstance?.lastUpdated = datatablesUtilService?.syncTime()
        manPowerJobInstance?.setNamaManPower(nama)
        manPowerJobInstance?.setOperation(ops)
        manPowerJobInstance?.setStaDel("0")
        manPowerJobInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        manPowerJobInstance.setLastUpdProcess("INSERT")
        if(ManPowerJob.findByNamaManPowerAndOperationAndStaDel(nama,ops,"0")!=null){
            flash.message= "Data sudah ada"
            render(view: "create", model: [manPowerJobInstance: manPowerJobInstance])
            return
        }
        if (!manPowerJobInstance.save(flush: true)) {
            render(view: "create", model: [manPowerJobInstance: manPowerJobInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'manPowerJob.label', default: 'Klasifikasi Job Man Power'), manPowerJobInstance.id])
        redirect(action: "show", id: manPowerJobInstance.id)
    }

    def show(Long id) {
        def manPowerJobInstance = ManPowerJob.get(id)
        if (!manPowerJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJob.label', default: 'ManPowerJob'), id])
            redirect(action: "list")
            return
        }

        [manPowerJobInstance: manPowerJobInstance]
    }

    def edit(Long id) {
        def manPowerJobInstance = ManPowerJob.get(id)
        if (!manPowerJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJob.label', default: 'ManPowerJob'), id])
            redirect(action: "list")
            return
        }

        [manPowerJobInstance: manPowerJobInstance]
    }

    def update(Long id, Long version) {
        def manPowerJobInstance = ManPowerJob.get(id)
        if (!manPowerJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJob.label', default: 'ManPowerJob'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (manPowerJobInstance.version > version) {

                manPowerJobInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'manPowerJob.label', default: 'ManPowerJob')] as Object[],
                        "Another user has updated this ManPowerJob while you were editing")
                render(view: "edit", model: [manPowerJobInstance: manPowerJobInstance])
                return
            }
        }

        def nama = NamaManPower.findByT015NamaLengkap(params.namaManPower)
        def ops= Operation.findById(params.operation.id)
        if(nama==null){
            flash.message= "Tidak ada data nama man power dengan nama "+params.namaManPower
            render(view: "edit", model: [manPowerJobInstance: manPowerJobInstance])
            return
        }
        if(ManPowerJob.findByNamaManPowerAndOperationAndStaDel(nama,ops,"0")!=null){
            flash.message= "Data sudah ada"
            render(view: "edit", model: [manPowerJobInstance: manPowerJobInstance])
            return
        }


        manPowerJobInstance?.setNamaManPower(nama)
        manPowerJobInstance?.setOperation(ops)
        manPowerJobInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        manPowerJobInstance?.setLastUpdProcess("UPDATE")
        manPowerJobInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if (!manPowerJobInstance.save(flush: true)) {
            render(view: "edit", model: [manPowerJobInstance: manPowerJobInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'manPowerJob.label', default: 'Klasifikasi Job Man Power'), manPowerJobInstance.id])
        redirect(action: "show", id: manPowerJobInstance.id)
    }

    def delete(Long id) {
        def manPowerJobInstance = ManPowerJob.get(id)
        if (!manPowerJobInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerJob.label', default: 'ManPowerJob'), id])
            redirect(action: "list")
            return
        }

        try {
            manPowerJobInstance?.setStaDel("1")
            manPowerJobInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            manPowerJobInstance?.setLastUpdProcess("DELETE")
            manPowerJobInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerJobInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'manPowerJob.label', default: 'ManPowerJob'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'manPowerJob.label', default: 'ManPowerJob'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(ManPowerJob, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(ManPowerJob, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def getManPowerSumber() {
        def res = [:]

        //   def result = GroupManPower.createCriteria().list(){eq("staDel","0")}
        def result = ManPowerJob.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.namaManPower.t015NamaLengkap
        }
        res."options" = opts
        render res as JSON
    }

    def getManPower() {
        def res = [:]

        //   def result = GroupManPower.createCriteria().list(){eq("staDel","0")}
        def cari = ManPowerJob.findAllWhere(staDel : '0')
        def opts = []
        if(cari.size()>0){
            def result = NamaManPower.findAllWhere(staDel : '0')
            result.each {
                opts << it.t015NamaLengkap
            }
        }
        res."options" = opts
        render res as JSON
    }

    def kodeList() {
        def res = [:]

        def result = NamaManPower.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.t015NamaLengkap
        }

        res."options" = opts
        render res as JSON
    }


}
