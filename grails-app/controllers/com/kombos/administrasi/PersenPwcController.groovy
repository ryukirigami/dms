package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PersenPwcController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = PersenPwc.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params."sCriteria_companyDealer"){
                companyDealer{
                    ilike("m011NamaWorkshop","%" + (params."sCriteria_companyDealer") + "%")
                }
            }

			if(params."sCriteria_m008TanggalBerlaku"){
				ge("m008TanggalBerlaku",params."sCriteria_m008TanggalBerlaku")
				lt("m008TanggalBerlaku",params."sCriteria_m008TanggalBerlaku" + 1)
			}
            String str = params."sCriteria_m008PersenPwc"
			if(str){
				eq("m008PersenPwc",Double.parseDouble(str?.replace(",","")))
			}
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companyDealer: it.companyDealer?.m011NamaWorkshop,
			
						m008TanggalBerlaku: it.m008TanggalBerlaku?it.m008TanggalBerlaku.format(dateFormat):"",
			
						m008PersenPwc: it.m008PersenPwc + '%',
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[persenPwcInstance: new PersenPwc(params)]
	}

	def save() {
		def persenPwcInstance = new PersenPwc(params)
//        persenPwcInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
//        persenPwcInstance?.lastUpdProcess = "INSERT"
        def cek = PersenPwc.createCriteria()
        def result = cek.list() {
            and{
                eq("companyDealer",persenPwcInstance.companyDealer)
                eq("m008TanggalBerlaku",persenPwcInstance.m008TanggalBerlaku)
                //eq("m008PersenPwc",persenPwcInstance.m008PersenPwc)
            }
        }

        if(!result){
            persenPwcInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            persenPwcInstance?.lastUpdProcess = "INSERT"

            if (!persenPwcInstance.save(flush: true)) {
                render(view: "create", model: [persenPwcInstance: persenPwcInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.persenpwc.error.message', default: 'Data sudah pernah dimasukkan.')
            render(view: "create", model: [persenPwcInstance: persenPwcInstance])
            return
        }
		flash.message = message(code: 'default.created.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), persenPwcInstance.id])
		redirect(action: "show", id: persenPwcInstance.id)
	}

	def show(Long id) {
		def persenPwcInstance = PersenPwc.get(id)
		if (!persenPwcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), id])
			redirect(action: "list")
			return
		}

		[persenPwcInstance: persenPwcInstance]
	}

	def edit(Long id) {
		def persenPwcInstance = PersenPwc.get(id)
		if (!persenPwcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), id])
			redirect(action: "list")
			return
		}

		[persenPwcInstance: persenPwcInstance]
	}

	def update(Long id, Long version) {
		def persenPwcInstance = PersenPwc.get(id)
		if (!persenPwcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (persenPwcInstance.version > version) {
				
				persenPwcInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'persenPwc.label', default: 'Persen PWC')] as Object[],
				"Another user has updated this Persen PWC while you were editing")
				render(view: "edit", model: [persenPwcInstance: persenPwcInstance])
				return
			}
		}

        def cek = PersenPwc.createCriteria()
        def result = cek.list() {
            and{
                ne("id",id)
                eq("companyDealer",CompanyDealer.findById(Long.parseLong(params."companyDealer.id")))
                eq("m008TanggalBerlaku",new Date().parse("dd-MM-yyyy", params.m008TanggalBerlaku_dp))
                //eq("m008PersenPwc",Double.parseDouble(params.m008PersenPwc))
            }
        }

        if(!result){
            persenPwcInstance.properties = params
            persenPwcInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            persenPwcInstance?.lastUpdProcess = "UPDATE"
            if (!persenPwcInstance.save(flush: true)) {
                render(view: "edit", model: [persenPwcInstance: persenPwcInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.update.persenPwc.error.message', default: 'Duplikat data Persen PWC')
            render(view: "edit", model: [persenPwcInstance: persenPwcInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), persenPwcInstance.id])
		redirect(action: "show", id: persenPwcInstance.id)
	}

	def delete(Long id) {
		def persenPwcInstance = PersenPwc.get(id)
		if (!persenPwcInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), id])
			redirect(action: "list")
			return
		}

		try {
            persenPwcInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            persenPwcInstance?.lastUpdProcess = "DELETE"
			persenPwcInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'persenPwc.label', default: 'Persen PWC'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(PersenPwc, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(PersenPwc, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
