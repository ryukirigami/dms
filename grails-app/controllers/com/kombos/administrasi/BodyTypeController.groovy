package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class BodyTypeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams=params

        def c = BodyType.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params."sCriteria_baseModel"){
                baseModel{
                    ilike("m102NamaBaseModel","%"+ (params."sCriteria_baseModel") +"%")
                }
            }

            if(params."sCriteria_modelName"){
//				eq("modelName",ModelName.findByM104NamaModelName(params."sCriteria_modelName"))
                modelName{
                    ilike("m104NamaModelName","%"+ (params."sCriteria_modelName") +"%")
                }
            }

			if(params."sCriteria_m105KodeBodyType"){
				ilike("m105KodeBodyType","%" + (params."sCriteria_m105KodeBodyType" as String) + "%")
			}
	
			if(params."sCriteria_m105NamaBodyType"){
				ilike("m105NamaBodyType","%" + (params."sCriteria_m105NamaBodyType" as String) + "%")
			}
	
			ilike("staDel",'0')


			switch(sortProperty){
                case "baseModel":
                    baseModel{
                        order("m102KodeBaseModel",sortDir)
                    }
                    break;
                case "modelName":
                    modelName{
                        order("m104KodeModelName",sortDir)
                    }
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m105ID: it.m105ID,

                        baseModel: it.baseModel?.m102NamaBaseModel+" - "+it.baseModel?.m102KodeBaseModel,
			
						modelName: it.modelName.m104NamaModelName+" - "+it.modelName.m104KodeModelName,
			
						m105KodeBodyType: it.m105KodeBodyType,
			
						m105NamaBodyType: it.m105NamaBodyType,
			
						staDel: it.staDel,
						
						createdBy: it.createdBy,
						
											updatedBy: it.updatedBy,
						
											lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[bodyTypeInstance: new BodyType(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def bodyTypeInstance = new BodyType(params)
        def cek = BodyType.createCriteria().list() {
            and{
                eq("baseModel",bodyTypeInstance.baseModel)
                eq("modelName",bodyTypeInstance.modelName)
                eq("m105KodeBodyType",bodyTypeInstance.m105KodeBodyType?.trim(), [ignoreCase: true])
                eq("m105NamaBodyType",bodyTypeInstance.m105NamaBodyType?.trim(), [ignoreCase: true])
                eq("staDel",'0')
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [bodyTypeInstance: bodyTypeInstance])
            return
        }

		bodyTypeInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		bodyTypeInstance?.lastUpdProcess = "INSERT"
		bodyTypeInstance?.setStaDel('0')

		if (!bodyTypeInstance.save(flush: true)) {
			render(view: "create", model: [bodyTypeInstance: bodyTypeInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'bodyType.label', default: 'BodyType'), bodyTypeInstance?.getM105NamaBodyType()])
		redirect(action: "show", id: bodyTypeInstance.id)
	}

	def show(Long id) {
		def bodyTypeInstance = BodyType.get(id)
		if (!bodyTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bodyType.label', default: 'BodyType'), id])
			redirect(action: "list")
			return
		}

		[bodyTypeInstance: bodyTypeInstance]
	}

	def edit(Long id) {
		def bodyTypeInstance = BodyType.get(id)
		if (!bodyTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bodyType.label', default: 'BodyType'), id])
			redirect(action: "list")
			return
		}

		[bodyTypeInstance: bodyTypeInstance]
	}

	def update(Long id, Long version) {
		def bodyTypeInstance = BodyType.get(id)
		if (!bodyTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bodyType.label', default: 'BodyType'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (bodyTypeInstance.version > version) {
				
				bodyTypeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'bodyType.label', default: 'BodyType')] as Object[],
				"Another user has updated this BodyType while you were editing")
				render(view: "edit", model: [bodyTypeInstance: bodyTypeInstance])
				return
			}
		}
        def cek = BodyType.createCriteria()
        def result = cek.list() {
            eq("baseModel",BaseModel.findById(params.baseModel?.id))
            eq("modelName",ModelName.findById(params.modelName?.id))
            eq("m105KodeBodyType",params.m105KodeBodyType?.trim(), [ignoreCase: true])
            eq("m105NamaBodyType",params.m105NamaBodyType?.trim(), [ignoreCase: true])
            eq("staDel",'0')
        }
        if(result && BodyType.findByM105KodeBodyTypeAndStaDel(params.m105KodeBodyType,'0')?.id != id ){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Kode Sudah Ada')
            render(view: "edit", model: [bodyTypeInstance: bodyTypeInstance])
            return
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        bodyTypeInstance.properties = params
		bodyTypeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		bodyTypeInstance?.lastUpdProcess = "UPDATE"
		if (!bodyTypeInstance.save(flush: true)) {
			render(view: "edit", model: [bodyTypeInstance: bodyTypeInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'bodyType.label', default: 'Body Type'), bodyTypeInstance?.getM105NamaBodyType()])
		redirect(action: "show", id: bodyTypeInstance.id)
	}

	def delete(Long id) {
		def bodyTypeInstance = BodyType.get(id)
		if (!bodyTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bodyType.label', default: 'BodyType'), id])
			redirect(action: "list")
			return
		}

		try {
			//bodyTypeInstance.delete(flush: true)
			bodyTypeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			bodyTypeInstance?.lastUpdProcess = "DELETE"
			bodyTypeInstance?.setStaDel('1')
            bodyTypeInstance?.lastUpdated = datatablesUtilService?.syncTime()
            bodyTypeInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'bodyType.label', default: 'BodyType'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'bodyType.label', default: 'BodyType'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(BodyType, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(BodyType, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def getBaseModels (){
        def idKategoriKendaraan = params.idKategoriKendaraan
        def kategoriKendaraan = KategoriKendaraan.get(idKategoriKendaraan)

        def baseModelList = BaseModel.createCriteria().list {
            eq("kategoriKendaraan", kategoriKendaraan)
            order("m102KodeBaseModel", "asc")
        }

        def res = []
        baseModelList.each {
            res << [
                    id : it.id,
                    kodeBaseModel : it.m102KodeBaseModel+" - "+it.m102NamaBaseModel
            ]
        }
        render res as JSON
    }

    def getModelNames (){
        def idBaseModel = params.idBaseModel
        def baseModel = BaseModel.get(idBaseModel)

        def modelNameList = ModelName.findAllByBaseModel(baseModel)

        def res = []
        modelNameList.each {
            res << [
                    id : it.id,
                    kodeModelName : it.m104KodeModelName+" - "+it.m104NamaModelName
            ]
        }
        render res as JSON
    }

    def getBodyTypes (){
        def idModelName = params.idModelName
        def modelName = ModelName.get(idModelName)

        def bodyTypeList = BodyType.createCriteria().list {
            eq("modelName", modelName)
            order("m105KodeBodyType", "asc")
        }

        def res = []
        bodyTypeList.each {
            res << [
                    id : it.id,
                    kodeBodyType : it.m105KodeBodyType+" - "+it.m105NamaBodyType
            ]
        }
        render res as JSON
    }

    def getGears (){
        def idBodyType = params.idBodyType
        def bodyType = BodyType.get(idBodyType)

        def gearList = Gear.createCriteria().list {
            eq("bodyType", bodyType)
            order("m106KodeGear", "asc")
        }

        def res = []
        gearList.each {
            res << [
                    id : it.id,
                    kodeGear : it.m106KodeGear+" - "+it.m106NamaGear
            ]
        }
        render res as JSON
    }

    def getGrades (){
        def idGear = params.idGear
        def gear = Gear.get(idGear)

        def gradeList = Grade.createCriteria().list {
            eq("gear", gear)
            order("m107KodeGrade", "asc")
        }

        def res = []
        gradeList.each {
            res << [
                    id : it.id,
                    kodeGrade : it.m107KodeGrade+" - "+it.m107NamaGrade
            ]
        }
        render res as JSON
    }

    def getEngines (){
        def idGrade = params.idGrade
        def grade = Grade.get(idGrade)

        def engineList = Engine.createCriteria().list {
            eq("grade", grade)
            order("m108KodeEngine", "asc")
        }

        def res = []
        engineList.each {
            res << [
                    id : it.id,
                    kodeEngine : it.m108KodeEngine+" - "+it.m108NamaEngine
            ]
        }
        render res as JSON
    }
	
	
}
