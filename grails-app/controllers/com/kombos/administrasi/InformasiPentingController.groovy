package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class InformasiPentingController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = InformasiPenting.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer", session?.userCompanyDealer)
            eq("m032StaTampil","0")
            if (params."sCriteria_m032Tanggal") {
                ge("m032Tanggal", params."sCriteria_m032Tanggal")
                lt("m032Tanggal", params."sCriteria_m032Tanggal" + 1)
            }

            if (params."sCriteria_m032UserUpload") {
                ilike("m032UserUpload", "%" + (params."sCriteria_m032UserUpload" as String) + "%")
            }

            if (params."sCriteria_m032Judul") {
                ilike("m032Judul", "%" + (params."sCriteria_m032Judul" as String) + "%")
            }

            if (params."sCriteria_m032Konten") {
                ilike("m032Konten", "%" + (params."sCriteria_m032Konten" as String) + "%")
            }

            if (params."sCriteria_m032Image1") {
                eq("m032Image1", params."sCriteria_m032Image1")
            }

            if (params."sCriteria_m032StaTampil") {
                ilike("m032StaTampil", "%" + (params."sCriteria_m032StaTampil" as String) + "%")
            }

            if (params."sCriteria_m032Image2") {
                eq("m032Image2", params."sCriteria_m032Image2")
            }

            if (params."sCriteria_m032Image3") {
                eq("m032Image3", params."sCriteria_m032Image3")
            }

            if (params."sCriteria_m032Image4") {
                eq("m032Image4", params."sCriteria_m032Image4")
            }

            if (params."sCriteria_m032Image5") {
                eq("m032Image5", params."sCriteria_m032Image5")
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_m032ID") {
                eq("m032ID", params."sCriteria_m032ID")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m032Tanggal: it?.m032Tanggal ? it?.m032Tanggal.format(dateFormat) : "",

                    m032UserUpload: it?.m032UserUpload,

                    m032Judul: it?.m032Judul ? it?.m032Judul:"-",

                    m032Konten: it?.m032Konten ? it?.m032Konten:"-",

                    m032Image1: it?.m032Image1 ,

                    m032StaTampil: it?.m032StaTampil,

                    m032Image2: it?.m032Image2,

                    m032Image3: it?.m032Image3,

                    m032Image4: it?.m032Image4,

                    m032Image5: it?.m032Image5,

                    companyDealer: it?.companyDealer,

                    m032ID: it?.m032ID,

                    createdBy: it?.createdBy ? it?.createdBy:"system",

                    updatedBy: it?.updatedBy ? it?.updatedBy:"system",

                    lastUpdProcess: it?.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [informasiPentingInstance: new InformasiPenting(params)]
    }

    def save() {

        def informasiPentingInstance = new InformasiPenting()
        informasiPentingInstance?.m032UserUpload = params.m032UserUpload
        informasiPentingInstance?.m032Judul = params.m032Judul
        informasiPentingInstance?.m032Konten = params.m032Konten
//        informasiPentingInstance?.m032Image1 = params.m032Image1
        informasiPentingInstance?.m032Tanggal = new Date().parse("dd/MM/yyyy",params.m032Tanggal);
        informasiPentingInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        informasiPentingInstance?.companyDealer = session.userCompanyDealer
        informasiPentingInstance?.lastUpdProcess = "INSERT"
        informasiPentingInstance?.m032StaTampil = "0";

        def m032Image1 = request.getFile('m032Image1')
        def notAllowContFoto = ['application/octet-stream']
        if(m032Image1 !=null && !notAllowContFoto.contains(m032Image1.getContentType()) ){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(m032Image1.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'create', model:[informasiPentingInstance: informasiPentingInstance])
                return;
            }

            informasiPentingInstance.m032Image1 = m032Image1.getBytes()
            informasiPentingInstance.imageMime = m032Image1.getContentType()
        }
        
        if (!informasiPentingInstance.save(flush: true)) {
            render(view: "create", model: [informasiPentingInstance: informasiPentingInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'informasiPenting.label', default: 'Informasi Penting'), informasiPentingInstance.id])
        redirect(action: "show", id: informasiPentingInstance.id)
    }

    def show(Long id) {
        def informasiPentingInstance = InformasiPenting.get(id)
        if (!informasiPentingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'informasiPenting.label', default: 'InformasiPenting'), id])
            redirect(action: "list")
            return
        }

        [informasiPentingInstance: informasiPentingInstance]
    }

    def edit(Long id) {
        def informasiPentingInstance = InformasiPenting.get(id)
        if (!informasiPentingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'informasiPenting.label', default: 'InformasiPenting'), id])
            redirect(action: "list")
            return
        }

        [informasiPentingInstance: informasiPentingInstance]
    }

    def update(Long id, Long version) {
        def informasiPentingInstance = InformasiPenting.get(params.id)
        if (!informasiPentingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'informasiPenting.label', default: 'InformasiPenting'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (informasiPentingInstance.version > version) {

                informasiPentingInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'informasiPenting.label', default: 'InformasiPenting')] as Object[],
                        "Another user has updated this InformasiPenting while you were editing")
                render(view: "edit", model: [informasiPentingInstance: informasiPentingInstance])
                return
            }
        }

        informasiPentingInstance?.m032UserUpload = params.m032UserUpload
        informasiPentingInstance?.m032Judul = params.m032Judul
        informasiPentingInstance?.m032Konten = params.m032Konten
        informasiPentingInstance?.m032Tanggal = new Date().parse("dd/MM/yyyy",params.m032Tanggal);
        informasiPentingInstance?.companyDealer = session.userCompanyDealer
        informasiPentingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        informasiPentingInstance?.lastUpdProcess = "UPDATE"

        def m032Image1 = request.getFile('m032Image1')
        def notAllowContFoto = ['application/octet-stream']
        if(m032Image1 !=null && !notAllowContFoto.contains(m032Image1.getContentType()) ){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(m032Image1.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[informasiPentingInstance: informasiPentingInstance])
                return;
            }

            informasiPentingInstance.m032Image1 = m032Image1.getBytes()
            informasiPentingInstance.imageMime = m032Image1.getContentType()
        }

        if (!informasiPentingInstance.save(flush: true)) {
            render(view: "edit", model: [informasiPentingInstance: informasiPentingInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'informasiPenting.label', default: 'Informasi Penting'), informasiPentingInstance.id])
        redirect(action: "show", id: informasiPentingInstance.id)
    }

    def delete(Long id) {
        def informasiPentingInstance = InformasiPenting.get(id)
        if (!informasiPentingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'informasiPenting.label', default: 'InformasiPenting'), id])
            redirect(action: "list")
            return
        }

        try {
            informasiPentingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            informasiPentingInstance?.lastUpdProcess = "DELETE"
            informasiPentingInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'informasiPenting.label', default: 'InformasiPenting'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'informasiPenting.label', default: 'InformasiPenting'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(InformasiPenting, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(InformasiPenting, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def showM032Image1 = {
        def InformasiPenting = InformasiPenting.get(params.id)

        response.setContentLength(InformasiPenting.m032Image1.size())
        OutputStream out = response.getOutputStream();
        out.write(InformasiPenting.m032Image1);
        out.close();
    }

}
