package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory

class UploadJobController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
    }

    def upload() {
        def operationInstance = null
        def requestBody = request.JSON

        //println "asd : "+requestBody.dump()
        Serial serial = null
        Section section = null
        KategoriJob kategoriJob = null

        requestBody.each{
            //println "ASD : " + it?.dump()
            serial = Serial.findByM052ID(it.serial)
            section = Section.findByM051ID(it.section)
            kategoriJob = KategoriJob.findByM055KategoriJob(it.kategoriJob)
            if(section && kategoriJob && (it.m053Id && it.m053Id!="") && (it.m053Ket && it.m053Ket!="") &&
                    (it.m053NamaOperation && it.m053NamaOperation!="") && (it.m053StaLift && it.m053StaLift!="") &&
                    (it.m053StaPaket && it.m053StaPaket!="") && (it.m053StaPaint && it.m053StaPaint!="")){

                operationInstance = Operation.findBySectionAndKategoriJobAndSerialAndM053IdAndM053NamaOperation(section, kategoriJob, serial, it.m053Id, it.m053NamaOperation)

                if(!operationInstance){
                    operationInstance = new Operation();
                    operationInstance?.dateCreated = datatablesUtilService?.syncTime()
                    operationInstance?.lastUpdated = datatablesUtilService?.syncTime()
                }
                operationInstance.kategoriJob = kategoriJob
                operationInstance.section = section
                operationInstance.serial = serial
                operationInstance.m053Id = it.m053Id
                operationInstance.m053JobsId = it.m053Id
                operationInstance.m053Ket = it.m053Ket
                operationInstance.m053NamaOperation = it.m053NamaOperation
                if(it.m053StaLift?.toString()?.toLowerCase()?.equals("ya")){
                    operationInstance.m053StaLift = "1"
                } else if(it.m053StaLift?.toString()?.toLowerCase()?.equals("tidak")){
                    operationInstance.m053StaLift = "0"
                }
                if(it.m053StaPaket?.toString()?.toLowerCase()?.equals("ya")){
                    operationInstance.m053StaPaket = "1"
                } else if(it.m053StaPaket?.toString()?.toLowerCase()?.equals("tidak")){
                    operationInstance.m053StaPaket = "0"
                }
                if(it.m053StaPaint?.toString()?.toLowerCase()?.equals("ya")){
                    operationInstance.m053StaPaint = "1"
                } else if(it.m053StaPaint?.toString()?.toLowerCase()?.equals("tidak")){
                    operationInstance.m053StaPaint = "0"
                }
                operationInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                operationInstance.lastUpdProcess = "INSERT"
                operationInstance.setStaDel('0')
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                def cek = Operation.find(operationInstance)
                if(!cek){
                    operationInstance.save()
                }
            }
        }

        flash.message = message(code: 'default.uploadJob.message', default: "Save Job Done")
        render(view: "index", model: [operationInstance: operationInstance])
    }

    def view() {
        def operationInstance = new Operation(params)

        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'section',
                        'B':'serial',
                        'C':'m053Id',
                        'D':'m053NamaOperation',
                        'E':'m053StaLift',
                        'F':'m053StaPaket',
                        'G':'kategoriJob',
                        'H':'m053StaPaint',
                        'I':'m053Ket',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        int jmlhDataDuplikat = 0;

        if(!uploadExcel?.empty){
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                //println "asd"
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [operationInstance: operationInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)


            jsonData = jobList as JSON

            String namaOpt = ""
            String kodeId = ""
            Serial serial = null
            Section section = null
            KategoriJob kategoriJob = null

            String status = "0", style = ""
            def styleList = [:]

            jobList?.each {
                //operationInstance = new Operation();
                //println it.dump()

                if((it.section && it.section!="") && (it.serial && it.serial!="") && (it.kategoriJob && it.kategoriJob!="") && (it.m053Id && it.m053Id!="") && (it.m053Ket && it.m053Ket!="") &&
                        (it.m053NamaOperation && it.m053NamaOperation!="") && (it.m053StaLift && it.m053StaLift!="") &&
                        (it.m053StaPaket && it.m053StaPaket!="") && (it.m053StaPaint && it.m053StaPaint!="")){

                    serial = Serial.findByM052IDAndStaDel(it.serial,"0")
                    section = Section.findByM051IDAndStaDel(it.section,"0")
                    kategoriJob = KategoriJob.findByM055KategoriJob(it.kategoriJob)
                    namaOpt = it.m053NamaOperation
                    kodeId = it.m053Id

                    if(!section || !serial || !kategoriJob ||namaOpt.length() > 250 || it.m053Ket.length() > 250 ){

                        if(!section)
                            styleList.section = "style='color:red;'"

                        if(!serial)
                            styleList.serial = "style='color:red;'"

                        if(!kategoriJob)
                            styleList.kategoriJob = "style='color:red;'"

                        if(namaOpt.length() > 250)
                            styleList.namaOpt = "style='color:yellow;'"

                        if(it.m053Ket.length() > 250)
                            styleList.ket = "style='color:yellow;'"

                        jmlhDataError++;
                        status = "1";

                    }

                    
                    def cek = Operation.findBySerialAndSectionAndKategoriJobAndM053NamaOperationIlikeAndM053IdIlike(serial,section,kategoriJob, namaOpt, kodeId)
                    def cek2 = Operation.findByM053IdOrM053NamaOperation(kodeId, namaOpt)
                    if(cek){
                        status = "2"
                        jmlhDataDuplikat++
                    }else if(cek2){
                        status = "2"
                        jmlhDataDuplikat++
                    }


                } else {

                    jmlhDataError++;
                    status = "1";

                }

               
                if(status.equals("2")){
                    style = "style='color:blue;'"
                } else {
                    style = ""
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td "+styleList.section+">\n" +
                        "                                "+it.section+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.serial+">\n" +
                        "                                "+it.serial+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m053Id+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.namaOpt+">\n" +
                        "                                "+it.m053NamaOperation+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m053StaLift+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m053StaPaket+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.kategoriJob+">\n" +
                        "                                "+it.kategoriJob+"\n" +
                        "                            </td>\n" +
                        "                            <td "+style+">\n" +
                        "                                "+it.m053StaPaint+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.ket+">\n" +
                        "                                "+it.m053Ket+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"

                status = "0"
            }
        }
        def messageInfo;

        if(jmlhDataError>0){
            messageInfo = message(code: 'default.uploadJobError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid (merah) / panjang melebihi batas max (kuning) (Nama Opt dan keterangan 250 karakter)")
        } else if(jmlhDataDuplikat>0){
            messageInfo = message(code: 'default.uploadJobDuplicate.message', default: "Read File Done : Terdapat "+jmlhDataDuplikat+" data yang duplikat (Biru). Tekan Simpan untuk Mereplace data")
        }else{
            messageInfo = message(code: 'default.uploadJob.default.message', default : "Read File Done")
        }

        flash.message = messageInfo

        render(view: "index", model: [operationInstance: operationInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }

}
