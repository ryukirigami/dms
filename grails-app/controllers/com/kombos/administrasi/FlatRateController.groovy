package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class FlatRateController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def conversi =new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = FlatRate.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_operation") {
                operation{
                    ilike("m053NamaOperation","%"+params."sCriteria_operation"+"%")
                }
            }

            if (params."sCriteria_baseModel") {
                baseModel{
                    or{
                        ilike("m102KodeBaseModel","%"+params."sCriteria_baseModel"+"%")
                        ilike("m102NamaBaseModel","%"+params."sCriteria_baseModel"+"%")

                    }
                }
            }

            if (params."sCriteria_t113TMT") {
                ge("t113TMT", params."sCriteria_t113TMT")
                lt("t113TMT", params."sCriteria_t113TMT" + 1)
            }

            if (params."sCriteria_t113FlatRate") {
                eq("t113FlatRate", Double.parseDouble(params."sCriteria_t113FlatRate"))
            }

            if (params."sCriteria_t113ProductionRate") {
                eq("t113ProductionRate", Double.parseDouble(params."sCriteria_t113ProductionRate"))
            }

            if (params."sCriteria_t113StaPriceMenu") {
                ilike("t113StaPriceMenu", "%" + (params."sCriteria_t113StaPriceMenu" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }
            ilike("staDel","%0%")

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    operation: it.operation?.m053NamaOperation,

                    baseModel: it.baseModel?.m102NamaBaseModel+" ("+it?.baseModel?.m102KodeBaseModel+")",

                    t113TMT: it.t113TMT ? it.t113TMT.format(dateFormat) : "",

                    t113FlatRate: it.t113FlatRate,

                    t113ProductionRate: it.t113ProductionRate,

                    t113StaPriceMenu: it.t113StaPriceMenu,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [flatRateInstance: new FlatRate(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def flatRateInstance = new FlatRate(params)

        flatRateInstance.staDel = '0'

        flatRateInstance?.operation = Operation.findById(params.jobsId as Long)
        flatRateInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        flatRateInstance?.lastUpdProcess = "INSERT"

        if (!flatRateInstance.save(flush: true)) {
            render(view: "create", model: [flatRateInstance: flatRateInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), flatRateInstance.id])
        redirect(action: "show", id: flatRateInstance.id)
    }

    def show(Long id) {
        def flatRateInstance = FlatRate.get(id)
        if (!flatRateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), id])
            redirect(action: "list")
            return
        }

        [flatRateInstance: flatRateInstance]
    }

    def edit(Long id) {
        def flatRateInstance = FlatRate.get(id)
        if (!flatRateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), id])
            redirect(action: "list")
            return
        }

        [flatRateInstance: flatRateInstance]
    }

    def update(Long id, Long version) {
        def flatRateInstance = FlatRate.get(id)
        if (!flatRateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (flatRateInstance.version > version) {

                flatRateInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'flatRate.label', default: 'FlatRate')] as Object[],
                        "Another user has updated this FlatRate while you were editing")
                render(view: "edit", model: [flatRateInstance: flatRateInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        flatRateInstance.properties = params
        flatRateInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        flatRateInstance?.lastUpdProcess = "UPDATE"

        if (!flatRateInstance.save(flush: true)) {
            render(view: "edit", model: [flatRateInstance: flatRateInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), flatRateInstance.id])
        redirect(action: "show", id: flatRateInstance.id)
    }

    def delete(Long id) {
        def flatRateInstance = FlatRate.get(id)
        if (!flatRateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), id])
            redirect(action: "list")
            return
        }

        flatRateInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        flatRateInstance?.lastUpdProcess = "DELETE"
        flatRateInstance?.lastUpdated = datatablesUtilService?.syncTime()
        flatRateInstance.staDel = '1'

        try {
            flatRateInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'flatRate.label', default: 'FlatRate'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDel(FlatRate, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(FlatRate, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def listOps() {
        def res = [:]
        def opts = []
        def z = Operation.createCriteria()
        def results=z.list {
            or{
                ilike("m053NamaOperation",params.query + "%")
                ilike("m053Id",params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.m053Id.trim() +" || "+ it.m053NamaOperation
        }
        res."options" = opts
        render res as JSON
    }

    def detailOps(){
        def result = [:]
        def data= params.noJobs as String
        if(data.contains("||")){
            def dataNojobs = data.substring(0,data.indexOf("|"));
            def dataNamajobs = data.substring(data.lastIndexOf("|")+1,data.length());

            def jobs = Operation.createCriteria().list {
                eq("staDel","0")
                eq("m053Id",dataNojobs?.trim(),[ignoreCase : true])
                ilike("m053NamaOperation",dataNamajobs?.trim()+"%")
            }
            if(jobs.size()>0){
                def jobsData = jobs.last()
                result."hasil" = "ada"
                result."id" = jobsData?.id
                result."namaJobs" = jobsData?.m053NamaOperation
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }
}
