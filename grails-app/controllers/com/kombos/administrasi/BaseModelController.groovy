package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class BaseModelController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]


        session.exportParams = params

        def c = BaseModel.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_kategoriKendaraan") {
                kategoriKendaraan{
                    ilike("m101NamaKategori","%"+ (params."sCriteria_kategoriKendaraan") +"%")
                }
            }

            if (params."sCriteria_bahanBakar") {
                bahanBakar{
                    ilike("m110NamaBahanBakar","%"+ (params."sCriteria_bahanBakar") + "%")
                }
            }

            if (params."sCriteria_m102KodeBaseModel") {
                ilike("m102KodeBaseModel", "%" + (params."sCriteria_m102KodeBaseModel" as String) + "%")
            }

            if (params."sCriteria_m102NamaBaseModel") {
                ilike("m102NamaBaseModel", "%" + (params."sCriteria_m102NamaBaseModel" as String) + "%")
            }

            if (params."sCriteria_m102Foto") {
                eq("m102Foto", params."sCriteria_m102Foto")
            }

            if (params."sCriteria_m102FotoWAC") {
                eq("m102FotoWAC", params."sCriteria_m102FotoWAC")
            }


            ilike("staDel", "0")





            switch (sortProperty) {
                case "kategoriKendaraan":
                    kategoriKendaraan{
                        order("m101NamaKategori",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)

                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kategoriKendaraan: it.kategoriKendaraan.m101NamaKategori,

                    bahanBakar: it.bahanBakar.m110NamaBahanBakar,

                    m102KodeBaseModel: it.m102KodeBaseModel,

                    m102NamaBaseModel: it.m102NamaBaseModel,

                    staDel: it.staDel,

                    m102Foto: it.m102Foto,

                    m102FotoWAC: it.m102FotoWAC,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [baseModelInstance: new BaseModel(params)]
    }

    def save() {
        def baseModelInstance = new BaseModel(params)

        baseModelInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        baseModelInstance?.lastUpdProcess = "INSERT"
        baseModelInstance.staDel = '0'
        baseModelInstance?.dateCreated = datatablesUtilService?.syncTime()
        baseModelInstance?.lastUpdated = datatablesUtilService?.syncTime()


        def katKendaraan = KategoriKendaraan.get(params.kategoriKendaraan.id)
        def bahanBakar = BahanBakar.get(params.bahanBakar.id)
        def cekDuplicate = BaseModel.findByKategoriKendaraanAndBahanBakarAndM102KodeBaseModelIlikeAndM102NamaBaseModelIlikeAndStaDel(katKendaraan,bahanBakar,params.m102KodeBaseModel,params.m102NamaBaseModel,"0")

        if(cekDuplicate)
        {
            flash.message = message(code: 'baseModel.duplicate.label', default: 'Data sudah tersedia')
            render(view: "create", model: [baseModelInstance: baseModelInstance])
            return
        }

        def foto = request.getFile('m102Foto')
        def notAllowContFoto = ['application/octet-stream']
        if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){

            log.info('Foto ada')
            log.info('isi foto '+request.getFile('m102Foto'))
            log.info('Jenis konten '+foto.getContentType())
            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(foto.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[baseModelInstance: baseModelInstance])
                return;
            }

            baseModelInstance.m102Foto = foto.getBytes()
            baseModelInstance.m102FotoImageMime = foto.getContentType()
        }

        def fotoWac = request.getFile('m102FotoWAC')
        def notAllowContFotoWac = ['application/octet-stream']
        if(fotoWac !=null && !notAllowContFotoWac.contains(fotoWac.getContentType()) ){

            log.info('Foto ada')
            log.info('isi foto '+request.getFile('m0102FotoWAC'))
            log.info('Jenis konten '+fotoWac.getContentType())
            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(fotoWac.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'create', model:[baseModelInstance: baseModelInstance])
                return;
            }

            baseModelInstance.m102FotoWAC = fotoWac.getBytes()
            baseModelInstance.m102FotoWACImageMime = fotoWac.getContentType()
        }

        if (!baseModelInstance.save(flush: true)) {
            render(view: "create", model: [baseModelInstance: baseModelInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), baseModelInstance.id])
        redirect(action: "show", id: baseModelInstance.id)
    }

    def show(Long id) {
        def baseModelInstance = BaseModel.get(id)
        if (!baseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), id])
            redirect(action: "list")
            return
        }

        [baseModelInstance: baseModelInstance,logostamp: new Date().format("yyyyMMddhhmmss")]
    }

    def edit(Long id) {
        def baseModelInstance = BaseModel.get(id)
        if (!baseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), id])
            redirect(action: "list")
            return
        }

        [baseModelInstance: baseModelInstance]
    }

    def update(Long id, Long version) {


        def baseModelInstance = BaseModel.findById(Double.parseDouble(params.id))

        def cek = BaseModel.createCriteria()
        def result = cek.list() {
            and{
                ne("id",id)
                eq("m102KodeBaseModel",params.m102KodeBaseModel.trim(), [ignoreCase: true])
                eq("m102NamaBaseModel",params.m102NamaBaseModel.trim(), [ignoreCase: true])
                eq("kategoriKendaraan",KategoriKendaraan.findById(Long.parseLong(params.kategoriKendaraan.id)))
                eq("bahanBakar",BahanBakar.findById(Long.parseLong(params.bahanBakar.id)))
                eq("staDel","0")
            }
        }

        if(result){
            flash.message = "Data sudah digunakan"
            render(view: "edit", model: [baseModelInstance: baseModelInstance])
            return;
        }


        if (!baseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), id])
            redirect(action: "list")
            return
        }

        baseModelInstance.bahanBakar = BahanBakar.findById(Double.parseDouble(params.bahanBakar.id))
        baseModelInstance.kategoriKendaraan = KategoriKendaraan.findById(Double.parseDouble(params.kategoriKendaraan.id))
        baseModelInstance.m102KodeBaseModel = params.m102KodeBaseModel
        baseModelInstance.m102NamaBaseModel = params.m102NamaBaseModel
        baseModelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        baseModelInstance?.lastUpdProcess = "UPDATE"
        baseModelInstance?.lastUpdated = datatablesUtilService?.syncTime()

        def foto = request.getFile('m102Foto')
        def notAllowContFoto = ['application/octet-stream']
        if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){

          def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(foto.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[baseModelInstance: baseModelInstance])
                return;
            }

            baseModelInstance.m102Foto = foto.getBytes()
            baseModelInstance.m102FotoImageMime = foto.getContentType()
        }

        def fotoWac = request.getFile('m102FotoWAC')
        def notAllowContFotoWac = ['application/octet-stream']
        if(fotoWac !=null && !notAllowContFotoWac.contains(fotoWac.getContentType()) ){

           // log.info('Foto ada')
           // log.info('isi foto '+request.getFile('m0102FotoWAC'))
           // log.info('Jenis konten '+fotoWac.getContentType())
            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(fotoWac.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[baseModelInstance: baseModelInstance])
                return;
            }

            baseModelInstance.m102FotoWAC = fotoWac.getBytes()
            baseModelInstance.m102FotoWACImageMime = fotoWac.getContentType()
        }

        if (version != null) {
            if (baseModelInstance.version > version) {

                baseModelInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'baseModel.label', default: 'BaseModel')] as Object[],
                        "Another user has updated this BaseModel while you were editing")
                render(view: "edit", model: [baseModelInstance: baseModelInstance])
                return
            }
        }



        if (!baseModelInstance.save(flush: true)) {
            render(view: "edit", model: [baseModelInstance: baseModelInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), baseModelInstance.id])
        redirect(action: "show", id: baseModelInstance.id)
    }

    def delete(Long id) {
        def baseModelInstance = BaseModel.get(id)
        if (!baseModelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), id])
            redirect(action: "list")
            return
        }

        try {
            baseModelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            baseModelInstance?.lastUpdProcess = "DELETE"
            baseModelInstance.staDel = '1'
            baseModelInstance?.lastUpdated = datatablesUtilService?.syncTime()
            baseModelInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'baseModel.label', default: 'BaseModel'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDel(BaseModel, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(BaseModel, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def showFoto = {
        def baseModel = BaseModel.get(params.id)

        /* if (!avatarUser || !avatarUser.avatar || !avatarUser.avatarType) {
           response.sendError(404)
           return;
         }*/


        response.setContentType(baseModel.m102FotoImageMime)
        response.setContentLength(baseModel.m102Foto.size())
        OutputStream out = response.getOutputStream();
        out.write(baseModel.m102Foto);
        out.close();
    }

    def showFotoWAC = {
        def baseModel = BaseModel.get(params.id)


        response.setContentType(baseModel.m102FotoWACImageMime)
        response.setContentLength(baseModel.m102FotoWAC.size())
        OutputStream out = response.getOutputStream();
        out.write(baseModel.m102FotoWAC);
        out.close();
    }


}
