package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class TarifCBUNTAMController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def conversi =new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = TarifCBUNTAM.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_companyDealer") {
               companyDealer{
                   ilike("m011NamaWorkshop","%"+params."sCriteria_companyDealer"+"%" )
               }
            }

            if (params."sCriteria_kategori") {
                kategori{
                    ilike("m101NamaKategori","%"+params."sCriteria_kategori"+"%" )
                }
            }

            if (params."sCriteria_t113bTMT") {
                ge("t113bTMT", params."sCriteria_t113bTMT")
                lt("t113bTMT", params."sCriteria_t113bTMT" + 1)
            }

            if (params."sCriteria_t113bTarifPerjamCBUNTAMSB") {
                eq("t113bTarifPerjamCBUNTAMSB",Double.parseDouble( params."sCriteria_t113bTarifPerjamCBUNTAMSB"))
            }

            if (params."sCriteria_t113bTarifPerjamCBUNTAMGR") {
                eq("t113bTarifPerjamCBUNTAMGR", Double.parseDouble(params."sCriteria_t113bTarifPerjamCBUNTAMGR"))
            }

            if (params."sCriteria_t113bTarifPerjamCBUNTAMBP") {
                eq("t113bTarifPerjamCBUNTAMBP",Double.parseDouble(params."sCriteria_t113bTarifPerjamCBUNTAMBP"))
            }

                ilike("staDel", "0")


                switch (sortProperty) {
                case "companyDealer" :
                    companyDealer{
                        order("m011NamaWorkshop", sortDir)
                    }

                    break;
                case "kategori" :
                    kategori{
                        order("m101NamaKategori", sortDir)
                    }
                break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }

            companyDealer{
                eq("staDel", "0")
            }

        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer.m011NamaWorkshop,

                    kategori: it.kategori.m101NamaKategori,

                    t113bTMT: it.t113bTMT ? it.t113bTMT.format(dateFormat) : "",

                    t113bTarifPerjamCBUNTAMSB: conversi.toRupiah(it.t113bTarifPerjamCBUNTAMSB),

                    t113bTarifPerjamCBUNTAMGR: conversi.toRupiah(it.t113bTarifPerjamCBUNTAMGR),

                    t113bTarifPerjamCBUNTAMBP: conversi.toRupiah( it.t113bTarifPerjamCBUNTAMBP),

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [tarifCBUNTAMInstance: new TarifCBUNTAM(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def tarifCBUNTAMInstance = new TarifCBUNTAM(params)
        tarifCBUNTAMInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        tarifCBUNTAMInstance?.lastUpdProcess = "INSERT"
        tarifCBUNTAMInstance?.setStaDel ('0')
        if (!tarifCBUNTAMInstance.save(flush: true)) {
            render(view: "create", model: [tarifCBUNTAMInstance: tarifCBUNTAMInstance])
            return
        }

        tarifCBUNTAMInstance.staDel = '0'
        flash.message = message(code: 'default.created.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), tarifCBUNTAMInstance.id])
        redirect(action: "show", id: tarifCBUNTAMInstance.id)
    }

    def show(Long id) {
        def tarifCBUNTAMInstance = TarifCBUNTAM.get(id)
        if (!tarifCBUNTAMInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), id])
            redirect(action: "list")
            return
        }

        [tarifCBUNTAMInstance: tarifCBUNTAMInstance]
    }

    def edit(Long id) {
        def tarifCBUNTAMInstance = TarifCBUNTAM.get(id)
        if (!tarifCBUNTAMInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), id])
            redirect(action: "list")
            return
        }

        [tarifCBUNTAMInstance: tarifCBUNTAMInstance]
    }

    def update(Long id, Long version) {
        def tarifCBUNTAMInstance = TarifCBUNTAM.get(id)
        if (!tarifCBUNTAMInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (tarifCBUNTAMInstance.version > version) {

                tarifCBUNTAMInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM')] as Object[],
                        "Another user has updated this TarifCBUNTAM while you were editing")
                render(view: "edit", model: [tarifCBUNTAMInstance: tarifCBUNTAMInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        tarifCBUNTAMInstance.properties = params
        tarifCBUNTAMInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        tarifCBUNTAMInstance?.lastUpdProcess = "UPDATE"


        if (!tarifCBUNTAMInstance.save(flush: true)) {
            render(view: "edit", model: [tarifCBUNTAMInstance: tarifCBUNTAMInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), tarifCBUNTAMInstance.id])
        redirect(action: "show", id: tarifCBUNTAMInstance.id)
    }

    def delete(Long id) {
        def tarifCBUNTAMInstance = TarifCBUNTAM.get(id)
        if (!tarifCBUNTAMInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), id])
            redirect(action: "list")
            return
        }

        try {
            tarifCBUNTAMInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            tarifCBUNTAMInstance?.lastUpdProcess = "DELETE"
            tarifCBUNTAMInstance?.setStaDel('1')
            tarifCBUNTAMInstance?.lastUpdated = datatablesUtilService?.syncTime()
            tarifCBUNTAMInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tarifCBUNTAM.label', default: 'TarifCBUNTAM'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDel(TarifCBUNTAM, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(TarifCBUNTAM, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
