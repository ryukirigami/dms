package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GradeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Grade.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            if (params."sCriteria_m107ID") {
                eq("m107ID", params."sCriteria_m107ID")
            }

            if(params."sCriteria_baseModel"){
                baseModel{
                    ilike("m102NamaBaseModel","%"+ (params."sCriteria_baseModel") +"%")
                }
            }

            if(params."sCriteria_modelName"){
                modelName{
                    ilike("m104NamaModelName","%"+ (params."sCriteria_modelName") +"%")
                }
            }

            if(params."sCriteria_bodyType"){
                bodyType{
                    ilike("m105NamaBodyType","%"+ (params."sCriteria_bodyType") +"%")
                }
            }

            if (params."sCriteria_gear") {
                gear{
                    ilike("m106NamaGear","%"+ (params."sCriteria_gear") +"%")
                }
            }

            if (params."sCriteria_m107KodeGrade") {
                ilike("m107KodeGrade", "%" + (params."sCriteria_m107KodeGrade" as String) + "%")
            }

            if (params."sCriteria_m107NamaGrade") {
                ilike("m107NamaGrade", "%" + (params."sCriteria_m107NamaGrade" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }


            switch (sortProperty) {
                case "baseModel":
                    baseModel{
                        order("m102NamaBaseModel",sortDir)
                    }
                    break;
                case "modelName":
                    modelName{
                        order("m104NamaModelName",sortDir)
                    }
                    break;
                case "bodyType":
                    bodyType{
                        order("m105NamaBodyType",sortDir)
                    }
                    break;
                case "gear":
                    gear{
                        order("m106NamaGear",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m107ID: it.m107ID,

                    baseModel: it.baseModel?.m102NamaBaseModel+" - "+it.baseModel?.m102KodeBaseModel,

                    modelName: it.modelName.m104NamaModelName+" - "+it.modelName.m104KodeModelName,

                    bodyType: it.bodyType?.m105NamaBodyType+" - "+it.bodyType?.m105KodeBodyType,

                    gear: it.gear.m106NamaGear+" - "+it.gear.m106KodeGear,

                    m107KodeGrade: it.m107KodeGrade,

                    m107NamaGrade: it.m107NamaGrade,

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [gradeInstance: new Grade(params)]
    }

    def save() {
        def gradeInstance = new Grade(params)
        gradeInstance?.setStaDel("0")
        gradeInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        gradeInstance?.setLastUpdProcess("INSERT")
        def cek = Grade.createCriteria().list() {
            and{
                eq("baseModel",gradeInstance.baseModel)
                eq("modelName",gradeInstance.modelName)
                eq("bodyType",gradeInstance.bodyType)
                eq("gear",gradeInstance.gear)
                eq("m107KodeGrade",gradeInstance?.m107KodeGrade.trim(), [ignoreCase: true])
                eq("m107NamaGrade",gradeInstance?.m107NamaGrade.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [gradeInstance: gradeInstance])
            return
        }

        if (!gradeInstance.save(flush: true)) {
            render(view: "create", model: [gradeInstance: gradeInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'grade.label', default: 'Grade'), gradeInstance.id])
        redirect(action: "show", id: gradeInstance.id)
    }

    def show(Long id) {
        def gradeInstance = Grade.get(id)
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "list")
            return
        }

        [gradeInstance: gradeInstance]
    }

    def edit(Long id) {
        def gradeInstance = Grade.get(id)
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "list")
            return
        }

        [gradeInstance: gradeInstance]
    }

    def update(Long id, Long version) {
        def gradeInstance = Grade.get(id)
        gradeInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        gradeInstance?.setLastUpdProcess("UPDATE")
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (gradeInstance.version > version) {

                gradeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'grade.label', default: 'Grade')] as Object[],
                        "Another user has updated this Grade while you were editing")
                render(view: "edit", model: [gradeInstance: gradeInstance])
                return
            }
        }

        def cek = Grade.createCriteria().list {
            eq("staDel","0")
            eq("m107KodeGrade",params?.m107KodeGrade?.trim(), [ignoreCase: true])
        }
        if (cek) {
            for(find in cek){
                if(find.id!=id){
                    flash.message = "Kode Grade sudah di gunakan"
                    render(view: "edit", model: [gradeInstance: gradeInstance])
                    return
                    break
                }
            }
        }
        def cek2 = Grade.createCriteria().list {
            eq("staDel","0")
            eq("m107NamaGrade",params?.m107NamaGrade?.trim(), [ignoreCase: true])
        }
        if (cek2) {
            for(find in cek2){
                if(find.id!=id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [gradeInstance: gradeInstance])
                    return
                    break
                }
            }
        }

        gradeInstance.properties = params

        if (!gradeInstance.save(flush: true)) {
            render(view: "edit", model: [gradeInstance: gradeInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'grade.label', default: 'Grade'), gradeInstance.id])
        redirect(action: "show", id: gradeInstance.id)
    }

    def delete(Long id) {
        def gradeInstance = Grade.get(id)
        if (!gradeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "list")
            return
        }

        try {
            gradeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            gradeInstance?.lastUpdProcess = "DELETE"
            gradeInstance?.staDel = "1"
            gradeInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'grade.label', default: 'Grade'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Grade, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Grade, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
