package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StirController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = Stir.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if(params."sCriteria_m103ID"){
				eq("m103ID",params."sCriteria_m103ID")
			}
	
			if(params."sCriteria_m103KodeStir"){
				ilike("m103KodeStir","%" + (params."sCriteria_m103KodeStir" as String) + "%")
			}
	
			if(params."sCriteria_m103NamaStir"){
				ilike("m103NamaStir","%" + (params."sCriteria_m103NamaStir" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m103ID: it.m103ID,
			
						m103KodeStir: it.m103KodeStir.toUpperCase(),
			
						m103NamaStir: it.m103NamaStir.toUpperCase(),
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[stirInstance: new Stir(params)]
	}

	def save() {
		def stirInstance = new Stir(params)
                stirInstance?.setStaDel("0")
        stirInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        stirInstance?.setLastUpdProcess("INSERT")
        def stir = Stir.createCriteria().list {
            eq("staDel","0")
            eq("m103KodeStir",params.m103KodeStir,[ignoreCase: true])
        }
        if(stir){
            for (find in stir){
                flash.message =" Kode Stir Telah digunakan"
                render(view: "create", model: [stirInstance: stirInstance])
                return
                break
            }
        }
		if (!stirInstance.save(flush: true)) {
			render(view: "create", model: [stirInstance: stirInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'stir.label', default: 'Stir'), stirInstance.id])
		redirect(action: "show", id: stirInstance.id)
	}

	def show(Long id) {
		def stirInstance = Stir.get(id)
		if (!stirInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stir.label', default: 'Stir'), id])
			redirect(action: "list")
			return
		}

		[stirInstance: stirInstance]
	}

	def edit(Long id) {
		def stirInstance = Stir.get(id)
		if (!stirInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stir.label', default: 'Stir'), id])
			redirect(action: "list")
			return
		}

		[stirInstance: stirInstance]
	}

	def update(Long id, Long version) {
		def stirInstance = Stir.get(id)
		if (!stirInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stir.label', default: 'Stir'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (stirInstance.version > version) {
				
				stirInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'stir.label', default: 'Stir')] as Object[],
				"Another user has updated this Stir while you were editing")
				render(view: "edit", model: [stirInstance: stirInstance])
				return
			}
		}

        def stir = Stir.createCriteria().list {
            eq("staDel","0")
            eq("m103KodeStir",params.m103KodeStir,[ignoreCase: true])
        }
        if(stir){
            for(find in stir) {
                if(find?.id!=id){
                    flash.message =" Kode Stir Telah digunakan"
                    render(view: "edit", model: [stirInstance: stirInstance])
                    return
                    break
                }
            }
        }

        stirInstance.properties = params
        stirInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        stirInstance?.setLastUpdProcess("UPDATE")

		if (!stirInstance.save(flush: true)) {
			render(view: "edit", model: [stirInstance: stirInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'stir.label', default: 'Stir'), stirInstance.id])
		redirect(action: "show", id: stirInstance.id)
	}

	def delete(Long id) {
		def stirInstance = Stir.get(id)
		if (!stirInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'stir.label', default: 'Stir'), id])
			redirect(action: "list")
			return
		}

		try {
            stirInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            stirInstance?.setLastUpdProcess("DELETE")
                        stirInstance?.setStaDel("1")
			stirInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'stir.label', default: 'Stir'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'stir.label', default: 'Stir'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Stir, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Stir, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
