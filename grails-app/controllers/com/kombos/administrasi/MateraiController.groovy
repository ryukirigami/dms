package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MateraiController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = Materai.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_m705TglBerlaku"){
				ge("m705TglBerlaku",params."sCriteria_m705TglBerlaku")
				lt("m705TglBerlaku",params."sCriteria_m705TglBerlaku" + 1)
			}

			if(params."sCriteria_m705Nilai1"){
				eq("m705Nilai1",params."sCriteria_m705Nilai1")
			}

			if(params."sCriteria_m705Nilai2"){
				eq("m705Nilai2",params."sCriteria_m705Nilai2")
			}

			if(params."sCriteria_m705NilaiMaterai"){
				eq("m705NilaiMaterai",params."sCriteria_m705NilaiMaterai")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

            eq("m705StaDel","0")
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m705TglBerlaku: it.m705TglBerlaku?it.m705TglBerlaku.format(dateFormat):"",
			
						m705Nilai1: it.m705Nilai1,
			
						m705Nilai2: it.m705Nilai2,
			
						m705NilaiMaterai: it.m705NilaiMaterai,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[materaiInstance: new Materai(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def materaiInstance = new Materai(params)
        Materai cek = Materai.findByM705TglBerlakuAndM705Nilai1AndM705Nilai2AndM705StaDel(params.m705TglBerlaku, params.m705Nilai1, params.m705Nilai2, '0')
        if(cek){
            flash.message = message(code: 'materai.m705TglBerlaku.unique')
            render(view: "create", model: [materaiInstance: materaiInstance])
            return
        }

		if (!materaiInstance.save(flush: true)) {
			render(view: "create", model: [materaiInstance: materaiInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'materai.label', default: 'Materai'), materaiInstance.id])
		redirect(action: "show", id: materaiInstance.id)
	}

	def show(Long id) {
		def materaiInstance = Materai.get(id)
		if (!materaiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'materai.label', default: 'Materai'), id])
			redirect(action: "list")
			return
		}

		[materaiInstance: materaiInstance]
	}

	def edit(Long id) {
		def materaiInstance = Materai.get(id)
		if (!materaiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'materai.label', default: 'Materai'), id])
			redirect(action: "list")
			return
		}

		[materaiInstance: materaiInstance]
	}

	def update(Long id, Long version) {
		def materaiInstance = Materai.get(id)
		if (!materaiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'materai.label', default: 'Materai'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (materaiInstance.version > version) {
				
				materaiInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'materai.label', default: 'Materai')] as Object[],
				"Another user has updated this Materai while you were editing")
				render(view: "edit", model: [materaiInstance: materaiInstance])
				return
			}
		}

        params?.lastUpdated = datatablesUtilService?.syncTime()
        materaiInstance.properties = params

        Materai cek = Materai.findByM705TglBerlakuAndM705Nilai1AndM705Nilai2AndM705StaDelAndIdNotEqual(params.m705TglBerlaku, params.m705Nilai1, params.m705Nilai2, '0', id)
        if(cek){
            flash.message = message(code: 'materai.m705TglBerlaku.unique')
            render(view: "edit", model: [materaiInstance: materaiInstance])
            return
        }

		if (!materaiInstance.save(flush: true)) {
			render(view: "edit", model: [materaiInstance: materaiInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'materai.label', default: 'Materai'), materaiInstance.id])
		redirect(action: "show", id: materaiInstance.id)
	}

	def delete(Long id) {
		def materaiInstance = Materai.get(id)
		if (!materaiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'materai.label', default: 'Materai'), id])
			redirect(action: "list")
			return
		}

		try {
			//materaiInstance.delete(flush: true)
			//flash.message = message(code: 'default.deleted.message', args: [message(code: 'materai.label', default: 'Materai'), id])
			 materaiInstance.m705StaDel = "1"
            materaiInstance?.lastUpdated = datatablesUtilService?.syncTime()
            //materaiInstance.delete(flush: true)
            if (!materaiInstance.save(flush: true)) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'materaiInstance.label', default: 'Materai'), id])
                redirect(action: "show", id: id)
                return
            }
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'materai.label', default: 'Materai'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
//		def res = [:]
//		try {
//			datatablesUtilService.massDelete(Materai, params)
//			res.message = "Mass Delete Success"
//		} catch (e) {
//			log.error(e.message, e)
//			res.message = e.message?:e.cause?.message
//		}
//		render "ok"
        def res = [:]
        try {
            log.info(params.ids)
            def jsonArray = JSON.parse(params.ids)
            jsonArray.each {
                def materaiInstance = Materai.get(it)

                materaiInstance.m705StaDel = "1"
                materaiInstance?.lastUpdated = datatablesUtilService?.syncTime()
                //namaDokumenInstance.delete(flush: true)
                materaiInstance.save(flush: true)

            }

            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Materai, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
