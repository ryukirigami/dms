package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class KategoriKendaraanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = KategoriKendaraan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			
			if(params."sCriteria_m101KodeKategori"){
				ilike("m101KodeKategori","%" + (params."sCriteria_m101KodeKategori" as String) + "%")
			}
	
			if(params."sCriteria_m101NamaKategori"){
				ilike("m101NamaKategori","%" + (params."sCriteria_m101NamaKategori" as String) + "%")
			}
	
			ilike("staDel",'0')
			
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m101ID: it.m101ID,
			
						m101KodeKategori: it.m101KodeKategori,
			
						m101NamaKategori: it.m101NamaKategori,
			
						staDel: it.staDel,
						
						createdBy: it.createdBy,
						
						updatedBy: it.updatedBy,
						
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[kategoriKendaraanInstance: new KategoriKendaraan(params), aksi : 'create']
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def kategoriKendaraanInstance = new KategoriKendaraan(params)
        def cek = KategoriKendaraan.createCriteria()
        def result = cek.list() {
            or{
                eq("m101KodeKategori",kategoriKendaraanInstance.m101KodeKategori?.trim(), [ignoreCase: true])
                eq("m101NamaKategori",kategoriKendaraanInstance.m101NamaKategori?.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }
        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "create", model: [kategoriKendaraanInstance: kategoriKendaraanInstance])
            return
        }

        kategoriKendaraanInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		kategoriKendaraanInstance?.lastUpdProcess = "INSERT"
		kategoriKendaraanInstance?.setStaDel('0')

		if (!kategoriKendaraanInstance.save(flush: true)) {
			render(view: "create", model: [kategoriKendaraanInstance: kategoriKendaraanInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), kategoriKendaraanInstance?.getM101NamaKategori()])
		redirect(action: "show", id: kategoriKendaraanInstance.id)
	}

	def show(Long id) {
		def kategoriKendaraanInstance = KategoriKendaraan.get(id)
		if (!kategoriKendaraanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), id])
			redirect(action: "list")
			return
		}

		[kategoriKendaraanInstance: kategoriKendaraanInstance]
	}

	def edit(Long id) {
		def kategoriKendaraanInstance = KategoriKendaraan.get(id)
		if (!kategoriKendaraanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), id])
			redirect(action: "list")
			return
		}

		[kategoriKendaraanInstance: kategoriKendaraanInstance,aksi : 'edit']
	}

	def update(Long id, Long version) {
		def kategoriKendaraanInstance = KategoriKendaraan.get(id)
		if (!kategoriKendaraanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), id])
			redirect(action: "list")
			return
		}


		if (version != null) {
			if (kategoriKendaraanInstance.version > version) {

				kategoriKendaraanInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan')] as Object[],
				"Another user has updated this KategoriKendaraan while you were editing")
				render(view: "edit", model: [kategoriKendaraanInstance: kategoriKendaraanInstance])
				return
			}
		}

        def cek = KategoriKendaraan.createCriteria()
        def result = cek.list() {
            eq("m101KodeKategori",params.m101KodeKategori, [ignoreCase: true])
            eq("staDel",'0')
        }
        def cek2 = KategoriKendaraan.createCriteria()
        def result2 = cek2.list() {
            eq("m101NamaKategori",params.m101NamaKategori, [ignoreCase: true])
            eq("staDel",'0')
        }
        if(result && KategoriKendaraan.findByM101KodeKategoriAndStaDel(params.m101KodeKategori,'0')?.id != id ){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Kode Sudah Ada')
            render(view: "edit", model: [kategoriKendaraanInstance: kategoriKendaraanInstance])
            return
        }
        if(result2 && KategoriKendaraan.findByM101NamaKategoriAndStaDel(params.m101NamaKategori,'0')?.id != id ){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Sudah Ada')
            render(view: "edit", model: [kategoriKendaraanInstance: kategoriKendaraanInstance])
            return
        }
        params?.lastUpdated = datatablesUtilService?.syncTime()
        kategoriKendaraanInstance.properties = params
		kategoriKendaraanInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		kategoriKendaraanInstance?.lastUpdProcess = "UPDATE"
		if (!kategoriKendaraanInstance.save(flush: true)) {
			render(view: "edit", model: [kategoriKendaraanInstance: kategoriKendaraanInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), kategoriKendaraanInstance.id])
		redirect(action: "show", id: kategoriKendaraanInstance.id)
	}

	def delete(Long id) {
		def kategoriKendaraanInstance = KategoriKendaraan.get(id)
		if (!kategoriKendaraanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), id])
			redirect(action: "list")
			return
		}

		try {
			//kategoriKendaraanInstance.delete(flush: true)
			kategoriKendaraanInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			kategoriKendaraanInstance?.lastUpdProcess = "DELETE"
			kategoriKendaraanInstance?.setStaDel('1')
            kategoriKendaraanInstance?.lastUpdated = datatablesUtilService?.syncTime()
            kategoriKendaraanInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'kategoriKendaraan.label', default: 'KategoriKendaraan'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(KategoriKendaraan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(KategoriKendaraan, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
