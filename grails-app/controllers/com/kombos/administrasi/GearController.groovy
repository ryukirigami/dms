package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GearController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = Gear.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if(params."sCriteria_baseModel"){
                baseModel{
                    ilike("m102NamaBaseModel","%"+ (params."sCriteria_baseModel") +"%")
                }
			}

			if(params."sCriteria_modelName"){
//				eq("modelName",ModelName.findByM104NamaModelName(params."sCriteria_modelName"))
                modelName{
                    ilike("m104NamaModelName","%"+ (params."sCriteria_modelName") +"%")
                }
			}

			if(params."sCriteria_bodyType"){
//				eq("bodyType",BodyType.findByM105NamaBodyType(params."sCriteria_bodyType"))
                bodyType{
                    ilike("m105NamaBodyType","%"+ (params."sCriteria_bodyType") +"%")
                }
			}

			if(params."sCriteria_m106ID"){
				eq("m106ID",params."sCriteria_m106ID")
			}
	
			if(params."sCriteria_m106KodeGear"){
				ilike("m106KodeGear","%" + (params."sCriteria_m106KodeGear" as String) + "%")
			}
	
			if(params."sCriteria_m106NamaGear"){
				ilike("m106NamaGear","%" + (params."sCriteria_m106NamaGear" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			switch(sortProperty){
                case "baseModel":
                    baseModel{
                        order("m102NamaBaseModel",sortDir)
                    }
                    break;
                case "modelName":
                    modelName{
                        order("m104NamaModelName",sortDir)
                    }
                    break;
                case "bodyType":
                    bodyType{
                        order("m105NamaBodyType",sortDir)
                    }
                    break;
                default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,

                        baseModel: it.baseModel?.m102NamaBaseModel+" - "+it.baseModel?.m102KodeBaseModel,

                        modelName: it.modelName.m104NamaModelName+" - "+it.modelName.m104KodeModelName,
			
						bodyType: it.bodyType?.m105NamaBodyType+" - "+it.bodyType?.m105KodeBodyType,
			
						m106ID: it.m106ID,
			
						m106KodeGear: it.m106KodeGear,
			
						m106NamaGear: it.m106NamaGear,
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[gearInstance: new Gear(params)]
	}

	def save() {
		def gearInstance = new Gear(params)
        def cek = Gear.createCriteria().list() {
            and{
                eq("baseModel",gearInstance.baseModel)
                eq("modelName",gearInstance.modelName)
                eq("bodyType",gearInstance.bodyType)
                eq("m106KodeGear",gearInstance.m106KodeGear?.trim(), [ignoreCase: true])
                eq("m106NamaGear",gearInstance.m106NamaGear?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [gearInstance: gearInstance])
            return
        }
        gearInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        gearInstance?.lastUpdProcess = "INSERT"
        gearInstance?.setStaDel('0')

        if (!gearInstance.save(flush: true)) {
            render(view: "create", model: [bodyTypeInstance: gearInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'gear.label', default: 'Gear'), gearInstance.id])
		redirect(action: "show", id: gearInstance.id)
	}

	def show(Long id) {
		def gearInstance = Gear.get(id)
		if (!gearInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gear.label', default: 'Gear'), id])
			redirect(action: "list")
			return
		}

		[gearInstance: gearInstance]
	}

	def edit(Long id) {
		def gearInstance = Gear.get(id)
		if (!gearInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gear.label', default: 'Gear'), id])
			redirect(action: "list")
			return
		}

		[gearInstance: gearInstance]
	}

	def update(Long id, Long version) {
		def gearInstance = Gear.get(id)
		if (!gearInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gear.label', default: 'Gear'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (gearInstance.version > version) {
				
				gearInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'gear.label', default: 'Gear')] as Object[],
				"Another user has updated this Gear while you were editing")
				render(view: "edit", model: [gearInstance: gearInstance])
				return
			}
		}
        if(Gear.findByM106KodeGearAndStaDel(params.m106KodeGear,'0')!=null){
            if(Gear.findByM106KodeGearAndStaDel(params.m106KodeGear,'0').id != id){
            flash.message = '* Kode Sudah Pernah Digunakan'
            render(view: "edit", model: [gearInstance: gearInstance])
            return
            }
        }
        def cek = Gear.createCriteria()
        def result = cek.list() {
            and{
                //eq("m106ID",gearInstance.m106ID)
                ne("id",id)
                eq("baseModel",BaseModel.findById(Long.parseLong(params."baseModel.id")))
                eq("modelName",ModelName.findById(Long.parseLong(params."modelName.id")))
                eq("bodyType",BodyType.findById(Long.parseLong(params."bodyType.id")))
                eq("m106KodeGear",params.m106KodeGear?.trim(), [ignoreCase: true])
                eq("m106NamaGear",params.m106NamaGear?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }

        //find(operationInstance)
        if(!result){
            gearInstance.properties = params
            gearInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            gearInstance?.lastUpdProcess = "UPDATE"
            //gearInstance?.setStaDel('0')

            if (!gearInstance.save(flush: true)) {
                render(view: "edit", model: [gearInstance: gearInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.gear.error.message', default: 'Duplicate Data Gear.')
            render(view: "edit", model: [gearInstance: gearInstance])
            return
        }
//		gearInstance.properties = params
//        gearInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
//        gearInstance?.lastUpdProcess = "UPDATE"
//
//		if (!gearInstance.save(flush: true)) {
//			render(view: "edit", model: [gearInstance: gearInstance])
//			return
//		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'gear.label', default: 'Gear'), gearInstance.id])
		redirect(action: "show", id: gearInstance.id)
	}

	def delete(Long id) {
		def gearInstance = Gear.get(id)
		if (!gearInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'gear.label', default: 'Gear'), id])
			redirect(action: "list")
			return
		}

		try {
            gearInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            gearInstance?.lastUpdProcess = "DELETE"
			gearInstance?.setStaDel('1')
			gearInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'gear.label', default: 'Gear'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'gear.label', default: 'Gear'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Gear, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Gear, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}


	
	
}
