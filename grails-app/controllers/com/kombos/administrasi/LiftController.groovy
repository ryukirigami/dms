package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class LiftController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Lift.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m029NamaLift") {
                ilike("m029NamaLift", "%" + (params."sCriteria_m029NamaLift" as String) + "%")
            }

            if (params."sCriteria_m029ID") {
                eq("m029ID", params."sCriteria_m029ID")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m029NamaLift: it.m029NamaLift,

                    m029ID: it.m029ID,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [liftInstance: new Lift(params)]
    }

    def save() {
        def liftInstance = new Lift(params)
        liftInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        liftInstance?.lastUpdProcess = "UPDATE"
        def cek = Lift.createCriteria().list() {
                eq("m029NamaLift",liftInstance.m029NamaLift?.trim(), [ignoreCase: true])
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [liftInstance: liftInstance])
            return
        }
		if (!liftInstance.save(flush: true)) {
            render(view: "create", model: [liftInstance: liftInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'lift.label', default: 'Lift'), liftInstance.id])
        redirect(action: "show", id: liftInstance.id)
    }

    def show(Long id) {
        def liftInstance = Lift.get(id)
        if (!liftInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'lift.label', default: 'Lift'), id])
            redirect(action: "list")
            return
        }

        [liftInstance: liftInstance]
    }

    def edit(Long id) {
        def liftInstance = Lift.get(id)
        if (!liftInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'lift.label', default: 'Lift'), id])
            redirect(action: "list")
            return
        }

        [liftInstance: liftInstance]
    }

    def update(Long id, Long version) {
        def liftInstance = Lift.get(id)
        if (!liftInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'lift.label', default: 'Lift'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (liftInstance.version > version) {

                liftInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'lift.label', default: 'Lift')] as Object[],
                        "Another user has updated this Lift while you were editing")
                render(view: "edit", model: [liftInstance: liftInstance])
                return
            }
        }
        def cek = Lift.createCriteria()
        def result = cek.list() {
            eq("m029NamaLift",liftInstance.m029NamaLift?.trim(), [ignoreCase: true])
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [liftInstance: liftInstance])
            return
        }

        liftInstance.properties = params
        liftInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        liftInstance?.lastUpdProcess = "UPDATE"

        if (!liftInstance.save(flush: true)) {
            render(view: "edit", model: [liftInstance: liftInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'lift.label', default: 'Lift'), liftInstance.id])
        redirect(action: "show", id: liftInstance.id)
    }

    def delete(Long id) {
        def liftInstance = Lift.get(id)
        if (!liftInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'lift.label', default: 'Lift'), id])
            redirect(action: "list")
            return
        }

        try {
            liftInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'lift.label', default: 'Lift'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'lift.label', default: 'Lift'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Lift, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Lift, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
