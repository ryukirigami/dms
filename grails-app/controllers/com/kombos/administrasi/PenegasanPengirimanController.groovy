package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class PenegasanPengirimanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def penegasanPengirimanService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render penegasanPengirimanService.datatablesList(params) as JSON
    }

    def create() {
        def result = penegasanPengirimanService.create(params)

        if (!result.error)
            return [penegasanPengirimanInstance: result.penegasanPengirimanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = penegasanPengirimanService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PenegasanPengiriman", result.penegasanPengirimanInstance.id])
            redirect(action: 'show', id: result.penegasanPengirimanInstance.id)
            return
        }

        render(view: 'create', model: [penegasanPengirimanInstance: result.penegasanPengirimanInstance])
    }

    def show(Long id) {
        def result = penegasanPengirimanService.show(params)

        if (!result.error)
            return [penegasanPengirimanInstance: result.penegasanPengirimanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = penegasanPengirimanService.show(params)

        if (!result.error)
            return [penegasanPengirimanInstance: result.penegasanPengirimanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = penegasanPengirimanService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PenegasanPengiriman", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [penegasanPengirimanInstance: result.penegasanPengirimanInstance.attach()])
    }

    def delete() {
        def result = penegasanPengirimanService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PenegasanPengiriman", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PenegasanPengiriman, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
