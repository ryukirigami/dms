package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class DealerPenjualController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		
		session.exportParams=params
		
		def c = DealerPenjual.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if(params."sCriteria_m091NamaDealer"){
				ilike("m091NamaDealer","%" + (params."sCriteria_m091NamaDealer" as String) + "%")
			}
	
			if(params."sCriteria_m091AlamatDealer"){
				ilike("m091AlamatDealer","%" + (params."sCriteria_m091AlamatDealer" as String) + "%")
			}

			if(params."sCriteria_m091ID"){
				eq("m091ID",params."sCriteria_m091ID")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m091NamaDealer: it.m091NamaDealer,
			
						m091AlamatDealer: it.m091AlamatDealer,
			
						m091ID: it.m091ID,
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[dealerPenjualInstance: new DealerPenjual(params)]
	}

	def save() {
        def find = DealerPenjual.findAllByStaDel('0')
        String id=''
        if(find){
            int a=find.last().m091ID.toInteger()+1
            for(int b=a.toString().length();b<4;b++){
                id+='0'
            }
            id = id+a
        }else{
            id = '0001'
        }
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def dealerPenjualInstance = new DealerPenjual(params)
        dealerPenjualInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        dealerPenjualInstance?.lastUpdProcess = "INSERT"
		dealerPenjualInstance?.setStaDel ('0')
        dealerPenjualInstance?.setM091ID(id)
        def cek = DealerPenjual.createCriteria().list() {
            and{
                eq("m091NamaDealer",dealerPenjualInstance.m091NamaDealer?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [dealerPenjualInstance: dealerPenjualInstance])
            return
        }

        if (!dealerPenjualInstance.save(flush: true)) {
			render(view: "create", model: [dealerPenjualInstance: dealerPenjualInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), dealerPenjualInstance.id])
		redirect(action: "show", id: dealerPenjualInstance.id)
	}

	def show(Long id) {
		def dealerPenjualInstance = DealerPenjual.get(id)
		if (!dealerPenjualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), id])
			redirect(action: "list")
			return
		}

		[dealerPenjualInstance: dealerPenjualInstance]
	}

	def edit(Long id) {
		def dealerPenjualInstance = DealerPenjual.get(id)
		if (!dealerPenjualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), id])
			redirect(action: "list")
			return
		}

		[dealerPenjualInstance: dealerPenjualInstance]
	}

	def update(Long id, Long version) {
		def dealerPenjualInstance = DealerPenjual.get(id)
		if (!dealerPenjualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (dealerPenjualInstance.version > version) {
				
				dealerPenjualInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'dealerPenjual.label', default: 'DealerPenjual')] as Object[],
				"Another user has updated this DealerPenjual while you were editing")
				render(view: "edit", model: [dealerPenjualInstance: dealerPenjualInstance])
				return
			}
		}

        def cek = DealerPenjual.createCriteria()
        def result = cek.list() {
            and{
                eq("m091NamaDealer",params.m091NamaDealer?.trim(), [ignoreCase: true])
                eq("m091AlamatDealer",params.m091AlamatDealer?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [dealerPenjualInstance: dealerPenjualInstance])
            return
        }

        def ceknama = DealerPenjual.createCriteria()
        def resultnama = ceknama.list() {
            and{
                eq("m091NamaDealer",params.m091NamaDealer?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(resultnama){
            for(find in resultnama){
                if(id!=find.id){
                    flash.message = "Nama Dealer sudah ada"
                    render(view: "edit", model: [dealerPenjualInstance: dealerPenjualInstance])
                    return
                }
            }

        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        dealerPenjualInstance.properties = params
        dealerPenjualInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        dealerPenjualInstance?.lastUpdProcess = "UPDATE"

		if (!dealerPenjualInstance.save(flush: true)) {
			render(view: "edit", model: [dealerPenjualInstance: dealerPenjualInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), dealerPenjualInstance.id])
		redirect(action: "show", id: dealerPenjualInstance.id)
	}

	def delete(Long id) {
		def dealerPenjualInstance = DealerPenjual.get(id)
		if (!dealerPenjualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), id])
			redirect(action: "list")
			return
		}

		try {
            dealerPenjualInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            dealerPenjualInstance?.lastUpdProcess = "DELETE"
			dealerPenjualInstance?.setStaDel('1')
            dealerPenjualInstance.lastUpdated = datatablesUtilService?.syncTime()
            dealerPenjualInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'dealerPenjual.label', default: 'DealerPenjual'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(DealerPenjual, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(DealerPenjual, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
