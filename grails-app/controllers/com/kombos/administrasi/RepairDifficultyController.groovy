package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class RepairDifficultyController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = RepairDifficulty.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m042Tipe") {
                ilike("m042Tipe", "%" + (params."sCriteria_m042Tipe" as String) + "%")
            }

            if (params."sCriteria_m042Keterangan") {
                ilike("m042Keterangan", "%" + (params."sCriteria_m042Keterangan" as String) + "%")
            }

            if (params."sCriteria_m042ID") {
                eq("m042ID", params."sCriteria_m042ID")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m042Tipe: it.m042Tipe,

                    m042Keterangan: it.m042Keterangan,

                    m042ID: it.m042ID,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [repairDifficultyInstance: new RepairDifficulty(params)]
    }

    def save() {
        def repairDifficultyInstance = new RepairDifficulty(params)

        def tipe = params.m042Tipe
        def keterangan = params.m042Keterangan

        def duplicate = RepairDifficulty.findByM042TipeIlike(tipe.trim())
        if(duplicate){
            flash.message = "Data sudah tersedia"
            render(view: "create", model: [repairDifficultyInstance: repairDifficultyInstance])
            return
        }

        repairDifficultyInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        repairDifficultyInstance?.lastUpdProcess = "INSERT"


        if (!repairDifficultyInstance.save(flush: true)) {
            render(view: "create", model: [repairDifficultyInstance: repairDifficultyInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), repairDifficultyInstance.id])
        redirect(action: "show", id: repairDifficultyInstance.id)
    }

    def show(Long id) {
        def repairDifficultyInstance = RepairDifficulty.get(id)
        if (!repairDifficultyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), id])
            redirect(action: "list")
            return
        }

        [repairDifficultyInstance: repairDifficultyInstance]
    }

    def edit(Long id) {
        def repairDifficultyInstance = RepairDifficulty.get(id)
        if (!repairDifficultyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), id])
            redirect(action: "list")
            return
        }

        [repairDifficultyInstance: repairDifficultyInstance]
    }

    def update(Long id, Long version) {
        def repairDifficultyInstance = RepairDifficulty.get(id)

        def tipe = params.m042Tipe
        def keterangan = params.m042Keterangan

        def duplicate = RepairDifficulty.findByM042TipeIlike(tipe.trim())

        if(duplicate){
            flash.message = "Data sama dengan sebelumnya atau sudah tersedia"
            render(view: "edit", model: [repairDifficultyInstance: repairDifficultyInstance])
            return
        }


        if (!repairDifficultyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (repairDifficultyInstance.version > version) {

                repairDifficultyInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'repairDifficulty.label', default: 'Repair Difficulty')] as Object[],
                        "Another user has updated this RepairDifficulty while you were editing")
                render(view: "edit", model: [repairDifficultyInstance: repairDifficultyInstance])
                return
            }
        }

        repairDifficultyInstance.properties = params
        repairDifficultyInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        repairDifficultyInstance?.lastUpdProcess = "UPDATE"

        if (!repairDifficultyInstance.save(flush: true)) {
            render(view: "edit", model: [repairDifficultyInstance: repairDifficultyInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), repairDifficultyInstance.id])
        redirect(action: "show", id: repairDifficultyInstance.id)
    }

    def delete(Long id) {
        def repairDifficultyInstance = RepairDifficulty.get(id)
        if (!repairDifficultyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), id])
            redirect(action: "list")
            return
        }

        try {
            repairDifficultyInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'repairDifficulty.label', default: 'Repair Difficulty'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(RepairDifficulty, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(RepairDifficulty, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
