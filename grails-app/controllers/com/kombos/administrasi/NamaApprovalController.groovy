package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import groovy.sql.Sql
import org.springframework.dao.DataIntegrityViolationException

class NamaApprovalController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST",autoCompleteCode:"GET"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def dataSource

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = NamaApproval.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            userProfileDelegate{
                eq("companyDealer",session.userCompanyDealer)
            }
            if (params."sCriteria_kegiatanApproval") {
                kegiatanApproval{
                    ilike("m770KegiatanApproval","%"+params."sCriteria_kegiatanApproval"+"%")
                }
            }

            if (params."sCriteria_kondisiApproval") {
                eq("kondisiApproval", params."sCriteria_kondisiApproval")
            }

            if (params."sCriteria_userProfile") {
                userProfile{
                    ilike("fullname","%"+params."sCriteria_userProfile"+"%")
                }
            }

            if (params."sCriteria_m771Level") {
                eq("m771Level", Integer.parseInt(params."sCriteria_m771Level"))
            }

            if (params."sCriteria_userProfileDelegate") {
                userProfileDelegate{
                    ilike("fullname","%"+params."sCriteria_userProfileDelegate"+"%")
                }
            }

            if (params."sCriteria_m771TglAwal") {
                ge("m771TglAwal", params."sCriteria_m771TglAwal")
                lt("m771TglAwal", params."sCriteria_m771TglAwal" + 1)
            }

            if (params."sCriteria_m771TglAkhir") {
                ge("m771TglAkhir", params."sCriteria_m771TglAkhir")
                lt("m771TglAkhir", params."sCriteria_m771TglAkhir" + 1)
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                case "kegiatanApproval":
                    kegiatanApproval{
                        order("m770KegiatanApproval",sortDir)
                    }
                    break;
                case "userProfile":
                    userProfile{
                        order("fullname",sortDir)
                    }
                    break;
                case "userProfileDelegate":
                    userProfileDelegate{
                        order("fullname",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kegiatanApproval: it.kegiatanApproval.m770KegiatanApproval,

                    kondisiApproval: it.kondisiApproval,

                    userProfile: it.userProfile.fullname,

                    m771Level: it.m771Level,

                    userProfileDelegate: it.userProfileDelegate.fullname,

                    m771TglAwal: it.m771TglAwal ? it.m771TglAwal.format(dateFormat) : "",

                    m771TglAkhir: it.m771TglAkhir ? it.m771TglAkhir.format(dateFormat) : "",

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [namaApprovalInstance: new NamaApproval(params)]
    }

    def save() {
        def namaApprovalInstance = new NamaApproval()
        namaApprovalInstance?.dateCreated = datatablesUtilService?.syncTime()
        namaApprovalInstance?.lastUpdated = datatablesUtilService?.syncTime()
        namaApprovalInstance?.setKegiatanApproval(KegiatanApproval.findById(params.kegiatanApproval.id))
        namaApprovalInstance?.setM771Level(Integer.parseInt(params.m771Level))
        namaApprovalInstance?.setM771TglAwal(params.m771TglAwal)
        namaApprovalInstance?.setM771TglAkhir(params.m771TglAkhir)
        namaApprovalInstance?.approvelRole = Role.get(params.approvelRole.id)
        namaApprovalInstance?.delegasiRole = Role.get(params.delegasiRole.id)
        namaApprovalInstance?.setM771TglAkhir(params.m771TglAkhir)
        namaApprovalInstance?.setStaDel("0")
        namaApprovalInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        namaApprovalInstance?.setLastUpdProcess("INSERT")

        def user1=User.findByFullname(params.userProfile)
        def user2=User.findByFullname(params.userProfileDelegate)
        if(user1==null || user2==null){
            String validasi= "Tidak ada data user ";
            int a=0;
            if(user1==null){
                a=1;
                validasi+=params.userProfile+" dengan role "+params.approvelRole.id+" "
            }
            if(user2==null){
                if(a==1){
                    validasi+=" dan user "
                }
                validasi+=params.userProfileDelegate+" dengan role "+params.delegasiRole.id
            }
            flash.message = validasi+"<br/><br/>"
            render(view: "create", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }
        namaApprovalInstance?.setUserProfile(user1)
        namaApprovalInstance?.setUserProfileDelegate(user2)

        def cek = NamaApproval.createCriteria().list {
            eq("staDel","0")
            kegiatanApproval{
                eq("id",params.kegiatanApproval.id.toLong())
            }
            eq("m771Level",params.m771Level.toInteger())
            eq("userProfile",user1)
            eq("userProfileDelegate",user2)
        }

        if(params.m771TglAwal.after(params.m771TglAkhir)){
            flash.message = "Tanggal akhir harus sesudah tanggal awal"
            render(view: "create", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }

        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }

        if(user1 == user2 && params.approvelRole == params.delegasiRole){
            flash.message = "'Approver Name' dan 'Delegasikan Kepada' tidak boleh sama"
            render(view: "create", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }
        if (!namaApprovalInstance.save(flush: true)) {
            render(view: "create", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), ''])
        redirect(action: "show", id: namaApprovalInstance.id)
    }

    def show(Long id) {
        def namaApprovalInstance = NamaApproval.get(id)
        if (!namaApprovalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), id])
            redirect(action: "list")
            return
        }

        [namaApprovalInstance: namaApprovalInstance]
    }

    def edit(Long id) {
        def namaApprovalInstance = NamaApproval.get(id)
        if (!namaApprovalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), id])
            redirect(action: "list")
            return
        }

        [namaApprovalInstance: namaApprovalInstance]
    }

    def update(Long id, Long version) {
        def namaApprovalInstance = NamaApproval.get(id)
        if (!namaApprovalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (namaApprovalInstance.version > version) {

                namaApprovalInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'namaApproval.label', default: 'NamaApproval')] as Object[],
                        "Another user has updated this NamaApproval while you were editing")
                render(view: "edit", model: [namaApprovalInstance: namaApprovalInstance])
                return
            }
        }

        def user1=User.findByFullname(params.userProfile)
        def user2=User.findByFullname(params.userProfileDelegate)
        if(user1==null || user2==null){
            String validasi= "Tidak ada data user ";
            int a=0;
            if(user1==null){
                a=1;
                validasi+=params.userProfile+" dengan role "+params.approvelRole.id+" "
            }
            if(user2==null){
                if(a==1){
                    validasi+=" dan user "
                }
                validasi+=params.userProfileDelegate+" dengan role "+params.delegasiRole.id
            }
            flash.message = validasi+"<br/><br/>"
            render(view: "edit", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }

        namaApprovalInstance?.setUserProfile(user1)
        namaApprovalInstance?.setUserProfileDelegate(user2)

        def cek = NamaApproval.createCriteria().list {
            eq("staDel","0")
            kegiatanApproval{
                eq("id",params.kegiatanApproval.id.toLong())
            }
            eq("m771Level",params.m771Level.toInteger())
            eq("userProfile",user1)
            eq("userProfileDelegate",user2)
            ge("m771TglAwal", params.m771TglAwal)
            lt("m771TglAwal", params.m771TglAwal + 1)
            ge("m771TglAkhir", params.m771TglAkhir)
            lt("m771TglAkhir", params.m771TglAkhir + 1)
        }

        if(cek){
            for(find in cek){
                if(find.id!=id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [namaApprovalInstance: namaApprovalInstance])
                    return
                }
            }
        }
        if(params.m771TglAwal.after(params.m771TglAkhir)){
            flash.message = "Tanggal akhir harus sesudah tanggal awal"
            render(view: "edit", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }

        if(user1 == user2 && params.approvelRole.id == params.delegasiRole.id){
            flash.message = "'Approver Name' dan 'Delegasikan Kepada' tidak boleh sama"
            render(view: "edit", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }


        namaApprovalInstance?.setKegiatanApproval(KegiatanApproval.findById(params.kegiatanApproval.id))
        namaApprovalInstance?.setM771Level(Integer.parseInt(params.m771Level))
        namaApprovalInstance?.setM771TglAwal(params.m771TglAwal)
        namaApprovalInstance?.setM771TglAkhir(params.m771TglAkhir)
        namaApprovalInstance?.approvelRole = Role.get(params.approvelRole.id)
        namaApprovalInstance?.delegasiRole = Role.get(params.delegasiRole.id)
        namaApprovalInstance?.setM771TglAkhir(params.m771TglAkhir)
        namaApprovalInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        namaApprovalInstance?.setLastUpdProcess("UPDATE")
        namaApprovalInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if (!namaApprovalInstance.save(flush: true)) {
            render(view: "edit", model: [namaApprovalInstance: namaApprovalInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), ''])
        redirect(action: "show", id: namaApprovalInstance.id)
    }

    def delete(Long id) {
        def namaApprovalInstance = NamaApproval.get(id)
        if (!namaApprovalInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), id])
            redirect(action: "list")
            return
        }

        try {
            namaApprovalInstance.setStaDel("1")
            namaApprovalInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            namaApprovalInstance.setLastUpdProcess("DELETE")
            namaApprovalInstance?.lastUpdated = datatablesUtilService?.syncTime()
            namaApprovalInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'namaApproval.label', default: 'NamaApproval'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(NamaApproval, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(NamaApproval, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def getUser(){
        def res = [:]
        def opts = []
        if(params.roles){
            def role = Role.get(params.roles)
            def sql=new Sql(dataSource)
            sql.eachRow("""\
                select dom.user_id as user_id from dom_user_roles dom, dom_user dos where dom.role_id = '${role.id}' and dos.t001_stadel = '0' group by dom.user_id
            """) {
                def user = User.get(it.USER_ID)
                opts<<user.fullname
            }
        }
        res."options" = opts
        render res as JSON
    }

}


