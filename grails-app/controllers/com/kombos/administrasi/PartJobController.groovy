package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Goods
import com.kombos.parts.Konversi
import com.kombos.parts.Satuan
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PartJobController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
    def conversi= new Konversi()
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = PartJob.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_operation"){
				//eq("operation",params."sCriteria_operation")
				eq("operation",Operation.findByM053NamaOperationIlike("%"+params."sCriteria_operation"+"%"))
			}

			if(params."sCriteria_fullModelCode"){
				//eq("fullModelCode",params."sCriteria_fullModelCode")
				eq("fullModelCode",FullModelCode.findByT110FullModelCodeIlike("%"+params."sCriteria_fullModelCode"+"%"))
			}

			if(params."sCriteria_goods"){
				//eq("goods",params."sCriteria_goods")
				eq("goods",Goods.findByM111NamaIlike("%"+params."sCriteria_goods"+"%"))
			}

			if(params."sCriteria_t112Jumlah"){
				eq("t112Jumlah",Double.parseDouble(params."sCriteria_t112Jumlah"))
			}
            ilike("staDel", "0")
			if(params."sCriteria_satuan"){
				//eq("satuan",params."sCriteria_satuan")
				eq("satuan",Satuan.findById(params."sCriteria_satuan"))
			}

			if(params."sCriteria_namaProsesBP"){
			//	eq("namaProsesBP",params."sCriteria_namaProsesBP")
				eq("namaProsesBP",NamaProsesBP.findByM190NamaProsesBPIlike("%"+params."sCriteria_namaProsesBP"+"%"))
			}


			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						operation: it.operation.m053NamaOperation,
			
						fullModelCode: it.fullModelCode.t110FullModelCode,
			
						goods: it.goods.m111Nama,
			
						t112Jumlah: conversi.toRupiah(it.t112Jumlah),
			
						satuan: it.satuan.m118Satuan1,
			
						namaProsesBP: it?.namaProsesBP?.m190NamaProsesBP,
			
						m102Foto: it.m102Foto,
			
						m102FotoImageMime: it.m102FotoImageMime,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[partJobInstance: new PartJob(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def partJobInstance = new PartJob(params)
        partJobInstance?.goods = Goods.findById(params.goods.id as Long)
		partJobInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		partJobInstance?.lastUpdProcess = "INSERT"
		partJobInstance.staDel = '0'

		def foto = request.getFile('m102Foto')
		def notAllowContFoto = ['application/octet-stream']
		if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){

			log.info('Foto ada')
			log.info('isi foto '+request.getFile('m102Foto'))
			log.info('Jenis konten '+foto.getContentType())
			def okcontents = ['image/png', 'image/jpeg', 'image/gif']
			if (! okcontents.contains(foto.getContentType())) {
				flash.message = "Jenis gambar harus berupa: ${okcontents}"
				render(view:'edit', model:[partJobInstance: partJobInstance])
				return;
			}

			partJobInstance.m102Foto = foto.getBytes()
			partJobInstance.m102FotoImageMime = foto.getContentType()
		}
		if (!partJobInstance.save(flush: true)) {
			render(view: "create", model: [partJobInstance: partJobInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'partJob.label', default: 'Job Part'), partJobInstance.id])
		redirect(action: "show", id: partJobInstance.id)
	}

	def show(Long id) {
		def partJobInstance = PartJob.get(id)
		if (!partJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'partJob.label', default: 'Job Part'), id])
			redirect(action: "list")
			return
		}

		 [partJobInstance: partJobInstance,logostamp: new Date().format("yyyyMMddhhmmss")]
	}

	def edit(Long id) {
		def partJobInstance = PartJob.get(id)
		if (!partJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'partJob.label', default: 'Job Part'), id])
			redirect(action: "list")
			return
		}

		[partJobInstance: partJobInstance]
	}

	def update(Long id, Long version) {
		def partJobInstance = PartJob.get(id)
		if (!partJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'partJob.label', default: 'Job Part'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (partJobInstance.version > version) {
				
				partJobInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'partJob.label', default: 'PartJob')] as Object[],
				"Another user has updated this PartJob while you were editing")
				render(view: "edit", model: [partJobInstance: partJobInstance])
				return
			}
		}

        params?.lastUpdated = datatablesUtilService?.syncTime()
        partJobInstance.properties = params
        partJobInstance?.goods = Goods.findById(params.goods.id as Long)
		partJobInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		partJobInstance?.lastUpdProcess = "UPDATE"

		def foto = request.getFile('m102Foto')
		def notAllowContFoto = ['application/octet-stream']
		if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){

			log.info('Foto ada')
			log.info('isi foto '+request.getFile('m102Foto'))
			log.info('Jenis konten '+foto.getContentType())
			def okcontents = ['image/png', 'image/jpeg', 'image/gif']
			if (! okcontents.contains(foto.getContentType())) {
				flash.message = "Jenis gambar harus berupa: ${okcontents}"
				render(view:'edit', model:[partJobInstance: partJobInstance])
				return;
			}

			partJobInstance.m102Foto = foto.getBytes()
			partJobInstance.m102FotoImageMime = foto.getContentType()
		}

		if (!partJobInstance.save(flush: true)) {
			render(view: "edit", model: [partJobInstance: partJobInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'partJob.label', default: 'Job Part'), partJobInstance.id])
		redirect(action: "show", id: partJobInstance.id)
	}

	def delete(Long id) {
		def partJobInstance = PartJob.get(id)
		if (!partJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'partJob.label', default: 'Job Part'), id])
			redirect(action: "list")
			return
		}

		try {
			partJobInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            partJobInstance?.lastUpdProcess = "DELETE"
            partJobInstance.staDel = '1'
            partJobInstance?.lastUpdated = datatablesUtilService?.syncTime()
            partJobInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'partJob.label', default: 'Job Part'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'partJob.label', default: 'Job Part'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(PartJob, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(PartJob, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	def showFoto = {
		def partJob = PartJob.get(params.id)

		/* if (!avatarUser || !avatarUser.avatar || !avatarUser.avatarType) {
		   response.sendError(404)
		   return;
		 }*/


		response.setContentType(partJob.m102FotoImageMime)
		response.setContentLength(partJob.m102Foto.size())
		OutputStream out = response.getOutputStream();
		out.write(partJob.m102Foto);
		out.close();
	}
    def listGoods() {
        def res = [:]
        def opts = []
        def c = Goods.createCriteria()
        def results=c.list {
            or{
                ilike("m111Nama","%" + params.query + "%")
                ilike("m111ID","%" + params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.m111ID.trim() +" || "+ it.m111Nama
        }
        res."options" = opts
        render res as JSON

    }

    def listFullModelCode() {
        def res = [:]
        def opts = []
        def c = FullModelCode.createCriteria()
        def results=c.list {
            ilike("t110FullModelCode","%" + params.query + "%")
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.t110FullModelCode.trim()
        }
        res."options" = opts
        render res as JSON

    }

    def detailGoods(){
        def result = [:]
        def data= params.noGoods as String
        if(data.contains("||")){
            def dataNogoods = data.split(" ")
            def goods = Goods.createCriteria().list {
                eq("staDel","0")
                ilike("m111ID","%"+dataNogoods[0].trim() + "%")
            }
            if(goods.size()>0){
                def goodsData = goods.last()
                result."hasil" = "ada"
                result."id" = goodsData?.id
                result."namaGoods" = goodsData?.m111Nama
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }

    def detailFMC(){
        def result = [:]
        def data= params.noFMC as String
        if(data.contains("-")){
            def fmc = FullModelCode.createCriteria().list {
                eq("staDel","0")
                ilike("t110FullModelCode","%"+data.trim() + "%")
            }
            if(fmc.size()>0){
                def fmcData = fmc.last()
                result."hasil" = "ada"
                result."id" = fmcData?.id
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }
}
