package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ManPowerDetailController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = ManPowerDetail.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")

            if(params."sCriteria_manPower"){
                manPower{
                    ilike("m014JabatanManPower","%"+(params."sCriteria_manPower")+"%")
                }
			}
	
			if(params."sCriteria_m015LevelManPower"){
				ilike("m015LevelManPower","%" + (params."sCriteria_m015LevelManPower" as String) + "%")
			}
	
			if(params."sCriteria_m015Inisial"){
				ilike("m015Inisial","%" + (params."sCriteria_m015Inisial" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_m015ID"){
				ilike("m015ID","%" + (params."sCriteria_m015ID" as String) + "%")
			}

			
			switch(sortProperty){
                case "manPower":
                    manPower{
                        order("m014JabatanManPower",sortDir)
                    }
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						manPower: it.manPower?.m014JabatanManPower,
			
						m015LevelManPower: it.m015LevelManPower.toUpperCase(),
			
						m015Inisial: it.m015Inisial.toUpperCase(),
			
						staDel: it.staDel,
			
						m015ID: it.m015ID,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[manPowerDetailInstance: new ManPowerDetail(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def manPowerDetailInstance = new ManPowerDetail(params)
        manPowerDetailInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        manPowerDetailInstance?.lastUpdProcess = "INSERT"
		manPowerDetailInstance?.setStaDel('0')

        def cek = ManPowerDetail.createCriteria().list() {
            and{
                eq("manPower",manPowerDetailInstance.manPower)
                eq("m015LevelManPower",manPowerDetailInstance.m015LevelManPower?.trim(), [ignoreCase: true])
                eq("m015Inisial",manPowerDetailInstance.m015Inisial?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [manPowerDetailInstance: manPowerDetailInstance])
            return
        }

		if (!manPowerDetailInstance.save(flush: true)) {
			render(view: "create", model: [manPowerDetailInstance: manPowerDetailInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'manPowerDetail.label', default: 'Level Man Power'), manPowerDetailInstance.id])
		redirect(action: "show", id: manPowerDetailInstance.id)
	}

	def show(Long id) {
		def manPowerDetailInstance = ManPowerDetail.get(id)
		if (!manPowerDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerDetail.label', default: 'ManPowerDetail'), id])
			redirect(action: "list")
			return
		}

		[manPowerDetailInstance: manPowerDetailInstance]
	}

	def edit(Long id) {
		def manPowerDetailInstance = ManPowerDetail.get(id)
		if (!manPowerDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerDetail.label', default: 'ManPowerDetail'), id])
			redirect(action: "list")
			return
		}

		[manPowerDetailInstance: manPowerDetailInstance]
	}

	def update(Long id, Long version) {
		def manPowerDetailInstance = ManPowerDetail.get(id)
        if (!manPowerDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerDetail.label', default: 'ManPowerDetail'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (manPowerDetailInstance.version > version) {
				
				manPowerDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'manPowerDetail.label', default: 'ManPowerDetail')] as Object[],
				"Another user has updated this ManPowerDetail while you were editing")
				render(view: "edit", model: [manPowerDetailInstance: manPowerDetailInstance])
				return
			}
		}

        if(ManPowerDetail.findByManPowerAndM015LevelManPowerAndM015InisialAndStaDel(ManPower.findById(params.manPower?.id),params.m015LevelManPower,params.m015Inisial,'0')!=null){
            if(ManPowerDetail.findByManPowerAndM015LevelManPowerAndM015InisialAndStaDel(ManPower.findById(params.manPower?.id),params.m015LevelManPower,params.m015Inisial,'0').id != id){

                flash.message = '* Data Sudah Ada'
                render(view: "edit", model: [manPowerDetailInstance: manPowerDetailInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        manPowerDetailInstance.properties = params
        manPowerDetailInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        manPowerDetailInstance?.lastUpdProcess = "UPDATE"

		if (!manPowerDetailInstance.save(flush: true)) {
			render(view: "edit", model: [manPowerDetailInstance: manPowerDetailInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'manPowerDetail.label', default: 'Level Man Power'), manPowerDetailInstance.id])
		redirect(action: "show", id: manPowerDetailInstance.id)
	}

	def delete(Long id) {
		def manPowerDetailInstance = ManPowerDetail.get(id)
		if (!manPowerDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPowerDetail.label', default: 'ManPowerDetail'), id])
			redirect(action: "list")
			return
		}

		try {
            manPowerDetailInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            manPowerDetailInstance?.lastUpdProcess = "DELETE"
			manPowerDetailInstance?.setStaDel('1')
            manPowerDetailInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerDetailInstance.save(flush:true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'manPowerDetail.label', default: 'ManPowerDetail'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'manPowerDetail.label', default: 'ManPowerDetail'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(ManPowerDetail, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ManPowerDetail, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
