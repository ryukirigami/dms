package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class DinamicDiscController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = DinamicDisc.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_companyDealer"){
				eq("companyDealer",params."sCriteria_companyDealer")
			}

			if(params."sCriteria_m020TMT"){
				ge("m020TMT",params."sCriteria_m020TMT")
				lt("m020TMT",params."sCriteria_m020TMT" + 1)
			}

			if(params."sCriteria_m020BookingSBEProg"){
				eq("m020BookingSBEProg",params."sCriteria_m020BookingSBEProg")
			}

			if(params."sCriteria_m020BookingSBEMax"){
				eq("m020BookingSBEMax",params."sCriteria_m020BookingSBEMax")
			}

			if(params."sCriteria_m020NonBookingSBEProg"){
				eq("m020NonBookingSBEProg",params."sCriteria_m020NonBookingSBEProg")
			}

			if(params."sCriteria_m020NonBookingSBEMax"){
				eq("m020NonBookingSBEMax",params."sCriteria_m020NonBookingSBEMax")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			eq("m020StaDel", "0")
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companyDealer: it.companyDealer,
			
						m020TMT: it.m020TMT?it.m020TMT.format(dateFormat):"",
			
						m020BookingSBEProg: it.m020BookingSBEProg,
			
						m020BookingSBEMax: it.m020BookingSBEMax,
			
						m020NonBookingSBEProg: it.m020NonBookingSBEProg,
			
						m020NonBookingSBEMax: it.m020NonBookingSBEMax,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[dinamicDiscInstance: new DinamicDisc(params)]
	}

	def save() {
        CompanyDealer companyDealer = session.userCompanyDealer
        DinamicDisc dinamicDiscInstance = new DinamicDisc(params)
        dinamicDiscInstance.companyDealer = companyDealer

        if(params.m020TMT){
            DinamicDisc cek = DinamicDisc.findByCompanyDealerAndM020TMTAndM020StaDel(companyDealer, params.m020TMT, '0')
            if(cek){
                flash.message = message(code: 'dinamicDisc.m020TMT.unique')
                render(view: "create", model: [dinamicDiscInstance: dinamicDiscInstance])
                return
            }
        }

		if (!dinamicDiscInstance.save(flush: true)) {
			render(view: "create", model: [dinamicDiscInstance: dinamicDiscInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'dinamicDisc.label', default: 'DinamicDisc'), dinamicDiscInstance.id])
		redirect(action: "show", id: dinamicDiscInstance.id)
	}

	def show(Long id) {
		def dinamicDiscInstance = DinamicDisc.get(id)
		if (!dinamicDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dinamicDisc.label', default: 'DinamicDisc'), id])
			redirect(action: "list")
			return
		}

		[dinamicDiscInstance: dinamicDiscInstance]
	}

	def edit(Long id) {
		def dinamicDiscInstance = DinamicDisc.get(id)
		if (!dinamicDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dinamicDisc.label', default: 'DinamicDisc'), id])
			redirect(action: "list")
			return
		}

		[dinamicDiscInstance: dinamicDiscInstance]
	}

	def update(Long id, Long version) {
		def dinamicDiscInstance = DinamicDisc.get(id)
		if (!dinamicDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dinamicDisc.label', default: 'DinamicDisc'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (dinamicDiscInstance.version > version) {
				
				dinamicDiscInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'dinamicDisc.label', default: 'DinamicDisc')] as Object[],
				"Another user has updated this DinamicDisc while you were editing")
				render(view: "edit", model: [dinamicDiscInstance: dinamicDiscInstance])
				return
			}
		}

        if(params.m020TMT){
            DinamicDisc cek = DinamicDisc.findByCompanyDealerAndM020TMTAndM020StaDelAndId(companyDealer, params.m020TMT, '0', id)
            if(cek){
                flash.message = message(code: 'dinamicDisc.m020TMT.unique')
                render(view: "create", model: [dinamicDiscInstance: dinamicDiscInstance])
                return
            }
        }

		dinamicDiscInstance.properties = params

		if (!dinamicDiscInstance.save(flush: true)) {
			render(view: "edit", model: [dinamicDiscInstance: dinamicDiscInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'dinamicDisc.label', default: 'DinamicDisc'), dinamicDiscInstance.id])
		redirect(action: "show", id: dinamicDiscInstance.id)
	}

	def delete(Long id) {
		DinamicDisc dinamicDiscInstance = DinamicDisc.get(id)
		if (!dinamicDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dinamicDisc.label', default: 'DinamicDisc'), id])
			redirect(action: "list")
			return
		}

		try {
//			dinamicDiscInstance.delete(flush: true)
            dinamicDiscInstance.m020StaDel = "1"
            if (!dinamicDiscInstance.save(flush: true)) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'dinamicDisc.label', default: 'Dinamic Disc'), id])
                redirect(action: "show", id: id)
                return
            }
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'dinamicDisc.label', default: 'Dinamic Disc'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'dinamicDisc.label', default: 'Dinamic Disc'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
//			datatablesUtilService.massDelete(DinamicDisc, params)
            def jsonArray = JSON.parse(params.ids)

            jsonArray.each {
                DinamicDisc dinamicDiscInstance = DinamicDisc.get(it)

                dinamicDiscInstance.m020StaDel = "1"
                //namaDokumenInstance.delete(flush: true)
                dinamicDiscInstance.save(flush: true)

            }

            res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(DinamicDisc, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
