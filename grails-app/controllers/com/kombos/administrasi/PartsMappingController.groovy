package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Goods
import com.kombos.parts.Konversi
import com.kombos.parts.Satuan
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PartsMappingController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
     def conversi =new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PartsMapping.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_fullModelCode") {
                fullModelCode{
                    ilike("t110FullModelCode", "%"+params."sCriteria_fullModelCode"+"%")
                }
           //     eq("fullModelCode", params."sCriteria_fullModelCode")
            }

            if (params."sCriteria_goods") {
               goods{
                   ilike("m111Nama","%"+params."sCriteria_goods"+"%")
               //    ilike("m111ID","%"+params."sCriteria_goods"+"%")

               }
               // eq("goods",m111Nama)
            }

            if (params."sCriteria_goodsID") {
                goods{
                    ilike("m111ID","%"+params."sCriteria_goodsID"+"%")
                    //    ilike("m111ID","%"+params."sCriteria_goods"+"%")

                }
                // eq("goods",m111Nama)
            }

            if (params."sCriteria_t111Jumlah1") {
                eq("t111Jumlah1", Double.parseDouble(params."sCriteria_t111Jumlah1"))
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_foto") {
                eq("foto", params."sCriteria_foto")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_satuan") {
                satuan{
                    ilike("m118Satuan1","%"+params."sCriteria_satuan"+"%")
                }
            // eq("satuan", params."sCriteria_satuan")
            }

            eq("staDel", "0")


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    fullModelCode: it.fullModelCode?.toString(),

                    goodsID : it.goods?.m111ID,

                    goods: it.goods?.m111Nama,

                    t111Jumlah1: it.t111Jumlah1,

                    staDel: it.staDel,

                    foto: it.foto,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    satuan: it.satuan?.m118Satuan1,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [partsMappingInstance: new PartsMapping(params)]
    }

    def save() {
        def partsMappingInstance = new PartsMapping(params)
        partsMappingInstance?.companyDealer = session.userCompanyDealer
        partsMappingInstance.staDel = '0'
        partsMappingInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        partsMappingInstance?.lastUpdProcess = "INSERT"

        def logo = request.getFile('foto')
        def notAllowCont = ['application/octet-stream']
        if(logo !=null && !notAllowCont.contains(logo.getContentType()) ){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(logo.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'create', model:[partsMappingInstance: partsMappingInstance])
                return;
            }

            partsMappingInstance.foto = logo.getBytes()
            partsMappingInstance.imageMime = logo.getContentType()
        }

        if (!partsMappingInstance.save(flush: true)) {
            render(view: "create", model: [partsMappingInstance: partsMappingInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'partsMapping.label', default: 'Mapping Full Model Parts'), partsMappingInstance.id])
        redirect(action: "show", id: partsMappingInstance.id)
    }

    def show(Long id) {
        def partsMappingInstance = PartsMapping.get(id)
        if (!partsMappingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsMapping.label', default: 'PartsMapping'), id])
            redirect(action: "list")
            return
        }

        [partsMappingInstance: partsMappingInstance, logostamp: new Date().format("yyyyMMddhhmmss")]
    }

    def edit(Long id) {
        def partsMappingInstance = PartsMapping.get(id)
        if (!partsMappingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsMapping.label', default: 'PartsMapping'), id])
            redirect(action: "list")
            return
        }

        [partsMappingInstance: partsMappingInstance]
    }

    def update(Long id, Long version) {
        def partsMappingInstance = PartsMapping.get(Double.parseDouble(params.id))

        if (!partsMappingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsMapping.label', default: 'PartsMapping'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (partsMappingInstance.version > version) {

                partsMappingInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'partsMapping.label', default: 'PartsMapping')] as Object[],
                        "Another user has updated this PartsMapping while you were editing")
                render(view: "edit", model: [partsMappingInstance: partsMappingInstance])
                return
            }
        }

        partsMappingInstance.fullModelCode = FullModelCode.get(params.fullModelCode.id)
        partsMappingInstance.goods = Goods.get(params.goods.id)
        String parsed = params.t111Jumlah1.replace(",","");
        partsMappingInstance.t111Jumlah1 = Double.parseDouble(parsed)
        partsMappingInstance.satuan = Satuan.get(params.satuan.id)

        def logo = request.getFile('foto')
        def notAllowCont = ['application/octet-stream']
        if(logo !=null && !notAllowCont.contains(logo.getContentType()) ){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(logo.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[partsMappingInstance: partsMappingInstance])
                return;
            }

            partsMappingInstance.foto = logo.getBytes()
            partsMappingInstance.imageMime = logo.getContentType()
        }

      //  partsMappingInstance.properties = params
        partsMappingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        partsMappingInstance?.lastUpdProcess = "UPDATE"


        if (!partsMappingInstance.save(flush: true)) {
            render(view: "edit", model: [partsMappingInstance: partsMappingInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'partsMapping.label', default: 'Mapping Full Model Parts'), partsMappingInstance.id])
        redirect(action: "show", id: partsMappingInstance.id)
    }

    def delete(Long id) {
        def partsMappingInstance = PartsMapping.get(id)
        if (!partsMappingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsMapping.label', default: 'PartsMapping'), id])
            redirect(action: "list")
            return
        }

        partsMappingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        partsMappingInstance?.lastUpdProcess = "DELETE"

        partsMappingInstance.staDel = '1'
        try {
            partsMappingInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'partsMapping.label', default: 'PartsMapping'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'partsMapping.label', default: 'PartsMapping'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PartsMapping, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PartsMapping, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def showFoto = {
        def partsMapping = PartsMapping.get(params.id)
        response.setContentLength(partsMapping.foto.size())
        OutputStream out = response.getOutputStream();
        out.write(partsMapping.foto);
        out.close();
    }


    def showLogo = {
        def partsMapping = PartsMapping.get(params.id)


        response.setContentLength(partsMapping.foto.size())
        OutputStream out = response.getOutputStream();
        out.write(partsMapping.foto);
        out.close();
    }

}
