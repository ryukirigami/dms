package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ManPowerController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = ManPower.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")	
			if(params."sCriteria_m014JabatanManPower"){
				ilike("m014JabatanManPower","%" + (params."sCriteria_m014JabatanManPower" as String) + "%")
			}
	
			if(params."sCriteria_m014Inisial"){
				ilike("m014Inisial","%" + (params."sCriteria_m014Inisial" as String) + "%")
			}
	
			if(params."sCriteria_m014staGRBPADM"){
				eq("m014staGRBPADM",params."sCriteria_m014staGRBPADM")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_m014ID"){
				ilike("m014ID","%" + (params."sCriteria_m014ID" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m014JabatanManPower: it.m014JabatanManPower.toUpperCase(),
			
						m014Inisial: it.m014Inisial.toUpperCase(),
			
						m014staGRBPADM: it.m014staGRBPADM,
			
						staDel: it.staDel,
			
						m014ID: it.m014ID,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[manPowerInstance: new ManPower(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def manPowerInstance = new ManPower(params)
        manPowerInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        manPowerInstance?.lastUpdProcess = "INSERT"
        manPowerInstance?.setStaDel('0')

        if(ManPower.findByM014JabatanManPowerAndStaDel(params.m014JabatanManPower,'0')!=null){

            flash.message = '* Jabatan Sudah Pernah Digunakan'
            render(view: "create", model: [manPowerInstance: manPowerInstance])
            return
        }

        def cek = ManPower.createCriteria().list() {
            and{
                eq("m014JabatanManPower",manPowerInstance.m014JabatanManPower?.trim(), [ignoreCase: true])
                eq("m014Inisial",manPowerInstance.m014Inisial?.trim(), [ignoreCase: true])
                eq("m014staGRBPADM",manPowerInstance.m014staGRBPADM?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [manPowerInstance: manPowerInstance])
            return
        }
        if (!manPowerInstance.save(flush: true)) {
            render(view: "create", model: [manPowerInstance: manPowerInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'manPower.label', default: 'Jabatan Man Power'), manPowerInstance.id])
		redirect(action: "show", id: manPowerInstance.id)
	}

	def show(Long id) {
		def manPowerInstance = ManPower.get(id)
		if (!manPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPower.label', default: 'ManPower'), id])
			redirect(action: "list")
			return
		}

		[manPowerInstance: manPowerInstance]
	}

	def edit(Long id) {
		def manPowerInstance = ManPower.get(id)
        if (!manPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPower.label', default: 'ManPower'), id])
			redirect(action: "list")
			return
		}

		[manPowerInstance: manPowerInstance]
	}

	def update(Long id, Long version) {
		def manPowerInstance = ManPower.get(id)

		if (!manPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPower.label', default: 'ManPower'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (manPowerInstance.version > version) {
				
				manPowerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'manPower.label', default: 'ManPower')] as Object[],
				"Another user has updated this ManPower while you were editing")
				render(view: "edit", model: [manPowerInstance: manPowerInstance])
				return
			}
		}

        if(ManPower.findByM014JabatanManPowerAndStaDel(params.m014JabatanManPower,'0')!=null){
            if(ManPower.findByM014JabatanManPowerAndStaDel(params.m014JabatanManPower,'0').id !=id){

            flash.message = '* Jabatan Sudah Pernah Digunakan'
            render(view: "edit", model: [manPowerInstance: manPowerInstance])
            return
            }
        }

        if(ManPower.findByM014JabatanManPowerAndM014InisialAndM014staGRBPADMAndStaDel(params.m014JabatanManPower,params.m014Inisial,params.m014staGRBPADM,'0')!=null){
            if(ManPower.findByM014JabatanManPowerAndM014InisialAndM014staGRBPADMAndStaDel(params.m014JabatanManPower,params.m014Inisial,params.m014staGRBPADM,'0').id !=id){

                flash.message = '* Data Sudah Ada'
                render(view: "edit", model: [manPowerInstance: manPowerInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        manPowerInstance.properties = params
        manPowerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        manPowerInstance?.lastUpdProcess = "UPDATE"

		if (!manPowerInstance.save(flush: true)) {
			render(view: "edit", model: [manPowerInstance: manPowerInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'manPower.label', default: 'Jabatan Man Power'), manPowerInstance.id])
		redirect(action: "show", id: manPowerInstance.id)
	}

	def delete(Long id) {
		def manPowerInstance = ManPower.get(id)
		if (!manPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'manPower.label', default: 'ManPower'), id])
			redirect(action: "list")
			return
		}

		try {
            manPowerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            manPowerInstance?.lastUpdProcess = "DELETE"
			manPowerInstance?.setStaDel('1')
            manPowerInstance?.lastUpdated = datatablesUtilService?.syncTime()
            manPowerInstance.save(flush:true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'manPower.label', default: 'ManPower'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'manPower.label', default: 'ManPower'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(ManPower, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ManPower, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
