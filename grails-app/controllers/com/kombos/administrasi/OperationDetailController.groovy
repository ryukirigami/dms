package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class OperationDetailController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = OperationDetail.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            jobID{
                eq("staDel", "0")
            }
            jobIdDetail{
                eq("staDel", "0")
            }
            if(params."sCriteria_jobID"){
                String param = params."sCriteria_jobID"
                def data =  param.split("-")
                if(param.contains('-')){
                    jobID{
                        and{
                            ilike("m053NamaOperation","%" + (data[2].trim() as String) + "%")
                            ilike("m053JobsId","%" + (data[0].trim() as String) + "%")
                            eq("serial", Serial.findByM052NamaSerialIlike("%" + (data[1].trim() as String) + "%"))
                        }
                    }
                }else{
                    jobID{
                        or{
                            ilike("m053NamaOperation","%" + (param.trim()) + "%")
                            ilike("m053JobsId","%" + (param.trim()) + "%")
                            eq("serial", Serial.findByM052NamaSerialIlike("%" + (param.trim()) + "%"))
                        }
                    }
                }
			}

			if(params."sCriteria_jobIdDetail"){
                String param = params."sCriteria_jobIdDetail"
                def data =  param.split("-")
                if(param.contains('-')){
                    jobIdDetail{
                        and{
                            ilike("m053NamaOperation","%" + (data[2].trim() as String) + "%")
                            ilike("m053JobsId","%" + (data[0].trim() as String) + "%")
                            eq("serial", Serial.findByM052NamaSerialIlike("%" + (data[1].trim() as String) + "%"))
                        }
                    }
                }else{
                    jobIdDetail{
                        or{
                            ilike("m053NamaOperation","%" + (param.trim()) + "%")
                            ilike("m053JobsId","%" + (param.trim()) + "%")
                            eq("serial", Serial.findByM052NamaSerialIlike("%" + (param.trim()) + "%"))
                        }
                    }
                }
			}
	

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						jobID: it.jobID?.m053JobsId+" - "+it.jobID?.serial?.m052NamaSerial+" - "+it.jobID?.m053NamaOperation,//it.jobID?.m053NamaOperation,
			
						jobIdDetail: it.jobIdDetail?.m053JobsId+" - "+it.jobIdDetail?.serial?.m052NamaSerial+" - "+it.jobIdDetail?.m053NamaOperation,//it.jobIdDetail?.m053NamaOperation,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[operationDetailInstance: new OperationDetail(params)]
	}

	def save() {
		def operationDetailInstance = new OperationDetail(params)
        operationDetailInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        operationDetailInstance?.lastUpdProcess = "INSERT"
        operationDetailInstance?.setStaDel('0')
        if (operationDetailInstance?.jobID?.id==operationDetailInstance?.jobIdDetail?.id) {
            flash.message = message(code: 'default.created.error.message', default: 'Kode Job Paket tidak boleh sama dengan Kode Job Detail')
            render(view: "create", model: [operationDetailInstance: operationDetailInstance])
            return
        }
        if(OperationDetail.findByJobIDAndJobIdDetailAndStaDel(operationDetailInstance?.jobID,operationDetailInstance?.jobIdDetail,'0')!=null){
            flash.message = '* Data Sudah Ada'
            render(view: "create",model: [operationDetailInstance: operationDetailInstance])
            return
        }
        if (!operationDetailInstance.save(flush: true)) {
            render(view: "create", model: [operationDetailInstance: operationDetailInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), operationDetailInstance.id])
		redirect(action: "show", id: operationDetailInstance.id)
	}

	def show(Long id) {
		def operationDetailInstance = OperationDetail.get(id)
		if (!operationDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), id])
			redirect(action: "list")
			return
		}

		[operationDetailInstance: operationDetailInstance]
	}

	def edit(Long id) {
		def operationDetailInstance = OperationDetail.get(id)
		if (!operationDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), id])
			redirect(action: "list")
			return
		}

		[operationDetailInstance: operationDetailInstance]
	}

	def update(Long id, Long version) {
		def operationDetailInstance = OperationDetail.get(id)
		if (!operationDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (operationDetailInstance.version > version) {
				
				operationDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'operationDetail.label', default: 'Operation Detail')] as Object[],
				"Another user has updated this OperationDetail while you were editing")
				render(view: "edit", model: [operationDetailInstance: operationDetailInstance])
				return
			}
		}
        if (params.jobIdDetail.id==params.jobID.id) {
            flash.message = message(code: 'default.created.error.message', default: 'Kode Job Paket tidak boleh sama dengan Kode Job Detail')
            render(view: "edit", model: [operationDetailInstance: operationDetailInstance])
            return
        }
        if(OperationDetail.findByJobIDAndJobIdDetailAndStaDel(Operation.findById(params.jobID.id),Operation.findById(params.jobIdDetail.id),'0')!=null){
            if(OperationDetail.findByJobIDAndJobIdDetailAndStaDel(Operation.findById(params.jobID.id),Operation.findById(params.jobIdDetail.id),'0').id != id){
                flash.message = '* Data Sudah Ada'
                render(view: "edit", model: [operationDetailInstance: operationDetailInstance])
                return
            }
        }

		operationDetailInstance.properties = params
        operationDetailInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        operationDetailInstance?.lastUpdProcess = "UPDATE"


        if (!operationDetailInstance.save(flush: true)) {
			render(view: "edit", model: [operationDetailInstance: operationDetailInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), operationDetailInstance.id])
		redirect(action: "show", id: operationDetailInstance.id)
	}

	def delete(Long id) {
		def operationDetailInstance = OperationDetail.get(id)
		if (!operationDetailInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), id])
			redirect(action: "list")
			return
		}

		try {
            operationDetailInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            operationDetailInstance?.lastUpdProcess = "DELETE"
            operationDetailInstance?.setStaDel('1')
            operationDetailInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'operationDetail.label', default: 'Operation Detail'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(OperationDetail, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(OperationDetail, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def getJob() {
        def res = [:]
        def result = Operation.createCriteria().list() {
            eq("staDel", "0")
            or{
                ilike("m053NamaOperation","%" + (params."query" as String) + "%")
                serial{
                    ilike("m052NamaSerial", "%" + (params."query" as String) + "%")
                }
                ilike("m053JobsId", "%" + (params."query" as String) + "%")
            }
        }//Operation.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.m053NamaOperation//it.m053JobsId+"."+it.serial?.m052NamaSerial+"-"+it.m053NamaOperation
        }

        res."options" = opts
        render res as JSON
    }

    def getJobDetail() {
        def res = [:]

        def result = Operation.createCriteria().list() {
            eq("staDel", "0")
            or{
                ilike("m053NamaOperation","%" + (params."query" as String) + "%")
                serial{
                    ilike("m052NamaSerial", "%" + (params."query" as String) + "%")
                }
                ilike("m053JobsId", "%" + (params."query" as String) + "%")
            }
        }//Operation.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.m053NamaOperation//it.m053JobsId+"."+it.serial?.m052NamaSerial+"-"+it.m053NamaOperation
        }

        res."options" = opts
        render res as JSON
    }

}
