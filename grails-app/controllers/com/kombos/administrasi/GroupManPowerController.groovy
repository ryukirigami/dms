package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.reception.Reception
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.context.request.RequestContextHolder

class GroupManPowerController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST",autoCompleteCode:"GET"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = GroupManPower.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("companyDealer", session?.userCompanyDealer)

			if(params."sCriteria_t021Group"){
				ilike("t021Group","%" + (params."sCriteria_t021Group" as String) + "%")
			}

			if(params."sCriteria_namaManPower"){
				//eq("namaManPower",params."sCriteria_namaManPower")
				eq("namaManPower",NamaManPower.findByT015NamaLengkapIlike("%"+params."sCriteria_namaManPower"+"%"))
			}
	
			ilike("staDel",'0')
			
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						t021Group: it?.t021Group?.toUpperCase(),
			
						namaManPower: it?.namaManPowerForeman?.t015NamaLengkap,
			
						companyDealer: it?.companyDealer,
			
						staDel: it?.staDel,
						
						createdBy: it?.createdBy,
						
											updatedBy: it.updatedBy,
						
											lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailMembers'] = null
        def data = NamaManPower.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",session?.companyDealer)
            manPowerDetail{
                manPower{
                    ilike("m014JabatanManPower","%foreman%")
                }
            }
        }
		[groupManPowerInstance: new GroupManPower(params), data : data]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session.userCompanyDealer
		def groupManPowerInstance = new GroupManPower(params)
        def data = NamaManPower.findAllByManPowerDetailAndCompanyDealer(ManPowerDetail.findByManPower(ManPower.findByM014JabatanManPowerIlike("%Foreman%")),session.userCompanyDealer)
        def cek = GroupManPower.createCriteria().get {
            eq("staDel","0")
            eq("companyDealer", session.userCompanyDealer)
            eq("namaManPowerForeman",NamaManPower.findById(params.namaManPowerForeman.id));
            eq("t021Group",params?.t021Group?.trim(),[ignoreCase : true])
            maxResults(1);
        }
        if(cek){
            flash.message = "* Data Sudah Ada"

            render(view: "create", model: [groupManPowerInstance: groupManPowerInstance,data:data])
            return
        }
        def currentRequest = RequestContextHolder.requestAttributes
        def rows = currentRequest?.session?.detailMembers
        rows.each{
            def manPower = NamaManPower.get(it.id.toLong())
            groupManPowerInstance.addToNamaManPowers(manPower);
        }
        groupManPowerInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		groupManPowerInstance?.lastUpdProcess = "INSERT"

		groupManPowerInstance?.setStaDel('0')
		if (!groupManPowerInstance.save(flush: true)) {
			render(view: "create", model: [groupManPowerInstance: groupManPowerInstance,data:data])
			return
		}
        rows.each{
            def manPower = NamaManPower.get(it.id.toLong())
            manPower.groupManPower = groupManPowerInstance
            manPower.save(flush: true);
        }
        currentRequest.session['detailMembers']=null
		flash.message = message(code: 'default.created.message', args: [message(code: 'groupManPower.label', default: 'Group Man Power'), groupManPowerInstance.id])
		redirect(action: "show", id: groupManPowerInstance.id)
	}

	def show(Long id) {
		def groupManPowerInstance = GroupManPower.get(id)
		if (!groupManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupManPower.label', default: 'GroupManPower'), id])
			redirect(action: "list")
			return
		}

		[groupManPowerInstance: groupManPowerInstance]
	}

	def edit(Long id) {

		def groupManPowerInstance = GroupManPower.get(id)
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailMembers'] = null
        def members = groupManPowerInstance.namaManPowers
        def rows = []
        members.each {
            rows << [
                    id : it.id,
                    nama : it.t015NamaLengkap
            ]
        }
        currentRequest.session['detailMembers'] = rows
        if (!groupManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupManPower.label', default: 'GroupManPower'), id])
			redirect(action: "list")
			return
		}
        def data = NamaManPower.findAllByManPowerDetailAndCompanyDealer(ManPowerDetail.findByManPower(ManPower.findByM014JabatanManPowerIlike("%Foreman%")),session.userCompanyDealer)
		[groupManPowerInstance: groupManPowerInstance,data: data]
	}

	def update(Long id, Long version) {
		def groupManPowerInstance = GroupManPower.get(id)
        def data = NamaManPower.findAllByManPowerDetailAndCompanyDealer(ManPowerDetail.findByManPower(ManPower.findByM014JabatanManPowerIlike("%Foreman%")),session.userCompanyDealer)
		if (!groupManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupManPower.label', default: 'GroupManPower'), id])
			redirect(action: "list")
			return
		}
		if (version != null) {
			if (groupManPowerInstance.version > version) {
				
				groupManPowerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'groupManPower.label', default: 'GroupManPower')] as Object[],
				"Another user has updated this GroupManPower while you were editing")
				render(view: "edit", model: [groupManPowerInstance: groupManPowerInstance,data:data])
				return
			}
		}
        def membersss = groupManPowerInstance?.namaManPowers
        membersss.each {
            it.groupManPower = null
            it.save(flush: true)
        }
        groupManPowerInstance?.namaManPowers?.clear()
        def currentRequest = RequestContextHolder.requestAttributes
        def rows = currentRequest?.session?.detailMembers
        rows.each{
            def manPower = NamaManPower.get(it.id.toLong())
            groupManPowerInstance.namaManPowers.add(manPower);
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        groupManPowerInstance.properties = params
		groupManPowerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		groupManPowerInstance?.lastUpdProcess = "UPDATE"
        def cek = GroupManPower.findAllByNamaManPowerForemanAndStaDel(NamaManPower.findById(params.namaManPowerForeman.id),'0')
        if(cek.size()>0){
            cek.each {
                if(it.id!=id){
                    flash.message = "* Data Sudah Ada"
                    render(view: "edit", model: [groupManPowerInstance: groupManPowerInstance,data:data])
                    return
                }
            }
        }
		if (!groupManPowerInstance.save(flush: true)) {
			render(view: "edit", model: [groupManPowerInstance: groupManPowerInstance])
			return
		}
        rows.each{
            def manPower = NamaManPower.get(it.id.toLong())
            manPower.groupManPower = groupManPowerInstance
            manPower.save(flush: true);
        }
        currentRequest.session['detailMembers']=null
		flash.message = message(code: 'default.updated.message', args: [message(code: 'groupManPower.label', default: 'Group Man Power'), groupManPowerInstance.id])
		redirect(action: "show", id: groupManPowerInstance.id)
	}

	def delete(Long id) {
		def groupManPowerInstance = GroupManPower.get(id)
		if (!groupManPowerInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupManPower.label', default: 'GroupManPower'), id])
			redirect(action: "list")
			return
		}

		try {
			groupManPowerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			groupManPowerInstance?.lastUpdProcess = "DELETE"
			groupManPowerInstance?.setStaDel('1')
            groupManPowerInstance?.lastUpdated = datatablesUtilService?.syncTime()
            groupManPowerInstance.save(flush: true)
			//groupManPowerInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'groupManPower.label', default: 'GroupManPower'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'groupManPower.label', default: 'GroupManPower'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(GroupManPower, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(GroupManPower, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"

        String a = "apa"

	}

    def kodeList() {
        def res = [:]

//        def result = GroupManPower.findAllWhere(staDel : '0')
//        def opts = []
//        result.each {
//            opts << it.t021Group
//        }
//        res."options" = opts
//        render res as JSON

        def c = GroupManPower.createCriteria()
        def results=c.listDistinct {
            projections {
                property('t021Group')
            }
            eq('staDel', '0')
        }
        res."options" = results
        render res as JSON

    }


    def getManPowerInfo(){
        def idBoardPower = params.id
       // def manPower = NamaManPower.get(idManPower)

        def manPower = NamaManPower.get(idBoardPower)
        def res = [:]

        def groupMember = NamaManPower.findAllByGroupManPower(GroupManPower.findByNamaManPowerForeman(manPower))
        def groupManPower = []
        groupMember.each {
           groupManPower << it.t015NamaBoard
        }

        res.put("namaLengkap", manPower?.t015NamaLengkap)
        res.put("jabatan", manPower?.manPowerDetail?.m015LevelManPower)
        res.put("group", manPower?.groupManPower?.t021Group)
        res.put("namaBoard", manPower?.t015NamaBoard)
        res.put("namaWorkshop", manPower?.companyDealer?.m011NamaWorkshop)
        res.put("anggota", groupManPower)


        render res as JSON

    }

    def getTeknisiJPB(){
        def idTeknisi = params.id
        def idJPB = params.idJPB

        def manPower = NamaManPower.get(idTeknisi)

        def res = [:]

        res.put("namaLengkap", manPower?.t015NamaLengkap)
        res.put("namaBoard", manPower?.t015NamaBoard)
        res.put("jabatan", manPower?.manPowerDetail?.m015LevelManPower)
        res.put("group", manPower?.groupManPower?.t021Group)
        res.put("kepalaGroup", manPower?.groupManPower?.namaManPowerForeman?.t015NamaLengkap)

        def jpb = JPB.get(idJPB)
        def prevJPB = JPB.findByT451TglJamPlanLessThanAndNamaManPower(jpb?.t451TglJamPlan, manPower)
        def prevJob = prevJPB?.flatRate?.operation
        def nextJPB = JPB.findByT451TglJamPlanGreaterThanAndNamaManPower(jpb?.t451TglJamPlan, manPower)
        def nextJob = nextJPB?.flatRate?.operation
        def currJob = jpb?.flatRate?.operation

        def prevTime = prevJPB?.t451TglJamPlan
        def currTime = jpb?.t451TglJamPlan
        def nextTime = nextJPB?.t451TglJamPlan



        res.put("prevJob", prevJob?.m053NamaOperation?:"")
        res.put("prevNoWO", Reception.findByOperation(prevJob)?Reception.findByOperation(prevJob).t401NoWO:"")
        res.put("prevTime", prevTime?:" "+ " - "+prevTime?.format("hh:mm")?:"")
        res.put("currJob", currJob?.m053NamaOperation?:"")
        res.put("currNoWO", Reception.findByOperation(currJob)?.t401NoWO?:"")
        res.put("currTime", currTime?.format("hh:mm")?:"")
        res.put("nextJob", nextJob?.m053NamaOperation?:"")
        res.put("nextNoWO", Reception.findByOperation(nextJob)?Reception.findByOperation(nextJob).t401NoWO:"")
        res.put("nextTime", nextTime?:" "+ " - "+nextTime?.format("hh:mm")?:"")

        render res as JSON
    }

    def setSession(){
        def currentRequest = RequestContextHolder.requestAttributes
        def rows = currentRequest?.session?.detailMembers
        def manPower = NamaManPower.get(params?.id?.toLong())
        if(rows){
            rows << [
                id   : manPower.id,
                nama : manPower.t015NamaLengkap
            ]
        }else{
            rows = []
            rows << [
                id   : manPower.id,
                nama : manPower.t015NamaLengkap
            ]
        }
        currentRequest.session['detailMembers'] = rows
        render "ok"
    }

    def doDelete(){
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailMembers'] = null
        def rows = []
        def jsonArray = JSON.parse(params.idExist)
        jsonArray.each {
            def manPower = NamaManPower.get(it?.toLong())
            rows << [
                    id   : manPower.id,
                    nama : manPower.t015NamaLengkap
            ]
        }
        currentRequest.session['detailMembers'] = rows
        render "ok"
    }


}
