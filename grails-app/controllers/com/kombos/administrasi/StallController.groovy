package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StallController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = Stall.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            and{
                eq("staDel", "0")
                companyDealer{
                    eq("staDel","0")
                }
                jenisStall{
                    eq("staDel","0")
                }
                subJenisStall{
                    eq("staDel","0")
                }
            }
            if(params."sCriteria_companyDealer"){
                //eq("companyDealer",params."sCriteria_companyDealer")
                companyDealer{
                    ilike("m011NamaWorkshop","%" + (params."sCriteria_companyDealer" as String) + "%")
                }
            }

            if(params."sCriteria_jenisStall"){
                //eq("jenisStall",params."sCriteria_jenisStall")
                jenisStall{
                    ilike("m021NamaJenisStall","%" + (params."sCriteria_jenisStall" as String) + "%")
                }
            }

            if(params."sCriteria_subJenisStall"){
                //eq("subJenisStall",params."sCriteria_subJenisStall")
                subJenisStall{
                    ilike("m023NamaSubJenisStall","%" + (params."sCriteria_subJenisStall" as String) + "%")
                }
            }

            if(params."sCriteria_m022NamaStall"){
                ilike("m022NamaStall","%" + (params."sCriteria_m022NamaStall" as String) + "%")
            }

            if(params."sCriteria_m022Singkat"){
                ilike("m022Singkat","%" + (params."sCriteria_m022Singkat" as String) + "%")
            }

            if(params."sCriteria_m022StaTersedia"){
                ilike("m022StaTersedia","%" + (params."sCriteria_m022StaTersedia" as String) + "%")
            }

            if(params."sCriteria_m022TglJamAwal"){
                ge("m022TglJamAwal",params."sCriteria_m022TglJamAwal")
                lt("m022TglJamAwal",params."sCriteria_m022TglJamAwal" + 1)
            }

            if(params."sCriteria_m022TglJamAkhir"){
                ge("m022TglJamAkhir",params."sCriteria_m022TglJamAkhir")
                lt("m022TglJamAkhir",params."sCriteria_m022TglJamAkhir" + 1)
            }

            if(params."sCriteria_lift"){
                //eq("lift",params."sCriteria_lift")
                lift{
                    ilike("m029NamaLift","%" + (params."sCriteria_lift" as String) + "%")
                }
            }

            if(params."sCriteria_m022StaTPSLine"){
                ilike("m022StaTPSLine","%" + (params."sCriteria_m022StaTPSLine" as String) + "%")
            }

            if(params."sCriteria_namaProsesBP"){
                //eq("namaProsesBP",params."sCriteria_namaProsesBP")
                namaProsesBP{
                    ilike("m190NamaProsesBP","%" + (params."sCriteria_namaProsesBP" as String) + "%")
                }
            }

            if(params."sCriteria_staDel"){
                ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
            }

            if(params."sCriteria_createdBy"){
                ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if(params."sCriteria_updatedBy"){
                ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if(params."sCriteria_lastUpdProcess"){
                ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if(params."sCriteria_m022StallID"){
                ilike("m022StallID","%" + (params."sCriteria_m022StallID" as String) + "%")
            }

            if(params."sCriteria_m022ID"){
                ilike("m022ID","%" + (params."sCriteria_m022ID" as String) + "%")
            }


            switch(sortProperty){
                case "jenisStall":
                    jenisStall{
                        order("m021NamaJenisStall",sortDir)
                    }
                    break;
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer?.m011NamaWorkshop,

                    jenisStall: it.jenisStall?.m021NamaJenisStall,

                    subJenisStall: it.subJenisStall?.m023NamaSubJenisStall,

                    m022NamaStall: it.m022NamaStall,

                    m022Singkat: it.m022Singkat,

                    m022StaTersedia: it.m022StaTersedia,

                    m022TglJamAwal: it.m022TglJamAwal?it.m022TglJamAwal.format(dateFormat):"",

                    m022TglJamAkhir: it.m022TglJamAkhir?it.m022TglJamAkhir.format(dateFormat):"",

                    lift: it.lift?.m029NamaLift,

                    m022StaTPSLine: it.m022StaTPSLine == "0"? "Tidak" : "Ya",

                    namaProsesBP: it.namaProsesBP?.m190NamaProsesBP,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    m022StallID: it.m022StallID,

                    m022ID: it.m022ID,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [stallInstance: new Stall(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def stallInstance = new Stall(params)

        if(params.m022StaTersedia?.toString()?.equals("0")){
            stallInstance?.m022TglJamAwal?.set(minute: Integer.parseInt(params?.m022TglJamAwal_minute), hourOfDay: Integer.parseInt(params?.m022TglJamAwal_hour))//.setHours(Integer.parseInt(params?.m022TglJamAwal_hour))
            stallInstance?.m022TglJamAkhir?.set(minute: Integer.parseInt(params?.m022TglJamAkhir_minute), hourOfDay: Integer.parseInt(params?.m022TglJamAkhir_hour))//.setHours(Integer.parseInt(params?.m022TglJamAkhir_hour))

            //validasi tanggal
            if(stallInstance?.m022TglJamAkhir?.before(stallInstance?.m022TglJamAwal)){
                flash.message = message(code: 'default.error.created.namastall.message', default: "Tanggal Jam Akhir kurang dari Tanggal Jam Awal.")
                render(view: "create", model: [stallInstance: stallInstance])
                return
            }

        } else {
            stallInstance?.m022TglJamAwal=null
            stallInstance?.m022TglJamAkhir=null
        }
        
        if (params.namaProsesBP.id.equals("-1")){
            stallInstance?.namaProsesBP = null
        }else{
            stallInstance?.namaProsesBP = NamaProsesBP.findById(Long.parseLong(params.namaProsesBP.id))
        }

        def c = Stall.createCriteria()
        def result = c.list() {
            eq("staDel","0")
//            eq("companyDealer",CompanyDealer.findById(params.companyDealer.id))
            eq("jenisStall",JenisStall.findById(params.jenisStall.id))
            eq("lift",Lift.findById(params.lift.id))
            eq("subJenisStall",SubJenisStall.findById(params.subJenisStall.id))
            if (stallInstance?.namaProsesBP?.id=="-1"){
            }else {
                eq("namaProsesBP",NamaProsesBP.findById(params.namaProsesBP.id))
            }
            eq("m022NamaStall", params?.m022NamaStall.trim(), [ignoreCase: true])
            eq("m022Singkat", params?.m022Singkat.trim(), [ignoreCase: true])
            eq("m022StaTersedia",params.m022StaTersedia)
            eq("m022StaTPSLine",params.m022StaTPSLine)
            if(stallInstance?.m022StaTersedia=="0"){
                eq("m022TglJamAwal",params?.m022TglJamAwal)
                eq("m022TglJamAkhir",params?.m022TglJamAkhir)
            }
        }

        if(result){
            flash.message = "Data Sudah Ada"
            render(view: "create", model: [stallInstance: stallInstance])
            return
        }

        stallInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stallInstance?.lastUpdProcess = "INSERT"
        stallInstance?.companyDealer = session.userCompanyDealer
        stallInstance?.setStaDel('0')

        if (!stallInstance.save(flush: true)) {
            render(view: "create", model: [stallInstance: stallInstance])
            return
        }
        stallInstance?.m022StallID = stallInstance?.jenisStall?.id?.toString()+stallInstance?.subJenisStall?.id?.toString()+stallInstance?.id?.toString()
        if (!stallInstance.save(flush: true)) {
            render(view: "create", model: [stallInstance: stallInstance])
            return
        }
        flash.message = message(code: 'default.created.message', args: [message(code: 'stall.label', default: 'Stall'), stallInstance.id])
        redirect(action: "show", id: stallInstance.id)
    }

    def show(Long id) {
        def stallInstance = Stall.get(id)
        String m022StaTersedia = stallInstance?.m022StaTersedia == "0" ? "Tidak" : "Ya"
        String m022StaTPSLine = stallInstance?.m022StaTPSLine == "0" ? "Tidak" : "Ya"
        if (!stallInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'stall.label', default: 'Stall'), id])
            redirect(action: "list")
            return
        }

        [stallInstance: stallInstance, m022StaTersedia: m022StaTersedia, m022StaTPSLine: m022StaTPSLine]
    }

    def edit(Long id) {
        def stallInstance = Stall.get(id)
        if (!stallInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'stall.label', default: 'Stall'), id])
            redirect(action: "list")
            return
        }
        [stallInstance: stallInstance]
    }

    def update(Long id, Long version) {
        def stallInstance = Stall.get(id)
        if (!stallInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'stall.label', default: 'Stall'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (stallInstance.version > version) {

                stallInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'stall.label', default: 'Stall')] as Object[],
                        "Another user has updated this Stall while you were editing")
                render(view: "edit", model: [stallInstance: stallInstance])
                return
            }
        }

        if(params.m022StaTersedia?.toString()?.equals("0")){
            stallInstance?.m022TglJamAwal?.set(minute: Integer.parseInt(params?.m022TglJamAwal_minute), hourOfDay: Integer.parseInt(params?.m022TglJamAwal_hour))//.setHours(Integer.parseInt(params?.m022TglJamAwal_hour))
            stallInstance?.m022TglJamAkhir?.set(minute: Integer.parseInt(params?.m022TglJamAkhir_minute), hourOfDay: Integer.parseInt(params?.m022TglJamAkhir_hour))//.setHours(Integer.parseInt(params?.m022TglJamAkhir_hour))
            if(stallInstance?.m022TglJamAkhir?.before(stallInstance?.m022TglJamAwal)){
                flash.message = message(code: 'default.error.created.namastall.message', default: "Tanggal Jam Akhir kurang dari Tanggal Jam Awal.")
                render(view: "edit", model: [stallInstance: stallInstance])
                return
            }

        } else {
            stallInstance?.m022TglJamAwal=null
            stallInstance?.m022TglJamAkhir=null
        }

        if (params.namaProsesBP.id.equals("-1")){
            stallInstance?.namaProsesBP = null
        }else{
            stallInstance?.namaProsesBP = NamaProsesBP.findById(Long.parseLong(params.namaProsesBP.id))
        }
        def c = Stall.createCriteria()
        def result = c.list() {
            eq("staDel","0")
            eq("companyDealer",CompanyDealer.findById(params.companyDealer?.id))
            eq("jenisStall", JenisStall.findById(params?.jenisStall?.id))
            eq("lift", Lift.findById(params?.lift?.id))
            eq("subJenisStall",SubJenisStall?.findById(params?.subJenisStall?.id))
            if (params.namaProsesBP.id.equals("-1")){
                stallInstance?.namaProsesBP = null
            }else{
                eq("namaProsesBP",NamaProsesBP.findById(params?.namaProsesBP?.id))
            }
            eq("m022NamaStall",params?.m022NamaStall.trim(), [ignoreCase: true])
            eq("m022Singkat",params?.m022Singkat.trim(), [ignoreCase: true])
            eq("m022StaTersedia",params?.m022StaTersedia)
            eq("m022StaTPSLine",params?.m022StaTPSLine)
            if(params?.m022StaTersedia=="1"){
                eq("m022TglJamAwal",params?.m022TglJamAwal)
                eq("m022TglJamAkhir",params?.m022TglJamAkhir)
            }
        }

        if(result){
            String addId = "["+id+"]"
            if(addId!=result.id.toString()){
                flash.message = "Data Sudah Ada"
                render(view: "edit", model: [stallInstance: stallInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        stallInstance.properties = params
        if (params.namaProsesBP.id.equals("-1")){
            stallInstance?.namaProsesBP = null
        }
        stallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stallInstance?.lastUpdProcess = "UPDATE"
        stallInstance?.m022StallID = stallInstance?.jenisStall?.id?.toString()+stallInstance?.subJenisStall?.id?.toString()+stallInstance?.id?.toString()


        if (!stallInstance.save(flush: true)) {
            render(view: "edit", model: [stallInstance: stallInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'stall.label', default: 'Stall'), stallInstance.id])
        redirect(action: "show", id: stallInstance.id)
    }

    def delete(Long id) {
        def stallInstance = Stall.get(id)
        if (!stallInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'stall.label', default: 'Stall'), id])
            redirect(action: "list")
            return
        }

        try {
            stallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stallInstance?.lastUpdProcess = "DELETE"
            stallInstance?.setStaDel('1')
            stallInstance?.lastUpdated = datatablesUtilService?.syncTime()
            stallInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'stall.label', default: 'Stall'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'stall.label', default: 'Stall'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Stall, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(Stall, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }


}
