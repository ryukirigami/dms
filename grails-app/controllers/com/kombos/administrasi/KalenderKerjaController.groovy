package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class KalenderKerjaController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST", fetchIdByDate:"POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList', 'fetchIdByDate']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = KalenderKerja.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_m031ID"){
				ge("m031ID",params."sCriteria_m031ID")
				lt("m031ID",params."sCriteria_m031ID" + 1)
			}

			if(params."sCriteria_companyDealer"){
				eq("companyDealer",params."sCriteria_companyDealer")
			}
	
			if(params."sCriteria_m031StaLibur"){
				ilike("m031StaLibur","%" + (params."sCriteria_m031StaLibur" as String) + "%")
			}

			if(params."sCriteria_m031JamMulai1"){
				eq("m031JamMulai1",params."sCriteria_m031JamMulai1")
			}

			if(params."sCriteria_m031JamMulai2"){
				eq("m031JamMulai2",params."sCriteria_m031JamMulai2")
			}

			if(params."sCriteria_m031JamMulai3"){
				eq("m031JamMulai3",params."sCriteria_m031JamMulai3")
			}

			if(params."sCriteria_m031JamMulai4"){
				eq("m031JamMulai4",params."sCriteria_m031JamMulai4")
			}

			if(params."sCriteria_m031JamSelesai1"){
				eq("m031JamSelesai1",params."sCriteria_m031JamSelesai1")
			}

			if(params."sCriteria_m031JamSelesai2"){
				eq("m031JamSelesai2",params."sCriteria_m031JamSelesai2")
			}

			if(params."sCriteria_m031JamSelesai3"){
				eq("m031JamSelesai3",params."sCriteria_m031JamSelesai3")
			}

			if(params."sCriteria_m031JamSelesai4"){
				eq("m031JamSelesai4",params."sCriteria_m031JamSelesai4")
			}
	
			if(params."sCriteria_m031Ket"){
				ilike("m031Ket","%" + (params."sCriteria_m031Ket" as String) + "%")
			}
	
			if(params."sCriteria_m031XNamaUser"){
				ilike("m031XNamaUser","%" + (params."sCriteria_m031XNamaUser" as String) + "%")
			}
	
			if(params."sCriteria_m031XNamaDivisi"){
				ilike("m031XNamaDivisi","%" + (params."sCriteria_m031XNamaDivisi" as String) + "%")
			}
	
			if(params."sCriteria_m031StaDel"){
				ilike("m031StaDel","%" + (params."sCriteria_m031StaDel" as String) + "%")
			}

			if(params."sCriteria_createDtm"){
				ge("createDtm",params."sCriteria_createDtm")
				lt("createDtm",params."sCriteria_createDtm" + 1)
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}

			if(params."sCriteria_lastUpdDtm"){
				ge("lastUpdDtm",params."sCriteria_lastUpdDtm")
				lt("lastUpdDtm",params."sCriteria_lastUpdDtm" + 1)
			}
	
			if(params."sCriteria_lastUpdBy"){
				ilike("lastUpdBy","%" + (params."sCriteria_lastUpdBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m031ID: it.m031ID?it.m031ID.format(dateFormat):"",
			
						companyDealer: it.companyDealer.m011NamaWorkshop,
			
						m031StaLibur: it.m031StaLibur=='Y'?"Ya":"Tidak",
			
						m031JamMulai1: it.m031JamMulai1.format("HH:mm"),
			
						m031JamMulai2: it.m031JamMulai2.format("HH:mm"),
			
						m031JamMulai3: it.m031JamMulai3.format("HH:mm"),
			
						m031JamMulai4: it.m031JamMulai4.format("HH:mm"),
			
						m031JamSelesai1: it.m031JamSelesai1.format("HH:mm"),
			
						m031JamSelesai2: it.m031JamSelesai2.format("HH:mm"),
			
						m031JamSelesai3: it.m031JamSelesai3.format("HH:mm"),
			
						m031JamSelesai4: it.m031JamSelesai4.format("HH:mm"),
			
						m031Ket: it.m031Ket,
			
						m031XNamaUser: it.m031XNamaUser,
			
						m031XNamaDivisi: it.m031XNamaDivisi
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
        int jamMulai1 = 0
        String menitMulai1 = "00"
        int jamMulai2 = 0
        String menitMulai2 = "00"
        int jamMulai3 = 0
        String menitMulai3 = "00"
        int jamMulai4 = 0
        String menitMulai4 = "00"
        int jamSelesai1 = 0
        String menitSelesai1 = "00"
        int jamSelesai2 = 0
        String menitSelesai2= "00"
        int jamSelesai3 = 0
        String menitSelesai3= "00"
        int jamSelesai4 = 0
        String menitSelesai4= "00"
        def arrData = [:]
        arrData <<[
                jamMulai1:jamMulai1,
                menitMulai1:menitMulai1,
                jamMulai2:jamMulai2,
                menitMulai2:menitMulai2,
                jamMulai3:jamMulai3,
                menitMulai3:menitMulai3,
                jamMulai4:jamMulai4,
                menitMulai4:menitMulai4,
                jamSelesai1:jamSelesai1,
                menitSelesai1:menitSelesai1,
                jamSelesai2:jamSelesai2,
                menitSelesai2:menitSelesai2,
                jamSelesai3:jamSelesai3,
                menitSelesai3:menitSelesai3,
                jamSelesai4:jamSelesai4,
                menitSelesai4:menitSelesai4
        ]

		[kalenderKerjaInstance: new KalenderKerja(params),dataInstance: arrData]
	}

	def save() {
        def lastId

        String formatDatePicker = "dd/MM/yyyy"

        def startDate = new Date().parse(formatDatePicker, params.m031ID_start_dp)

        def endDate = new Date().parse(formatDatePicker, params.m031ID_end_dp)

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);

        Calendar end = Calendar.getInstance();
        end.setTime(endDate);


        if(start.after(end)){
            flash.message = message(code: 'default.periode_tanggal.message', default: 'Periode Tanggal Invalid From ' + params.m031ID_start_dp + ' to ' + params.m031ID_end_dp)
            render(view: "create", model: [kalenderKerjaInstance: new KalenderKerja()])
            return
        }

       while(!start.after(end)){
            Date targetDay = start.getTime();
            // Do Work Here

            def kalenderKerjaInstance = new KalenderKerja()
            kalenderKerjaInstance?.m031ID = new java.sql.Date(start.getTimeInMillis())
            kalenderKerjaInstance?.m031Ket = params.m031Ket?:'-'
            kalenderKerjaInstance?.companyDealer = session.userCompanyDealer
            kalenderKerjaInstance?.m031StaLibur = params.m031StaLibur
            kalenderKerjaInstance?.dateCreated = datatablesUtilService?.syncTime()
            kalenderKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()

            if(kalenderKerjaInstance.m031StaLibur.equals("Y")){
                params.m031JamMulai1_hour = "0"
                params.m031JamMulai2_hour = "0"
                params.m031JamMulai3_hour = "0"
                params.m031JamMulai4_hour = "0"

                params.m031JamMulai1_minute = "0"
                params.m031JamMulai2_minute = "0"
                params.m031JamMulai3_minute = "0"
                params.m031JamMulai4_minute = "0"

                params.m031JamSelesai1_hour = "0"
                params.m031JamSelesai2_hour = "0"
                params.m031JamSelesai3_hour = "0"
                params.m031JamSelesai4_hour = "0"

                params.m031JamSelesai1_minute = "0"
                params.m031JamSelesai2_minute = "0"
                params.m031JamSelesai3_minute = "0"
                params.m031JamSelesai4_minute = "0"
            }
            def jamMulai1 = params?.m031JamMulai1_hour + "." +params?.m031JamMulai1_minute
            def jamMulai2 = params?.m031JamMulai2_hour + "." +params?.m031JamMulai2_minute
            def jamMulai3 = params?.m031JamMulai3_hour + "." +params?.m031JamMulai3_minute
            def jamMulai4 = params?.m031JamMulai4_hour + "." +params?.m031JamMulai4_minute

            def jamSelesai1 = params?.m031JamSelesai1_hour + "." +params?.m031JamSelesai1_minute
            def jamSelesai2 = params?.m031JamSelesai2_hour + "." +params?.m031JamSelesai2_minute
            def jamSelesai3 = params?.m031JamSelesai3_hour + "." +params?.m031JamSelesai3_minute
            def jamSelesai4 = params?.m031JamSelesai4_hour + "." +params?.m031JamSelesai4_minute

            kalenderKerjaInstance?.m031JamMulai1 = new Double(jamMulai1)
            kalenderKerjaInstance?.m031JamMulai2 = new Double(jamMulai2)
            kalenderKerjaInstance?.m031JamMulai3 = new Double(jamMulai3)
            kalenderKerjaInstance?.m031JamMulai4 = new Double(jamMulai4)

            kalenderKerjaInstance?.m031JamSelesai1 = new Double(jamSelesai1)
            kalenderKerjaInstance?.m031JamSelesai2 = new Double(jamSelesai2)
            kalenderKerjaInstance?.m031JamSelesai3 = new Double(jamSelesai3)
            kalenderKerjaInstance?.m031JamSelesai4 = new Double(jamSelesai4)

            kalenderKerjaInstance?.staDel = "0"
            kalenderKerjaInstance?.m031XNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()

            String namaDivisi = "no division"
            def divisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi
            if(divisi){
                namaDivisi = divisi.m012NamaDivisi
            }
            kalenderKerjaInstance?.m031XNamaDivisi = namaDivisi

            kalenderKerjaInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kalenderKerjaInstance?.lastUpdProcess = "INSERT"
            if (!kalenderKerjaInstance?.save()) {
                render(view: "create", model: [kalenderKerjaInstance: kalenderKerjaInstance])
                return
            }
            kalenderKerjaInstance.errors.each {
                it
            }

            lastId = kalenderKerjaInstance.getId()

            start.add(Calendar.DATE, 1);
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), lastId])
		redirect(action: "show", id: lastId)
	}

	def show(Long id) {
		def kalenderKerjaInstance = KalenderKerja.get(id)
		if (!kalenderKerjaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), id])
			redirect(action: "list")
			return
		}

		[kalenderKerjaInstance: kalenderKerjaInstance]
	}

	def edit(Long id) {
		def kalenderKerjaInstance = KalenderKerja.get(id)
		if (!kalenderKerjaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), id])
			redirect(action: "list")
			return
		}
            String dataMulai1 = kalenderKerjaInstance?.m031JamMulai1;
            int jamMulai1 = 0;
            String menitMulai1 = "00";
            def arrData = [:]
            if(dataMulai1.contains('.')){
                def arrDataMulai1 = dataMulai1.split("\\.");
                jamMulai1 = Integer.parseInt(arrDataMulai1[0].toString());
                menitMulai1 = arrDataMulai1[1].toString();
                if(menitMulai1.length()<2){
                    menitMulai1 = menitMulai1 + "0"
                }
            }else{
                jamMulai1 = Integer.parseInt(dataMulai1);
            }

            String dataMulai2 = kalenderKerjaInstance?.m031JamMulai2;
            int jamMulai2 = 0;
            String menitMulai2 = "00";
            if(dataMulai2.contains('.')){
                def arrDataMulai2 = dataMulai2.split("\\.");
                jamMulai2 = Integer.parseInt(arrDataMulai2[0].toString());
                menitMulai2 = arrDataMulai2[1].toString();
                if(menitMulai2.length()<2){
                    menitMulai2 = menitMulai2 + "0"
                }
            }else{
                jamMulai2 = Integer.parseInt(dataMulai2);
            }

            String dataMulai3 = kalenderKerjaInstance?.m031JamMulai3;
            int jamMulai3 = 0;
            String menitMulai3 = "00";
            if(dataMulai3.contains('.')){
                def arrDataMulai3 = dataMulai3.split("\\.");
                jamMulai3 = Integer.parseInt(arrDataMulai3[0].toString());
                menitMulai3 = arrDataMulai3[1].toString();
                if(menitMulai3.length()<2){
                    menitMulai3 = menitMulai3 + "0"
                }
            }else{
                jamMulai3 = Integer.parseInt(dataMulai3);
            }

            String dataMulai4 = kalenderKerjaInstance?.m031JamMulai4;
            int jamMulai4 = 0;
            String menitMulai4 = "00";
            if(dataMulai4.contains('.')){
                def arrDataMulai4 = dataMulai4.split("\\.");
                jamMulai4 = Integer.parseInt(arrDataMulai4[0].toString());
                menitMulai4 = arrDataMulai4[1].toString();
                if(menitMulai4.length()<2){
                    menitMulai4 = menitMulai4 + "0"
                }
            }else{
                jamMulai4 = Integer.parseInt(dataMulai4);
            }

            String dataSelesai1 = kalenderKerjaInstance?.m031JamSelesai1;
            int jamSelesai1 = 0;
            String menitSelesai1 = "00";
            if(dataSelesai1.contains('.')){
                def arrDataSelesai1 = dataSelesai1.split("\\.");
                jamSelesai1 = Integer.parseInt(arrDataSelesai1[0].toString());
                menitSelesai1 = arrDataSelesai1[1].toString();
                if(menitSelesai1.length()<2){
                    menitSelesai1 = menitSelesai1 + "0"
                }
            }else{
                jamSelesai1 = Integer.parseInt(jamSelesai1);
            }

            String dataSelesai2 = kalenderKerjaInstance?.m031JamSelesai2;
            int jamSelesai2 = 0;
            String menitSelesai2 = "00";
            if(dataSelesai2.contains('.')){
                def arrDataSelesai2 = dataSelesai2.split("\\.");
                jamSelesai2 = Integer.parseInt(arrDataSelesai2[0].toString());
                menitSelesai2 = arrDataSelesai2[1].toString();
                if(menitSelesai2.length()<2){
                    menitSelesai2 = menitSelesai2 + "0"
                }
            }else{
                jamSelesai2 = Integer.parseInt(jamSelesai2);
            }


            String dataSelesai3 = kalenderKerjaInstance?.m031JamSelesai3;
            int jamSelesai3 = 0;
            String menitSelesai3 = "00";
            if(dataSelesai3.contains('.')){
                def arrDataSelesai3 = dataSelesai3.split("\\.");
                jamSelesai3 = Integer.parseInt(arrDataSelesai3[0].toString());
                menitSelesai3 = arrDataSelesai3[1].toString();
                if(menitSelesai3.length()<2){
                    menitSelesai3 = menitSelesai3 + "0"
                }
            }else{
                jamSelesai3 = Integer.parseInt(jamSelesai3);
            }

            String dataSelesai4 = kalenderKerjaInstance?.m031JamSelesai4;
            int jamSelesai4 = 0;
            String menitSelesai4 = "00";
            if(dataSelesai4.contains('.')){
                def arrDataSelesai4 = dataSelesai4.split("\\.");
                jamSelesai4 = Integer.parseInt(arrDataSelesai4[0].toString());
                menitSelesai4 = arrDataSelesai4[1].toString();
                if(menitSelesai4.length()<2){
                    menitSelesai4 = menitSelesai4 + "0"
                }
            }else{
                jamSelesai4 = Integer.parseInt(jamSelesai4);
            }

            arrData <<[
                    jamMulai1:jamMulai1,
                    menitMulai1:menitMulai1,
                    jamMulai2:jamMulai2,
                    menitMulai2:menitMulai2,
                    jamMulai3:jamMulai3,
                    menitMulai3:menitMulai3,
                    jamMulai4:jamMulai4,
                    menitMulai4:menitMulai4,
                    jamSelesai1:jamSelesai1,
                    menitSelesai1:menitSelesai1,
                    jamSelesai2:jamSelesai2,
                    menitSelesai2:menitSelesai2,
                    jamSelesai3:jamSelesai3,
                    menitSelesai3:menitSelesai3,
                    jamSelesai4:jamSelesai4,
                    menitSelesai4:menitSelesai4
            ]

		[kalenderKerjaInstance: kalenderKerjaInstance,dataInstance:arrData]
	}

	def update(Long id, Long version) {
		def kalenderKerjaInstance = KalenderKerja.get(id)

		if (!kalenderKerjaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (kalenderKerjaInstance.version > version) {
				
				kalenderKerjaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'kalenderKerja.label', default: 'KalenderKerja')] as Object[],
				"Another user has updated this KalenderKerja while you were editing")
				render(view: "edit", model: [kalenderKerjaInstance: kalenderKerjaInstance])
				return
			}
		}

        kalenderKerjaInstance?.m031Ket = params.m031Ket
        kalenderKerjaInstance?.companyDealer = session.userCompanyDealer
        kalenderKerjaInstance?.m031StaLibur = params.m031StaLibur
        kalenderKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if(kalenderKerjaInstance.m031StaLibur.equals("Y")){
            params.m031JamMulai1_hour = "0"
            params.m031JamMulai2_hour = "0"
            params.m031JamMulai3_hour = "0"
            params.m031JamMulai4_hour = "0"

            params.m031JamMulai1_minute = "0"
            params.m031JamMulai2_minute = "0"
            params.m031JamMulai3_minute = "0"
            params.m031JamMulai4_minute = "0"

            params.m031JamSelesai1_hour = "0"
            params.m031JamSelesai2_hour = "0"
            params.m031JamSelesai3_hour = "0"
            params.m031JamSelesai4_hour = "0"

            params.m031JamSelesai1_minute = "0"
            params.m031JamSelesai2_minute = "0"
            params.m031JamSelesai3_minute = "0"
            params.m031JamSelesai4_minute = "0"
        }
        def jamMulai1 = params?.m031JamMulai1_hour + "." +params?.m031JamMulai1_minute
        def jamMulai2 = params?.m031JamMulai2_hour + "." +params?.m031JamMulai2_minute
        def jamMulai3 = params?.m031JamMulai3_hour + "." +params?.m031JamMulai3_minute
        def jamMulai4 = params?.m031JamMulai4_hour + "." +params?.m031JamMulai4_minute

        def jamSelesai1 = params?.m031JamSelesai1_hour + "." +params?.m031JamSelesai1_minute
        def jamSelesai2 = params?.m031JamSelesai2_hour + "." +params?.m031JamSelesai2_minute
        def jamSelesai3 = params?.m031JamSelesai3_hour + "." +params?.m031JamSelesai3_minute
        def jamSelesai4 = params?.m031JamSelesai4_hour + "." +params?.m031JamSelesai4_minute

        kalenderKerjaInstance?.m031JamMulai1 = new Double(jamMulai1)
        kalenderKerjaInstance?.m031JamMulai2 = new Double(jamMulai2)
        kalenderKerjaInstance?.m031JamMulai3 = new Double(jamMulai3)
        kalenderKerjaInstance?.m031JamMulai4 = new Double(jamMulai4)

        kalenderKerjaInstance?.m031JamSelesai1 = new Double(jamSelesai1)
        kalenderKerjaInstance?.m031JamSelesai2 = new Double(jamSelesai2)
        kalenderKerjaInstance?.m031JamSelesai3 = new Double(jamSelesai3)
        kalenderKerjaInstance?.m031JamSelesai4 = new Double(jamSelesai4)

        kalenderKerjaInstance?.staDel = "0"
        kalenderKerjaInstance?.m031XNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()

        String namaDivisi = "no division"
        def divisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi
        if(divisi){
            namaDivisi = divisi.m012NamaDivisi
        }
        kalenderKerjaInstance?.m031XNamaDivisi = namaDivisi

        kalenderKerjaInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        kalenderKerjaInstance?.lastUpdProcess = "INSERT"

		if (!kalenderKerjaInstance.save(flush: true)) {
			render(view: "edit", model: [kalenderKerjaInstance: kalenderKerjaInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), kalenderKerjaInstance.id])
		redirect(action: "show", id: kalenderKerjaInstance.id)
	}

	def delete(Long id) {
		def kalenderKerjaInstance = KalenderKerja.get(id)
		if (!kalenderKerjaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), id])
			redirect(action: "list")
			return
		}

		try {
			kalenderKerjaInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'kalenderKerja.label', default: 'KalenderKerja'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(KalenderKerja, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(KalenderKerja, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	def fetchIdByDate(){
        def id = 0

        def startDate = new Date().parse("MM/dd/yyyy", params.inputDate)

        Calendar start = Calendar.getInstance();
        start.setTime(startDate);

        def kalenderKerja = KalenderKerja.findByM031IDAndStaDel(new java.sql.Date(start.getTimeInMillis()),'0')

        if(kalenderKerja){
            id = kalenderKerja.getId()
        }

        render id
    }

    def fetchAllData(){
        def rows = []

        def kalenderKerjas = KalenderKerja.findAllByStaDel('0')
        if(kalenderKerjas){
            String staLibur = "X"
            kalenderKerjas.each {
                staLibur = it.m031StaLibur=="Y"?"X":" "
                rows << [ "${it.m031ID.format("MM-dd-yyyy")}" : "${staLibur}"]
            }
        }
        render rows as JSON
    }
}
