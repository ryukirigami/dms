package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class KecamatanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def kecamatanService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render kecamatanService.datatablesList(params) as JSON
    }

    def create() {
        def result = kecamatanService.create(params)

        if (!result.error)
            return [kecamatanInstance: result.kecamatanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = kecamatanService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Kecamatan", result.kecamatanInstance.id])
            redirect(action: 'show', id: result.kecamatanInstance.id)
            return
        }else if(result.error.code == "dataGanda"){
            flash.message = "Data Sudah Ada"
            render(view:'create', model:[kecamatanInstance: result.kecamatanInstance])
            return
        }

        render(view: 'create', model: [kecamatanInstance: result.kecamatanInstance])
    }

    def show(Long id) {
        def result = kecamatanService.show(params)

        if (!result.error)
            return [kecamatanInstance: result.kecamatanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = kecamatanService.show(params)

        if (!result.error)
            return [kecamatanInstance: result.kecamatanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = kecamatanService.update(params)
        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Kecamatan", params.id])
            redirect(action: 'show', id: params.id)
            return
        }else if(result.error.code == "dataGanda"){
            flash.message = "Data Sudah Ada"
            render(view:'edit', model:[kecamatanInstance: result.kecamatanInstance])
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [kecamatanInstance: result.kecamatanInstance.attach()])
    }

    def delete() {
        def result = kecamatanService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Kecamatan", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Kecamatan, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
