package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class TemporaryJobController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Operation.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("m053Ket", "TEMPORARY")
            eq("staDel", "0")

            if (params."sCriteria_m053JobsId") {
                ilike("m053JobsId", "%" + (params."sCriteria_m053JobsId" as String) + "%")
            }

            if (params."sCriteria_m053NamaOperation") {
                ilike("m053NamaOperation", "%" + (params."sCriteria_m053NamaOperation" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m053JobsId: it.m053JobsId,

                    m053NamaOperation: it.m053NamaOperation,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [operationInstance: new Operation(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def operationInstance = new Operation(params)
        operationInstance?.m053JobsId=generateCodeService.codeGenerateSequence('M053_JobID',null)
        operationInstance?.m053NamaOperation=params.m053NamaOperation
        operationInstance?.m053Ket="TEMPORARY"
        operationInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        operationInstance?.lastUpdProcess = "INSERT"
        operationInstance?.setStaDel('0')
        def cek = Operation.createCriteria().list() {
            and{
                eq("m053NamaOperation",operationInstance.m053NamaOperation?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [operationInstance: operationInstance])
            return
        }
        if (!operationInstance.save(flush: true)) {
            render(view: "create", model: [operationInstance: operationInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'operation.label', default: 'Temporary Job'), operationInstance.id])
        redirect(action: "show", id: operationInstance.id)
    }

    def show(Long id) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'TemporaryJob'), id])
            redirect(action: "list")
            return
        }

        [operationInstance: operationInstance]
    }

    def edit(Long id) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'TemporaryJob'), id])
            redirect(action: "list")
            return
        }

        [operationInstance: operationInstance]
    }

    def update(Long id, Long version) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'TemporaryJob'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (operationInstance.version > version) {

                operationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'operation.label', default: 'TemporaryJob')] as Object[],
                        "Another user has updated this Operation while you were editing")
                render(view: "edit", model: [operationInstance: operationInstance])
                return
            }
        }

        def cek = Operation.createCriteria()
        def result = cek.list() {
            and{
                eq("m053NamaOperation",params.m053NamaOperation?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [operationInstance: operationInstance])
            return
        }

        operationInstance.properties = params
        operationInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        operationInstance?.lastUpdProcess = "UPDATE"

        if (!operationInstance.save(flush: true)) {
            render(view: "edit", model: [operationInstance: operationInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'operation.label', default: 'Temporary Job'), operationInstance.id])
        redirect(action: "show", id: operationInstance.id)
    }

    def delete(Long id) {
        def operationInstance = Operation.get(id)
        if (!operationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'operation.label', default: 'TemporaryJob'), id])
            redirect(action: "list")
            return
        }

        try {
            operationInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            operationInstance?.lastUpdProcess = "DELETE"
            operationInstance?.setStaDel('1')
            operationInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'operation.label', default: 'TemporaryJob'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'operation.label', default: 'TemporaryJob'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Operation, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Operation, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}
