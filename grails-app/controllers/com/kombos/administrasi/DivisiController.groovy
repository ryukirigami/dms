package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class DivisiController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = Divisi.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel", "0")
            companyDealer{
                eq("staDel", "0")
            }
			if(params."sCriteria_companyDealer"){
//				eq("companyDealer", CompanyDealer.findByM011NamaWorkshop(params."sCriteria_companyDealer"))
                companyDealer{
                    ilike("m011NamaWorkshop","%"+(params."sCriteria_companyDealer" as String)+"%")
                }
			}
	
			if(params."sCriteria_m012ID"){
				ilike("m012ID","%" + (params."sCriteria_m012ID" as String) + "%")
			}
	
			if(params."sCriteria_m012NamaDivisi"){
				ilike("m012NamaDivisi","%" + (params."sCriteria_m012NamaDivisi" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			
			switch(sortProperty){
                case "companyDealer":
                    companyDealer{
                        order("m011NamaWorkshop",sortDir)
                    }
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companyDealer: it.companyDealer?.m011NamaWorkshop,
			
						m012ID: it.m012ID,
			
						m012NamaDivisi: it.m012NamaDivisi,
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[divisiInstance: new Divisi(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def divisiInstance = new Divisi(params)
        divisiInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        divisiInstance?.lastUpdProcess = "INSERT"
		divisiInstance?.setStaDel('0')
        def cek = Divisi.createCriteria().list() {
            and{
                eq("companyDealer",divisiInstance.companyDealer)
                eq("m012NamaDivisi",divisiInstance.m012NamaDivisi?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [divisiInstance: divisiInstance])
            return
        }

        if (!divisiInstance.save(flush: true)) {
			render(view: "create", model: [divisiInstance: divisiInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'divisi.label', default: 'Divisi'), divisiInstance.id])
		redirect(action: "show", id: divisiInstance.id)
	}

	def show(Long id) {
		def divisiInstance = Divisi.get(id)
		if (!divisiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'divisi.label', default: 'Divisi'), id])
			redirect(action: "list")
			return
		}

		[divisiInstance: divisiInstance]
	}

	def edit(Long id) {
		def divisiInstance = Divisi.get(id)
		if (!divisiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'divisi.label', default: 'Divisi'), id])
			redirect(action: "list")
			return
		}

		[divisiInstance: divisiInstance]
	}

	def update(Long id, Long version) {
		def divisiInstance = Divisi.get(id)
		if (!divisiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'divisi.label', default: 'Divisi'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (divisiInstance.version > version) {
				
				divisiInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'divisi.label', default: 'Divisi')] as Object[],
				"Another user has updated this Divisi while you were editing")
				render(view: "edit", model: [divisiInstance: divisiInstance])
				return
			}
		}

        def cek = Divisi.createCriteria()
        def result = cek.list() {
            and{
                eq("companyDealer",CompanyDealer.findById(params.companyDealer?.id))
                eq("m012NamaDivisi",params.m012NamaDivisi?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [divisiInstance: divisiInstance])
            return
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        divisiInstance.properties = params
        divisiInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        divisiInstance?.lastUpdProcess = "UPDATE"

		if (!divisiInstance.save(flush: true)) {
			render(view: "edit", model: [divisiInstance: divisiInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'divisi.label', default: 'Divisi'), divisiInstance.id])
		redirect(action: "show", id: divisiInstance.id)
	}

	def delete(Long id) {
		def divisiInstance = Divisi.get(id)
		if (!divisiInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'divisi.label', default: 'Divisi'), id])
			redirect(action: "list")
			return
		}

		try {
            divisiInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            divisiInstance?.lastUpdProcess = "DELETE"
			divisiInstance?.setStaDel('1')
            divisiInstance?.lastUpdated = datatablesUtilService?.syncTime()
            divisiInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'divisi.label', default: 'Divisi'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'divisi.label', default: 'Divisi'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Divisi, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Divisi, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
