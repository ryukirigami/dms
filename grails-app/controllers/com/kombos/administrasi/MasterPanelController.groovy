package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MasterPanelController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = MasterPanel.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m094NamaPanel") {
                ilike("m094NamaPanel", "%" + (params."sCriteria_m094NamaPanel" as String) + "%")
            }

            if (params."sCriteria_m094ID") {
                eq("m094ID", params."sCriteria_m094ID")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m094NamaPanel: it.m094NamaPanel,

                    m094ID: it.m094ID,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [masterPanelInstance: new MasterPanel(params)]
    }

    def save() {
        def masterPanelInstance = new MasterPanel(params)
        masterPanelInstance?.m094ID = generateCodeService?.codeGenerateSequence('M094_ID',null)
        masterPanelInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        masterPanelInstance?.lastUpdProcess = "INSERT"
        def cek = MasterPanel.createCriteria().list() {
            and{
                eq("m094NamaPanel",masterPanelInstance.m094NamaPanel?.trim(), [ignoreCase: true])
            }
        }
        if(cek){
            flash.message = "Nama Panel sudah ada"
            render(view: "create", model: [masterPanelInstance: masterPanelInstance])
            return
        }
        if (!masterPanelInstance.save(flush: true)) {
            render(view: "create", model: [masterPanelInstance: masterPanelInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'masterPanel.label', default: 'Panel'), masterPanelInstance.id])
        redirect(action: "show", id: masterPanelInstance.id)
    }

    def show(Long id) {
        def masterPanelInstance = MasterPanel.get(id)
        if (!masterPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterPanel.label', default: 'Panel'), id])
            redirect(action: "list")
            return
        }

        [masterPanelInstance: masterPanelInstance]
    }

    def edit(Long id) {
        def masterPanelInstance = MasterPanel.get(id)
        if (!masterPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterPanel.label', default: 'Panel'), id])
            redirect(action: "list")
            return
        }

        [masterPanelInstance: masterPanelInstance]
    }

    def update(Long id, Long version) {
        def masterPanelInstance = MasterPanel.get(id)
        if (!masterPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterPanel.label', default: 'Panel'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (masterPanelInstance.version > version) {

                masterPanelInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'masterPanel.label', default: 'Panel')] as Object[],
                        "Another user has updated this Panel while you were editing")
                render(view: "edit", model: [masterPanelInstance: masterPanelInstance])
                return
            }
        }

        def cek = MasterPanel.createCriteria()
        def result = cek.list() {
            and{
                eq("m094NamaPanel",params.m094NamaPanel?.trim(), [ignoreCase: true])
            }
        }
        if(result){
            flash.message = "Nama Panel sudah ada"
            render(view: "edit", model: [masterPanelInstance: masterPanelInstance])
            return
        }

        masterPanelInstance.properties = params
        masterPanelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        masterPanelInstance?.lastUpdProcess = "UPDATE"

        if (!masterPanelInstance.save(flush: true)) {
            render(view: "edit", model: [masterPanelInstance: masterPanelInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'masterPanel.label', default: 'Panel'), masterPanelInstance.id])
        redirect(action: "show", id: masterPanelInstance.id)
    }

    def delete(Long id) {
        def masterPanelInstance = MasterPanel.get(id)
        if (!masterPanelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterPanel.label', default: 'Panel'), id])
            redirect(action: "list")
            return
        }

        try {
            masterPanelInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'masterPanel.label', default: 'Panel'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'masterPanel.label', default: 'Panel'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MasterPanel, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(MasterPanel, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
