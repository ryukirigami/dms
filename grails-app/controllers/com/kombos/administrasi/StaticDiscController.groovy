package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StaticDiscController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = StaticDisc.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_companydealer"){
				eq("companydealer",params."sCriteria_companydealer")
			}

			if(params."sCriteria_m019TMT"){
				ge("m019TMT",params."sCriteria_m019TMT")
				lt("m019TMT",params."sCriteria_m019TMT" + 1)
			}

			if(params."sCriteria_m019BookingSBJasa"){
				eq("m019BookingSBJasa",params."sCriteria_m019BookingSBJasa")
			}

			if(params."sCriteria_m019BookingSBPart"){
				eq("m019BookingSBPart",params."sCriteria_m019BookingSBPart")
			}

			if(params."sCriteria_m019BookingGRBPJasa"){
				eq("m019BookingGRBPJasa",params."sCriteria_m019BookingGRBPJasa")
			}

			if(params."sCriteria_m019BookingGRBPPart"){
				eq("m019BookingGRBPPart",params."sCriteria_m019BookingGRBPPart")
			}

			if(params."sCriteria_m019NonBookingSBJasa"){
				eq("m019NonBookingSBJasa",params."sCriteria_m019NonBookingSBJasa")
			}

			if(params."sCriteria_m019NonBookingSBPart"){
				eq("m019NonBookingSBPart",params."sCriteria_m019NonBookingSBPart")
			}

			if(params."sCriteria_m019NonBookingGRBPJasa"){
				eq("m019NonBookingGRBPJasa",params."sCriteria_m019NonBookingGRBPJasa")
			}

			if(params."sCriteria_m019NonBookingGRBPPart"){
				eq("m019NonBookingGRBPPart",params."sCriteria_m019NonBookingGRBPPart")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			eq("m019StaDel", '0')
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companydealer: it.companydealer,
			
						m019TMT: it.m019TMT?it.m019TMT.format(dateFormat):"",
			
						m019BookingSBJasa: it.m019BookingSBJasa,
			
						m019BookingSBPart: it.m019BookingSBPart,
			
						m019BookingGRBPJasa: it.m019BookingGRBPJasa,
			
						m019BookingGRBPPart: it.m019BookingGRBPPart,
			
						m019NonBookingSBJasa: it.m019NonBookingSBJasa,
			
						m019NonBookingSBPart: it.m019NonBookingSBPart,
			
						m019NonBookingGRBPJasa: it.m019NonBookingGRBPJasa,
			
						m019NonBookingGRBPPart: it.m019NonBookingGRBPPart,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[staticDiscInstance: new StaticDisc(params)]
	}

	def save() {
        CompanyDealer companyDealer = session.userCompanyDealer


		def staticDiscInstance = new StaticDisc(params)
        staticDiscInstance.companydealer = companyDealer

        if(params.m019TMT){
            StaticDisc cek = StaticDisc.findByCompanydealerAndM019TMTAndM019StaDel(companyDealer, params.m019TMT, '0')
            if(cek){
                flash.message = message(code: 'staticDisc.m019TMT.unique')
                render(view: "create", model: [staticDiscInstance: staticDiscInstance])
                return
            }
        }

        if (!staticDiscInstance.save(flush: true)) {
			render(view: "create", model: [staticDiscInstance: staticDiscInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), staticDiscInstance.id])
		redirect(action: "show", id: staticDiscInstance.id)
	}

	def show(Long id) {
		def staticDiscInstance = StaticDisc.get(id)
		if (!staticDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), id])
			redirect(action: "list")
			return
		}

		[staticDiscInstance: staticDiscInstance]
	}

	def edit(Long id) {
		def staticDiscInstance = StaticDisc.get(id)
		if (!staticDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), id])
			redirect(action: "list")
			return
		}

		[staticDiscInstance: staticDiscInstance]
	}

	def update(Long id, Long version) {
		def staticDiscInstance = StaticDisc.get(id)
		if (!staticDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (staticDiscInstance.version > version) {
				
				staticDiscInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'staticDisc.label', default: 'StaticDisc')] as Object[],
				"Another user has updated this StaticDisc while you were editing")
				render(view: "edit", model: [staticDiscInstance: staticDiscInstance])
				return
			}
		}

        if(params.m019TMT){
            StaticDisc cek = StaticDisc.findByCompanydealerAndM019TMTAndIdNotEqualAndM019StaDel(companyDealer, params.m019TMT, id, '0')
            if(cek){
                flash.message = message(code: 'staticDisc.m019TMT.unique')
                render(view: "create", model: [staticDiscInstance: staticDiscInstance])
                return
            }
        }

		staticDiscInstance.properties = params

		if (!staticDiscInstance.save(flush: true)) {
			render(view: "edit", model: [staticDiscInstance: staticDiscInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), staticDiscInstance.id])
		redirect(action: "show", id: staticDiscInstance.id)
	}

	def delete(Long id) {
		def staticDiscInstance = StaticDisc.get(id)
		if (!staticDiscInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), id])
			redirect(action: "list")
			return
		}

		try {
//			staticDiscInstance.delete(flush: true)
            staticDiscInstance.m019StaDel = "1"
            if (!staticDiscInstance.save(flush: true)) {
                flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), id])
                redirect(action: "show", id: id)
                return
            }
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'staticDisc.label', default: 'StaticDisc'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			//datatablesUtilService.massDelete(StaticDisc, params)
            //log.info(params.ids)
            def jsonArray = JSON.parse(params.ids)

            jsonArray.each {
                def staticDiscInstance = StaticDisc.get(it)

                staticDiscInstance.m019StaDel = "1"
                //namaDokumenInstance.delete(flush: true)
                staticDiscInstance.save(flush: true)

            }

            res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(StaticDisc, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
