package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ServiceGratisController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", tambah: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def tambah(){
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Operation.get(it)  }
        def service = null, ceks = null
        oList.each{
            service = new ServiceGratis()
            service?.operation = it
            service?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            service?.lastUpdProcess = "INSERT"
            ceks = ServiceGratis.findByOperation(it)
            if(!ceks){
                service.save()
            }else{
                render "ada"
            }

        }
        render "ok"
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = ServiceGratis.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_operation") {
                operation{
                    ilike("m053Id", "%" + (params."sCriteria_operation" as String) + "%")
                }
            }

            if (params."sCriteria_namaOperation") {
                operation{
                    ilike("m053NamaOperation", "%" + (params."sCriteria_namaOperation" as String) + "%")
                }
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            switch (sortProperty) {
                case "operation":
                    operation{
                        order("m053Id",sortDir)
                    }
                    break;
                case "namaoperation":
                    operation{
                        order("m053NamaOperation",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.operation.id,

                    operation: it.operation.m053Id,

                    namaOperation: it.operation.m053NamaOperation,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [serviceGratisInstance: new ServiceGratis(params)]
    }

    def save() {
        def serviceGratisInstance = new ServiceGratis(params)
        if (!serviceGratisInstance.save(flush: true)) {
            render(view: "create", model: [serviceGratisInstance: serviceGratisInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), serviceGratisInstance.id])
        redirect(action: "show", id: serviceGratisInstance.id)
    }

    def show(Long id) {
        def serviceGratisInstance = ServiceGratis.get(id)
        if (!serviceGratisInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), id])
            redirect(action: "list")
            return
        }

        [serviceGratisInstance: serviceGratisInstance]
    }

    def edit(Long id) {
        def serviceGratisInstance = ServiceGratis.get(id)
        if (!serviceGratisInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), id])
            redirect(action: "list")
            return
        }

        [serviceGratisInstance: serviceGratisInstance]
    }

    def update(Long id, Long version) {
        def serviceGratisInstance = ServiceGratis.get(id)
        if (!serviceGratisInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (serviceGratisInstance.version > version) {

                serviceGratisInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'serviceGratis.label', default: 'ServiceGratis')] as Object[],
                        "Another user has updated this ServiceGratis while you were editing")
                render(view: "edit", model: [serviceGratisInstance: serviceGratisInstance])
                return
            }
        }

        serviceGratisInstance.properties = params

        if (!serviceGratisInstance.save(flush: true)) {
            render(view: "edit", model: [serviceGratisInstance: serviceGratisInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), serviceGratisInstance.id])
        redirect(action: "show", id: serviceGratisInstance.id)
    }

    def delete(Long id) {
        def serviceGratisInstance = ServiceGratis.get(id)
        if (!serviceGratisInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), id])
            redirect(action: "list")
            return
        }

        try {
            serviceGratisInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'serviceGratis.label', default: 'ServiceGratis'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        log.info(params.ids)
        try {
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            jsonArray.each { oList << ServiceGratis.findByOperation(Operation.get(it))  }
            oList.each{ it.delete() }
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(ServiceGratis, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
