package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class TargetBPController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def conversi =new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = TargetBP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m036TglBerlaku") {
                ge("m036TglBerlaku", params."sCriteria_m036TglBerlaku")
                lt("m036TglBerlaku", params."sCriteria_m036TglBerlaku" + 1)
            }

            if (params."sCriteria_companyDealer") {

                    def dealers = CompanyDealer.findAllByM011NamaWorkshopIlike("%" + (params."sCriteria_companyDealer" as String) + "%")
                    if(dealers.size() != 0) {
                        dealers.each {
                            CompanyDealer dealer = it
                            or {
                                eq("companyDealer", dealer)
                            }
                        }
                    } else {
                        eq("companyDealer", null)
                    }
            }

            if (params."sCriteria_m036COGSDeprecation") {
                eq("m036COGSDeprecation", (params."sCriteria_m036COGSDeprecation" as Double))
            }

            if (params."sCriteria_m036COGSLabor") {
                eq("m036COGSLabor", (params."sCriteria_m036COGSLabor" as Double))
            }

            if (params."sCriteria_m036COGSOil") {
                eq("m036COGSOil", (params."sCriteria_m036COGSOil" as Double))
            }

            if (params."sCriteria_m036COGSOverHead") {
                eq("m036COGSOverHead", (params."sCriteria_m036COGSOverHead" as Double))
            }

            if (params."sCriteria_m036COGSPart") {
                eq("m036COGSPart", (params."sCriteria_m036COGSPart" as Double))
            }

            if (params."sCriteria_m036COGSSublet") {
                eq("m036COGSSublet", (params."sCriteria_m036COGSSublet" as Double))
            }

            if (params."sCriteria_m036COGSTotalSGA") {
                eq("m036COGSTotalSGA", (params."sCriteria_m036COGSTotalSGA" as Double))
            }

            if (params."sCriteria_m036Q1D") {
                eq("m036Q1D", (params."sCriteria_m036Q1D" as Double))
            }

            if (params."sCriteria_m036Q1S") {
                eq("m036Q1S", (params."sCriteria_m036Q1S" as Double))
            }

            if (params."sCriteria_m036Q2D") {
                eq("m036Q2D", (params."sCriteria_m036Q2D" as Double))
            }

            if (params."sCriteria_m036Q2S") {
                eq("m036Q2S", (params."sCriteria_m036Q2S" as Double))
            }

            if (params."sCriteria_m036Q3D") {
                eq("m036Q3D", (params."sCriteria_m036Q3D" as Double))
            }

            if (params."sCriteria_m036Q3S") {
                eq("m036Q3S", (params."sCriteria_m036Q3S" as Double))
            }

            if (params."sCriteria_m036Q4D") {
                eq("m036Q4D", (params."sCriteria_m036Q4D" as Double))
            }

            if (params."sCriteria_m036Q4S") {
                eq("m036Q4S", (params."sCriteria_m036Q4S" as Double))
            }

            if (params."sCriteria_m036Q5D") {
                eq("m036Q5D", (params."sCriteria_m036Q5D" as Double))
            }

            if (params."sCriteria_m036Q5S") {
                eq("m036Q5S", (params."sCriteria_m036Q5S" as Double))
            }

            if (params."sCriteria_m036Q6D") {
                eq("m036Q6D", (params."sCriteria_m036Q6D" as Double))
            }

            if (params."sCriteria_m036Q6S") {
                eq("m036Q6S", (params."sCriteria_m036Q6S" as Double))
            }

            if (params."sCriteria_m036TWCLeadTime") {
                eq("m036TWCLeadTime", (params."sCriteria_m036TWCLeadTime" as Double))
            }

            if (params."sCriteria_m036TargetQty") {
                eq("m036TargetQty", (params."sCriteria_m036TargetQty" as Double))
            }

            if (params."sCriteria_m036TargetSales") {
                eq("m036TargetSales", (params."sCriteria_m036TargetSales" as Double))
            }

            if (params."sCriteria_m036TechReport") {
                eq("m036TechReport", (params."sCriteria_m036TechReport" as Double))
            }

            if (params."sCriteria_m036ThisMonthD") {
                eq("m036ThisMonthD", (params."sCriteria_m036ThisMonthD" as Double))
            }

            if (params."sCriteria_m036ThisMonthS") {
                eq("m036ThisMonthS", (params."sCriteria_m036ThisMonthS" as Double))
            }

            if (params."sCriteria_m036TotalClaimAmount") {
                eq("m036TotalClaimAmount", (params."sCriteria_m036TotalClaimAmount" as Double))
            }

            if (params."sCriteria_m036TotalClaimItem") {
                eq("m036TotalClaimItem", (params."sCriteria_m036TotalClaimItem" as Double))
            }

            if (params."sCriteria_m036YTDD") {
                eq("m036YTDD", (params."sCriteria_m036YTDD" as Double))
            }

            if (params."sCriteria_m036YTDS") {
                eq("m036YTDS", (params."sCriteria_m036YTDS" as Double))
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }


        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m036TglBerlaku: it.m036TglBerlaku ? it.m036TglBerlaku.format(dateFormat) : "",

                    companyDealer: it.companyDealer.m011NamaWorkshop,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    m036COGSDeprecation: conversi.toRupiah(it.m036COGSDeprecation),

                    m036COGSLabor:  conversi.toRupiah(it.m036COGSLabor),

                    m036COGSOil:  conversi.toRupiah(it.m036COGSOil),

                    m036COGSOverHead: conversi.toRupiah( it.m036COGSOverHead),

                    m036COGSPart:  conversi.toRupiah(it.m036COGSPart),

                    m036COGSSublet:  conversi.toRupiah(it.m036COGSSublet),

                    m036COGSTotalSGA: conversi.toRupiah( it.m036COGSTotalSGA),

                    m036Q1D:  conversi.toRupiah(it.m036Q1D),

                    m036Q1S:  conversi.toRupiah(it.m036Q1S),

                    m036Q2D:  conversi.toRupiah(it.m036Q2D),

                    m036Q2S:  conversi.toRupiah(it.m036Q2S),

                    m036Q3D:  conversi.toRupiah(it.m036Q3D),

                    m036Q3S:  conversi.toRupiah(it.m036Q3S),

                    m036Q4D:  conversi.toRupiah(it.m036Q4D),

                    m036Q4S:  conversi.toRupiah(it.m036Q4S),

                    m036Q5D:  conversi.toRupiah(it.m036Q5D),

                    m036Q5S: conversi.toRupiah(it.m036Q5S),

                    m036Q6D: conversi.toRupiah(it.m036Q6D),

                    m036Q6S: conversi.toRupiah(it.m036Q6S),

                    m036TWCLeadTime: conversi.toRupiah(it.m036TWCLeadTime),

                    m036TargetQty: conversi.toRupiah(it.m036TargetQty),

                    m036TargetSales: conversi.toRupiah(it.m036TargetSales),

                    m036TechReport:conversi.toRupiah( it.m036TechReport),

                    m036ThisMonthD: conversi.toRupiah(it.m036ThisMonthD),

                    m036ThisMonthS: conversi.toRupiah(it.m036ThisMonthS),

                    m036TotalClaimAmount:conversi.toRupiah( it.m036TotalClaimAmount),

                    m036TotalClaimItem: conversi.toRupiah(it.m036TotalClaimItem),

                    m036YTDD:conversi.toRupiah( it.m036YTDD),

                    m036YTDS:conversi.toRupiah( it.m036YTDS),

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [targetBPInstance: new TargetBP(params)]
    }

    def save() {
        def m036TglBerlaku = Date.parse("dd/MM/yyyy", params.m036TglBerlaku_date_dp)
        Calendar start = Calendar.getInstance();
        start.setTime(m036TglBerlaku);
        params.remove("m036TglBerlaku_date_dp")

        params.m036TglBerlaku = "date.struct"
        params.m036TglBerlaku_day = ""+start.get(Calendar.DATE)
        params.m036TglBerlaku_month = ""+start.get(Calendar.MONTH)
        params.m036TglBerlaku_year = ""+start.get(Calendar.YEAR)
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.staDel = '0'

        def targetBPInstance = new TargetBP(params)
        def targetBP = TargetBP.find(targetBPInstance)

        if(targetBP != null) {
            targetBPInstance.errors.rejectValue("version", "default.duplicate.primary.message",
                    [message(code: 'targetBP.label', default: 'Target BP')] as Object[],
                    "Terjadi duplikasi data.")
            render(view: "create", model: [targetBPInstance: targetBPInstance])
            return
        }

        if (!targetBPInstance.save(flush: true)) {
            render(view: "create", model: [targetBPInstance: targetBPInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'targetBP.label', default: 'Target BP'), targetBPInstance.id])
        redirect(action: "show", id: targetBPInstance.id)
    }

    def show(Long id) {
        def targetBPInstance = TargetBP.get(id)
        if (!targetBPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetBP.label', default: 'TargetBP'), id])
            redirect(action: "list")
            return
        }

        [targetBPInstance: targetBPInstance]
    }

    def edit(Long id) {
        def targetBPInstance = TargetBP.get(id)
        if (!targetBPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetBP.label', default: 'TargetBP'), id])
            redirect(action: "list")
            return
        }

        [targetBPInstance: targetBPInstance]
    }

    def update(Long id, Long version) {
        def targetBPInstance = TargetBP.get(id)
        if (!targetBPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetBP.label', default: 'TargetBP'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (targetBPInstance.version > version) {

                targetBPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'targetBP.label', default: 'TargetBP')] as Object[],
                        "Another user has updated this TargetBP while you were editing")
                render(view: "edit", model: [targetBPInstance: targetBPInstance])
                return
            }
        }

        targetBPInstance.properties = params

        if (!targetBPInstance.save(flush: true)) {
            render(view: "edit", model: [targetBPInstance: targetBPInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'targetBP.label', default: 'TargetBP'), targetBPInstance.id])
        redirect(action: "show", id: targetBPInstance.id)
    }

    def delete(Long id) {
        def targetBPInstance = TargetBP.get(id)
        if (!targetBPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetBP.label', default: 'TargetBP'), id])
            redirect(action: "list")
            return
        }

        try {
            targetBPInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'targetBP.label', default: 'TargetBP'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'targetBP.label', default: 'TargetBP'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(TargetBP, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(TargetBP, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
