package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PanelRepairController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PanelRepair.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            companyDealer{
                eq("staDel", "0")
            }
//            if (params."sCriteria_companyDealer") {
//                companyDealer{
//                    ilike("m011NamaWorkshop", "%" + (params."sCriteria_companyDealer" as String) + "%")
//                }
//                //eq("companyDealer", params."sCriteria_companyDealer")
//            }

            if (params."sCriteria_m043TanggalBerlaku") {
                ge("m043TanggalBerlaku", params."sCriteria_m043TanggalBerlaku")
                lt("m043TanggalBerlaku", params."sCriteria_m043TanggalBerlaku" + 1)
            }

            if (params."sCriteria_repairDifficulty") {
                repairDifficulty{
                    ilike("m042Tipe", "%" + (params."sCriteria_repairDifficulty" as String) + "%")
                }
            }

            if (params."sCriteria_m043Area1") {
                eq("m043Area1", Double.parseDouble(params."sCriteria_m043Area1"))
            }

            if (params."sCriteria_m043Area2") {
                eq("m043Area2", Double.parseDouble(params."sCriteria_m043Area2"))
            }

            if (params."sCriteria_m043StdTime") {
                eq("m043StdTime", Double.parseDouble(params."sCriteria_m043StdTime"))
            }

            if (params."sCriteria_m043ID") {
                eq("m043ID", params."sCriteria_m043ID")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

//                    companyDealer: it.companyDealer?.m011NamaWorkshop,

                    m043TanggalBerlaku: it.m043TanggalBerlaku ? it.m043TanggalBerlaku.format(dateFormat) : "",

                    repairDifficulty: it.repairDifficulty?.m042Tipe,

                    m043Area1: it.m043Area1,

                    m043Area2: it.m043Area2,

                    m043StdTime: it.m043StdTime,

                    m043ID: it.m043ID,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [panelRepairInstance: new PanelRepair(params)]
    }

    def save() {
        def panelRepairInstance = new PanelRepair(params)
        panelRepairInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        panelRepairInstance?.lastUpdProcess = "INSERT"
        panelRepairInstance?.companyDealer = session.userCompanyDealer
        if (panelRepairInstance.getM043Area2()<=panelRepairInstance.getM043Area1()) {
            flash.message = message(code: 'default.error.created.panelRepair.message', default: 'Area 2 tidak boleh kurang dari atau sama dengan Area 1.')
            render(view: "create", model: [panelRepairInstance: panelRepairInstance])
            return
        }
        if (panelRepairInstance.getM043StdTime()<=0) {
            flash.message = message(code: 'default.error.created.panelRepair.message', default: 'Standar Time tidak boleh 0.')
            render(view: "create", model: [panelRepairInstance: panelRepairInstance])
            return
        }
        def cek = PanelRepair.createCriteria()
        def result = cek.list() {
            and{
                eq("companyDealer",panelRepairInstance.companyDealer)
                eq("m043TanggalBerlaku",panelRepairInstance.m043TanggalBerlaku)
                eq("repairDifficulty",panelRepairInstance.repairDifficulty)
                eq("m043Area1",panelRepairInstance.m043Area1)
                eq("m043Area2",panelRepairInstance.m043Area2)
                eq("m043StdTime",panelRepairInstance.m043StdTime)
            }
        }

        //find(panelRepairInstance)
        if(!result){
            panelRepairInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            panelRepairInstance?.lastUpdProcess = "INSERT"

            if (!panelRepairInstance.save(flush: true)) {
                render(view: "create", model: [panelRepairInstance: panelRepairInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.operation.error.message', default: 'Duplicate Data Repair')
            render(view: "create", model: [panelRepairInstance: panelRepairInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), panelRepairInstance.id])
        redirect(action: "show", id: panelRepairInstance.id)
    }

    def show(Long id) {
        def panelRepairInstance = PanelRepair.get(id)
        if (!panelRepairInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), id])
            redirect(action: "list")
            return
        }

        [panelRepairInstance: panelRepairInstance]
    }

    def edit(Long id) {
        def panelRepairInstance = PanelRepair.get(id)
        if (!panelRepairInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), id])
            redirect(action: "list")
            return
        }

        [panelRepairInstance: panelRepairInstance]
    }

    def update(Long id, Long version) {
        params.m043Area1=params.m043Area1.replace(",","")
        params.m043Area2=params.m043Area2.replace(",","")
        params.m043StdTime=params.m043StdTime.replace(",","")
        def panelRepairInstance = PanelRepair.get(id)
        if (!panelRepairInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (panelRepairInstance.version > version) {

                panelRepairInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'panelRepair.label', default: 'Panel Repair')] as Object[],
                        "Another user has updated this PanelRepair while you were editing")
                render(view: "edit", model: [panelRepairInstance: panelRepairInstance])
                return
            }
        }
        if (Double.parseDouble(params.m043Area2) < Double.parseDouble(params.m043Area1)) {
            flash.message = message(code: 'default.error.created.panelRepair.message', default: 'Area 2 tidak boleh kurang dari Area 1.')
            render(view: "edit", model: [panelRepairInstance: panelRepairInstance])
            return
        }
        if (Double.parseDouble(params.m043StdTime) <= 0) {
            flash.message = message(code: 'default.error.created.panelRepair.message', default: 'Inputan Standar Time harus lebih besar dari 0.')
            render(view: "edit", model: [panelRepairInstance: panelRepairInstance])
            return
        }
        def cek = PanelRepair.createCriteria()
        def result = cek.list() {
            and{
                eq("m043TanggalBerlaku",params.m043TanggalBerlaku)
                eq("repairDifficulty",RepairDifficulty.findById(params.repairDifficulty.id))
                eq("m043Area1",Double.parseDouble(params.m043Area1))
                eq("m043Area2",Double.parseDouble(params.m043Area2))
                eq("m043StdTime",Double.parseDouble(params.m043StdTime))
            }
        }
        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Duplicate Data Repair')
            render(view: "edit", model: [panelRepairInstance: panelRepairInstance])
            return
        }

        panelRepairInstance.properties = params
        panelRepairInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        panelRepairInstance?.lastUpdProcess = "UPDATE"

        if(!result){
            panelRepairInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            panelRepairInstance?.lastUpdProcess = "UPDATE"

            if (!panelRepairInstance.save(flush: true)) {
                render(view: "edit", model: [panelRepairInstance: panelRepairInstance])
                return
            }
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), panelRepairInstance.id])
        redirect(action: "show", id: panelRepairInstance.id)
    }

    def delete(Long id) {
        def panelRepairInstance = PanelRepair.get(id)
        if (!panelRepairInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), id])
            redirect(action: "list")
            return
        }

        try {
            panelRepairInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'panelRepair.label', default: 'Panel Repair'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PanelRepair, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PanelRepair, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
