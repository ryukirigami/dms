package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MappingWorkItemsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = MappingWorkItems.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", CompanyDealer.findByM011NamaWorkshopIlike("%"+params."sCriteria_companyDealer"+"%"))
            }

            if (params."sCriteria_operation") {
			operation{
				ilike("m053NamaOperation","%"+(params."sCriteria_operation")+"%")
				}
            }

            if (params."sCriteria_workItems") {
			workItems{
				ilike("m039WorkItems","%"+(params."sCriteria_workItems")+"%")
				}
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer.m011NamaWorkshop,

                    operation: it.operation.m053NamaOperation,

                    workItems: it.workItems.m039WorkItems,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [mappingWorkItemsInstance: new MappingWorkItems(params)]
    }

    def save() {
        def mappingWorkItemsInstance = new MappingWorkItems(params)
        mappingWorkItemsInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingWorkItemsInstance?.lastUpdProcess = "INSERT"
        mappingWorkItemsInstance?.companyDealer = session.userCompanyDealer
        if(MappingWorkItems.findByOperationAndWorkItems(Operation.findById(params.operation.id),
        WorkItems.findById(params.workItems.id))){
            flash.message = "Data sudah ada"
            render(view: "create", model: [mappingWorkItemsInstance: mappingWorkItemsInstance])
            return
        }
        if (!mappingWorkItemsInstance.save(flush: true)) {
            render(view: "create", model: [mappingWorkItemsInstance: mappingWorkItemsInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'mappingWorkItems.label', default: 'Mapping Job Work Items'), mappingWorkItemsInstance.id])
        redirect(action: "show", id: mappingWorkItemsInstance.id)
    }

    def show(Long id) {
        def mappingWorkItemsInstance = MappingWorkItems.get(id)
        if (!mappingWorkItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingWorkItems.label', default: 'MappingWorkItems'), id])
            redirect(action: "list")
            return
        }

        [mappingWorkItemsInstance: mappingWorkItemsInstance]
    }

    def edit(Long id) {
        def mappingWorkItemsInstance = MappingWorkItems.get(id)
        if (!mappingWorkItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingWorkItems.label', default: 'MappingWorkItems'), id])
            redirect(action: "list")
            return
        }

        [mappingWorkItemsInstance: mappingWorkItemsInstance]
    }

    def update(Long id, Long version) {
        def mappingWorkItemsInstance = MappingWorkItems.get(id)
        if (!mappingWorkItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingWorkItems.label', default: 'MappingWorkItems'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (mappingWorkItemsInstance.version > version) {

                mappingWorkItemsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'mappingWorkItems.label', default: 'MappingWorkItems')] as Object[],
                        "Another user has updated this MappingWorkItems while you were editing")
                render(view: "edit", model: [mappingWorkItemsInstance: mappingWorkItemsInstance])
                return
            }
        }

        def work = WorkItems.findById(params.workItems.id)
        def operation = Operation.findByIdAndStaDel(params.operation.id,"0")
        def workItems = MappingWorkItems.findByWorkItemsAndOperation(work,operation)
        if(workItems){
            if(id!=workItems.id){
                flash.message = "Data sudah ada"
                render(view: "edit", model: [mappingWorkItemsInstance: mappingWorkItemsInstance])
                return
            }
        }
        mappingWorkItemsInstance.properties = params
        mappingWorkItemsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingWorkItemsInstance?.lastUpdProcess = "UPDATE"
        if (!mappingWorkItemsInstance.save(flush: true)) {
            render(view: "edit", model: [mappingWorkItemsInstance: mappingWorkItemsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'mappingWorkItems.label', default: 'Mappin Job Work Items'), mappingWorkItemsInstance.id])
        redirect(action: "show", id: mappingWorkItemsInstance.id)
    }

    def delete(Long id) {
        def mappingWorkItemsInstance = MappingWorkItems.get(id)
        if (!mappingWorkItemsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingWorkItems.label', default: 'MappingWorkItems'), id])
            redirect(action: "list")
            return
        }

        try {
            mappingWorkItemsInstance?.lastUpdProcess = "DELETE"
            mappingWorkItemsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            mappingWorkItemsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'mappingWorkItems.label', default: 'MappingWorkItems'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'mappingWorkItems.label', default: 'MappingWorkItems'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MappingWorkItems, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(MappingWorkItems, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
