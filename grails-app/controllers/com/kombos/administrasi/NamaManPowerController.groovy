package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class NamaManPowerController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        session.exportParams = params
        def c = NamaManPower.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            companyDealer{
                eq("id", session.userCompanyDealerId)
            }
            eq("staDel","0")
            if (params."sCriteria_companyDealer") {
                eq("companyDealer", CompanyDealer.findByM011NamaWorkshopIlike("%"+params."sCriteria_companyDealer"+"%"))
            }

//            if (params."sCriteria_manPower") {
//                eq("manPower", ManPower.findByM014JabatanManPowerIlike("%"+params."sCriteria_manPower"+"%"))
//            }

            if (params."sCriteria_manPowerDetail") {
                eq("manPowerDetail",ManPowerDetail.get(params."sCriteria_manPowerDetail"))
            }

            if (params."sCriteria_t015NamaBoard") {
                ilike("t015NamaBoard", "%" + (params."sCriteria_t015NamaBoard" as String) + "%")
            }

            if (params."sCriteria_t015NamaLengkap") {
                ilike("t015NamaLengkap", "%" + (params."sCriteria_t015NamaLengkap" as String) + "%")
            }

            if (params."sCriteria_t015StaAktif") {
                eq("t015StaAktif", params."sCriteria_t015StaAktif" as String)
            }

            if (params."sCriteria_t015StatusManPower") {
                eq("t015StatusManPower", params."sCriteria_t015StatusManPower" as String)
            }

            if (params."sCriteria_userProfile") {
                eq("userProfile", params."sCriteria_userProfile")
            }

            if (params."sCriteria_t015TanggalLahir") {
                ge("t015TanggalLahir", params."sCriteria_t015TanggalLahir")
                lt("t015TanggalLahir", params."sCriteria_t015TanggalLahir" + 1)
            }

            if (params."sCriteria_t015Alamat") {
                ilike("t015Alamat", "%" + (params."sCriteria_t015Alamat" as String) + "%")
            }

            if (params."sCriteria_t015NoTelp") {
                ilike("t015NoTelp", "%" + (params."sCriteria_t015NoTelp" as String) + "%")
            }

            if (params."sCriteria_t015TglBergabungTAM") {
                ge("t015TglBergabungTAM", params."sCriteria_t015TglBergabungTAM")
                lt("t015TglBergabungTAM", params."sCriteria_t015TglBergabungTAM" + 1)
            }

            if (params."sCriteria_t015Ket") {
                ilike("t015Ket", "%" + (params."sCriteria_t015Ket" as String) + "%")
            }

            if (params."sCriteria_t015xNamaUser") {
                ilike("t015xNamaUser", "%" + (params."sCriteria_t015xNamaUser" as String) + "%")
            }
            if (params."sCriteria_t015xNamaDivisi") {
                ilike("t015xNamaDivisi", "%" + (params."sCriteria_t015xNamaDivisi" as String) + "%")
            }
            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer?.m011NamaWorkshop,

//                    manPower: it.manPower?.m014JabatanManPower,

                    manPowerDetail: it.manPowerDetail?.m015LevelManPower,

                    t015NamaBoard: it.t015NamaBoard,

                    t015NamaLengkap: it.t015NamaLengkap,

                    t015StaAktif: it.t015StaAktif,

                    t015StatusManPower: it.t015StatusManPower,

                    userProfile: it.userProfile?.t001NamaPegawai,

                    t015TanggalLahir: it.t015TanggalLahir ? it.t015TanggalLahir.format(dateFormat) : "",

                    t015Alamat: it.t015Alamat,

                    t015NoTelp: it.t015NoTelp,

                    t015TglBergabungTAM: it.t015TglBergabungTAM ? it.t015TglBergabungTAM.format(dateFormat) : "",

                    t015Ket: it.t015Ket,

                    t015xNamaUser: it.t015xNamaUser,

                    t015xNamaDivisi: it.t015xNamaDivisi,

                    staDel: it.staDel,

            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

    def create() {

        def mustDeletePengganti = ManPowerManPowerPengganti.findAllByManPowerIsNull()
        mustDeletePengganti.each {
            it.delete(flush: true)
        }
        [namaManPowerInstance: new NamaManPower(params), manPowerPengganti : NamaManPower.findAll()]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def namaManPowerInstance = new NamaManPower(params)
        namaManPowerInstance?.companyDealer = CompanyDealer.findByIdAndStaDel(params.companyDealer.id as long, "0")
        namaManPowerInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        namaManPowerInstance?.lastUpdProcess = "INSERT"
        namaManPowerInstance.t015NamaBoard = params.t015NamaBoard.toUpperCase()
        def cekuser=User.createCriteria().list {
            eq("staDel","0")
            eq("username",params.username.trim(),[ignoreCase: true])
        };
        if(cekuser.size()<1)
        { flash.message="Data User Tidak Ditemukan"
          render(view: "create", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
         return }
        def cekuse2r=NamaManPower.createCriteria().list {
            eq("staDel","0")
            eq("userProfile",User.findByUsername(params.username))
        };
        if(cekuse2r.size()>0){
            flash.message="User Sudah Di Gunakan"
            render(view: "create", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
            return
        }
        namaManPowerInstance?.userProfile = User.findByUsernameAndStaDel(params.username,"0")

        namaManPowerInstance.t015NamaLengkap = params.t015NamaLengkap.toUpperCase()
        namaManPowerInstance.staDel = '0'
        def cekId = NamaManPower.createCriteria().list {
            eq("staDel","0");
            eq("t015IdManPower",params.t015IdManPower.trim(),[ignoreCase: true])
        };
        if(cekId){
            flash.message = "ID sudah digunakan"
            render(view: "create", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
        }
        def cek = NamaManPower.createCriteria()
        def result = cek.list() {
            and{
                eq("manPowerDetail",namaManPowerInstance.manPowerDetail)
                eq("t015NamaBoard",namaManPowerInstance.t015NamaBoard.trim(), [ignoreCase: true])
                eq("t015NamaLengkap",namaManPowerInstance.t015NamaLengkap.trim(), [ignoreCase: true])
                eq("t015Alamat",namaManPowerInstance.t015Alamat.trim(), [ignoreCase: true])
                eq("t015TanggalLahir",namaManPowerInstance.t015TanggalLahir)
                eq("t015StatusManPower",namaManPowerInstance.t015StatusManPower)
                eq("t015StaAktif",namaManPowerInstance.t015StaAktif)
                eq("t015NoTelp",namaManPowerInstance.t015NoTelp.trim(), [ignoreCase: true])
                eq("t015TglBergabungTAM",namaManPowerInstance.t015TglBergabungTAM)
             //   eq("userProfile",namaManPowerInstance.userProfile)
//                eq("t015Ket",namaManPowerInstance.t015Ket.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        result.each {
            //println "hasil result "+it.t015NamaLengkap
        }
        if(!result){
            if (!namaManPowerInstance.save(flush: true)) {
                render(view: "create", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
                return
            }
        }else {
            flash.message = message(code: 'default.created.operation.error.message', default: 'Duplicate Data Repair')
            render(view: "create", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
            return
        }
        def listManPowerPengganti = ManPowerManPowerPengganti.findWhere(manPower : null)
        listManPowerPengganti.each {
            def manPowerPengganti = ManPowerManPowerPengganti.get(it.id)
            manPowerPengganti.manPower = namaManPowerInstance
            manPowerPengganti.save(flush:true)

        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'namaManPower.label', default: 'Biodata Man Power'), namaManPowerInstance.id])
        redirect(action: "show", id: namaManPowerInstance.id)
    }

    def show(Long id) {
        def namaManPowerInstance = NamaManPower.get(id)
        if (!namaManPowerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaManPower.label', default: 'Nama Man Power'), id])
            redirect(action: "list")
            return
        }

        [namaManPowerInstance: namaManPowerInstance]
    }

    def edit(Long id) {
        def namaManPowerInstance = NamaManPower.get(id)
        if (!namaManPowerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaManPower.label', default: 'Nama Man Power'), id])
            redirect(action: "list")
            return
        }
        [namaManPowerInstance: namaManPowerInstance, manPowerPengganti : NamaManPower.findAll()]
    }

    def update(Long id, Long version) {
        def namaManPowerInstance = NamaManPower.get(id)
        if (!namaManPowerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaManPower.label', default: 'Nama Man Power'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (namaManPowerInstance.version > version) {

                namaManPowerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'namaManPower.label', default: 'NamaManPower')] as Object[],
                        "Another user has updated this NamaManPower while you were editing")
                render(view: "edit", model: [namaManPowerInstance: namaManPowerInstance], params :[manPowerPengganti : NamaManPower.findAll()])
                return
            }
        }
        def cekId = NamaManPower.createCriteria().list {
            eq("staDel","0");
            eq("t015IdManPower",params.t015IdManPower.trim(),[ignoreCase: true])
        };
        if(cekId){
            for(find in cekId){
                if(id!=find.id){
                    flash.message = "ID sudah digunakan"
                    render(view: "edit", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
                }
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        namaManPowerInstance.properties = params
        namaManPowerInstance.companyDealer = CompanyDealer.findByIdAndStaDel(params.companyDealer.id as long, "0")

        def cekuser=User.createCriteria().list {
            eq("staDel","0")
            eq("username",params.username.trim(),[ignoreCase: true])
        };
        if(cekuser.size()<1)
        { flash.message="Data User Tidak Ditemukan"
            render(view: "create", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
            return }
        def cekuse2r=NamaManPower.createCriteria().list {
            eq("staDel","0")
            eq("userProfile",User.findByUsername(params.username))
        };
        if(cekuse2r.size()>0){
            flash.message="User Sudah Di Gunakan"
            render(view: "create", model: [namaManPowerInstance: namaManPowerInstance],manPowerPengganti : NamaManPower.findAll())
            return
        }
        namaManPowerInstance?.userProfile = User.findByUsernameAndStaDel(params.username,"0")

        namaManPowerInstance?.userProfile = User.findByUsernameAndStaDel(params.username,"0")
        namaManPowerInstance.t015NamaBoard = params.t015NamaBoard.toUpperCase()
        namaManPowerInstance.t015NamaLengkap = params.t015NamaLengkap.toUpperCase()
        namaManPowerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        namaManPowerInstance?.lastUpdProcess = "UPDATE"
        def cek = NamaManPower.createCriteria()
        def result = cek.list() {
            and{
                eq("manPowerDetail",namaManPowerInstance.manPowerDetail)
                eq("t015NamaBoard",namaManPowerInstance.t015NamaBoard.trim(), [ignoreCase: true])
                eq("t015NamaLengkap",namaManPowerInstance.t015NamaLengkap.trim(), [ignoreCase: true])
                eq("t015Alamat",namaManPowerInstance.t015Alamat.trim(), [ignoreCase: true])
                eq("t015TanggalLahir",namaManPowerInstance.t015TanggalLahir)
                eq("t015StatusManPower",namaManPowerInstance.t015StatusManPower)
                eq("t015StaAktif",namaManPowerInstance.t015StaAktif)
                eq("t015NoTelp",namaManPowerInstance.t015NoTelp.trim(), [ignoreCase: true])
                eq("t015TglBergabungTAM",namaManPowerInstance.t015TglBergabungTAM)
                eq("staDel","0")
            }
        }
        if(!result){
            if (!namaManPowerInstance.save(flush: true)) {
                render(view: "edit", model: [namaManPowerInstance: namaManPowerInstance], params :[manPowerPengganti : NamaManPower.findAll()])
                return
            }
        }else {
            for(find in result){
                if(id!=find.id){
                    flash.message = message(code: 'default.created.operation.error.message', default: 'Data yang diupdate sama atau Data Duplikat')
                    render(view: "edit", model: [namaManPowerInstance: namaManPowerInstance], params :[manPowerPengganti : NamaManPower.findAll()])
                    return
                }
            }
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'namaManPower.label', default: 'Biodata Man Power'), namaManPowerInstance.id])
        redirect(action: "show", id: namaManPowerInstance.id)
    }

    def delete(Long id) {
        def namaManPowerInstance = NamaManPower.get(id)
        if (!namaManPowerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaManPower.label', default: 'NamaManPower'), id])
            redirect(action: "list")
            return
        }

        try {
            namaManPowerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            namaManPowerInstance?.lastUpdProcess = "DELETE"
            namaManPowerInstance?.setStaDel('1')
            namaManPowerInstance?.lastUpdated = datatablesUtilService?.syncTime()
            namaManPowerInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'namaManPower.label', default: 'NamaManPower'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'namaManPower.label', default: 'NamaManPower'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {

            datatablesUtilService.massDeleteStaDelNew(NamaManPower, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(NamaManPower, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


    def loadManPowerPengganti(){
        redirect(action: "showManPowerPengganti",params: [idManPower : params.idManPower] )
    }

    def addManPowerPengganti (){
        def manPowerPenggantiTemp = NamaManPower.get(Double.parseDouble(params.id))
        def idManPower = '0'
        def cekSamaNull = ManPowerManPowerPengganti.findByManPowerPenggantiAndManPowerIsNull(manPowerPenggantiTemp)
        def cekSudahAda = cekSamaNull
        ManPowerManPowerPengganti manPowerPengganti = new ManPowerManPowerPengganti()
        manPowerPengganti.manPowerPengganti = manPowerPenggantiTemp
        manPowerPengganti.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        manPowerPengganti.lastUpdProcess = "INSERT"

        if(params.idManPower){
          //  log.info("id Man ada di add")
            idManPower = params.idManPower
            def manPowerTemp = NamaManPower.get(idManPower)
            cekSudahAda = ManPowerManPowerPengganti.findByManPowerAndManPowerPengganti(manPowerTemp,manPowerPenggantiTemp)
            manPowerPengganti.manPower = manPowerTemp

            if(cekSudahAda==null){
                manPowerPengganti.save()
                //println "lagi ngesave man power id"
            }

        }else if(cekSamaNull==null){
            manPowerPengganti.save()
            //println "lagi ngesave sama null"
        }

        redirect(action: "showManPowerPengganti", params: [idManPower : idManPower])

    }

    def deleteManPowerPengganti(){
        def manPowerPengganti
        def idManPower, manPower
        manPowerPengganti = ManPowerManPowerPengganti.get(Double.parseDouble(params.id))
        manPower = NamaManPower.get(manPowerPengganti.manPowerPengganti?.id)


        if(params.idManPower){
            idManPower = params.idManPower
        }else{
            idManPower = '0'
        }
        manPowerPengganti.delete()


        redirect(action: "showManPowerPengganti", params: [idManPower : idManPower, manPower: manPower])

    }

    def showManPowerPengganti(){
        def manPowerPenggantis

        def idManPower = params.idManPower
        if(idManPower){
            if(idManPower.equalsIgnoreCase('0'))
                manPowerPenggantis = ManPowerManPowerPengganti.findAllWhere(manPower : null)
            else{
                manPowerPenggantis = ManPowerManPowerPengganti.findAllByManPower(NamaManPower.get(Double.parseDouble(idManPower)))
            }

        }

        [manPowerPenggantis : manPowerPenggantis, idManPower : idManPower ]
    }

    def addPenggantiToTable(){
        def idManPower = params.idManPower
        def namaManPower = NamaManPower.get(idManPower)

        def res = [:]
        res.id = namaManPower.id
        res.namaLengkap = namaManPower.t015NamaLengkap

        render res as JSON
    }

    def removePenggantiFromTable(){
        def idManPower = params.idManPower
        def idPengganti = params.idPengganti
        def pengganti = ManPowerManPowerPengganti.get(idPengganti)
        def namaManPower = NamaManPower.get(idManPower)
        def manPowerPengganti = ManPowerManPowerPengganti.findByManPowerAndManPowerPengganti(namaManPower,pengganti)

        manPowerPengganti.delete(flush:true)
        render "DELETE MAN POWER PENGGANTI"

    }

    def loadLevelManPower(){
        def manPowerDetail = ManPowerDetail.createCriteria()
        def manPowerDetailList = manPowerDetail.list {
            order("m015LevelManPower","asc")
            distinct("m015LevelManPower")
        }
        def res = []

        manPowerDetailList.each {
            res << [
                    id : it.id,
                    namaLevel : it.m015LevelManPower
            ]
        }

        render res as JSON
    }

    def addManPowerToTable(){

    }

}
