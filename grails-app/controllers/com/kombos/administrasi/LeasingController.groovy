package com.kombos.administrasi

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class LeasingController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    private static final String EMAIL_PATTERN = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/;

    public boolean validasiEmail(String email) {
        email ==~ EMAIL_PATTERN
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = Leasing.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params."sCriteria_m1000NamaLeasing"){
                ilike("m1000NamaLeasing","%" + (params."sCriteria_m1000NamaLeasing" as String) + "%")
            }

            if(params."sCriteria_m1000Alamat"){
                ilike("m1000Alamat","%" + (params."sCriteria_m1000Alamat" as String) + "%")
            }

            if(params."sCriteria_m1000Npwp"){
                ilike("m1000Npwp","%" + (params."sCriteria_m1000Npwp" as String) + "%")
            }

            if(params."sCriteria_m1000NoTelp"){
                ilike("m1000NoTelp","%" + (params."sCriteria_m1000NoTelp" as String) + "%")
            }

            if(params."sCriteria_m1000Email"){
                ilike("m1000Email","%" + (params."sCriteria_m1000Email" as String) + "%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m1000NamaLeasing: it.m1000NamaLeasing,

                    m1000Alamat: it.m1000Alamat,

                    m1000Npwp: it.m1000Npwp,

                    m1000NoTelp: it.m1000NoTelp,

                    m1000Email: it.m1000Email

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [leasingInstance: new Leasing(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def leasingInstance = new Leasing(params)

        String email = params.m1000Email
        def checkEmail = validasiEmail(email)

        if(email != ''){
            if(!checkEmail){
                flash.message = 'Input Email dengan format : xxx@xxx.xxx'
                render(view: "create", model: [leasingInstance: leasingInstance])
                return
            }
        }
        if(Leasing.findByM1000NamaLeasingAndM1000AlamatAndM1000Email(params.m1000NamaLeasing,params.m1000Alamat,params.m1000Email)!=null){
            flash.message = '* Data Leasing Sudah Ada'
            render(view: "create", model: [leasingInstance: leasingInstance])
            return
        }


        leasingInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        leasingInstance?.lastUpdProcess = "INSERT"
        leasingInstance?.setStaDel('0')
        if (!leasingInstance.save(flush: true)) {
            render(view: "create", model: [leasingInstance: leasingInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'leasing.label', default: 'Leasing'), leasingInstance.id])
        redirect(action: "show", id: leasingInstance.id)
    }

    def show(Long id) {
        def leasingInstance = Leasing.get(id)
        if (!leasingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'leasing.label', default: 'Leasing'), id])
            redirect(action: "list")
            return
        }

        [leasingInstance: leasingInstance]
    }

    def edit(Long id) {
        def leasingInstance = Leasing.get(id)
        if (!leasingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'leasing.label', default: 'Leasing'), id])
            redirect(action: "list")
            return
        }

        [leasingInstance: leasingInstance]
    }

    def update(Long id, Long version) {
        def leasingInstance = Leasing.get(id)
        if (!leasingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'leasing.label', default: 'Leasing'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (leasingInstance.version > version) {

                leasingInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'leasing.label', default: 'Leasing')] as Object[],
                        "Another user has updated this leasing while you were editing")
                render(view: "edit", model: [leasingInstance: leasingInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        leasingInstance.properties = params
        leasingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        leasingInstance?.lastUpdProcess = "UPDATE"

        String email = params.m1000Email
        def checkEmail = validasiEmail(email)

        if(email != ''){
            if(!checkEmail){
                flash.message = 'Input Email dengan format : xxx@xxx.xxx'
                render(view: "create", model: [leasingInstance: leasingInstance])
                return
            }
        }

        if(Leasing.findByM1000NamaLeasingAndM1000AlamatAndM1000Email(params.m1000NamaLeasing,params.m1000Alamat,params.m1000Email)!=null){
            if(Leasing.findByM1000NamaLeasingAndM1000AlamatAndM1000Email(params.m1000NamaLeasing,params.m1000Alamat,params.m1000Email).id!=id){
                flash.message = '* Data Leasing Sudah Ada'
                render(view: "create", model: [leasingInstance: leasingInstance])
                return
            }
        }

        if (!leasingInstance.save(flush: true)) {
            render(view: "edit", model: [leasingInstance: leasingInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'leasing.label', default: 'Leasing'), leasingInstance.id])
        redirect(action: "show", id: leasingInstance.id)
    }

    def delete(Long id) {
        def leasingInstance = Leasing.get(id)
        if (!leasingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'leasing.label', default: 'Leasing'), id])
            redirect(action: "list")
            return
        }

        try {
            leasingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            leasingInstance?.lastUpdProcess = "DELETE"
            leasingInstance?.setStaDel('1')
            leasingInstance?.lastUpdated = datatablesUtilService?.syncTime()
            leasingInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'leasing.label', default: 'Leasing'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'leasing.label', default: 'Leasing'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Leasing, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(Leasing, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }


}
