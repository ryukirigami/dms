package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ColorMatchingController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams = params

        def c = ColorMatching.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m046TglBerlaku") {
                ge("m046TglBerlaku", params."sCriteria_m046TglBerlaku")
                lt("m046TglBerlaku", params."sCriteria_m046TglBerlaku" + 1)
            }

            if (params."sCriteria_m046Klasifikasi") {
                eq("m046Klasifikasi", Integer.parseInt(params."sCriteria_m046Klasifikasi"))
            }

            if (params."sCriteria_m046JmlPanel") {
                eq("m046JmlPanel", Integer.parseInt(params."sCriteria_m046JmlPanel"))
            }

            if (params."sCriteria_m046StdTime") {
                eq("m046StdTime", Double.parseDouble (params."sCriteria_m046StdTime"))
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m046TglBerlaku: it.m046TglBerlaku ? it.m046TglBerlaku.format(dateFormat) : "",

                    m046Klasifikasi: it.m046Klasifikasi,

                    m046JmlPanel: it.m046JmlPanel,

                    m046StdTime: it.m046StdTime,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    companyDealer: it.companyDealer,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [colorMatchingInstance: new ColorMatching(params)]
    }

    def save() {
        def colorMatchingInstance = new ColorMatching(params)
        colorMatchingInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        colorMatchingInstance?.lastUpdProcess = "INSERT"
        if (colorMatchingInstance.getM046JmlPanel()<=0) {
		    flash.message = message(code: 'default.error.created.colorMatching.message', default: 'Inputan Jumlah Panel harus lebih dari 0.')
            render(view: "create", model: [colorMatchingInstance: colorMatchingInstance])
            return
        }
        if (colorMatchingInstance.getM046StdTime()<=0) {
            flash.message = message(code: 'default.error.created.colorMatching.message', default: 'Inputan Standar Time harus lebih dari 0.')
            render(view: "create", model: [colorMatchingInstance: colorMatchingInstance])
            return
        }
        if (ColorMatching.findByM046KlasifikasiAndM046JmlPanelAndM046StdTime(params.m046Klasifikasi,params.m046JmlPanel,params.StdTime)!=null){
            flash.message = '* Data Sudah Digunakan'
            render(view: "create", model: [colorMatchingInstance: colorMatchingInstance])
            return
        }
        if (!colorMatchingInstance.save(flush: true)) {
            render(view: "create", model: [colorMatchingInstance: colorMatchingInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'colorMatching.label', default: 'Color Matching'), colorMatchingInstance.id])
        redirect(action: "show", id: colorMatchingInstance.id)
    }

    def show(Long id) {
        def colorMatchingInstance = ColorMatching.get(id)
        if (!colorMatchingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'colorMatching.label', default: 'ColorMatching'), id])
            redirect(action: "list")
            return
        }

        [colorMatchingInstance: colorMatchingInstance]
    }

    def edit(Long id) {
        def colorMatchingInstance = ColorMatching.get(id)
        if (!colorMatchingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'colorMatching.label', default: 'ColorMatching'), id])
            redirect(action: "list")
            return
        }

        [colorMatchingInstance: colorMatchingInstance]
    }

    def update(Long id, Long version) {
        def colorMatchingInstance = ColorMatching.get(id)
        if (!colorMatchingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'colorMatching.label', default: 'ColorMatching'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (colorMatchingInstance.version > version) {

                colorMatchingInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'colorMatching.label', default: 'ColorMatching')] as Object[],
                        "Another user has updated this ColorMatching while you were editing")
                render(view: "edit", model: [colorMatchingInstance: colorMatchingInstance])
                return
            }
        }

        colorMatchingInstance.properties = params
        colorMatchingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        colorMatchingInstance?.lastUpdProcess = "UPDATE"
        if (colorMatchingInstance.getM046JmlPanel()<=0) {
            flash.message = message(code: 'default.error.created.colorMatching.message', default: 'Inputan Jumlah Panel harus lebih dari 0.')
            render(view: "edit", model: [colorMatchingInstance: colorMatchingInstance])
            return
        }
        if (colorMatchingInstance.getM046StdTime()<=0) {
            flash.message = message(code: 'default.error.created.colorMatching.message', default: 'Inputan Standar Time harus lebih dari 0.')
            render(view: "edit", model: [colorMatchingInstance: colorMatchingInstance])
            return
        }
        if (ColorMatching.findByM046KlasifikasiAndM046JmlPanelAndM046StdTime(params.m046Klasifikasi,params.m046JmlPanel,params.StdTime)!=null){
            if (ColorMatching.findByM046KlasifikasiAndM046JmlPanelAndM046StdTime(params.m046Klasifikasi,params.m046JmlPanel,params.StdTime).id!=id){
                flash.message = '* Data Sudah Digunakan'
                render(view: "create", model: [colorMatchingInstance: colorMatchingInstance])
                return
            }
        }
        if (!colorMatchingInstance.save(flush: true)) {
            render(view: "edit", model: [colorMatchingInstance: colorMatchingInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'colorMatching.label', default: 'Color Matching'), colorMatchingInstance.id])
        redirect(action: "show", id: colorMatchingInstance.id)
    }

    def delete(Long id) {
        def colorMatchingInstance = ColorMatching.get(id)
        if (!colorMatchingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'colorMatching.label', default: 'ColorMatching'), id])
            redirect(action: "list")
            return
        }

        try {
            colorMatchingInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            colorMatchingInstance?.lastUpdProcess = "DELETE"
            colorMatchingInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'colorMatching.label', default: 'ColorMatching'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'colorMatching.label', default: 'ColorMatching'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(ColorMatching, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(ColorMatching, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
