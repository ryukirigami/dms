package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

class AudioController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]

		session.exportParams=params
		
		def c = Audio.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m405Text"){
				ilike("m405Text","%" + (params."sCriteria_m405Text" as String) + "%")
			}
	
			if(params."sCriteria_audioPath"){
				ilike("audioPath","%" + (params."sCriteria_audioPath" as String) + "%")
			}
	
			if(params."sCriteria_mimeType"){
				ilike("mimeType","%" + (params."sCriteria_mimeType" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,

						m405Text: it.m405Text,
			
						audioPath: it.audioPath,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[audioInstance: new Audio(params)]
	}

	def save() {
		def audioInstance = new Audio(params)

        audioInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        audioInstance?.lastUpdProcess = "INSERT"
        audioInstance?.dateCreated = new Date()
        def audioFile = request.getFile('audioPath')
        def notAllowCont = ['application/octet-stream']
        if(audioFile !=null && !notAllowCont.contains(audioFile.getContentType()) ){
            MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
            CommonsMultipartFile fileUpload = (CommonsMultipartFile) mpr.getFile("audioPath");
            def okcontents = ['wav', 'mp3', 'ogg','audio/mpeg','audio/mp3']
            if (! okcontents.contains(audioFile.getContentType())) {
                log.info("tipe konten : "+audioFile.getContentType());
                flash.message = "Audio diisi dan harus berupa: ${okcontents}"
                render(view:'create', model:[audioInstance: audioInstance])
                return;
            }

            String name = fileUpload.getOriginalFilename()
            String path = request.getRealPath("/audio/"+name)
            if(!audioFile.isEmpty()){
                log.info("ada isi file")
                try{
                    audioFile.transferTo(new File(path))

                }catch(Exception ex){
                    log.error('Gagal simpan audio');
                }


               audioInstance.audioPath = name
                log.info("path : "+path)

            }

        }else{
            flash.message = "Audio harus diisi"
            render(view:'create', model:[audioInstance: audioInstance])
            return;

        }

        def cekAudio = Audio.createCriteria().list() {
            or{
                eq("m405Text",audioInstance.m405Text.trim(), [ignoreCase: true])
                eq("audioPath", audioInstance.audioPath)
          //      notEqual("id",audioInstance.id)
            }
        }

        if(cekAudio){
            flash.message = "Data sudah tersedia (duplikat)"
            render(view: "create", model: [audioInstance: audioInstance])
            return
        }

        
        if (!audioInstance.save(flush: true)) {
			render(view: "create", model: [audioInstance: audioInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'audio.label', default: 'Audio'), audioInstance.id])
		redirect(action: "show", id: audioInstance.id)
	}

	def show(Long id) {
		def audioInstance = Audio.get(id)
		if (!audioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'audio.label', default: 'Audio'), id])
			redirect(action: "list")
			return
		}

		[audioInstance: audioInstance]
	}

	def edit(Long id) {
		def audioInstance = Audio.get(params.id)

		if (!audioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'audio.label', default: 'Audio'), id])
			redirect(action: "list")
			return
		}

		[audioInstance: audioInstance]
	}

	def update(Long id, Long version) {
		def audioInstance = Audio.get(params.id)

		if (!audioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'audio.label', default: 'Audio'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (audioInstance.version > version) {
				
				audioInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'audio.label', default: 'Audio')] as Object[],
				"Another user has updated this Audio while you were editing")
				render(view: "edit", model: [audioInstance: audioInstance])
				return
			}
		}

        def oldAudio = audioInstance.audioPath
        audioInstance.properties = params
      //  audioInstance?.m405Text = params.m405Text
        audioInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        audioInstance?.lastUpdProcess = "UPDATE"
        audioInstance?.lastUpdated = new Date()


        def audioFile = request.getFile('audioPath')
        def notAllowCont = ['application/octet-stream']

        if(audioFile !=null && !notAllowCont.contains(audioFile.getContentType()) ){

            MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
            CommonsMultipartFile fileUpload = (CommonsMultipartFile) mpr.getFile("audioPath");


            def okcontents = ['wav', 'mp3', 'ogg','audio/mp3','audio/mpeg']
            if (! okcontents.contains(audioFile.getContentType())) {
                flash.message = "Audio harus berupa: ${okcontents}"
                render(view:'edit', model:[audioInstance: audioInstance])
                return;
            }

            String name = fileUpload.getOriginalFilename()

            if(!audioFile.isEmpty()){
                log.info("ada isi file")

                String path = request.getRealPath("/audio/"+name)
                try{
                    audioFile.transferTo(new File(path))
                }catch (Exception e){

                }
                audioInstance.audioPath = name
              //  log.info("path : "+path)

            }

        }else{
            audioInstance.audioPath = oldAudio
        }

        def cekAudio = Audio.createCriteria().list() {
            and{
             or{
                 eq("m405Text",audioInstance.m405Text.trim(), [ignoreCase: true])
                 eq("audioPath", audioInstance.audioPath)
             }
                notEqual("id",audioInstance.id)
            }
        }


        if(cekAudio){
            flash.message = "Data text/suara sudah tersedia (duplikat)"
            audioInstance.discard();

            render(view: "edit", model: [audioInstance: audioInstance])
            return
        }else{
            if (!audioInstance.save(flush: true)) {
                render(view: "edit", model: [audioInstance: audioInstance])
                return
            }

            flash.message = message(code: 'default.updated.message', args: [message(code: 'audio.label', default: 'Audio'), audioInstance.id])
            redirect(action: "show", id: audioInstance.id)
        }

    }

	def delete(Long id) {
		def audioInstance = Audio.get(id)
		if (!audioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'audio.label', default: 'Audio'), id])
			redirect(action: "list")
			return
		}

		try {
			audioInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'audio.label', default: 'Audio'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'audio.label', default: 'Audio'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(Audio, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Audio, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}


    def downloadAudio(){
        def pathName = params.audioPath
        def file = new File(request.getRealPath("/audio/"+pathName))

        if (file.exists()) {
            log.info("file exist")
            response.setContentType("audio/mpeg")
            response.setHeader("Content-disposition", "filename=${pathName}")
            response.outputStream << file.bytes
            return
        }
    }
	
}
