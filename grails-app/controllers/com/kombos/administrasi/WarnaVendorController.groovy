package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class WarnaVendorController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = WarnaVendor.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if(params."sCriteria_vendorCat"){
				//eq("vendorCat", VendorCat.findByM191Nama(params."sCriteria_vendorCat"))
                vendorCat{
                    ilike("m191Nama","%" + (params."sCriteria_vendorCat" as String) + "%")
                }

			}
	
			if(params."sCriteria_m192IDWarna"){
				ilike("m192IDWarna","%" + (params."sCriteria_m192IDWarna" as String) + "%")
			}
	
			if(params."sCriteria_m192NamaWarna"){
				ilike("m192NamaWarna","%" + (params."sCriteria_m192NamaWarna" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			
			switch(sortProperty){
                case "vendorCat":
                    vendorCat{
                        order("m191Nama",sortDir)
                    }
                    break;
                default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						vendorCat: it.vendorCat?.m191Nama,
			
						m192IDWarna: it.m192IDWarna,
			
						m192NamaWarna: it.m192NamaWarna,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[warnaVendorInstance: new WarnaVendor(params)]
	}

	def save() {
		def warnaVendorInstance = new WarnaVendor(params)

		warnaVendorInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		warnaVendorInstance?.lastUpdProcess = "INSERT"
		warnaVendorInstance?.setStaDel('0')
//        if (WarnaVendor.findByM192IDWarna(params.m192IDWarna)) {
//            flash.message = message(code: 'default.error.created.warnaVendor.message', default: 'Kode Warna sudah digunakan.')
//            render(view: "create", model: [warnaVendorInstance: warnaVendorInstance])
//            return
//        }
//        if (WarnaVendor.findByM192NamaWarna(params.m192NamaWarna)) {
//            flash.message = message(code: 'default.error.created.warnaVendor.message', default: 'Nama Warna sudah digunakan.')
//            render(view: "create", model: [warnaVendorInstance: warnaVendorInstance])
//            return
//        }
        def cek = WarnaVendor.createCriteria()
        def result = cek.list() {
            and{
                eq("vendorCat",warnaVendorInstance.vendorCat)
                or{
                    eq("m192IDWarna",warnaVendorInstance.m192IDWarna?.trim(), [ignoreCase: true])
                    eq("m192NamaWarna",warnaVendorInstance.m192NamaWarna?.trim(), [ignoreCase: true])
                }
                eq("staDel","0")
            }
        }

        if(!result){
            warnaVendorInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            warnaVendorInstance?.lastUpdProcess = "INSERT"

            if (!warnaVendorInstance.save(flush: true)) {
                render(view: "create", model: [warnaVendorInstance: warnaVendorInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.warnaVendor.error.message', default: 'Data has been used.')
            render(view: "create", model: [operationInstance: warnaVendorInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'warnaVendor.label', default: 'WarnaVendor'), warnaVendorInstance.id])
		redirect(action: "show", id: warnaVendorInstance.id)
	}

	def show(Long id) {
		def warnaVendorInstance = WarnaVendor.get(id)
		if (!warnaVendorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'warnaVendor.label', default: 'WarnaVendor'), id])
			redirect(action: "list")
			return
		}

		[warnaVendorInstance: warnaVendorInstance]
	}

	def edit(Long id) {
		def warnaVendorInstance = WarnaVendor.get(id)
		if (!warnaVendorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'warnaVendor.label', default: 'WarnaVendor'), id])
			redirect(action: "list")
			return
		}

		[warnaVendorInstance: warnaVendorInstance]
	}

	def update(Long id, Long version) {
		def warnaVendorInstance = WarnaVendor.get(id)
		if (!warnaVendorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'warnaVendor.label', default: 'WarnaVendor'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (warnaVendorInstance.version > version) {
				
				warnaVendorInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'warnaVendor.label', default: 'WarnaVendor')] as Object[],
				"Another user has updated this WarnaVendor while you were editing")
				render(view: "edit", model: [warnaVendorInstance: warnaVendorInstance])
				return
			}
		}

        def cek = WarnaVendor.createCriteria()
        def result = cek.list() {
            and{
                ne("id",id)
                eq("vendorCat",VendorCat.findById(Long.parseLong(params."vendorCat.id")))
                or{
                    eq("m192IDWarna",params.m192IDWarna?.trim(), [ignoreCase: true])
                    eq("m192NamaWarna",params.m192NamaWarna?.trim(), [ignoreCase: true])
                }
                eq("staDel","0")
            }
        }
        if(!result){
            warnaVendorInstance.properties = params
            warnaVendorInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            warnaVendorInstance?.lastUpdProcess = "UPDATE"

            if (!warnaVendorInstance.save(flush: true)) {
                render(view: "edit", model: [warnaVendorInstance: warnaVendorInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.update.warnaVendor.error.message', default: 'Duplicate Data Warna Vendor')
            render(view: "edit", model: [warnaVendorInstance: warnaVendorInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'warnaVendor.label', default: 'Warna Vendor'), warnaVendorInstance.id])
		redirect(action: "show", id: warnaVendorInstance.id)
	}

	def delete(Long id) {
		def warnaVendorInstance = WarnaVendor.get(id)
		if (!warnaVendorInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'warnaVendor.label', default: 'WarnaVendor'), id])
			redirect(action: "list")
			return
		}

		try {
			warnaVendorInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			warnaVendorInstance?.lastUpdProcess = "DELETE"
			warnaVendorInstance?.setStaDel('1')
			warnaVendorInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'warnaVendor.label', default: 'WarnaVendor'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'warnaVendor.label', default: 'WarnaVendor'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(WarnaVendor, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(WarnaVendor, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
