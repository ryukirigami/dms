package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.SimpleDateFormat

class UploadCustomerSatisfactionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
  def conversi =new Konversi()
    def index() {
    }

    def upload() {
        def operationInstance = null
        def requestBody = request.JSON

        String bpGr = params.bpGr
        CompanyDealer companyDealer = CompanyDealer.findById(new Long(params.companyDealer.id))
        String insertUpdate = params.insertUpdate

        TargetBP targetBP
        TargetGR targetGR
        TargetBP targetBPCek
        TargetGR targetGRCek

        requestBody.each{
            if(bpGr.equalsIgnoreCase("BP")){
                targetBP = new TargetBP()
                targetBP.m036TglBerlaku = new Date(new SimpleDateFormat("yyyy-mm-dd").parse(it.tglBerlaku).getTime())
                targetBP.companyDealer = companyDealer
                targetBP.m036YTDS = it.ytdS
                targetBP.m036YTDD = it.ytdD
                targetBP.m036ThisMonthS = it.thisMonthS
                targetBP.m036ThisMonthD = it.thisMonthD
                targetBP.m036Q1S = it.q1S
                targetBP.m036Q1D = it.q1D
                targetBP.m036Q2S = it.q2S
                targetBP.m036Q2D = it.q2D
                targetBP.m036Q3S = it.q3S
                targetBP.m036Q3D = it.q3D
                targetBP.m036Q4S = it.q4S
                targetBP.m036Q4D = it.q4D
                targetBP.m036Q5S = it.q5S
                targetBP.m036Q5D = it.q5D
                targetBP.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                targetBP.setStaDel('0')
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                targetBPCek = TargetBP.findByM036TglBerlakuAndCompanyDealer(targetBP.m036TglBerlaku,targetBP.companyDealer)
                if(!targetBPCek){
                    targetBP.lastUpdProcess = "INSERT"
                    if (!targetBP.save()) {
                        targetBP.errors.each {
                            //println it
                        }
                    }
                }else if(insertUpdate.equalsIgnoreCase("REPLACE")){
                    targetBPCek.m036YTDS = it.ytdS
                    targetBPCek.m036YTDD = it.ytdD
                    targetBPCek.m036ThisMonthS = it.thisMonthS
                    targetBPCek.m036ThisMonthD = it.thisMonthD
                    targetBPCek.m036Q1S = it.q1S
                    targetBPCek.m036Q1D = it.q1D
                    targetBPCek.m036Q2S = it.q2S
                    targetBPCek.m036Q2D = it.q2D
                    targetBPCek.m036Q3S = it.q3S
                    targetBPCek.m036Q3D = it.q3D
                    targetBPCek.m036Q4S = it.q4S
                    targetBPCek.m036Q4D = it.q4D
                    targetBPCek.m036Q5S = it.q5S
                    targetBPCek.m036Q5D = it.q5D
                    targetBP.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    targetBPCek.lastUpdProcess = "UPDATE"
                    if (!targetBPCek.save()) {
                        targetBPCek.errors.each {
                            //println it
                        }
                    }
                }
            }else{
                targetGR = new TargetGR()
                targetGR.m037TglBerlaku = new Date(new SimpleDateFormat("yyyy-mm-dd").parse(it.tglBerlaku).getTime())
                targetGR.companyDealer = companyDealer
                targetGR.m037YTDS = it.ytdS
                targetGR.m037YTDD = it.ytdD
                targetGR.m037ThisMonthS = it.thisMonthS
                targetGR.m037ThisMonthD = it.thisMonthD
                targetGR.m037Q1S = it.q1S
                targetGR.m037Q1D = it.q1D
                targetGR.m037Q2S = it.q2S
                targetGR.m037Q2D = it.q2D
                targetGR.m037Q3S = it.q3S
                targetGR.m037Q3D = it.q3D
                targetGR.m037Q4S = it.q4S
                targetGR.m037Q4D = it.q4D
                targetGR.m037Q5S = it.q5S
                targetGR.m037Q5D = it.q5D

                targetGR.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                targetGR.lastUpdProcess = "INSERT"
                targetGR.setStaDel('0')
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                targetGRCek = TargetGR.findByM037TglBerlakuAndCompanyDealer(targetGR.m037TglBerlaku,targetGR.companyDealer)
                if(!targetGRCek){
                    targetGR.lastUpdProcess = "INSERT"
                    if (!targetGR.save()) {
                        targetGR.errors.each {
                            //println it
                        }
                    }
                }else if(insertUpdate.equalsIgnoreCase("REPlACE")){
                    targetGRCek.m037YTDS = it.ytdS
                    targetGRCek.m037YTDD = it.ytdD
                    targetGRCek.m037ThisMonthS = it.thisMonthS
                    targetGRCek.m037ThisMonthD = it.thisMonthD
                    targetGRCek.m037Q1S = it.q1S
                    targetGRCek.m037Q1D = it.q1D
                    targetGRCek.m037Q2S = it.q2S
                    targetGRCek.m037Q2D = it.q2D
                    targetGRCek.m037Q3S = it.q3S
                    targetGRCek.m037Q3D = it.q3D
                    targetGRCek.m037Q4S = it.q4S
                    targetGRCek.m037Q4D = it.q4D
                    targetGRCek.m037Q5S = it.q5S
                    targetGRCek.m037Q5D = it.q5D
                    targetGR.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    targetGRCek.lastUpdProcess = "UPDATE"
                    if (!targetGRCek.save()) {
                        targetGRCek.errors.each {
                            //ln it
                        }
                    }
                }
            }
        }

        flash.message = message(code: 'default.uploadCustomerSatisfaction.message', default: "Save Job Done")
//        render(view: "index", model: [operationInstance: operationInstance])
        render(view: "index")
    }

    def view() {
//        def operationInstance = new Operation(params)
        String bpGr = params.bpGr
        CompanyDealer companyDealer = CompanyDealer.findById(new Long(params.companyDealer.id))
        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1, //starts from 0
                columnMap:  [
                        //Col, Map-Key
                        'A':'tglBerlaku',
                        'B':'ytdS',
                        'C':'ytdD',
                        'D':'thisMonthS',
                        'E':'thisMonthD',
                        'F':'q1S',
                        'G':'q1D',
                        'H':'q2S',
                        'I':'q2D',
                        'J':'q3S',
                        'K':'q3D',
                        'L':'q4S',
                        'M':'q4D',
                        'N':'q5S',
                        'O':'q5D'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
//                render(view: "index", model: [operationInstance: operationInstance])
                render(view: "index")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def targetList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = targetList as JSON

//            Serial serial = null
//            Section section = null
//            KategoriJob kategoriJob = null

            String status = "0", style = ""
            org.joda.time.LocalDate tglBerlaku

            targetList?.each {
                //operationInstance = new Operation();
                if(
                        (it.tglBerlaku && it.tglBerlaku!="") &&
                        (it.ytdS && it.ytdS!="") &&
                        (it.ytdD && it.ytdD!="") &&
                        (it.thisMonthS && it.thisMonthS!="") &&
                        (it.thisMonthD && it.thisMonthD!="") &&
                        (it.q1S && it.q1S!="") &&
                        (it.q1D && it.q1D!="") &&
                        (it.q2S && it.q2S!="") &&
                        (it.q2D && it.q2D!="") &&
                        (it.q3S && it.q3S!="") &&
                        (it.q3D && it.q3D!="") &&
                        (it.q4S && it.q4S!="") &&
                        (it.q4D && it.q4D!="") &&
                        (it.q5S && it.q5S!="") &&
                        (it.q5D && it.q5D!="")
                ){
                    tglBerlaku = it.tglBerlaku

                    if(bpGr.equalsIgnoreCase("BP")){
                        if(TargetBP.findByM036TglBerlakuAndCompanyDealer(new java.sql.Date(tglBerlaku.toDate().getTime()),companyDealer)){
                            jmlhDataError++;
                            status = "1";
                        }
                    }else{
                        if(TargetGR.findByM037TglBerlakuAndCompanyDealer(new java.sql.Date(tglBerlaku.toDate().getTime()),companyDealer)){
                            jmlhDataError++;
                            status = "1";
                        }
                    }
                } else {
                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+new java.sql.Date(tglBerlaku.toDate().getTime()).format("dd/MM/yyyy")+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.ytdS)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.ytdD)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.thisMonthS)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.thisMonthD)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q1S)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q1D)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q2S)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q2D)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q3S)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q3D)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q4S)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q4D)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q5S)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.q5D)+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadCustomerSatisfactionError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadCustomerSatisfaction.message', default: "Read File Done")
        }

        render(view: "index", model: [htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError, companyDealer: params.companyDealer.id, bpGr: params.bpGr])
    }

}
