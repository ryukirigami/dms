package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PaintingApplicationTimeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PaintingApplicationTime.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m044TglBerlaku") {
                ge("m044TglBerlaku", params."sCriteria_m044TglBerlaku")
                lt("m044TglBerlaku", params."sCriteria_m044TglBerlaku" + 1)
            }

            if (params."sCriteria_masterPanel") {
			masterPanel{
				ilike("m094NamaPanel","%"+(params."sCriteria_masterPanel")+"%")
				}
            }

            if (params."sCriteria_m044NewMultiple") {
                eq("m044NewMultiple",Double.parseDouble (params."sCriteria_m044NewMultiple"))
            }

            if (params."sCriteria_m044NewUnit") {
                eq("m044NewUnit", Double.parseDouble(params."sCriteria_m044NewUnit"))
            }

            if (params."sCriteria_m044RepMultiple") {
                eq("m044RepMultiple",Double.parseDouble (params."sCriteria_m044RepMultiple"))
            }

            if (params."sCriteria_m044RepUnit") {
                eq("m044RepUnit",Double.parseDouble (params."sCriteria_m044RepUnit"))
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m044TglBerlaku: it.m044TglBerlaku ? it.m044TglBerlaku.format(dateFormat) : "",

                    masterPanel: it.masterPanel?.m094NamaPanel,

                    m044NewMultiple: it.m044NewMultiple,

                    m044NewUnit: it.m044NewUnit,

                    m044RepMultiple: it.m044RepMultiple,

                    m044RepUnit: it.m044RepUnit,

                    companyDealer: it.companyDealer,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [paintingApplicationTimeInstance: new PaintingApplicationTime(params)]
    }

    def save() {
        def paintingApplicationTimeInstance = new PaintingApplicationTime(params)
        paintingApplicationTimeInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        paintingApplicationTimeInstance?.lastUpdProcess = "INSERT"
        if (paintingApplicationTimeInstance.getM044NewMultiple()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan New Part Multiple Panel harus lebih besar dari 0.')
            render(view: "create", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if (paintingApplicationTimeInstance.getM044NewUnit()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan New Part Unit Panel harus lebih besar dari 0.')
            render(view: "create", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if (paintingApplicationTimeInstance.getM044RepMultiple()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan Repaired Multiple Panel harus lebih besar dari 0.')
            render(view: "create", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if (paintingApplicationTimeInstance.getM044RepUnit()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan Repaired Unit Panel harus lebih besar dari 0.')
            render(view: "create", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if(PaintingApplicationTime.findByMasterPanelAndM044NewMultipleAndM044NewUnitAndM044RepMultipleAndM044RepUnit(MasterPanel.findById(Long.parseLong(params?.masterPanel?.id)),params.m044NewMultiple,params.m044NewUnit,params.m044RepMultiple,params.m044RepUnit)!=null){
            flash.message = '* Data Sudah Pernah Digunakan'
            render(view: "create", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }

        if (!paintingApplicationTimeInstance.save(flush: true)) {
            render(view: "create", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'paintingApplicationTime.label', default: 'Painting Application Time'), paintingApplicationTimeInstance.id])
        redirect(action: "show", id: paintingApplicationTimeInstance.id)
    }

    def show(Long id) {
        def paintingApplicationTimeInstance = PaintingApplicationTime.get(id)
        if (!paintingApplicationTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'paintingApplicationTime.label', default: 'PaintingApplicationTime'), id])
            redirect(action: "list")
            return
        }

        [paintingApplicationTimeInstance: paintingApplicationTimeInstance]
    }

    def edit(Long id) {
        def paintingApplicationTimeInstance = PaintingApplicationTime.get(id)
        if (!paintingApplicationTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'paintingApplicationTime.label', default: 'PaintingApplicationTime'), id])
            redirect(action: "list")
            return
        }

        [paintingApplicationTimeInstance: paintingApplicationTimeInstance]
    }

    def update(Long id, Long version) {
        def paintingApplicationTimeInstance = PaintingApplicationTime.get(id)
        if (!paintingApplicationTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'paintingApplicationTime.label', default: 'PaintingApplicationTime'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (paintingApplicationTimeInstance.version > version) {

                paintingApplicationTimeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'paintingApplicationTime.label', default: 'PaintingApplicationTime')] as Object[],
                        "Another user has updated this PaintingApplicationTime while you were editing")
                render(view: "edit", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
                return
            }
        }

        paintingApplicationTimeInstance.properties = params
        paintingApplicationTimeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        paintingApplicationTimeInstance?.lastUpdProcess = "UPDATE"
        if (paintingApplicationTimeInstance.getM044NewMultiple()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan New Part Multiple Panel harus lebih besar dari 0.')
            render(view: "edit", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if (paintingApplicationTimeInstance.getM044NewUnit()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan New Part Unit Panel harus lebih besar dari 0.')
            render(view: "edit", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if (paintingApplicationTimeInstance.getM044RepMultiple()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan Repaired Multiple Panel harus lebih besar dari 0.')
            render(view: "edit", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if (paintingApplicationTimeInstance.getM044RepUnit()<=0) {
            flash.message = message(code: 'default.error.created.paintingApplicationTime.message', default: 'Inputan Repaired Unit Panel harus lebih besar dari 0.')
            render(view: "edit", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }
        if (!paintingApplicationTimeInstance.save(flush: true)) {
            render(view: "edit", model: [paintingApplicationTimeInstance: paintingApplicationTimeInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'paintingApplicationTime.label', default: 'Painting Application Time'), paintingApplicationTimeInstance.id])
        redirect(action: "show", id: paintingApplicationTimeInstance.id)
    }

    def delete(Long id) {
        def paintingApplicationTimeInstance = PaintingApplicationTime.get(id)
        if (!paintingApplicationTimeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'paintingApplicationTime.label', default: 'PaintingApplicationTime'), id])
            redirect(action: "list")
            return
        }

        try {
            paintingApplicationTimeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            paintingApplicationTimeInstance?.lastUpdProcess = "DELETE"
            paintingApplicationTimeInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'paintingApplicationTime.label', default: 'PaintingApplicationTime'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'paintingApplicationTime.label', default: 'PaintingApplicationTime'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PaintingApplicationTime, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PaintingApplicationTime, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
