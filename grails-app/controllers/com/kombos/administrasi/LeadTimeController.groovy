package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.sql.Time

class LeadTimeController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = LeadTime.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_m035TglBerlaku"){
				ge("m035TglBerlaku",params."sCriteria_m035TglBerlaku")
				lt("m035TglBerlaku",params."sCriteria_m035TglBerlaku" + 1)
			}

//			if(params."sCriteria_companyDealer"){
//				eq("companyDealer",params."sCriteria_companyDealer")
//			}

			if(params."sCriteria_m035LTReception"){
				eq("m035LTReception",params."sCriteria_m035LTReception")
			}

			if(params."sCriteria_m035LTPreDiagnose"){
				eq("m035LTPreDiagnose",params."sCriteria_m035LTPreDiagnose")
			}

			if(params."sCriteria_m035LTProduction"){
				eq("m035LTProduction",params."sCriteria_m035LTProduction")
			}

			if(params."sCriteria_m035LTIDR"){
				eq("m035LTIDR",params."sCriteria_m035LTIDR")
			}

			if(params."sCriteria_m035LTFinalInspection"){
				eq("m035LTFinalInspection",params."sCriteria_m035LTFinalInspection")
			}

			if(params."sCriteria_m035Invoicing"){
				eq("m035Invoicing",params."sCriteria_m035Invoicing")
			}

			if(params."sCriteria_m035Washing"){
				eq("m035Washing",params."sCriteria_m035Washing")
			}

			if(params."sCriteria_m035Delivery"){
				eq("m035Delivery",params."sCriteria_m035Delivery")
			}

			if(params."sCriteria_m035JobStopped"){
				eq("m035JobStopped",params."sCriteria_m035JobStopped")
			}

			if(params."sCriteria_m035WFReception"){
				eq("m035WFReception",params."sCriteria_m035WFReception")
			}

			if(params."sCriteria_m035WFJobDispatch"){
				eq("m035WFJobDispatch",params."sCriteria_m035WFJobDispatch")
			}

			if(params."sCriteria_m035WFClockOn"){
				eq("m035WFClockOn",params."sCriteria_m035WFClockOn")
			}

			if(params."sCriteria_m035WFFinalInspection"){
				eq("m035WFFinalInspection",params."sCriteria_m035WFFinalInspection")
			}

			if(params."sCriteria_m035WFInvoicing"){
				eq("m035WFInvoicing",params."sCriteria_m035WFInvoicing")
			}

			if(params."sCriteria_m035WFWashing"){
				eq("m035WFWashing",params."sCriteria_m035WFWashing")
			}

			if(params."sCriteria_m035WFNotifikasi"){
				eq("m035WFNotifikasi",params."sCriteria_m035WFNotifikasi")
			}

			if(params."sCriteria_m035WFDelivery"){
				eq("m035WFDelivery",params."sCriteria_m035WFDelivery")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m035TglBerlaku: it.m035TglBerlaku?it.m035TglBerlaku.format(dateFormat):"",
			
//						companyDealer: it.companyDealer.m011NamaWorkshop,
			
						m035LTReception: it.m035LTReception.format("HH:mm"),
			
						m035LTPreDiagnose: it.m035LTPreDiagnose.format("HH:mm"),
			
						m035LTProduction: it.m035LTProduction.format("HH:mm"),
			
						m035LTIDR: it.m035LTIDR.format("HH:mm"),
			
						m035LTFinalInspection: it.m035LTFinalInspection.format("HH:mm"),
			
						m035Invoicing: it.m035Invoicing.format("HH:mm"),
			
						m035Washing: it.m035Washing.format("HH:mm"),
			
						m035Delivery: it.m035Delivery.format("HH:mm"),
			
						m035JobStopped: it.m035JobStopped.format("HH:mm"),
			
						m035WFReception: it.m035WFReception.format("HH:mm"),
			
						m035WFJobDispatch: it.m035WFJobDispatch.format("HH:mm"),
			
						m035WFClockOn: it.m035WFClockOn.format("HH:mm"),
			
						m035WFFinalInspection: it.m035WFFinalInspection.format("HH:mm"),
			
						m035WFInvoicing: it.m035WFInvoicing.format("HH:mm"),
			
						m035WFWashing: it.m035WFWashing.format("HH:mm"),
			
						m035WFNotifikasi: it.m035WFNotifikasi.format("HH:mm"),
			
						m035WFDelivery: it.m035WFDelivery.format("HH:mm"),
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[leadTimeInstance: new LeadTime(params)]
	}

	def save() {
        def m035TglBerlaku = Date.parse("dd/MM/yyyy", params.m035TglBerlaku_date)
        Calendar start = Calendar.getInstance();
        start.setTime(m035TglBerlaku);
        params.remove("m035TglBerlaku_date")

        def leadTimeInstance = new LeadTime()
        leadTimeInstance.setM035TglBerlaku(new java.sql.Date(start.getTimeInMillis()))
        if(LeadTime.findByM035TglBerlaku(leadTimeInstance.getM035TglBerlaku())==null){
//            leadTimeInstance?.companyDealer = CompanyDealer?.findById(Long.parseLong(params?.companyDealer?.id))
            Time m035LTReception = new Time(Integer?.parseInt(params?.m035LTReception_hour),Integer?.parseInt(params?.m035LTReception_minute),0)
            Time m035LTPreDiagnose = new Time(Integer?.parseInt(params?.m035LTPreDiagnose_hour),Integer?.parseInt(params?.m035LTPreDiagnose_minute),0)
            Time m035LTProduction = new Time(Integer?.parseInt(params?.m035LTProduction_hour),Integer?.parseInt(params?.m035LTProduction_minute),0)
            Time m035LTIDR = new Time(Integer?.parseInt(params?.m035LTIDR_hour),Integer?.parseInt(params?.m035LTIDR_minute),0)
            Time m035LTFinalInspection = new Time(Integer?.parseInt(params?.m035LTFinalInspection_hour),Integer?.parseInt(params?.m035LTFinalInspection_minute),0)
            Time m035Invoicing = new Time(Integer?.parseInt(params?.m035Invoicing_hour),Integer?.parseInt(params?.m035Invoicing_minute),0)
            Time m035Washing = new Time(Integer?.parseInt(params?.m035Washing_hour),Integer?.parseInt(params?.m035Washing_minute),0)
            Time m035Delivery = new Time(Integer?.parseInt(params?.m035Delivery_hour),Integer?.parseInt(params?.m035Delivery_minute),0)
            Time m035JobStopped = new Time(Integer?.parseInt(params?.m035JobStopped_hour),Integer?.parseInt(params?.m035JobStopped_minute),0)
            Time m035WFReception = new Time(Integer?.parseInt(params?.m035WFReception_hour),Integer?.parseInt(params?.m035WFReception_minute),0)
            Time m035WFJobDispatch = new Time(Integer?.parseInt(params?.m035WFJobDispatch_hour),Integer?.parseInt(params?.m035WFJobDispatch_minute),0)
            Time m035WFClockOn = new Time(Integer?.parseInt(params?.m035WFClockOn_hour),Integer?.parseInt(params?.m035WFClockOn_minute),0)
            Time m035WFFinalInspection = new Time(Integer?.parseInt(params?.m035WFFinalInspection_hour),Integer?.parseInt(params?.m035WFFinalInspection_minute),0)
            Time m035WFInvoicing = new Time(Integer?.parseInt(params?.m035WFInvoicing_hour),Integer?.parseInt(params?.m035WFInvoicing_minute),0)
            Time m035WFWashing = new Time(Integer?.parseInt(params?.m035WFWashing_hour),Integer?.parseInt(params?.m035WFWashing_minute),0)
            Time m035WFNotifikasi = new Time(Integer?.parseInt(params?.m035WFNotifikasi_hour),Integer?.parseInt(params?.m035WFNotifikasi_minute),0)
            Time m035WFDelivery = new Time(Integer?.parseInt(params?.m035WFDelivery_hour),Integer?.parseInt(params?.m035WFDelivery_minute),0)

            leadTimeInstance?.setM035LTReception(m035LTReception)
            leadTimeInstance?.setM035LTPreDiagnose(m035LTPreDiagnose)
            leadTimeInstance?.setM035LTProduction(m035LTProduction)
            leadTimeInstance?.setM035LTIDR(m035LTIDR)
            leadTimeInstance?.setM035LTFinalInspection(m035LTFinalInspection)
            leadTimeInstance?.setM035Invoicing(m035Invoicing)
            leadTimeInstance?.setM035Washing(m035Washing)
            leadTimeInstance?.setM035Delivery(m035Delivery)
            leadTimeInstance?.setM035JobStopped(m035JobStopped)
            leadTimeInstance?.setM035WFReception(m035WFReception)
            leadTimeInstance?.setM035WFJobDispatch(m035WFJobDispatch)
            leadTimeInstance?.setM035WFClockOn(m035WFClockOn)
            leadTimeInstance?.setM035WFFinalInspection(m035WFFinalInspection)
            leadTimeInstance?.setM035WFInvoicing(m035WFInvoicing)
            leadTimeInstance?.setM035WFWashing(m035WFWashing)
            leadTimeInstance?.setM035WFNotifikasi(m035WFNotifikasi)
            leadTimeInstance?.setM035WFDelivery(m035WFDelivery)

            leadTimeInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            leadTimeInstance?.companyDealer = session.userCompanyDealer
            leadTimeInstance.setLastUpdProcess("INSERT")
            leadTimeInstance.setStaDel('0')

            if (leadTimeInstance.save(flush: true)) {
                flash.message = message(code: 'default.created.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), leadTimeInstance.id])
                redirect(action: "show", id: leadTimeInstance.id)
            } else {
                render(view: "create", model: [leadTimeInstance: leadTimeInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.failed.message', args: [message(code: 'leadTime.label', default: 'Lead Time'), m035TglBerlaku, params?.m035TglBerlaku])
            render(view: "create", model: [leadTimeInstance : leadTimeInstance ])
            return
        }
	}

	def show(Long id) {
		def leadTimeInstance = LeadTime.get(id)
		if (!leadTimeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), id])
			redirect(action: "list")
			return
		}

		[leadTimeInstance: leadTimeInstance]
	}

	def edit(Long id) {
		def leadTimeInstance = LeadTime.get(id)
		if (!leadTimeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), id])
			redirect(action: "list")
			return
		}

		[leadTimeInstance: leadTimeInstance]
	}

	def update(Long id, Long version) {
		def leadTimeInstance = LeadTime.get(id)
		if (!leadTimeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (leadTimeInstance.version > version) {
				
				leadTimeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'leadTime.label', default: 'LeadTime')] as Object[],
				"Another user has updated this LeadTime while you were editing")
				render(view: "edit", model: [leadTimeInstance: leadTimeInstance])
				return
			}
		}

		leadTimeInstance.properties = params

		if (!leadTimeInstance.save(flush: true)) {
			render(view: "edit", model: [leadTimeInstance: leadTimeInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), leadTimeInstance.id])
		redirect(action: "show", id: leadTimeInstance.id)
	}

	def delete(Long id) {
		def leadTimeInstance = LeadTime.get(id)
		if (!leadTimeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), id])
			redirect(action: "list")
			return
		}

		try {
			leadTimeInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'leadTime.label', default: 'LeadTime'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(LeadTime, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(LeadTime, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
