package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SectionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Section.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m051ID") {
                ilike("m051ID", "%" + (params."sCriteria_m051ID" as String) + "%")
            }

            if (params."sCriteria_m051NamaSection") {
                ilike("m051NamaSection", "%" + (params."sCriteria_m051NamaSection" as String) + "%")
            }

            if (params."sCriteria_m051WarnaSection") {
                ilike("m051WarnaSection", "%" + (params."sCriteria_m051WarnaSection" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m051ID: it.m051ID,

                    m051NamaSection: it.m051NamaSection,

                    m051WarnaSection: it.m051WarnaSection,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [sectionInstance: new Section(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def sectionInstance = new Section(params)
        sectionInstance?.m051ID = generateCodeService.codeGenerateSequence("M051_ID",null)
        sectionInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        sectionInstance?.lastUpdProcess = "INSERT"
        sectionInstance?.setStaDel('0')
        if(Section.findByM051NamaSectionAndM051WarnaSectionAndStaDel(params.m051NamaSection,params.m051WarnaSection,"0")!=null){
            flash.message = "Data sudah ada"
            render(view: "create", model: [sectionInstance: sectionInstance])
            return
        }
        if (!sectionInstance.save(flush: true)) {
            render(view: "create", model: [sectionInstance: sectionInstance])
            return
        }
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("M051_ID",null,sectionInstance?.m051ID)
        flash.message = message(code: 'default.created.message', args: [message(code: 'section.label', default: 'Section'), sectionInstance.id])
        redirect(action: "show", id: sectionInstance.id)
    }

    def show(Long id) {
        def sectionInstance = Section.get(id)
        if (!sectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'section.label', default: 'Section'), id])
            redirect(action: "list")
            return
        }

        [sectionInstance: sectionInstance]
    }

    def edit(Long id) {
        def sectionInstance = Section.get(id)
        if (!sectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'section.label', default: 'Section'), id])
            redirect(action: "list")
            return
        }

        [sectionInstance: sectionInstance]
    }

    def update(Long id, Long version) {
        def sectionInstance = Section.get(id)
        if(Section.findByM051NamaSectionAndM051WarnaSectionAndStaDel(params.m051NamaSection,params.m051WarnaSection,"0")!=null){
            flash.message = "Data sudah ada"
            render(view: "create", model: [sectionInstance: sectionInstance])
            return
        }
        if (!sectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'section.label', default: 'Section'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (sectionInstance.version > version) {

                sectionInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'section.label', default: 'Section')] as Object[],
                        "Another user has updated this Section while you were editing")
                render(view: "edit", model: [sectionInstance: sectionInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        sectionInstance.properties = params
        sectionInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        sectionInstance?.lastUpdProcess = "UPDATE"

        if (!sectionInstance.save(flush: true)) {
            render(view: "edit", model: [sectionInstance: sectionInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'section.label', default: 'Section'), sectionInstance.id])
        redirect(action: "show", id: sectionInstance.id)
    }

    def delete(Long id) {
        def sectionInstance = Section.get(id)
        if (!sectionInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'section.label', default: 'Section'), id])
            redirect(action: "list")
            return
        }

        try {
            sectionInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            sectionInstance?.lastUpdProcess = "DELETE"
            sectionInstance?.staDel = "1"
            sectionInstance?.lastUpdated = datatablesUtilService?.syncTime()
            sectionInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'section.label', default: 'Section'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'section.label', default: 'Section'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Section, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Section, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
