package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class KabKotaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def kabKotaService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render kabKotaService.datatablesList(params) as JSON
    }

    def create() {
        def result = kabKotaService.create(params)

        if (!result.error)
            return [kabKotaInstance: result.kabKotaInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = kabKotaService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["KabKota", result.kabKotaInstance.id])
            redirect(action: 'show', id: result.kabKotaInstance.id)
            return
        }else if(result.error.code == "dataGanda"){
            flash.message = "Data Sudah Ada"
            render(view:'create', model:[kabKotaInstance: result.kabKotaInstance])
            return
        }

        render(view: 'create', model: [kabKotaInstance: result.kabKotaInstance])
    }

    def show(Long id) {
        def result = kabKotaService.show(params)

        if (!result.error)
            return [kabKotaInstance: result.kabKotaInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = kabKotaService.show(params)

        if (!result.error)
            return [kabKotaInstance: result.kabKotaInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = kabKotaService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["KabKota", params.id])
            redirect(action: 'show', id: params.id)
            return
        }else if(result.error.code == "dataGanda"){
            flash.message = "Data Sudah Ada"
            render(view:'edit', model:[kabKotaInstance: result.kabKotaInstance])
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [kabKotaInstance: result.kabKotaInstance.attach()])
    }

    def delete() {
        def result = kabKotaService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["KabKota", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(KabKota, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
