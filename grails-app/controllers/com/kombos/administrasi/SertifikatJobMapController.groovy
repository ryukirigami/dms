package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SertifikatJobMapController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", copy:"GET"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = SertifikatJobMap.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_sertifikat") {
                sertifikat{
                    ilike("m016NamaSertifikat","%"+params."sCriteria_sertifikat"+"%")
                }
            }

            if (params."sCriteria_section") {
                section{
                    ilike("m051NamaSection","%"+params."sCriteria_section"+"%")
                }
            }

            if (params."sCriteria_operation") {
                operation{
                    ilike("m053NamaOperation","%"+params."sCriteria_operation"+"%")
                }
            }

            switch (sortProperty) {
                case "sertifikat":
                    sertifikat{
                        order("m016NamaSertifikat",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    sertifikat: it.sertifikat.m016NamaSertifikat,

                    section: it.section.m051NamaSection,

                    operation: it.operation.m053NamaOperation,


            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [sertifikatJobMapInstance: new SertifikatJobMap(params)]
    }

    def copy(){
        String result = ""
        def sertSumber = new SertifikatJobMap()
        def sertTujuan = new SertifikatJobMap()
        def sertifikat = new Sertifikat()
        sertifikat = Sertifikat.findByM016NamaSertifikat(params.sertifikatTujuan)
        sertSumber=SertifikatJobMap.findBySertifikatAndStaDel(Sertifikat.findByM016NamaSertifikat(params.sertifikatSumber),"0")
        sertTujuan=SertifikatJobMap.findBySertifikatAndStaDel(sertifikat,"0")
        if(sertifikat!=null && sertTujuan==null){
            def sertifikatSave = new SertifikatJobMap()
            sertifikatSave?.sertifikat = sertifikat
            sertifikatSave?.companyDealer = session.userCompanyDealer
            sertifikatSave?.section = sertSumber.section
            sertifikatSave?.operation = sertSumber.operation
            sertifikatSave?.staDel = "0"
            sertifikatSave?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            sertifikatSave?.lastUpdProcess = "INSERT"
            sertifikatSave?.save(flush: true)
            result = "1";
        }
        if(sertSumber?.id==sertTujuan?.id){
            result = "-1";
        } else {
            //proses copy
            if(SertifikatJobMap.findBySertifikatAndOperationAndSectionAndStaDel(sertifikat, sertSumber.operation,sertSumber.section,"0")){
                result = "2";
            }else{
                sertTujuan?.setOperation(sertSumber.operation)
                sertTujuan?.setSection(sertSumber.section)
                result = "1";
            }

        }
        render result
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session.userCompanyDealer
        def sertifikatJobMapInstance = new SertifikatJobMap(params)
        sertifikatJobMapInstance?.setStaDel("0")
        sertifikatJobMapInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        sertifikatJobMapInstance?.setLastUpdProcess("INSERT")

        def cari = SertifikatJobMap.createCriteria().list {
            eq("staDel","0")
            sertifikat{
                eq("id",params.sertifikat.id.toLong())
            }
            operation{
                eq("id",params.operation.id.toLong())
            }
            section{
                eq("id",params.section.id.toLong())
            }
        }
        if(cari){
            flash.message = "Data sudah ada"
            render(view: "create", model: [sertifikatJobMapInstance: sertifikatJobMapInstance])
            return
        }
        if (!sertifikatJobMapInstance.save(flush: true)) {
            render(view: "create", model: [sertifikatJobMapInstance: sertifikatJobMapInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), sertifikatJobMapInstance.id])
        redirect(action: "show", id: sertifikatJobMapInstance.id)
    }

    def show(Long id) {
        def sertifikatJobMapInstance = SertifikatJobMap.get(id)
        if (!sertifikatJobMapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), id])
            redirect(action: "list")
            return
        }

        [sertifikatJobMapInstance: sertifikatJobMapInstance]
    }

    def edit(Long id) {
        def sertifikatJobMapInstance = SertifikatJobMap.get(id)
        if (!sertifikatJobMapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), id])
            redirect(action: "list")
            return
        }

        [sertifikatJobMapInstance: sertifikatJobMapInstance]
    }

    def update(Long id, Long version) {
        def sertifikatJobMapInstance = SertifikatJobMap.get(id)
        sertifikatJobMapInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        sertifikatJobMapInstance?.setLastUpdProcess("UPDATE")
        if (!sertifikatJobMapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (sertifikatJobMapInstance.version > version) {

                sertifikatJobMapInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap')] as Object[],
                        "Another user has updated this SertifikatJobMap while you were editing")
                render(view: "edit", model: [sertifikatJobMapInstance: sertifikatJobMapInstance])
                return
            }
        }
        def cari = SertifikatJobMap.createCriteria().list {
            eq("staDel","0")
            sertifikat{
                eq("id",params.sertifikat.id.toLong())
            }
            operation{
                eq("id",params.operation.id.toLong())
            }
            section{
                eq("id",params.section.id.toLong())
            }
        }

        if(cari){
            for(find in cari){
                if(id!=find.id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [sertifikatJobMapInstance: sertifikatJobMapInstance])
                    return
                }
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        sertifikatJobMapInstance.properties = params

        if (!sertifikatJobMapInstance.save(flush: true)) {
            render(view: "edit", model: [sertifikatJobMapInstance: sertifikatJobMapInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), sertifikatJobMapInstance.id])
        redirect(action: "show", id: sertifikatJobMapInstance.id)
    }

    def delete(Long id) {
        def sertifikatJobMapInstance = SertifikatJobMap.get(id)
        if (!sertifikatJobMapInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), id])
            redirect(action: "list")
            return
        }

        try {
            sertifikatJobMapInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            sertifikatJobMapInstance?.setLastUpdProcess("DELETE")
            sertifikatJobMapInstance?.setStaDel("1")
            sertifikatJobMapInstance?.lastUpdated = datatablesUtilService?.syncTime()
            sertifikatJobMapInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'sertifikatJobMap.label', default: 'SertifikatJobMap'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(SertifikatJobMap, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(SertifikatJobMap, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def getSertifikatSumber() {
        def res = [:]

        //   def result = GroupManPower.createCriteria().list(){eq("staDel","0")}
        def result = SertifikatJobMap.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.sertifikat.m016NamaSertifikat
        }
        res."options" = opts
        render res as JSON
    }

    def getSertifikat() {
        def res = [:]

        //   def result = GroupManPower.createCriteria().list(){eq("staDel","0")}
        def cari = SertifikatJobMap.findAllWhere(staDel : '0')
        def opts = []
        if(cari.size()>0){
            def result = Sertifikat.findAllWhere(staDel : '0')
            result.each {
                opts << it.m016NamaSertifikat
            }
        }
        res."options" = opts
        render res as JSON
    }


}
