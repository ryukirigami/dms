package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Goods
import com.kombos.parts.Konversi
import com.kombos.parts.Satuan
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class XPartJobMinorChangeController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
    def conversi =new Konversi()
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = XPartJobMinorChange.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_operation"){
				eq("operation",Operation.findByM053NamaOperationIlike("%"+params."sCriteria_operation"+"%"))
			}

			if(params."sCriteria_fullModelCode"){
				eq("fullModelCode",FullModelCode.findByT110FullModelCodeIlike("%"+params."sCriteria_fullModelCode"+"%"))
			}

			if(params."sCriteria_goods"){
				eq("goods",Goods.findByM111NamaIlike("%"+params."sCriteria_goods"+"%"))
			}

			if(params."sCriteria_t112Jumlah"){
				eq("t112Jumlah",params."sCriteria_t112Jumlah")
			}
			
			if(params."sCriteria_t112xThn"){
				eq("t112xThn",params."sCriteria_t112xThn")
			}
			
			if(params."sCriteria_t112xBln"){
				eq("t112xBln",params."sCriteria_t112xBln")
			}

			if(params."sCriteria_satuan"){
				eq("satuan",Satuan.findByM118KodeSatuanIlike("%"+params."sCriteria_satuan"+"%"))
			}

			if(params."sCriteria_namaProsesBP"){
				eq("namaProsesBP",NamaProsesBP.findByM190NamaProsesBPIlike("%"+params."sCriteria_namaProsesBP"+"%"))
			}

			ilike("t112xStaDel",'0')
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []

        results.each {
            rows << [

                    id: it.id,

                    operation: it.operation.m053NamaOperation,

                    fullModelCode: it.fullModelCode.t110FullModelCode,

                    goods: it.partsMapping.goods.m111Nama,

                    t112Jumlah: conversi.toRupiah(it.t112xJumlah1),

                    satuan: it.partsMapping.satuan.m118KodeSatuan,

                    namaProsesBP: it.namaProsesBP.m190NamaProsesBP,

                    m102Foto: it.partsMapping.foto,

                    m102FotoImageMime: it.partsMapping.imageMime,

                    staDel: it.t112xStaDel,

                    t112xThn: it.t112xThnBln,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[XPartJobMinorChangeInstance: new XPartJobMinorChange(params)]
	}

	def save() {
		def XPartJobMinorChangeInstance = new XPartJobMinorChange(params)
		XPartJobMinorChangeInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		XPartJobMinorChangeInstance?.lastUpdProcess = "INSERT"
		XPartJobMinorChangeInstance.staDel = '0'

		def foto = request.getFile('m102Foto')
		def notAllowContFoto = ['application/octet-stream']
		if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){

			log.info('Foto ada')
			log.info('isi foto '+request.getFile('m102Foto'))
			log.info('Jenis konten '+foto.getContentType())
			def okcontents = ['image/png', 'image/jpeg', 'image/gif']
			if (! okcontents.contains(foto.getContentType())) {
				flash.message = "Jenis gambar harus berupa: ${okcontents}"
				render(view:'edit', model:[XPartJobMinorChangeInstance: XPartJobMinorChangeInstance])
				return;
			}

			XPartJobMinorChangeInstance.m102Foto = foto.getBytes()
			XPartJobMinorChangeInstance.m102FotoImageMime = foto.getContentType()
		}
		if (!XPartJobMinorChangeInstance.save(flush: true)) {
			render(view: "create", model: [XPartJobMinorChangeInstance: XPartJobMinorChangeInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), XPartJobMinorChangeInstance.id])
		redirect(action: "show", id: XPartJobMinorChangeInstance.id)
	}

	def show(Long id) {
		def XPartJobMinorChangeInstance = XPartJobMinorChange.get(id)
		if (!XPartJobMinorChangeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), id])
			redirect(action: "list")
			return
		}

	 [XPartJobMinorChangeInstance: XPartJobMinorChangeInstance,logostamp: new Date().format("yyyyMMddhhmmss")]
	}

	def edit(Long id) {
		def XPartJobMinorChangeInstance = XPartJobMinorChange.get(id)
		if (!XPartJobMinorChangeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), id])
			redirect(action: "list")
			return
		}

		[XPartJobMinorChangeInstance: XPartJobMinorChangeInstance]
	}

	def update(Long id, Long version) {
		def XPartJobMinorChangeInstance = XPartJobMinorChange.get(id)
		if (!XPartJobMinorChangeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (XPartJobMinorChangeInstance.version > version) {
				
				XPartJobMinorChangeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange')] as Object[],
				"Another user has updated this XPartJobMinorChange while you were editing")
				render(view: "edit", model: [XPartJobMinorChangeInstance: XPartJobMinorChangeInstance])
				return
			}
		}

		XPartJobMinorChangeInstance.properties = params
		XPartJobMinorChangeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		XPartJobMinorChangeInstance?.lastUpdProcess = "UPDATE"


		def foto = request.getFile('m102Foto')
		def notAllowContFoto = ['application/octet-stream']
		if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){

			log.info('Foto ada')
			log.info('isi foto '+request.getFile('m102Foto'))
			log.info('Jenis konten '+foto.getContentType())
			def okcontents = ['image/png', 'image/jpeg', 'image/gif']
			if (! okcontents.contains(foto.getContentType())) {
				flash.message = "Jenis gambar harus berupa: ${okcontents}"
				render(view:'edit', model:[XPartJobMinorChangeInstance: XPartJobMinorChangeInstance])
				return;
			}

			XPartJobMinorChangeInstance.m102Foto = foto.getBytes()
			XPartJobMinorChangeInstance.m102FotoImageMime = foto.getContentType()
		}
		if (!XPartJobMinorChangeInstance.save(flush: true)) {
			render(view: "edit", model: [XPartJobMinorChangeInstance: XPartJobMinorChangeInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), XPartJobMinorChangeInstance.id])
		redirect(action: "show", id: XPartJobMinorChangeInstance.id)
	}

	def delete(Long id) {
		def XPartJobMinorChangeInstance = XPartJobMinorChange.get(id)
		if (!XPartJobMinorChangeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), id])
			redirect(action: "list")
			return
		}

		try {
			XPartJobMinorChangeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            XPartJobMinorChangeInstance?.lastUpdProcess = "DELETE"
            XPartJobMinorChangeInstance.staDel = '1'
            XPartJobMinorChangeInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'XPartJobMinorChange.label', default: 'XPartJobMinorChange'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteNew(XPartJobMinorChange, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(XPartJobMinorChange, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	def showFoto = {
		def XPartJobMinorChange = XPartJobMinorChange.get(params.id)

		/* if (!avatarUser || !avatarUser.avatar || !avatarUser.avatarType) {
		   response.sendError(404)
		   return;
		 }*/


		response.setContentType(XPartJobMinorChange.m102FotoImageMime)
		response.setContentLength(XPartJobMinorChange.m102Foto.size())
		OutputStream out = response.getOutputStream();
		out.write(XPartJobMinorChange.m102Foto);
		out.close();
	}
	
}
