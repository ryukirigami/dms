package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class BahanBakarController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]

		session.exportParams=params
		
		def c = BahanBakar.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_m110ID"){
				ilike("m110ID","%" + (params."sCriteria_m110ID" as String) + "%")
			}
			
	
			if(params."sCriteria_m110NamaBahanBakar"){
				ilike("m110NamaBahanBakar","%" + (params."sCriteria_m110NamaBahanBakar" as String) + "%")
			}

			
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m110ID: it.m110ID,
			
						m110NamaBahanBakar: it.m110NamaBahanBakar,
						
						createdBy: it.createdBy,
						
                        updatedBy: it.updatedBy,

                        lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[bahanBakarInstance: new BahanBakar(params)]
	}

	def save() {
		def bahanBakarInstance = new BahanBakar(params)
        def cek = BahanBakar.createCriteria()
        def result = cek.list() {
            or{
                eq("m110ID",bahanBakarInstance.m110ID?.trim(), [ignoreCase: true])
                eq("m110NamaBahanBakar",bahanBakarInstance.m110NamaBahanBakar?.trim(), [ignoreCase: true])
            }
        }

        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "create", model: [bahanBakarInstance: bahanBakarInstance])
            return
        }

		bahanBakarInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		bahanBakarInstance?.lastUpdProcess = "INSERT"

		if (!bahanBakarInstance.save(flush: true)) {
			render(view: "create", model: [bahanBakarInstance: bahanBakarInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'bahanBakar.label', default: 'Bahan Bakar'), bahanBakarInstance?.getM110NamaBahanBakar()])
		redirect(action: "show", id: bahanBakarInstance.id)
	}

	def show(Long id) {
		def bahanBakarInstance = BahanBakar.get(id)
		if (!bahanBakarInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bahanBakar.label', default: 'Bahan Bakar'), id])
			redirect(action: "list")
			return
		}

		[bahanBakarInstance: bahanBakarInstance]
	}

	def edit(Long id) {
		def bahanBakarInstance = BahanBakar.get(id)
		if (!bahanBakarInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bahanBakar.label', default: 'Bahan Bakar'), id])
			redirect(action: "list")
			return
		}

		[bahanBakarInstance: bahanBakarInstance]
	}

	def update(Long id, Long version) {
		def bahanBakarInstance = BahanBakar.get(id)
		if (!bahanBakarInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bahanBakar.label', default: 'Bahan Bakar'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (bahanBakarInstance.version > version) {
				
				bahanBakarInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'bahanBakar.label', default: 'BahanBakar')] as Object[],
				"Another user has updated this BahanBakar while you were editing")
				render(view: "edit", model: [bahanBakarInstance: bahanBakarInstance])
				return
			}
		}
        def cek = BahanBakar.createCriteria()
        def result  = cek.list() {
                eq("m110ID", params.m110ID , [ignoreCase: true])
        }

        if(result  && BahanBakar.findByM110ID(params.m110ID)?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Kode Bahan Bakar Sudah Ada')
            render(view: "edit", model: [bahanBakarInstance: bahanBakarInstance])
            return
        }

        def cek2 = BahanBakar.createCriteria()
        def result2 = cek2.list() {
            eq("m110NamaBahanBakar", params.m110NamaBahanBakar , [ignoreCase: true])
        }

        if(result2  && BahanBakar.findByM110NamaBahanBakarIlike(params.m110NamaBahanBakar)?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Bahan Bakar Sudah Ada')
            render(view: "edit", model: [bahanBakarInstance: bahanBakarInstance])
            return
        }
		bahanBakarInstance.properties = params
		bahanBakarInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		bahanBakarInstance?.lastUpdProcess = "UPDATE"
		if (!bahanBakarInstance.save(flush: true)) {
			render(view: "edit", model: [bahanBakarInstance: bahanBakarInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'bahanBakar.label', default: 'Bahan Bakar'), bahanBakarInstance.id])
		redirect(action: "show", id: bahanBakarInstance.id)
	}

	def delete(Long id) {
		def bahanBakarInstance = BahanBakar.get(id)
		if (!bahanBakarInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'bahanBakar.label', default: 'Bahan Bakar'), id])
			redirect(action: "list")
			return
		}

		try {
			bahanBakarInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'bahanBakar.label', default: 'Bahan Bakar'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'bahanBakar.label', default: 'Baha Bakar'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(BahanBakar, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(BahanBakar, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
