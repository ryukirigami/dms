package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SubJenisStallController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = SubJenisStall.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            and{
                eq("staDel", "0")
                jenisStall{
                    eq("staDel","0")
                }
            }
			if(params."sCriteria_jenisStall"){
                jenisStall{
                    ilike("m021NamaJenisStall","%" + (params."sCriteria_jenisStall" as String) + "%")
                }
			}
	
			if(params."sCriteria_m023NamaSubJenisStall"){
				ilike("m023NamaSubJenisStall","%" + (params."sCriteria_m023NamaSubJenisStall" as String) + "%")
			}
	
			if(params."sCriteria_m023ID"){
				ilike("m023ID","%" + (params."sCriteria_m023ID" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
                case "jenisStall":
                    jenisStall{
                        order("m021NamaJenisStall",sortDir)
                    }
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						jenisStall: it.jenisStall?.m021NamaJenisStall,
			
						m023NamaSubJenisStall: it.m023NamaSubJenisStall,
			
						m023ID: it.m023ID,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[subJenisStallInstance: new SubJenisStall(params)]
	}

	def save() {
		def subJenisStallInstance = new SubJenisStall(params)
        subJenisStallInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        subJenisStallInstance?.lastUpdProcess = "INSERT"
        subJenisStallInstance?.setStaDel('0')
        def cek = SubJenisStall.createCriteria().list() {
            and{
                eq("jenisStall",subJenisStallInstance.jenisStall)
                eq("m023NamaSubJenisStall",subJenisStallInstance.m023NamaSubJenisStall?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [subJenisStallInstance: subJenisStallInstance])
            return
        }
        if (!subJenisStallInstance.save(flush: true)) {
			render(view: "create", model: [subJenisStallInstance: subJenisStallInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), subJenisStallInstance.id])
		redirect(action: "show", id: subJenisStallInstance.id)
	}

	def show(Long id) {
		def subJenisStallInstance = SubJenisStall.get(id)
		if (!subJenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), id])
			redirect(action: "list")
			return
		}

		[subJenisStallInstance: subJenisStallInstance]
	}

	def edit(Long id) {
		def subJenisStallInstance = SubJenisStall.get(id)
		if (!subJenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), id])
			redirect(action: "list")
			return
		}

		[subJenisStallInstance: subJenisStallInstance]
	}

	def update(Long id, Long version) {
		def subJenisStallInstance = SubJenisStall.get(id)

		if (!subJenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (subJenisStallInstance.version > version) {
				
				subJenisStallInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'subJenisStall.label', default: 'SubJenisStall')] as Object[],
				"Another user has updated this Sub Jenis Stall while you were editing")
				render(view: "edit", model: [subJenisStallInstance: subJenisStallInstance])
				return
			}
		}

        def cek = SubJenisStall.createCriteria()
        def result = cek.list() {
            and{
                eq("jenisStall",JenisStall.findById(Long.parseLong(params.jenisStall.id)))
                eq("m023NamaSubJenisStall",params.m023NamaSubJenisStall?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [subJenisStallInstance: subJenisStallInstance])
            return
        }

		subJenisStallInstance.properties = params
        subJenisStallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        subJenisStallInstance?.lastUpdProcess = "UPDATE"

		if (!subJenisStallInstance.save(flush: true)) {
			render(view: "edit", model: [subJenisStallInstance: subJenisStallInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), subJenisStallInstance.id])
		redirect(action: "show", id: subJenisStallInstance.id)
	}

	def delete(Long id) {
		def subJenisStallInstance = SubJenisStall.get(id)
		if (!subJenisStallInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), id])
			redirect(action: "list")
			return
		}

		try {
            subJenisStallInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            subJenisStallInstance?.lastUpdProcess = "DELETE"
            subJenisStallInstance?.setStaDel('1')
            subJenisStallInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subJenisStall.label', default: 'Sub Jenis Stall'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(SubJenisStall, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(SubJenisStall, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
