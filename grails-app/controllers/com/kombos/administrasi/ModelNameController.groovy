package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ModelNameController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = ModelName.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            if (params."sCriteria_m104ID") {
                eq("m104ID", params."sCriteria_m104ID")
            }

            if (params."sCriteria_baseModel") {
                baseModel{
                    ilike("m102NamaBaseModel","%"+ (params."sCriteria_baseModel") +"%")
                }
            }

            if (params."sCriteria_m104KodeModelName") {
                ilike("m104KodeModelName", "%" + (params."sCriteria_m104KodeModelName" as String) + "%")
            }

            if (params."sCriteria_m104NamaModelName") {
                ilike("m104NamaModelName", "%" + (params."sCriteria_m104NamaModelName" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }


            switch (sortProperty) {
                case "baseModel":
                    baseModel{
                        order("m102KodeBaseModel",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m104ID: it.m104ID,

                    baseModel: it.baseModel?.m102NamaBaseModel+" - "+it.baseModel?.m102KodeBaseModel,

                    m104KodeModelName: it.m104KodeModelName.toUpperCase(),

                    m104NamaModelName: it.m104NamaModelName.toUpperCase(),

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [modelNameInstance: new ModelName(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def modelNameInstance = new ModelName(params)
        modelNameInstance?.setStaDel("0")
        modelNameInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        modelNameInstance?.setLastUpdProcess("INSERT")

        def c = ModelName.createCriteria()
        def result = c.list() {
            eq("staDel","0")
            eq("m104KodeModelName" , modelNameInstance?.m104KodeModelName?.trim(), [ignoreCase: true])
            eq("m104NamaModelName" , modelNameInstance?.m104NamaModelName?.trim(), [ignoreCase: true])
            eq("baseModel" , modelNameInstance?.baseModel)
        }
        if (result) {
            flash.message = "Data Sudah Ada"
            render(view: "create", model: [modelNameInstance: modelNameInstance])
            return
        }

        if (!modelNameInstance.save(flush: true)) {
            render(view: "create", model: [modelNameInstance: modelNameInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'modelName.label', default: 'ModelName'), modelNameInstance.id])
        redirect(action: "show", id: modelNameInstance.id)
    }

    def show(Long id) {
        def modelNameInstance = ModelName.get(id)
        if (!modelNameInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'modelName.label', default: 'ModelName'), id])
            redirect(action: "list")
            return
        }

        [modelNameInstance: modelNameInstance]
    }

    def edit(Long id) {
        def modelNameInstance = ModelName.get(id)
        if (!modelNameInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'modelName.label', default: 'ModelName'), id])
            redirect(action: "list")
            return
        }

        [modelNameInstance: modelNameInstance]
    }

    def update(Long id, Long version) {
        def modelNameInstance = ModelName.get(id)
        modelNameInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        modelNameInstance?.setLastUpdProcess("UPDATE")
        if (!modelNameInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'modelName.label', default: 'ModelName'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (modelNameInstance.version > version) {

                modelNameInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'modelName.label', default: 'ModelName')] as Object[],
                        "Another user has updated this ModelName while you were editing")
                render(view: "edit", model: [modelNameInstance: modelNameInstance])
                return
            }
        }

        def c = ModelName.createCriteria()
        def result = c.list() {
            eq("staDel","0")
            eq("m104KodeModelName" , params.m104KodeModelName.trim(), [ignoreCase: true])
            eq("m104NamaModelName" , params.m104NamaModelName.trim(), [ignoreCase: true])
            eq("baseModel",BaseModel.findById(params.baseModel?.id))
        }
        if (result) {
            for(find in result){
                if(id!=find.id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [modelNameInstance: modelNameInstance])
                    return
                    break
                }
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        modelNameInstance.properties = params
        modelNameInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        modelNameInstance?.setLastUpdProcess("UPDATE")

        if (!modelNameInstance.save(flush: true)) {
            render(view: "edit", model: [modelNameInstance: modelNameInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'modelName.label', default: 'ModelName'), modelNameInstance.id])
        redirect(action: "show", id: modelNameInstance.id)
    }

    def delete(Long id) {
        def modelNameInstance = ModelName.get(id)
        if (!modelNameInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'modelName.label', default: 'ModelName'), id])
            redirect(action: "list")
            return
        }

        try {
            modelNameInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            modelNameInstance?.setLastUpdProcess("DELETE")
            modelNameInstance?.setStaDel("1")
            modelNameInstance?.lastUpdated = datatablesUtilService?.syncTime()
            modelNameInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'modelName.label', default: 'ModelName'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'modelName.label', default: 'ModelName'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(ModelName, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(ModelName, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
