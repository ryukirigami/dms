package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.StaImport
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

class UploadFullModelCodeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", uploadVinCode: "POST", viewVinCode: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
    }

    def view() {
        def fullModelCodeInstance = new FullModelCode(params)

        //handle upload file
        Map CONFIG_FULL_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'kategoriKendaraan',
                        'B':'m101NamaKategori',
                        'C':'baseModel',
                        'D':'m102NamaBaseModel',
                        'E':'stir',
                        'F':'m103NamaStir',
                        'G':'modelName',
                        'H':'m104NamaModelName',
                        'I':'bodyType',
                        'J':'m105NamaBodyType',
                        'K':'gear',
                        'L':'m106NamaGear',
                        'M':'grade',
                        'N':'m107NamaGrade',
                        'O':'engine',
                        'P':'m108NamaEngine',
                        'Q':'country',
                        'R':'m109NamaNegara',
                        'S':'formOfVehicle',
                        'T':'m114NamaFoV',
                        'U':'t110FullModelCode',
                        'V':'staImport',

                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcelFullModel");
        }

        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [fullModelCodeInstance: fullModelCodeInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def fullModelList = excelImportService.columns(workbook, CONFIG_FULL_COLUMN_MAP)

            jsonData = fullModelList as JSON

            KategoriKendaraan kategoriKendaraan = null
            BaseModel baseModel = null
            Stir stir = null
            ModelName modelName = null
            BodyType bodyType = null
            Gear gear = null
            Grade grade = null
            Engine engine = null
            Country country = null
            FormOfVehicle formOfVehicle = null
            StaImport staImport = null

            String status = "0", style = ""
            fullModelList?.each {
                if((it.kategoriKendaraan && it.kategoriKendaraan!="")
                        && (it.baseModel && it.baseModel!="")
                        && (it.stir && it.stir!="")
                        && (it.modelName && it.modelName!="")
                        && (it.bodyType && it.bodyType!="")
                        && (it.gear && it.gear!="")
                        && (it.grade && it.grade!="")
                        && (it.engine && it.engine!="")
                        && (it.country && it.country!="")
                        && (it.formOfVehicle && it.formOfVehicle!="")
                        && (it.t110FullModelCode && it.t110FullModelCode!="")
                        && (it.staImport && it.staImport!="")){
                    kategoriKendaraan = KategoriKendaraan.findByM101KodeKategori(it.kategoriKendaraan)
                    baseModel = BaseModel.findByM102KodeBaseModel(it.baseModel)
                    stir = Stir.findByM103KodeStir(it.stir)
                    modelName = ModelName.findByM104KodeModelName(it.modelName)
                    bodyType = BodyType.findByM105KodeBodyType(it.bodyType)
                    gear = Gear.findByM106KodeGear(it.gear)
                    grade = Grade.findByM107KodeGrade(it.grade)
                    engine = Engine.findByM108KodeEngine(it.engine)
                    country = Country.findByM109KodeNegara(it.country)
                    String kodeFov = it.formOfVehicle?.toString()?.replace(".0","")
                    formOfVehicle = FormOfVehicle.findByM114KodeFoV(it.kodeFov)
                    staImport = StaImport.findByM100KodeStaImport(it.staImport)

                    if(!kategoriKendaraan || !baseModel || !stir || !modelName || !bodyType
                            || !gear || !grade || !engine || !country || !formOfVehicle || !staImport){
                        jmlhDataError++;
                        status = "1";
                    }

                } else {
                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.kategoriKendaraan+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m101NamaKategori+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.baseModel+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m102NamaBaseModel+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.stir+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m103NamaStir+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.modelName+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m104NamaModelName+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.bodyType+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m105NamaBodyType+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.gear+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m106NamaGear+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.grade+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m107NamaGrade+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.engine+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m108NamaEngine+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.country+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m109NamaNegara+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.formOfVehicle+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m114NamaFoV+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t110FullModelCode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.staImport+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadFullModelCode.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadFullModelCode.message', default: "Read File Done")
        }

        render(view: "index", model: [fullModelCodeInstance: fullModelCodeInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }

    def upload() {
        def fullModelCodeInstance = null
        def requestBody = request.JSON

        KategoriKendaraan kategoriKendaraan = null
        BaseModel baseModel = null
        Stir stir = null
        ModelName modelName = null
        BodyType bodyType = null
        Gear gear = null
        Grade grade = null
        Engine engine = null
        Country country = null
        FormOfVehicle formOfVehicle = null
        StaImport staImport = null

        requestBody.each{
            fullModelCodeInstance = new FullModelCode();
            kategoriKendaraan = KategoriKendaraan.findByM101KodeKategori(it.kategoriKendaraan)
            baseModel = BaseModel.findByM102KodeBaseModel(it.baseModel)
            stir = Stir.findByM103KodeStir(it.stir)
            modelName = ModelName.findByM104KodeModelName(it.modelName)
            bodyType = BodyType.findByM105KodeBodyType(it.bodyType)
            gear = Gear.findByM106KodeGear(it.gear)
            grade = Grade.findByM107KodeGrade(it.grade)
            engine = Engine.findByM108KodeEngine(it.engine)
            country = Country.findByM109KodeNegara(it.country)
            formOfVehicle = FormOfVehicle.findByM114KodeFoV(it.formOfVehicle)
            staImport = StaImport.findByM100KodeStaImport(it.staImport)

            if(kategoriKendaraan && baseModel && stir && modelName && bodyType && gear
                    && grade && engine && country && formOfVehicle && (it.t110FullModelCode && it.t110FullModelCode!="")){
                fullModelCodeInstance.kategoriKendaraan = kategoriKendaraan
                fullModelCodeInstance.baseModel = baseModel
                fullModelCodeInstance.stir = stir
                fullModelCodeInstance.modelName = modelName
                fullModelCodeInstance.bodyType = bodyType
                fullModelCodeInstance.gear = gear
                fullModelCodeInstance.grade = grade
                fullModelCodeInstance.engine = engine
                fullModelCodeInstance.country = country
                fullModelCodeInstance.formOfVehicle = formOfVehicle
                fullModelCodeInstance.t110FullModelCode = it.t110FullModelCode
                fullModelCodeInstance.staImport = staImport
                fullModelCodeInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                fullModelCodeInstance.lastUpdProcess = "UPLOAD"
                fullModelCodeInstance.setStaDel('0')
                fullModelCodeInstance?.dateCreated = datatablesUtilService?.syncTime()
                fullModelCodeInstance?.lastUpdated = datatablesUtilService?.syncTime()
                def cek = FullModelCode.find(fullModelCodeInstance)
                if(!cek){
                    fullModelCodeInstance.save()
                }
            }
        }

        flash.message = message(code: 'default.uploadFullModelCode.message', default: "Save Full Model Code Done")
        render(view: "index", model: [fullModelCodeInstance: fullModelCodeInstance])
    }

    def viewVinCode() {
        def fullModelVinCodeInstance = new FullModelVinCode(params)

        //handle upload file
        Map CONFIG_VIN_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'t109WMI',
                        'B':'t109VDS',
                        'C':'t109CekDigit',
                        'D':'t109VIS',
                        'E':'customerVehicle',
                        'F':'fullModelCode',
                        'G':'t109ThnPembuatan',
                        'H':'t109BlnPembuatan',

                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcelVinModel");
        }

        String htmlDataVinCode = ""
        def jsonDataVinCode = ""
        int jmlhDataErrorVinCode = 0;
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [fullModelVinCodeInstance: fullModelVinCodeInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def fullModelList = excelImportService.columns(workbook, CONFIG_VIN_COLUMN_MAP)

            jsonDataVinCode = fullModelList as JSON

//            CustomerVehicle customerVehicle = null
            FullModelCode fullModelCode = null

            String status = "0", style = ""
            def styleList = [:]
            fullModelList?.each {

                if((it.customerVehicle && it.customerVehicle!="")
                        && (it.fullModelCode && fullModelCode!="")
                        && (it.t109WMI && it.t109WMI!="")
                        && (it.t109VDS && it.t109VDS!="")
                        && (it.t109CekDigit && it.t109CekDigit!="")
                        && (it.t109VIS && it.t109VIS!="")
                        && (it.t109ThnPembuatan && it.t109ThnPembuatan!="")
                        && (it.t109BlnPembuatan && it.t109BlnPembuatan!="")){

//                    customerVehicle = CustomerVehicle.findByT103VinCode(it.customerVehicle)
                    fullModelCode = FullModelCode.findByT110FullModelCode(it.fullModelCode)

//                    if(!customerVehicle || !fullModelCode){
//                        jmlhDataErrorVinCode++;
//                        status = "1";
//                    }
//                    if(!customerVehicle){
//                        jmlhDataErrorVinCode++;
//                        status = "1";
//                        styleList.customerVehicle = "style='color:red;'"

//                    }
                    if(!fullModelCode){
                        jmlhDataErrorVinCode++;
                        status = "1";
                        styleList.fullModelCode = "style='color:red;'"
                        println "eror fullModelCode"
                    }

                } else {
                    jmlhDataErrorVinCode++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }
                htmlDataVinCode+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.t109WMI+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t109VDS+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t109CekDigit+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t109VIS+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.customerVehicle+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.fullModelCode+">\n" +
                        "                                "+it.fullModelCode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t109ThnPembuatan+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.t109BlnPembuatan+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataErrorVinCode>0){
            flash.message = message(code: 'default.uploadfullModelVinCode.message', default: "Read File Done : Terdapat "+jmlhDataErrorVinCode+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadfullModelVinCode.message', default: "Read File Done")
        }

        render(view: "index", model: [fullModelVinCodeInstance: fullModelVinCodeInstance, htmlDataVinCode:htmlDataVinCode, jsonDataVinCode:jsonDataVinCode, jmlhDataErrorVinCode:jmlhDataErrorVinCode])
    }

    def uploadVinCode() {
        def fullModelVinCodeInstance = null
        def requestBody = request.JSON

        CustomerVehicle customerVehicle = null
        FullModelCode fullModelCode = null

        requestBody.each{
            fullModelVinCodeInstance = new FullModelVinCode();
            customerVehicle = CustomerVehicle.findByT103VinCode(it.customerVehicle)
            fullModelCode = FullModelCode.findByT110FullModelCode(it.fullModelCode)

            if(customerVehicle && fullModelCode && (it.t109WMI && it.t109WMI!="") && (it.t109VDS && it.t109VDS!="") && (it.t109CekDigit && it.t109CekDigit!="")
                    && (it.t109VIS && it.t109VIS!="") && (it.t109ThnPembuatan && it.t109ThnPembuatan!="") && (it.t109BlnPembuatan && it.t109BlnPembuatan!="")){
                fullModelVinCodeInstance.customerVehicle = customerVehicle
                fullModelVinCodeInstance.fullModelCode = fullModelCode
                fullModelVinCodeInstance.t109WMI = it.t109WMI
                fullModelVinCodeInstance.t109VDS = it.t109VDS
                fullModelVinCodeInstance.t109CekDigit = it.t109CekDigit
                fullModelVinCodeInstance.t109VIS = it.t109VIS
                fullModelVinCodeInstance.t109ThnBlnPembuatan = it.t109ThnPembuatan +""+ it.t109BlnPembuatan
//                fullModelVinCodeInstance.t109BlnPembuatan = t109BlnPembuatan
                fullModelVinCodeInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                fullModelVinCodeInstance.lastUpdProcess = "UPLOAD"
                fullModelVinCodeInstance.setStaDel('0')
                fullModelVinCodeInstance?.dateCreated = datatablesUtilService?.syncTime()
                fullModelVinCodeInstance?.lastUpdated = datatablesUtilService?.syncTime()
                def cek = FullModelVinCode.find(fullModelVinCodeInstance)
                if(!cek){
                    fullModelVinCodeInstance.save()
                }
            }
        }

        flash.message = message(code: 'default.uploadfullModelVinCode.message', default: "Save Full Model Vin Code Done")
        render(view: "index", model: [fullModelVinCodeInstance: fullModelVinCodeInstance])
    }
}
