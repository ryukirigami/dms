package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class AuditTrailController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def exportService
    def grailsApplication

    def list(Integer max) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy")
        if(!params.max) params.max = 10
        if(params?.format && params.format != "html"){
            response.contentType = grailsApplication.config.grails.mime.types[params.format]
            response.setHeader("Content-disposition", "attachment; filename=AuditTrail.${params.extension}")

            List fields = ["m777Tgl", "userProfile","m777NamaActivity"]
            Map labels = ["m777Tgl": "Tanggal", "userProfile": "Nama User","m777NamaActivity":"Aktifitas"]
            def formatTanggal = {
                domain, value -> return dateFormat.format(value)
            }
            Map formatters = [m777Tgl: formatTanggal]
            Map parameters = [title: "Audit Trail", "column.widths": [0.2, 0.3, 0.5]]
            exportService.export(params.format, response.outputStream, AuditTrail.createCriteria().list(params){eq("staDel","0")}, fields, labels, formatters, parameters)
        }
        [ auditTrailInstanceList: AuditTrail.createCriteria().list(params){eq("staDel","0")} ]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams = params

        def c = AuditTrail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_m777Tgl" && params."sCriteria_m777Tglakhir") {
                ge("m777Tgl", params."sCriteria_m777Tgl")
                lt("m777Tgl", params."sCriteria_m777Tglakhir" + 1)
            }

            if (params."sCriteria_m777Id") {
                eq("m777Id", params."sCriteria_m777Id")
            }

            if (params."sCriteria_userProfile") {
                userProfile{
                    eq("id", (params."sCriteria_userProfile" as Long))
                }
                //eq("userProfile", params."sCriteria_userProfile")
            }

            if (params."sCriteria_role") {
                userProfile{
                    userRole{
                        eq("id", (params."sCriteria_role" as Long))
                    }
                }
                //eq("userProfile", params."sCriteria_userProfile")
            }

            if (params."sCriteria_m777StalUD") {
                ilike("m777StalUD", "%" + (params."sCriteria_m777StalUD" as String) + "%")
            }

            if (params."sCriteria_m777NamaForm") {
                ilike("m777NamaForm", "%" + (params."sCriteria_m777NamaForm" as String) + "%")
            }

            if (params."sCriteria_m777NamaActivity") {
                ilike("m777NamaActivity", "%" + (params."sCriteria_m777NamaActivity" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }


        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m777Tgl: it.m777Tgl ? it.m777Tgl.format(dateFormat) : "",

                    m777Id: it.m777Id,

                    userProfile: it.userProfile.t001NamaPegawai+" ("+it.userProfile.t001Inisial+")"+" - "+it.userProfile.userRole.t003NamaRole,

                    m777StalUD: it.m777StalUD,

                    m777NamaForm: it.m777NamaForm,

                    m777NamaActivity: it.m777NamaActivity,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def export (){
        String reslut = "1";
        render reslut
    }

    def create() {
        [auditTrailInstance: new AuditTrail(params)]
    }

    def save() {
        def auditTrailInstance = new AuditTrail(params)
        if (!auditTrailInstance.save(flush: true)) {
            render(view: "create", model: [auditTrailInstance: auditTrailInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), auditTrailInstance.id])
        redirect(action: "show", id: auditTrailInstance.id)
    }

    def show(Long id) {
        def auditTrailInstance = AuditTrail.get(id)
        if (!auditTrailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), id])
            redirect(action: "list")
            return
        }

        [auditTrailInstance: auditTrailInstance]
    }

    def edit(Long id) {
        def auditTrailInstance = AuditTrail.get(id)
        if (!auditTrailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), id])
            redirect(action: "list")
            return
        }

        [auditTrailInstance: auditTrailInstance]
    }

    def update(Long id, Long version) {
        def auditTrailInstance = AuditTrail.get(id)
        if (!auditTrailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (auditTrailInstance.version > version) {

                auditTrailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'auditTrail.label', default: 'AuditTrail')] as Object[],
                        "Another user has updated this AuditTrail while you were editing")
                render(view: "edit", model: [auditTrailInstance: auditTrailInstance])
                return
            }
        }

        auditTrailInstance.properties = params

        if (!auditTrailInstance.save(flush: true)) {
            render(view: "edit", model: [auditTrailInstance: auditTrailInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), auditTrailInstance.id])
        redirect(action: "show", id: auditTrailInstance.id)
    }

    def delete(Long id) {
        def auditTrailInstance = AuditTrail.get(id)
        if (!auditTrailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), id])
            redirect(action: "list")
            return
        }

        try {
            auditTrailInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'auditTrail.label', default: 'AuditTrail'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(AuditTrail, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(AuditTrail, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
