package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class KelurahanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        def x = 0
		
		session.exportParams=params
		
		def c = Kelurahan.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel", "0")
			if(params."sCriteria_provinsi"){
                provinsi{
                    ilike("m001NamaProvinsi","%" + (params."sCriteria_provinsi" as String) + "%")
                }
			}

			if(params."sCriteria_kabkota"){
                kabkota{
                    ilike("m002NamaKabKota","%" + (params."sCriteria_kabkota" as String) + "%")
                }
			}

			if(params."sCriteria_kecamatan"){
                kecamatan{
                    ilike("m003NamaKecamatan","%" + (params."sCriteria_kecamatan" as String) + "%")
                }
			}
	
			if(params."sCriteria_m004NamaKelurahan"){
				ilike("m004NamaKelurahan","%" + (params."sCriteria_m004NamaKelurahan" as String) + "%")
			}
	
			if(params."sCriteria_m004ID"){
				ilike("m004ID","%" + (params."sCriteria_m004ID" as String) + "%")
			}

            if(params."sCriteria_m004KodePos"){
                ilike("m004KodePos","%" + (params."sCriteria_m004KodePos" as String) + "%")
            }
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			
			switch(sortProperty){
                case "provinsi" :
                    provinsi{
                        order("m001NamaProvinsi",sortDir)
                    }
                    break;
                case "kabkota" :
                    kabkota{
                        order("m002NamaKabKota", sortDir)
                    }
                    break;
                case "kecamatan" :
                    kecamatan{
                        order("m003NamaKecamatan", sortDir)
                    }
                    break;

				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						provinsi: it.provinsi.m001NamaProvinsi,
			
						kabkota: it.kabkota.m002NamaKabKota,
			
						kecamatan: it.kecamatan.m003NamaKecamatan,
			
						m004NamaKelurahan: it.m004NamaKelurahan,
			
						m004ID: it.m004ID,

                        m004KodePos: it.m004KodePos,
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[kelurahanInstance: null]
	}

	def save() {
		def kelurahanInstance = new Kelurahan()

        def cek = Kelurahan.createCriteria()
        def result = cek.list() {
            and{
                eq("m004NamaKelurahan",params.m004NamaKelurahan?.trim(), [ignoreCase: true])
                eq("m004KodePos",params.m004KodePos?.trim)
                eq("provinsi",Provinsi.get(params.provinsi.id))
                eq("kecamatan",Kecamatan.get(params.kecamatan.id))
                eq("kabkota",KabKota.get(params.kabkota.id))
                eq("staDel",'0')
            }
        }

        if(!result){
            kelurahanInstance?.m004KodePos = params.m004KodePos
            kelurahanInstance?.m004NamaKelurahan = params.m004NamaKelurahan
            kelurahanInstance?.provinsi = Provinsi.get(params.provinsi.id)
            kelurahanInstance?.kecamatan = Kecamatan.get(params.kecamatan.id)
            kelurahanInstance?.kabkota = KabKota.get(params.kabkota.id)
            kelurahanInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kelurahanInstance?.lastUpdProcess = "INSERT"
            kelurahanInstance?.setStaDel('0')
            if (!kelurahanInstance.save(flush: true)) {
                render(view: "create", model: [kelurahanInstance: kelurahanInstance])
                return
            }

        }else {
            flash.message = message(code: 'default.update.kelurahan.error.message', default: 'Duplicate Data Repair')
            render(view: "create", model: [kelurahanInstance: kelurahanInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), kelurahanInstance.id])
		redirect(action: "show", id: kelurahanInstance.id)
	}

	def show(Long id) {
		def kelurahanInstance = Kelurahan.get(id)
		if (!kelurahanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), id])
			redirect(action: "list")
			return
		}

		[kelurahanInstance: kelurahanInstance]
	}

	def edit(Long id) {
		def kelurahanInstance = Kelurahan.get(id)
		if (!kelurahanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), id])
			redirect(action: "list")
			return
		}

		[kelurahanInstance: kelurahanInstance]
	}

	def update(Long id, Long version) {
		def kelurahanInstance = Kelurahan.get(id)
		if (!kelurahanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (kelurahanInstance.version > version) {
				
				kelurahanInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'kelurahan.label', default: 'Kelurahan')] as Object[],
				"Another user has updated this Kelurahan while you were editing")
				render(view: "edit", model: [kelurahanInstance: kelurahanInstance])
				return
			}
		}

        def cek = Kelurahan.createCriteria()
        def result = cek.list() {
            and{
                eq("m004NamaKelurahan",params.m004NamaKelurahan?.trim(), [ignoreCase: true])
                eq("m004KodePos",params.m004KodePos)
                eq("provinsi",Provinsi.get(params.provinsi.id))
                eq("kecamatan",Kecamatan.get(params.kecamatan.id))
                eq("kabkota",KabKota.get(params.kabkota.id))
                notEqual("id", id)
                eq("staDel",'0')
            }
        }


        if(!result){
            kelurahanInstance.properties = params
            kelurahanInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kelurahanInstance?.lastUpdProcess = "UPDATE"

            if (!kelurahanInstance.save(flush: true)) {
                render(view: "edit", model: [kelurahanInstance: kelurahanInstance])
                return
            }

        }else {
            flash.message = message(code: 'default.update.kelurahan.error.message', default: 'Duplicate Data Repair')
            render(view: "edit", model: [kelurahanInstance: kelurahanInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), kelurahanInstance.id])
		redirect(action: "show", id: kelurahanInstance.id)
	}

	def delete(Long id) {
		def kelurahanInstance = Kelurahan.get(id)
		if (!kelurahanInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), id])
			redirect(action: "list")
			return
		}

		try {
            kelurahanInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kelurahanInstance?.lastUpdProcess = "DELETE"
            kelurahanInstance?.setStaDel('1')
            kelurahanInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'kelurahan.label', default: 'Kelurahan'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDel(Kelurahan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Kelurahan, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def insertProvinsi(){
        def namaProvinsi = params.namaProvinsi
        def prov = new Provinsi()
        def check = Provinsi.findByM001NamaProvinsiIlike("%"+namaProvinsi+"%")
        def res = [:]

        if(check){
            res.error = "duplicate"
            render res as JSON
        }else{
            prov.m001NamaProvinsi = namaProvinsi
            prov.dateCreated = new Date()
            prov.lastUpdated = new Date()
            prov?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            prov?.lastUpdProcess = "INSERT"
            prov?.staDel = "0"

            prov.save(flush:true)

            def provData = Provinsi.last();
            res.put("id", provData.id)
            res.put("namaProvinsi", provData.m001NamaProvinsi)
            render res as JSON

        }


    }

    def updateProvinsi(){
        def namaProvinsi = params.namaProvinsi
        def idProv = params.idProv
        def prov = Provinsi.get(idProv)
        prov.m001NamaProvinsi = namaProvinsi
        prov.lastUpdated = new Date()
        prov?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        prov?.lastUpdProcess = "UPDATED"
        prov?.staDel = "0"

        prov.save(flush:true)
        render "success updated"

    }

    def deleteProvinsi(){
        def idProv = params.idProv
        def prov = Provinsi.get(idProv)
        prov?.staDel = "1"

        prov.save(flush:true)
        render "success updated"

    }

    def insertKabKota(){
        def namaKabKota = params.namaKabKota
        def idProv = params.idProv
        def prov = Provinsi.findById(Double.parseDouble(idProv))

        def check = KabKota.findByM002NamaKabKotaIlikeAndProvinsi("%"+namaKabKota+"%",prov)
        def res = [:]

        if(check){
            res.error = "duplicate"

            render res as JSON
        }else{
            def kabKota = new KabKota()
            kabKota.m002NamaKabKota = namaKabKota
            kabKota.provinsi = prov
            kabKota?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kabKota?.lastUpdProcess = "INSERT"
            kabKota?.lastUpdated = new Date()
            kabKota.dateCreated = new Date()
            kabKota?.staDel = "0"

            kabKota?.save(flush:true)
            def kabKotaData = KabKota.last();
            res.put("id", kabKotaData.id)
            res.put("namaKabKota", kabKota.m002NamaKabKota)
            render res as JSON
        }


    }

    def insertKecamatan(){
        def namaKecamatan = params.namaKecamatan
        def idKabKota = params.idKabKota
        def idProv = params.idProv

        def check = Kecamatan.findByM003NamaKecamatanIlikeAndProvinsiAndKabKota("%"+namaKecamatan+"%", Provinsi.get(idProv), KabKota.get(idKabKota))
        def res = [:]

        if(check){
           res.error = "duplicate"
           render res as JSON
        }else{
            def kecamatan = new Kecamatan()

            kecamatan.m003NamaKecamatan = namaKecamatan
            kecamatan.kabKota = KabKota.get(idKabKota)
            kecamatan.provinsi = Provinsi.get(idProv)
            kecamatan?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kecamatan?.lastUpdProcess = "INSERT"
            kecamatan?.staDel = "0"

            kecamatan.save(flush : true)
            def kecData = Kecamatan.last();
            res.put("id", kecData.id)
            res.put("namaKecamatan", kecData.m003NamaKecamatan)
            render res as JSON
        }


    }

    def getKabupatens (){
        def idProp = params.idProp
        def provinsi = Provinsi.get(idProp)

        def kabKotaList = KabKota.findAllByProvinsi(provinsi)

        def res = []
        kabKotaList.each {
            res << [
                    id : it.id,
                    namaKabupaten : it.m002NamaKabKota
            ]

        }

        render res as JSON

    }

    def getKabupatensEdit (){

        def kelurahan = Kelurahan.get(params.idKelurahan)
      //  def kabKota = Kelurahan.findById(Double.parseDouble(params.IdKelurahan)).kabkota
        def provinsi = kelurahan.provinsi
        def kabKota = kelurahan.kabkota
        def kabKotaList = KabKota.findAllByProvinsiAndIdNotEqual(provinsi, kabKota.id)

        def res = []
        kabKotaList.each {
            res << [
                    id : it.id,
                    namaKabupaten : it.m002NamaKabKota
            ]

        }

        render res as JSON

    }


    def getKecamatansEdit(){

        def kelurahan = Kelurahan.get(params.idKelurahan)

       // def kecamatan = Kelurahan.findById(Double.parseDouble(params.idKelurahan)).kecamatan
        def kabKota = kelurahan.kabkota
        def kecamatan = kelurahan.kecamatan

        def kecamatanList = Kecamatan.createCriteria().list {
            eq("kabKota", kabKota)
            notEqual("id", kecamatan.id)
            order("m003NamaKecamatan", "asc")
        }
        def res = []
        kecamatanList.each {
            res << [
                    id : it.id,
                    namaKecamatan : it.m003NamaKecamatan
            ]
        }

        render res as JSON
    }


    def getKecamatans(){
        def idKab = params.idKab
        def kabKota = KabKota.get(Long.parseLong(idKab))

        def kecamatanList = Kecamatan.createCriteria().list {
            eq("kabKota", kabKota)
            order("m003NamaKecamatan", "asc")
        }
        def res = []
        kecamatanList.each {
            res << [
                    id : it.id,
                    namaKecamatan : it.m003NamaKecamatan
            ]
        }

        render res as JSON
    }

    def getKelurahans(){
        def idKec = params.idKec
        def kec = Kecamatan.get(idKec)

        def kelurahansList = Kelurahan.createCriteria().list {
            eq("kecamatan",kec )
            order("m004NamaKelurahan", "asc")
        }
        def res = []

        kelurahansList.each {
            res << [
                    id : it.id,
                    namaKelurahan : it.m004NamaKelurahan
            ]
        }

        render res as JSON
    }

}
