package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Goods
import com.kombos.parts.Satuan
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class XPartsMappingMinorChangeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def bulans = [[id: 0, nama: "Januari"], [id: 1, nama: "Pebruari"], [id: 2, nama: "Maret"], [id: 3, nama: "April"], [id: 4, nama: "Mei"], [id: 5, nama: "Juni"], [id: 6, nama: "Juli"], [id: 7, nama: "Agustus"], [id: 8, nama: "September"], [id: 9, nama: "Oktober"], [id: 10, nama: "Nopember"], [id: 11, nama: "Desember"]]



    def index() {

        log.info('masuk index xParts Mapping')
        redirect(action: "list", params: params)
    }

    def list(Integer max) {

    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = XPartsMappingMinorChange.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_fullModelCode") {
                eq("fullModelCode",FullModelCode.findByT110FullModelCodeIlike("%"+params."sCriteria_fullModelCode"+"%"))
            }

     /*       if (params."sCriteria_goods") {
                String kriteria = params.sCriteria_goods
                if(kriteria.contains("|")){
                    def data = kriteria.split("\\|")
                    def kriteria1 = data[0]
                    def kriteria2 = data[1]

                   goods{
                        or{
                            ilike("m111Nama", "%"+kriteria2.replaceAll(/^\s/,"")+"%")
                            ilike("m111ID","%"+kriteria1.replaceAll(/\s$/,"")+"%" )
                        }
                    }
                }else{
                    goods{
                        or{
                            ilike("m111Nama", "%"+params.sCriteria_goods+"%")
                            ilike("m111ID","%"+params.sCriteria_goods+"%" )
                        }
                    }
                }
            }
*/

            if (params."sCriteria_goods") {

                goods{
                        ilike("m111ID", "%"+params.sCriteria_goods+"%")
                }

            }

            if (params."sCriteria_goods2") {

                goods{
                    ilike("m111Nama", "%"+params.sCriteria_goods2+"%")
                }

            }
                if (params."sCriteria_t111xStaIO") {
                ilike("t111xStaIO", "%" + (params."sCriteria_t111xStaIO" as String) + "%")


            }

            if (params."sCriteria_t111xJumlah1") {
                eq("t111xJumlah1", Double.parseDouble(params."sCriteria_t111xJumlah1"))
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_gambar") {
                eq("gambar", params."sCriteria_gambar")
            }

            if (params."sCriteria_imageMime") {
                ilike("imageMime", "%" + (params."sCriteria_imageMime" as String) + "%")
            }

            if (params."sCriteria_t111xThn") {
                ilike("t111xThn", "%" + (params."sCriteria_t111xThn" as String) + "%")
            }

            if (params."sCriteria_t111xBln") {
                ilike("t111xBln", "%" + (params."sCriteria_t111xBln" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_satuan") {
               satuan{
                   ilike("m118Satuan1",  "%"+params."sCriteria_satuan"+"%")

                }
           //     eq("satuan", Satuan.findByM118KodeSatuanIlike("%"+params."sCriteria_satuan"+"%"))
            }

            eq("staDel","0")

            switch (sortProperty) {
                case "fullModelCode" :
                    fullModelCode{
                        order("t110FullModelCode",sortDir)
                    }
                    break;
                    case "goods" :
                        goods{
                            order("m111ID",sortDir)
                        }
                    break;
                    case "goods2" :
                        goods{
                            order("m111Nama", sortDir)
                        }
                    break;
                    default:
                         order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    fullModelCode: it.fullModelCode.t110FullModelCode,

                    goods: it.goods?.m111ID,

                    goods2: it.goods?.m111Nama,

                    t111xStaIO: it.t111xStaIO,

                    t111xJumlah1: it.t111xJumlah1,

                    staDel: it.staDel,

                    gambar: it.gambar,

                    imageMime: it.imageMime,

                    t111xThn: it.t111xThn,

                    t111xBln: it.t111xBln,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    satuan: it.satuan?.m118Satuan1

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        def tahuns = new ArrayList()
        for (int i = Calendar.getInstance().get(Calendar.YEAR) - 20; i < Calendar.getInstance().get(Calendar.YEAR) + 2; i++) {
            if (i < 2009) {
                continue;
            }
            tahuns.add(i);
        }

        [XPartsMappingMinorChangeInstance: new XPartsMappingMinorChange(params),tahun: Calendar.getInstance().get(Calendar.YEAR), bulans: bulans, bulan: Calendar.getInstance().get(Calendar.MONTH), tahuns: tahuns]

    }

    def save() {
        def XPartsMappingMinorChangeInstance = new XPartsMappingMinorChange(params)

        XPartsMappingMinorChangeInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        XPartsMappingMinorChangeInstance?.lastUpdProcess = "INSERT"
        XPartsMappingMinorChangeInstance.staDel = '0'

        def gambar = request.getFile('gambar')
        def notAllowContFoto = ['application/octet-stream']
        if(gambar !=null && !notAllowContFoto.contains(gambar.getContentType()) ){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(gambar.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[XPartsMappingMinorChangeInstance: XPartsMappingMinorChangeInstance])
                return;
            }

            XPartsMappingMinorChangeInstance.gambar = gambar.getBytes()
            XPartsMappingMinorChangeInstance.imageMime = gambar.getContentType()
        }
        if (!XPartsMappingMinorChangeInstance.save(flush: true)) {
            def tahuns = new ArrayList()
            for (int i = Calendar.getInstance().get(Calendar.YEAR) - 20; i < Calendar.getInstance().get(Calendar.YEAR) + 2; i++) {
                if (i < 2009) {
                    continue;
                }
                tahuns.add(i);
            }

            render(view: "create", model: [XPartsMappingMinorChangeInstance: XPartsMappingMinorChangeInstance, tahun: Calendar.getInstance().get(Calendar.YEAR), bulans: bulans, bulan: Calendar.getInstance().get(Calendar.MONTH), tahuns: tahuns] )
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'Mapping Full Model Parts Minor Change'), XPartsMappingMinorChangeInstance.id])
        redirect(action: "show", id: XPartsMappingMinorChangeInstance.id)
    }

    def show(Long id) {
        def XPartsMappingMinorChangeInstance = XPartsMappingMinorChange.get(id)
        if (!XPartsMappingMinorChangeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'XPartsMappingMinorChange'), id])
            redirect(action: "list")
            return
        }

        [XPartsMappingMinorChangeInstance: XPartsMappingMinorChangeInstance, logostamp: new Date().format("yyyyMMddhhmmss")]
    }

    def edit(Long id) {
        def XPartsMappingMinorChangeInstance = XPartsMappingMinorChange.get(id)
        if (!XPartsMappingMinorChangeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'XPartsMappingMinorChange'), id])
            redirect(action: "list")
            return
        }
        def tahuns = new ArrayList()
        for (int i = Calendar.getInstance().get(Calendar.YEAR) - 20; i < Calendar.getInstance().get(Calendar.YEAR) + 2; i++) {
            if (i < 2009) {
                continue;
            }
            tahuns.add(i);
        }

        [XPartsMappingMinorChangeInstance:XPartsMappingMinorChangeInstance,tahun: XPartsMappingMinorChangeInstance.t111xThn, bulans: bulans, bulan: XPartsMappingMinorChangeInstance.t111xBln, tahuns: tahuns]

    }

    def update(Long id, Long version) {
        def XPartsMappingMinorChangeInstance = XPartsMappingMinorChange.get((Double.parseDouble(params.id)))
        if (!XPartsMappingMinorChangeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'XPartsMappingMinorChange'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (XPartsMappingMinorChangeInstance.version > version) {

                XPartsMappingMinorChangeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'XPartsMappingMinorChange.label', default: 'XPartsMappingMinorChange')] as Object[],
                        "Another user has updated this XPartsMappingMinorChange while you were editing")
                render(view: "edit", model: [XPartsMappingMinorChangeInstance: XPartsMappingMinorChangeInstance])
                return
            }
        }

        XPartsMappingMinorChangeInstance.fullModelCode = FullModelCode.get(params.fullModelCode.id)
        XPartsMappingMinorChangeInstance.t111xBln = params.t111xBln
        XPartsMappingMinorChangeInstance.t111xThn = params.t111xThn
        XPartsMappingMinorChangeInstance.goods = Goods.get(params.goods.id)
        String parseString = params.t111xJumlah1.toString().replace(",","")
        XPartsMappingMinorChangeInstance.t111xJumlah1 = Double.parseDouble(parseString)
        XPartsMappingMinorChangeInstance.t111xStaIO = params.t111xStaIO
        XPartsMappingMinorChangeInstance.satuan = Satuan.get(params.satuan.id)
        XPartsMappingMinorChangeInstance.lastUpdProcess = "UPDATE"
        XPartsMappingMinorChangeInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()

      //  XPartsMappingMinorChangeInstance.properties = params

        def gambar = request.getFile('gambar')
        def notAllowContFoto = ['application/octet-stream']
        if(gambar !=null && !notAllowContFoto.contains(gambar.getContentType()) ){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(gambar.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit', model:[XPartsMappingMinorChangeInstance: XPartsMappingMinorChangeInstance])
                return;
            }

            XPartsMappingMinorChangeInstance.gambar = gambar.getBytes()
            XPartsMappingMinorChangeInstance.imageMime = gambar.getContentType()
        }
        if (!XPartsMappingMinorChangeInstance.save(flush: true)) {
            def tahuns = new ArrayList()
            for (int i = Calendar.getInstance().get(Calendar.YEAR) - 20; i < Calendar.getInstance().get(Calendar.YEAR) + 2; i++) {
                if (i < 2009) {
                    continue;
                }
                tahuns.add(i);
            }

            render(view: "edit", model: [XPartsMappingMinorChangeInstance: XPartsMappingMinorChangeInstance, tahun: Calendar.getInstance().get(Calendar.YEAR), bulans: bulans, bulan: Calendar.getInstance().get(Calendar.MONTH), tahuns: tahuns])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'Mapping Full Model Parts Minor Change'), XPartsMappingMinorChangeInstance.id])
        redirect(action: "show", id: XPartsMappingMinorChangeInstance.id)
    }

    def delete(Long id) {
        def XPartsMappingMinorChangeInstance = XPartsMappingMinorChange.get(id)
        XPartsMappingMinorChangeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        XPartsMappingMinorChangeInstance?.lastUpdProcess = "DELETE"

        XPartsMappingMinorChangeInstance.staDel = '1'

        if (!XPartsMappingMinorChangeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'XPartsMappingMinorChange'), id])
            redirect(action: "list")
            return
        }

        try {
            XPartsMappingMinorChangeInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'XPartsMappingMinorChange'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'XPartsMappingMinorChange.label', default: 'XPartsMappingMinorChange'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]

        try {
            datatablesUtilService.massDeleteStaDel(XPartsMappingMinorChange, params)

            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(XPartsMappingMinorChange, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def showGambar = {
        def XPartsMappingMinorChange = XPartsMappingMinorChange.get(params.id)

        response.setContentLength(XPartsMappingMinorChange.gambar.size())
        OutputStream out = response.getOutputStream();
        out.write(XPartsMappingMinorChange.gambar);
        out.close();
    }

}
