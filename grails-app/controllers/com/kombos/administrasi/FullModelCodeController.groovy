package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class FullModelCodeController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']

    def conversi =new Konversi()
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = FullModelCode.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel", "0")
			if(params."sCriteria_kategoriKendaraan"){
                kategoriKendaraan{
                    ilike("m101KodeKategori","%"+(params."sCriteria_kategoriKendaraan")+"%")
                }
			}

            if(params."sCriteria_baseModel"){
                baseModel{
                    ilike("m102KodeBaseModel","%"+ (params."sCriteria_baseModel") +"%")
                }
            }

			if(params."sCriteria_stir"){
                stir{
                    ilike("m103KodeStir","%"+(params."sCriteria_stir")+"%")
                }
			}

            if(params."sCriteria_modelName"){
                modelName{
                    ilike("m104KodeModelName","%"+ (params."sCriteria_modelName") +"%")
                }
            }

            if(params."sCriteria_bodyType"){
                bodyType{
                    ilike("m105KodeBodyType","%"+ (params."sCriteria_bodyType") +"%")
                }
            }

            if (params."sCriteria_gear") {
                gear{
                    ilike("m106KodeGear","%"+ (params."sCriteria_gear") +"%")
                }
            }

            if (params."sCriteria_grade") {
                grade{
                    ilike("m107KodeGrade","%" + (params."sCriteria_grade") + "%")
                }
            }

			if(params."sCriteria_engine"){
                engine{
                    ilike("m108KodeEngine","%"+(params."sCriteria_engine")+"%")
                }
			}

			if(params."sCriteria_country"){
				country{
                    ilike("m109KodeNegara","%"+(params."sCriteria_country")+"%")
                }
			}

			if(params."sCriteria_formOfVehicle"){
                formOfVehicle{
                    ilike("m114KodeFoV","%"+(params."sCriteria_formOfVehicle")+"%")
                }
			}

			if(params."sCriteria_staImport"){
				eq("staImport",params."sCriteria_staImport")
			}
	
			if(params."sCriteria_t110FullModelCode"){
				ilike("t110FullModelCode","%" + (params."sCriteria_t110FullModelCode" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						kategoriKendaraan: it.kategoriKendaraan?.m101KodeKategori,
			
						baseModel: it.baseModel?.m102KodeBaseModel,
			
						stir: it.stir?.m103KodeStir,
			
						modelName: it.modelName?.m104KodeModelName,
			
						bodyType: it.bodyType?.m105KodeBodyType,
			
						gear: it.gear?.m106KodeGear,
			
						grade: it.grade?.m107KodeGrade,
			
						engine: it.engine?.m108KodeEngine,
			
						country: it.country?.m109KodeNegara,
			
						formOfVehicle: it.formOfVehicle?.m114KodeFoV,
			
						staImport: it.staImport,
			
						t110FullModelCode: it.t110FullModelCode,
			
						staDel: it.staDel,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[fullModelCodeInstance: new FullModelCode(params)]
	}

	def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def fullModelCodeInstance = new FullModelCode(params)

        def cek = FullModelCode.createCriteria()
        def result = cek.list() {
            and{
                eq("t110FullModelCode",fullModelCodeInstance?.baseModel?.m102KodeBaseModel+fullModelCodeInstance?.stir?.m103KodeStir+fullModelCodeInstance?.modelName?.m104KodeModelName+fullModelCodeInstance?.bodyType?.m105KodeBodyType+fullModelCodeInstance?.gear?.m106KodeGear+fullModelCodeInstance?.grade?.m107KodeGrade+fullModelCodeInstance?.engine?.m108KodeEngine+fullModelCodeInstance?.country?.m109KodeNegara+fullModelCodeInstance?.formOfVehicle?.m114KodeFoV)
                eq("kategoriKendaraan",fullModelCodeInstance.kategoriKendaraan)
                eq("baseModel",fullModelCodeInstance.baseModel)
                eq("stir",fullModelCodeInstance.stir)
                eq("modelName",fullModelCodeInstance.modelName)
                eq("bodyType",fullModelCodeInstance.bodyType)
                eq("gear",fullModelCodeInstance.gear)
                eq("grade",fullModelCodeInstance.grade)
                eq("engine",fullModelCodeInstance.engine)
                eq("country",fullModelCodeInstance.country)
                eq("formOfVehicle",fullModelCodeInstance.formOfVehicle)
                eq("staDel","0")
                //eq("staImport",fullModelCodeInstance.staImport)
            }
        }

        if(!result){
            String FoV = fullModelCodeInstance?.formOfVehicle?.m114KodeFoV?fullModelCodeInstance?.formOfVehicle?.m114KodeFoV:''
            fullModelCodeInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            fullModelCodeInstance?.lastUpdProcess = "INSERT"
            fullModelCodeInstance?.setStaDel('0')
            fullModelCodeInstance?.setT110FullModelCode(fullModelCodeInstance?.baseModel?.m102KodeBaseModel+fullModelCodeInstance?.stir?.m103KodeStir+fullModelCodeInstance?.modelName?.m104KodeModelName+fullModelCodeInstance?.bodyType?.m105KodeBodyType+fullModelCodeInstance?.gear?.m106KodeGear+fullModelCodeInstance?.grade?.m107KodeGrade+fullModelCodeInstance?.engine?.m108KodeEngine+fullModelCodeInstance?.country?.m109KodeNegara+FoV)

            if (!fullModelCodeInstance.save(flush: true)) {
                render(view: "create", model: [fullModelCodeInstance: fullModelCodeInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.fullModelCode.error.message', default: 'Data has been used.')
            render(view: "create", model: [fullModelCodeInstance: fullModelCodeInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), fullModelCodeInstance.id])
		redirect(action: "show", id: fullModelCodeInstance.id)
	}

	def show(Long id) {
		def fullModelCodeInstance = FullModelCode.get(id)
		if (!fullModelCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), id])
			redirect(action: "list")
			return
		}

		[fullModelCodeInstance: fullModelCodeInstance]
	}

	def edit(Long id) {
		def fullModelCodeInstance = FullModelCode.get(id)
		if (!fullModelCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), id])
			redirect(action: "list")
			return
		}

		[fullModelCodeInstance: fullModelCodeInstance]
	}

	def update(Long id, Long version) {
		def fullModelCodeInstance = FullModelCode.get(id)
		if (!fullModelCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (fullModelCodeInstance.version > version) {
				
				fullModelCodeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'fullModelCode.label', default: 'Full Model Code')] as Object[],
				"Another user has updated this FullModelCode while you were editing")
				render(view: "edit", model: [fullModelCodeInstance: fullModelCodeInstance])
				return
			}
		}

        def cek = FullModelCode.createCriteria()

        def result = cek.list() {
            and{
                ne("id",id)
                //eq("t110FullModelCode",fullModelCodeInstance?.kategoriKendaraan?.m101KodeKategori+fullModelCodeInstance?.baseModel?.m102KodeBaseModel+fullModelCodeInstance?.stir?.m103KodeStir+fullModelCodeInstance?.modelName?.m104KodeModelName+fullModelCodeInstance?.bodyType?.m105KodeBodyType+fullModelCodeInstance?.gear?.m106KodeGear+fullModelCodeInstance?.grade?.m107KodeGrade+fullModelCodeInstance?.engine?.m108KodeEngine+fullModelCodeInstance?.country?.m109KodeNegara+fullModelCodeInstance?.formOfVehicle?.m114KodeFoV)
                eq("kategoriKendaraan",KategoriKendaraan.findById(Long.parseLong(params."kategoriKendaraan.id")))
                eq("baseModel",BaseModel.findById(Long.parseLong(params."baseModel.id")))
                eq("stir",Stir.findById(Long.parseLong(params."stir.id")))
                eq("modelName",ModelName.findById(Long.parseLong(params."modelName.id")))
                eq("bodyType",BodyType.findById(Long.parseLong(params."bodyType.id")))
                eq("gear",Gear.findById(Long.parseLong(params."gear.id")))
                eq("grade",Grade.findById(Long.parseLong(params."grade.id")))
                eq("engine",Engine.findById(Long.parseLong(params."engine.id")))
                eq("country",Country.findById(Long.parseLong(params."country.id")))
                eq("formOfVehicle",FormOfVehicle.findById(Long.parseLong(params."formOfVehicle.id")))
                eq("staDel","0")
            }
        }

        if(!result){
            params?.lastUpdated = datatablesUtilService?.syncTime()
            fullModelCodeInstance.properties = params
            fullModelCodeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            fullModelCodeInstance?.lastUpdProcess = "UPDATE"
            if (!fullModelCodeInstance.save(flush: true)) {
                render(view: "edit", model: [fullModelCodeInstance: fullModelCodeInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.update.tariTwc.error.message', default: 'Duplicate Data Full Model Code')
            render(view: "edit", model: [fullModelCodeInstance: fullModelCodeInstance])
            return
        }


        flash.message = message(code: 'default.updated.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), fullModelCodeInstance.id])
		redirect(action: "show", id: fullModelCodeInstance.id)
	}

	def delete(Long id) {
		def fullModelCodeInstance = FullModelCode.get(id)
		if (!fullModelCodeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), id])
			redirect(action: "list")
			return
		}

		try {
            fullModelCodeInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            fullModelCodeInstance?.lastUpdProcess = "DELETE"
			fullModelCodeInstance.setStaDel('1')
            fullModelCodeInstance?.lastUpdated = datatablesUtilService?.syncTime()
            fullModelCodeInstance.save(flush:true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'fullModelCode.label', default: 'Full Model Code'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(FullModelCode, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(FullModelCode, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
