package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MasterWacController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def conversi=new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = MasterWac.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m403Nomor") {
                eq("m403Nomor", Integer.parseInt(params."sCriteria_m403Nomor"))
            }

            if (params."sCriteria_m403KondisiWAC") {
                eq("m403KondisiWAC", params."sCriteria_m403KondisiWAC")
            }

            if (params."sCriteria_m403NamaPerlengkapan") {
                ilike("m403NamaPerlengkapan", "%" + (params."sCriteria_m403NamaPerlengkapan" as String) + "%")
            }

            if (params."sCriteria_m403JmlhMaksItemWAC") {
                eq("m403JmlhMaksItemWAC",Double.parseDouble(params."sCriteria_m403JmlhMaksItemWAC"))
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m403Nomor: it.m403Nomor,

                    m403KondisiWAC: it.m403KondisiWAC,

                    m403NamaPerlengkapan: it.m403NamaPerlengkapan,

                    m403JmlhMaksItemWAC: conversi.toRupiah(it.m403JmlhMaksItemWAC),

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [masterWacInstance: new MasterWac(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def masterWacInstance = new MasterWac(params)

        def kondisiWacPic = request.getFile('m403KondisiWAC')

        def noComa = params.m403Nomor.replace(",","");
        def noComaJmlhMax = params.m403JmlhMaksItemWAC.replace(",","")


        def cek = MasterWac.createCriteria()
        def result = cek.list() {
            and{
                eq("m403Nomor",Integer.parseInt(noComa))
                eq("m403NamaPerlengkapan", params.m403NamaPerlengkapan)
                eq("m403JmlhMaksItemWAC", Double.parseDouble(noComaJmlhMax))
            }


        }

        if(result){
            flash.message = "Data sudah tersedia (Duplikat)"
            render(view:'create',model : [masterWacInstance: masterWacInstance])
            return;

        }

        def notAllowCont = ['application/octet-stream']

        if(kondisiWacPic !=null && !kondisiWacPic.getContentType().contains(notAllowCont)){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(kondisiWacPic.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'create',model : [masterWacInstance: masterWacInstance])
                return;
            }

            masterWacInstance.m403KondisiWAC = kondisiWacPic.getBytes()
            masterWacInstance.imageMime = kondisiWacPic.getContentType()

        }

        masterWacInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        masterWacInstance?.lastUpdProcess = "INSERT"

        if (!masterWacInstance.save(flush: true)) {
            render(view: "create", model: [masterWacInstance: masterWacInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), masterWacInstance.id])
        redirect(action: "show", id: masterWacInstance.id)
    }

    def show(Long id) {
        def masterWacInstance = MasterWac.get(id)
        if (!masterWacInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), id])
            redirect(action: "list")
            return
        }

        [masterWacInstance: masterWacInstance, logostamp: new Date().format("yyyyMMddhhmmss")]
    }

    def edit(Long id) {
        def masterWacInstance = MasterWac.get(id)
        if (!masterWacInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), id])
            redirect(action: "list")
            return
        }

        [masterWacInstance: masterWacInstance]
    }

    def update(Long id, Long version) {
        def masterWacInstance = MasterWac.get(Double.parseDouble(params.id))
        if (!masterWacInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), id])
            redirect(action: "list")
            return
        }


        def noComa = params.m403Nomor.replace(",","");
        def noComaJmlhMax = params.m403JmlhMaksItemWAC.replace(",","")


        def cek = MasterWac.createCriteria()
        def result = cek.list() {
            and{
                eq("m403Nomor",Integer.parseInt(noComa))
                eq("m403NamaPerlengkapan", params.m403NamaPerlengkapan)
                eq("m403JmlhMaksItemWAC", Double.parseDouble(noComaJmlhMax))
                notEqual("id", Long.parseLong(params.id))
            }


        }

        if(result){
            flash.message = "Data sudah tersedia (Duplikat)"
            render(view:'edit',model : [masterWacInstance: masterWacInstance])
            return;

        }
        masterWacInstance.m403JmlhMaksItemWAC = Double.parseDouble(params.m403JmlhMaksItemWAC.replace(",",""))
        String nomorNoComa = params.m403Nomor.replace(",","")
        masterWacInstance.m403Nomor = Integer.parseInt(nomorNoComa)
        masterWacInstance.m403NamaPerlengkapan = params.m403NamaPerlengkapan
        masterWacInstance?.lastUpdated = datatablesUtilService?.syncTime()
        masterWacInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        masterWacInstance?.lastUpdProcess = "UPDATE"

        def kondisiWacPic = request.getFile('m403KondisiWAC')

        def notAllowCont = ['application/octet-stream']
        if(kondisiWacPic !=null && !kondisiWacPic.getContentType().contains(notAllowCont)){
            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(kondisiWacPic.getContentType())) {
                flash.message = "Jenis gambar harus berupa: ${okcontents}"
                render(view:'edit',model : [masterWacInstance: masterWacInstance])
                return;
            }

            masterWacInstance.m403KondisiWAC = kondisiWacPic.getBytes()
            masterWacInstance.imageMime = kondisiWacPic.getContentType()

        }



        if (version != null) {
            if (masterWacInstance.version > version) {

                masterWacInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'masterWac.label', default: 'MasterWac')] as Object[],
                        "Another user has updated this MasterWac while you were editing")
                render(view: "edit", model: [masterWacInstance: masterWacInstance])
                return
            }
        }

      //  masterWacInstance.properties = params

        if (!masterWacInstance.save(flush: true)) {
            render(view: "edit", model: [masterWacInstance: masterWacInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), masterWacInstance.id])
        redirect(action: "show", id: masterWacInstance.id)
    }

    def delete(Long id) {
        def masterWacInstance = MasterWac.get(id)
        if (!masterWacInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), id])
            redirect(action: "list")
            return
        }

        try {
            masterWacInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'masterWac.label', default: 'MasterWac'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MasterWac, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(MasterWac, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def showKondisiWac = {
        def masterWac = MasterWac.get(params.id)

        response.setContentLength(masterWac.m403KondisiWAC.size())
        OutputStream out = response.getOutputStream();
        out.write(masterWac.m403KondisiWAC);
        out.close();
    }

}
