package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class TarifPerJamController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def conversi =new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = TarifPerJam.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if (params."sCriteria_companyDealer") {
                    companyDealer{
                        ilike("m011NamaWorkshop","%"+ (params."sCriteria_companyDealer") + "%")
                    }
                }
            }else{
                eq("companyDealer",session?.userCompanyDealer)
            }

            if (params."sCriteria_kategori") {
                kategori{
                    ilike("m101NamaKategori","%"+ (params."sCriteria_kategori") +"%")
                }
            }

            if (params."sCriteria_t152TMT") {
                ge("t152TMT", params."sCriteria_t152TMT")
                lt("t152TMT", params."sCriteria_t152TMT" + 1)
            }

            if (params."sCriteria_t152TarifPerjamSB") {
                eq("t152TarifPerjamSB",Double.parseDouble(params."sCriteria_t152TarifPerjamSB"))
            }

            if (params."sCriteria_t152TarifPerjamGR") {
                eq("t152TarifPerjamGR",Double.parseDouble (params."sCriteria_t152TarifPerjamGR"))
            }

            if (params."sCriteria_t152TarifPerjamBP") {
                eq("t152TarifPerjamBP",Double.parseDouble(params."sCriteria_t152TarifPerjamBP"))
            }

            if (params."sCriteria_t152TarifPerjamSBI") {
                eq("t152TarifPerjamSBI",Double.parseDouble(params."sCriteria_t152TarifPerjamSBI"))
            }

            if (params."sCriteria_t152TarifPerjamPDS") {
                eq("t152TarifPerjamPDS",Double.parseDouble(params."sCriteria_t152TarifPerjamPDS"))
            }

            if (params."sCriteria_t152TarifPerjamSPO") {
                eq("t152TarifPerjamSPO",Double.parseDouble(params."sCriteria_t152TarifPerjamSPO"))
            }

            eq("staDel", "0")

            companyDealer{
                eq("staDel", "0")
            }



            switch (sortProperty) {
                case "companyDealer" :
                    companyDealer{
                        order("m011NamaWorkshop", sortDir)
                    }
                    break;
                case "kategori" :
                    kategori{
                        order("m101NamaKategori", sortDir)

                    }

                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it?.companyDealer?.m011NamaWorkshop,

                    kategori: it?.kategori?.m101NamaKategori,

                    t152TMT: it?.t152TMT ? it?.t152TMT?.format(dateFormat) : "",

                    t152TarifPerjamSB: conversi.toRupiah(it?.t152TarifPerjamSB),

                    t152TarifPerjamGR: conversi.toRupiah(it?.t152TarifPerjamGR),

                    t152TarifPerjamBP: conversi.toRupiah(it?.t152TarifPerjamBP),

                    t152TarifPerjamSBI: conversi.toRupiah(it?.t152TarifPerjamSBI),

                    t152TarifPerjamPDS: conversi.toRupiah(it?.t152TarifPerjamPDS),

                    t152TarifPerjamSPO: conversi.toRupiah(it?.t152TarifPerjamSPO),

                    t152TarifPerjamDealer: conversi.toRupiah(it?.t152TarifPerjamDealer),

                    staDel: it?.staDel,
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [tarifPerJamInstance: new TarifPerJam(params)]
    }

    def save() {

        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        params.t152TarifPerjamGR = params.t152TarifPerjamGR?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamSB = params.t152TarifPerjamSB?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamBP = params.t152TarifPerjamBP?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamPDS = params.t152TarifPerjamPDS?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamSBI = params.t152TarifPerjamSBI?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamDealer = params.t152TarifPerjamDealer?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamSPO = params.t152TarifPerjamSPO?.toString()?.replace(",","").toDouble()
        def tarifPerJamInstance = new TarifPerJam(params)
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")


        tarifPerJamInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        tarifPerJamInstance.lastUpdProcess = "INSERT"
        tarifPerJamInstance.staDel = '0'

        def cek = TarifPerJam.createCriteria()
        def result = cek.list() {
            and{
                eq("companyDealer",CompanyDealer.get(params.companyDealer.id))
                eq("kategori", KategoriKendaraan.get(params.kategori.id))
                eq("t152TMT",df.parse(params.t152TMT_dp))
                eq("staDel", "0")
                //eq("m053JobsId",operationInstance.m053JobsId?.trim(), [ignoreCase: true])
             //   eq("m164KmWarranty",Double.parseDouble(params.m164KmWarranty))
            }
        }

        if(!result){
            tarifPerJamInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            tarifPerJamInstance.setLastUpdProcess("INSERT")
            if (!tarifPerJamInstance.save(flush: true)) {
                render(view: "create", model: [tarifPerJamInstance: tarifPerJamInstance])
                return
            }

        } else {
            flash.message = message(code: 'default.created.error.message', default: 'Data has been used.')
            render(view: "create", model: [tarifPerJamInstance: tarifPerJamInstance])
            return
        }


        flash.message = message(code: 'default.created.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), tarifPerJamInstance.id])
        redirect(action: "show", id: tarifPerJamInstance.id)
    }

    def show(Long id) {
        def tarifPerJamInstance = TarifPerJam.get(id)
        if (!tarifPerJamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), id])
            redirect(action: "list")
            return
        }

        [tarifPerJamInstance: tarifPerJamInstance]
    }

    def edit(Long id) {
        def tarifPerJamInstance = TarifPerJam.get(id)
        if (!tarifPerJamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), id])
            redirect(action: "list")
            return
        }

        [tarifPerJamInstance: tarifPerJamInstance]
    }

    def update(Long id, Long version) {
        params.t152TarifPerjamGR = params.t152TarifPerjamGR?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamSB = params.t152TarifPerjamSB?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamBP = params.t152TarifPerjamBP?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamPDS = params.t152TarifPerjamPDS?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamSBI = params.t152TarifPerjamSBI?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamDealer = params.t152TarifPerjamDealer?.toString()?.replace(",","").toDouble()
        params.t152TarifPerjamSPO = params.t152TarifPerjamSPO?.toString()?.replace(",","").toDouble()
        def tarifPerJamInstance = TarifPerJam.get(id)
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")

        if (!tarifPerJamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (tarifPerJamInstance.version > version) {

                tarifPerJamInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'tarifPerJam.label', default: 'TarifPerJam')] as Object[],
                        "Another user has updated this TarifPerJam while you were editing")
                render(view: "edit", model: [tarifPerJamInstance: tarifPerJamInstance])
                return
            }
        }


        def cek = TarifPerJam.createCriteria()
        def result = cek.list() {
            and{
                eq("companyDealer",CompanyDealer.get(params.companyDealer.id))
                eq("kategori", KategoriKendaraan.get(params.kategori.id))
                eq("t152TMT",df.parse(params.t152TMT_dp))
                eq("staDel", "0")
                notEqual("id", tarifPerJamInstance.id)
            }


        }

        if(!result){
            params?.lastUpdated = datatablesUtilService?.syncTime()
            tarifPerJamInstance.properties = params
            params.t152TarifPerjamGR = params.t152TarifPerjamGR?.toString()?.replace(",","").toDouble()
            params.t152TarifPerjamSB = params.t152TarifPerjamSB?.toString()?.replace(",","").toDouble()
            params.t152TarifPerjamBP = params.t152TarifPerjamBP?.toString()?.replace(",","").toDouble()
            params.t152TarifPerjamPDS = params.t152TarifPerjamPDS?.toString()?.replace(",","").toDouble()
            params.t152TarifPerjamSBI = params.t152TarifPerjamSBI?.toString()?.replace(",","").toDouble()
            params.t152TarifPerjamDealer = params.t152TarifPerjamDealer?.toString()?.replace(",","").toDouble()
            params.t152TarifPerjamSPO = params.t152TarifPerjamSPO?.toString()?.replace(",","").toDouble()
            tarifPerJamInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            tarifPerJamInstance.setLastUpdProcess("UPDATE")

            if (!tarifPerJamInstance.save(flush: true)) {
                render(view: "edit", model: [tarifPerJamInstance: tarifPerJamInstance])
                return
            }

        }else {
            flash.message = message(code: 'default.update.tarifPerJam.error.message', default: 'Duplicate Data Repair')
            render(view: "edit", model: [tarifPerJamInstance : tarifPerJamInstance])
            return
        }


        flash.message = message(code: 'default.updated.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), tarifPerJamInstance.id])
        redirect(action: "show", id: tarifPerJamInstance.id)
    }

    def delete(Long id) {
        def tarifPerJamInstance = TarifPerJam.get(id)
        if (!tarifPerJamInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), id])
            redirect(action: "list")
            return
        }

        tarifPerJamInstance.staDel = "1"

        try {
            tarifPerJamInstance?.lastUpdated = datatablesUtilService?.syncTime()
            tarifPerJamInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tarifPerJam.label', default: 'TarifPerJam'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDel(TarifPerJam, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(TarifPerJam, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
