package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Goods
import com.kombos.parts.Konversi
import com.kombos.parts.Satuan
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

class UploadPartJobController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def conversi=new Konversi()
    def index() {
        //redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }



    def view() {
        def partJobInstance = new PartJob(params)

        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'full',
                        'B':'job',
                        'C':'proses',
                        'D':'kode',
                        'E':'nama',
                        'F':'jumlah',
                        'G':'satuan',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        def jsonData = ""
        int jmlhDataError = 0;
        String htmlData = ""
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [partJobInstance: partJobInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = jobList as JSON
            FullModelCode fullModelCode = null
            Goods goods= null
            NamaProsesBP namaProsesBP = null
            Operation operation = null
            Satuan satuan = null

            String  status = "0", style1 = "",style2 = "",style3 = "",style4 = "",style5 = "",style6 = "",style7 = ""
            jobList?.each {
                style1 = ""
                style2 = ""
                style3 = ""
                style4 = ""
                style5 = ""
                style6 = ""
                style7 = ""
                try {
                    fullModelCode = FullModelCode.findByT110FullModelCode(it.full)
                    namaProsesBP = NamaProsesBP.findByM190NamaProsesBP(it.proses)
                    goods = Goods.findByM111Nama(it.nama)
                    operation = Operation.findByM053NamaOperation(it.job)
                        satuan = Satuan.findByM118KodeSatuan(it.satuan)
                }catch(Exception e){

                }
                if((it.full && it.full!="") && (it.job && it.job!="") && (it.proses && it.proses!="") && (it.kode && it.kode !="") && (it.nama && it.nama!="") &&
                        (it.jumlah && it.jumlah!="") && (it.satuan && it.satuan!="")){

                    if(!namaProsesBP || !operation || !goods || !satuan  ){
                        jmlhDataError++;

                    }

                    if(!fullModelCode){
                        style1 =  "style='color:gold'"
                    }
                    if(!namaProsesBP){
                        style3 =  "style='color:gold'"
                    }
                    if(!operation){
                        style2 =  "style='color:gold'"
                    }
                    if(!goods){
                        style5 =  "style='color:gold'"
                        style4 =  "style='color:gold'"
                    }
                    if(!satuan){
                        style7 =  "style='color:gold'"
                    }
                } else {
                    jmlhDataError++;
                    if(!fullModelCode){
                        style1 =  "style='color:gold'"
                    }
                    if(!namaProsesBP){
                        style3 =  "style='color:gold'"
                    }
                    if(!operation){
                        style2 =  "style='color:gold'"
                    }
                    if(!goods){
                        style5 =  "style='color:gold'"
                        style4 =  "style='color:gold'"
                    }
                    if(!satuan){
                        style7 =  "style='color:gold'"
                    }
                    if (it.full==null || it.full==""){
                        style1 =  "style='color:red;'"
                    }
                    if (it.job==null || it.job==""){
                        style2 =  "style='color:red;'"
                    }
                    if (it.proses==null || it.proses==""){
                        style3 =  "style='color:red;'"
                    }
                    if (it.kode==null || it.kode==""){
                        style4 =  "style='color:red;'"
                    }
                    if (it.nama==null || it.nama==""){
                        style5 =  "style='color:red;'"
                    }
                    if (it.jumlah==null || it.jumlah==""){
                        style6 =  "style='color:red;'"
                    }
                    if (it.satuan==null || it.satuan==""){
                        style7 =  "style='color:red;'"
                    }

                }
                htmlData+="<tr>\n" +
                        "                            <td "+style1+">\n" +
                        "                                "+it.full+"\n" +
                        "                            </td>\n" +
                        "                            <td  "+style2+">\n" +
                        "                                "+it.job+"\n" +
                        "                            </td>\n" +
                        "                            <td "+style3+">\n" +
                        "                                "+it.proses+"\n" +
                        "                            </td>\n" +
                        "                            <td  "+style4+">\n" +
                        "                                "+it.kode+"\n" +
                        "                            </td>\n" +
                        "                            <td  "+style5+">\n" +
                        "                                "+it.nama+"\n" +
                        "                            </td>\n" +
                        "                            <td  "+style6+">\n" +
                        "                                "+conversi.toRupiah(it.jumlah)+"\n" +
                        "                            </td>\n" +
                        "                            <td  "+style7+">\n" +
                        "                                "+it.satuan+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadJob.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah dan Kuning")
        } else {
            flash.message = message(code: 'default.uploadJob.message', default: "Read File Done")
        }

        render(view: "index", model: [partJobInstance: partJobInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }

    def upload() {
        PartJob partJobInstance = null
        def requestBody = request.JSON
        FullModelCode fullModelCode = null
        Goods goods= null
        NamaProsesBP namaProsesBP = null
        Operation operation = null
        Satuan satuan = null
        int saved = 0
        int notCoplete = 0
        int exist = 0
        for(it in requestBody){
            partJobInstance = new PartJob();
            if(it.full==null || it.proses ==null || it.job ==null|| it.kode ==null || it.nama ==null || it.jumlah ==null || it.satuan ==null){
                notCoplete++
                flash.message = message(code: 'default.uploadJob.message', default: "Data Belum Valid")
                render(view: "index", model: [partJobInstance: partJobInstance])
                return
            }else{
                fullModelCode = FullModelCode.findByT110FullModelCode(it.full)
                namaProsesBP = NamaProsesBP.findByM190NamaProsesBP(it.proses)
                goods = Goods.findByM111Nama(it.nama)
                operation = Operation.findByM053NamaOperation(it.job)
                satuan = Satuan.findByM118KodeSatuan(it.satuan)

                partJobInstance.operation = operation
                partJobInstance.fullModelCode = fullModelCode
                partJobInstance.namaProsesBP = namaProsesBP
                partJobInstance.goods = goods
                partJobInstance.t112Jumlah = it?.jumlah
                partJobInstance.satuan = satuan
                partJobInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                partJobInstance.lastUpdProcess = "INSERT"
                partJobInstance.setStaDel('0')
                def cek = PartJob.createCriteria().get {
                    eq("staDel","0")
                    eq("fullModelCode",partJobInstance?.fullModelCode)
                    eq("operation",partJobInstance?.operation)
                    eq("namaProsesBP",partJobInstance?.namaProsesBP)
                    eq("goods",partJobInstance?.goods)
                    eq("t112Jumlah",partJobInstance?.t112Jumlah)
                    eq("satuan",partJobInstance?.satuan)
                    maxResults(1);
                }
                if(!cek){
                    saved++
                    partJobInstance.save(flush: true)
                }else{
                    exist++
                }


            }


        }

        flash.message = message(code: 'default.uploadJob.message', default: "Save Job Done")
        render(view: "index", model: [partJobInstance: partJobInstance])
    }
}
