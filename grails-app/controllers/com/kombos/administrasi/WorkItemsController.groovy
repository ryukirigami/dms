package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class WorkItemsController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = WorkItems.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
				
			if(params."sCriteria_m039WorkItems"){
				ilike("m039WorkItems","%" + (params."sCriteria_m039WorkItems" as String) + "%")
			}

			if(params."sCriteria_companyDealer"){
				eq("companyDealer",CompanyDealer.findByM011NamaWorkshopIlike("%"+params."sCriteria_companyDealer"+"%"))
			}

			if(params."sCriteria_m039WorkID"){
				eq("m039WorkID",params."sCriteria_m039WorkID")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m039WorkItems: it.m039WorkItems,
			
						companyDealer: it.companyDealer.m011NamaWorkshop,
			
						m039WorkID: it.m039WorkID,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[workItemsInstance: new WorkItems(params)]
	}

	def save() {
		def workItemsInstance = new WorkItems(params)
        workItemsInstance?.companyDealer = session.userCompanyDealer
        workItemsInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        workItemsInstance?.lastUpdProcess = "INSERT"
        def c = WorkItems.createCriteria().list() {
            and {
                eq("companyDealer",workItemsInstance?.companyDealer)
                eq("m039WorkItems",workItemsInstance?.m039WorkItems?.trim(), [ignoreCase: true])
            }
        }
        if(c){
            flash.message = "Data sudah ada"
            render(view: "create", model: [workItemsInstance: workItemsInstance])
            return
        }
		if (!workItemsInstance.save(flush: true)) {
			render(view: "create", model: [workItemsInstance: workItemsInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'workItems.label', default: 'Work Items'), workItemsInstance.id])
		redirect(action: "show", id: workItemsInstance.id)
	}

	def show(Long id) {
		def workItemsInstance = WorkItems.get(id)
		if (!workItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'workItems.label', default: 'WorkItems'), id])
			redirect(action: "list")
			return
		}

		[workItemsInstance: workItemsInstance]
	}

	def edit(Long id) {
		def workItemsInstance = WorkItems.get(id)
		if (!workItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'workItems.label', default: 'WorkItems'), id])
			redirect(action: "list")
			return
		}

		[workItemsInstance: workItemsInstance]
	}

	def update(Long id, Long version) {
		def workItemsInstance = WorkItems.get(id)


		if (!workItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'workItems.label', default: 'WorkItems'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (workItemsInstance.version > version) {
				
				workItemsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'workItems.label', default: 'WorkItems')] as Object[],
				"Another user has updated this WorkItems while you were editing")
				render(view: "edit", model: [workItemsInstance: workItemsInstance])
				return
			}
		}

        def c = WorkItems.createCriteria()
        def result = c.list() {
            eq("companyDealer",workItemsInstance?.companyDealer)
            eq("m039WorkItems", params?.m039WorkItems?.trim(), [ignoreCase: true])
        }

        if(result){
            for(find in result){
                if(find.id!=id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [workItemsInstance: workItemsInstance])
                    return
                    break
                }
            }
        }

        workItemsInstance.properties = params
        workItemsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        workItemsInstance?.lastUpdProcess = "UPDATE"


		if (!workItemsInstance.save(flush: true)) {
			render(view: "edit", model: [workItemsInstance: workItemsInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'workItems.label', default: 'Work Items'), workItemsInstance.id])
		redirect(action: "show", id: workItemsInstance.id)
	}

	def delete(Long id) {
		def workItemsInstance = WorkItems.get(id)
		if (!workItemsInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'workItems.label', default: 'WorkItems'), id])
			redirect(action: "list")
			return
		}

		try {
            workItemsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            workItemsInstance?.lastUpdProcess = "DELETE"
			workItemsInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'workItems.label', default: 'WorkItems'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'workItems.label', default: 'WorkItems'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(WorkItems, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(WorkItems, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
