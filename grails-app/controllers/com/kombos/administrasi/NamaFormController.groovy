package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class NamaFormController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = NamaForm.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_t004Id") {
                ilike("t004Id", "%" + (params."sCriteria_t004Id" as String) + "%")
            }

            if (params."sCriteria_t004NamaObjek") {
                ilike("t004NamaObjek", "%" + (params."sCriteria_t004NamaObjek" as String) + "%")
            }

            if (params."sCriteria_t004NamaAlias") {
                ilike("t004NamaAlias", "%" + (params."sCriteria_t004NamaAlias" as String) + "%")
            }

            if (params."sCriteria_t004JenisFR") {
                ilike("t004JenisFR", "%" + (params."sCriteria_t004JenisFR" as String) + "%")
            }

            if (params."sCriteria_t004StaAuditTrail") {
                ilike("t004StaAuditTrail", "%" + (params."sCriteria_t004StaAuditTrail" as String) + "%")
            }

            if (params."sCriteria_t004StaPerformanceLog") {
                ilike("t004StaPerformanceLog", "%" + (params."sCriteria_t004StaPerformanceLog" as String) + "%")
            }

            if (params."sCriteria_t004StaFreeze") {
                ilike("t004StaFreeze", "%" + (params."sCriteria_t004StaFreeze" as String) + "%")
            }

            if (params."sCriteria_t004StaDel") {
                ilike("t004StaDel", "%" + (params."sCriteria_t004StaDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t004Id: it.t004Id,

                    t004NamaObjek: it.t004NamaObjek,

                    t004NamaAlias: it.t004NamaAlias,

                    t004JenisFR: it.t004JenisFR=="F"?"Form":"Report",

                    t004StaAuditTrail: it.t004StaAuditTrail=="1"?"Ya":"Tidak",

                    t004StaPerformanceLog: it.t004StaPerformanceLog=="1"?"Ya":"Tidak",

                    t004StaFreeze: it.t004StaFreeze=="1"?"Ya":"Tidak",

                    t004StaDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [namaFormInstance: new NamaForm(params)]
    }

    def save() {
        params.staDel = '0'
        params.lastUpdProcess = "INSERT"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        def namaFormInstance = new NamaForm(params)
        if (!namaFormInstance.save(flush: true)) {
            render(view: "create", model: [namaFormInstance: namaFormInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), namaFormInstance.id])
        redirect(action: "show", id: namaFormInstance.id)
    }

    def show(Long id) {
        def namaFormInstance = NamaForm.get(id)
        if (!namaFormInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), id])
            redirect(action: "list")
            return
        }

        [namaFormInstance: namaFormInstance]
    }

    def edit(Long id) {
        def namaFormInstance = NamaForm.get(id)
        if (!namaFormInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), id])
            redirect(action: "list")
            return
        }

        [namaFormInstance: namaFormInstance]
    }

    def update(Long id, Long version) {
        def namaFormInstance = NamaForm.get(id)
        if (!namaFormInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (namaFormInstance.version > version) {

                namaFormInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'namaForm.label', default: 'NamaForm')] as Object[],
                        "Another user has updated this NamaForm while you were editing")
                render(view: "edit", model: [namaFormInstance: namaFormInstance])
                return
            }
        }

        def cek = NamaForm.createCriteria().list {
            eq("staDel","0")
            eq("t004NamaAlias",params.t004NamaAlias.trim(),[ignoreCase : true])
        }
        if(cek){
            for(find in cek){
                if(find?.id!=id){
                    flash.message="Nama Alias sudah digunakan"
                    render(view: "edit", model: [namaFormInstance: namaFormInstance])
                    return
                    break
                }
            }
        }
        params.lastUpdProcess = "UPDATE"
        params.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        namaFormInstance.properties = params

        if (!namaFormInstance.save(flush: true)) {
            render(view: "edit", model: [namaFormInstance: namaFormInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), namaFormInstance.id])
        redirect(action: "show", id: namaFormInstance.id)
    }

    def delete(Long id) {
        def namaFormInstance = NamaForm.get(id)
        if (!namaFormInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), id])
            redirect(action: "list")
            return
        }

        try {
            namaFormInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'namaForm.label', default: 'NamaForm'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(NamaForm, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(NamaForm, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
