package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class EngineController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Engine.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            if(params."sCriteria_baseModel"){
                baseModel{
                    ilike("m102NamaBaseModel","%"+ (params."sCriteria_baseModel") +"%")
                }
            }

            if(params."sCriteria_modelName"){
                modelName{
                    ilike("m104NamaModelName","%"+ (params."sCriteria_modelName") +"%")
                }
            }

            if(params."sCriteria_bodyType"){
                bodyType{
                    ilike("m105NamaBodyType","%"+ (params."sCriteria_bodyType") +"%")
                }
            }

            if (params."sCriteria_gear") {
                gear{
                    ilike("m106NamaGear","%"+ (params."sCriteria_gear") +"%")
                }
            }

            if (params."sCriteria_grade") {
                grade{
                    ilike("m107NamaGrade","%" + (params."sCriteria_grade") + "%")
                }
            }

            if (params."sCriteria_m108KodeEngine") {
                ilike("m108KodeEngine", "%" + (params."sCriteria_m108KodeEngine" as String) + "%")
            }

            if (params."sCriteria_m108NamaEngine") {
                ilike("m108NamaEngine", "%" + (params."sCriteria_m108NamaEngine" as String) + "%")
            }


            switch (sortProperty) {
                case "baseModel":
                    baseModel{
                        order("m102NamaBaseModel",sortDir)
                    }
                    break;
                case "modelName":
                    modelName{
                        order("m104NamaModelName",sortDir)
                    }
                    break;
                case "bodyType":
                    bodyType{
                        order("m105NamaBodyType",sortDir)
                    }
                    break;
                case "gear":
                    gear{
                        order("m106NamaGear",sortDir)
                    }
                    break;
                case "grade":
                    grade{
                        order("m107NamaGrade",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m108ID: it.m108ID,

                    baseModel: it.baseModel?.m102NamaBaseModel+" - "+it.baseModel?.m102KodeBaseModel,

                    modelName: it.modelName.m104NamaModelName+" - "+it.modelName.m104KodeModelName,

                    bodyType: it.bodyType?.m105NamaBodyType+" - "+it.bodyType?.m105KodeBodyType,

                    gear: it.gear.m106NamaGear+" - "+it.gear.m106KodeGear,

                    grade: it.grade.m107NamaGrade+" - "+it.grade.m107KodeGrade,

                    m108KodeEngine: it.m108KodeEngine,

                    m108NamaEngine: it.m108NamaEngine,

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [engineInstance: new Engine(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def engineInstance = new Engine(params)
        engineInstance?.setStaDel("0")
        engineInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        engineInstance?.setLastUpdProcess("INSERT")
        def cek = Engine.createCriteria().list() {
            and{
                eq("baseModel",engineInstance.baseModel)
                eq("modelName",engineInstance.modelName)
                eq("bodyType",engineInstance.bodyType)
                eq("gear",engineInstance.gear)
                eq("grade",engineInstance?.grade)
                eq("m108KodeEngine",engineInstance?.m108KodeEngine.trim(), [ignoreCase: true])
                eq("m108NamaEngine",engineInstance?.m108NamaEngine.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [engineInstance: engineInstance])
            return
        }
        if (!engineInstance.save(flush: true)) {
            render(view: "create", model: [engineInstance: engineInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'engine.label', default: 'Engine'), engineInstance.id])
        redirect(action: "show", id: engineInstance.id)
    }

    def show(Long id) {
        def engineInstance = Engine.get(id)
        if (!engineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'engine.label', default: 'Engine'), id])
            redirect(action: "list")
            return
        }

        [engineInstance: engineInstance]
    }

    def edit(Long id) {
        def engineInstance = Engine.get(id)
        if (!engineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'engine.label', default: 'Engine'), id])
            redirect(action: "list")
            return
        }

        [engineInstance: engineInstance]
    }

    def update(Long id, Long version) {
        def engineInstance = Engine.get(id)
        engineInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        engineInstance?.setLastUpdProcess("UPDATE")
        if (!engineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'engine.label', default: 'Engine'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (engineInstance.version > version) {

                engineInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'engine.label', default: 'Engine')] as Object[],
                        "Another user has updated this Engine while you were editing")
                render(view: "edit", model: [engineInstance: engineInstance])
                return
            }
        }
        def engine = Engine.createCriteria().list {
            eq("staDel","0")
            eq("m108KodeEngine",params.m108KodeEngine.trim(),[ignoreCase:true])
        }
        if (engine) {
            for(find in engine){
                if(id!=find.id){
                    flash.message = "Kode Engine Telah Di gunakan"
                    render(view: "edit", model: [engineInstance: engineInstance])
                    return
                    break
                }
            }
        }
        def engine2 = Engine.createCriteria().list {
            eq("staDel","0")
            eq("m108NamaEngine",params.m108NamaEngine.trim(),[ignoreCase:true])
        }
        if (engine2) {
            for(find in engine2){
                if(id!=find.id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [engineInstance: engineInstance])
                    return
                    break
                }
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        engineInstance.properties = params

        if (!engineInstance.save(flush: true)) {
            render(view: "edit", model: [engineInstance: engineInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'engine.label', default: 'Engine'), engineInstance.id])
        redirect(action: "show", id: engineInstance.id)
    }

    def delete(Long id) {
        def engineInstance = Engine.get(id)

        if (!engineInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'engine.label', default: 'Engine'), id])
            redirect(action: "list")
            return
        }

        try {
            engineInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            engineInstance?.setLastUpdProcess("DELETE")
            engineInstance?.setStaDel("1")
            engineInstance?.lastUpdated = datatablesUtilService?.syncTime()
            engineInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'engine.label', default: 'Engine'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'engine.label', default: 'Engine'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Engine, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Engine, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
