package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PerhitunganNextServiceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def conversi =new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PerhitunganNextService.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_t114TglBerlaku") {
                ge("t114TglBerlaku", params."sCriteria_t114TglBerlaku")
                lt("t114TglBerlaku", params."sCriteria_t114TglBerlaku" + 1)
            }

            if (params."sCriteria_t114KategoriService") {
                ilike("t114KategoriService", "%" + (params."sCriteria_t114KategoriService" as String) + "%")
            }

            if (params."sCriteria_bahanBakar") {
                eq("bahanBakar", BahanBakar.findByM110NamaBahanBakarIlike("%" + params."sCriteria_bahanBakar" + "%"))
            }

            if (params."sCriteria_t114Km") {
                eq("t114Km", Double.parseDouble(params.sCriteria_t114Km))
            }

            if (params."sCriteria_t114Bulan") {
                eq("t114Bulan", Double.parseDouble(params.sCriteria_t114Bulan))
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t114TglBerlaku: it.t114TglBerlaku ? it.t114TglBerlaku.format(dateFormat) : "",

                    t114KategoriService: it.t114KategoriService,

                    bahanBakar: it.bahanBakar.m110NamaBahanBakar,

                    t114Km: conversi.toRupiah(it.t114Km),

                    t114Bulan: it.t114Bulan,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [perhitunganNextServiceInstance: new PerhitunganNextService(params)]
    }

    def save() {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def perhitunganNextServiceInstance = new PerhitunganNextService(params)



        def findNext = PerhitunganNextService.createCriteria().list {
            eq("t114Bulan",Double.parseDouble(params.t114Bulan))
            eq("t114Km",Double.parseDouble(params.t114Km))
            eq("t114KategoriService",params.t114KategoriService)
            bahanBakar{
                eq("id",Long.parseLong(params.bahanBakar.id))
            }
        }

        if(findNext){
            for(find in findNext){
                if(find.t114TglBerlaku.compareTo(params.t114TglBerlaku)==0){
                    flash.message = 'Data sudah ada'
                    render(view: "create", model: [perhitunganNextServiceInstance: perhitunganNextServiceInstance])
                    return
                    break
                }
            }
        }
        perhitunganNextServiceInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        perhitunganNextServiceInstance?.setLastUpdProcess("INSERT")
        if (!perhitunganNextServiceInstance.save(flush: true)) {
            render(view: "create", model: [perhitunganNextServiceInstance: perhitunganNextServiceInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), perhitunganNextServiceInstance.id])
        redirect(action: "show", id: perhitunganNextServiceInstance.id)
    }

    def show(Long id) {
        def perhitunganNextServiceInstance = PerhitunganNextService.get(id)
        if (!perhitunganNextServiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), id])
            redirect(action: "list")
            return
        }

        [perhitunganNextServiceInstance: perhitunganNextServiceInstance]
    }

    def edit(Long id) {
        def perhitunganNextServiceInstance = PerhitunganNextService.get(id)
        if (!perhitunganNextServiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), id])
            redirect(action: "list")
            return
        }

        [perhitunganNextServiceInstance: perhitunganNextServiceInstance]
    }

    def update(Long id, Long version) {
        def perhitunganNextServiceInstance = PerhitunganNextService.get(id)
        if (!perhitunganNextServiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), id])
            redirect(action: "list")
            return
        }
        def findNext = PerhitunganNextService.createCriteria().list {
            eq("t114Bulan",Double.parseDouble(params.t114Bulan))
            eq("t114Km",Double.parseDouble(params.t114Km))
            eq("t114KategoriService",params.t114KategoriService)
            bahanBakar{
                eq("id",Long.parseLong(params.bahanBakar.id))
            }
        }

        if(findNext){
            for(find in findNext){
                if(find.id!=id){
                    if(find.t114TglBerlaku.compareTo(params.t114TglBerlaku)==0){
                        flash.message = 'Data sudah ada'
                        render(view: "edit", model: [perhitunganNextServiceInstance: perhitunganNextServiceInstance])
                        return
                        break
                    }
                }
            }
        }

        if (version != null) {
            if (perhitunganNextServiceInstance.version > version) {

                perhitunganNextServiceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService')] as Object[],
                        "Another user has updated this PerhitunganNextService while you were editing")
                render(view: "edit", model: [perhitunganNextServiceInstance: perhitunganNextServiceInstance])
                return
            }
        }

        params?.lastUpdated = datatablesUtilService?.syncTime()
        perhitunganNextServiceInstance.properties = params
        perhitunganNextServiceInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        perhitunganNextServiceInstance?.setLastUpdProcess("UPDATE")

        if (!perhitunganNextServiceInstance.save(flush: true)) {
            render(view: "edit", model: [perhitunganNextServiceInstance: perhitunganNextServiceInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), perhitunganNextServiceInstance.id])
        redirect(action: "show", id: perhitunganNextServiceInstance.id)
    }

    def delete(Long id) {
        def perhitunganNextServiceInstance = PerhitunganNextService.get(id)
        if (!perhitunganNextServiceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), id])
            redirect(action: "list")
            return
        }

        try {
            perhitunganNextServiceInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'perhitunganNextService.label', default: 'PerhitunganNextService'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PerhitunganNextService, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PerhitunganNextService, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
