package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class TipeBeratController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def conversi=new Konversi()

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = TipeBerat.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m026NamaTipeBerat") {
                ilike("m026NamaTipeBerat", "%" + (params."sCriteria_m026NamaTipeBerat" as String) + "%")
            }

            if (params."sCriteria_m026Berat1") {
                eq("m026Berat1", Double.parseDouble(params."sCriteria_m026Berat1"))
            }

            if (params."sCriteria_m026Berat2") {
                eq("m026Berat2", Double.parseDouble(params."sCriteria_m026Berat2"))
            }

            if (params."sCriteria_m026Id") {
                ilike("m026Id", "%" + (params."sCriteria_m026Id" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m026NamaTipeBerat: it.m026NamaTipeBerat,

                    m026Berat1: conversi.toRupiah(it.m026Berat1),

                    m026Berat2: conversi.toRupiah(it.m026Berat2),

                    m026Id: it.m026Id,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
		def tipeBeratInstance = new TipeBerat(params)
		tipeBeratInstance.setM026Berat1(0.0001)
        tipeBeratInstance.setM026Berat2(0.0001)
        [tipeBeratInstance: tipeBeratInstance]
    }

    def save() {
        def tipeBeratInstance = new TipeBerat(params)

        def c = TipeBerat.createCriteria()
        def result = c.list() {
            eq("m026NamaTipeBerat",params.m026NamaTipeBerat.trim(), [ignoreCase: true])
            eq("m026Berat1", Double.parseDouble(params.m026Berat1))
            eq("m026Berat2", Double.parseDouble(params.m026Berat2))
        }
        if(result){
            flash.message = "Data Sudah Ada"
            render(view: "create", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }
        if(TipeBerat.findByM026Berat1GreaterThanEqualsOrM026Berat2GreaterThanEquals(Double.parseDouble(params.m026Berat1),Double.parseDouble(params.m026Berat2))!=null){

            flash.message = '* Range Berat Sudah Ada'
            render(view: "create", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }
        tipeBeratInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        tipeBeratInstance?.lastUpdProcess = "INSERT"
        if (tipeBeratInstance.getM026Berat1()<=0 || tipeBeratInstance.getM026Berat2()<=0) {
            flash.message = message(code: 'default.error.created.tipeberat.message', default: 'Inputan Berat 1 dan Berat 2 harus lebih besar dari 0.')
			render(view: "create", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }
        if (tipeBeratInstance.getM026Berat1()>=tipeBeratInstance.getM026Berat2()) {
            flash.message = message(code: 'default.error.created.tipeberat.message', default: 'Berat 2 harus lebih besar dari Berat 1.')
            render(view: "create", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }
		if (!tipeBeratInstance.save(flush: true)) {
            render(view: "create", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), tipeBeratInstance.id])
        redirect(action: "show", id: tipeBeratInstance.id)
    }

    def show(Long id) {
        def tipeBeratInstance = TipeBerat.get(id)
        if (!tipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        [tipeBeratInstance: tipeBeratInstance]
    }

    def edit(Long id) {
        def tipeBeratInstance = TipeBerat.get(id)
        if (!tipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        [tipeBeratInstance: tipeBeratInstance]
    }

    def update(Long id, Long version) {
        def tipeBeratInstance = TipeBerat.get(id)
        if (!tipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (tipeBeratInstance.version > version) {

                tipeBeratInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'tipeBerat.label', default: 'Tipe Berat')] as Object[],
                        "Another user has updated this TipeBerat while you were editing")
                render(view: "edit", model: [tipeBeratInstance: tipeBeratInstance])
                return
            }
        }

        def c = TipeBerat.createCriteria()
        def result = c.list() {
            eq("m026NamaTipeBerat",params.m026NamaTipeBerat.trim(), [ignoreCase: true])
            eq("m026Berat1", Double.parseDouble(params.m026Berat1))
            eq("m026Berat2", Double.parseDouble(params.m026Berat2))
        }

        if(result){
            if(result.id!=id){
                flash.message = "Data Sudah Ada"
                render(view: "edit", model: [tipeBeratInstance: tipeBeratInstance])
                return
            }
        }
        if(TipeBerat.findByM026Berat1GreaterThanEqualsOrM026Berat2GreaterThanEquals(Double.parseDouble(params.m026Berat1),Double.parseDouble(params.m026Berat2))!=null){
            if(TipeBerat.findByM026Berat1GreaterThanEqualsOrM026Berat2GreaterThanEquals(Double.parseDouble(params.m026Berat1),Double.parseDouble(params.m026Berat2)).id!=id){
            flash.message = '* Range Berat Sudah Ada'
            render(view: "edit", model: [tipeBeratInstance: tipeBeratInstance])
            return
            }
        }
        tipeBeratInstance.properties = params
        tipeBeratInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        tipeBeratInstance?.lastUpdProcess = "UPDATE"
        if (tipeBeratInstance.getM026Berat1()<=0 || tipeBeratInstance.getM026Berat2()<=0) {
            flash.message = message(code: 'default.error.created.tipeberat.message', default: 'Inputan Berat 1 dan Berat 2 harus lebih besar dari 0.')
            render(view: "edit", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }
        if (tipeBeratInstance.getM026Berat1()>=tipeBeratInstance.getM026Berat2()) {
            flash.message = 'Berat 2 harus lebih besar dari Berat 1.'
            render(view: "edit", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }
        if (!tipeBeratInstance.save(flush: true)) {
            render(view: "edit", model: [tipeBeratInstance: tipeBeratInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), tipeBeratInstance.id])
        redirect(action: "show", id: tipeBeratInstance.id)
    }

    def delete(Long id) {
        def tipeBeratInstance = TipeBerat.get(id)
        if (!tipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        try {
            tipeBeratInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tipeBerat.label', default: 'Tipe Berat'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(TipeBerat, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(TipeBerat, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
