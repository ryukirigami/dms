package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile
import org.apache.poi.ss.usermodel.Sheet

class UploadWarnaVendorController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
    }

    def upload() {
        def warnaVendorInstance = null
        def requestBody = request.JSON

        VendorCat vendorCat = null

        requestBody.each{
            warnaVendorInstance = new WarnaVendor();
            vendorCat = VendorCat.findByM191ID(it.vendorCat)
            if(vendorCat && (it.m192IDWarna && it.m192IDWarna!="") && (it.m192NamaWarna && it.m192NamaWarna!="")){
                warnaVendorInstance.vendorCat = vendorCat
                warnaVendorInstance.m192IDWarna = it.m192IDWarna
                warnaVendorInstance.m192NamaWarna = it?.m192NamaWarna?.toString()?.toUpperCase()
                warnaVendorInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                warnaVendorInstance.lastUpdProcess = "INSERT"
                warnaVendorInstance.setStaDel('0')
                def cek = WarnaVendor.find(warnaVendorInstance)
                if(!cek){
                    warnaVendorInstance.save()
                }
            }
        }

        flash.message = message(code: 'default.uploadJob.message', default: "Save Warna Vendor Done")
        render(view: "index", model: [warnaVendorInstance: warnaVendorInstance])
    }

    def view() {
        def warnaVendorInstance = new WarnaVendor(params)

        //handle upload file
        Map CONFIG_WARNA_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'vendorCat',
                        'B':'m192IDWarna',
                        'C':'m192NamaWarna',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcelWarna");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [warnaVendorInstance: warnaVendorInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def warnaList = excelImportService.columns(workbook, CONFIG_WARNA_COLUMN_MAP)

            jsonData = warnaList as JSON

            VendorCat vendorCat = null

            String status = "0", style = ""
            warnaList?.each {


                if((it.vendorCat && it.vendorCat!="") && (it.m192IDWarna && it.m192IDWarna!="") && (it.m192NamaWarna && it.m192NamaWarna!="")){
                    vendorCat = VendorCat.findByM191ID(it.vendorCat)
                    if(!vendorCat){
                        jmlhDataError++;
                        status = "1";
                    }

                } else {
                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.vendorCat+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m192IDWarna+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.m192NamaWarna+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadWarnaVendor.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadWarnaVendor.message', default: "Read File Done")
        }

        render(view: "index", model: [warnaVendorInstance: warnaVendorInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }
}
