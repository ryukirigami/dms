package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MappingStallTipeBeratController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {

    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def ngecekGam = TipeBerat.findByM026NamaTipeBeratIlike("Gam");


        if(ngecekGam){
        }


        session.exportParams = params

        def c = MappingStallTipeBerat.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            stall{
                eq("staDel","0")
            }
            if (params."sCriteria_stall") {
                stall{
                    ilike("m022NamaStall", "%" + (params."sCriteria_stall" as String) + "%")
                }
            }

            if (params."sCriteria_tipeBerat") {
                tipeBerat{
                    ilike("m026NamaTipeBerat", "%" + (params."sCriteria_tipeBerat" as String) + "%")
                }
                //eq("tipeBerat", params."sCriteria_tipeBerat")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    stall: it.stall?.m022NamaStall,

                    tipeBerat: it.tipeBerat?.m026NamaTipeBerat,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [mappingStallTipeBeratInstance: new MappingStallTipeBerat(params)]
    }

    def save() {
        def mappingStallTipeBeratInstance = new MappingStallTipeBerat(params)
        mappingStallTipeBeratInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingStallTipeBeratInstance?.lastUpdProcess = "INSERT"
        if (!mappingStallTipeBeratInstance.save(flush: true)) {
            flash.message = "Data Sudah Tersedia"
            render(view: "create", model: [mappingStallTipeBeratInstance: mappingStallTipeBeratInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), mappingStallTipeBeratInstance.id])
        redirect(action: "show", id: mappingStallTipeBeratInstance.id)
    }

    def show(Long id) {
        def mappingStallTipeBeratInstance = MappingStallTipeBerat.get(id)
        if (!mappingStallTipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        [mappingStallTipeBeratInstance: mappingStallTipeBeratInstance]
    }

    def edit(Long id) {
        def mappingStallTipeBeratInstance = MappingStallTipeBerat.get(id)
        if (!mappingStallTipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        [mappingStallTipeBeratInstance: mappingStallTipeBeratInstance]
    }

    def update(Long id, Long version) {
        def mappingStallTipeBeratInstance = MappingStallTipeBerat.get(id)
        if (!mappingStallTipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        def tipeBerat = TipeBerat.get(params.tipeBerat.id)
        def stall = Stall.get(params.stall.id)

        def duplicate = MappingStallTipeBerat.findByTipeBeratAndStall(tipeBerat,stall)

        if(duplicate){
            flash.message = "Data Sama dengan sebelumnya atau Sudah Tersedia"
            render(view: "edit", model: [mappingStallTipeBeratInstance: mappingStallTipeBeratInstance])
            return
        }

        if (version != null) {
            if (mappingStallTipeBeratInstance.version > version) {

                mappingStallTipeBeratInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat')] as Object[],
                        "Another user has updated this MappingStallTipeBerat while you were editing")
                render(view: "edit", model: [mappingStallTipeBeratInstance: mappingStallTipeBeratInstance])
                return
            }
        }

        mappingStallTipeBeratInstance.properties = params
        mappingStallTipeBeratInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        mappingStallTipeBeratInstance?.lastUpdProcess = "UPDATE"

        if (!mappingStallTipeBeratInstance.save(flush: true)) {
            flash.message = "Data Sama dengan sebelumnya atau Sudah Tersedia"
            render(view: "edit", model: [mappingStallTipeBeratInstance: mappingStallTipeBeratInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), mappingStallTipeBeratInstance.id])
        redirect(action: "show", id: mappingStallTipeBeratInstance.id)
    }

    def delete(Long id) {
        def mappingStallTipeBeratInstance = MappingStallTipeBerat.get(id)
        if (!mappingStallTipeBeratInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), id])
            redirect(action: "list")
            return
        }

        try {
            mappingStallTipeBeratInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'mappingStallTipeBerat.label', default: 'Mapping Stall Tipe Berat'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MappingStallTipeBerat, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(MappingStallTipeBerat, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
