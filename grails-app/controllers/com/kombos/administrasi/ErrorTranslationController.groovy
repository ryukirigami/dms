package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ErrorTranslationController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = ErrorTranslation.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			if(params."sCriteria_m780Id"){
				eq("m780Id",params."sCriteria_m780Id")
			}
	
			if(params."sCriteria_m780Tahun"){
				ilike("m780Tahun","%" + (params."sCriteria_m780Tahun" as String) + "%")
			}
	
			if(params."sCriteria_m780ErrorMsg"){
				ilike("m780ErrorMsg","%" + (params."sCriteria_m780ErrorMsg" as String) + "%")
			}
	
			if(params."sCriteria_m780ErrTranslation"){
				ilike("m780ErrTranslation","%" + (params."sCriteria_m780ErrTranslation" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m780Id: it.m780Id,
			
						m780Tahun: it.m780Tahun,
			
						m780ErrorMsg: it.m780ErrorMsg,
			
						m780ErrTranslation: it.m780ErrTranslation,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[errorTranslationInstance: new ErrorTranslation(params)]
	}

	def save() {
		def errorTranslationInstance = new ErrorTranslation(params)
        errorTranslationInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        errorTranslationInstance.setLastUpdProcess("INSERT")
        def cek = ErrorTranslation.createCriteria().list {
            eq("m780ErrorMsg",params.m780ErrorMsg.trim(), [ignoreCase : true])
            eq("m780ErrTranslation",params.m780ErrTranslation.trim(), [ignoreCase : true])
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [errorTranslationInstance: errorTranslationInstance])
            return
        }
		if (!errorTranslationInstance.save(flush: true)) {
			render(view: "create", model: [errorTranslationInstance: errorTranslationInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'errorTranslation.label', default: 'Error Translation'), errorTranslationInstance.id])
		redirect(action: "show", id: errorTranslationInstance.id)
	}

	def show(Long id) {
		def errorTranslationInstance = ErrorTranslation.get(id)
		if (!errorTranslationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorTranslation.label', default: 'ErrorTranslation'), id])
			redirect(action: "list")
			return
		}

		[errorTranslationInstance: errorTranslationInstance]
	}

	def edit(Long id) {
		def errorTranslationInstance = ErrorTranslation.get(id)
		if (!errorTranslationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorTranslation.label', default: 'ErrorTranslation'), id])
			redirect(action: "list")
			return
		}

		[errorTranslationInstance: errorTranslationInstance]
	}

	def update(Long id, Long version) {
		def errorTranslationInstance = ErrorTranslation.get(id)
        errorTranslationInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        errorTranslationInstance.setLastUpdProcess("UPDATE")
		if (!errorTranslationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorTranslation.label', default: 'ErrorTranslation'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (errorTranslationInstance.version > version) {
				
				errorTranslationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'errorTranslation.label', default: 'ErrorTranslation')] as Object[],
				"Another user has updated this ErrorTranslation while you were editing")
				render(view: "edit", model: [errorTranslationInstance: errorTranslationInstance])
				return
			}
		}
        def cek = ErrorTranslation.createCriteria().list {
            eq("m780ErrorMsg",params.m780ErrorMsg.trim(), [ignoreCase : true])
            eq("m780ErrTranslation",params.m780ErrTranslation.trim(), [ignoreCase : true])
        }
        if(cek){
            for(find in cek){
                if(find.id !=id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [errorTranslationInstance: errorTranslationInstance])
                    return
                    break
                }
            }
        }
        errorTranslationInstance.properties = params

		if (!errorTranslationInstance.save(flush: true)) {
			render(view: "edit", model: [errorTranslationInstance: errorTranslationInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'errorTranslation.label', default: 'Error Translation'), errorTranslationInstance.id])
		redirect(action: "show", id: errorTranslationInstance.id)
	}

	def delete(Long id) {
		def errorTranslationInstance = ErrorTranslation.get(id)
		if (!errorTranslationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'errorTranslation.label', default: 'ErrorTranslation'), id])
			redirect(action: "list")
			return
		}

		try {
            errorTranslationInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            errorTranslationInstance.setLastUpdProcess("DELETE")
			errorTranslationInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'errorTranslation.label', default: 'ErrorTranslation'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'errorTranslation.label', default: 'ErrorTranslation'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(ErrorTranslation, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ErrorTranslation, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
