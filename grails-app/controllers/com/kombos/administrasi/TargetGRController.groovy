package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class TargetGRController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
   def conversi =new Konversi()
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = TargetGR.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m037TglBerlaku") {
                ge("m037TglBerlaku", params."sCriteria_m037TglBerlaku")
                lt("m037TglBerlaku", params."sCriteria_m037TglBerlaku" + 1)
            }

            if (params."sCriteria_m037TargetUnitSales") {
                eq("m037TargetUnitSales", (params."sCriteria_m037TargetUnitSales" as Double))
            }

            if (params."sCriteria_m037TargetCPUS") {
                eq("m037TargetCPUS", (params."sCriteria_m037TargetCPUS" as Double))
            }

            if (params."sCriteria_m037TargetSBI") {
                eq("m037TargetSBI", (params."sCriteria_m037TargetSBI" as Double))
            }

            if (params."sCriteria_m037TargetSBE") {
                eq("m037TargetSBE", (params."sCriteria_m037TargetSBE" as Double))
            }

            if (params."sCriteria_m037TargetAmountSales") {
                eq("m037TargetAmountSales", (params."sCriteria_m037TargetAmountSales" as Double))
            }

            if (params."sCriteria_m037COGSPart") {
                eq("m037COGSPart", (params."sCriteria_m037COGSPart" as Double))
            }

            if (params."sCriteria_m037COGSOil") {
                eq("m037COGSOil", (params."sCriteria_m037COGSOil" as Double))
            }

            if (params."sCriteria_m037COGSSublet") {
                eq("m037COGSSublet", (params."sCriteria_m037COGSSublet" as Double))
            }

            if (params."sCriteria_m037COGSLabor") {
                eq("m037COGSLabor", (params."sCriteria_m037COGSLabor" as Double))
            }

            if (params."sCriteria_m037COGSOverHead") {
                eq("m037COGSOverHead", (params."sCriteria_m037COGSOverHead" as Double))
            }

            if (params."sCriteria_m037COGSDeprecation") {
                eq("m037COGSDeprecation", (params."sCriteria_m037COGSDeprecation" as Double))
            }

            if (params."sCriteria_m037COGSTotalSGA") {
                eq("m037COGSTotalSGA", (params."sCriteria_m037COGSTotalSGA" as Double))
            }

            if (params."sCriteria_m037TotalClaimItem") {
                eq("m037TotalClaimItem", (params."sCriteria_m037TotalClaimItem" as Double))
            }

            if (params."sCriteria_m037TotalClaimAmount") {
                eq("m037TotalClaimAmount", (params."sCriteria_m037TotalClaimAmount" as Double))
            }

            if (params."sCriteria_m037TechReport") {
                eq("m037TechReport", (params."sCriteria_m037TechReport" as Double))
            }

            if (params."sCriteria_m037TWCLeadTime") {
                eq("m037TWCLeadTime", (params."sCriteria_m037TWCLeadTime" as Double))
            }

            if (params."sCriteria_m037YTDS") {
                eq("m037YTDS", (params."sCriteria_m037YTDS" as Double))
            }

            if (params."sCriteria_m037YTDD") {
                eq("m037YTDD", (params."sCriteria_m037YTDD" as Double))
            }

            if (params."sCriteria_m037ThisMonthS") {
                eq("m037ThisMonthS", (params."sCriteria_m037ThisMonthS" as Double))
            }

            if (params."sCriteria_m037ThisMonthD") {
                eq("m037ThisMonthD", (params."sCriteria_m037ThisMonthD" as Double))
            }

            if (params."sCriteria_m037Q1S") {
                eq("m037Q1S", (params."sCriteria_m037Q1S" as Double))
            }

            if (params."sCriteria_m037Q1D") {
                eq("m037Q1D", (params."sCriteria_m037Q1D" as Double))
            }

            if (params."sCriteria_m037Q2S") {
                eq("m037Q2S", (params."sCriteria_m037Q2S" as Double))
            }

            if (params."sCriteria_m037Q2D") {
                eq("m037Q2D", (params."sCriteria_m037Q2D" as Double))
            }

            if (params."sCriteria_m037Q3S") {
                eq("m037Q3S", (params."sCriteria_m037Q3S" as Double))
            }

            if (params."sCriteria_m037Q3D") {
                eq("m037Q3D", (params."sCriteria_m037Q3D" as Double))
            }

            if (params."sCriteria_m037Q4S") {
                eq("m037Q4S", (params."sCriteria_m037Q4S" as Double))
            }

            if (params."sCriteria_m037Q4D") {
                eq("m037Q4D", (params."sCriteria_m037Q4D" as Double))
            }

            if (params."sCriteria_m037Q5S") {
                eq("m037Q5S", (params."sCriteria_m037Q5S" as Double))
            }

            if (params."sCriteria_m037Q5D") {
                eq("m037Q5D", (params."sCriteria_m037Q5D" as Double))
            }

            if (params."sCriteria_m037Q6S") {
                eq("m037Q6S", (params."sCriteria_m037Q6S" as Double))
            }

            if (params."sCriteria_m037Q6D") {
                eq("m037Q6D", (params."sCriteria_m037Q6D" as Double))
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m037TglBerlaku: it.m037TglBerlaku ? it.m037TglBerlaku.format(dateFormat) : "",

                    m037TargetUnitSales: conversi.toRupiah(it.m037TargetUnitSales),

                    m037TargetCPUS: conversi.toRupiah(it.m037TargetCPUS),

                    m037TargetSBI: conversi.toRupiah(it.m037TargetSBI),

                    m037TargetSBE: conversi.toRupiah(it.m037TargetSBE),

                    m037TargetAmountSales: conversi.toRupiah(it.m037TargetAmountSales),

                    m037COGSPart: conversi.toRupiah(it.m037COGSPart),

                    m037COGSOil: conversi.toRupiah(it.m037COGSOil),

                    m037COGSSublet: conversi.toRupiah(it.m037COGSSublet),

                    m037COGSLabor: conversi.toRupiah(it.m037COGSLabor),

                    m037COGSOverHead: conversi.toRupiah(it.m037COGSOverHead),

                    m037COGSDeprecation: conversi.toRupiah(it.m037COGSDeprecation),

                    m037COGSTotalSGA: conversi.toRupiah(it.m037COGSTotalSGA),

                    m037TotalClaimItem: conversi.toRupiah(it.m037TotalClaimItem),

                    m037TotalClaimAmount: conversi.toRupiah(it.m037TotalClaimAmount),

                    m037TechReport: conversi.toRupiah(it.m037TechReport),

                    m037TWCLeadTime: conversi.toRupiah(it.m037TWCLeadTime),

                    m037YTDS: conversi.toRupiah(it.m037YTDS),

                    m037YTDD: conversi.toRupiah(it.m037YTDD),

                    m037ThisMonthS: conversi.toRupiah(it.m037ThisMonthS),

                    m037ThisMonthD: conversi.toRupiah(it.m037ThisMonthD),

                    m037Q1S: conversi.toRupiah(it.m037Q1S),

                    m037Q1D: conversi.toRupiah(it.m037Q1D),

                    m037Q2S: conversi.toRupiah(it.m037Q2S),

                    m037Q2D: conversi.toRupiah(it.m037Q2D),

                    m037Q3S: conversi.toRupiah(it.m037Q3S),

                    m037Q3D: conversi.toRupiah(it.m037Q3D),

                    m037Q4S: conversi.toRupiah(it.m037Q4S),

                    m037Q4D: conversi.toRupiah(it.m037Q4D),

                    m037Q5S: conversi.toRupiah(it.m037Q5S),

                    m037Q5D: conversi.toRupiah(it.m037Q5D),

                    m037Q6S: conversi.toRupiah(it.m037Q6S),

                    m037Q6D: conversi.toRupiah(it.m037Q6D),

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [targetGRInstance: new TargetGR(params)]
    }

    def save() {
        def m037TglBerlaku = Date.parse("dd/MM/yyyy", params.m037TglBerlaku_date_dp)
        Calendar start = Calendar.getInstance();
        start.setTime(m037TglBerlaku);
        params.remove("m037TglBerlaku_date")

        params.m037TglBerlaku = "date.struct"
        params.m037TglBerlaku_day = ""+start.get(Calendar.DATE)
        params.m037TglBerlaku_month = ""+start.get(Calendar.MONTH)
        params.m037TglBerlaku_year = ""+start.get(Calendar.YEAR)
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.companyDealer = session.userCompanyDealer
        params.lastUpdProcess = "INSERT"
        params.staDel = '0'

        def targetGRInstance = new TargetGR(params)
        def targetGR = TargetGR.find(targetGRInstance)

        if(targetGR != null) {
            targetGRInstance.errors.rejectValue("version", "default.duplicate.primary.message",
                    [message(code: 'targetGR.label', default: 'Target GR')] as Object[],
                    "Terjadi duplikasi data.")
            render(view: "create", model: [targetGRInstance: targetGRInstance])
            return
        }

        if (!targetGRInstance.save(flush: true)) {
            render(view: "create", model: [targetGRInstance: targetGRInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), targetGRInstance.id])
        redirect(action: "show", id: targetGRInstance.id)
    }

    def show(Long id) {
        def targetGRInstance = TargetGR.get(id)
        if (!targetGRInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), id])
            redirect(action: "list")
            return
        }

        [targetGRInstance: targetGRInstance]
    }

    def edit(Long id) {
        def targetGRInstance = TargetGR.get(id)
        if (!targetGRInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), id])
            redirect(action: "list")
            return
        }

        [targetGRInstance: targetGRInstance]
    }

    def update(Long id, Long version) {
        def targetGRInstance = TargetGR.get(id)
        if (!targetGRInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (targetGRInstance.version > version) {

                targetGRInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'targetGR.label', default: 'TargetGR')] as Object[],
                        "Another user has updated this TargetGR while you were editing")
                render(view: "edit", model: [targetGRInstance: targetGRInstance])
                return
            }
        }

        targetGRInstance.properties = params

        if (!targetGRInstance.save(flush: true)) {
            render(view: "edit", model: [targetGRInstance: targetGRInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), targetGRInstance.id])
        redirect(action: "show", id: targetGRInstance.id)
    }

    def delete(Long id) {
        def targetGRInstance = TargetGR.get(id)
        if (!targetGRInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), id])
            redirect(action: "list")
            return
        }
        try {
            targetGRInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'targetGR.label', default: 'TargetGR'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(TargetGR, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(TargetGR, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
