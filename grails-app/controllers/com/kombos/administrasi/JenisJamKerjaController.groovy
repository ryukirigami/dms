package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat

class JenisJamKerjaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def cekWaktu(String jam){
        String jamTemp = jam;
        if(jamTemp.indexOf(':')<0){
            jamTemp+=':00'
        }else{
            if(jamTemp.indexOf(':')==0){
                String temp = jamTemp
                jamTemp = '-1'+temp
            }
            if(jamTemp.indexOf(':')==jamTemp.length()-1){
                jamTemp+='00'
            }
        }
        return jamTemp;
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = JenisJamKerja.createCriteria()
        DateFormat df = new SimpleDateFormat("HH:mm")
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            if (params."sCriteria_companyDealer") {
                companyDealer{
                    ilike("m011NamaWorkshop","%"+(params."sCriteria_companyDealer")+"%")
                }
            }

            if (params."sCriteria_m030ID") {
                ilike("m030ID", "%" + (params."sCriteria_m030ID" as String) + "%")
            }

            if (params."sCriteria_m030JamMulai1") {
                String jam = cekWaktu(params."sCriteria_m030JamMulai1")
                Date date1 = df.parse(jam)
                eq("m030JamMulai1", date1)
            }

            if (params."sCriteria_m030JamSelesai1") {
                String jam = cekWaktu(params."sCriteria_m030JamSelesai1")
                Date date12 = df.parse(jam)
                eq("m030JamSelesai1", date12)
            }

            if (params."sCriteria_m030JamMulai2") {
                String jam = cekWaktu(params."sCriteria_m030JamMulai2")
                Date date2 = df.parse(jam)
                eq("m030JamMulai2", date2)
            }

            if (params."sCriteria_m030JamSelesai2") {
                String jam = cekWaktu(params."sCriteria_m030JamSelesai2")
                Date date21 = df.parse(jam)
                eq("m030JamSelesai2", date21)
            }

            if (params."sCriteria_m030JamMulai3") {
                String jam = cekWaktu(params."sCriteria_m030JamMulai3")
                Date date3 = df.parse(jam)
                eq("m030JamMulai3", date3)
            }

            if (params."sCriteria_m030JamSelesai3") {
                String jam = cekWaktu(params."sCriteria_m030JamSelesai3")
                Date date31 = df.parse(jam)
                eq("m030JamSelesai3", date31)
            }

            if (params."sCriteria_m030JamMulai4") {
                String jam = cekWaktu(params."sCriteria_m030JamMulai4")
                Date date4 = df.parse(jam)
                eq("m030JamMulai4", date4)
            }

            if (params."sCriteria_m030JamSelesai4") {
                String jam = cekWaktu(params."sCriteria_m030JamSelesai4")
                Date date41 = df.parse(jam)
                eq("m030JamSelesai4", date41)
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }



            switch (sortProperty) {
                case "companyDealer":
                    companyDealer{
                        order("m011NamaWorkshop",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer?.m011NamaWorkshop,

                    m030ID: it.m030ID,

                    m030JamMulai1: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamMulai1?.getTime())),

                    m030JamSelesai1: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamSelesai1?.getTime())),

                    m030JamMulai2: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamMulai2?.getTime())),

                    m030JamSelesai2: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamSelesai2?.getTime())),

                    m030JamMulai3: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamMulai3?.getTime())),

                    m030JamSelesai3: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamSelesai3?.getTime())),

                    m030JamMulai4: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamMulai4?.getTime())),

                    m030JamSelesai4: new SimpleDateFormat("HH:mm").format(new Date(it.m030JamSelesai4?.getTime())),

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [jenisJamKerjaInstance: new JenisJamKerja(params)]
    }

    def save() {
        def jenisJamKerjaInstance = new JenisJamKerja()

        //cek duplicate data
        if(JenisJamKerja.findByCompanyDealerAndM030IDIlikeAndStaDel(CompanyDealer?.findById(Long.parseLong(params?.companyDealer?.id)),params?.m030ID?.trim(),'0')==null){
            jenisJamKerjaInstance?.m030ID = params?.m030ID
            jenisJamKerjaInstance?.companyDealer = CompanyDealer?.findById(Long.parseLong(params?.companyDealer?.id))
            Time time1 = new Time(Integer?.parseInt(params?.m030JamMulai1_hour),Integer?.parseInt(params?.m030JamMulai1_minute),0)
            Time time11 = new Time(Integer?.parseInt(params?.m030JamSelesai1_hour),Integer?.parseInt(params?.m030JamSelesai1_minute),0)
            Time time2 = new Time(Integer?.parseInt(params?.m030JamMulai2_hour),Integer?.parseInt(params?.m030JamMulai2_minute),0)
            Time time21 = new Time(Integer?.parseInt(params?.m030JamSelesai2_hour),Integer?.parseInt(params?.m030JamSelesai2_minute),0)
            Time time3 = new Time(Integer?.parseInt(params?.m030JamMulai3_hour),Integer?.parseInt(params?.m030JamMulai3_minute),0)
            Time time31 = new Time(Integer?.parseInt(params?.m030JamSelesai3_hour),Integer?.parseInt(params?.m030JamSelesai3_minute),0)
            Time time4 = new Time(Integer?.parseInt(params?.m030JamMulai4_hour),Integer?.parseInt(params?.m030JamMulai4_minute),0)
            Time time41 = new Time(Integer?.parseInt(params?.m030JamSelesai4_hour),Integer?.parseInt(params?.m030JamSelesai4_minute),0)
            jenisJamKerjaInstance?.setM030JamMulai1(time1)
            jenisJamKerjaInstance?.setM030JamSelesai1(time11)
            jenisJamKerjaInstance?.setM030JamMulai2(time2)
            jenisJamKerjaInstance?.setM030JamSelesai2(time21)
            jenisJamKerjaInstance?.setM030JamMulai3(time3)
            jenisJamKerjaInstance?.setM030JamSelesai3(time31)
            jenisJamKerjaInstance?.setM030JamMulai4(time4)
            jenisJamKerjaInstance?.setM030JamSelesai4(time41)
            jenisJamKerjaInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            jenisJamKerjaInstance?.lastUpdProcess = "INSERT"
            jenisJamKerjaInstance?.setStaDel('0')
            jenisJamKerjaInstance?.dateCreated = datatablesUtilService?.syncTime()
            jenisJamKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()

            if (!jenisJamKerjaInstance.save(flush: true)) {
                render(view: "create", model: [jenisJamKerjaInstance: jenisJamKerjaInstance])
                return
            }
        } else {
            flash.message = message(code: 'default.created.failed.message', args: [message(code: 'jenisJamKerja.label', default: 'Jenis Jam Kerja'), CompanyDealer?.findById(Long.parseLong(params?.companyDealer?.id)), params?.m030ID])
            render(view: "create", model: [jenisJamKerjaInstance: jenisJamKerjaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'jenisJamKerja.label', default: 'Jenis Jam Kerja'), jenisJamKerjaInstance?.id])
        redirect(action: "show", id: jenisJamKerjaInstance?.id)
    }

    def show(Long id) {
        def jenisJamKerjaInstance = JenisJamKerja.get(id)
        if (!jenisJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisJamKerja.label', default: 'Jenis Jam Kerja'), id])
            redirect(action: "list")
            return
        }

        [jenisJamKerjaInstance: jenisJamKerjaInstance]
    }

    def edit(Long id) {
        def jenisJamKerjaInstance = JenisJamKerja.get(id)
        if (!jenisJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisJamKerja.label', default: 'JenisJamKerja'), id])
            redirect(action: "list")
            return
        }

        [jenisJamKerjaInstance: jenisJamKerjaInstance]
    }

    def update(Long id, Long version) {
        def jenisJamKerjaInstance = JenisJamKerja.get(id)
        if (!jenisJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisJamKerja.label', default: 'Jenis Jam Kerja'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (jenisJamKerjaInstance.version > version) {

                jenisJamKerjaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'jenisJamKerja.label', default: 'JenisJamKerja')] as Object[],
                        "Another user has updated this JenisJamKerja while you were editing")
                render(view: "edit", model: [jenisJamKerjaInstance: jenisJamKerjaInstance])
                return
            }
        }

        //jenisJamKerjaInstance.properties = params
        jenisJamKerjaInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        jenisJamKerjaInstance?.lastUpdProcess = "UPDATE"
        def jamEdit = JenisJamKerja.findByCompanyDealerAndM030IDIlikeAndStaDel(CompanyDealer?.findById(Long.parseLong(params?.companyDealer?.id)),params?.m030ID?.trim(),'0')
        if(!jamEdit||jamEdit.id==id){
        jenisJamKerjaInstance?.m030ID = params?.m030ID
        jenisJamKerjaInstance?.companyDealer = CompanyDealer?.findById(Long.parseLong(params?.companyDealer?.id))
        Time time1 = new Time(Integer?.parseInt(params?.m030JamMulai1_hour),Integer?.parseInt(params?.m030JamMulai1_minute),0)
        Time time11 = new Time(Integer?.parseInt(params?.m030JamSelesai1_hour),Integer?.parseInt(params?.m030JamSelesai1_minute),0)
        Time time2 = new Time(Integer?.parseInt(params?.m030JamMulai2_hour),Integer?.parseInt(params?.m030JamMulai2_minute),0)
        Time time21 = new Time(Integer?.parseInt(params?.m030JamSelesai2_hour),Integer?.parseInt(params?.m030JamSelesai2_minute),0)
        Time time3 = new Time(Integer?.parseInt(params?.m030JamMulai3_hour),Integer?.parseInt(params?.m030JamMulai3_minute),0)
        Time time31 = new Time(Integer?.parseInt(params?.m030JamSelesai3_hour),Integer?.parseInt(params?.m030JamSelesai3_minute),0)
        Time time4 = new Time(Integer?.parseInt(params?.m030JamMulai4_hour),Integer?.parseInt(params?.m030JamMulai4_minute),0)
        Time time41 = new Time(Integer?.parseInt(params?.m030JamSelesai4_hour),Integer?.parseInt(params?.m030JamSelesai4_minute),0)
        jenisJamKerjaInstance?.setM030JamMulai1(time1)
        jenisJamKerjaInstance?.setM030JamSelesai1(time11)
        jenisJamKerjaInstance?.setM030JamMulai2(time2)
        jenisJamKerjaInstance?.setM030JamSelesai2(time21)
        jenisJamKerjaInstance?.setM030JamMulai3(time3)
        jenisJamKerjaInstance?.setM030JamSelesai3(time31)
        jenisJamKerjaInstance?.setM030JamMulai4(time4)
        jenisJamKerjaInstance?.setM030JamSelesai4(time41)
        jenisJamKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()

            if (!jenisJamKerjaInstance.save(flush: true)) {
            render(view: "edit", model: [jenisJamKerjaInstance: jenisJamKerjaInstance])
            return
        }
        } else {
            flash.message = message(code: 'default.created.failed.message', args: [message(code: 'jenisJamKerja.label', default: 'Jenis Jam Kerja'), CompanyDealer?.findById(Long.parseLong(params?.companyDealer?.id)), params?.m030ID])
            render(view: "edit", model: [jenisJamKerjaInstance: jenisJamKerjaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'jenisJamKerja.label', default: 'Jenis Jam Kerja'), jenisJamKerjaInstance.id])
        redirect(action: "show", id: jenisJamKerjaInstance.id)
    }

    def delete(Long id) {
        def jenisJamKerjaInstance = JenisJamKerja.get(id)
        if (!jenisJamKerjaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisJamKerja.label', default: 'JenisJamKerja'), id])
            redirect(action: "list")
            return
        }

        try {
            jenisJamKerjaInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            jenisJamKerjaInstance?.lastUpdProcess = "DELETE"
            jenisJamKerjaInstance?.setStaDel('1')
            jenisJamKerjaInstance?.lastUpdated = datatablesUtilService?.syncTime()
            jenisJamKerjaInstance?.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'jenisJamKerja.label', default: 'JenisJamKerja'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'jenisJamKerja.label', default: 'JenisJamKerja'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDel(JenisJamKerja, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(JenisJamKerja, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
