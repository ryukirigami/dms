package com.kombos.administrasi

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class FormOfVehicleController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = FormOfVehicle.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			
			 
			if(params."sCriteria_m114KodeFoV"){
				ilike("m114KodeFoV","%" + (params."sCriteria_m114KodeFoV" as String) + "%")
			}
	
			if(params."sCriteria_m114NamaFoV"){
				ilike("m114NamaFoV","%" + (params."sCriteria_m114NamaFoV" as String) + "%")
			}
	
			ilike("staDel",'0')

			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m114ID: it.m114ID,
			
						m114KodeFoV: it.m114KodeFoV,
			
						m114NamaFoV: it.m114NamaFoV,
			
						staDel: it.staDel,
						createdBy: it.createdBy,
						
											updatedBy: it.updatedBy,
						
											lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[formOfVehicleInstance: new FormOfVehicle(params)]
	}

	def save() {
		def formOfVehicleInstance = new FormOfVehicle(params)
        def cek = FormOfVehicle.createCriteria()
        def result = cek.list() {
            or{
                eq("m114KodeFoV",formOfVehicleInstance.m114KodeFoV?.trim(), [ignoreCase: true])
                eq("m114NamaFoV",formOfVehicleInstance.m114NamaFoV?.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }

        if(result){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "create", model: [formOfVehicleInstance: formOfVehicleInstance])
            return
        }

		formOfVehicleInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		formOfVehicleInstance?.lastUpdProcess = "INSERT"
		formOfVehicleInstance?.setStaDel('0')

		if (!formOfVehicleInstance.save(flush: true)) {
			render(view: "create", model: [formOfVehicleInstance: formOfVehicleInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), formOfVehicleInstance?.getM114NamaFoV()])
		redirect(action: "show", id: formOfVehicleInstance.id)
	}

	def show(Long id) {
		def formOfVehicleInstance = FormOfVehicle.get(id)
		if (!formOfVehicleInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), id])
			redirect(action: "list")
			return
		}

		[formOfVehicleInstance: formOfVehicleInstance]
	}

	def edit(Long id) {
		def formOfVehicleInstance = FormOfVehicle.get(id)
		if (!formOfVehicleInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), id])
			redirect(action: "list")
			return
		}

		[formOfVehicleInstance: formOfVehicleInstance]
	}

	def update(Long id, Long version) {
		def formOfVehicleInstance = FormOfVehicle.get(id)
		if (!formOfVehicleInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (formOfVehicleInstance.version > version) {
				
				formOfVehicleInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'formOfVehicle.label', default: 'FormOfVehicle')] as Object[],
				"Another user has updated this FormOfVehicle while you were editing")
				render(view: "edit", model: [formOfVehicleInstance: formOfVehicleInstance])
				return
			}
		}
        def cek = FormOfVehicle.createCriteria()
        def result = cek.list() {
            and{
                eq("m114KodeFoV",params.m114KodeFoV, [ignoreCase: true])
            }
            eq("staDel",'0')
        }
        def cek2 = FormOfVehicle.createCriteria()
        def result2 = cek2.list() {
            and{
                eq("m114NamaFoV",params.m114NamaFoV, [ignoreCase: true])
            }
            eq("staDel",'0')
        }

        if(result && FormOfVehicle.findByM114KodeFoVAndStaDel(params.m114KodeFoV,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Kode Sudah Ada')
            render(view: "edit", model: [formOfVehicleInstance: formOfVehicleInstance])
            return
        }
        if(result2 && FormOfVehicle.findByM114NamaFoVAndStaDel(params.m114NamaFoV,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Nama Sudah Ada')
            render(view: "edit", model: [formOfVehicleInstance: formOfVehicleInstance])
            return
        }
		formOfVehicleInstance.properties = params
		formOfVehicleInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
		formOfVehicleInstance?.lastUpdProcess = "UPDATE"
		if (!formOfVehicleInstance.save(flush: true)) {
			render(view: "edit", model: [formOfVehicleInstance: formOfVehicleInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), formOfVehicleInstance.id])
		redirect(action: "show", id: formOfVehicleInstance.id)
	}

	def delete(Long id) {
		def formOfVehicleInstance = FormOfVehicle.get(id)
		if (!formOfVehicleInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), id])
			redirect(action: "list")
			return
		}

		try {
			//formOfVehicleInstance.delete(flush: true)
			formOfVehicleInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
			formOfVehicleInstance?.lastUpdProcess = "DELETE"
			formOfVehicleInstance?.setStaDel('1')
			formOfVehicleInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'formOfVehicle.label', default: 'Form Of Vehicle'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(FormOfVehicle, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(FormOfVehicle, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
