package com.kombos.delivery

import grails.converters.JSON

class ExportFileController {

    def deliveryService
    def exportFileApprovalService

    static viewPermissions = [
      'index',

    ]

    def index() {
      redirect(action: "salesList", params: params)
    }

    /* Start Sales Action */
    def salesList() {
      def metodeBayar = deliveryService.getMetodeBayarList()
      def tipeInvoice = deliveryService.getTipeInvoiceList()
      def customerSPK = deliveryService.getCustomerSpkList()
      [idTable: new Date().format("yyyyMMddhhmmss"), metodeBayar: metodeBayar, tipeInvoice: tipeInvoice, customerSPK: customerSPK]
    }

    def salesDataTablesList() {
      def dataTables = exportFileApprovalService.salesDataTablesList(params)
      render dataTables as JSON
    }

    def toExcelSales() {

    }

    def toTextSales() {
      [data:'OK']
    }
    /* End Sales Action */


    /* Start SPK Invoice, Settlement, Refund Pph 23, Refund PPN  Action */
    def spkInvoiceSettlementList() {
      [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def spkInvoiceSettlementDataTablesList() {
      def dataTables = exportFileApprovalService.spkInvoiceSettlementDataTablesList(params)
      render dataTables as JSON
    }

    def toExcelSpkInvoiceSettlement() {

    }

    def toTextSpkInvoiceSettlement() {
      [data:'OK']
    }
    /* End SpkInvoiceSettlement Action */


    /* Start Booking Fee Action */
    def bookingFeeList(){
      [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def bookingFeeDataTablesList() {
      def dataTables = exportFileApprovalService.bookingFeeDataTablesList(params)
      render dataTables as JSON
    }

    def toExcelBookingFee() {

    }

    def toTextBookingFee() {
      [data:'OK']
    }
    /* End Booking Fee Action */


    /* Start Refund-Booking Fee Action */
    def refundBookingFeeList(){
      [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def refundBookingFeeDataTablesList() {
      def dataTables = exportFileApprovalService.refundBookingFeeDataTablesList(params)
      render dataTables as JSON
    }

    def toExcelRefundBookingFee() {

    }

    def toTextRefundBookingFee() {
      [data:'OK']
    }
    /* End Refund Booking Fee Action */

}
