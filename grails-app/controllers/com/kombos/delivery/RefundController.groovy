package com.kombos.delivery

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class RefundController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def deliveryService

    def jasperService

    static allowedMethods = [save: "POST"]

    static editPermissions = ['save']

    static viewPermissions = ['index', 'list', 'searchData', 'kuitansiDataTablesList', 'print']

    def index() {redirect(action: "list", params: params)}

    def list(){
        def jenisRefund = deliveryService.getJenisRefundList()
        def alasanRefund = deliveryService.getAlasanRefundList()
        def dateFormat = appSettingParamDateFormat?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def bank = deliveryService.getBankList()
        if(dateFormat == null)dateFormat="yyyyMMddhhmmss";
        [jenisRefund: jenisRefund, alasanRefund: alasanRefund, idTable: new Date().format(dateFormat), bank: bank]
    }

    def searchData(){
        params.companyDealer = session?.userCompanyDealer
        def result = deliveryService.searchRefundByNomorWO(params)
        render result as JSON
    }

    def kuitansiDataTablesList(){
        params.companyDealer = session?.userCompanyDealer
        def result = deliveryService.getKuitansiDataTables(params)
        render result as JSON
    }

    def save(){
        params.companyDealer = session?.userCompanyDealer
        def refundData = deliveryService.saveRefundData(params)
        if(refundData){
            render refundData as JSON
        }else{
            response.setStatus(404)
        }
    }

    def print(){
        def refundData = deliveryService.searchRefundByNomorRefund(params)

        def jasperData = [:]
        if(refundData.nomorRefund){
            jasperData.put("refundFlag",refundData?.jenisRefund)
            jasperData.put("refundNo",refundData?.nomorRefund)
            jasperData.put("noWO",refundData?.nomorWO)
            jasperData.put("noPolisi",refundData?.nomorPolisi)
            jasperData.put("customerName",refundData?.customer)
            jasperData.put("invoiceNo",refundData?.invoice)
            jasperData.put("amount",refundData?.amount)
            jasperData.put("sayInWord",refundData?.sayInWord)
            jasperData.put("reason",refundData?.reasonOther)
            jasperData.put("bankName",refundData?.namaBank)
            jasperData.put("accountName",refundData?.actName)
            jasperData.put("accountNo",refundData?.actNo)
            jasperData.put("refundDate",refundData?.tanggalRefund)
            jasperData.put("workshopName",refundData?.namaWorkshop)
            jasperData.put("workshopAddress",refundData?.alamatWorkshop)
            jasperData.put("workshopPhone",refundData?.teleponWorkshop)
            jasperData.put("workshopDirect",refundData?.teleponWorkshop)
            jasperData.put("workshopExt",refundData?.extWorkshop)
            jasperData.put("workshopFax",refundData?.faxWorkshop)
            jasperData.put("kotaDealer",refundData?.kotaDealer)
        }else{
            jasperData.put("refundFlag","-")
            jasperData.put("refundNo","-")
            jasperData.put("noWO","-")
            jasperData.put("noPolisi","-")
            jasperData.put("customerName","-")
            jasperData.put("invoiceNo","-")
            jasperData.put("amount","-")
            jasperData.put("sayInWord","-")
            jasperData.put("reason","-")
            jasperData.put("bankName","-")
            jasperData.put("accountName","-")
            jasperData.put("accountNo","-")
            jasperData.put("refundDate","-")
            jasperData.put("workshopName","-")
            jasperData.put("workshopAddress","-")
            jasperData.put("workshopPhone","-")
            jasperData.put("workshopDirect","-")
            jasperData.put("workshopExt","-")
            jasperData.put("workshopFax","-")
            jasperData.put("kotaDealer","-")
        }

        def reportData = new ArrayList();
        reportData.add(jasperData)

        def jasperReport = new JasperReportDef(
                name:'refundPrint.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        def file = File.createTempFile("Refund_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(jasperReport).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
