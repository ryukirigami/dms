package com.kombos.delivery

import com.kombos.maintable.InvoiceT701
import grails.converters.JSON

class ResponseApprovalDiscountGoodWillController {

    def sendResponseApprovalService

    def datatablesUtilService

    static allowedMethods = [save: "POST"]

    static addPermissions = ['save']

    static viewPermissions = ['index']

    def index() {
        InvoiceT701 invoiceT701 = InvoiceT701.first()
        [invoiceInstance: invoiceT701]
    }

    def responseApprovalDataTablesList() {
        params.companyDealer = session?.userCompanyDealer
        def result = sendResponseApprovalService.sendApprovalDataTablesList(params) //responseApprovalDataTablesList
        render result as JSON
    }

    def responseApproval() {
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each {
            oList << InvoiceT701.get(it?.toString())
        }

        oList.each {

            InvoiceT701 invoice = InvoiceT701.findById(Long.valueOf("" + it.id))

            String persenResponse = params.persenResponse
            String nominalResponse = params.nominalResponse
            String keterangan = params.keterangan
            if (persenResponse != null && !persenResponse.equals("")) {
                Double persen = Double.parseDouble(persenResponse)
                invoice.t701GoodWillPersen = persen
                invoice.t701GoodWillRp = persen * invoice.t701TotalInv / 100
            }
            if (nominalResponse != null && !nominalResponse.equals("")) {
                Double nominal = Double.parseDouble(nominalResponse)
                invoice.t701GoodWillRp = nominal
                invoice.t701GoodWillPersen = invoice.t701TotalInv / (nominal)
            }

            invoice.t701StaApprovalGoodWill = "1"
//            invoice.t701TglResponApprovalGoodWill = new Date()
            invoice.t701TglResponApprovalGoodWill = datatablesUtilService?.syncTime()
            invoice.t701xKet = keterangan
            invoice.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            invoice?.dateCreated = datatablesUtilService?.syncTime()
            invoice?.lastUpdated = datatablesUtilService?.syncTime()
            invoice.save(flush: true)
        }

        render "ok"
    }

    def responseReject() {
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each {
            oList << InvoiceT701.get(it?.toString())
        }
        oList.each {
            InvoiceT701 invoice = InvoiceT701.findById(Long.valueOf("" + it.id))

            String nomorInvoice = params.nomorInvoice
            String keterangan = params.keterangan

            invoice.t701GoodWillRp = 0D
            invoice.t701GoodWillPersen = 0D

            invoice.t701StaApprovalGoodWill = "2"
            invoice.t701TglResponApprovalGoodWill = new Date()
            invoice.t701xKet = keterangan
            invoice.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            invoice.save(flush: true)
        }
        render "ok"
    }
}