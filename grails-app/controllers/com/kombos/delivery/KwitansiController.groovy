package com.kombos.delivery

import com.kombos.administrasi.ManPower
import com.kombos.baseapp.sec.shiro.User
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Kwitansi
import com.kombos.parts.Konversi
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import com.kombos.utils.MoneyUtil
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class KwitansiController {

    def deliveryService

    def jasperService

    static allowedMethods = [save: "POST"]

    static addPermissions = ['inputBayar']

    static editPermissions = ['save']

    static viewPermissions = ['index', 'kwitansiDataTablesList', 'getRate', 'print']

    def datatablesUtilService

    def index() {
        clearDataSession()
        def metodeBayar = deliveryService.getMetodeBayarList()
        def jenisBayar = deliveryService.getJenisBayarList()
        def namaBank = deliveryService.getBankList()
        def mesinEDC = deliveryService.getMesinEDCList()
        [metodeBayar:metodeBayar, jenisBayar:jenisBayar, namaBank: namaBank, mesinEDC:mesinEDC]
    }

    def kwitansiDataTablesList() {
        params.companyDealer = session?.userCompanyDealer
        def result = deliveryService.getKwitansiDataTables(params)
        render result as JSON
    }

    def searchData() {
        params.companyDealer = session.userCompanyDealer
        def result = deliveryService.searchKuitansiByNomorWO(params)
        render result as JSON
    }

    def inputBayar() {
        def result = deliveryService.addPembayaran(params)
        if(result){
            render result as JSON
        }else{
            response.setStatus(404)
        }
    }

    def getRate() {
        def rate = deliveryService.getRate(params)
        render rate as JSON
    }

    def save() {
        params.companyDealer = session?.userCompanyDealer
        def kwitansiData = deliveryService.saveKwitansiData(params)
        if(kwitansiData){
            render kwitansiData as JSON
        }else{
            response.setStatus(404)
        }
    }

    def print() {
        def konversi = new Konversi()
        def reportData =[]
        def kwitansi = Kwitansi.findByT722NoKwitansiAndT722StaDel(params.nomorKuitansi,"0");
        def user = User.findByUsernameAndStaDel(org?.apache?.shiro?.SecurityUtils?.subject?.principal?.toString(),"0")
        def moneyUtil = new MoneyUtil()
        String jenis = ""
        String bayar = ""

        String si_app = ""
        String perhatian = "* SPAREPARTS YANG SUDAH DIPESAN TIDAK BISA DIBATALKAN "
        String perhatian2 = "** JANGKA WAKTU PENGAMBILAN SPAREPARTS PALING LAMBAT 1 (SATU) BULAN SETELAH DIKONFIRMASI SPAREPARTS SUDAH ADA "
        String perhatian3 = "PERHATIAN !!  : "
        if(kwitansi?.t722StaBFPartService=="0" || params.jenis=="0"){
            jenis = "(DP PARTS)"
            bayar = "DP Parts"
            try {
                if(kwitansi.reception?.t401NoAppointment){
                    si_app = " NO APP. " + kwitansi.reception?.t401NoAppointment
                }else{
                    si_app = " NO WO. " + kwitansi.reception?.t401NoWO
                }
            }catch (Exception e){

            }

        }else if(kwitansi?.t722StaBFPartService=="1" || params.jenis=="1"){
            jenis = "(DP SERVICE)"
            bayar = "DP Service"
            try {
                si_app = " NO SO. " + kwitansi.reception.t401NoWO
            }catch (Exception e){

            }
        }else if(kwitansi?.t722StaBFPartService=="2" || params.jenis=="2"){
            jenis = "(ON RISK)"
            bayar = "On Risk (Resiko Sendiri)"
            try {
                si_app = " NO SO. " + kwitansi.reception.t401NoWO
            }catch (Exception e){

            }
            perhatian = ""
            perhatian2 = ""
            perhatian3 = ""
        }
        def kasir = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KASIR%"),'0',session.userCompanyDealer)?.nama
        def kabeng = Karyawan?.findByJabatanAndBranchAndStaDel(ManPower?.findByM014JabatanManPowerIlikeAndStaDel("%"+"KEPALA BENGKEL"+"%","0"),session?.userCompanyDealer, "0")
        reportData <<[
                lblTambahan : "No. ",
                noTambahan  : params?.nomorKuitansi,
                jenis       : jenis,
                no          : "",
                perhatian3  : perhatian3,
                perhatian   : perhatian,
                perhatian2  : perhatian2,
                pembayar    : kwitansi?.t722Nama,
                terbilang   : moneyUtil.convertMoneyToWords(kwitansi?.t722JmlUang ? kwitansi?.t722JmlUang: 0.toDouble()),
                tujuan      : "Pembayaran "+bayar+" / "+kwitansi?.t722Nopol + " / " + si_app,
                jumlah      : konversi.toRupiah(kwitansi?.t722JmlUang),
                tggl        : kwitansi?.companyDealer?.kabKota?.m002NamaKabKota+" , "+datatablesUtilService?.syncTime()?.format("dd MMMM yyyy"),
                tgglPrint   : datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm:ss"),
                kasir       : user?.fullname,
                kabeng      : kabeng ? kabeng?.nama : ""
        ]
        List<JasperReportDef> reportDefList = []
        def reportDef = new JasperReportDef(name:'Kwitansi.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        reportDefList.add(reportDef);

        def file = File.createTempFile("Kwitansi_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }

    private void clearDataSession() {
        session['detailPembayaran'] = []
        session['totalPembayaran'] = 0.0
        session['receptionInstance'] = null
        session['historyCustomerInstance'] = null
        session['customerInstance'] = null
        session['companyDealerInstance'] = null
    }
}
