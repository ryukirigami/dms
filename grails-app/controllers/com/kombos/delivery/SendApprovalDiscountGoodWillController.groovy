package com.kombos.delivery

import com.kombos.maintable.InvoiceT701
import grails.converters.JSON

class SendApprovalDiscountGoodWillController {

    def sendResponseApprovalService

    def datatablesUtilService

    static allowedMethods = [save: "POST"]

    static addPermissions = ['save']

    static viewPermissions = ['index']

    def index() {
    }

    def sendApprovalDataTablesList() {
        def result = sendResponseApprovalService.sendApprovalDataTablesList(params)
        render result as JSON
    }

    def sendApproval() {
//        params.dateCreated = datatablesUtilService?.syncTime()
//        params.lastUpdated = datatablesUtilService?.syncTime()
        String nomorInvoice = params.nomorInvoice
        String persenRequest = params.persenRequest
        String nominalRequest = params.nominalRequest
        InvoiceT701 invoice = InvoiceT701.findByT701NoInv(nomorInvoice)
        if (persenRequest != null && !persenRequest.equals("")) {
            Double persen = Double.parseDouble(persenRequest)
            invoice.t701RequestGoodWillPersen = persen
            invoice.t701RequestGoodWillRp = persen * invoice.t701TotalInv / 100
        }
        if (nominalRequest != null && !nominalRequest.equals("")) {
            Double nominal = Double.parseDouble(nominalRequest)
            invoice.t701RequestGoodWillRp = nominal
            invoice.t701RequestGoodWillPersen = invoice.t701TotalInv / (nominal)
        }
        invoice.t701StaApprovalGoodWill = "0"
//        invoice.t701TglRequestApprovalGoodWill = new Date()
        invoice.t701TglRequestApprovalGoodWill = datatablesUtilService?.syncTime()
        invoice.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        invoice.save(flush: true)
        render "ok"
    }
}
