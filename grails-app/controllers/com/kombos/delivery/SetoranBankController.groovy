package com.kombos.delivery

import com.kombos.maintable.KasKasir
import grails.converters.JSON

class SetoranBankController {

    def setoranBankService

    static allowedMethods = [save: "POST"]

    static addPermissions = ['save']

    static viewPermissions = ['index']

    def index() {
        KasKasir kasKasir = KasKasir.first()
        [kasKasirInstance: kasKasir]

    }

    def setoranBankDataTablesList() {
        def result = setoranBankService.setoranBankDataTablesList(params)
        render result as JSON
    }

    def saveSetor() {
        KasKasir kasKasirInstance = params.kasKasirInstance
    }
}
