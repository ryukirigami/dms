package com.kombos.test

class TestController {

    def testService

    def index() {
        /*grailsApplication.config.environments.development.dataSource_lookup = {
            dialect = org.hibernate.dialect.MySQLInnoDBDialect
            driverClassName = 'menu.mysql.jdbc.Driver'
            username = 'lookup'
            password = 'secret'
            url = 'jdbc:mysql://localhost/lookup'
            dbCreate = 'update'
        }*/

        testService.test()

        /*[grails:[
            project: [groupId: QualityControlMonitor],
             mime: [file: [extensions: true], use: [accept: [header: false]], types: [html: [text / html, application / xhtml + xml], xml: [text / xml, application / xml], text: text / plain, js: text / javascript, rss: application / rss + xml, atom: application / atom + xml, css: text / css, csv: text / csv, all: '', json: [application / json, text / json], form: application / x - www - form - urlencoded, multipartForm: multipart / form - data]],
             resources: [adhoc: [patterns: [/images/ *, /css/ *, /js/ *, /plugins/ *], includes: [:], excludes: [:]], uri: [prefix: [:]], processing: [enabled: [:]], optional: [dispositions: [:]], mappers: [cssrewriter: [includes: [:], excludes: [:]], bundle: [includes: [:], excludes: [:]], csspreprocessor: [includes: [:], excludes: [:]]], modules: [:], work: [dir: [:]], debug: [:], rewrite: [css: [:]]],
             views: [default: [codec: none], gsp: [encoding: UTF - 8, sitemesh: [preprocess: true]], enable: [jsessionid: [:]], javascript: [library: [:]]],
             converters: [encoding: UTF - 8],
             scaffolding: [templates: [domainSuffix: Instance]],
             json: [legacy: [builder: false]],
             enable: [native2ascii: true],
             spring: [bean: [packages: []], disable: [aspectj: [autoweaving: [:]]]],
             web: [disable: [multipart: false], url: [converter: [:]]],
             exceptionresolver: [params: [exclude: [password]]],
             hibernate: [cache: [queries: true]],
             logging: [jul: [usebridge: true]],
             config: [defaults: [locations: [:]], locations: [:]],
             com.kombos.baseapp: [context: [:]],
             gorm: [default: [constraints: [:], mapping: [:]]],
             dbconsole: [enabled: [:], urlRoot: [:]],
             naming: [entries: [:]],
             gsp: [view: [dir: [:]]],
             serverURL: [:],
             disableCommonsMultipart: [:],
             controllers: [defaultScope: [:]],
             validateable: [classes: [:]]
            ],
            log4j: Config$_run_closure2 @9 b36be,
            dataSource: [pooled: true, driverClassName: org.h2.Driver, username: sa, password:, dbCreate: create - drop, url: jdbc: h2: mem: devDb; MVCC = TRUE; LOCK_TIMEOUT = 10000, jndiName: [:], readOnly: [:], properties: [:], configClass: [:], loggingSql: [:], logSql: [:], formatSql: [:], dialect: [:]],
            hibernate: [cache: [use_second_level_cache: true, use_query_cache: false, region: [factory_class: net.sf.ehcache.hibernate.EhCacheRegionFactory], provider_class: [:]], naming_strategy: [:], config: [location: [:]], flush: [mode: [:]]],
            dataSource_lookup: [driverClassName: org.h2.Driver, username: lookup, password: secret, url: jdbc: h2: prodDb; MVCC = TRUE; LOCK_TIMEOUT = 10000, dbCreate: update, jndiName: [:], readOnly: [:], pooled: [:], passwordEncryptionCodec: [:], properties: [:], configClass: [:], loggingSql: [:], logSql: [:], formatSql: [:], dialect: [:]],
            hibernate_lookup: [:],
            beans: [:],
            jquery: [defaultPlugins: [:]],
            environments: [development: [dataSource_lookup: [:]]]
        ]*/

        render """\
            ${message(code: "app.today.label",default: "Today is")} ${formatDate(date: new Date())}<br />
            ${message(code: "app.yourmoney.label",default: "Your money is")} ${formatNumber(number: new Double(100000000.00), type: "currency")}
        """
    }
}
