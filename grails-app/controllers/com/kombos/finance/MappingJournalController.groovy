package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class MappingJournalController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def mappingJournalService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render mappingJournalService.datatablesList(params) as JSON
	}

	def create() {

		def result = mappingJournalService.create(params)

        if(!result.error)
            return [mappingJournalInstance: result.mappingJournalInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        def result = mappingJournalService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["MappingJournal", result.mappingJournalInstance.id])
            redirect(action:'show', id: result.mappingJournalInstance.id)
            return
        }

        render(view:'create', model:[mappingJournalInstance: result.mappingJournalInstance])
	}

	def show(Long id) {
		def result = mappingJournalService.show(params)

		if(!result.error)
			return [ mappingJournalInstance: result.mappingJournalInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = mappingJournalService.show(params)

		if(!result.error){
            def dataDetail = MappingJournalDetail.createCriteria().list {
                eq("staDel","0");
                eq("mappingJournal",result.mappingJournalInstance)
                order("accountTransactionType")
                accountNumber{
                    order("accountNumber")
                }
            }
            return [ mappingJournalInstance: result.mappingJournalInstance,dataDetail:dataDetail, params: params ]
        }

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = mappingJournalService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["MappingJournal", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[mappingJournalInstance: result.mappingJournalInstance.attach()])
	}

	def delete() {
		def result = mappingJournalService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["MappingJournal", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(MappingJournal, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def showDetail() {
        def criteria = AccountNumber.createCriteria()
        def results = criteria.list() {
            eq("staDel","0")
            eq("accountMutationType", "MUTASI")
            order("accountNumber","asc")
        }
        render results as JSON
    }

    def removeDetail()
    {
        if (params.detailid && params.mappingJournalId) {
            def mappingJournalInstance = MappingJournal.findById(Integer.parseInt(params.mappingJournalId as String))
            if (mappingJournalInstance) {
                def detailJournalInstance = MappingJournalDetail.findById(Integer.parseInt(params.detailid as String))
                mappingJournalInstance.removeFromMappingJournalDetail(detailJournalInstance)
                detailJournalInstance.delete()
                if (mappingJournalInstance.save(flush: true))
                    render "Success"
            }
        }
    }
}
