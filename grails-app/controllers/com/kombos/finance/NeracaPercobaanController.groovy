package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class NeracaPercobaanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
    def index() {}
    def previewData(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Date tgl = new Date().parse('dd/MM/yyyy',params.tanggal)
        Date tglAwal = new Date().parse('dd/MM/yyyy',params.tanggal)
        tglAwal.setDate(1)
        String yearMonth = new MonthlyBalanceController().lastYearMonth(tgl.format("MM").toInteger(),tgl.format("yyyy").toInteger())
        def reportData = []
        def dataKiri = []
        def dataKanan = []
        def totalKiriAkun1 = 0.0;
        def results = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","1%")
                like("accountNumber","2%")
                like("accountNumber","3%")
                order("accountNumber","asc")
            }
        }
        def sumTotalKiri = 0.00, totalKiri = 0.00, perBarisKiri = 0.00

        results.each { an ->
            sumTotalKiri = 0.00
            def idSudahKiri = []
            def subLedger = ""
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", yearMonth)
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    ilike("accountNumber",an.accountNumber+ "%")
                    order("accountNumber")
                }
                order("subType")
                order("subLedger")
            }
            def dataJurnalDetail = JournalDetail.createCriteria().list {
                eq("staDel","0");
                accountNumber{
                    eq("staDel","0")
                    ilike("accountNumber",an.accountNumber + "%")
                    order("accountNumber")
                }
                journal{
                    eq("companyDealer",companyDealer)
                    ge("journalDate",tglAwal)
                    lt("journalDate",tgl + 1)
                    eq("isApproval", "1")
                    eq("staDel","0");
                }
                order("subType")
                order("subLedger")
            }

            if(data.size() > 0 || dataJurnalDetail.size() > 0 ){
                if(params?.level!="5"){
                    def totMB = 0.0
                    data.each{
                        sumTotalKiri  += it.endingBalance
                        totMB+=it?.endingBalance
                    }
                    def tesKiri = 0.0
                    dataJurnalDetail.each {
                        sumTotalKiri  += (it?.debitAmount-it.creditAmount)
                        tesKiri  += (it?.debitAmount-it.creditAmount)
                    }
                    if(sumTotalKiri!=0){
                        dataKiri << [
                                rekeningNumber : an?.accountNumber,
                                rekeningNama : an?.accountName,
                                rekeningValue : sumTotalKiri < 0 ? "(" + konversi.toRupiah((sumTotalKiri * (-1))) + ")": konversi.toRupiah(sumTotalKiri),
                        ]
                    }
                }else {
                    data.each {
                        perBarisKiri = 0.00
                        sumTotalKiri  += it.endingBalance
                        perBarisKiri += it.endingBalance
                        AccountNumber acc = it?.accountNumber
                        def subL = it?.subLedger
                        def subT = it?.subType
                        subLedger = cariNamaSubledger(it?.subType,it?.subLedger)
                        def dataJurnalDetailSama = JournalDetail.createCriteria().list {
                            eq("staDel","0")
                            accountNumber{
                                eq("staDel","0")
                                eq("accountNumber",acc?.accountNumber)
                                order("accountNumber")
                            }
                            journal{
                                eq("companyDealer",companyDealer)
                                ge("journalDate",tglAwal)
                                lt("journalDate",tgl + 1)
                                eq("isApproval", "1")
                                eq("staDel","0")
                            }
                            if(subL){
                                eq("subLedger",subL)
                            }else {
                                isNull("subLedger")
                            }
                            if(subT){
                                eq("subType",subT)
                            }else {
                                isNull("subType")
                            }
                        }
                        def totDebet = 0.00,totKredit = 0.00
                        dataJurnalDetailSama?.each {
                            if(idSudahKiri.indexOf(it?.id)<0){
                                idSudahKiri<<it?.id
                                sumTotalKiri  += (it?.debitAmount-it?.creditAmount)
                                perBarisKiri += (it?.debitAmount-it?.creditAmount)
                                totDebet+=it?.debitAmount
                                totKredit+=it?.creditAmount
                            }
                        }

                        if(perBarisKiri!=0){
                            dataKiri << [
                                    rekeningNumber : an?.accountNumber,
                                    rekeningNama : an?.accountName?.toUpperCase() + subLedger,
                                    rekeningValue : perBarisKiri < 0 ? "(" + konversi.toRupiah((perBarisKiri * (-1))) + ")": konversi.toRupiah(perBarisKiri),
                            ]
                        }
                    }
                    dataJurnalDetail.each {
                        def totDebet = 0.00,totKredit = 0.00;
                        perBarisKiri = 0.00
                        if(idSudahKiri.indexOf(it?.id)<0){
                            def acc = it?.accountNumber
                            def subL = it?.subLedger
                            def subT = it?.subType
                            subLedger = cariNamaSubledger(it?.subType,it?.subLedger)
                            def dataJurnalDetailSama = JournalDetail.createCriteria().list {
                                eq("staDel","0")
                                accountNumber{
                                    eq("staDel","0")
                                    eq("accountNumber",acc?.accountNumber)
                                    order("accountNumber","asc")
                                }
                                journal{
                                    eq("staDel","0")
                                    eq("companyDealer",companyDealer)
                                    ge("journalDate",tglAwal)
                                    lt("journalDate",tgl + 1)
                                    eq("isApproval", "1")
                                }
                                if(subL){
                                    eq("subLedger",subL)
                                }else {
                                    isNull("subLedger")
                                }
                                if(subT){
                                    eq("subType",subT)
                                }else {
                                    isNull("subType")
                                }
                            }
                            dataJurnalDetailSama?.each {
                                if(idSudahKiri.indexOf(it?.id)<0){
                                    idSudahKiri<<it?.id
                                    sumTotalKiri  += (it.debitAmount-it?.creditAmount)
                                    perBarisKiri += (it.debitAmount-it?.creditAmount)
                                    totKredit+=it?.creditAmount
                                    totDebet+=it?.debitAmount
                                }
                            }
                        }

                        if(perBarisKiri!=0){
                            dataKiri << [
                                    rekeningNumber : an?.accountNumber,
                                    rekeningNama : an?.accountName + subLedger,
                                    rekeningValue : perBarisKiri < 0 ? "(" + konversi.toRupiah((perBarisKiri * (-1))) + ")": konversi.toRupiah(perBarisKiri),
                            ]
                        }
                    }
                }
                if(an?.accountNumber?.substring(0,1)=="1"){
                    totalKiriAkun1 +=sumTotalKiri
                }
                totalKiri += sumTotalKiri
            }
        }

        def results2 = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","4%")
                like("accountNumber","5%")
                like("accountNumber","6%")
                like("accountNumber","7%")
                like("accountNumber","8%")
                order("accountNumber","asc")
            }
        }
        def sumTotalKanan = 0.00, totalKanan = 0.00,perBarisKanan = 0.00
        results2.each { an ->
            sumTotalKanan = 0.00
            def idSudahKanan = []
            def subLedger = ""
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", yearMonth)
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    order("accountNumber","asc")
                }
                order("subType")
                order("subLedger")
            }
            def dataJurnalDetail = JournalDetail.createCriteria().list {
                eq("staDel","0")
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    order("accountNumber","asc")
                }
                journal{
                    eq("companyDealer",companyDealer)
                    ge("journalDate",tglAwal)
                    lt("journalDate",tgl + 1)
                    eq("isApproval", "1")
                    eq("staDel","0")
                }
                order("subType")
                order("subLedger")
            }

            if(data.size() > 0 || dataJurnalDetail.size() > 0 ){
                if(params?.level!="5"){
                    data.each{
                        def saldoAkhir = (it.endingBalance * -1)
                        sumTotalKanan  += saldoAkhir
                    }
                    dataJurnalDetail.each {
                        def saldoAkhir = ((it.creditAmount-it?.debitAmount))
                        sumTotalKanan  += saldoAkhir
                    }
                    if(sumTotalKanan!=0){
                        dataKanan << [
                                rekeningNumber : an?.accountNumber,
                                rekeningNama : an?.accountName,
                                rekeningValue : sumTotalKanan < 0 ? "(" + konversi.toRupiah((sumTotalKanan * -1)) + ")" : konversi.toRupiah(sumTotalKanan),
                        ]
                    }
                }else {
                    data.each {
                        perBarisKanan = 0.00
                        sumTotalKanan += (it.endingBalance * -1)
                        perBarisKanan += (it.endingBalance * -1)
                        AccountNumber acc = it?.accountNumber
                        def subL = it?.subLedger
                        def subT = it?.subType
                        subLedger = cariNamaSubledger(it?.subType,it?.subLedger)
                        def dataJurnalDetailSama = JournalDetail.createCriteria().list {
                            eq("staDel","0")
                            accountNumber{
                                eq("staDel","0")
                                eq("accountNumber",acc?.accountNumber)
                                order("accountNumber","asc")
                            }
                            journal{
                                eq("companyDealer",companyDealer)
                                ge("journalDate",tglAwal)
                                lt("journalDate",tgl + 1)
                                eq("isApproval", "1")
                                eq("staDel","0")
                            }
                            if(subL){
                                eq("subLedger",subL)
                            }else {
                                isNull("subLedger")
                            }
                            if(subT){
                                eq("subType",subT)
                            }else {
                                isNull("subType")
                            }
                        }
                        dataJurnalDetailSama?.each {
                            if(idSudahKanan.indexOf(it?.id)<0){
                                idSudahKanan<<it?.id
                                def saldoAkhir = ((it.creditAmount-it?.debitAmount))
                                sumTotalKanan += saldoAkhir
                                perBarisKanan += saldoAkhir
                            }
                        }
                        if(perBarisKanan!=0){
                            dataKanan << [
                                    rekeningNumber : an?.accountNumber,
                                    rekeningNama : an?.accountName?.toUpperCase() + subLedger,
                                    rekeningValue : perBarisKanan < 0 ? "(" + konversi.toRupiah((perBarisKanan * (-1))) + ")": konversi.toRupiah(perBarisKanan),
                            ]
                        }
                    }

                    dataJurnalDetail.each {
                        perBarisKanan = 0.00
                        if(idSudahKanan.indexOf(it?.id)<0){
                            def acc = it?.accountNumber
                            def subL = it?.subLedger
                            def subT = it?.subType
                            subLedger = cariNamaSubledger(it?.subType,it?.subLedger)
                            def dataJurnalDetailSama = JournalDetail.createCriteria().list {
                                eq("staDel","0")
                                accountNumber{
                                    eq("staDel","0")
                                    eq("accountNumber",acc?.accountNumber)
                                    order("accountNumber","asc")
                                }
                                journal{
                                    eq("companyDealer",companyDealer)
                                    ge("journalDate",tglAwal)
                                    lt("journalDate",tgl + 1)
                                    eq("isApproval", "1")
                                    eq("staDel","0")
                                }
                                if(subL){
                                    eq("subLedger",subL)
                                }else {
                                    isNull("subLedger")
                                }
                                if(subT){
                                    eq("subType",subT)
                                }else {
                                    isNull("subType")
                                }
                            }
                            dataJurnalDetailSama?.each {
                                if(idSudahKanan.indexOf(it?.id)<0){
                                    idSudahKanan<<it?.id
                                    def saldoAkhir = ((it.creditAmount-it?.debitAmount))
                                    sumTotalKanan += saldoAkhir
                                    perBarisKanan += saldoAkhir
                                }
                            }
                        }
                        if(perBarisKanan!=0){
                            dataKanan << [
                                    rekeningNumber : an?.accountNumber,
                                    rekeningNama : an?.accountName + subLedger,
                                    rekeningValue : perBarisKanan < 0 ? "(" + konversi.toRupiah((perBarisKanan * (-1))) + ")": konversi.toRupiah(perBarisKanan),
                            ]
                        }
                    }
                }

                totalKanan += sumTotalKanan
            }
        }

        totalKanan = totalKanan < 0 ? totalKanan * -1 : totalKanan
        reportData << [
                dataKiri : dataKiri,
                dataKanan : dataKanan,
                totalKiri : totalKiri < 0 ? "(" + konversi.toRupiah((totalKiri * (-1))) + ")" : konversi.toRupiah(totalKiri),
                totalKanan : totalKanan < 0 ? "(" + konversi.toRupiah((totalKanan * (-1))) + ")" : konversi.toRupiah(totalKanan),
                companyDealer : companyDealer,
                perTanggal : "Per Tanggal " + tgl.format("dd MMMM yyyy").toString(),
                judul : "NERACA PERCOBAAN"
        ]

        def reportDef = new JasperReportDef(name:'neraca.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("NERACA_PERCOBAAN("+companyDealer.id+")_LVL"+params?.level+"_"+tgl.format('ddMMyyyy')+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def cariNamaSubledger(SubType subType, String idSubLedger){
        String subLedger = " a.n ";
        if(subType?.description?.toUpperCase()=="CUSTOMER"){
            subLedger += HistoryCustomer.findById(idSubLedger as Long) ? HistoryCustomer.findById(idSubLedger as Long)?.fullNama : null
        }else if(subType?.description?.toUpperCase()=="VENDOR"){
            subLedger += Vendor.findById(idSubLedger as Long) ? Vendor.findById(idSubLedger as Long).m121Nama : null
        }else if(subType?.description?.toUpperCase()=="ASURANSI"){
            subLedger += VendorAsuransi.findById(idSubLedger as Long) ? VendorAsuransi.findById(idSubLedger as Long)?.m193Nama : null
        }else if(subType?.description?.toUpperCase()=="KARYAWAN"){
            subLedger += Karyawan.findById(idSubLedger as Long) ? Karyawan.findById(idSubLedger as Long).nama : null
        }else if(subType?.description?.toUpperCase()=="CABANG"){
            subLedger += CompanyDealer.findById(idSubLedger as Long) ? CompanyDealer.findById(idSubLedger as Long).m011NamaWorkshop : null
        }else if(subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
            subLedger += Company.findById(idSubLedger as Long) ? Company.findById(idSubLedger as Long).namaPerusahaan : null
        }

        if(subType==null && idSubLedger==null){
            subLedger = ""
        }
        return subLedger
    }
}
