package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Konversi
import com.kombos.reception.JobInv
import com.kombos.woinformation.JobRCP
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class KontribusiHasilController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
    def resetData(def data){
        for(int i=1;i<=13;i++){
            data[i] = 0
        }
    }
    def validasiData(def data){
        for(int i=1;i<=13;i++){
            if(data[i] == 0){
             data[i] = ""
            }else{
                data[i] = konversi.toRupiah(data[i].toString().toDouble())
            }
        }
    }
    def totalData(def data){
        def total = 0
        for(int i=1;i<=12;i++){
            if(data[i]!=""){
                def angka = data[i].toString().replaceAll(",","").toDouble()
                total += angka
            }
        }
        if(total!=0){
            total = konversi.toRupiah(total.toString().toDouble())
        }else{
            total =""
        }
        return total
    }
    def index() {}
    def previewData(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Date tglAwal = new Date().parse('dd/MM/yyyy',"01/01/" + params.tahun)
        Date tglAkhir = new Date().parse('dd/MM/yyyy',"31/12/" + params.tahun)
        def dataGrUnit = [], dataGrPendapatanJasa = [],dataGrPendapatanPart = [],dataGrPendapatanBahan = [],dataGrPendapatanSublet = [], dataGrPendapatanAdm = []
        def dataPendapatanJasa = [],dataPendapatanPart = [],dataPendapatanBahan = [],dataPendapatanSublet = [], dataPendapatanAdm = []
        def diatas100k = [], kelipatan5k = []

        def dataKm10000 = [],dataKm20000 = [],dataKm30000 = [],dataKm40000 = [],dataKm50000 = [],dataKm60000 = [],
                dataKm70000 = [],dataKm80000 = [],dataKm90000 = [],dataKm100000 = []

        def tDataKm = []

        def sbeUnit = []
        def sbeRekening = []
        def inv = InvoiceT701.createCriteria().list {
            eq("companyDealer",companyDealer)
            ge("t701TglJamInvoice", tglAwal)
            lt("t701TglJamInvoice", tglAkhir+1)
            ilike("t701Tipe","%0%");
            isNull("t701NoInv_Reff")
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            order("t701TglJamInvoice","asc")
        }

        resetData(dataKm10000)
        resetData(dataKm20000)
        resetData(dataKm30000)
        resetData(dataKm40000)
        resetData(dataKm50000)
        resetData(dataKm60000)
        resetData(dataKm70000)
        resetData(dataKm80000)
        resetData(dataKm90000)
        resetData(dataKm100000)
        resetData(tDataKm)
        resetData(diatas100k)
        resetData(kelipatan5k)

        resetData(dataGrUnit)
        resetData(dataGrPendapatanJasa)
        resetData(dataGrPendapatanPart)
        resetData(dataGrPendapatanBahan)
        resetData(dataGrPendapatanSublet)
        resetData(dataGrPendapatanAdm)

        resetData(dataPendapatanJasa)
        resetData(dataPendapatanPart)
        resetData(dataPendapatanBahan)
        resetData(dataPendapatanSublet)
        resetData(dataPendapatanAdm)

        inv.each {
            def invoice = it
            def kj = JobInv.createCriteria().get {
                eq("invoice",invoice);
                eq("staDel","0");
                operation{
                    ilike("m053Id","%SB%");
                }
                maxResults(1);
            }

            int bln= it.t701TglJamInvoice.format("M").toInteger()
            for(int i=1;i<=12;i++){
                if(bln==i){
                    dataGrUnit[i] += 1
                    dataGrPendapatanJasa[i] += it.t701JasaRp
                    dataGrPendapatanPart[i] += it.t701PartsRp
                    dataGrPendapatanBahan[i] += (it.t701MaterialRp + it.t701OliRp)
                    dataGrPendapatanSublet[i] += (it.t701SubletRp ? it.t701SubletRp : 0)
                    dataGrPendapatanAdm[i] += it.t701AdmRp
                    dataGrUnit[13]++
                }
            }

            if(kj!=null && (!kj?.operation?.m053Id?.equalsIgnoreCase("SB1K") || !kj?.operation?.m053NamaOperation?.contains("1.000") || !kj?.operation?.m053NamaOperation?.contains("1000") )){
                def sb = kj?.operation?.m053NamaOperation
                int a= it.t701TglJamInvoice.format("M").toInteger()
                for(int i=1;i<=12;i++){
                    if(a==i){
                        dataPendapatanJasa[i] += it.t701JasaRp
                        dataPendapatanPart[i] += it.t701PartsRp
                        dataPendapatanBahan[i] += (it.t701MaterialRp + it.t701OliRp)
                        dataPendapatanSublet[i] += (it.t701SubletRp ? it.t701SubletRp : 0)
                        dataPendapatanAdm[i] += it.t701AdmRp
                        if(sb.contains("10.000")){
                            dataKm10000[i]++
                            dataKm10000[13]++
                        }else if(sb.contains("20.000")){
                            dataKm20000[i]++
                            dataKm20000[13]++
                        }else if(sb.contains("30.000")){
                            dataKm30000[i]++
                            dataKm30000[13]++
                        }else if(sb.contains("40.000")){
                            dataKm40000[i]++
                            dataKm40000[13]++
                        }else if(sb.contains("50.000")){
                            dataKm50000[i]++
                            dataKm50000[13]++
                        }else if(sb.contains("60.000")){
                            dataKm60000[i]++
                            dataKm60000[13]++
                        }else if(sb.contains("70.000")){
                            dataKm70000[i]++
                            dataKm70000[13]++
                        }else if(sb.contains("80.000")){
                            dataKm80000[i]++
                            dataKm80000[13]++
                        }else if(sb.contains("90.000")){
                            dataKm90000[i]++
                            dataKm90000[13]++
                        }else if(sb.contains("100.000")){
                            dataKm100000[i]++
                            dataKm100000[13]++
                        }else if(it?.reception?.t401KmSaatIni>100000){
                            diatas100k[i]++
                            diatas100k[13]++
                        }
                    }
                    tDataKm[i] = (dataKm10000[i]+dataKm20000[i]+dataKm30000[i]+dataKm40000[i]+dataKm50000[i]+
                            dataKm60000[i]+dataKm70000[i]+dataKm80000[i]+dataKm90000[i]+dataKm100000[i]) + diatas100k[i]
                    tDataKm[13] = (dataKm10000[13]+dataKm20000[13]+dataKm30000[13]+dataKm40000[13]+dataKm50000[13]+
                            dataKm60000[13]+dataKm70000[13]+dataKm80000[13]+dataKm90000[13]+dataKm100000[13]) + diatas100k[13]
                }

            }
        }
        validasiData(dataKm10000)
        validasiData(dataKm20000)
        validasiData(dataKm30000)
        validasiData(dataKm40000)
        validasiData(dataKm50000)
        validasiData(dataKm60000)
        validasiData(dataKm70000)
        validasiData(dataKm80000)
        validasiData(dataKm90000)
        validasiData(dataKm100000)
        validasiData(tDataKm)
        validasiData(kelipatan5k)
        validasiData(diatas100k)

        validasiData(dataGrUnit)
        validasiData(dataGrPendapatanJasa)
        validasiData(dataGrPendapatanPart)
        validasiData(dataGrPendapatanBahan)
        validasiData(dataGrPendapatanSublet)
        validasiData(dataGrPendapatanAdm)

        validasiData(dataPendapatanJasa)
        validasiData(dataPendapatanPart)
        validasiData(dataPendapatanBahan)
        validasiData(dataPendapatanSublet)
        validasiData(dataPendapatanAdm)
        def gr = []
        def reportData = []
        sbeUnit << [
                km : konversi.toRupiah(10000),
                jan : dataKm10000[1],feb : dataKm10000[2],mar : dataKm10000[3],apr : dataKm10000[4],
                mei : dataKm10000[5],jun : dataKm10000[6],jul : dataKm10000[7],agu : dataKm10000[8],
                sept : dataKm10000[9],okt : dataKm10000[10],nov : dataKm10000[11],des : dataKm10000[12],
                totalSamping : dataKm10000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(20000),
                jan : dataKm20000[1],feb : dataKm20000[2],mar : dataKm20000[3],apr : dataKm20000[4],
                mei : dataKm20000[5],jun : dataKm20000[6],jul : dataKm20000[7],agu : dataKm20000[8],
                sept : dataKm20000[9],okt : dataKm20000[10],nov : dataKm20000[11],des : dataKm20000[12],
                totalSamping : dataKm20000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(30000),
                jan : dataKm30000[1],feb : dataKm30000[2],mar : dataKm30000[3],apr : dataKm30000[4],
                mei : dataKm30000[5],jun : dataKm30000[6],jul : dataKm30000[7],agu : dataKm30000[8],
                sept : dataKm30000[9],okt : dataKm30000[10],nov : dataKm30000[11],des : dataKm30000[12],
                totalSamping : dataKm30000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(40000),
                jan : dataKm40000[1],feb : dataKm40000[2],mar : dataKm40000[3],apr : dataKm40000[4],
                mei : dataKm40000[5],jun : dataKm40000[6],jul : dataKm40000[7],agu : dataKm40000[8],
                sept : dataKm40000[9],okt : dataKm40000[10],nov : dataKm40000[11],des : dataKm40000[12],
                totalSamping : dataKm40000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(50000),
                jan : dataKm50000[1],feb : dataKm50000[2],mar : dataKm50000[3],apr : dataKm50000[4],
                mei : dataKm50000[5],jun : dataKm50000[6],jul : dataKm50000[7],agu : dataKm50000[8],
                sept : dataKm50000[9],okt : dataKm50000[10],nov : dataKm50000[11],des : dataKm50000[12],
                totalSamping : dataKm50000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(60000),
                jan : dataKm60000[1],feb : dataKm60000[2],mar : dataKm60000[3],apr : dataKm60000[4],
                mei : dataKm60000[5],jun : dataKm60000[6],jul : dataKm60000[7],agu : dataKm60000[8],
                sept : dataKm60000[9],okt : dataKm60000[10],nov : dataKm60000[11],des : dataKm60000[12],
                totalSamping : dataKm60000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(70000),
                jan : dataKm70000[1],feb : dataKm70000[2],mar : dataKm70000[3],apr : dataKm70000[4],
                mei : dataKm70000[5],jun : dataKm70000[6],jul : dataKm70000[7],agu : dataKm70000[8],
                sept : dataKm70000[9],okt : dataKm70000[10],nov : dataKm70000[11],des : dataKm70000[12],
                totalSamping : dataKm70000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(80000),
                jan : dataKm80000[1],feb : dataKm80000[2],mar : dataKm80000[3],apr : dataKm80000[4],
                mei : dataKm80000[5],jun : dataKm80000[6],jul : dataKm80000[7],agu : dataKm80000[8],
                sept : dataKm80000[9],okt : dataKm80000[10],nov : dataKm80000[11],des : dataKm80000[12],
                totalSamping : dataKm80000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(90000),
                jan : dataKm90000[1],feb : dataKm90000[2],mar : dataKm90000[3],apr : dataKm90000[4],
                mei : dataKm90000[5],jun : dataKm90000[6],jul : dataKm90000[7],agu : dataKm90000[8],
                sept : dataKm90000[9],okt : dataKm90000[10],nov : dataKm90000[11],des : dataKm90000[12],
                totalSamping : dataKm90000[13]
        ]
        sbeUnit << [
                km : konversi.toRupiah(100000),
                jan : dataKm100000[1],feb : dataKm100000[2],mar : dataKm100000[3],apr : dataKm100000[4],
                mei : dataKm100000[5],jun : dataKm100000[6],jul : dataKm100000[7],agu : dataKm100000[8],
                sept : dataKm100000[9],okt : dataKm100000[10],nov : dataKm100000[11],des : dataKm100000[12],
                totalSamping : dataKm100000[13]
        ]

        sbeUnit << [
                km : "Kelipatan 5K di bawah 100K [15,25,35,dst]",
                jan : kelipatan5k[1],feb : kelipatan5k[2],mar : kelipatan5k[3],apr : kelipatan5k[4],
                mei : kelipatan5k[5],jun : kelipatan5k[6],jul : kelipatan5k[7],agu : kelipatan5k[8],
                sept : kelipatan5k[7],okt : kelipatan5k[10],nov : kelipatan5k[11],des : kelipatan5k[12]
        ]
        sbeUnit << [
                km : "SBE km lainnya di atas 100K",
                jan : diatas100k[1],feb : diatas100k[2],mar : diatas100k[3],apr : diatas100k[4],
                mei : diatas100k[5],jun : diatas100k[6],jul : diatas100k[7],agu : diatas100k[8],
                sept : diatas100k[9],okt : diatas100k[10],nov : diatas100k[11],des : diatas100k[12],totalSamping : diatas100k[13],

                tJan : tDataKm[1],tFeb : tDataKm[2],tMar : tDataKm[3],tApr : tDataKm[4],
                tMei : tDataKm[5],tJun : tDataKm[6],tJul : tDataKm[7],tAgu : tDataKm[8],
                tSept : tDataKm[9],tOkt : tDataKm[10],tNov : tDataKm[11],tDes : tDataKm[12], total : tDataKm[13]
        ]
        sbeRekening << [
                km : "PENDAPATAN JASA",
                jan : dataPendapatanJasa[1],feb : dataPendapatanJasa[2],mar : dataPendapatanJasa[3],apr : dataPendapatanJasa[4],
                mei : dataPendapatanJasa[5],jun : dataPendapatanJasa[6],jul : dataPendapatanJasa[7],agu : dataPendapatanJasa[8],
                sept : dataPendapatanJasa[9],okt : dataPendapatanJasa[10],nov : dataPendapatanJasa[11],des : dataPendapatanJasa[12],
                totalSamping : totalData(dataPendapatanJasa)
        ]
        sbeRekening << [
                km : "PENDAPATAN PART",
                jan : dataPendapatanPart[1],feb : dataPendapatanPart[2],mar : dataPendapatanPart[3],apr : dataPendapatanPart[4],
                mei : dataPendapatanPart[5],jun : dataPendapatanPart[6],jul : dataPendapatanPart[7],agu : dataPendapatanPart[8],
                sept : dataPendapatanPart[9],okt : dataPendapatanPart[10],nov : dataPendapatanPart[11],des : dataPendapatanPart[12],
                totalSamping : totalData(dataPendapatanPart)
        ]
        sbeRekening << [
                km : "PENDAPATAN BAHAN",
                jan : dataPendapatanBahan[1],feb : dataPendapatanBahan[2],mar : dataPendapatanBahan[3],apr : dataPendapatanBahan[4],
                mei : dataPendapatanBahan[5],jun : dataPendapatanBahan[6],jul : dataPendapatanBahan[7],agu : dataPendapatanBahan[8],
                sept : dataPendapatanBahan[9],okt : dataPendapatanBahan[10],nov : dataPendapatanBahan[11],des : dataPendapatanBahan[12],
                totalSamping : totalData(dataPendapatanBahan)
        ]
        sbeRekening << [
                km : "PENDAPATAN PEKERJAAN LUAR",
                jan : dataPendapatanSublet[1],feb : dataPendapatanSublet[2],mar : dataPendapatanSublet[3],apr : dataPendapatanSublet[4],
                mei : dataPendapatanSublet[5],jun : dataPendapatanSublet[6],jul : dataPendapatanSublet[7],agu : dataPendapatanSublet[8],
                sept : dataPendapatanSublet[9],okt : dataPendapatanSublet[10],nov : dataPendapatanSublet[11],des : dataPendapatanSublet[12],
                totalSamping : totalData(dataPendapatanSublet)
        ]
        sbeRekening << [
                km : "PENDAPATAN ADM",
                jan : dataPendapatanAdm[1],feb : dataPendapatanAdm[2],mar : dataPendapatanAdm[3],apr : dataPendapatanAdm[4],
                mei : dataPendapatanAdm[5],jun : dataPendapatanAdm[6],jul : dataPendapatanAdm[7],agu : dataPendapatanAdm[8],
                sept : dataPendapatanAdm[9],okt : dataPendapatanAdm[10],nov : dataPendapatanAdm[11],des : dataPendapatanAdm[12],
                totalSamping : totalData(dataPendapatanAdm)
        ]
        sbeRekening << [
                km : "",
        ]
        gr << [
                km : "SERVICE GR",
                jan : dataGrUnit[1],feb : dataGrUnit[2],mar : dataGrUnit[3],apr : dataGrUnit[4],
                mei : dataGrUnit[5],jun : dataGrUnit[6],jul : dataGrUnit[7],agu : dataGrUnit[8],
                sept : dataGrUnit[9],okt : dataGrUnit[10],nov : dataGrUnit[11],des : dataGrUnit[12],
                totalSamping : dataGrUnit[13]
        ]
        gr << [
                km : "PENDAPATAN JASA",
                jan : dataGrPendapatanJasa[1],feb : dataGrPendapatanJasa[2],mar : dataGrPendapatanJasa[3],apr : dataGrPendapatanJasa[4],
                mei : dataGrPendapatanJasa[5],jun : dataGrPendapatanJasa[6],jul : dataGrPendapatanJasa[7],agu : dataGrPendapatanJasa[8],
                sept : dataGrPendapatanJasa[9],okt : dataGrPendapatanJasa[10],nov : dataGrPendapatanJasa[11],des : dataGrPendapatanJasa[12],
                totalSamping : totalData(dataGrPendapatanJasa)
        ]
        gr << [
                km : "PENDAPATAN PART",
                jan : dataGrPendapatanPart[1],feb : dataGrPendapatanPart[2],mar : dataGrPendapatanPart[3],apr : dataGrPendapatanPart[4],
                mei : dataGrPendapatanPart[5],jun : dataGrPendapatanPart[6],jul : dataGrPendapatanPart[7],agu : dataGrPendapatanPart[8],
                sept : dataGrPendapatanPart[9],okt : dataGrPendapatanPart[10],nov : dataGrPendapatanPart[11],des : dataGrPendapatanPart[12],
                totalSamping : totalData(dataGrPendapatanPart)
        ]
        gr << [
                km : "PENDAPATAN BAHAN",
                jan : dataGrPendapatanBahan[1],feb : dataGrPendapatanBahan[2],mar : dataGrPendapatanBahan[3],apr : dataGrPendapatanBahan[4],
                mei : dataGrPendapatanBahan[5],jun : dataGrPendapatanBahan[6],jul : dataGrPendapatanBahan[7],agu : dataGrPendapatanBahan[8],
                sept : dataGrPendapatanBahan[9],okt : dataGrPendapatanBahan[10],nov : dataGrPendapatanBahan[11],des : dataGrPendapatanBahan[12],
                totalSamping : totalData(dataGrPendapatanBahan)
        ]
        gr << [
                km : "PENDAPATAN PEKERJAAN LUAR",
                jan : dataGrPendapatanSublet[1],feb : dataGrPendapatanSublet[2],mar : dataGrPendapatanSublet[3],apr : dataGrPendapatanSublet[4],
                mei : dataGrPendapatanSublet[5],jun : dataGrPendapatanSublet[6],jul : dataGrPendapatanSublet[7],agu : dataGrPendapatanSublet[8],
                sept : dataGrPendapatanSublet[9],okt : dataGrPendapatanSublet[10],nov : dataGrPendapatanSublet[11],des : dataGrPendapatanSublet[12],
                totalSamping : totalData(dataGrPendapatanSublet)
        ]
        gr << [
                km : "PENDAPATAN ADM",
                jan : dataGrPendapatanAdm[1],feb : dataGrPendapatanAdm[2],mar : dataGrPendapatanAdm[3],apr : dataGrPendapatanAdm[4],
                mei : dataGrPendapatanAdm[5],jun : dataGrPendapatanAdm[6],jul : dataGrPendapatanAdm[7],agu : dataGrPendapatanAdm[8],
                sept : dataGrPendapatanAdm[9],okt : dataGrPendapatanAdm[10],nov : dataGrPendapatanAdm[11],des : dataGrPendapatanAdm[12],
                totalSamping : totalData(dataGrPendapatanAdm)
        ]
        reportData << [
                sbeUnit : sbeUnit,
                sbeRekening : sbeRekening,
                gr : gr,
                companyDealer : companyDealer,
                tahun : "Tahun " + params.tahun
        ]

        def reportDef = new JasperReportDef(name:'kontribusiHasil.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("LAP_KONTRIBUSIHASIL("+session?.userCompanyDealerId+")"+params.tahun+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
