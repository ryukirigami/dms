package com.kombos.finance

import com.kombos.administrasi.Bank
import com.kombos.administrasi.GeneralParameter
import com.kombos.generatecode.GenerateCodeService
import com.kombos.parts.Konversi
import grails.converters.JSON

import java.text.SimpleDateFormat

class PengeluaranLainLainController {

    private static int SHOW_BANK_LIST = 0
    private static int SHOW_ACCOUNT_NUMBER_LIST = 1
    def datatablesUtilService

    static allowedMethods = [insert: 'POST']
    static addPermissions = ['insert']
    def konversi = new Konversi()
    def index() {
        def noteSaldo = ""
        def saldoGM = 0
        def saldoKas = 0
        def generalParameter = GeneralParameter.findByCompanyDealer(session.userCompanyDealer)
        def saldoAkhir = TransactionOperasional.findByCompanyDealer(session.userCompanyDealer,[sort : 'dateCreated' , order : 'desc'])
        if(generalParameter){
            saldoGM = generalParameter?.m000SaldoKasOperasional
        }
        if(saldoAkhir){
            saldoKas = saldoAkhir?.ta022Saldo
        }
        if(saldoKas < saldoGM){
//            noteSaldo = "Saldo Kas Operasional < Rp." + konversi.toRupiah(saldoGM as int)
        }
        [noteSaldo:noteSaldo]
    }

    def showDetails() {
        def ret = []
        switch (Integer.parseInt(params.type)) {
            case SHOW_ACCOUNT_NUMBER_LIST:
                if(params.noAkun && params.namaAkun){
                     ret = AccountNumber.findAllByAccountNameIlikeAndAccountNumberIlikeAndAccountMutationTypeAndStaDel("%"+params.namaAkun+"%","%"+params.noAkun+"%","MUTASI","0")
                }else if(params.namaAkun){
                     ret = AccountNumber.findAllByAccountNameIlikeAndAccountMutationTypeAndStaDel("%"+params.namaAkun+"%","MUTASI","0")
                }else if(params.noAkun){
                     ret = AccountNumber.findAllByAccountNumberIlikeAndAccountMutationTypeAndStaDel("%"+params.noAkun+"%","MUTASI","0")
                }else{
                    ret = AccountNumber.findAllByAccountMutationTypeAndStaDel("MUTASI","0")
                }
                break
            case SHOW_BANK_LIST:
                def bank = BankAccountNumber.createCriteria().list {eq("companyDealer",session?.userCompanyDealer);eq("staDel","0");bank{order("m702NamaBank")}}
                bank.each {data ->
                    ret << [
                            bankId: data.bank.id,
                            bankName: data.bank.m702NamaBank,
                            bankCabang: data.bank.m702Cabang,
                            accountNumber: data.accountNumber.accountNumber
                    ]
                }

                break
        }

        render ret as JSON
    }

    def insert() {
        String isOperasional       = params.biayaOperasional
        def transactionCode        = new GenerateCodeService().generateTransactionCode()
        def transactionType        = TransactionType.findByTransactionType(params.transactionType as String)
        def transactionDate        = Date.parse("dd/MM/yyyy", params.transactionDate as String)
        def amount                 = params?.amount?.toString()?.toBigDecimal()
        def description            = params.description
        def biayaOperasional       = params.biayaOperasional
        def bank                   = params.bankId? Bank.findById(Integer.parseInt(params.bankId as String)) : null
        def bankAccountNumber      = params.bankId ? BankAccountNumber.findByBankAndCompanyDealer(bank,session?.userCompanyDealer) : null  //BankAccountNumber.findByAccountNumber(kasAccount)
        def transferDate           = params.transferData ? Date.parse("dd/mm/yyyy", params.transferDate as String) : null
        def dueDate                = params.dueDae? Date.parse("dd/mm/yyyy", params.dueDate as String) : null
        def docCategory            = DocumentCategory.findByDocumentCategory("DOCKL")
        def journalType = JournalType.findByJournalType("TJ")
        def journalCode = new GenerateCodeService().generateJournalCode("TJ")
        def ret                    = [:]
        def journal     = new Journal()
        journal.companyDealer = session.userCompanyDealer
        journal.journalCode = journalCode
        journal.journalDate = transactionDate
        journal.isOperasional = isOperasional
        journal.isSetoranBank = params?.setoranBank
        journal.isApproval = "0"
        journal.totalDebit = amount
        journal.totalCredit = amount
        journal.description = description? description : ""
        journal.journalType = journalType
        journal.docNumber = journalCode
        journal.staDel = "0"
        journal.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        journal.lastUpdProcess = "INSERT"
        journal.dateCreated = datatablesUtilService.syncTime()
        journal.lastUpdated =  datatablesUtilService.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journal?.journalCode,journal?.description,journal?.totalDebit,journal?.totalCredit);
        if(checkExist){
            ret.error = "DUPLICATE"
        }else{
            journal.save(flush: true, failOnError: true)
            journal?.errors?.each {
                println it
            }
            if (params.details) {
                def detailArray = JSON.parse(params.details)
                def rowCount    = 0
                detailArray.each {detail ->
                    def total       = detail?.total?.toString()?.toBigDecimal()
                    def subtype                = detail.subtype ? SubType.findByDescriptionIlike("%"+detail.subtype+"%") : null
                    def subdleger              = detail.subledger

                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            creditAmount: (detail.transactionType as String).equalsIgnoreCase("Debet") ? 0 : total,
                            debitAmount: (detail.transactionType as String).equalsIgnoreCase("Debet") ? total : 0,
                            staDel: "0",
                            accountNumber: AccountNumber.findByAccountNumber(detail.accountNumber),
                            journal: journal,
                            subType: subtype,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: subdleger,
                            description: detail?.description,
                            dateCreated: datatablesUtilService.syncTime(),
                            lastUpdated:  datatablesUtilService.syncTime()

                    ).save(flush: true, failOnError: true)
                    journalDetail?.errors?.each {
                        println it
                    }

                    if (journalDetail.hasErrors()) {
                        throw new Exception("${journal.errors}")
                    } else {
                        rowCount++
                    }
                }
                ret.total = rowCount
            }
        }
        if (docCategory) {
            def saldo = 0;
            def trxOpt = TransactionOperasional.findByCompanyDealer(session.userCompanyDealer,[sort : 'dateCreated' , order : 'desc'])
            if(trxOpt){
                saldo = trxOpt?.ta022Saldo
            }
                def transaction = new Transaction(
                        companyDealer: session.userCompanyDealer,
                        documentCategory: docCategory,
                        transactionCode: transactionCode,
                        staOperasional: biayaOperasional,
                        transactionType: transactionType,
                        transactionDate: transactionDate,
                        transferDate: transferDate,
                        amount: amount,
                        journal: journal,
                        inOutType: "OUT",
                        description: description,
                        bankAccountNumber: bankAccountNumber,
                        dueDate: dueDate,
                        isOperasional: params?.biayaOperasional,
                        isSetoranBank: params?.setoranBank,
                        staDel: "0",
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        dateCreated: datatablesUtilService.syncTime(),
                        lastUpdated:  datatablesUtilService.syncTime()

                ).save(flush: true, failOnError: true)

                if (transaction.hasErrors()) {
                    throw new Exception("${transaction.errors}")
                } else {

                    if(isOperasional=="1"){
                        def transaksiOperasional = new TransactionOperasional(
                                transaction : transaction,
                                companyDealer : session.userCompanyDealer,
                                ta022Saldo :  saldo - amount,
                                staDel : '0',
                                createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                                lastUpdProcess: "INSERT",
                                dateCreated: datatablesUtilService.syncTime(),
                                lastUpdated:  datatablesUtilService.syncTime()

                        ).save(flush: true, failOnError: true)
                    }
                }

        } else {
            ret.error = "Document Category tidak ditemukan!"
        }

        ret.codeTrx = (transactionCode ? transactionCode : -1)
        render ret as JSON
    }

}
//woke