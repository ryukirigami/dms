package com.kombos.finance

import com.kombos.administrasi.KegiatanApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.ApprovalT770
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Invoice
import com.kombos.parts.Konversi
import grails.converters.JSON

class CashBalanceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def cashBalanceService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def noLastMB = false;
        def metodTggl = new MonthlyBalanceController()
        def lastYM = metodTggl.lastYearMonth((new Date()?.format("MM")).toInteger(),(new Date()?.format("yyyy")).toInteger())
        def cekMB = MonthlyBalance.findByCompanyDealerAndYearMonth(session?.userCompanyDealer,lastYM);
        if(!cekMB){
            noLastMB = true;
        }
        [noLastMB : noLastMB]
    }

    def listInvoice(Integer max) {
        def tgglCari = new Date().clearTime()
        if(params?.tgglCari){
            tgglCari =  new Date().parse("dd/MM/yyyy",params?.tgglCari?.toString())
        }
        [tgglCari : tgglCari]
    }

    def doMigrateInvoice(){
        String hasil = "";
        def jsonInv = JSON.parse(params.jsonInv);
        def invoice = InvoiceT701.get(jsonInv?.last()?.toString()?.toLong())
        if(invoice){
            hasil = "ada";
            def kegiatan = KegiatanApproval.findByM770KegiatanApprovalAndStaDel(KegiatanApproval.MIGRATE_INVOICE,"0")
            def approval = new ApprovalT770(
                t770FK:invoice?.id,
                companyDealer: session?.userCompanyDealer,
                kegiatanApproval: kegiatan,
                t770NoDokumen: invoice?.t701NoInv,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: "Perubahan jenis invoice, No. in "+invoice?.t701NoInv,
                dateCreated: datatablesUtilService.syncTime(),
                lastUpdated: datatablesUtilService.syncTime()
            )
            approval.save(flush:true)
            if(approval.hasErrors()){
                hasil = "gagal";
                approval.errors.each{ println it }
            }else {
                invoice?.t701StaApproveMigrate = "2";
                invoice?.save(flush: true);
            }
        }else {
            hasil = "gagal";
        }
        render hasil
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render cashBalanceService.datatablesList(params) as JSON
    }

    def datatablesListInvoice() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render cashBalanceService.datatablesListInvoice(params) as JSON
    }

    def create() {
        def result      = cashBalanceService.create(params)
        def pecahanUang = PecahanUang.createCriteria()
        def pecahanUangKertas = pecahanUang.list {
            eq("type", "Kertas")
        }
        def logam = PecahanUang.createCriteria()
        def pecahanUangLogam = logam.list {
            eq("type", "Logam")
        }
        Date today = new Date().clearTime()

        def dataCari = cariData(today)

        def konversi = new Konversi()

        if (!result.error)
            return [cashBalanceInstance: result.cashBalanceInstance, saldoAkhirKas: konversi.toRupiah(dataCari?.saldoAkhirKas), totalMutasiNonOpHariIni: konversi.toRupiah(dataCari?.totalMutasiNonOpHariIni), totalMutasiOpHariIni: konversi.toRupiah(dataCari?.totalMutasiOpHariIni), pecahanUangKertas: pecahanUangKertas, pecahanUangLogam: pecahanUangLogam, saldoAkhirKasKemarin: konversi.toRupiah(dataCari?.saldoAkhirKasKemarin),konversi : konversi]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def cekTanggal(){
        def hasil = false
        def tanggal = new Date().parse("dd/MM/yyyy",params.tanggal)
        def cekcb = CashBalance.createCriteria().list {
            eq("companyDealer", session.userCompanyDealer)
            eq("staDel", "0")
            ge("requiredDate", tanggal)
            lt("requiredDate", tanggal+1)
        }
        if(cekcb.size()>0){
            hasil = true
        }
        render hasil
    }

    def changeDate(){
        Date today = new Date().parse("dd/MM/yyyy",params?.tanggal?.toString())

        def dataCari = cariData(today)
        def konversi = new Konversi()
        def ret = [saldoAkhirKas: konversi.toRupiah(dataCari?.saldoAkhirKas), totalMutasiNonOpHariIni: konversi.toRupiah(dataCari?.totalMutasiNonOpHariIni), totalMutasiOpHariIni: konversi.toRupiah(dataCari?.totalMutasiOpHariIni), saldoAkhirKasKemarin: konversi.toRupiah(dataCari?.saldoAkhirKasKemarin)]
        render ret as JSON
    }

    def viewDetail(){
        def cb = CashBalance.get(params.id.toLong());
        def cbDetailKertas = CashBalanceDetail.createCriteria().list {
            eq("staDel","0");
            eq("cashBalance",cb)
            pecahanUang{
                ilike("type","%Kertas%")
                order("nilai","desc")
            }
        }
        def rows = []
        cbDetailKertas.each {
            rows << [
                    nilai : it?.pecahanUang?.nilai,
                    jum   : it?.quantity,
                    total : it?.quantity * it?.pecahanUang?.nilai
            ]
        }
        def cbDetailLogam = CashBalanceDetail.createCriteria().list {
            eq("staDel","0");
            eq("cashBalance",cb)
            pecahanUang{
                ilike("type","%Logam%")
                order("nilai","desc")
            }
        }
        def rows2 = []
        cbDetailLogam.each {
            rows2 << [
                    nilai : it?.pecahanUang?.nilai,
                    jum   : it?.quantity,
                    total : it?.quantity * it?.pecahanUang?.nilai
            ]
        }
        def ret = [dataKertas : rows, dataLogam : rows2]
        render ret as JSON
    }

    def save() {

        def dateSave = new Date().parse("dd/MM/yyyy", params.requiredDate as String)
        def cekInvoice = InvoiceT701.createCriteria().list {
            eq("companyDealer",session?.userCompanyDealer);
            ge("t701TglJamInvoice",dateSave?.clearTime());
            lt("t701TglJamInvoice",dateSave?.clearTime()+1);
            eq("t701JenisInv","T",[ignoreCase:true])
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            eq("t701StaSettlement","0")
            eq("t701StaDel","0")
        }

        if (cekInvoice?.size()>0) {
            def result = [:]
            result.result = "Ada data invoice tunai hari ini yang belum dibayar, Klik tombol View Detail untuk melihat data"
            result.cekInvoice = true
            render result as JSON
        }else{
            def result = cashBalanceService.save(params)
            render result as JSON
        }
      /*  if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["CashBalance", result.cashBalanceInstance.id])
            redirect(action: 'show', id: result.cashBalanceInstance.id)
            return
        }

        render(view: 'create', model: [cashBalanceInstance: result.cashBalanceInstance])*/
    }

    def show(Long id) {
        def result = cashBalanceService.show(params)

        if (!result.error)
            return [cashBalanceInstance: result.cashBalanceInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = cashBalanceService.show(params)
        def pecahanUang = PecahanUang.createCriteria()
        def pecahanUangKertas = pecahanUang.list {
            eq("type", "Kertas")
        }
        def logam = PecahanUang.createCriteria()
        def pecahanUangLogam = logam.list {
            eq("type", "Logam")
        }
        Date today = result.cashBalanceInstance.requiredDate

        def dataCari = cariData(today)

        def konversi = new Konversi()
        def detailCashBalance = CashBalanceDetail.createCriteria().list {
            eq("staDel","0")
            eq("cashBalance",result.cashBalanceInstance);
        }
        if (!result.error)
            return [cashBalanceInstance: result.cashBalanceInstance, saldoAkhirKas: dataCari?.saldoAkhirKas, totalMutasiNonOpHariIni: dataCari?.totalMutasiNonOpHariIni, totalMutasiOpHariIni: dataCari?.totalMutasiOpHariIni, pecahanUangKertas: pecahanUangKertas, pecahanUangLogam: pecahanUangLogam, saldoAkhirKasKemarin: dataCari?.saldoAkhirKasKemarin,konversi : konversi,datadetail : detailCashBalance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.lastUpdProcess = "UPDATE"
        def result = cashBalanceService.update(params)

        render result as JSON
    }

    def delete() {
        def result = cashBalanceService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["CashBalance", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(CashBalance, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def cariData(Date tgglCari){
        Date today = tgglCari;
        Date yesterday = today - 1;
        def tgglAkhirTrx = JournalDetail.createCriteria().get {
            journal{
                eq("companyDealer",session?.userCompanyDealer);
                lt("journalDate", today);
                eq("isApproval", "1");
                order("journalDate","desc");
                eq("staDel","0");
            }
            accountNumber{
                ilike("accountNumber","1.10.10.01.001%")
            }
            eq("staDel","0");
            maxResults(1);
        }
        Date lastTrx = tgglAkhirTrx?.journal ? tgglAkhirTrx?.journal?.journalDate : yesterday
        String lastYM = new MonthlyBalanceController().lastYearMonth(lastTrx?.format("MM")?.toInteger(),lastTrx?.format("yyyy")?.toInteger());
        def mb = MonthlyBalance.createCriteria().get{
            eq("companyDealer",session?.userCompanyDealer);
            accountNumber{
                ilike("accountNumber","1.10.10.01.001%");
            }
            eq("yearMonth",lastYM);
            maxResults(1);
        }

        def saldoAkhirKasKemarin = 0.00
        if(mb){
            saldoAkhirKasKemarin = mb?.endingBalance
        }else{
            def lastMB = MonthlyBalance.createCriteria().get {
                eq("companyDealer",session?.userCompanyDealer);
                accountNumber{
                    ilike("accountNumber","1.10.10.01.001%");
                }
                order("dateCreated","desc");
                maxResults(1);
            }
            Date tgglAwalTrx = new Date()
            if(lastMB){
                String nlastYM = plusYearMonth(lastMB?.yearMonth?.substring(0,2)?.toInteger(),lastMB?.yearMonth?.substring(2,lastMB?.yearMonth?.length())?.toInteger())
                String tgglAkhir = new MonthlyBalanceController().tanggalAkhir(lastYM?.substring(0,2)?.toInteger(),lastYM?.substring(2,lastMB?.yearMonth?.length())?.toInteger())+"/"+lastYM?.substring(0,2)+"/"+lastYM?.substring(2,lastMB?.yearMonth?.length())
                tgglAwalTrx = new Date().parse("dd/MM/yyyy","01/"+nlastYM?.substring(0,2)+"/"+nlastYM?.substring(2,lastMB?.yearMonth?.length()))
                saldoAkhirKasKemarin = lastMB?.endingBalance
                def lastDayTrx = JournalDetail.createCriteria().list {
                    journal{
                        eq("companyDealer",session.userCompanyDealer)
                        ge("journalDate", tgglAwalTrx.clearTime());
                        lt("journalDate", new Date().parse("dd/MM/yyyy",tgglAkhir)+1);
                        eq("isApproval", "1");
                        eq("staDel","0");
                    }
                    accountNumber{
                        ilike("accountNumber","1.10.10.01.001%");
                    }
                    eq("staDel","0");
                }
                lastDayTrx?.each {
                    saldoAkhirKasKemarin = saldoAkhirKasKemarin + (it?.debitAmount - it?.creditAmount)
                }
            }

        }

        def lastDayTrx = JournalDetail.createCriteria().list {
            journal{
                eq("companyDealer",session.userCompanyDealer)
                ge("journalDate", new Date().parse("dd/MM/yyyy","01/"+lastTrx?.format("MM/yyyy")));
                lt("journalDate", (lastTrx+1).clearTime());
                eq("isApproval", "1");
                eq("staDel","0");
            }
            accountNumber{
                ilike("accountNumber","1.10.10.01.001%");
            }
            eq("staDel","0");
        }
        lastDayTrx?.each {
            saldoAkhirKasKemarin = saldoAkhirKasKemarin + (it?.debitAmount - it?.creditAmount)
        }

        BigDecimal saldoAkhirKas = 0.00
        BigDecimal totalMutasiNonOpHariIni = 0.00
        BigDecimal totalMutasiOpHariIni = 0.00
        def lastTransaction = JournalDetail.createCriteria().list {
            journal{
                eq("companyDealer",session.userCompanyDealer)
                ge("journalDate", today)
                lt("journalDate", today+1)
                eq("isApproval", "1")
                eq("staDel","0");
            }
            accountNumber{
                ilike("accountNumber","1.10.10.01.001%")
            }
            eq("staDel","0");
        }

        if (lastTransaction) {
            def sum = 0.00;
            def sumOps = 0.00;
            lastTransaction.each {
                if(it?.journal?.isOperasional && it?.journal?.isOperasional=="1"){
                    sumOps = sumOps + it?.debitAmount - it?.creditAmount;
                }else{
                    sum = sum + it.debitAmount - it.creditAmount
                }
            }
            totalMutasiNonOpHariIni = sum;
            totalMutasiOpHariIni = sumOps;
        }

        saldoAkhirKas = saldoAkhirKasKemarin + totalMutasiNonOpHariIni + totalMutasiOpHariIni

        def result = [:]
        def konversi = new Konversi()
        result.saldoAkhirKasKemarin = saldoAkhirKasKemarin
        result.totalMutasiNonOpHariIni = totalMutasiNonOpHariIni
        result.totalMutasiOpHariIni = totalMutasiOpHariIni
        result.saldoAkhirKas = saldoAkhirKas

        return result
    }

    String plusYearMonth(int bulan,int tahun){
        String lastMonth = "",lastYear = ""
        if(bulan==12){
            lastMonth = "1"
            lastYear = (tahun+1).toString()
        }else {
            lastMonth = (bulan+1).toString()
            lastYear = tahun.toString()
        }
        lastMonth = lastMonth.size() > 1 ? lastMonth : "0"+lastMonth
        return lastMonth+lastYear
    }


}