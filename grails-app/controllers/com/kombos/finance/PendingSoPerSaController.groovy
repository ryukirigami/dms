package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Konversi
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class PendingSoPerSaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]


    def index() {}
    def pendingMasuk(){
        List<JasperReportDef> reportDefList = []
        String bulanParam =  params.bulan +""+ params.tahun
        def reportData = calculatePendingMasuk(params)

        def reportDef = new JasperReportDef(name:'f_lapPendingSoPerSa.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("PENDING_MASUK_SO_PER_SA("+session?.userCompanyDealerId+")_"+bulanParam+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def pendingKeluar(){
        List<JasperReportDef> reportDefList = []
        String bulanParam =  params.bulan +""+ params.tahun
        def reportData = calculatePendingKeluar(params)

        def reportDef = new JasperReportDef(name:'f_lapPendingSoPerSa.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("PENDING_KELUAR_SO_PER_SA("+session?.userCompanyDealerId+")_"+bulanParam+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }
    def calculatePendingMasuk(def params){
        def companyDealer = session?.userCompanyDealer;
        def judul = "DAFTAR PENDING MASUK SO PER SA";
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        String bulanParam =  params.tahun + "-" + params.bulan + "-1"
        Date tgl = new Date().parse('yyyy-MM-d',bulanParam.toString())
        def cariAkhir = new MonthlyBalanceController()
        String tgglAkhir = cariAkhir.tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-"+tgglAkhir
        Date tgl2 = new Date().parse('yyyy-MM-d',bulanParam2.toString())
        def periode = tgl2.format("MMMM yyyy").toUpperCase()
        def c = Reception.createCriteria()
        def results = c.list{
            eq("companyDealer",companyDealer)
            eq("staSave","0")
            eq("staDel", "0")
            eq("t401StaReceptionEstimasiSalesQuotation", "reception")
            isNull("t401StaInvoice")
            lt("t401TanggalWO",tgl2 + 1)
            order("t401NoWO","desc")
            order("t401NamaSA","asc")
        }

        int count = 0
        results.each { p ->
            def job = ""
            def inv = InvoiceT701.createCriteria().list {
                eq("t701StaDel",'0')
                isNull('t701NoInv_Reff')
                isNull('t701StaApprovedReversal')
                lt("t701TglJamInvoice",tgl2 + 1)
                reception{
                    eq("id",p.id)
                }
            }
            if(inv.size()==0){

                def data = [:]
                count = count + 1
                data.put("companyDealer",companyDealer)
                data.put("periode",periode)
                data.put("judul",judul)

                data.put("noUrut",count)
                data.put("noSo",p?.t401NoWO)
                data.put("nopol",p.historyCustomerVehicle?.fullNoPol)
                data.put("tglSo",p?.t401TanggalWO? p.t401TanggalWO.format("dd/MM/yyyy") : "")
                data.put("namaPelanggan",p?.historyCustomer?.getFullNama())
                int cJob = 1
                def recept = p
                def jobss = JobRCP.createCriteria().list {
                    eq("staDel","0")
                    eq("reception",recept)
                    or{
                        ilike("t402StaTambahKurang","0")
                        and{
                            not{
                                ilike("t402StaTambahKurang","%1%")
                                ilike("t402StaApproveTambahKurang","%1%")
                            }
                        }
                        and {
                            isNull("t402StaTambahKurang")
                            isNull("t402StaApproveTambahKurang")
                        }
                    }
                }
                job = "\n"
                jobss.each {
                    job +=  (cJob++) + ". " + it.operation.m053NamaOperation + " \n"
                }
                data.put("namasa",p?.t401NamaSA.toString().toUpperCase())
                data.put("pekerjaan",job)
                reportData.add(data)
            }
        }
        if(results.size()==0 || count==0){
            def data = [:]
            count = count + 1
            data.put("companyDealer",companyDealer)
            data.put("periode",periode)
            data.put("judul",judul)

            data.put("noUrut","1")
            reportData.add(data)
        }
        return reportData

    }

    def calculatePendingKeluar(def params){
        def companyDealer = session?.userCompanyDealer;
        def judul = "DAFTAR PENDING KELUAR SO PER SA";
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        String bulanParam =  params.tahun + "-" + params.bulan + "-1"
        Date tgl = new Date().parse('yyyy-MM-d',bulanParam.toString())
        def cariAkhir = new MonthlyBalanceController()
        String tgglAkhir = cariAkhir.tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-"+tgglAkhir
        Date tgl2 = new Date().parse('yyyy-MM-d',bulanParam2.toString())
        def periode = tgl2.format("MMMM yyyy").toUpperCase()
        def c = Reception.createCriteria()
        def results = c.list{
            eq("companyDealer",companyDealer)
            eq("staSave","0")
            eq("staDel", "0")
            isNull("t401StaInvoice")
            lt("t401TanggalWO",tgl2 + 1)
            order("t401NoWO","desc")
            order("t401NamaSA","asc")
        }

        int count = 0
        results.each { p ->
            def job = ""
            def pelangganDitagih = "\n"
            def inv = InvoiceT701.createCriteria().list {
                ge("t701TglJamInvoice",tgl)
                lt("t701TglJamInvoice",tgl2 + 1)
                eq("t701StaDel",'0')
                isNull('t701NoInv_Reff')
                isNull('t701StaApprovedReversal')
                reception{
                    eq("id",p.id)
                }
            }
            if(inv.size()>0){
                def data = [:]
                count = count + 1
                data.put("companyDealer",companyDealer)
                data.put("periode",periode)
                data.put("judul",judul)

                data.put("noUrut",count)
                data.put("noSo",p.t401NoWO)
                data.put("nopol",p.historyCustomerVehicle?.fullNoPol)
                data.put("tglSo",p.t401TanggalWO? p.t401TanggalWO.format("dd/MM/yyyy") : "")
                data.put("namaPelanggan",p?.historyCustomer?.getFullNama())
                int cTagh = 1
                inv.each {
                    pelangganDitagih += (cTagh++) + ". " + it.t701Customer + " \n"
                }
                data.put("namaPelangganDiTagih",pelangganDitagih)
                data.put("noInvoice",inv.get(0).t701NoInv)
                data.put("ket",inv.get(0).t701xKet)
                int cJob = 1
                def recept = p
                def jobss = JobRCP.createCriteria().list {
                    eq("staDel","0")
                    eq("reception",recept)
                    or{
                        ilike("t402StaTambahKurang","0")
                        and{
                            not{
                                ilike("t402StaTambahKurang","%1%")
                                ilike("t402StaApproveTambahKurang","%1%")
                            }
                        }
                        and {
                            isNull("t402StaTambahKurang")
                            isNull("t402StaApproveTambahKurang")
                        }
                    }
                }
                job = "\n"
                jobss.each {
                    job +=  (cJob++) + ". " + it.operation.m053NamaOperation + " \n"
                }
                data.put("namasa",p?.t401NamaSA.toString().toUpperCase())
                data.put("pekerjaan",job)
                reportData.add(data)
            }
        }
        if(results.size()==0 || count==0){
            def data = [:]
            count = count + 1
            data.put("companyDealer",companyDealer)
            data.put("periode",periode)
            data.put("judul",judul)

            data.put("noUrut","1")
            reportData.add(data)
        }
        return reportData

    }
}
