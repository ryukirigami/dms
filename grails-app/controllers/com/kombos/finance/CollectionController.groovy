package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class CollectionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def collectionService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def forUpdate(Integer max) {

    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render collectionService.datatablesList(params) as JSON
    }

    def save() {
        params?.companyDealer = session?.userCompanyDealer
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = collectionService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Collection", result.collectionInstance.id])
            redirect(action: 'show', id: result.collectionInstance.id)
            return
        }

        render(view: 'create', model: [collectionInstance: result.collectionInstance])
    }



    def update() {
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            println "LOOPING "+it
            def nColl = Collection.get(it?.toLong())
            nColl.paidAmount = nColl.amount
            nColl.paymentStatus = "LUNAS"
//            nColl.dateCreated = new Date()
            nColl.lastUpdated = datatablesUtilService?.syncTime()
            nColl?.lastUpdProcess = "UPDATE"
            nColl.save(flush: true)
            nColl.errors.each {println it}
        }

        render "OK"
    }

    def delete() {
        def result = collectionService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Collection", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Collection, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}