package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class BankAccountNumberController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def bankAccountNumberService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        //params.companyDealer = session.userCompanyDealer
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        render bankAccountNumberService.datatablesList(params) as JSON
    }

    def create() {
        def result = bankAccountNumberService.create(params)

        if (!result.error)
            return [bankAccountNumberInstance: result.bankAccountNumberInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.accountNumber = AccountNumber.findById(params.idAkun)
        params.companyDealer = session.userCompanyDealer
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = bankAccountNumberService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["BankAccountNumber", result.bankAccountNumberInstance.id])
            redirect(action: 'show', id: result.bankAccountNumberInstance.id)
            return
        }

        render(view: 'create', model: [bankAccountNumberInstance: result.bankAccountNumberInstance])
    }

    def show(Long id) {
        def result = bankAccountNumberService.show(params)

        if (!result.error)
            return [bankAccountNumberInstance: result.bankAccountNumberInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = bankAccountNumberService.show(params)

        if (!result.error)
            return [bankAccountNumberInstance: result.bankAccountNumberInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.lastUpdProcess = "UPDATE"
        params.accountNumber = AccountNumber.findById(params.idAkun)
        def result = bankAccountNumberService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["BankAccountNumber", params.id])
            redirect(action: 'show', id: params.id)
            return
        }
        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [bankAccountNumberInstance: result.bankAccountNumberInstance.attach()])
    }

    def delete() {
        def result = bankAccountNumberService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["BankAccountNumber", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(BankAccountNumber, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
    def listAccount() {
        def res = [:]
        def opts = []
        def z = AccountNumber.createCriteria()
        def results=z.list {
            or{
                ilike("accountName","%" + params.query + "%")
                ilike("accountNumber","%" + params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.accountNumber.trim() +" || "+ it.accountName
        }
        res."options" = opts
        render res as JSON

    }
    def detailAccount(){
        def result = [:]
        def data= params.noAkun as String
        if(data.contains("||")){
            def dataNoAkuns = data.split(" ")
            def accountNumber = AccountNumber.createCriteria().list {
                eq("staDel","0")
                ilike("accountNumber","%"+dataNoAkuns[0].trim() + "%")
            }
            if(accountNumber.size()>0){
                def accountData = accountNumber.last()
                result."hasil" = "ada"
                result."id" = accountData?.id
                result."namaAkun" = accountData?.accountName
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }
}