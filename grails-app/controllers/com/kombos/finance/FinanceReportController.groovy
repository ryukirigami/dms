package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.GroupManPower
import com.kombos.administrasi.ManPower
import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.customerprofile.SPK
import com.kombos.customerprofile.SPKAsuransi
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.maintable.FakturPajak
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.PartInv
import com.kombos.parts.Invoice
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import com.kombos.reception.Appointment
import com.kombos.reception.JobInv
import com.kombos.reception.Reception
import com.kombos.utils.MoneyUtil
import oracle.jdbc.driver.DatabaseError
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification

class FinanceReportController {
    def jasperService
    def konversi = new Konversi()
    private MoneyUtil moneyUtil = new MoneyUtil()

    def index() {
        def user = User.get(org.apache.shiro.SecurityUtils.subject.principal.toString())
        def isTeknisi = false;
        if(user){
            user?.roles?.each {
                if(it?.name?.toLowerCase()?.contains("teknisi") || it?.name?.toLowerCase()?.contains("foreman")){
                    isTeknisi = true;
                }
            }
        }
        def blnSkrg = -1
        def skrg = new Date().clearTime()
        if(skrg.format("MM")=="01"){
            blnSkrg = 12
        }else{
            blnSkrg = skrg.format("MM").toInteger()-1
        }
        [bulanCari : blnSkrg,isTeknisi:isTeknisi]
    }

    def previewData(){
        List<JasperReportDef> reportDefList = []
        def file = null
        def reportData = calculateReportData(params)
        def reportData2 = calculateReportDetailData(params)
        def formatFile = ""
        if(params.format=="x"){
            formatFile = ".xls"
        }else{
            formatFile = ".pdf"
        }
        if(params.namaReport=="LHKB") {
            Date awal = new Date().parse("dd-MM-yyyy",params.tanggal)
            def reportDef = new JasperReportDef(name:'LAPORAN HARIAN KAS DAN BANK.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("DATA_LHKB("+session.userCompanyDealerId+")_"+awal.format("ddMMMyyyy")+"_",formatFile)
        }

        if(params.namaReport=="ANALISA_KAS") {
            Date awal = new Date().parse("dd-MM-yyyy",params.tanggal)
            def parameters = [
                    tanggal : awal.format("dd-MM-yyyy")
            ]
            def reportDef = new JasperReportDef(name:'ANALISA UANG KAS.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData,
                    parameters:parameters
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("ANALISA_KAS("+session.userCompanyDealerId+")_"+awal.format("ddMMMyyyy")+"_",formatFile)
        }

        if(params.namaReport=="REGISTRASI_CEK_BG") {
            Date awal = new Date().parse("dd-MM-yyyy",params.tanggal)
            def reportDef = new JasperReportDef(name:'RegisterCekBG.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("RREGISTRASI_Cek_BG_",formatFile)
        }

        if(params.namaReport=="REGISTER_SERVICE_ORDER") {
            def reportDef = new JasperReportDef(name:'REGISTER_SERVICE_ORDER.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("REGISTER_SO_",formatFile)
        }

        if(params.namaReport=="DAFTAR_PENAGIHAN") {
            def reportDef = new JasperReportDef(name:'DAFTAR PENAGIHAN.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("DAFTAR_PENAGIHAN_",formatFile)
        }

        if(params.namaReport=="REGISTRASI_FAKTUR_PAJAK") {
            def reportDef = new JasperReportDef(name:'REGISTER FAKTUR PAJAK.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("REGIASTRASI_FAKTUR_PAJAK_",formatFile)
        }

        if(params.namaReport=="REKONSILIASI_BANK") {
            def reportDef = new JasperReportDef(name:'Rekonsiliasi Bank.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("REKONSILIASI_BANK_",formatFile)
        }

        if(params.namaReport=="PERINCIAN_TRANSAKSI_KAS") {
            def reportDef = new JasperReportDef(name:'Perincian Transaksi Kas.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("PERINCIAN_TRX_KAS_",formatFile)
        }

        if(params.namaReport=="PERINCIAN_TRANSAKSI_BANK") {
            def reportDef = new JasperReportDef(name:'PERINCIAN TRANSAKSI BANK.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("PERINCIAN_TRX_BANK_",formatFile)
        }

        if(params.namaReport.toString().contains("BUKTI_KAS_KELUAR")) {
            def reportDef = new JasperReportDef(name:'BUKTI KAS KELUAR.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("BUKTI_KAS_KELUAR_",formatFile)
        }

        if(params.namaReport.toString().contains("BUKTI_KAS_MASUK")) {
            def reportDef = new JasperReportDef(name:'BUKTI KAS KELUAR.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("BUKTI_KAS_MASUK_",formatFile)
        }

        if(params.namaReport.toString().contains("BUKTI_BANK_KELUAR")) {
            def reportDef = new JasperReportDef(name:'BUKTI KAS KELUAR.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("BUKTI_BANK_KELUAR_",formatFile)
        }

        if(params.namaReport.toString().contains("BUKTI_BANK_MASUK")) {
            def reportDef = new JasperReportDef(name:'BUKTI KAS KELUAR.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("BUKTI_BANK_MASUK_",formatFile)
        }

        if(params.namaReport=="REKAP_PENCAPAIAN_HASIL_MEKANIK") {
            def reportDef = new JasperReportDef(name:'Rekap Pencapaian Mekanik.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            def reportDef2 = new JasperReportDef(name:'Rekap Pencapaian Mekanik2.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData2
            )
            reportDefList.add(reportDef2)
            file = File.createTempFile("REKAP_PENCAPAIAN_HASIL_MEKANIK_",formatFile)
        }

        if(params.namaReport=="PENCAPAIAN_HASIL_MEKANIK") {
            def parameters = [
                    SUBREPORT_DIR : servletContext.getRealPath('/reports') + "/"
            ]
                def reportDef = new JasperReportDef(name:'PencapaianMekanik.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData,
                        parameters: parameters
                )
                reportDefList.add(reportDef)

                def reportDef2 = new JasperReportDef(name:'PencapaianMekanik.jasper',
                        fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                        reportData: reportData2,
                        parameters: parameters
                )
                reportDefList.add(reportDef2)

            file = File.createTempFile("PENCAPAIAN_HASIL_MEKANIK_",formatFile)
        }

        if(params.namaReport=="DAFTAR_PENJELASAN_UMUR_PIUTANG") {
            Date awal = new Date().parse("dd/MM/yyyy","01/"+new Date().format("MM/yyyy"))
            def tanggalAKhir = new Date().clearTime()
            def panggil = new MonthlyBalanceController()
            def thnSkrg = new Date().format("yyyy")
            if(params.bulan=="12"){
                thnSkrg = (thnSkrg.toInteger()-1).toString()
            }
            String tglEnd = panggil.tanggalAkhir(params.bulan.toInteger(),thnSkrg.toInteger())
            def tanggalAKhirCari = new Date().parse("dd/M/yyyy",tglEnd+"/"+params.bulan+"/"+thnSkrg)
            def parameters = [
                    SUBREPORT_DIR : servletContext.getRealPath('/reports') + "/",
                    tglAwalSaldo : (tanggalAKhirCari)?.format("dd-MM-yyyy"),
                    tglPembayaran : awal?.format("dd")+" s.d. "+tanggalAKhir?.format("dd MMM yyyy"),
                    tglAkhirSaldo : tanggalAKhir?.format("dd MMM yyyy")
            ]
            def reportDef = new JasperReportDef(name:'PenjelasanUmurPiutang.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData,
                    parameters: parameters
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("PENJELASAN_UMUR_PIUTANG_",formatFile)
        }

        if(params.namaReport=="DAFTAR_UMUR_PIUTANG") {
            def thnSkrg = new Date().format("yyyy")
            Date awal = new Date().parse("dd/MM/yyyy","01/"+params.bulan+"/"+thnSkrg)
            def panggil = new MonthlyBalanceController()
            String tglEnd = panggil.tanggalAkhir(params.bulan.toInteger(),thnSkrg.toInteger())
            def tanggalAKhirCari = new Date().parse("dd/M/yyyy",tglEnd+"/"+params.bulan+"/"+thnSkrg)
            def parameters = [
                    SUBREPORT_DIR : servletContext.getRealPath('/reports') + "/",
                    tglAwalSaldo : awal?.format("dd-MM-yyyy"),
                    tglAkhirSaldo : tanggalAKhirCari?.format("dd MMM yyyy")
            ]
            def reportDef = new JasperReportDef(name:'UmurPiutang.jasper',
                    fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData,
                    parameters: parameters
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("UMUR_PIUTANG_",formatFile)
        }

        file.deleteOnExit() //Aktifkan dulu
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def calculateReportData(def params){
        Date awal = new Date().clearTime()
        if(params.tanggal){
            awal = new Date().parse("dd-MM-yyyy",params.tanggal)
        }
        def reportData = new ArrayList();
        CompanyDealer companydealer = session.userCompanyDealer
        if(params.companyDealer){
            companydealer = CompanyDealer.get(params.companyDealer.toLong())
        }
        if(params.namaReport=="REGISTER_SERVICE_ORDER"){
            int count = 0
            def receptions = Reception.createCriteria().list {
                eq("staDel","0")
                eq("staSave","0")
                ge("dateCreated",awal)
                lt("dateCreated",awal+1)
                eq("companyDealer",companydealer)
                order("dateCreated","asc")
            }

            if(receptions.size()<1){
                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("Tanggal",awal?.format("dd MMMM yyyy"));
                data.put("No", "-")
                data.put("NoSeviceOrder", "-")
                data.put("NamaPelanggan", "-")
                data.put("JenisPekerjaan", "-")
                data.put("NoPolisi", "-")
                data.put("Status", "-")
                data.put("NoInvoice", "-")
                reportData.add(data)
            }

            receptions.each {
                def recept = it
                def invoices = InvoiceT701.createCriteria().list {
                    eq("t701StaDel","0")
                    or{
                        isNull("t701StaApprovedReversal")
                        eq("t701StaApprovedReversal","1")
                    }
                    gt("t701TotalBayarRp",0.toDouble())
                    eq("reception",recept)
                    isNull("t701NoInv_Reff")
                    order("t701NoInv")
                }
                if(invoices.size()>0){
                    invoices.each {
                        def data = [:]
                        count++
                        data.put("Cabang",it.reception.companyDealer)
                        data.put("Tanggal",it.reception.dateCreated.format("dd MMMM yyyy"))
                        data.put("No", count)
                        data.put("NoSeviceOrder", it.reception.t401NoWO)
                        data.put("NamaPelanggan", it?.t701Customer)
                        data.put("JenisPekerjaan", it.reception.customerIn.tujuanKedatangan.m400Tujuan)
                        data.put("NoPolisi", it.reception.historyCustomerVehicle.fullNoPol)
                        data.put("Status", "")
                        data.put("NoInvoice", it.t701NoInv)
                        reportData.add(data)
                    }
                }else{
                    def data = [:]
                    count++
                    data.put("Cabang",it.companyDealer.m011NamaWorkshop)
                    data.put("Tanggal",it.dateCreated.format("dd MMMM yyyy"))
                    data.put("No", count)
                    data.put("NoSeviceOrder", it.t401NoWO)
                    data.put("NamaPelanggan", it.historyCustomer?.fullNama)
                    data.put("JenisPekerjaan", it?.customerIn?.tujuanKedatangan?.m400Tujuan)
                    data.put("NoPolisi", it?.historyCustomerVehicle?.fullNoPol)
                    data.put("Status", "")
                    data.put("NoInvoice", "")
                    reportData.add(data)
                }
            }
        }
        if(params.namaReport=="REGISTRASI_FAKTUR_PAJAK"){
            int count = 0
            def fakturs = FakturPajak.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companydealer)
                ge("dateCreated",awal)
                lt("dateCreated",awal+1)
            }

            if(fakturs.size()<1){
                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("Tanggal",awal?.format("dd MMMM yyyy"));
                data.put("No", "-")
                data.put("NamaPelangganYangDiTagih", "-")
                data.put("NoInvoice", "-")
                data.put("TanggalInvoice", "-")
                data.put("DPP", "-")
                data.put("PPn", "-")
                data.put("NoFakturPajak", "-")
                reportData.add(data)
            }

            fakturs.each {
                count++
                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("Tanggal",awal?.format("dd MMMM yyyy"));
                data.put("No", count)
                data.put("NamaPelangganYangDiTagih", it.invoiceT701?.t701Customer)
                data.put("NoInvoice", it.invoiceT701.t701NoInv)
                data.put("TanggalInvoice", it.invoiceT701.t701TglJamInvoice.format("dd/MM/yyyy"))
                data.put("DPP", konversi.toRupiah(it?.invoiceT701?.t701SubTotalRp))
                data.put("PPn", konversi.toRupiah(it?.invoiceT701?.t701PPnRp))
                data.put("NoFakturPajak", it?.t711NoFakturPajak)
                reportData.add(data)
            }
        }
        if(params.namaReport=="DAFTAR_PENAGIHAN"){
            int count = 0

            def piutangs = Collection.createCriteria().list {
                eq("staDel","0")
                eq("paymentStatus","BELUM",[ignoreCase:true])
                lt("dueDate",awal+1)
                invoiceT071{
                    or{
                        not{
                            ilike("t701Customer","%HASJRAT%");
                            ilike("t701Customer","%KOMBOS%");
                        }
                    }
                    reception{
                        eq("companyDealer",companydealer)
                    }
                }
                order("dueDate")
            }

            if(piutangs.size()<1){
                def data = [:]
                data.put("cabang",companydealer?.m011NamaWorkshop);
                data.put("tanggal",awal?.format("dd MMMM yyyy"));
                data.put("No", "-")
                data.put("NoSO", "-")
                data.put("NoInvoice", "-")
                data.put("NamaPelanggan", "-")
                data.put("JumlahInvoice", "-")
                data.put("JumlahUangMukaPanjar", "-")
                data.put("JumlahYangSudahDibayar", "-")
                data.put("JumlahYangDitagih", "-")
                data.put("SA", "-")
                data.put("ParafSA", "-")
                data.put("DetailPekerjaan", "-")
                reportData.add(data)
            }

            piutangs.each {
                def data = [:]
                count++;
                data.put("cabang",companydealer?.m011NamaWorkshop);
                data.put("tanggal",awal?.format("dd MMMM yyyy"));
                data.put("No", count)
                data.put("NoSO", it.invoiceT071.reception.t401NoWO)
                data.put("NoInvoice", it.invoiceT071.t701NoInv)
                data.put("NamaPelanggan", it.invoiceT071.t701Customer)
                data.put("JumlahInvoice", konversi?.toRupiah(it?.amount))
                data.put("JumlahUangMukaPanjar", konversi?.toRupiah(it?.paidAmount))
                data.put("JumlahYangSudahDibayar", konversi?.toRupiah(it?.paidAmount))
                data.put("JumlahYangDitagih", konversi?.toRupiah(it?.amount-it?.paidAmount))
                data.put("SA", it.invoiceT071.reception.t401NamaSA)
                data.put("ParafSA", "")
                data.put("DetailPekerjaan", "-")
                reportData.add(data)
            }
        }
        if(params.namaReport=="REKONSILIASI_BANK"){
            int count = 0
            def mb = new MonthlyBalanceController()
            def akhir = mb.tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
            Date tgglAwal = new Date().parse("dd/MM/yyyy","01/"+params.bulan+"/"+params.tahun)
            Date tgglAkhir = new Date().parse("dd/MM/yyyy HH:mm",akhir+"/"+params.bulan+"/"+params.tahun+" 24:00")

            def results = BankReconciliation.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer",companydealer)
                ge("reconciliationDate",tgglAwal)
                lt("reconciliationDate",tgglAkhir)
                order("reconciliationDate")
            }
            def arrItem1 = []
            def arrItem2 = []
            if(results.size()<1){
                arrItem1 << [
                        no1 : "-",
                        transaksi1 : "-",
                        debet1 : "-",
                        kredit1 : "-"
                ]
                arrItem2 << [
                        no2 : "-",
                        transaksi2 : "-",
                        debet2 : "-",
                        kredit2 : "-"
                ]
                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("Pertanggal","1 - "+akhir+" "+tgglAwal.format("MMMM")+" "+tgglAwal.format("yyyy"));
                data.put("NamaBank", "-")
                data.put("SaldoBank", "-")
                data.put("SaldoAkhir1", "-")
                data.put("SaldoPerusahaan", "-")
                data.put("TotalTransaksi1", "-")
                data.put("TotalTransaksi2", "-")
                data.put("SaldoAkhir2", "-")
                data.put("Selisih", "-")
                data.put("kota", ".....")
                data.put("TanggalTandaTangan", ".....")
                data.put("items1", arrItem1)
                data.put("items2", arrItem2)
                reportData.add(data)
            }

            results.each {
                def totDP = 0
                def totDB = 0
                def totKP = 0
                def totKB = 0
                def recBank = it
                def trxB = BankReconciliationDetail.createCriteria().list {
                    eq("staDel","0")
                    eq("reconTransactionType","B",[ignoreCase: true])
                    bankRecon{
                        eq("id",recBank.id)
                    }
                }
                def trxP = BankReconciliationDetail.createCriteria().list {
                    eq("staDel","0")
                    eq("reconTransactionType","P",[ignoreCase: true])
                    bankRecon{
                        eq("id",recBank.id)
                    }
                }
                if(trxB.size()<1){
                    arrItem1 << [
                            no1 : "-",
                            transaksi1 : "-",
                            debet1 : "-",
                            kredit1 : "-"
                    ]
                }
                trxB.each {
                    count++
                    totDB+=it.debitAmount
                    totKB+=it.creditAmount
                    arrItem1 << [
                            no1 : count,
                            transaksi1 : it.description,
                            debet1 : konversi.toRupiah(it.debitAmount),
                            kredit1 : konversi.toRupiah(it.creditAmount)
                    ]
                }
                count = 0
                if(trxP.size()<1){
                    arrItem2 << [
                            no2 : "-",
                            transaksi2 : "-",
                            debet2 : "-",
                            kredit2 : "-"
                    ]
                }

                trxP.each {
                    totDP+=it.debitAmount
                    totKP+=it.creditAmount
                    count++
                    arrItem2 << [
                            no2 : count,
                            transaksi2 : it.description,
                            debet2 : konversi.toRupiah(it.debitAmount),
                            kredit2 : konversi.toRupiah(it.creditAmount)
                    ]
                }

                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("Pertanggal","1 - "+akhir+" "+tgglAwal.format("MMMM")+" "+tgglAwal.format("yyyy"));
                data.put("NamaBank", recBank?.bank?.m702Cabang ? recBank.bank.m702NamaBank+" - "+recBank?.bank?.m702Cabang : recBank.bank.m702NamaBank)
                data.put("SaldoBank", konversi.toRupiah(it.saldoAwalBank))
                data.put("SaldoAkhir1", konversi.toRupiah(it.saldoAkhirBank))
                data.put("SaldoPerusahaan", konversi.toRupiah(it.saldoAwalPerusahaan))
                data.put("TotalTransaksi1", konversi.toRupiah((totDB-totKB)))
                data.put("TotalTransaksi2", konversi.toRupiah((totDP-totKP)))
                data.put("SaldoAkhir2", konversi.toRupiah(it.saldoAkhirPerusahaan))
                data.put("Selisih", konversi.toRupiah(it.selisih))
                data.put("kota", it?.companyDealer?.kabKota ? it?.companyDealer?.kabKota?.m002NamaKabKota+" , " : "....  , ")
                data.put("TanggalTandaTangan", new Date().format("dd MMMM yyyy"))
                data.put("items1", arrItem1)
                data.put("items2", arrItem2)
                reportData.add(data)
            }
        }
        if(params.namaReport=="PERINCIAN_TRANSAKSI_KAS"){
            int count = 0

            def results = Journal.createCriteria().list {
                eq("isApproval", "1")
                eq("companyDealer",companydealer)
                eq("staDel","0")
                ge("journalDate",awal)
                lt("journalDate",awal+1)
                journalDetail {
                    eq("staDel","0")
                    accountNumber{
                        eq("accountNumber","1.10.10.01.001",[ignoreCase: true]);
                    }
                }
                order("dateCreated")
            }

            if(results.size()<1){
                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("Tanggal",awal.format("dd MMMM yyyy"));
                data.put("No", "-")
                data.put("NamaAkunLawanTransaksi1", "-")
                data.put("NoRekening", "-")
                data.put("Keterangan", "-")
                data.put("Jumlah1", "-")
                data.put("NamaAkunLawanTransaksi2", "-")
                data.put("NoRekening2", "-")
                data.put("Keterangan2", "-")
                data.put("Jumlah2", "-")
                data.put("Total1", "-")
                data.put("Total2", "-")
                reportData.add(data)
            }

            def totD = 0
            def totK = 0
            results.each {
                def jurnal = it
                def details = JournalDetail.createCriteria().list {
                    eq("staDel","0")
                    journal{
                        eq("id",jurnal.id)
                        eq("isApproval", "1")
                    }
                    order("creditAmount")
                    accountNumber{
                        order("accountNumber")
                    }
                }
                details.each {
                    count++
                    def staKB = it.debitAmount!=0 ? "D" : "K"
                    def data = [:]
                    data.put("Cabang",companydealer?.m011NamaWorkshop);
                    data.put("Tanggal",awal.format("dd MMMM yyyy"));
                    data.put("No", count)
                    if(staKB=="D"){
                        totD+=it.debitAmount
                        data.put("NamaAkunLawanTransaksi1", it?.accountNumber?.accountName)
                        data.put("NoRekening", it?.accountNumber?.accountNumber)
                        data.put("Keterangan", jurnal?.description)
                        data.put("Jumlah1", konversi?.toRupiah(it.debitAmount))
                        data.put("NamaAkunLawanTransaksi2", "-")
                        data.put("NoRekening2", "-")
                        data.put("Keterangan2", "-")
                        data.put("Jumlah2", "-")
                    }else {
                        totK+=it.creditAmount
                        data.put("NamaAkunLawanTransaksi1", "-")
                        data.put("NoRekening", "-")
                        data.put("Keterangan", "-")
                        data.put("Jumlah1", "-")
                        data.put("NamaAkunLawanTransaksi2", it?.accountNumber?.accountName)
                        data.put("NoRekening2", it?.accountNumber?.accountNumber)
                        data.put("Keterangan2", jurnal?.description)
                        data.put("Jumlah2", konversi?.toRupiah(it?.creditAmount))
                    }
                    data.put("Total1", konversi?.toRupiah(totD))
                    data.put("Total2", konversi?.toRupiah(totK))
                    reportData.add(data)
                }
            }
        }
        if(params.namaReport=="PERINCIAN_TRANSAKSI_BANK"){
            int count = 0

            def results = Journal.createCriteria().list {
                eq("isApproval", "1")
                eq("companyDealer",companydealer)
                eq("staDel","0")
                ge("journalDate",awal)
                lt("journalDate",awal+1)
                journalDetail {
                    eq("staDel","0")
                    accountNumber{
                        ilike("accountNumber","1.10.20.%")
                    }
                }
                order("dateCreated")
            }

            if(results.size()<1){
                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("Tanggal",awal.format("dd MMMM yyyy"));
                data.put("No", "-")
                data.put("NamaAkunLawanTransaksi1", "-")
                data.put("NoRekening", "-")
                data.put("Keterangan", "-")
                data.put("Jumlah1", "-")
                data.put("NamaAkunLawanTransaksi2", "-")
                data.put("NoRekening2", "-")
                data.put("Keterangan2", "-")
                data.put("Jumlah2", "-")
                data.put("Total1", "-")
                data.put("Total2", "-")
                reportData.add(data)
            }

            def totD = 0
            def totK = 0
            results.each {
                def jurnal = it
                def details = JournalDetail.createCriteria().list {
                    eq("staDel","0")
                    journal{
                        eq("id",jurnal.id)
                        eq("isApproval", "1")
                        eq("staDel","0");
                    }
                    order("creditAmount")
                    accountNumber{
                        order("accountNumber")
                    }
                }
                details.each {
                    count++
                    def staKB = it.debitAmount!=0 ? "D" : "K"
                    def data = [:]
                    data.put("Cabang",companydealer?.m011NamaWorkshop);
                    data.put("Tanggal",awal.format("dd MMMM yyyy"));
                    data.put("No", count)
                    if(staKB=="D"){
                        totD+=it.debitAmount
                        data.put("NamaAkunLawanTransaksi1", it?.accountNumber?.accountName)
                        data.put("NoRekening", it?.accountNumber?.accountNumber)
                        data.put("Keterangan", jurnal?.description)
                        data.put("Jumlah1", konversi?.toRupiah(it?.debitAmount))
                        data.put("NamaAkunLawanTransaksi2", "-")
                        data.put("NoRekening2", "-")
                        data.put("Keterangan2", "-")
                        data.put("Jumlah2", "-")
                    }else {
                        totK+=it.creditAmount
                        data.put("NamaAkunLawanTransaksi1", "-")
                        data.put("NoRekening", "-")
                        data.put("Keterangan", "-")
                        data.put("Jumlah1", "-")
                        data.put("NamaAkunLawanTransaksi2", it?.accountNumber?.accountName)
                        data.put("NoRekening2", it?.accountNumber?.accountNumber)
                        data.put("Keterangan2", jurnal?.description)
                        data.put("Jumlah2", konversi?.toRupiah(it?.creditAmount))
                    }
                    data.put("Total1", konversi?.toRupiah(totD))
                    data.put("Total2", konversi?.toRupiah(totK))
                    reportData.add(data)
                }
            }
        }
        if(params.namaReport=="LHKB"){
            def kasir = "", adku = "", kabeng = "", kacab = ""
            try {
                kasir = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KASIR%"),'0',session.userCompanyDealer)?.nama
                adku = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KABAG ADKEU%"),'0',session.userCompanyDealer)?.nama
                kabeng = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KEPALA BENGKEL%"),'0',session.userCompanyDealer)?.nama
                kacab = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KEPALA CABANG%"),'0',session.userCompanyDealer)?.nama
            }catch (Exception e){

            }
            int count = 0
            def mb = new MonthlyBalanceController()
            String lastPeriod = mb.lastYearMonth(awal.format("MM").toInteger(),awal.format("yyyy").toInteger())
            def results = []
            def tglTrx = awal
            def staNoData = false
            while(results.size()==0){
                results = JournalDetail.createCriteria().list {
                    eq("staDel","0")
                    accountNumber{
                        or{
                            eq("accountNumber","1.10.10.01.001",[ignoreCase: true]);
                            ilike("accountNumber","1.10.20.%")
                        }
                    }
                    journal {
                        ge("journalDate",tglTrx)
                        lt("journalDate",tglTrx+1)
                        eq("staDel","0")
                        eq("companyDealer",companydealer)
                        eq("isApproval", "1")
                    }
                    resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                    projections {
                        accountNumber{
                            groupProperty("accountName", "account")
                            groupProperty("accountNumber", "number")
                        }
                    }
                }
                if(results.size()==0){
                    tglTrx = tglTrx - 1
                    staNoData = true
                }
            }
            def items1 = []
            def items2 = []
            if(results.size()<1){
                items1 << [
                    pecahan1 : "-",
                    fisik1 : "-",
                    jum1 : "-"
                ]
                items2 << [
                    pecahan2 : "-",
                    fisik2 : "-",
                    jum2 : "-"
                ]
                def data = [:]

                data.put("CabangKombos",companydealer?.m011NamaWorkshop);
                data.put("Tanggal",new Konversi().getHari(awal.getDay())+", "+awal.format("dd MMMM yyyy"));
                data.put("keterangan", "-")
                data.put("saldoawal", "-")
                data.put("penerimaan", "-")
                data.put("pengeluaran", "-")
                data.put("setorbankKas", "-")
                data.put("saldoakhir", "-")
                data.put("totBsaldoawal", "-")
                data.put("totBpenerimaan", "-")
                data.put("totBpengeluaran", "-")
                data.put("totBankSetor", "-")
                data.put("totBsaldoakhir", "-")
                data.put("totKsaldoawal", "-")
                data.put("totKpenerimaan", "-")
                data.put("totKpengeluaran", "-")
                data.put("totKasBankSetor", "-")
                data.put("totKsaldoakhir", "-")
                data.put("jumUK", "-")
                data.put("jumUL", "-")
                data.put("totOpname", "-")
                data.put("selisih", "-")
                data.put("KASIR", kasir)
                data.put("ADKU", adku)
                data.put("KACAB", kacab)
                data.put("KABENG", kabeng)
                data.put("items1", items1)
                data.put("items2", items2)
                reportData.add(data)
            }

            for(int a=0;a<results.size();a++){
                if(a==0 && results[a].number.toString().equalsIgnoreCase("1.10.10.01.001")){
                    break
                }else{
                    if(results[a].number.toString().equalsIgnoreCase("1.10.10.01.001")){
                        def temp = results[0]
                        results[0] = results[a]
                        results[a] = temp
                        break
                    }
                }
            }

            def totKawal = 0
            def totKterima = 0
            def totKkeluar = 0
            def totBawal = 0
            def totBterima = 0
            def totBkeluar = 0
            def totBankSetor = 0
            def totKasSetor = 0
            def totUK = 0
            def totUL = 0

            def bankBelum = BankAccountNumber.createCriteria().list {
                eq("companyDealer",companydealer)
                eq("staDel","0")
                bank{
                    order("m702NamaBank")
                }
            }

            def staSudahBank = false
            int indData=0
            def totSemuaKas = 0
            def pembulatanKemarin = 0
            def cariCB = CashBalance.createCriteria().get {
                eq("staDel","0")
                eq("companyDealer",companydealer)
                ge("requiredDate",awal)
                lt("requiredDate",awal+1)
                order("dateCreated","desc")
                maxResults(1);
            }
            def pecahans = PecahanUang.createCriteria().list {
                eq("staDel","0")
                order("type","asc")
                order("nilai","desc")
            }
            pecahans.each {
                def cbDetail = CashBalanceDetail.findByCashBalanceAndStaDelAndPecahanUang(cariCB,"0",it)
                def qtyTemp = cbDetail ? cbDetail.quantity : 0
                def nilaiTemp = qtyTemp * it.nilai
                if(it?.type?.equalsIgnoreCase("KERTAS")){
                    items1 << [
                            pecahan1 : konversi.toRupiah(it.nilai),
                            fisik1 : konversi.toRupiah(qtyTemp),
                            jum1 : konversi.toRupiah(nilaiTemp)
                    ]
                    totUK+=nilaiTemp
                }else{
                    items2 << [
                            pecahan2 : konversi.toRupiah(it.nilai),
                            fisik2 : konversi.toRupiah(qtyTemp),
                            jum2 : konversi.toRupiah(nilaiTemp)
                    ]
                    totUL+=nilaiTemp
                }
            }

            results.each {
                indData++
                def totD = 0
                def totK = 0
                def totSetor = 0
                String namaAkun = ""
                def dataTemp = it
                AccountNumber accNumber = AccountNumber.findByAccountNumberIlikeAndStaDelAndAccountMutationType(dataTemp.number,"0","MUTASI")
                MonthlyBalance mbalance;
                def saldoAkhirBefore = 0
                if(indData==1){ //KAS
                    dataTemp?.number = "1.10.10.01.001"
                    AccountNumber akunKas = AccountNumber.findByAccountNumberIlikeAndStaDelAndAccountMutationType(dataTemp?.number,"0","MUTASI")
                    def moreDetail = JournalDetail.createCriteria().list {
                        eq("staDel","0")
                        accountNumber{
                            eq("accountNumber",dataTemp?.number)
                        }
                        journal {
                            ge("journalDate",awal)
                            lt("journalDate",awal+1)
                            eq("staDel","0")
                            eq("companyDealer",companydealer)
                            eq("isApproval", "1")
                        }
                    }
                    mbalance = MonthlyBalance.findByAccountNumberAndYearMonthAndCompanyDealer(akunKas,lastPeriod,companydealer)
                    if(mbalance){
                        saldoAkhirBefore = mbalance.endingBalance
                    }
                    if(awal.format("dd")!="01"){
                        String noAkunCari = dataTemp?.number
                        def detailBefore = JournalDetail.createCriteria().list {
                            eq("staDel","0")
                            accountNumber{
                                ilike("accountNumber","%"+noAkunCari)
                            }
                            journal {
                                ge("journalDate",new Date().parse("dd/MM/yyyy","01/"+awal?.format("MM/yyyy")))
                                lt("journalDate",awal)
                                eq("staDel","0")
                                eq("companyDealer",companydealer)
                                eq("isApproval", "1")
                            }
                        }
                        detailBefore.each {
                            saldoAkhirBefore = saldoAkhirBefore + it.debitAmount - it.creditAmount
                        }
                    }
                    if(dataTemp?.number?.contains("1.10.10.01.001")){
                        totKawal+=saldoAkhirBefore
                        namaAkun = "KAS"
                    }else{
                        totBawal+=saldoAkhirBefore
                        def findBank = BankAccountNumber.findByAccountNumberAndStaDel(AccountNumber.findByAccountNumberAndStaDel(dataTemp.number,"0"),"0")
                        if(findBank){namaAkun = findBank?.bank?.m702NamaBank+" "+(findBank?.bank?.m702Cabang?findBank?.bank?.m702Cabang:"") }else{namaAkun="BANK"}
                    }

                    moreDetail.each {
                        if(it?.accountNumber?.accountNumber?.equalsIgnoreCase("1.10.10.01.001")){
                            if(it?.journal?.isSetoranBank && it?.journal?.isSetoranBank=="1"){
                                totKasSetor+= (it?.creditAmount>0 ? it?.creditAmount : it?.debitAmount)
                            }else{
                                totKterima+=it.debitAmount
                                totKkeluar+=it.creditAmount
                            }
                        }else{
                            if(it?.journal?.isSetoranBank && it?.journal?.isSetoranBank=="1"){
                                totBankSetor+= (it?.creditAmount>0 ? it?.creditAmount : it?.debitAmount)
                            }else{
                                totBterima+=it.debitAmount
                                totBkeluar+=it.creditAmount
                            }
                        }
                        if(it?.journal?.isSetoranBank && it?.journal?.isSetoranBank=="1"){
                            totSetor+= (it?.creditAmount>0 ? it?.creditAmount : it?.debitAmount)
                        }else{
                            totK+=it.creditAmount
                            totD+=it.debitAmount
                        }
                    }
                    totSemuaKas = saldoAkhirBefore+totD-totK-totSetor
                    if(cariCB){
                        pembulatanKemarin = cariCB?.cashBalance
                    }else {
                        pembulatanKemarin = totSemuaKas
                    }
                    if(staNoData==true){
                        totD = 0
                        totK = 0
                        totSetor = 0
                        totBterima = 0
                        totBkeluar = 0
                    }
                    def data = [:]

                    data.put("CabangKombos",companydealer?.m011NamaWorkshop);
                    data.put("Tanggal",new Konversi().getHari(awal.getDay()) + ", " + awal.format("dd MMMM yyyy"));
                    data.put("keterangan", namaAkun)
                    data.put("saldoawal", konversi.toRupiah(saldoAkhirBefore))
                    data.put("penerimaan", konversi.toRupiah(totD))
                    data.put("pengeluaran", konversi.toRupiah(totK))
                    data.put("setorbankKas", konversi.toRupiah(totSetor))
                    data.put("saldoakhir", konversi.toRupiah(saldoAkhirBefore+totD-totK-totSetor))
                    data.put("totBsaldoawal", konversi.toRupiah(totBawal))
                    data.put("totBpenerimaan", konversi.toRupiah(totBterima))
                    data.put("totBpengeluaran", konversi.toRupiah(totBkeluar))
                    data.put("totBankSetor", konversi.toRupiah(totBankSetor))
                    data.put("totBsaldoakhir", konversi.toRupiah(totBawal+totBterima-totBkeluar-totBankSetor))
                    data.put("totKsaldoawal", konversi.toRupiah(totKawal+totBawal))
                    data.put("totKpenerimaan", konversi.toRupiah(totKterima+totBterima))
                    data.put("totKpengeluaran", konversi.toRupiah(totKkeluar+totBkeluar))
                    data.put("totKasBankSetor", konversi.toRupiah(totKasSetor+totBankSetor))
                    data.put("totKsaldoakhir", konversi.toRupiah((totBawal+totBterima-totBkeluar-totBankSetor)+(totKawal+totKterima-totKkeluar-totKasSetor)))
                    data.put("jumUK", konversi.toRupiah(totUK))
                    data.put("jumUL", konversi.toRupiah(totUL))
                    data.put("KASIR", kasir)
                    data.put("ADKU", adku)
                    data.put("KACAB", kacab)
                    data.put("KABENG", kabeng)
                    data.put("totOpname", konversi.toRupiah(totUK+totUL))
                    data.put("selisih", konversi.toRupiah(pembulatanKemarin))
                    data.put("items1", items1)
                    data.put("items2", items2)
                    reportData.add(data)
                }


                if(indData==results.size() && bankBelum.size()>0){
                    bankBelum.each {
                        totD=0
                        totK=0
                        totSetor=0
                        totSetor=0
                        accNumber = it.accountNumber
                        saldoAkhirBefore = 0
                        mbalance = MonthlyBalance.findByAccountNumberAndYearMonthAndCompanyDealer(accNumber,lastPeriod,companydealer)
                        if(mbalance){
                            saldoAkhirBefore = mbalance.endingBalance
                        }
                        if(awal.format("dd")!="01"){
                            String noAkunCari = it?.accountNumber?.accountNumber
                            def detailBefore = JournalDetail.createCriteria().list {
                                eq("staDel","0")
                                accountNumber{
                                    ilike("accountNumber","%"+noAkunCari)
                                }
                                journal {
                                    ge("journalDate",new Date().parse("dd/MM/yyyy","01/"+awal?.format("MM/yyyy")))
                                    lt("journalDate",awal)
                                    eq("staDel","0")
                                    eq("companyDealer",companydealer)
                                    eq("isApproval", "1")
                                }
                            }
                            detailBefore.each {
                                saldoAkhirBefore = saldoAkhirBefore + it.debitAmount - it.creditAmount
                            }
                        }
                        totBawal+=saldoAkhirBefore
                        def bankAcc=it
                        def moreDetail2 = JournalDetail.createCriteria().list {
                            eq("staDel","0")
                            accountNumber{
                                eq("accountNumber",bankAcc?.accountNumber?.accountNumber)
                            }
                            journal {
                                ge("journalDate",awal)
                                lt("journalDate",awal+1)
                                eq("staDel","0")
                                eq("companyDealer",companydealer)
                                eq("isApproval", "1")
                            }
                        }
                        moreDetail2.each {
                            if(it?.journal?.isSetoranBank && it?.journal?.isSetoranBank=="1"){
                                totBankSetor+= (it?.creditAmount>0 ? it?.creditAmount : it?.debitAmount)
                            }else{
                                totBterima+=it.debitAmount
                                totBkeluar+=it.creditAmount
                            }
                            if(it?.journal?.isSetoranBank && it?.journal?.isSetoranBank=="1"){
                                totSetor+= (it?.creditAmount>0 ? it?.creditAmount : it?.debitAmount)
                            }else{
                                totK+=it.creditAmount
                                totD+=it.debitAmount
                            }
                        }

                        namaAkun = it?.bank?.m702NamaBank+" "+(it?.bank?.m702Cabang?it?.bank?.m702Cabang:"")

                        if(staNoData==true){
                            totD = 0
                            totK = 0
                            totSetor = 0
                            totBterima = 0
                            totBkeluar = 0
                        }
                        def data2 = [:]
                        data2.put("CabangKombos",companydealer?.m011NamaWorkshop);
                        data2.put("Tanggal",new Konversi().getHari(awal.getDay())+", "+awal.format("dd MMMM yyyy"));
                        data2.put("keterangan", namaAkun)
                        data2.put("saldoawal", konversi.toRupiah(saldoAkhirBefore))
                        data2.put("penerimaan", konversi.toRupiah(totD))
                        data2.put("pengeluaran", konversi.toRupiah(totK))
                        data2.put("setorbankKas", konversi.toRupiah(totSetor))
                        data2.put("saldoakhir", konversi.toRupiah(saldoAkhirBefore+totD-totK-totSetor))
                        data2.put("totBsaldoawal", konversi.toRupiah(totBawal))
                        data2.put("totBpenerimaan", konversi.toRupiah(totBterima))
                        data2.put("totBpengeluaran", konversi.toRupiah(totBkeluar))
                        data2.put("totBankSetor", konversi.toRupiah(totBankSetor))
                        data2.put("totBsaldoakhir", konversi.toRupiah(totBawal+totBterima-totBkeluar-totBankSetor))
                        data2.put("totKsaldoawal", konversi.toRupiah(totKawal+totBawal))
                        data2.put("totKpenerimaan", konversi.toRupiah(totKterima+totBterima))
                        data2.put("totKpengeluaran", konversi.toRupiah(totKkeluar+totBkeluar))
                        data2.put("totKasBankSetor", konversi.toRupiah(totKasSetor+totBankSetor))
                        data2.put("totKsaldoakhir", konversi.toRupiah((totBawal+totBterima-totBkeluar-totBankSetor)+(totKawal+totKterima-totKkeluar-totKasSetor)))
                        data2.put("jumUK", konversi.toRupiah(totUK))
                        data2.put("jumUL", konversi.toRupiah(totUL))
                        data2.put("totOpname", konversi.toRupiah(totUK+totUL))
                        data2.put("selisih", konversi.toRupiah(pembulatanKemarin))
                        data2.put("KASIR", kasir)
                        data2.put("ADKU", adku)
                        data2.put("KACAB", kacab)
                        data2.put("KABENG", kabeng)
                        data2.put("items1", items1)
                        data2.put("items2", items2)
                        reportData.add(data2)
                    }
                }

            }
        }
        if(params.namaReport=="ANALISA_KAS"){
            params.companydealer = companydealer
            reportData = new LapAnalisaKasService().laporan(params)
        }
        if(params.namaReport=="REGISTRASI_CEK_BG"){
            int count = 0
            def nilai1=0,nilai2=0,nilai3=0,nilai4=0,nilai5=0,nilai6=0,nilai7=0,nilai8=0,nilai9=0,nilai10=0,nilai11=0,nilai12=0,nilai13=0
            def alldata = JournalDetail.createCriteria().list {
                journal{
                    eq("companyDealer",companydealer)
                    ge("journalDate",awal)
                    lt("journalDate",awal+1)
                    eq("staDel","0")
                    eq("isApproval", "1")
                }
                eq("staDel","0")
                accountNumber{
                    eq("accountNumber","1.10.10.01.001",[ignoreCase: true]);
                }
                ne("debitAmount",0.toBigDecimal())
            }
            def results = JournalDetail.createCriteria().list {
                eq("staDel","0")
                accountNumber{
                    eq("accountNumber","1.10.10.01.001",[ignoreCase: true]);
                }
                journal {
                    ge("journalDate",awal)
                    lt("journalDate",awal+1)
                    eq("staDel","0")
                    eq("companyDealer",companydealer)
                    eq("isApproval", "1")
                }
            }
            results.each {
                nilai7 = nilai7 + it.debitAmount - it.creditAmount
            }

            alldata.each {
                if(it.journal.description.toUpperCase().contains("PEMBAYARAN PIUTANG")){
                    nilai1+= it.debitAmount
                }else if(it.journal.description.toUpperCase().contains("CASH INVOICE")){
                    nilai2+= it.debitAmount
                }else if(it.journal.description.toUpperCase().contains("U.M. PELANGGAN")){
                    nilai3+= it.debitAmount
                }else if(it.journal.description.toUpperCase().contains("PEMBAYARAN PEMBELIAN PARTS")){
                    nilai4+= it.debitAmount
                }else{
                    nilai5+= it.debitAmount
                }
            }
            nilai6=nilai1+nilai2+nilai3+nilai4+nilai5
            nilai8=nilai6-nilai7
            def data = [:]
            data.put("cabang",companydealer?.m011NamaWorkshop);
            data.put("tanggal","Hari/Tgl "+awal.format("dd MMMM yyyy"));
            data.put("tanggal2", awal.format("dd-MM-yyyy"))
            data.put("kasPenagihan", konversi.toRupiah(nilai1))
            data.put("kasInvTunai", konversi.toRupiah(nilai2))
            data.put("kasInvDP", konversi.toRupiah(nilai3))
            data.put("kasSalesOrder", konversi.toRupiah(nilai4))
            data.put("kasOther", konversi.toRupiah(nilai5))
            data.put("totalKasTunai", konversi.toRupiah(nilai6))
            data.put("totalKasLHKB", konversi.toRupiah(nilai7))
            data.put("selisih", konversi.toRupiah(nilai8))
            data.put("kasPenerimaanCekGiro", konversi.toRupiah(nilai9))
            data.put("kasPengeluaranCekGiro", konversi.toRupiah(nilai10))
            data.put("totalCekGiro", konversi.toRupiah(nilai11))
            data.put("totalPengeluaranCekGiro", konversi.toRupiah(nilai11))
            data.put("selisih2", konversi.toRupiah(nilai13))
            reportData.add(data)
        }
        if(params.namaReport=="REKAP_PENCAPAIAN_HASIL_MEKANIK"){
            int count = 0
            def tanggalAKhir = new Date().parse("dd-MM-yyyy",params.tanggalAkhir)

            def results2 = InvoiceT701.createCriteria().list {
                eq("t701StaDel","0")
                eq("companyDealer",companydealer)
                like("t701Tipe","%0%")
                isNull("t701NoInv_Reff");
                or{
                    isNull("t701StaApprovedReversal")
                    eq("t701StaApprovedReversal","1")
                }
                isNotNull("t701Teknisi");
                gt("t701TotalBayarRp",0.toDouble())
                ge("t701TglJamInvoice",awal)
                lt("t701TglJamInvoice",tanggalAKhir+1)
                resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                projections {
                    groupProperty("t701Teknisi", "teknisi")
                    order("t701Teknisi")
                }
            }
            def tekSpr = NamaManPower.createCriteria().get {
                eq("companyDealer",companydealer);
                eq("staDel","0")
                manPowerDetail{
                    ilike("m015LevelManPower","%SPOORING%");
                }
                maxResults(1);
            }
            def sppor = false
            if(tekSpr && results2.size()>0){
                if(results2?.teknisi?.indexOf(tekSpr?.t015NamaLengkap)>=0 && results2?.teknisi?.last()!=tekSpr?.t015NamaLengkap){
                    def indTemp = results2?.teknisi?.indexOf(tekSpr?.t015NamaLengkap)
                    def namaTemp = results2?.last()
                    results2[results2.size()-1] = results2[indTemp]
                    results2[indTemp] = namaTemp
                }else{
                    results2.add(results2.size(),results2?.last())
                    sppor = true
                }
            }

            if(results2.size()<1){
                def data = [:]
                data.put("Cabang",companydealer?.m011NamaWorkshop);
                data.put("periode", awal?.format("dd/MM/yyyy")+" s.d. "+tanggalAKhir?.format("dd/MM/yyyy"))
                data.put("No", "-")
                data.put("NamaMekanikGroup", "-")
                data.put("Jasa", "-")
                data.put("Parts", "-")
                data.put("Bahan", "-")
                data.put("Spooring", "-")
                data.put("Total", "-")
                reportData.add(data)
            }
            def totSpooring = 0.00
            int countSSS = 0
            results2.each {
                countSSS++
                boolean staTekSpo = false
                def namaTek = it?.teknisi
                if(sppor==true){
                    if(countSSS==results2.size()){
                        namaTek = tekSpr.t015NamaLengkap
                    }
                }
                boolean staSpooring = false;
                def totJasa = 0.00
                def totBahan = 0.00
                def totParts = 0.00
                if(namaTek==tekSpr?.t015NamaLengkap){
                    staTekSpo=true;
                }
                def resultsDetail = InvoiceT701.createCriteria().list {
                    eq("t701StaDel","0")
                    eq("companyDealer",companydealer)
                    like("t701Tipe","%0%")
                    isNull("t701NoInv_Reff");
                    or{
                        isNull("t701StaApprovedReversal")
                        eq("t701StaApprovedReversal","1")
                    }
                    eq("t701Teknisi",namaTek);
                    gt("t701TotalBayarRp",0.toDouble())
                    ge("t701TglJamInvoice",awal)
                    lt("t701TglJamInvoice",tanggalAKhir+1)
                }

                resultsDetail.each {
                    def jobInv = JobInv.findAllByInvoiceAndStaDel(it,"0")
                    jobInv.each {
                        staSpooring=false;
                        if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SPOORING") ||
                                it?.operation?.m053JobsId?.toUpperCase()?.contains("SPOORING") || it?.operation?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")){
                            staSpooring = true;
                        }
                        if(staSpooring){
                            totSpooring += it.t702TotalRp
                        }else {
                            totJasa += it.t702TotalRp
                        }
                    }
                    totJasa = totJasa - it.t701SubletRp
                    totParts += (it?.t701PartsRpCampuran + it?.t701PartsRpToyota)
                    totBahan += (it?.t701MaterialRp + it?.t701OliRp)
                }
                def allTot = 0
                if(staTekSpo){
                    totParts = 0
                    totBahan = 0
                    totJasa  = 0
                    allTot = totSpooring
                }else{
                    allTot = totJasa + totParts + totBahan
                }
                if(allTot!=0){
                    count++;
                    def data = [:]
                    data.put("Cabang",companydealer?.m011NamaWorkshop);
                    data.put("periode", awal?.format("dd/MM/yyyy")+" s.d. "+tanggalAKhir?.format("dd/MM/yyyy"))
                    data.put("No", count)
                    data.put("NamaMekanikGroup", namaTek)
                    data.put("Jasa", staTekSpo?0:konversi.toRupiah(totJasa))
                    data.put("Parts", konversi.toRupiah(totParts))
                    data.put("Bahan", konversi.toRupiah(totBahan))
                    data.put("Spooring", (staTekSpo?konversi.toRupiah(totSpooring):0))
                    data.put("Total", konversi.toRupiah(allTot))
                    reportData.add(data)
                }
            }
        }
        if(params.namaReport=="PENCAPAIAN_HASIL_MEKANIK"){
            def tanggalAKhir = new Date().parse("dd-MM-yyyy",params.tanggalAkhir)
            def results2 = InvoiceT701.createCriteria().list {
                eq("t701StaDel","0")
                eq("companyDealer",companydealer)
                like("t701Tipe","%0%")
                isNull("t701NoInv_Reff");
                or{
                    isNull("t701StaApprovedReversal")
                    eq("t701StaApprovedReversal","1")
                }
                isNotNull("t701Teknisi");
                gt("t701TotalBayarRp",0.toDouble())
                ge("t701TglJamInvoice",awal)
                lt("t701TglJamInvoice",tanggalAKhir+1)
                resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                projections {
                    groupProperty("t701Teknisi", "teknisi")
                    max("dateCreated", "dateCreated")
                    order("dateCreated","asc")
                    order("t701Teknisi")
                }

            }
            def tekSpr = NamaManPower.createCriteria().get {
                eq("companyDealer",companydealer);
                eq("staDel","0")
                manPowerDetail{
                    ilike("m015LevelManPower","%SPOORING%");
                }
                maxResults(1);
            }
            def sppor = false
            if(tekSpr && results2.size()>0){
                if(results2?.teknisi?.indexOf(tekSpr?.t015NamaLengkap)>=0 && results2?.teknisi?.last()!=tekSpr?.t015NamaLengkap){
                    def indTemp = results2?.teknisi?.indexOf(tekSpr?.t015NamaLengkap)
                    def namaTemp = results2?.last()
                    results2[results2.size()-1] = results2[indTemp]
                    results2[indTemp] = namaTemp
                }else{
                  results2.add(results2.size(),results2?.last())
                    sppor = true
                }
            }

            def hasil = []
            def arrSpo = []
            int count = 0
            results2.each {
                count++
                hasil = []
                def teknisi = it?.teknisi
                if(sppor==true){
                    if(count==results2.size()){
                        teknisi = tekSpr.t015NamaLengkap
                    }
                }
                boolean staTekSpo = false
                def totJasa = 0.00,totPart=0.00,totBahan=0.00
                if(teknisi==tekSpr?.t015NamaLengkap){
                    staTekSpo=true;
                }
                def invoices = InvoiceT701.createCriteria().list {
                    eq("t701StaDel","0")
                    eq("companyDealer",companydealer)
                    like("t701Tipe","%0%")
                    isNull("t701NoInv_Reff");
                    or{
                        isNull("t701StaApprovedReversal")
                        eq("t701StaApprovedReversal","1")
                    }
                    eq("t701Teknisi",teknisi);
                    gt("t701TotalBayarRp",0.toDouble())
                    ge("t701TglJamInvoice",awal)
                    lt("t701TglJamInvoice",tanggalAKhir+1)
                    reception{
                        order("t401TanggalWO","asc")
                    }
                    order("dateCreated","asc");
                    order("t701NoInv");
                }

                invoices.each {
                    def jasa = 0.00
                    def sublet = it.t701SubletRp
                    def jobInv = JobInv.findAllByInvoiceAndStaDel(it,"0")
                    jobInv.each {
                        def staSpooring=false;
                        if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SPOORING") ||
                                it?.operation?.m053JobsId?.toUpperCase()?.contains("SPOORING") || it?.operation?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")){
                            staSpooring = true;
                        }
                        if(staSpooring){
                            arrSpo << [
                                    noInv : it?.invoice?.id,
                                    harga : it?.t702TotalRp
                            ]
                        }else {
                            jasa+=it.t702TotalRp
                        }
                    }
                    jasa = jasa - sublet;
                    totJasa += jasa
                    totPart+=it?.t701PartsRp
                    totBahan+=(it?.t701MaterialRp+it?.t701OliRp)
                    if(!staTekSpo){
                        hasil << [
                            namaTeknisi : teknisi,
                            bulan : awal?.format("dd-MM-yyyy")+" s.d. "+tanggalAKhir?.format("dd-MM-yyyy"),
                            noWO : it?.reception?.t401NoWO,
                            tgglWO : it?.reception?.t401TanggalWO ? it?.reception?.t401TanggalWO?.format("dd/MM/yyyy") : it?.reception?.dateCreated?.format("dd/MM/yyyy"),
                            noSI : it?.t701NoInv,
                            tgglSI : it?.dateCreated ? it?.dateCreated?.format("dd/MM/yyyy") : it?.dateCreated?.format("dd/MM/yyyy"),
                            jasa : konversi?.toRupiah(jasa),
                            part : konversi?.toRupiah((it?.t701PartsRpCampuran + it?.t701PartsRpToyota)),
                            bahan : konversi?.toRupiah((it?.t701MaterialRp + it?.t701OliRp)),
                            totJasa : konversi?.toRupiah(totJasa),
                            totPart : konversi?.toRupiah(totPart),
                            totBahan : konversi?.toRupiah(totBahan)
                        ]
                    }
                }

                if(staTekSpo){
                    totJasa= 0
                    hasil = []
                    arrSpo.each {
                        totJasa+=it?.harga
                        def inV = InvoiceT701.get(it?.noInv?.toLong())
                        hasil << [
                                namaTeknisi : teknisi,
                                bulan : awal?.format("dd-MM-yyyy")+" s.d. "+tanggalAKhir?.format("dd-MM-yyyy"),
                                noWO : inV?.reception?.t401NoWO,
                                tgglWO : inV?.reception?.t401TanggalWO ? inV?.reception?.t401TanggalWO?.format("dd/MM/yyyy") : inV?.reception?.dateCreated?.format("dd/MM/yyyy"),
                                noSI : inV?.t701NoInv,
                                tgglSI : inV?.dateCreated ? inV?.dateCreated?.format("dd/MM/yyyy") : inV?.dateCreated?.format("dd/MM/yyyy"),
                                jasa : konversi?.toRupiah(it?.harga),
                                part : konversi?.toRupiah(0),
                                bahan : konversi?.toRupiah(0),
                                totJasa : konversi?.toRupiah(totJasa),
                                totPart : konversi?.toRupiah(0),
                                totBahan : konversi?.toRupiah(0)
                        ]
                    }
                }
                def data = [:]
                data.put("cabang",companydealer?.m011NamaWorkshop);
                data.put("listDetail", hasil)
                reportData.add(data)
            }


            if(results2.size()<1){
                hasil << [
                        namaTeknisi : "",
                        bulan : awal?.format("dd-MM-yyyy")+" s.d. "+tanggalAKhir?.format("dd-MM-yyyy"),
                        noWO : "-",
                        tgglWO : "-",
                        noSI : "-",
                        tgglSI : "-",
                        jasa : "-",
                        part : "-",
                        bahan : "-"
                ]
                def data = [:]
                data.put("cabang",companydealer?.m011NamaWorkshop);
                data.put("listDetail", hasil)
                reportData.add(data)
            }
        }
        if(params.namaReport.toString().contains("BUKTI_KAS_KELUAR")){
            reportData = calculateBuktiKeluarMasukKasBank(params)
        }
        if(params.namaReport.toString().contains("BUKTI_KAS_MASUK")){
            reportData = calculateBuktiKeluarMasukKasBank(params)
        }
        if(params.namaReport.toString().contains("BUKTI_BANK_KELUAR")){
            reportData = calculateBuktiKeluarMasukKasBank(params)
        }
        if(params.namaReport.toString().contains("BUKTI_BANK_MASUK")){
            reportData = calculateBuktiKeluarMasukKasBank(params)
        }
        if(params.namaReport=="DAFTAR_PENJELASAN_UMUR_PIUTANG"){
            awal = new Date().parse("dd/MM/yyyy","01/"+new Date().format("MM/yyyy"))
            def tanggalAKhir = new Date().clearTime()
            def panggil = new MonthlyBalanceController()
            def thnSkrg = new Date().format("yyyy")
            if(params.bulan=="12"){
                thnSkrg = (thnSkrg.toInteger()-1).toString()
            }
            String tglEnd = panggil.tanggalAkhir(params.bulan.toInteger(),thnSkrg.toInteger())
            def tanggalAKhirCari = new Date().parse("dd/M/yyyy",tglEnd+"/"+params.bulan+"/"+thnSkrg)
            int count = 0
            def grand1 = 0,grand2 = 0,grand3 = 0,grand4 = 0,grand5 = 0,grand6 = 0,grand7 = 0
            def grandNo1 = 0,grandNo2 = 0,grandNo3 = 0,grandNo4 = 0,grandNo5 = 0,grandNo6 = 0,grandNo7 = 0
            def arrsubReportBeanList = []
            def arritemSubSub = []
            def arrGrupDebitur = ["INSTANSI :","ASURANSI :","PERORANGAN :","HASJRAT ABADI :"]
            for(int a = 0;a<5;a++){
                def total1 = 0,total2 = 0,total3 = 0,total4 = 0,total5 = 0,total6 = 0,total7 = 0
                arrsubReportBeanList = []
                arritemSubSub = []
                count = 0;
                def collections = []
                if(a==0){
                    collections = Collection.createCriteria().list {
                        eq("paymentStatus","BELUM")
                        eq("subType",SubType.findBySubTypeIlike("COMPANY"));
                        ge("dateCreated",awal)
                        lt("dateCreated",(tanggalAKhir+1).clearTime())
                        invoiceT071{
                            not{
                                ilike("t701Customer","%HASJRAT%");
                            }
                        }
                    }
                }
                if(a==1){
                    collections = Collection.createCriteria().list {
                        eq("paymentStatus","BELUM")
                        eq("subType",SubType.findBySubTypeIlike("ASURANSI"));
                        ge("dateCreated",awal)
                        lt("dateCreated",(tanggalAKhir+1).clearTime())
                        invoiceT071{
                            not{
                                ilike("t701Customer","%HASJRAT%");
                            }
                        }
                    }
                }
                if(a==2){
                    collections = Collection.createCriteria().list {
                        eq("paymentStatus","BELUM")
                        eq("subType",SubType.findBySubType("CUSTOMER"));
                        ge("dateCreated",awal)
                        lt("dateCreated",(tanggalAKhir+1).clearTime())
                        invoiceT071{
                            not{
                                ilike("t701Customer","%HASJRAT%");
                            }
                        }
                    }
                }
                if(a==3){
                    collections = Collection.createCriteria().list {
                        lt("dueDate",tanggalAKhirCari+1);
                        ilike("paymentStatus","%BELUM%");
                        eq("companyDealer",companydealer)
                        invoiceT071{
                            ilike("t701Customer","%HASJRAT%");
                        }
                        ge("dateCreated",awal)
                        lt("dateCreated",(tanggalAKhir+1).clearTime())
                    }

                }

                collections.each {
                    def jumlah1 = 0,jumlah2 = 0,jumlah3 = 0,jumlah4 = 0,jumlah5 = 0,jumlah6 = 0,jumlah7 = 0
                    String namaSubReport = ""
                    String debitur=""
                    String noPolis=""
                    String noTglSPK=""
                    def temp = it
                    if(a==0 || a==2 || a==3){
                        count++;
                        if(a==0){
                            temp = it[0]
                            def spk = SPK.get(it[1].toLong())
                            if(spk){
                                noTglSPK = spk?.t191NoSPK
                            }
                        }
                        def paid = 0
                        def saldo = temp?.amount - temp?.paidAmount
                        def saldoBaru = temp?.amount - temp?.paidAmount
                        def dpPaid = temp?.invoiceT071?.t701BookingFee ? temp?.invoiceT071?.t701BookingFee  + (temp?.paidAmount ? temp?.paidAmount  : 0) : (temp?.paidAmount ? temp?.paidAmount  : 0)
                        def paidInv = Collection.createCriteria().get {
                            eq("companyDealer",companydealer)
                            ilike("paymentStatus","%PAID%")
                            ge("lastUpdated",awal)
                            lt("lastUpdated",(tanggalAKhir+1).clearTime())
                            eq("invoiceT071",temp?.invoiceT071)
                            order("lastUpdated","desc")
                            maxResults(1);
                        }
                        if(paidInv){
                            paid = paidInv?.paidAmount
                            saldoBaru = paidInv?.amount - paidInv?.paidAmount
                            dpPaid = dpPaid - paid
                            saldo = saldo + paid
                        }
                        if(a==0){
                            namaSubReport = "";
                            debitur = temp?.invoiceT071?.t701Customer;
                        }else{
                            namaSubReport = ""
                            debitur = temp?.invoiceT071?.reception?.historyCustomer?.fullNama;
                        }
                        jumlah1+=temp?.invoiceT071?.t701TotalBayarRp
                        jumlah2+=temp?.invoiceT071?.t701OnRisk
                        jumlah3+=(temp?.invoiceT071?.t701OnRisk ? temp?.invoiceT071?.t701TotalBayarRp - temp?.invoiceT071?.t701OnRisk : 0)
                        jumlah4+=dpPaid
                        jumlah5+=saldo
                        jumlah6+=paid
                        jumlah7+=saldoBaru
                        arritemSubSub << [
                                namaSubReport : namaSubReport,
                                no : count,
                                debitur : debitur,
                                nopol : temp?.invoiceT071?.reception?.historyCustomerVehicle?.fullNoPol,
                                noRangka : temp?.invoiceT071?.reception?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                                noMesin : temp?.invoiceT071?.reception?.historyCustomerVehicle?.t183NoMesin,
                                noTglSPK : noTglSPK,
                                noPolis : noPolis,
                                noInv : temp?.invoiceT071?.t701NoInv,
                                tglInv : temp?.invoiceT071?.t701TglJamInvoice?temp?.invoiceT071?.t701TglJamInvoice?.format("dd-MM-yyyy"):"",
                                jumlahInv : konversi?.toRupiah(Math.round(temp?.invoiceT071?.t701TotalRp)),
                                onRisk : konversi?.toRupiah(Math.round(temp?.invoiceT071?.t701OnRisk ? temp?.invoiceT071?.t701OnRisk : 0)),
                                sisaTagihan : konversi?.toRupiah(Math.round(temp?.invoiceT071?.t701TotalBayarRp)) ,
                                jumlahDP : konversi?.toRupiah(Math.round(dpPaid)),
                                tglDP : "",
                                saldo : konversi?.toRupiah(Math.round(saldo)),
                                pembayaran : konversi?.toRupiah(Math.round(paid)),
                                saldoBaru : konversi?.toRupiah(Math.round(saldoBaru)),
                                tglPenyerahanDok : "",
                                penjelasan : "",
                                jumlah1 : konversi?.toRupiah(jumlah1),
                                jumlah2 : konversi?.toRupiah(jumlah2),
                                jumlah3 : konversi?.toRupiah(jumlah3),
                                jumlah4 : konversi?.toRupiah(jumlah4),
                                jumlah5 : konversi?.toRupiah(jumlah5),
                                jumlah6 : konversi?.toRupiah(jumlah6),
                                jumlah7 : konversi?.toRupiah(jumlah7)
                        ]
                    }else{
                        count=0;
                        arritemSubSub = []
                        def allColects = Collection.createCriteria().list {
                            eq("staDel","0")
                            ilike("paymentStatus","BELUM")
                            lt("dueDate",(tanggalAKhirCari+1).clearTime())
                            eq("companyDealer",companydealer)
                            invoiceT071{
                                ilike("t701Customer","%"+temp.invoiceT071.t701Customer+"%")
                            }
                        }
                        allColects.each {
                            count++;
                            noPolis = "";noTglSPK = "";
                            def tempCol = it
                            def paid = 0
                            def saldo = it?.amount - it?.paidAmount
                            def saldoBaru = it?.amount - it?.paidAmount
                            def dpPaid = it?.invoiceT071?.t701BookingFee ? it?.invoiceT071?.t701BookingFee  + (it?.paidAmount ? it?.paidAmount  : 0) : (it?.paidAmount ? it?.paidAmount  : 0)
                            def paidInv = Collection.createCriteria().get {
                                eq("companyDealer",companydealer)
                                ilike("paymentStatus","%PAID%")
                                ge("lastUpdated",awal)
                                lt("lastUpdated",(tanggalAKhir+1).clearTime())
                                eq("invoiceT071",tempCol?.invoiceT071)
                                order("lastUpdated","desc")
                                maxResults(1);
                            }
                            if(paidInv){
                                paid = paidInv?.paidAmount
                                saldoBaru = paidInv?.amount - paidInv?.paidAmount
                                dpPaid = dpPaid - paid
                                saldo = saldo + paid
                            }
                            if(a==1){
                                namaSubReport = it?.invoiceT071?.t701Customer;
                                debitur = it?.invoiceT071?.reception?.historyCustomer?.fullNama;
                                def spk = SPKAsuransi.findByVendorAsuransi(VendorAsuransi.get(temp[2]?.toLong()),[short : 't193TanggalSPK',order : 'desc']);
                                if(spk){
                                    noPolis = spk?.t193NomorPolis
                                    noTglSPK = spk?.t193NomorSPK
                                }
                            }
                            if(a==2){
                                namaSubReport = it?.invoiceT071?.t701Customer
                                debitur = it?.invoiceT071?.reception?.historyCustomer?.fullNama;
                                def cmp = Company.findByNamaPerusahaanAndAlamatPerusahaan(temp[0],temp[1])
                                if(cmp){
                                    def spk = SPK.findByCompany(cmp);
                                    if(spk){
                                        noTglSPK = spk?.t191NoSPK
                                    }
                                }
                            }
                            jumlah1+=it?.invoiceT071?.t701TotalBayarRp
                            jumlah2+=it?.invoiceT071?.t701OnRisk
                            jumlah3+=(it?.invoiceT071?.t701OnRisk ? it?.invoiceT071?.t701TotalBayarRp - it?.invoiceT071?.t701OnRisk : 0)
                            jumlah4+=dpPaid
                            jumlah5+=saldo
                            jumlah6+=paid
                            jumlah7+=saldoBaru
                            arritemSubSub << [
                                    namaSubReport : namaSubReport,
                                    no : count,
                                    debitur : debitur,
                                    nopol : it?.invoiceT071?.reception?.historyCustomerVehicle?.fullNoPol,
                                    noRangka : it?.invoiceT071?.reception?.historyCustomerVehicle?.customerVehicle?.t103VinCode,
                                    noMesin : it?.invoiceT071?.reception?.historyCustomerVehicle?.t183NoMesin,
                                    noTglSPK : noTglSPK,
                                    noPolis : noPolis,
                                    noInv : it?.invoiceT071?.t701NoInv,
                                    tglInv : it?.invoiceT071?.t701TglJamInvoice?it?.invoiceT071?.t701TglJamInvoice?.format("dd-MM-yyyy"):"",
                                    jumlahInv : konversi.toRupiah(it?.invoiceT071?.t701TotalBayarRp),
                                    onRisk : konversi.toRupiah(it?.invoiceT071?.t701OnRisk ? it?.invoiceT071?.t701OnRisk : 0),
                                    sisaTagihan : konversi.toRupiah(it?.invoiceT071?.t701OnRisk ? it?.invoiceT071?.t701TotalBayarRp - it?.invoiceT071?.t701OnRisk : 0)  ,
                                    jumlahDP : konversi.toRupiah(dpPaid),
                                    tglDP : "",
                                    saldo : konversi.toRupiah(saldo),
                                    pembayaran : konversi.toRupiah(paid),
                                    saldoBaru : konversi.toRupiah(saldoBaru),
                                    tglPenyerahanDok : "",
                                    penjelasan : "",
                                    jumlah1 : konversi.toRupiah(jumlah1),
                                    jumlah2 : konversi.toRupiah(jumlah2),
                                    jumlah3 : konversi.toRupiah(jumlah3),
                                    jumlah4 : konversi.toRupiah(jumlah4),
                                    jumlah5 : konversi.toRupiah(jumlah5),
                                    jumlah6 : konversi.toRupiah(jumlah6),
                                    jumlah7 : konversi.toRupiah(jumlah7)
                            ]
                        }
                    }
                    total1+=jumlah1;total2+=jumlah2;total3+=jumlah3;total4+=jumlah4;total5+=jumlah5;total6+=jumlah6;total7+=jumlah7;
                    if(a==0 || a==2 || a==3){
                        if(count==collections.size()){
                            arrsubReportBeanList << [
                                    itemsSubSub : arritemSubSub,
                                    total1 : konversi.toRupiah(total1),
                                    total2 : konversi.toRupiah(total2),
                                    total3 : konversi.toRupiah(total3),
                                    total4 : konversi.toRupiah(total4),
                                    total5 : konversi.toRupiah(total5),
                                    total6 : konversi.toRupiah(total6),
                                    total7 : konversi.toRupiah(total7)
                          ]
                        }
                    }else{
                        arrsubReportBeanList << [
                                itemsSubSub : arritemSubSub,
                                total1 : konversi.toRupiah(total1),
                                total2 : konversi.toRupiah(total2),
                                total3 : konversi.toRupiah(total3),
                                total4 : konversi.toRupiah(total4),
                                total5 : konversi.toRupiah(total5),
                                total6 : konversi.toRupiah(total6),
                                total7 : konversi.toRupiah(total7)
                      ]
                    }
                }
                if(collections.size()<1){
                    arritemSubSub << [
                            namaSubReport : "",
                            no : "",
                            debitur : "",
                            nopol : "",
                            noRangka : "",
                            noMesin : "",
                            noTglSPK : "",
                            noPolis : "",
                            noInv : "",
                            tglInv : "",
                            jumlahInv : "",
                            onRisk : "",
                            sisaTagihan : "",
                            jumlahDP : "",
                            tglDP : "",
                            saldo : "",
                            pembayaran : "",
                            saldoBaru : "",
                            tglPenyerahanDok : "",
                            penjelasan : "",
                            jumlah1 : "",
                            jumlah2 : "",
                            jumlah3 : "",
                            jumlah4 : "",
                            jumlah5 : "",
                            jumlah6 : "",
                            jumlah7 : ""
                    ]
                    arrsubReportBeanList << [
                            itemsSubSub : arritemSubSub,
                            total1 : "",
                            total2 : "",
                            total3 : "",
                            total4 : "",
                            total5 : "",
                            total6 : "",
                            total7 : "",
                    ]
                }
                grand1+=total1;grand2+=total2;grand3+=total3;grand4+=total4;grand5+=total5;grand6+=total6;grand7+=total7;
                if(a!=3){
                    grandNo1=grand1;grandNo2=grand2;grandNo3=grand3;grandNo4=grand4;grandNo5=grand5;grandNo6=grand6;grandNo7=grand7;
                }
                def data = [:]
                data.put("cabang",companydealer?.m011NamaWorkshop);
                data.put("tanggal",tanggalAKhirCari.format("dd MMMM yyyy"));
                data.put("grupDebitur",arrGrupDebitur[a]);
                data.put("subReportBeanList", arrsubReportBeanList)
                data.put("kota",companydealer?.kabKota ? companydealer?.kabKota?.m002NamaKabKota+" ," : " . . . ,")
                data.put("tgglAkhirTTD", tanggalAKhir ? tanggalAKhir?.format("dd MMMM yyyy") : " . . .")
                data.put("grand1", konversi.toRupiah(grand1))
                data.put("grand2", konversi.toRupiah(grand2))
                data.put("grand3", konversi.toRupiah(grand3))
                data.put("grand4", konversi.toRupiah(grand4))
                data.put("grand5", konversi.toRupiah(grand5))
                data.put("grand6", konversi.toRupiah(grand6))
                data.put("grand7", konversi.toRupiah(grand7))
                data.put("grandNo1", konversi.toRupiah(grandNo1))
                data.put("grandNo2", konversi.toRupiah(grandNo2))
                data.put("grandNo3", konversi.toRupiah(grandNo3))
                data.put("grandNo4", konversi.toRupiah(grandNo4))
                data.put("grandNo5", konversi.toRupiah(grandNo5))
                data.put("grandNo6", konversi.toRupiah(grandNo6))
                data.put("grandNo7", konversi.toRupiah(grandNo7))
                reportData.add(data)
            }
        }

        if(params.namaReport=="DAFTAR_UMUR_PIUTANG"){
            awal = new Date().parse("dd/MM/yyyy","01/"+params.bulan+new Date().format("/yyyy"))
            def panggil = new MonthlyBalanceController()
            def thnSkrg = new Date().format("yyyy")
            if((params.bulan as int) < 10){
                params.bulan = "0" + params.bulan
            }

            def periode = params.bulan+""+thnSkrg
            String tglEnd = panggil.tanggalAkhir(params.bulan.toInteger(),thnSkrg.toInteger())
            def tanggalAKhirCari = new Date().parse("dd/M/yyyy",tglEnd+"/"+params.bulan+"/"+thnSkrg)
            int count = 0
            def  total130 = 0,total3160 = 0,total6190 = 0, total91180 = 0,totalLebih180 = 0
            def arritemSubSub = []
            def arrGrupDebitur = ["ASURANSI","INSTANSI SWASTA","PERORANGAN","HASJRAT"]
            def arrTotalGrupDebitur = ["TOTAL ASURANSI","TOTAL INSTANSI SWASTA","TOTAL PERORANGAN","TOTAL HASJRAT"]
            for(int a = 0;a<4;a++){
                def total130_D = 0,total3160_D = 0,total6190_D = 0,total91180_D = 0,totalLebih180_D = 0;
                arritemSubSub = []
                count = 0;
               def collections = InvoiceT701.createCriteria().list {
                   eq("companyDealer",session.userCompanyDealer)
                   isNull("t701NoInv_Reff");
                   or{
                       isNull("t701StaApprovedReversal")
                       eq("t701StaApprovedReversal","1")
                   }
                   gt("t701TotalInv",0.toDouble())
                   eq("t701JenisInv","K")
                   eq("t701StaSettlement","0")
                   isNotNull("t701Subtype")
                   isNotNull("t701Subledger")
                   resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                   projections {
                       groupProperty("companyDealer", "companyDealer")
                       groupProperty("t701Subtype", "t701Subtype")
                       groupProperty("t701Subledger", "t701Subledger")
                       order("t701Subtype")
                       order("t701Subledger")
                   }
               }
                collections.each { piu->
                    def nama = "-";
                    def kode = "-";

                    if(a==0){
                        kode = piu?.t701Subledger
                        if(SubType.findById(piu?.t701Subtype)?.subType == "ASURANSI"){
                            nama = VendorAsuransi.findById(piu.t701Subledger as Long)?.m193Nama
                        }
                    }else if(a==1){
                        kode = piu?.t701Subledger
                        if(SubType.findById(piu?.t701Subtype)?.subType == "VENDOR"){
                            nama = Vendor.findById(piu.t701Subledger as Long)?.m121Nama
                        }else if(SubType.findById(piu?.t701Subtype)?.subType == "CUSTOMER COMPANY"
                                && (!Company.findById(piu?.t701Subledger as Long)?.namaPerusahaan?.toUpperCase()?.contains("HASJRAT")
                                    && !Company.findById(piu?.t701Subledger as Long)?.namaPerusahaan?.toUpperCase()?.contains("KOMBOS"))){
                            nama = Company.findById(piu?.t701Subledger as Long)?.namaPerusahaan
                        }else if(SubType.findById(piu?.t701Subtype)?.subType == "CABANG"){
                            nama = CompanyDealer.findById(piu?.t701Subledger as Long)?.m011NamaWorkshop
                        }
                    }else if(a==2){
                        kode = piu?.t701Subledger
                        if(SubType.findById(piu?.t701Subtype)?.subType == "CUSTOMER"){
                            nama = HistoryCustomer.findById(piu?.t701Subledger as Long).t182NamaDepan
                        }
                    }else{
                        kode = piu?.t701Subledger
                        if(SubType.findById(piu?.t701Subtype)?.subType == "CUSTOMER COMPANY"
                                && (Company.findById(piu?.t701Subledger as Long)?.namaPerusahaan?.toUpperCase()?.contains("HASJRAT")
                                || Company.findById(piu?.t701Subledger as Long)?.namaPerusahaan?.toUpperCase()?.contains("KOMBOS"))){
                            nama = Company.findById(piu?.t701Subledger as Long)?.namaPerusahaan
                        }
                    }
                    if(nama!="-"){
                        count++;
                        arritemSubSub << [
                                namaKategori : arrGrupDebitur[a],
                                no : count,
                                kode : kode,
                                namaPiutang : nama,
                                jum130 : konversi?.toRupiah(jumlahHutang(piu?.t701Subtype,kode,tanggalAKhirCari,1,30)),
                                jum3060 : konversi?.toRupiah(jumlahHutang(piu?.t701Subtype,kode,tanggalAKhirCari,30,60)),
                                jum6090 : konversi?.toRupiah(jumlahHutang(piu?.t701Subtype,kode,tanggalAKhirCari,60,90)),
                                jum90180 : konversi?.toRupiah(jumlahHutang(piu?.t701Subtype,kode,tanggalAKhirCari,90,180)),
                                lebihDari180 : konversi?.toRupiah(jumlahHutang(piu?.t701Subtype,kode,tanggalAKhirCari,180,1000)),
                                keterangan : "",
                                total130 : konversi?.toRupiah(total130_D),
                                total3160 : konversi?.toRupiah(total3160_D),
                                total6190 : konversi?.toRupiah(total6190_D),
                                total91180 : konversi?.toRupiah(total91180_D),
                                totalLebih180 : konversi?.toRupiah(totalLebih180_D),
                                totalNamaKategori : arrTotalGrupDebitur[a]
                        ]
                    }
                }
                if(collections.size()<1){
                    arritemSubSub << [
                            namaKategori : arrGrupDebitur[a],
                            no : "",
                            kode : "",
                            namaPiutang : "",
                            saldoAwal : "",
                            debet : "",
                            kredit : "",
                            saldoAkhir : "",
                            jum130 : "",
                            jum3060 : "",
                            jum6090 : "",
                            jum90180 : "",
                            lebihDari180 : "",
                            keterangan : "",
                            totalSaldoAwal : "",
                            totalDebet : "",
                            totalKredit : "",
                            totalSaldoPer : "",
                            total130 : "",
                            total3160 : "",
                            total6190 : "",
                            total91180 : "",
                            totalLebih180 : "",
                            totalNamaKategori : arrTotalGrupDebitur[a]
                    ]
                }

                total130+=total130_D
                total3160+=total3160_D
                total6190+=total6190_D
                total91180+=total91180_D
                totalLebih180+=totalLebih180_D

                def data = [:]
                data.put("cabang",companydealer?.m011NamaWorkshop);
                data.put("tanggal",tanggalAKhirCari.format("dd MMMM yyyy"));
                data.put("subReportBeanList", arritemSubSub)
                data.put("total1-30", konversi.toRupiah(total130))
                data.put("total31-60", konversi.toRupiah(total3160))
                data.put("total61-90", konversi.toRupiah(total6190))
                data.put("total91-180", konversi.toRupiah(total91180))
                data.put("totalLebih180", konversi.toRupiah(totalLebih180))
                reportData.add(data)
            }
        }
        return reportData
    }

    def calculateReportDetailData(def params){
        def reportData = new ArrayList();
        Date awal = new Date().clearTime()
        if(params.tanggal){
            awal = new Date().parse("dd-MM-yyyy",params.tanggal)
        }
        CompanyDealer companydealer = session.userCompanyDealer
        if(params.companyDealer){
            companydealer = CompanyDealer.get(params.companyDealer.toLong())
        }
        if(params.namaReport=="REKAP_PENCAPAIAN_HASIL_MEKANIK"){
            int count = 0
            def tanggalAKhir = new Date().parse("dd-MM-yyyy",params.tanggalAkhir)
            def result2 = InvoiceT701.createCriteria().list {
                eq("t701StaDel","0")
                isNull("t701NoInv_Reff");
                or{
                    isNull("t701StaApprovedReversal")
                    eq("t701StaApprovedReversal","1")
                }
                ilike("t701Tipe","%1%");
                gt("t701TotalBayarRp",0.toDouble())
                eq("companyDealer",companydealer)
                ge("t701TglJamInvoice",awal)
                lt("t701TglJamInvoice",tanggalAKhir+1)
            }
            def groupTeknisi = GroupManPower.createCriteria().list {
                eq("companyDealer",companydealer)
                eq("staDel","0")
                ilike("t021Group","%BP%")
            }
            groupTeknisi.each { gt ->
                def allTot = 0.00
                def totJasa = 0.00
                def totSpooring = 0.00
                def totBahan = 0.00
                def totParts = 0.00
                NamaManPower manPower = null
                boolean staSpooring = false;
                result2.each {
                    manPower = NamaManPower.findByT015NamaLengkapAndStaDel(it?.t701Teknisi, "0")
                    def grp = GroupManPower.createCriteria().get {
                        eq("companyDealer", companydealer)
                        namaManPowers {
                            eq("id", manPower?.id)
                        }
                        maxResults(1);
                    }
                    if (gt?.id == grp?.id) {
                        def jobInv = JobInv.findAllByInvoiceAndStaDel(it,"0")
                        staSpooring=false;
                        jobInv.each {
                            if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SPOORING") ||
                                    it?.operation?.m053JobsId?.toUpperCase()?.contains("SPOORING") || it?.operation?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")){
                                   staSpooring = true;
                            }
                            if(staSpooring){
                                totSpooring += it.t702TotalRp
                            }else {
                                totJasa += it.t702TotalRp
                            }
                        }
                        if(totSpooring>0){
                            totJasa = 0
                        }
                        totJasa = totJasa - it.t701SubletRp;
                        totParts += (it?.t701PartsRpCampuran + it?.t701PartsRpToyota)
                        totBahan += (it?.t701MaterialRp + it?.t701OliRp)
                        allTot = totJasa + totParts + totBahan + totSpooring
                    }
                }
                if(allTot!=0){
                    count++;
                    def data = [:]
                    data.put("Cabang",companydealer?.m011NamaWorkshop);
                    data.put("No", count)
                    data.put("NamaMekanikGroup", gt?.namaManPowerForeman?.t015NamaLengkap+"/Group Body " + count)
                    data.put("Jasa", konversi.toRupiah(totJasa))
                    data.put("Parts", konversi.toRupiah(totParts))
                    data.put("Bahan", konversi.toRupiah(totBahan))
                    data.put("Spooring", konversi.toRupiah(totSpooring))
                    data.put("Total", konversi.toRupiah(allTot))
                    reportData.add(data)
                }
            }
        }else if(params.namaReport=="PENCAPAIAN_HASIL_MEKANIK"){
            def tanggalAKhir = new Date().parse("dd-MM-yyyy",params.tanggalAkhir)
            def listInv = InvoiceT701.createCriteria().list {
                eq("t701StaDel","0")
                isNull("t701NoInv_Reff");
                or{
                    isNull("t701StaApprovedReversal")
                    eq("t701StaApprovedReversal","1")
                }
                ilike("t701Tipe","%1%");
                gt("t701TotalBayarRp",0.toDouble())
                eq("companyDealer",companydealer)
                ge("t701TglJamInvoice",awal)
                lt("t701TglJamInvoice",tanggalAKhir+1)
                reception{
                    order("t401TanggalWO","asc")
                }
                order("t701Teknisi");
                order("t701TglJamInvoice","asc");
                order("t701NoInv");
            }

            NamaManPower manPower = null
            def allTot = 0.00
            def totJasa = 0.00
            def totBahan = 0.00
            def totParts = 0.00
            int count  = 0
            def groupTeknisi = GroupManPower.createCriteria().list {
                eq("companyDealer",companydealer)
                eq("staDel","0")
                ilike("t021Group","%BP%")
            }
            groupTeknisi.each { gt ->
                def hasil = []
                count++
                allTot = 0.00
                totJasa = 0.00
                totBahan = 0.00
                totParts = 0.00
                listInv.each {
                    boolean staSpooring = false;
                    manPower = NamaManPower.findByT015NamaLengkapAndStaDel(it?.t701Teknisi, "0")
                    def grp = GroupManPower.createCriteria().get {
                        eq("companyDealer", companydealer)
                        namaManPowers {
                            eq("id", manPower?.id)
                        }
                        maxResults(1);
                    }
                    if (gt?.id == grp?.id) {
                        def jobInv = JobInv.findAllByInvoiceAndStaDel(it, "0")
                        jobInv.each {
                            staSpooring = false;
                            if (it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SPOORING") ||
                                    it?.operation?.m053JobsId?.toUpperCase()?.contains("SPOORING") || it?.operation?.m053NamaOperation?.toUpperCase()?.contains("SPOORING")) {
                                staSpooring = true;
                            }
                            if (!staSpooring) {
                                totJasa += it.t702TotalRp
                            }
                        }
                        totParts += (it?.t701PartsRpCampuran + it?.t701PartsRpToyota)
                        totBahan += (it?.t701MaterialRp + it?.t701OliRp)
                        allTot = totJasa + totParts + totBahan
                        hasil << [
                                namaTeknisi: gt?.namaManPowerForeman?.t015NamaLengkap + "/Group Body " + count,
                                bulan      : awal?.format("dd-MM-yyyy") + " s.d. " + tanggalAKhir?.format("dd-MM-yyyy"),
                                noWO       : it?.reception?.t401NoWO,
                                tgglWO     : it?.reception?.t401TanggalWO ? it?.reception?.t401TanggalWO?.format("dd/MM/yyyy") : it?.reception?.dateCreated?.format("dd/MM/yyyy"),
                                noSI       : it?.t701NoInv,
                                tgglSI     : it?.t701TglJamInvoice ? it?.t701TglJamInvoice?.format("dd/MM/yyyy") : it?.dateCreated?.format("dd/MM/yyyy"),
                                jasa       : konversi?.toRupiah(it?.t701JasaRp),
                                part       : konversi?.toRupiah(it?.t701PartsRpCampuran + it?.t701PartsRpToyota),
                                bahan      : konversi?.toRupiah(it?.t701MaterialRp + it?.t701OliRp),
                                totJasa    : konversi?.toRupiah(totJasa),
                                totPart    : konversi?.toRupiah(totParts),
                                totBahan   : konversi?.toRupiah(totBahan)
                        ]
                    }
                }
                if(hasil.size()>0){
                    def data = [:]
                    data.put("cabang",companydealer?.m011NamaWorkshop);
                    data.put("listDetail", hasil)
                    reportData.add(data)
                }
            }

        }

        return reportData
    }

    def calculateBuktiKeluarMasukKasBank(def params){
        def arrTitel1 = ["Uang Muka Service an ","Hutang Usaha an ","Piutang Usaha an "]
        def arrTitel2 = ["Pengembalian DP an ","Pembayaran PO no ",""]
        def arrTitelTunai = ["Pendapatan Jasa Tunai","Pendapatan Parts Tunai","Pendapatan Bahan Tunai","Pendapatan Pekerjaan Luar Tunai",
                "Pendapatan Jasa Adm Tunai","PPn Keluaran Penjualan PD Umum","Pendapatan Lainnya","\tPotongan Harga Jasa","\tPotongan Harga Parts",
                "\tPotongan Harga Bahan","\tPotongan Harga Oli"]
        def arrAkunTunai = ["6.10.10.01.001","6.10.10.02.001","6.10.10.03.001","6.10.10.03.001",
                "6.10.10.05.001","4.20.10.01.001","8.10.10.04.001","6.10.30.01.001","6.10.30.01.002",
                "6.10.30.01.003",""]
        Date awal = new Date().clearTime()
        if(params.tanggal){
            awal = new Date().parse("dd-MM-yyyy",params.tanggal)
        }
        def reportData = new ArrayList();
        CompanyDealer companydealer = session.userCompanyDealer
        if(params.companyDealer){
            companydealer = CompanyDealer.get(params.companyDealer.toLong())
        }
        String namaReport = params.namaReport;
        namaReport = namaReport.substring(namaReport.lastIndexOf("_"));
        String judulReport = "";
        String title = "BUKTI ";
        int indTitle = 0
        def results = JournalDetail.createCriteria().list {
            eq("staDel","0")
            accountNumber{
                if(params?.namaReport?.toString()?.contains("KAS")){
                    title+="KAS "
                    ilike("accountNumber","%1.10.10.01.001%");
                }else{
                    title+="BANK "
                    ilike("accountNumber","%1.10.20.%")
                }
            }
            if(params?.namaReport?.toString()?.contains("KELUAR")){
                ne("creditAmount",0.toBigDecimal())
            }else{
                ne("debitAmount",0.toBigDecimal())
            }
            journal{
                eq("isApproval", "1")
                eq("staDel","0")
                eq("companyDealer",companydealer)
                ge("journalDate",awal)
                lt("journalDate",awal+1)
                if(params?.namaReport?.toString()?.contains("KELUAR")){
                    title+="KELUAR ";
                    if(namaReport.equalsIgnoreCase("_REFUND")){
                        judulReport = "Pengembalian DP";
                        ilike("description","%Pengembalian Uang Muka%")
                    }else if(namaReport.equalsIgnoreCase("_HUTANG")){
                        indTitle = 1;
                        judulReport = "Pembayaran Hutang Usaha";
                        ilike("description","%Hutang Toko Luar%")
                    }else {
                        indTitle = -1;
                        judulReport = "Pengeluaran Operasional dan lain lain";
                        not {
                            ilike("description","%Pengembalian Uang Muka%")
                            ilike("description","%Hutang Toko Luar%")
                        }
                    }
                }
                else{
                    title+="MASUK ";
                    if(namaReport.equalsIgnoreCase("_TUNAI")){
                        indTitle = -2;
                        judulReport = "Pendapatan Tunai";
                        ilike("description","%Cash Invoice%")
                    }else if(namaReport.equalsIgnoreCase("_PIUTANG")){
                        indTitle = 2;
                        judulReport = "Penerimaan Piutang";
                        ilike("description","%Pembayaran Piutang Kredit%")
                    }else {

                        indTitle = -3;
                        judulReport = "Penerimaan lain lain";
                        not {
                            ilike("description","%Cash Invoice%")
                            ilike("description","%Pembayaran Piutang Kredit%")
                        }
                    }
                }
                order("journalCode");
            }

        }

        def tblData = []

        if(results.size()<1){
            def data = [:]
            data.put("judul",title);
            data.put("Kantor1",companydealer?.m011NamaWorkshop);
            data.put("jenisTrx", judulReport)
            data.put("No1", "-")
            data.put("Tanggal1",awal.format("dd-MM-yyyy"))
            data.put("items",tblData)
            data.put("Terbilang1", konversi.toRupiah(0))
            data.put("JmlTerbilang1", 0)
            reportData.add(data)
        }
        int indLoop = 0
        int indLoopDDDD = 0
        int count = 0
        boolean doMapping = false
        def totalJumlah = 0
        def totalJumlahKredit = 0
        def invArr = []
        int jumlahData = results.size()
        results.each { res ->
            def journal = res?.journal
            count++;
            indLoop++;
            def jurnDetail = res
            def jurnDetail2 = res

            String an = "";
            String an2 = "";
            String noPol = "";
            if(indTitle==0){
                def recept = Reception.findByT401NoAppointmentIlike("%"+res?.journal?.docNumber+"%")
                def app = Appointment.findByReception(recept);
                an = app ? app?.historyCustomer?.fullNama : ""
                an2 = an
                noPol = app ? app?.historyCustomerVehicle?.fullNoPol : ""
            }else if(indTitle==1){
                def inv = Invoice.findByT166NoInvIlike("%"+res?.journal?.docNumber+"%")
                an = inv ? inv?.vendor?.m121Nama : ""
                an2 = inv ? inv?.t166NoInv : ""
            }
            else if(indTitle==2){
                def inv = InvoiceT701.findByT701NoInvIlike("%"+res?.journal?.docNumber+"%")
                an = inv ? inv?.t701Customer : ""
                an2 = "No. Inv. "+inv?.t701NoInv
                jurnDetail2 = JournalDetail.createCriteria().get {
                    eq("journal",journal);
                    eq("staDel","0");
                    accountNumber{
                        ilike("accountNumber","1.10.20.01.%");
                    }
                    maxResults(1);
                }
            }

            if(indTitle==-2){
                invArr << res.journal.docNumber
            }else {
                if(indTitle>=0){
//                    totalJumlah += res?.debitAmount
                    if(res?.debitAmount == 0){
                        totalJumlah += res?.creditAmount
                        totalJumlahKredit += res?.creditAmount
                    }else {
                        totalJumlah += res?.debitAmount
                        totalJumlahKredit += res?.debitAmount
                    }
                        tblData << [
                        txtJudul : "Nama Rekening\t:",
                        txtKeterangan : arrTitel1[indTitle]+an,
                        noAkun : jurnDetail ? jurnDetail?.accountNumber?.accountNumber : "",
//                        jumUang : jurnDetail2 ? konversi.toRupiah(jurnDetail2?.debitAmount)  : konversi.toRupiah(jurnDetail?.debitAmount),
                        jumUang : jurnDetail2 ? (jurnDetail2?.creditAmount != 0 ? konversi.toRupiah(jurnDetail2?.creditAmount) : konversi.toRupiah(jurnDetail2?.debitAmount))  : (jurnDetail?.creditAmount != 0 ? konversi.toRupiah(jurnDetail?.creditAmount) : konversi.toRupiah(jurnDetail?.debitAmount)),
                    ]
                    tblData << [
                        txtJudul : "Keterangan\t:",
                        txtKeterangan : arrTitel2[indTitle]+an2,
                        noAkun : "",
                        jumUang : "",
                    ]
                    if(indTitle==0){
                        tblData << [
                                txtJudul : "",
                                txtKeterangan : "No Polisi : "+noPol,
                                noAkun : "",
                                jumUang : ""
                        ]
                    }
                    tblData << [
                        txtJudul : "",
                        txtKeterangan : "",
                        noAkun : "",
                        jumUang : ""
                    ]
                }
                else {
                    indLoop = 0
                    def docNumberTem = "";
                    def akunNumberTem = "";
                    int sizeDaaa = 0;
                    jumlahData = JournalDetail.findAllByJournalAndStaDel(res.journal,'0').size()
                    JournalDetail.findAllByJournalAndStaDel(res.journal,'0', [sort: "journal.docNumber", order: "asc"]).each {
                        if(!it.accountNumber.accountNumber.toUpperCase().contains("1.10.20.01") && !it.accountNumber.accountNumber.toUpperCase().contains("1.10.10.01.001") ){
                            sizeDaaa++;
                        }
                    }
                    JournalDetail.findAllByJournalAndStaDel(res.journal,'0',[sort: "journal.docNumber", order: "asc"]).each{
                       if(!it.accountNumber.accountNumber.toUpperCase().contains("1.10.20.01") && !it.accountNumber.accountNumber.toUpperCase().contains("1.10.10.01.001") ){
                           if(it.journal.docNumber!=docNumberTem){
//                               if (it.accountNumber != akunNumberTem) {
                                   indLoop = 0
//                               }
                           }
                           indLoop++
                           docNumberTem = it.journal.docNumber
                           akunNumberTem = it.accountNumber
                           totalJumlah += it?.creditAmount!=0?it?.creditAmount: it?.debitAmount
                           totalJumlahKredit += res?.creditAmount

                           tblData << [
                                    txtJudul : "Nama Rekening\t:",
                                    txtKeterangan : it ? it?.accountNumber?.accountName + atasNama(it.subType,it.subLedger) : "",
                                    noAkun : it ? it?.accountNumber?.accountNumber : "",
                                    jumUang : konversi.toRupiah(it?.creditAmount != 0 ? it?.creditAmount : it?.debitAmount)
                           ]
                           tblData << [
                                    txtJudul : "Keterangan:",
                                    txtKeterangan : it?.description ? it?.description : it?.journal.description,
                                    noAkun : "",
                                    jumUang : "",
                           ]
                           tblData << [
                                    txtJudul : "",
                                    txtKeterangan : "",
                                    noAkun : "",
                                    jumUang : ""
                           ]
                             if(sizeDaaa==indLoop || indLoop==jumlahData || indLoop==4){
                                 indLoop = 0;
                                 if(totalJumlah>0){
                                    def data = [:]
                                    data.put("judul",title);
                                    data.put("Kantor1",companydealer?.m011NamaWorkshop);
                                    data.put("jenisTrx", judulReport)
                                    data.put("No1", "-")
                                    data.put("Tanggal1",awal.format("dd-MM-yyyy"))
                                    data.put("items",tblData)
                                    if(indTitle==-1){
                                        data.put("Terbilang1", moneyUtil.convertMoneyToWords(totalJumlah))
                                        data.put("JmlTerbilang1", konversi.toRupiah(totalJumlah))
                                    }else{
                                        data.put("Terbilang1", moneyUtil.convertMoneyToWords(totalJumlah))
                                        data.put("JmlTerbilang1", konversi.toRupiah(totalJumlah))
                                    }
                                    reportData.add(data);
                                    tblData = [];
                                    totalJumlah = 0;
                                    totalJumlahKredit = 0;
                                 }
                            }
                        }
                    }
                }
            }
            if((count==jumlahData || indLoop==4) && indTitle!=-2 && indTitle!=-3 && indTitle!=-1){
                doMapping = true;
            }

            if(doMapping==true && indTitle!=-2 && indTitle!=-3 && indTitle!=-1){
                doMapping = false;
                indLoop = 0;
                def data = [:]
                data.put("judul",title);
                data.put("Kantor1",companydealer?.m011NamaWorkshop);
                data.put("jenisTrx", judulReport)
                data.put("No1", "-")
                data.put("Tanggal1",awal.format("dd-MM-yyyy"))
                data.put("items",tblData)
                data.put("Terbilang1", moneyUtil.convertMoneyToWords(totalJumlah))
                data.put("JmlTerbilang1", konversi.toRupiah(totalJumlah))
                reportData.add(data);
                tblData = [];
                totalJumlah = 0;
                totalJumlahKredit = 0;
            }

            if(params?.namaReport?.toString()?.contains("BANK") && indTitle==2){
                def jurnDetails2 = JournalDetail.createCriteria().list {
                    eq("staDel","0");
                    gt("debitAmount",0.toBigDecimal());
                    eq("journal",jurnDetail?.journal);
                    ne("accountNumber",jurnDetail?.accountNumber)
                }
                jumlahData+= jurnDetails2?.size()
                jurnDetails2.each { res2 ->
                    count ++
                    indLoop++;
                    tblData << [
                            txtJudul : "Nama Rekening\t:",
                            txtKeterangan : res2?.accountNumber?.accountName + atasNama(res2.subType,res2.subLedger),
                            noAkun : res2?.accountNumber?.accountNumber,
                            jumUang : konversi.toRupiah(res2?.debitAmount)
                    ]
                    tblData << [
                            txtJudul : "Keterangan\t:",
                            txtKeterangan : "No. Inv. "+res2?.journal?.docNumber,
                            noAkun : "",
                            jumUang : "",
                    ]

                    tblData << [
                            txtJudul : "",
                            txtKeterangan : "",
                            noAkun : "",
                            jumUang : ""
                    ]
                    if((count==results.size() || indLoop==3) && indTitle!=-2){
                        doMapping = true;
                    }
                    if(doMapping && indTitle!=-2){
                        doMapping = false;
                        indLoop = 0;
                        def data = [:]
                        data.put("judul",title);
                        data.put("Kantor1",companydealer?.m011NamaWorkshop);
                        data.put("jenisTrx", judulReport)
                        data.put("No1", "-")
                        data.put("Tanggal1",awal.format("dd-MM-yyyy"))
                        data.put("items",tblData)
                        if(indTitle==-1){
                            data.put("Terbilang1", moneyUtil.convertMoneyToWords(totalJumlahKredit))
                            data.put("JmlTerbilang1", konversi.toRupiah(totalJumlahKredit))
                        }else{
                            data.put("Terbilang1", moneyUtil.convertMoneyToWords(totalJumlah))
                            data.put("JmlTerbilang1", konversi.toRupiah(totalJumlah))
                        }

                        reportData.add(data);
                        tblData = [];
                        totalJumlah = 0;
                        totalJumlahKredit = 0;
                    }
                }
            }
        }

        count = 0;
        def nilai = []
        for(int a=0;a<11;a++){
            nilai << 0
        }
        if(invArr.size()<1 && indTitle==-2){
            for(int x=0;x<11;x++){
                tblData << [
                        txtJudul : x==0 ? "Nama Rekening\t:": "",
                        txtKeterangan : arrTitelTunai[x],
                        noAkun : arrAkunTunai[x],
                        jumUang : ""
                ]
            }
            tblData << [
                    txtJudul : "Keterangan\t:",
                    txtKeterangan : "Sesuai HASS Tanggal "+awal?.format("dd-MM-yyyy"),
                    noAkun : "",
                    jumUang : ""
            ]
            def data = [:]
            data.put("judul",title);
            data.put("Kantor1",companydealer?.m011NamaWorkshop);
            data.put("jenisTrx", judulReport)
            data.put("No1", "-")
            data.put("Tanggal1",awal.format("dd-MM-yyyy"))
            data.put("items",tblData)
            data.put("Terbilang1", moneyUtil.convertMoneyToWords(0.toDouble()))
            data.put("JmlTerbilang1", 0)
            reportData.add(data);
        }
        invArr.each {
            count++;
            def invoice = InvoiceT701.findByT701NoInvIlikeAndT701StaDel("%"+it+"%","0")
            if(invoice){
                nilai[0]+= invoice?.t701JasaRp
                nilai[1]+= invoice?.t701PartsRp
                nilai[2]+= invoice?.t701OliRp + invoice?.t701MaterialRp
                nilai[3]+= invoice?.t701SubletRp
                nilai[4]+= invoice?.t701AdmRp
                nilai[5]+= invoice?.t701PPnRp
                nilai[6]+= invoice?.t701MateraiRp
                def jobs = JobInv.findAllByInvoiceAndStaDel(invoice,"0")
                jobs.each {
                    nilai[7]+= (it.t702DiscRp ? it.t702DiscRp : 0)
                }
                def parts = PartInv.findAllByInvoiceAndT703StaDel(invoice,"0")
                parts.each {
                    def klas = KlasifikasiGoods.findByGoods(it.goods)
                    if(klas){
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("CAMPURAN")){
                            nilai[8]+=(it.t703DiscRp ? it.t703DiscRp : 0)
                        }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("ACCESORIES")){
                            nilai[8]+=(it.t703DiscRp ? it.t703DiscRp : 0)
                        }else if(klas.franc.m117NamaFranc.toUpperCase().contains("TOYOTA")){
                            nilai[8]+=(it.t703DiscRp ? it.t703DiscRp : 0)
                        }else if(klas.franc.m117NamaFranc.toUpperCase().contains("OIL")){
                            nilai[10]+=(it.t703DiscRp ? it.t703DiscRp : 0)
                        }else{
                            nilai[9]+=(it.t703DiscRp ? it.t703DiscRp : 0)
                        }
                    }else {
                        nilai[9]+= (it.t703DiscRp ? it.t703DiscRp : 0)
                    }
                }
            }
            if(count==invArr.size()){
                for(int x=0;x<11;x++){
                    if(x<7){
                        totalJumlah += nilai[x]
                    }else {
                        totalJumlah -= nilai[x]
                    }
                    tblData << [
                            txtJudul : x==0 ? "Nama Rekening\t:": "",
                            txtKeterangan : arrTitelTunai[x],
                            noAkun : arrAkunTunai[x],
                            jumUang : konversi?.toRupiah(nilai[x])
                    ]
                }
                tblData << [
                        txtJudul : "Keterangan\t:",
                        txtKeterangan : "Sesuai HASS Tanggal "+awal?.format("dd-MM-yyyy"),
                        noAkun : "",
                        jumUang : ""
                ]
                def data = [:]
                data.put("judul",title);
                data.put("Kantor1",companydealer?.m011NamaWorkshop);
                data.put("jenisTrx", judulReport)
                data.put("No1", "-")
                data.put("Tanggal1",awal.format("dd-MM-yyyy"))
                data.put("items",tblData)
                data.put("Terbilang1", moneyUtil.convertMoneyToWords(totalJumlah))
                data.put("JmlTerbilang1", konversi.toRupiah(totalJumlah))
                reportData.add(data);
            }
        }
        return reportData
    }

    def atasNama(def subtype, def subledger){
        def hasil = ""
        try {
            if(subtype?.subType == "VENDOR"){
                hasil = " an. " + Vendor.findById(Long.parseLong(subledger))?.m121Nama
            }else if(subtype?.subType == "CUSTOMER"){
                hasil = " an. " + HistoryCustomer.findById(Long.parseLong(subledger))?.t182NamaDepan
            }else if(subtype?.subType == "ASURANSI"){
                hasil = " an. " + VendorAsuransi.findById(Long.parseLong(subledger))?.m193Nama
            }else if(subtype?.subType == "KARYAWAN"){
                hasil = " an. " + Karyawan.findById(Long.parseLong(subledger))?.nama
            }else if(subtype?.subType == "CUSTOMER COMPANY"){
                hasil = " an. " + Company.findById(Long.parseLong(subledger))?.namaPerusahaan
            }
        }catch (Exception e){}
        return hasil
    }

    def jumlahHutang(def subtype, def subledger, tanggalAKhirCari, int mulai, int akhir){
        def total = 0
        def collections = Collection.createCriteria().list {
            eq("companyDealer",session.userCompanyDealer)
            eq("staDel","0")
            ge("dateCreated",tanggalAKhirCari - akhir)
            lt("dateCreated",tanggalAKhirCari - (mulai + 1))
            eq("subLedger", subledger)
            eq("subType", SubType.findById(subtype as Long))
        }
        collections.each {
            total = total + (it.amount - it.paidAmount)
        }
        return total;
    }
}
