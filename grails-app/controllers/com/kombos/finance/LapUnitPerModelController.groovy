package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.PartInv
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import com.kombos.reception.JobInv
import com.kombos.woinformation.JobRCP
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class LapUnitPerModelController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
    def resetData(def data){
        for(int i=1;i<=25;i++){
            data[i] = 0
        }
    }
    def validasiData(def data){
        for(int i=1;i<=25;i++){
            if(data[i] == 0){
                data[i] = ""
            }else{
                data[i] = konversi.toRupiah(data[i] as int)
            }
        }
    }
    def validasiDataBagi1000(def data){
        for(int i=1;i<=25;i++){
            if(data[i] == 0){
                data[i] = ""
            }else{
                data[i] = konversi.toRupiah((data[i]/1000) as int)
            }
        }
    }
    def totalData(def data){
        def total = 0
        for(int i=1;i<=25;i++){
            if(data[i]!=""){
                total += data[i].toString().replace(",","").toInteger()
            }
        }
        if(total!=0){
            total = konversi.toRupiah(total as int)
        }else{
            total =""
        }
        return total
    }
    def index() {}
    def previewDataGR(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        def jenis = "BP"
        if(params.jenis=="General Repair"){
            jenis = "GR"
        }
        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params?.tahun as int,(params?.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = []
        String bulanParam =  params?.tahun + "-" + params?.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        def sbeUnit = []
        def sbeRekening = []

        def diatas100k = [], kelipatan5k = []
        def tDataKm = []
        def dataKm10000 = [],dataKm20000 = [],dataKm30000 = [],dataKm40000 = [],dataKm50000 = [],dataKm60000 = [],
            dataKm70000 = [],dataKm80000 = [],dataKm90000 = [],dataKm100000 = []

        def gantiOli = [], grLainnya = [], subTotalGr = [], totalUnitCpus = []
        def sb1km = [], sb1kmTotal = [], warranty = [], returnJob = [], preDeliveryService = []
        def subTotalNonCPus = [], totalUnitEntry = []

        def jasa = [], partsToyota = [], oli = [], sublet = [], subTotalRevenue = []
        def inv = InvoiceT701.createCriteria().list {
            ge("t701TglJamInvoice", tgl)
            lt("t701TglJamInvoice", tgl2)
            eq("companyDealer",companyDealer)
            ilike("t701Tipe","%0%")
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            order("t701TglJamInvoice","asc")
        }
        def mobil = ["-","Agya","Alphard","Avanza","Camry","Corolla Altis","Dyna","Etios","FJCruiser","Fortuner","Hiace","Hilux","Kijang Kapsul","Kijang Innova","Land Cruiser","Limo","Nav","Prius","Rush","Vios","Yaris","86","Calya","Sienta","CBU TAM","CBU non TAM"]
        resetData(dataKm10000)
        resetData(dataKm20000)
        resetData(dataKm30000)
        resetData(dataKm40000)
        resetData(dataKm50000)
        resetData(dataKm60000)
        resetData(dataKm70000)
        resetData(dataKm80000)
        resetData(dataKm90000)
        resetData(dataKm100000)
        resetData(diatas100k)
        resetData(kelipatan5k)
        resetData(tDataKm)

        resetData(gantiOli)
        resetData(grLainnya)
        resetData(subTotalGr)
        resetData(totalUnitCpus)
        resetData(sb1km)
        resetData(sb1kmTotal)
        resetData(warranty)
        resetData(returnJob)
        resetData(preDeliveryService)
        resetData(subTotalNonCPus)
        resetData(totalUnitEntry)

        resetData(jasa)
        resetData(partsToyota)
        resetData(oli)
        resetData(sublet)
        resetData(subTotalRevenue)

        inv.each {
            def invoice = it
            def kj = JobInv.createCriteria().get {
                eq("invoice",invoice);
                eq("staDel","0");
                operation{
                    ilike("m053Id","%SB%");
                }
                maxResults(1);
            }
            def sb = kj ? kj?.operation?.m053NamaOperation : (JobInv?.findByInvoiceAndStaDel(invoice,"0") ? JobInv?.findByInvoiceAndStaDel(invoice,"0")?.operation?.m053NamaOperation : "")

            def bm =  it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
            def bm22 =  it?.reception?.historyCustomerVehicle?.dealerPenjual
            if(kj!=null && (!kj?.operation?.m053Id?.equalsIgnoreCase("SB1K") || !kj?.operation?.m053NamaOperation?.contains("1.000") || !kj?.operation?.m053NamaOperation?.contains("1000") )){
                for(int i=1;i<=25;i++){
                    if(bm?.toUpperCase()?.contains(mobil[i]?.toUpperCase())){
                        def partInv = PartInv.findAllByInvoice(it)
                        partInv.each {
                            def klas = KlasifikasiGoods.findByGoods(it.goods)
                            if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                                oli[i] += (it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)
                            }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("PARTS TOYOTA")){
                                partsToyota[i] += (it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)
                            }
                        }
                        jasa[i] += it?.t701JasaRp
                        sublet[i] += (it?.t701SubletRp?it?.t701SubletRp:0)
                        if(sb.contains("10.000")){
                            dataKm10000[i]++
                        }else if(sb.contains("20.000")){
                            dataKm20000[i]++
                        }else if(sb.contains("30.000")){
                            dataKm30000[i]++
                        }else if(sb.contains("40.000")){
                            dataKm40000[i]++
                        }else if(sb.contains("50.000")){
                            dataKm50000[i]++
                        }else if(sb.contains("60.000")){
                            dataKm60000[i]++
                        }else if(sb.contains("70.000")){
                            dataKm70000[i]++
                        }else if(sb.contains("80.000")){
                            dataKm80000[i]++
                        }else if(sb.contains("90.000")){
                            dataKm90000[i]++
                        }else if(sb.contains("100.000")){
                            dataKm100000[i]++
                        }else if(it?.reception?.t401KmSaatIni>100000){
                            diatas100k[i]++
                        }
                    }
                    tDataKm[i] = (dataKm10000[i]+dataKm20000[i]+dataKm30000[i]+dataKm40000[i]+dataKm50000[i]+
                            dataKm60000[i]+dataKm70000[i]+dataKm80000[i]+dataKm90000[i]+dataKm100000[i]) + diatas100k[i]
                }
            }else{
                if((kj?.operation?.m053Id?.equalsIgnoreCase("SB1K") || !kj?.operation?.m053NamaOperation?.contains("1.000") || kj?.operation?.m053NamaOperation?.contains("1000") )){
                    for(int i=1;i<=25;i++){
                        if(bm?.toUpperCase()?.contains(mobil[i]?.toUpperCase())){
                            sb1km[i]++
                        }
                        sb1kmTotal[i] = sb1km[i]
                    }
                }
            }

            for(int i=1;i<=25;i++){
                if(bm?.toUpperCase()?.contains(mobil[i]?.toUpperCase())){
                    def partInv = PartInv.findAllByInvoice(it)
                    partInv.each {
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                            oli[i] += (it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)
                        }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("PARTS TOYOTA")){
                            partsToyota[i] += (it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)
                        }
                    }
                    jasa[i] += it.t701JasaRp
                    sublet[i] += (it.t701SubletRp?it?.t701SubletRp:0)
                    if(sb.toUpperCase().contains("GANTI OLI")){
                        gantiOli[i]++
                    }else{
                        grLainnya[i]++
                    }
                }
                    subTotalGr[i] = gantiOli[i] + grLainnya[i]
                    totalUnitCpus[i] = subTotalGr[i] + tDataKm[i]
            }

            for(int i=1;i<=25;i++){
                totalUnitEntry[i] = totalUnitCpus[i] + subTotalNonCPus[i]
                subTotalRevenue[i] = jasa[i] + partsToyota[i]+ oli[i] + sublet[i]
            }
        }
        validasiData(dataKm10000)
        validasiData(dataKm20000)
        validasiData(dataKm30000)
        validasiData(dataKm40000)
        validasiData(dataKm50000)
        validasiData(dataKm60000)
        validasiData(dataKm70000)
        validasiData(dataKm80000)
        validasiData(dataKm90000)
        validasiData(dataKm100000)
        validasiData(kelipatan5k)
        validasiData(diatas100k)
        validasiData(tDataKm)

        validasiData(gantiOli)
        validasiData(grLainnya)
        validasiData(subTotalGr)
        validasiData(totalUnitCpus)
        validasiData(sb1km)
        validasiData(sb1kmTotal)
        validasiData(warranty)
        validasiData(returnJob)
        validasiData(preDeliveryService)
        validasiData(subTotalNonCPus)
        validasiData(totalUnitEntry)
        validasiDataBagi1000(jasa)
        validasiDataBagi1000(partsToyota)
        validasiDataBagi1000(oli)
        validasiDataBagi1000(sublet)
        validasiDataBagi1000(subTotalRevenue)
        sbeUnit << [
                km : "         " + konversi.toRupiah(10000) + " KM",
                agya : dataKm10000[1],alphard : dataKm10000[2],avanza : dataKm10000[3],camry : dataKm10000[4],corollaAltis : dataKm10000[5],
                dyna : dataKm10000[6],etiosValco :dataKm10000[7] ,FJCruiser : dataKm10000[8],fortuner : dataKm10000[9],hiace : dataKm10000[10],
                hilux : dataKm10000[11],kiangKapsul : dataKm10000[12],kijangInnova :dataKm10000[13] ,landCruiser : dataKm10000[14],limo : dataKm10000[15],
                nav1 : dataKm10000[16],prius : dataKm10000[17],rush : dataKm10000[18],vios : dataKm10000[19],yaris : dataKm10000[20],
                _86 : dataKm10000[21],calya : dataKm10000[22],sienta : dataKm10000[23],
                cbuTam : dataKm10000[24],cbuNonTam : dataKm10000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(10000) + " KM",
                agya : dataKm20000[1],alphard : dataKm20000[2],avanza : dataKm20000[3],camry : dataKm20000[4],corollaAltis : dataKm20000[5],
                dyna : dataKm20000[6],etiosValco :dataKm20000[7] ,FJCruiser : dataKm20000[8],fortuner : dataKm20000[9],hiace : dataKm20000[10],
                hilux : dataKm20000[11],kiangKapsul : dataKm20000[12],kijangInnova :dataKm20000[13] ,landCruiser : dataKm20000[14],limo : dataKm20000[15],
                nav1 : dataKm20000[16],prius : dataKm20000[17],rush : dataKm20000[18],vios : dataKm20000[19],yaris : dataKm20000[20],
                _86 : dataKm20000[21],calya : dataKm20000[22],sienta : dataKm20000[23],
                cbuTam : dataKm20000[24],cbuNonTam : dataKm20000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(30000) + " KM",
                agya : dataKm30000[1],alphard : dataKm30000[2],avanza : dataKm30000[3],camry : dataKm30000[4],corollaAltis : dataKm30000[5],
                dyna : dataKm30000[6],etiosValco :dataKm30000[7] ,FJCruiser : dataKm30000[8],fortuner : dataKm30000[9],hiace : dataKm30000[10],
                hilux : dataKm30000[11],kiangKapsul : dataKm30000[12],kijangInnova :dataKm30000[13] ,landCruiser : dataKm30000[14],limo : dataKm30000[15],
                nav1 : dataKm30000[16],prius : dataKm30000[17],rush : dataKm30000[18],vios : dataKm30000[19],yaris : dataKm30000[20],
                _86 : dataKm30000[21],calya : dataKm30000[22],sienta : dataKm30000[23],
                cbuTam : dataKm30000[24],cbuNonTam : dataKm30000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(40000) + " KM",
                agya : dataKm40000[1],alphard : dataKm40000[2],avanza : dataKm40000[3],camry : dataKm40000[4],corollaAltis : dataKm40000[5],
                dyna : dataKm40000[6],etiosValco :dataKm40000[7] ,FJCruiser : dataKm40000[8],fortuner : dataKm40000[9],hiace : dataKm40000[10],
                hilux : dataKm40000[11],kiangKapsul : dataKm40000[12],kijangInnova :dataKm40000[13] ,landCruiser : dataKm40000[14],limo : dataKm40000[15],
                nav1 : dataKm40000[16],prius : dataKm40000[17],rush : dataKm40000[18],vios : dataKm40000[19],yaris : dataKm40000[20],
                _86 : dataKm40000[21],calya : dataKm40000[22],sienta : dataKm40000[23],
                cbuTam : dataKm40000[24],cbuNonTam : dataKm40000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(50000) + " KM",
                agya : dataKm50000[1],alphard : dataKm50000[2],avanza : dataKm50000[3],camry : dataKm50000[4],corollaAltis : dataKm50000[5],
                dyna : dataKm50000[6],etiosValco :dataKm50000[7] ,FJCruiser : dataKm50000[8],fortuner : dataKm50000[9],hiace : dataKm50000[10],
                hilux : dataKm50000[11],kiangKapsul : dataKm50000[12],kijangInnova :dataKm50000[13] ,landCruiser : dataKm50000[14],limo : dataKm50000[15],
                nav1 : dataKm50000[16],prius : dataKm50000[17],rush : dataKm50000[18],vios : dataKm50000[19],yaris : dataKm50000[20],
                _86 : dataKm50000[21],calya : dataKm50000[22],sienta : dataKm50000[23],
                cbuTam : dataKm50000[24],cbuNonTam : dataKm50000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(60000) + " KM",
                agya : dataKm60000[1],alphard : dataKm60000[2],avanza : dataKm60000[3],camry : dataKm60000[4],corollaAltis : dataKm60000[5],
                dyna : dataKm60000[6],etiosValco :dataKm60000[7] ,FJCruiser : dataKm60000[8],fortuner : dataKm60000[9],hiace : dataKm60000[10],
                hilux : dataKm60000[11],kiangKapsul : dataKm60000[12],kijangInnova :dataKm60000[13] ,landCruiser : dataKm60000[14],limo : dataKm60000[15],
                nav1 : dataKm60000[16],prius : dataKm60000[17],rush : dataKm60000[18],vios : dataKm60000[19],yaris : dataKm60000[20],
                _86 : dataKm60000[21],calya : dataKm60000[22],sienta : dataKm60000[23],
                cbuTam : dataKm60000[24],cbuNonTam : dataKm60000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(70000) + " KM",
                agya : dataKm70000[1],alphard : dataKm70000[2],avanza : dataKm70000[3],camry : dataKm70000[4],corollaAltis : dataKm70000[5],
                dyna : dataKm70000[6],etiosValco :dataKm70000[7] ,FJCruiser : dataKm70000[8],fortuner : dataKm70000[9],hiace : dataKm70000[10],
                hilux : dataKm70000[11],kiangKapsul : dataKm70000[12],kijangInnova :dataKm70000[13] ,landCruiser : dataKm70000[14],limo : dataKm70000[15],
                nav1 : dataKm70000[16],prius : dataKm70000[17],rush : dataKm70000[18],vios : dataKm70000[19],yaris : dataKm70000[20],
                _86 : dataKm70000[21],calya : dataKm70000[22],sienta : dataKm70000[23],
                cbuTam : dataKm70000[24],cbuNonTam : dataKm70000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(80000) + " KM",
                agya : dataKm80000[1],alphard : dataKm80000[2],avanza : dataKm80000[3],camry : dataKm80000[4],corollaAltis : dataKm80000[5],
                dyna : dataKm80000[6],etiosValco :dataKm80000[7] ,FJCruiser : dataKm80000[8],fortuner : dataKm80000[9],hiace : dataKm80000[10],
                hilux : dataKm80000[11],kiangKapsul : dataKm80000[12],kijangInnova :dataKm80000[13] ,landCruiser : dataKm80000[14],limo : dataKm80000[15],
                nav1 : dataKm80000[16],prius : dataKm80000[17],rush : dataKm80000[18],vios : dataKm80000[19],yaris : dataKm80000[20],
                _86 : dataKm80000[21],calya : dataKm80000[22],sienta : dataKm80000[23],
                cbuTam : dataKm80000[24],cbuNonTam : dataKm80000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(90000) + " KM",
                agya : dataKm90000[1],alphard : dataKm90000[2],avanza : dataKm90000[3],camry : dataKm90000[4],corollaAltis : dataKm90000[5],
                dyna : dataKm90000[6],etiosValco :dataKm90000[7] ,FJCruiser : dataKm90000[8],fortuner : dataKm90000[9],hiace : dataKm90000[10],
                hilux : dataKm90000[11],kiangKapsul : dataKm90000[12],kijangInnova :dataKm90000[13] ,landCruiser : dataKm90000[14],limo : dataKm90000[15],
                nav1 : dataKm90000[16],prius : dataKm90000[17],rush : dataKm90000[18],vios : dataKm90000[19],yaris : dataKm90000[20],
                _86 : dataKm90000[21],calya : dataKm90000[22],sienta : dataKm90000[23],
                cbuTam : dataKm90000[24],cbuNonTam : dataKm90000[25]
        ]
        sbeUnit << [
                km : "         " + konversi.toRupiah(100000) + " KM",
                agya : dataKm100000[1],alphard : dataKm100000[2],avanza : dataKm100000[3],camry : dataKm100000[4],corollaAltis : dataKm100000[5],
                dyna : dataKm100000[6],etiosValco :dataKm100000[7] ,FJCruiser : dataKm100000[8],fortuner : dataKm100000[9],hiace : dataKm100000[10],
                hilux : dataKm100000[11],kiangKapsul : dataKm100000[12],kijangInnova :dataKm100000[13] ,landCruiser : dataKm100000[14],limo : dataKm100000[15],
                nav1 : dataKm100000[16],prius : dataKm100000[17],rush : dataKm100000[18],vios : dataKm100000[19],yaris : dataKm100000[20],
                _86 : dataKm100000[21],calya : dataKm100000[22],sienta : dataKm100000[23],
                cbuTam : dataKm100000[24],cbuNonTam : dataKm100000[25]
        ]

        sbeUnit << [
                km : "         Kelipatan 5K di bawah 100K [15,25,35,dst]",
                agya : kelipatan5k[1],alphard : kelipatan5k[2],avanza : kelipatan5k[3],camry : kelipatan5k[4],corollaAltis : kelipatan5k[5],
                dyna : kelipatan5k[6],etiosValco :kelipatan5k[7] ,FJCruiser : kelipatan5k[8],fortuner : kelipatan5k[9],hiace : kelipatan5k[10],
                hilux : kelipatan5k[11],kiangKapsul : kelipatan5k[12],kijangInnova :kelipatan5k[13] ,landCruiser : kelipatan5k[14],limo : kelipatan5k[15],
                nav1 : kelipatan5k[16],prius : kelipatan5k[17],rush : kelipatan5k[18],vios : kelipatan5k[19],yaris : kelipatan5k[20],
                _86 : kelipatan5k[21],calya : kelipatan5k[22],sienta : kelipatan5k[23],
                cbuTam : kelipatan5k[24],cbuNonTam : kelipatan5k[25]
        ]
        sbeUnit << [
                km : "         SBE km lainnya di atas 100K",
                agya : diatas100k[1],alphard : diatas100k[2],avanza : diatas100k[3],camry : diatas100k[4],corollaAltis : diatas100k[5],
                dyna : diatas100k[6],etiosValco :diatas100k[7] ,FJCruiser : diatas100k[8],fortuner : diatas100k[9],hiace : diatas100k[10],
                hilux : diatas100k[11],kiangKapsul : diatas100k[12],kijangInnova :diatas100k[13] ,landCruiser : diatas100k[14],limo : diatas100k[15],
                nav1 : diatas100k[16],prius : diatas100k[17],rush : diatas100k[18],vios : diatas100k[19],yaris : diatas100k[20],
                _86 : diatas100k[21],calya : diatas100k[22],sienta : diatas100k[23],
                cbuTam : diatas100k[24],cbuNonTam : diatas100k[25]
        ]
        sbeUnit << [
                km : "         TOTAL SERVICE BERKALA",
                agya : tDataKm[1],alphard : tDataKm[2],avanza : tDataKm[3],camry : tDataKm[4],corollaAltis : tDataKm[5],
                dyna : tDataKm[6],etiosValco :tDataKm[7] ,FJCruiser : tDataKm[8],fortuner : tDataKm[9],hiace : tDataKm[10],
                hilux : tDataKm[11],kiangKapsul : tDataKm[12],kijangInnova :tDataKm[13] ,landCruiser : tDataKm[14],limo : tDataKm[15],
                nav1 : tDataKm[16],prius : tDataKm[17],rush : tDataKm[18],vios : tDataKm[19],yaris : tDataKm[20],
                _86 : tDataKm[21],calya : tDataKm[22],sienta : tDataKm[23],
                cbuTam : tDataKm[24],cbuNonTam : tDataKm[25]
        ]

        sbeUnit << [
                km : "     GENERAL REPAIR",
                agya : "",alphard : "",avanza : "",camry : "",corollaAltis : "",
                dyna : "",etiosValco : "",FJCruiser : "",fortuner : "",hiace : "",
                hilux : "",kiangKapsul : "",kijangInnova :"",landCruiser : "",limo : "",
                nav1 : "",prius : "",rush : "",vios : "",yaris : "",
                _86 : "",calya : "",sienta : "",cbuTam : "",cbuNonTam : ""
        ]

        sbeUnit << [
                km : "         GANTI OLI",
                agya : gantiOli[1],alphard : gantiOli[2],avanza : gantiOli[3],camry : gantiOli[4],corollaAltis : gantiOli[5],
                dyna : gantiOli[6],etiosValco :gantiOli[7] ,FJCruiser : gantiOli[8],fortuner : gantiOli[9],hiace : gantiOli[10],
                hilux : gantiOli[11],kiangKapsul : gantiOli[12],kijangInnova :gantiOli[13] ,landCruiser : gantiOli[14],limo : gantiOli[15],
                nav1 : gantiOli[16],prius : gantiOli[17],rush : gantiOli[18],vios : gantiOli[19],yaris : gantiOli[20],
                _86 : gantiOli[21],calya : gantiOli[22],sienta : gantiOli[23],
                cbuTam : gantiOli[24],cbuNonTam : gantiOli[25],
        ]
        sbeUnit << [
                km : "         GENERAL REPAIR LAINNYA",
                agya : grLainnya[1],alphard : grLainnya[2],avanza : grLainnya[3],camry : grLainnya[4],corollaAltis : grLainnya[5],
                dyna : grLainnya[6],etiosValco :grLainnya[7] ,FJCruiser : grLainnya[8],fortuner : grLainnya[9],hiace : grLainnya[10],
                hilux : grLainnya[11],kiangKapsul : grLainnya[12],kijangInnova :grLainnya[13] ,landCruiser : grLainnya[14],limo : grLainnya[15],
                nav1 : grLainnya[16],prius : grLainnya[17],rush : grLainnya[18],vios : grLainnya[19],yaris : grLainnya[20],
                _86 : gantiOli[21],calya : gantiOli[22],sienta : gantiOli[23],
                cbuTam : gantiOli[24],cbuNonTam : gantiOli[25],
        ]
        sbeUnit << [
                km : "         SUB TOTAL GENERAL REPAIR",
                agya : subTotalGr[1],alphard : subTotalGr[2],avanza : subTotalGr[3],camry : subTotalGr[4],corollaAltis : subTotalGr[5],
                dyna : subTotalGr[6],etiosValco :subTotalGr[7] ,FJCruiser : subTotalGr[8],fortuner : subTotalGr[9],hiace : subTotalGr[10],
                hilux : subTotalGr[11],kiangKapsul : subTotalGr[12],kijangInnova :subTotalGr[13] ,landCruiser : subTotalGr[14],limo : subTotalGr[15],
                nav1 : subTotalGr[16],prius : subTotalGr[17],rush : subTotalGr[18],vios : subTotalGr[19],yaris : subTotalGr[20],
                _86 : gantiOli[21],calya : gantiOli[22],sienta : gantiOli[23],
                cbuTam : gantiOli[24],cbuNonTam : gantiOli[25],
        ]
        sbeUnit << [
                km : "   TOTAL UNIT CPUS",
                agya : totalUnitCpus[1],alphard : totalUnitCpus[2],avanza : totalUnitCpus[3],camry : totalUnitCpus[4],corollaAltis : totalUnitCpus[5],
                dyna : totalUnitCpus[6],etiosValco :totalUnitCpus[7] ,FJCruiser : totalUnitCpus[8],fortuner : totalUnitCpus[9],hiace : totalUnitCpus[10],
                hilux : totalUnitCpus[11],kiangKapsul : totalUnitCpus[12],kijangInnova :totalUnitCpus[13] ,landCruiser : totalUnitCpus[14],limo : totalUnitCpus[15],
                nav1 : totalUnitCpus[16],prius : totalUnitCpus[17],rush : totalUnitCpus[18],vios : totalUnitCpus[19],yaris : totalUnitCpus[20],
                _86 : gantiOli[21],calya : gantiOli[22],sienta : gantiOli[23],
                cbuTam : gantiOli[24],cbuNonTam : gantiOli[25],
        ]
        sbeUnit << [
                km : "  b. UNIT NON CPUS",
                agya : "",alphard : "",avanza : "",camry : "",corollaAltis : "",
                dyna : "",etiosValco : "",FJCruiser : "",fortuner : "",hiace : "",
                hilux : "",kiangKapsul : "",kijangInnova :"",landCruiser : "",limo : "",
                nav1 : "",prius : "",rush : "",vios : "",yaris : "",
                _86 : "",calya : "",sienta : "",cbuTam : "",cbuNonTam : ""
        ]
        sbeUnit << [
                km : "     SBI 1K - 1K Penjualan outlet yang sama",
                agya : sb1km[1],alphard : sb1km[2],avanza : sb1km[3],camry : sb1km[4],corollaAltis : sb1km[5],
                dyna : sb1km[6],etiosValco :sb1km[7] ,FJCruiser : sb1km[8],fortuner : sb1km[9],hiace : sb1km[10],
                hilux : sb1km[11],kiangKapsul : sb1km[12],kijangInnova :sb1km[13] ,landCruiser : sb1km[14],limo : sb1km[15],
                nav1 : sb1km[16],prius : sb1km[17],rush : sb1km[18],vios : sb1km[19],yaris : sb1km[20],
                _86 : sb1km[21],calya : sb1km[22],sienta : sb1km[23],
                cbuTam : sb1km[24],cbuNonTam : sb1km[25],
        ]
        sbeUnit << [
                km : "     SBI 1K - 1K Total",
                agya : sb1kmTotal[1],alphard : sb1kmTotal[2],avanza : sb1kmTotal[3],camry : sb1kmTotal[4],corollaAltis : sb1kmTotal[5],
                dyna : sb1kmTotal[6],etiosValco :sb1kmTotal[7] ,FJCruiser : sb1kmTotal[8],fortuner : sb1kmTotal[9],hiace : sb1kmTotal[10],
                hilux : sb1kmTotal[11],kiangKapsul : sb1kmTotal[12],kijangInnova :sb1kmTotal[13] ,landCruiser : sb1kmTotal[14],limo : sb1kmTotal[15],
                nav1 : sb1kmTotal[16],prius : sb1kmTotal[17],rush : sb1kmTotal[18],vios : sb1kmTotal[19],yaris : sb1kmTotal[20],
                _86 : sb1kmTotal[21],calya : sb1kmTotal[22],sienta : sb1kmTotal[23],
                cbuTam : sb1kmTotal[24],cbuNonTam : sb1kmTotal[25],
        ]
        sbeUnit << [
                km : "     Warranty",
                agya : warranty[1],alphard : warranty[2],avanza : warranty[3],camry : warranty[4],corollaAltis : warranty[5],
                dyna : warranty[6],etiosValco :warranty[7] ,FJCruiser : warranty[8],fortuner : warranty[9],hiace : warranty[10],
                hilux : warranty[11],kiangKapsul : warranty[12],kijangInnova :warranty[13] ,landCruiser : warranty[14],limo : warranty[15],
                nav1 : warranty[16],prius : warranty[17],rush : warranty[18],vios : warranty[19],yaris : warranty[20],
                _86 : gantiOli[21],calya : gantiOli[22],sienta : gantiOli[23],
                cbuTam : gantiOli[24],cbuNonTam : gantiOli[25],
        ]
        sbeUnit << [
                km : "     Return Job/Pekerjaan Ulang",
                agya : returnJob[1],alphard : returnJob[2],avanza : returnJob[3],camry : returnJob[4],corollaAltis : returnJob[5],
                dyna : returnJob[6],etiosValco :returnJob[7] ,FJCruiser : returnJob[8],fortuner : returnJob[9],hiace : returnJob[10],
                hilux : returnJob[11],kiangKapsul : returnJob[12],kijangInnova :returnJob[13] ,landCruiser : returnJob[14],limo : returnJob[15],
                nav1 : returnJob[16],prius : returnJob[17],rush : returnJob[18],vios : returnJob[19],yaris : returnJob[20],
                _86 : returnJob[21],calya : returnJob[22],sienta : returnJob[23],
                cbuTam : returnJob[24],cbuNonTam : returnJob[25],
        ]
        sbeUnit << [
                km : "     Pre Delivery Service",
                agya : preDeliveryService[1],alphard : preDeliveryService[2],avanza : preDeliveryService[3],camry : preDeliveryService[4],corollaAltis : preDeliveryService[5],
                dyna : preDeliveryService[6],etiosValco :preDeliveryService[7] ,FJCruiser : preDeliveryService[8],fortuner : preDeliveryService[9],hiace : preDeliveryService[10],
                hilux : preDeliveryService[11],kiangKapsul : preDeliveryService[12],kijangInnova :preDeliveryService[13] ,landCruiser : preDeliveryService[14],limo : preDeliveryService[15],
                nav1 : preDeliveryService[16],prius : preDeliveryService[17],rush : preDeliveryService[18],vios : preDeliveryService[19],yaris : preDeliveryService[20],
                _86 : preDeliveryService[21],calya : preDeliveryService[22],sienta : preDeliveryService[23],
                cbuTam : preDeliveryService[24],cbuNonTam : preDeliveryService[25],
        ]
        sbeUnit << [
                km : "     SUB TOTAL NON CPUS",
                agya : subTotalNonCPus[1],alphard : subTotalNonCPus[2],avanza : subTotalNonCPus[3],camry : subTotalNonCPus[4],corollaAltis : subTotalNonCPus[5],
                dyna : subTotalNonCPus[6],etiosValco :subTotalNonCPus[7] ,FJCruiser : subTotalNonCPus[8],fortuner : subTotalNonCPus[9],hiace : subTotalNonCPus[10],
                hilux : subTotalNonCPus[11],kiangKapsul : subTotalNonCPus[12],kijangInnova :subTotalNonCPus[13] ,landCruiser : subTotalNonCPus[14],limo : subTotalNonCPus[15],
                nav1 : subTotalNonCPus[16],prius : subTotalNonCPus[17],rush : subTotalNonCPus[18],vios : subTotalNonCPus[19],yaris : subTotalNonCPus[20],
                _86 : subTotalNonCPus[21],calya : subTotalNonCPus[22],sienta : subTotalNonCPus[23],
                cbuTam : subTotalNonCPus[24],cbuNonTam : subTotalNonCPus[25],
        ]
        sbeUnit << [
                km : "   JUMLAH TOTAL UNIT ENTRY",
                agya : totalUnitEntry[1],alphard : totalUnitEntry[2],avanza : totalUnitEntry[3],camry : totalUnitEntry[4],corollaAltis : totalUnitEntry[5],
                dyna : totalUnitEntry[6],etiosValco :totalUnitEntry[7] ,FJCruiser : totalUnitEntry[8],fortuner : totalUnitEntry[9],hiace : totalUnitEntry[10],
                hilux : totalUnitEntry[11],kiangKapsul : totalUnitEntry[12],kijangInnova :totalUnitEntry[13] ,landCruiser : totalUnitEntry[14],limo : totalUnitEntry[15],
                nav1 : totalUnitEntry[16],prius : totalUnitEntry[17],rush : totalUnitEntry[18],vios : totalUnitEntry[19],yaris : totalUnitEntry[20],
                _86 : totalUnitEntry[21],calya : totalUnitEntry[22],sienta : totalUnitEntry[23],
                cbuTam : totalUnitEntry[24],cbuNonTam : totalUnitEntry[25],
        ]

        sbeRekening << [
                km : "         Labor ( X IDR 1,000)",
                agya : jasa[1],alphard : jasa[2],avanza : jasa[3],camry : jasa[4],corollaAltis : jasa[5],
                dyna : jasa[6],etiosValco :jasa[7] ,FJCruiser : jasa[8],fortuner : jasa[9],hiace : jasa[10],
                hilux : jasa[11],kiangKapsul : jasa[12],kijangInnova :jasa[13] ,landCruiser : jasa[14],limo : jasa[15],
                nav1 : jasa[16],prius : jasa[17],rush : jasa[18],vios : jasa[19],yaris : jasa[20],
                _86 : jasa[21],calya : jasa[22],sienta : jasa[23],
                cbuTam : jasa[24],cbuNonTam : jasa[25],
        ]
        sbeRekening << [
                km : "         Parts TOYOTA & Oli TMO ( X IDR 1,000)",
                agya : partsToyota[1],alphard : partsToyota[2],avanza : partsToyota[3],camry : partsToyota[4],corollaAltis : partsToyota[5],
                dyna : partsToyota[6],etiosValco :partsToyota[7] ,FJCruiser : partsToyota[8],fortuner : partsToyota[9],hiace : partsToyota[10],
                hilux : partsToyota[11],kiangKapsul : partsToyota[12],kijangInnova :partsToyota[13] ,landCruiser : partsToyota[14],limo : partsToyota[15],
                nav1 : partsToyota[16],prius : partsToyota[17],rush : partsToyota[18],vios : partsToyota[19],yaris : partsToyota[20],
                _86 : partsToyota[21],calya : partsToyota[22],sienta : partsToyota[23],
                cbuTam : partsToyota[24],cbuNonTam : partsToyota[25],
        ]
        sbeRekening << [
                km : "         Oli / Bahan Non TOYOTA ( X IDR 1,000)",
                agya : oli[1],alphard : oli[2],avanza : oli[3],camry : oli[4],corollaAltis : oli[5],
                dyna : oli[6],etiosValco :oli[7] ,FJCruiser : oli[8],fortuner : oli[9],hiace : oli[10],
                hilux : oli[11],kiangKapsul : oli[12],kijangInnova :oli[13] ,landCruiser : oli[14],limo : oli[15],
                nav1 : oli[16],prius : oli[17],rush : oli[18],vios : oli[19],yaris : oli[20],
                _86 : oli[21],calya : oli[22],sienta : oli[23],
                cbuTam : oli[24],cbuNonTam : oli[25],
        ]
        sbeRekening << [
                km : "         Pekerjaan Luar ( X IDR 1,000)",
                agya : sublet[1],alphard : sublet[2],avanza : sublet[3],camry : sublet[4],corollaAltis : sublet[5],
                dyna : sublet[6],etiosValco :sublet[7] ,FJCruiser : sublet[8],fortuner : sublet[9],hiace : sublet[10],
                hilux : sublet[11],kiangKapsul : sublet[12],kijangInnova :sublet[13] ,landCruiser : sublet[14],limo : sublet[15],
                nav1 : sublet[16],prius : sublet[17],rush : sublet[18],vios : sublet[19],yaris : sublet[20],
                _86 : sublet[21],calya : sublet[22],sienta : sublet[23],
                cbuTam : sublet[24],cbuNonTam : sublet[25],
        ]
        sbeRekening << [
                km : "         Sub Total Revenue CPUS",
                agya : subTotalRevenue[1],alphard : subTotalRevenue[2],avanza : subTotalRevenue[3],camry : subTotalRevenue[4],corollaAltis : subTotalRevenue[5],
                dyna : subTotalRevenue[6],etiosValco :subTotalRevenue[7] ,FJCruiser : subTotalRevenue[8],fortuner : subTotalRevenue[9],hiace : subTotalRevenue[10],
                hilux : subTotalRevenue[11],kiangKapsul : subTotalRevenue[12],kijangInnova :subTotalRevenue[13] ,landCruiser : subTotalRevenue[14],limo : subTotalRevenue[15],
                nav1 : subTotalRevenue[16],prius : subTotalRevenue[17],rush : subTotalRevenue[18],vios : subTotalRevenue[19],yaris : subTotalRevenue[20],
                _86 : subTotalRevenue[21],calya : subTotalRevenue[22],sienta : subTotalRevenue[23],
                cbuTam : subTotalRevenue[24],cbuNonTam : subTotalRevenue[25],
        ]
        reportData << [
                sbe : sbeUnit,
                sbeRekening : sbeRekening,
                companyDealer : companyDealer,
                periode : "Periode " + periode
        ]

        def reportDef = new JasperReportDef(name:'lapUnitPerModelGr.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)

        def file = File.createTempFile("lapUnitPerModelGr_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def previewDataBP(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        def jenis = "BP"
        if(params.jenis=="General Repair"){
            jenis = "GR"
        }
        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params?.tahun as int,(params?.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = []
        String bulanParam =  params?.tahun + "-" + params?.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params?.tahun + "-" + params?.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        def sbeUnit = []
        def sbeRekening = []

        def pelangganUmum = [], pelangganAsuransiTI = [], pelangganAsuransiNTI = [], totalPelanggan = []
        def warranty = [],rtj = [], subTotalNonCPUS = [], totalUnitService = []
        def rusakRingan = [],rusakSedang = [],rusakBerat = [], totalJenisKerusakan = []

        def labor = [], partsToyota = [],bahan = [], sublet = [], subTotalRevenue = []

        def inv = InvoiceT701.createCriteria().list {
            ge("t701TglJamInvoice", tgl)
            lt("t701TglJamInvoice", tgl2)
            eq("companyDealer",companyDealer)
            ilike("t701Tipe","%1%");
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            order("t701TglJamInvoice","asc")
        }
        def mobil = ["-","Agya","Alphard","Avanza","Camry","Corolla Altis","Dyna","Etios","FJCruiser","Fortuner","Hiace","Hilux","Kijang Kapsul","Kijang Innova","Land Cruiser","Limo","Nav","Prius","Rush","Vios","Yaris","86","Calya","Sienta","CBU TAM","CBU non TAM"]
        resetData(pelangganUmum)
        resetData(pelangganAsuransiTI)
        resetData(pelangganAsuransiNTI)
        resetData(totalPelanggan)
        resetData(warranty)
        resetData(rtj)
        resetData(subTotalNonCPUS)
        resetData(totalUnitService)
        resetData(labor)
        resetData(partsToyota)
        resetData(bahan)
        resetData(sublet)
        resetData(subTotalRevenue)

        inv.each {
            def invoice = it
            def bm =  it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel

            for(int i=1;i<=25;i++){
                if(bm?.toUpperCase()?.contains(mobil[i]?.toUpperCase())){
                    if(invoice?.reception?.staTwc){
                        warranty[i] ++
                    }else{
                        def cari = VendorAsuransi.findByM193NamaAndM193AlamatAndStaDel(invoice?.t701Customer,invoice?.t701Alamat,"0")
                        if(cari){
                            if(cari?.m193Nama?.toUpperCase()?.contains("TOYOTA")){
                                pelangganAsuransiTI[i]=pelangganAsuransiTI[i]+1
                            }else{
                                pelangganAsuransiNTI[i]=pelangganAsuransiNTI[i]+1
                            }
                        }else{
                            pelangganUmum[i]=pelangganUmum[i]+1
                        }
                    }
                    def partInv = PartInv.findAllByInvoice(it)
                    partInv.each {
                        def klas = KlasifikasiGoods.findByGoods(it.goods)
                        if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                            bahan[i] += (it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)
                        }else if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("PARTS TOYOTA")){
                            partsToyota[i] += (it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)
                        }
                    }
                    labor[i] += it.t701JasaRp
                    sublet[i] += (it.t701SubletRp?it?.t701SubletRp:0)
                    totalPelanggan[i] = pelangganUmum[i]+pelangganAsuransiTI[i]+pelangganAsuransiNTI[i]
                    subTotalNonCPUS[i] = warranty[i]+rtj[i]
                }
            }

            for(int i=1;i<=25;i++){
                totalUnitService[i] = totalPelanggan[i] + subTotalNonCPUS[i]
                subTotalRevenue[i] = labor[i] + partsToyota[i]+ bahan[i] + sublet[i]
            }
        }
        validasiData(pelangganUmum)
        validasiData(pelangganAsuransiTI)
        validasiData(pelangganAsuransiNTI)
        validasiData(totalPelanggan)
        validasiData(warranty)
        validasiData(rtj)
        validasiData(subTotalNonCPUS)
        validasiData(totalUnitService)
        validasiData(labor)
        validasiData(partsToyota)
        validasiData(bahan)
        validasiData(sublet)
        validasiData(subTotalRevenue)
        sbeUnit << [
                km : "    CPUS",
                agya : "",alphard : "",avanza : "",camry : "",corollaAltis : "",
                dyna : "",etiosValco : "",FJCruiser : "",fortuner : "",hiace : "",
                hilux : "",kiangKapsul : "",kijangInnova :"",landCruiser : "",limo : "",
                nav1 : "",prius : "",rush : "",vios : "",yaris : "",
                _86 : "",calya : "",sienta : "",
                cbuTam : "",cbuNonTam : ""
        ]
        sbeUnit << [
                km : "        JENIS PELANGGAN",
                agya : "",alphard : "",avanza : "",camry : "",corollaAltis : "",
                dyna : "",etiosValco : "",FJCruiser : "",fortuner : "",hiace : "",
                hilux : "",kiangKapsul : "",kijangInnova :"",landCruiser : "",limo : "",
                nav1 : "",prius : "",rush : "",vios : "",yaris : "",
                _86 : "",calya : "",sienta : "",
                cbuTam : "",cbuNonTam : ""
        ]
        sbeUnit << [
                km : "            Pelanggan Umum",
                agya : pelangganUmum[1],alphard : pelangganUmum[2],avanza : pelangganUmum[3],camry : pelangganUmum[4],corollaAltis : pelangganUmum[5],
                dyna : pelangganUmum[6],etiosValco :pelangganUmum[7] ,FJCruiser : pelangganUmum[8],fortuner : pelangganUmum[9],hiace : pelangganUmum[10],
                hilux : pelangganUmum[11],kiangKapsul : pelangganUmum[12],kijangInnova :pelangganUmum[13] ,landCruiser : pelangganUmum[14],limo : pelangganUmum[15],
                nav1 : pelangganUmum[16],prius : pelangganUmum[17],rush : pelangganUmum[18],vios : pelangganUmum[19],yaris : pelangganUmum[20],
                _86 : pelangganUmum[21],calya : pelangganUmum[22],sienta : pelangganUmum[23],
                cbuTam : pelangganUmum[24],cbuNonTam : pelangganUmum[25]
        ]
        sbeUnit << [
                km : "            Pelanggan Asuransi Toyota Insurance (TI)",
                agya : pelangganAsuransiTI[1],alphard : pelangganAsuransiTI[2],avanza : pelangganAsuransiTI[3],camry : pelangganAsuransiTI[4],corollaAltis : pelangganAsuransiTI[5],
                dyna : pelangganAsuransiTI[6],etiosValco :pelangganAsuransiTI[7] ,FJCruiser : pelangganAsuransiTI[8],fortuner : pelangganAsuransiTI[9],hiace : pelangganAsuransiTI[10],
                hilux : pelangganAsuransiTI[11],kiangKapsul : pelangganAsuransiTI[12],kijangInnova :pelangganAsuransiTI[13] ,landCruiser : pelangganAsuransiTI[14],limo : pelangganAsuransiTI[15],
                nav1 : pelangganAsuransiTI[16],prius : pelangganAsuransiTI[17],rush : pelangganAsuransiTI[18],vios : pelangganAsuransiTI[19],yaris : pelangganAsuransiTI[20],
                _86 : pelangganAsuransiTI[21],calya : pelangganAsuransiTI[22],sienta : pelangganAsuransiTI[23],
                cbuTam : pelangganAsuransiTI[24],cbuNonTam : pelangganAsuransiTI[25]
        ]
        sbeUnit << [
                km : "            Pelanggan Asuransi Toyota Non Insurance (NTI)",
                agya : pelangganAsuransiNTI[1],alphard : pelangganAsuransiNTI[2],avanza : pelangganAsuransiNTI[3],camry : pelangganAsuransiNTI[4],corollaAltis : pelangganAsuransiNTI[5],
                dyna : pelangganAsuransiNTI[6],etiosValco :pelangganAsuransiNTI[7] ,FJCruiser : pelangganAsuransiNTI[8],fortuner : pelangganAsuransiNTI[9],hiace : pelangganAsuransiNTI[10],
                hilux : pelangganAsuransiNTI[11],kiangKapsul : pelangganAsuransiNTI[12],kijangInnova :pelangganAsuransiNTI[13] ,landCruiser : pelangganAsuransiNTI[14],limo : pelangganAsuransiNTI[15],
                nav1 : pelangganAsuransiNTI[16],prius : pelangganAsuransiNTI[17],rush : pelangganAsuransiNTI[18],vios : pelangganAsuransiNTI[19],yaris : pelangganAsuransiNTI[20],
                _86 : pelangganAsuransiNTI[21],calya : pelangganAsuransiNTI[22],sienta : pelangganAsuransiNTI[23],
                cbuTam : pelangganAsuransiNTI[24],cbuNonTam : pelangganAsuransiNTI[25]
        ]
        sbeUnit << [
                km : "            Total Pelanggan",
                agya : totalPelanggan[1],alphard : totalPelanggan[2],avanza : totalPelanggan[3],camry : totalPelanggan[4],corollaAltis : totalPelanggan[5],
                dyna : totalPelanggan[6],etiosValco :totalPelanggan[7] ,FJCruiser : totalPelanggan[8],fortuner : totalPelanggan[9],hiace : totalPelanggan[10],
                hilux : totalPelanggan[11],kiangKapsul : totalPelanggan[12],kijangInnova :totalPelanggan[13] ,landCruiser : totalPelanggan[14],limo : totalPelanggan[15],
                nav1 : totalPelanggan[16],prius : totalPelanggan[17],rush : totalPelanggan[18],vios : totalPelanggan[19],yaris : totalPelanggan[20],
                _86 : totalPelanggan[21],calya : totalPelanggan[22],sienta : totalPelanggan[23],
                cbuTam : totalPelanggan[24],cbuNonTam : totalPelanggan[25]
        ]

        sbeUnit << [
                km : "    NON CPUS",
                agya : "",alphard : "",avanza : "",camry : "",corollaAltis : "",
                dyna : "",etiosValco : "",FJCruiser : "",fortuner : "",hiace : "",
                hilux : "",kiangKapsul : "",kijangInnova :"",landCruiser : "",limo : "",
                nav1 : "",prius : "",rush : "",vios : "",yaris : "",
                _86 : "",calya : "",sienta : "",cbuTam : "",cbuNonTam : ""
        ]
        sbeUnit << [
                km : "            Warranty",
                agya : warranty[1],alphard : warranty[2],avanza : warranty[3],camry : warranty[4],corollaAltis : warranty[5],
                dyna : warranty[6],etiosValco :warranty[7] ,FJCruiser : warranty[8],fortuner : warranty[9],hiace : warranty[10],
                hilux : warranty[11],kiangKapsul : warranty[12],kijangInnova :warranty[13] ,landCruiser : warranty[14],limo : warranty[15],
                nav1 : warranty[16],prius : warranty[17],rush : warranty[18],vios : warranty[19],yaris : warranty[20],
                _86 : warranty[21],calya : warranty[22],sienta : warranty[23],
                cbuTam : warranty[24],cbuNonTam : warranty[25]
        ]
        sbeUnit << [
                km : "            RTJ",
                agya : rtj[1],alphard : rtj[2],avanza : rtj[3],camry : rtj[4],corollaAltis : rtj[5],
                dyna : rtj[6],etiosValco :rtj[7] ,FJCruiser : rtj[8],fortuner : rtj[9],hiace : rtj[10],
                hilux : rtj[11],kiangKapsul : rtj[12],kijangInnova :rtj[13] ,landCruiser : rtj[14],limo : rtj[15],
                nav1 : rtj[16],prius : rtj[17],rush : rtj[18],vios : rtj[19],yaris : rtj[20],
                _86 : rtj[21],calya : rtj[22],sienta : rtj[23],
                cbuTam : rtj[24],cbuNonTam : rtj[25]
        ]
        sbeUnit << [
                km : "            Sub Total Non CPUS",
                agya : subTotalNonCPUS[1],alphard : subTotalNonCPUS[2],avanza : subTotalNonCPUS[3],camry : subTotalNonCPUS[4],corollaAltis : subTotalNonCPUS[5],
                dyna : subTotalNonCPUS[6],etiosValco :subTotalNonCPUS[7] ,FJCruiser : subTotalNonCPUS[8],fortuner : subTotalNonCPUS[9],hiace : subTotalNonCPUS[10],
                hilux : subTotalNonCPUS[11],kiangKapsul : subTotalNonCPUS[12],kijangInnova :subTotalNonCPUS[13] ,landCruiser : subTotalNonCPUS[14],limo : subTotalNonCPUS[15],
                nav1 : subTotalNonCPUS[16],prius : subTotalNonCPUS[17],rush : subTotalNonCPUS[18],vios : subTotalNonCPUS[19],yaris : subTotalNonCPUS[20],
                _86 : subTotalNonCPUS[21],calya : subTotalNonCPUS[22],sienta : subTotalNonCPUS[23],
                cbuTam : subTotalNonCPUS[24],cbuNonTam : subTotalNonCPUS[25]
        ]
        sbeUnit << [
                km : "    Total Unit Service",
                agya : totalUnitService[1],alphard : totalUnitService[2],avanza : totalUnitService[3],camry : totalUnitService[4],corollaAltis : totalUnitService[5],
                dyna : totalUnitService[6],etiosValco :totalUnitService[7] ,FJCruiser : totalUnitService[8],fortuner : totalUnitService[9],hiace : totalUnitService[10],
                hilux : totalUnitService[11],kiangKapsul : totalUnitService[12],kijangInnova :totalUnitService[13] ,landCruiser : totalUnitService[14],limo : totalUnitService[15],
                nav1 : totalUnitService[16],prius : totalUnitService[17],rush : totalUnitService[18],vios : totalUnitService[19],yaris : totalUnitService[20],
                _86 : totalUnitService[21],calya : totalUnitService[22],sienta : totalUnitService[23],
                cbuTam : totalUnitService[24],cbuNonTam : totalUnitService[25]
        ]
        sbeUnit << [
                km : "    Jenis Kerusakan",
                agya : "",alphard : "",avanza : "",camry : "",corollaAltis : "",
                dyna : "",etiosValco : "",FJCruiser : "",fortuner : "",hiace : "",
                hilux : "",kiangKapsul : "",kijangInnova :"",landCruiser : "",limo : "",
                nav1 : "",prius : "",rush : "",vios : "",yaris : "",
                _86 : "",calya : "",sienta : "",cbuTam : "",cbuNonTam : ""
        ]
        sbeUnit << [
                km : "            Rusak Ringan (1-3 Panel)",
                agya : rusakRingan[1],alphard : rusakRingan[2],avanza : rusakRingan[3],camry : rusakRingan[4],corollaAltis : rusakRingan[5],
                dyna : rusakRingan[6],etiosValco :rusakRingan[7] ,FJCruiser : rusakRingan[8],fortuner : rusakRingan[9],hiace : rusakRingan[10],
                hilux : rusakRingan[11],kiangKapsul : rusakRingan[12],kijangInnova :rusakRingan[13] ,landCruiser : rusakRingan[14],limo : rusakRingan[15],
                nav1 : rusakRingan[16],prius : rusakRingan[17],rush : rusakRingan[18],vios : rusakRingan[19],yaris : rusakRingan[20],
                _86 : rusakRingan[21],calya : rusakRingan[22],sienta : rusakRingan[23],
                cbuTam : rusakRingan[24],cbuNonTam : rusakRingan[25]
        ]
        sbeUnit << [
                km : "            Rusak Sedang (4-7 Panel)",
                agya : rusakSedang[1],alphard : rusakSedang[2],avanza : rusakSedang[3],camry : rusakSedang[4],corollaAltis : rusakSedang[5],
                dyna : rusakSedang[6],etiosValco :rusakSedang[7] ,FJCruiser : rusakSedang[8],fortuner : rusakSedang[9],hiace : rusakSedang[10],
                hilux : rusakSedang[11],kiangKapsul : rusakSedang[12],kijangInnova :rusakSedang[13] ,landCruiser : rusakSedang[14],limo : rusakSedang[15],
                nav1 : rusakSedang[16],prius : rusakSedang[17],rush : rusakSedang[18],vios : rusakSedang[19],yaris : rusakSedang[20],
                _86 : rusakSedang[21],calya : rusakSedang[22],sienta : rusakSedang[23],
                cbuTam : rusakSedang[24],cbuNonTam : rusakSedang[25]
        ]
        sbeUnit << [
                km : "            Rusak Berat (8 Up Panel)",
                agya : rusakBerat[1],alphard : rusakBerat[2],avanza : rusakBerat[3],camry : rusakBerat[4],corollaAltis : rusakBerat[5],
                dyna : rusakBerat[6],etiosValco :rusakBerat[7] ,FJCruiser : rusakBerat[8],fortuner : rusakBerat[9],hiace : rusakBerat[10],
                hilux : rusakBerat[11],kiangKapsul : rusakBerat[12],kijangInnova :rusakBerat[13] ,landCruiser : rusakBerat[14],limo : rusakBerat[15],
                nav1 : rusakBerat[16],prius : rusakBerat[17],rush : rusakBerat[18],vios : rusakBerat[19],yaris : rusakBerat[20],
                _86 : rusakBerat[21],calya : rusakBerat[22],sienta : rusakBerat[23],
                cbuTam : rusakBerat[24],cbuNonTam : rusakBerat[25]
        ]
        sbeUnit << [
                km : "     Total Jenis Kerusakan",
                agya : totalJenisKerusakan[1],alphard : totalJenisKerusakan[2],avanza : totalJenisKerusakan[3],camry : totalJenisKerusakan[4],corollaAltis : totalJenisKerusakan[5],
                dyna : totalJenisKerusakan[6],etiosValco :totalJenisKerusakan[7] ,FJCruiser : totalJenisKerusakan[8],fortuner : totalJenisKerusakan[9],hiace : totalJenisKerusakan[10],
                hilux : totalJenisKerusakan[11],kiangKapsul : totalJenisKerusakan[12],kijangInnova :totalJenisKerusakan[13] ,landCruiser : totalJenisKerusakan[14],limo : totalJenisKerusakan[15],
                nav1 : totalJenisKerusakan[16],prius : totalJenisKerusakan[17],rush : totalJenisKerusakan[18],vios : totalJenisKerusakan[19],yaris : totalJenisKerusakan[20],
                _86 : totalJenisKerusakan[21],calya : totalJenisKerusakan[22],sienta : totalJenisKerusakan[23],
                cbuTam : totalJenisKerusakan[24],cbuNonTam : totalJenisKerusakan[25]
        ]
        sbeRekening  << [
                km : "    CPUS",
                agya : "",alphard : "",avanza : "",camry : "",corollaAltis : "",
                dyna : "",etiosValco : "",FJCruiser : "",fortuner : "",hiace : "",
                hilux : "",kiangKapsul : "",kijangInnova :"",landCruiser : "",limo : "",
                nav1 : "",prius : "",rush : "",vios : "",yaris : "",
                _86 : "",calya : "",sienta : "",cbuTam : "",cbuNonTam : ""
        ]
        sbeRekening << [
                km : "         Labor",
                agya : labor[1],alphard : labor[2],avanza : labor[3],camry : labor[4],corollaAltis : labor[5],
                dyna : labor[6],etiosValco :labor[7] ,FJCruiser : labor[8],fortuner : labor[9],hiace : labor[10],
                hilux : labor[11],kiangKapsul : labor[12],kijangInnova :labor[13] ,landCruiser : labor[14],limo : labor[15],
                nav1 : labor[16],prius : labor[17],rush : labor[18],vios : labor[19],yaris : labor[20],
                _86 : labor[21],calya : labor[22],sienta : labor[23],
                cbuTam : labor[24],cbuNonTam : labor[25]
        ]
        sbeRekening << [
                km : "         Parts TOYOTA",
                agya : partsToyota[1],alphard : partsToyota[2],avanza : partsToyota[3],camry : partsToyota[4],corollaAltis : partsToyota[5],
                dyna : partsToyota[6],etiosValco :partsToyota[7] ,FJCruiser : partsToyota[8],fortuner : partsToyota[9],hiace : partsToyota[10],
                hilux : partsToyota[11],kiangKapsul : partsToyota[12],kijangInnova :partsToyota[13] ,landCruiser : partsToyota[14],limo : partsToyota[15],
                nav1 : partsToyota[16],prius : partsToyota[17],rush : partsToyota[18],vios : partsToyota[19],yaris : partsToyota[20],
                _86 : partsToyota[21],calya : partsToyota[22],sienta : partsToyota[23],
                cbuTam : partsToyota[24],cbuNonTam : partsToyota[25]
        ]
        sbeRekening << [
                km : "         Bahan Non Toyota",
                agya : bahan[1],alphard : bahan[2],avanza : bahan[3],camry : bahan[4],corollaAltis : bahan[5],
                dyna : bahan[6],etiosValco :bahan[7] ,FJCruiser : bahan[8],fortuner : bahan[9],hiace : bahan[10],
                hilux : bahan[11],kiangKapsul : bahan[12],kijangInnova :bahan[13] ,landCruiser : bahan[14],limo : bahan[15],
                nav1 : bahan[16],prius : bahan[17],rush : bahan[18],vios : bahan[19],yaris : bahan[20],
                _86 : bahan[21],calya : bahan[22],sienta : bahan[23],
                cbuTam : bahan[24],cbuNonTam : bahan[25]
        ]
        sbeRekening << [
                km : "         Pekerjaan",
                agya : sublet[1],alphard : sublet[2],avanza : sublet[3],camry : sublet[4],corollaAltis : sublet[5],
                dyna : sublet[6],etiosValco :sublet[7] ,FJCruiser : sublet[8],fortuner : sublet[9],hiace : sublet[10],
                hilux : sublet[11],kiangKapsul : sublet[12],kijangInnova :sublet[13] ,landCruiser : sublet[14],limo : sublet[15],
                nav1 : sublet[16],prius : sublet[17],rush : sublet[18],vios : sublet[19],yaris : sublet[20],
                _86 : sublet[21],calya : sublet[22],sienta : sublet[23],
                cbuTam : sublet[24],cbuNonTam : sublet[25]
        ]
        sbeRekening << [
                km : "         Sub Total Revenue CPUS",
                agya : subTotalRevenue[1],alphard : subTotalRevenue[2],avanza : subTotalRevenue[3],camry : subTotalRevenue[4],corollaAltis : subTotalRevenue[5],
                dyna : subTotalRevenue[6],etiosValco :subTotalRevenue[7] ,FJCruiser : subTotalRevenue[8],fortuner : subTotalRevenue[9],hiace : subTotalRevenue[10],
                hilux : subTotalRevenue[11],kiangKapsul : subTotalRevenue[12],kijangInnova :subTotalRevenue[13] ,landCruiser : subTotalRevenue[14],limo : subTotalRevenue[15],
                nav1 : subTotalRevenue[16],prius : subTotalRevenue[17],rush : subTotalRevenue[18],vios : subTotalRevenue[19],yaris : subTotalRevenue[20],
                _86 : subTotalRevenue[21],calya : subTotalRevenue[22],sienta : subTotalRevenue[23],
                cbuTam : subTotalRevenue[24],cbuNonTam : subTotalRevenue[25]
        ]
        reportData << [
                sbe : sbeUnit,
                sbeRekening : sbeRekening,
                companyDealer : companyDealer,
                periode : "Periode " + periode
        ]

        def reportDef = new JasperReportDef(name:'lapUnitPerModelBp.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("lapUnitPerModelBp_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}