package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.maintable.Customer
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class NeracaController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
    def index() {}
    def previewData(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params.tahun as int,(params.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = []
        def reportData2 = []

        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        def dataKiri = []
        def dataKanan = []
        def results = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","1%")
                like("accountNumber","2%")
                like("accountNumber","3%")
            }
            order("accountNumber","asc")
        }
        def sumTotalKiri = 0, totalKiri = 0.00

        if((params.bulan as int) < 10){
            params.bulan = "0" + params.bulan
        }
        results.each { an ->
            sumTotalKiri = 0.00
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    order("accountNumber","asc")
                }
                order("subType")
                order("subLedger")
            }
            if(data.size() > 0){
                if(data.size()>=1 && params.level == '5'){
                    data.each {
                        def subLedger = ""
                        if(it?.subType?.description?.toUpperCase()=="CUSTOMER"){
                            subLedger = HistoryCustomer.findById(it.subLedger as Long) ? " a.n "+ HistoryCustomer.findById(it.subLedger as Long)?.fullNama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="VENDOR"){
                            subLedger = Vendor.findById(it.subLedger as Long) ? " a.n "+ Vendor.findById(it.subLedger as Long).m121Nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="ASURANSI"){
                            subLedger = VendorAsuransi.findById(it.subLedger as Long) ? " a.n "+ VendorAsuransi.findById(it.subLedger as Long)?.m193Nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="KARYAWAN"){
                            subLedger = Karyawan.findById(it.subLedger as Long) ? " a.n "+ Karyawan.findById(it.subLedger as Long).nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="CABANG"){
                            subLedger = CompanyDealer.findById(it.subLedger as Long) ? " a.n "+ CompanyDealer.findById(it.subLedger as Long).m011NamaWorkshop : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                            subLedger = Company.findById(it.subLedger as Long) ? " a.n "+ Company.findById(it.subLedger as Long).namaPerusahaan : " a.n "+ null
                        }
                        sumTotalKiri  += it.endingBalance
                        if(it.endingBalance!=0){
                            dataKiri << [
                                    rekeningNumber : an?.accountNumber,
                                    rekeningNama : an?.accountName?.toUpperCase() + subLedger,
                                    rekeningValue : it.endingBalance < 0 ? "(" + konversi.toRupiah((it.endingBalance * (-1))) + ")": konversi.toRupiah(it.endingBalance),
                            ]
                        }
                    }
                }else{
                    data.each{
                        sumTotalKiri  += it.endingBalance
                    }
                    if(sumTotalKiri!=0){
                        dataKiri << [
                            rekeningNumber : an?.accountNumber,
                            rekeningNama : an?.accountName,
                            rekeningValue : sumTotalKiri < 0 ? "(" + konversi.toRupiah((sumTotalKiri * (-1))) + ")": konversi.toRupiah(sumTotalKiri),
                        ]
                    }
                }
                totalKiri += sumTotalKiri
            }
        }

        def results2 = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","4%")
                like("accountNumber","5%")
                like("accountNumber","6%")
                like("accountNumber","7%")
                like("accountNumber","8%")
            }
            order("accountNumber","asc")
        }

        def sumTotalKanan = 0.00, totalKanan = 0.00
        results2.each { an ->
            sumTotalKanan = 0.00
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    order("accountNumber","asc")
                }
                order("subType")
                order("subLedger")
            }
            if(data.size() > 0){
                if(data.size()>=1 && params.level == '5'){
                    data.each {
                        def subLedger = ""
                        if(it?.subType?.description?.toUpperCase()=="CUSTOMER"){
                            subLedger = HistoryCustomer.findById(it.subLedger as Long) ? " a.n "+ HistoryCustomer.findById(it.subLedger as Long)?.fullNama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="VENDOR"){
                            subLedger = Vendor.findById(it.subLedger as Long) ? " a.n "+ Vendor.findById(it.subLedger as Long).m121Nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="ASURANSI"){
                            subLedger = VendorAsuransi.findById(it.subLedger as Long) ? " a.n "+ VendorAsuransi.findById(it.subLedger as Long)?.m193Nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="KARYAWAN"){
                            subLedger = Karyawan.findById(it.subLedger as Long) ? " a.n "+ Karyawan.findById(it.subLedger as Long).nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="CABANG"){
                            subLedger = CompanyDealer.findById(it.subLedger as Long) ? " a.n "+ CompanyDealer.findById(it.subLedger as Long).m011NamaWorkshop : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                            subLedger = Company.findById(it.subLedger as Long) ? " a.n "+ Company.findById(it.subLedger as Long).namaPerusahaan : " a.n "+ null
                        }

                        def saldoAkhir = (it?.endingBalance * -1)
                        sumTotalKanan += saldoAkhir
                        if(saldoAkhir!=0){
                            dataKanan << [
                                    rekeningNumber : an?.accountNumber,
                                    rekeningNama : an?.accountName?.toUpperCase() + subLedger,
                                    rekeningValue : saldoAkhir < 0 ? "(" + konversi.toRupiah((saldoAkhir * -1)) + ")" : konversi.toRupiah(saldoAkhir),
                            ]
                        }
                    }
                }else{
                    data.each{
                        def saldoAkhir = (it?.endingBalance * -1)
                        sumTotalKanan += saldoAkhir
                    }

                    dataKanan << [
                            rekeningNumber : an?.accountNumber,
                            rekeningNama : an?.accountName,
                            rekeningValue : sumTotalKanan < 0 ? "(" + konversi.toRupiah((sumTotalKanan * -1)) + ")" : konversi.toRupiah(sumTotalKanan),
                    ]
                }
                totalKanan += sumTotalKanan

            }
        }

//        totalKanan = (totalKanan < 0 ? (totalKanan * -1) : totalKanan)
        reportData << [
                dataKiri : dataKiri,
                dataKanan : dataKanan,
                totalKiri : totalKiri < 0 ? "(" + konversi.toRupiah((totalKiri * (-1))) + ")" : konversi.toRupiah(totalKiri),
                totalKanan : totalKanan < 0 ? "(" + konversi.toRupiah((totalKanan * (-1))) + ")" : konversi.toRupiah(totalKanan),
                companyDealer : companyDealer,
                perTanggal : "Per Tanggal " + tgl2.format("dd MMMM yyyy").toString(),
                judul : "NERACA"
        ]

        def reportDef = new JasperReportDef(name:'neraca.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportData2 = previewData2(params)
        def reportDef2 = new JasperReportDef(name:'f_neracaKombos.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData2
        )
        reportDefList.add(reportDef)
        reportDefList.add(reportDef2)

        def file = File.createTempFile("Neraca_"+params.bulan+""+params.tahun+"_LVL"+params.level+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def previewData2(def params){

        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        Calendar cal = Calendar.getInstance()
        cal.set(params.tahun as int,(params.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = new ArrayList();

        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()

        def results = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","1%")
                like("accountNumber","2%")
                like("accountNumber","3%")
                like("accountNumber","4%")
                like("accountNumber","5%")
                like("accountNumber","6%")
                like("accountNumber","7%")
                like("accountNumber","8%")
            }
            order("accountNumber","asc")
        }
        def saldoAwal_K = 0.00, saldoAwal_D = 0.00,T_saldoAwal_K = 0.00, T_saldoAwal_D = 0.00
        def saldoAkhir_K = 0.00, saldoAkhir_D = 0.00,T_saldoAkhir_K = 0.00, T_saldoAkhir_D = 0.00
        def mutasi_K = 0.00, mutasi_D = 0,T_mutasi_K = 0.00, T_mutasi_D = 0.00

        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        int count = 0
        results.each { an ->
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    like("accountNumber",an.accountNumber + "%")
                }
            }
            def subLedger = ""
            if(data.size() > 0){
                if(data.size()>=1 && params.level == '5'){
                    data.each {
                        subLedger = ""
                        saldoAwal_K = 0.00; saldoAwal_D = 0.00;
                        saldoAkhir_K = 0.00; saldoAkhir_D = 0.00
                        mutasi_K = 0.00; mutasi_D = 0;
                        count = count + 1
                        if(it?.subType?.description?.toUpperCase()=="CUSTOMER"){
                            subLedger = HistoryCustomer.findById(it.subLedger as Long) ? " a.n "+ HistoryCustomer.findById(it.subLedger as Long)?.fullNama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="VENDOR"){
                            subLedger = Vendor.findById(it.subLedger as Long) ? " a.n "+ Vendor.findById(it.subLedger as Long).m121Nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="ASURANSI"){
                            subLedger = VendorAsuransi.findById(it.subLedger as Long) ? " a.n "+ VendorAsuransi.findById(it.subLedger as Long)?.m193Nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="KARYAWAN"){
                            subLedger = Karyawan.findById(it.subLedger as Long) ? " a.n "+ Karyawan.findById(it.subLedger as Long).nama : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="CABANG"){
                            subLedger = CompanyDealer.findById(it.subLedger as Long) ? " a.n "+ CompanyDealer.findById(it.subLedger as Long).m011NamaWorkshop : " a.n "+ null
                        }else if(it?.subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                            subLedger = Company.findById(it.subLedger as Long) ? " a.n "+ Company.findById(it.subLedger as Long).namaPerusahaan : " a.n "+ null
                        }else{
                        }
                        mutasi_D = it.debitMutation
                        mutasi_K = it.creditMutation
                        T_mutasi_D += mutasi_D
                        T_mutasi_K += mutasi_K
                        if(it.startingBalance  < 0){
                            saldoAwal_K = it.startingBalance  * -1
                            T_saldoAwal_K+=saldoAwal_K
                        }else{
                            saldoAwal_D = it.startingBalance
                            T_saldoAwal_D+=saldoAwal_D
                        }

                        if(it.endingBalance  < 0){
                            saldoAkhir_K = it.endingBalance  * -1
                            T_saldoAkhir_K += saldoAkhir_K
                        }else{
                            saldoAkhir_D = it.endingBalance
                            T_saldoAkhir_D += saldoAkhir_D
                        }
                        def dataSS = [:]
                        dataSS.put("NO",count)
                        dataSS.put("COMPANY_DEALER",companyDealer)
                        dataSS.put("PERIODE",periode)
                        dataSS.put("NO_REKENING",an.accountNumber)
                        dataSS.put("NAMA_REKENING",an.accountName + subLedger)
                        dataSS.put("SALDO_AWAL_K",saldoAwal_K)
                        dataSS.put("SALDO_AWAL_D",saldoAwal_D)
                        dataSS.put("MUTASI_DEBET",mutasi_D)
                        dataSS.put("MUTASI_KREDIT",mutasi_K)
                        dataSS.put("SALDO_AKHIR_D",saldoAkhir_D)
                        dataSS.put("SALDO_AKHIR_K",saldoAkhir_K)
                        dataSS.put("TOT_SALDO_AWAL_K",T_saldoAwal_K)
                        dataSS.put("TOT_SALDO_AWAL_D",T_saldoAwal_D)
                        dataSS.put("TOT_MUTASI_DEBET",T_mutasi_D)
                        dataSS.put("TOT_MUTASI_KREDIT",T_mutasi_K)
                        dataSS.put("TOT_SALDO_AKHIR_D",T_saldoAkhir_D)
                        dataSS.put("TOT_SALDO_AKHIR_K",T_saldoAkhir_K)
                        reportData.add(dataSS)
                    }
                }else{
                    saldoAwal_K = 0.00; saldoAwal_D = 0.00;
                    saldoAkhir_K = 0.00; saldoAkhir_D = 0.00
                    mutasi_K = 0.00; mutasi_D = 0.00;
                    def sa = 0.00, si = 0.00
                    count = count + 1
                    data.each {
                        mutasi_D += it.debitMutation
                        mutasi_K += it.creditMutation
                        sa += it.startingBalance
                        si += it.endingBalance
                    }
                    T_mutasi_D += mutasi_D
                    T_mutasi_K += mutasi_K
                    if(sa  < 0){
                        saldoAwal_K += sa  * -1
                        T_saldoAwal_K+=saldoAwal_K
                    }else{
                        saldoAwal_D += sa
                        T_saldoAwal_D+=saldoAwal_D
                    }

                    if(si  < 0){
                        saldoAkhir_K += si * -1
                        T_saldoAkhir_K += saldoAkhir_K
                    }else{
                        saldoAkhir_D += si
                        T_saldoAkhir_D += saldoAkhir_D
                    }
                    def dataSS = [:]
                    dataSS.put("NO",count)
                    dataSS.put("COMPANY_DEALER",companyDealer)
                    dataSS.put("PERIODE",periode)
                    dataSS.put("NO_REKENING",an.accountNumber)
                    dataSS.put("NAMA_REKENING",an.accountName + subLedger)
                    dataSS.put("SALDO_AWAL_K",saldoAwal_K)
                    dataSS.put("SALDO_AWAL_D",saldoAwal_D)
                    dataSS.put("MUTASI_DEBET",mutasi_D)
                    dataSS.put("MUTASI_KREDIT",mutasi_K)
                    dataSS.put("SALDO_AKHIR_D",saldoAkhir_D)
                    dataSS.put("SALDO_AKHIR_K",saldoAkhir_K)
                    dataSS.put("TOT_SALDO_AWAL_K",T_saldoAwal_K)
                    dataSS.put("TOT_SALDO_AWAL_D",T_saldoAwal_D)
                    dataSS.put("TOT_MUTASI_DEBET",T_mutasi_D)
                    dataSS.put("TOT_MUTASI_KREDIT",T_mutasi_K)
                    dataSS.put("TOT_SALDO_AKHIR_D",T_saldoAkhir_D)
                    dataSS.put("TOT_SALDO_AKHIR_K",T_saldoAkhir_K)
                    reportData.add(dataSS)
                }

            }
        }
        return reportData
    }
}
