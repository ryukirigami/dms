package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Franc
import com.kombos.parts.GoodsHargaJual
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.Konversi
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class PendingPartController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    def pendingMasuk(){
        def jsonArray = JSON.parse(params.tahun)
        def returnsList = []
        String periode =  params.bulan+""+params.tahun
        List<JasperReportDef> reportDefList = []

        def reportData = calculatePendingMasuk(params)

        def reportDef = new JasperReportDef(name:'f_lapPendingPart.jasper',
                fileFormat:JasperExportFormat.XLS_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("PENDING_MASUK_PARTS("+session?.userCompanyDealerId+")_"+periode+"_",".xls")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def pendingKeluar(){
        def jsonArray = JSON.parse(params.tahun)
        def returnsList = []
        String periode =  params.bulan+""+params.tahun
        List<JasperReportDef> reportDefList = []

        def reportData = calculatePendingKeluar(params)

        def reportDef = new JasperReportDef(name:'f_lapPendingPart.jasper',
                fileFormat:JasperExportFormat.XLS_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("PENDING_KELUAR_PARTS("+session?.userCompanyDealerId+")_"+periode+"_",".xls")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculatePendingMasuk(def params){
        def companyDealer = session?.userCompanyDealer;
        def judul = "DAFTAR PENDING MASUK PARTS";
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def c = PartsRCP.createCriteria()
        String bulanParam =  params.tahun + "-" + params.bulan + "-1"
        Date tgl = new Date().parse('yyyy-MM-d',bulanParam.toString())
        def cariAkhir = new MonthlyBalanceController()
        String tgglAkhir = cariAkhir.tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-"+tgglAkhir
        Date tgl2 = new Date().parse('yyyy-MM-d',bulanParam2.toString())
        def periode = tgl2.format("MMMM yyyy").toUpperCase()
        def results = c.list{
            reception{
                eq("companyDealer",companyDealer)
                eq("staSave","0")
                eq("staDel", "0")
                isNull("t401StaInvoice")
                lt("t401TanggalWO",tgl2 + 1)
                order("t401NoWO","desc")
                order("t401NamaSA","asc")
            }
            ilike("staDel", "0")
            or{
                ilike("t403StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t403StaTambahKurang","%1%")
                        ilike("t403StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t403StaTambahKurang")
                    isNull("t403StaApproveTambahKurang")
                }
            }
        }

        int count = 0
        double total = 0
        results.each { p->
            def franc = Franc.findAllByM117NamaFrancIlike("%Parts%")
            def inv = InvoiceT701.createCriteria().list {
                eq("t701StaDel",'0')
                isNull('t701NoInv_Reff')
                isNull('t701StaApprovedReversal')
                lt("t701TglJamInvoice",tgl2 + 1)
                reception{
                    eq("id",p.receptionId)
                }
            }
            def klasifikasi = KlasifikasiGoods.findByFrancInListAndGoods(franc,p.goods)
            if(klasifikasi && p.t403Jumlah1.toInteger()>0 && inv.size()==0){
                def data = [:]
                count = count + 1
                data.put("companyDealer",companyDealer)
                data.put("periode",periode)
                data.put("judul",judul)
                data.put("noUrut",count)
                data.put("noSo",p.reception.t401NoWO)
                data.put("nopol",p.reception.historyCustomerVehicle?.fullNoPol)
                data.put("tglSo",p.reception.t401TanggalWO? p.reception.t401TanggalWO.format("dd/MM/yyyy") : "")
                data.put("namasa",p?.reception?.t401NamaSA.toString().toUpperCase())
                data.put("tglPemakaian",p.reception.t401TanggalWO? p.reception.t401TanggalWO.format("dd/MM/yyyy") : "")
                data.put("kode_parts",p.goods.m111ID)
                data.put("nama_parts",p.goods.m111Nama)
                data.put("jml",p.t403Jumlah1.toInteger())
                data.put("hpp",konversi.toRupiah(p?.t403HargaRp))
                data.put("jml_hpp",konversi.toRupiah(p?.t403HargaRp * p.t403Jumlah1.toInteger()))
                total +=  (p?.t403HargaRp * p.t403Jumlah1.toInteger())
                data.put("total",konversi.toRupiah(total))
                reportData.add(data)
            }
        }
        if(results.size()==0 || count==0){
            def data = [:]
            data.put("companyDealer",companyDealer)
            data.put("periode",periode)
            data.put("judul",judul)
            data.put("noUrut","1")
            data.put("noSo","")
            reportData.add(data)
        }
        return reportData

    }

    def calculatePendingKeluar(def params){
        def companyDealer = session?.userCompanyDealer;
        def judul = "DAFTAR PENDING KELUAR PARTS";
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def c = PartsRCP.createCriteria()
        String bulanParam =  params.tahun + "-" + params.bulan + "-1"
        Date tgl = new Date().parse('yyyy-MM-d',bulanParam.toString())
        def cariAkhir = new MonthlyBalanceController()
        String tgglAkhir = cariAkhir.tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-"+tgglAkhir
        Date tgl2 = new Date().parse('yyyy-MM-d',bulanParam2.toString())
        def periode = tgl2.format("MMMM yyyy").toUpperCase()
        def results = c.list{
            reception{
                eq("companyDealer",companyDealer)
                eq("staSave","0")
                eq("staDel", "0")
                isNull("t401StaInvoice")
                lt("t401TanggalWO",tgl2 + 1)
                order("t401NoWO","desc")
                order("t401NamaSA","asc")
            }
            ilike("staDel", "0")
            or{
                ilike("t403StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t403StaTambahKurang","%1%")
                        ilike("t403StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t403StaTambahKurang")
                    isNull("t403StaApproveTambahKurang")
                }
            }
        }

        int count = 0
        double total = 0
        results.each { p->
            def franc = Franc.findAllByM117NamaFrancIlike("%Parts%")
            def inv = InvoiceT701.createCriteria().list {
                ge("t701TglJamInvoice",tgl)
                lt("t701TglJamInvoice",tgl2 + 1)
                eq("t701StaDel",'0')
                isNull('t701NoInv_Reff')
                isNull('t701StaApprovedReversal')
                reception{
                    eq("id",p.receptionId)
                }
            }
            def klasifikasi = KlasifikasiGoods.findByFrancInListAndGoods(franc,p.goods)
            if(klasifikasi && p.t403Jumlah1.toInteger()>0 && inv.size()>0){
                def data = [:]
                count = count + 1
                data.put("companyDealer",companyDealer)
                data.put("periode",periode)
                data.put("judul",judul)
                data.put("noUrut",count)
                data.put("noSo",p.reception.t401NoWO)
                data.put("nopol",p.reception.historyCustomerVehicle?.fullNoPol)
                data.put("tglSo",p.reception.t401TanggalWO? p.reception.t401TanggalWO.format("dd/MM/yyyy") : "")
                data.put("namasa",p?.reception?.t401NamaSA.toString().toUpperCase())
                data.put("tglPemakaian",p.reception.t401TanggalWO? p.reception.t401TanggalWO.format("dd/MM/yyyy") : "")
                data.put("kode_parts",p.goods.m111ID)
                data.put("nama_parts",p.goods.m111Nama)
                data.put("jml",p.t403Jumlah1.toInteger())
                data.put("hpp",konversi.toRupiah(p?.t403HargaRp))
                data.put("jml_hpp",konversi.toRupiah(p?.t403HargaRp * p.t403Jumlah1.toInteger()))
                total +=  (p?.t403HargaRp * p.t403Jumlah1.toInteger())
                data.put("total",konversi.toRupiah(total))
                reportData.add(data)
            }
        }
        if(results.size()==0 || count==0){
            def data = [:]
            data.put("companyDealer",companyDealer)
            data.put("periode",periode)
            data.put("judul",judul)
            data.put("noUrut","1")
            data.put("noSo","")
            reportData.add(data)
        }
        return reportData

    }
}
