package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class AccountNumberController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def accountNumberService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render accountNumberService.datatablesList(params) as JSON
	}

	def create() {
		def result = accountNumberService.create(params)
        def parentAcc = null;
        def level = "";
        if(params?.id){
            def cari = AccountNumber?.get(params?.id?.toLong())
            parentAcc = cari;
        }
        if(!result.error)
            return [accountNumberInstance: result.accountNumberInstance, parent : parentAcc]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

    def createParent() {
        def result = accountNumberService.create(params)

        if(!result.error)
            return [accountNumberInstance: result.accountNumberInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

	def save() {
        if (params.isParent)
            params.level = 0
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session.userCompanyDealer
		def result = accountNumberService.save(params)
        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["AccountNumber", result.accountNumberInstance.id])
            redirect(action:'show', id: result.accountNumberInstance.id)
            return
        }

        render(view:'create', model:[accountNumberInstance: result.accountNumberInstance])
	}

	def show(Long id) {
		def result = accountNumberService.show(params)

		if(!result.error)
			return [ accountNumberInstance: result.accountNumberInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        println params
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def result = accountNumberService.show(params)

		if(!result.error)
			return [ accountNumberInstance: result.accountNumberInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = accountNumberService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["AccountNumber", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[accountNumberInstance: result.accountNumberInstance.attach()])
	}

	def delete() {
		def result = accountNumberService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["AccountNumber", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(AccountNumber, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
}
