package com.kombos.finance

import com.kombos.administrasi.Bank
import com.kombos.administrasi.ManPower
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.sec.shiro.User
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.generatecode.GenerateCodeService
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import com.kombos.utils.MoneyUtil
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.SimpleDateFormat

class PenerimaanLainLainController {

    private static int SHOW_BANK_LIST = 0
    private static int SHOW_ACCOUNT_NUMBER_LIST = 1
  private  static int SHOW_SUBTYPE_LIST  = 0
    def datatablesUtilService

    static allowedMethods = [insert: 'POST']
    static addPermissions = ['insert']
    def jasperService

    def index() {

    }

    def showDetails() {
        def ret = []
        switch (Integer.parseInt(params.type)) {
            case SHOW_ACCOUNT_NUMBER_LIST:
                if(params.noAkun && params.namaAkun){
                    ret = AccountNumber.findAllByAccountNameIlikeAndAccountNumberIlikeAndAccountMutationTypeAndStaDel("%"+params.namaAkun+"%","%"+params.noAkun+"%","MUTASI","0")
                }else if(params.namaAkun){
                    ret = AccountNumber.findAllByAccountNameIlikeAndAccountMutationTypeAndStaDel("%"+params.namaAkun+"%","MUTASI","0")
                }else if(params.noAkun){
                    ret = AccountNumber.findAllByAccountNumberIlikeAndAccountMutationTypeAndStaDel("%"+params.noAkun+"%","MUTASI","0")
                }else{
                    ret = AccountNumber.findAllByAccountMutationTypeAndStaDel("MUTASI","0")
                }
                break
            case SHOW_BANK_LIST:
                def bank = BankAccountNumber.createCriteria().list {eq("companyDealer",session?.userCompanyDealer);eq("staDel","0");bank{order("m702NamaBank")}}
                bank.each {data ->
                    ret << [
                            bankId: data.bank.id,
                            bankName: data.bank.m702NamaBank,
                            bankCabang: data.bank.m702Cabang,
                            accountNumber: data.accountNumber.accountNumber
                    ]
                }

                break

            case SHOW_SUBTYPE_LIST:
                def subtype = SubType.list()
                subtype.each {data ->
                    ret << [
                            id: data.id,
                            subtype: data.description

                    ]
                }

                break

        }

        render ret as JSON
    }

    def listSubtype(){
        def ret=[]
        switch (Integer.parseInt(params.type)) {
            case SHOW_SUBTYPE_LIST:
                def subtype = SubType.list()
                subtype.each {data ->
                    ret << [
                            id: data.id,
                            subtype: data.description

                    ]
                }

                break
        }
        render ret as JSON
    }

    def insert() {
        def ret = [:]
        def saldo = 0
        def journalCode = ""
        String isOperasional       = params.biayaOperasional
        def transactionCode        = new GenerateCodeService().generateTransactionCode()
        def transactionType        = TransactionType.findByTransactionType(params.transactionType as String)
        def transactionDate        = Date.parse("dd/MM/yyyy", params.transactionDate as String)
        def amount                 = params?.amount?.toString()?.toBigDecimal()
        def description            = params.description
        def biayaOperasional       = params.biayaOperasional
        def bank                   = params.bankId? Bank.findById(Integer.parseInt(params.bankId as String)) : null
        def bankAccountNumber      = params.bankId ? BankAccountNumber.findByBankAndCompanyDealer(bank,session?.userCompanyDealer) : null  //BankAccountNumber.findByAccountNumber(kasAccount)
        def transferDate           = params.transferData ? Date.parse("dd/mm/yyyy", params.transferDate as String) : null
        def dueDate                = params.dueDae? Date.parse("dd/mm/yyyy", params.dueDate as String) : null
        def docCategory            = DocumentCategory.findByDocumentCategory("DOCTL")
        def journalType = JournalType.findByJournalType("TJ")
        def journal     = new Journal()
        journalCode = new GenerateCodeService().generateJournalCode("TJ")
        journal.companyDealer= session.userCompanyDealer
        journal.journalCode= journalCode
        journal.journalDate= transactionDate
        journal.isOperasional= isOperasional
        journal.isSetoranBank= params?.setoranBank
        journal.isApproval= "0"
        journal.totalDebit=  amount
        journal.totalCredit= amount
        journal.description= description? description : ""
        journal.journalType= journalType
        journal.docNumber= journalCode
        journal.staDel= "0"
        journal.createdBy= org.apache.shiro.SecurityUtils.subject.principal.toString()
        journal.lastUpdProcess= "INSERT"
        journal.dateCreated= datatablesUtilService.syncTime()
        journal.lastUpdated=  datatablesUtilService.syncTime()
        boolean checkExist = new JournalController().checkBeforeInsert(journal?.journalCode,journal?.description,journal?.totalDebit,journal?.totalCredit);
        if(checkExist){
            ret.error = "DUPLICATE"
        }else{
            journal.save(flush: true, failOnError: true)

            journal?.errors?.each {
                println it
            }
            if (params.details) {
                def detailArray = JSON.parse(params.details)
                def rowCount    = 0
                detailArray.each {detail ->
                    def total       = detail?.total?.toString()?.toBigDecimal()
                    def subtype                = detail.subtype ? SubType.findByDescriptionIlike("%"+detail.subtype+"%") : null
                    def subdleger              = detail.subledger
                    def journalDetail = new JournalDetail(
                            journalTransactionType:journalType.journalType,
                            debitAmount: (detail.transactionType as String).equalsIgnoreCase("Kredit") ? 0 : total,
                            creditAmount: (detail.transactionType as String).equalsIgnoreCase("Kredit") ? total : 0,
                            staDel: "0",
                            accountNumber: AccountNumber.findByAccountNumber(detail.accountNumber),
                            subType: subtype,
                            journal: journal,
                            createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            lastUpdProcess: "INSERT",
                            subLedger: subdleger,
                            description: detail?.description,
                            dateCreated: datatablesUtilService.syncTime(),
                            lastUpdated:  datatablesUtilService.syncTime()

                    ).save(flush: true, failOnError: true)

                    journalDetail?.errors?.each {
                        println it

                    }

                    if (journalDetail.hasErrors()) {
                        throw new Exception("${journal.errors}")
                    } else {
                        rowCount++
                    }
                }
                ret.total = rowCount
            }

        }

        def transaction = new Transaction(
                documentCategory: docCategory,
                transactionCode: transactionCode,
                transactionType: transactionType,
                staOperasional: biayaOperasional,
                transactionDate: transactionDate,
                companyDealer: session.userCompanyDealer,
                transferDate: transferDate,
                inOutType: "IN",
                amount: amount,
                description: description,
                bankAccountNumber: bankAccountNumber,
                dueDate: dueDate,
                staDel: "0",
                journal: journal,
                isOperasional: params?.biayaOperasional,
                isSetoranBank: params?.setoranBank,
                createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                lastUpdProcess: "INSERT",
                dateCreated: datatablesUtilService.syncTime(),
                lastUpdated:  datatablesUtilService.syncTime()
        ).save(flush: true, failOnError: true)

        def trxOpt = TransactionOperasional.findByCompanyDealer(session.userCompanyDealer,[sort : 'dateCreated' , order : 'desc'])
        if(trxOpt){
            saldo = trxOpt?.ta022Saldo
        }
        if (transaction.hasErrors()) {
            ret.error = transaction.errors
        } else {

            if(isOperasional=="1"){
                def transaksiOperasional = new TransactionOperasional(
                        transaction : transaction,
                        companyDealer : session.userCompanyDealer,
                        ta022Saldo :  saldo + amount,
                        staDel : '0',
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        dateCreated: datatablesUtilService.syncTime(),
                        lastUpdated:  datatablesUtilService.syncTime()
                ).save(flush: true, failOnError: true)
            }

        }
        ret.idTrx = (transaction ? transaction.id : -1)
        ret.codeTrx = (transactionCode ? transactionCode : -1)
        ret.codeJurnal = (journalCode ? journalCode : -1)
        render ret as JSON
    }

    def printKW(){
        def konversi = new Konversi()
        def reportData =[]
        def trx = Transaction.get(params.id);
        def user = User.findByUsernameAndStaDel(org?.apache?.shiro?.SecurityUtils?.subject?.principal?.toString(),"0")
        def moneyUtil = new MoneyUtil()
        def kabeng = Karyawan?.findByJabatanAndBranch(ManPower?.findByM014JabatanManPowerIlikeAndStaDel("%"+"KEPALA BENGKEL"+"%","0"),session?.userCompanyDealer)
        String nama = ""
        try {

            JournalDetail.findAllByJournal(trx.journal).each {
                if(it.subLedger){
                    nama += atasNama(it.subType,it.subLedger)
                }
            }

        }catch (Exception e){

        }
        reportData <<[
                lblTambahan : "",
                noTambahan  : "",
                jenis       : "(Lain-lain)",
                no          : "",
                pembayar    : nama,
                terbilang   : moneyUtil?.convertMoneyToWords(trx?.amount),
                tujuan      : trx?.description + " / "+trx?.transactionType,
                jumlah      : konversi.toRupiah(trx?.amount),
                tggl        : trx?.companyDealer?.kabKota?.m002NamaKabKota+" , "+new Date().format("dd MMMM yyyy"),
                tgglPrint   : datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm:ss"),
                kasir       : (user ? user.fullname : ""),
                kabeng      : kabeng ? kabeng?.nama : ""
        ]
        List<JasperReportDef> reportDefList = []
        def reportDef = new JasperReportDef(name:'Kwitansi.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        reportDefList.add(reportDef);

        def file = File.createTempFile("Kwitansi_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }
    def atasNama(def subtype, def subledger){
        def hasil = ""
        try {
            if(subtype?.subType == "VENDOR"){
                hasil = Vendor.findById(Long.parseLong(subledger))?.m121Nama
            }else if(subtype?.subType == "CUSTOMER"){
                hasil = HistoryCustomer.findById(Long.parseLong(subledger))?.t182NamaDepan
            }else if(subtype?.subType == "ASURANSI"){
                hasil = VendorAsuransi.findById(Long.parseLong(subledger))?.m193Nama
            }else if(subtype?.subType == "KARYAWAN"){
                hasil = Karyawan.findById(Long.parseLong(subledger))?.nama
            }else if(subtype?.subType == "CUSTOMER COMPANY"){
                hasil = Company.findById(Long.parseLong(subledger))?.namaPerusahaan
            }
        }catch (Exception e){}
        return hasil
    }
    def listSubledger(){
        def res = [:]
        def opts = []
        def cari = VendorAsuransi.createCriteria().list {
            eq("staDel","0")
            ilike("m193Nama",params.query+"%")
            order("m193Nama")
            maxResults(10);
        }
        cari.each {
            opts<<it.m193Nama
        }
        res."options" = opts
        render res as JSON
    }
}
//woke

