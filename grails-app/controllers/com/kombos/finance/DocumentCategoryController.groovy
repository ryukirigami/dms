package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class DocumentCategoryController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def documentCategoryService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render documentCategoryService.datatablesList(params) as JSON
    }

    def create() {
        def result = documentCategoryService.create(params)

        if (!result.error)
            return [documentCategoryInstance: result.documentCategoryInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = documentCategoryService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["DocumentCategory", result.documentCategoryInstance.id])
            redirect(action: 'show', id: result.documentCategoryInstance.id)
            return
        }

        render(view: 'create', model: [documentCategoryInstance: result.documentCategoryInstance])
    }

    def show(Long id) {
        def result = documentCategoryService.show(params)

        if (!result.error)
            return [documentCategoryInstance: result.documentCategoryInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = documentCategoryService.show(params)

        if (!result.error)
            return [documentCategoryInstance: result.documentCategoryInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = documentCategoryService.update(params)
        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["DocumentCategory", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [documentCategoryInstance: result.documentCategoryInstance.attach()])
    }

    def delete() {
        def result = documentCategoryService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["DocumentCategory", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(DocumentCategory, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
