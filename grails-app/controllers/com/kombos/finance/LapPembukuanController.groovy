package com.kombos.finance

import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User

class LapPembukuanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]


    def index() {
        def KB = NamaManPower.createCriteria().get {
            eq("staDel","0");
            eq("companyDealer",session?.userCompanyDealer)
            manPowerDetail{
                manPower{
                    or{
                        eq("m014JabatanManPower","KEPALA BENGKEL",[ignoreCase: true])
                        eq("m014Inisial","KBNG",[ignoreCase: true])
                    }
                }
            }
            maxResults(1);
        }
        def namaKB = KB?.t015NamaLengkap ? KB?.t015NamaLengkap : "-"
        def namaUser = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())?.t001NamaPegawai
        [dibuat : namaUser, disetujui : namaKB]
    }
}
