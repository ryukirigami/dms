package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class PersenTargetController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams=params

        def c = PersenTarget.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            if(session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                if (params."sCriteria_companyDealer") {
                    companyDealer{
                        ilike("m011NamaWorkshop","%"+ (params."sCriteria_companyDealer") + "%")
                    }
                }
            }else{
                eq("companyDealer",session?.userCompanyDealer)
            }

            if (params."sCriteria_accountNumber") {
                accountNumber{
                    or{
                        ilike("accountNumber","%"+ (params."sCriteria_accountNumber") + "%")
                        ilike("accountName","%"+ (params."sCriteria_accountNumber") + "%")
                    }
                }
            }

            if (params."sCriteria_persenBP") {
                eq("persenBP",params."sCriteria_persenBP" as Double)
            }

            if (params."sCriteria_persenGR") {
                eq("persenGR",params."sCriteria_persenGR" as Double)
            }

            if (params."sCriteria_evtGrup") {
                ilike("evtGroup","%"+params."sCriteria_evtGrup"+"%")
            }

            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it?.companyDealer?.m011NamaWorkshop,

                    accountNumber: it?.accountNumber ? it?.accountNumber?.accountNumber+" - "+it?.accountNumber?.accountName: "",

                    persenGR: it?.persenGR,

                    persenBP: it?.persenBP,

                    evtGrup: it?.evtGroup,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [persenTargetInstance: new PersenTarget(params)]
    }

    def save() {
        def akun = params.akun.toString().contains("|") ? params.akun.toString().substring(0,params.akun.toString().indexOf("|")): params.akun.toString().replace(" ","")
        def akunInstance = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")
        def persenTargetInstance = new PersenTarget(params)
        if(!akunInstance){
            flash.message = "Account Number Tidak ditemukan"
            render(view: "create", model: [persenTargetInstance: persenTargetInstance])
            return
        }
        persenTargetInstance?.accountNumber = akunInstance
        persenTargetInstance?.companyDealer = CompanyDealer.get(params?.companyDealer?.id)
        persenTargetInstance?.setStaDel('0')
        persenTargetInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        persenTargetInstance?.setLastUpdProcess("INSERT")

        if (!persenTargetInstance.save(flush: true)) {
            render(view: "create", model: [persenTargetInstance: persenTargetInstance])
            return
        }
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("M702_ID",null,persenTargetInstance?.m702ID)

        flash.message = message(code: 'default.created.message', args: [message(code: 'persenTarget.label', default: 'Persen Target'), persenTargetInstance.id])
        redirect(action: "show", id: persenTargetInstance.id)
    }

    def show(Long id) {
        def persenTargetInstance = PersenTarget.get(id)
        if (!persenTargetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenTarget.label', default: 'Persen Target'), id])
            redirect(action: "list")
            return
        }

        [persenTargetInstance: persenTargetInstance]
    }

    def edit(Long id) {
        def persenTargetInstance = PersenTarget.get(id)
        if (!persenTargetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenTarget.label', default: 'Persen Target'), id])
            redirect(action: "list")
            return
        }

        [persenTargetInstance: persenTargetInstance]
    }

    def update(Long id, Long version) {
        def persenTargetInstance = PersenTarget.get(id)
        if (!persenTargetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenTarget.label', default: 'PersenTarget'), id])
            redirect(action: "list")
            return
        }


        if (version != null) {
            if (persenTargetInstance.version > version) {

                persenTargetInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'persenTarget.label', default: 'PersenTarget')] as Object[],
                        "Another user has updated this PersenTarget while you were editing")
                render(view: "edit", model: [persenTargetInstance: persenTargetInstance])
                return
            }
        }

        def akun = params.akun.toString().contains("|") ? params.akun.toString().substring(0,params.akun.toString().indexOf("|")): params.akun.toString().replace(" ","")
        def akunInstance = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")
        if(!akunInstance){
            flash.message = "Account Number Tidak ditemukan"
            render(view: "edit", model: [persenTargetInstance: persenTargetInstance])
            return
        }

        persenTargetInstance.properties = params
        persenTargetInstance?.accountNumber = akunInstance
        persenTargetInstance?.companyDealer = CompanyDealer.get(params?.companyDealer?.id);
        persenTargetInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        persenTargetInstance?.setLastUpdProcess("UPDATE")

        if (!persenTargetInstance.save(flush: true)) {
            render(view: "edit", model: [persenTargetInstance: persenTargetInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'persenTarget.label', default: 'PersenTarget'), persenTargetInstance.id])
        redirect(action: "show", id: persenTargetInstance.id)
    }

    def delete(Long id) {
        def persenTargetInstance = PersenTarget.get(id)
        if (!persenTargetInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'persenTarget.label', default: 'PersenTarget'), id])
            redirect(action: "list")
            return
        }

        try {
            persenTargetInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            persenTargetInstance?.setLastUpdProcess("DELETE")
            persenTargetInstance?.lastUpdated = datatablesUtilService?.syncTime()
            persenTargetInstance?.setStaDel('1')
            persenTargetInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'persenTarget.label', default: 'PersenTarget'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'persenTarget.label', default: 'PersenTarget'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(PersenTarget, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(PersenTarget, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }


}
