package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class BankReconciliationController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def bankReconciliationService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def skrg = new Date().clearTime()
        def bsok = (new Date()+1).clearTime()
        String staAkhirBulan = "N"
        if(skrg.format("MM")!=bsok.format("MM")){
            staAkhirBulan = "Y"
        }
        [skrg : skrg,staAkhirBulan : staAkhirBulan]
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render bankReconciliationService.datatablesList(params) as JSON
    }

    def create() {
        def result = bankReconciliationService.create(params)

        if (!result.error)
            return [bankReconciliationInstance: result.bankReconciliationInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = bankReconciliationService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["BankReconciliation", result.bankReconciliationInstance.id])
            redirect(action: 'show', id: result.bankReconciliationInstance.id)
            return
        }
        render(view: 'create', model: [bankReconciliationInstance: result.bankReconciliationInstance])
    }

    def show(Long id) {
        def result = bankReconciliationService.show(params)

        if (!result.error)
            return [bankReconciliationInstance: result.bankReconciliationInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = bankReconciliationService.show(params)

        if (!result.error)
            return [bankReconciliationInstance: result.bankReconciliationInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = bankReconciliationService.update(params)

        /*if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["BankReconciliation", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }
*/
       /* render(view: 'edit', model: [bankReconciliationInstance: result.bankReconciliationInstance.attach()])*/
        render result as JSON
    }

    def delete() {
        def result = bankReconciliationService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["BankReconciliation", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(BankReconciliation, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}
