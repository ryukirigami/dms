package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.parts.Vendor
import grails.converters.JSON

class SubledgerModalController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", generateSerial: "GET"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss"),subType : params?.subtype]
    }

    def datatablesList() {
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        session.exportParams = params
        def rows = []
        if(params?.subtype?.toString()?.equalsIgnoreCase("ASURANSI")){
            def c = VendorAsuransi.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("staDel", "0")
                if (params."sCriteria_accountNumber") {
                    ilike("m193Nama", "%" + (params."sCriteria_accountNumber" as String) + "%")
                }
                if (params."sCriteria_alamat") {
                    ilike("m193Alamat", "%" + (params."sCriteria_alamat" as String) + "%")
                }
                order("m193Nama")
            }

            results.each {
                rows << [
                        id: it.id,
                        accountNumber: it?.m193Nama,
                        alamat: it?.m193Alamat
                ]
            }

            ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }else if(params?.subtype?.toString()?.equalsIgnoreCase("CABANG")){
            def c = CompanyDealer.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("staDel", "0")
                if (params."sCriteria_accountNumber") {
                    ilike("m011NamaWorkshop", "%" + (params."sCriteria_accountNumber" as String) + "%")
                }
                if (params."sCriteria_alamat") {
                    ilike("m011Alamat", "%" + (params."sCriteria_alamat" as String) + "%")
                }
                order("m011NamaWorkshop")
            }

            results.each {
                rows << [
                        id: it.id,
                        accountNumber: it?.m011NamaWorkshop,
                        alamat: it?.m011Alamat
                ]
            }

            ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }else if(params?.subtype?.toString()?.equalsIgnoreCase("CUSTOMER")){
            def c = HistoryCustomer.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("staDel", "0")
                if (params."sCriteria_accountNumber") {
                    ilike("fullNama", "%" + (params."sCriteria_accountNumber" as String) + "%")
                }
                if (params."sCriteria_alamat") {
                    ilike("t182Alamat", "%" + (params."sCriteria_alamat" as String) + "%")
                }
                order("fullNama")
            }

            results.each {
                rows << [
                        id: it.id,
                        accountNumber: it?.fullNama,
                        alamat: it?.t182Alamat
                ]
            }

            ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }else if(params?.subtype?.toString()?.equalsIgnoreCase("KARYAWAN")){
            def c = Karyawan.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("staDel", "0")
                or{
                    isNull("branch")
                    eq("branch",session?.userCompanyDealer)
                }
                if (params."sCriteria_accountNumber") {
                    ilike("nama", "%" + (params."sCriteria_accountNumber" as String) + "%")
                }
                if (params."sCriteria_alamat") {
                    ilike("alamat", "%" + (params."sCriteria_alamat" as String) + "%")
                }

                order("nama")
            }

            results.each {
                rows << [
                        id: it.id,
                        accountNumber: it?.nama,
                        alamat: it?.alamat
                ]
            }

            ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }else if(params?.subtype?.toString()?.equalsIgnoreCase("VENDOR")){
            def c = Vendor.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("staDel", "0")
                if (params."sCriteria_accountNumber") {
                    ilike("m121Nama", "%" + (params."sCriteria_accountNumber" as String) + "%")
                }
                if (params."sCriteria_alamat") {
                    ilike("m121Alamat", "%" + (params."sCriteria_alamat" as String) + "%")
                }

                order("m121Nama")
            }

            results.each {
                rows << [

                        id: it.id,
                        accountNumber: it?.m121Nama,
                        alamat: it?.m121Alamat

                ]
            }


            ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }else if(params?.subtype?.toString()?.equalsIgnoreCase("CUSTOMER COMPANY")){
            def c = Company.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                eq("staDel", "0")
                if (params."sCriteria_accountNumber") {
                    ilike("namaPerusahaan", "%" + (params."sCriteria_accountNumber" as String) + "%")
                }
                if (params."sCriteria_alamat") {
                    ilike("alamatPerusahaan", "%" + (params."sCriteria_alamat" as String) + "%")
                }

                order("namaPerusahaan")
            }

            results.each {
                rows << [

                        id: it.id,
                        accountNumber: it?.namaPerusahaan,
                        alamat: it?.alamatPerusahaan

                ]
            }


            ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        }


        render ret as JSON
    }
}