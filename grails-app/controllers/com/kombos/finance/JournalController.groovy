package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.generatecode.GenerateCodeService
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class JournalController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def journalService

    def jasperService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST", journalApproval: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        def appSettingParamDateFormat = AppSettingParam.findByCode(com.kombos.baseapp.AppSettingParamService.DATE_FORMAT)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def isNoParam = false;

        if(!params."sCriteria_journalCode" && !params."sCriteria_journalDate_dp" && !params."sCriteria_journalDate_dp_to"
                && !params."sCriteria_description" && !params."sCriteria_totalDebit" && !params."sCriteria_totalCredit"){
            isNoParam = true;
        }

        def c = Journal.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(!session.userAuthority.toString().contains("ROLE_KALINGGA")){
                eq("companyDealer",params?.companyDealer)
                if (params.journalTransaction) {
                    journalType {
                        eq("journalType", "TJ")
                    }
                } else if (params.journalMemorial) {
                    journalType {
                        eq("journalType", "MJ")
                    }
                }

                if (params.approval) {
                    eq("isApproval", "0")
                }else{
                    eq("isApproval", "1")
                    if(isNoParam){
                        eq("id",-1000.toLong())
                    }
                }
            }
            if(params."sCriteria_journalCode"){
                ilike("journalCode","%" + (params."sCriteria_journalCode" as String) + "%")
            }

            if(params."sCriteria_journalDate_dp"){
                ge("journalDate", Date.parse("dd/MM/yyyy", params."sCriteria_journalDate_dp"))
                if (params."sCriteria_journalDate_dp_to")
                    lt("journalDate", Date.parse("dd/MM/yyyy", params."sCriteria_journalDate_dp_to")+1)
            }

            if(params."sCriteria_description"){
                ilike("description","%" + (params."sCriteria_description" as String) + "%")
            }

            if(params."sCriteria_totalDebit"){
                eq("totalDebit",params?."sCriteria_totalDebit"?.toBigDecimal())
            }

            if(params."sCriteria_totalCredit"){
                eq("totalCredit",params?."sCriteria_totalCredit"?.toBigDecimal())
            }

            if(params."sCriteria_docNumber"){
                ilike("docNumber","%" + (params."sCriteria_docNumber" as String) + "%")
            }

            if (params."sCriteria_accountNumber") {
                journalDetail {
                    accountNumber {
                        eq("accountNumber", params."sCriteria_accountNumber",[ignoreCase: true])
                    }
                }
            }

			switch(sortProperty){
                case "docNumber" :
                    order("docNumber",sortDir)
                    break;
                case "journalCode" :
                    order("journalCode",sortDir)
                    break;
                case "journalDate" :
                    order("journalDate",sortDir)
                    break;
				default:
                    order("journalDate","desc")
					break;
			}
        }


        def rows = []
        def konversi = new Konversi()
        results.each {

            rows << [

                    id: it.id,

                    journalCode: it.journalCode,

                    journalDate: it.journalDate?it.journalDate.format(dateFormat):"",

                    description: it.description,

                    totalDebit: konversi.toRupiah(it.totalDebit),

                    totalCredit: konversi.toRupiah(it.totalCredit),

                    docNumber: it.docNumber,

                    isApproval: it.isApproval,

                    journalType: it.journalType,

                    total: konversi.toRupiah(it.totalCredit + it.totalDebit)
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

	def create() {
		def result = journalService.create(params)
        if(!result.error)
            return [journalInstance: result.journalInstance, newCode: new GenerateCodeService().generateJournalCode("MJ")]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

    def cekSave(){
        def hasil = false
        params.companyDealer = session.userCompanyDealer
        def cekJournal = Journal.findByCompanyDealerAndJournalTypeAndStaDelAndDocNumber(params.companyDealer,JournalType.findByJournalType('MJ'),'0',params.docNumber)
        if(cekJournal){
            hasil = true
        }
        render hasil
    }
	def save() {
        params.companyDealer = session.userCompanyDealer
		def result = journalService.save(params)
        render result as JSON
	}

	def show(Long id) {
		def result = journalService.show(params)

		if(!result.error)
			return [ journalInstance: result.journalInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = journalService.show(params)

		if(!result.error)
			return [ journalInstance: result.journalInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = journalService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Journal", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[journalInstance: result.journalInstance.attach()])
	}

	def deleteJurnal() {
        def hasil= journalService.deleteJurnal(params)
        render hasil
	}

    def showDetail() {
        def ret = null
        if ((params.type as String).equals("subType"))
            ret = SubType.list()
        else
            ret = AccountNumber.findAllByAccountMutationType("MUTASI")
        render ret as JSON
    }

    def removeDetail() {
        if (params.detailId && params.journalId) {
            def journalInstance = Journal.findById(Integer.parseInt(params.journalId as String))
            if (journalInstance) {
                def detailJournalInstance = JournalDetail.findById(Integer.parseInt(params.detailId as String))
                journalInstance.removeFromJournalDetail(detailJournalInstance)
                detailJournalInstance.delete()
                if (journalInstance.save(flush: true))
                    render "Success"
            }
        }
    }

    // Jurnal Transaksi lama
    def transactionList() {

    }


    // Jurnal Transaksi Baru
    def jurnalTransaksi() {
        def htmlData = ""

        if(params.dari=="cari"){
            params?.companyDealer = session?.userCompanyDealer
            def data = journalService.jurnalTrxDTList(params)
            def no = 0
            data.each{
                no++;
                def clas = "class='even'"
                if(no % 2 == 1){
                    clas = "class='odd'"
                }
                htmlData += "<tr "+clas+">"
                htmlData += "<td> <a href='javascript:void(0);'  onclick='showDetail("+it.id+");'>" + it.journalCode+ " </a></td>"
                htmlData += "<td>" + it.journalDate+ "</td>"
                htmlData += "<td>" + it.description + "</td>"
                htmlData += "<td>" + it.totalDebit+ "</td>"
                htmlData += "<td>" + it.totalCredit+ "</td>"
                htmlData += "</tr>"
            }

            if(data.size()==0){
                htmlData += "<tr class='odd'>"
                htmlData += "<td class='dataTables_empty' valign='top' colspan='5'>No Data Available in Table</td>"
                htmlData += "</tr>"
            }

            params.jumlahdata = no

            if(params.sCriteria_journalDate_dp){
                params.sCriteria_journalDate_dp = new Date().parse("dd/MM/yyyy",params.sCriteria_journalDate_dp)
            }
            if(params.sCriteria_journalDate_dp_to){
                params.sCriteria_journalDate_dp_to = new Date().parse("dd/MM/yyyy",params.sCriteria_journalDate_dp_to)
            }
        }else{
            htmlData += "<tr class='odd'>"
            htmlData += "<td class='dataTables_empty' valign='top' colspan='5'>No Data Available in Table</td>"
            htmlData += "</tr>"
        }
        [htmlData: htmlData, params : params]
    }

    def showTransactionDetail() {
        def result = Journal.findById(Long.parseLong(params.id))
        int dari = 0

        if(params.dari){
            dari = params.dari.toInteger()
        }

        def test = JournalDetail.executeQuery("from JournalDetail jd where staDel = '0' and journal.id = ${result?.id} order by debitAmount desc, accountNumber.accountNumber asc ")
        def konversi = new Konversi()
        if(result)
            return [journalInstance: result,journalDetailInstance :test, dari : dari, konversi : konversi ]

        /*flash.message = g.message(code: result.error.code, args: result.error.args)*/
        redirect(action: 'list')
    }

    // Buku Besar
    def bukuBesar() {
        def htmlData = ""

        if(params.dari=="cari"){
            def mb = monthlyBalance(params)
            params.saldoAwal = mb?.saldoAwal!=null ? mb?.saldoAwal : 0
            params.grandTotDebet = mb?.totDebet!=null ? mb?.totDebet : 0
            params.grandTotKredit = mb?.totKredit!=null ? mb?.totKredit : 0
            params.grandTot = mb?.totMutasi!=null ? mb?.totMutasi : 0
            def data = bukuBesarDTList(params)
            def no = 0
            data.each{
                def nama = ""
                if(!params.subLedger){
                    nama =  atasNama(it.subType, it.subLedger)
                }
                no++
                def clas = "class='even'"
                if(no % 2 == 1){
                    clas = "class='odd'"
                }
                htmlData += "<tr "+clas+">"
                htmlData += "<td> <a href='javascript:void(0);'  onclick='showDetail("+it.idJurnal+");'>" + it.journalCode+ " </a></td>"
                htmlData += "<td>" + it.journalDate+ "</td>"
                htmlData += "<td>" + it.description+ " <b>"+ nama +"</b></td>"
                htmlData += "<td>" + it.totalDebit+ "</td>"
                htmlData += "<td>" + it.totalCredit+ "</td>"
                htmlData += "<td>" + it.total+ "</td>"
                htmlData += "</tr>"
            }
            if(data.size()==0){
                htmlData += "<tr class='odd'>"
                htmlData += "<td class='dataTables_empty' valign='top' colspan='6'>No Data Available in Table</td>"
                htmlData += "</tr>"
            }
            params.sCriteria_journalDate_dp = new Date().parse("dd/MM/yyyy",params.sCriteria_journalDate_dp)
            params.jumlahdata = no
            params.sCriteria_journalDate_dp_to = new Date().parse("dd/MM/yyyy",params.sCriteria_journalDate_dp_to)
        }
        [htmlData: htmlData, params : params]
    }


    def bukuBesarDTList(def params) {
        params?.companyDealer = session?.userCompanyDealer
        return journalService.bukuBesarDTList(params)
    }


    def monthlyBalance(def params) {
        def awal = new Date().parse("dd/MM/yyyy",params?.sCriteria_journalDate_dp)
        def akhir = new Date().parse("dd/MM/yyyy",params?.sCriteria_journalDate_dp_to)
        def metod = new MonthlyBalanceController()
        def bln = params."sCriteria_journalDate_dp".toString().substring(params."sCriteria_journalDate_dp".toString().indexOf("/")+1,params."sCriteria_journalDate_dp".toString().lastIndexOf("/"))
        def thn = params."sCriteria_journalDate_dp".toString().substring(params."sCriteria_journalDate_dp".toString().lastIndexOf("/")+1)
        String lastYM = metod.lastYearMonth(bln.toInteger(),thn.toInteger());
        def akun = params.akun.toString().contains("|") ? params.akun.toString().substring(0,params.akun.toString().indexOf("|")): params.akun.toString().replace(" ","")
        def accNumber  = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")
        def saldoAwal = 0
        def totDebet = 0
        def totKredit = 0
        def totMutasi = 0
        if(accNumber){
            //saldo awal lama cuma 1 row
//            def mb = MonthlyBalance.createCriteria().get{
//                eq("companyDealer",session?.userCompanyDealer);
//                eq("accountNumber",accNumber);
//                eq("yearMonth",lastYM);
//                if(params.subLedger){
//                    eq("subLedger",params.subLedger,[ignoreCase : true])
//                    subType{
//                        eq("id",params.subType.toLong())
//                    }
//                }
//                maxResults(1);
//            }
//            saldoAwal = mb ? mb.endingBalance : 0

            def mbList = MonthlyBalance.createCriteria().list{
                eq("companyDealer",session?.userCompanyDealer);
                eq("accountNumber",accNumber);
                eq("yearMonth",lastYM);
                if(params.subLedger){
                    eq("subLedger",params.subLedger,[ignoreCase : true])
                    subType{
                        eq("id",params.subType.toLong())
                    }
                }
            }
            mbList.each{
               saldoAwal += it ? it.endingBalance : 0
            }

            if(awal?.format("dd")!="01"){
                def journals = JournalDetail.createCriteria().list {
                    eq("staDel","0")
                    eq("accountNumber",accNumber)
                    journal {
                        ge("journalDate",new Date().parse("dd/MM/yyyy","01/"+awal?.format("MM/yyyy")))
                        lt("journalDate",awal)
                        eq("staDel","0")
                        eq("companyDealer",session?.userCompanyDealer)
                        eq("isApproval", "1")
                    }
                    if(params.subLedger){
                        eq("subLedger",params.subLedger,[ignoreCase : true])
                        subType{
                            eq("id",params.subType.toLong())
                        }
                    }
                }

                def hcds = 0
                journals.each {
                    if(it.accountNumber.accountType.saldoNormal=='KREDIT'){
                        hcds =  (it.creditAmount - it.debitAmount)
                        saldoAwal = saldoAwal - hcds
                    }else{
                        hcds =  (it.debitAmount - it.creditAmount)
                        saldoAwal = saldoAwal + hcds
                    }
                }
            }
            def journalTotal = JournalDetail.createCriteria().list {
                eq("staDel","0")
                eq("accountNumber",accNumber)
                journal {
                    ge("journalDate",awal)
                    lt("journalDate",akhir+1)
                    eq("staDel","0")
                    eq("companyDealer",session?.userCompanyDealer)
                    eq("isApproval", "1")
                }
                if(params.subLedger){
                    eq("subLedger",params.subLedger,[ignoreCase : true])
                    subType{
                        eq("id",params.subType.toLong())
                    }
                }
            }
            journalTotal.each {
                totDebet += it?.debitAmount
                totKredit += it?.creditAmount
            }
        }
        def hcd = 0
        if(accNumber.accountType.saldoNormal == "DEBET"){
            hcd = totDebet - totKredit
        }else{
            hcd = totKredit - totDebet
        }
        if(accNumber.accountType.saldoNormal == "KREDIT"){
            saldoAwal = saldoAwal* (-1)
        }
        totMutasi = saldoAwal + hcd
        def result = [:]
        def k  = new Konversi()
        def data = saldoAwal < 0 ? "(" + k.toRupiah((saldoAwal * (-1))) + ")": k.toRupiah(saldoAwal)
        def data2 = totDebet < 0 ? "(" + k.toRupiah((totDebet * (-1))) + ")": k.toRupiah(totDebet)
        def data3 = totKredit < 0 ? "(" + k.toRupiah((totKredit * (-1))) + ")": k.toRupiah(totKredit)
        def data4 = totMutasi < 0 ? "(" + k.toRupiah((totMutasi * (-1))) + ")": k.toRupiah(totMutasi)
        result.saldoAwal = data;
        result.totDebet = data2;
        result.totKredit = data3;
        result.totMutasi = data4;

        return result
    }

    def approval() {
        session.exportParams=params
        def htmlData = ""
        def c = Journal.createCriteria()
        def results = c.list () {
            eq("staDel","0")
            eq("companyDealer",session.userCompanyDealer)
            eq("isApproval", "0")
            order("journalDate","asc")
        }
        def rows = []
        def konversi = new Konversi()
        def no = 0
        results.each {
            def clas = "class='even'"
            no++
            if(no % 2 == 1){
                clas = "class='odd'"
            }
            htmlData += "<tr "+clas+">"
            htmlData += "<td>" + no + "</td>"
            htmlData += "<td>" + it.journalCode + "</td>"
            htmlData += "<td>" + it.journalDate?.format("dd/MM/yyyy") + "</td>"
            htmlData += "<td>" + it.description + "</td>"
            htmlData += "<td>" + konversi.toRupiah(it.totalDebit)+ "</td>"
            htmlData += "<td>" + konversi.toRupiah(it.totalCredit) + "</td>"
            htmlData += "<td>" + it.docNumber + "</td>"
            htmlData += "<td>" + it.createdBy + "</td>"
            htmlData += "<td>"
            htmlData += '<input type="hidden" value="'+it.id+'">&nbsp;&nbsp;<a href="javascript:void(0);" id = "need_'+ it.id+'" onclick="showDetailApproval('+it.id+');">Need Approval</a> <div id = "hasil_'+ it.id +'" ></div>';
            htmlData += "</td>"
            htmlData += "</tr>"
        }
        if(results.size()==0){
            htmlData += "<tr class='odd'>"
            htmlData += "<td class='dataTables_empty' valign='top' colspan='9'>No Data Available in Table</td>"
            htmlData += "</tr>"
        }
        [htmlData : htmlData]
    }

    def journalApproval() {
        def ret = [:]
        if (params.id) {
            def journalInstance = Journal.findById(Long.parseLong(params.id))
            if (journalInstance) {
                if(params?.status=="approve"){
                    journalInstance.isApproval = "1"
                    journalInstance.lastUpdated = new Date()
                    journalInstance.save(flush: true)
                 }else{
                    journalInstance.isApproval = "2"
                    journalInstance.staDel = "1"
                    journalInstance.lastUpdated = new Date()
                    journalInstance.save(flush: true)
                    def jurnalDetail = JournalDetail.findAllByJournalAndStaDel(journalInstance,"0");
                    jurnalDetail.each {
                        it?.staDel = "1"
                        it?.save(flush: true)
                    }
                    if (journalInstance.hasErrors()) {
                        ret.error = journalInstance.errors
                    }
                }
            }
        } else {
            ret.error = "Id tidak ditemukan"
        }

        render ret as JSON
    }

    def arrayYearMonth(String ym1,String ym2){
        def hasil = []
        int bulan1 = ym1?.substring(0,2)?.toInteger(),tahun1 = ym1?.substring(2,ym1?.length())?.toInteger()
        int bulan2 = ym2?.substring(0,2)?.toInteger(),tahun2 = ym2?.substring(2,ym2?.length())?.toInteger()
        if(tahun1<tahun2){
            (bulan1..12).each{
                String bln = it?.toString()
                if(bln.size()<2){
                    bln="0"+bln
                }
                String ym = bln+tahun1.toString()
                hasil<<ym
            }
            (1..bulan2).each{
                String bln = it?.toString()
                if(bln.size()<2){
                    bln="0"+bln
                }
                String ym = bln+tahun2.toString()
                hasil<<ym
            }
        }else {
            (bulan1..bulan2).each{
                String bln = it?.toString()
                if(bln.size()<2){
                    bln="0"+bln
                }
                String ym = bln+tahun1.toString()
                hasil<<ym
            }
        }
        return hasil
    }

    def accountNumbersDT() {
        def data = journalService.accountNumberDT(params)
        render data as JSON
    }

    def printBukuBesar(){
        List<JasperReportDef> reportDefList = []

        def reportData = calculateReportDataBukuBesar(params)
        def akun = params.nomorAkun.toString().contains("|") ? params.nomorAkun.toString().substring(0,params.nomorAkun.toString().indexOf("|")): params.nomorAkun.toString().replace(" ","")
        def accountName  = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")
        accountName = accountName.accountName.replaceAll(" ","_")
        def formatFile = ""
        if(params.format=="xls"){
            formatFile = ".xls"
        }else{
            formatFile = ".pdf"
        }

        def reportDef = new JasperReportDef(name:'Mutasi_Buku_Besar.jasper',
                fileFormat:params.format=="xls" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("BukuBesar("+ accountName+")_"+session.userCompanyDealerId +"_",formatFile)
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="xls"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculateReportDataBukuBesar(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def akun = params.nomorAkun.toString().contains("|") ? params.nomorAkun.toString().substring(0,params.nomorAkun.toString().indexOf("|")): params.nomorAkun.toString().replace(" ","")

        def accNumber  = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")
        String tglAwal =  params.tglAwal
        String tglAkhir =  params.tglAkhir
        Date tgl = new Date().parse('d/MM/yyyy',tglAwal.toString())
        Date tgl2 = new Date().parse('d/MM/yyyy',tglAkhir.toString())
        Date awalBulan = new Date().parse('d/MM/yyyy',"01/" + tgl.format("MM/yyyy"))

        def metod = new MonthlyBalanceController()
        String lastYM = metod.lastYearMonth(tgl.format("MM").toInteger(),tgl.format("yyyy").toInteger());

        def saldoAwal = 0.00
        def debitAmmount = 0.00
        def creditAmmount = 0.00
        def hasilDC = 0.00
        if(accNumber){
            def ab = JournalDetail.createCriteria().list {
                eq("staDel", "0")
                eq("accountNumber",accNumber)
                journal{
                    eq("staDel", "0")
                    ge("journalDate",awalBulan)
                    lt("journalDate",tgl)
                    eq("companyDealer",session?.userCompanyDealer)
                    eq("isApproval", "1")
                }
                if(params.subLedger){
                    eq("subLedger",params.subLedger,[ignoreCase : true])
                    subType{
                        eq("id",params.subType.toLong())
                    }
                }

                journal{
                    order("journalDate","asc")
                }
            }
            ab.each {
                debitAmmount  += it.debitAmount
                creditAmmount += it.creditAmount
            }

            if(accNumber.accountType.saldoNormal == "DEBET"){
                hasilDC = debitAmmount - creditAmmount
            }else{
                hasilDC = creditAmmount - debitAmmount
            }

            def mb = MonthlyBalance.createCriteria().list {
                eq("accountNumber",accNumber)
                eq("yearMonth",lastYM)
                eq("companyDealer",session?.userCompanyDealer);
                if(params?.subLedger){
                    eq("subLedger",params?.subLedger);
                }else {
                    isNull("subLedger")
                }
                if(params?.subType){
                    eq("subType",SubType.get(params?.subType?.toLong()))
                }else {
                    isNull("subType")
                }
            }
            mb.each {
                saldoAwal += it?.endingBalance
            }
            if(accNumber.accountType.saldoNormal == "DEBET"){
                hasilDC = debitAmmount - creditAmmount
                saldoAwal = saldoAwal + hasilDC
            }else{
                hasilDC = creditAmmount - debitAmmount
                saldoAwal = saldoAwal - hasilDC
            }


        }
        if(accNumber.accountType.saldoNormal == "KREDIT"){
            saldoAwal  = saldoAwal * (-1)
        }

        def c = JournalDetail.createCriteria()
        def results = c.list{
            eq("staDel", "0")
            eq("accountNumber",accNumber)
            journal{
                eq("companyDealer",session?.userCompanyDealer)
                ge("journalDate",tgl)
                lt("journalDate",tgl2+1)
                eq("isApproval", "1")
            }
            if(params.subLedger){
                eq("subLedger",params.subLedger,[ignoreCase : true])
                subType{
                    eq("id",params.subType.toLong())
                }
            }
            journal{
                order("journalDate","asc")
                order("journalCode","asc")
            }
        }
        def konversi = new Konversi()
        int count = 0
        def saldoAkhir = 0
        def da = 0
        def ca = 0
        def hcd = 0
        def subLedger = ""
        if(params?.subLedger && params?.subType){
            def subType = SubType.get(params?.subType?.toLong())
            if(subType?.description?.toUpperCase()=="CUSTOMER"){
                subLedger = HistoryCustomer.findById(params?.subLedger as Long) ? " a.n "+ HistoryCustomer.findById(params?.subLedger as Long)?.fullNama : ""                
            }else if(subType?.description?.toUpperCase()=="VENDOR"){
                subLedger = Vendor.findById(params?.subLedger as Long) ? " a.n "+ Vendor.findById(params?.subLedger as Long).m121Nama : ""
            }else if(subType?.description?.toUpperCase()=="ASURANSI"){
                subLedger = VendorAsuransi.findById(params?.subLedger as Long) ? " a.n "+ VendorAsuransi.findById(params?.subLedger as Long)?.m193Nama : ""
            }else if(subType?.description?.toUpperCase()=="KARYAWAN"){
                subLedger = Karyawan.findById(params?.subLedger as Long) ? " a.n "+ Karyawan.findById(params?.subLedger as Long).nama : ""
            }else if(subType?.description?.toUpperCase()=="CABANG"){
                subLedger = CompanyDealer.findById(params?.subLedger as Long) ? " a.n "+ CompanyDealer.findById(params?.subLedger as Long).m011NamaWorkshop : ""
            }else if(subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                subLedger = Company.findById(params?.subLedger as Long) ? " a.n "+ Company.findById(params?.subLedger as Long).namaPerusahaan : ""
            }
        }
        if(results.size()==0){
            if(saldoAwal!=0){
                def data = [:]
                count = count + 1
                data.put("CVKombosCabang",session.userCompanyDealer)
                data.put("NoRekening",accNumber.accountNumber)
                data.put("NamaRekening",accNumber.accountName+subLedger)
                data.put("Periode",tgl.format("dd MMMM yyyy") +" s.d "+ tgl2.format("dd MMMM yyyy"))
                data.put("saldoAwal",saldoAwal.toInteger()<0?"("+konversi.toRupiah(saldoAwal.toInteger()*(-1))+")":konversi.toRupiah(saldoAwal.toInteger()))

                data.put("no","")
                data.put("TanggalTransaksi","")
                data.put("TanggalInputSistem","")
                data.put("Keterangan","Tidak Ada Transaksi")
                data.put("Debet","")
                data.put("Kredit","")
                saldoAkhir = saldoAwal + hcd
                data.put("SaldoAhir",saldoAkhir.toInteger()<0?"("+konversi.toRupiah(saldoAkhir.toInteger()*(-1))+")":konversi.toRupiah(saldoAkhir.toInteger()))
                reportData.add(data)
            }
        }else{
            results.each {
                def data = [:]
                count = count + 1
                data.put("CVKombosCabang",session.userCompanyDealer)
                data.put("NoRekening",accNumber.accountNumber)
                data.put("NamaRekening",accNumber.accountName+subLedger)
                data.put("Periode",tgl.format("dd MMMM yyyy") +" s.d "+ tgl2.format("dd MMMM yyyy"))
                data.put("saldoAwal",saldoAwal.toInteger()<0?"("+konversi.toRupiah(saldoAwal *(-1))+")":konversi.toRupiah(saldoAwal))
                da += it.debitAmount
                ca += it.creditAmount
                if(accNumber.accountType.saldoNormal == "DEBET"){
                    hcd = da - ca
                }else{
                    hcd = ca - da
                }
                data.put("no",count)
                data.put("TanggalTransaksi",it.journal.journalDate.format("dd/MM/yyyy"))
                data.put("JournalCode",it?.journal?.journalCode)
                data.put("TanggalInputSistem",it.dateCreated.format("dd/MM/yyyy"))
                data.put("Keterangan",(!it?.description || it?.description=="-")? it.journal?.description:it?.description)
                data.put("Debet",konversi.toRupiah(it.debitAmount))
                data.put("Kredit",konversi.toRupiah(it.creditAmount))
                data.put("TotalKredit",konversi.toRupiah(ca))
                data.put("TotalDebet",konversi.toRupiah(da))
                saldoAkhir = saldoAwal + hcd
                data.put("saldo",konversi.toRupiah(saldoAkhir))
                data.put("SaldoAhir",saldoAkhir.toInteger()<0?"("+konversi.toRupiah(saldoAkhir *(-1))+")":konversi.toRupiah(saldoAkhir))
                reportData.add(data)
            }
        }

        return reportData

    }

    def checkBeforeInsert(String jurnalCode,String desc,def debet,def credit){
        boolean isExist = false;
        Journal journal = Journal.createCriteria().get {
            eq("journalCode",jurnalCode,[ignoreCase: true]);
            eq("description",desc,[ignoreCase: true]);
            eq("totalDebit",debet);
            eq("staDel",'0');
            eq("isApproval",'1');
            eq("totalCredit",credit);
        }
        if(journal){
            isExist = true;
        }
        return isExist;
    }

    def printJurnalTransaksi(){
        List<JasperReportDef> reportDefList = []

        def reportData = printJurnalTransaksilDetail(params)

        def reportDef = new JasperReportDef(name:'jurnal_transaksi.jasper',
                fileFormat:params.format=="xls" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("JURNAL_TRANSAKSI",params.format=="xls" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        if(params.format=="xls"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def printJurnalTransaksilDetail(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def periode = "-";
        def jurnalcode = "-";
        Date tgl = new Date();
        Date tgl2 = new Date();
        if(params.journalCode){
            jurnalcode = " Kode Jurnal : " + String.valueOf(params.journalCode).padLeft(18,"X")
            periode = jurnalcode
        }
        if(params.tglAwal && params.tglAkhir){
            String tglAwal =  params.tglAwal
            String tglAkhir =  params.tglAkhir
            tgl = new Date().parse('d/MM/yyyy',tglAwal.toString())
            tgl2 = new Date().parse('d/MM/yyyy',tglAkhir.toString())
            periode = tgl.format("dd/MM/yyyy") + " sd " + tgl2.format("dd/MM/yyyy")
        }



        def c = JournalDetail.createCriteria()
        def results = c.list{
            journal{
                eq("companyDealer",session?.userCompanyDealer)
                if(params.tglAwal && params.tglAkhir){
                    ge("journalDate",tgl)
                    lt("journalDate",tgl2+1)
                }

                if(params.journalCode){
                    ilike("journalCode","%"+(params.journalCode as String))
                }

                journalType {
                    eq("journalType", "TJ")
                }
                eq("isApproval", "1")
                eq("staDel", "0")
                order("journalDate","asc")
                order("journalCode","asc")
            }
            order("debitAmount","desc")
        }
        def count = 0
        def code = "";def subLedger = ""; def cetakCode = "";
        def cetakCount = "";
        def FLAG = "Keterangan  :  \n ";

        results.each{
            subLedger = ""
            cetakCode = ""
            cetakCount = ""
            if(code != it.journal.journalCode){
                cetakCode = it.journal.journalCode + "\n" + it.journal.journalDate.format("dd/MM/yyyy")
                code = it.journal.journalCode

                count = count + 1
                cetakCount = count
            }
            def data = [:]
            data.put("COMPANY_DEALER",session.userCompanyDealer)
            data.put("PERIODE",periode)
            data.put("NOMOR",cetakCount)
            data.put("FLAG",FLAG)
            data.put("NOMOR_JURNAL",cetakCode)
            data.put("NOMOR_REKENING",it.accountNumber)
            if(it?.subLedger && it?.subType){
                def subType = it.subType
                if(subType?.description?.toUpperCase()=="CUSTOMER"){
                    subLedger = HistoryCustomer.findById(it?.subLedger as Long) ? " a.n "+ HistoryCustomer.findById(it?.subLedger as Long)?.fullNama : ""
                }else if(subType?.description?.toUpperCase()=="VENDOR"){
                    subLedger = Vendor.findById(it?.subLedger as Long) ? " a.n "+ Vendor.findById(it?.subLedger as Long).m121Nama : ""
                }else if(subType?.description?.toUpperCase()=="ASURANSI"){
                    subLedger = VendorAsuransi.findById(it?.subLedger as Long) ? " a.n "+ VendorAsuransi.findById(it?.subLedger as Long)?.m193Nama : ""
                }else if(subType?.description?.toUpperCase()=="KARYAWAN"){
                    subLedger = Karyawan.findById(it?.subLedger as Long) ? " a.n "+ Karyawan.findById(it?.subLedger as Long).nama : ""
                }else if(subType?.description?.toUpperCase()=="CABANG"){
                    subLedger = CompanyDealer.findById(it?.subLedger as Long) ? " a.n "+ CompanyDealer.findById(it?.subLedger as Long).m011NamaWorkshop : ""
                }else if(subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                    subLedger = Company.findById(it?.subLedger as Long) ? " a.n "+ Company.findById(it?.subLedger as Long).namaPerusahaan : ""
                }
            }
            data.put("NAMA_REKENING",it.accountNumber.accountName + " " + subLedger)
            data.put("URAIAN",it.description ? it.description : it.journal.description)
            data.put("DEBET",new Konversi().toRupiah2(it.debitAmount))
            data.put("KREDIT",new Konversi().toRupiah2(it.creditAmount))
            reportData.add(data)
        }
        if(results.size()==0){
            def data = [:]
            data.put("COMPANY_DEALER",session.userCompanyDealer)
            data.put("PERIODE",periode)
            data.put("NOMOR","")
            data.put("NOMOR_JURNAL","")
            data.put("DEBET","")
            data.put("FLAG","")
            data.put("KREDIT","")
            reportData.add(data)
        }
        return reportData
    }


    def printJurnalMemorial(){
        List<JasperReportDef> reportDefList = []

        def reportData = printJurnalMemorialDetail(params)

        def reportDef = new JasperReportDef(name:'jurnal_memorial.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("JURNAL_MEMORIAL_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
    def printJurnalMemorialDetail(def params){
        params.dibuat = (params.dibuat as String).toUpperCase()
        params.disetujui = (params.disetujui as String).toUpperCase()
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def periode = "";
        String tglAwal =  params.tanggal
        String tglAkhir =  params.tanggal2
        Date tgl = new Date().parse('d/MM/yyyy',tglAwal.toString())
        Date tgl2 = new Date().parse('d/MM/yyyy',tglAkhir.toString())
        periode = tgl.format("dd/MM/yyyy") + " sd " + tgl2.format("dd/MM/yyyy")
        def c = JournalDetail.createCriteria()
        def results = c.list{
            journal{
                eq("companyDealer",session?.userCompanyDealer)
                ge("journalDate",tgl)
                lt("journalDate",tgl2+1)
                journalType {
                    eq("journalType", "MJ")
                }
                eq("isApproval", "1")
                eq("staDel", "0")
                order("journalDate","asc")
                order("journalCode","asc")
            }
            order("debitAmount","desc")
        }
        def count = 0
        def code = "";def subLedger = "";
        def cetakCode = "";def cetakTgl = "";def cetakTglInput = "";
        results.each{
            subLedger = ""
            cetakCode = ""
            cetakTgl = ""
            cetakTglInput = ""
            if(code != it.journal.journalCode){
                cetakCode = it.journal.journalCode
                cetakTgl =  it.journal.journalDate.format("dd/MM/yyyy")
                cetakTglInput =  it.dateCreated.format("dd/MM/yyyy")
                code = it.journal.journalCode
            }
            count = count + 1
            def data = [:]
            data.put("COMPANY_DEALER",session.userCompanyDealer)
            data.put("PERIODE",periode)
            data.put("DIBUAT",params.dibuat)
            data.put("DISETUJUI",params.disetujui)
            data.put("NOMOR",count)
            data.put("NOMOR_MEMORIAL",cetakCode)
            data.put("TANGGAL",cetakTgl)
            data.put("TANGGAL_INPUT",cetakTglInput)
            data.put("NOMOR_REKENING",it.accountNumber)
            if(it?.subLedger && it?.subType){
                def subType = it.subType
                if(subType?.description?.toUpperCase()=="CUSTOMER"){
                    subLedger = HistoryCustomer.findById(it?.subLedger as Long) ? " a.n "+ HistoryCustomer.findById(it?.subLedger as Long)?.fullNama : ""
                }else if(subType?.description?.toUpperCase()=="VENDOR"){
                    subLedger = Vendor.findById(it?.subLedger as Long) ? " a.n "+ Vendor.findById(it?.subLedger as Long).m121Nama : ""
                }else if(subType?.description?.toUpperCase()=="ASURANSI"){
                    subLedger = VendorAsuransi.findById(it?.subLedger as Long) ? " a.n "+ VendorAsuransi.findById(it?.subLedger as Long)?.m193Nama : ""
                }else if(subType?.description?.toUpperCase()=="KARYAWAN"){
                    subLedger = Karyawan.findById(it?.subLedger as Long) ? " a.n "+ Karyawan.findById(it?.subLedger as Long).nama : ""
                }else if(subType?.description?.toUpperCase()=="CABANG"){
                    subLedger = CompanyDealer.findById(it?.subLedger as Long) ? " a.n "+ CompanyDealer.findById(it?.subLedger as Long).m011NamaWorkshop : ""
                }else if(subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                    subLedger = Company.findById(it?.subLedger as Long) ? " a.n "+ Company.findById(it?.subLedger as Long).namaPerusahaan : ""
                }
            }
            data.put("NAMA_REKENING",it.accountNumber.accountName + " " + subLedger)
            data.put("URAIAN",it.description ? it.description : it.journal.description)
            data.put("DEBET",new Konversi().toRupiah2(it.debitAmount))
            data.put("KREDIT",new Konversi().toRupiah2(it.creditAmount))
            reportData.add(data)
        }
        if(results.size()==0){
            def data = [:]
            data.put("COMPANY_DEALER",session.userCompanyDealer)
            data.put("PERIODE",periode)
            data.put("NOMOR","")
            data.put("NOMOR_MEMORIAL","")
            data.put("DEBET","")
            data.put("KREDIT","")
            data.put("DIBUAT",params.dibuat)
            data.put("DISETUJUI",params.disetujui)
            reportData.add(data)
        }
        return reportData
    }

    def atasNama(def subtype, def subledger){
        def hasil = ""
        try {
            if(subtype?.subType == "VENDOR"){
                hasil = "=> an. " + Vendor.findById(Long.parseLong(subledger))?.m121Nama
            }else if(subtype?.subType == "CUSTOMER"){
                hasil = "=> an. " + HistoryCustomer.findById(Long.parseLong(subledger))?.t182NamaDepan
            }else if(subtype?.subType == "ASURANSI"){
                hasil = "=> an. " + VendorAsuransi.findById(Long.parseLong(subledger))?.m193Nama
            }else if(subtype?.subType == "KARYAWAN"){
                hasil = "=> an. " + Karyawan.findById(Long.parseLong(subledger))?.nama
            }else if(subtype?.subType == "CUSTOMER COMPANY"){
                hasil = "=> an. " + Company.findById(Long.parseLong(subledger))?.namaPerusahaan
            }
        }catch (Exception e){}
        return hasil
    }
}
