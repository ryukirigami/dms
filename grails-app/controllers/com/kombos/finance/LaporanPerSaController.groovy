package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.PartInv
import com.kombos.parts.Goods
import com.kombos.parts.GoodsHargaJual
import com.kombos.parts.KlasifikasiGoods
import com.kombos.parts.Konversi
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.hibernate.criterion.CriteriaSpecification

class LaporanPerSaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    def index() {}

    def tmo(){

    }

    def penjualanBarang(){

    }
    def printTmo(){

        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params.tahun as int,(params.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = new ArrayList()
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        def c = PartInv.createCriteria()
        def results = c.list{
            eq("t703StaDel","0")
            invoice{
                eq("companyDealer",companyDealer)
                ge("t701TglJamInvoice",tgl)
                lt("t701TglJamInvoice",tgl2+1)
                eq("t701StaDel",'0')
                isNull('t701NoInv_Reff')
                isNull('t701StaApprovedReversal')
                reception{
                    order("t401NamaSA","asc")
                }
            }

        }
        int count = 0
        def total = 0
        results.each {
            def klas = KlasifikasiGoods.findByGoods(it.goods)
            if(klas?.franc?.m117NamaFranc?.toUpperCase()?.contains("OLI")){
                if(it?.t703Jumlah1.toInteger() > 0){
                    total += (((it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)).toInteger())
                    def data = [:]
                    count = count + 1

                    data.put("companyDealer",companyDealer)
                    data.put("bulan",periode)
                    data.put("noUrut",count)
                    data.put("sa",it.invoice.reception.t401NamaSA.toString().toUpperCase())

                    data.put("kode_parts",it.goods.m111ID)
                    data.put("nama_parts",it.goods.m111Nama)
                    data.put("jml",it.t703Jumlah1.toInteger())
                    data.put("hargaJual",konversi.toRupiah(it?.t703HargaRp?.toInteger()))
                    data.put("total_hargaJual",((it?.t703TotalRp?it?.t703TotalRp:0) * (it?.t703Jumlah1?it?.t703Jumlah1:0)).toInteger())
                    data.put("total",konversi.toRupiah(total))
                    reportData.add(data)
                }
            }
        }
        if(count==0){
            def data = [:]
            count = count + 1

            data.put("companyDealer",companyDealer)
            data.put("bulan",periode)
            data.put("noUrut",count)
            data.put("sa","")

            data.put("kode_parts","")
            data.put("nama_parts","")
            data.put("jml","0")
            data.put("hargaJual","0")
            data.put("total_hargaJual",0)
            data.put("total",0)
            reportData.add(data)
        }

        def reportDef = new JasperReportDef(name:'f_lapPenjualanTmoPerSa.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("PENJUALAN_TMO("+companyDealer.id+")_"+ tgl.format('MMMyyyy')+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
    def printPenjualanBarang(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        def namaGoods = params.namaGoods as Long


        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params.tahun as int,(params.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = new ArrayList()
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        def c = InvoiceT701.createCriteria()
        def results = c.list{
            ge("t701TglJamInvoice",tgl)
            lt("t701TglJamInvoice",tgl2+1)
            eq("companyDealer",companyDealer)
            isNull("t701NoInv_Reff")
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalBayarRp",0.toDouble())
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            reception{
                isNotNull("t401NamaSA")
                eq("staSave","0")
            }
            projections {
                reception{
                    groupProperty("t401NamaSA", "t401NamaSA")
                }
            }
        }
        int count = 0
        def total = 0
        results.each { inv ->
                def partInv = PartInv.createCriteria().list {
                    goods{
                        eq("id",namaGoods as Long)
                    }
                    invoice{
                        reception{
                            eq("t401NamaSA",inv.t401NamaSA)
                        }
                    }
                    eq("t703StaDel","0")
                }

                def data = [:]
                count = count + 1

                data.put("companyDealer",companyDealer)
                data.put("bulan",periode)
                data.put("namaBarang",Goods.findById(namaGoods as Long).m111ID.trim() + " /  " + Goods.findById(namaGoods as Long).m111Nama)
                def rega = GoodsHargaJual.findByGoodsAndStaDel(Goods.findById(namaGoods as Long),"0")?.t151HargaTanpaPPN
                data.put("noUrut",count)
                data.put("namaSa",(inv.t401NamaSA as String).toString().toUpperCase())
                data.put("jml",partInv.size())
                data.put("hargaJual",rega?konversi.toRupiah(rega.toInteger()):"-")
                total += rega?rega.toInteger() * partInv.size().toInteger():0
                data.put("totHargaJual",rega?konversi.toRupiah(rega.toInteger() * partInv.size().toInteger()):"-")
                data.put("total",konversi.toRupiah(total.toInteger()))
                reportData.add(data)
        }

        def reportDef = new JasperReportDef(name:'f_lapPenjualanBarangPerSa.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("Lap_PenjualanBarang_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

}
