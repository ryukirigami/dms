package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.ManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.hrd.Karyawan
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.PartInv
import com.kombos.parts.Konversi
import com.kombos.reception.JobInv
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class LaporanHassController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    def index() {}


    def previewData(){
        def kasie = "", adku = "", kabeng = "", kacab = ""
        try {
            kasie = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KEUANGAN%"),'0',session.userCompanyDealer)?.nama
            adku = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KABAG ADKEU%"),'0',session.userCompanyDealer)?.nama
            kabeng = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KEPALA BENGKEL%"),'0',session.userCompanyDealer)?.nama
            kacab = Karyawan.findByJabatanAndStaDelAndBranch(ManPower.findByM014JabatanManPowerIlike("%KEPALA CABANG%"),'0',session.userCompanyDealer)?.nama
        }catch (Exception e){

        }
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Date tgl = new Date().parse('dd/MM/yyyy',params.tanggal)
        Date tglAwal = new Date().parse('dd/MM/yyyy',params.tanggal)
        tglAwal.setDate(1)
        def reportData = new ArrayList()
        def totalBank= 0 , totalPiutang = 0, totalDiscJasa = 0,totalDiscPart = 0, totalHsGr = 0,totalHsbody = 0, totalHsLain = 0,
            totalHasilPart = 0,totalHasilMaterial = 0, totalHasilAdm = 0, totalPpn = 0,totalSublet = 0,
            totalPds = 0, totalGoli = 0,totalWarranty = 0, totalCuci = 0, totalUangMuka= 0
        def hariIniBank= 0 , hariIniPiutang = 0,hariIniDiscJasa = 0,hariIniDiscPart = 0,hariIniHsGr = 0,hariIniHsbody = 0, hariIniHsLain = 0,
            hariIniHasilPart = 0,hariIniHasilMaterial = 0, hariIniHasilAdm = 0, hariIniPpn = 0,hariIniSublet = 0,
            hariIniPds = 0, hariIniGoli = 0,hariIniWarranty = 0, hariIniCuci = 0, hariIniUangMuka = 0

        def kemarinBank= 0 , kemarinPiutang = 0,kemarinDiscJasa = 0,kemarinDiscPart = 0,kemarinHsGr = 0,kemarinHsbody = 0, kemarinHsLain = 0,
            kemarinHasilPart = 0,kemarinHasilMaterial = 0, kemarinHasilAdm = 0, kemarinPpn = 0,kemarinSublet = 0,
            kemarinPds = 0, kemarinGoli = 0, kemarinWarranty = 0, kemarinCuci = 0, kemarinUangMuka = 0
        def cK = InvoiceT701.createCriteria()
        def results2 = cK.list{
            eq("t701StaDel","0");
            isNull("t701NoInv_Reff");
            or{
                isNull("t701StaApprovedReversal")
                eq("t701StaApprovedReversal","1")
            }
            gt("t701TotalInv",0.toDouble())
            ge("t701TglJamInvoice",tglAwal)
            lt("t701TglJamInvoice",tgl)
            eq("companyDealer",companyDealer)
            order("t701NoInv","asc")
        }
        results2.each { inv ->
            if(inv.t701JenisInv=="K"){
                kemarinPiutang += (inv?.t701TotalBayarRp?inv.t701TotalBayarRp:0)
            }else if(inv.t701JenisInv=="T"){
                kemarinBank += (inv.t701TotalBayarRp?inv.t701TotalBayarRp:0)
            }

            def jobInv = JobInv.findAllByInvoiceAndStaDel(inv,"0").each {
                kemarinDiscJasa += it.t702DiscRp?it.t702DiscRp:0
                if(it?.t702xKet?.toUpperCase()?.contains("SUBLET")){
                    kemarinSublet += (it?.t702HargaRp?it?.t702HargaRp:0)
                    kemarinHsLain += (it?.t702HargaRp?it?.t702HargaRp:0)
                }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("GR") || it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("SB")){
                    kemarinHsGr += (it?.t702HargaRp?it?.t702HargaRp:0)

                }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("BP")){
                    kemarinHsbody += (it?.t702HargaRp?it?.t702HargaRp:0)

                }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("ESTIMASI")){
                    if(it.reception.customerIn.tujuanKedatangan.m400Tujuan.toUpperCase()?.contains("GR")){
                        kemarinHsGr += (it?.t702HargaRp?it?.t702HargaRp:0)
                    }else{
                        kemarinHsbody += (it?.t702HargaRp?it?.t702HargaRp:0)
                    }
                }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("DIAGNOSE")){
                    if(it?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.toUpperCase()?.contains("GR")){
                        kemarinHsGr += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }else{
                        kemarinHsbody += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }
                }else{
                    if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("PDS")){
                        kemarinPds += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("OLI")){
                        kemarinGoli += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("WARRANTY")){
                        kemarinWarranty += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("TWC")){
                        kemarinWarranty += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("CUCI")){
                        kemarinCuci += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }
                    kemarinHsLain += (it?.t702HargaRp?it?.t702HargaRp:0)
                }
            }

            def partInv = PartInv.findAllByInvoiceAndT703StaDel(inv,"0").each {
                kemarinDiscPart += it.t703DiscRp?it.t703DiscRp:0
            }
            if(inv?.t701BookingFee || inv?.t701OnRisk){
                kemarinUangMuka += (inv?.t701BookingFee + inv?.t701OnRisk)
            }

            kemarinHasilPart += (inv?.t701PartsRp?inv?.t701PartsRp:0)
            kemarinHasilMaterial += ((inv?.t701MaterialRp?inv?.t701MaterialRp:0) + (inv?.t701OliRp? inv?.t701OliRp : 0))
            kemarinHasilAdm += ((inv?.t701MateraiRp?inv?.t701MateraiRp:0)+(inv?.t701AdmRp?inv?.t701AdmRp:0))
            kemarinPpn += inv?.t701PPnRp?inv.t701PPnRp:0
            if(kemarinSublet.toInteger()==0){
                kemarinSublet += inv?.t701SubletRp?inv.t701SubletRp:0
                kemarinHsLain += inv?.t701SubletRp?inv.t701SubletRp:0
            }
        }

        def c = InvoiceT701.createCriteria()
        def results = c.list{
            eq("t701StaDel","0");
            ge("t701TglJamInvoice",tgl)
            lt("t701TglJamInvoice",tgl+1)
            eq("companyDealer",companyDealer)
            order("t701NoInv","asc")
        }

        int count = 0
          int countNotBalance = 0
        results.each { inv ->
            def bank= 0 , piutang = 0,discJasa = 0,discPart = 0,hsGr = 0, hsBody = 0, hsLain = 0,
                hasilPart =0 , hasilMaterial = 0, hasilAdm = 0, ppn = 0,sublet=0, pds = 0, goli = 0, warranty = 0, cuci = 0,
                uangMuka = 0
            def data = [:]
            count = count + 1
            data.put("noUrut",count)
            data.put("companyDealer",companyDealer)
            data.put("tanggal","Tanggal : " + tgl.format("dd MMMM yyyy"))
            data.put("mutasiHariIni","Mutasi " + tgl.format("dd MMMM yyyy"))
            if(tgl.format("dd").toString().equals("01")){
                data.put("mutasiKemarin","")
            }else{
                data.put("mutasiKemarin","Mutasi s.d " + ((tgl-1).format("dd MMMM yyyy")))
            }
            data.put("mutasiTotal","Mutasi s.d " + tgl.format("dd MMMM yyyy"))
            data.put("nomorWo",inv?.reception?.t401NoWO)
            data.put("tglWo",inv?.reception?.t401TanggalWO ? inv?.reception?.t401TanggalWO?.format("dd/MM/yyyy") : "")
            String nomorSI = inv?.t701NoInv
            if(inv.t701StaApprovedReversal=='0'){
                nomorSI += "*"
            }else if(inv.t701NoInv_Reff!=null){
                nomorSI += "**"
            }
            def JobChek = ""
            data.put("nomorSi",nomorSI)
            data.put("pelanggan",inv?.t701Customer)
            if(inv.t701NoInv_Reff || inv.t701StaApprovedReversal=='0'){
                bank= 0;  piutang = 0; discJasa = 0;discPart = 0;hsGr = 0; hsBody = 0; hsLain = 0;
                hasilPart =0 ; hasilMaterial = 0; hasilAdm = 0; ppn = 0; sublet=0;pds = 0; goli = 0; warranty = 0; cuci = 0;
                uangMuka = 0
            }else{
                if(inv.t701JenisInv=="K"){
                    piutang += (inv?.t701TotalBayarRp?inv?.t701TotalBayarRp:0)
                }else if(inv.t701JenisInv=="T"){
                    bank += (inv?.t701TotalBayarRp?inv?.t701TotalBayarRp:0)
                }

                def jobInv = JobInv.findAllByInvoiceAndStaDel(inv,"0").each {
                    discJasa += it.t702DiscRp?it.t702DiscRp:0
                    if(it?.t702xKet?.toUpperCase()?.contains("SUBLET")){
                        sublet += (it.t702HargaRp?it.t702HargaRp:0)
                        hsLain += (it.t702HargaRp?it.t702HargaRp:0)
                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("GR") || it.operation.kategoriJob.m055KategoriJob.toUpperCase().contains("SB")){
                        hsGr += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("BP")){
                        hsBody += (it?.t702HargaRp?it?.t702HargaRp:0)

                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("ESTIMASI")){
                        if(it?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.toUpperCase()?.contains("GR")){
                            hsGr += (it?.t702HargaRp?it?.t702HargaRp:0)
                        }else{
                            hsBody += (it?.t702HargaRp?it?.t702HargaRp:0)
                        }
                    }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("DIAGNOSE")){
                        if(it?.reception?.customerIn?.tujuanKedatangan?.m400Tujuan?.toUpperCase()?.contains("GR")){
                            hsGr += (it?.t702HargaRp?it?.t702HargaRp:0)
                        }else{
                            hsBody += (it?.t702HargaRp?it?.t702HargaRp:0)

                        }
                    }else{
                        if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("PDS")){
                            pds += (it?.t702HargaRp?it?.t702HargaRp:0)

                        }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("OLI")){
                            goli += (it?.t702HargaRp?it?.t702HargaRp:0)

                        }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("WARRANTY")) {
                            warranty += (it?.t702HargaRp ? it?.t702HargaRp : 0)

                        }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("TWC")){
                            warranty += (it?.t702HargaRp?it?.t702HargaRp:0)

                        }else if(it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()?.contains("CUCI")){
                            cuci += (it?.t702HargaRp?it?.t702HargaRp:0)

                        }
                        JobChek = it?.operation?.kategoriJob?.m055KategoriJob?.toUpperCase()
                        hsLain += (it?.t702HargaRp?it?.t702HargaRp:0)
                    }
                }

                def partInv = PartInv.findAllByInvoiceAndT703StaDel(inv,"0").each {
                    discPart += it.t703DiscRp?it.t703DiscRp:0
                }
                if(inv?.t701BookingFee || inv?.t701OnRisk){
                    uangMuka += (inv?.t701BookingFee + inv?.t701OnRisk)
                }
                hasilPart = (inv.t701PartsRp?inv.t701PartsRp:0)
                hasilMaterial = (inv?.t701MaterialRp+inv?.t701OliRp)
                hasilAdm = (inv?.t701MateraiRp+inv?.t701AdmRp)
                ppn = (inv.t701PPnRp?inv.t701PPnRp:0)
                if(sublet.toInteger()==0){
                    sublet += inv?.t701SubletRp?inv.t701SubletRp:0
                    hsLain += inv?.t701SubletRp?inv.t701SubletRp:0
                }
            }
            hariIniBank += bank
            hariIniPiutang += piutang

            totalPiutang = (hariIniPiutang + kemarinPiutang)
            totalBank = (hariIniBank + kemarinBank)
            hariIniHsGr += hsGr
            totalHsGr = (hariIniHsGr+kemarinHsGr)

            hariIniHsbody += hsBody
            totalHsbody = (hariIniHsbody+kemarinHsbody)

            hariIniSublet += sublet
            totalSublet = (hariIniSublet+kemarinSublet)

            hariIniPds += pds
            totalPds = (hariIniPds + kemarinPds)

            hariIniGoli  += goli
            totalGoli = (hariIniGoli +kemarinGoli)

            hariIniWarranty  += warranty
            totalWarranty = (hariIniWarranty +kemarinWarranty)

            hariIniCuci  += cuci
            totalCuci = (hariIniCuci + kemarinCuci)

            hariIniHsLain += hsLain
            totalHsLain = (hariIniHsLain+kemarinHsLain)

            hariIniDiscJasa += discJasa
            totalDiscJasa = (hariIniDiscJasa + kemarinDiscJasa)

            hariIniDiscPart += discPart
            totalDiscPart = hariIniDiscPart + kemarinDiscPart

            hariIniUangMuka += uangMuka
            totalUangMuka = (hariIniUangMuka + kemarinUangMuka)

            hariIniHasilPart+= hasilPart
            hariIniHasilMaterial += hasilMaterial
            hariIniHasilAdm += hasilAdm
            hariIniPpn += ppn

            totalHasilPart = hariIniHasilPart + kemarinHasilPart
            totalHasilMaterial = hariIniHasilMaterial + kemarinHasilMaterial
            totalHasilAdm = hariIniHasilAdm + kemarinHasilAdm
            totalPpn = hariIniPpn + kemarinPpn

            data.put("bank",konversi.toRupiah(bank))
            data.put("piutang",konversi.toRupiah(piutang))
            data.put("hariIniBank",konversi.toRupiah(hariIniBank))
            data.put("hariIniPiutang",konversi.toRupiah(hariIniPiutang))
            data.put("kemarinBank",konversi.toRupiah(kemarinBank))
            data.put("kemarinPiutang",konversi.toRupiah(kemarinPiutang))
            data.put("totalBank",konversi.toRupiah(totalBank))
            data.put("totalPiutang",konversi.toRupiah(totalPiutang))
            data.put("discJasa",konversi.toRupiah(discJasa))
            data.put("discPart",konversi.toRupiah(discPart))
            data.put("hariIniDiscJasa",konversi.toRupiah(hariIniDiscJasa))
            data.put("hariIniDiscPart",konversi.toRupiah(hariIniDiscPart))
            data.put("kemarinDiscJasa",konversi.toRupiah(kemarinDiscJasa))
            data.put("kemarinDiscPart",konversi.toRupiah(kemarinDiscPart))
            data.put("totalDiscJasa",konversi.toRupiah(totalDiscJasa))
            data.put("totalDiscPart",konversi.toRupiah(totalDiscPart))
            data.put("uangMuka",konversi.toRupiah(uangMuka))
            data.put("hariIniUangMuka",konversi.toRupiah(hariIniUangMuka))
            data.put("kemarinUangMuka",konversi.toRupiah(kemarinUangMuka))
            data.put("totalUangMuka",konversi.toRupiah(totalUangMuka))
            data.put("hsGr",konversi.toRupiah(hsGr))
            data.put("hsBody",konversi.toRupiah(hsBody))
            data.put("hsLain",konversi.toRupiah(hsLain))
            data.put("hariIniHsGr",konversi.toRupiah(hariIniHsGr))
            data.put("hariIniHsBody",konversi.toRupiah(hariIniHsbody))
            data.put("hariIniHsLain",konversi.toRupiah(hariIniHsLain))
            data.put("kemarinHsGr",konversi.toRupiah(kemarinHsGr))
            data.put("kemarinHsBody",konversi.toRupiah(kemarinHsbody))
            data.put("kemarinHsLain",konversi.toRupiah(kemarinHsLain))
            data.put("totalHsGr",konversi.toRupiah(totalHsGr))
            data.put("totalHsBody",konversi.toRupiah(totalHsbody))
            data.put("totalHsLain",konversi.toRupiah(totalHsLain))
            data.put("hasilPart",konversi.toRupiah(hasilPart))
            data.put("hasilMaterial",konversi.toRupiah(hasilMaterial))
            data.put("hasilAdm",konversi.toRupiah(hasilAdm))
            data.put("ppn",konversi.toRupiah(ppn))
            data.put("hariIniHasilPart",konversi.toRupiah(hariIniHasilPart))
            data.put("hariIniHasilMaterial",konversi.toRupiah(hariIniHasilMaterial))
            data.put("hariIniHasilAdm",konversi.toRupiah(hariIniHasilAdm))
            data.put("hariIniPpn",konversi.toRupiah(hariIniPpn))
            data.put("kemarinHasilPart",konversi.toRupiah(kemarinHasilPart))
            data.put("kemarinHasilMaterial",konversi.toRupiah(kemarinHasilMaterial))
            data.put("kemarinHasilAdm",konversi.toRupiah(kemarinHasilAdm))
            data.put("kemarinPPn",konversi.toRupiah(kemarinPpn))
            data.put("totalHasilPart",konversi.toRupiah(totalHasilPart))
            data.put("totalHasilMaterial",konversi.toRupiah(totalHasilMaterial))
            data.put("totalHasilAdm",konversi.toRupiah(totalHasilAdm))
            data.put("totalPpn",konversi.toRupiah(totalPpn))


            data.put("sublet",konversi.toRupiah(sublet))
            data.put("hariIniSublet",konversi.toRupiah(hariIniSublet))
            data.put("kemarinSublet",konversi.toRupiah(kemarinSublet))
            data.put("totalSublet",konversi.toRupiah(totalSublet))

            data.put("pds",konversi.toRupiah(pds))
            data.put("hariIniPds",konversi.toRupiah(hariIniPds))
            data.put("kemarinPds",konversi.toRupiah(kemarinPds))
            data.put("totalPds",konversi.toRupiah(totalPds))

            data.put("goli",konversi.toRupiah(goli))
            data.put("hariIniGoli",konversi.toRupiah(hariIniGoli))
            data.put("kemarinGoli",konversi.toRupiah(kemarinGoli))
            data.put("totalGoli",konversi.toRupiah(totalGoli))

            data.put("warranty",konversi.toRupiah(warranty))
            data.put("hariIniWarranty",konversi.toRupiah(hariIniWarranty))
            data.put("kemarinWarranty",konversi.toRupiah(kemarinWarranty))
            data.put("totalWarranty",konversi.toRupiah(totalWarranty))

            data.put("cuci",konversi.toRupiah(cuci))
            data.put("hariIniCuci",konversi.toRupiah(hariIniCuci))
            data.put("kemarinCuci",konversi.toRupiah(kemarinCuci))
            data.put("totalCuci",konversi.toRupiah(totalCuci))

            data.put("kacab",kacab)
            data.put("kabeng",kabeng)
            data.put("kasie",kasie)
            data.put("adku",adku)
            reportData.add(data)
            def debit = bank + piutang + discJasa + discPart + uangMuka
            def kredit = hsGr + hsBody+ hsLain+ hasilPart+ hasilMaterial + hasilAdm + ppn
            if(debit!= kredit){
                countNotBalance++
            }
            def totLain = goli + warranty + cuci + pds + sublet
            if (totLain!= hsLain){
            }
        }
        if(reportData.size()==0){
            def data = [:]
            count = count + 1
            data.put("noUrut",count)
            data.put("companyDealer",companyDealer)
            data.put("tanggal","Tanggal : " + tgl.format("dd MMMM yyyy"))
            data.put("mutasiHariIni","Mutasi " + tgl.format("dd MMMM yyyy"))
            if(tgl.format("dd").toString().equals("01")){
                data.put("mutasiKemarin","")
            }else{
                data.put("mutasiKemarin","Mutasi s.d " + ((tgl-1).format("dd MMMM yyyy")))
            }
            data.put("mutasiTotal","Mutasi s.d " + tgl.format("dd MMMM yyyy"))
            data.put("kacab",kacab)
            data.put("kabeng",kabeng)
            data.put("kasie",kasie)
            data.put("adku",adku)
            reportData.add(data)
        }

        def reportDef = new JasperReportDef(name:'f_lapHass.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("LAP_HASS("+companyDealer.id.toString()+")-"+tgl.format("ddMMyyyy")+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
