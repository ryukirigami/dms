package com.kombos.finance

import com.kombos.administrasi.ManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.hrd.Karyawan
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.*
import com.kombos.reception.InvoiceSublet
import com.kombos.utils.MoneyUtil
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class TransactionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def transactionService

    def uangMukaSubletJournalService

    def hutangTokoLuarJournalService

    def pembayaranPiutangKreditJournalService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def jasperService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render transactionService.datatablesList(params) as JSON
    }

    def create() {
        def result = transactionService.create(params)

        if (!result.error)
            return [transactionInstance: result.transactionInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.companyDealer = session.userCompanyDealer
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = transactionService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Transaction", result.transactionInstance.id])
            redirect(action: 'show', id: result.transactionInstance.id)
            return
        }

        render(view: 'create', model: [transactionInstance: result.transactionInstance])
    }

    def show(Long id) {
        def result = transactionService.show(params)

        if (!result.error)
            return [transactionInstance: result.transactionInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = transactionService.show(params)

        if (!result.error)
            return [transactionInstance: result.transactionInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.companyDealer = session.userCompanyDealer
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated   = datatablesUtilService?.syncTime()
        def result = transactionService.update(params)
        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Transaction", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [transactionInstance: result.transactionInstance.attach()])
    }

    def delete() {
        def result = transactionService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Transaction", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Transaction, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def pembayaranSublet() {
        [bankInstances: BankAccountNumber.list().bank.m702NamaBank]
    }

    def nomorPOSubletDT() {
        params.companyDealer = session?.userCompanyDealer
        render transactionService.nomorPOSublistDT(params) as JSON
    }

    def getSubletJumlahBayar() {
        def ret = [:]
        def harga = 0
        def bayar = 0
        def hasil = 0
        if (params.nomorInvoice) {
            def invoiceSublet = InvoiceSublet.findAllByT413NoInv(params.nomorInvoice)
            if (invoiceSublet) {
                invoiceSublet.each {
                    harga += it?.t413JmlSublet?it?.t413JmlSublet:0
                    bayar = it?.t413JmlBayar?it?.t413JmlBayar:0
                }
                hasil = harga - bayar
                ret.val = hasil

            } else{
                ret.error = "Invoice Sublet tidak ditemukan"
            }
        } else {
            ret.error = "Mohon masukan nomor Invoice"
        }
        render ret as JSON
    }

    def saveSublet() {

    }

    def saveByDocCategory() {
        params.companyDealer = session.userCompanyDealer
        def result = transactionService.saveByDocCategory(params)

        def remap = [:]
        def bankAcc = new BankAccountNumber() ;
        if(params.bank){
            bankAcc = BankAccountNumber.get(params.bank.toLong())
        }
        remap.details  = params.details
        remap.tipeBayar  = params.transactionType=="KAS" ? "KAS" : "BANK"
        remap.namaBank   = params.bank && bankAcc ? bankAcc?.bank?.m702NamaBank : null
        remap.idBank     = params.bank ? params.bank : null
        remap.docNumber  = params.docNumber
        remap.transactionDate  = params.transactionDate
        remap.totalBiaya = (params.amount as String).toBigDecimal()
        remap.transaction = result?.transactionInstance

        if ((params.typeJournal as String).equals("SUBLET")) {
            uangMukaSubletJournalService.createJournal(remap)
        } else if ((params.typeJournal as String).equals("TAGIHAN")) {
            remap.tambahanBiaya = Float.parseFloat(params.tambahanBiaya)
            def coll = Collection.findByInvoiceT071AndStaDel(InvoiceT701.findByT701NoInv(params.docNumber),"0")
            def nColl = new Collection()
            nColl.properties = coll.properties
            nColl.paidAmount = nColl.paidAmount + (remap.totalBiaya + remap.tambahanBiaya)
            if(nColl.paidAmount >= nColl.amount){
                nColl.paymentStatus = "LUNAS";
                try {
                    def inv = InvoiceT701.findByT701NoInv(params.docNumber).id
                    def ubhInv = InvoiceT701.get(inv)
                    ubhInv.t701StaSettlement = '1'
                    ubhInv.save(flush: true);
                }catch (Exception e){
                }
            }else{
                nColl.paymentStatus = "BELUM"
            }
            nColl.lastUpdated = datatablesUtilService?.syncTime()
            nColl.subType = coll.subType
            nColl.subLedger = coll.subLedger
            nColl.save(flush: true)
            coll.paymentStatus = "PAID"
            coll.lastUpdProcess = "PAID"
            coll?.lastUpdated = datatablesUtilService?.syncTime()
            coll.paidAmount = coll.paidAmount + (remap.totalBiaya + remap.tambahanBiaya)
            coll.staDel = "1"
            coll.save(flush: true)
            remap.subtype = nColl.subType.id
            remap.subLedger =  nColl.subLedger
            pembayaranPiutangKreditJournalService.createJournal(remap)
        } else if ((params.typeJournal as String).equals("HUTANG")) {
            def tgglInv = new Date().parse("dd/MM/yyyy",params?.tgglInv);
            def collec = Collection167.findByGoodsReceiveAndStaDel(GoodsReceive.findByT167NoReff(params.docNumber),'0')
            collec.paidAmount = ( Float.parseFloat(params.amount) + collec.paidAmount)
            collec.paymentDate = datatablesUtilService.syncTime()
            if(collec.paidAmount == collec.amount){
                collec.paymentStatus = "LUNAS"
            }
            collec.save(flush: true)
            String staTunaiKredit = "";
            def ppn = collec.goodsReceive?.t167Ppn;
            def jumlah = GoodsReceiveDetail.findByGoodsReceive(collec.goodsReceive)*.t167NetSalesPrice
            String noReceive = "";
            Long idReceive = 0.toLong()
            staTunaiKredit = collec?.goodsReceive?.t167TipeBayar
            ppn=collec?.goodsReceive?.t167Ppn
            noReceive=collec?.goodsReceive?.t167NoReff
            idReceive = collec?.goodsReceive?.id

            remap.ppn = ppn
            remap.goodsReceive = noReceive
            remap.idGoodsReceive = noReceive
            if(staTunaiKredit?.toUpperCase()?.contains("TUNAI")){
                remap.idGoodsReceive = collec?.goodsReceive.id
                remap.totalBiaya = (params.amount as String).toBigDecimal()
                def servicePPB = new PPBJournalService()
                servicePPB.createJournalTunai(remap)
            }else{
                hutangTokoLuarJournalService.createJournal(remap)
            }
        }

        render result as JSON
    }

    def saveTransaction() {
        def backToHome = false
        def mssg = "Data Goods dengan kode "
        if(params.isPenjualan){
            def delivOrder = DeliveryOrder.findByDoNumberIlikeAndStaDelAndCompanyDealer("%"+params.docNumber+"%","0",session?.userCompanyDealer)
            if(delivOrder){
                def details = SalesOrderDetail.findAllBySorNumberAndStaDel(delivOrder?.sorNumber,"0")
                for(cari in details){
                    def klas = KlasifikasiGoods.findByGoods(cari?.materialCode)
                    if(!klas){
                        backToHome = true
                        mssg+=cari?.materialCode?.m111ID+" , "
                    }
                }
            }
        }
        if(backToHome){
            def hasil = [:]
            mssg+="\nBelum memiliki klasifikasi goods\nMohon dilengkapi"
            hasil.error = mssg
            render hasil as JSON
        }else {
            params.companyDealer = session.userCompanyDealer
            params.dateCreated = datatablesUtilService?.syncTime()
            params.lastUpdated = datatablesUtilService?.syncTime()
            def result = transactionService.saveByDocCategory(params)

            def remap = [:]

            remap.tipeBayar  = params.transactionType=="KAS" ? "KAS" : "BANK"
            remap.idBank     = params.bank ? params.bank : "-"
            remap.docNumber  = params.docNumber
            remap.tipeBeli   = params.tipeBeli
            remap.totalBiaya = Double.parseDouble(params.amount)
            remap.sebelumPpn = params?.sebelumPpn
            remap.transactionDate = params?.transactionDate
            remap.ppn = 0
            remap.transaction = result.transactionInstance
            if(params.isPenjualan){
                def delivOrder = DeliveryOrder.findByDoNumberIlikeAndStaDelAndCompanyDealer("%"+params.docNumber+"%","0",session?.userCompanyDealer);
                if(delivOrder){
                    delivOrder?.paymentStatus = "LUNAS";
                    delivOrder.save(flush: true)
                    if(delivOrder?.sorNumber?.ppn){
                        remap.ppn = delivOrder?.sorNumber?.ppn
                    }
                }
            }
            if(remap.tipeBeli=="Tunai"){
                def service = new PenerimaanPenjualanJournalService()
                service.createJournal(remap)
            }else{
                def service = new PenerimaanPenjualanJournalService()
                service.createJournalPembayaranKredit(remap)
            }

            render result as JSON
        }
    }

    def cariTransaksiOld() {

    }

    def cariTransaksi() {

        //kalau versi lama code kebawah hapus Saja
        def htmlData = ""

        if(params.dari=="cari"){
            params?.companyDealer = session?.userCompanyDealer
            def data = transactionService.cariTrxList(params)
            def no = 0
            data.each{
                no++;
                def clas = "class='even'"
                if(no % 2 == 1){
                    clas = "class='odd'"
                }
                htmlData += "<tr "+clas+">"
                htmlData += "<td> <a href='javascript:void(0);'  onclick='showDetail("+it.id+");'>" + it.transactionCode+ " </a></td>"
                htmlData += "<td>" + it.transactionDate+ "</td>"
                htmlData += "<td>" + it.transactionType + "</td>"
                htmlData += "<td>" + it.amount+ "</td>"
                htmlData += "<td>" + it.documentCategory+ "</td>"
                htmlData += "<td>" + it.docNumber+ "</td>"
                htmlData += "<td>" + it.description+ "</td>"
                htmlData += "<td>" + it.approval+ "</td>"
                htmlData += "</tr>"
            }

            if(data.size()==0){
                htmlData += "<tr class='odd'>"
                htmlData += "<td class='dataTables_empty' valign='top' colspan='8'>No Data Available in table</td>"
                htmlData += "</tr>"
            }

            params.jumlahdata = no

            if(params.sCriteria_transactionDate_dp){
                params.sCriteria_transactionDate_dp = new Date().parse("dd/MM/yyyy",params.sCriteria_transactionDate_dp)
            }
            if(params.sCriteria_transactionDate_dp_to){
                params.sCriteria_transactionDate_dp_to = new Date().parse("dd/MM/yyyy",params.sCriteria_transactionDate_dp_to)
            }
        }else{
            htmlData += "<tr class='odd'>"
            htmlData += "<td class='dataTables_empty' valign='top' colspan='8'>No Data Available in table</td>"
            htmlData += "</tr>"
        }
        [htmlData: htmlData, params : params]
    }
    def showTransactionDetail() {
        def tr = Transaction.findById(Long.parseLong(params.id))
        def result = tr.journal
        def test = JournalDetail.executeQuery("from JournalDetail jd where journal.id = ${result?.id} order by debitAmount desc, accountNumber.accountNumber asc ")
        tr.journal.docNumber = tr.transactionCode
        def konversi = new Konversi()
        if(result)
            [journalInstance: result,journalDetailInstance :test, konversi : konversi ]

    }
    def pembayaranHutang() {
        [bankInstances: BankAccountNumber.list().bank.m702NamaBank]
    }

    def penerimaanPenjualanParts() {
        [bankInstances: BankAccountNumber.list().bank.m702NamaBank]
    }

    def penerimaanHasilTagihanServiceInvoice() {
        [bankInstances: BankAccountNumber.list().bank.m702NamaBank]
    }

    def nomorKwitansiDT() {
        params.companyDealer = session?.userCompanyDealer;
        render transactionService.nomorKwitansiDT(params) as JSON
    }

    def nomorInvoiceT701DT() {
        params.companyDealer = session.userCompanyDealer
        render transactionService.nomorKwitansiT701DT(params) as JSON
    }

    def nomorDeliveryOrder() {
        params.companyDealer = session.userCompanyDealer
        render transactionService.nomorDeliveryOrder(params) as JSON
    }

    def getTransactionByDocNumber() {
        def jumTagihan = 0
        if(params.dari){
            def ppnParts = 0
            def goodsreceive = GoodsReceive.findByT167NoReffAndCompanyDealerAndStaDel(params.docNumber,session?.userCompanyDealer,"0")
            def dataIn = GoodsReceiveDetail.findAllByGoodsReceive(goodsreceive)
            dataIn.each {
                ppnParts=it?.goodsReceive?.t167Ppn
                jumTagihan += it?.t167NetSalesPrice
            }
            if(ppnParts>0){
                jumTagihan = jumTagihan + (jumTagihan*ppnParts/100)
            }
        }else {
            def invoiceT071 = InvoiceT701.findByT701NoInv(params.docNumber)
            jumTagihan = invoiceT071?.t701TotalBayarRp
        }
        def transaction = Transaction.findAllByDocNumberAndCompanyDealerAndStaDel(params.docNumber,session?.userCompanyDealer,'0')
        def ret = [:]
        ret.tagihan = jumTagihan
        if (transaction)
            ret.result = transaction
        else
            ret.result = null

        render ret as JSON
    }

    def getTransactionPartsByDocNumber() {
        def dO = DeliveryOrder.findByDoNumberAndCompanyDealerAndStaDel(params.docNumber,session?.userCompanyDealer,"0");
        def soDetail = SalesOrderDetail.findAllBySorNumberAndStaDel(dO?.sorNumber,"0")
        def jumlahTagih = 0
        def totSebelumPPN = 0.00
        soDetail.each {
            def discTemp = it.discount
            def totalTemp = discTemp && discTemp!=0 ? (it.quantity * it.unitPrice - (it.quantity * it.unitPrice  * discTemp / 100)) : (it.quantity * it.unitPrice)
            jumlahTagih+=(totalTemp)
            totSebelumPPN+=totalTemp
        }

        def ppn = dO?.sorNumber?.ppn
        if(ppn >= 0){
            jumlahTagih = (jumlahTagih + (ppn/100*jumlahTagih))
        }else {
            jumlahTagih = (jumlahTagih + (10/100*jumlahTagih))
        }
        def sudahBayar = 0
        def ret = [:]

        ret.tipe = dO ? dO?.sorNumber?.paymentType : ""
        ret.tagihan = Math.ceil(jumlahTagih)
        ret.sudahBayar = sudahBayar
        ret.sebelumPpn = totSebelumPPN
        render ret as JSON
    }

    def getTagihanPelanggan() {
        def invoiceT071 = InvoiceT701.findByT701NoInv(params.noInv)
        def collection
        def ret = [:]

        ret.amount = null

        if (invoiceT071) {
            collection = Collection.findByInvoiceT071AndStaDel(invoiceT071,"0")
            if (collection) {
                ret.amount = collection.amount //jml tagihan
                ret.serviceAdvisor = collection?.invoiceT071?.reception?.t401NamaSA.toUpperCase() //Sa
                ret.paidAmount = collection.paidAmount //jml tagihan
            }
        }

        render ret as JSON
    }

    def printKW(){
        def konversi = new Konversi()
        def reportData =[]
        def trx = Transaction.get(params.idTrx);
        String lblTambahan = ""
        String noTambahan = ""
        String jenis = ""
        String pembayar = ""
        String tujuan = ""
        Double jml = 0
        if(params.noDO){
            def delivOrder = DeliveryOrder.findByDoNumberIlikeAndStaDelAndCompanyDealer("%"+params.noDO+"%","0",session?.userCompanyDealer)
            lblTambahan = "Nomor DO :"
            noTambahan = delivOrder?.doNumber
            jenis = "(Penjualan Parts)"
            pembayar = (delivOrder?.sorNumber?.customerName?delivOrder?.sorNumber?.customerName:"")
            tujuan = trx?.description
            jml = trx?.amount
        }
        if(params.noInv){
            def invoice = InvoiceT701.findByT701NoInvIlikeAndT701StaDelAndCompanyDealer("%"+params.noInv+"%","0",session?.userCompanyDealer)
            lblTambahan = "Nomor Inv. :"
            noTambahan = invoice?.t701NoInv
            jenis = "(Invoice Kredit)"
            pembayar = invoice?.t701Customer
            tujuan = "Pembayaran invoice kredit a.n. "+invoice?.t701Customer
            jml = trx?.amount
        }
        def user = User.findByUsernameAndStaDel(org?.apache?.shiro?.SecurityUtils?.subject?.principal?.toString(),"0")
        def moneyUtil = new MoneyUtil()
        def kabeng = Karyawan?.findByJabatanAndBranch(ManPower?.findByM014JabatanManPowerIlikeAndStaDel("%"+"KEPALA BENGKEL"+"%","0"),session?.userCompanyDealer)
        reportData <<[
                lblTambahan : lblTambahan,
                noTambahan  : noTambahan,
                jenis       : jenis,
                no          : "",
                perhatian   : "* SPAREPARTS YANG SUDAH DIBELI TIDAK BISA DIKEMBALIKAN ",
                perhatian2   : "",
                perhatian3   : "PERHATIAN!!  : ",
                pembayar    : pembayar,
                terbilang   : moneyUtil?.convertMoneyToWords(jml),
                tujuan      : tujuan+ " / "+trx?.transactionType,
                jumlah      : konversi.toRupiah(jml),
                tggl        : trx?.companyDealer?.kabKota?.m002NamaKabKota+" , "+new Date().format("dd MMMM yyyy"),
                tgglPrint   : datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm:ss"),
                kasir       : (user ? user.fullname : ""),
                kabeng      : kabeng ? kabeng?.nama : ""
        ]
        List<JasperReportDef> reportDefList = []
        def reportDef = new JasperReportDef(name:'Kwitansi.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        reportDefList.add(reportDef);

        def file = File.createTempFile("Kwitansi_",".pdf")

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${file.getName()}")

        response.outputStream << file.newInputStream()
    }
}
