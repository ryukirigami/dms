package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.Konversi
import groovy.sql.Sql
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRTableModelDataSource
import net.sf.jasperreports.engine.export.JExcelApiExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import javax.swing.table.DefaultTableModel

class EvaluasiTargetController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
    def koneksiToOracleService
    def evaluasiTargetService
    def jasperService
    def DefaultTableModel tableModel;
    def rootPath;
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
    def index() {}
    def generate(def params){
        def bulanTahun  = params?.bulanTahun
        Sql sql = Sql.newInstance(koneksiToOracleService.url, koneksiToOracleService.usernm, koneksiToOracleService.passwd)
        def query = "call EVALUASI_TARGET('"+session.userCompanyDealerId+"','"+bulanTahun+"')";
        sql.call(query)
        sql.commit();
    }
    def previewData(){
        params.bulanTahun = params.bulan+""+params?.tahun
        if((params?.bulan as int) < 10 ){
            params.bulanTahun = "0"+params.bulan+""+params?.tahun
        }
        try {
            generate(params);
        }catch (Exception e){

        }
        params.file = ".xls"
        params.buntut = "application/vnd.ms-excel";
        rootPath = request.getSession().getServletContext().getRealPath("/") + "reports/";

        //parameter bulan tanggal
        Calendar cal = Calendar.getInstance()
        cal.set(params?.tahun as int,(params?.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        String bulanParam =  params?.tahun + "-" + params?.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        params.tgl = tgl
        params.tgl2 = tgl2

        tableModelData(params);
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        def workshop = CompanyDealer.get(params?.companyDealer?.toLong())
        parameters.put("SUBREPORT_DIR", rootPath);
        parameters.put("workshop", workshop);
        parameters.put("periode", periode);
        parameters.put("tahun", ((params?.tahun as int) - 1).toString());
        parameters.put("tahun2", ((params?.tahun as int) - 2).toString());
        JRDataSource dataSourceGR = new JRTableModelDataSource(dataModel(params,'GR'));
        JRDataSource dataSourceBP = new JRTableModelDataSource(dataModel(params,'BP'));
        JRDataSource dataSourceGAB = new JRTableModelDataSource(dataModel(params,'GAB'));
        //tahun lalu
        JRDataSource dataSourceGRLY = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-1),'GR','1'));
        JRDataSource dataSourceGRLY_ = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-1),'GR','0'));
        JRDataSource dataSourceBPLY = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-1),'BP','1'));
        JRDataSource dataSourceBPLY_ = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-1),'BP','0'));
        JRDataSource dataSourceGABLY = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-1),'GAB','1'));
        JRDataSource dataSourceGABLY_ = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-1),'GAB','0'));
        //2tahun lalu
        JRDataSource dataSourceGRLY2 = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-2),'GR','0'));
        JRDataSource dataSourceBPLY2 = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-2),'BP','0'));
        JRDataSource dataSourceGABLY2 = new JRTableModelDataSource(dataModelLY(params,((params.tahun as int)-2),'GAB','0'));
        parameters.put("summaryDataSource", dataSourceGR);
        parameters.put("summaryBP", dataSourceBP);
        parameters.put("summaryGAB", dataSourceGAB);
        parameters.put("summaryGRLY", dataSourceGRLY);
        parameters.put("summaryGRLY_", dataSourceGRLY_);
        parameters.put("summaryBPLY", dataSourceBPLY);
        parameters.put("summaryBPLY_", dataSourceBPLY_);
        parameters.put("summaryGABLY", dataSourceGABLY);
        parameters.put("summaryGABLY_", dataSourceGABLY_);
        parameters.put("summaryGRLY2", dataSourceGRLY2);
        parameters.put("summaryBPLY2", dataSourceBPLY2);
        parameters.put("summaryGABLY2", dataSourceGABLY2);

        JasperPrint jasperPrint = JasperFillManager.fillReport(rootPath + "evaluasiTarget.jasper", parameters,
                new JRTableModelDataSource(tableModel));
        File pdf = File.createTempFile("LapEvt("+params?.companyDealer?.toString()+")_" +tgl.format("MMyyyy") + "-", params.file);
        pdf.deleteOnExit();
        JExcelApiExporter exporterXLS = new JExcelApiExporter();
        exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
        exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response?.getOutputStream());
        response.setHeader("Content-Type", params.buntut);
        response.setHeader("Content-disposition", "attachment;filename=${pdf.name}");
        exporterXLS.exportReport();


    }
    def dataModel(def params, def type){
        def String[] columnNames = ["column_0", "field1", "field2", "field3", "field4", "field5", "field6", "field7", "field8", "field9", "field10", "field11", "field12"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = evaluasiTargetService.dataTablesReport(params,type);
            results.each {
                def Object[] object = [
                    String.valueOf(it.column_0?:""),
                    String.valueOf(it.field1?:""),   //URAIAN
                    String.valueOf(it.field2?:""),
                    String.valueOf(it.field3?:""),
                    String.valueOf(it.field4?:""),
                    String.valueOf(it.field5?:""),
                    String.valueOf(it.field6?:""),
                    String.valueOf(it.field7?:""),
                    String.valueOf(it.field8?:""),
                    String.valueOf(it.field9?:""),
                    String.valueOf(it.field10?:""),
                    String.valueOf(it.field11?:""),
                    String.valueOf(it.field12?:"")

                ]
                model.addRow(object)
            }
        }catch (Exception e){

        }
        return model;
    }
    def dataModelLY(def params, def tahun, def type, def isTarget){
        def String[] columnNames = ["column_0", "field1", "field2", "field3", "field4", "field5", "field6", "field7", "field8", "field9", "field10", "field11", "field12"];
        def DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        try {
            def results = evaluasiTargetService.dataTablesReportLastYear(params,tahun,type,isTarget);
            results.each {
                def Object[] object = [
                        String.valueOf(it.column_0?:""),
                        String.valueOf(it.field1?:""), //URAIAN
                        String.valueOf(it.field2?:""),
                        String.valueOf(it.field3?:""),
                        String.valueOf(it.field4?:""),
                        String.valueOf(it.field5?:""),
                        String.valueOf(it.field6?:""),
                        String.valueOf(it.field7?:""),
                        String.valueOf(it.field8?:""),
                        String.valueOf(it.field9?:""),
                        String.valueOf(it.field10?:""),
                        String.valueOf(it.field11?:""),
                        String.valueOf(it.field12?:"")
                ]
                model.addRow(object)
            }
        }catch (Exception e){

        }
        return model;
    }
    def tableModelData(def params){
        def String[] columnNames = ["static_text"];
        def String[][] data = [["light"]];
        tableModel = new DefaultTableModel(data, columnNames);
    }
}