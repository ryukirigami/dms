package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.InvoiceT701
import com.kombos.parts.GoodsHargaJual
import com.kombos.parts.Konversi
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON

class JurnalPendingMasukController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def journalPendingMasukService

    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def save(){
        def ret = [:]
        def wo = ""
        int saveSucces = 0,total= 0
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def partRcp = PartsRCP.get(it as Long)
            partRcp.isPending = "0"
            partRcp.save(flush: true)
            total++
            def cekJpm = JournalPendingMasuk.findByReceptionAndGoodsAndStaDel(partRcp.reception,partRcp.goods,"0")
            if(!cekJpm){
                saveSucces++
                def jpm = new JournalPendingMasuk()
                jpm?.reception = partRcp.reception
                jpm?.goods  = partRcp.goods
                jpm?.qty = partRcp.t403Jumlah1
                jpm?.price = GoodsHargaJual.findByGoodsAndStaDel(partRcp.goods, "0")?GoodsHargaJual.findByGoodsAndStaDel(partRcp.goods, "0")?.t151HargaTanpaPPN:0
                jpm?.periode = params.src_bulan+""+params.src_tahun
                jpm?.staDel = "0"
                jpm?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                jpm?.lastUpdProcess = "INSERT"
                jpm?.dateCreated = datatablesUtilService?.syncTime()
                jpm?.lastUpdated = datatablesUtilService?.syncTime()
                jpm.save(flush: true)
                jpm.errors.each {
                    println(it)
                }
            }
            if(!wo.contains(partRcp.reception.t401NoWO)){
                params.noWo = partRcp.reception.t401NoWO
                params.bulan = params.src_bulan+""+params.src_tahun
                journalPendingMasukService.createJournal(params)
            }
            wo = partRcp.reception.t401NoWO
        }
        ret.berhasil = saveSucces
        ret.totalSave = total

        render ret as JSON
    }
    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PartsRCP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(!params.tahun){
                params.tahun = 1
            }

            String bulanParam =  params.tahun + "-" + params.bulan + "-1"
            String bulanParam2 =  params.tahun + "-" + params.bulan + "-31"
            Date tgl = new Date().parse('yyyy-MM-d',bulanParam.toString())
            Date tgl2 = new Date().parse('yyyy-MM-d',bulanParam2.toString())

            reception{
                eq("companyDealer",session.userCompanyDealer)
                eq("staSave","0")

//                ge("t401TanggalWO",tgl)
                lt("t401TanggalWO",tgl2 + 1)

            }
            isNull("isPending")
            ilike("staDel", "0")

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        int count = 0
        results.each {
            def inv = InvoiceT701.findByReceptionAndT701StaDel(it.reception,'0')
            if(!inv){
                count++;
                rows << [

                        no : count,

                        id: it.id,

                        noWo: it.reception.t401NoWO,

                        noPol: it.reception.historyCustomerVehicle.fullNoPol,

                        tglWo: it.reception.t401TanggalWO? (it.reception.t401TanggalWO.format("dd/MM/yyyy")):"-",

                        m111ID: it.goods.m111ID,

                        m111Nama: it.goods.m111Nama,

                        qty: it.t403Jumlah1,

                        harga: GoodsHargaJual.findByGoodsAndStaDel(it.goods, "0")?konversi.toRupiah(GoodsHargaJual.findByGoodsAndStaDel(it.goods, "0")?.t151HargaTanpaPPN):"-",

                        total: it?.t403TotalRp?konversi.toRupiah(it?.t403TotalRp):"-",
                ]
            }
        }

        ret = [sEcho: params.sEcho, iTotalRecords: count, iTotalDisplayRecords: count, aaData: rows]

        render ret as JSON
    }
}