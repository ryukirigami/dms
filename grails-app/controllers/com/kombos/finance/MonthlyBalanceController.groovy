package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

class MonthlyBalanceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params
        String yerMan = params.periodeBulan && params.periodeTahun ? params.periodeBulan+params.periodeTahun : ""
        def akun = params.akun.toString().contains("|") ? params.akun.toString().substring(0,params.akun.toString().indexOf("|")): params.akun.toString().replace(" ","")
        def akunInstance = AccountNumber.findByAccountNumberAndStaDelAndAccountMutationType(akun.trim(),"0","MUTASI")
        def c = MonthlyBalance.createCriteria()
        def results = c.list (max: 1000.toInteger(), offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)
            if(params.periodeBulan && params.periodeTahun){
                eq("yearMonth",yerMan,[ignoreCase:true])
            }

            if(params.akun){
                eq("accountNumber",akunInstance)
            }

            if(params.subType){
                eq("subType",SubType.get(params.subType.toLong()))
            }

            if(params.subledger){
                ilike("subLedger","%"+params.subledger+"%")
            }

            if(params.staAwal && params.staAwal!="tidak"){
                if(params.akun){
                    eq("accountNumber",akunInstance)
                }else{
                    eq("id",1000.toLong())
                }
            }

            accountNumber{
                order("accountNumber");
            }

        }

        def rows = []
        def konversi = new Konversi()
        results.each {
            String subLedger = "";
            if(it?.subType?.description?.toUpperCase()=="CUSTOMER"){
                subLedger = HistoryCustomer.findById(it.subLedger as Long) ? " a.n "+HistoryCustomer.findById(it.subLedger as Long)?.fullNama : ""
            }else if(it?.subType?.description?.toUpperCase()=="VENDOR"){
                subLedger = Vendor.findById(it.subLedger as Long) ? " a.n "+Vendor.findById(it.subLedger as Long).m121Nama : ""
            }else if(it?.subType?.description?.toUpperCase()=="ASURANSI"){
                subLedger = VendorAsuransi.findById(it.subLedger as Long) ? " a.n "+VendorAsuransi.findById(it.subLedger as Long)?.m193Nama : ""
            }else if(it?.subType?.description?.toUpperCase()=="KARYAWAN"){
                subLedger = Karyawan.findById(it.subLedger as Long) ? " a.n "+Karyawan.findById(it.subLedger as Long).nama : ""
            }else if(it?.subType?.description?.toUpperCase()=="CABANG"){
                subLedger = CompanyDealer.findById(it.subLedger as Long) ? " a.n "+CompanyDealer.findById(it.subLedger as Long).m011NamaWorkshop : ""
            }else if(it?.subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                subLedger = Company.findById(it.subLedger as Long) ? " a.n "+Company.findById(it.subLedger as Long).namaPerusahaan : ""
            }
            rows << [

                    id: it.id,

                    periode: it?.yearMonth,

                    accountNumber: it?.accountNumber?.accountNumber,

                    accountName: it?.accountNumber?.accountName+subLedger,

                    startingBalance: konversi?.toRupiah(it?.startingBalance),

                    debetMutation: konversi.toRupiah(it?.debitMutation),

                    creditMutation: konversi.toRupiah(it?.creditMutation),

                    endingBalance: konversi.toRupiah(it?.endingBalance)

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
    def getJumlah(){
        def hasil = []
        String yerMan = params.bulan && params.tahun ? params.bulan+params.tahun : ""

        def m = MonthlyBalance.findAllByYearMonthAndCompanyDealer(yerMan,session.userCompanyDealer)
        hasil << [
                startingBalance : new Konversi().toRupiah2(m.startingBalance.sum()),
                debetMutation : new Konversi().toRupiah2(m.debitMutation.sum()),
                creditMutation : new Konversi().toRupiah2(m.creditMutation.sum()),
                endingBalance : new Konversi().toRupiah2(m.endingBalance.sum())
        ]
        render hasil as JSON
    }
    def index() {
        def skrg = new Date().clearTime()
        def bsok = (new Date()+1).clearTime()
        String staAkhirBulan = "N"
        if(skrg.format("MM")!=bsok.format("MM")){
            staAkhirBulan = "Y"
        }
        [skrg : skrg,staAkhirBulan : staAkhirBulan]
    }
    def doRemove(){
        def hasil="ok"
        String periode = params.bulan+params.tahun
        def mb = MonthlyBalance.createCriteria().list {
            eq("companyDealer",session?.userCompanyDealer)
            eq("yearMonth",periode)
        }
        mb*.delete();
        render hasil
    }
    def doSave(){
        String hasil="";
        String periode = params.bulan+params.tahun
        def tgglAkhir = tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
        def lastPeriode = lastYearMonth(params.bulan.toInteger(),params.tahun.toInteger())

        def cekLastMB = MonthlyBalance.findByCompanyDealerAndYearMonth(session?.userCompanyDealer,lastPeriode);
        if(!cekLastMB){
            hasil="gagal";
        }else {
            hasil="ok";
            def akunInstance = AccountNumber.createCriteria().list {
                eq("staDel","0");
                eq("accountMutationType","MUTASI",[ignoreCase: true])
                order("accountNumber")
            }
            Date skrg = new Date().parse("dd/MM/yyyy","01/"+new Date().format("MM")+"/"+new Date().format("yyyy"))
            Date awal = new Date().parse("dd/MM/yyyy","01/"+params.bulan+"/"+params.tahun)
            Date akhir = new Date().parse("dd/MM/yyyy HH:mm",tgglAkhir+"/"+params.bulan+"/"+params.tahun+" 24:00")
            def konversi = new Konversi()
            def cekMB = MonthlyBalance?.findByCompanyDealerAndYearMonth(session?.userCompanyDealer, periode);
            if(akunInstance?.size()>0){
                if(!cekMB){
                    akunInstance.each {
                        def tempAcc = it
                        def grupsubLedger = JournalDetail.createCriteria().list {
                            eq("staDel","0");
                            journal{
                                eq("companyDealer",session.userCompanyDealer)
                                eq("staDel","0")
                                ge("journalDate",awal)
                                lt("journalDate", akhir)
                                eq("isApproval", "1")
                            }
                            eq("accountNumber",tempAcc)
                            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                            projections {
                                groupProperty("subLedger", "subLedger")
                                groupProperty("subType", "subType")
                            }

                        }

                        def isNoTRX = MonthlyBalance.createCriteria().list {
                            eq("companyDealer",session.userCompanyDealer)
                            eq("yearMonth",lastPeriode)
                            eq("accountNumber",tempAcc)
                            ne("endingBalance",0.toBigDecimal())
                            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
                            projections {
                                groupProperty("subLedger", "subLedger")
                                groupProperty("subType", "subType")
                            }
                        }
                        def detailSLST = []
                        int indLoop = 0
                        if(grupsubLedger.size()>1){
                            grupsubLedger.each {
                                indLoop++;
                                def grups = it
                                detailSLST << ((grups?.subLedger?grups?.subLedger:"-1")+(grups?.subType?grups?.subType:"-1"))

                                def monBalanceLast = MonthlyBalance.createCriteria().get {
                                    eq("companyDealer",session.userCompanyDealer)
                                    eq("yearMonth",lastPeriode)
                                    eq("accountNumber",tempAcc)
                                    if(grups?.subLedger){
                                        eq("subLedger",grups?.subLedger)
                                    }else {
                                        isNull("subLedger")
                                    }
                                    if(grups?.subType){
                                        eq("subType",grups?.subType)
                                    }else {
                                        isNull("subType")
                                    }
                                    ne("endingBalance",0.toBigDecimal())
                                    maxResults(1);
                                }
                                def lastBalance = monBalanceLast ? monBalanceLast.endingBalance : 0
                                def nMonBalance = new MonthlyBalance()

                                def debetM = 0
                                def kreditM = 0
                                def jurnal = JournalDetail.createCriteria().list {
                                    eq("staDel","0");
                                    eq("accountNumber",tempAcc)
                                    if(grups?.subLedger){
                                        eq("subLedger",grups?.subLedger)
                                    }else {
                                        isNull("subLedger")
                                    }
                                    if(grups?.subType){
                                        eq("subType",grups?.subType)
                                    }else {
                                        isNull("subType")
                                    }
                                    journal{
                                        eq("companyDealer",session.userCompanyDealer)
                                        eq("staDel","0")
                                        ge("journalDate",awal)
                                        lt("journalDate", akhir)
                                        eq("isApproval", "1")

                                    }
                                }
                                SubType st = grups?.subType
                                String subLedGerr = grups?.subLedger
                                jurnal.each {
                                    debetM+=it.debitAmount
                                    kreditM+=it.creditAmount
                                }
                                def kurangi = lastBalance + debetM - kreditM
                                if((lastBalance!=0 || debetM!=0 || kreditM!=0 || kurangi!=0) && !cekMBExsist(tempAcc,st,subLedGerr,periode,kurangi)){
                                    nMonBalance.companyDealer = session.userCompanyDealer
                                    nMonBalance.yearMonth = periode
                                    nMonBalance.startingBalance = lastBalance
                                    nMonBalance.debitMutation = debetM
                                    nMonBalance.creditMutation = kreditM
                                    nMonBalance.endingBalance = kurangi
                                    nMonBalance.realmutation = debetM - kreditM
                                    nMonBalance.subLedger = subLedGerr
                                    nMonBalance.startingDate = awal
                                    nMonBalance.endingDate = akhir
                                    nMonBalance.accountNumber = tempAcc
                                    nMonBalance.subType = st
                                    nMonBalance.createdBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    nMonBalance.updatedBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    nMonBalance.lastUpdProcess = "INSERT"
                                    nMonBalance.dateCreated = datatablesUtilService?.syncTime()
                                    nMonBalance.lastUpdated = datatablesUtilService?.syncTime()
                                    nMonBalance.save(flush: true)
                                    if(nMonBalance.hasErrors()){
                                        nMonBalance.errors.each {println it}
                                    }
                                }
                            }
                            if(indLoop==grupsubLedger.size()){
                                isNoTRX.each {
                                    def grups = it
                                    String cariDetail = ((grups?.subLedger?grups?.subLedger:"-1")+(grups?.subType?grups?.subType:"-1"))
                                    if(detailSLST.indexOf(cariDetail)<0){
                                        def fMB = MonthlyBalance.createCriteria().get {
                                            eq("companyDealer",session.userCompanyDealer)
                                            eq("yearMonth",lastPeriode)
                                            eq("accountNumber",tempAcc)
                                            if(grups?.subLedger){
                                                eq("subLedger",grups?.subLedger)
                                            }else {
                                                isNull("subLedger")
                                            }
                                            if(grups?.subType){
                                                eq("subType",grups?.subType)
                                            }else {
                                                isNull("subType")
                                            }
                                            ne("endingBalance",0.toBigDecimal())
                                            maxResults(1);
                                        }
                                        if(fMB){
                                            if(fMB.endingBalance!=0 && !cekMBExsist(fMB?.accountNumber,fMB?.subType,fMB?.subLedger,periode,fMB?.endingBalance)){
                                                def nMB = new MonthlyBalance()
                                                nMB.properties = fMB.properties
                                                nMB.yearMonth = periode
                                                nMB.startingBalance = fMB.endingBalance
                                                nMB.debitMutation = 0
                                                nMB.creditMutation = 0
                                                nMB.endingBalance = fMB.endingBalance
                                                nMB.realmutation = 0
                                                nMB.startingDate = awal
                                                nMB.endingDate = akhir
                                                nMB.createdBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                                nMB.updatedBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                                nMB.lastUpdProcess = "INSERT"
                                                nMB.dateCreated = datatablesUtilService?.syncTime()
                                                nMB.lastUpdated = datatablesUtilService?.syncTime()
                                                nMB.save(flush: true)
                                                if(nMB?.hasErrors()){
                                                    nMB.errors.each {println it}
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        else {
                            def group = grupsubLedger[0]
                            if(grupsubLedger?.size()>0){
                                String cariDetail = ((group?.subLedger?group?.subLedger:"-1")+(group?.subType?group?.subType:"-1"))
                                detailSLST << cariDetail

                                def monBalanceLast = MonthlyBalance.createCriteria().get {
                                    eq("companyDealer",session.userCompanyDealer)
                                    eq("yearMonth",lastPeriode)
                                    eq("accountNumber",tempAcc)
                                    if(group?.subType){
                                        eq("subLedger",group?.subLedger)
                                    }else{
                                        isNull("subLedger")
                                    }
                                    if(group?.subType){
                                        eq("subType",group?.subType)
                                    }else{
                                        isNull("subType")
                                    }
                                    ne("endingBalance",0.toBigDecimal())
                                    maxResults(1);
                                }

                                def lastBalance = monBalanceLast ? monBalanceLast.endingBalance : 0
                                def nMonBalance = new MonthlyBalance()

                                def debetM = 0
                                def kreditM = 0
                                def jurnal = JournalDetail.createCriteria().list {
                                    eq("staDel","0");
                                    eq("accountNumber",tempAcc)
                                    if(group?.subType){
                                        eq("subLedger",group?.subLedger)
                                    }else{
                                        isNull("subLedger")
                                    }
                                    if(group?.subType){
                                        eq("subType",group?.subType)
                                    }else{
                                        isNull("subType")
                                    }
                                    journal{
                                        eq("companyDealer",session.userCompanyDealer)
                                        eq("staDel","0")
                                        ge("journalDate",awal)
                                        lt("journalDate", akhir)
                                        eq("isApproval", "1")
                                    }
                                }
                                SubType st = group?.subType ? group?.subType : null
                                String subLedGerr = group?.subLedger ? group?.subLedger : null
                                jurnal.each {
                                    debetM+=it.debitAmount
                                    kreditM+=it.creditAmount
                                }
                                def kurangi = lastBalance + debetM - kreditM
                                if((lastBalance!=0 || debetM!=0 || kreditM!=0 || kurangi!=0) && !cekMBExsist(tempAcc,st,subLedGerr,periode,kurangi)){
                                    nMonBalance.companyDealer = session.userCompanyDealer
                                    nMonBalance.yearMonth = periode
                                    nMonBalance.startingBalance = lastBalance
                                    nMonBalance.debitMutation = debetM
                                    nMonBalance.creditMutation = kreditM
                                    nMonBalance.endingBalance = kurangi
                                    nMonBalance.realmutation = debetM-kreditM
                                    nMonBalance.subLedger = subLedGerr
                                    nMonBalance.startingDate = awal
                                    nMonBalance.endingDate = akhir
                                    nMonBalance.accountNumber = tempAcc
                                    nMonBalance.subType = st
                                    nMonBalance.createdBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    nMonBalance.updatedBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                    nMonBalance.lastUpdProcess = "INSERT"
                                    nMonBalance.dateCreated = datatablesUtilService.syncTime()
                                    nMonBalance.lastUpdated = datatablesUtilService.syncTime()
                                    nMonBalance.save(flush: true)
                                    if(nMonBalance.hasErrors()){
                                        nMonBalance.errors.each {println it}
                                    }
                                }
                            }
                            isNoTRX.each {
                                def grups = it
                                String cariDetail = ((grups?.subLedger?grups?.subLedger:"-1")+(grups?.subType?grups?.subType:"-1"))
                                if(detailSLST.indexOf(cariDetail)<0){
                                    def fMB = MonthlyBalance.createCriteria().get {
                                        eq("companyDealer",session.userCompanyDealer)
                                        eq("yearMonth",lastPeriode)
                                        eq("accountNumber",tempAcc)
                                        if(grups?.subLedger){
                                            eq("subLedger",grups?.subLedger)
                                        }else {
                                            isNull("subLedger")
                                        }
                                        if(grups?.subType){
                                            eq("subType",grups?.subType)
                                        }else {
                                            isNull("subType")
                                        }
                                        ne("endingBalance",0.toBigDecimal())
                                        maxResults(1);
                                    }
                                    if(fMB){
                                        if(fMB?.endingBalance!=0 && !cekMBExsist(fMB?.accountNumber,fMB?.subType,fMB?.subLedger,periode,fMB?.endingBalance)){
                                            def nMB = new MonthlyBalance()
                                            nMB.properties = fMB.properties
                                            nMB.yearMonth = periode
                                            nMB.startingBalance = fMB.endingBalance
                                            nMB.debitMutation = 0
                                            nMB.creditMutation = 0
                                            nMB.endingBalance = fMB.endingBalance
                                            nMB.realmutation = 0
                                            nMB.startingDate = awal
                                            nMB.endingDate = akhir
                                            nMB.createdBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                            nMB.updatedBy =org.apache.shiro.SecurityUtils.subject.principal.toString()
                                            nMB.lastUpdProcess = "INSERT"
                                            nMB.dateCreated = datatablesUtilService.syncTime()
                                            nMB.lastUpdated = datatablesUtilService.syncTime()
                                            nMB.save(flush: true)
                                            if(nMB.hasErrors()){
                                                nMB.errors.each {println it}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        render hasil
    }

    String lastYearMonth(int bulan,int tahun){
        String lastMonth = "",lastYear = ""
        if(bulan==1){
            lastMonth = "12"
            lastYear = (tahun-1).toString()
        }else {
            lastMonth = (bulan-1).toString()
            lastYear = tahun.toString()
        }
        lastMonth = lastMonth.size() > 1 ? lastMonth : "0"+lastMonth
        return lastMonth+lastYear
    }

    String tanggalAkhir(int bulan,int tahun){
        boolean kabisat = false
        String tgglAkhir = ""
        if(tahun%4==0){
            kabisat = true
        }
        if(bulan>7){
            if(bulan%2==0){
                tgglAkhir = "31"
            }else{
                tgglAkhir = "30"
            }
        }else {
            if(bulan%2==0){
                if(bulan==2){
                    if(kabisat){
                        tgglAkhir="29"
                    }else{
                        tgglAkhir="28"
                    }
                }else{
                    tgglAkhir = "30"
                }
            }else{
                tgglAkhir = "31"
            }
        }
        return tgglAkhir
    }

    def cekMBExsist(AccountNumber accountNumber, SubType subType, String subledger, String yearMonth, def saldoAkhir){
        def exist = false;
        def cekAcc = MonthlyBalance.createCriteria().get {
            eq("companyDealer",session?.userCompanyDealer);
            eq("accountNumber",accountNumber);
            eq("yearMonth",yearMonth);
            eq("endingBalance",saldoAkhir?.toString()?.toBigDecimal() );
            if(subType){
                eq("subType",subType)
            }else {
                isNull("subType")
            }
            if(subledger){
                eq("subLedger",subledger)
            }else {
                isNull("subLedger")
            }
            maxResults(1);
        }
        if(cekAcc){
            exist = true;
        }
        return exist;
    }
}
