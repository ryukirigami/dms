package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.VendorAsuransi
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.hrd.Karyawan
import com.kombos.maintable.Company
import com.kombos.maintable.Customer
import com.kombos.parts.Konversi
import com.kombos.parts.Vendor
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class RincianTransaksiController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    def index() {}

    def neraca(){

    }

    def rugiLaba(){

    }

    def printRugiLaba(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        def mBalanceController = new MonthlyBalanceController()
        def reportData = new ArrayList()
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +mBalanceController.tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        def c = AccountNumber.createCriteria()
        def results = c.list{
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","4%")
                like("accountNumber","5%")
                like("accountNumber","6%")
                like("accountNumber","7%")
                like("accountNumber","8%")
            }

            order("accountNumber","asc")
        }
        int count = 0
        results.each { acc->
            def da = 0.0
            def ca = 0.0
            def hcd = 0.0
            def cj = JournalDetail.createCriteria()
            def rj = cj.list{
                eq("staDel","0");
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",acc.accountNumber + "%")
                    order("accountNumber","asc")
                }
                journal{
                    eq("companyDealer",companyDealer)
                    ge("journalDate",tgl)
                    lt("journalDate",tgl2+1)
                    eq("isApproval", "1")
                    eq("staDel","0");
                }
            }
            rj.each {
                da += it.debitAmount
                ca += it.creditAmount
            }
            hcd = da - ca
            def metod = new MonthlyBalanceController()
            String lastYM = metod.lastYearMonth(tgl.format("MM").toInteger(),tgl.format("yyyy").toInteger());
            def dataMB = MonthlyBalance.createCriteria().list {
                eq("yearMonth", lastYM)
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",acc.accountNumber + "%")
                    order("accountNumber","asc")
                }
            }
            def saldoAwal = 0.0
            dataMB.each{
                saldoAwal += it.endingBalance
            }

            if(saldoAwal!=0 || ca!=0 || da!=0){
                def data = [:]
                count = count + 1

                data.put("companyDealer",companyDealer)
                data.put("bulan",periode)

                data.put("noUrut",count)
                data.put("namaRekening",acc.accountName.toUpperCase())
                data.put("noRekening",acc.accountNumber)
                data.put("tglAwal",tgl.format("dd/MM/yyyy"))
                data.put("tglAkhir",tgl2.format("dd/MM/yyyy"))

                data.put("saldoAwal",saldoAwal<0?"("+konversi.toRupiah(saldoAwal*(-1))+")":konversi.toRupiah(saldoAwal))
                data.put("debet",konversi.toRupiah(da))
                data.put("credit",konversi.toRupiah(ca))
                if(da==0){
                    data.put("debet","-")
                }
                if(ca==0){
                    data.put("credit","-")
                }
                data.put("debetCredit",hcd<0?"("+konversi.toRupiah(hcd*(-1))+")":konversi.toRupiah(hcd))
                def saldoAkhir = saldoAwal + hcd
                data.put("saldoAkhir",saldoAkhir<0?"("+konversi.toRupiah(saldoAkhir*(-1))+")":konversi.toRupiah(saldoAkhir))

                reportData.add(data)
            }
        }

        def reportDef = new JasperReportDef(name:'f_lapRincianLabaRugi.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("R_LabaRugi_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def printNeraca(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        def mbControll = new MonthlyBalanceController()
        def reportData = new ArrayList()
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        def tgglAkhir = new Date().parse('dd/MM/yyyy',params?.tanggal)
        def periode = "Periode : "+tgl?.format("dd-MM-yyyy")+" s.d. "+tgglAkhir?.format("dd-MM-yyyy")
        def c = AccountNumber.createCriteria()
        def results = c.list{
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","1%")
                like("accountNumber","2%")
                like("accountNumber","3%")
                like("accountNumber","4%")
                like("accountNumber","5%")
                like("accountNumber","6%")
                like("accountNumber","7%")
                like("accountNumber","8%")
            }

            order("accountNumber","asc")
        }

        int count = 0

        results.each { acc->
            def da = 0.00
            def ca = 0.00
            def hcd = 0.00
            def cj = JournalDetail.createCriteria()
            def rj = cj.list{
                eq("staDel","0")
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",acc.accountNumber + "%")
                    order("accountNumber","asc")
                }
                journal{
                    eq("companyDealer",companyDealer)
                    ge("journalDate",tgl)
                    lt("journalDate",tgglAkhir+1)
                    eq("isApproval", "1")
                    eq("staDel","0")
                }
            }
            rj.each {
                da += it.debitAmount
                ca += it.creditAmount
            }
            hcd = da - ca
            def metod = new MonthlyBalanceController()
            String lastYM = metod.lastYearMonth(tgl.format("MM").toInteger(),tgl.format("yyyy").toInteger());
            def dataMB = MonthlyBalance.createCriteria().list {
                eq("companyDealer",companyDealer)
                eq("yearMonth", lastYM)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",acc.accountNumber + "%")
                    order("accountNumber","asc")
                }
            }
            def saldoAwal = 0.00
            if(params.level == '5' && dataMB.size()>=1){
                def subLedger = ""
                dataMB.each{ mon ->
                    def daDetail = 0.00, caDetail = 0.00, hcdDetail = 0.00
                    count = count + 1
                    def data = [:]
                    data.put("companyDealer",companyDealer)
                    data.put("bulan",periode)
                    if(mon?.subType?.description?.toUpperCase()=="CUSTOMER"){
                        subLedger = HistoryCustomer.findById(mon.subLedger as Long)?HistoryCustomer.findById(mon.subLedger as Long)?.fullNama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="VENDOR"){
                        subLedger = Vendor.findById(mon.subLedger as Long)?Vendor?.findById(mon.subLedger as Long)?.m121Nama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="ASURANSI"){
                        subLedger = VendorAsuransi.findById(mon.subLedger as Long)?VendorAsuransi.findById(mon.subLedger as Long).m193Nama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="KARYAWAN"){
                        subLedger = Karyawan.findById(mon.subLedger as Long)?Karyawan.findById(mon.subLedger as Long).nama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="CABANG"){
                        subLedger = CompanyDealer.findById(mon.subLedger as Long)?CompanyDealer.findById(mon.subLedger as Long).m011NamaWorkshop:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                        subLedger = Company.findById(mon.subLedger as Long)?Company.findById(mon.subLedger as Long).namaPerusahaan:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else{
                        data.put("namaRekening",acc.accountName.toUpperCase())
                        rj.each {
                            daDetail += it.debitAmount
                            caDetail += it.creditAmount
                        }
                    }
                    hcdDetail = daDetail - caDetail
                    data.put("noUrut",count)
                    saldoAwal = mon.endingBalance

                    data.put("noRekening",acc.accountNumber)
                    data.put("tglAwal",tgl.format("dd/MM/yyyy"))
                    data.put("tglAkhir",tgglAkhir.format("dd/MM/yyyy"))
                    data.put("saldoAwal",saldoAwal<0?"("+konversi.toRupiah(saldoAwal*(-1))+")":konversi.toRupiah(saldoAwal))
                    data.put("debet",konversi.toRupiah(daDetail))
                    data.put("credit",konversi.toRupiah(caDetail))
                    if(daDetail==0){
                        data.put("debet","-")
                    }
                    if(caDetail==0){
                        data.put("credit","-")
                    }
                    data.put("debetCredit",hcdDetail<0?"("+konversi.toRupiah(hcdDetail*(-1))+")":konversi.toRupiah(hcdDetail))
                    def saldoAkhir = saldoAwal + hcdDetail
                    data.put("saldoAkhir",saldoAkhir<0?"("+konversi.toRupiah(saldoAkhir*(-1))+")":konversi.toRupiah(saldoAkhir))
                    if(saldoAwal!=0 || caDetail!=0 || daDetail!=0){
                        reportData.add(data)
                    }
                }
            }else{
                dataMB.each{
                    saldoAwal += it.endingBalance
                }
                if(saldoAwal!=0 || ca!=0 || da!=0){
                    def data = [:]
                    count = count + 1

                    data.put("companyDealer",companyDealer)
                    data.put("bulan",periode)

                    data.put("noUrut",count)
                    data.put("namaRekening",acc.accountName.toUpperCase())
                    data.put("noRekening",acc.accountNumber)
                    data.put("tglAwal",tgl.format("dd/MM/yyyy"))
                    data.put("tglAkhir",tgglAkhir.format("dd/MM/yyyy"))

                    data.put("saldoAwal",saldoAwal<0?"("+konversi.toRupiah(saldoAwal*(-1))+")":konversi.toRupiah(saldoAwal))
                    data.put("debet",konversi.toRupiah(da))
                    data.put("credit",konversi.toRupiah(ca))
                    if(da==0){
                        data.put("debet","-")
                    }
                    if(ca==0){
                        data.put("credit","-")
                    }
                    data.put("debetCredit",hcd<0?"("+konversi.toRupiah(hcd*(-1))+")":konversi.toRupiah(hcd))
                    def saldoAkhir = saldoAwal + hcd
                    data.put("saldoAkhir",saldoAkhir<0?"("+konversi.toRupiah(saldoAkhir*(-1))+")":konversi.toRupiah(saldoAkhir))

                    reportData.add(data)
                }
            }
        }

        def reportDef = new JasperReportDef(name:'f_lapRincianNeraca.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )

        reportDefList.add(reportDef)


        def file = File.createTempFile("R_Neraca_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def printNeracaPercobaan(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        def mBalanceController = new MonthlyBalanceController()
        def reportData = new ArrayList()
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +mBalanceController.tanggalAkhir(params.bulan.toInteger(),params.tahun.toInteger())
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()
        def c = AccountNumber.createCriteria()
        def results = c.list{
            eq("staDel","0")
            eq("level",params.level as int)
            or{
                like("accountNumber","1%")
                like("accountNumber","2%")
                like("accountNumber","3%")
                like("accountNumber","4%")
                like("accountNumber","5%")
                like("accountNumber","6%")
                like("accountNumber","7%")
                like("accountNumber","8%")
            }

            order("accountNumber","asc")
        }
        int count = 0
        def totSaldoAwal = 0.00, totDebet= 0.00, totCredit= 0.00, totDebetCredit= 0.00, totSaldoAkhir = 0.00
        results.each { acc->
            def da = 0.00
            def ca = 0.00
            def hcd = 0.00
            def cj = JournalDetail.createCriteria()
            def rj = cj.list{
                eq("staDel","0");
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",acc.accountNumber + "%")
                    order("accountNumber","asc")
                }
                journal{
                    eq("companyDealer",companyDealer)
                    ge("journalDate",tgl)
                    lt("journalDate",tgl2+1)
                    eq("isApproval", "1");
                    eq("staDel","0");
                }
            }

            rj.each {
                da += it.debitAmount
                ca += it.creditAmount
            }

            hcd = da - ca
            def metod = new MonthlyBalanceController()
            String lastYM = metod.lastYearMonth(tgl.format("MM").toInteger(),tgl.format("yyyy").toInteger());
            def dataMB = MonthlyBalance.createCriteria().list {
                eq("companyDealer",companyDealer)
                eq("yearMonth", lastYM)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",acc.accountNumber + "%")
                    order("accountNumber","asc")
                }
            }

            def saldoAwal = 0.00
            if(params.level == '5' && dataMB.size()>=1){
                def subLedger = ""
                dataMB.each{ mon ->
                    def daDetail = 0.00, caDetail = 0.00, hcdDetail = 0.00
                    count = count + 1
                    def data = [:]
                    data.put("companyDealer",companyDealer)
                    data.put("bulan",periode)
                    if(mon?.subType?.description?.toUpperCase()=="CUSTOMER"){
                        subLedger = HistoryCustomer?.findById(mon.subLedger as Long)?HistoryCustomer?.findById(mon.subLedger as Long)?.fullNama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="VENDOR"){
                        subLedger = Vendor?.findById(mon.subLedger as Long)?Vendor?.findById(mon.subLedger as Long)?.m121Nama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="ASURANSI"){
                        subLedger = VendorAsuransi?.findById(mon.subLedger as Long)?VendorAsuransi?.findById(mon.subLedger as Long)?.m193Nama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="KARYAWAN"){
                        subLedger = Karyawan?.findById(mon.subLedger as Long)?Karyawan?.findById(mon.subLedger as Long)?.nama:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="CABANG"){
                        subLedger = CompanyDealer?.findById(mon.subLedger as Long)?CompanyDealer?.findById(mon.subLedger as Long)?.m011NamaWorkshop:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else if(mon?.subType?.description?.toUpperCase()=="CUSTOMER COMPANY"){
                        subLedger = Company?.findById(mon.subLedger as Long)?Company?.findById(mon.subLedger as Long)?.namaPerusahaan:""
                        rj.each {
                            if(mon?.subLedger == it?.subLedger){
                                daDetail += it.debitAmount
                                caDetail += it.creditAmount
                            }
                        }
                        data.put("namaRekening",acc.accountName.toUpperCase() + " a.n "+ subLedger)
                    }else{
                        data.put("namaRekening",acc.accountName.toUpperCase())
                        rj.each {
                            daDetail += it.debitAmount
                            caDetail += it.creditAmount
                        }
                    }
                    hcdDetail = daDetail - caDetail
                    data.put("noUrut",count)
                    saldoAwal = mon.endingBalance

                    data.put("noRekening",acc.accountNumber)
                    data.put("tglAwal",tgl.format("dd/MM/yyyy"))
                    data.put("tglAkhir",tgl2.format("dd/MM/yyyy"))
                    data.put("saldoAwal",saldoAwal<0?"("+konversi.toRupiah(saldoAwal*(-1))+")":konversi.toRupiah(saldoAwal))
                    data.put("debet",konversi.toRupiah(daDetail))
                    data.put("credit",konversi.toRupiah(caDetail))
                    if(daDetail==0){
                        data.put("debet","-")
                    }
                    if(caDetail==0){
                        data.put("credit","-")
                    }
                    data.put("debetCredit",hcdDetail<0?"("+konversi.toRupiah(hcdDetail*(-1))+")":konversi.toRupiah(hcdDetail))
                    def saldoAkhir = saldoAwal + hcdDetail
                    data.put("saldoAkhir",saldoAkhir<0?"("+konversi.toRupiah(saldoAkhir*(-1))+")":konversi.toRupiah(saldoAkhir))

                    totSaldoAwal += saldoAwal
                    totDebet += daDetail
                    totCredit += caDetail
                    totDebetCredit += hcdDetail
                    totSaldoAkhir += saldoAkhir

                    data.put("totSaldoAwal",totSaldoAwal<0?"("+konversi.toRupiah(totSaldoAwal*(-1))+")":konversi.toRupiah(totSaldoAwal))
                    data.put("totDebet",konversi.toRupiah(totDebet))
                    data.put("totCredit",konversi.toRupiah(totCredit))
                    data.put("totDebetCredit",totDebetCredit<0?"("+konversi.toRupiah(totDebetCredit*(-1))+")":konversi.toRupiah(totDebetCredit))
                    data.put("totSaldoAkhir",totSaldoAkhir<0?"("+konversi.toRupiah(totSaldoAkhir*(-1))+")":konversi.toRupiah(totSaldoAkhir))

                    reportData.add(data)
                }
            }else{
                def data = [:]
                dataMB.each{
                    saldoAwal += it.endingBalance
                }
                count = count + 1
                data.put("companyDealer",companyDealer)
                data.put("bulan",periode)
                data.put("noUrut",count)
                data.put("namaRekening",acc.accountName.toUpperCase())
                data.put("noRekening",acc.accountNumber)
                data.put("tglAwal",tgl.format("dd/MM/yyyy"))
                data.put("tglAkhir",tgl2.format("dd/MM/yyyy"))
                data.put("saldoAwal",saldoAwal<0?"("+konversi.toRupiah(saldoAwal*(-1))+")":konversi.toRupiah(saldoAwal))
                data.put("debet",konversi.toRupiah(da))
                data.put("credit",konversi.toRupiah(ca))
                if(da==0){
                    data.put("debet","-")
                }
                if(ca==0){
                    data.put("credit","-")
                }
                data.put("debetCredit",hcd<0?"("+konversi.toRupiah(hcd*(-1))+")":konversi.toRupiah(hcd))
                def saldoAkhir = saldoAwal + hcd
                data.put("saldoAkhir",saldoAkhir<0?"("+konversi.toRupiah(saldoAkhir*(-1))+")":konversi.toRupiah(saldoAkhir))
                totSaldoAwal += saldoAwal
                totDebet += da
                totCredit += ca
                totDebetCredit += hcd
                totSaldoAkhir += saldoAkhir
                data.put("totSaldoAwal",totSaldoAwal<0?"("+konversi.toRupiah(totSaldoAwal*(-1))+")":konversi.toRupiah(totSaldoAwal))
                data.put("totDebet",konversi.toRupiah(totDebet))
                data.put("totCredit",konversi.toRupiah(totCredit))
                data.put("totDebetCredit",totDebetCredit<0?"("+konversi.toRupiah(totDebetCredit*(-1))+")":konversi.toRupiah(totDebetCredit))
                data.put("totSaldoAkhir",totSaldoAkhir<0?"("+konversi.toRupiah(totSaldoAkhir*(-1))+")":konversi.toRupiah(totSaldoAkhir))
                reportData.add(data)
            }
        }

        def reportDef = new JasperReportDef(name:'f_lapRincianNeracaPercobaan.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        reportDefList.add(reportDef)

        def file = File.createTempFile("R_NP_LVL"+params.level+"_P"+params.bulan+""+params.tahun+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
