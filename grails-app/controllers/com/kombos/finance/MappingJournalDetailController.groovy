package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class MappingJournalDetailController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def mappingJournalDetailService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render mappingJournalDetailService.datatablesList(params) as JSON
    }

    def create() {
        def result = mappingJournalDetailService.create(params)

        if (!result.error)
            return [mappingJournalDetailInstance: result.mappingJournalDetailInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = mappingJournalDetailService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["MappingJournalDetail", result.mappingJournalDetailInstance.id])
            redirect(action: 'show', id: result.mappingJournalDetailInstance.id)
            return
        }

        render(view: 'create', model: [mappingJournalDetailInstance: result.mappingJournalDetailInstance])
    }

    def show(Long id) {
        def result = mappingJournalDetailService.show(params)

        if (!result.error)
            return [mappingJournalDetailInstance: result.mappingJournalDetailInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = mappingJournalDetailService.show(params)

        if (!result.error)
            return [mappingJournalDetailInstance: result.mappingJournalDetailInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = mappingJournalDetailService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["MappingJournalDetail", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [mappingJournalDetailInstance: result.mappingJournalDetailInstance.attach()])
    }

    def delete() {
        def result = mappingJournalDetailService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["MappingJournalDetail", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MappingJournalDetail, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
