package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class RugiLabaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
    def index() {}
    def previewData(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params.tahun as int,(params.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = []
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase() +" (BULANAN)"
        if(params?.tipe && params?.tipe=="t"){
            periode = params?.tahun + " (TAHUNAN)"
        }
        def pendapatan = []
        def results = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","6%")
            order("accountNumber","asc")
        }
        def sumPendapatan = 0.0, totalPendapatan = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        results.each { an ->
            sumPendapatan = 0.0

            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    ilike("accountName","%PENDAPATAN%")
                    order("accountNumber","asc")
                }
            }
            if(data.size() > 0){
                data.each{
                    def bl = (it.realmutation * -1)
                    sumPendapatan  += bl
                }
                totalPendapatan += sumPendapatan
                if(sumPendapatan!=0){
                    pendapatan << [
                            rekeningName : an?.accountNumber+" "+an?.accountName,
                            rekeningValue : sumPendapatan < 0 ? "(" + konversi.toRupiah((sumPendapatan * (-1))) + ")": konversi.toRupiah(sumPendapatan),
                            totalPendapatan : totalPendapatan < 0 ? "(" + konversi.toRupiah((totalPendapatan * (-1))) + ")": konversi.toRupiah(totalPendapatan),
                    ]
                }
            }
        }
            //----------------
        def potongan = []
        def resultsPotongan = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","6%")
            order("accountNumber","asc")
        }
        def sumPotongan = 0.0, totalPotongan = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        resultsPotongan.each { an ->
            sumPotongan = 0.0
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    ilike("accountName","POTONGAN %")
                    order("accountNumber","asc")
                }
            }

            if(data.size() > 0){
                data.each{
                    def bl = (it.realmutation * -1)
                    sumPotongan  += bl
                }
                totalPotongan += sumPotongan
                if(sumPotongan!=0){
                    potongan << [
                            rekeningName : an?.accountNumber+" "+an?.accountName,
                            rekeningValue : sumPotongan < 0 ? "(" + konversi.toRupiah((sumPotongan * (-1))) + ")": konversi.toRupiah(sumPotongan),
                            totalPotongan : totalPotongan < 0 ? "(" + konversi.toRupiah((totalPotongan * (-1))) + ")": konversi.toRupiah(totalPotongan),
                            pendapatanPotongan : (totalPendapatan + totalPotongan) < 0 ? "(" + konversi.toRupiah(((totalPendapatan + totalPotongan) * (-1))) + ")": konversi.toRupiah(totalPendapatan + totalPotongan),
                    ]
                }
            }
        }
        def pendapatanPotongan = totalPendapatan + totalPotongan
        //-------------------BIAYA
        def biaya = []
        def resultsBiaya = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","7%")
            order("accountNumber","asc")
        }
        def sumBiaya = 0.0, totalBiaya = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        resultsBiaya.each { an ->
            sumBiaya = 0.0

            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    or{
                        ilike("accountName","BIAYA%")
                        ilike("accountName","BY%")
                    }
                    order("accountNumber","asc")
                }
            }

            if(data.size() > 0){
                data.each{
                    sumBiaya  += it.realmutation
                }
                totalBiaya += sumBiaya
                if(sumBiaya!=0){
                    biaya << [
                            rekeningName : an?.accountNumber+" "+an?.accountName,
                            rekeningValue : sumBiaya < 0 ? "(" + konversi.toRupiah((sumBiaya * (-1))) + ")": konversi.toRupiah(sumBiaya),
                            totalBiaya : totalBiaya < 0 ? "(" + konversi.toRupiah((totalBiaya * (-1))) + ")": konversi.toRupiah(totalBiaya),
                    ]
                }
            }
        }

        //GROSS PROFIT
        def grossProfit = (pendapatanPotongan - totalBiaya)
        def lainlain = []
        if(grossProfit!=0){
            lainlain << [
                    rekeningName: "3. GROSS PROFIT",
                    rekeningValue: grossProfit < 0 ? "(" + konversi.toRupiah((grossProfit * (-1))) + ")": konversi.toRupiah(grossProfit),
            ]
        }
           //----- LAIN LAIN
        //----PENDAPATANLAIN2
        def resultsPendLain = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","8%")
            order("accountNumber","asc")
        }
        def sumPendLain = 0.0, totalPendLain = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        String akunPend = ""
        resultsPendLain .each { an ->
            sumPendLain = 0
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    or{
                        ilike("accountName","PENDAPATAN%")
                        ilike("accountName","PEND%")
                    }
                    order("accountNumber","asc")
                }
            }

            if(data.size() > 0){
                data.each{
                    def bl = (it.realmutation * -1)
                    sumPendLain  += bl
                    akunPend=it?.accountNumber?.accountNumber
                }
                if(sumPendLain!=0){
                    lainlain << [
                            rekeningName: akunPend+" "+an?.accountName,
                            rekeningValue: sumPendLain < 0 ? "(" + konversi.toRupiah((sumPendLain * (-1))) + ")": konversi.toRupiah(sumPendLain),
                    ]
                }
                totalPendLain += sumPendLain
            }
        }
        //-------------------BIAYALAIN2
        def resultsBiayaLain = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","8%")
            order("accountNumber","asc")
        }
        def sumBiayaLain = 0.0, totalBiayaLain = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        String akunBiaya = ""
        resultsBiayaLain .each { an ->
            sumBiayaLain = 0
            def data = MonthlyBalance.createCriteria().list {
                eq("yearMonth", (params.bulan+""+params.tahun).toString())
                eq("companyDealer",companyDealer)
                accountNumber{
                    eq("staDel","0")
                    like("accountNumber",an.accountNumber + "%")
                    or{
                        ilike("accountName","BIAYA%")
                        ilike("accountName","BY%")
                    }
                    order("accountNumber","asc")
                }
            }

            if(data.size() > 0){
                data.each{
                        sumBiayaLain  += it.realmutation
                        akunBiaya=it?.accountNumber?.accountNumber
                }
                if(sumBiayaLain!=0){
                    lainlain << [
                            rekeningName: akunBiaya+" "+an?.accountName,
                            rekeningValue: sumBiayaLain < 0 ? "(" + konversi.toRupiah((sumBiayaLain * (-1))) + ")": konversi.toRupiah(sumBiayaLain),
                    ]
                }
                totalBiayaLain += sumBiayaLain
            }
        }
        def labaSblmPjk =  (grossProfit + (totalPendLain - totalBiayaLain))

        if(labaSblmPjk!=0){
            lainlain << [
                    rekeningName: "LABA SEBELUM PAJAK",
                    rekeningValue: labaSblmPjk < 0 ? "(" + konversi.toRupiah(labaSblmPjk * (-1)) + ")": konversi.toRupiah(labaSblmPjk),
            ]
        }
        reportData << [
                pendapatan : pendapatan,
                potongan : potongan,
                biaya : biaya,
                lainlain : lainlain,
                companyDealer : companyDealer,
                periode : "Periode " + periode
        ]

        def reportDef = new JasperReportDef(name:'rugiLaba.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("RugiLaba"+params.bulan+""+params.tahun+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def previewDataTahunan(){
        def companyDealer = session?.userCompanyDealer;
        if(params?.companyDealer){
            companyDealer = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params.tahun as int,(params.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = []
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase() +" (BULANAN)"
        if(params?.tipe && params?.tipe=="t"){
            periode = params?.tahun + " (TAHUNAN)"
        }
        def pendapatan = []
        def results = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","6%")
            order("accountNumber","asc")
        }
        def sumPendapatan = 0.0, totalPendapatan = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        results.each { an ->
            sumPendapatan = 0.0
            (1..12).each{
                String bulan = (it?.toString()?.size() < 2 ? "0"+it?.toString() : it?.toString())
                def yearMonth = bulan+params?.tahun
                def data = MonthlyBalance.createCriteria().list {
                    eq("yearMonth", yearMonth)
                    eq("companyDealer",companyDealer)
                    accountNumber{
                        eq("staDel","0")
                        like("accountNumber",an.accountNumber + "%")
                        ilike("accountName","%PENDAPATAN%")
                        order("accountNumber","asc")
                    }
                }
                data.each{
                    def bl = (it.realmutation * -1)
                    sumPendapatan  += bl
                    totalPendapatan += bl
                }
            }
            if(sumPendapatan!=0){
                pendapatan << [
                        rekeningName : an?.accountNumber+" "+an?.accountName,
                        rekeningValue : sumPendapatan < 0 ? "(" + konversi.toRupiah((sumPendapatan * (-1))) + ")": konversi.toRupiah(sumPendapatan),
                        totalPendapatan : totalPendapatan < 0 ? "(" + konversi.toRupiah((totalPendapatan * (-1))) + ")": konversi.toRupiah(totalPendapatan),
                ]
            }
        }
        //----------------
        def potongan = []
        def resultsPotongan = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","6%")
            order("accountNumber","asc")
        }
        def sumPotongan = 0.0, totalPotongan = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        resultsPotongan.each { an ->
            sumPotongan = 0.0
            (1..12).each {
                String bulan = (it?.toString()?.size() < 2 ? "0"+it?.toString() : it?.toString())
                def yearMonth = bulan+params?.tahun
                def data = MonthlyBalance.createCriteria().list {
                    eq("yearMonth", yearMonth)
                    eq("companyDealer",companyDealer)
                    accountNumber{
                        eq("staDel","0")
                        like("accountNumber",an.accountNumber + "%")
                        ilike("accountName","POTONGAN %")
                        order("accountNumber","asc")
                    }
                }

                if(data.size() > 0){
                    data.each{
                        def bl = (it.realmutation * -1)
                        sumPotongan  += bl
                        totalPotongan += bl
                    }
                }
            }

            if(sumPotongan!=0){
                potongan << [
                        rekeningName : an?.accountNumber+" "+an?.accountName,
                        rekeningValue : sumPotongan < 0 ? "(" + konversi.toRupiah((sumPotongan * (-1))) + ")": konversi.toRupiah(sumPotongan),
                        totalPotongan : totalPotongan < 0 ? "(" + konversi.toRupiah((totalPotongan * (-1))) + ")": konversi.toRupiah(totalPotongan),
                        pendapatanPotongan : (totalPendapatan + totalPotongan) < 0 ? "(" + konversi.toRupiah(((totalPendapatan + totalPotongan) * (-1))) + ")": konversi.toRupiah(totalPendapatan + totalPotongan),
                ]
            }
        }
        def pendapatanPotongan = totalPendapatan + totalPotongan
        //-------------------biaya
        def biaya = []
        def resultsBiaya = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","7%")
            order("accountNumber","asc")
        }
        def sumBiaya = 0.0, totalBiaya = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        resultsBiaya.each { an ->
            sumBiaya = 0.0
            (1..12).each {
                String bulan = (it?.toString()?.size() < 2 ? "0"+it?.toString() : it?.toString())
                def yearMonth = bulan+params?.tahun
                def data = MonthlyBalance.createCriteria().list {
                    eq("yearMonth", yearMonth)
                    eq("companyDealer",companyDealer)
                    accountNumber{
                        eq("staDel","0")
                        like("accountNumber",an.accountNumber + "%")
                        or{
                            ilike("accountName","BIAYA%")
                            ilike("accountName","BY%")
                        }
                        order("accountNumber","asc")
                    }
                }

                if(data.size() > 0){
                    data.each{
                        sumBiaya  += it.realmutation
                        totalBiaya += it.realmutation
                    }
                }
            }

            if(sumBiaya!=0){
                biaya << [
                        rekeningName : an?.accountNumber+" "+an?.accountName,
                        rekeningValue : sumBiaya < 0 ? "(" + konversi.toRupiah((sumBiaya * (-1))) + ")": konversi.toRupiah(sumBiaya),
                        totalBiaya : totalBiaya < 0 ? "(" + konversi.toRupiah((totalBiaya * (-1))) + ")": konversi.toRupiah(totalBiaya),
                ]
            }
        }
        //GROSS PROFIT
        def grossProfit = (pendapatanPotongan - totalBiaya)
        def lainlain = []
        if(grossProfit!=0){
            lainlain << [
                    rekeningName: "3. GROSS PROFIT",
                    rekeningValue: grossProfit < 0 ? "(" + konversi.toRupiah((grossProfit * (-1))) + ")": konversi.toRupiah(grossProfit),
            ]
        }
        //----- LAIN LAIN
        //-------------------PENDPATAN LAIN2
        def resultsPendLain = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","8%")
            order("accountNumber","asc")
        }
        def sumPendLain = 0.0, totalPendLain = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        String noAKunPendapatan="";
        resultsPendLain.each { an ->
            sumPendLain = 0
            (1..12).each {
                String bulan = (it?.toString()?.size() < 2 ? "0"+it?.toString() : it?.toString())
                def yearMonth = bulan+params?.tahun
                def data = MonthlyBalance.createCriteria().list {
                    eq("yearMonth", yearMonth)
                    eq("companyDealer",companyDealer)
                    accountNumber{
                        eq("staDel","0")
                        like("accountNumber",an.accountNumber + "%")
                        or{
                            ilike("accountName","PENDAPATAN%")
                            ilike("accountName","PEND%")
                        }
                        order("accountNumber","asc")
                    }
                }

                if(data.size() > 0){
                    data.each{
                        def bl = (it.realmutation * -1)
                        sumPendLain  += bl
                        totalPendLain += bl
                        noAKunPendapatan= it?.accountNumber?.accountNumber
                    }
                }
            }
            if(sumPendLain!=0){
                lainlain << [
                        rekeningName: noAKunPendapatan+" " + an.accountName,
                        rekeningValue: sumPendLain < 0 ? "(" + konversi.toRupiah((sumPendLain * (-1))) + ")": konversi.toRupiah(sumPendLain),
                ]
            }

        }
        //-------------------BIAYA LAIN2
        def resultsBiayaLain = AccountNumber.createCriteria().list {
            eq("staDel","0")
            eq("level",params.level as int)
            like("accountNumber","8%")
            order("accountNumber","asc")
        }

        def sumBiayaLain = 0.0, totalBiayaLain = 0.0
        if((params.bulan as int) < 10){
            params.bulan = "0" + (params.bulan as int)
        }
        String noAkunBiaya = ""
        resultsBiayaLain .each { an ->
            sumPendLain = 0
            sumBiayaLain = 0

            (1..12).each {
                String bulan = (it?.toString()?.size() < 2 ? "0"+it?.toString() : it?.toString())
                def yearMonth = bulan+params?.tahun
                def data = MonthlyBalance.createCriteria().list {
                    eq("yearMonth", yearMonth)
                    eq("companyDealer",companyDealer)
                    accountNumber{
                        eq("staDel","0")
                        like("accountNumber",an.accountNumber + "%")
                        or{
                            ilike("accountName","BIAYA%")
                            ilike("accountName","BY%")
                        }
                        order("accountNumber","asc")
                    }
                }

                if(data.size() > 0){
                    data.each{
                        sumBiayaLain  += it.realmutation
                        totalBiayaLain += it.realmutation
                        noAkunBiaya = it?.accountNumber?.accountNumber
                    }
                }
            }
            if(sumBiaya!=0){
                lainlain << [
                        rekeningName: noAkunBiaya+" " + an.accountName,
                        rekeningValue: sumBiayaLain < 0 ? "(" + konversi.toRupiah((sumBiayaLain * (-1))) + ")": konversi.toRupiah(sumBiayaLain),
                ]
            }

        }
        def labaSblmPjk =  (grossProfit + (totalPendLain - totalBiayaLain))

        if(labaSblmPjk!=0){
            lainlain << [
                    rekeningName: "LABA SEBELUM PAJAK",
                    rekeningValue: labaSblmPjk < 0 ? "(" + konversi.toRupiah(labaSblmPjk * (-1)) + ")": konversi.toRupiah(labaSblmPjk),
            ]
        }
        reportData << [
                pendapatan : pendapatan,
                potongan : potongan,
                biaya : biaya,
                lainlain : lainlain,
                companyDealer : companyDealer,
                periode : "Periode " + periode
        ]

        def reportDef = new JasperReportDef(name:'rugiLaba.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("RugiLaba"+params.bulan+""+params.tahun+"_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
