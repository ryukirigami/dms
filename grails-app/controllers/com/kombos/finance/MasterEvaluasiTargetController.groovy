package com.kombos.finance





import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class MasterEvaluasiTargetController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def masterEvaluasiTargetService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
		render masterEvaluasiTargetService.datatablesList(params) as JSON
	}

	def create() {
        def bulanPeriode = new Date().format("MM")
        def tahunPeriode = new Date().format("yyyy")
		def result = masterEvaluasiTargetService.create(params)

        if(!result.error)
            return [masterEvaluasiTargetInstance: result.masterEvaluasiTargetInstance,bulanPeriode:bulanPeriode,tahunPeriode:tahunPeriode]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		 def result = masterEvaluasiTargetService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["MasterEvaluasiTarget", result.masterEvaluasiTargetInstance.id])
            redirect(action:'show', id: result.masterEvaluasiTargetInstance.id)
            return
        }

        render(view:'create', model:[masterEvaluasiTargetInstance: result.masterEvaluasiTargetInstance])
	}

	def show(Long id) {
		def result = masterEvaluasiTargetService.show(params)

		if(!result.error)
			return [ masterEvaluasiTargetInstance: result.masterEvaluasiTargetInstance]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = masterEvaluasiTargetService.show(params)
        def periode = result.masterEvaluasiTargetInstance.monthYear as String
        def bulanPeriode = periode.substring(0,2)
        def tahunPeriode = periode.substring(2)
		if(!result.error)
			return [ masterEvaluasiTargetInstance: result.masterEvaluasiTargetInstance,tahunPeriode: tahunPeriode,bulanPeriode:bulanPeriode  ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = masterEvaluasiTargetService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["MasterEvaluasiTarget", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[masterEvaluasiTargetInstance: result.masterEvaluasiTargetInstance.attach()])
	}

	def delete() {
		def result = masterEvaluasiTargetService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["MasterEvaluasiTarget", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(MasterEvaluasiTarget, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
