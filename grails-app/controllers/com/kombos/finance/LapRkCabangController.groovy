package com.kombos.finance

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.parts.Konversi
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class LapRkCabangController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService
    def konversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]
    def index() {}
    def previewData(){
        def an = session?.userCompanyDealer;
        if(params?.companyDealer){
            an = CompanyDealer.get(params?.companyDealer?.toLong())
        }
        List<JasperReportDef> reportDefList = []
        Calendar cal = Calendar.getInstance()
        cal.set(params.tahun as int,(params.bulan as int) - 1,1 )
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))

        def reportData = []
        String bulanParam =  params.tahun + "-" + params.bulan +"-1"
        Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
        String bulanParam2 =  params.tahun + "-" + params.bulan + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
        def periode = tgl.format("MMMM yyyy").toUpperCase()

        int count = 0
        def startingBalance = 0, debit = 0,credit = 0, realMutasi = 0, endingBalance = 0
        def cd = an
        def data = MonthlyBalance.createCriteria().list {
            eq("yearMonth", (params.bulan+""+params.tahun).toString())
            eq("companyDealer", an)
            accountNumber{
                eq("staDel","0")
                or{
                    eq("accountNumber",cd?.m011NomorAkunLainnya?.accountNumber)
                    eq("accountNumber",cd?.m011NomorAkunTransfer?.accountNumber)
                }
                order("accountNumber","asc")
            }
        }
        data.each{
            startingBalance+= it.startingBalance
            debit += it.debitMutation
            credit += it.creditMutation
            realMutasi += it.realmutation
            endingBalance += it.endingBalance
        }
        count++
        reportData << [
                no : count,
                nama : an.m011NamaWorkshop,
                startingBalance : startingBalance < 0 ? "(" + konversi.toRupiah((startingBalance * (-1))) + ")": konversi.toRupiah(startingBalance),
                debit : debit < 0 ? "(" + konversi.toRupiah((debit * (-1))) + ")": konversi.toRupiah(debit),
                credit : credit < 0 ? "(" + konversi.toRupiah((credit * (-1))) + ")": konversi.toRupiah(credit),
                realMutation: realMutasi < 0 ? "(" + konversi.toRupiah((realMutasi * (-1))) + ")": konversi.toRupiah(realMutasi),
                endingBalance : endingBalance < 0 ? "(" + konversi.toRupiah((endingBalance * (-1))) + ")": konversi.toRupiah(endingBalance),
                companyDealer : an,
                periode : "Periode " + periode,
        ]



        def reportDef = new JasperReportDef(name:'lapRkCabang.jasper',
                fileFormat:params.format=="x" ? JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                reportData: reportData

        )
        reportDefList.add(reportDef)

        def file = File.createTempFile("Lap_RK_Cabang_",params.format=="x" ? ".xls" : ".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        if(params.format=="x"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
}
