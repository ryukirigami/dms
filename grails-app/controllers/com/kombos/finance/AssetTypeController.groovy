package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class AssetTypeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def assetTypeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render assetTypeService.datatablesList(params) as JSON
    }

    def create() {
        def result = assetTypeService.create(params)

        if (!result.error)
            return [assetTypeInstance: result.assetTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = assetTypeService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["AssetType", result.assetTypeInstance.id])
            redirect(action: 'show', id: result.assetTypeInstance.id)
            return
        }
        render(view: 'create', model: [assetTypeInstance: result.assetTypeInstance])
    }

    def show(Long id) {
        def result = assetTypeService.show(params)
        if (!result.error)
            return [assetTypeInstance: result.assetTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = assetTypeService.show(params)

        if (!result.error)
            return [assetTypeInstance: result.assetTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = assetTypeService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["AssetType", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }
        render(view: 'edit', model: [assetTypeInstance: result.assetTypeInstance.attach()])
    }

    def delete() {
        def result = assetTypeService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["AssetType", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(AssetType, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
