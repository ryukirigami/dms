package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class JournalTypeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def journalTypeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render journalTypeService.datatablesList(params) as JSON
    }

    def create() {
        def result = journalTypeService.create(params)

        if (!result.error)
            return [journalTypeInstance: result.journalTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = journalTypeService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["JournalType", result.journalTypeInstance.id])
            redirect(action: 'show', id: result.journalTypeInstance.id)
            return
        }

        render(view: 'create', model: [journalTypeInstance: result.journalTypeInstance])
    }

    def show(Long id) {
        def result = journalTypeService.show(params)

        if (!result.error)
            return [journalTypeInstance: result.journalTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = journalTypeService.show(params)
        if (!result.error)
            return [journalTypeInstance: result.journalTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = journalTypeService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["JournalType", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [journalTypeInstance: result.journalTypeInstance.attach()])
    }

    def delete() {
        def result = journalTypeService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["JournalType", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(JournalType, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}
//woke
