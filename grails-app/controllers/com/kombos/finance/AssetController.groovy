package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class AssetController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def assetService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
        params.companyDealer=session.userCompanyDealer
	}

	def list(Integer max) {
        [
            assetTypes: AssetType.list().typeAsset,
            assetStatuses: AssetStatus.list().assetStatus]
	    }

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session.userCompanyDealer
		render assetService.datatablesList(params) as JSON
	}

	def create() {
		def result = assetService.create(params)

        if(!result.error)
            return [assetInstance: result.assetInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.companyDealer=session.userCompanyDealer
		 def result = assetService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Asset", result.assetInstance.id])
            redirect(action:'show', id: result.assetInstance.id)
            return
        }

        render(view:'create', model:[assetInstance: result.assetInstance])
	}

	def show(Long id) {
		def result = assetService.show(params)

		if(!result.error)
			return [ assetInstance: result.assetInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = assetService.show(params)

		if(!result.error)
			return [ assetInstance: result.assetInstance, params: params ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = assetService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Asset", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[assetInstance: result.assetInstance.attach()])
	}

	def delete() {
		def result = assetService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Asset", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(Asset, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

}
