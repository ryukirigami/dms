package com.kombos.finance

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class JournalDetailController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def journalDetailService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render journalDetailService.datatablesList(params) as JSON
    }

    def create() {
        def result = journalDetailService.create(params)

        if (!result.error)
            return [journalDetailInstance: result.journalDetailInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = journalDetailService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["JournalDetail", result.journalDetailInstance.id])
            redirect(action: 'show', id: result.journalDetailInstance.id)
            return
        }

        render(view: 'create', model: [journalDetailInstance: result.journalDetailInstance])
    }

    def show(Long id) {
        def result = journalDetailService.show(params)

        if (!result.error)
            return [journalDetailInstance: result.journalDetailInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = journalDetailService.show(params)

        if (!result.error)
            return [journalDetailInstance: result.journalDetailInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = journalDetailService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["JournalDetail", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [journalDetailInstance: result.journalDetailInstance.attach()])
    }

    def delete() {
        def result = journalDetailService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["JournalDetail", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(JournalDetail, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
