package com.kombos.production

import com.kombos.administrasi.GroupManPower
import com.kombos.administrasi.JenisAlert
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.maintable.*
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.Progress

class TeknisiGrController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def noWo = null, nopol = null, model = null, stall = null, status = null, nama = null, radio = null, catatan = "-", namaJob = null, alasan=null
        def staReadOnly = false, idJob= null
        def jpb = JPB.get(params.id)
        def datecreatedLogon = datatablesUtilService?.syncTime()
        def aktualJobOff = Actual?.findAllByJpbAndStaDelAndStatusActual(jpb,'0',StatusActual.findById(4))
        def aktual = Actual?.findAllByJpbAndStaDel(jpb,'0',[sort: 'dateCreated',order: 'asc'])
        def c = JobRCP.createCriteria()
        def jobRcp = c.list {
            eq("reception",jpb?.reception)
            eq("staDel","0")
            aktualJobOff.each {
                ne("operation",it.operation)
            }
            or{
                isNull("t402StaOkCancel")
                eq("t402StaOkCancel","0")
            }
        }

        if(aktual?.size()>0){
            def aktualOpe = Actual?.findAllByJpbAndStaDelAndOperation(jpb,'0',aktual?.last()?.operation,[sort : 'dateCreated',order : 'desc'])
            if(aktualOpe?.last()?.statusActual?.m452Id == "4"){
                staReadOnly = false
                radio = 0
                status = "SELESAI SERVICE"
                alasan = 2
            }else{
                staReadOnly = true
                catatan =aktual?.last()?.t452Ket
                radio = aktual?.size() > 0 ? aktual?.last()?.statusActual?.m452Id?.toInteger() : null
                if(aktual?.last()?.t452staProblemTeknis?.isNumber()){
                    alasan=aktual?.last()?.t452staProblemTeknis?.toInteger()
                }
                if(radio==0){
                    status = "TUNGGU SERVICE"
                }else{
                    status = "WORK IN PROGRESS"
                }
                namaJob = aktual?.last()?.operation?.m053NamaOperation
                def jrp = JobRCP.findByReceptionAndOperationAndStaDel(aktual?.last()?.reception,aktual?.last()?.operation,"0")
                if(jrp?.t402StaOkCancel=="1"){
                    staReadOnly = false
                }
                idJob = JobRCP.findByReceptionAndOperationAndStaDel(jpb?.reception, aktual?.last()?.operation, "0").id
            }

        }else{
            radio = 0
            status = "TUNGGU SERVICE"
            alasan = 2
        }
        noWo = jpb?.reception?.t401NoWO
        nopol = jpb?.reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID  + " " + jpb?.reception?.historyCustomerVehicle?.t183NoPolTengah + " " + jpb?.reception?.historyCustomerVehicle?.t183NoPolBelakang
        model = jpb?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        nama =  jpb?.namaManPower?.id
        catatan = catatan==""?"-":catatan
            [id:params.id,noWo : noWo,nopol:nopol, model:model, stall:stall, tanggal:new Date().format("yyyy-MM-dd"),status:status, radio : radio, nama:nama,alasan:alasan,
                    namaJob:jobRcp,catatan:catatan,staReadOnly:staReadOnly,dataJob:namaJob,idJob:idJob]
    }

    def save(){

          def jpbb = JPB.get(params.id)
            def jobs = JobRCP.findById(params.job.id as Long)
            def  id = new Date().format("yyyyMMddHHmmss")
            def addActual = new Actual()
            addActual?.companyDealer = session.userCompanyDealer
            addActual?.jpb = jpbb
            addActual?.t452ID = id
            addActual?.namaManPower = jpbb?.namaManPower
            addActual?.stall = jpbb?.stall
            addActual?.reception = jpbb?.reception
            addActual?.operation = jobs?.operation
            addActual?.statusActual = StatusActual?.findByM452Id(params.aksi)
            addActual?.t452TglJam = datatablesUtilService?.syncTime()
            addActual?.t452Ket = params.catatan
            if(params.alasan){
                addActual?.t452staProblemTeknis = params.alasan
            }else{
                addActual?.t452staProblemTeknis = '-'
            }

            addActual?.staDel = 0
            addActual?.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
            addActual?.dateCreated = datatablesUtilService?.syncTime()
            addActual?.lastUpdated = datatablesUtilService?.syncTime()
            addActual?.lastUpdProcess = "INSERT"
            addActual?.save(flush:true)
            addActual?.errors.each{
                println it
            }

            String sSta = null
            if(params.aksi=="1" || params.aksi=="3"){
                sSta = "Sedang Service"
            }

            if(params.aksi=="2"){
                sSta = "Sedang Panggil Foreman / Istirahat"
            }

            if(params.aksi=="4"){
                sSta = "Selesai Service"
            }

            if(sSta==null){
                def progresss = Progress.findByReceptionAndStaDel(jpbb?.reception,"0",[sort : 'dateCreated',order : 'descending'])
                sSta = progresss?.statusKendaraan?.m999NamaStatusKendaraan
            }

            def saveStatus = StatusKendaraan.findByM999NamaStatusKendaraanIlikeAndStaDel("%"+sSta+"%", "0")
            if(!saveStatus){
                String idStaKendaraan = "";
                def terakhir = StatusKendaraan?.list()?.size()
                terakhir=terakhir+1
                idStaKendaraan = terakhir?.toString()
                if(terakhir?.toString()?.length()==1){
                    idStaKendaraan = "0"+terakhir
                }
                saveStatus = new StatusKendaraan()
                saveStatus.m999NamaStatusKendaraan= sSta
                saveStatus.staDel= "0"
                saveStatus.m999Id= idStaKendaraan
                saveStatus.lastUpdProcess= "INSERT"
                saveStatus.createdBy= "SYSTEM"
                saveStatus.dateCreated= datatablesUtilService?.syncTime()
                saveStatus.lastUpdated= datatablesUtilService?.syncTime()
                saveStatus.save(flush: true)
            }
            def addProgres = new Progress()
            addProgres?.reception = jpbb?.reception
            addProgres?.t999ID = id
            addProgres?.companyDealer = session.userCompanyDealer
            addProgres?.t999TglJamStatus = new Date()
            addProgres?.statusKendaraan = saveStatus
            addProgres?.t999XKet = params.catatan
            addProgres?.t999XNamaUser =  org.apache.shiro.SecurityUtils.subject.principal.toString()
            addProgres?.t999XDivisi = 'DIVISI'
            addProgres?.staDel =  '0'
            addProgres?.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
            addProgres?.lastUpdProcess = "INSERT"
            addProgres?.dateCreated = datatablesUtilService?.syncTime()
            addProgres?.lastUpdated = datatablesUtilService?.syncTime()
            addProgres?.save(flush:true)
            addProgres.errors.each{
                println it
           }

        def jobList = JobRCP.createCriteria().list {
            eq("staDel","0")
            or{
                ilike("t402StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t402StaTambahKurang","%1%")
                        ilike("t402StaApproveTambahKurang","%1%")
                    }
                }
                and {
                    isNull("t402StaTambahKurang")
                    isNull("t402StaApproveTambahKurang")
                }
            }
            reception{
                eq("t401NoWO", params.noWo, [ignoreCase: true])
            }
        }

        def jml = jobList.size()
        def act = Actual.findAllByReceptionAndStaDelAndStatusActual(jobList?.last()?.reception, "0", StatusActual.findByM452StatusActualIlike("%Clock Off%"));
        def jml2 = act.size()

        if(jml == jml2){
            //insert alert
            def jenisAlert = JenisAlert.findByM901NamaAlert('Final Inspection')
            if(!jenisAlert){
                jenisAlert = new JenisAlert(
                    m901Id: '001',
                    m901NamaAlert: 'Final Inspection',
                    m901StaPerluTindakLanjut: '1',
                    m901NamaFormTindakLanjut: 'Form Final Inspection',
                    staDel:"0",
                    createdBy: 'SYSTEM',
                    lastUpdProcess:'INSERT',
                    dateCreated : datatablesUtilService?.syncTime(),
                    lastUpdated : datatablesUtilService?.syncTime()
                )
                jenisAlert.save(flush:true)
                jenisAlert.errors.each{ println it }
            }

            def tipeAlert = TipeAlert.findByM902JenisAlert('Final Inspection')
            if(!tipeAlert){
                tipeAlert = new TipeAlert(
                    m902Id: '1',
                    m902JenisAlert: 'Final Inspection',
                    m902StaDel:"0",
                    createdBy: 'SYSTEM',
                    lastUpdProcess:'INSERT',
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
                )
                tipeAlert.save(flush:true)
                tipeAlert.errors.each { println it}
            }

            def tujuanAlert = TujuanAlert.findByJenisAlertAndUserProfile(JenisAlert.findByM901NamaAlertAndStaDel('Final Inspection',"0") ,User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()))
            if(!tujuanAlert){
                tujuanAlert = new TujuanAlert(
                    userProfile:User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()),
                    jenisAlert:jenisAlert,
                    m903StaDel:"0",
                    createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess:'INSERT',
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
                )
                tujuanAlert.save(flush:true)
                tujuanAlert.errors.each { println 'ERORNYA TUJUAN ALERT : ' +it}
            }

            def namaForeman = "";
            if(jpbb?.namaManPower){
                def grups = GroupManPower.createCriteria().get {
                    eq("staDel","0");
                    namaManPowers {
                        eq("id",jpbb?.namaManPower?.id);
                    }
                    order("dateCreated","desc");
                    maxResults(1);
                }
                if(grups){
                    namaForeman = grups?.namaManPowerForeman?.t015NamaLengkap
                }
            }
            def alert = new Alert(
                    namaFK: jpbb?.reception?.t401NoWO,
                    companyDealer:session.userCompanyDealer,
                    createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdProcess: 'INSERT',
                    staDel: "0",
                    jenisAlert: jenisAlert,
                    noDokumen: jpbb?.reception?.historyCustomerVehicle?.fullNoPol,
                    tglAksi: datatablesUtilService?.syncTime(),
                    tanggal: datatablesUtilService?.syncTime(),
                    tglJamAlert: datatablesUtilService?.syncTime(),
                    tglJamBaca: new Date(),
                    tglJamTinjakLanjut: new Date(),
                    tipeAlert: tipeAlert,
                    xNamaUserBaca: namaForeman,
                    xNamaUserTindakLanjut: org.apache.shiro.SecurityUtils.subject.principal.toString()
                )
                alert.save(flush:true)
                alert.errors.each {println 'iki erore ALERT cah : '+it}


        }
        render "ok"
    }
}

