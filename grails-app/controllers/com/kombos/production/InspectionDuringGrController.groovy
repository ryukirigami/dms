package com.kombos.production

import com.kombos.administrasi.Operation
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class InspectionDuringGrController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss") ]
    }
    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [noWo: params.noWo, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsublist() {
        [noWo: params.noWo,idJob:params.idJob, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def c = Reception.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","GR")
                }
            }

            if(params."sCriteria_noWO"){
                ilike("t401NoWO","%" + (params."sCriteria_noWO" as String) + "%")
            }

            if(params."sCriteria_noPol"){
                historyCustomerVehicle{
                    ilike("fullNoPol","%" + (params."sCriteria_noPol" as String) + "%")
                }
            }

            if(params.cekTglWo){
                if (params."sCriteria_t017Tanggal" && params."sCriteria_t017Tanggal2") {

                        ge("t401TanggalWO", params."sCriteria_t017Tanggal")
                        lt("t401TanggalWO", params."sCriteria_t017Tanggal2" + 1)

                }
            }

            eq("staDel","0");
            eq("staSave","0");
            isNotNull("t401NoWO")
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)
            order("id",'desc')
        }
        def rows = []
        results.each {
            def nopol  = it?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " + it?.historyCustomerVehicle?.t183NoPolTengah + " " + it?.historyCustomerVehicle.t183NoPolBelakang
            rows << [
                    id: it.id,
                    noWo: it?.t401NoWO,
                    noPol: nopol,
                    tglWo: it?.t401TanggalWO.format("dd/MM/yyyy"),
                    SA: it?.t401NamaSA,
            ]
        }
        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON

    }
    def datatablesSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def ret
        def x = 0
        def c = JobRCP.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                ilike("t401NoWO","%" + params.noWo + "%")
            }
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)
        }
        def rows = []

        results.each {
            def jpb = JPB.findByReception(Reception.findByT401NoWO(params.noWo))
            rows << [
                    id: it.id,
                    operation: it?.operation?.m053NamaOperation,
                    foreman: jpb?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,
                    noWo : params.noWo,
                    idJob: it.operation?.id
            ]
        }

        ret =  [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
          render  ret as JSON
    }
    def datatablesSubList2() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def ret
        def x = 0
        def c = FinalInspection.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                ilike("t401NoWO","%" + params.noWo + "%")
            }
        }
        println "list2 : " + results.dump()
        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    FI: 'Final Inspection',
                    t508TglJamMulai: it.t508TglJamMulai.format("dd/MM/yyyy HH:mm"),
                    t508TglJamSelesai: it.t508TglJamSelesai.format("dd/MM/yyyy HH:mm"),
                    t508StaInspection: it.t508StaInspection=="1"?"Ya":"Tidak",
                    t508Keterangan: it.t508Keterangan,
                    t508RedoKeProses_M190_ID: it.operation?.m053NamaOperation,
                    noWo : params.noWo,

            ]
        }

        ret =  [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render  ret as JSON
    }

    def datatablesSubSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def ret
        def c = IDR.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                ilike("t401NoWO","%" + params.noWo + "%")
            }
            eq("t506M053_JobID_Redo",Operation.findById(params.idJob))
            order("dateCreated","asc")
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    IDR: 'Inspect During Repair',
                    t506TglJamMulai: it.t506TglJamMulai.format("dd/MM/yyyy HH:mm"),
                    t506TglJamSelesai: it.t506TglJamSelesai.format("dd/MM/yyyy HH:mm"),
                    t506StaIDR: it.t506StaIDR=="1"?"Ya":"Tidak",
                    t506AlasanIDR: it.t506AlasanIDR,
                    t506RedoKeProses_M190_ID: it.t506RedoKeProses_M190_ID?.m053NamaOperation,
                    noWo : params.noWo,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }

    def inspecDuring() {
        def id=null, noWo = null, nopol = null, model = null, stall = null, status = null, nama = null
        def staRedo = null, alasan = null, staIDR = null
        def jobrcp = JobRCP.findById(params.idJob as Long)
        def reception = jobrcp.reception
        def jpb = JPB.findByReception(reception)
        def c = JobRCP.createCriteria()
        def jobRcp = c.list {
            eq("reception",jpb?.reception)
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)

        }
        nopol = HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.kodeKotaNoPol?.m116ID  + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolTengah + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        nama =  jpb?.namaManPower?.t015NamaBoard
        def idr = IDR.findAllByCompanyDealerAndReceptionAndT506M053_JobID_Redo(session.userCompanyDealer,reception,jobrcp.operation)
        if(idr){
            staIDR =  idr?.last()?.t506StaIDR
            staRedo = idr?.last()?.t506StaRedo
            alasan = idr?.last()?.t506AlasanIDR
        }
        def jamMulai = new Date().format("dd/MM/yyyy HH:mm:ss")
        [idJob:params.idJob,noWo : reception.t401NoWO,nopol:nopol, model:model, stall:stall, tanggal:new Date().format("yyyy-MM-dd"),
                nama:nama,staIDR:staIDR,staRedo:staRedo,alasan:alasan,jobRcp:jobRcp,jamMulai:jamMulai]
    }
    def editInspect(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        Date date41 = dateFormat.parse(params.jamMulai)
        def jobrcp = JobRCP.findById(params.idJob as Long)
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReception(reception)

        def idr = new IDR()
        idr?.reception = reception
        idr?.companyDealer = session.userCompanyDealer
        idr?.stall = jpb.stall
        idr?.t506M053_JobID_Redo = jobrcp?.operation
        idr?.t506StaIDR = params.staIDR
        idr?.t506TglJamMulai = date41
//        idr?.t506TglJamSelesai = new Date()
        idr?.t506TglJamSelesai = datatablesUtilService?.syncTime()
        idr?.t506AlasanIDR = params.alasan?:'-'
        idr?.t506StaRedo = params.staRedo
        idr?.userProfile = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        idr?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        idr?.lastUpdProcess = 'INSERT'
        idr?.dateCreated = datatablesUtilService?.syncTime()
        idr?.lastUpdated = datatablesUtilService?.syncTime()

        if(params.staRedo=='1'){
            idr?.t506RedoKeProses_M190_ID = Operation.findByM053NamaOperation(params.redoKeJob)
        }
        idr?.save(flush: true)
        println idr.dump()
        render "ok"
    }
}
