package com.kombos.production

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.board.JPB
import com.kombos.board.JPBService
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.ApprovalT770
import com.kombos.maintable.KebutuhanPart
import com.kombos.maintable.KirimAlertKepada
import com.kombos.maintable.StatusKendaraan
import com.kombos.parts.Goods
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.Progress
import grails.converters.JSON
import groovy.time.TimeCategory

class JobInstructionGrController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService
    def jobInstructionGrService
    def jasperService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sublist() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }
        def subsublist() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss"),idJob:params.idJob]
    }
    def subsubsublist() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss"),teknisi: params.teknisi,idJob:params.idJob]
    }

    def datatablesList() {
        session.exportParams=params
        params?.companyDealer = session?.userCompanyDealer
        render jobInstructionGrService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        render jobInstructionGrService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {
        render jobInstructionGrService.datatablesSubSubList(params) as JSON
    }
    def datatablesSubSubSubList() {
        render jobInstructionGrService.datatablesSubSubSubList(params) as JSON
    }

    def ambilWO(){
        def id=null, noWo = null, nopol = null, model = null, stall = null, tanggalWo = null
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReception(reception)
        nopol = reception?.historyCustomerVehicle?.fullNoPol
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        id = reception?.id
        tanggalWo = reception?.t401TanggalWO

        [id:id,noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggalWO:tanggalWo,tanggal:new Date().format("yyyy-MM-dd HH:mm:ss")]
    }

    def ubahAmbilWo(){
        def reception = Reception.get(params.id)
        if(reception){
            reception?.t401StaAmbilWO = params.updateStaWO
            reception?.t401TglJamAmbilWO = new Date()
            reception?.lastUpdated = datatablesUtilService?.syncTime()
            reception?.save(flush:true)
            render "OK"
        }

    }

    def addTeknisi(){
        def id=null, noWo = null, nopol = null, model = null, stall = null, tanggalWo = null
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReception(reception)
        nopol = HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle.customerVehicle)?.kodeKotaNoPol?.m116ID  + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolTengah + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        id = reception.id
        tanggalWo = reception?.t401TanggalWO
//        jpb?.t451IDJPB=id
//        jpb?.reception=reception
//        jpb?.namaManPower=params.id
//        jpb.save(flush:true)

        [id:id,noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggalWO:tanggalWo,tanggal:new Date().format("yyyy-MM-dd HH:mm:ss")]
    }
    def editTeknisi(){
        def rec = Reception.findByT401NoWO(params.noWo)
        def jpb = new JPB()
        def jpbExist = JPB.findByReceptionAndStaDel(rec,"0")
        def stall = jpbExist?.stall
        jpb?.companyDealer = session.userCompanyDealer
        jpb?.t451IDJPB = jpb.count() + 1
        jpb?.alasanRefund = AlasanRefund.first()
        jpb?.reception = rec
//        jpb?.t451TglJamPlan = new Date()
        jpb?.t451TglJamPlan = datatablesUtilService?.syncTime()
        jpb?.namaManPower = NamaManPower.findByT015NamaBoard(params.teknisi)
        jpb?.stall = stall
        jpb?.flatRate = FlatRate.findByOperation(Operation.findByM053JobsId(params.job))
        jpb?.t451StaTambahWaktu= '1'
        jpb?.t451staOkCancelReSchedule = '1'
        jpb?.t451AlasanReSchedule = '1'
        jpb?.t451TglJamPlanFinished=rec.t401TglJamJanjiPenyerahan
        jpb?.staDel= '0'
        jpb?.lastUpdProcess = "INSERT"
        jpb?.dateCreated = datatablesUtilService?.syncTime()
        jpb?.lastUpdated = datatablesUtilService?.syncTime()
        jpb?.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        jpb?.save(flush: true)
        jpb?.errors?.each {println it}
        render "OK"
    }

    def kurangiJob(){
        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_KURANGI_JOB)
        def approver = ""
        if(kegiatanApproval){
            for(NamaApproval namaApproval : kegiatanApproval.namaApprovals){
                if(namaApproval.userProfile.companyDealer.id == session.userCompanyDealerId){
                    approver += namaApproval.userProfile.fullname + "\r\n"
                }
            }
        }else{
            def kegiatanApprovalAdd = new KegiatanApproval()
            kegiatanApprovalAdd?.afterApprovalService = "jobInstructionGrService"
            kegiatanApprovalAdd?.createdBy= "SYSTEM"
            kegiatanApprovalAdd?.lastUpdProcess= "INSERT"
            kegiatanApprovalAdd?.dateCreated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.lastUpdated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.m770IdApproval= KegiatanApproval.last().m770IdApproval + 1
            kegiatanApprovalAdd?.m770KegiatanApproval= KegiatanApproval.APPROVAL_KURANGI_JOB
            kegiatanApprovalAdd?.m770NamaDomain= "com.kombos.reception.reception"
            kegiatanApprovalAdd?.m770NamaFieldDiupdate= "T402_STADEL"
            kegiatanApprovalAdd?.m770NamaFk = "T402_STADEL"
            kegiatanApprovalAdd?.m770NamaFormDetail = ""
            kegiatanApprovalAdd?.m770NamaTabel = "T402_JOBRCP"
            kegiatanApprovalAdd?.staDel= "0"
            kegiatanApprovalAdd?.m770StaButuhNilai = "0"
            kegiatanApprovalAdd?.save(flush: true)
        }
        def id = null, noWo = null, nopol = null, model = null, stall = null, tanggalWo = null, aprrover = null
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReception(reception)
        nopol = HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle.customerVehicle)?.kodeKotaNoPol?.m116ID  + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolTengah + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        tanggalWo = reception?.t401TanggalWO
        def jobRcp = JobRCP.findById(params.idRcp as Long)
        [  noWo:params.noWo, nopol:nopol, model:model, stall:stall, tanggalWO:tanggalWo, tanggal:new Date().format("yyyy-MM-dd HH:mm:ss"), aprrover:aprrover,jobRcp:jobRcp,approver:approver]
    }

    def editKurangiJob(){
        def reception = Reception.findByT401NoWO(params.noWo)
        def jobRCP = JobRCP.findById(params.idJobRcp as Long)
        def approval = new ApprovalT770()
        approval?.createdBy = "SYSTEM"
        approval?.companyDealer = session.userCompanyDealer
        approval?.t770NoDokumen = "Dokumen_"+params.noWo
        approval?.kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_KURANGI_JOB)
        approval?.lastUpdProcess= "INSERT"
        approval?.dateCreated = datatablesUtilService?.syncTime()
        approval?.lastUpdated = datatablesUtilService?.syncTime()
        approval?.staDel = "0"
        approval?.t770CurrentLevel = 0
        approval?.t770FK = params.idJobRcp
        approval?.t770Pesan = params.pesan
        approval?.t770xNamaUser = "USER"
        approval?.t770TglJamSend = datatablesUtilService?.syncTime()
        approval?.dateCreated = datatablesUtilService?.syncTime()
        approval?.lastUpdated = datatablesUtilService?.syncTime()
        approval?.save(flush: true)

        render "OK"
    }

    def sendProblemFinding(){
        def id=null, noWo = null, nopol = null, model = null, stall = null, teknisi = null, job = null
        def deskripsiProblem = null, tanggalUpdate = null, tanggalProblem = null
        def act = Actual.findById(params.id)
        def reception = act?.jpb?.reception
        def jpb = act?.jpbId
        noWo =  reception.t401NoWO
        nopol = HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle.customerVehicle)?.kodeKotaNoPol?.m116ID  + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolTengah + " " + HistoryCustomerVehicle.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = act?.jpb?.stall?.m022NamaStall
        teknisi = act?.jpb?.namaManPower?.t015NamaBoard + "(" + act?.jpb?.namaManPower?.t015NamaLengkap + ")"
        job = act.operation?.m053NamaOperation
        deskripsiProblem = act?.t452Ket
        tanggalUpdate = new Date().format("YYYY-MM-dd HH:mm:ss")
        tanggalProblem = act?.t452TglJam.format("dd/MM/yyyy HH:mm")
        [noWo:noWo, nopol : nopol, model:model, stall: stall, teknisi:teknisi, job:job,deskripsiProblem:deskripsiProblem,
                tanggalProblem:tanggalProblem,tanggalUpdate:tanggalUpdate,actId:params.id
        ]
    }
    def editSendProblemFinding(){
        def act = Actual.findById(params.actId)
        def jpb = act?.jpb
        def masalah = new Masalah()
        println masalah?.companyDealer = session.userCompanyDealer
        println masalah?.jpb = jpb
        println masalah?.actual = act
        println masalah?.t501ID = Masalah.count + 1
        println masalah?.t501staProblemTeknis = params.jenisProblem as String
        println masalah?.t501CatatanTambahan = params.catatanTambahan as String
        println masalah?.t501TanggalKirim = datatablesUtilService?.syncTime()
        println masalah?.t501NamaKirim = org.apache.shiro.SecurityUtils.subject.principal.toString()
        println masalah?.t501StaButuhPause = params.butuhPauseJob as String
        println masalah?.t501Sta_TL_ATL_NTL =params.sta_TL_ATL_NTL as String
        println masalah?.t501StaBisaKrjSdri = params.bisaDikerjakanSendiri as String
        println masalah?.t501StaBthTambahWkt = params.perluTambahanWaktu as String
        masalah?.t501DurasiTambahWaktu = params.durasi as String
        masalah?.t501StaBthJob = params.butuhTambahanJob as String
        masalah?.t501KetBthJob = params.namaJob as String
        masalah?.t501StaBthParts = params.butuhParts as String
        masalah?.t501HasilDSS = '0'
        masalah?.t501StaSolved = '0'
//        masalah?.t501TglSolved=new Date()
//        masalah?.t501TglUpdate=params.
        masalah?.t501StaTagihKepada = '0'
        masalah?.statusKendaraan = StatusKendaraan.get(params.statusKendaraan)
        masalah?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        masalah?.lastUpdProcess = "INSERT"
        masalah?.dateCreated = datatablesUtilService?.syncTime()
        masalah?.save(flush:true)
        masalah.errors.each {
            println "Masalah : " + it
        }

        def dataAlert = ''
        if(params.alert1 == '1'){
            def alert = new KirimAlertKepada()
            alert?.companyDealer = session.userCompanyDealer
            alert?.jPB = jpb
            alert?.actual = act
            alert?.masalah = masalah
            alert?.userRole = Role.findById('SERVICE_ADVISOR_GENERAL_REPAIR')
            alert?.createdBy  = org.apache.shiro.SecurityUtils.subject.principal.toString()
            alert?.updatedBy  = org.apache.shiro.SecurityUtils.subject.principal.toString()
            alert?.lastUpdProcess = 'INSERT'
            alert?.dateCreated = datatablesUtilService?.syncTime()
            alert?.lastUpdated = datatablesUtilService?.syncTime()
            alert?.save(flush:true)
            alert.errors.each {
                println "alert : " + it
            }
        }
        if(params.alert2 == '1'){
            def alert2 = new KirimAlertKepada()
            alert2?.companyDealer = session.userCompanyDealer
            alert2?.jPB = jpb
            alert2?.actual = act
            alert2?.masalah = masalah
            alert2?.userRole = Role.findById('KEPALA_BENGKEL')
            alert2?.createdBy  = org.apache.shiro.SecurityUtils.subject.principal.toString()
            alert2?.lastUpdProcess = 'INSERT'
            alert2?.dateCreated = datatablesUtilService?.syncTime()
            alert2?.lastUpdated = datatablesUtilService?.syncTime()
            alert2?.save(flush:true)
            alert2.errors.each {
                println "alert2 : " + it
            }
        }

        def prog = new Progress()
        prog?.reception = jpb?.reception
        prog?.t999ID =  new Date().format("YYYYMMddHHmmss").toString()
//        prog?.t999TglJamStatus = new Date()
        prog?.t999TglJamStatus = datatablesUtilService?.syncTime()
        prog?.statusKendaraan = StatusKendaraan.findByM999NamaStatusKendaraan(params.statusKendaraan)
        prog?.t999XKet = params.catatanTambahan as String
        prog?.t999XNamaUser =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        prog?.t999XDivisi = 'divisi'
        prog?.staDel = '0'
        prog?.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        prog?.lastUpdProcess = 'INSERT'
        prog?.dateCreated = datatablesUtilService?.syncTime()
        prog?.lastUpdated = datatablesUtilService?.syncTime()
        prog?.save(flush:true)
        prog.errors.each {
            println "prog : " + it
        }

        def jsonArrayGoods = JSON.parse(params.idGoods)
        def jsonArrayQtys = JSON.parse(params.qtys)
        int a=0;

        jsonArrayGoods.each {
            println jsonArrayQtys[a]
            def kebutuhanPart = new KebutuhanPart()
            kebutuhanPart?.companyDealer = jpb?.companyDealer
            kebutuhanPart?.jpb = jpb
            kebutuhanPart?.actual = act
            kebutuhanPart?.masalah = masalah
            kebutuhanPart?.goods = Goods.findByM111ID(it)
            kebutuhanPart?.t509Qty1 = new Double(jsonArrayQtys[a])
            kebutuhanPart?.t509Qty2= new Double(jsonArrayQtys[a])
            kebutuhanPart?.createdBy = "SYSTEM"
            kebutuhanPart?.updatedBy  = "SYSTEM"
            kebutuhanPart?.lastUpdProcess  = "INSERT"
            kebutuhanPart?.dateCreated = datatablesUtilService?.syncTime()
            kebutuhanPart?.lastUpdated = datatablesUtilService?.syncTime()
            kebutuhanPart?.save(flush:true)
            kebutuhanPart.errors.each {
                println "kebutuhanPart : " + it
            }
            a++;
        }
        if(params.perluTambahanWaktu && params?.perluTambahanWaktu=="1"){
            makeDSS(jpb?.reception,params.durasi)
        }
        render "Ok"
    }
//edit end

    def makeDSS(Reception reception,String durasi){
        Date skrg = reception?.t401TglJamJanjiPenyerahan
        skrg.clearTime()
        params.aksi	= "input"
        params.checkJanjiDatang="checked"
        params.jamRencana="07:00"
        params.noWo	= reception?.t401NoWO
        params.tglRencana = (skrg+1).format("dd-MM-yyyy")
        params.statusRate = "sudah"
        params.durasi = durasi
////        if(durasi=="0")
////        durasi="1"
//        durasi="0"
        def servis = new JPBService()
        def makeDss = servis.dssAppointment(params, session.userCompanyDealer)
        String rencana = ""
        String penyerahan = ""
        Date dateAkhir = new Date()
        def jpbTemp = null
        if(makeDss?."jumData">1){
            for(int a=0;a<makeDss?."jumData";a++){
                ManPowerStall mps = ManPowerStall.get(makeDss?.stallId[a] as long)
                def slots = makeDss.slot[a]
                rencana = makeDss.tanggalDSS[a] + " " + fixTime(slots[0].substring(3))
                penyerahan = makeDss.tanggalDSS[a] + " " + fixTime(slots[slots.size()-1].substring(3))
                use(TimeCategory){
                    def jpb = new JPB(
                            companyDealer: session.userCompanyDealer,
                            reception: reception,
                            t451TglJamPlan: new Date().parse('dd/MM/yyyy HH:mm', rencana.toString()),
                            t451TglJamPlanFinished: new Date().parse('dd/MM/yyyy HH:mm', penyerahan.toString()) + 15.minutes,
                            namaManPower: mps?.namaManPower,
                            stall: mps?.stall,
                            t451StaTambahWaktu: '1',
                            t451staOkCancelReSchedule: '1',
                            t451AlasanReSchedule: '1',
                            staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                            dateCreated: datatablesUtilService?.syncTime(), lastUpdated: datatablesUtilService?.syncTime()
                    )
                    jpb?.save(flush: true)
                    jpb?.errors?.each {println "JPB "+it}
                    dateAkhir = jpb?.t451TglJamPlanFinished
                    jpbTemp = jpb
                }
            }
        }else{
            ManPowerStall mps = ManPowerStall.get(makeDss?.stallId[0] as long)
            def slots = makeDss.slot[0]
            rencana = makeDss.tanggalDSS[0] + " " + fixTime(slots[0].substring(3))
            penyerahan = makeDss.tanggalDSS[0] + " " + fixTime(slots[slots.size()-1].substring(3))
            use(TimeCategory){
                def jpb = new JPB(
                        companyDealer: session.userCompanyDealer,
                        reception: reception,
                        t451TglJamPlan: new Date().parse('dd/MM/yyyy HH:mm', rencana.toString()),
                        t451TglJamPlanFinished: new Date().parse('dd/MM/yyyy HH:mm', penyerahan.toString()) + 15.minutes,
                        namaManPower: mps?.namaManPower,
                        stall: mps?.stall,
                        t451StaTambahWaktu: '1',
                        t451staOkCancelReSchedule: '1',
                        t451AlasanReSchedule: '1',
                        staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        dateCreated: datatablesUtilService?.syncTime(), lastUpdated: datatablesUtilService?.syncTime()
                )
                jpb?.save(flush: true)
                jpb?.errors?.each {println "JPB "+it}
                dateAkhir = jpb?.t451TglJamPlanFinished
                jpbTemp = jpb
            }
        }
        def recep = reception
        recep?.addToJpbs(jpbTemp)
        recep?.t401TglJamJanjiPenyerahan = dateAkhir
        recep?.t401TglJamPenyerahanReSchedule = dateAkhir
        recep?.t401TglJamPenyerahan = dateAkhir
        recep?.save(flush: true)
        recep?.errors?.each {println "RCP : "+it}
    }

    String fixTime(String time){
        String waktu = ""
        int jum = time.trim().length()
        if(jum<=2){
            if(jum==1){
                waktu = "0"+time.trim()+":00"
            }else{
                waktu = time.trim()+":00"
            }
        }else {
            if(jum==3){
                waktu = "0"+time.charAt(0)+":"+time.substring(1)
            }else{
                waktu = time.substring(0,2)+":"+time.substring(2)
            }
        }
        return waktu
    }


    def getDataGoods(){
        def goods = null
        if(params.valueKode){
          println  goods = Goods.findByM111IDIlike("%"+params.valueKode+"%")
        }else{
            println  goods = Goods.findByM111Nama(params.valueNama)
        }

        def row = []
            row << [
                    id : goods.id,
                    kode : goods.m111ID,
                    nama : goods.m111Nama
            ]

        render row as JSON
    }
    def listKodeParts() {
        def res = [:]
        def opts = []
        def z = Goods.createCriteria()
        def results=z.list {
            or{
                ilike("m111ID","%" + params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.m111ID.trim()
        }
        res."options" = opts
        render res as JSON

    }
    def listNamaParts() {
        def res = [:]
        def opts = []
        def z = Goods.createCriteria()
        def results=z.list {
            or{
                ilike("m111Nama","%" + params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.m111Nama
        }
        res."options" = opts
        render res as JSON

    }
}