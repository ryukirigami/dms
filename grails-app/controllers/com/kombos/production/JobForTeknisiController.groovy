package com.kombos.production

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.ManPower
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.parts.MappingPartRegion
import com.kombos.reception.Reception
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class JobForTeknisiController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
    def datatablesUtilService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {

    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params
        def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
            eq("staDel","0");
            eq("companyDealer",session?.userCompanyDealer);
        }

        def mappingRegion = MappingPartRegion.createCriteria().get {
            if(mappingCompRegion?.size()>0){
                inList("region",mappingCompRegion?.region)
            }else{
                eq("id",-10000.toLong())
            }
            maxResults(1);
        }

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat jamtarget = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
            Calendar cal = Calendar.getInstance()
            cal.add(Calendar.HOUR,1)
            Date sejam = cal.getTime()

//            def waktuSekarang = df.format(new Date())
            def waktuSekarang = df.format(sejam)
            def waktu = datatablesUtilService?.syncTime()

        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")

        def user = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).fullname

        def c = JPB.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session.userCompanyDealer)

            if((session.userAuthority).contains("TEKNISI") || (session.userAuthority).contains("BONGKAR_PASANG") ||
                    (session.userAuthority).contains("LAS_KETOK") || (session.userAuthority).contains("PENDEMPULAN") ||
                    (session.userAuthority).contains("PAINT") || (session.userAuthority).contains("POLES") ||
                    (session.userAuthority).contains("MIXING") || (session.userAuthority).contains("CONTROLLER") ||
                    (session.userAuthority).contains("CUCI")){
                namaManPower{
                    eq("t015NamaLengkap", user)
                }
            }

            eq("staDel", "0")
            if (params."sCriteria_teknisi"){

                namaManPower{
                    ilike("t015NamaBoard","%"+params."sCriteria_teknisi"+"%")
                }
            }

            if (params."sCriteria_nopol"){
                reception{
                    historyCustomerVehicle{
                        ilike("fullNoPol","%"+params."sCriteria_nopol"+"%")
                    }
                }
            }

            if (params."sCriteria_stallParkir"){
                stall{
                    ilike("m022NamaStall","%"+params."sCriteria_stallParkir"+"%")
                }
            }

            if (params."sCriteria_noWo"){
                reception{
                    ilike("t401NoWO","%"+params."sCriteria_noWo"+"%")
                }
            }

            if(params?."sCriteria_tglTarget"){
                ge("t451TglJamPlan",params?."sCriteria_tglTarget")
                lt("t451TglJamPlan",params?."sCriteria_tglTarget"+1)
            }

            if(params?."sCriteria_tglSelesai"){
                ge("t451TglJamPlanFinished",params?."sCriteria_tglSelesai")
                lt("t451TglJamPlanFinished",params?."sCriteria_tglSelesai"+1)
            }


            if (params."sCriteria_namaSA"){
                reception{
                    ilike("t401NamaSA","%"+params."sCriteria_namaSA"+"%")
                }
            }

            reception{
                isNotNull("t401NoWO")
                ge("t401TglJamJanjiPenyerahan",dateAwal)
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            def nopol = it?.reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " + it?.reception?.historyCustomerVehicle?.t183NoPolTengah + " " + it?.reception?.historyCustomerVehicle?.t183NoPolBelakang
//            def selesai = it?.t451TglJamPlanFinished?.format(jamtarget.format(waktu)) ? it?.t451TglJamPlanFinished?.format(jamtarget?.format(waktu)) :""
//            def targetMulai =it?.t451TglJamPlan?.format(jamtarget.format(waktu)) ? it?.t451TglJamPlan?.format(jamtarget.format(waktu)):""
            rows << [

                    id: it.id,

                    teknisi: NamaManPower.findById(it?.namaManPower?.id)?.t015NamaBoard,

                    nopol: nopol,

                    noWo:  it?.reception?.t401NoWO,

                    jamTargetMulai: it?.t451TglJamPlan.format("dd/MM/YYYY HH:mm:ss"),

                    jamTargetSelesai: it?.t451TglJamPlanFinished.format("dd/MM/YYYY HH:mm:ss"),

                    stallParkir: it?.stall?.m022NamaStall,

                    createdBy: it?.reception?.t401NamaSA ? it?.reception?.t401NamaSA : "",

                    updatedBy: it?.updatedBy,

                    lastUpdProcess: it?.lastUpdProcess,


            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
    def jenisWo(){
        def jpb = JPB.findById(params.id)
        def rec = Reception.findByT401NoWO(jpb.reception.t401NoWO)?.customerIn?.tujuanKedatangan?.m400Tujuan

        if(rec == "GR"){
            render "GR"
        }
        else{
            render "BP"
        }
        render ""
    }
}