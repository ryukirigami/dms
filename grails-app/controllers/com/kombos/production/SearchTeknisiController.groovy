package com.kombos.production

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class SearchTeknisiController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def searchTeknisiService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", findNoPol: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def grailsApplication

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render searchTeknisiService.datatablesList(params) as JSON
    }

    def datatablesHistoryList() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render searchTeknisiService.datatablesHistoryList(params) as JSON
    }


    def findData(){
        def result = searchTeknisiService.findData(params);
        if(result!=null){
            render result as JSON
        }

    }

    def create() {
        def result = searchTeknisiService.create(params)
        if(!result.error)
            return [namaManPowerInstance : result]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }
}