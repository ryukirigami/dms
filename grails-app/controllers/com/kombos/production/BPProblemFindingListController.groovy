package com.kombos.production

import com.kombos.administrasi.UserRole
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.maintable.KebutuhanPart
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.Progress
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class BPProblemFindingListController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def BPProblemFindingListService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def exportService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm")
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render BPProblemFindingListService.datatablesList(params) as JSON
    }

    def datatablesSub1List() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render BPProblemFindingListService.datatablesSub1List(params) as JSON
    }

    def datatablesSub2List() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render BPProblemFindingListService.datatablesSub2List(params) as JSON
    }

    def datatablesSub3List() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render BPProblemFindingListService.datatablesSub3List(params) as JSON
    }

    def datatablesSub4List() {
        session.exportParams=params
        render BPProblemFindingListService.datatablesSub4List(params) as JSON
    }

    def sub1list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub2list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub3list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub4list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def save(){
        def reception = Reception.findByT401NoWO(params.noWO)
        if(reception){
            def jpb = JPB.findByReception(reception)
            def actual=Actual.findByReception(reception)
            def masalah = Masalah.findByJpbAndActual(jpb,actual)
            def respon = new Respon()
            if(jpb && actual && masalah){
                respon?.t505Respon=params.respon
                respon?.companyDealer = jpb?.companyDealer
                respon?.jpb = jpb
                respon?.actual = actual
                respon?.t505ID = Respon.count + 1
                respon?.masalah = masalah
                respon?.userRole = UserRole.findByT003NamaRole("KEPALA BENGKEL")
                respon?.userProfile = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
                respon?.t505StaSolusi = "0"
                respon?.t505TglUpdate = new Date()
                respon?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                respon?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                respon?.lastUpdProcess = "INSERT"
                respon?.dateCreated = datatablesUtilService?.syncTime()
                respon?.lastUpdated = datatablesUtilService?.syncTime()
                respon?.save(flush: true)
                respon?.save()

            }
            render "ok"
        }
    }

    def getTableData(){
        def reception = Reception.findByT401NoWO(params.noWO)
        def jpb=JPB.findByReception(reception)
        def actual = Actual.findByReceptionAndJpb(reception,jpb)
        def masalah = Masalah.findByJpbAndActual(jpb,actual)
        def c = KebutuhanPart.createCriteria()
        def result = c.list {
            if(masalah){
                eq("masalah",masalah)
            }
            if(actual){
                eq("actual",actual)
            }
            if(jpb){
                eq("jpb",jpb)
            }

        }

        def row = []
        result.each {
            row << [
                    kodeParts : it?.goods?.m111ID,
                    namaParts : it?.goods?.m111Nama,
                    qty : it?.t509Qty1
            ]
        }
        render row as JSON
    }

    def respondProblem(){
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def teknisi = null,foreman = null,jpb = null,actual = null,masalah=null,respon = null,progress = null, reception=null
        String getTeknisi="-";
        String getForeman="-";
        Long idJpb= null
        if(params.id){
            masalah = Masalah.get(Long.parseLong(params.id))
            reception = Reception.findByT401NoWO(params.noWo)
            jpb = masalah?.jpb
            actual = masalah?.actual
            progress = Progress.findByReception(jpb?.reception)
            reception = jpb?.reception
            teknisi = jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"
            if(jpb?.namaManPower?.userProfile){
                for(find in jpb?.namaManPower?.userProfile?.roles){
                    if(find.name.equalsIgnoreCase("foreman")){
                        //foreman = jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"

                       foreman= jpb?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard;
                    }
                }
            }
            if(actual?.namaManPower?.userProfile){
                for(find in actual?.namaManPower?.userProfile?.roles){
                    if(find.name.equalsIgnoreCase("foreman")){
                        foreman = jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"
                    }
                }
            }


            def jpbb = JPB.findAllByReceptionAndStaDel(reception,"0")
            if(jpbb){
                int a=0;
                getTeknisi="";
                jpb.each {
                    idJpb=it?.id
                    a++;
                    getTeknisi+=it?.namaManPower?.t015NamaBoard
                    if(a<jpbb.size()){
                        getTeknisi+=" , "
                    }
                    def grup = it?.namaManPower?.groupManPower
                    if(grup){
                        getForeman = grup?.namaManPowerForeman?.t015NamaBoard
                    }
                }
            }



        }else {
            if(params.noWo){
                reception = Reception.findByT401NoWO(params.noWo)
                if(reception){
                    progress = Progress.findByReception(reception)
                    actual = Actual.findByReception(reception)
                    jpb= JPB.findByReception(reception)
                    if(jpb && actual){
                        teknisi = jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"
                        if(jpb?.namaManPower?.userProfile){
                            for(find in jpb?.namaManPower?.userProfile?.roles){
                                if(find.name.equalsIgnoreCase("foreman")){
                                   // foreman = jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"
                                    foreman= jpb?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard;
                                }
                            }
                        }
                        if(actual?.namaManPower?.userProfile){
                            for(find in actual?.namaManPower?.userProfile?.roles){
                                if(find.name.equalsIgnoreCase("foreman")){
                                    foreman = jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"
                                }
                            }
                        }


                        def jpbb = JPB.findAllByReceptionAndStaDel(it?.reception,"0")
                        if(jpbb){
                            int a=0;
                            getTeknisi="";
                            jpb.each {
                                idJpb=it?.id
                                a++;
                                getTeknisi+=it?.namaManPower?.t015NamaBoard
                                if(a<jpbb.size()){
                                    getTeknisi+=" , "
                                }
                                def grup = it?.namaManPower?.groupManPower
                                if(grup){
                                    getForeman = grup?.namaManPowerForeman?.t015NamaBoard
                                }
                            }
                        }


                        masalah = Masalah.findByJpbAndActual(jpb,actual)
                        if(masalah){
                            respon = Respon.findByJpbAndActualAndMasalah(jpb,actual,masalah)
                        }
                    }

                }
            }
        }
        [reception : reception,teknisi : teknisi,foreman : getForeman, actual : actual,jpb : jpb, masalah : masalah, progress : progress,tanggalUpdate : df.format(new Date())]
    }
    def ubah(){
        def rec = Reception.findByT401NoWO(params.noWo)
        params.lastUpdated = datatablesUtilService?.syncTime()
        def jpb = JPB.findByReception(rec)
        def mas = Masalah.findByJpb(jpb)
        if(mas){
            def masalah = Masalah.get(mas.id)
            masalah?.t501StaSolved = '1'
            masalah?.save(flush: true)
        }
        render "Solved"
    }

    def ubah2(){
        def respon = Respon.get(Long.parseLong(params.id))
        params.lastUpdated = datatablesUtilService?.syncTime()
        if(respon){
            respon?.setT505StaSolusi("1")
            respon?.save()
        }
        render "Ya"
    }


    def report = {
        def c = Reception.createCriteria()
        def result = c.list {
            eq("staDel","0");
            eq("staSave","0");
            eq("companyDealer",session?.userCompanyDealer)
            customerIn{
                tujuanKedatangan{
                    eq("m400Tujuan","bp", [ignoreCase: true])
                }
            }
        }

        def file = BPProblemFindingListService.createReport(result)

        response.setHeader('Content-disposition', 'attachment;filename=Report.xls')
        response.setHeader('Content-length', "${file.size()}")

        OutputStream out = new BufferedOutputStream(response.outputStream)

        try {
            out.write(file.bytes)

        } finally {
            out.close()
            return false
        }
    }
}