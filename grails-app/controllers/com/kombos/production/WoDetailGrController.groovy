package com.kombos.production

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.customerprofile.FA
import com.kombos.maintable.*
import com.kombos.reception.KeluhanRcp
import com.kombos.reception.Reception
import com.kombos.woinformation.*
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class WoDetailGrController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def woDetailGrService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", findNoPol: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
        params.companyDealer=session.userCompanyDealer
    }

    def grailsApplication

    def list(Integer max) {
        DateFormat df =  new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def receptionInstance = new Reception();
        def getStall = null,getKeluhan = null,namaCustomer = null,tanggalWO=null,tanggalPenyerahan="",permintaan=null, k=""
        if(params.aksi == 'jobt' ){
            receptionInstance = Reception.findByT401NoWO(params.id)
            if(receptionInstance){
                namaCustomer = receptionInstance?.historyCustomer?.t182NamaDepan+" " + (receptionInstance.historyCustomer?.t182NamaBelakang ? receptionInstance.historyCustomer?.t182NamaBelakang : "")
                tanggalWO = df.format(receptionInstance?.t401TanggalWO)
                tanggalPenyerahan = receptionInstance?.t401TglJamJanjiPenyerahan? receptionInstance?.t401TglJamJanjiPenyerahan.format("dd/MM/yyyy HH:mm"): "-"
                //tanggalPenyerahan=""

                def jpb = JPB.findByReceptionAndStaDel(receptionInstance, "0")
                permintaan=receptionInstance?.t401PermintaanCust
                if(jpb){
                    getStall = jpb.stall.m022NamaStall
                }

            }
                def keluhan=KeluhanRcp.findByReception(receptionInstance)
                if(keluhan)
                {
                    getKeluhan=keluhan?.t411NamaKeluhan
                }

        }else if(params.aksi=="jobForteknisi"){
            def jpb = JPB.findById(params.id)
            receptionInstance = jpb.reception
            namaCustomer = receptionInstance?.historyCustomer?.t182NamaDepan+" "+receptionInstance.historyCustomer.t182NamaBelakang
            tanggalWO = receptionInstance?.t401TanggalWO? receptionInstance?.t401TanggalWO.format("dd/MM/yyyy HH:mm") : "-"
            tanggalPenyerahan = receptionInstance?.t401TglJamJanjiPenyerahan? receptionInstance?.t401TglJamJanjiPenyerahan.format("dd/MM/yyyy HH:mm") : "-"
            getStall = jpb.stall.m022NamaStall
            permintaan=receptionInstance?.t401PermintaanCust
            def preDiag = Prediagnosis.findByReception(receptionInstance)
            def keluhan=KeluhanRcp.findAllByReception(receptionInstance)
                       def a=[]
                           keluhan.each {
                               a<<[
                                       keluhan: it.t411NamaKeluhan
                               ]
                           }
           if(keluhan)
           {getKeluhan=keluhan?.t411NamaKeluhan}
            params.id = receptionInstance?.t401NoWO
//
//            [receptionInstance : receptionInstance, getStall : getStall, namaCustomer : namaCustomer,tangalWO : tanggalWO , tanggalPenyerahan : tanggalPenyerahan,noWo:params.id , keluhan : k ]

        }

        [receptionInstance : receptionInstance, getStall : getStall, keluhan : getKeluhan, namaCustomer : namaCustomer,tangalWO : tanggalWO , tanggalPenyerahan : tanggalPenyerahan,noWo:params.id ]

    }

    def datatablesList() {
        session.exportParams=params

        render woDetailGrService.datatablesList(params) as JSON
    }

    def findData(){
        params.companyDealer=session.userCompanyDealer
        def result = woDetailGrService.findData(params);
        if(result!=null){
            render result as JSON
        }

    }

    def getTableBiayaData(){
        def result = woDetailGrService.getTableBiayaData(params);
        if(result!=null){
            render result as JSON
        }
    }

    def getTableRateData(){
        def result = woDetailGrService.getTableRateData(params);
        if(result!=null){
            render result as JSON
        }
    }

    def save() {
        params.sessionId = session.id
        params.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
        def result = woDetailGrService.save(params)
        render result as JSON

    }
    def editRevisi(){
        def paramstanggal = params.tanggalBaru + " " + params.jamBaru + ":" + params.menitBaru
        def tanggal = new Date().parse("dd/MM/yyyy HH:mm",paramstanggal)
        def recId = Reception.findByT401NoWO(params.noWo).id
        def editRec = Reception.get(recId)
        editRec.t401TglJamPenyerahanReSchedule = tanggal
        editRec.t401TglJamJanjiPenyerahan = tanggal
        editRec.save(flush: true)

        def JPBEdit = JPB.get(JPB.findByReception(editRec).id)
        JPBEdit.t451StaTambahWaktu = '1'
        JPBEdit.t451TglJamPlanFinished = tanggal
        JPBEdit.t451AlasanReSchedule = params.pesan
        JPBEdit.t451staOkCancelReSchedule = '2'
        JPBEdit.save(flush: true)
        render "ok"
    }
    def revisi(){
        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_CLOSE_WO_BEROBAT_JALAN)
        def approver = ""
        if(kegiatanApproval){
            for(NamaApproval namaApproval : kegiatanApproval.namaApprovals){
                if(namaApproval.userProfile.companyDealer.id == session.userCompanyDealerId){
                    approver += namaApproval.userProfile.fullname + "\r\n"
                }
            }
        }
        def id=null, noWo = null, nopol = null, model = null, stall = null, tanggal = null
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReceptionAndStaDel(reception, "0")
        nopol = reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID  + " " + reception?.historyCustomerVehicle?.t183NoPolTengah + " " + reception?.historyCustomerVehicle?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        id = reception.id
        tanggal = reception?.t401TglJamJanjiPenyerahan
        [id:id,noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggal:tanggal,approver:approver,kegiatanApproval:KegiatanApproval.APPROVAL_CLOSE_WO_BEROBAT_JALAN]
    }
    def woJalan(){
        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_CLOSE_WO_BEROBAT_JALAN)
        def approver = ""
        if(kegiatanApproval){
            for(NamaApproval namaApproval : kegiatanApproval.namaApprovals){
                if(namaApproval.userProfile.companyDealer.id == session.userCompanyDealerId){
                    approver += namaApproval.userProfile.fullname + "\r\n"
                }
            }
        }
        def id=null, noWo = null, nopol = null, model = null, stall = null, tanggal = null
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReceptionAndStaDel(reception, "0")
        nopol = reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID  + " " + reception?.historyCustomerVehicle?.t183NoPolTengah + " " + reception?.historyCustomerVehicle?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        id = reception.id
        tanggal = datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm")
        def staObatJalan = reception.t401StaObatJalan
        if(staObatJalan=="1"){
            render staObatJalan
        }
        [id:id,noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggal:tanggal,approver:approver,kegiatanApproval:KegiatanApproval.APPROVAL_CLOSE_WO_BEROBAT_JALAN,staObatJalan:staObatJalan]
    }
    def editWoJalan(){
        def app = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_CLOSE_WO_BEROBAT_JALAN)
        if(!app){
            def kegiatanApproval = new KegiatanApproval()
            kegiatanApproval?.afterApprovalService = "woDetailGrService"
            kegiatanApproval?.createdBy= "SYSTEM"
            kegiatanApproval?.lastUpdProcess= "INSERT"
            kegiatanApproval?.dateCreated = datatablesUtilService?.syncTime()
            kegiatanApproval?.lastUpdated = datatablesUtilService?.syncTime()
            kegiatanApproval?.m770IdApproval= KegiatanApproval.last().m770IdApproval + 1
            kegiatanApproval?.m770KegiatanApproval= KegiatanApproval.APPROVAL_CLOSE_WO_BEROBAT_JALAN
            kegiatanApproval?.m770NamaDomain= "com.kombos.reception.reception"
            kegiatanApproval?.m770NamaFieldDiupdate= "T401_staObatJalan"
            kegiatanApproval?.m770NamaFk = "T401_NoWO"
            kegiatanApproval?.m770NamaFormDetail = "frmApprovalCloseWOBerobatJalan"
            kegiatanApproval?.m770NamaTabel = "T401_Reception"
            kegiatanApproval?.staDel= "0"
            kegiatanApproval?.m770StaButuhNilai = "0"
            kegiatanApproval?.save(flush: true)
        }
        def app2 = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_CLOSE_WO_BEROBAT_JALAN)
        def approval = new ApprovalT770()
        approval?.createdBy = "SYSTEM"
        approval?.companyDealer = session.userCompanyDealer
        approval?.t770NoDokumen = "Dokumen"
        approval?.kegiatanApproval = app2
        approval?.lastUpdProcess= "INSERT"
        approval?.dateCreated = datatablesUtilService?.syncTime()
        approval?.lastUpdated = datatablesUtilService?.syncTime()
        approval?.staDel = "0"
        approval?.t770CurrentLevel = 0
        approval?.t770FK = params.noWo
        approval?.t770Pesan = params.pesan
        approval?.dateCreated = datatablesUtilService?.syncTime()
        approval?.lastUpdated = datatablesUtilService?.syncTime()
        approval?.t770xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
        approval?.t770TglJamSend = datatablesUtilService?.syncTime()
        approval?.save(flush: true)
        render "ok"
    }

    def finalInspection(){
        def noWo = null, nopol = null, model = null, stall = null, status = null, nama = null, staFI = null, staRedo = null, statusKendaraan = null, clockOff =null, staRSA = null, idV = null
        def jobSugest = null, tanggalSugest = null, ket = null,fir= null,fa= null,FAdiperiksa = null, FAdiperbaiki = null, namaFA = null, staKategori = null, id = null, redoKeJob = null
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReceptionAndStaDel(reception, "0")
        def progres = Progress.findAllByReceptionAndStaDel(reception, "0").last()
        nopol = reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID  + " " + reception?.historyCustomerVehicle?.t183NoPolTengah + " " + reception?.historyCustomerVehicle?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        nama =  jpb?.namaManPower?.t015NamaLengkap
        clockOff = Actual.findByReception(reception)?.t452TglJam?.format("dd/MM/yyyy | HH:mm")
        statusKendaraan = progres?.statusKendaraan?.m999NamaStatusKendaraan
        jobSugest = JobSuggestion.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t503KetJobSuggest
        tanggalSugest = JobSuggestion.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t503TglJobSuggest
        def fi = FinalInspection.findAllByCompanyDealerAndReception(session.userCompanyDeler,reception)
        if(fi){
            staRSA = fi?.t508StaButuhRSA
            staFI =  fi?.t508StaInspection
            staRedo = fi?.t508StaRedo
            ket = fi?.t508Keterangan
            fa = fi?.fa?.jenisFAId
            FAdiperiksa = VehicleFA.findByFa(fi?.fa)?.t185StaSudahDiperiksa
            FAdiperbaiki = VehicleFA.findByFa(fi?.fa)?.t185StaSudahDikerjakan
            namaFA = fi?.fa?.id
            staKategori = fi?.t508StaKategori
            id  = fi?.id
            redoKeJob = fi?.t508RedoKeProses_M190_ID
            idV = VehicleFA.findByFa(fi?.fa)?.id
            fir = Prediagnosis.findByReception(reception)?.t406KeluhanCust
        }
        def c = JobRCP.createCriteria()
        def jobRcp = c.list {
            eq("reception",jpb?.reception)
            eq("staDel","0")

        }
        def jamMulai = datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm:ss")
        [noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggal:datatablesUtilService?.syncTime()?.format("yyyy-MM-dd"),nama:nama,staFI:staFI,staRedo:staRedo,statusKendaraan:statusKendaraan, clockOff :clockOff,staRSA :staRSA,
                ket:ket,jobSugest:jobSugest,tanggalSugest:tanggalSugest, tanggal : datatablesUtilService?.syncTime()?.format("yyyy-MM-dd"),fa:fa,idV:idV,
                FAdiperiksa:FAdiperiksa,FAdiperbaiki:FAdiperbaiki,namaFA:namaFA,staKategori:staKategori,id:id,redoKeJob:redoKeJob,
                jobRcp:jobRcp,jamMulai:jamMulai
        ]
    }

    def editFinal(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        Date date41 = dateFormat.parse(params.jamMulai)
        def receptionw = Reception.findByT401NoWO(params.noWo)
        def fi = new FinalInspection()
        fi?.reception = receptionw
        fi?.companyDealer = session.userCompanyDealer
        fi?.t508TglJamMulai = date41
        fi?.t508TglJamSelesai = datatablesUtilService?.syncTime()
        fi?.operation = receptionw?.operation
        fi?.t508StaButuhRSA = params.butuhRSA
        fi?.t508StaKategori = params.kategori
        fi?.t508Keterangan = params.ket
        fi?.t508StaInspection  = params.staFI
        fi?.t508StaRedo = params.staRedo
        fi?.userProfile = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        fi?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        fi?.dateCreated = datatablesUtilService?.syncTime()
        fi?.lastUpdated = datatablesUtilService?.syncTime()
        if(params.staRedo=='1'){
            fi?.operation = Operation.findByM053NamaOperation(params.redoKeJob)
        }

        if(params.fa=='1'){
            fi?.fa = FA.findByM185NamaFA(params.deskripsiFA)
        }
        
        if(fi?.t508StaInspection=="1"){
            def antriCuci = AntriCuci.findByReception(receptionw)
            if(!antriCuci){
                try {
                    def currentDate = new Date().format("dd/MM/yyyy")
                    def c = AntriCuci.createCriteria()
                    def res = c.list() {
                        eq("companyDealer",session.userCompanyDealer)
                        ge("t600Tanggal",df.parse(currentDate))
                        lt("t600Tanggal",df.parse(currentDate) + 1)
                    }

                    def createCuci = new AntriCuci()
                    createCuci.t600NomorAntrian = res.size() + 1
                    createCuci.companyDealer=session?.userCompanyDealer
                    createCuci.reception = receptionw
                    createCuci.createdBy = "SYSTEM"
                    createCuci.lastUpdProcess = "Insert"
                    createCuci.t600Tanggal = datatablesUtilService?.syncTime()
                    createCuci.t600StaDel = "0"
                    createCuci.dateCreated = datatablesUtilService?.syncTime()
                    createCuci.lastUpdated = datatablesUtilService?.syncTime()
                    createCuci.save(flush: true)
                    createCuci.errors.each {
                        println "cuci : " + it
                    }
                }catch (Exception e){

                }
            }
            def saveStatus = StatusKendaraan.findByM999NamaStatusKendaraanIlikeAndStaDel("%Selesai FI%", "0")
            if(!saveStatus){
                String idStaKendaraan = "";
                def terakhir = StatusKendaraan?.list()?.size()
                terakhir=terakhir+1
                idStaKendaraan = terakhir?.toString()
                if(terakhir?.toString()?.length()==1){
                    idStaKendaraan = "0"+terakhir
                }
                saveStatus = new StatusKendaraan()
                saveStatus.m999NamaStatusKendaraan= "Selesai FI"
                saveStatus.staDel= "0"
                saveStatus.m999Id= idStaKendaraan
                saveStatus.lastUpdProcess= "INSERT"
                saveStatus.createdBy= "SYSTEM"
                saveStatus.dateCreated= datatablesUtilService?.syncTime()
                saveStatus.lastUpdated= datatablesUtilService?.syncTime()
                saveStatus.save(flush: true)


            }

            def progress= new Progress(
                    reception : receptionw,
                    t999TglJamStatus : datatablesUtilService?.syncTime(),
                    statusKendaraan : saveStatus,
                    t999ID : (Progress.count+1).toString(),
                    t999XKet : "-",
                    t999XNamaUser : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    t999XDivisi : "IT",
                    staDel : "0",
                    createdBy : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess : "INSERT",
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
            )
            progress.save(flush: true)
            progress.errors.each {println it}
        }
        fi.save(flush : true)
        fi.errors.each{ println it }

        Date tglNext = df.parse(params.tglNext)
        def jobSugestion = new JobSuggestion()
        jobSugestion?.customerVehicle = receptionw?.historyCustomerVehicle?.customerVehicle
        jobSugestion?.t503ID = JobSuggestion.count + 1
        jobSugestion?.t503KetJobSuggest = params.jobSugest
        jobSugestion?.t503TglJobSuggest = tglNext
        jobSugestion?.t503staJob = "0"
        jobSugestion?.staDel = "0"
        jobSugestion?.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        jobSugestion?.lastUpdProcess   = "INSERT"
        jobSugestion?.dateCreated = datatablesUtilService?.syncTime()
        jobSugestion?.lastUpdated = datatablesUtilService?.syncTime()
        jobSugestion?.save(flush: true)

        jobSugestion.errors.each{ println it }
        render "ok"
        //cek comiit ulang
    }

    def rateTeknisi(){
        def id=null, noWo = null, nopol = null, model = null, stall = null, tanggalWo = null
        def namaJob = params.namaJob
        def teknisi = params.teknisi
        def reception = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReception(reception)
        nopol = reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID  + " " + reception?.historyCustomerVehicle?.t183NoPolTengah + " " + reception?.historyCustomerVehicle?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel

        stall = jpb?.stall?.m022NamaStall
        id = reception?.id
        tanggalWo = reception?.t401TanggalWO
        def actualRate = ActualRateTeknisi.findByReception(reception)
        String namaProses = "";
        Double findRate = null

        if(actualRate){
            findRate = actualRate?.t453ActualRate
        }else{
            findRate = FlatRate.findByOperation(reception.operation)?.t113FlatRate
        }


        [id:id,noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggal:datatablesUtilService?.syncTime()?.format("yyyy-MM-dd HH:mm:ss"),tanggalWo : tanggalWo, findRate:findRate,namaJob:namaJob,teknisi:teknisi]
    }
    def editRateTeknisi(){
        def teknisi = NamaManPower.findByT015NamaBoard(params.teknisi)
        def reception = Reception.findByT401NoWO(params.noWo)
        def operation = Operation.findByM053JobsId(params.job)
        def act = ActualRateTeknisi.findByReception(Reception.findByT401NoWO(params.noWo))
        if(act){
            def actRate = ActualRateTeknisi.get(act.id)
            actRate?.createdBy = "SYSTEM"
            actRate?.lastUpdProcess = "INSERT"
            actRate?.operation = operation
            actRate?.companyDealer = session.userCompanyDealer
            actRate?.namaManPower = teknisi
            actRate?.reception = reception
            actRate?.t453ActualRate = params.rateTeknisi
            actRate?.t453RevisiRate = params.rateTeknisi
            actRate?.t453TglRevisi = datatablesUtilService?.syncTime()
            actRate?.save(flush:true)
        }else{
            def actRate = new ActualRateTeknisi()
            actRate?.createdBy = "SYSTEM"
            actRate?.lastUpdProcess = "INSERT"
            actRate?.dateCreated = datatablesUtilService?.syncTime()
            actRate?.operation = operation
            actRate?.companyDealer = session.userCompanyDealer
            actRate?.namaManPower = teknisi
            actRate?.reception = reception
            actRate?.t453ActualRate = params.rateTeknisi
            actRate?.t453RevisiRate = params.rateTeknisi
            actRate?.t453TglRevisi = datatablesUtilService?.syncTime()
            actRate?.save(flush:true)
        }
        render "ok"
    }
    def findWoTerkait(){
        def hcv = Reception.findByT401NoWO(params.noWo)?.historyCustomerVehicle
        def rec = Reception.findAllByHistoryCustomerVehicleAndStaDelAndStaSave(hcv,'0',"0");
        def res = []
        rec.each {
            res << [
                    id : it.id,
                    noWo : it.t401NoWO
            ]

        }

        render res as JSON
    }
}