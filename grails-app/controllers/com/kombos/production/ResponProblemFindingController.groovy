package com.kombos.production

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import grails.converters.JSON

class ResponProblemFindingController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def noWO = params.noWO
        def rec = Reception.findByT401NoWO(noWO)
       // def jpb=JPB.findByReception(rec)
        def tanggalWO = rec?.t401TglJamCetakWO ?  rec?.t401TglJamCetakWO.format("dd/MM/YYYY HH:mm"): "-"
        def nopol = rec?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " + rec?.historyCustomerVehicle?.t183NoPolTengah + " " + rec?.historyCustomerVehicle?.t183NoPolBelakang
       // def waktuPenyerahan = rec?.t401TglJamPenyerahan.format("dd/MM/yyyy HH:mm") ? rec?.t401TglJamPenyerahan : "-"
        def waktuPenyerahan = rec?.t401TglJamJanjiPenyerahan ? rec?.t401TglJamJanjiPenyerahan.format("dd/MM/yyyy HH:mm") : "-"
        def jpb = JPB.findByReception(rec)
        def foreman=jpb?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard;
      //  def foreman = rec.namaManPower?.toStrin
//        def foreman = ""
        def serviceAdvisor = rec.t401NamaSA

        [noWO : noWO, tanggal :tanggalWO,nopol : nopol, waktuPenyerahan : waktuPenyerahan, foreman : foreman, serviceAdvisor : serviceAdvisor
        ]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def rec = Reception.findByT401NoWO(params.noWO)
        def listJPB = JPB.findAllByReception(rec)
        def masalahs = []

        listJPB.each {
            def masalah = Masalah.findByJpb(it)
            if(masalah)
                 masalahs.add(masalah)
        }


        def rows = []

        masalahs.each {
            def teknis=it.t501staProblemTeknis.toString()
            if(teknis.equalsIgnoreCase("1"))
            {teknis="Teknis"
            }else
            { teknis="Non Teknis"}
            def a=Actual.findByJpb(it.jpb)?.t452Ket

            rows << [

                    id: it.id,

                    jamKirimMasalah : it.t501TanggalKirim.format("dd/MM/YYYY HH:mm:ss"),

                    namaJob : it.actual?.operation?.m053NamaOperation,

                    jenisProblem : teknis,

                    statusKendaraan : it.statusKendaraan?.m999NamaStatusKendaraan ? it.statusKendaraan?.m999NamaStatusKendaraan : "-",

                    deskripsiProblem : Actual.findByJpb(it.jpb)?.t452Ket ? Actual.findByJpb(it.jpb)?.t452Ket :"-",


                    catatanForeman : it.t501CatatanTambahan

            ]

        }

        ret = [sEcho: params.sEcho, iTotalRecords:  masalahs.size(),  iTotalDisplayRecords: masalahs.size(), aaData: rows]

        render ret as JSON
    }


    def check(){
        def idProblem = params.id
        def masalah = Masalah.get(idProblem)
        def jpb = masalah.jpb
        def reception = jpb.reception
        def tujuan = reception.customerIn?.tujuanKedatangan?.m400Tujuan

        def res = [:]
        res.put("status", tujuan)

        render res as JSON
    }




}
