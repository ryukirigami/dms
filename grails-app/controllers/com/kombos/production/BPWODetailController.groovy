package com.kombos.production

import com.kombos.administrasi.NamaProsesBP
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.maintable.AntriCuci
import com.kombos.maintable.JobSuggestion
import com.kombos.reception.KeluhanRcp
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.Prediagnosis
import com.kombos.woinformation.Progress
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class BPWODetailController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def BPWODetailService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", findNoPol: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def grailsApplication

    def list(Integer max) {
        DateFormat df =  new SimpleDateFormat("dd/MM/yyyy HH:mm")
        def receptionInstance = new Reception();
        def getStall = null,getKeluhan = null,namaCustomer = null,tanggalWO=null,tanggalPenyerahan=null,permintaan=null,k=null
        if(params.idUbah){
            receptionInstance = Reception.findByT401NoWO(params.idUbah)
            if(receptionInstance){
                namaCustomer = receptionInstance?.historyCustomer?.t182NamaDepan+" "+receptionInstance.historyCustomer.t182NamaBelakang
                tanggalWO = df.format(receptionInstance?.t401TanggalWO)
                tanggalPenyerahan = df.format(receptionInstance?.t401TglJamJanjiPenyerahan)
                def jpb = JPB.findByReception(receptionInstance)
                if(jpb){
                    getStall = jpb.stall.m022NamaStall
                }
                def preDiag = Prediagnosis.findByReception(receptionInstance)
                if(preDiag){
                    getKeluhan = preDiag?.t406KeluhanCust
                }
            }
        }

        else if(params.aksi=="jobForteknisi"){
            def jpb = JPB.findById(params.id)
            receptionInstance = jpb.reception
            namaCustomer = receptionInstance?.historyCustomer?.t182NamaDepan+" "+receptionInstance.historyCustomer.t182NamaBelakang
            tanggalWO = receptionInstance?.t401TanggalWO? receptionInstance?.t401TanggalWO.format("dd/MM/yyyy HH:mm") : "-"
            tanggalPenyerahan = receptionInstance?.t401TglJamJanjiPenyerahan? receptionInstance?.t401TglJamJanjiPenyerahan.format("dd/MM/yyyy HH:mm") : "-"
            getStall = jpb.stall.m022NamaStall
            permintaan=receptionInstance?.t401PermintaanCust
            def preDiag = Prediagnosis.findByReception(receptionInstance)
            def keluhan=KeluhanRcp.findAllByReception(receptionInstance)
//                       def a=[]
//                           keluhan.each {
//                               a<<[
//                                       keluhan: it.t411NamaKeluhan
//                               ]
//                           }
            if(keluhan)
            {getKeluhan=keluhan?.t411NamaKeluhan}
            params.id = receptionInstance?.t401NoWO
//
//            [receptionInstance : receptionInstance, getStall : getStall, namaCustomer : namaCustomer,tangalWO : tanggalWO , tanggalPenyerahan : tanggalPenyerahan,noWo:params.id , keluhan : k ]

        }


        [receptionInstance : receptionInstance, getStall : getStall,getKeluhan : getKeluhan, namaCustomer : namaCustomer,tangalWO : tanggalWO , tanggalPenyerahan : tanggalPenyerahan]
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render BPWODetailService.datatablesList(params) as JSON
    }

    def findData(){
        def result = BPWODetailService.findData(params);
        if(result!=null){
            render result as JSON
        }

    }

    def getSelectData(){
        String result = ""
        def receptionInstance = Reception.findByT401NoWO(params.noWO)
        def c=Reception.createCriteria()
        def results = c.list() {
            historyCustomerVehicle{
                eq("fullNoPol",receptionInstance?.historyCustomerVehicle?.fullNoPol)
            }
            eq("staDel","0");
            eq("staSave","0");
            eq("companyDealer",session?.userCompanyDealer)
        }


        def rows = []

        results.each {
            result+='<option value="'+it.t401NoWO+'">'+it.t401NoWO+'</option>'
        }

        render result
    }

    def getTableBiayaData(){
        params.companyDealer = session?.userCompanyDealer
        def result = BPWODetailService.getTableBiayaData(params);
        if(result!=null){
            render result as JSON
        }
    }

    def getTableRateData(){
        def result = BPWODetailService.getTableRateData(params);
        if(result!=null){
            render result as JSON
        }
    }

    def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = BPWODetailService.save(params)
        render result

    }

    def finalInspection(){
        def noWo = null, nopol = null, model = null, stall = null, statusKendaraan = null,id=null
        def nama
        def reception = Reception.findByT401NoWO(params.noWo)
        if(reception){
            nama =  reception?.namaManPower?.t015NamaLengkap+" ("+reception?.namaManPower?.manPowerDetail?.m015Inisial+")" ?
                    reception?.namaManPower?.t015NamaLengkap+" ("+reception?.namaManPower?.manPowerDetail?.m015Inisial+")":"-"
            stall = reception?.stall?.m022NamaStall
            nopol = reception?.historyCustomerVehicle?.fullNoPol
            def fi = FinalInspection.findByReception(reception)
            if(fi){
                model = fi?.reception?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName
                id=fi.id
            }
            def progres = Progress.findByReception(reception)
            if(progres){
                statusKendaraan = progres?.statusKendaraan?.m999NamaStatusKendaraan
            }
        }

        def job = JobRCP.createCriteria().list {
            eq("reception",reception)
            eq("staDel",'0')
        }
        def namaProses = NamaProsesBP.createCriteria().list {
        }
        [noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggal:datatablesUtilService?.syncTime()?.format("dd-MM-yyyy HH:mm"),
                nama:nama,statusKendaraan:statusKendaraan , id : id, job:job,namaProses:namaProses
        ]
    }

    def editFinal(){
        def fi = FinalInspection.get(params.id)

        String tanggalParams=params.tanggalUpdate
        def strDate = new Date().parse("dd-MM-yyyy HH:mm",tanggalParams)
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy")
        def reception = Reception.findByT401NoWO(params.noWo)
        if(fi){
            def jsuges = JobSuggestion.findByCustomerVehicle(fi.reception.historyCustomerVehicle.customerVehicle)
            fi?.t508Keterangan = params.hasilFinal
            fi?.t508StaInspection  = params.lolos
            if(params.lolos=="0"){
                fi?.operation = params.redoJob
                fi?.t508RedoKeProses_M190_ID = params.redoProses
            }
            fi?.t508TglJamMulai = strDate
            fi?.t508TglJamSelesai = datatablesUtilService?.syncTime()
            fi?.companyDealer = session.userCompanyDealer
            fi?.save()
            if(jsuges){
                jsuges?.t503KetJobSuggest =  params.jobSuggest
                jsuges?.t503TglJobSuggest = params.tanggalNext
                jsuges?.save()
            }

        }else{
            fi = new FinalInspection()
            fi?.t508Keterangan = params.hasilFinal
            fi?.reception = reception
            fi?.t508StaInspection  = params.lolos
            if(params.lolos=="0"){
                fi?.operation = params.redoJob
                fi?.t508RedoKeProses_M190_ID = params.redoProses
            }
            fi?.t508TglJamMulai = strDate
            fi?.t508TglJamSelesai = datatablesUtilService?.syncTime()
            fi?.companyDealer = session.userCompanyDealer
            fi?.save()

        }
        def antriCuci = AntriCuci.findByReception(fi.reception)
        if(!antriCuci){
            def currentDate = new Date().format("dd/MM/YYYY")
            def c = AntriCuci.createCriteria()
            def res = c.list() {
                eq("companyDealer",session.userCompanyDealer)
                ge("t600Tanggal",dateFormat.parse(currentDate))
                lt("t600Tanggal",dateFormat.parse(currentDate) + 1)
            }

            def createCuci = new AntriCuci()
            createCuci.t600NomorAntrian = res.size() + 1
            createCuci.companyDealer=session?.userCompanyDealer
            createCuci.reception = fi.reception
            createCuci.createdBy = "SYSTEM"
            createCuci.lastUpdProcess = "Insert"
            createCuci.t600Tanggal = datatablesUtilService?.syncTime()
            createCuci.t600StaDel = "0"
            createCuci.dateCreated = datatablesUtilService?.syncTime()
            createCuci.lastUpdated = datatablesUtilService?.syncTime()
            createCuci.save(flush: true)
            createCuci.errors.each {
                println "cuci : " + it
            }
        }
        render "ok"
    }

}