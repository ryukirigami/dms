package com.kombos.production

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Reception
import com.kombos.woinformation.ActualRateTeknisi
import com.kombos.woinformation.JobRCP

class BPRevisiRateTeknisiController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def noWo = null, nopol = null, model = null,tanggalWO=null,job=null, nama=null, stall = null,proses = null,rate = null , id = null
        def jobRcp = JobRCP.get(params.id)
        if(jobRcp){
            id=params.id
            noWo = jobRcp?.reception?.t401NoWO
            nopol = jobRcp?.reception?.historyCustomerVehicle?.fullNoPol
            model = jobRcp?.reception?.historyCustomerVehicle?.fullModelCode?.modelName?.m104NamaModelName
            stall = jobRcp?.reception?.stall
            proses = jobRcp?.reception?.stall?.namaProsesBP?.m190NamaProsesBP
            tanggalWO=jobRcp?.reception?.t401TglJamCetakWO
            job = jobRcp?.operation
            nama= jobRcp?.reception?.namaManPower
            def actualRate = ActualRateTeknisi.findByReception(jobRcp?.reception)
            if(actualRate){
                rate = actualRate?.t453ActualRate
                if(actualRate?.t453RevisiRate){
                    rate = actualRate?.t453RevisiRate
                }
            }
        }


        [id:params.id,noWo : noWo,nopol:nopol,tanggalWO:tanggalWO, model:model, proses : proses, stall:stall,
                tanggalRevisi:new Date().format("dd-MM-yyyy HH:mm:ss"),rate : rate, job : job,nama : nama, id :id]
    }
    def save(){
        def jobRcp = JobRCP.get(params.id)
        String tanggalParams=params.tanggalRevisi
        def strDate = new Date().parse("dd-MM-yyyy HH:mm:ss",tanggalParams)
        if(jobRcp){
            def recep = Reception.get(jobRcp.reception.id)
            if(recep){
                def actualRate = ActualRateTeknisi.findByReceptionAndNamaManPowerAndOperation(recep,recep.namaManPower,jobRcp.operation)
                if(actualRate){
                    actualRate?.t453RevisiRate = Double.parseDouble(params.rateTeknisi)
                    actualRate?.t453TglRevisi = strDate
                    actualRate?.save()
                }
                recep.stall = params.stall
                recep?.namaManPower = params.namaTeknisi
                recep?.save()
            }
            jobRcp?.operation = params.job
            jobRcp?.save()

        }
        render "ok"
    }
}