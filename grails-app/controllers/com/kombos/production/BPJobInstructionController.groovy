package com.kombos.production

import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.Stall
import com.kombos.administrasi.VendorCat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.Role
import com.kombos.board.JPB
import com.kombos.maintable.KebutuhanPart
import com.kombos.maintable.KirimAlertKepada
import com.kombos.maintable.StatusKendaraan
import com.kombos.parts.Goods
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class BPJobInstructionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def BPJobInstructionService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def ambilWO = "semua"
        [idTable: new Date().format("yyyyMMddhhmmss"),ambilWO: ambilWO]
    }

    def datatablesList() {
        session.exportParams=params
        params?.companyDealer = session?.userCompanyDealer
        render BPJobInstructionService.datatablesList(params) as JSON
    }

    def datatablesSub1List() {
        render BPJobInstructionService.datatablesSub1List(params) as JSON
    }

    def datatablesSub2List() {
        render BPJobInstructionService.datatablesSub2List(params) as JSON
    }

    def datatablesSub3List() {
        render BPJobInstructionService.datatablesSub3List(params) as JSON
    }

    def datatablesSub4List() {
        render BPJobInstructionService.datatablesSub4List(params) as JSON
    }

    def sub1list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub2list() {
        [t401NoWO: params.t401NoWO, idJob : params.idJob, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub3list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub4list() {
        [idManPower: params.idManPower, t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def updateWO(){
        def stall=null,model=null
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def reception = Reception.findByT401NoWOAndStaDel(params.noWo,"0")
        def noRecep = reception?.t401TglJamCetakWO ? reception?.t401TglJamCetakWO : "-"
        def jpb=JPB.findByReception(reception)
        stall=jpb.stall.m022NamaStall
        model=reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        [reception : reception , tanggalWO : noRecep, tanggalUpdate : df.format(new Date()), stall:stall,model: model]
    }

    def addTeknisi(){
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def getJob = ""
        def reception = Reception.findByT401NoWOAndStaDelAndCompanyDealer(params.noWo,"0", session.userCompanyDealer)
        def jobRcp = JobRCP.findAllByReceptionAndStaDel(reception,"0")
        def jpb = new JPB()
        if(params.id){
            jpb = JPB.get(params.id.toLong())
        }
        if(jobRcp){
            int a=0;
            jobRcp.each {
                a++;
                getJob += it?.operation?.m053NamaOperation
                if(a<jobRcp.size()){
                    getJob+=" , "
                }
            }
        }

        String tanggalWO = reception?.t401TglJamCetakWO ? df.format(reception?.t401TglJamCetakWO) : ""

        [reception : reception , tanggalWO : tanggalWO,job : getJob,jpb:jpb, tanggalTambah : df.format(new Date()),proses : params?.proses]
    }

    def sendProblemFinding(){
        def model=null,nopol=null
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def getJob=""
        def reception = Reception.findByT401NoWOAndStaDelAndCompanyDealer(params.noWo , "0", session.userCompanyDealer)
        model=reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        def jpb=JPB.findByReception(reception)
        if(jpb){
            nopol = jpb?.reception?.historyCustomerVehicle?.fullNoPol
        }


        def actual = Actual.get(Long.parseLong(params.id))
        def jobRcp = JobRCP.findAllByReceptionAndStaDel(reception,"0")
        if(jobRcp){
            int a=0;
            jobRcp.each {
                a++;
                getJob += it?.operation?.m053NamaOperation
                if(a<jobRcp.size()){
                    getJob+=" , "
                }
            }
        }
        [reception : reception,tanggalSend : df.format(new Date()),actual : actual,job : getJob , model:model, nopol:nopol]
    }

    def saveUpdate(){
        String tanggalParams=params.tanggalUpdate
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def strDate = new Date().parse("dd/MM/yyyy HH:mm",tanggalParams)
        def reception = Reception.findByT401NoWOAndStaDel(params.noWO,"0")
        if(reception){
            reception?.setT401StaAmbilWO(params.updateStaWO)
            reception?.setT401TglJamAmbilWO(strDate)
            reception?.save()
            render "OK"
        }
    };

    def saveTeknisi(){
        String tanggalParams=params.tanggalTambah
        def strDate = new Date().parse("dd/MM/yyyy HH:mm",tanggalParams)
        def reception = Reception.findByT401NoWOAndStaDelAndCompanyDealer(params.noWO,"0", session.userCompanyDealer)
        def jpb = null
        if(params.idJpb){
            jpb = JPB.get(Long.parseLong(params.idJpb))
        }
        if(reception && jpb){
            def add = new JPB()
            add?.companyDealer = jpb?.companyDealer
//            add?.t451IDJPB = (JPB.last().t451IDJPB.toInteger()+1).toString()
            add?.alasanRefund = jpb?.alasanRefund
            add?.reception = reception
            add?.t451TglJamPlan = strDate
            add?.namaManPower = NamaManPower.get(params.teknisi)
            add?.stall = Stall.get(params.stall)
            add?.flatRate = jpb?.flatRate
            add?.t451StaTambahWaktu = jpb?.t451StaTambahWaktu
            add?.t451staOkCancelReSchedule = jpb?.t451staOkCancelReSchedule
            add?.t451AlasanReSchedule = jpb?.t451AlasanReSchedule
            add?.t451TglJamPlanFinished=jpb?.reception?.t401TglJamJanjiPenyerahan
            add?.staDel = "0"
            add?.dateCreated = datatablesUtilService?.syncTime()
            add?.lastUpdated = datatablesUtilService?.syncTime()
            add?.createdBy = "SYSTEM"
            add?.updatedBy  = "SYSTEM"
            add?.lastUpdProcess  = "INSERT"
            add?.save(flush: true)
            add?.errors?.each {println "ADD_TEKNISI.ERRORS"+it}
            render "OK"
        }else{
            render "gagal"
        }
    };

    def getDataGoods(){
        def c=Goods.createCriteria()
        def result = c.list {
            eq("staDel","0")
            eq("id",Long.parseLong(params.value))

        }

        def row = []
        result.each {
            row << [
                    kode : it.m111ID,

                    nama : it.m111Nama
            ]
        }

        render row as JSON
    }

    def saveData(){
        String tanggalParams=params.tanggalSend
        println tanggalParams
        def strDate = new Date().parse("dd/MM/yyyy HH:mm",tanggalParams)
//        def jsonArray = JSON.parse(params.idRole)
        def jsonArrayGoods = JSON.parse(params.idGoods)
        def jsonArrayQtys = JSON.parse(params.qtys)
        def reception = Reception.findByT401NoWOAndStaDel(params.noWO,"0")
        def jpb=JPB.findByReceptionAndStaDel(reception,"0")
        def actual = Actual.findByJpbAndStaDel(jpb,"0")
//        def oList = []
//        jsonArray.each { oList << Role.get(it)  }
        def masalah = new Masalah()
        masalah?.companyDealer = jpb?.companyDealer
        masalah?.jpb = jpb
        masalah?.actual = actual
        masalah?.t501ID = Masalah?.last()?.t501ID ? Masalah.last().t501ID+1 : 1
        masalah?.t501staProblemTeknis = params.staProblem
        masalah?.t501CatatanTambahan = params.catatanTambahan as String
//        masalah?.t501TglUpdate = new Date()
        masalah?.t501TglUpdate = datatablesUtilService?.syncTime()
        masalah?.t501TanggalKirim = strDate
        masalah?.t501NamaKirim = "-"
        masalah?.t501StaButuhPause = params.staPauseJob
        masalah?.t501StaButuhRSA="-"
        masalah?.t501Sta_TL_ATL_NTL="-"
        masalah?.t501StaBisaKrjSdri= "-"
        masalah?.t501StaBthTambahWkt = params.staTambahWaktu
        masalah?.t501DurasiTambahWaktu = params.durasiTambah
        masalah?.t501StaBthJob = params.staTambahJob
        masalah?.t501KetBthJob = params.namaJobTambah
        masalah?.t501StaBthParts = params.staButuhParts
        masalah?.t501HasilDSS = "-"
        masalah?.t501StaSolved = "0"
        masalah?.t501TglSolved = new Date()
        masalah?.t501StaButuhCat = params.staButuhCat
        masalah?.t501StaTagihKepada = params.tagihKe
        masalah?.vendorCat = VendorCat.get(params.vendor)
        masalah?.statusKendaraan = StatusKendaraan.get(params.staKendaraan)
        masalah?.createdBy = "SYSTEM"
        masalah?.updatedBy  = "SYSTEM"
        masalah?.lastUpdProcess  = "INSERT"
        masalah?.save()

        int a=0;
        jsonArrayGoods.each {
            def kebutuhanPart = new KebutuhanPart()
            kebutuhanPart?.companyDealer = jpb?.companyDealer
            kebutuhanPart?.jpb = jpb
            kebutuhanPart?.actual = actual
            kebutuhanPart?.masalah = masalah
            kebutuhanPart?.goods = Goods.findByM111ID(it)
            kebutuhanPart?.t509Qty1 = new Double(jsonArrayQtys[a])
            kebutuhanPart?.t509Qty2=  new Double(jsonArrayQtys[a])
            kebutuhanPart?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            kebutuhanPart?.updatedBy  =org.apache.shiro.SecurityUtils.subject.principal.toString()
            kebutuhanPart?.lastUpdProcess  = "INSERT"
            kebutuhanPart?.dateCreated = datatablesUtilService?.syncTime()
            kebutuhanPart?.save()
            a++;
        }

        def dataAlert = ''
        if(params.alert1 == '1'){
            def alert = new KirimAlertKepada()
            alert?.companyDealer = session.userCompanyDealer
            alert?.jPB = jpb
            alert?.actual = actual
            alert?.masalah = masalah
            alert?.userRole = Role.findById('SERVICE_ADVISOR_GENERAL_REPAIR')
            alert?.createdBy  = org.apache.shiro.SecurityUtils.subject.principal.toString()
            alert?.updatedBy  = org.apache.shiro.SecurityUtils.subject.principal.toString()
            alert?.lastUpdProcess = 'INSERT'
            alert?.dateCreated = datatablesUtilService?.syncTime()
            alert?.lastUpdated = datatablesUtilService?.syncTime()
            alert?.save(flush:true)
            alert.errors.each {
                println "alert : " + it
            }
        }
        if(params.alert2 == '1'){
            def alert2 = new KirimAlertKepada()
            alert2?.companyDealer = session.userCompanyDealer
            alert2?.jPB = jpb
            alert2?.actual = actual
            alert2?.masalah = masalah
            alert2?.userRole = Role.findById('KEPALA_BENGKEL')
            alert2?.createdBy  = org.apache.shiro.SecurityUtils.subject.principal.toString()
            alert2?.lastUpdProcess = 'INSERT'
            alert2?.dateCreated = datatablesUtilService?.syncTime()
            alert2?.save(flush:true)
            alert2.errors.each {
                println "alert2 : " + it
            }
        }

//        oList.each {
//            def kirimAlert = new KirimAlertKepada()
//            kirimAlert?.companyDealer = jpb.companyDealer
//            kirimAlert?.jPB = jpb
//            kirimAlert?.actual = actual
//            kirimAlert?.masalah = masalah
//            kirimAlert?.userRole = it
//            kirimAlert?.createdBy = "SYSTEM"
//            kirimAlert?.updatedBy  = "SYSTEM"
//            kirimAlert?.lastUpdProcess  = "INSERT"
//            kirimAlert?.save()
//        }
        render "OK"
    }
}
