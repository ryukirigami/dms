package com.kombos.production

import com.kombos.administrasi.NamaManPower
import com.kombos.administrasi.UserRole
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.maintable.KebutuhanPart
import com.kombos.maintable.StatusActual
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import grails.converters.JSON
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.write.*
import org.hibernate.criterion.CriteriaSpecification

import java.text.DateFormat
import java.text.SimpleDateFormat

class ProblemFindingGrController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss") ]
    }
    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [noWo: params.noWo, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsublist() {
        [noWo: params.noWo, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsubsublist() {
        [idMasalah: params.id,noWo: params.noWo, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def c = Masalah.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            if (params."sCriteria_t017Tanggal" && params."sCriteria_t017Tanggal2") {
                jpb{
                    reception{
                        ge("t401TanggalWO", params."sCriteria_t017Tanggal")
                        lt("t401TanggalWO", params."sCriteria_t017Tanggal2" + 1)
                    }
                }
            }

            if(params."status") {
                ilike("t501StaSolved","%" + (params."status" as String) + "%")
            }
            jpb{
                reception{
                    customerIn{
                        tujuanKedatangan{
                            eq("m400Tujuan","GR")
                        }
                    }
                }
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("jpb", "jpb")
            }

        }
        println "list: " + results
        def rows = []

        results.each {
            println it.jpb.reception.id
            def nopol  = Reception.findById(it.jpb.reception.id)?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " + Reception.findById(it.jpb.reception.id)?.historyCustomerVehicle?.t183NoPolTengah + " " + Reception.findById(it.jpb.reception.id)?.historyCustomerVehicle.t183NoPolBelakang
            rows << [
                    id : it.id,
                    noWo: it.jpb.reception.t401NoWO,
                    tglWo  : it.jpb.reception.t401TanggalWO.format("dd/MM/yyyy HH:mm"),
                    nopol  : nopol,
                    start :"-",
                    stop  :"-",
//                    start  : Actual.findAllByReceptionAndStatusActual(Reception.findById(it.jpb.reception.id),StatusActual.findById(1))?.first().t452TglJam?.format("dd/MM/yyyy HH:mm"),
//                    stop  : Actual.findAllByReceptionAndStatusActual(Reception.findById(it.jpb.reception.id),StatusActual.findById(4))?.last().t452TglJam?.format("dd/MM/yyyy HH:mm"),
                    finalInspection  : FinalInspection.findByReception(Reception.findById(it.jpb.reception.id))?.t508TglJamSelesai?FinalInspection.findByReception(Reception.findById(it.jpb.reception.id))?.t508TglJamSelesai.format("dd/MM/yyyy HH:mm"):'-',
                    SA  : it.jpb.reception?.t401NamaSA,
                    janjiPenyerahan :it.jpb.reception?.t401TglJamJanjiPenyerahan?.format("dd/MM/yyyy HH:mm")
//                    it.t172TglJamReturn?.format(dateFormat),

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON

    }
    def datatablesSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def ret
        def x = 0
        def c = Masalah.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            jpb{
                eq("reception",Reception.findByT401NoWO(params.noWo))
            }

            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                groupProperty("jpb", "jpb")
            }
        }
        def c2 = Masalah.createCriteria()
        def jum = c2.list{
            jpb{
                eq("reception",Reception.findByT401NoWO(params.noWo))
            }

        }
        println "list: " + results
        def rows = []

        results.each {

            rows << [
                    operation: Masalah.findByJpb(it?.jpb)?.actual?.operation?.m053NamaOperation,

                    t501TglUpdate : Masalah.findByJpb(it?.jpb)?.t501TanggalKirim?.format("dd/MM/yyyy HH:mm"),

                    lastRespon : Respon.findByJpb(it?.jpb)?.t505TglUpdate?.format("dd/MM/yyyy HH:mm"),

                    jumlah : jum.size(),

                    foreman: it?.jpb?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard,

                    teknisi : it.jpb?.namaManPower?.t015NamaBoard,

                    staSolved :  Masalah.findByJpb(it?.jpb)?.t501StaSolved=="1"?"Solved":"Not Solved",

                    noWo : params.noWo,

            ]
        }

        ret =  [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render  ret as JSON
    }

    def datatablesSubSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def ret
        def c = Masalah.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            jpb{
                eq("reception",Reception.findByT401NoWO(params.noWo))
            }

        }
        println "list: " + results
        def rows = []

        results.each {

            rows << [

                    id: it.id,

                    rolePengirim: 'Foreman',

                    pengirim: it?.jpb?.namaManPower?.groupManPower.namaManPowerForeman?.t015NamaBoard,

                    tanggalKirim : it?.t501TanggalKirim?.format("dd/MM/yyyy HH:mm"),

                    deskripsiMasalah : it.actual?.t452Ket,

                    tanggalSolved :it?.t501TglSolved?.format("dd/MM/yyyy HH:mm"),

                    staSolved :  it?.t501StaSolved=="1"?"Solved":"Not Solved",

                    noWo : params.noWo,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }
    def datatablesSubSubSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        def ret
        def c = Respon.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("masalah",Masalah.findById(params.idMasalah))
        }
        println "list: " + results
        def rows = []

        results.each {

            rows << [
                    roleRespon: it?.userRole?.t003NamaRole,
                    responder: it?.userProfile?.username,
                    tanggalRespon: it?.t505TglUpdate.format("dd/MM/yyyy HH:mm"),

                    deskripsiRespon :it?.t505Respon,

                    solusi :  it?.t505StaSolusi=="1"?"Ya":"Tidak",

                    noWo : params.noWo,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
        render ret as JSON
    }
    def responProblem() {
        def nopol = null,model = null, stall = null, teknisi = null, foreman = null, job = null
        def deskripsi = null, tanggalProblem = null, jenisProblem = null, statusKendaraan = null, catatanTambahan = null
        def butuhRSA = null, bisaDikerjakanSendiri = null, perluTambahanWaktu = null, butuhTambahanJob = null
        def perluTambahanMaterial = null, partDibutuhkan = null, tanggalUpdate = null, respon = null
        def masalah = null
        println "Masalah id : " + params.id
        masalah = Masalah.findById(params.id)

        def reception = masalah?.jpb?.reception
        def jpb = masalah?.jpb
        nopol = reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID  + " " + reception?.historyCustomerVehicle?.t183NoPolTengah + " " + reception?.historyCustomerVehicle?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        teknisi = jpb?.namaManPower?.t015NamaLengkap
        foreman = jpb?.namaManPower?.groupManPower?.namaManPowerForeman?.t015NamaBoard
        job = masalah.actual?.operation?.m053NamaOperation
        deskripsi = Actual.findByJpbAndOperationAndStatusActual(jpb,masalah.actual?.operation,StatusActual.findByM452Id("2"))?.t452Ket
        tanggalProblem = masalah?.t501TanggalKirim
        jenisProblem = masalah?.t501staProblemTeknis=="1"?"Teknis":"Non Teknis"
        statusKendaraan = masalah?.statusKendaraan?.m999NamaStatusKendaraan
        catatanTambahan = masalah?.t501CatatanTambahan
        butuhRSA = masalah?.t501StaButuhRSA == "1"?"Ya":"Tidak"
        def dt = ""
        if(masalah?.t501Sta_TL_ATL_NTL=="1"){
            dt = "TL"
        }else if(masalah?.t501Sta_TL_ATL_NTL=="0"){
            dt = "ATL"
        }else{
            dt = "NTL"
        }
        butuhRSA += " ("+ dt +")"
        bisaDikerjakanSendiri = masalah?.t501StaBisaKrjSdri == "1"?"Ya":"Tidak"
        perluTambahanWaktu = masalah?.t501StaBthTambahWkt == "1"?"Ya":"Tidak"
        perluTambahanWaktu += " ("+ masalah?.t501DurasiTambahWaktu +")"
        butuhTambahanJob = masalah?.t501StaBthJob == "1"?"Ya":"Tidak"
        butuhTambahanJob += " ("+ masalah?.t501KetBthJob +")"
        perluTambahanMaterial = masalah?.t501StaBthParts == "1"?"Ya":"Tidak"
        partDibutuhkan = masalah?.t501HasilDSS
        tanggalUpdate = new Date().format("yyyy-MM-dd HH:mm")
        println "perluTambahanMaterial : " + masalah?.t501StaBthParts
        respon = Respon.findByJpb(JPB.findByReception(reception))?.t505Respon
        [noWo: reception?.t401NoWO, nopol : nopol, model : model, stall :stall, teknisi: teknisi, foreman: foreman, job : job,
            deskripsi:deskripsi,tanggalProblem:tanggalProblem, jenisProblem:jenisProblem, statusKendaraan : statusKendaraan, catatanTambahan : catatanTambahan,
            butuhRSA : butuhRSA, bisaDikerjakanSendiri : bisaDikerjakanSendiri, perluTambahanWaktu : perluTambahanWaktu,
            butuhTambahanJob : butuhTambahanJob, perluTambahanMaterial : perluTambahanMaterial, butuhTambahanJob : butuhTambahanJob,
            partDibutuhkan  : partDibutuhkan, tanggalUpdate : tanggalUpdate,respon : respon,idMasalah : params.id
        ]
    }
    def getTableKebutuhanPart(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def rows2 = []
        def ret

        def e = KebutuhanPart.createCriteria()
        def resultsRate = e.list() {
           masalah{
                eq("id",params.idMasalah as Long)
           }
        }
        resultsRate.each {
            rows2 << [
                    id : it.id,
                    kodeParts : it?.goods?.m111ID,

                    namaParts : it?.goods?.m111Nama,

                    qty : it?.t509Qty1,

            ]
        }

        render rows2 as JSON
    }
    def respon(){
        def rec = Reception.findByT401NoWO(params.noWo)
        def mas = Masalah.findById(params.idMasalah as Long)
        def jpb = JPB.findByReception(rec)

            def respon = new Respon()
            respon?.companyDealer = session.userCompanyDealer
            respon?.jpb = jpb
            respon?.actual = mas.actual
            respon?.masalah = mas
            respon?.userRole = UserRole.findByT003NamaRole("KEPALA BENGKEL")
            respon?.userProfile = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
            respon?.t505ID = Respon.count + 1
            respon?.t505Respon = params.responProblem
            respon?.t505StaSolusi = '0'
//            respon?.t505TglUpdate = new Date()
            respon?.t505TglUpdate = datatablesUtilService?.syncTime()
            respon?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            respon?.lastUpdProcess = 'INSERT'
            respon?.dateCreated = datatablesUtilService?.syncTime()
            respon?.lastUpdated = datatablesUtilService?.syncTime()
            respon?.save(flush: true)
            respon.errors.each {
                println it
            }

        render "ok"
    }
    def ubah(){
        def rec = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReception(rec)
        def mas = Masalah.findByJpb(jpb)
        if(mas){
            def masalah = Masalah.get(mas.id)
            masalah?.t501StaSolved = '1'
            masalah?.t501TglSolved = datatablesUtilService?.syncTime()
            masalah?.dateCreated = datatablesUtilService?.syncTime()
            masalah?.lastUpdated = datatablesUtilService?.syncTime()
            masalah?.save(flush: true)
        }
        render "Solved"
    }

    def ubah2(){
        def rec = Reception.findByT401NoWO(params.noWo)
        def jpb = JPB.findByReception(rec)
        def res = Respon.findByJpb(jpb)
        if(res){
            def respon = Respon.get(res.id)
            respon?.t505StaSolusi = '1'
            respon?.lastUpdated = datatablesUtilService?.syncTime()
            respon?.dateCreated = datatablesUtilService?.syncTime()
            respon?.save(flush: true)
        }
        render "Ya"
    }


    private File createReport(def list) {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        WorkbookSettings workbookSettings = new WorkbookSettings()
        workbookSettings.locale = Locale.default

        def file = File.createTempFile('myExcelDocument', '.xls')
        file.deleteOnExit()

        WritableWorkbook workbook = Workbook.createWorkbook(file, workbookSettings)

        WritableFont fontTitle = new WritableFont(WritableFont.ARIAL, 12)
        fontTitle.setBoldStyle(WritableFont.BOLD)
        WritableCellFormat format = new WritableCellFormat(fontTitle)

        WritableFont font = new WritableFont(WritableFont.ARIAL, 10)
        WritableCellFormat formatIsi = new WritableCellFormat(font)

        def row = 0
        WritableSheet sheet = workbook.createSheet('MySheet', 0)

        sheet.addCell(new Label(0, row, "No", format))
        sheet.addCell(new Label(1, row, "Nomor Wo", format))
        sheet.addCell(new Label(2, row, "Nomor Polisi", format))
        sheet.addCell(new Label(3, row, "Tanggal WO", format))
        sheet.addCell(new Label(4, row, "Model Kendaraan", format))
        sheet.addCell(new Label(5, row, "Nama SA", format))
        sheet.addCell(new Label(6, row, "Waktu Penyerahan", format))
        sheet.addCell(new Label(7, row, "Nama Job", format))
        sheet.addCell(new Label(8, row, "Nama Foreman", format))
        sheet.addCell(new Label(9, row, "Nama Teknisi", format))
        sheet.addCell(new Label(10, row, "Proses", format))
        sheet.addCell(new Label(11, row, "Nama Pengirim", format))
        sheet.addCell(new Label(12, row, "Tanggal Kirim", format))
        sheet.addCell(new Label(13, row, "Deskripsi Masalah", format))
        sheet.addCell(new Label(14, row, "Jenis Masalah", format))
        sheet.addCell(new Label(15, row, "Catatan Tambahan", format))
        sheet.addCell(new Label(16, row, "Status Butuh RSA", format))
        sheet.addCell(new Label(17, row, "Bisa Dikerjakan Sendiri", format))
        sheet.addCell(new Label(18, row, "Perlu Tambahan Waktu", format))
        sheet.addCell(new Label(19, row, "Butuh Tambahan Job", format))
        sheet.addCell(new Label(20, row, "Butuh Parts / Material", format))
        sheet.addCell(new Label(21, row, "Butuh Pause Job", format))
        sheet.addCell(new Label(22, row, "Butuh Cat Ulang", format))
        sheet.addCell(new Label(23, row, "Tagih Kepada", format))
        sheet.addCell(new Label(24, row, "Vendor Cat", format))
        sheet.addCell(new Label(25, row, "Status Kendaraan", format))
        sheet.addCell(new Label(26, row, "Role Respon", format))
        sheet.addCell(new Label(27, row, "Responder", format))
        sheet.addCell(new Label(28, row, "Tanggal Respon", format))
        sheet.addCell(new Label(29, row, "Deskripsi Respon", format))
        sheet.addCell(new Label(30, row, "Status Solusi", format))
        def nopol = ""
        list.each {
            nopol  = it.jpb.reception.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " + it.jpb.reception.historyCustomerVehicle?.t183NoPolTengah + " " + it.jpb.reception.historyCustomerVehicle.t183NoPolBelakang
            def butuhRSA = it.t501StaButuhRSA == "1"?"Ya":"Tidak"
            def actual = Actual.findByJpbAndStaDel(it?.jpb,"0")
            def respon = Respon.findByJpbAndActualAndMasalah(it?.jpb,actual,it)
            def dt = ""
            if(it.t501Sta_TL_ATL_NTL=="1"){
                dt = "TL"
            }else if(it.t501Sta_TL_ATL_NTL=="0"){
                dt = "ATL"
            }else{
                dt = "NTL"
            }
            butuhRSA += " ("+ dt +")"
            def kerjaSendiri = it.t501StaBisaKrjSdri == "1"?"Ya":"Tidak"
            kerjaSendiri += " ("+ it.t501DurasiTambahWaktu +")"

            def perluTambahanWaktu = it?.t501StaBthTambahWkt == "1"?"Ya":"Tidak"
            perluTambahanWaktu += " ("+ it?.t501DurasiTambahWaktu +")"

            def butuhTambahanJob = it?.t501StaBthJob == "1"?"Ya":"Tidak"
            butuhTambahanJob += " ("+ it?.t501KetBthJob +")"

            def foreman = "",getRoles="",getResponder=""
            if(it?.jpb?.namaManPower?.userProfile){
                for(find in it?.jpb?.namaManPower?.userProfile?.roles){
                    if(find.name.equalsIgnoreCase("foreman")){
                        foreman=it?.jpb?.namaManPower?.t015NamaLengkap+" ("+it?.jpb?.namaManPower?.manPowerDetail?.m015Inisial+")"
                    }
                }
            }
            if(respon?.userProfile){
                def nama=NamaManPower.findByUserProfileAndStaDel(respon?.userProfile,"0")
                if(nama){
                    getResponder = nama?.t015NamaBoard
                }
                for(find in respon?.userProfile?.roles){
                    getRoles+=find.name+" "
                }
            }

            row++
            sheet.addCell(new Label(0, row, ""+row))
            sheet.addCell(new Label(1, row, ""+it.jpb.reception.t401NoWO, formatIsi))
            sheet.addCell(new Label(2, row, ""+nopol, formatIsi))
            sheet.addCell(new Label(3, row, ""+it.jpb.reception.t401TanggalWO?it.jpb.reception.t401TanggalWO.format(dateFormat):"", formatIsi))
            sheet.addCell(new Label(4, row, ""+it.jpb.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel, formatIsi))
            sheet.addCell(new Label(5, row, ""+it.jpb.reception?.t401NamaSA, formatIsi))
            sheet.addCell(new Label(6, row, ""+it.jpb.reception?.t401TglJamPenyerahan?it.jpb.reception.t401TglJamPenyerahan.format(dateFormat):"", formatIsi))
            sheet.addCell(new Label(7, row, ""+it.jpb.reception?.operation?.m053NamaOperation , formatIsi))
            sheet.addCell(new Label(8, row, ""+it.jpb.reception?.namaManPower?.manPowerDetail?.manPower?.m014Inisial , formatIsi))
            sheet.addCell(new Label(9, row, ""+it.jpb.reception?.namaManPower?.manPowerDetail?.manPower?.m014Inisial , formatIsi))
            sheet.addCell(new Label(10, row, ""+ it.jpb?.stall?.namaProsesBP?.m190NamaProsesBP , formatIsi))
            sheet.addCell(new Label(11, row, ""+ it?.t501NamaKirim, formatIsi))
            sheet.addCell(new Label(12, row, ""+ it.t501TanggalKirim? it.t501TanggalKirim.format(dateFormat):"", formatIsi))
            sheet.addCell(new Label(13, row, ""+ it.actual?.t452Ket, formatIsi))
            sheet.addCell(new Label(14, row, ""+ it?.t501staProblemTeknis, formatIsi))
            sheet.addCell(new Label(15, row, ""+  it?.t501CatatanTambahan, formatIsi))
            sheet.addCell(new Label(16, row, ""+ butuhRSA, formatIsi))
            sheet.addCell(new Label(17, row, ""+ kerjaSendiri, formatIsi))
            sheet.addCell(new Label(18, row, ""+  perluTambahanWaktu, formatIsi))
            sheet.addCell(new Label(19, row, ""+ butuhTambahanJob, formatIsi))
            sheet.addCell(new Label(20, row, ""+ it?.t501StaBthParts == "1"?"Ya":"Tidak", formatIsi))
            sheet.addCell(new Label(21, row, ""+ it?.t501StaButuhPause=="1" ? "Ya" : "Tidak", formatIsi))
            sheet.addCell(new Label(22, row, ""+ it?.t501StaButuhCat=="1" ? "Ya" : "Tidak", formatIsi))
            sheet.addCell(new Label(23, row, ""+ it?.t501StaTagihKepada, formatIsi))
            sheet.addCell(new Label(24, row, ""+ it?.vendorCat?.m191Nama, formatIsi))
            sheet.addCell(new Label(25, row, ""+ it?.statusKendaraan?.m999NamaStatusKendaraan, formatIsi))
            sheet.addCell(new Label(26, row, ""+ getRoles, formatIsi))
            sheet.addCell(new Label(27, row, ""+ getResponder, formatIsi))
            sheet.addCell(new Label(28, row, ""+ respon?.t505TglUpdate, formatIsi))
            sheet.addCell(new Label(29, row, ""+ respon?.t505Respon, formatIsi))
            sheet.addCell(new Label(30, row, ""+ respon?.t505StaSolusi=="1" ? "Ya" : "Tidak", formatIsi))
        }

        workbook.write()
        workbook.close()
        return file
    }
    def report = {
        def c = Masalah.createCriteria()
        def result = c.list {
            jpb{
                reception{
                    customerIn{
                        tujuanKedatangan{
                            eq("m400Tujuan","GR", [ignoreCase: true])
                        }
                    }
                }
            }
        }

        def file  = createReport(result)
        response.setHeader('Content-length', "${file.size()}")

        response.setHeader('Content-disposition', 'attachment;filename=Report.xls')

        OutputStream out = new BufferedOutputStream(response.outputStream)

        try {
            out.write(file.bytes)

        } finally {
            out.close()
            return false
        }
    }

    def databaleparts(){

        println ("cetak id "+params.idMasalah)

        def parts=KebutuhanPart.createCriteria()
        def   result=parts.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
                  masalah{
                      eq("id",params.idMasalah as Long)
                  }
        }
        def row=[]
        println("cetak jumlah part "+result.size() )
        result.each {
             row<<[
                     kodeParts: it?.goods?.m111ID,
                     namaParts:it?.goods?.m111Nama,
                     qty:it.t509Qty1
             ]
         }

      def ret= [sEcho: params.sEcho, iTotalRecords: result.totalCount, iTotalDisplayRecords: result.totalCount, aaData: row]

        render ret as JSON
    }
}