package com.kombos.production

import com.kombos.administrasi.Operation
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import com.kombos.board.JPB
import com.kombos.customerprofile.FA
import com.kombos.maintable.AntriCuci
import com.kombos.maintable.JobSuggestion
import com.kombos.maintable.StatusActual
import com.kombos.maintable.StatusKendaraan
import com.kombos.maintable.VehicleFA
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import com.kombos.woinformation.JobRCP
import com.kombos.woinformation.Prediagnosis
import com.kombos.woinformation.Progress
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class BPQualityControlController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def BPQualityControlService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub1list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub2list() {
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sub3list() {
        def reception = Reception.findByT401NoWOAndStaDelAndStaSave(params.t401NoWO, "0", "0")
        def quality = null
        def inspect = null
        if(reception){
            quality = InspectionQC.findByReception(reception)
            inspect = IDR.findByReception(reception)
        }
        println "============ "+quality?.id+"\n\n\n"+"============= "+inspect?.id
        [t401NoWO: params.t401NoWO, idTable: new Date().format("yyyyMMddhhmmss") , quality : quality, inspect : inspect]
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session.userCompanyDealer
        render BPQualityControlService.datatablesList(params) as JSON
    }


    def datatablesSub1List() {
        session.exportParams=params
        params.companyDealer = session.userCompanyDealer
        render BPQualityControlService.datatablesSub1List(params) as JSON
    }

    def datatablesSub2List() {
        session.exportParams=params
        params.companyDealer = session.userCompanyDealer
        render BPQualityControlService.datatablesSub2List(params) as JSON
    }

    def datatablesSub3QualityList() {
        session.exportParams=params
        params.companyDealer = session.userCompanyDealer
        render BPQualityControlService.datatablesSub3QualityList(params) as JSON
    }

    def datatablesSub3InspectionList() {
        session.exportParams=params
        params.companyDealer = session.userCompanyDealer
        render BPQualityControlService.datatablesSub3InspectionList(params) as JSON
    }

    def qualityControl(){
        def proses=null,teknisi=null,namaJob=null,st=null
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        def reception = Reception.findByT401NoWO(params.noWo)
        def model     =reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        def jpb=JPB?.findByReception(reception)

        def c = JobRCP.createCriteria()
        def jobRcp = c.list {
            eq("reception",jpb?.reception)
            eq("staDel","0")
            eq("companyDealer",session.userCompanyDealer)
        }
        if(jpb)
        {
            proses = jpb?.stall?.namaProsesBP?.m190NamaProsesBP
            teknisi=jpb?.namaManPower?.t015NamaLengkap
            st = jpb?.stall?.m022NamaStall
            println("cetak Stall "+st)
            def jobs = JobRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",jpb?.reception)
            }
            int xxx = 0
            namaJob = ""
            jobs.each {
                xxx++
                namaJob+=it?.operation?.m053NamaOperation
                if(xxx < jobs?.size()){
                    namaJob+", "
                }
            }
        }
        println("cetak Stall "+st)
        [reception : reception,tanggalUpdate : df.format(new Date()) , model:model,proses:proses,teknisi:teknisi,namaJob : namaJob,jobRcp:jobRcp ]
    }

    def inspectionDuringRepair(){

        def proses=null,teknisi=null,namaJob=null,stall=null
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        def reception = Reception.findByT401NoWO(params.noWo)
        def model     =reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel

        def jpb=JPB?.findByReception(reception)
        if(jpb)
        {
            proses = jpb?.stall?.namaProsesBP?.m190NamaProsesBP
            teknisi=jpb?.namaManPower?.t015NamaLengkap+" ("+jpb?.namaManPower?.t015NamaBoard+")"
            stall = jpb?.stall?.m022NamaStall

            def jobs = JobRCP.createCriteria().list {
                eq("staDel","0")
                eq("reception",jpb?.reception)
            }
            int xxx = 0
            namaJob = ""
            jobs.each {
                xxx++
                namaJob+=it?.operation?.m053NamaOperation
                if(xxx < jobs?.size()){
                    namaJob+", "
                }
            }
        }

        [reception : reception,tanggalUpdate : df.format(new Date()),model:model,stall:stall,teknisi: teknisi]
    }

    def saveInspection(){
        def reception = Reception.findByT401NoWO(params.noWO)
        String tanggalParams=params.tanggalUpdate
        def strDate = new Date().parse("dd/MM/yyyy HH:mm:ss",tanggalParams)
        def jpb = JPB.findByReception(reception)
        if(reception){
            def idr=new IDR()
            idr.reception = reception
            idr.stall = jpb?.stall
//            idr?.t506TglJamMulai = strDate
//            idr?.t506TglJamSelesai = new Date()

            idr?.t506TglJamMulai = datatablesUtilService?.syncTime()
            idr?.t506TglJamSelesai = datatablesUtilService?.syncTime()

            idr.t506StaIDR = params.staIdr
            idr.t506StaRedo=params.staRedo
            if(params.staRedo=="1"){
                idr.t506M053_JobID_Redo = Operation.get(Long.parseLong(params.redoJob))
                idr.t506RedoKeProses_M190_ID = Operation.get(Long.parseLong(params.redoProses))
            }
            idr.t506AlasanIDR = params.alasanStaIdr
            idr.createdBy = "SYSTEM"
            idr.updatedBy = "SYSTEM"
            idr.lastUpdProcess = "INSERT"
            idr?.dateCreated = datatablesUtilService?.syncTime()
            idr?.lastUpdated = datatablesUtilService?.syncTime()
            idr?.save()
            idr.errors.each {
                println("cetak idr "+it)
            }
            render "OK"
        }
    }

    def saveQC(){
        def reception = Reception.findByT401NoWO(params.noWO)
        String tanggalParams=params.tanggalUpdate
        def strDate = new Date().parse("dd/MM/yyyy HH:mm:ss",tanggalParams)
        def jpb = JPB.findByReception(reception)
        if(reception){
            def inspectQC = new InspectionQC()
            inspectQC?.reception = reception
            inspectQC?.stall = jpb?.stall
//            inspectQC?.t507TglJamMulai = strDate
//            inspectQC?.t507TglJamSelesai = new Date()
            inspectQC?.t507TglJamMulai = datatablesUtilService?.syncTime()
            inspectQC?.t507TglJamSelesai = datatablesUtilService?.syncTime()
            inspectQC?.t507StaLolosQC = params.staLolosQC
            if(params.redoJob && params.redoProses){
                inspectQC?.t507RedoKeProses_M190_ID = Operation.get(Long.parseLong(params.redoJob))
                inspectQC?.t507M053_JobID_Redo = Operation.get(Long.parseLong(params.redoProses))
            }

            inspectQC?.t507Catatan = params.catatan
//            inspectQC?.t507TglUpdate = strDate
            inspectQC?.t507TglUpdate = datatablesUtilService?.syncTime()
            inspectQC?.createdBy = "SYSTEM"
            inspectQC?.updatedBy = "SYSTEM"
            inspectQC?.lastUpdProcess = "INSERT"
            inspectQC?.dateCreated = datatablesUtilService?.syncTime()
            inspectQC?.lastUpdated = datatablesUtilService?.syncTime()
            inspectQC?.save()
            render "OK"
            inspectQC.errors.each {
                println("qc error"+it)
            }
        }
    }

    def finalInspection(){
        def noWo = null, nopol = null, model = null, stall = null, status = null, nama = null, staFI = null, staRedo = null, statusKendaraan = null, clockOff =null, staRSA = null, idV = null
        def jobSugest = null, tanggalSugest = null, ket = null,fir= null,fa= null,FAdiperiksa = null, FAdiperbaiki = null, namaFA = null, staKategori = null, id = null, redoKeJob = null

        def reception = Reception.findByT401NoWOAndStaDel(params.noWo,"0")
        def jpb = JPB.findByReceptionAndStaDel(reception, "0")
        def progres = Progress.findAllByReceptionAndStaDel(reception, "0").last()
        nopol = reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID  + " " + reception?.historyCustomerVehicle?.t183NoPolTengah + " " + reception?.historyCustomerVehicle?.t183NoPolBelakang
        model = reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel
        stall = jpb?.stall?.m022NamaStall
        nama =  jpb?.namaManPower?.t015NamaLengkap
        clockOff = Actual.findByReception(reception)?.t452TglJam?.format("dd/MM/yyyy | HH:mm")
        statusKendaraan = progres?.statusKendaraan?.m999NamaStatusKendaraan
        jobSugest = JobSuggestion.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t503KetJobSuggest
        tanggalSugest = JobSuggestion.findByCustomerVehicle(reception?.historyCustomerVehicle?.customerVehicle)?.t503TglJobSuggest
        def fi = FinalInspection.findAllByCompanyDealerAndReception(session.userCompanyDeler,reception)

        if(fi){
            staRSA = fi?.t508StaButuhRSA
            staFI =  fi?.t508StaInspection
            staRedo = fi?.t508StaRedo
            ket = fi?.t508Keterangan
            fa = fi?.fa?.jenisFAId
            FAdiperiksa = VehicleFA.findByFa(fi?.fa)?.t185StaSudahDiperiksa
            FAdiperbaiki = VehicleFA.findByFa(fi?.fa)?.t185StaSudahDikerjakan
            namaFA = fi?.fa?.id
            staKategori = fi?.t508StaKategori
            id  = fi?.id
            redoKeJob = fi?.t508RedoKeProses_M190_ID
            idV = VehicleFA.findByFa(fi?.fa)?.id
            fir = Prediagnosis.findByReception(reception)?.t406KeluhanCust
        }

        def c = JobRCP.createCriteria()
        def jobRcp = c.list {
            eq("reception", reception)
            eq("staDel", "0")
        }

//        println("cetak Model "+mdl)
        def jamMulai = datatablesUtilService?.syncTime()?.format("dd/MM/yyyy HH:mm:ss")
        [noWo : params.noWo,nopol:nopol, model:model, stall:stall, tanggal:datatablesUtilService?.syncTime()?.format("yyyy-MM-dd"),nama:nama,staFI:staFI,staRedo:staRedo,statusKendaraan:statusKendaraan, clockOff :clockOff,staRSA :staRSA,
         ket:ket,jobSugest:jobSugest,tanggalSugest:tanggalSugest, tanggal : datatablesUtilService?.syncTime()?.format("yyyy-MM-dd"),fa:fa,idV:idV,
         FAdiperiksa:FAdiperiksa,FAdiperbaiki:FAdiperbaiki,namaFA:namaFA,staKategori:staKategori,id:id,redoKeJob:redoKeJob,
         jobRcp:jobRcp,jamMulai:jamMulai
        ]
    }

    def editFinal(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        Date startDate = dateFormat.parse(params.jamMulai)
        def reception=Reception.findByT401NoWOAndStaDel(params.nomorWO,"0")

//        Date tglNext = df.parse(params.tanggalNext)

        println("Rec : "+reception)
        println("Waktu Mulai : "+startDate)
        println("Keterangan : "+params.ket)
        println("Lolos : "+params.staFI)
        println("Redo Job : "+Operation.findByM053NamaOperation(params.redoKeJob))
        println("Waktu Next : "+params.tanggalNext)
        println(params.id +", "+params.nomorPolisi+", "+params.jobSuggest+", "+params.tanggalUpdate)

        def fi = new FinalInspection()

        fi?.reception = reception
        fi?.companyDealer = session.userCompanyDealer
        fi?.t508TglJamMulai = startDate
        fi?.t508TglJamSelesai = datatablesUtilService?.syncTime()
        fi?.t508Keterangan = params.ket
        fi?.t508StaInspection = params.staFI
        fi?.t508StaRedo = params.staFI
        if (params.staFI == '0') {
            fi?.t508StaRedo = '1'
            fi?.operation = Operation.findByM053NamaOperation(params.redoKeJob)
        }else{
            fi?.t508StaRedo = '0'
        }
        fi?.userProfile = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        fi?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        fi?.dateCreated = datatablesUtilService?.syncTime()
        fi?.lastUpdated = datatablesUtilService?.syncTime()

        if(fi?.t508StaInspection=="1"){
            def antriCuci = AntriCuci.findByReception(reception)
            if(!antriCuci){
                try {
                    def currentDate = new Date().format("dd/MM/yyyy")
                    def c = AntriCuci.createCriteria()
                    def res = c.list() {
                        eq("companyDealer",session.userCompanyDealer)
                        ge("t600Tanggal",df.parse(currentDate))
                        lt("t600Tanggal",df.parse(currentDate) + 1)
                    }

                    def createCuci = new AntriCuci()
                    createCuci.t600NomorAntrian = res.size() + 1
                    createCuci.companyDealer=session?.userCompanyDealer
                    createCuci.reception = reception
                    createCuci.createdBy = "SYSTEM"
                    createCuci.lastUpdProcess = "Insert"
                    createCuci.t600Tanggal = datatablesUtilService?.syncTime()
                    createCuci.t600StaDel = "0"
                    createCuci.dateCreated = datatablesUtilService?.syncTime()
                    createCuci.lastUpdated = datatablesUtilService?.syncTime()
                    createCuci.save(flush: true)
                    createCuci.errors.each {
                        println "cuci : " + it
                    }
                }catch (Exception e){

                }
            }
            def saveStatus = StatusKendaraan.findByM999NamaStatusKendaraanIlikeAndStaDel("%Selesai FI%", "0")
            if(!saveStatus){
                String idStaKendaraan = "";
                def terakhir = StatusKendaraan?.list()?.size()
                terakhir=terakhir+1
                idStaKendaraan = terakhir?.toString()
                if(terakhir?.toString()?.length()==1){
                    idStaKendaraan = "0"+terakhir
                }
                saveStatus = new StatusKendaraan()
                saveStatus.m999NamaStatusKendaraan= "Selesai FI"
                saveStatus.staDel= "0"
                saveStatus.m999Id= idStaKendaraan
                saveStatus.lastUpdProcess= "INSERT"
                saveStatus.createdBy= "SYSTEM"
                saveStatus.dateCreated= datatablesUtilService?.syncTime()
                saveStatus.lastUpdated= datatablesUtilService?.syncTime()
                saveStatus.save(flush: true)
            }

            def progress= new Progress(
                    reception : reception,
                    t999TglJamStatus : datatablesUtilService?.syncTime(),
                    statusKendaraan : saveStatus,
                    t999ID : (Progress.count+1).toString(),
                    t999XKet : "-",
                    t999XNamaUser : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    t999XDivisi : "IT",
                    staDel : "0",
                    createdBy : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    lastUpdProcess : "INSERT",
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
            )
            progress.save(flush: true)
            progress.errors.each {println it}
        }

        fi.save(flush: true)
        fi.errors.each { println it }

        Date tglNext = df.parse(params.tanggalNext)
        def jobSugestion = new JobSuggestion()
        jobSugestion?.customerVehicle = reception?.historyCustomerVehicle?.customerVehicle
        jobSugestion?.t503ID = JobSuggestion.count + 1
        jobSugestion?.t503KetJobSuggest = params.jobSuggest
        jobSugestion?.t503TglJobSuggest = tglNext
        jobSugestion?.t503staJob = "0"
        jobSugestion?.staDel = "0"
        jobSugestion?.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
        jobSugestion?.lastUpdProcess   = "INSERT"
        jobSugestion?.dateCreated = datatablesUtilService?.syncTime()
        jobSugestion?.lastUpdated = datatablesUtilService?.syncTime()
        jobSugestion?.save(flush: true)

        jobSugestion.errors.each{ println it }
        render "ok"
    }
}
