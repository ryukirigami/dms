package com.kombos.production

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class ForemanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def foremanService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", findNoPol: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def grailsApplication

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams=params

        render foremanService.datatablesList(params,session) as JSON
    }

}