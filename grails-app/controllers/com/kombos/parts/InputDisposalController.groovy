package com.kombos.parts

import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.NamaApproval
import com.kombos.customerprofile.FA
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
import org.activiti.engine.runtime.ProcessInstance
import org.hibernate.criterion.CriteriaSpecification

import java.text.SimpleDateFormat

class InputDisposalController {

    static activiti = true
    def generateCodeService
    // XXX activiti
    def taskService
    def activitiService
    def datatablesUtilService

    def index() {
        String staUbah = "create"
        if(params.id){
            staUbah = "ubah"
        } else {
        }
        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.PARTS_DISPOSAL)
        def approver = ""
        for(NamaApproval namaApproval : kegiatanApproval.namaApprovals){
            if(namaApproval.userProfile.companyDealer.id == session.userCompanyDealerId){
                approver += namaApproval.userProfile.fullname + "\r\n"
            }
        }
        [kegiatanApproval: kegiatanApproval, approver: approver,idUbah:params.id,aksi:staUbah,noDokumen:generateNomorDokumen().value]
    }
    def getTableData(){
        def hasil = PartsDisposalDetail.createCriteria().list() {
            partsDisposal{
                eq("t147ID",params.id)
            }
        }
        def result = []
        hasil.each {
            result << [

                    id: it.id,

                    kode: it.goods.m111ID,

                    nama: it.goods.m111Nama,

                    qty: it.t148Jumlah1,

                    satuan : it.goods.satuan?.m118Satuan1,

                    icc: KlasifikasiGoods.findByGoods(it.goods)?.parameterICC?.m155NamaICC,

                    lokasi: KlasifikasiGoods.findByGoods(it.goods)?.location?.m120NamaLocation

            ]

        }
        render result as JSON
    }
    def searchAndAddParts() {

    }

    def searchPartsDatatablesList() {
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }

        def ret

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_kode") {
                ilike("m111ID", "%" + (params."sCriteria_kode" as String) + "%")
            }

            if (params."sCriteria_nama") {
                ilike("m111Nama", "%" + (params."sCriteria_nama" as String) + "%")
            }
            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
        }

        def rows = []

        results.each {
            KlasifikasiGoods klasifikasiGoods = KlasifikasiGoods.findByGoods(it)
            rows << [
                    id: it.id,
                    kode: it?.m111ID,
                    nama: it?.m111Nama,
                    qty : 0,
                    satuan: it?.satuan?.m118Satuan1,
                    icc: klasifikasiGoods?.parameterICC?.m155NamaICC,
                    lokasi: klasifikasiGoods?.location?.m120NamaLocation
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }


    def tambahParts() {
        def fa = FA.read(params.fa)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << it }
        session.selectedParts = oList
        render "ok"
    }

    def partsDatatablesList() {
        def results = session?.selectedParts ? session?.selectedParts : new ArrayList<>()
        def rows = []
        def partsDisposal = null
        if (session?.selectedPartsDisposal) {
            partsDisposal = session?.selectedPartsDisposal
        }

        results.each {
            def goods = Goods.read(it?.toString())
            KlasifikasiGoods klasifikasiGoods = KlasifikasiGoods.findByGoods(goods)
            PartsDisposalDetail partsDisposalDetail = partsDisposal == null ? null : PartsDisposalDetail.findByGoodsAndPartsDisposal(goods, partsDisposal)
            rows << [
                    id: goods.id,
                    pilih: partsDisposalDetail != null,
                    kode: goods?.m111ID,
                    nama: goods?.m111Nama,
                    qty: partsDisposalDetail == null ? 0 : partsDisposalDetail?.t148Jumlah1,
                    satuan: goods?.satuan?.m118Satuan1,
                    icc: klasifikasiGoods?.parameterICC?.m155NamaICC,
                    lokasi: klasifikasiGoods?.location?.m120NamaLocation
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

        render ret as JSON
    }


    def doSave() {
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Goods.get(it?.toString()) }

        PartsDisposal partsDisposal
        if (session?.selectedPartsDisposal) {
            partsDisposal = session?.selectedPartsDisposal
            partsDisposal = PartsDisposal.get(partsDisposal.id)
            PartsDisposal.executeUpdate("delete PartsDisposalDetail where goods not in :goods", [goods: oList])
        } else {
            partsDisposal = new PartsDisposal()
            partsDisposal.t147ID = generateCodeService?.codeGenerateSequence("T147_ID",session.userCompanyDealer)
            partsDisposal.dateCreated = datatablesUtilService?.syncTime()
            partsDisposal.companyDealer = session?.userCompanyDealer
        }
        partsDisposal.lastUpdated = datatablesUtilService?.syncTime()
        partsDisposal.t147TglDisp = new SimpleDateFormat("dd-MM-yyyy").parse(params.t147TglDisp)
        partsDisposal.save(flush: true)

        oList.each {
            def cekPDDetail = PartsDisposalDetail.findByGoodsAndPartsDisposal(it, partsDisposal)
            PartsDisposalDetail partsDisposalDetail = PartsDisposalDetail.findByGoodsAndPartsDisposal(it, partsDisposal) ?: new PartsDisposalDetail()
            if(!cekPDDetail){
                partsDisposalDetail.dateCreated = datatablesUtilService?.syncTime()
            }
            partsDisposalDetail.lastUpdated = datatablesUtilService?.syncTime()
            partsDisposalDetail.goods = it
            partsDisposalDetail.partsDisposal = partsDisposal
            partsDisposalDetail.t148Jumlah1 = request?.getParameter("qty_" + it.id)?.toString()?.toDouble()
            partsDisposalDetail.t148Jumlah2 = request?.getParameter("qty_" + it.id)?.toString()?.toDouble()
            partsDisposalDetail.save(flush: true)
        }


        session?.selectedPartsDisposal = partsDisposal

        render "ok"
    }


    def doSend() {
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Goods.get(it?.toString()) }

        PartsDisposal partsDisposal
        if (session?.selectedPartsDisposal) {
            partsDisposal = session?.selectedPartsDisposal
            partsDisposal = PartsDisposal.get(partsDisposal.id)
            PartsDisposal.executeUpdate("delete PartsDisposalDetail where goods not in :goods", [goods: oList])
        } else {
            partsDisposal = new PartsDisposal()
            partsDisposal.t147ID = generateCodeService?.codeGenerateSequence("T147_ID",session.userCompanyDealer)
            partsDisposal.dateCreated = datatablesUtilService?.syncTime()
            partsDisposal?.companyDealer = session.userCompanyDealer
        }
        partsDisposal?.lastUpdated = datatablesUtilService?.syncTime()
        partsDisposal?.t147TglDisp = new SimpleDateFormat("dd-MM-yyyy").parse(params.t147TglDisp)
        partsDisposal.save(flush: true)
        partsDisposal.errors.each{
               println it
        }

        oList.each {
            def cekPDDetail = PartsDisposalDetail.findByGoodsAndPartsDisposal(it, partsDisposal)
            PartsDisposalDetail partsDisposalDetail = PartsDisposalDetail.findByGoodsAndPartsDisposal(it, partsDisposal) ?: new PartsDisposalDetail()
            if(!cekPDDetail){
                partsDisposalDetail.dateCreated = datatablesUtilService?.syncTime()
            }
            partsDisposalDetail.lastUpdated = datatablesUtilService?.syncTime()
            partsDisposalDetail.goods = it
            partsDisposalDetail.partsDisposal = partsDisposal
            partsDisposalDetail.t148Jumlah1 = request?.getParameter("qty_" + it.id)?.toString()?.toDouble()
            partsDisposalDetail.t148Jumlah2 = request?.getParameter("qty_" + it.id)?.toString()?.toDouble()
            partsDisposalDetail.save(flush: true)
        }


        session?.selectedPartsDisposal = partsDisposal


        def approval = new ApprovalT770(
                t770FK:partsDisposal.id as String,
                companyDealer: session?.userCompanyDealer,
                kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.PARTS_DISPOSAL),
                t770NoDokumen: params.dokumen,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.Pesan,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush:true)
        approval.errors.each{ println it }

        session?.selectedPartsDisposal = null
        render "ok"
    }

    def generateNomorDokumen(){
        def result = [:]
        def c = ApprovalT770.createCriteria()
        def results = c.get () {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                max("id","id")
            }
        }

        def m = results.id?:0

        result.value = String.format('%014d',m+1)
        return result
    }
}
