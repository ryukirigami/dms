package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.GeneralParameter
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GoodsHargaJualController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def dataTablesGoods() {
		String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]

		def x = 0


		session.exportParams = params


		def c = Goods.createCriteria()

		//mendapatkan value dari field cari
		def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

			if(params.sCriteria_koGo){
				ilike("m111ID", "%"+params.sCriteria_koGo+"%")
			}

			if(params.sCriteria_naGo){
				ilike("m111Nama", "%"+params.sCriteria_naGo+"%")
			}

			if(params.sCriteria_satGo){
				satuan{
					ilike("m118Satuan1", "%"+params.sCriteria_satGo+"%")
				}
			}

			if(params.sCriteria_klsGo){
				ilike("m111Nama", "%"+params.sCriteria_klsGo+"%")
			}

			eq("staDel","0")

		}

		def rows = []



		results.each {
			def klasifikasi = KlasifikasiGoods.findByGoods(it)?.franc?.m117NamaFranc
			rows << [

					id: it.id,

					kodeGoods : it.m111ID,

					namaGoods : it.m111Nama,

					satuan: it.satuan?.m118Satuan1,

					klasifikasi: klasifikasi
			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		render ret as JSON
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = GoodsHargaJual.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
//			companyDealer{
//                eq("id",session.userCompanyDealer.id)
//            }

			if(params."sCriteria_kategori"){
                goods{
                    ilike("m111ID", "%" + (params."sCriteria_kategori" as String) + "%")
                }
			}

			if(params."sCriteria_t151TMT"){
				ge("t151TMT",params."sCriteria_t151TMT")
				lt("t151TMT",params."sCriteria_t151TMT" + 1)
			}

			if(params."sCriteria_t151HargaTanpaPPN"){
				eq("t151HargaTanpaPPN",params."sCriteria_t151HargaTanpaPPN" as Double)
			}

			if(params."sCriteria_t151HargaDenganPPN"){
				eq("t151HargaDenganPPN",params."sCriteria_t151HargaDenganPPN" as Double)
			}

			if(params."sCriteria_satuan"){
                satuan{
                    ilike("m118Satuan1","%"+(params."sCriteria_satuan")+"%")
                }
			}

			if(params."sCriteria_goods"){
                goods{
                    ilike("m111Nama", "%" + (params."sCriteria_goods" as String) + "%")
                }
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
                case "goods1":
                    goods{
                        order("m111ID",sortDir)
                    }
                    break;
                case "goods2":
                    goods{
                        order("m111Nama",sortDir)
                    }
                    break;
                case "satuan":
                    satuan{
                        order("m118Satuan1",sortDir)
                    }
                    break;
                default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						companyDealer: it.companyDealer,
			
						kode: it.goods.m111ID,
			
						t151TMT: it.t151TMT?it.t151TMT?.format(dateFormat):"",
			
						t151HargaTanpaPPN: it.t151HargaTanpaPPN,
			
						t151HargaDenganPPN: it.t151HargaDenganPPN,
			
						satuan: it.satuan.m118Satuan1,
			
						goods: it.goods.m111Nama,
			
						staDel: it.staDel,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[goodsHargaJualInstance: new GoodsHargaJual(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		params.t151HargaDenganPPN = params?.t151HargaTanpaPPN?.toString()?.replace(",","").toDouble() + (params?.t151HargaTanpaPPN?.toString()?.replace(",","").toDouble() * 0.1)
        params.t151HargaTanpaPPN = params.t151HargaTanpaPPN?.toString()?.replace(",","").toDouble()
        def goodsHargaJualInstance = new GoodsHargaJual(params)
        goodsHargaJualInstance?.goods = Goods.findById(params.goodsId as Long)
        goodsHargaJualInstance?.satuan = Goods.findById(params.goodsId as Long)?.satuan
        goodsHargaJualInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        goodsHargaJualInstance?.setLastUpdProcess("INSERT")
        goodsHargaJualInstance?.setCompanyDealer(session.userCompanyDealer)
        goodsHargaJualInstance?.setStaDel("0")
        def cek = GoodsHargaJual.createCriteria().list {
            eq("staDel","0")
            goods{
                eq("id",params.goodsId as Long)
            }
            ge("t151TMT",params.t151TMT)
            lt("t151TMT",params.t151TMT + 1)

        }
        if(cek){
            flash.message = "Data Sudah Ada"
            render(view: "create", model: [goodsHargaJualInstance: goodsHargaJualInstance])
            return
        }
		if (!goodsHargaJualInstance.save(flush: true)) {
			render(view: "create", model: [goodsHargaJualInstance: goodsHargaJualInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'goodsHargaJual.label', default: 'Harga Jual Goods'), ""])
		redirect(action: "show", id: goodsHargaJualInstance.id)
	}

	def show(Long id) {
		def goodsHargaJualInstance = GoodsHargaJual.get(id)
		if (!goodsHargaJualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsHargaJual.label', default: 'GoodsHargaJual'), id])
			redirect(action: "list")
			return
		}

		[goodsHargaJualInstance: goodsHargaJualInstance]
	}

	def edit(Long id) {
		def goodsHargaJualInstance = GoodsHargaJual.get(id)
		if (!goodsHargaJualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsHargaJual.label', default: 'GoodsHargaJual'), id])
			redirect(action: "list")
			return
		}

		[goodsHargaJualInstance: goodsHargaJualInstance]
	}

	def update(Long id, Long version) {
        params?.t151HargaDenganPPN = params?.t151HargaTanpaPPN?.toString()?.replace(",","").toDouble() + (params?.t151HargaTanpaPPN?.toString()?.replace(",","").toDouble() * 0.1)
        params?.t151HargaTanpaPPN = params?.t151HargaTanpaPPN?.toString()?.replace(",","").toDouble()
        def goodsHargaJualInstance = GoodsHargaJual?.get(id)
		if (!goodsHargaJualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsHargaJual.label', default: 'GoodsHargaJual'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (goodsHargaJualInstance.version > version) {

				goodsHargaJualInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'goodsHargaJual.label', default: 'GoodsHargaJual')] as Object[],
				"Another user has updated this GoodsHargaJual while you were editing")
				render(view: "edit", model: [goodsHargaJualInstance: goodsHargaJualInstance])
				return
			}
		}

        def cek = GoodsHargaJual.createCriteria().list {
            eq("staDel","0")
            goods{
                eq("id",params?.goodsId as Long)
            }

            ge("t151TMT",params.t151TMT)
            lt("t151TMT",params.t151TMT + 1)

        }
//        if(cek){
//            for(find in cek){
//                if(find.id!=id){
//                    flash.message = "Data Sudah Ada"
//                    render(view: "edit", model: [goodsHargaJualInstance: goodsHargaJualInstance])
//                    return
//                }
//            }
//        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        goodsHargaJualInstance?.properties = params
        goodsHargaJualInstance?.setLastUpdProcess("UPDATE")
        goodsHargaJualInstance?.setCompanyDealer(session.userCompanyDealer)
        goodsHargaJualInstance?.goods = Goods.findById(params?.goodsId as Long) // as Long
        goodsHargaJualInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
		if (!goodsHargaJualInstance.save(flush: true)) {
			render(view: "edit", model: [goodsHargaJualInstance: goodsHargaJualInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'goodsHargaJual.label', default: 'Harga Jual Goods'), ""])
		redirect(action: "show", id: goodsHargaJualInstance.id)
	}

	def delete(Long id) {
		def goodsHargaJualInstance = GoodsHargaJual.get(id)
		if (!goodsHargaJualInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsHargaJual.label', default: 'GoodsHargaJual'), id])
			redirect(action: "list")
			return
		}

		try {
            goodsHargaJualInstance?.setLastUpdProcess("DELETE")
            goodsHargaJualInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            goodsHargaJualInstance?.setStaDel("1")
			goodsHargaJualInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'goodsHargaJual.label', default: 'GoodsHargaJual'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'goodsHargaJual.label', default: 'GoodsHargaJual'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(GoodsHargaJual, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(GoodsHargaJual, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def getPPN(){
        Double result = -1
        def company=CompanyDealer.findById(session.userCompanyDealer.id)
        def general = GeneralParameter.findByCompanyDealerAndM000StaDel(company,"0")
        if(general){
            if(general?.m000Tax!=null){
                result = general.m000Tax
            }
        }
        render result
    }

    def listParts() {
        def res = [:]
        def opts = []
        def z = Goods.createCriteria()
        def results=z.list {
            or{
                ilike("m111Nama",params.query + "%")
                ilike("m111ID",params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.m111ID.trim() +" || "+ it.m111Nama
        }
        res."options" = opts
        render res as JSON

    }

    def detailParts(){
        def result = [:]
        def data= params.noGoods as String
        if(data.contains("||")){
            String charKodeGoods = ""
            String kodeGoods = ""
            def dataNogoods = data.split()
            dataNogoods.each {
                if(it.contains("||")){
                    kodeGoods = charKodeGoods
                    return false
                }
                charKodeGoods +=" "+ it
            }
            def goods = Goods.createCriteria().list {
                eq("staDel","0")
                eq("m111ID",kodeGoods.trim())

            }
            if(goods.size()>0){
                def goodsData = goods.last()
                result."hasil" = "ada"
                result."id" = goodsData?.id
                result."namaGoods" = goodsData?.m111Nama
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }
	
	
}
