package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadHargaJualGoodsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService
    def conversi = new Konversi()

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
    }

    def upload() {
        def goodsHargaJualInstance = null
        def requestBody = request.JSON

        def cekGoods=null
        def cekSatuan=null
        DateFormat df=new SimpleDateFormat("dd-MM-yyyy")
        requestBody.each{
            goodsHargaJualInstance = new GoodsHargaJual();
            String goodsTemp = it.goods , satuanTemp = it.satuan
            def cari = GoodsHargaJual.createCriteria().list {
                eq('staDel','0');
                goods{
                    eq('staDel','0')
                    ilike('m111ID',goodsTemp.trim() + "%")
                }
                maxResults(1);
            }
            cekGoods=Goods.createCriteria().list {
                eq('staDel','0')
                ilike('m111ID',goodsTemp.trim() + "%")
            }
            cekSatuan=Satuan.createCriteria().list {
                eq('staDel','0')
                or {
                    eq('m118Satuan1',satuanTemp.trim(),[ignoreCase: true])
                }
                or{
                    eq('m118Satuan2',satuanTemp.trim(),[ignoreCase: true])
                }
            }
            def newGoods = new Goods()
            if(!cekGoods){
                newGoods = new Goods(
                        m111ID : goodsTemp.trim(),
                        m111Nama : it.goodsNama,
                        satuan : cekSatuan.first(),
                        staDel : "0",
                        createdBy : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess : "UPLOAD",
                        dateCreated: datatablesUtilService?.syncTime(),
                        lastUpdated: datatablesUtilService?.syncTime()
                ).save(flush: true)
            }
            if(cekSatuan && (it.t151TMT && it.t151TMT!="") && (it.t151HargaTanpaPPN && it.t151HargaTanpaPPN!="") &&
                    (it.t151HargaDenganPPN && it.t151HargaDenganPPN!="")){
                if(cari){
                    goodsHargaJualInstance = cari.first()
                    goodsHargaJualInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    goodsHargaJualInstance?.lastUpdProcess = "UPDATE"
                }else{
                    if(!cekGoods){
                        goodsHargaJualInstance?.goods = newGoods
                    }else{
                         goodsHargaJualInstance?.goods = cekGoods.first()
                    }
                    goodsHargaJualInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    goodsHargaJualInstance?.setStaDel('0')
                    goodsHargaJualInstance?.lastUpdProcess = "INSERT"
                    goodsHargaJualInstance?.dateCreated = datatablesUtilService?.syncTime()
                }
                goodsHargaJualInstance?.companyDealer = session.userCompanyDealer
                goodsHargaJualInstance?.lastUpdated = datatablesUtilService?.syncTime()
                goodsHargaJualInstance?.satuan = cekSatuan.first()
                goodsHargaJualInstance?.t151TMT = new Date().parse("yyyy-M-dd",it.t151TMT)
                goodsHargaJualInstance?.t151HargaTanpaPPN = it.t151HargaTanpaPPN
                goodsHargaJualInstance?.t151HargaDenganPPN = it.t151HargaDenganPPN

                goodsHargaJualInstance.save()
                goodsHargaJualInstance.errors.each {println it}
            }
        }

        flash.message = message(code: 'default.uploadHargaJualGoods.message', default: "Upload Harga Jual Goods Done")
        render(view: "index", model: [goodsHargaJualInstance: goodsHargaJualInstance])
    }

    def view() {
        def goodsHargaJualInstance = new GoodsHargaJual(params)
        DateFormat df=new SimpleDateFormat("dd-MM-yyyy")
        //handle upload file
        Map CONFIG_GOODSHARGAJUAL_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'t151TMT',
                        'B':'goods',
                        'C':'goodsNama',
                        'D':'t151HargaTanpaPPN',
                        'E':'t151HargaDenganPPN',
                        'F':'satuan',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        int jmlDataSama = 0;
        int jmlBlmTerdaftar = 0;
        if(!uploadExcel?.empty){
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream','application/vnd.ms-excel','application/vnd.msexcel'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [goodsHargaJualInstance: goodsHargaJualInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def goodsHargaJualList = excelImportService.columns(workbook, CONFIG_GOODSHARGAJUAL_COLUMN_MAP)

            jsonData = goodsHargaJualList as JSON

            def cekGoods=null
            def cekSatuan=null

            String status = "0",staSama="0", style = "",staDaftar="0"
            goodsHargaJualList?.each {
                staSama="0"
                style = ""
                staDaftar="0"
                String goodsTemp = it.goods , satuanTemp = it.satuan
                def cari = GoodsHargaJual.createCriteria().list {
                    eq('staDel','0');
                    goods{
                        eq('staDel','0')
                        ilike('m111ID',goodsTemp.trim() + "%")
                    }
                }

                if(cari){
                    jmlDataSama++;
                    staSama = "1";
                }

                if((it.goods && it.goods!="")&&(it.satuan && it.satuan!="")&&(it.t151TMT && it.t151TMT!="") && (it.t151HargaTanpaPPN && it.t151HargaTanpaPPN!="") &&
                        (it.t151HargaDenganPPN && it.t151HargaDenganPPN!="") & it.t151HargaDenganPPN.toString().isNumber() && it.t151HargaTanpaPPN.toString().isNumber()){
                    cekGoods=Goods.createCriteria().list {
                        eq('staDel','0')
                        ilike('m111ID',goodsTemp.trim()+"%")
                    }
                    cekSatuan=Satuan.createCriteria().list {
                        eq('staDel','0')
                        or {
                            eq('m118Satuan1',satuanTemp.trim(),[ignoreCase: true])
                        }
                        or{
                            eq('m118Satuan2',satuanTemp.trim(),[ignoreCase: true])
                        }
                    }

                    if(!cekSatuan){
                        jmlhDataError++;
                        status = "1";
                    }
                    if(!cekGoods){
                        jmlBlmTerdaftar++
                        staDaftar = "1"
                    }

                } else {
                    jmlhDataError++;
                    status = "1";

                }
                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    if(staSama.equalsIgnoreCase("1")){
                        style = "style='color:green;'"
                    }else if(staDaftar.equalsIgnoreCase("1")){
                        style = "style='color:blue;'"
                    }
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.t151TMT+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.goods+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.goodsNama+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.t151HargaTanpaPPN)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+conversi.toRupiah(it.t151HargaDenganPPN)+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.satuan+"\n" +
                        "                            </td>\n" +
                        "                         </tr>"

                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadDataSalesError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadDataSales.message', default: "Read File Done")
        }

        render(view: "index", model: [goodsHargaJualInstance: goodsHargaJualInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError,dataSama : jmlDataSama,jmlBlmTerdaftar:jmlBlmTerdaftar])
    }

}
