package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON


class PartsAgingController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def index() {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def dataTablesSub() {
        String idTable = new Date().format("yyyyMMddhhmmss")+params.noApp
        [idApp: params.idApp, idTable: idTable]
    }

    def subList(){
        String idTable = new Date().format("ddMMyyyyhhmm")+params?.idDetail
        [idDetail: params.idDetail, idTable: idTable]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0


        session.exportParams=params
        String kodeGoods = params?.sCriteria_goods ? params?.sCriteria_goods?.toString()?.substring(0,params?.sCriteria_goods?.toString()?.indexOf("|")) : "--"
        def goods = Goods.findByM111IDIlikeAndStaDel("%"+kodeGoods.trim()+"%","0")
        def c = GoodsReceive.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.companyDealer)
            eq("staDel","0")
            goodsReceiveDetail {
                eq("goods",goods)
            }
            if(params?.sCriteria_tanggalAkhir){
                le("t167TglJamReceive",new Date().parse("dd-MM-yyyy",params?.sCriteria_tanggalAkhir))
            }else{
                eq("staDel","5")
            }
            if(params?.sCriteria_tanggalAwal){
                ge("t167TglJamReceive",new Date().parse("dd-MM-yyyy",params?.sCriteria_tanggalAwal))
            }else{
                eq("staDel","5")
            }
            order("t167TglJamReceive")
        }

        def rows = []
        def partRecepIds = []
        results.each {
            def goodsTrue = GoodsReceiveDetail.findByGoodsAndGoodsReceive(goods,it);
            def parts = PartsRCP.findAllByGoodsAndStaDel(goodsTrue?.goods,"0");
            def qtyLoop = goodsTrue?.t167Qty1Issued
            def qtyUsed = 0
            parts?.each {
                if(qtyLoop>0){
                    partRecepIds << it?.id
                    qtyLoop-=it?.t403Jumlah1
                    qtyUsed+=it.t403Jumlah1
                }
            }
            rows << [

                    id: it.id,

                    idDetail : goodsTrue?.id,

                    tanggalReceive: it?.t167TglJamReceive ? it?.t167TglJamReceive?.format("dd/MM/yyyy") : "",

                    qty: goodsTrue?.t167Qty1Issued,

                    qtyUsed: qtyUsed,

                    qtySisa: goodsTrue?.t167Qty1Issued - qtyUsed,


            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params
        def goods = GoodsReceiveDetail.get(params?.idDetail?.toLong())
        def result = []
        def parts = PartsRCP.createCriteria().list {
            eq("staDel","0");
            eq("goods",goods?.goods);
            reception{
                eq("companyDealer",session?.userCompanyDealer)
            }
            or{
                ilike("t403StaTambahKurang","%0%")
                and{
                    not{
                        ilike("t403StaTambahKurang","%1%")
                        ilike("t403StaApproveTambahKurang","%1%")
                    }
                }
                and{
                    isNull("t403StaTambahKurang")
                    isNull("t403StaApproveTambahKurang")
                }
            }
        }
        def qtyLoop = goods?.t167Qty1Issued
        def qtyUsed = 0
        parts?.each {
            if(qtyLoop>0){
                result << it
                qtyLoop-=it?.t403Jumlah1
                qtyUsed+=it.t403Jumlah1
            }
        }

        def rows = []
        def partRecepIds = []
        result.each {
            rows << [

                    id: it.id,

                    noWO : it?.reception?.t401NoWO,

                    tanggal: it?.reception?.t401TanggalWO ? it?.reception?.t401TanggalWO?.format("dd/MM/yyyy") : "",

                    qty: it?.t403Jumlah1,


            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  result.size(), iTotalDisplayRecords: result.size(), aaData: rows]

        render ret as JSON
    }

    def listGoods(){
        def res = [:]
        def opts = []
        def vincodes = Goods.createCriteria().list {
            eq("staDel","0")
            or{
                ilike("m111ID","%"+params.word+"%")
                ilike("m111Nama","%"+params.word+"%")
            }
            order("m111ID")
            maxResults(10);
        }
        vincodes.each {
            opts<<it.m111ID.trim()+" | "+it.m111Nama.trim()
        }
        res."options" = opts
        render res as JSON
    }
}
