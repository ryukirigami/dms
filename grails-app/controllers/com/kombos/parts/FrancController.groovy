package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.finance.AccountNumber
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class FrancController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Franc.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m117ID") {
                ilike("m117ID","%" + (params."sCriteria_m117ID" as String) + "%")
            }

            if (params."sCriteria_m117NamaFranc") {
                ilike("m117NamaFranc", "%" + (params."sCriteria_m117NamaFranc" as String) + "%")
            }

            if (params."sCriteria_m117Ket") {
                ilike("m117Ket", "%" + (params."sCriteria_m117Ket" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_m117NomorAkun") {
                m117NomorAkun {
                    ilike("accountNumber", "%" + (params."sCriteria_m117NomorAkun" as String) + "%")
                }
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m117ID: it.m117ID,

                    m117NamaFranc: it.m117NamaFranc,

                    m117Ket: it.m117Ket,

                    m117NomorAkun: it.m117NomorAkun.accountNumber,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [francInstance: new Franc(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def francInstance = new Franc(params)
        francInstance?.m117NomorAkun = AccountNumber.findById(params.m117NomorAkun.id as Long)
        francInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        francInstance?.lastUpdProcess = "INSERT"
        def cekkode = Franc.createCriteria().list() {
            and{
                eq("m117ID",francInstance.m117ID?.trim(), [ignoreCase: true])
            }
        }
        if(cekkode){
            flash.message = "Kode Franchise sudah ada"
            render(view: "create", model: [francInstance: francInstance])
            return
        }
        def ceknama = Franc.createCriteria().list() {
            and{
                eq("m117NamaFranc",francInstance.m117NamaFranc?.trim(), [ignoreCase: true])
            }
        }
        if(ceknama){
            flash.message = "Nama Franchise sudah ada"
            render(view: "create", model: [francInstance: francInstance])
            return
        }
        if (!francInstance.save(flush: true)) {
            render(view: "create", model: [francInstance: francInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'franc.label', default: 'Franc'), francInstance.id])
        redirect(action: "show", id: francInstance.id)
    }

    def show(Long id) {
        def francInstance = Franc.get(id)
        if (!francInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'franc.label', default: 'Franc'), id])
            redirect(action: "list")
            return
        }

        [francInstance: francInstance]
    }

    def edit(Long id) {
        def francInstance = Franc.get(id)
        if (!francInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'franc.label', default: 'Franc'), id])
            redirect(action: "list")
            return
        }

        [francInstance: francInstance]
    }

    def update(Long id, Long version) {
        def francInstance = Franc.get(id)
        if (!francInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'franc.label', default: 'Franc'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (francInstance.version > version) {

                francInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'franc.label', default: 'Franc')] as Object[],
                        "Another user has updated this Franc while you were editing")
                render(view: "edit", model: [francInstance: francInstance])
                return
            }
        }

        def cekfull = Franc.createCriteria()
        def resultfull = cekfull.list() {
            and{
                eq("m117ID",params.m117ID?.trim(), [ignoreCase: true])
                eq("m117NamaFranc",params.m117NamaFranc?.trim(), [ignoreCase: true])
                eq("m117Ket",params.m117Ket?.trim(),  [ignoreCase: true])
            }
        }
        if(resultfull){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [francInstance: francInstance])
            return
        }

        def cekkode = Franc.createCriteria()
        def resultkode = cekkode.list() {
            and{
                eq("m117ID",params.m117ID?.trim(), [ignoreCase: true])
            }
        }
        if(resultkode){
            for(find in resultkode){
                if(id!=find.id){
                    flash.message = "Kode Franchise sudah ada"
                    render(view: "edit", model: [francInstance: francInstance])
                    return
                }
            }
        }

        def ceknama = Franc.createCriteria()
        def resultnama = ceknama.list() {
            and{
                eq("m117NamaFranc",params.m117NamaFranc?.trim(), [ignoreCase: true])
            }
        }
        if(resultnama){
            for(find in resultnama){
                if(id!=find.id){
                    flash.message = "Nama franchise sudah ada"
                    render(view: "edit", model: [francInstance: francInstance])
                    return
                }
            }
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        francInstance.properties = params
        francInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        francInstance?.lastUpdProcess = "UPDATE"

        if (!francInstance.save(flush: true)) {
            render(view: "edit", model: [francInstance: francInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'franc.label', default: 'Franc'), francInstance.id])
        redirect(action: "show", id: francInstance.id)
    }

    def delete(Long id) {
        def francInstance = Franc.get(id)
        if (!francInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'franc.label', default: 'Franc'), id])
            redirect(action: "list")
            return
        }

        try {
            francInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            francInstance?.lastUpdProcess = "DELETE"
            francInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'franc.label', default: 'Franc'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'franc.label', default: 'Franc'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Franc, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Franc, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def listAccount() {
        def res = [:]
        def opts = []
        def z = AccountNumber.createCriteria()
        def results=z.list {
            or{
                ilike("accountNumber","%" + params.query + "%")
                ilike("accountName","%" + params.query + "%")
            }
            eq("accountMutationType","MUTASI")
            eq('staDel', '0')
            order("accountNumber","asc")
//            setMaxResults(10)
        }
        results.each {
            opts<<it.accountNumber +" || "+ it.accountName
        }
        res."options" = opts
        render res as JSON
    }

    def detailAccount(){
        def result = [:]
        def data= params.noAccount as String
        if(data.contains("||")){
            def dataNoAccount = data.split(" ")
            def account = AccountNumber.createCriteria().list {
                eq("staDel","0")
                ilike("accountNumber","%" + dataNoAccount[0].trim() + "%")
            }
            if(account.size()>0){
                def accountData = account.last()
                result."hasil" = "ada"
                result."id" = accountData?.id
                result."namaAccount" = accountData?.accountName
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }


}
