package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class ETAController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def ETAService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render ETAService.datatablesList(params) as JSON
	}

	def create() {
		def result = ETAService.create(params)

        if(!result.error)
            return [ETAInstance: result.ETAInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = ETAService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["ETA", result.ETAInstance.id])
            redirect(action:'show', id: result.ETAInstance.id)
            return
        }

        render(view:'create', model:[ETAInstance: result.ETAInstance])
	}

	def show(Long id) {
		def result = ETAService.show(params)

		if(!result.error)
			return [ ETAInstance: result.ETAInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = ETAService.show(params)

		if(!result.error)
			return [ ETAInstance: result.ETAInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
		def result = ETAService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["ETA", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[ETAInstance: result.ETAInstance.attach()])
	}

	def delete() {
		def result = ETAService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["ETA", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(ETA, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
