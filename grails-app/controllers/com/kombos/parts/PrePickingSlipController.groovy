package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class PrePickingSlipController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def prePickingSlipService
    def jasperService
    def konversi = new Konversi()
    def vendor = null
    def noref = null
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }


    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [t141Wo: params.t141Wo, idTable: new Date().format("yyyyMMddhhmmss"),sCriteria_status: params.sCriteria_status]
    }

    def datatablesList() {
        params?.companyDealer = session?.userCompanyDealer
        render prePickingSlipService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params?.companyDealer = session?.userCompanyDealer
        render prePickingSlipService.datatablesSubList(params) as JSON
    }


    def printPrePickingSlip(){
        def jsonArray = JSON.parse(params.idPickingSlip)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'prePicking.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )
            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("prePicking_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def reportData = new ArrayList();
        def reception = Reception.findByT401NoWO(id)
        def nopol = reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + "-" + reception?.historyCustomerVehicle?.t183NoPolTengah + "-" + reception?.historyCustomerVehicle?.t183NoPolBelakang
        def results = PartsRCP.findAllByReception(reception)
        results.sort {
            it.goods.m111ID
        }

        int count = 0

        results.each {
            def data = [:]

            count = count + 1

            data.put("tgl", konversi.tanggalIndonesia(JPB.findByReception(it.reception)?.t451TglJamPlan?.format("dd-MM-yyyy").toString()))
            data.put("nomorWO", reception.t401NoWO as String)
            data.put("noPol", nopol)
            data.put("companyDealer", reception?.companyDealer?.m011NamaWorkshop)
            data.put("kota", reception?.companyDealer?.kabKota?.m002NamaKabKota)

            data.put("noUrut", count as String)
            data.put("kodeGoods",it.goods?.m111ID)
            data.put("namaGoods",it.goods?.m111Nama)
            data.put("lokasi",KlasifikasiGoods.findByGoods(it.goods)?.getLocation()?.m120NamaLocation)
            data.put("qty",it.t403Jumlah1 as String)
            data.put("satuan",it.goods.satuan.m118Satuan1)
            data.put("tanggalSekarang", konversi.tanggalIndonesia(new Date().format("dd-MM-yyyy").toString()))

            reportData.add(data)
        }

        return reportData

    }

}
