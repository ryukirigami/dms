package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class PoInvoiceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def poInvoiceService
    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = PO.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            if(params."tanggalStart" && params."tanggalEnd"){
               ge("t164TglPO",params."tanggalStart")
               le("t164TglPO",params."tanggalEnd" + 1)
            }
            if(params."staSPLD1"){
                vendor{
                    eq("m121staSPLD","0")
                }
            }
            if(params."staSPLD2"){
                vendor{
                    eq("m121staSPLD","1")
                }
            }
            order("t164NoPO","desc")
            vendor{
                order("m121Nama","asc")
            }

        }
        def rows = []
        PO po
        def tampil = false
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
        results.each {
            tampil = false
            if(params."staSPLD3"){
                def inv = Invoice.findByPo(it)
                if(!inv){
                    tampil = true
                }
            }else if(params."staSPLD4"){
                def inv = Invoice.findByPo(it)
                if(inv){
                    tampil = true
                }
            }else{
                tampil = true
            }
                po = it
            if(tampil == true){
                rows << [
                        id: po.id,

                        vendor: po.vendor.m121Nama,

                        noPo: po.t164NoPO,

                        tglPo: sdf.format(po.t164TglPO),

                        tipeOrder : PODetail.findByPo(po)?.validasiOrder?.partTipeOrder?.m112TipeOrder
                ]
            }

        }
        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def sublist(){
        [id: params.id,idTable:new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
        render poInvoiceService.datatablesSubList(params) as JSON
    }

    def subsublist(){
        [id: params.id,idPart : params.idPart,idTable:new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubSubList() {
        params.companyDealer = session?.userCompanyDealer
        render poInvoiceService.datatablesSubSubList(params) as JSON
    }

    def printPartsReconsile(){
        def jsonArray = JSON.parse(params.idPartsReconsile)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }


            def reportData = []
            int count = 0
            returnsList.each {
                count++;
                def pox = PO.findById(it)
                def poDetail = PODetail.findByPo(pox)
                def c = PODetail.createCriteria()
                def results = c.list {
                    eq("po",pox)
                }
                def dataParts = []
                int countParts = 0
                results.each {
                    countParts++
                    def inv = Invoice?.findByPoAndStaDel(it.po,"0")
                    dataParts << [
                            noTable : countParts,
                            kode : it?.validasiOrder?.requestDetail?.goods?.m111ID,
                            namaParts : it?.validasiOrder?.requestDetail?.goods?.m111Nama,
                            qty : it?.t164Qty1? it.t164Qty1:'',
                            noInvoice: inv?.t166NoInv? inv?.t166NoInv:'-',
                            tglInvoice: inv?.t166TglInv?inv?.t166TglInv.format("dd/MM/yyyy"):'-',
                            qtyInvoice : inv?.t166Qty1?:''
                    ]
                }
                reportData << [
                        dataParts : dataParts,
                        vendor : pox?.vendor?.m121Nama,
                        noPo : pox?.t164NoPO,
                        tanggalPo : pox?.t164TglPO.format("dd/MM/yyyy"),
                        tipeOrder : poDetail?.validasiOrder.partTipeOrder?.m112TipeOrder,
                        no : count,
                        dealer : session.userCompanyDealer,
                ]

            }

            def reportDef = new JasperReportDef(name:'partReconsile.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )

            reportDefList.add(reportDef)


        def file = File.createTempFile("partReconsile_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }
}
