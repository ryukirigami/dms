package com.kombos.parts

import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GoodsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m111ID") {
                ilike("m111ID", "%" + (params."sCriteria_m111ID" as String) + "%")
            }

            if (params."sCriteria_m111Nama") {
                ilike("m111Nama", "%" + (params."sCriteria_m111Nama" as String) + "%")
            }

            if (params."sCriteria_satuan") {
                eq("satuan", Satuan.findById(params."sCriteria_satuan"))
            }


            ilike("staDel", "0")

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            def ghj = GoodsHargaJual.findByGoodsAndStaDelAndT151TMTLessThanEquals(it, "0",new Date(),[sort: "t151TMT",order: "desc"])
            def mappingCompRegion = MappingCompanyRegion.createCriteria().list {
                eq("staDel","0");
                eq("companyDealer",session?.userCompanyDealer);
            }
            def mappingRegion = MappingPartRegion.createCriteria().get {
                if(mappingCompRegion?.size()>0){
                    inList("region",mappingCompRegion?.region)
                }else{
                    eq("id",-10000.toLong())
                }
                maxResults(1);
            }
            def hargaTanpaPPN = ghj?.t151HargaTanpaPPN ? ghj?.t151HargaTanpaPPN : 0
            if(mappingRegion){
                hargaTanpaPPN = hargaTanpaPPN + (hargaTanpaPPN * mappingRegion?.percent);
            }
            rows << [

                    id: it.id,

                    m111ID: it.m111ID,

                    m111Nama: it.m111Nama,

                    satuan: it.satuan.m118Satuan1,

                    staDel: it.staDel,

                    franc: KlasifikasiGoods.findByGoods(it)? KlasifikasiGoods.findByGoods(it)?.franc?.m117NamaFranc : "",

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    harga: hargaTanpaPPN

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [goodsInstance: new Goods(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def goodsInstance = new Goods(params)
        def cek = goodsInstance.createCriteria()
        def result = cek.list() {
            ilike("m111ID",goodsInstance.m111ID?.trim() + "%")
            eq("staDel",'0')
        }
        if(result?.size()>0 && result?.m111ID?.indexOf(params?.m111ID)>=0){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Data Sudah Ada')
            render(view: "create", model: [goodsInstance: goodsInstance])
            return
        }
        goodsInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        goodsInstance?.lastUpdProcess = "INSERT"
        goodsInstance?.setStaDel('0')

        if (!goodsInstance.save(flush: true)) {
            render(view: "create", model: [goodsInstance: goodsInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'goods.label', default: 'Goods'), goodsInstance.id])
        redirect(action: "show", id: goodsInstance.id)
    }

    def show(Long id) {
        def goodsInstance = Goods.get(id)
        if (!goodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'goods.label', default: 'Goods'), id])
            redirect(action: "list")
            return
        }

        [goodsInstance: goodsInstance]
    }

    def edit(Long id) {
        def goodsInstance = Goods.get(id)
        if (!goodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'goods.label', default: 'Goods'), id])
            redirect(action: "list")
            return
        }

        [goodsInstance: goodsInstance]
    }

    def update(Long id, Long version) {
        def goodsInstance = Goods.get(id)
        if (!goodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'goods.label', default: 'Goods'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (goodsInstance.version > version) {

                goodsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'goods.label', default: 'Goods')] as Object[],
                        "Another user has updated this Goods while you were editing")
                render(view: "edit", model: [goodsInstance: goodsInstance])
                return
            }
        }
        def cek = goodsInstance.createCriteria()
        def result = cek.list() {
            eq("m111ID",params.m111ID, [ignoreCase: true])
            eq("staDel",'0')
        }

        if(result && Goods.findByM111IDIlikeAndStaDel(params.m111ID,'0')?.id != id){
            flash.message = message(code: 'default.created.operation.error.message', default: 'Kode Sudah Ada')
            render(view: "edit", model: [goodsInstance: goodsInstance])
            return
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        goodsInstance.properties = params
        goodsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        goodsInstance?.lastUpdProcess = "UPDATE"
        if (!goodsInstance.save(flush: true)) {
            render(view: "edit", model: [goodsInstance: goodsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'goods.label', default: 'Goods'), goodsInstance.id])
        redirect(action: "show", id: goodsInstance.id)
    }

    def delete(Long id) {
        def goodsInstance = Goods.get(id)
        if (!goodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'goods.label', default: 'Goods'), id])
            redirect(action: "list")
            return
        }

        try {
            //goodsInstance.delete(flush: true)
            goodsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            goodsInstance?.lastUpdProcess = "DELETE"
            goodsInstance?.setStaDel('1')
            goodsInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'goods.label', default: 'Goods'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'goods.label', default: 'Goods'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Goods, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Goods, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
