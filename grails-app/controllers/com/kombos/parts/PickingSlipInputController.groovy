package com.kombos.parts

import com.kombos.administrasi.NamaManPower
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.board.JPB
import com.kombos.finance.Journal
import com.kombos.finance.JournalDetail
import com.kombos.finance.JournalPendingMasukService
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class PickingSlipInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def jam = datatablesUtilService.syncTime().getHours()
        def menit = datatablesUtilService.syncTime().getMinutes()
        [jam : jam, menit : menit]
    }
    def validasiStok(){
        def jsonArray = JSON.parse(params.ids)
        def result = [:]
        def oList = []
        def nilai = []
        def stokR = null
        def klasifikasi = null
        def goodshargabeli = null
        def goodshargajual = null
        def prcp = []
        result.save = 'succes'
        jsonArray.each { oList << PartsRCP.get(it)
            nilai << params."qty-${it}"
            prcp << PartsRCP.findById(it)
        }
        result.message = 'Save Failed!! Data Parts'
        int count = -1
        oList.each{
            count++
            if(prcp.get(count)){
                stokR = PartsStok.findByGoodsAndCompanyDealerAndStaDel(it.goods,session.userCompanyDealer,'0')?.t131Qty1
                if(stokR==null || stokR <=0 ){
                    result.save = 'failed'
                    result.message += '\n\n' +  it.goods + ' ==> Jumlah Stok : ' + stokR
                }

                klasifikasi = KlasifikasiGoods.findByGoods(it.goods)
                if(!klasifikasi || klasifikasi==null){
                    result.save = 'failed'
                    result.message += '\n\n' +  it.goods + ' ==> Belum diklasifikasikan.'
                }

                goodshargabeli = GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',session.userCompanyDealer)
                if(!goodshargabeli || goodshargabeli==null){
                    result.save = 'failed'
                    result.message += '\n\n' +  it.goods + ' ==> Belum ada Harga Beli.'
                }

                goodshargajual = GoodsHargaJual.findByGoodsAndStaDel(it.goods,'0')
                if(!goodshargajual || goodshargajual==null){
                    result.save = 'failed'
                    result.message += '\n\n' +  it.goods + ' ==> Belum ada Harga Jual.'
                }

                try {
                    if(goodshargabeli.t150Harga.doubleValue() > goodshargajual.t151HargaTanpaPPN.doubleValue()){
                        result.save = 'failed'
                        result.message += '\n\n' +  it.goods + ' ==> HARGA BELI tidak boleh lebih Besar dari HARGA JUAL'
                    }
                }catch (Exception e){

                }

            }
        }
        result.message += '\n\n\n * Mohon untuk melengkapi Data Parts Terlebih Dahulu'
        render result as JSON
    }
    def req(){
        def date = new Date()
        def sdf = new SimpleDateFormat("yyMMdHms")
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
        def tanggal =  params.tanggal
        def pickAdd = null,  pickAdd2 = null,ceks = null, partStok = null
        long ida = 0
        if(params.tanggal=="" || params.noWo=="" || params.namaTeknisi==""){}
        else{
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            def nilai = []
            def prcp = []
            jsonArray.each { oList << PartsRCP.get(it)
                def c = (params."qty-${it}" as String).replace(',','.')
                def angka = Double.valueOf(c as String)
                nilai << angka
                println it
                prcp << PartsRCP.findById(it)
            }
            pickAdd = new PickingSlip()
            pickAdd?.reception =  Reception.findByT401NoWO(params.noWo)
            pickAdd?.t141ID = "PS-" + session?.userCompanyDealer.m011ID+"." + sdf.format(date)
            pickAdd?.t141TglPicking = params.tanggal
            pickAdd?.t141NamaTeknisi = params.namaTeknisi
            pickAdd?.t141Ket = 'ket'
            pickAdd?.t141xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
            pickAdd?.t141xNamaDivisi = 'divisi'
            pickAdd?.staDel = '0'
            pickAdd?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            pickAdd?.lastUpdProcess = "INSERT"
            pickAdd?.companyDealer = session?.userCompanyDealer
            pickAdd?.dateCreated = datatablesUtilService?.syncTime()
            pickAdd?.lastUpdated = datatablesUtilService?.syncTime()

            pickAdd.save()
            pickAdd.errors.each {
                println "ini eror pickAdd : " + it
            }
            int c = -1
            oList.each{
                c+=1
                if(prcp.get(c)){
                    pickAdd2 = new PickingSlipDetail()
                    pickAdd2?.pickingSlip = pickAdd
                    pickAdd2?.goods = it.goods
                    pickAdd2?.t142Qty1 = nilai.get(c) as Double
                    pickAdd2?.t142Qty2 = nilai.get(c) as Double
                    pickAdd2?.t142StaPrePicking = '1'
                    pickAdd2?.t142StaPicking = '0'
                    pickAdd2?.staDel = '0'
                    pickAdd2?.status = '1'
                    pickAdd2?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    pickAdd2?.lastUpdProcess = "INSERT"
                    pickAdd2?.dateCreated = datatablesUtilService?.syncTime()
                    pickAdd2?.lastUpdated = datatablesUtilService?.syncTime()
                    pickAdd2.save()
                    pickAdd2.errors.each {
                        println "ini eror pickAdd2 : " + it
                    }
                    try {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',session.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(it.goods,session.userCompanyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.goods,'0',session.userCompanyDealer)
                        }
                            def kurangStok2 =  PartsStok.get(goodsStok.id)
                            kurangStok2?.companyDealer = session.userCompanyDealer
                            kurangStok2?.goods = it.goods
                            kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - pickAdd2?.t142Qty1
                            kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - pickAdd2?.t142Qty1
                            kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - pickAdd2?.t142Qty1
                            kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - pickAdd2?.t142Qty1
                            kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok2.lastUpdProcess = 'PICKING'
                            kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                            kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                            kurangStok2?.save(flush: true)
                            kurangStok2.errors.each {
                                println "ini eror kurang stok : " + it
                            }

                    }catch (Exception e){
                        println "ini eror Exception : " + e
                    }
                }
            }
            try {
                def journalId = Journal.findByDocNumberAndDescriptionIlikeAndStaDel(params.noWo,"%Journal Pending Masuk%","0")
                if(journalId){
                    def journalDel = Journal.get(journalId.id as Long)
                    journalDel.staDel = '1'
                    journalDel.lastUpdProcess = 'DELETE'
                    journalDel.save(flush: true)
                    journalDel.each {
                        println it
                    }

                    def journalDetail = JournalDetail.findAllByJournal(journalDel)
                    journalDetail.each {
                        def jDel = JournalDetail.get(it.id as Long)
                        jDel.staDel = '1'
                        jDel.lastUpdProcess = 'DELETE'
                        jDel.save(flush: true)
                        jDel.each {
                            println it
                        }
                    }
                }
                new JournalPendingMasukService().createJournal(params)
            }catch (Exception e){
                println e
            }
            flash.message = "* Sukses Ditambahkan"
        }
        redirect(controller: "pickingSlip", action: "list")
    }
    def show(Long id) {
        def pickingSlipInstance = PickingSlipDetail.get(id)
        if (!pickingSlipInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'returns.label', default: 'pickingSlip'), id])
            redirect(action: "list")
            return
        }

        [pickingSlipInstance: pickingSlipInstance]
    }

    def edit(Long id) {
        def pickingSlipInstance = PickingSlipDetail.get(id)
        if (!pickingSlipInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'returns.label', default: 'pickingSlip'), id])
            redirect(action: "list")
            return
        }

        [pickingSlipInstance: pickingSlipInstance]
    }

    def update(Long id, Long version) {

        def pickingSlipInstance = PickingSlipDetail.get(id)
        if (!pickingSlipInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'PickingSlipDetail.label', default: 'pickingSlip'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (pickingSlipInstance.version > version) {
                pickingSlipInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'pickingSlip.label', default: 'pickingSlip')] as Object[],
                        "Another user has updated this pickingSlip while you were editing")
                render(view: "edit", model: [pickingSlipInstance: pickingSlipInstance])
                return
            }
        }

        def pickingSlipInstance2 = pickingSlipInstance
        pickingSlipInstance2?.pickingSlip = pickingSlipInstance?.pickingSlip
        pickingSlipInstance2?.staDel = "0"
        pickingSlipInstance2.t142Qty1 = Double.parseDouble(params.t142Qty1)
        pickingSlipInstance2.t142Qty2 = Double.parseDouble(params.t142Qty1)
        pickingSlipInstance2.goods = pickingSlipInstance?.goods
        pickingSlipInstance2.t142StaPrePicking = '1'
        pickingSlipInstance2.t142StaPicking = '0'
        pickingSlipInstance2.staDel = '0'
        pickingSlipInstance2.status = '1'
        pickingSlipInstance2.lastUpdProcess = 'UPDATE'
        pickingSlipInstance2.lastUpdated = datatablesUtilService?.syncTime()

        //params.satuan
        if (!pickingSlipInstance2.save(flush: true)) {
//            render(view: "edit", model: [pickingSlipInstance: pickingSlipInstance])
            redirect(action: "show", id: pickingSlipInstance2.id)
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'pickingSlip.label', default: 'pickingSlip'), pickingSlipInstance.id])
        redirect(action: "show", id: pickingSlipInstance.id)
    }

    def delete(Long id) {
        def pickingSlipInstance = PickingSlipDetail.get(id)
        if (!pickingSlipInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pickingSlipDetail.label', default: 'pickingSlip'), id])
            redirect(action: "list")
            return
        }

        try {
            pickingSlipInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'pickingSlipDetail.label', default: 'pickingSlip'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'pickingSlipDetail.label', default: 'pickingSlip'), id])
            redirect(action: "show", id: id)
        }
    }
    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PickingSlipDetail, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(PickingSlipDetail, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }
    def kodeList() {
        def res = [:]
        def c = NamaManPower.createCriteria()
        def result = c.listDistinct {
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer);
            ilike("t015NamaLengkap","%"+params?.query+"%");
            manPowerDetail{
                manPower{
                    ilike("m014JabatanManPower","%Teknisi%")
                }
            }
        }
        def opts = []
        result.each {
            opts << it.t015NamaLengkap
        }

        res."options" = opts
        render res as JSON
    }
    def woList() {
        def res = [:]
        def z = Reception.createCriteria()
        def result=z.list {
            eq('staSave', '0');
            eq('staDel', '0');
            ilike("t401NoWO","%"+params?.query+"%");
            eq("companyDealer",session?.userCompanyDealer);
            setMaxResults(10)
        }
        def opts = []
        result.each {
            opts << it.t401NoWO
        }

        res."options" = opts
        render res as JSON
    }

    def cariTeknisi(){
        String nama = "";
        def noWO = params?.noWO;
        if(noWO){
            def reception = Reception.findByT401NoWOIlikeAndCompanyDealerAndStaSaveAndStaDel("%"+noWO+"%",session?.userCompanyDealer,"0","0");
            if(reception){
                def jpb = JPB.findByReceptionAndStaDel(reception,"0",[sort : 'dateCreated',order : 'desc']);
                if(jpb){
                    nama = jpb?.namaManPower?.t015NamaLengkap
                }
            }
        }
        render nama
    }
}
