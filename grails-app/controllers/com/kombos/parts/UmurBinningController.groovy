package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class UmurBinningController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = UmurBinning.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m163TglBerlaku") {
                ge("m163TglBerlaku", params."sCriteria_m163TglBerlaku")
                lt("m163TglBerlaku", params."sCriteria_m163TglBerlaku" + 1)
            }

            if (params."sCriteria_m163BatasWaktuBinning") {
                eq("m163BatasWaktuBinning",Integer.parseInt(params."sCriteria_m163BatasWaktuBinning"))
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m163TglBerlaku: it.m163TglBerlaku ? it.m163TglBerlaku.format(dateFormat) : "",

                    m163BatasWaktuBinning: it.m163BatasWaktuBinning,

                    companyDealer: it.companyDealer,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [umurBinningInstance: new UmurBinning(params)]
    }

    def save() {
        def umurBinningInstance = new UmurBinning(params)
        umurBinningInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        umurBinningInstance?.companyDealer = session.userCompanyDealer
        umurBinningInstance?.lastUpdProcess = "INSERT"
        umurBinningInstance?.dateCreated = datatablesUtilService?.syncTime()
        umurBinningInstance?.lastUpdated = datatablesUtilService?.syncTime()

        def cek = UmurBinning.createCriteria().list() {
            and{
                eq("m163TglBerlaku",umurBinningInstance.m163TglBerlaku)
                eq("m163BatasWaktuBinning",umurBinningInstance.m163BatasWaktuBinning)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [umurBinningInstance: umurBinningInstance])
            return
        }
        if (!umurBinningInstance.save(flush: true)) {
            render(view: "create", model: [umurBinningInstance: umurBinningInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), umurBinningInstance.id])
        redirect(action: "show", id: umurBinningInstance.id)
    }

    def show(Long id) {
        def umurBinningInstance = UmurBinning.get(id)
        if (!umurBinningInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), id])
            redirect(action: "list")
            return
        }

        [umurBinningInstance: umurBinningInstance]
    }

    def edit(Long id) {
        def umurBinningInstance = UmurBinning.get(id)
        if (!umurBinningInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), id])
            redirect(action: "list")
            return
        }

        [umurBinningInstance: umurBinningInstance]
    }

    def update(Long id, Long version) {

        params.lastUpdated = datatablesUtilService?.syncTime()
        def umurBinningInstance = UmurBinning.get(id)
        if (!umurBinningInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (umurBinningInstance.version > version) {

                umurBinningInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'umurBinning.label', default: 'UmurBinning')] as Object[],
                        "Another user has updated this UmurBinning while you were editing")
                render(view: "edit", model: [umurBinningInstance: umurBinningInstance])
                return
            }
        }

        def cek = UmurBinning.createCriteria()
        def result = cek.list() {
            and{
                eq("m163TglBerlaku",params.m163TglBerlaku)
                eq("m163BatasWaktuBinning",Integer.parseInt(params.m163BatasWaktuBinning))
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [umurBinningInstance: umurBinningInstance])
            return
        }

        umurBinningInstance.properties = params
        umurBinningInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        umurBinningInstance?.lastUpdProcess = "UPDATE"

        if (!umurBinningInstance.save(flush: true)) {
            render(view: "edit", model: [umurBinningInstance: umurBinningInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), umurBinningInstance.id])
        redirect(action: "show", id: umurBinningInstance.id)
    }

    def delete(Long id) {
        def umurBinningInstance = UmurBinning.get(id)
        if (!umurBinningInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), id])
            redirect(action: "list")
            return
        }

        try {
            umurBinningInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            umurBinningInstance?.lastUpdProcess = "DELETE"
            umurBinningInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'umurBinning.label', default: 'UmurBinning'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(UmurBinning, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        try {
            datatablesUtilService.updateField(UmurBinning, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
