package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GroupController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Group.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_m181TglBerlaku") {
                ge("m181TglBerlaku", params."sCriteria_m181TglBerlaku")
                lt("m181TglBerlaku", params."sCriteria_m181TglBerlaku" + 1)
            }

            if (params."sCriteria_m181ID") {
                ilike("m181ID", "%" + (params."sCriteria_m181ID" as String) + "%")
            }

            if (params."sCriteria_m181NamaGroup") {
                ilike("m181NamaGroup", "%" + (params."sCriteria_m181NamaGroup" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m181TglBerlaku: it.m181TglBerlaku ? it.m181TglBerlaku.format(dateFormat) : "",

                    m181ID: it.m181ID,

                    m181NamaGroup: it.m181NamaGroup,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [groupInstance: new Group(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def groupInstance = new Group(params)
        groupInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        groupInstance?.lastUpdProcess = "INSERT"
        groupInstance?.setStaDel ('0')
        def cek = Group.createCriteria().list() {
            and{
                eq("m181NamaGroup",groupInstance.m181NamaGroup?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Nama Group sudah ada"
            render(view: "create", model: [groupInstance: groupInstance])
            return
        }
        groupInstance?.m181ID = generateCodeService.codeGenerateSequence("M181_ID",null)
        if (!groupInstance.save(flush: true)) {
            render(view: "create", model: [groupInstance: groupInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'group.label', default: 'Group'), groupInstance.id])
        redirect(action: "show", id: groupInstance.id)
    }

    def show(Long id) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }

        [groupInstance: groupInstance]
    }

    def edit(Long id) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }

        [groupInstance: groupInstance]
    }

    def update(Long id, Long version) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (groupInstance.version > version) {

                groupInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'group.label', default: 'Group')] as Object[],
                        "Another user has updated this Group while you were editing")
                render(view: "edit", model: [groupInstance: groupInstance])
                return
            }
        }

        def cek = Group.createCriteria()
        def result = cek.list() {
            and{
                eq("m181TglBerlaku",params.m181TglBerlaku)
                eq("m181NamaGroup",params.m181NamaGroup?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [groupInstance: groupInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        groupInstance.properties = params
        groupInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        groupInstance?.lastUpdProcess = "UPDATE"

        if (!groupInstance.save(flush: true)) {
            render(view: "edit", model: [groupInstance: groupInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'group.label', default: 'Group'), groupInstance.id])
        redirect(action: "show", id: groupInstance.id)
    }

    def delete(Long id) {
        def groupInstance = Group.get(id)
        if (!groupInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
            return
        }

        try {
            groupInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            groupInstance?.lastUpdProcess = "DELETE"
            groupInstance?.setStaDel('1')
            groupInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'group.label', default: 'Group'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Group, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Group, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
