package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SOQController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
	
	def partsOrderService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [iTanggal : new Date()]
    }
    def tambah(){
		def result = [:]
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { 
			oList << SOQ.get(it)
        }
        //def soq = null, 
		def reqDetail=null,ceks = null

		def currentUserUsername = org.apache.shiro.SecurityUtils.subject.principal.toString()

	    def req = new Request()
		req.t162TglRequest = new Date()
		req.t162StaFA = '0'
		req.t162NoReff = new Date().format('yyyyMdhms')
		req.t162NamaPemohon = org.apache.shiro.SecurityUtils.subject.principal.toString()
		req.t162xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
		req.t162xNamaDivisi = com.kombos.baseapp.sec.shiro.User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi?.m012NamaDivisi
		req.dateCreated = datatablesUtilService?.syncTime()
		req.lastUpdated = datatablesUtilService?.syncTime()
		req.save(flush:true)
		req.errors.each{
			println it
		}
		
        oList.each{ SOQ soq ->

		  soq.statusParts = '1'
          soq.updatedBy = currentUserUsername
          soq.lastUpdProcess = "UPDATE"
          soq.save(flush: true)

            reqDetail = new RequestDetail()
            reqDetail?.goods = soq.goods
            reqDetail?.t162Qty1 = soq.t156Qty1
            reqDetail?.t162Qty2 = soq.t156Qty2
            reqDetail?.t162Qty1Available = 0
            reqDetail?.t162Qty2Available = 0
            reqDetail?.request = req
            reqDetail?.dateCreated = datatablesUtilService?.syncTime()
            reqDetail?.lastUpdated = datatablesUtilService?.syncTime()
            req.addToRequests(reqDetail)
            req.save(flush: true)
            reqDetail?.save(flush: true)
        }
        params.ids = jsonArray.join(',')
			result.status = 'ok'

        render result as JSON
    }
    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = SOQ.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_t156Tanggal") {
                ge("t156Tanggal", params."sCriteria_t156Tanggal")
                lt("t156Tanggal", params."sCriteria_t156Tanggal" + 1)
            }

            if (params."sCriteria_status") {
                eq("statusParts", params."sCriteria_status")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def nos = params.iDisplayStart as int
        results.each {
            nos = nos + 1
            rows << [

                    id: it.id,

                    norut:nos,

                    companyDealer: it.companyDealer,

                    t156Tanggal: it.t156Tanggal ? it.t156Tanggal.format(dateFormat) : "",

                    goods: it.goods.m111ID,

                    goods2: it.goods.m111Nama,

                    satuan1 : it.goods.satuan.m118Satuan1,

                    t156Qty1: it.t156Qty1,

                    t156Qty2: it.t156Qty2,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [SOQInstance: new SOQ(params), aksi : "add"]
    }

    def save() {
        def SOQInstance = new SOQ(params)
        SOQInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        SOQInstance?.setLastUpdProcess("INSERT")
        SOQInstance?.companyDealer = CompanyDealer.findById(session.userCompanyDealerId)
        SOQInstance?.setDateCreated(datatablesUtilService?.syncTime())
        SOQInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        if (!SOQInstance.save(flush: true)) {
            render(view: "create", model: [SOQInstance: SOQInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'SOQ.label', default: 'SOQ'), SOQInstance.id])
        redirect(action: "show", id: SOQInstance.id,)
    }

    def show(Long id) {
        def SOQInstance = SOQ.get(id)
        if (!SOQInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SOQ.label', default: 'SOQ'), id])
            redirect(action: "list")
            return
        }

        [SOQInstance: SOQInstance]
    }

    def edit(Long id) {
        def SOQInstance = SOQ.get(id)
        if (!SOQInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SOQ.label', default: 'SOQ'), id])
            redirect(action: "list")
            return
        }

        [SOQInstance: SOQInstance,aksi : "ubah"]
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def SOQInstance = SOQ.get(id)
        if (!SOQInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SOQ.label', default: 'SOQ'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (SOQInstance.version > version) {

                SOQInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'SOQ.label', default: 'SOQ')] as Object[],
                        "Another user has updated this SOQ while you were editing")
                render(view: "edit", model: [SOQInstance: SOQInstance])
                return
            }
        }

        SOQInstance.properties = params

        if (!SOQInstance.save(flush: true)) {
            render(view: "edit", model: [SOQInstance: SOQInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'SOQ.label', default: 'SOQ'), SOQInstance.id])
        redirect(action: "show", id: SOQInstance.id)
    }

    def delete(Long id) {
        def SOQInstance = SOQ.get(id)
        if (!SOQInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SOQ.label', default: 'SOQ'), id])
            redirect(action: "list")
            return
        }

        try {
            SOQInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'SOQ.label', default: 'SOQ'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'SOQ.label', default: 'SOQ'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(SOQ, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        try {
            datatablesUtilService.updateField(SOQ, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
