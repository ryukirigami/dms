package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class RequestAddController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
		def exist = []
		if(params."sCriteria_exist"){
			JSON.parse(params."sCriteria_exist").each{
				exist << it
			}
		}
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_goods") {
                ilike("m111ID", "%" + (params."sCriteria_goods" as String) + "%")
            }

            if (params."sCriteria_goods2") {
                ilike("m111Nama", "%" + (params."sCriteria_goods2" as String) + "%")
            }
			
			if(exist.size()>0){
				not { 
					or {
						exist.each {
							eq('id', it as long)
						}
					}
				}	
			}

            switch (sortProperty) {

                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def nos = 0
        results.each {
            nos = nos + 1
            def hargaJual = 0
            try{
                hargaJual = GoodsHargaJual.findByGoodsAndStaDel(it,  "0")?GoodsHargaJual.findByGoodsAndStaDel(it, "0")?.t151HargaTanpaPPN:0
            }catch (Exception e){

            }
            def stokR =0
            try {
                stokR = PartsStok.findByGoodsAndCompanyDealerAndStaDel(it,session.userCompanyDealer,'0')?.t131Qty1
            }catch (Exception e){

            }
            rows << [

                    id: it?.id,

                    norut:nos,

                    goods: it?.m111ID,

                    goods2: it?.m111Nama,

                    satuan1 : it?.satuan?.m118Satuan1,
                    hargaJual : hargaJual,
                    stockGudang : stokR

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

}