package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.SimpleDateFormat

class RequestController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def requestService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
        def date = new Date()
        def com = CompanyDealer.findById(session.userCompanyDealerId)?.m011ID
        def sdf = new SimpleDateFormat("yyyyMMdd")
        def sdfJam = new SimpleDateFormat("Hmmss")
        def noRequest = com + "."+sdf.format(date)+ "."+sdfJam.format(date)
        [iNoref : noRequest]
	}
	
	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params?.companyDealer = session?.userCompanyDealer
		def result = requestService.save(params)

		render result as JSON
	}

	def show(Long id) {
		def requestInstance = RequestDetail.get(id)
		if (!requestInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'request.label', default: 'Request'), id])
			redirect(action: "list")
			return
		}

		[requestInstance: requestInstance]
	}

	def edit(Long id) {
		def requestInstance = RequestDetail.get(id)
		if (!requestInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'request.label', default: 'Request'), id])
			redirect(action: "list")
			return
		}

		[requestInstance: requestInstance]
	}

	def update(Long id, Long version) {
		def requestInstance = RequestDetail.get(id)
		if (!requestInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'request.label', default: 'Request'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (requestInstance.version > version) {
				
				requestInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'request.label', default: 'Request')] as Object[],
				"Another user has updated this Request while you were editing")
				render(view: "edit", model: [requestInstance: requestInstance])
				return
			}
		}

		//requestInstance.properties = params
        requestInstance?.t162Qty1 = Double.parseDouble(params.t162Qty1)
        def goodsEdit = Goods.findByM111ID(params.goods)
        def goods = Goods.get(goodsEdit.id)
        goods?.satuan = Satuan.findById(params.satuan.id)
        goods?.save()

        requestInstance?.lastUpdated = datatablesUtilService?.syncTime()
        requestInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        requestInstance?.lastUpdProcess = "UPDATE"

		if (!requestInstance.save(flush: true)) {
			render(view: "edit", model: [requestInstance: requestInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'request.label', default: 'Request'), requestInstance.id])
		redirect(action: "show", id: requestInstance.id)
	}

	def delete(Long id) {
		def requestInstance = Request.get(id)
		if (!requestInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'request.label', default: 'Request'), id])
			redirect(action: "list")
			return
		}

		try {
			requestInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'request.label', default: 'Request'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'request.label', default: 'Request'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(RequestDetail, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Request, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
    def kodeList() {
        def res = [:]
        def result = Request.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.t162NoReff
        }
//
//        opts << "Bandung"
//        opts << "Solo"
//        opts << "Yogyakarta"

        res."options" = opts
        render res as JSON
    }
	
}
