package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.finance.AccountNumber
import grails.converters.JSON

class CategorySlipController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def categorySlipService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render categorySlipService.datatablesList(params,session) as JSON
    }

    def create() {
        def result = categorySlipService.create(params)

        if (!result.error)
            return [categorySlipInstance: result.categorySlipInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def akun = params?.accountNumbers?.toString()?.contains("|") ? params?.accountNumbers?.toString()?.substring(0,params?.accountNumbers?.toString()?.indexOf("|"))?.replace(" ",""): params?.accountNumbers?.toString()?.replace(" ","")
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "INSERT"
        params.staDel = "0"
        params.companyDealer = session.userCompanyDealer
        params.accountNumber = AccountNumber.findByAccountNumberAndStaDel(akun,"0");
        def result = categorySlipService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["CategorySlip", result.categorySlipInstance.id])
            redirect(action: 'show', id: result.categorySlipInstance.id)
            return
        }

        render(view: 'create', model: [categorySlipInstance: result.categorySlipInstance])
    }

    def show(Long id) {
        def result = categorySlipService.show(params)

        if (!result.error)
            return [categorySlipInstance: result.categorySlipInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = categorySlipService.show(params)

        if (!result.error)
            return [categorySlipInstance: result.categorySlipInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = categorySlipService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["CategorySlip", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [categorySlipInstance: result.categorySlipInstance.attach()])
    }

    def delete() {
        def result = categorySlipService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["CategorySlip", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(CategorySlip, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def listAccount(){
        def res = [:]
        def opts = []
        def vincodes = AccountNumber.createCriteria().list {
            eq("staDel","0")
            ilike("accountNumber","%"+params.query+"%")
            order("accountNumber")
            maxResults(10);
        }
        vincodes.each {
            opts<<it.accountNumber
        }
        res."options" = opts
        render res as JSON
    }

    def namaAccount(){
        def hasil ="";
        def akun = params.accNumber.toString().contains("|") ? params.accNumber.toString().substring(0,params.accNumber.toString().indexOf("|")).replace(" ",""): params.accNumber.toString().replace(" ","")
        def accNumber = AccountNumber.createCriteria().get {
            eq("staDel","0")
            eq("accountMutationType","MUTASI")
            eq("accountNumber",akun)
            maxResults(1)
        }

        if(accNumber){
            hasil = accNumber?.accountName
        }
        render hasil
    }

}
