package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class InvoiceController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def invoiceService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
        params.companyDealer = session.userCompanyDealer
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
        String criteria = " 1=1 "
		def rows = []
        def c = Invoice.createCriteria()
        def results = c.list {

            eq("companyDealer",session.userCompanyDealer)
            if (params."tanggalStart" && params."tanggalEnd") {
                ge("t166TglInv", params."tanggalStart")
                lt("t166TglInv", params."tanggalEnd" + 1)
            }
            eq("staDel",'0')
            if(params."search_noInv"){
                ilike("t166NoInv","%"+params."search_noInv"+"%")
            }
            if(params."vendor"){
                po{
                    vendor{
                        eq("id",params.vendor as Long)
                    }
                }
            }
            if(params."staSPLD1"){
                po{
                    vendor{
                        eq("m121staSPLD","0")
                    }
                }
            }
            if(params."staSPLD2"){
                po{
                    vendor{
                        eq("m121staSPLD","1")
                    }
                }
            }
            po{
                vendor{
                    order("m121Nama","asc")
                }
            }

        }
        def temp = ""

        int size = 0
		results.each {
            String nama = it.po.vendor.m121Nama
            if(nama!=temp){
                rows << [
                        id: it.id,

                        vendorId : it.po.vendor.id,

                        vendor: it.po.vendor.m121Nama,

                        search_noInv: params."search_noInv",

                ]
                size++
            }
            temp = nama
		}

		ret = [sEcho: params.sEcho, iTotalRecords:  size, iTotalDisplayRecords: size, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[invoiceInstance: new Invoice(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def invoiceInstance = new Invoice(params)
		if (!invoiceInstance.save(flush: true)) {
			render(view: "create", model: [invoiceInstance: invoiceInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
		redirect(action: "show", id: invoiceInstance.id)
	}

	def show(Long id) {
		def invoiceInstance = Invoice.get(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
			return
		}

		[invoiceInstance: invoiceInstance]
	}

	def edit(Long id) {
		def invoiceInstance = Invoice.get(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
			return
		}

		[invoiceInstance: invoiceInstance]
	}

	def update(Long id, Long version) {
		def invoiceInstance = Invoice.get(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (invoiceInstance.version > version) {
				
				invoiceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'invoice.label', default: 'Invoice')] as Object[],
				"Another user has updated this Invoice while you were editing")
				render(view: "edit", model: [invoiceInstance: invoiceInstance])
				return
			}
		}

        params.lastUpdated = datatablesUtilService?.syncTime()
		invoiceInstance.properties = params

		if (!invoiceInstance.save(flush: true)) {
			render(view: "edit", model: [invoiceInstance: invoiceInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
		redirect(action: "show", id: invoiceInstance.id)
	}

	def delete(Long id) {
		def invoiceInstance = Invoice.get(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
			return
		}

		try {
			invoiceInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {

            log.info(params.idsVendor)
            def jsonArray = JSON.parse(params.idsVendor)

            Vendor vendor
            jsonArray.each {
                vendor = Vendor.findById(new Long(it))
                def results = Invoice.executeQuery(
                        "SELECT a as vendor FROM Invoice a where a.staDel != '1' and a.po.vendor = ? group by a.po.vendor",[vendor]
                )

                results.each {
                    Invoice.executeUpdate(
                        "update Invoice a set a.staDel='1', a.lastUpdProcess='DELETE', a.lastUpdated =?, a.updatedBy=? where a.id=?",[new Date(),org.apache.shiro.SecurityUtils.subject.principal.toString(),it.id]
                    )
                }
            }

            log.info(params.idsInv)
            jsonArray = JSON.parse(params.idsInv)
            jsonArray.each {
                Invoice.executeUpdate(
                    "update Invoice a set a.staDel='1', a.lastUpdProcess='DELETE', a.lastUpdated =?, a.updatedBy=? where a.t166NoInv=?",[new Date(),org.apache.shiro.SecurityUtils.subject.principal.toString(),it]
                )
            }

			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Invoice, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}


    def addParts(){
        def res = [:]
        Invoice invoiceInstance

        try {
            User user = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
            invoiceInstance = invoiceService.addParts(user, params)
            res.message = "Add Parts Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }

//        redirect(action: "edit", id: invoiceInstance.id)
        def ret = [id:invoiceInstance.id]
        render ret as JSON
    }

    def sublist(){
        [id: params.id,idTable:new Date().format("yyyyMMddhhmmss"),vendorId:params.vendorId,search_noInv:params.search_noInv]
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
        render invoiceService.datatablesSubList(params) as JSON
    }

    def subsublist(){
        [invoiceNo: params.t166NoInv,idTable:new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubSubList() {
        params.companyDealer = session?.userCompanyDealer
        render invoiceService.datatablesSubSubList(params) as JSON
    }

}
