package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class VendorController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
	
	def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    private static final String EMAIL_PATTERN = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})/;

    public boolean validasiEmail(String email) {
        email ==~ EMAIL_PATTERN
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Vendor.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_m121ID") {
                ilike("m121ID", "%" + (params."sCriteria_m121ID" as String) + "%")
            }

            if (params."sCriteria_m121Nama") {
                ilike("m121Nama", "%" + (params."sCriteria_m121Nama" as String) + "%")
            }

            if (params."sCriteria_m121staSPLD") {
                ilike("m121staSPLD", "%" + (params."sCriteria_m121staSPLD" as String) + "%")
            }

            if (params."sCriteria_m121Alamat") {
                ilike("m121Alamat", "%" + (params."sCriteria_m121Alamat" as String) + "%")
            }

            if (params."sCriteria_m121AlamatNPWP") {
                ilike("m121AlamatNPWP", "%" + (params."sCriteria_m121AlamatNPWP" as String) + "%")
            }

            if (params."sCriteria_m121Telp") {
                ilike("m121Telp", "%" + (params."sCriteria_m121Telp" as String) + "%")
            }

            if (params."sCriteria_m121NPWP") {
                ilike("m121NPWP", "%" + (params."sCriteria_m121NPWP" as String) + "%")
            }

            if (params."sCriteria_m121eMail") {
                ilike("m121eMail", "%" + (params."sCriteria_m121eMail" as String) + "%")
            }

            if (params."sCriteria_m121ContactPerson") {
                ilike("m121ContactPerson", "%" + (params."sCriteria_m121ContactPerson" as String) + "%")
            }

            if (params."sCriteria_m121TelpCP") {
                ilike("m121TelpCP", "%" + (params."sCriteria_m121TelpCP" as String) + "%")
            }

            if (params."sCriteria_m121NamaBank1") {
                ilike("m121NamaBank1", "%" + (params."sCriteria_m121NamaBank1" as String) + "%")
            }

            if (params."sCriteria_m121NoRekBank1") {
                ilike("m121NoRekBank1", "%" + (params."sCriteria_m121NoRekBank1" as String) + "%")
            }

            if (params."sCriteria_m121AtasNamaBank1") {
                ilike("m121AtasNamaBank1", "%" + (params."sCriteria_m121AtasNamaBank1" as String) + "%")
            }

            if (params."sCriteria_m121NamaBank2") {
                ilike("m121NamaBank2", "%" + (params."sCriteria_m121NamaBank2" as String) + "%")
            }

            if (params."sCriteria_m121NoRekBank2") {
                ilike("m121NoRekBank2", "%" + (params."sCriteria_m121NoRekBank2" as String) + "%")
            }

            if (params."sCriteria_m121AtasNamaBank2") {
                ilike("m121AtasNamaBank2", "%" + (params."sCriteria_m121AtasNamaBank2" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m121ID: it.m121ID,

                    m121Nama: it.m121Nama,

                    m121staSPLD: it.m121staSPLD,

                    m121Alamat: it.m121Alamat,

                    m121AlamatNPWP: it.m121AlamatNPWP,

                    m121Telp: it.m121Telp,

                    m121NPWP: it.m121NPWP,

                    m121eMail: it.m121eMail,

                    m121ContactPerson: it.m121ContactPerson,

                    m121TelpCP: it.m121TelpCP,

                    m121NamaBank1: it.m121NamaBank1,

                    m121NoRekBank1: it.m121NoRekBank1,

                    m121AtasNamaBank1: it.m121AtasNamaBank1,

                    m121NamaBank2: it.m121NamaBank2,

                    m121NoRekBank2: it.m121NoRekBank2,

                    m121AtasNamaBank2: it.m121AtasNamaBank2,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [vendorInstance: new Vendor(params)]
    }

    def save() {
        def vendorInstance = new Vendor(params)
        String email = params.m121eMail
        def checkEmail = validasiEmail(email)

        if(email != ''){
            if(!checkEmail){
                //    flash.message = message(code: 'default.companyDealer.email', args: [message(code: 'companyDealer.email.error', default: 'Masukan email dengan format xxx@xxx.xxx'),''])
                flash.message = 'Input Email dengan format : xxx@xxx.xxx'
                render(view: "create", model: [vendorInstance: vendorInstance])
                return
            }
        }
        vendorInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        vendorInstance?.lastUpdProcess = "INSERT"
        vendorInstance?.dateCreated = datatablesUtilService?.syncTime()
        vendorInstance?.lastUpdated = datatablesUtilService?.syncTime()
        vendorInstance?.setStaDel ('0')
        def cek = Vendor.createCriteria().list() {
            and{
                eq("m121Nama",vendorInstance.m121Nama?.trim(), [ignoreCase: true])
                eq("m121staSPLD",vendorInstance.m121staSPLD)
                eq("m121Alamat",vendorInstance.m121Alamat?.trim(), [ignoreCase: true])
                eq("m121AlamatNPWP",vendorInstance.m121AlamatNPWP?.trim(), [ignoreCase: true])
                eq("m121Telp",vendorInstance.m121Telp?.trim(), [ignoreCase: true])
                eq("m121ContactPerson",vendorInstance.m121ContactPerson?.trim(), [ignoreCase: true])
                eq("m121TelpCP",vendorInstance.m121TelpCP?.trim(), [ignoreCase: true])
                eq("m121eMail",vendorInstance.m121eMail?.trim(), [ignoreCase: true])
                eq("m121NPWP",vendorInstance.m121NPWP?.trim(), [ignoreCase: true])
                eq("m121NamaBank1",vendorInstance.m121NamaBank1?.trim(), [ignoreCase: true])
                eq("m121NoRekBank1",vendorInstance.m121NoRekBank1?.trim(), [ignoreCase: true])
                eq("m121AtasNamaBank1",vendorInstance.m121AtasNamaBank1?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [vendorInstance: vendorInstance])
            return
        }
        vendorInstance?.m121ID = generateCodeService.codeGenerateSequence("M121_ID",session.userCompanyDealer)
        if (!vendorInstance.save(flush: true)) {
            render(view: "create", model: [vendorInstance: vendorInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'vendor.label', default: 'Vendor'), vendorInstance.id])
        redirect(action: "show", id: vendorInstance.id)
    }

    def show(Long id) {
        def vendorInstance = Vendor.get(id)
        if (!vendorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendor.label', default: 'Vendor'), id])
            redirect(action: "list")
            return
        }

        [vendorInstance: vendorInstance]
    }

    def edit(Long id) {
        def vendorInstance = Vendor.get(id)
        if (!vendorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendor.label', default: 'Vendor'), id])
            redirect(action: "list")
            return
        }

        [vendorInstance: vendorInstance]
    }

    def update(Long id, Long version) {
        def vendorInstance = Vendor.get(id)
        params.lastUpdated = datatablesUtilService?.syncTime()
        if (!vendorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendor.label', default: 'Vendor'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (vendorInstance.version > version) {

                vendorInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'vendor.label', default: 'Vendor')] as Object[],
                        "Another user has updated this Vendor while you were editing")
                render(view: "edit", model: [vendorInstance: vendorInstance])
                return
            }
        }

        def cek = Vendor.createCriteria()
        def result = cek.list() {
            and{
                eq("m121Nama",params.m121Nama?.trim(), [ignoreCase: true])
                eq("m121staSPLD",params.m121staSPLD)
                eq("m121Alamat",params.m121Alamat?.trim(), [ignoreCase: true])
                eq("m121AlamatNPWP",params.m121AlamatNPWP?.trim(), [ignoreCase: true])
                eq("m121Telp",params.m121Telp?.trim(), [ignoreCase: true])
                eq("m121ContactPerson",params.m121ContactPerson?.trim(), [ignoreCase: true])
                eq("m121TelpCP",params.m121TelpCP?.trim(), [ignoreCase: true])
                eq("m121eMail",params.m121eMail?.trim(), [ignoreCase: true])
                eq("m121NPWP",params.m121NPWP?.trim(), [ignoreCase: true])
                eq("m121NamaBank1",params.m121NamaBank1?.trim(), [ignoreCase: true])
                eq("m121NoRekBank1",params.m121NoRekBank1?.trim(), [ignoreCase: true])
                eq("m121AtasNamaBank1",params.m121AtasNamaBank1?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [vendorInstance: vendorInstance])
            return
        }

        vendorInstance.properties = params
        vendorInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        vendorInstance?.lastUpdProcess = "UPDATE"

        String email = params.m121eMail
        def checkEmail = validasiEmail(email)

        if(email != ''){
            if(!checkEmail){
                //    flash.message = message(code: 'default.companyDealer.email', args: [message(code: 'companyDealer.email.error', default: 'Masukan email dengan format xxx@xxx.xxx'),''])
                flash.message = 'Input Email dengan format : xxx@xxx.xxx'
                render(view: "create", model: [vendorInstance: vendorInstance])
                return
            }
        }

        if (!vendorInstance.save(flush: true)) {
            render(view: "edit", model: [vendorInstance: vendorInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'vendor.label', default: 'Vendor'), vendorInstance.id])
        redirect(action: "show", id: vendorInstance.id)
    }

    def delete(Long id) {
        def vendorInstance = Vendor.get(id)
        if (!vendorInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendor.label', default: 'Vendor'), id])
            redirect(action: "list")
            return
        }

        try {
            vendorInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            vendorInstance?.lastUpdProcess = "DELETE"
            vendorInstance?.setStaDel('1')
            vendorInstance?.lastUpdated = datatablesUtilService?.syncTime()
            vendorInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'vendor.label', default: 'Vendor'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'vendor.label', default: 'Vendor'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Vendor, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        try {
            datatablesUtilService.updateField(Vendor, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
