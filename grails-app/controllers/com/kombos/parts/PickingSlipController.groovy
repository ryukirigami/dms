package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class PickingSlipController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def pickingSlipService
    def jasperService
    def konversi = new Konversi()
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }


    def sublist() {

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [goods: params.goods, t141ID:params.t141ID, tanggal : params.tanggal, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        params?.companyDealer = session?.userCompanyDealer
        render pickingSlipService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params?.companyDealer = session?.userCompanyDealer
        render pickingSlipService.datatablesSubList(params) as JSON
    }



    def show(Long id) {
        def result = pickingSlipService.show(params)

        if (!result.error)
            return [pickingSlipInstance: result.pickingSlipInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = pickingSlipService.show(params)

        if (!result.error)
            return [pickingSlipInstance: result.pickingSlipInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.userCompanyDealer = session.userCompanyDealer
        def result = pickingSlipService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Picking Slip", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [pickingSlipInstance: result.pickingSlipInstance.attach()])
    }

    def delete() {
        def result = pickingSlipService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Picking Slip", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        params.userCompanyDealer = session.userCompanyDealer
        def hasil= pickingSlipService.massDelete(params)
        render hasil
    }

    def printPickingSlip(){
        def jsonArray = JSON.parse(params.idPickingSlip)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {
            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'pickingSlip.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )
            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("pickingSlip_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def ps = PickingSlip.findByT141ID(id)

        def nopol = ps?.reception?.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + "-" + ps?.reception?.historyCustomerVehicle?.t183NoPolTengah + "-" + ps?.reception?.historyCustomerVehicle?.t183NoPolBelakang
        def pickingSlip = PickingSlip.findByT141IDAndReceptionAndT141TglPicking(ps.t141ID,ps?.reception, ps.t141TglPicking)
        def results = PickingSlipDetail.findAllByPickingSlip(ps)

        results.sort {
            it.goods.m111ID
        }

        int count = 0
        double sumQty = 0
        results.each {
            def data = [:]
            sumQty = sumQty + it.t142Qty1

            count = count + 1
            log.info("nama parttts : "+it.goods?.m111Nama)

            data.put("nomorPickingSlip",id as String)
            data.put("tglPickingSlip", ps?.t141TglPicking? konversi.tanggalIndonesia(ps?.t141TglPicking?.format("dd-MM-yyyy").toString()):konversi.tanggalIndonesia(ps?.dateCreated?.format("dd-MM-yyyy").toString()))
            data.put("noWO", ps.reception.t401NoWO as String)
            data.put("noPolisi", nopol)
            data.put("companyDealer", ps.reception?.companyDealer?.m011NamaWorkshop)
            data.put("kota", ps.reception?.companyDealer?.kabKota?.m002NamaKabKota)

            data.put("noUrut", count as String)
            data.put("kodeGoods",it.goods?.m111ID)
            data.put("namaGoods",it.goods?.m111Nama)
            data.put("lokasi",KlasifikasiGoods.findByGoods(it.goods)?.getLocation()?.m120NamaLocation)
            data.put("qtyPickingSlip",it.t142Qty1 as String)
            data.put("satuan",it.goods.satuan.m118Satuan1)
            data.put("tanggalSekarang", konversi.tanggalIndonesia(new Date().format("dd-MM-yyyy").toString()))
            data.put("teknisi", pickingSlip.t141NamaTeknisi)

            data.put("sumQty",sumQty as String)

            reportData.add(data)
        }

            return reportData

    }
}
