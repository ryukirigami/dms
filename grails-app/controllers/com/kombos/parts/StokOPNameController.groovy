package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class StokOPNameController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def stokOPNameService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList' , 'datatablesListTemp']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit' , 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render stokOPNameService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
        render stokOPNameService.datatablesSubList(params) as JSON
    }

    def sublist() {
        [t132ID: params.t132ID, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def tambah(){
        def result=stokOPNameService.tambah(params)
        render result
    }


    def create() {
        def result = stokOPNameService.create(params)

        if(!result.error)
            return [stokOPNameInstance: result.stokOPNameInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session?.userCompanyDealer
        def result = stokOPNameService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["StokOPName", result.stokOPNameInstance.id])
            redirect(action:'show', id: result.stokOPNameInstance.id)
            return
        }

        render(view:'create', model:[stokOPNameInstance: result.stokOPNameInstance])
    }

    def show(Long id) {
        def result = stokOPNameService.show(params)

        if(!result.error)
            return [ stokOPNameInstance: result.stokOPNameInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = stokOPNameService.show(params)

        if(!result.error)
            return [ stokOPNameInstance: result.stokOPNameInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {

        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = stokOPNameService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["StokOPName", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[stokOPNameInstance: result.stokOPNameInstance.attach()])
    }

    def delete() {
        def result = stokOPNameService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["StokOPName", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
    }

    def massdelete() {
        def hasil=stokOPNameService.massDelete(params)
        render "ok"
    }

    def printKartuStok(){
        def jsonArray = JSON.parse(params.IDPrint)

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            def stokopname = StokOPName.findByT132ID(it)
            if(stokopname!=null){
                def reportData = calculateReportData(it)
                def reportDef
                if(params.fileFormat=="Pdf"){
                    reportDef = new JasperReportDef(name:'KartuStok.jasper',
                            fileFormat:JasperExportFormat.PDF_FORMAT,
                            reportData: reportData
                    )
                }else{
                    reportDef = new JasperReportDef(name:'KartuStok.jasper',
                            fileFormat:JasperExportFormat.XLS_FORMAT,
                            reportData: reportData
                    )
                }

                reportDefList.add(reportDef)
            }
        }


        def file=null

        if(params.fileFormat=="Pdf"){
            file = File.createTempFile("KartuStok",".pdf")
        }else{
            file = File.createTempFile("KartuStok",".xls")
        }

        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        if(params.fileFormat=="Pdf"){
            response.setHeader("Content-Type", "application/pdf")
        }else{
            response.setHeader("Content-Type", "application/xls")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")

        response.outputStream << file.newInputStream()

    }

    def calculateReportData(id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def results = StokOPName.findAllByT132ID(id)
        results.sort {
            it.goods.goods.m111ID
        }

        int count = 0

        results.each {
            def data = [:]

            count = count + 1
            data.put("no", count)
            data.put("noStokOpname", it?.t132ID)
            data.put("tanggal", it?.t132Tanggal.format(dateFormat))
            data.put("franchise", it?.goods?.franc?.m117NamaFranc)
            data.put("kodeGoods", it?.goods.goods.m111ID)
            data.put("namaGoods", it.goods?.goods?.m111Nama)
            data.put("kodeLokasi",it.goods?.location?.m120ID)
            data.put("icc",it.goods?.location?.parameterICC?.m155NamaICC )
            data.put("qty11",it.t132Jumlah11 )
            data.put("qty12",it.t132Jumlah21 )
            data.put("qty21",it.t132Jumlah12 )
            data.put("qty22",it.t132Jumlah22 )
            data.put("qty31",it.t132Jumlah13 )
            data.put("qty32",it.t132Jumlah23 )
            reportData.add(data)
        }

        return reportData

    }

}
