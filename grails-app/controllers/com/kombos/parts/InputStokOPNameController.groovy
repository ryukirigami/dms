package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class InputStokOPNameController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def inputStokOPNameService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def generateCodeService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy")
        String staUbah = "create"
        def cari = new StokOPName()
        if(params.idUbah!=null){
            staUbah="edit"
            cari = StokOPName.findByT132ID(params.idUbah)
        }
        [status : staUbah, idUbah : params.idUbah, tanggalUbah : cari?.t132Tanggal]
    }

    def save() {
        String nomorStockOpname = generateCodeService.codeGenerateSequence("T132_ID",session.userCompanyDealer)
        params?.companyDealer = session?.userCompanyDealer
        def result = inputStokOPNameService.save(params,nomorStockOpname)
        render result
    }

    def update(){
        params.companyDealer = session?.userCompanyDealer
        def result = inputStokOPNameService.update(params)
        render result
    }


    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(StokOPName, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(Request, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def getTableData(){
        def result = []
        def hasil = StokOPName.createCriteria().list() {
            eq("companyDealer",session?.userCompanyDealer)
            eq("t132ID",params.id)
        }
        hasil.each {
            result << [
                id : it.goods.id,
                kode : it.goods.goods.m111ID,
                nama : it.goods.goods.m111Nama,
                lokasi : it.goods.location.m120NamaLocation
            ]
        }
        render result as JSON
    }

}
