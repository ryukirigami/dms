package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class DpPartsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

   	def datatablesUtilService

    def dpPartsService

   	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

   	static viewPermissions = ['index', 'list', 'datatablesList']

   	static addPermissions = ['create', 'save']

   	static editPermissions = ['edit', 'update']

   	static deletePermissions = ['delete']

   	def index() {
   		redirect(action: "list", params: params)
   	}

   	def list(Integer max) {
   	}

   	def datatablesList() {
        session.exportParams=params
   		render dpPartsService.datatablesList(params) as JSON
   	}

   	def create() {
   		[dpPartsInstance: new DpParts(params)]
   	}

   	def save() {
        params.companyDealer = session.userCompanyDealer

        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
   		def dpPartsInstance = new DpParts(params)
        dpPartsInstance.clearErrors()
        if (dpPartsInstance.m162ID == null) {
            int id = 1
            DpParts dpParts = DpParts.last()
            if (dpParts != null && dpParts.m162ID != null) {
                id = dpParts.m162ID + 1
            }
            dpPartsInstance.m162ID = id
        }
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        Date tglBerlaku = df.parse(params."m162TglBerlaku_dp")
        Date tomorrow = tglBerlaku + 1
        def cek = DpParts.createCriteria().list() {
            and{
                ge("m162TglBerlaku", tglBerlaku)
                lt("m162TglBerlaku", tomorrow)
                eq("m162BatasNilaiKenaDP1", dpPartsInstance.m162BatasNilaiKenaDP1)
                eq("m162BatasNilaiKenaDP2", dpPartsInstance.m162BatasNilaiKenaDP2)
                eq("m162PersenDP", dpPartsInstance.m162PersenDP)
                eq("m162MaxHariBayarDP", dpPartsInstance.m162MaxHariBayarDP)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [dpPartsInstance: dpPartsInstance])
            return
        }

        if (dpPartsInstance.m162BatasNilaiKenaDP2 <= dpPartsInstance.m162BatasNilaiKenaDP1) {
            flash.message = "Batas Nilai Kena DP Akhir Harus Lebih Besar Daripada Nilai Awal"
            render(view: "create", model: [dpPartsInstance: dpPartsInstance])
            return
        }
        dpPartsInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        dpPartsInstance.setLastUpdProcess("INSERT")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
        dpPartsInstance.setStaDel("0") // untuk data awal di set 0 karena bukan data yang dihapus
        if (!dpPartsInstance.save(flush: true)) {
   			render(view: "create", model: [dpPartsInstance: dpPartsInstance])
   			return
   		}

   		flash.message = message(code: 'default.created.message', args: [message(code: 'dpParts.label', default: 'DpParts'), dpPartsInstance.id])
   		redirect(action: "show", id: dpPartsInstance.id)
   	}

   	def show(Long id) {
   		def dpPartsInstance = DpParts.get(id)
   		if (!dpPartsInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dpParts.label', default: 'DpParts'), id])
   			redirect(action: "list")
   			return
   		}

   		[dpPartsInstance: dpPartsInstance]
   	}

   	def edit(Long id) {
   		def dpPartsInstance = DpParts.get(id)
   		if (!dpPartsInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dpParts.label', default: 'DpParts'), id])
   			redirect(action: "list")
   			return
   		}

   		[dpPartsInstance: dpPartsInstance]
   	}

   	def update(Long id, Long version) {
   		def dpPartsInstance = DpParts.get(id)
   		if (!dpPartsInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dpParts.label', default: 'DpParts'), id])
   			redirect(action: "list")
   			return
   		}

   		if (version != null) {
   			if (dpPartsInstance.version > version) {

   				dpPartsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
   				[message(code: 'dpParts.label', default: 'DpParts')] as Object[],
   				"Another user has updated this Religion while you were editing")
   				render(view: "edit", model: [dpPartsInstance: dpPartsInstance])
   				return
   			}
   		}
        params.lastUpdated = datatablesUtilService?.syncTime()
   		dpPartsInstance.properties = params
        dpPartsInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        dpPartsInstance.setLastUpdProcess("UPDATE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        Date tglBerlaku = df.parse(params."m162TglBerlaku_dp")
        Date tomorrow = tglBerlaku + 1
        def cek = DpParts.createCriteria().list() {
            and{
                ge("m162TglBerlaku", tglBerlaku)
                lt("m162TglBerlaku", tomorrow)
                eq("m162BatasNilaiKenaDP1", dpPartsInstance.m162BatasNilaiKenaDP1)
                eq("m162BatasNilaiKenaDP2", dpPartsInstance.m162BatasNilaiKenaDP2)
                eq("m162PersenDP", dpPartsInstance.m162PersenDP)
                eq("m162MaxHariBayarDP", dpPartsInstance.m162MaxHariBayarDP)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [dpPartsInstance: dpPartsInstance])
            return
        }

        if (dpPartsInstance.m162BatasNilaiKenaDP2 <= dpPartsInstance.m162BatasNilaiKenaDP1) {
            flash.message = "Batas Nilai Kena DP Akhir Harus Lebih Besar Daripada Nilai Awal"
            render(view: "edit", model: [dpPartsInstance: dpPartsInstance])
            return
        }
        if (!dpPartsInstance.save(flush: true)) {
   			render(view: "edit", model: [dpPartsInstance: dpPartsInstance])
   			return
   		}

   		flash.message = message(code: 'default.updated.message', args: [message(code: 'dpParts.label', default: 'DpParts'), dpPartsInstance.id])
   		redirect(action: "show", id: dpPartsInstance.id)
   	}

   	def delete(Long id) {
   		def dpPartsInstance = DpParts.get(id)
   		if (!dpPartsInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'dpParts.label', default: 'DpParts'), id])
   			redirect(action: "list")
   			return
   		}

   		try {
               dpPartsInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
               dpPartsInstance.setLastUpdProcess("DELETE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
               dpPartsInstance.setStaDel("1") // untuk data awal di set 0 karena bukan data yang dihapus
               dpPartsInstance.lastUpdated(datatablesUtilService.syncTime())
               dpPartsInstance.save(flush: true)

               flash.message = message(code: 'default.deleted.message', args: [message(code: 'dpParts.label', default: 'DpParts'), id])
   			redirect(action: "list")
   		}
   		catch (DataIntegrityViolationException e) {
   			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'dpParts.label', default: 'DpParts'), id])
   			redirect(action: "show", id: id)
   		}
   	}

   	def massdelete() {
   		def res = [:]
   		try {
   			datatablesUtilService.massDeleteStaDelNew(DpParts, params)
               // ada 2 function yang bisa digunakan
               // yaitu massDelete dan massDeleteStaDelNew
               // massDelete khusus untuk domain2 yang tidak ada field staDel
               // massDeleteStaDelNew khusus untuk domain2 yang ada field staDel
   			res.message = "Mass Delete Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}

   	def updatefield(){
   		def res = [:]
   		try {
   			datatablesUtilService.updateField(DpParts, params)
   			res.message = "Update Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}

}
