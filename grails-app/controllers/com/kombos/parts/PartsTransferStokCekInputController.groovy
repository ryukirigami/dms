package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartsTransferStokCekInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
    def generateCodeService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def generateNomorPengiriman(){
        def result = [:]
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = PartsTransferStok.createCriteria()
        def results = c.list {
            ge("dateCreated",df.parse(currentDate))
            lt("dateCreated",df.parse(currentDate) + 1)
            eq("companyDealerTujuan",session.userCompanyDealer)
        }

        def m = results.size()?:0
        def reg = MappingCompanyRegion.findByCompanyDealer(session.userCompanyDealer).companyDealer?.m011ID
        def data = new Date().format("yyyyMMdd").toString() + "-" + reg +"-" + String.format('PTS%05d',m+1)
//        result.value = String.format('%014d',m+1)
        result.value = data
        return result
    }

    def index() {

        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        String staUbah = "create"
        def cari = new PartsTransferStok()
        def u_tanggal = ""
        def data = PartsTransferStok.findByNomorPO(params.nomorPO)
        def u_companyDealer = data.companyDealer?.m011NamaWorkshop
        if(params.noPartsTransferStok!=null){
            staUbah="ubah"
            cari = PartsTransferStok.findByNomorPO(params.noPartsTransferStok)
            u_tanggal = cari?.tglTransferStok
            u_companyDealer = cari?.companyDealerTujuan.m011NamaWorkshop?cari?.companyDealerTujuan.m011NamaWorkshop:""
        }
        [noPartsTransferStok: params.noPartsTransferStok, u_tanggal:u_tanggal, tanggal : new Date().format("yyyy-MM-dd"), aksi:staUbah, u_companyDealer  : u_companyDealer,nomorPO : params.nomorPO ]
    }

    def req(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
        String tanggal =  params.tanggal
        def hasil = null
        def noPo = null
        def companyDealer =  CompanyDealer.findByM011NamaWorkshop(params.namaCompanyDealer)
        if(companyDealer){
            def jsonArray = JSON.parse(params.ids)
            def jsonArrayNocek = JSON.parse(params.idss)
            jsonArrayNocek.each{
                PartsTransferStok partsTransferStok = PartsTransferStok.get(it)
                if(partsTransferStok){
                    partsTransferStok?.cekBaca = '1'
                    partsTransferStok?.staSudahInput = '0'
                    partsTransferStok?.lastUpdated = datatablesUtilService?.syncTime()
                    partsTransferStok.save(flush: true)
                }
            }
            jsonArray.each {
                PartsTransferStok partsTransferStok = PartsTransferStok.get(it)
                noPo = partsTransferStok.nomorPO
                if(partsTransferStok){
                    partsTransferStok?.cekKetersediaan = '1'
                    partsTransferStok?.noPengiriman = generateNomorPengiriman().value
                    partsTransferStok?.lastUpdated = datatablesUtilService?.syncTime()
                    partsTransferStok.save(flush: true)
                    try {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(partsTransferStok.goods,'0',session.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(partsTransferStok.goods,session.userCompanyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(partsTransferStok.goods,'0',session.userCompanyDealer)
                        }
                        def kurangStok2 =  PartsStok.get(goodsStok.id)
                        kurangStok2?.companyDealer = session.userCompanyDealer
                        kurangStok2?.t131Tanggal = new Date()
                        kurangStok2?.goods = partsTransferStok.goods
                        kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - partsTransferStok?.qtyParts
                        kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - partsTransferStok?.qtyParts
                        kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - partsTransferStok?.qtyParts
                        kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - partsTransferStok?.qtyParts
                        kurangStok2.staDel = '0'
                        kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        kurangStok2.lastUpdProcess = 'TRANSFER'
                        kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                        kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                        kurangStok2?.save(flush: true)
                        kurangStok2.errors.each {
                            println it
                        }
                    }catch (Exception ex){
                        println "tidak ada data"
                    }
                }
            }
                hasil = noPo
        }else {
            hasil = "fail"
        }
        render hasil
    }
    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def hasil = PartsTransferStok.createCriteria().list() {
//            eq("companyDealer",CompanyDealer.findByM011NamaWorkshop(params.companyDealer))
             eq("companyDealerTujuan",session.userCompanyDealer)
             eq("nomorPO",params.nomorPO)
        }
        def ret
        def result = []
        hasil.each {
            def status = "Tidak Tersedia"
            def jmlhStokGudang = PartsStok.findByCompanyDealerAndGoodsAndStaDel(session.userCompanyDealer,it?.goods,'0')?.t131Qty1
            if(jmlhStokGudang >= it.qtyParts){
                status = "Tersedia ("+ jmlhStokGudang+")"
            }else{
                status = "Tidak Tersedia ("+ jmlhStokGudang+")"
            }
            result << [

                    id: it.id,

                    kode: it.goods.m111ID,

                    nama: it.goods.m111Nama,

                    nomorPO : it.nomorPO,

                    tglTransferStok : it.tglTransferStok? it.tglTransferStok.format(dateFormat) : "",

                    Qty : it.qtyParts,

                    stokDigudang: status,

                    hargaBeli: GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session.userCompanyDealer)?.t150Harga : 0,

            ]

        }
        ret = [sEcho: params.sEcho, iTotalRecords: hasil.size(), iTotalDisplayRecords: hasil.size(), aaData: result]
        render ret as JSON
    }

}