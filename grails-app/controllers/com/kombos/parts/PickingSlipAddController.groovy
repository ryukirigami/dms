package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON

class PickingSlipAddController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def wo = null

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
      [idWO : params.idWO,idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PartsRCP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            reception{
                eq("companyDealer",session?.userCompanyDealer)
            }
            if (params.sCriteria_goods){
                goods{
                    ilike("m111ID","%"+ params.sCriteria_goods +"%")
                }
            }
            if (params.sCriteria_goods2){
                goods{
                    ilike("m111Nama","%"+ params.sCriteria_goods2 +"%")
                }
            }
            or{
                eq("t403StaTambahKurang","0")
                isNull("t403StaTambahKurang")
            }
            eq("staDel",'0')
            reception{
                eq("t401NoWO",params.idWO as String)
            }
            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
            switch (sortProperty) {
                case "goods" :
                    goods{
                        order("m111ID", sortDir)
                    }
                    break;
                case "goods2" :
                    goods{
                        order("m111Nama", sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def nos = 0
        def qtySdhDiambil = 0, qtySisa = 0
        results.each { rcp ->
            qtySdhDiambil = 0
            PickingSlip.findAllByReceptionAndStaDel(Reception.findByT401NoWO(params.idWO),"0").each {
                qtySdhDiambil += PickingSlipDetail.findByGoodsAndPickingSlipAndStaDel(rcp.goods,it,'0')?PickingSlipDetail.findByGoodsAndPickingSlipAndStaDel(rcp.goods,it,'0').t142Qty1:0
            }
            def hargaBeli = 0
            def hargaJual = 0
            try{
                hargaJual = rcp.t403HargaRp
                hargaBeli = (GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(rcp.goods,"0",session?.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(rcp.goods,"0",session?.userCompanyDealer)?.t150Harga : 0 )
            }catch (Exception e){}

            def stokR =0
            try {
                stokR = PartsStok.findByGoodsAndCompanyDealerAndStaDel(rcp.goods,session.userCompanyDealer,'0')?.t131Qty1
            }catch (Exception e){}
            
            def klas = ""
            try {
                klas = KlasifikasiGoods.findByGoods(rcp.goods)?.franc?.m117NamaFranc
            }catch (Exception e){}

            nos = nos + 1
            qtySdhDiambil = qtySdhDiambil ? qtySdhDiambil : 0
            qtySisa =  rcp?.t403Jumlah1 - qtySdhDiambil
            rows << [

                    id: rcp.id,

                    norut:nos,

                    goods: rcp.goods.m111ID + " / " + klas,

                    goods2: rcp.goods.m111Nama,

                    satuan: rcp.goods.satuan.m118Satuan1,

                    hargaJual : hargaJual,

                    hargaBeli : hargaBeli ,

                    qtyHrsDiambil : rcp?.t403Jumlah1,

                    qtySdhDiAmbil : qtySdhDiambil,

                    t142Qty1    : qtySisa,

                    stockGudang : stokR
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
}
