package com.kombos.parts

import com.kombos.administrasi.GeneralParameter
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.sun.mail.handlers.multipart_mixed
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class SendPOController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def sendPOService

    def jasperService

    def mailService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        params.companyDealer = session?.userCompanyDealer
        render sendPOService.datatablesList(params) as JSON
    }


    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
//        Date tglId = Date.parse(dateFormat,params.tanggalRequest)

        [ idTable: new Date().format("yyyyMMddhhmmss"), nomorPO: params.t164NoPO]
    }
    def subsublist() {
        [goods: params.kodePart, idTable: new Date().format("yyyyMMddhhmmss"), nomorPO: params.nomorPO]
    }

    def datatablesSubList() {
        render sendPOService.datatablesSubList(params) as JSON
    }


    def create() {
        def result = SendPOService.create(params)

        if(!result.error)
            return [POInstance: result.POInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = SendPOService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PO", result.POInstance.id])
            redirect(action:'show', id: result.POInstance.id)
            return
        }

        render(view:'create', model:[POInstance: result.POInstance])
    }

    def show(Long id) {
        def result = SendPOService.show(params)

        if(!result.error)
            return [ POInstance: result.POInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = SendPOService.show(params)

        if(!result.error)
            return [ POInstance: result.POInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = SendPOService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PO", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[POInstance: result.POInstance.attach()])
    }

    def delete() {
        def result = SendPOService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PO", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PO, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

   

    def sendPOSPLD(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def idPO = params.idPO

        PO po = PO.findByT164NoPO(idPO)


        def reportData = calculateReportData(po)

        def parameters = [nomorPO: po?.t164NoPO,tanggalPO: po?.t164TglPO.format(dateFormat)]
        parameters.tipeOrder = PartTipeOrder.findByVendor(po.vendor)?.getM112TipeOrder()
        parameters.namaVendor = po.vendor.m121Nama
        parameters.alamatVendor = po.vendor.m121Alamat
        parameters.tanggalBuat = new Date().format(dateFormat)


        def reportDef = new JasperReportDef(name:'sendPO.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData,
                parameters: parameters
        )

        def file = File.createTempFile("po_",".pdf")
    //    file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        byte[] bytes = jasperService.generateReport(reportDef).toByteArray()

        def emailTujuan = po.vendor.m121eMail;
        def subject = "PO : "+po.t164NoPO


        def companyDealerId =  session.userCompanyDealer.id
        def generalParameter = GeneralParameter.findById(companyDealerId)
        def username = generalParameter.m000UserName
        def password = generalParameter.m000Password
        def port = generalParameter.m000Port
        def messageBody = generalParameter.m000FormatEmail
        def attachment = file

        GroovyEmailer emailer = new GroovyEmailer(username,password,"smtp.gmail.com")
        emailer.sendMail(emailTujuan,subject,messageBody,file)

        po.staKirimPrint = "1"
        po.save(flush:true)


        render "ok"

    }

    def exportPO(){
    //  def result = sendPOService.exportPO(params)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def jsonArray = JSON.parse(params.idPOs)
        def poList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            poList << PO.findByT164NoPO(it)
        }

        poList.each {

            def reportData = calculateReportData(it)

            def parameters = [nomorPO: it?.t164NoPO,tanggalPO: it?.t164TglPO.format(dateFormat)]
            parameters.tipeOrder = PartTipeOrder.findByVendor(it.vendor)?.getM112TipeOrder()
            parameters.namaVendor = it.vendor.m121Nama
            parameters.alamatVendor = it.vendor.m121Alamat
            parameters.tanggalBuat = new Date().format(dateFormat)


            def reportDef = new JasperReportDef(name:'sendPO.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData,
                    parameters: parameters
            )

            reportDefList.add(reportDef)

      }

        def file = File.createTempFile("po_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())


        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }

    def printPO(){
        //  def result = sendPOService.exportPO(params)
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def jsonArray = JSON.parse(params.idPOs)
        def poList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            poList << PO.findByT164NoPO(it)
        }

        poList.each {

            def reportData = calculateReportData(it)

            def parameters = [nomorPO: it?.t164NoPO,tanggalPO: it?.t164TglPO.format(dateFormat)]
            parameters.tipeOrder = PartTipeOrder.findByVendor(it.vendor)?.getM112TipeOrder()
            parameters.namaVendor = it.vendor.m121Nama
            parameters.alamatVendor = it.vendor.m121Alamat
            parameters.tanggalBuat = new Date().format(dateFormat)
            parameters.tanggalBuat = new Date().format(dateFormat)
            parameters.IS_USING_IMAGES_TO_ALIGN = false
            parameters.HTML_HEADER = "<script>window.onload = function () { window.print() }</script>"

            def reportDef = new JasperReportDef(name:'sendPO.jasper',
                    fileFormat:JasperExportFormat.HTML_FORMAT,
                    reportData: reportData,
                    parameters: parameters
            )

            it.staKirimPrint = "1"
            it.save(flush:true)

            reportDefList.add(reportDef)
        }


            def file = File.createTempFile("po_",".html")
            file.deleteOnExit()

            //  def reportDef = result.reportDef
           FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
           render  response.outputStream << file.newInputStream()

    }



    def calculateReportData(def po){

        def reportData = new ArrayList();
        def grandTotal = 0.0
        def hargaTotal = 0.0
        def hargaSatuan = 0.0
        def qty = 0

        def results = PODetail.findAllByPo(po)
        results.sort {
            it.validasiOrder.requestDetail.goods.m111ID
        }

        int count = 0

        results.each {
            def data = [:]

            hargaSatuan = GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.validasiOrder.requestDetail.goods,"0",session?.userCompanyDealer)? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.validasiOrder.requestDetail.goods,"0",session?.userCompanyDealer).t150Harga : 0
            qty = it.t164Qty1
            hargaTotal = hargaSatuan * qty


            count = count + 1
            //    log.info("nama parts : "+it.goods?.m111Nama)
            data.put("noUrut", count)
            data.put("kodePart", it?.validasiOrder?.requestDetail?.goods?.m111ID)
            data.put("namaPart", it?.validasiOrder?.requestDetail?.goods?.m111Nama)
            data.put("satuan",it?.validasiOrder?.requestDetail?.goods?.satuan?.m118Satuan1)
            data.put("qty",qty ? qty:0)
            data.put("hargaSatuan", hargaSatuan ? hargaSatuan:0 )
            data.put("totalHarga", hargaTotal ? hargaTotal:0)
            grandTotal = grandTotal + hargaTotal

            data.put("grandTotal",grandTotal)
            reportData.add(data)
        }

        return reportData

    }

}
