package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class ClaimController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def claimService


    def index() {

        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }


    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [vendor: params.vendor, t171ID : params.t171ID, t171TglJamClaim : params.t171TglJamClaim, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def subsublist() {

        [t167NoReff: params.t167NoReff, t171ID : params.t171ID , t171TglJamClaim : params.t171TglJamClaim, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        params?.companyDealer = session?.userCompanyDealer
        render claimService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params?.companyDealer = session?.userCompanyDealer
        render claimService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {
        params?.companyDealer = session?.userCompanyDealer
        render claimService.datatablesSubSubList(params) as JSON
    }
    def create() {
        def result = claimService.create(params)

        if (!result.error)
            return [claimInstance: result.claimInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session?.companyDealer
        def result = claimService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Claim", result.claimInstance.id])
            redirect(action: 'show', id: result.claimInstance.id)
            return
        }

        render(view: 'create', model: [claimInstance: result.claimInstance])
    }

    def show(Long id) {
        def result = claimService.show(params)

        if (!result.error)
            return [claimInstance: result.claimInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = claimService.show(params)

        if (!result.error)
            return [claimInstance: result.claimInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session?.userCompanyDealer
        def result = claimService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Claim", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [claimInstance: result.claimInstance.attach()])
    }

    def delete() {
        def result = claimService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Claim", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def hasil=claimService.massDelete(params)
        render "ok"
    }

    def printClaim(){
        def jsonArray = JSON.parse(params.idClaim)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'claim.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )

            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("claim_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();

//        def results = Claim.findAllById(id as Long)
        def results = Claim.findAllByT171ID(id)
        results.sort {
            it.goods.m111ID
        }

        int count = 0
        double sumQtyIssued = 0,sumQtyRusak = 0,sumQtySalah = 0,sumQtyReff = 0
        results.each {
            def data = [:]
            sumQtyIssued= sumQtyIssued + it.t171Qty1Issued
            sumQtyRusak= sumQtyRusak + it.t171Qty1Rusak
            sumQtySalah= sumQtySalah + it.t171Qty1Salah
            sumQtyReff= sumQtyReff + it.t171Qty1Reff

            count = count + 1

            data.put("nomorClaim",it.t171ID)
            data.put("tglClaim", it.t171TglJamClaim.format(dateFormat))
            data.put("vendor", it.vendor.m121Nama)
            data.put("petugasClaim", it.t171PetugasClaim)
            data.put("alamatVendor",  it.vendor.m121Alamat)

            data.put("noUrut", count as String)
            data.put("kodeGoods",it.goods?.m111ID)
            data.put("namaGoods",it.goods?.m111Nama)
            data.put("nomorPO",GoodsReceiveDetail.findByGoodsAndInvoice(it.goods,it.invoice)?.poDetail.po?.t164NoPO)
            data.put("tglPO",it.invoice.po.t164TglPO.format(dateFormat))
            data.put("qtyIssued",it.t171Qty1Issued as String)
            data.put("qtyRusak",it.t171Qty1Rusak as String)
            data.put("qtySalah",it.t171Qty1Salah as String)
            data.put("qtyReff",it.t171Qty1Reff as String)
            data.put("satuan",it.goods.satuan.m118Satuan1)
            data.put("ket",it.t171Keterangan)

            data.put("sumQtyIssued",sumQtyIssued as String)
            data.put("sumQtyRusak",sumQtyRusak as String)
            data.put("sumQtySalah",sumQtySalah as String)
            data.put("sumQtyReff",sumQtyReff as String)

            reportData.add(data)
        }

        return reportData

    }
}
