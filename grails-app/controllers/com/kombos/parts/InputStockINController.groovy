package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.FA
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class InputStockINController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
    def stockINService
    def generateCodeService
    def datatablesUtilService
    
        def index() {
            def stockIN = StockIN.read(params.id);
            if(params.id && stockIN){

            } else {
                session?.selectedStockIN = null;
                stockIN = new StockIN();
                stockIN?.t169ID = generateCodeService.codeGenerateSequence("T169_ID",session.userCompanyDealer)
            }
              def tgl = datatablesUtilService.syncTime()
            [stockINInstance: stockIN,tgl:tgl]
        }
    
        def searchAndAddParts() {
    
        }

        def searchPartsDatatablesList() {
            render stockINService.datatablesListInputAddParts(params) as JSON
    
        }
    
    def save() {
        def hasil = [:]
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each {
            if (params.stockIN_id ) {
                oList << StockINDetail.get(it?.toString())
            } else {
                oList << BinningDetail.get(it?.toString())
            }
        }
        def backToHome = false
        oList.each {
            BinningDetail bd = BinningDetail.findById(Long.valueOf("" + it.id))
            if(StockINDetail.findByBinningDetail(bd)){
                hasil.eror = "eror"
                backToHome = true
            }
        }
        if(backToHome == true){
            render hasil
        }

        DateFormat dfSimple = new SimpleDateFormat("dd-MM-yyyy HH:mm")
        def jam = datatablesUtilService?.syncTime().getHours()
        def menit = datatablesUtilService?.syncTime().getMinutes()
        Date input_rcvDate = dfSimple.parse(params.input_rcvDate +" "+ jam +":"+menit)

        String qStockIN = params.qStockIN
        String rakRusak = params.rakRusak
        String[] qStockINs = qStockIN.split("&")
        String[] rakRusaks = rakRusak.split("&")

        StockIN stockIN

        if (params.stockIN_id ) {
            stockIN = StockIN.findById(params.stockIN_id)
            stockIN.lastUpdProcess = "UPDATE"
        } else {
            stockIN = new StockIN()
            stockIN?.t169ID = generateCodeService.codeGenerateSequence("T169_ID",session.userCompanyDealer)

            stockIN?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stockIN?.lastUpdProcess = "INSERT"
            stockIN?.staDel= '0'
            stockIN?.dateCreated = datatablesUtilService?.syncTime()
            stockIN?.companyDealer = session?.userCompanyDealer
        }
        stockIN?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        stockIN?.t169PetugasStockIn = org.apache.shiro.SecurityUtils.subject.principal.toString()

        stockIN?.t169TglJamStockIn = input_rcvDate
        stockIN?.lastUpdated = datatablesUtilService?.syncTime()
        def simpan = false
        oList.each {
            BinningDetail bd
            bd = BinningDetail.findById(Long.valueOf("" + it.id))
            if(!StockINDetail.findByBinningDetail(bd)){
                simpan = true
            }
        }
        if( simpan == true){
            stockIN.save(flush: true)
        }
        int i = 1;

        oList.each {
            String[] parsed = qStockINs[i].split("=")
            String[] rakRusakParsed = rakRusaks[i].split("=")
            def quantity1 = 0.0
            def isiRakRusak = ""
            if (parsed[1] != null && !parsed[1].equals(""))
                quantity1 = Double.valueOf(parsed[1] as String)

            if (rakRusakParsed[1] != null && !rakRusakParsed[1].equals(""))
                isiRakRusak = rakRusakParsed[1]

            def lokasiRakRusak = Location.findById(isiRakRusak as Long)
            StockINDetail stockINDetail = new StockINDetail()
            BinningDetail bd
            if (params.stockIN_id) {
                stockINDetail = StockINDetail.findById("" + it.id)
            } else {
                stockINDetail.stockIN = stockIN
                bd = BinningDetail.findById(Long.valueOf("" + it.id))
                stockINDetail.binningDetail = bd
                bd.staStockIn = '1'
                bd.lastUpdated =  datatablesUtilService?.syncTime()
                bd.save(flush: true)
                stockINDetail?.dateCreated = datatablesUtilService?.syncTime()
            }
            stockINDetail.t169Qty1StockIn = quantity1
            stockINDetail.t169Qty2StockIn = quantity1
            stockINDetail.location = KlasifikasiGoods.findByGoods(bd?.goodsReceiveDetail?.goods)?.location
            if(lokasiRakRusak){
                println stockINDetail.locationRusak = lokasiRakRusak
            }
            stockINDetail.t169xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stockINDetail.t169xNamaDivisi = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stockINDetail?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stockINDetail?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            stockINDetail?.lastUpdProcess = "INSERT"
            stockINDetail?.lastUpdated = datatablesUtilService?.syncTime()
            stockINDetail?.staDel= '0'
            if(!StockINDetail.findByBinningDetail(bd)){
                stockINDetail.save(flush: true)
                try {
                    def podd = bd.goodsReceiveDetail.invoice.po.id
                    def pod = PO.findById(podd as Long)
                    pod.t164StaOpenCancelClose = POStatus.CLOSE
                    pod.save(flush:true)

                    if(!PartsStok.findByGoodsAndStaDelAndCompanyDealer(bd?.goodsReceiveDetail?.goods,"0",session?.userCompanyDealer)){
                        def tambahStok =  new PartsStok()
                        tambahStok?.companyDealer = session.userCompanyDealer
                        tambahStok?.t131Tanggal = new Date()
                        tambahStok?.goods = bd?.goodsReceiveDetail?.goods
                        tambahStok?.t131Qty1Free = quantity1
                        tambahStok?.t131Qty2Free = quantity1
                        tambahStok?.t131Qty1Reserved = 0
                        tambahStok?.t131Qty2Reserved = 0
                        tambahStok?.t131Qty1WIP = 0
                        tambahStok?.t131Qty2WIP = 0
                        tambahStok?.t131Qty1BlockStock = 0
                        tambahStok?.t131Qty2BlockStock = 0
                        tambahStok?.t131LandedCost = 0
                        tambahStok?.t131Qty1 = quantity1
                        tambahStok?.t131Qty2 = quantity1
                        tambahStok.staDel = '0'
                        tambahStok.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        tambahStok.lastUpdProcess = 'INSERT'
                        tambahStok.dateCreated = datatablesUtilService?.syncTime()
                        tambahStok.lastUpdated = datatablesUtilService?.syncTime()
                        tambahStok?.save(flush: true)
                        tambahStok.errors.each {
                            println "stok baru-> " + it
                        }
                    }else if(PartsStok.findByGoodsAndStaDelAndCompanyDealer(bd?.goodsReceiveDetail?.goods,"0",session?.userCompanyDealer)){
                        def ubh = PartsStok.findByGoodsAndStaDelAndCompanyDealer(bd?.goodsReceiveDetail?.goods,"0",session?.userCompanyDealer)
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(ubh.goods,'0',session?.userCompanyDealer)
                        if(goodsStok){
                            def tambahStock2 =  PartsStok.get(goodsStok.id)
                            tambahStock2?.companyDealer = tambahStock2?.companyDealer
                            tambahStock2?.t131Tanggal = new Date()
                            tambahStock2?.goods = ubh?.goods
                            tambahStock2?.t131Qty1Free = tambahStock2.t131Qty1Free + quantity1
                            tambahStock2?.t131Qty2Free = tambahStock2.t131Qty2Free + quantity1
                            tambahStock2?.t131Qty1 = tambahStock2?.t131Qty1 + quantity1
                            tambahStock2?.t131Qty2 = tambahStock2?.t131Qty2 + quantity1
                            tambahStock2.staDel = '0'
                            tambahStock2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            tambahStock2.lastUpdProcess = 'STOCKIN'
                            tambahStock2.dateCreated = datatablesUtilService?.syncTime()
                            tambahStock2.lastUpdated = datatablesUtilService?.syncTime()
                            tambahStock2?.save(flush: true)
                            tambahStock2.errors.each {
                                println it
                            }
                        }
                    }
                }catch(Exception exx){}

            }else{
                try {
                    def stokdel = StockIN.findById(stockIN.id)
                    stokdel.delete()
                }catch(Exception exx){}
            }

            i += 1
        }
        //session?.selectedBinning = stockIN
        render "ok"
    }

        def tambahParts() {
            def fa = FA.read(params.fa)
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            jsonArray.each { oList << it }
            session.selectedParts = oList
            render "ok"
        }

    def addModal(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def inputStockINDatatablesList() {
        render stockINService.inputStockINDatatablesList(params) as JSON
    }

    def addModalDatatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params


        def c = BinningDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            binning{
                eq("companyDealer",session?.userCompanyDealer)
            }
            isNull('staStockIn')

            goodsReceiveDetail{
                poDetail{
                    po{
                        eq("t164StaOpenCancelClose",POStatus.OPEN)
                    }
                }
            }
            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
            if(params.sCriteria_kodeParts){
                goodsReceiveDetail{
                    goods{
                        ilike("m111ID","%"+params.sCriteria_kodeParts+"%")
                    }
                }
            }
            if(params.sCriteria_namaParts){
                goodsReceiveDetail{
                    goods{
                        ilike("m111Nama","%"+params.sCriteria_namaParts+"%")
                    }
                }
            }
            if(params.sCriteria_nomorPO){
                goodsReceiveDetail{
                    poDetail{
                        po{
                            ilike("t164NoPO","%"+params.sCriteria_nomorPO+"%")
                        }
                    }
                }
            }
            if(params.sCriteria_tglPO){
                goodsReceiveDetail{
                    poDetail{
                        po{
                            ge("t164TglPO", params.sCriteria_tglPO)
                            lt("t164TglPO", params.sCriteria_tglPO + 1)
                        }
                    }
                }
            }
            if(params.sCriteria_nomorInvoice){
                goodsReceiveDetail{
                    invoice{
                        ilike("t166NoInv","%"+params.sCriteria_nomorInvoice+"%")
                    }
                }
            }
            if(params.sCriteria_tglInvoice){
                goodsReceiveDetail{
                    invoice{
                        ge("t166TglInv", params.sCriteria_tglInvoice)
                        lt("t166TglInv", params.sCriteria_tglInvoice + 1)
                    }
                }
            }

            if(params.sCriteria_nomorBinning){
                binning{
                    ilike("t168ID","%"+params.sCriteria_nomorBinning+"%")
                }
            }
            if(params.sCriteria_tglBinning){
                binning{
                        ge("t168TglJamBinning", params.sCriteria_tglBinning)
                        lt("t168TglJamBinning", params.sCriteria_tglBinning + 1)
                }
            }

            if(params.sCriteria_qReceive){
                goodsReceiveDetail{
                    eq("t167Qty1Issued",params.sCriteria_qReceive as Double)
                }
            }
            if(params.sCriteria_qBinning){
                eq("t168Qty1Binning",params.sCriteria_qBinning as Double)
            }
            if(params.sCriteria_qRusak){
                goodsReceiveDetail{
                    eq("t167Qty1Rusak",params.sCriteria_qRusak as Double)
                }
            }

            if(params.sCriteria_qSalah){
                goodsReceiveDetail{
                    eq("t167Qty1Salah",params.sCriteria_qSalah as Double)
                }
            }

        }

             def rows = []
             def nos = 0
        DateFormat dfSimple = new SimpleDateFormat("dd-MM-yyyy HH:mm")
          results.each {
             nos = nos + 1
             rows << [

                     id: it.id,

                     norut:nos,

                     kodePart: it.goodsReceiveDetail?.goods?.m111ID,

                     namaPart: it.goodsReceiveDetail?.goods?.m111Nama,

                     nomorPO: it.goodsReceiveDetail?.poDetail?.po?.t164NoPO? it.goodsReceiveDetail?.poDetail?.po?.t164NoPO : it.goodsReceiveDetail?.updatedBy,

                     tglPO: it.goodsReceiveDetail?.poDetail?.po?.t164TglPO ? it.goodsReceiveDetail?.poDetail?.po?.t164TglPO.format(dateFormat) :  (PO.findByT164NoPOLike("%"+it.goodsReceiveDetail?.updatedBy+"%")?.t164TglPO ? PO.findByT164NoPOLike("%"+it.goodsReceiveDetail?.updatedBy+"%").t164TglPO.format(dateFormat):""),

                     nomorInvoice: it.goodsReceiveDetail?.invoice?.t166NoInv,

                     tglInvoice: it.goodsReceiveDetail?.invoice?.t166TglInv?it.goodsReceiveDetail?.invoice?.t166TglInv.format(dateFormat):'-',

                     nomorBinning: it.binning?.t168ID,

                     tglJamBinning: dfSimple.format(it.binning?.t168TglJamBinning),

                     qBinning: it?.t168Qty1Binning,

                     qBinning2: it?.t168Qty2Binning,

                     qLokasiRak: KlasifikasiGoods.findByGoods(it.goodsReceiveDetail?.goods)? KlasifikasiGoods.findByGoods(it.goodsReceiveDetail?.goods)?.location?.m120NamaLocation : '-',

                     qRusak: it.goodsReceiveDetail?.t167Qty1Rusak,
                     qReceive: it.goodsReceiveDetail?.t167Qty1Issued,
                     qSalah: it.goodsReceiveDetail?.t167Qty1Salah,
                     qReff1: it.goodsReceiveDetail?.t167Qty1Reff,

                     qRusak2: it.goodsReceiveDetail?.t167Qty2Rusak,
                     qIssued2: it.goodsReceiveDetail?.t167Qty2Issued,
                     qSalah2: it.goodsReceiveDetail?.t167Qty2Salah,
                     qReff2: it.goodsReceiveDetail?.t167Qty2Reff,

             ]
           }

             ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

             render ret as JSON
    }
}
