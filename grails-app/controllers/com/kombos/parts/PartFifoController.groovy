package com.kombos.parts





import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class PartFifoController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def partFifoService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
		[idTable: new Date().format("yyyyMMddhhmmss")]
	}

	def datatablesList() {
		session.exportParams = params
		params?.companyDealer = session?.userCompanyDealer
		def dataTables = partFifoService.datatablesList(params)
		render dataTables as JSON
	}

	def datatablesSubList(){
		def dataTablesSub = partFifoService.datatablesSubList(params)
		render dataTablesSub as JSON
	}

	def create() {
		def result = partFifoService.create(params)

        if(!result.error)
            return [partFifoInstance: result.partFifoInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		 def result = partFifoService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PartFifo", result.partFifoInstance.id])
            redirect(action:'show', id: result.partFifoInstance.id)
            return
        }

        render(view:'create', model:[partFifoInstance: result.partFifoInstance])
	}

	def show(Long id) {
		def result = partFifoService.show(params)

		if(!result.error)
			return [ partFifoInstance: result.partFifoInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = partFifoService.show(params)

		if(!result.error)
			return [ partFifoInstance: result.partFifoInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = partFifoService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PartFifo", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[partFifoInstance: result.partFifoInstance.attach()])
	}

	def delete() {
		def result = partFifoService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PartFifo", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(PartFifo, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

	def listGoods(){
		def res = [:]
		def opts = []
		def vincodes = Goods.createCriteria().list {
			eq("staDel","0")
			or{
				ilike("m111ID","%"+params.word+"%")
				ilike("m111Nama","%"+params.word+"%")
			}
			order("m111ID")
			maxResults(10);
		}
		vincodes.each {
			opts<<it.m111ID.trim()+" | "+it.m111Nama.trim()
		}
		res."options" = opts
		render res as JSON
	}

	def sublist() {
		[idTable: new Date().format("yyyyMMddhhmmss"), fifoId: params.po, kodeGoods: params.kodeGoods]
	}
}
