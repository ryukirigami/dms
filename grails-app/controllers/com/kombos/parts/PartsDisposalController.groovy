package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class PartsDisposalController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def partsDisposalService
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss") ]
    }

    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [id: params.id,idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        params.companyDealer = session?.userCompanyDealer
        render partsDisposalService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        render partsDisposalService.datatablesSubList(params) as JSON
    }


    def create() {
        def result = partsDisposalService.create(params)

        if (!result.error)
            return [partsDisposalInstance: result.partsDisposalInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session?.userCompanyDealer
        def result = partsDisposalService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Returns", result.partsDisposalInstance.id])
            redirect(action: 'show', id: result.partsDisposalInstance.id)
            return
        }

        render(view: 'create', model: [partsDisposalInstance: result.partsDisposalInstance])
    }

    def show(Long id) {
        def result = partsDisposalService.show(params)

        if (!result.error)
            return [partsDisposalInstance: result.partsDisposalInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = partsDisposalService.show(params)

        def reff = GoodsReceiveDetail?.findByInvoice(Returns.findById(id)?.invoice)?.t167Qty1Reff
        def rec = GoodsReceiveDetail?.findByInvoice(Returns.findById(id)?.invoice)?.t167Qty1Issued
        if (!result.error)
            return [partsDisposalInstance: result.partsDisposalInstance,idTable: new Date().format("yyyyMMddhhmmss"), reff : reff, rec : rec]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list', params: [ nama : "coba"])
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = partsDisposalService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Returns", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [partsDisposalInstance: result.partsDisposalInstance.attach()])
    }

    def delete() {
        def result = partsDisposalService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Returns", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def hasil = partsDisposalService.massDelete(params)
    }

}
