package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ParameterICCController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = ParameterICC.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("staDel","0")
			if(params."sCriteria_m155Tanggal"){
				ge("m155Tanggal",params."sCriteria_m155Tanggal")
				lt("m155Tanggal",params."sCriteria_m155Tanggal" + 1)
			}
	
			if(params."sCriteria_m155KodeICC"){
				ilike("m155KodeICC","%" + (params."sCriteria_m155KodeICC" as String) + "%")
			}
	
			if(params."sCriteria_m155NamaICC"){
				ilike("m155NamaICC","%" + (params."sCriteria_m155NamaICC" as String) + "%")
			}

			if(params."sCriteria_m155NilaiDAD"){
                and{
                    le("m155NilaiDAD1",Double.parseDouble(params."sCriteria_m155NilaiDAD"))

                    ge("m155NilaiDAD2",Double.parseDouble(params."sCriteria_m155NilaiDAD"))
                }

//                eq("m155NilaiDAD1",ParameterICC.findByM155NilaiDAD1Between(params.m155NilaiDAD1,params.m155NilaiDAD2))
//                lt("m155nilaiDAD1",params."sCriteria_m155nilaiDAD2" + 1)
			}

//			if(params."sCriteria_m155nilaiDAD2"){
//				eq("m155nilaiDAD2",params."sCriteria_m155nilaiDAD2")
//			}
//
//			if(params."sCriteria_m155ID"){
//				eq("m155ID",params."sCriteria_m155ID")
//			}
//
//			if(params."sCriteria_companyDealer"){
//				eq("companyDealer",params."sCriteria_companyDealer")
//			}
//
//			if(params."sCriteria_staDel"){
//				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
//			}
//
//			if(params."sCriteria_createdBy"){
//				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
//			}
//
//			if(params."sCriteria_updatedBy"){
//				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
//			}
//
//			if(params."sCriteria_lastUpdProcess"){
//				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
//			}


			switch(sortProperty){
                case "m155KodeICC":
                    order("m155KodeICC",sortDir)
                    break;
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m155Tanggal: it.m155Tanggal?it.m155Tanggal.format(dateFormat):"",
			
						m155KodeICC: it.m155KodeICC,
			
						m155NamaICC: it.m155NamaICC,
			
						m155NilaiDAD1: it.m155NilaiDAD1 + " - " + it.m155NilaiDAD2,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[parameterICCInstance: new ParameterICC(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def parameterICCInstance = new ParameterICC(params)
        def ceka = ParameterICC.createCriteria().list() {
            and{
                eq("m155KodeICC",parameterICCInstance.m155KodeICC?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(ceka){
            flash.message = "Kode ICC sudah ada"
            render(view: "create", model: [parameterICCInstance: parameterICCInstance])
            return
        }
        def cekb = ParameterICC.createCriteria().list() {
            and{
                eq("m155NamaICC",parameterICCInstance.m155NamaICC?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cekb){
            flash.message = "Nama ICC sudah ada"
            render(view: "create", model: [parameterICCInstance: parameterICCInstance])
            return
        }

        def cekc = ParameterICC.createCriteria().list() {
            and{
                eq("m155KodeICC",parameterICCInstance.m155KodeICC?.trim(), [ignoreCase: true])
                eq("m155NamaICC",parameterICCInstance.m155NamaICC?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cekc){
            flash.message = "Data sudah ada"
            render(view: "create", model: [parameterICCInstance: parameterICCInstance])
            return
        }
        if(parameterICCInstance.m155NilaiDAD1>=parameterICCInstance.m155NilaiDAD2){
            flash.message = "Nilai DAD kedua harus lebih besar dari nilai DAD pertama"
            render(view: "create", model: [parameterICCInstance: parameterICCInstance])
            return
        }
        def cek2=ParameterICC.findAllByM155NilaiDAD1BetweenOrM155NilaiDAD2Between(Double.parseDouble(params.m155NilaiDAD1),Double.parseDouble(params.m155NilaiDAD2),Double.parseDouble(params.m155NilaiDAD1),Double.parseDouble(params.m155NilaiDAD2))
        if(cek2!=null){
            for(find in cek2){
                if(find.staDel.equalsIgnoreCase('0')){
                    flash.message = '* Range DAD Sudah Ada'
                    render(view: "create", model: [parameterICCInstance: parameterICCInstance])
                    return
                }
            }
        }
        parameterICCInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        parameterICCInstance?.lastUpdProcess = "INSERT"
        parameterICCInstance?.setStaDel ('0')

		if (!parameterICCInstance.save(flush: true)) {
			render(view: "create", model: [parameterICCInstance: parameterICCInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'parameterICC.label', default: 'Parameter ICC'), parameterICCInstance.id])
		redirect(action: "show", id: parameterICCInstance.id)
	}

	def show(Long id) {
		def parameterICCInstance = ParameterICC.get(id)
		if (!parameterICCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'parameterICC.label', default: 'ParameterICC'), id])
			redirect(action: "list")
			return
		}

		[parameterICCInstance: parameterICCInstance]
	}

	def edit(Long id) {
		def parameterICCInstance = ParameterICC.get(id)
		if (!parameterICCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'parameterICC.label', default: 'ParameterICC'), id])
			redirect(action: "list")
			return
		}

		[parameterICCInstance: parameterICCInstance]
	}

	def update(Long id, Long version) {
		def parameterICCInstance = ParameterICC.get(id)
		if (!parameterICCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'parameterICC.label', default: 'ParameterICC'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (parameterICCInstance.version > version) {
				
				parameterICCInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'parameterICC.label', default: 'ParameterICC')] as Object[],
				"Another user has updated this ParameterICC while you were editing")
				render(view: "edit", model: [parameterICCInstance: parameterICCInstance])
				return
			}
		}
        def ceka = ParameterICC.createCriteria()
        def resulta = ceka.list() {
            and{
                eq("m155KodeICC",params.m155KodeICC?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(resulta){
            for(find in resulta){
                if (id!=find.id){
                    flash.message = "Kode ICC sudah ada"
                    render(view: "edit", model: [parameterICCInstance: parameterICCInstance])
                    return
                }
            }

        }
        def cekb = ParameterICC.createCriteria()
        def resultb = cekb.list() {
            and{
                eq("m155NamaICC",params.m155NamaICC?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(resultb){
            for(find in resultb){
                if (id!=find.id){
                    flash.message = "Nama ICC sudah ada"
                    render(view: "edit", model: [parameterICCInstance: parameterICCInstance])
                    return
                }
            }
        }

        def cekc = ParameterICC.createCriteria()
        def resultc = cekc.list() {
            and{
                eq("m155KodeICC",params.m155KodeICC?.trim(), [ignoreCase: true])
                eq("m155NamaICC",params.m155NamaICC?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(resultc){
            for(find in resultc){
                if (id!=find.id){
                    flash.message = "Data sudah ada"
                    render(view: "edit", model: [parameterICCInstance: parameterICCInstance])
                    return
                }
            }
        }

        if(params.m155NilaiDAD1>=params.m155NilaiDAD2){
            flash.message = "Nilai DAD kedua harus lebih besar dari nilai DAD pertama"
            render(view: "create", model: [parameterICCInstance: parameterICCInstance])
            return
        }
        def cek2=ParameterICC.findAllByM155NilaiDAD1BetweenOrM155NilaiDAD2Between(Double.parseDouble(params.m155NilaiDAD1),Double.parseDouble(params.m155NilaiDAD2),Double.parseDouble(params.m155NilaiDAD1),Double.parseDouble(params.m155NilaiDAD2))
        if(cek2!=null){
            for(find in cek2){
                if(find.staDel.equalsIgnoreCase('0')&&find.id!=id){
                    flash.message = '* Range DAD Sudah Ada'
                    render(view: "edit", model: [parameterICCInstance: parameterICCInstance])
                    return
                }
            }
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
		parameterICCInstance.properties = params
        parameterICCInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        parameterICCInstance?.lastUpdProcess = "UPDATE"

		if (!parameterICCInstance.save(flush: true)) {
			render(view: "edit", model: [parameterICCInstance: parameterICCInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'parameterICC.label', default: 'Parameter ICC'), parameterICCInstance.id])
		redirect(action: "show", id: parameterICCInstance.id)
	}

	def delete(Long id) {
		def parameterICCInstance = ParameterICC.get(id)
		if (!parameterICCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'parameterICC.label', default: 'ParameterICC'), id])
			redirect(action: "list")
			return
		}

		try {
            parameterICCInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            parameterICCInstance?.lastUpdProcess = "DELETE"
            parameterICCInstance?.setStaDel('1')
            parameterICCInstance?.lastUpdated = datatablesUtilService?.syncTime()
            parameterICCInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'parameterICC.label', default: 'ParameterICC'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'parameterICC.label', default: 'ParameterICC'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(ParameterICC, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(ParameterICC, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
