package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class ClaimInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def addModal(Integer max) {
        [vendor : params.vendor, idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def list(Integer max) {
        DateFormat df=new SimpleDateFormat("dd/MM/yyyy")
        String staUbah = "create"
        def cari = new Claim()
        if(params.noClaim!=null){
            staUbah="ubah"
            cari = Claim.findByT171ID(params.noClaim)
        }
        [nomorClaim: "", vendor : cari?.vendor?.m121Nama, noClaim : params.noClaim, tglClaim: cari?.t171TglJamClaim, aksi : staUbah]
    }
    def ubahData(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm")
        def vendor =  Vendor.findByM121Nama(params.namaVendor)
        if(vendor){
            def jsonArray = JSON.parse(params.ids)
            def claim = null;
            jsonArray.each {
                def binningDetail = BinningDetail.get(it)
                if(binningDetail){
                    def dataClaim = Claim.findByT171IDAndGoods(params.nomorClaim,binningDetail.goodsReceiveDetail.goods)
                    if(!dataClaim){
                        claim = new Claim()
                        claim?.t171ID = params.nomorClaim
                        claim?.t171TglJamClaim = Claim.findByT171ID(params.nomorClaim).t171TglJamClaim
                        claim?.t171PetugasClaim = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        claim?.vendor = vendor
                        claim?.invoice = binningDetail.goodsReceiveDetail.invoice
                        claim?.goodsReceive = binningDetail.goodsReceiveDetail.goodsReceive
                        claim?.goods = binningDetail.goodsReceiveDetail.goods
                        claim?.t171Qty1Issued = binningDetail.goodsReceiveDetail.t167Qty1Issued
                        claim?.t171Qty2Issued = binningDetail.goodsReceiveDetail.t167Qty2Issued
                        claim?.t171Qty1Reff = binningDetail.goodsReceiveDetail.t167Qty1Reff
                        claim?.t171Qty2Reff = binningDetail.goodsReceiveDetail.t167Qty2Reff
                        claim?.t171Qty1Rusak = binningDetail.goodsReceiveDetail.t167Qty1Rusak
                        claim?.t171Qty2Rusak = binningDetail.goodsReceiveDetail.t167Qty2Rusak
                        claim?.t171Qty1Salah = binningDetail.goodsReceiveDetail.t167Qty1Salah
                        claim?.t171Qty2Salah = binningDetail.goodsReceiveDetail.t167Qty2Salah
                        claim?.t171Keterangan = "ket"
                        claim?.t171xNamaDivisi = "DIVISI"
                        claim?.t171xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        claim?.staDel = '0'
                        claim?.status = '1'

                        claim?.dateCreated = datatablesUtilService.syncTime()
                        claim?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        claim?.lastUpdated = datatablesUtilService.syncTime()
                        claim?.updatedBy  =org.apache.shiro.SecurityUtils.subject.principal.toString()
                        claim?.lastUpdProcess  ="INSERT"
                        claim?.companyDealer  = session?.userCompanyDealer
                        claim.save(flush: true)
                    }
                }
            }
        }else {
            render "noVendor"
        }
        render "ok"
    }
    def req(){
        DateFormat df = new SimpleDateFormat("yyyy-M-d")
        def vendor =  Vendor.findByM121Nama(params.namaVendor)
        def hasil = null
        def t171ID  = null
        if(vendor){
            def jsonArray = JSON.parse(params.ids)
            if(params.noClaimBefore==null || params.noClaimBefore==""){
                t171ID = generateCodeService.codeGenerateSequence("T171_ID",session.userCompanyDealer)
            }
            def claim = null;

            jsonArray.each {
                def binningDet = BinningDetail.get(it)
                def tanggal = df.format(params.tanggal) + " " + params.jam + ":" + params.menit
                if(binningDet){
                    claim = new Claim()
                    if(params.noClaimBefore==null || params.noClaimBefore==""){
                        claim?.t171ID = t171ID
                    }else{
                        claim?.t171ID = params.noClaimBefore
                    }
                    claim?.t171TglJamClaim = new Date().parse('yyyy-M-d HH:mm', tanggal)
                    claim?.t171PetugasClaim = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    claim?.vendor = vendor
                    claim?.invoice = binningDet.goodsReceiveDetail.invoice
                    claim?.goodsReceive = binningDet.goodsReceiveDetail.goodsReceive
                    claim?.goods = binningDet.goodsReceiveDetail.goods
                    claim?.t171Qty1Issued = binningDet.goodsReceiveDetail.t167Qty1Issued
                    claim?.t171Qty2Issued = binningDet.goodsReceiveDetail.t167Qty2Issued
                    claim?.t171Qty1Reff = binningDet.goodsReceiveDetail.t167Qty1Reff
                    claim?.t171Qty2Reff = binningDet.goodsReceiveDetail.t167Qty2Reff
                    claim?.t171Qty1Rusak = binningDet.goodsReceiveDetail.t167Qty1Rusak
                    claim?.t171Qty2Rusak = binningDet.goodsReceiveDetail.t167Qty2Rusak
                    claim?.t171Qty1Salah = binningDet.goodsReceiveDetail.t167Qty1Salah
                    claim?.t171Qty2Salah = binningDet.goodsReceiveDetail.t167Qty2Salah
                    claim?.t171Keterangan = "ket"
                    claim?.t171xNamaDivisi = "DIVISI"
                    claim?.t171xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    claim?.staDel = '0'
                    claim?.status = '1'

                    claim?.dateCreated = datatablesUtilService.syncTime()
                    claim?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    claim?.lastUpdated = datatablesUtilService.syncTime()
                    claim?.updatedBy  =org.apache.shiro.SecurityUtils.subject.principal.toString()
                    claim?.lastUpdProcess  ="INSERT"
                    claim?.companyDealer = session?.userCompanyDealer
                    claim.save(flush: true)
                }
            }
                hasil = t171ID
        }else {
            hasil = "fail"
        }
        render hasil
    }


    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Claim, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(Request, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def kodeList() {
        def res = [:]

        def result = Vendor.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.m121Nama
        }

        res."options" = opts
        render res as JSON
    }

    def getTableData(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def result = []
        def hasil = Claim.createCriteria().list() {
            eq("companyDealer",session?.userCompanyDealer);
            eq("t171ID",params.id)
        }
        hasil.each {
            result << [
                    id : it.goods.id,
                    goods : it.goods.m111ID,
                    goods2 : it.goods.m111Nama,
                    po : GoodsReceiveDetail.findByGoodsAndInvoice(it.goods,it.invoice)?.poDetail.po?.t164NoPO,
                    tglpo : it.invoice.po.t164TglPO ? it.invoice.po.t164TglPO.format(dateFormat) : "",
                    nomorInvoice : GoodsReceiveDetail.findByGoodsAndInvoice(it.goods,it.invoice)?.invoice?.t166NoInv,
                    tglReceive: it.goodsReceive.t167TglJamReceive ? it.goodsReceive.t167TglJamReceive.format(dateFormat) : "",
                    qtyInvoice: it.t171Qty1Reff,
                    qtyReceive: it.t171Qty1Issued,
                    qtySalah: it.t171Qty1Salah,
                    qtyRusak: it.t171Qty1Rusak,
                    tglInvoice : it.invoice.t166TglInv ? it.invoice.t166TglInv.format(dateFormat) : ""
            ]
        }
        render result as JSON
    }


}
