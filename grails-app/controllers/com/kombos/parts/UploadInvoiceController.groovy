package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

class UploadInvoiceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService
    def conversi = new Konversi()

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {

        params.companyDealer = session.userCompanyDealer
    }

    def upload() {
        def operationInstance = null
        def requestBody = request.JSON

        String insertUpdate = params.insertUpdate

        User user = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        PO po
        Invoice invoice

        requestBody.each{
            po = PO.findByT164NoPO((it.noPo as String).trim())
            PODetail poDetail = PODetail.findByPo(po)
            def t166TglInv = new Date().parse("yyyy-MM-dd", it.tglInvoice)
            Calendar start = Calendar.getInstance();
            start.setTime(t166TglInv);

            invoice = new Invoice()

            invoice.t166NoInv = it.noInvoice
            invoice.t166TglInv = start.getTime()
            invoice.po = po
            invoice.goods = poDetail.validasiOrder.requestDetail.goods
            invoice.t166Qty1 = 0
            invoice.t166Qty2 = new Double(it.qtyReceive)
            invoice.t166RetailPrice = new Double(it.retailPrice)
            invoice.t166Disc = new Double(it.diskon)
            invoice.t166NetSalesPrice = new Double(it.netSalesPrice)
            invoice.staDel = '0'
            invoice.t166xNamaUser = user.username
            invoice.t166xNamaDivisi = user.divisi==null?"no division":user.divisi.m012NamaDivisi
            invoice.createdBy = user.username
            invoice.updatedBy = user.username
            invoice.lastUpdProcess = "UPLOAD"
            invoice.companyDealer=session.session.userCompanyDealer
            invoice.dateCreated = datatablesUtilService?.syncTime()
            invoice.lastUpdated = datatablesUtilService?.syncTime()

            invoice.save()
            invoice.errors.each{
                println it
            }
        }

        flash.message = message(code: 'default.uploadCOGS.message', default: "Save Job Done")
//        render(view: "index", model: [operationInstance: operationInstance])
        render(view: "index")
    }

    def view() {
        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1, //starts from 0
                columnMap:  [
                        //Col, Map-Key
                        'A':'noPo',
                        'B':'tipeOrder',
                        'C':'kodePart',
                        'D':'qtyReceive',
                        'E':'noInvoice',
                        'F':'tglInvoice',
                        'G':'retailPrice',
                        'H':'diskon',
                        'I':'netSalesPrice'
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
//                render(view: "index", model: [operationInstance: operationInstance])
                render(view: "index")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def targetList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = targetList as JSON

//            Serial serial = null
//            Section section = null
//            KategoriJob kategoriJob = null

            String style = "style='color:red;'"
            String styleBlack = "style='color:black;'"
            org.joda.time.LocalDate tglInvoice
            String tglInvoiceFormatted = ""
            String allError = "123456789"
            String error = ""
            Goods goods
            targetList?.each {
                error = ""
                tglInvoice = it.tglInvoice
                tglInvoiceFormatted = it.tglInvoice
                if(
                        (it.noPo && it.noPo != "") &&
                        (it.tipeOrder && it.tipeOrder != "") &&
                        (it.kodePart && it.kodePart != "") &&
                        (it.qtyReceive && it.qtyReceive != "") &&
                        (it.noInvoice && it.noInvoice != "") &&
                        (it.tglInvoice) &&
                        (it.retailPrice && it.retailPrice != "") &&
                        (it.diskon && it.diskon != "") &&
                        (it.netSalesPrice && it.netSalesPrice !="")
                ){
                    if(PO.findByT164NoPO((it.noPo as String).trim())==null){
                        error = error + "1"
                        jmlhDataError++;
                    }

                    goods = Goods.findByM111ID(it.kodePart)
                    if(goods==null){
                        error = error + "3"
                        jmlhDataError++;
                    } else {
                        PODetail poDetail = PODetail.findByPo(PO.findByT164NoPO((it.noPo as String).trim()))
                        if(!poDetail.validasiOrder.requestDetail.goods.m111ID.equals((it.kodePart as String).trim())){
                            error = error + "13"
                            jmlhDataError++;
                        }
                    }

                    try{
                        tglInvoiceFormatted = new java.sql.Date(tglInvoice.toDate().getTime()).format("dd/MM/yyyy")
                    }catch(Exception e){
                        error = error + "6"
                        jmlhDataError++;
                    }
                }else{
                    error = allError
                    jmlhDataError++;
                }

                htmlData+="<tr "+(error==""?"":"class='error'")+" >\n" +
                        "                            <td "+(error.contains('1')?style:styleBlack)+">\n" +
                        "                                "+it.noPo+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('2')?style:styleBlack)+">\n" +
                        "                                "+it.tipeOrder+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('3')?style:styleBlack)+">\n" +
                        "                                "+it.kodePart+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('4')?style:styleBlack)+">\n" +
                        "                                "+it.qtyReceive+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('5')?style:styleBlack)+">\n" +
                        "                                "+it.noInvoice+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('6')?style:styleBlack)+">\n" +
                        "                                "+tglInvoiceFormatted+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('7')?style:styleBlack)+">\n" +
                        "                                "+conversi.toRupiah(it.retailPrice)+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('8')?style:styleBlack)+">\n" +
                        "                                "+it.diskon+"\n" +
                        "                            </td>\n" +
                        "                            <td "+(error.contains('9')?style:styleBlack)+">\n" +
                        "                                "+conversi.toRupiah(it.netSalesPrice)+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadCOGSError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadCOGS.message', default: "Read File Done")
        }

        render(view: "index", model: [htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }

}
