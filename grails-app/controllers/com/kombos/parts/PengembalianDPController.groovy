package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PengembalianDPController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PengembalianDP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m166TglBerlaku") {
                ge("m166TglBerlaku", params."sCriteria_m166TglBerlaku")
                lt("m166TglBerlaku", params."sCriteria_m166TglBerlaku" + 1)
            }

            if (params."sCriteria_m166PersenKembaliDP") {
                eq("m166PersenKembaliDP",Double.parseDouble (params."sCriteria_m166PersenKembaliDP"))
            }

            if (params."sCriteria_m166MaxKembaliDiKasir") {
                eq("m166MaxKembaliDiKasir",Double.parseDouble (params."sCriteria_m166MaxKembaliDiKasir"))
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m166TglBerlaku: it.m166TglBerlaku ? it.m166TglBerlaku.format(dateFormat) : "",

                    m166PersenKembaliDP: it.m166PersenKembaliDP,

                    m166MaxKembaliDiKasir: it.m166MaxKembaliDiKasir,

                    companyDealer: it.companyDealer,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [pengembalianDPInstance: new PengembalianDP(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def pengembalianDPInstance = new PengembalianDP(params)
        pengembalianDPInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        pengembalianDPInstance?.lastUpdProcess = "INSERT"
        def cek = PengembalianDP.createCriteria().list() {
            and{
                eq("m166TglBerlaku",pengembalianDPInstance.m166TglBerlaku)
//                eq("m166PersenKembaliDP",pengembalianDPInstance.m166PersenKembaliDP)
//                eq("m166MaxKembaliDiKasir",pengembalianDPInstance.m166MaxKembaliDiKasir)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [pengembalianDPInstance: pengembalianDPInstance])
            return
        }
        if (!pengembalianDPInstance.save(flush: true)) {
            render(view: "create", model: [pengembalianDPInstance: pengembalianDPInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'pengembalianDP.label', default: 'Pengembalian DP Parts'), pengembalianDPInstance.id])
        redirect(action: "show", id: pengembalianDPInstance.id)
    }

    def show(Long id) {
        def pengembalianDPInstance = PengembalianDP.get(id)
        if (!pengembalianDPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pengembalianDP.label', default: 'PengembalianDP'), id])
            redirect(action: "list")
            return
        }

        [pengembalianDPInstance: pengembalianDPInstance]
    }

    def edit(Long id) {
        def pengembalianDPInstance = PengembalianDP.get(id)
        if (!pengembalianDPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pengembalianDP.label', default: 'PengembalianDP'), id])
            redirect(action: "list")
            return
        }

        [pengembalianDPInstance: pengembalianDPInstance]
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.m166MaxKembaliDiKasir=params.m166MaxKembaliDiKasir.replace(",","")
        def pengembalianDPInstance = PengembalianDP.get(id)
        if (!pengembalianDPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pengembalianDP.label', default: 'PengembalianDP'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (pengembalianDPInstance.version > version) {

                pengembalianDPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'pengembalianDP.label', default: 'PengembalianDP')] as Object[],
                        "Another user has updated this PengembalianDP while you were editing")
                render(view: "edit", model: [pengembalianDPInstance: pengembalianDPInstance])
                return
            }
        }

        def cek = PengembalianDP.createCriteria()
        def result = cek.list() {
            and{
                eq("m166TglBerlaku",params.m166TglBerlaku)
                eq("m166PersenKembaliDP",Double.parseDouble(params.m166PersenKembaliDP))
                eq("m166MaxKembaliDiKasir",Double.parseDouble(params.m166MaxKembaliDiKasir))
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [pengembalianDPInstance: pengembalianDPInstance])
            return
        }

        pengembalianDPInstance.properties = params
        pengembalianDPInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        pengembalianDPInstance?.lastUpdProcess = "UPDATE"

        if (!pengembalianDPInstance.save(flush: true)) {
            render(view: "edit", model: [pengembalianDPInstance: pengembalianDPInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'pengembalianDP.label', default: 'Pengembalian DP Parts'), pengembalianDPInstance.id])
        redirect(action: "show", id: pengembalianDPInstance.id)
    }

    def delete(Long id) {
        def pengembalianDPInstance = PengembalianDP.get(id)
        if (!pengembalianDPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pengembalianDP.label', default: 'PengembalianDP'), id])
            redirect(action: "list")
            return
        }

        try {
            pengembalianDPInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            pengembalianDPInstance?.lastUpdProcess = "DELETE"
            pengembalianDPInstance?.lastUpdated = datatablesUtilService?.syncTime()
            pengembalianDPInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'pengembalianDP.label', default: 'PengembalianDP'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'pengembalianDP.label', default: 'PengembalianDP'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(PengembalianDP, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PengembalianDP, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
