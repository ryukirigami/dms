package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadGroupDiskonController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def list(Integer max) {
    }
    def index() {}
    def view() {
        def groupDiskonInstance = new GroupDiskon(params)

        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'tglMulai',
                        'B':'tglSelesai',
                        'C':'namaGroup',
                        'D':'diskon',

                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        def jsonData = ""
        int jmlhDataError = 0;
        String htmlData = "",cekData = ""
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream','text/plain'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [groupDiskonInstance: groupDiskonInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = jobList as JSON
            //Group group = null


            String status = "0", style = ""
            jobList?.each {
                def cekTgl = ""
                String mul = "",sel = ""
                def persen = null
                Date tglMulai= null, tglSelesai = null
                def group = it
                try {
                    String data = it.diskon
                    persen = Double.parseDouble(data.toString())
                    if(persen>100){
                        cekTgl = "gagal"
                    }
                }catch (NumberFormatException ee){
                   cekTgl = "gagal"
                }
                try {
                    mul = it.tglMulai
                    tglMulai = new Date().parse('yyyy-MM-d', mul.toString())

                }catch (Exception e){
                    cekTgl = "gagal"
                }
                try {
                    sel = it.tglSelesai
                    tglSelesai = new Date().parse('yyyy-MM-d', sel.toString())
                 }catch (Exception e){
                    cekTgl = "gagal"
                 }
                if((it.tglMulai && it.tglMulai!="") && (it.tglSelesai && it.tglSelesai !="") && (it.namaGroup && it.namaGroup!= "") && (it.diskon && it.diskon!= "")){
                    def cek = Group.createCriteria()
                    def result = cek.list() {
                        and{
                            eq("m181NamaGroup",group.namaGroup, [ignoreCase: true])
                            eq("staDel",'0')
                        }
                    }

                    def cek2 = GroupDiskon.createCriteria()
                    def result2 = cek2.list() {
                        eq("group", Group.findByM181NamaGroupIlike(group.namaGroup))
                        ilike("staDel", "0")
                    }
                    def tanggalBeririsan = 0
                    result2.each {
                        if(tglMulai >= it?.m172TglAwal && tglMulai <= it?.m172TglAkhir){
                            tanggalBeririsan = 1
                        }
                        if(tglSelesai >= it?.m172TglAwal && tglSelesai <= it?.m172TglAkhir){
                            tanggalBeririsan = 1
                        }
                        if(tglMulai <= it?.m172TglAwal && tglSelesai >= it?.m172TglAkhir){
                            tanggalBeririsan = 1
                        }
                    }
                    if(tanggalBeririsan == 1){
                        jmlhDataError++
                        status = "2"
                    }
                    if(!result){
                        jmlhDataError++
                        status = "1"
                    }
                    if(tglMulai > tglSelesai){
                        jmlhDataError++
                        status = "1"
                    }
                    if(cekTgl == "gagal"){

                        jmlhDataError++
                        status = "1"
                    }

                } else {
                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                }else if(status.equals("2")){
                    style = "style='color:blue;'"
                } else {
                    style = ""
                }
                def isi1= ""
                if(tglMulai==null || tglMulai==""){
                    isi1 = it.tglMulai
                }else{
                    isi1 = tglMulai.format("dd/MM/YYYY")
                }
                def isi2= ""
                if(tglSelesai==null || tglSelesai==""){
                    isi2 = it.tglSelesai
                }else{
                    isi2 = tglSelesai.format("dd/MM/YYYY")
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+ isi1 +"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+ isi2 +"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.namaGroup+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.diskon+"\n" +
                        "                            </td>\n" +

                        "                        </tr>"
                status = "0"
                cekData = ""
            }
        }

        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadGroupDiskon.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah, baris warna biru data sudah ada")
        } else {
            flash.message = message(code: 'default.uploadGroupDiskon.message', default: "Read File Done")
        }

        render(view: "index", model: [groupDiskonInstance: groupDiskonInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }
    def upload() {
        DateFormat df = new SimpleDateFormat("YYYY-MM-dd")

        def groupDiskonInstance = null

        def requestBody = request.JSON

        requestBody.each{
            groupDiskonInstance = new GroupDiskon()
            def data = it
            def group = Group.findByM181NamaGroupIlikeAndStaDel(data.namaGroup,'0')

            if(group){
                String mul = it.tglMulai
                String sel = it.tglSelesai
                Date tglMulai = new Date().parse('yyyy-MM-d', mul.toString())
                def grupDis = GroupDiskon?.findByM172TglAwalAndStaDel(tglMulai,'0')
                if(!grupDis){
                    groupDiskonInstance.m172TglAwal = new Date().parse('yyyy-MM-d',mul.toString())
                    groupDiskonInstance.m172TglAkhir = new Date().parse('yyyy-MM-d', sel.toString())
                    groupDiskonInstance.group = group
                    def persen = (double) it.diskon
                    groupDiskonInstance.m172PersenDiskon = persen

                    groupDiskonInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    groupDiskonInstance.lastUpdProcess = "UPLOAD"
                    groupDiskonInstance.setStaDel('0')
                    groupDiskonInstance.dateCreated = datatablesUtilService?.syncTime()
                    groupDiskonInstance.lastUpdated = datatablesUtilService?.syncTime()
                    try {
                        groupDiskonInstance.save(flush : true)
                    }catch (Exception e){
                        e.printStackTrace()
                    }
                }
                flash.message = message(code: 'default.groupDiskon.message', default: "Save Job Done")
            }else{
                flash.message = message(code: 'default.groupDiskon.message', default: "gagal")
            }

        }

        render(view: "index", model: [groupDiskonInstance: groupDiskonInstance])

    }
}
