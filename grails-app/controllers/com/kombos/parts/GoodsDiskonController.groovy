package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GoodsDiskonController {

	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST", autoCompleteCode:"GET"]

	static viewPermissions = ['index', 'list', 'datatablesList']

	static addPermissions = ['create', 'save']

	static editPermissions = ['edit', 'update']

	static deletePermissions = ['delete']

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0

		session.exportParams=params

		def c = GoodsDiskon.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

			if(params."sCriteria_goods"){
                or {
				    eq("goods",Goods.findByM111IDIlike("%"+params."sCriteria_goods"+"%"))
                    eq("goods",Goods.findByM111NamaIlike("%"+params."sCriteria_goods"+"%"))
                }
			}

			if(params."sCriteria_group"){
				eq("group",Group.findByM181NamaGroupIlike("%"+params."sCriteria_group"+"%"))
			}

			if(params."sCriteria_m171TglAwal"){
				ge("m171TglAwal",params."sCriteria_m171TglAwal")
				lt("m171TglAwal",params."sCriteria_m171TglAwal" + 1)
			}

			if(params."sCriteria_m171TglAkhir"){
				ge("m171TglAkhir",params."sCriteria_m171TglAkhir")
				lt("m171TglAkhir",params."sCriteria_m171TglAkhir" + 1)
			}

			if(params."sCriteria_m171PersenDiskon"){
				eq("m171PersenDiskon",Double.parseDouble(params."sCriteria_m171PersenDiskon"))
			}


				ilike("staDel","0")



			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}

		def rows = []

		results.each {
			rows << [

						id: it.id,

						companyDealer: it.companyDealer,

						goods: it.goods.m111ID + ' - ' +it.goods.m111Nama,

						group: it.group.m181NamaGroup,

						m171TglAwal: it.m171TglAwal?it.m171TglAwal.format(dateFormat):"",

						m171TglAkhir: it.m171TglAkhir?it.m171TglAkhir.format(dateFormat):"",

						m171PersenDiskon: it.m171PersenDiskon,

						staDel: it.staDel,

						createdBy: it.createdBy,

						updatedBy: it.updatedBy,

						lastUpdProcess: it.lastUpdProcess,

			]
		}

		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

		render ret as JSON
	}

	def create() {
		[goodsDiskonInstance: new GoodsDiskon(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def goodsDiskonInstance = new GoodsDiskon(params)
        goodsDiskonInstance?.goods = Goods.findById(params.goods.id as Long)
        goodsDiskonInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        goodsDiskonInstance?.lastUpdProcess = "INSERT"
        goodsDiskonInstance?.companyDealer = CompanyDealer.findById(session.userCompanyDealerId)
        goodsDiskonInstance?.setStaDel('0')
        def tgl = GoodsDiskon.findByM171TglAwalAndStaDel(goodsDiskonInstance?.m171TglAwal,'0')
        def cek = GoodsDiskon.createCriteria()
        def result = cek.list() {
            eq("group", Group.findById(params.group.id))
            eq("goods", Goods.findById(params.goods.id))

            ilike("staDel", "0")
        }
        def tanggalBeririsan = 0
        result.each {
            if(goodsDiskonInstance?.m171TglAwal >= it?.m171TglAwal && goodsDiskonInstance?.m171TglAwal <= it?.m171TglAkhir){
                tanggalBeririsan = 1
            }
            if(goodsDiskonInstance?.m171TglAkhir >= it?.m171TglAwal && goodsDiskonInstance?.m171TglAkhir <= it?.m171TglAkhir){
                tanggalBeririsan = 1
            }
            if(goodsDiskonInstance?.m171TglAwal <= it?.m171TglAwal && goodsDiskonInstance?.m171TglAkhir >= it?.m171TglAkhir){
                tanggalBeririsan = 1
            }
        }
        if(tanggalBeririsan == 1){
            flash.message = "* Tanggal Awal Beririsan dengan data tanggal group dan goods yang sama sebelumnya"
            render(view: "create", model: [goodsDiskonInstance: goodsDiskonInstance])
            return
        }
        if(goodsDiskonInstance?.m171TglAwal > goodsDiskonInstance?.m171TglAkhir){
            flash.message = "* Tanggal Akhir Harus Lebih Besar"
            render(view: "create", model: [goodsDiskonInstance: goodsDiskonInstance])
            return
        }
        if(tgl){
            flash.message = "* Data sudah terdaftar"
            render(view: "create", model: [goodsDiskonInstance: goodsDiskonInstance])
            return
        }
		if (!goodsDiskonInstance.save(flush: true)) {
			render(view: "create", model: [goodsDiskonInstance: goodsDiskonInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), goodsDiskonInstance.id])
		redirect(action: "show", id: goodsDiskonInstance.id)
	}

	def show(Long id) {
		def goodsDiskonInstance = GoodsDiskon.get(id)
		if (!goodsDiskonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), id])
			redirect(action: "list")
			return
		}

		[goodsDiskonInstance: goodsDiskonInstance]
	}

	def edit(Long id) {
		def goodsDiskonInstance = GoodsDiskon.get(id)
		if (!goodsDiskonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), id])
			redirect(action: "list")
			return
		}

		[goodsDiskonInstance: goodsDiskonInstance]
	}

	def update(Long id, Long version) {
		def goodsDiskonInstance = GoodsDiskon.get(id)
		if (!goodsDiskonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (goodsDiskonInstance.version > version) {

				goodsDiskonInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'goodsDiskon.label', default: 'GoodsDiskon')] as Object[],
				"Another user has updated this GoodsDiskon while you were editing")
				render(view: "edit", model: [goodsDiskonInstance: goodsDiskonInstance])
				return
			}
		}
        def cek = GoodsDiskon.findAllByGroupAndGoodsAndStaDel(Group.findById(params.group.id), Goods.findById(params.goods.id),'0')
        if(cek){
            for(find in cek) {
                if(find.id<id){
                    if(find?.m171TglAkhir.after(params.m171TglAwal) || find?.m171TglAkhir.compareTo(params.m171TglAwal)==0){
                        flash.message = "* Tanggal Awal Beririsan dengan data tanggal group dan goods yang sama sebelumnya"
                        render(view: "edit", model: [goodsDiskonInstance: goodsDiskonInstance])
                        return
                        break
                    }
                }
                if(find.id>id){
                    if(find?.m171TglAwal.before(params.m171TglAwal) || find?.m171TglAwal.compareTo(params.m171TglAwal)==0){
                        flash.message = "* Tanggal Awal Beririsan dengan data tanggal group dan goods yang sama sebelumnya"
                        render(view: "edit", model: [goodsDiskonInstance: goodsDiskonInstance])
                        return
                        break
                    }
                }
            }
        }

        def tgl = GoodsDiskon.findByM171TglAwalAndStaDel(params.m171TglAwal,'0')
        if(params.m171TglAwal > params.m171TglAkhir){
            flash.message = "* Tanggal Akhir Harus Lebih Besar"
            render(view: "edit", model: [goodsDiskonInstance: goodsDiskonInstance])
            return
        }
        if(tgl && tgl?.id != id){
            flash.message = "* Data sudah terdaftar"
            render(view: "edit", model: [goodsDiskonInstance: goodsDiskonInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
		goodsDiskonInstance.properties = params
        goodsDiskonInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        goodsDiskonInstance?.lastUpdProcess = "UPDATE"
		if (!goodsDiskonInstance.save(flush: true)) {
			render(view: "edit", model: [goodsDiskonInstance: goodsDiskonInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), goodsDiskonInstance.id])
		redirect(action: "show", id: goodsDiskonInstance.id)
	}

	def delete(Long id) {
		def goodsDiskonInstance = GoodsDiskon.get(id)
		if (!goodsDiskonInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), id])
			redirect(action: "list")
			return
		}

		try {
			//goodsDiskonInstance.delete(flush: true)
            goodsDiskonInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            goodsDiskonInstance?.lastUpdProcess = "DELETE"
            goodsDiskonInstance?.setStaDel('1')
            goodsDiskonInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'goodsDiskon.label', default: 'GoodsDiskon'), id])
			redirect(action: "show", id: id)
		}
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(GoodsDiskon, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(GoodsDiskon, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def listParts() {
        def res = [:]
        def opts = []
        def z = Goods.createCriteria()
        def results=z.list {
            or{
                ilike("m111Nama","%" + params.query + "%")
                ilike("m111ID","%" + params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.m111ID.trim() +" || "+ it.m111Nama
        }
        res."options" = opts
        render res as JSON

    }

    def detailParts(){
        def result = [:]
        def data= params.noGoods as String
        if(data.contains("||")){
            def dataNogoods = data.split(" ")
            def goods = Goods.createCriteria().list {
                eq("staDel","0")
                ilike("m111ID","%"+dataNogoods[0].trim() + "%")
            }
            if(goods.size()>0){
                def goodsData = goods.last()
                result."hasil" = "ada"
                result."id" = goodsData?.id
                result."namaGoods" = goodsData?.m111Nama
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }

}
