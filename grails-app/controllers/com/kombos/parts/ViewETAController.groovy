package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ViewETAController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = ETA.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
			eq("companyDealer",session?.userCompanyDealer)
			if(params."tanggalStart"){
				ge("t165ETA",params."tanggalStart")
				lt("t165ETA",params."tanggalEnd" + 1)
			}

            if(params."vendor"){
                'in'("po", PO.findAllByVendorAndStaDelAndCompanyDealer(Vendor.findById(new Long(params."vendor")),'0'),params?.companyDealer);
            }

            if(params."goods"){
                eq("goods.id", new Long(params."goods"))
            }

            ne("staDel",'1')
			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						goods: it.goods,
			
						t165ETA: it.t165ETA?it.t165ETA.format("dd/MM/yyyy HH:mm"):"",

                        kode_vendor: it.po.vendor.m121ID,

                        nama_vendor: it.po.vendor.m121Nama,
			
						kode_part: it.goods.m111ID,

                        nama_part: it.goods.m111Nama,

                        tanggal_po: it.po.t164TglPO.format(dateFormat),

                        no_po: it.po.t164NoPO
            ]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[ETAInstance: new ETA(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def ETAInstance = new ETA(params)
		if (!ETAInstance.save(flush: true)) {
			render(view: "create", model: [ETAInstance: ETAInstance])
			return
		}
		flash.message = message(code: 'default.created.message', args: [message(code: 'ETA.label', default: 'ETA'), ETAInstance.id])
		redirect(action: "show", id: ETAInstance.id)
	}

	def show(Long id) {
		def ETAInstance = ETA.get(id)
		if (!ETAInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'ETA.label', default: 'ETA'), id])
			redirect(action: "list")
			return
		}

		[ETAInstance: ETAInstance]
	}

	def edit(Long id) {
		def ETAInstance = ETA.get(id)
		if (!ETAInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'ETA.label', default: 'ETA'), id])
			redirect(action: "list")
			return
		}

		[ETAInstance: ETAInstance]
	}

	def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
		def ETAInstance = ETA.get(id)
		if (!ETAInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'ETA.label', default: 'ETA'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (ETAInstance.version > version) {
				
				ETAInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'ETA.label', default: 'ETA')] as Object[],
				"Another user has updated this ETA while you were editing")
				render(view: "edit", model: [ETAInstance: ETAInstance])
				return
			}
		}

		ETAInstance.properties = params

		if (!ETAInstance.save(flush: true)) {
			render(view: "edit", model: [ETAInstance: ETAInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'ETA.label', default: 'ETA'), ETAInstance.id])
		redirect(action: "show", id: ETAInstance.id)
	}

	def delete(Long id) {
		def ETAInstance = ETA.get(id)
		if (!ETAInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'ETA.label', default: 'ETA'), id])
			redirect(action: "list")
			return
		}

		try {
			ETAInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'ETA.label', default: 'ETA'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'ETA.label', default: 'ETA'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(ETA, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		try {
			datatablesUtilService.updateField(ETA, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

}
