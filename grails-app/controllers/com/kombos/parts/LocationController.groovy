package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class LocationController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Location.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")

            if (params."sCriteria_parameterICC") {
                eq("parameterICC",ParameterICC.findByM155KodeICC (params."sCriteria_parameterICC"))
            }

            if (params."sCriteria_m120NamaLocation") {
                ilike("m120NamaLocation", "%" + (params."sCriteria_m120NamaLocation" as String) + "%")
            }

            if (params."sCriteria_m120Ket") {
                ilike("m120Ket", "%" + (params."sCriteria_m120Ket" as String) + "%")
            }

            switch(sortProperty){
                case "parameterICC":
                parameterICC{
                    order("m155KodeICC",sortDir)
                }
                break;
            default:
                order(sortProperty,sortDir)
                break;
            }

        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    parameterICC: it.parameterICC?.m155KodeICC,

                    m120NamaLocation: it.m120NamaLocation,

                    m120Ket: it.m120Ket,

//                    m120ID: it.m120ID,
//
//                    companyDealer: it.companyDealer,
//
//                    staDel: it.staDel,
//
//                    createdBy: it.createdBy,
//
//                    updatedBy: it.updatedBy,
//
//                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [locationInstance: new Location(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def locationInstance = new Location(params)
        locationInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        locationInstance?.lastUpdProcess = "INSERT"
        locationInstance?.setStaDel ('0')
        def cek = Location.createCriteria().list() {
            and{
                eq("parameterICC",locationInstance.parameterICC)
                eq("m120NamaLocation",locationInstance.m120NamaLocation?.trim(), [ignoreCase: true])
//                eq("m120Ket",locationInstance.m120Ket?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [locationInstance: locationInstance])
            return
        }
        if (!locationInstance.save(flush: true)) {
            render(view: "create", model: [locationInstance: locationInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'location.label', default: 'Location'), locationInstance.id])
        redirect(action: "show", id: locationInstance.id)
    }

    def show(Long id) {
        def locationInstance = Location.get(id)
        if (!locationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'location.label', default: 'Location'), id])
            redirect(action: "list")
            return
        }

        [locationInstance: locationInstance]
    }

    def edit(Long id) {
        def locationInstance = Location.get(id)
        if (!locationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'location.label', default: 'Location'), id])
            redirect(action: "list")
            return
        }

        [locationInstance: locationInstance]
    }

    def update(Long id, Long version) {
        def locationInstance = Location.get(id)
        if (!locationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'location.label', default: 'Location'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (locationInstance.version > version) {

                locationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'location.label', default: 'Location')] as Object[],
                        "Another user has updated this Location while you were editing")
                render(view: "edit", model: [locationInstance: locationInstance])
                return
            }
        }

        def cek = Location.createCriteria()
        def result = cek.list() {
            and{
                eq("parameterICC",ParameterICC.findById(Long.parseLong(params.parameterICC.id)))
                eq("m120NamaLocation",params.m120NamaLocation?.trim(), [ignoreCase: true])
//                eq("m120Ket",params.m120Ket?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            for(find in result){
                if(id!=find.id){
                    flash.message = "Nama Alert Sudah Ada"
                    render(view: "edit", model: [locationInstance: locationInstance])
                    return
                }
            }
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        locationInstance.properties = params
        locationInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        locationInstance?.lastUpdProcess = "UPDATE"

        if (!locationInstance.save(flush: true)) {
            render(view: "edit", model: [locationInstance: locationInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'location.label', default: 'Location'), locationInstance.id])
        redirect(action: "show", id: locationInstance.id)
    }

    def delete(Long id) {
        def locationInstance = Location.get(id)
        if (!locationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'location.label', default: 'Location'), id])
            redirect(action: "list")
            return
        }

        try {
            locationInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            locationInstance?.lastUpdProcess = "DELETE"
            locationInstance?.setStaDel('1')
            locationInstance?.setLastUpdated(datatablesUtilService?.syncTime())
            locationInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'location.label', default: 'Location'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'location.label', default: 'Location'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Location, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Location, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
