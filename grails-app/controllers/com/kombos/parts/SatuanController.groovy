package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SatuanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Satuan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if (params."sCriteria_m118ID") {
                eq("m118ID", params."sCriteria_m118ID")
            }

            if (params."sCriteria_m118KodeSatuan") {
                ilike("m118KodeSatuan", "%" + (params."sCriteria_m118KodeSatuan" as String) + "%")
            }

            if (params."sCriteria_m118Satuan1") {
                ilike("m118Satuan1", "%" + (params."sCriteria_m118Satuan1" as String) + "%")
            }

            if (params."sCriteria_m118Satuan2") {
                ilike("m118Satuan2", "%" + (params."sCriteria_m118Satuan2" as String) + "%")
            }

            if (params."sCriteria_m118Konversi") {
                eq("m118Konversi", params."sCriteria_m118Konversi" as Double)
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m118ID: it.m118ID,

                    m118KodeSatuan: it.m118KodeSatuan,

                    m118Satuan1: it.m118Satuan1,

                    m118Satuan2: it.m118Satuan2,

                    m118Konversi: it.m118Konversi,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [satuanInstance: new Satuan(params)]
    }

    def save() {
        def satuanInstance = new Satuan(params)
        satuanInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        satuanInstance?.setLastUpdProcess("INSERT")
        satuanInstance?.setDateCreated(datatablesUtilService?.syncTime())
        satuanInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        satuanInstance?.setStaDel("0")
        def c=Satuan.createCriteria()
        def find=c.list {
            eq("staDel","0");
            eq("m118KodeSatuan",params.m118KodeSatuan,[ignoreCase: true])
        }

        def d=Satuan.createCriteria()
        def findAgain=d.list {
            eq("staDel","0")
            eq("m118Satuan1",params.m118Satuan1,[ignoreCase: true])
            eq("m118Satuan2",params.m118Satuan2,[ignoreCase: true])
        }

        if(find){
            flash.message = "Kode Satuan sudah digunakan"
            render(view: "create", model: [satuanInstance: satuanInstance])
            return
        }
        if(findAgain){
            flash.message = "Data Sudah ada"
            render(view: "create", model: [satuanInstance: satuanInstance])
            return
        }
        if (!satuanInstance.save(flush: true)) {
            render(view: "create", model: [satuanInstance: satuanInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'satuan.label', default: 'Satuan'), satuanInstance.id])
        redirect(action: "show", id: satuanInstance.id)
    }

    def show(Long id) {
        def satuanInstance = Satuan.get(id)
        if (!satuanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'satuan.label', default: 'Satuan'), id])
            redirect(action: "list")
            return
        }

        [satuanInstance: satuanInstance]
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def satuanInstance = Satuan.get(id)
        if (!satuanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'satuan.label', default: 'Satuan'), id])
            redirect(action: "list")
            return
        }
        [satuanInstance: satuanInstance]
    }

    def update(Long id, Long version) {
        def satuanInstance = Satuan.get(id)
        if (!satuanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'satuan.label', default: 'Satuan'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (satuanInstance.version > version) {

                satuanInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'satuan.label', default: 'Satuan')] as Object[],
                        "Another user has updated this Satuan while you were editing")
                render(view: "edit", model: [satuanInstance: satuanInstance])
                return
            }
        }
        def c=Satuan.createCriteria()
        def find=c.list {
            eq("staDel","0");
            eq("m118KodeSatuan",params.m118KodeSatuan,[ignoreCase: true])
        }

        def d=Satuan.createCriteria()
        def findAgain=d.list {
            eq("staDel","0")
            eq("m118Satuan1",params.m118Satuan1,[ignoreCase: true])
            eq("m118Satuan2",params.m118Satuan2,[ignoreCase: true])
        }

        if(findAgain){
            for(cek in findAgain){
                if(id!=cek.id){
                    flash.message = "Data Sudah ada"
                    render(view: "edit", model: [satuanInstance: satuanInstance])
                    return
                    break
                }
            }
        }
        if(find){
            for(cek in find){
                if(id!=cek.id){
                    flash.message = "Kode Satuan sudah digunakan"
                    render(view: "edit", model: [satuanInstance: satuanInstance])
                    return
                    break
                }
            }
        }

        satuanInstance.properties = params
        satuanInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        satuanInstance?.setLastUpdProcess("UPDATE")
        satuanInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        if (!satuanInstance.save(flush: true)) {
            render(view: "edit", model: [satuanInstance: satuanInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'satuan.label', default: 'Satuan'), satuanInstance.id])
        redirect(action: "show", id: satuanInstance.id)
    }

    def delete(Long id) {
        def satuanInstance = Satuan.get(id)
        if (!satuanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'satuan.label', default: 'Satuan'), id])
            redirect(action: "list")
            return
        }

        try {
            satuanInstance?.setLastUpdProcess("DELETE")
            satuanInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            satuanInstance?.setStaDel("1")
            satuanInstance?.lastUpdated = datatablesUtilService?.syncTime()
            satuanInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'satuan.label', default: 'Satuan'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'satuan.label', default: 'Satuan'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Satuan, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        try {
            datatablesUtilService.updateField(Satuan, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
