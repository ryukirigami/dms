package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.dao.DataIntegrityViolationException

import java.text.SimpleDateFormat

class PartsAdjustController{

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def jasperService

    def index() {

        redirect(action: "list", params: params)
    }

    def sublist() {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubList() {
        def results = PartsAdjDetail.findAllByPartsAdjustAndT146StaDel(PartsAdjust.get(params.id),"0")
        def rows = []

        results.each { partsAdjDetail ->
            def goods = partsAdjDetail?.goods
            rows << [
                    id: goods.id,
                    kodePA : partsAdjDetail.partsAdjust.t145ID,
                    pilih: partsAdjDetail != null,
                    kode: goods?.m111ID,
                    nama: goods?.m111Nama,
                    awal: partsAdjDetail == null ? 0 : partsAdjDetail?.t146JmlAwal1,
                    real: partsAdjDetail == null ? 0 : partsAdjDetail?.t146JmlAkhir1,
                    alasan: partsAdjDetail == null ? 0 : partsAdjDetail?.alasanAdjusment?.m165AlasanAdjusment,
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

        render ret as JSON
    }

    def list(Integer max) {
        if(!params.idGoods){
            params.idGoods = ['0'];
        }
        [idGoods : params.idGoods,idTable: new Date().format("yyyyMMddhhmmss")]
    }


    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PartsAdjust.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t145StaDel","0");
            eq("companyDealer",session?.userCompanyDealer)
            if(params."idGoods"){
                def list = []
                def listPartsAdjust = []
                list = JSON.parse(params.idGoods)
                list.each {
                        def partsAdjDetail = PartsAdjDetail.findByGoods(Goods.findByM111ID(it))
                        partsAdjDetail.each {
                            listPartsAdjust << it.partsAdjust
                        }
                }

                listPartsAdjust.each {
                    or{
                        eq("id", it.id)
                    }
                }
            }

            if (params."sCriteria_t145TglAdj" && params."sCriteria_t145TglAdjakhir") {
                ge("t145TglAdj", params."sCriteria_t145TglAdj")
                lt("t145TglAdj", params."sCriteria_t145TglAdjakhir" + 1)
            }
            if (sortProperty && !sortProperty?.toString()?.empty) {
                switch (sortProperty) {
                    default:
                        order(sortProperty, sortDir)
                        break;
                }
            }
        }

        def rows = []

        results.each {
            def pdd = ""
            if (params."sCriteria_t145StatusApprove") {
                pdd = PartsAdjDetail.findByPartsAdjustAndT146StaDel(it,"0").statusApprove.toString()
                if(pdd==2){
                    pdd = 1
                }
            }
            if(pdd== params."sCriteria_t145StatusApprove" || !params."sCriteria_t145StatusApprove"){
                rows << [

                        id: it.id,

                        t145ID: it.t145ID,

                        t145TglAdj: it.t145TglAdj ? it.t145TglAdj.format(dateFormat) : ""

                ]
            }
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        def adj = new PartsAdjust(params)
        adj.t145ID = PartsAdjust.generateCode()
        [partsAdjustInstance: adj]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.t145TglAdj = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session?.userCompanyDealer
        def partsAdjustInstance = new PartsAdjust(params)
        if (!partsAdjustInstance.save(flush: true)) {
            render(view: "create", model: [partsAdjustInstance: partsAdjustInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), partsAdjustInstance.id])
        redirect(action: "show", id: partsAdjustInstance.id)
    }

    def show(Long id) {
        def partsAdjustInstance = PartsAdjust.get(id)
        if (!partsAdjustInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), id])
            redirect(action: "list")
            return
        }

        [partsAdjustInstance: partsAdjustInstance]
    }

    def edit(Long id) {
        def partsAdjustInstance = PartsAdjust.get(id)
        if (!partsAdjustInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), id])
            redirect(action: "list")
            return
        }

        [partsAdjustInstance: partsAdjustInstance]
    }

    def update(Long id, Long version) {
        def partsAdjustInstance = PartsAdjust.get(id)
        if (!partsAdjustInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (partsAdjustInstance.version > version) {

                partsAdjustInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'partsAdjust.label', default: 'PartsAdjust')] as Object[],
                        "Another user has updated this PartsAdjust while you were editing")
                render(view: "edit", model: [partsAdjustInstance: partsAdjustInstance])
                return
            }
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.t145TglAdj = datatablesUtilService?.syncTime()
        partsAdjustInstance.properties = params

        if (!partsAdjustInstance.save(flush: true)) {
            render(view: "edit", model: [partsAdjustInstance: partsAdjustInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), partsAdjustInstance.id])
        redirect(action: "show", id: partsAdjustInstance.id)
    }

    def delete(Long id) {
        def partsAdjustInstance = PartsAdjust.get(id)
        if (!partsAdjustInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), id])
            redirect(action: "list")
            return
        }

        try {

            PartsAdjDetail.executeUpdate("delete from PartsAdjDetail where partsAdjust = :partsAdjust", [partsAdjust: partsAdjustInstance])

            partsAdjustInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'partsAdjust.label', default: 'PartsAdjust'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {


            log.info(params.ids)
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            def oListSub = []
            jsonArray.each {
                if(it.toString().indexOf(';')>-1){
                    oListSub << [
                        idGoods: it.toString().substring(0,it.toString().indexOf(';')),
                        kodePA: it.toString().substring(it.toString().indexOf(';')+1,it.toString().length())
                    ]
                }else{
                    oList << PartsAdjust.get(it) }
                }

            oList*.discard() //detach all the objects from session
            oList.each {
                def pad = PartsAdjDetail.findAllByPartsAdjustAndT146StaDel(it,"0")
                if(pad){
                    for(find in pad){
                        find.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        find.lastUpdProcess = "DELETE"
                        find.t146StaDel = "1"
                        find.lastUpdated = datatablesUtilService?.syncTime()
                        find.save(flush: true)
                    }
                }
                def cekPA =  PartsAdjust.findByT145IDAndT145StaDel(it.t145ID,"0")
                if(cekPA){
                    cekPA.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    cekPA.lastUpdProcess = "DELETE"
                    cekPA.t145StaDel = "1"
                    cekPA.lastUpdated = datatablesUtilService?.syncTime()
                    cekPA.save(flush: true)
                }
            }

            oListSub.each {
                def cekPA =  PartsAdjust.findByT145IDAndT145StaDel(it.kodePA,"0")
                def find = PartsAdjDetail.findByGoodsAndPartsAdjust(Goods.get(it.idGoods.toLong()),cekPA)
                if(find){
                    find.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    find.lastUpdProcess = "DELETE"
                    find.t146StaDel = "1"
                    find.lastUpdated = datatablesUtilService?.syncTime()
                    find.save(flush: true)
                    find.errors.each {println it}
                }
                if(PartsAdjDetail.findByPartsAdjustAndT146StaDel(cekPA,"0")==null){
                    cekPA.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    cekPA.lastUpdProcess = "DELETE"
                    cekPA.t145StaDel = "1"
                    cekPA.lastUpdated = datatablesUtilService?.syncTime()
                    cekPA.save(flush: true)
                    cekPA.errors.each {println it}
                }

            }

//            if(oListSub.)

//            datatablesUtilService.massDelete(PartsAdjust, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PartsAdjust, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


    def doReport = {

        PartsAdjust partsAdjust = PartsAdjust.get(params.id)


        def reportData = new ArrayList();


        def results = PartsAdjDetail.findAllByPartsAdjust(partsAdjust)

        int no = 0;
        results.each { partsAdjDetail ->
            def goods = partsAdjDetail?.goods
            def partStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,"0",session?.userCompanyDealer)
            def data = [
                    no: (++no),
                    kode: goods?.m111ID,
                    nama: goods?.m111Nama,
                    qty: partsAdjDetail == null ? 0 : partsAdjDetail?.t146JmlAwal1,
                    satuan: goods?.satuan?.m118Satuan1,
                    harga_satuan: partStok == null || partStok?.t131LandedCost == null ? 0 : partStok?.t131LandedCost,
                    harga_total: partsAdjDetail == null || partsAdjDetail?.t146JmlAwal1 == null || partStok == null || partStok?.t131LandedCost == null ? 0 : partStok?.t131LandedCost * partsAdjDetail?.t146JmlAwal1,
                    alasan: partsAdjDetail == null ? 0 : partsAdjDetail?.alasanAdjusment?.m165AlasanAdjusment,
            ]
            reportData.add(data)
        }


        def parameters = [logo: request.getRealPath("/reports/LOGOnew.gif")];
        parameters.nomor = partsAdjust?.t145ID
        parameters.tanggal = partsAdjust?.dateCreated ? new SimpleDateFormat("dd MMMMM yyyy", new Locale("id")).format(partsAdjust?.dateCreated) : ""

        String tipe = params.tipe ? params.tipe : "PDF"
        def file = File.createTempFile("OnHandAdjustment", "." + tipe);
        file.deleteOnExit()

        def reportDef = new JasperReportDef(name: 'OnHandAdjustment.jasper', fileFormat: tipe.equals('PDF') ? JasperExportFormat.PDF_FORMAT : JasperExportFormat.XLS_FORMAT, parameters: parameters, reportData: reportData)
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }

}