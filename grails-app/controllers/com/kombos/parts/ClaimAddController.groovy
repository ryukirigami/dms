package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class ClaimAddController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {

        [vendor : params.vendor, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params
        def c = BinningDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            goodsReceiveDetail{
                poDetail{
                    po{
                        eq("vendor",Vendor.findByM121Nama(params.vendor))
                    }
                }
            }
            if (params."sCriteria_goods") {
                goodsReceiveDetail{
                    goods{
                        ilike("m111ID","%" + params."sCriteria_goods" + "%")
                    }
                }
            }
            if (params."sCriteria_goods2") {
                goodsReceiveDetail{
                    goods{
                        ilike("m111Nama","%" + ( params."sCriteria_goods2" as String) + "%")
                    }
                }
            }

            if(params."sCriteria_invoice"){
                goodsReceiveDetail{
                    invoice{
                        ilike("t166NoInv","%" + params."sCriteria_invoice" + "%")
                    }
                }
            }


            if(params."sCriteria_po"){
                goodsReceiveDetail{
                    poDetail{
                        po{
                            ilike("t164NoPO","%"+ params."sCriteria_po" +"%")
                        }
                    }
                }
            }


            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }

            switch (sortProperty) {
                case "goods":
                    goodsReceiveDetail{
                        goods{
                            order("m111ID",sortDir)
                        }
                    }
                    break;
                case "goods2":
                    goodsReceiveDetail{
                        goods{
                            order("m111Nama",sortDir)
                        }
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def nos = 0

        results.each {
            def Ngoods =  it?.goodsReceiveDetail?.goods
            def Ninvoice = it?.goodsReceiveDetail?.invoice
            nos = nos + 1
            def dataClaim = Claim.findByGoodsAndInvoiceAndVendor(Ngoods,Ninvoice,Vendor.findByM121Nama(params.vendor))
            if(!dataClaim){
                rows << [

                        id: it?.id,

                        norut:nos,

                        goods: it?.goodsReceiveDetail?.goods?.m111ID,

                        goods2: it?.goodsReceiveDetail?.goods?.m111Nama,

                        po : it?.goodsReceiveDetail?.poDetail?.po?.t164NoPO,

                        tglpo : it?.goodsReceiveDetail?.poDetail?.po?.t164TglPO ? it.goodsReceiveDetail?.poDetail.po.t164TglPO.format(dateFormat) : "",

                        nomorInvoice : it?.goodsReceiveDetail?.invoice?.t166NoInv,

                        tglReceive: it?.goodsReceiveDetail?.goodsReceive?.t167TglJamReceive ? it.goodsReceiveDetail?.goodsReceive.t167TglJamReceive.format(dateFormat) : "",

                        qtyInvoice: it?.goodsReceiveDetail?.t167Qty1Reff,

                        qtyReceive: it?.goodsReceiveDetail?.t167Qty1Issued,

                        qtySalah: it?.goodsReceiveDetail?.t167Qty1Salah,

                        qtyRusak: it?.goodsReceiveDetail?.t167Qty1Rusak,

                        tglInvoice : it?.goodsReceiveDetail?.invoice?.t166TglInv ? it.goodsReceiveDetail?.invoice?.t166TglInv.format(dateFormat) : ""

                ]
            }
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
}
