package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class PoMaintenanceController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def poMaintenanceService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = PO.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
            eq("companyDealer",session?.userCompanyDealer)
            if(params."search_noPo"){
                ilike("t164NoPO","%"+(params."search_noPo")+"%")
            }
            if(params."sta"){
                if(params."sta" == '1'){
                    eq("t164StaOpenCancelClose",POStatus.CANCEL)
                }else if(params."sta" == '2'){
                    eq("t164StaOpenCancelClose",POStatus.CLOSE)
                }else if(params."sta" == 'X'){
                    eq("t164StaOpenCancelClose",POStatus.OPEN)
                }
            }
            if(params."tanggalStart" && params."tanggalEnd"){
                ge("t164TglPO", params."tanggalStart")
                le("t164TglPO",params."tanggalEnd" + 1)
            }
            switch(sortProperty){
                case "noPo":
                        order("t164NoPO",sortDir)
                    break;
                case "tglPo":
                        order("t164TglPO",sortDir)
                    break;
                case "t164StaOpenCancelClose":
                        order("t164StaOpenCancelClose",sortDir)
                    break;
                case "vendor":
                        vendor{
                            order("m121Nama",sortDir)
                        }
                default:
                    order("t164TglPO",sortDir)
                    break;

            }
            eq("staDel","0")
            isNotNull("vendor")
        }

        def rows = []
        PO po
        PODetail poDetail
        results.each {
            po = it
            poDetail = PODetail.findByPo(po)
            def status = ""
            if(po.t164StaOpenCancelClose == POStatus.OPEN){
                status = " Open"
            }else if(po.t164StaOpenCancelClose == POStatus.CANCEL){
                status = " Cancel"
            }else if(po.t164StaOpenCancelClose == POStatus.CLOSE){
                status = " Close"
            }
            rows << [
                    id: po.id,

                    vendor: po?.vendor?.m121Nama,

                    noPo: po.t164NoPO,

                    tglPo: sdf.format(po.t164TglPO),

                    tipeOrder: poDetail?.validasiOrder?.partTipeOrder?.m112TipeOrder,

                    t164StaOpenCancelClose : status,

                    lastProses : poLastProses(po.id),

                    idPo : po?.id
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def poLastProses(def idPo){
        String hasil = "Validasi Order Parts";
        PO po = PO.get(idPo as Long)
        Invoice invoice = Invoice?.findByPoAndStaDel(po,"0")
        GoodsReceiveDetail goodsReceiveDetail = null
        BinningDetail binningDetail = null
        StockINDetail stockINDetail = null
        print(invoice)
        if(invoice){
            hasil = "Input Invoice";
            goodsReceiveDetail = GoodsReceiveDetail?.findByInvoiceAndStaDel(invoice,"0")
        }
        if(goodsReceiveDetail){
            hasil = "Parts Receive";
            binningDetail = BinningDetail?.findByGoodsReceiveDetailAndStaDel(goodsReceiveDetail,"0")
        }
        if (binningDetail){
            hasil = "Parts Binning";
            stockINDetail = StockINDetail?.findByBinningDetailAndStaDel(binningDetail,"0")
        }
        if (binningDetail){
            hasil = "Stock In";
        }

        return hasil
    }

    def sublist(){
        [id: params.idPo,idTable:new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
        render poMaintenanceService.datatablesSubList(params) as JSON
    }

    def subsublist(){
        [idPart : params.idPart,id: params.id,idTable:new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubSubList() {
        params?.companyDealer = session?.userCompanyDealer
        render poMaintenanceService.datatablesSubSubList(params) as JSON
    }

    def massAction() {
        def res = [:]
        try {
            poMaintenanceService.massAction(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}
