package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.FA
import com.kombos.finance.Collection167
import com.kombos.finance.SubType
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class InputGoodsReceiveController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def goodsReceiveService

    def PPBJournalService

    def journalRkNpbService

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    def index() {
        def tglSkrg = datatablesUtilService.syncTime()
        def jamSkrg = datatablesUtilService.syncTime().getHours().toString()
        def mntSkrg = datatablesUtilService.syncTime().getMinutes().toString()
        def gr = GoodsReceive.read(params.id)
        DateFormat sdf = new SimpleDateFormat("ddHHmm ")
        String rcvHour = "00"
        String rcvMinuite = "00"
        List<String> hourList = new ArrayList<>()
        for (int i = 0; i < 24; i++) {
            if (i < 10) hourList.add("0" + i)
            else hourList.add("" + i)
        }
        List<String> minuiteList = new ArrayList<>()
        for (int i = 0; i < 60; i++) {
            if (i < 10) minuiteList.add("0" + i)
            else minuiteList.add("" + i)
        }
        List<String> tipeBayarList = new ArrayList<>()
        tipeBayarList.add("Kredit")
        tipeBayarList.add("Tunai")

        if(params.id && gr){
            rcvHour = gr.t167TglJamReceive.getHours()
            rcvMinuite = gr.t167TglJamReceive.getMinutes()
        } else {
            session?.selectedGoodsReceive = null;
            gr = new GoodsReceive();
            gr.t167ID = GoodsReceive.generateCode()
        }
        if (gr.t167Ppn == null) gr.t167Ppn = 0
        [goodsReceiveInstance: gr, tglSkrg:tglSkrg,rcvHour: rcvHour, rcvMinuite: rcvMinuite, hourList: hourList, minuiteList: minuiteList, tipeBayarList: tipeBayarList,mntSkrg:mntSkrg,jamSkrg:jamSkrg]
    }

    def searchAndAddParts() {
        [nomorPO: params.nomorPO]
    }

    def searchPartsDatatablesList() {
        render goodsReceiveService.datatablesListInputAddParts(params) as JSON
    }

    def save() {
        def jsonArray = JSON.parse(params.ids)
        def simpan = true
        def oList = []
        jsonArray.each {
            if(params.pilihVendor=='0'){
                if(PartsTransferStok.get(it?.toString())){
                    oList << PartsTransferStok.get(it?.toString())
                }
                if(NotaPesananBarangDetail.get(it?.toString())){
                    oList << NotaPesananBarangDetail.get(it?.toString())
                }
            }else if (params.goodsReceive_id) {
                oList << GoodsReceiveDetail.get(it?.toString())
            } else {
                oList << PODetail.get(it?.toString())
            }
        }

        def mssg = "Data Goods dengan kode "
        def mssg2 = "Data PO dengan Nomor "
        def mssg3 = "Data Goods, dengan PO "
        def backToHome = false
        def backToHome2 = false
        def backToHome3 = false
        oList.each {
            if(params.pilihVendor=='0'){
                def cek=PartsTransferStok.get(it?.id?.toLong())
                if(cek){
                    def klas = KlasifikasiGoods.findByGoods(cek?.goods)
                    if(!klas){
                        mssg+=cek?.goods?.m111ID+" , "
                        backToHome = true
                    }
                }
                def cek2 = NotaPesananBarangDetail.get(it?.id?.toLong())
                if(cek2){
                    def klas = KlasifikasiGoods.findByGoods(cek2?.goods)
                    if(!klas){
                        mssg+=cek2?.goods?.m111ID+" , "
                        backToHome = true
                    }
                }

            }else if (params.goodsReceive_id) {
                def cek = GoodsReceiveDetail.get(it?.id?.toLong())
                if(cek){
                    def klas = KlasifikasiGoods.findByGoods(cek?.goods)
                    if(!klas){
                        mssg+=cek?.goods?.m111ID+" , "
                        backToHome = true
                    }
                }
            } else {
                def cek = PODetail.get(it?.id?.toLong())
                if(cek){
                    def klas = KlasifikasiGoods.findByGoods(cek?.validasiOrder?.requestDetail?.goods)
                    if(!klas){
                        mssg+=cek?.validasiOrder?.requestDetail?.goods?.m111ID+" , "
                        backToHome = true
                    }
                }
            }

            def poDet= PODetail.findById(Long.valueOf("" + it.id))
            def cekInv = Invoice.findByGoodsAndPoAndCompanyDealer(poDet.validasiOrder?.requestDetail?.goods,poDet.po,session?.userCompanyDealer)
            if(!cekInv){
                mssg2+=poDet?.po.t164NoPO +" , "
                backToHome2 = true
            }else{
                if(GoodsReceiveDetail.findByInvoiceAndStaDel(cekInv,'0')){
                    mssg3+=poDet?.po.t164NoPO +" , "
                    backToHome3 = true
                }
            }
        }
        if(backToHome){
            def hasil = [:]
            mssg+="\nBelum memiliki klasifikasi goods\nMohon dilengkapi"
            hasil.error = mssg
            render hasil as JSON
        }else if(backToHome2){
            def hasil = [:]
            mssg2+="\nBelum memiliki Invoice, harap melakuan invoicePart terlebih dahulu \n di menu PARTS->INVOICE->INPUT INVOICE"
            hasil.error = mssg2
            render hasil as JSON
        }else if(backToHome3){
            def hasil = [:]
            mssg3+="\nSudah Pernah DI Input. Harap tidak melakukan penginputan dengan data yang sama"
            hasil.error = mssg3
            render hasil as JSON
        }else {
            DateFormat dfSimple = new SimpleDateFormat("dd-MM-yyyy")
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy H:m")
            def input_ppn = params.input_ppn
            def input_tipeBayar = params.input_tipeBayar
            def input_noReff = params.input_noReff
            def pinput_reffDate = params.input_reffDate
            def pinput_rcvDate = params.input_rcvDate
            def pinput_rcvHour = params.input_rcvHour
            def pinput_rcvMinuite = params.input_rcvMinuite
            Date input_reffDate = dfSimple.parse(pinput_reffDate)
            Date input_rcvDate = df.parse(pinput_rcvDate + " " + pinput_rcvHour + ":" + pinput_rcvMinuite)
            def pilihVendor = params.pilihVendor
            Vendor input_vendor = Vendor.get(params.input_vendor)

            CompanyDealer input_companyDealer = CompanyDealer.get(params.input_companyDealer)

            String qty = params.qty
            String qty2 = params.qty2
            String[] qtys = qty.split("&")
            String[] qty2s = qty2.split("&")


            GoodsReceive goodsReceive


            if (params.goodsReceive_id) {
                goodsReceive = GoodsReceive.findByT167IDAndCompanyDealer(params.goodsReceive_id,session?.userCompanyDealer)
                goodsReceive?.lastUpdProcess = "UPDATE"
            } else {
                //cek jika noReff sudah ada maka error
                def c = GoodsReceive.createCriteria()
                def results = c.list() {
                    eq("companyDealer",session.userCompanyDealer)
                    ilike("t167NoReff", input_noReff )
                }
                if (results && results.size() > 0) {
                    def c2 = GoodsReceive.createCriteria()
                    def jumSama = c2.list() {
                        eq("companyDealer",session.userCompanyDealer)
                        ilike("t167NoReff", input_noReff + ".%" )
                    }
                    input_noReff = input_noReff + "." + (jumSama.size()+1)
                }

                goodsReceive = new GoodsReceive()
                goodsReceive?.t167ID = GoodsReceive.generateCode()
                goodsReceive?.dateCreated = datatablesUtilService?.syncTime()
                goodsReceive?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                goodsReceive?.lastUpdProcess = "INSERT"
                goodsReceive?.companyDealer = session?.userCompanyDealer

            }
            if(pilihVendor=='0'){
                goodsReceive?.companyDealerSupply = input_companyDealer
            }else{
                goodsReceive?.vendor = input_vendor
            }
            goodsReceive?.t167NoReff = input_noReff
            goodsReceive?.t167TglReff = input_reffDate
            goodsReceive?.t167TglJamReceive = input_rcvDate
            goodsReceive?.t167Ppn = input_ppn as Integer
            goodsReceive?.t167TipeBayar = input_tipeBayar

            goodsReceive?.t167PetugasReceive = org.apache.shiro.SecurityUtils.subject.principal.toString()
            goodsReceive?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            goodsReceive?.lastUpdated = datatablesUtilService?.syncTime()
            goodsReceive?.staDel= '0'

            goodsReceive?.save(flush: true)
            goodsReceive.errors.each{ println "DATA ERROR " : it }
            int i = 1;  //dimulai dari i = 1 , karena array ke-0=""
            def totalBiaya = 0
            oList.each {
                String[] parsed = qtys[i].split("=")
                String[] parsed2 = qty2s[i].split("=")
                def quantity1 = 0.0
                def quantity2 = 0.0
                if (parsed[1] != null && !parsed[1].equals(""))
                    quantity1 = Double.valueOf(parsed[1] as String)
                if (parsed2[1] != null && !parsed2[1].equals(""))
                    quantity2 = Double.valueOf(parsed2[1] as String)
                def goodsReceiveDetail = new GoodsReceiveDetail()
                Invoice inv
                PODetail poDetail
                if(pilihVendor=='0'){
                    def partsTransferStok= PartsTransferStok?.findById(it?.id as Long)
                    def npbd= NotaPesananBarangDetail?.findById(it?.id as Long)

                    if(partsTransferStok){

                        goodsReceiveDetail.updatedBy = partsTransferStok.nomorPO
                        goodsReceiveDetail.goods = partsTransferStok.goods
                        goodsReceiveDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                        goodsReceiveDetail.dateCreated = datatablesUtilService?.syncTime()
                        goodsReceiveDetail.companyDealer = session?.userCompanyDealer

                        def ubahTransferStok = PartsTransferStok.get(it.id)
                        ubahTransferStok?.staSudahInput = '1'
                        ubahTransferStok?.save(flush: true)
                    }
                    if(npbd){

                        goodsReceiveDetail.goods = npbd.goods
                        goodsReceiveDetail.staNPB = npbd.id
                        goodsReceiveDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    }
                }else if (params.goodsReceive_id) {
                    goodsReceiveDetail = GoodsReceiveDetail.findById("" + it.id)
                } else {
                    simpan = false
                    poDetail = PODetail.findById(Long.valueOf("" + it.id))
                    inv = Invoice.findByGoodsAndPoAndCompanyDealer(poDetail.validasiOrder?.requestDetail?.goods,poDetail.po,session?.userCompanyDealer)
                    if(!GoodsReceiveDetail.findByPoDetail(poDetail)){
                        goodsReceiveDetail.poDetail = poDetail
                        simpan = true
                    }
                    goodsReceiveDetail.invoice =  inv
                    goodsReceiveDetail.goods = poDetail.validasiOrder?.requestDetail?.goods
                    goodsReceiveDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    goodsReceiveDetail.dateCreated = datatablesUtilService?.syncTime()
                    goodsReceiveDetail.companyDealer = session?.userCompanyDealer

                }

                goodsReceiveDetail.goodsReceive = goodsReceive
                goodsReceiveDetail.t167Qty1Reff = quantity1
                goodsReceiveDetail.t167Qty2Reff = 0
                goodsReceiveDetail.t167Qty1Issued = quantity2
                goodsReceiveDetail.t167Qty2Issued = 0
                goodsReceiveDetail.t167Qty1Rusak = 0
                goodsReceiveDetail.t167Qty2Rusak = 0
                goodsReceiveDetail.t167Qty1Salah = 0
                goodsReceiveDetail.t167Qty2Salah = 0
                goodsReceiveDetail.t167CIF = 0
                goodsReceiveDetail.t167ImportDuty = 0
                goodsReceiveDetail.t167Unloading = 0
                goodsReceiveDetail.t167WHStorage = 0
                goodsReceiveDetail.t167Trans = 0
                goodsReceiveDetail.t167Other = 0
                goodsReceiveDetail.t167LandedCost = 0
                goodsReceiveDetail.t167RetailPrice = inv ? inv.t166RetailPrice : 0
                goodsReceiveDetail.t167Disc = inv ? inv.t166Disc : 0
                goodsReceiveDetail.t167NetSalesPrice = inv ? inv.t166NetSalesPrice : 0
                goodsReceiveDetail.staDel = '0'
                goodsReceiveDetail.lastUpdProcess = "INSERT"
                goodsReceiveDetail.lastUpdated = datatablesUtilService?.syncTime()
                if(simpan == true){
                    goodsReceiveDetail.save(flush: true)
                }
                goodsReceiveDetail.errors.each{ println "goodsReceiveDetail ==> " + it }
                i = i + 1;

                def klasifikasiGoods = KlasifikasiGoods.findByGoods(goodsReceiveDetail.goods)
                def purpose = 1
                if (klasifikasiGoods != null) {
                    def franc = klasifikasiGoods.franc.m117NamaFranc
                    if (franc != null) {
                        if (franc.toLowerCase().equals("toyota")) purpose = 1
                        else if (franc.toLowerCase().equals("parts campuran")) purpose =2
                        else if (franc.toLowerCase().equals("parts bahan")) purpose = 3
                    }
                }

                totalBiaya += goodsReceiveDetail.t167NetSalesPrice

            }
            if(pilihVendor=='1'){
                //add to Collaction167
                try {
                    def totalPPn = totalBiaya * ((input_ppn as Integer) / 100)
                    def totalBayar = totalBiaya?.toString()?.toBigDecimal() + totalPPn
                    def duedate = new Date()
                    duedate.setMonth(duedate.getYear()+1)
                    def collectionInstance = new Collection167()
                    collectionInstance.companyDealer = session.userCompanyDealer
                    collectionInstance.collectionDate = new Date()
                    collectionInstance.amount = totalBayar
                    collectionInstance.dueDate  = duedate
                    collectionInstance.paidAmount = 0
                    collectionInstance.paymentStatus = "BELUM"
                    collectionInstance.staDel = "0"
                    collectionInstance.subLedger = input_vendor?.id as String
                    collectionInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    collectionInstance.alasanTdkBayar = ""
                    collectionInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    collectionInstance.lastUpdProcess  = "INSERT"
                    collectionInstance.goodsReceive = goodsReceive
                    collectionInstance.subType = SubType.findBySubTypeLike("%VENDOR%")
                    collectionInstance.dateCreated = datatablesUtilService?.syncTime()
                    collectionInstance.lastUpdated =  datatablesUtilService?.syncTime()
                    collectionInstance.save(flush: true)
                    collectionInstance.errors.each {
                        println "COLLECTION FROM VENDOR :: " + it
                    }
                }catch(Exception e){

                }
            }else{
                try {
                    def totalPPn = totalBiaya * ((input_ppn as Integer) / 100)
                    def totalBayar = totalBiaya?.toString()?.toBigDecimal() + totalPPn
                    def duedate = new Date()
                    duedate.setMonth(duedate.getYear()+1)
                    def collectionInstance = new Collection167()
                    collectionInstance.companyDealer = session.userCompanyDealer
                    collectionInstance.collectionDate = new Date()
                    collectionInstance.amount = totalBayar
                    collectionInstance.dueDate  = duedate
                    collectionInstance.paidAmount = 0
                    collectionInstance.paymentStatus = "BELUM"
                    collectionInstance.staDel = "0"
                    collectionInstance.subLedger = input_vendor?.id as String
                    collectionInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    collectionInstance.alasanTdkBayar = ""
                    collectionInstance.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    collectionInstance.lastUpdProcess  = "INSERT"
                    collectionInstance.goodsReceive = goodsReceive
                    collectionInstance.subType = SubType.findBySubTypeLike("%CABANG%")
                    collectionInstance.dateCreated = datatablesUtilService?.syncTime()
                    collectionInstance.lastUpdated =  datatablesUtilService?.syncTime()
                    collectionInstance.save(flush: true)
                    collectionInstance.errors.each {
                        println "COLLECTION :: " + it
                    }
                }catch(Exception e){

                }
            }
            if((input_tipeBayar=="Kredit" && pilihVendor=='1') && simpan == true){

                // add to journal PBB
                params.transactionType = input_tipeBayar
                params.supplier = input_vendor.m121Nama
                params.vendor = input_vendor
                params.idVendor = input_vendor?.id
                params.usage = 1
                params.ppn = input_ppn as Integer
                params.purpose = 1
                /*params.totalBiaya = goodsReceiveDetail.goods?.goodsHargaBeli?.t150Harga*/
                params.totalBiaya = totalBiaya
                params.goodsReceive = goodsReceive?.t167NoReff
                params.docNumber = goodsReceive?.t167NoReff
                params.idGoodsReceive = goodsReceive?.id
                params.rk = Vendor.findById(input_vendor.id)?.m121NoRekBank2
                PPBJournalService.createJournal(params)
            }
            if((input_tipeBayar=="Kredit" && pilihVendor=='0') && simpan == true ){

                params.goodsReceive = goodsReceive.id
                params.noReferensi = goodsReceive.t167NoReff
                journalRkNpbService.createJournal(params)
            }
            //render "ok"
            redirect(controller: 'goodsReceive', action:'list')
        }
    }

    def tambahParts() {
        def fa = FA.read(params.fa)

        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << it }
        session.selectedParts = oList
        render "ok"
    }

    def addModal(Integer max) {
        def input_vendor = params.input_vendor
        def input_companyDealer = params.input_companyDealer
        def pilihVendor = params.pilihVendor
        [input_vendor: params.input_vendor,pilihVendor:pilihVendor,input_companyDealer:input_companyDealer]
    }

    def inputGoodsReceiveDatatablesList() {
        render goodsReceiveService.inputGoodsReceiveDatatablesList(params) as JSON
    }
    def addModalDatatablesList() {
        def input_vendor = params.input_vendor
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        if(params.pilihVendor=='1'){
     		def exist = []

     		if(params."sCriteria_exist"){

     			JSON.parse(params."sCriteria_exist").each{
     				exist << it
     			}
     		}
             def ret
             def propertiesToRender = params.sColumns.split(",")
             def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
             def sortProperty = propertiesToRender[params.iSortCol_0 as int]
             def x = 0

             session.exportParams = params

             def c = PODetail.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                po{
                    eq("companyDealer",session?.userCompanyDealer);
                    vendor{
                        eq("id", input_vendor as Long)
                    }
                    eq("t164StaOpenCancelClose",POStatus.OPEN)
                }

                validasiOrder{
                    requestDetail{
                        goods{
                            if (params."sCriteria_kodeParts"){
                                ilike("m111ID","%"+params."sCriteria_kodeParts"+"%")
                            }
                        }
                    }
                }

                validasiOrder{
                    requestDetail{
                        goods{
                            if (params."sCriteria_namaParts"){
                                ilike("m111Nama","%"+params."sCriteria_namaParts"+"%")
                            }
                        }
                    }
                }

                po{
                    if (params."sCriteria_nomorPO"){
                        ilike("t164NoPO","%"+params."sCriteria_nomorPO"+"%")
                    }

                }

                po{
                    if(params?."sCriteria_tglPO"){
                        ge("t164TglPO",params?."sCriteria_tglPO")
                        lt("t164TglPO",params?."sCriteria_tglPO"+1)
                    }
                }

                validasiOrder{
                    partTipeOrder{
                        if (params."sCriteria_tipeOrder"){
                            ilike("m112TipeOrder","%"+params."sCriteria_tipeOrder"+"%")
                        }
                    }
                }


                 if(exist.size()>0){
                     not {
                         or {
                             exist.each {
                                 eq('id', it as long)
                             }
                         }
                     }
                 }

                 switch(sortProperty){
                        default:
                            order(sortProperty,sortDir)
                            break;
                    }
             }
            if (params."sCriteria_kodeParts" || params."sCriteria_namaParts" || params."sCriteria_nomorPO" ||
                    params."sCriteria_tglPO" || params."sCriteria_tipeOrder") {

            }

             def rows = []
             def nos = 0
            results.each {
                def cekInv = Invoice.findByGoodsAndPoAndCompanyDealer(it?.validasiOrder?.requestDetail?.goods,it?.po,session?.userCompanyDealer)
                 def vid = it.id
                 def vValidasiOrder = it?.validasiOrder
                 def vRequestDetail = vValidasiOrder?.requestDetail
                 def vGoods = vRequestDetail?.goods
                 def vPartTipeOrder = vValidasiOrder?.partTipeOrder
                 def sdhAda = GoodsReceiveDetail?.findByPoDetail(it)?.t167Qty1Issued?GoodsReceiveDetail?.findByPoDetail(it)?.t167Qty1Issued:0
                 def vGoodMId = vGoods?.m111ID
                 def vGoodName = vGoods?.m111Nama
                 def vTipeOrder = vPartTipeOrder?.m112TipeOrder
                    if(cekInv){
                     nos = nos + 1
                     rows << [

                             id: vid,

                             norut:nos,

                             kodePart: vGoodMId,  //it.validasiOrder?.requestDetail?.goods.m111ID

                             namaPart: vGoodName, //it.validasiOrder?.requestDetail?.goods?.m111Nama,

                             tipeOrder: vTipeOrder,      //it.validasiOrder?.partTipeOrder?.m112TipeOrder,

                             nomorPO: it.po?.t164NoPO,

                             tglPO: it?.po?.t164TglPO.format(dateFormat),

                             qtyIssue: vValidasiOrder?.requestDetail?.t162Qty1 - sdhAda

                     ]
                    }
             }
             ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
             render ret as JSON
            }else{
                def rows = []
                def ret
                def exist = []

                if(params."sCriteria_exist"){

                    JSON.parse(params."sCriteria_exist").each{
                        exist << it
                    }
                }
                def c = PartsTransferStok.createCriteria()
                def results = c.list {
                        eq("companyDealerTujuan",CompanyDealer.findById(params.input_companyDealer as Long))
                        eq("companyDealer",session.userCompanyDealer)
                        eq("cekBaca",'1')
                        eq("cekKetersediaan",'1')
                        eq("staSudahInput",'0')
                    if(exist.size()>0){
                        not {
                            or {
                                exist.each {
                                    eq('id', it as long)
                                }
                            }
                        }
                    }
                }
                def nos = 0

                if(results.size()!=0){
                    results.each {
                        nos = nos + 1
                        rows << [
                                id: it.id,

                                norut:nos,

                                kodePart: it.goods.m111ID,  //it.validasiOrder?.requestDetail?.goods.m111ID

                                namaPart: it.goods.m111Nama, //it.validasiOrder?.requestDetail?.goods?.m111Nama,

                                tipeOrder: '-',      //it.validasiOrder?.partTipeOrder?.m112TipeOrder,

                                nomorPO: it.nomorPO,

                                tglPO: PO.findByT164NoPOLikeAndCompanyDealer("%"+it.nomorPO+"%",session?.userCompanyDealer)?.t164TglPO.format(dateFormat),

                                qtyIssue: it?.qtyParts

                        ]
                    }
                }

                    def c2 = PartsTransferStok.createCriteria()
                    def results2 = c2.list {
                        eq("companyDealer",session.userCompanyDealer)
                        eq("companyDealerTujuan",CompanyDealer.findById(params.input_companyDealer as Long))
                        eq("cekBaca",'-')
                        eq("staSudahInput",'0')
                        if(exist.size()>0){
                            not {
                                or {
                                    exist.each {
                                        eq('id', it as long)
                                    }
                                }
                            }
                        }
                    }
                    if(results2.size()!=0){
                            results2.each {
                                nos = nos + 1
                                rows << [
                                        id: it.id,

                                        norut:nos,

                                        kodePart: it.goods.m111ID,  //it.validasiOrder?.requestDetail?.goods.m111ID

                                        namaPart: it.goods.m111Nama, //it.validasiOrder?.requestDetail?.goods?.m111Nama,

                                        tipeOrder: '-',      //it.validasiOrder?.partTipeOrder?.m112TipeOrder,

                                        nomorPO: it.nomorPO,

                                        tglPO: PO.findByT164NoPOLikeAndCompanyDealer("%"+it.nomorPO+"%",session?.userCompanyDealer)?.t164TglPO.format(dateFormat),

                                        qtyIssue: it?.qtyParts
                                ]
                            }
                    }

                    def c3 = NotaPesananBarangDetail.createCriteria()
                    def results3 = c3.list {
                        notaPesananBarang{
                            eq("cabangPengirim",session.userCompanyDealer)
                            or{
                                and{
                                    eq("cabangDiteruskan",CompanyDealer.findById(params.input_companyDealer as Long))
                                    eq("cabangDiteruskanSta","1")
                                }
                                and{
                                    eq("cabangJakarta",CompanyDealer.findById(params.input_companyDealer as Long))
                                    eq("ptgsNPBJKTSta","1")
                                }
                            }

                        }
                        if(exist.size()>0){
                            not {
                                or {
                                    exist.each {
                                        eq('id', it as long)
                                    }
                                }
                            }
                        }
                    }
                    if(results3.size()!=0){
                        results3.each {
                            if(!GoodsReceiveDetail.findByStaNPBAndCompanyDealer(it.id,session?.userCompanyDealer)){

                                nos = nos + 1
                                rows << [
                                        id: it.id,

                                        norut:nos,

                                        kodePart: it.goods.m111ID,  //it.validasiOrder?.requestDetail?.goods.m111ID

                                        namaPart: it.goods.m111Nama, //it.validasiOrder?.requestDetail?.goods?.m111Nama,

                                        tipeOrder: '-',      //it.validasiOrder?.partTipeOrder?.m112TipeOrder,

                                        nomorPO: '-',

                                        tglPO: '-',

                                        qtyIssue: it?.jumlah
                                ]
                            }

                        }
                    }
                    ret = [sEcho: params.sEcho, iTotalRecords: results2.size(), iTotalDisplayRecords: results2.size(), aaData: rows]

                render ret as JSON
            }
    }
    def detailVendor(){
        def hasil=""

        if(params.vendorData){
            def rec = Vendor.createCriteria().get {
                eq("staDel","0")
                ilike("m121Nama",params.vendorData)
                maxResults(1);
            }
            if(rec){
                hasil = rec.id.toString()
            }
        }
        render hasil
    }
}