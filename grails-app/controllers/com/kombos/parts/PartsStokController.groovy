package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class PartsStokController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def partsStokService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render partsStokService.datatablesList(params) as JSON
    }

    def create() {
        def result = partsStokService.create(params)

        if (!result.error)
            return [partsStokInstance: result.partsStokInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.staDel ='0'
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = partsStokService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PartsStok", result.partsStokInstance.id])
            redirect(action: 'show', id: result.partsStokInstance.id)
            return
        }

        render(view: 'create', model: [partsStokInstance: result.partsStokInstance])
    }

    def show(Long id) {
        def result = partsStokService.show(params)

        if (!result.error)
            return [partsStokInstance: result.partsStokInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = partsStokService.show(params)

        if (!result.error)
            return [partsStokInstance: result.partsStokInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = partsStokService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PartsStok", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [partsStokInstance: result.partsStokInstance.attach()])
    }

    def delete() {
        def result = partsStokService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PartsStok", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PartsStok, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def printStock(){
        //formatBuntut = "application/vnd.ms-excel";
        List<JasperReportDef> reportDefList = []
        def data = ["-","PARTS TOYOTA","PARTS CAMPURAN","BAHAN"]
        (1..3).each{
            params.klasifikasi = data.get(it)
            params.nomor = it
            def reportData = calculateReportData(params)
            def reportDef = new JasperReportDef(name:'partStock.jasper',
                    fileFormat:params.format=="xls"?JasperExportFormat.XLS_FORMAT : JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("stok_",params.format=="xls"?".xls":".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        if(params.format=="xls"){
            response.setHeader("Content-Type", "application/vnd.ms-excel")
        }else {
            response.setHeader("Content-Type", "application/pdf")
        }
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculateReportData(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();

        def results = PartsStok.createCriteria().list {
            eq("companyDealer",session.userCompanyDealer)
            eq("staDel","0")
            gt("t131Qty1",0.toDouble())
            goods{
                order("m111Nama","asc")
            }
        }

        int count = 0
        results.each {
            def tampil = false
            def tampil2 = false
            def klasifikasi = "-"
            try {
                klasifikasi = KlasifikasiGoods.findByGoods(it.goods)?.franc?.m117NamaFranc?.toUpperCase()
            }catch (Exception e){
            }
            if(params.nomor==1){
                if(klasifikasi?.contains("TOYOTA")){
                    tampil = true
                }
            }else if(params.nomor==2){
                if(klasifikasi?.contains("CAMPURAN")){
                    tampil = true
                }
            }else if(params.nomor==3){
                if(!klasifikasi?.contains("PARTS")){
                    tampil = true
                }
            }
            def goods = GoodsReceiveDetail.findAllByGoodsAndCompanyDealer(it.goods,session.userCompanyDealer)
            goods.sort {
                it.dateCreated
            }
            def pembelian = "-"
            if(goods){
                pembelian = goods.last()?.dateCreated
            }
            def selisih = (new Date() - (pembelian=="-"? (new Date()-1000) :pembelian))
            def data2 = selisih.intValue() / 30
            if(params.status=="1"){
                if(data2.intValue() < 3 ){
                    tampil2 = true
                }
            }else if(params.status=="2"){
                if(data2.intValue() >= 3 && data2.intValue() <= 6 ){
                    tampil2 = true
                }
            }else if(params.status=="3"){
                if(data2.intValue() > 6 ){
                    tampil2 = true
                }
            }else{
                tampil2 = true
            }
            if(tampil == true && tampil2==true){
                def hargaBeli = GoodsHargaBeli.findByCompanyDealerAndGoodsAndStaDel(session.userCompanyDealer,it.goods,'0')
                def data = [:]
                count = count + 1
                data.put("CompanyDealer",session.userCompanyDealer.m011NamaWorkshop)
                data.put("klasifikasi",params.klasifikasi)
                data.put("noUrut", count as String)
                data.put("kodeGoods",it.goods?.m111ID)
                data.put("namaGoods",it.goods?.m111Nama)
                data.put("qtyIssued",it.t131Qty1 as String)
                data.put("satuan",it.goods.satuan.m118Satuan1)
                data.put("hargaBeli",hargaBeli? new Konversi().toRupiah(hargaBeli.t150Harga):"0")
                data.put("pembelian",pembelian!="-"? pembelian.format("dd/MM/yyyy"):"-")
                reportData.add(data)
            }
        }

        return reportData
    }
}
