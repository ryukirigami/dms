package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class ETAPOController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def ETAPOService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        params?.companyDealer = session?.userCompanyDealer
        render ETAPOService.datatablesList(params) as JSON
    }


    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
//        Date tglId = Date.parse(dateFormat,params.tanggalRequest)

       [ tanggalRequest : new Date(), idTable: new Date().format("yyyyMMddhhmmss"), nomorPO: params.t164NoPO]
    }
    def subsublist() {
        [goods: params.kodePart, idTable: new Date().format("yyyyMMddhhmmss"), nomorPO: params.nomorPO]
    }

    def datatablesSubList() {
        render ETAPOService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {
        render ETAPOService.datatablesSubSubList(params) as JSON
    }

    def create() {
        def result = ETAPOService.create(params)

        if(!result.error)
            return [POInstance: result.POInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = ETAPOService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PO", result.POInstance.id])
            redirect(action:'show', id: result.POInstance.id)
            return
        }

        render(view:'create', model:[POInstance: result.POInstance])
    }

    def show(Long id) {
        def result = ETAPOService.show(params)

        if(!result.error)
            return [ POInstance: result.POInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = ETAPOService.show(params)

        if(!result.error)
            return [ POInstance: result.POInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = ETAPOService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PO", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[POInstance: result.POInstance.attach()])
    }

    def delete() {
        def result = ETAPOService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PO", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PO, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def tambahETA(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def idPO = params.idPO
        def po = PO.findByT164NoPO(idPO)
        def namaVendor = po.vendor.m121Nama?po.vendor.m121Nama : ""
        def nomorPO = po.t164NoPO?po.t164NoPO:""
        def tanggalPO = po.t164TglPO?po.t164TglPO.format(dateFormat) : ""
        def poDetail = PODetail.findAllByPo(po)
        def goodness = []
        def qtyIssue = []
        def issue = 0
        poDetail.each {
            goodness << it.validasiOrder.requestDetail.goods
            qtyIssue << it.validasiOrder.requestDetail.t162Qty1
        }

        [po : po, namaVendor : namaVendor, nomorPO : nomorPO, tanggalPO : tanggalPO, goods : goodness,issue:qtyIssue]

    }

    def saveETA(){

       def idPO = params.idPO
       def qtyETA = params.qtyETA
       def jamETA = params.jamETA
       def menitETA = params.menitETA
       def idGoods = params.idGoods

       def dayETA = params.tglETA_day
       def monthETA = params.tglETA_month
       def yearETA = params.tglETA_year


       def dateETA = getDateETA(dayETA, monthETA, yearETA, jamETA, menitETA)

       def po = PO.findByT164NoPO(idPO)
       def goods = Goods.get(idGoods)


       ETA eta = new ETA()
       eta.po = po
       eta.goods = goods
       eta.t165ETA = dateETA
       eta.t165Qty1 = Double.parseDouble(qtyETA)

       eta.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
       eta.lastUpdProcess = "INSERT"
       eta.t165xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
       eta.t165xNamaDivisi = org.apache.shiro.SecurityUtils.subject.principal.toString()
       eta.staDel = "0"
        eta.dateCreated = datatablesUtilService?.syncTime()
        eta.lastUpdated = datatablesUtilService?.syncTime()

       eta.save(flush : true)

        redirect (action:"list")
    }


    def getDateETA(dayETA, monthETA, yearETA, jamETA, menitETA){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm")
        def tglETA = dayETA + "-"+monthETA+"-"+yearETA

        def dateUnit = tglETA + ' ' + jamETA + ':'+ menitETA
        Date date = df.parse(dateUnit)
        return date

    }

    def cekIssue(){
        def idPO = params.idPo
        def po = PO.findByT164NoPO(idPO)
        def poDetail = PODetail.findAllByPo(po)
        def issue = 0
        def vrg = ""
        def ids = ""
        poDetail.each {
            vrg = it.validasiOrder.requestDetail.goods.id.toString()
            ids = params.id as String
            if(vrg == ids){
                issue = it.validasiOrder.requestDetail.t162Qty1
            }
        }
        render issue
    }


}
