package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SCCController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def generateCodeService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

    def list(Integer max) {
    }
	
	def datatablesList() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		
		def ret
		def propertiesToRender = params.sColumns.split(",")
		def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
		def sortProperty = propertiesToRender[params.iSortCol_0 as int]
		def x = 0
		
		session.exportParams=params
		
		def c = SCC.createCriteria()
		def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
			
			if(params."sCriteria_m113TglBerlaku"){
				ge("m113TglBerlaku",params."sCriteria_m113TglBerlaku")
				lt("m113TglBerlaku",params."sCriteria_m113TglBerlaku" + 1)
			}
	
			if(params."sCriteria_m113KodeSCC"){
				ilike("m113KodeSCC","%" + (params."sCriteria_m113KodeSCC" as String) + "%")
			}
	
			if(params."sCriteria_m113NamaSCC"){
				ilike("m113NamaSCC","%" + (params."sCriteria_m113NamaSCC" as String) + "%")
			}
	
			if(params."sCriteria_m113StaRPP"){
				ilike("m113StaRPP","%" + (params."sCriteria_m113StaRPP" as String) + "%")
			}
	
			if(params."sCriteria_m113Ket"){
				ilike("m113Ket","%" + (params."sCriteria_m113Ket" as String) + "%")
			}
	
			if(params."sCriteria_staDel"){
				ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
			}

			if(params."sCriteria_m113ID"){
				eq("m113ID",params."sCriteria_m113ID")
			}
	
			if(params."sCriteria_createdBy"){
				ilike("createdBy","%" + (params."sCriteria_createdBy" as String) + "%")
			}
	
			if(params."sCriteria_updatedBy"){
				ilike("updatedBy","%" + (params."sCriteria_updatedBy" as String) + "%")
			}
	
			if(params."sCriteria_lastUpdProcess"){
				ilike("lastUpdProcess","%" + (params."sCriteria_lastUpdProcess" as String) + "%")
			}

			
			switch(sortProperty){
				default:
					order(sortProperty,sortDir)
					break;
			}
		}
		
		def rows = []
		
		results.each {
			rows << [
			
						id: it.id,
			
						m113TglBerlaku: it.m113TglBerlaku?it.m113TglBerlaku.format(dateFormat):"",
			
						m113KodeSCC: it.m113KodeSCC,
			
						m113NamaSCC: it.m113NamaSCC,
			
						m113StaRPP: it.m113StaRPP,
			
						m113Ket: it.m113Ket,
			
						staDel: it.staDel,
			
						m113ID: it.m113ID,
			
						createdBy: it.createdBy,
			
						updatedBy: it.updatedBy,
			
						lastUpdProcess: it.lastUpdProcess,
			
			]
		}
		
		ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]
		
		render ret as JSON
	}

	def create() {
		[SCCInstance: new SCC(params)]
	}

	def save() {
		def SCCInstance = new SCC(params)
        SCCInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        SCCInstance?.lastUpdProcess = "INSERT"
        SCCInstance?.dateCreated = datatablesUtilService?.syncTime()
        SCCInstance?.lastUpdated = datatablesUtilService?.syncTime()
        SCCInstance?.setStaDel ('0')
        def cek = SCC.createCriteria().list() {
            and{
                eq("m113TglBerlaku",SCCInstance.m113TglBerlaku)
                eq("m113NamaSCC",SCCInstance.m113NamaSCC?.trim(), [ignoreCase: true])
                eq("m113StaRPP",SCCInstance.m113StaRPP)
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [SCCInstance: SCCInstance])
            return
        }
        SCCInstance?.m113KodeSCC = generateCodeService.codeGenerateSequence("M113_KodeSCC",session.userCompanyDealer)
		if (!SCCInstance.save(flush: true)) {
			render(view: "create", model: [SCCInstance: SCCInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'SCC.label', default: 'SCC'), SCCInstance.id])
		redirect(action: "show", id: SCCInstance.id)
	}

	def show(Long id) {
		def SCCInstance = SCC.get(id)
		if (!SCCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'SCC.label', default: 'SCC'), id])
			redirect(action: "list")
			return
		}

		[SCCInstance: SCCInstance]
	}

	def edit(Long id) {
        params?.dateCreated = datatablesUtilService?.syncTime()
        params?.lastUpdated = datatablesUtilService?.syncTime()
		def SCCInstance = SCC.get(id)
		if (!SCCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'SCC.label', default: 'SCC'), id])
			redirect(action: "list")
			return
		}

		[SCCInstance: SCCInstance]
	}

	def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
		def SCCInstance = SCC.get(id)
		if (!SCCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'SCC.label', default: 'SCC'), id])
			redirect(action: "list")
			return
		}

		if (version != null) {
			if (SCCInstance.version > version) {
				
				SCCInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'SCC.label', default: 'SCC')] as Object[],
				"Another user has updated this SCC while you were editing")
				render(view: "edit", model: [SCCInstance: SCCInstance])
				return
			}
		}
        def cek = SCC.createCriteria()
        def result = cek.list() {
            and{
                eq("m113TglBerlaku",params.m113TglBerlaku)
                eq("m113NamaSCC",params.m113NamaSCC?.trim(), [ignoreCase: true])
                eq("m113StaRPP",params.m113StaRPP)
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [SCCInstance: SCCInstance])
            return
        }

		SCCInstance.properties = params
        SCCInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        SCCInstance?.lastUpdProcess = "UPDATE"

		if (!SCCInstance.save(flush: true)) {
			render(view: "edit", model: [SCCInstance: SCCInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'SCC.label', default: 'SCC'), SCCInstance.id])
		redirect(action: "show", id: SCCInstance.id)
	}

	def delete(Long id) {
		def SCCInstance = SCC.get(id)
		if (!SCCInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'SCC.label', default: 'SCC'), id])
			redirect(action: "list")
			return
		}

		try {
            SCCInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            SCCInstance?.lastUpdProcess = "DELETE"
            SCCInstance?.setStaDel('1')
            SCCInstance.save(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'SCC.label', default: 'SCC'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'SCC.label', default: 'SCC'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(SCC, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
        params.dateCreated = datatablesUtilService?.syncTime()
        params.dateCreated = datatablesUtilService?.syncTime()
		try {
			datatablesUtilService.updateField(SCC, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	

}
