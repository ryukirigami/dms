package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.dao.DataIntegrityViolationException

class KlasifikasiGoodsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def dataTablesGoods() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        def x = 0


        session.exportParams = params


        def c = Goods.createCriteria()

        //mendapatkan value dari field cari
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params.sCriteria_koGo){
                ilike("m111ID", "%"+params.sCriteria_koGo+"%")
            }

            if(params.sCriteria_naGo){
                ilike("m111Nama", "%"+params.sCriteria_naGo+"%")
            }

            if(params.sCriteria_satGo){
                satuan{
                    ilike("m118Satuan1", "%"+params.sCriteria_satGo+"%")
                }
            }

            if(params.sCriteria_klsGo){
                ilike("m111Nama", "%"+params.sCriteria_klsGo+"%")
            }

        }

        def rows = []



        results.each {
            def klasifikasi = KlasifikasiGoods.findByGoods(it)?.franc?.m117NamaFranc
            rows << [

                    id: it.id,

                    kodeGoods : it.m111ID,

                    namaGoods : it.m111Nama,

                    satuan: it.satuan?.m118Satuan1,

                    klasifikasi: klasifikasi
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        def x = 0


        session.exportParams = params


        def c = KlasifikasiGoods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_companyDealer") {
                companyDealer {
                    ilike('m011NamaWorkshop',"%"+params."sCriteria_companyDealer"+"%" )
                }

                // eq("companyDealer", CompanyDealer.findByM011NamaWorkshopIlike("%"+params."sCriteria_companyDealer"+"%"))
            }

            if (params."sCriteria_goods") {
                goods{
                    ilike("m111Nama","%"+params."sCriteria_goods"+"%" )
                }
                //      eq("goods",Goods.findByM111NamaIlike("%"+params."sCriteria_goods"+"%"))
            }



            if (params."sCriteria_kodeParts") {

                goods {
                    ilike('m111ID',"%"+params."sCriteria_kodeParts"+"%" )
                }

                //   eq("goods",Goods.findByM111IDIlike("%"+params."sCriteria_kodeParts"+"%"))
            }

            if (params."sCriteria_group") {
                //        eq("group", Group.findByM181NamaGroupIlike("%"+params."sCriteria_group"+"%"))
                group{
                    ilike("m181NamaGroup","%"+params."sCriteria_group"+"%" )
                }
            }

            if (params."sCriteria_franc") {
                // eq("franc", Franc.findByM117NamaFrancIlike("%"+params."sCriteria_franc"+"%"))
                franc{
                    ilike("m117NamaFranc","%"+params."sCriteria_franc"+"%" )
                }
            }

            if (params."sCriteria_scc") {
                //  eq("scc", SCC.findByM113NamaSCCIlike("%"+params."sCriteria_scc"+"%"))
                scc{
                    ilike("m113NamaSCC", "%"+params."sCriteria_scc"+"%")
                }
            }

            if (params."sCriteria_location") {

                location{
                    ilike("m120NamaLocation","%"+params."sCriteria_location"+"%" )
                }
                //  eq("location", Location.findByM120NamaLocationIlike("%"+params."sCriteria_location"+"%"))
            }

            if (params."sCriteria_parameterICC") {
                //     eq("parameterICC", ParameterICC.findByM155NamaICCIlike("%"+params.sCriteria_parameterICC+"%"))

                parameterICC{
                    ilike("m155NamaICC", "%"+params.sCriteria_parameterICC+"%")
                }
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {

                case "location":
                    location{
                        order("m120NamaLocation", sortDir)
                    }
                    break;
                case "namaGoods":
                    goods{
                        order("m111Nama", sortDir)
                    }
                    break;
                case "kodeParts":
                    goods{
                        order("m111ID", sortDir)
                    }
                    break;
                case "scc":
                    scc{
                        order("m113NamaSCC", sortDir)
                    }
                    break;
                case "parameterICC" :
                    parameterICC{
                        order("m155NamaICC", sortDir)
                    }
                    break;
                case "m181NamaGroup" :
                    group{
                        order("m181NamaGroup", sortDir)
                    }
                    break;
                case "franc":
                    franc{
                        order("m117NamaFranc", sortDir)
                    }
                    break;

                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []



        results.each {
            rows << [

                    id: it.id,

                    kodeParts : it.goods?.m111ID,

                    companyDealer: it.companyDealer?.m011NamaWorkshop,

                    goods: it.goods?.m111Nama,

                    group: it.group?.m181NamaGroup,

                    franc: it.franc?.m117NamaFranc,

                    scc: it.scc?.m113NamaSCC,

                    location: it.location?.m120NamaLocation,

                    parameterICC: it.parameterICC?.m155NamaICC,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [klasifikasiGoodsInstance: new KlasifikasiGoods(params)]
    }

    def save() {
        def guds = null;
        def tidakAdaGoods = false;
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def klasifikasiGoodsInstance = new KlasifikasiGoods(params)
        if(params.idGoods){
            guds = Goods.findByIdAndStaDel(params.idGoods,"0")
            if(!guds){
                tidakAdaGoods=true
            }
        }else{
            tidakAdaGoods=true;
        }
        if(tidakAdaGoods){
            flash.message = "Data Goods todak ditemukan"
            render(view: "create", model: [klasifikasiGoodsInstance: klasifikasiGoodsInstance])
        }

        klasifikasiGoodsInstance?.goods = guds;
        klasifikasiGoodsInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        klasifikasiGoodsInstance?.lastUpdProcess = "INSERT"
        klasifikasiGoodsInstance.companyDealer = session.userCompanyDealer

        def duplicate = KlasifikasiGoods.findByGoods(klasifikasiGoodsInstance.goods)

        if(duplicate){
            flash.message = "Goods sudah diklasifikasikan"
            render(view: "create", model: [klasifikasiGoodsInstance: klasifikasiGoodsInstance])
            return
        }

        if (!klasifikasiGoodsInstance.save(flush: true)) {
            klasifikasiGoodsInstance?.errors.each{
                println "gagal klasifikasiGoods ==> " + it
            }
            render(view: "create", model: [klasifikasiGoodsInstance: klasifikasiGoodsInstance])
            return
        }
        klasifikasiGoodsInstance?.errors.each{
            println "gagal klasifikasiGoods ==> " + it
        }
        flash.message = message(code: 'default.created.message', args: [message(code: 'klasifikasiGoods.label', default: 'Klasifikasi Goods'), klasifikasiGoodsInstance.id])
        redirect(action: "show", id: klasifikasiGoodsInstance.id)
    }

    def show(Long id) {
        def klasifikasiGoodsInstance = KlasifikasiGoods.get(id)
        if (!klasifikasiGoodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'klasifikasiGoods.label', default: 'KlasifikasiGoods'), id])
            redirect(action: "list")
            return
        }

        [klasifikasiGoodsInstance: klasifikasiGoodsInstance]
    }

    def edit(Long id) {
        def klasifikasiGoodsInstance = KlasifikasiGoods.get(id)
        if (!klasifikasiGoodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'klasifikasiGoods.label', default: 'KlasifikasiGoods'), id])
            redirect(action: "list")
            return
        }

        [klasifikasiGoodsInstance: klasifikasiGoodsInstance]
    }

    def update(Long id, Long version) {
        def guds=null;
        def klasifikasiGoodsInstance = KlasifikasiGoods.get(id)


        if (!klasifikasiGoodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'klasifikasiGoods.label', default: 'KlasifikasiGoods'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (klasifikasiGoodsInstance.version > version) {

                klasifikasiGoodsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'klasifikasiGoods.label', default: 'KlasifikasiGoods')] as Object[],
                        "Another user has updated this KlasifikasiGoods while you were editing")
                render(view: "edit", model: [klasifikasiGoodsInstance: klasifikasiGoodsInstance])
                return
            }
        }

        def duplicate = KlasifikasiGoods.findByGoodsAndIdNotEqual(klasifikasiGoodsInstance.goods, id)

        if(duplicate){
            flash.message = "Goods sudah diklasifikasikan"
            render(view: "edit", model: [klasifikasiGoodsInstance: klasifikasiGoodsInstance])
            return
        }

        klasifikasiGoodsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        klasifikasiGoodsInstance?.lastUpdProcess = "UPDATE"

        params.lastUpdated = datatablesUtilService?.syncTime()

        klasifikasiGoodsInstance.properties = params


        if (!klasifikasiGoodsInstance.save(flush: true)) {
            render(view: "edit", model: [klasifikasiGoodsInstance: klasifikasiGoodsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'klasifikasiGoods.label', default: 'Klasifikasi Goods'), klasifikasiGoodsInstance.id])
        redirect(action: "show", id: klasifikasiGoodsInstance.id)
    }

    def delete(Long id) {
        def klasifikasiGoodsInstance = KlasifikasiGoods.get(id)
        if (!klasifikasiGoodsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'klasifikasiGoods.label', default: 'KlasifikasiGoods'), id])
            redirect(action: "list")
            return
        }

        try {
            klasifikasiGoodsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'klasifikasiGoods.label', default: 'KlasifikasiGoods'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'klasifikasiGoods.label', default: 'KlasifikasiGoods'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(KlasifikasiGoods, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(KlasifikasiGoods, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def jasperService

    def exportKlasifikasiGoods(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def jsonArray = JSON.parse(params.idKlasifikasiGoods)
        def klasifikasiGoods = []

        jsonArray.each {
            klasifikasiGoods << KlasifikasiGoods.get(it)
        }

        int count = 0
        def reportData = new ArrayList();

        klasifikasiGoods.each {
            count = count + 1

            def data = [:]
            data.put('noUrut', count)
            data.put('kodeParts', it.goods?.m111ID)
            data.put('namaParts',it.goods?.m111Nama)
            data.put('group', it.group?.m181NamaGroup)
            data.put('franc', it.franc?.m117NamaFranc)
            data.put('scc', it.scc?.m113NamaSCC)
            data.put('namaWorkshop', it.location?.m120NamaLocation)
            //   data.put('namaWorkshop', it.companyDealer?.m011NamaWorkshop)

            reportData.add(data)
        }

        def reportDef = new JasperReportDef(name:'klasifikasiGoods.jasper',
                fileFormat:JasperExportFormat.XLS_FORMAT,
                reportData: reportData
        )

        def file = File.createTempFile("klasifikasiGoods_",".xls")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDef).toByteArray())

        response.setHeader("Content-Type", "application/xls")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }

    def listGoods(){
        def res = [:]
        def opts = []
        def goods = Goods.createCriteria().list {
            eq("staDel","0")
            or{
                ilike("m111ID","%"+params.query+"%")
                ilike("m111Nama","%"+params.query+"%")
            }
            order("m111ID")
            maxResults(10);
        }
        goods.each {
            opts<<it.m111ID.trim()+" | "+it.m111Nama.trim()
        }
        res."options" = opts
        render res as JSON
    }

    def idGoods(){
        def hasil=""
        if(params.kodeGoods){
            String kode = params.kodeGoods.toString().indexOf("|")>-1 ? params.kodeGoods.toString().substring(0,params.kodeGoods.toString().indexOf("|")) : params.kodeGoods
            def goods = Goods.createCriteria().get {
                eq("staDel","0")
                eq("m111ID",kode.trim())
                maxResults(1);
            }
            if(goods){
                hasil = goods.id.toString()
            }
        }
        render hasil
    }

}