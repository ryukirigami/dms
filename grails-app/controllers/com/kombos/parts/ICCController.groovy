package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class ICCController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def ICCService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render ICCService.datatablesList(params) as JSON
    }

    def create() {
        def result = ICCService.create(params)

        if (!result.error)
            return [ICCInstance: result.ICCInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = ICCService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["ICC", result.ICCInstance.id])
            redirect(action: 'show', id: result.ICCInstance.id)
            return
        }

        render(view: 'create', model: [ICCInstance: result.ICCInstance])
    }

    def show(Long id) {
        def result = ICCService.show(params)

        if (!result.error)
            return [ICCInstance: result.ICCInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = ICCService.show(params)

        if (!result.error)
            return [ICCInstance: result.ICCInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = ICCService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["ICC", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [ICCInstance: result.ICCInstance.attach()])
    }

    def delete() {
        def result = ICCService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["ICC", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(ICC, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def hitungICC(){
        def companyDealerId =  session?.userCompanyDealer?.id

        render ICCService.hitungICC(params,companyDealerId) as JSON
    }


}
