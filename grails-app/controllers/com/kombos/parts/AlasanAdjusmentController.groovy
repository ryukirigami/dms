package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class AlasanAdjusmentController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = AlasanAdjusment.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m165ID") {
                eq("m165ID", params."sCriteria_m165ID" as Integer)
            }

            if (params."sCriteria_m165AlasanAdjusment") {
                ilike("m165AlasanAdjusment", "%" + (params."sCriteria_m165AlasanAdjusment" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m165ID: it.m165ID,

                    m165AlasanAdjusment: it.m165AlasanAdjusment,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [alasanAdjusmentInstance: new AlasanAdjusment(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def alasanAdjusmentInstance = new AlasanAdjusment(params)
        alasanAdjusmentInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        alasanAdjusmentInstance?.setLastUpdProcess("INSERT")
        alasanAdjusmentInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        def alasan = AlasanAdjusment.createCriteria().list {
            eq("m165ID",Integer.parseInt(params.m165ID))
        }
        def alasan2 = AlasanAdjusment.createCriteria().list {
            eq("m165AlasanAdjusment",params.m165AlasanAdjusment,[ignoreCase: true])
        }
        if(alasan){
            flash.message = message(code: 'duplicateaKodelasanAdjusment.label', default: 'Kode sudah digunakan')
            render(view: "create", model: [alasanAdjusmentInstance: alasanAdjusmentInstance])
            return
        }
        if(alasan2){
            flash.message = 'Data sudah ada'
            render(view: "create", model: [alasanAdjusmentInstance: alasanAdjusmentInstance])
            return
        }
        if (!alasanAdjusmentInstance.save(flush: true)) {
            render(view: "create", model: [alasanAdjusmentInstance: alasanAdjusmentInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), alasanAdjusmentInstance.id])
        redirect(action: "show", id: alasanAdjusmentInstance.id)
    }

    def show(Long id) {
        def alasanAdjusmentInstance = AlasanAdjusment.get(id)
        if (!alasanAdjusmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), id])
            redirect(action: "list")
            return
        }

        [alasanAdjusmentInstance: alasanAdjusmentInstance]
    }

    def edit(Long id) {
        def alasanAdjusmentInstance = AlasanAdjusment.get(id)
        if (!alasanAdjusmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), id])
            redirect(action: "list")
            return
        }

        [alasanAdjusmentInstance: alasanAdjusmentInstance]
    }

    def update(Long id, Long version) {
        def alasanAdjusmentInstance = AlasanAdjusment.get(id)
        if (!alasanAdjusmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (alasanAdjusmentInstance.version > version) {

                alasanAdjusmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment')] as Object[],
                        "Another user has updated this AlasanAdjusment while you were editing")
                render(view: "edit", model: [alasanAdjusmentInstance: alasanAdjusmentInstance])
                return
            }
        }
        def alasan = AlasanAdjusment.createCriteria().list {
            eq("m165ID",Integer.parseInt(params.m165ID))
        }
        def alasan2 = AlasanAdjusment.createCriteria().list {
            eq("m165AlasanAdjusment",params.m165AlasanAdjusment,[ignoreCase: true])
        }
        if(alasan){
            for(find in alasan){
                if(find.id!=id){
                    flash.message = message(code: 'duplicateaKodelasanAdjusment.label', default: 'Kode sudah digunakan')
                    render(view: "edit", model: [alasanAdjusmentInstance: alasanAdjusmentInstance])
                    return
                    break
                }
            }
        }
        if(alasan2){
            for(find in alasan2){
                if(find.id!=id){
                    flash.message = 'Data sudah ada'
                    render(view: "edit", model: [alasanAdjusmentInstance: alasanAdjusmentInstance])
                    return
                }
            }
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        alasanAdjusmentInstance.properties = params
        alasanAdjusmentInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        alasanAdjusmentInstance?.setLastUpdProcess("UPDATE")

        if (!alasanAdjusmentInstance.save(flush: true)) {
            render(view: "edit", model: [alasanAdjusmentInstance: alasanAdjusmentInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), alasanAdjusmentInstance.id])
        redirect(action: "show", id: alasanAdjusmentInstance.id)
    }

    def delete(Long id) {
        def alasanAdjusmentInstance = AlasanAdjusment.get(id)
        if (!alasanAdjusmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), id])
            redirect(action: "list")
            return
        }

        try {
            alasanAdjusmentInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            alasanAdjusmentInstance?.setLastUpdProcess("DELETE")
            alasanAdjusmentInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'alasanAdjusment.label', default: 'AlasanAdjusment'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(AlasanAdjusment, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(AlasanAdjusment, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
