package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MIPController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = MIP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m160TglBerlaku") {
                ge("m160TglBerlaku", params."sCriteria_m160TglBerlaku")
                lt("m160TglBerlaku", params."sCriteria_m160TglBerlaku" + 1)
            }

            if (params."sCriteria_m160RangeDAD") {
                eq("m160RangeDAD",Double.parseDouble (params."sCriteria_m160RangeDAD"))
            }

            if (params."sCriteria_m160OrderCycle") {
                eq("m160OrderCycle",Double.parseDouble (params."sCriteria_m160OrderCycle"))
            }

            if (params."sCriteria_m160LeadTime") {
                eq("m160LeadTime", Double.parseDouble(params."sCriteria_m160LeadTime"))
            }

            if (params."sCriteria_m160SSLeadTime") {
                eq("m160SSLeadTime", Double.parseDouble(params."sCriteria_m160SSLeadTime"))
            }

            if (params."sCriteria_m160SSDemand") {
                eq("m160SSDemand", Double.parseDouble(params."sCriteria_m160SSDemand"))
            }

            if (params."sCriteria_m160DeadStock") {
                eq("m160DeadStock", Double.parseDouble(params."sCriteria_m160DeadStock"))
            }

            if (params."sCriteria_m160ID") {
                eq("m160ID", params."sCriteria_m160ID")
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m160TglBerlaku: it.m160TglBerlaku ? it.m160TglBerlaku.format(dateFormat) : "",

                    m160RangeDAD: it.m160RangeDAD,

                    m160OrderCycle: it.m160OrderCycle,

                    m160LeadTime: it.m160LeadTime,

                    m160SSLeadTime: it.m160SSLeadTime,

                    m160SSDemand: it.m160SSDemand,

                    m160DeadStock: it.m160DeadStock,

                    m160ID: it.m160ID,

                    companyDealer: it.companyDealer,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [MIPInstance: new MIP(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def MIPInstance = new MIP(params)
        MIPInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        MIPInstance?.companyDealer = session.userCompanyDealer
        MIPInstance?.lastUpdProcess = "INSERT"
        def cek = MIP.createCriteria().list() {
            and{
                eq("m160TglBerlaku",MIPInstance.m160TglBerlaku)
                eq("m160RangeDAD",MIPInstance.m160RangeDAD)
                eq("m160OrderCycle",MIPInstance.m160OrderCycle)
                eq("m160LeadTime",MIPInstance.m160LeadTime)
                eq("m160SSLeadTime",MIPInstance.m160SSLeadTime)
                eq("m160SSDemand",MIPInstance.m160SSDemand)
                eq("m160DeadStock",MIPInstance.m160DeadStock)
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [MIPInstance: MIPInstance])
            return
        }
        
        if (!MIPInstance.save(flush: true)) {
            render(view: "create", model: [MIPInstance: MIPInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'MIP.label', default: 'MIP'), MIPInstance.id])
        redirect(action: "show", id: MIPInstance.id)
    }

    def show(Long id) {
        def MIPInstance = MIP.get(id)
        if (!MIPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'MIP.label', default: 'MIP'), id])
            redirect(action: "list")
            return
        }

        [MIPInstance: MIPInstance]
    }

    def edit(Long id) {
        def MIPInstance = MIP.get(id)
        if (!MIPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'MIP.label', default: 'MIP'), id])
            redirect(action: "list")
            return
        }

        [MIPInstance: MIPInstance]
    }

    def update(Long id, Long version) {
        params.m160RangeDAD=params.m160RangeDAD.replace(",","")
        params.m160OrderCycle=params.m160OrderCycle.replace(",","")
        params.m160LeadTime=params.m160LeadTime.replace(",","")
        params.m160SSLeadTime=params.m160SSLeadTime.replace(",","")
        params.m160SSDemand=params.m160SSDemand.replace(",","")
        params.m160DeadStock=params.m160DeadStock.replace(",","")
        params.lastUpdated = datatablesUtilService?.syncTime()
        def MIPInstance = MIP.get(id)
        if (!MIPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'MIP.label', default: 'MIP'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (MIPInstance.version > version) {

                MIPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'MIP.label', default: 'MIP')] as Object[],
                        "Another user has updated this MIP while you were editing")
                render(view: "edit", model: [MIPInstance: MIPInstance])
                return
            }
        }

        def cek = MIP.createCriteria()
        def result = cek.list() {
            and{
                eq("m160TglBerlaku",params.m160TglBerlaku)
                eq("m160RangeDAD",Double.parseDouble (params.m160RangeDAD))
                eq("m160OrderCycle",Double.parseDouble (params.m160OrderCycle))
                eq("m160LeadTime",Double.parseDouble (params.m160LeadTime))
                eq("m160SSLeadTime",Double.parseDouble (params.m160SSLeadTime))
                eq("m160SSDemand",Double.parseDouble (params.m160SSDemand))
                eq("m160DeadStock",Double.parseDouble (params.m160DeadStock))
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [MIPInstance: MIPInstance])
            return
        }

        MIPInstance.properties = params
        MIPInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        MIPInstance?.companyDealer = session.userCompanyDealer
        MIPInstance?.lastUpdProcess = "UPDATE"

        if (!MIPInstance.save(flush: true)) {
            render(view: "edit", model: [MIPInstance: MIPInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'MIP.label', default: 'MIP'), MIPInstance.id])
        redirect(action: "show", id: MIPInstance.id)
    }

    def delete(Long id) {
        def MIPInstance = MIP.get(id)
        if (!MIPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'MIP.label', default: 'MIP'), id])
            redirect(action: "list")
            return
        }

        try {
            MIPInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            MIPInstance?.lastUpdProcess = "DELETE"
            MIPInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'MIP.label', default: 'MIP'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'MIP.label', default: 'MIP'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(MIP, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(MIP, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
