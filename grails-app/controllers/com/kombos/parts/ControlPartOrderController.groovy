package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.PartsApp
import com.kombos.reception.Appointment
import grails.converters.JSON
import groovy.time.TimeCategory

import java.text.DateFormat
import java.text.SimpleDateFormat

class ControlPartOrderController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def index() {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def dataTablesSub() {
        String idTable = new Date().format("yyyyMMddhhmmss")+params.noApp
        [idApp: params.idApp, idTable: idTable]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = Appointment.createCriteria()
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat dateF = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        use(TimeCategory){
            dateAkhir = dateAkhir + 7.day
        }
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer)
            eq("saveFlag","0")
            eq("staDel","0")
            if (params."sCriteria_t017Tanggal") {
                ge("t301TglJamRencana", params."sCriteria_t017Tanggal")
            }else if(params."sCriteria_t017Tanggal2"){
                lt("t301TglJamRencana", params."sCriteria_t017Tanggal2" + 1)
            }else{
                ge("t301TglJamRencana", dateAwal)
                ge("t301TglJamRencana", dateAwal)
            }

            not{
                eq("t301StaOkCancelReSchedule","1")
            }
            order("t301TglJamRencana","asc")
        }

        def rows = []

        results.each {
            rows << [

                    idApp: it.id,

                    noApp: it?.reception?.t401NoAppointment?it?.reception?.t401NoAppointment:it?.reception?.t401NoWO,

                    tgglApp: it.t301TglJamRencana?dateF.format(it.t301TglJamRencana):""

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesSubList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params
        def appointment = Appointment.get(params.idApp.toLong())
        def c = PartsApp.createCriteria()

        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t303StaDel","0")
            eq("reception",appointment?.reception)
        }

        def rows = []

        results.each {
            def status = "-"
            def tglEta = "-"
            if(appointment?.t301TglJamRencana >= new Date()){
                tglEta = ""
                def req = Request.findByT162NoReff(appointment?.reception?.t401NoWO)
                if(req){
                    def reqD = RequestDetail.findByGoodsAndRequestAndStatus(it.goods,req,RequestStatus.VALIDASI_BELUM_APPROVAL)
                    if(reqD){
                        def val = ValidasiOrder.findByRequestDetailAndStaDelAndT163StaApproval(reqD,'0',StatusApproval.APPROVED)
                        if(val){
                            def poD = PODetail.findByValidasiOrder(val)
                            def listETA = ETA.findAllByPo(poD.po)
                            listETA.each {
                                tglEta += it.t165ETA.format("dd/MM/yyyy HH:mm") + "\n"
                            }
                        }
                    }
                }

                def pts = PartsStok.findByCompanyDealerAndStaDelAndGoods(session.userCompanyDealer,'0',it.goods)
                if(pts){
                    if(pts.t131Qty1Free>=it?.t303Jumlah1){
                        status = "Tersedia ("+pts.t131Qty1Free+")"
                    }else{
                        status = "On Going"
                    }
                }else{
                    status = "On Going"
                }
            }


            rows << [

                    id: it.id,

                    kode: it?.goods?.m111ID,

                    nama: it.goods?.m111Nama,

                    qty: it?.t303Jumlah1,

                    statusParts : status,
                    ETA : tglEta,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
}
