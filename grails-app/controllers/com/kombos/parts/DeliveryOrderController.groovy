package com.kombos.parts

import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.finance.PemakaianPartsJournalService
import com.kombos.finance.PenerimaanPenjualanJournalService
import com.kombos.finance.SubType
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class DeliveryOrderController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService = new DatatablesUtilService()

    def deliveryOrderService
    def jasperService
    def konversi = new Konversi()
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def generateNomorDO(){
        def result = [:]
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = DeliveryOrder.createCriteria()
        def results = c.list {
            eq("companyDealer",session?.userCompanyDealer)
            eq("staDel","0")
            ge("dateCreated",df.parse(currentDate))
            lt("dateCreated",df.parse(currentDate) + 1)
        }

        def m = results.size()?:0
        def reg = "R"+session?.userCompanyDealer?.m011ID
        def data = reg +"." + new Date().format("yyyyMMdd").toString() + "." + String.format('DO%04d',m+1)
//        result.value = String.format('%014d',m+1)
        result.value = data
        return result
    }
    def massdelete() {
        params.companyDealer = session?.userCompanyDealer
        def hasil = deliveryOrderService.massDelete(params)
        render "ok"
    }
    def list(Integer max) {
        [idTable : new Date().format("ddMMyyyymmss")]
    }

    def addParts() {

    }

    def formAddParts() {
        def noDo = generateNomorDO().value
        [nomorDO:noDo]
    }

    def showParts() {
        DeliveryOrder doNumber = null
        SalesOrder sorNumber = null
        def sorNumberDetail = null
        try {
            doNumber =  DeliveryOrder.get(params.id as Long)
            sorNumber =  doNumber.sorNumber
            sorNumberDetail =  SalesOrderDetail.findAllBySorNumber(sorNumber)
        }catch (Exception e){}

        [idTable : new Date().format("ddMMyyyymmss"),doNumber : doNumber, sorNumber: sorNumber,sorNumberDetail:sorNumberDetail]
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render deliveryOrderService.datatablesList(params) as JSON
    }

    def datatablesSOList() {
        session.exportParams = params

        render deliveryOrderService.datatablesSOList(params) as JSON
    }

    def create() {
        def result = deliveryOrderService.create(params)

        if (!result.error)
            return [deliveryOrderInstance: result.deliveryOrderInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def jumlahTagih = 0
        def totSebelumPPN = 0.00
        def sorNumber = SalesOrder.createCriteria().list {
            eq("companyDealer",session?.userCompanyDealer)
            eq("staDel","0")
            eq("isApprove",0);
            eq("sorNumber",params.sorNumber,[ignoreCase: true])
        }
        def cariDO = null
        if(sorNumber.size()>0){
            cariDO = DeliveryOrder.createCriteria().get {
                eq("companyDealer",session?.userCompanyDealer);
                eq("staDel","0");
                eq("sorNumber",sorNumber?.last())
                maxResults(1);
            }
        }
        if(sorNumber.size()>0 && !cariDO){
            def cekDoNumber = DeliveryOrder.createCriteria().list {
                eq("companyDealer",session?.userCompanyDealer);
                eq("staDel","0");
                eq("doNumber",params?.doNumber,[ignoreCase: true]);
            }
            if(cekDoNumber?.size()>0){
                params.doNumber = generateNomorDO().value
            }
            def deliveryOrder = new DeliveryOrder()
            deliveryOrder?.companyDealer = session.userCompanyDealer
            deliveryOrder?.doNumber = params.doNumber
            deliveryOrder?.warehouse = params.warehouse
            deliveryOrder?.owner = params.warehouse
            deliveryOrder?.limitDate = new Date().parse("dd-MM-yyyy",params.limitDate)
            deliveryOrder?.sorNumber = sorNumber.last()
            deliveryOrder?.paymentStatus = "BELUM"
            deliveryOrder?.intext = params.intext
            deliveryOrder?.staDel = "0"
            deliveryOrder.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            deliveryOrder.lastUpdProcess="INSERT"
            deliveryOrder.dateCreated = datatablesUtilService?.syncTime()
            deliveryOrder.lastUpdated = datatablesUtilService?.syncTime()
            deliveryOrder?.save(flush: true)
            deliveryOrder?.errors?.each {println it}

            SalesOrderDetail.findAllBySorNumberAndStaDel(sorNumber.last(),"0").each {
                def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.materialCode,'0',session?.userCompanyDealer)
                if(!goodsStok){
                    def partsStokService = new PartsStokService()
                    partsStokService.newStock(it.materialCode,session?.userCompanyDealer)
                    goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it?.materialCode,'0',session?.userCompanyDealer)
                }
                    def kurangStok =  PartsStok.get(goodsStok.id)
                    kurangStok?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    kurangStok?.lastUpdProcess = 'TUNAI'
                    kurangStok?.t131Qty1 = kurangStok?.t131Qty1 - it?.quantity
                    kurangStok?.t131Qty2 = kurangStok?.t131Qty2 - it?.quantity
                    kurangStok?.t131Qty1Free = kurangStok.t131Qty1Free - it?.quantity
                    kurangStok?.t131Qty2Free = kurangStok.t131Qty2Free - it?.quantity
                    kurangStok.lastUpdated = datatablesUtilService?.syncTime()
                    kurangStok?.save(flush: true)
                    kurangStok.errors.each {
                        println it
                    }

                def discTemp = it.discount
                def totalTemp = discTemp && discTemp!=0 ? (it.quantity * it.unitPrice - (it.quantity * it.unitPrice  * discTemp / 100)) : (it.quantity * it.unitPrice)
                jumlahTagih+=(totalTemp)
                totSebelumPPN+=totalTemp
            }

            def reParam = [:]
            reParam.sorNumber = sorNumber?.last()?.sorNumber
            def servis = new PemakaianPartsJournalService()
            servis.createJournalPenjualan(reParam)
            if(deliveryOrder.sorNumber.paymentType=="Kredit"){
                def ppn = deliveryOrder?.sorNumber?.ppn
                if(ppn >= 0){
                    jumlahTagih = (jumlahTagih + (ppn/100*jumlahTagih))
                }else {
                    jumlahTagih = (jumlahTagih + (10/100*jumlahTagih))
                }
                params.ppn = ppn
                params.sebelumPpn = totSebelumPPN
                params.docNumber = deliveryOrder.doNumber
                params.tipeBeli = "Kredit"
                params.totalBiaya = jumlahTagih
                params.subledger = HistoryCustomer.findByT182NamaDepan(deliveryOrder.sorNumber.customerName)?.id
                def servis2 = new PenerimaanPenjualanJournalService()
                servis2.createJournalKredit(params)
            }

            render "OK"
        }else{
            String hasil = (cariDO!=null ? "EXIST" : "GAGAL");
            render hasil

        }

    }

    def show(Long id) {
        def result = deliveryOrderService.show(params)

        if (!result.error)
            return [deliveryOrderInstance: result.deliveryOrderInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = deliveryOrderService.show(params)

        if (!result.error)
            return [deliveryOrderInstance: result.deliveryOrderInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = deliveryOrderService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["DeliveryOrder", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [deliveryOrderInstance: result.deliveryOrderInstance.attach()])
    }

    def delete() {
        def result = deliveryOrderService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["DeliveryOrder", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def listSO(){
        def res = [:]
        def opts = []
        def salesOrders = SalesOrder.createCriteria().list {
            eq("staDel","0");
            eq("isApprove",0);
            eq("companyDealer",session?.userCompanyDealer);
            ilike("sorNumber","%"+params?.query+"%");
            maxResults(10);
        }
        salesOrders.each {
            if(!DeliveryOrder.findBySorNumberAndStaDelAndCompanyDealer(it,'0',session?.userCompanyDealer)){
                opts<<it.sorNumber
            }
        }
        res."options" = opts
        render res as JSON
    }

    def detailSO(){
        def result = [:]
        if(params.sorNumber){
            def so = SalesOrder.createCriteria().list {
                eq("companyDealer",session?.userCompanyDealer)
                eq("staDel","0")
                eq("isApprove",0);
                eq("sorNumber",params.sorNumber.trim(),[ignoreCase:true])
            }

            if(so.size()>0){
                def salesOrder = so.last()
                result."hasil" = "ada"
                result."idSO" = salesOrder?.id
                result."deliveryDate" = salesOrder?.deliveryDate ? salesOrder?.deliveryDate.format("dd-MM-yyyy") : ""
                result."paymentType" = salesOrder?.paymentType
                result."tradingFor" = salesOrder?.tradingFor
                result."customerName" = salesOrder?.customerName
                result."intext" = salesOrder?.tradingFor ? (salesOrder?.tradingFor.equalsIgnoreCase("umum") ? "External" : "Internal") : ""
                render result as JSON
            }else{
                result."hasil" = "tidak"
                render result as JSON
            }
        }else{
            result."hasil" = "tidak"
            render result as JSON
        }
    }

    def printDeliveryOrder(){
        def jsonArray = JSON.parse(params.idDeliveryOrder)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'salesOrder.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )

            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("DO_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")

        response.outputStream << file.newInputStream()

    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def dor = DeliveryOrder.findById(id as Long)
        def c = SalesOrderDetail.createCriteria()
        def results = c.list {
            sorNumber{
                eq("id",dor?.sorNumber?.id as Long)
            }
            eq("staDel","0")
        }

        results.sort {
            it.materialCode.m111ID
        }

        int count = 0
        def total = 0
        results.each {
            def data = [:]
            count = count + 1
            data.put("sor",it.sorNumber?.sorNumber)
            data.put("tglPengiriman",it?.sorNumber?.deliveryDate?.format("dd/MM/YYYY"))
            data.put("dealer",session.userCompanyDealer)
            data.put("bulan",it?.sorNumber?.deliveryDate?.format("MMMM"))
            data.put("pembeli",it?.sorNumber?.tradingFor?.toUpperCase())
            data.put("nama",it?.sorNumber?.customerName?.toUpperCase())
            data.put("alamat",it?.sorNumber?.deliveryAddress)
            data.put("franc",it?.sorNumber?.salesType?.m117NamaFranc?.toUpperCase())
            data.put("catatan",it?.sorNumber?.note)
            data.put("exGudang",it?.sorNumber?.deliveryLocation?.toUpperCase())
            //
            def hargaTotal = it.unitPrice*it?.quantity
            def hargaDisc = it?.discount ? (hargaTotal * it.discount/100) : 0
            def subTotal = hargaTotal-hargaDisc
            total += subTotal
            data.put("no",count)
            data.put("kodeParts",it.materialCode.m111ID)
            data.put("namaParts",it.materialCode.m111Nama)
            data.put("qty",it?.quantity)
            data.put("disc",it?.discount)
            data.put("harga",konversi.toRupiah(it.unitPrice))
            data.put("hargaTotal",konversi.toRupiah(hargaTotal))
            data.put("jumlah",konversi.toRupiah(subTotal))

            //
            data.put("total",konversi.toRupiah(total))
            data.put("noDo",dor?.doNumber)
            data.put("ppn","PPN (" + it?.sorNumber?.ppn + "%)")
            def hargaPPn = total* (it?.sorNumber?.ppn / 100)
            data.put("hargaPPN", konversi.toRupiah(hargaPPn as Integer))
            data.put("grandTotal", konversi.toRupiah(Math.ceil(total+hargaPPn)))

            reportData.add(data)
        }

        return reportData

    }
}
