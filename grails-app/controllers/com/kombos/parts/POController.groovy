package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class POController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def POService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render POService.datatablesList(params) as JSON
	}

	def create() {
		def result = POService.create(params)

        if(!result.error)
            return [POInstance: result.POInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = POService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PO", result.POInstance.id])
            redirect(action:'show', id: result.POInstance.id)
            return
        }

        render(view:'create', model:[POInstance: result.POInstance])
	}

	def show(Long id) {
		def result = POService.show(params)

		if(!result.error)
			return [ POInstance: result.POInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = POService.show(params)

		if(!result.error)
			return [ POInstance: result.POInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = POService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PO", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[POInstance: result.POInstance.attach()])
	}

	def delete() {
		def result = POService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PO", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(PO, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
