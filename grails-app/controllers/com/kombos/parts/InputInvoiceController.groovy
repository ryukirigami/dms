package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON

class InputInvoiceController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
    def conversi = new Konversi()


	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
        params.companyDealer = session.userCompanyDealer
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def addModal(){
        [vendorId:params.vendor,idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def kodeList() {
        def res = [:]

        def result = Vendor.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.m121Nama
        }

        res."options" = opts
        render res as JSON
    }
     def req(){
         def vendor =  Vendor.findByM121Nama(params.namaVendor)
         def hasil = null
         if(vendor){
             def jsonArray = JSON.parse(params.ids)
             jsonArray.each {
                 def podetil = PODetail.get(it)
                 Invoice invoice = new Invoice()
                 String qRetail = (params."qRetail-${it}" as String).replace(",","")
                 String qNetSalePrice = (params."qNetSalePrice-${it}" as String).replace(",","")
                 String qReceive = (params."qReceive-${it}" as String).replace(",","")
                 String qDisc = (params."qDiscount-${it}" as String).replace(",","")
                 invoice.t166NoInv = params.noInvoice
                 invoice.t166TglInv = params.tanggal
                 invoice.po = podetil?.po
                 invoice.goods = podetil?.validasiOrder?.requestDetail?.goods
                 invoice.t166Qty1 = Double.valueOf(qReceive)
                 invoice.t166Qty2 = Double.valueOf(qReceive)
                 invoice.t166RetailPrice = Double.valueOf(qRetail)
                 invoice.t166Disc = Double.valueOf(qDisc)
                 invoice.t166NetSalesPrice = Double.valueOf(qNetSalePrice)
                 invoice.staDel = '0'
                 invoice.t166xNamaUser =  org.apache.shiro.SecurityUtils.subject.principal.toString()
                 invoice.t166xNamaDivisi =  User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi==null?"no division":User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString()).divisi.m012NamaDivisi
                 invoice.createdBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
                 invoice.updatedBy =  org.apache.shiro.SecurityUtils.subject.principal.toString()
                 invoice.dateCreated = datatablesUtilService?.syncTime()
                 invoice.lastUpdated = datatablesUtilService?.syncTime()
                 invoice.companyDealer = session.userCompanyDealer
                 invoice.lastUpdProcess = "INSERT"

                 invoice.save()
                 invoice.errors.each{
                     println it
                 }

                 if(invoice?.t166Disc && invoice?.t166Disc>0){
                     def harga = GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(podetil?.validasiOrder?.requestDetail?.goods,session?.userCompanyDealer,"0")
                     if(harga){
                         harga?.t150Harga = invoice.t166RetailPrice - (invoice.t166RetailPrice*invoice.t166Disc/100)
                         harga.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                         harga.lastUpdated = datatablesUtilService?.syncTime()
                         harga.save()
                     }
                 }
             }
             hasil = "yes"
         }else{
             hasil = "fail"

         }
         render hasil
     }
	def create() {
		[invoiceInstance: new Invoice(params)]
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def invoiceInstance = new Invoice(params)
		if (!invoiceInstance.save(flush: true)) {
			render(view: "create", model: [invoiceInstance: invoiceInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'invoice.label', default: 'Invoice'), invoiceInstance.id])
		redirect(action: "show", id: invoiceInstance.id)
	}

	def show(Long id) {
		def invoiceInstance = Invoice.get(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
			return
		}

		[invoiceInstance: invoiceInstance]
	}

	def edit(Long id) {
		def invoiceInstance = Invoice.get(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoice.label', default: 'Invoice'), id])
			redirect(action: "list")
			return
		}

		[invoiceInstance: invoiceInstance]
	}

	def update() {
        def res = [:]
	    Invoice invoice = Invoice.findById(new Long(params.id))
        invoice.companyDealer=session.userCompanyDealer
        invoice.t166Qty2 = new Double(params.qtyReceive)
        invoice.t166RetailPrice = new Double(params.retailPrice)
        invoice.t166Disc = new Double(params.discount)
        invoice.t166NetSalesPrice = new Double(params.netSalePrice)
        invoice.lastUpdated = datatablesUtilService?.syncTime()

        if(invoice.save()){
            res.message = "Edit Success"
        }else{
            res.message = "Edit Fail"
        }
        render res as JSON
	}

    def delete() {
        def res = [:]
        Invoice invoice = Invoice.findById(new Long(params.id))

        invoice.staDel = '1'
        invoice.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        invoice.lastUpdProcess = "DELETE"
        invoice.lastUpdated = datatablesUtilService?.syncTime()

        if(invoice.save()){
            res.message = "Delete Success"
        }else{
            res.message = "Delete Fail"
        }
        render res as JSON
    }

    def massdelete(def params){
        log.info(params.ids)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Invoice.get(it)  }

        //oList*.discard() //detach all the objects from session
        oList.each{
            it?.staDel = '1'
            it?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            it?.lastUpdProcess = "DELETE"
            it?.lastUpdated = datatablesUtilService?.syncTime()
            it.save(flush:true)
        }
    }

	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(Invoice, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def datatablesListPO() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        if(params.vendorId){
            params."sCriteria_vendor"=Vendor.findByM121Nama(params.vendorId as String)
        }
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

//        session.exportParams = params

        def c = PODetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_vendor") {
                po{
                    eq("vendor", params."sCriteria_vendor")
                    eq("t164StaOpenCancelClose",POStatus.OPEN)
                }
            }
            po{
                eq("companyDealer",session?.userCompanyDealer)
            }
            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
            switch (sortProperty) {
                case 't164NoPO' :
                    po{
                        order('t164NoPO',sortDir)
                    }
                break
                case 't164TglPO' :
                    po{
                        order('t164TglPO',sortDir)
                    }
                    break
                default:
                   order(sortProperty, sortDir)
                break;
            }
        }

        def rows = []

        results.each {
            String etaEuy = ""

           ETA.findAllByPoAndGoods(it.po,it.validasiOrder.requestDetail.goods).each {
               etaEuy += it.t165ETA
               etaEuy += ", "
           }


                rows << [

                        id: it.id,

                        t164NoPO: it.po.t164NoPO,

                        t164TglPO: it.po.t164TglPO ? it.po.t164TglPO.format(dateFormat) : "",

                        vendor: it.po.vendor.m121Nama,

                        validasiOrder: it.validasiOrder?.partTipeOrder?.m112TipeOrder ? it.validasiOrder?.partTipeOrder?.m112TipeOrder : "",

                        request: it.validasiOrder.requestDetail.request.t162NoReff,

                        goods: it.validasiOrder.requestDetail.goods.m111Nama,

                        kodePart: it.validasiOrder.requestDetail.goods.m111ID,

                        t164Qty1: it.t164Qty1,

                        t164HargaSatuan: it.t164HargaSatuan ? conversi.toRupiah(it.t164HargaSatuan) : 0,

                        t164TotalHarga: it.t164TotalHarga ? conversi.toRupiah(it.t164TotalHarga) : 0,

                        eta: etaEuy
                ]

        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesListDetail() {
        def ret

        Invoice invoice = Invoice.findById(new Long(params.id))

        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def results = Invoice.executeQuery("SELECT a FROM Invoice a where a.staDel != '1' and a.t166NoInv = '${ invoice.t166NoInv}'  order by a.id asc")

        def rows = []
        Invoice inv
        PO po
        def i = 1
        results.each {
            inv = it
            po = inv.po
            PODetail poDetail = PODetail.findByPo(po)
            rows << [
                    id: inv.id,

                    kodePart: poDetail.validasiOrder.requestDetail.goods.m111ID,

                    namaPart: poDetail.validasiOrder.requestDetail.goods.m111Nama,

                    noPO: po.t164NoPO,

                    tanggalPO: po.t164TglPO.format("dd/MM/yyyy"),

                    tipeOrder: poDetail.validasiOrder.partTipeOrder.m112TipeOrder,

                    qtyReceive: inv.t166Qty2,

                    retailPrice: inv.t166RetailPrice ? conversi.toRupiah(inv.t166RetailPrice) : 0,

                    discount: inv.t166Disc,

                    netSalePrice: inv.t166NetSalesPrice ? conversi.toRupiah(inv.t166NetSalesPrice) : 0,

                    i:i
            ]
            i++
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

        render ret as JSON
    }

    def addParts(){

    }

}
