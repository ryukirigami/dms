package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadGoodsController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def list(Integer max) {
    }
    def index() {}
    def view() {
        def goodsInstance = new Goods(params)

        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'kodeGoods',
                        'B':'namaGoods',
                        'C':'satuan',

                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        def jsonData = ""
        int jmlhDataError = 0;
        String htmlData = "",status2 = "", statusReplace = ""
        if(!uploadExcel?.empty){
            //validate content type


            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream','text/plain'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [goodsInstance: goodsInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = jobList as JSON
            Satuan satuan = null

            String status = "0", style = ""
            jobList?.each {
                  String kdGoods = it.kodeGoods.toString()
                  String namaGoods = it.namaGoods.toString()
                if((it.kodeGoods && it.kodeGoods!="") && (it.namaGoods && it.namaGoods !="") && (it.satuan && it.satuan != "")){
                    satuan = Satuan.findByM118Satuan1AndStaDel(it.satuan,'0')
                    def cek = goodsInstance.createCriteria()
                    def goods = cek.list() {
                        or{
                            eq("m111ID",kdGoods.trim(), [ignoreCase: true])
                            eq("m111Nama",namaGoods.trim(), [ignoreCase: true])
                        }
                        eq("staDel",'0')
                    }
                    if(!satuan){
                        jmlhDataError++;
                        status = "1";
                    }
                    if(goods){
                        statusReplace  = "1"
                    }
                    if(kdGoods.length() > 17 || namaGoods.length() > 50){
                        jmlhDataError++;
                        status = "1"
                        status2 = "Jumlah Karakter Terlalu Banyak"
                    }
                } else {
                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                }else if(statusReplace.equals("1")){
                    style = "style='color:blue;'"
                }else {
                    style = ""
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.kodeGoods+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.namaGoods+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.satuan+"\n" +
                        "                            </td>\n" +

                        "                        </tr>"
                status = "0"

            }
        }
        if(jmlhDataError>0){
            if(status2!=""){
                flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Panjang Karakter Melebihi Batas")
            }else{
                flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah")
            }
        } else if(statusReplace.equals("1")){
            flash.message = message(code: 'default.uploadGoods.message', default: "Ada Data Double, apakah anda akan melanjutkan penyimpanan")
        }else {
            flash.message = message(code: 'default.uploadGoods.message', default: "Read File Done")
        }

        render(view: "index", model: [goodsInstance: goodsInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError, st : statusReplace])
    }
    def upload() {
        def requestBody = request.JSON
        DateFormat df = new SimpleDateFormat("dd-MM-YYYY")
        def goodsInstance = null

        Satuan satuan = null
        requestBody.each{
            goodsInstance = new Goods()
            String kdGoods = it.kodeGoods.toString()
            String namaGoods = it.namaGoods.toString()
            satuan = Satuan.findByM118Satuan1AndStaDel(it.satuan,'0')
            if(satuan){
                def cek = goodsInstance.createCriteria()
                def goods = cek.list() {
                    eq("m111ID",kdGoods.trim(), [ignoreCase: true])
                    eq("m111Nama",namaGoods.trim(), [ignoreCase: true])
                    eq("staDel",'0')
                }
                if(goods?.size()<1){
                    goodsInstance.satuan= satuan
                    goodsInstance.m111ID = it.kodeGoods
                    goodsInstance.m111Nama = it.namaGoods
                    goodsInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    goodsInstance.lastUpdProcess = "UPLOAD"
                    goodsInstance.setStaDel('0')
                    goodsInstance.dateCreated = datatablesUtilService?.syncTime()
                    goodsInstance.lastUpdated = datatablesUtilService?.syncTime()
                    try {
                        goodsInstance.save(flush : true)
                    }catch (Exception e){
                        e.printStackTrace()
                    }
                }else{
                    goodsInstance = Goods.get(goods.first().id as Long)
                    goodsInstance.satuan= satuan
                    goodsInstance.lastUpdated = datatablesUtilService?.syncTime()
                    try {
                        goodsInstance.save(flush : true)
                    }catch (Exception e){
                        e.printStackTrace()
                    }
                }

                flash.message = message(code: 'default.uploadGoods.message', default: "Save Job Done")
            }else{
                flash.message = message(code: 'default.uploadGoods.message', default: "gagal")
            }

        }

        render(view: "index", model: [goodsInstance: goodsInstance])

    }
}
