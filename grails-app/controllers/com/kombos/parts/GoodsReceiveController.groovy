package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class GoodsReceiveController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def goodsReceiveService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sublist() {
        [noReff: params.noReff, tglReff: params.tglReff, tglJamReceive: params.tglJamReceive, petugasReceive: params.petugasReceive,
            isSelected: params.isSelected, namaVendor: params.namaVendor, VendorId: params.VendorId,idTableSub: new Date().format("yyyyMMddhhmmss")]
   	}
   	def subsublist() {
   		[idGoodsReceive: params.idGoodsReceive, noReff: params.noReff, tglReff: params.tglReff, tglJamReceive: params.tglJamReceive, petugasReceive: params.petugasReceive,
            isSelectedSub: params.isSelectedSub, namaVendor: params.namaVendor, VendorId: params.VendorId, idTableSubSub: new Date().format("yyyyMMddhhmmss")]
   	}

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render goodsReceiveService.vendorList(params) as JSON
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
   		render goodsReceiveService.datatablesSubList(params) as JSON
   	}
   	def datatablesSubSubList() {
        params.companyDealer = session?.userCompanyDealer
        render goodsReceiveService.datatablesSubSubList(params) as JSON
   	}

    def datatablesListInput() {
        params.companyDealer = session?.userCompanyDealer
        render goodsReceiveService.datatablesListInput(params) as JSON
    }
    def create() {
        [goodsReceiveInstance: new GoodsReceive(params)]
    }

    def massdeleteVendor() {
        def res = [:]
        try {
            goodsReceiveService.massDeleteVendor(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def massdeleteGoodsReceive() {
        def res = [:]
        try {
            goodsReceiveService.massDeleteGoodsReceive(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def massdeleteGoodsReceiveDetail() {
        def res = [:]
        try {
            goodsReceiveService.massDeleteGoodsReceiveDetail(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    private ArrayList createReportDetail(List<GoodsReceiveDetail> grdList) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();

        int count = 0
        grdList.each {
            def data = [nomorGRD: it?.goodsReceive?.t167ID, tglReff: it?.goodsReceive?.t167TglReff.format(dateFormat),
                    tglJamReceive: it?.goodsReceive?.t167TglJamReceive.format(dateFormat), petugasReceive: it?.goodsReceive?.t167PetugasReceive]
            count = count + 1
            log.info("nama parts : " + it?.goods?.m111Nama ? it?.goods?.m111Nama:"-")
            data.put("noUrut", count)
            data.put("kodeParts", it?.goods?.m111ID ? it?.goods?.m111ID:"-")
            data.put("namaParts", it?.goods?.m111Nama ? it?.goods?.m111Nama:"-")
            data.put("nomorPO", it?.poDetail?.po?.t164NoPO ? it?.poDetail?.po?.t164NoPO:"-")
            data.put("tglPO", it?.poDetail?.po?.t164TglPO.format(dateFormat) ? it?.poDetail?.po?.t164TglPO.format(dateFormat):"-")
            data.put("qty", it?.t167Qty1Reff ? it?.t167Qty1Reff:"-")
            data.put("satuan", it?.goods?.satuan?.m118Satuan1 ? it?.goods?.satuan?.m118Satuan1:"-")
            data.put("kodeLokasi", "-")

            reportData.add(data)
        }
        return reportData
    }

    def printBinningInstruction(){
        def idVendors
        def idGoodsReceives
        def idGoodsReceiveDetails

        if (params.idVendors) idVendors = JSON.parse(params.idVendors)
        if (params.idGoodsReceives) idGoodsReceives = JSON.parse(params.idGoodsReceives)
        if (params.idGoodsReceiveDetails) idGoodsReceiveDetails = JSON.parse(params.idGoodsReceiveDetails)

        List<JasperReportDef> reportDefList = []
        def reportData = new ArrayList();

        if (idVendors) {
            List<Long> idVends = new ArrayList<Long>();
            idVendors.each {
                idVends.add(Long.parseLong(it))
            }
            List<Vendor> vendorList = Vendor.withCriteria {
                inList('id', idVends)
            }
            List<GoodsReceive> grList = GoodsReceive.withCriteria {
                inList('vendor', vendorList)
            }
            List<GoodsReceiveDetail> grdList = GoodsReceiveDetail.withCriteria {
                inList('goodsReceive', grList)
            }
            reportData = createReportDetail(grdList)
        } else if (idGoodsReceives)  {
            List<Long> idGrs = new ArrayList<Long>();
            idGoodsReceives.each {
                idGrs.add(Long.parseLong(it))
            }
            List<GoodsReceive> grList = GoodsReceive.withCriteria {
                inList('id', idGrs)
            }
            List<GoodsReceiveDetail> grdList = GoodsReceiveDetail.withCriteria {
                inList('goodsReceive', grList)
            }
            reportData = createReportDetail(grdList)
        } else {
            List<Long> ids = new ArrayList<Long>()
            idGoodsReceiveDetails.each {
                ids.add(Long.parseLong(it))
            }

            List<GoodsReceiveDetail> grdList = GoodsReceiveDetail.withCriteria {
                inList('id', ids)
            }
            reportData = createReportDetail(grdList)
        }

        def reportDef = new JasperReportDef(name:'binningInstruction.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

        reportDefList.add(reportDef)

        def file = File.createTempFile("binningInstruction_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

}