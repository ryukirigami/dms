package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class StockINController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
    def generateCodeService
    def stockINService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = []

   	static editPermissions = []

   	static deletePermissions = []

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sublist() {
        [nomorStockIN: params.nomorStockINs, tglJamStockIN: params.tglJamStockIN, petugasStockIN: params.petugasStockIN,
        idTableSub: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render stockINService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
   		render stockINService.datatablesSubList(params) as JSON
   	}

    def create() {
        def result = stockINService.create(params)

        if (!result.error)
            return [stockINInstance: result.stockINInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = stockINService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["StockIN", result.stockINInstance.id])
            redirect(action: 'show', id: result.stockINInstance.id)
            return
        }

        render(view: 'create', model: [stockINInstance: result.stockINInstance])
    }

    def show(Long id) {
        def result = stockINService.show(params)

        if (!result.error)
            return [stockINInstance: result.stockINInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = stockINService.show(params)

        if (!result.error)
            return [stockINInstance: result.stockINInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = stockINService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["StockIN", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [stockINInstance: result.stockINInstance.attach()])
    }

    def delete() {
        def result = stockINService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["StockIN", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdeleteStockIN() {
        def res = [:]
        try {
            params.companyDealer = session?.userCompanyDealer
            stockINService.massDeleteStockIN(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
    def massdeleteStockINDetail() {
        def res = [:]
        try {
            stockINService.massDeleteStockINDetail(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
