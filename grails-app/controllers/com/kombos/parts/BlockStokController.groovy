package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Reception
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class BlockStokController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def blockStokService

    def jasperService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
	}

	def datatablesList() {
		session.exportParams=params
        params.userCompanyDealer = session.userCompanyDealer
		render blockStokService.datatablesList(params) as JSON
	}



    def datatablesSubList() {
        params.userCompanyDealer = session.userCompanyDealer
        render blockStokService.datatablesSubList(params) as JSON
    }

    def sublist() {


        [idBlockStok: params.id, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def tambah(){
        params.userCompanyDealer = session.userCompanyDealer
        def result=blockStokService.tambah(params)
        render result
    }

    def saveStok(){

        def result = blockStokService.saveStok(params,session.userCompanyDealer)

        render "sukses"

    }

    def delStok(){
        def result = blockStokService.delStok(params)
        render result
    }

	def create () {

        def result = blockStokService.create(params)

        if(!result.error)
            return [blockStokInstance: result.blockStokInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')

	}


    def datatablesBlockStokDetail (){

        render blockStokService.datatablesListDetail(params) as JSON

    }

    def dTBlockStokDetailEdit (){
        render blockStokService.dTBlockStokDetailEdit(params) as JSON

    }


	def save() {
		 def result = blockStokService.save(params,session.userCompanyDealer)


        redirect(action:'list')
	}

	def show(Long id) {
		def result = blockStokService.show(params)

		if(!result.error)
			return [ blockStokInstance: result.blockStokInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = blockStokService.show(params)

		if(!result.error)
			return [ blockStokInstance: result.blockStokInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)


		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.userCompanyDealer = session.userCompanyDealer
        blockStokService.update(params)
        render "ok"
	}

	def delete() {
		def result = blockStokService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["BlockStok", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			blockStokService.massDelete(params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def addPickingSlip(){
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Goods.get(it)  }

        oList.each{
            BlokStokDetail blokStokDetail = new BlokStokDetail();
            blokStokDetail.goods = it
            blokStokDetail.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            blokStokDetail.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            blokStokDetail.dateCreated = datatablesUtilService.syncTime()
            blokStokDetail.lastUpdated = datatablesUtilService.syncTime()
            blokStokDetail.companyDealer = session?.userCompanyDealer
            blokStokDetail.lastUpdProcess = "INSERT"
            blokStokDetail.save()
        }

        render "ok"
    }

    def printBlockStokSlip(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def jsonArray = JSON.parse(params.idBlockStoks)
        def blockStokList = []

        jsonArray.each {
            blockStokList << BlockStok.get(it)
        }

        List<JasperReportDef> reportDefs = new ArrayList<JasperReportDef>();
        blockStokList.each {
            def reportData = new ArrayList();
            def blockStok = it



            def results = BlokStokDetail.findAllByBlockStok(it)
            results.sort {
                it.goods.m111ID
            }



            int count = 0

            results.each {
                def data = [nomorSlip: blockStok?.t157ID,tanggalBlockStok: blockStok?.t157TglBlockStok.format(dateFormat), nomorWO: blockStok?.reception?.t401NoWO, tanggalWO: blockStok?.reception?.t401TanggalWO.format(dateFormat)]

                count = count + 1
                log.info("nama parts : "+it.goods?.m111Nama)
                data.put("noUrut", count)
                data.put("kodeParts", it.goods?.m111ID)
                data.put("namaParts", it.goods?.m111Nama)
                data.put("satuan",it.goods?.satuan?.m118Satuan1)
                data.put("qtyDiterima",it.t158Qty1 )
                data.put("kodeLokasi", it.location?.m120NamaLocation)
                data.put("tanggal", new Date().format(dateFormat))

                reportData.add(data)
            }


            def reportDef = new JasperReportDef(name:'blockStok.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )

           reportDefs.add(reportDef)

        }


        def file = File.createTempFile("blockStok_",".pdf")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefs).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")

        response.outputStream << file.newInputStream()


    }

    def editBlockStokDetail(){
        blockStokService.editBlockStokDetail(params)
        render "succesful edit"
    }

    def getLocations()
    {
        def location = Location.findAll()
        def locationMap = []

        location.each {
            locationMap << [
                id : it.id,
                namaLokasi : it.m120NamaLocation
            ]
        }

        render locationMap as JSON
    }

    def getLocationsOthers()
    {
        def location = Location.findAllByM120NamaLocationNotEqual(params.location)

        def locationMap = []

        location.each {
            locationMap << [
                    id : it.id,
                    namaLokasi : it.m120NamaLocation
            ]
        }

        render locationMap as JSON
    }

    def getLocationSelected()
    {
        def location = Location.findByM120NamaLocation(params.location)
        def locationMap = []

        location.each {
            locationMap << [
                    id : it.id,
                    namaLokasi : it.m120NamaLocation
            ]
        }

        render locationMap as JSON
    }
    def detailWo(){
        def hasil=""
        if(params.noWo){
            def rec = Reception.createCriteria().get {
                eq("staDel","0");
                eq("staSave","0");
                ilike("t401NoWO",params.noWo)
                maxResults(1);
            }
            if(rec){
                hasil = rec.id.toString()
            }
        }
        render hasil
    }
}
