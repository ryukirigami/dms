package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class ReturnsInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
    def generateCodeService

    def journalReturPartsService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        String staUbah = "create"
        def cari = new Returns()
        def u_tanggal = ""
        def u_vendor = ""
        if(params.noReturns!=null){
            staUbah="ubah"
            cari = Returns.findByT172ID(params.noReturns)
            u_tanggal = cari?.t172TglJamReturn
            u_vendor = cari?.vendor.m121Nama?cari?.vendor.m121Nama:""
        }
        [nomorReturns: params.noReturns, u_tanggal:u_tanggal, tanggal : new Date().format("yyyy-MM-dd"), aksi:staUbah, u_vendor : u_vendor]
    }
    def ubah(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
        def hasil = null
            def jsonArray = JSON.parse(params.ids)
            jsonArray.each {
                def binningDetail = BinningDetail.get(it)
                if(binningDetail){
                    Returns noret = Returns.findByT172IDAndGoods(params.noReturns, binningDetail.goodsReceiveDetail?.goods)
                    def returns = Returns.get(noret.id as Long)
                    def returns2 = returns
                    returns2?.t172ID = returns.t172ID
                    returns2?.t172PetugasReturn = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    returns2?.vendor = returns.vendor
                    returns2?.companyDealer = session.userCompanyDealer
                    returns2?.invoice = binningDetail.goodsReceiveDetail?.invoice
                    returns2?.goodsReceive =  binningDetail.goodsReceiveDetail?.goodsReceive
                    returns2?.stockIN = StockIN?.findById(1)
                    returns2?.goods = binningDetail.goodsReceiveDetail?.goods
                    returns2?.t172Keterangan = "ket"
                    returns2?.t172xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    returns2?.t172xNamaDivisi = "DIVISI"
                    returns2?.t172TglJamReturn = params.tanggal
                    returns2?.t172Qty1Return = Double.parseDouble(params."qty-${it}" as String)
                    returns2?.t172Qty2Return = Double.parseDouble(params."qty-${it}" as String)
                    returns2?.staDel = '0'
                    returns2?.status = '1'
                    returns2?.lastUpdated =  datatablesUtilService?.syncTime()
                    returns2?.lastUpdProcess = "UPDATE"
                    returns2.save(flush: true)

                    def awal = returns?.t172Qty1Return
                    def akhir = returns2?.t172Qty1Return
                    def sisa = akhir - awal
                    try {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(binningDetail.goodsReceiveDetail?.goods,'0',session.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(binningDetail.goodsReceiveDetail?.goods,session.userCompanyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(binningDetail.goodsReceiveDetail?.goods,'0',session.userCompanyDealer)
                        }
                            def kurangStok2 =  PartsStok.get(goodsStok.id)
                            kurangStok2?.companyDealer = session.userCompanyDealer
                            kurangStok2?.goods = binningDetail.goodsReceiveDetail?.goods
                            kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - sisa
                            kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - sisa
                            kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - sisa
                            kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - sisa
                            kurangStok2.staDel = '0'
                            kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok2.lastUpdProcess = 'RETURNN'
                            kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                            kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                            kurangStok2?.save(flush: true)
                            kurangStok2.errors.each {
                                println it
                            }

                    }catch (Exception ex){

                    }
                }
            }
        hasil = params.noReturns
        render hasil
    }
    def req(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
        String tanggal =  params.tanggal
        def hasil = null
        def t172ID = null
        def vendor =  Vendor.findByM121Nama(params.namaVendor)
        if(vendor){
            def jsonArray = JSON.parse(params.ids)
            if(params.noReturnBefore==null || params.noReturnBefore==""){
                t172ID = generateCodeService.codeGenerateSequence("T172_ID",session.userCompanyDealer)
            }
            jsonArray.each {
                def binningDetail = BinningDetail.get(it)
                if(binningDetail){
                    Returns returns = new Returns()
                    if(params.noReturnBefore==null || params.noReturnBefore==""){
                        returns?.t172ID = t172ID
                    }else{
                        returns?.t172ID = params.noReturnBefore
                    }
                    returns?.t172TglJamReturn  = params.tanggal
                    returns?.t172PetugasReturn = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    returns?.vendor = vendor
                    returns?.companyDealer = session.userCompanyDealer
                    returns?.invoice = binningDetail.goodsReceiveDetail?.invoice
                    returns?.goodsReceive =  binningDetail.goodsReceiveDetail?.goodsReceive
                    returns?.stockIN = StockIN?.findById(1)
                    returns?.goods = binningDetail.goodsReceiveDetail?.goods
                    returns?.t172Qty1Return = Double.parseDouble(params."qty-${it}" as String)
                    returns?.t172Qty2Return = Double.parseDouble(params."qty-${it}" as String)
                    returns?.t172Keterangan = "ket"
                    returns?.t172xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    returns?.t172xNamaDivisi = "DIVISI"
                    returns?.staDel = '0'
                    returns?.status = '1'
                    returns?.dateCreated = datatablesUtilService?.syncTime()
                    returns?.lastUpdated = datatablesUtilService?.syncTime()
                    returns?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    returns?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    returns?.lastUpdProcess = "INSERT"

                    returns.save(flush: true)

                    try {
                        def goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(binningDetail.goodsReceiveDetail?.goods,'0',session.userCompanyDealer)
                        if(!goodsStok){
                            def partsStokService = new PartsStokService()
                            partsStokService.newStock(binningDetail.goodsReceiveDetail?.goods,session.userCompanyDealer)
                            goodsStok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(binningDetail.goodsReceiveDetail?.goods,'0',session.userCompanyDealer)
                        }
                            def kurangStok2 =  PartsStok.get(goodsStok.id)
                            kurangStok2?.companyDealer = session.userCompanyDealer
                            kurangStok2?.goods = binningDetail.goodsReceiveDetail?.goods
                            kurangStok2?.t131Qty1Free = kurangStok2.t131Qty1Free - returns?.t172Qty1Return
                            kurangStok2?.t131Qty2Free = kurangStok2.t131Qty2Free - returns?.t172Qty1Return
                            kurangStok2?.t131Qty1 = kurangStok2?.t131Qty1 - returns?.t172Qty1Return
                            kurangStok2?.t131Qty2 = kurangStok2?.t131Qty2 - returns?.t172Qty1Return
                            kurangStok2.staDel = '0'
                            kurangStok2.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                            kurangStok2.lastUpdProcess = 'RETURNSSS'
                            kurangStok2.dateCreated = datatablesUtilService?.syncTime()
                            kurangStok2.lastUpdated = datatablesUtilService?.syncTime()
                            kurangStok2?.save(flush: true)
                            kurangStok2.errors.each {
                                println it
                            }
                    }catch (Exception ex){

                    }
                 }
            }
            hasil = t172ID
        }else {
            hasil = "fail"
        }
        //jurnal
        params.nomorReturn = t172ID
        journalReturPartsService.createJournal(params)
        render hasil
    }

    def getTableData(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def hasil = Returns.createCriteria().list() {
            eq("companyDealer",session?.userCompanyDealer)
            eq("t172ID",params.id)
        }
        def result = []
        hasil.each {
            def goodsRecDet = GoodsReceiveDetail.findByGoodsReceiveAndGoods(it.goodsReceive,it.goods)
            result << [

                    id: goodsRecDet.id,

                    kode: it.goods.m111ID,

                    nama: it.goods.m111Nama,

                    po : goodsRecDet.poDetail.po.t164NoPO,

                    tglpo : goodsRecDet.poDetail.po.t164TglPO ? goodsRecDet.poDetail.po.t164TglPO.format(dateFormat) : "",

                    nomorInvoice : it.invoice.t166NoInv,

                    Qreff : goodsRecDet.t167Qty1Reff,

                    Qreturn : it.t172Qty1Return,

                    Qreceive : goodsRecDet.t167Qty1Issued,

                    tglInvoice : it.invoice.t166TglInv ? it.invoice.t166TglInv.format(dateFormat) : ""

            ]

        }
        render result as JSON
    }
    def show(Long id) {
        def returnsInstance = Returns.get(id)
        if (!returnsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'returns.label', default: 'returns'), id])
            redirect(action: "list")
            return
        }

        [returnsInstance: returnsInstance]
    }

    def edit(Long id) {
        def returnsInstance = Returns.get(id)
        if (!returnsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'returns.label', default: 'returns'), id])
            redirect(action: "list")
            return
        }

        [returnsInstance: returnsInstance]
    }

    def update(Long id, Long version) {
        def returnsInstance = Returns.get(id)
        if (!returnsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'Returns.label', default: 'returns'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (returnsInstance.version > version) {
                returnsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'Returns.label', default: 'returns')] as Object[],
                        "Another user has updated this returns while you were editing")
                render(view: "edit", model: [returnsInstance: returnsInstance])
                return
            }
        }

        //returnsInstance.properties = params
        returnsInstance?.lastUpdated = datatablesUtilService?.syncTime()
        returnsInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        returnsInstance?.lastUpdProcess = "UPDATE"
        returnsInstance.t172Qty1Return = Double.parseDouble(params.t172Qty1Return)
        if (!returnsInstance.save(flush: true)) {
            render(view: "edit", model: [returnsInstance: returnsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'returns.label', default: 'returns'), returnsInstance.id])
        redirect(action: "show", id: returnsInstance.id)
    }

    def delete(Long id) {
        def returnsInstance = Returns.get(id)
        if (!returnsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'Returns.label', default: 'returns'), id])
            redirect(action: "list")
            return
        }

        try {
            returnsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'Returns.label', default: 'returns'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'Returns.label', default: 'returns'), id])
            redirect(action: "show", id: id)
        }
    }
    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(Returns, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(Returns, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }
    def kodeList() {
        def res = [:]

        def result = Vendor.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.m121Nama
        }

        res."options" = opts
        render res as JSON
    }
}
