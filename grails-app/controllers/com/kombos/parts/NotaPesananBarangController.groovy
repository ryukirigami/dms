package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.activiti.engine.impl.util.json.JSONArray
import org.apache.commons.io.FileUtils
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class NotaPesananBarangController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def notaPesananBarangService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {

	 [idTable : new Date().format("yyyyMMddhhmmss")]
    }

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
		render notaPesananBarangService.datatablesList(params) as JSON
	}
    def formApproval(){
        def npb = NotaPesananBarang.findById(params.id as Long)
        def npbDetail = NotaPesananBarangDetail.findAllByNotaPesananBarang(npb)
        [npb : npb,npbDetail:npbDetail]
    }
    def editCabang(){
        def data = "Sukses"
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def npb = NotaPesananBarang.get(params.idNpb as Long)
        npb.cabangPenerimaSta = "1"
        npb.tglCabangDiterima = params.tglDiterima
        npb.cabangPenerima = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        def npbDetail = NotaPesananBarangDetail.findAllByNotaPesananBarang(npb)
            npbDetail.each {
                def ubh = NotaPesananBarangDetail.get(it.id as Long)
                ubh.cabangKeterangan = params."ket_${it.id}"
                ubh.cabangTglDiterima = params.tglDiterima
                ubh.lastUpdated = datatablesUtilService?.syncTime()
                ubh.save(flush: true)
            }

        npb?.lastUpdated = datatablesUtilService?.syncTime()
        npb?.save(flush: true)
        render data
    }
    def formApprovalJkt(){
        def npb = NotaPesananBarang.findById(params.id as Long)
        def npbDetail = NotaPesananBarangDetail.findAllByNotaPesananBarang(npb)
        [npb : npb,npbDetail:npbDetail]
    }
    def sendApproveJkt(){
        def data = ""
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy")
        def npb = NotaPesananBarang.get(params.idNpb as Long)
        npb.ptgsNPBJKT = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        if(params.staApproval=="1"){
            npb.ptgsNPBJKTSta = "1"
            npb.tglJKTBukaDO = params.tglDO
            npb.tglJKTKirimCabang = params.tglKirimCabang
            npb.tglJKTProsesToko = params.tglDO
            def npbDetail = NotaPesananBarangDetail.findAllByNotaPesananBarang(npb)
            npbDetail.each {
                def ubh = NotaPesananBarangDetail.get(it.id as Long)
                ubh.ktrJktKeterangan = params."ket_${it.id}"
                ubh.ktrJktTglBukaDO = params.tglDO
                ubh.ktrJktTglDiproses = params.tglProses
                ubh.nomorDO = params.nomorDO
                ubh.ktrJktTglKrmKeCabang = params.tglKirimCabang
                ubh.lastUpdated = datatablesUtilService?.syncTime()
                ubh.save(flush: true)
            }
            data = "Data Approved : "
        }else{
            npb.ptgsNPBJKTSta = "2"
            data = "Data unApproved : "
        }
        npb?.lastUpdated = datatablesUtilService?.syncTime()
        npb?.save(flush: true)
        render data
    }
    def formApprovalHo(){
        def npb = NotaPesananBarang.findById(params.id as Long)
        def npbDetail = NotaPesananBarangDetail.findAllByNotaPesananBarang(npb)
        [npb : npb,npbDetail:npbDetail]
    }
    def sendApproveHo(){
        def data = ""
        def npb = NotaPesananBarang.get(params.idNpb as Long)
        npb.partsHO = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        if(params.staApproval=="1"){
            npb.partsHOApproveSta = "1"
            def npbDetail = NotaPesananBarangDetail.findAllByNotaPesananBarang(npb)
            npbDetail.each {
                def ubh = NotaPesananBarangDetail.get(it.id as Long)
                ubh.hoKeterangan = params."ket_${it.id}"
                ubh.lastUpdated = datatablesUtilService?.syncTime()
                ubh.save(flush: true)
            }
            data = "Data Approved : "
        }else{
            npb.partsHOApproveSta = "2"
            data = "Data unApproved : "
        }

        if(params.teruskan=="1"){
            npb?.tglJKTDiterima = new Date()
            npb?.cabangJakarta = CompanyDealer.findByM011ID("012")
            npb?.tglHOKeJKT = new Date()
        }else{
            npb?.cabangDiteruskan = CompanyDealer.findById(params.cabangDiteruskan.id as Long)
        }

        def foto = request.getFile('fileDokumenHO')
        def notAllowContFoto = ['application/octet-stream']
        if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){
            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(foto.getContentType())) {
                return;
            }

            npb.fotoHo = foto.getBytes()
            npb.fotoImageMimeHo = foto.getContentType()
        }
        npb?.lastUpdated = datatablesUtilService?.syncTime()
        npb?.save(flush: true)
        render data
    }
    def formApprovalKabeng(){
        def npb = NotaPesananBarang.findById(params.id as Long)
        [npb : npb]
    }
    def sendApproveKabeng(){
        def data = ""
        def npb = NotaPesananBarang.get(params.idNpb as Long)
        npb?.tglApproveUnApprove = new Date()
        npb?.kabengApprover = User.findByUsername(org.apache.shiro.SecurityUtils.subject.principal.toString())
        if(params.staApproval=="1"){
            npb?.kabengApprovalSta = "1"
            npb?.tglHODiterima = new Date()
            def npbDetail = NotaPesananBarangDetail.findAllByNotaPesananBarang(npb)
            npbDetail.each {
                def ubh = NotaPesananBarangDetail.get(it.id as Long)
                ubh?.hoTglDiterima = new Date()
                ubh?.lastUpdated = datatablesUtilService?.syncTime()
                ubh?.save(flush: true)
            }
            npb.staApproval = StatusApproval.APPROVED
            data = "Data Approved : "
        }else{
            npb?.kabengApprovalSta = "2"
            npb?.staApproval = StatusApproval.REJECTED
            data = "Data unApproved : "
        }
        npb?.lastUpdated = datatablesUtilService?.syncTime()
        def foto = request.getFile('fileDokumenKabeng')
        def notAllowContFoto = ['application/octet-stream']
        if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){
            def okcontents = ['image/png', 'image/jpeg', 'image/gif']
            if (! okcontents.contains(foto.getContentType())) {
                return;
            }

            npb.fotoKabeng = foto.getBytes()
            npb.fotoImageMimeKabeng= foto.getContentType()
        }
        npb?.save(flush: true)
        render data

    }
    def doCabang(){
        def npb = NotaPesananBarang.get(params.id as Long)
        npb?.cabangDiteruskanSta = "1"
        npb?.lastUpdated = datatablesUtilService?.syncTime()
        npb?.save(flush: true)
        render "ok"
    }
    def npbApprovalList(){
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        def autorities = [:]
        def otoritas = ""
        roles.each {
            if(it.authority == "PETUGAS_NPB_HO"){
                autorities.put("petugasNPBHO", true)
                otoritas = "petugasNPBHO"
            }
            else if(it.authority == "PETUGAS_DO"){
                autorities.put("petugasDO",true)

            }
            else if(it.authority == "PETUGAS_NPB_JKT"){
                autorities.put("petugasNPBJKT",true)
                otoritas = "petugasNPBJKT"
            }
            else if(it.authority == "PARTS_HA"){
                autorities.put("partsHA", true)

            }
            else if(it.authority == "KEPALA_BENGKEL"){
                autorities.put("kepalaBengkel", true)
                otoritas = "kepalaBengkel"
            }
            else if(it.authority == "PARTSMAN"){
                autorities.put("partsman", true)

            }
            else if(it.authority == "ADMINISTRATION_HEAD"){
                autorities.put("adminHead", true)

            }
            else if(it.authority == "TEKNISI"){
                autorities.put("teknisi", true)

            }
            else if(it.authority == "SERVICE_ADVISOR_GENERAL_REPAIR"){
                autorities.put("SAGR", true)

            }

        }
        [otoritas : otoritas,idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def userService

    def editNPBApproval(){
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        def autorities = [:]


        roles.each {
            if(it.authority == "PETUGAS_NPB_HO")
                autorities.put("petugasNPBHO", true)
            else if(it.authority == "PETUGAS_DO")
                autorities.put("petugasDO",true)
            else if(it.authority == "PETUGAS_NPB_JKT")
                autorities.put("petugasNPBJKT",true)
            else if(it.authority == "PARTS_HA")
                autorities.put("partsHA", true)
            else if(it.authority == "KEPALA_BENGKEL")
                autorities.put("kepalaBengkel", true)
            else if(it.authority == "PARTSMAN")
                autorities.put("partsman", true)
            else if(it.authority == "ADMINISTRATION_HEAD")
                autorities.put("adminHead", true)
            else if(it.authority == "TEKNISI")
                autorities.put("teknisi", true)
            else if(it.authority == "SERVICE_ADVISOR_GENERAL_REPAIR")
                autorities.put("SAGR", true)

        }

        def npb = NotaPesananBarang.get(params.id)

        autorities.put("notaPesananBarangInstance", npb)
        autorities.put("idNPB", npb.id)

        return autorities
    }



    def nbpApprovalDatatables(){
        session.exportParams=params

        render notaPesananBarangService.npbApprovalDatatables(params) as JSON

    }

	def create() {
		def result = notaPesananBarangService.create(params)

        if(!result.error)
            return [notaPesananBarangInstance: result.notaPesananBarangInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		 def result = notaPesananBarangService.save(params, session.userCompanyDealer)

         redirect(action:'list')

	}

	def show(Long id) {
		def result = notaPesananBarangService.show(params)

		if(!result.error)
			return [ notaPesananBarangInstance: result.notaPesananBarangInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = notaPesananBarangService.show(params)

		if(!result.error)
			return [ notaPesananBarangInstance: result.notaPesananBarangInstance, idNPB : result.notaPesananBarangInstance.id ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = notaPesananBarangService.update(params)

        redirect(action:'list')
	}

    def updateNPBApprove(Long id, Long version) {
        def result = notaPesananBarangService.updateNPBApprove(params, userService.checkUserAuthorities())

        render "Ok"
    }



    def delete() {
		def result = notaPesananBarangService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["NotaPesananBarang", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(NotaPesananBarang, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def inputGoods(){

    }

    def goodsList(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            ilike("staDel", "%0%")
            if (params."sCriteria_m111ID_modal") {
                ilike("m111ID", "%" + params.sCriteria_m111ID_modal+ "%")
            }

            if (params."sCriteria_m111Nama_modal") {
               ilike("m111Nama","%" + params.sCriteria_m111Nama_modal + "%")
            }

            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        int count = 1

        results.each {
            rows << [

                    noUrut : count,

                    id: it.id,

                    m111ID_modal: it.m111ID,

                    m111Nama_modal: it.m111Nama



            ]

            count++;
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def sublist() {

        [idNPB: params.id, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubList(){

        render notaPesananBarangService.datatablesSubList(params) as JSON
    }

    def datatablesApprovalSubList(){

        render notaPesananBarangService.datatablesApprovalSubList(params) as JSON
    }
    def jasperService

    def printNotaPesananBarang(){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def jsonArray = JSON.parse(params.id)
        def notaPesananBarangList = []

        jsonArray.each {
            notaPesananBarangList << NotaPesananBarang.get(it)
        }

        List<JasperReportDef> reportDefs = new ArrayList<JasperReportDef>();
        notaPesananBarangList.each {
            def reportData = new ArrayList();

            def results = NotaPesananBarangDetail.findAllByNotaPesananBarang(it)

            int count = 0

            results.each {
                def data = [:]

                count = count + 1
                data.put("noNpb", it.notaPesananBarang.noNpb)
                data.put("companyDealer", it.notaPesananBarang.cabangPengirim.m011NamaWorkshop)
                data.put("alamat", it.notaPesananBarang.cabangPengirim.m011Alamat)
                data.put("telp", it.notaPesananBarang.cabangPengirim.m011Telp)
                data.put("namaPelanggan", it.notaPesananBarang.namaPemilik)
                data.put("noPol", it.notaPesananBarang.noPolisi)
                data.put("noRangka", it.notaPesananBarang.noEngine)
                data.put("tipeKendaraan", it.notaPesananBarang.tipeKendaraan?.baseModel.m102NamaBaseModel)
                data.put("tahun", it.notaPesananBarang.tahunPembuatan)
                data.put("tambahanKet", it.notaPesananBarang.keteranganTambahan)
                data.put("tgl", it.notaPesananBarang.cabangPengirim.provinsi?.m001NamaProvinsi+", " + new Date().format("dd MMMM yyyy"))
//                data.put("kabeng", session.)
                def kirim = it.notaPesananBarang?.penegasanPengiriman.id
                data.put("kirimPesawat", (kirim==1?"X":""))
                data.put("kirimKapal", (kirim==2?"X":""))

                data.put("noUrut", count)
                data.put("namaParts", it.goods?.m111Nama)
                data.put("jumlah", it.jumlah.toString())
                data.put("HOditerima",it.notaPesananBarang?.tglHODiterima?.format("dd/MM/YYYY"))
                data.put("HOditeruskan",it.notaPesananBarang?.tglHOKeJKT?.format("dd/MM/YYYY") )
                data.put("JKTditerima", it.notaPesananBarang?.tglJKTDiterima?.format("dd/MM/YYYY"))
                data.put("JKTdiproses", it.ktrJktTglDiproses?.format("dd/MM/YYYY"))
                data.put("JKTbukaDo", it.ktrJktTglBukaDO?.format("dd/MM/YYYY"))
                data.put("CBGkirim", it.ktrJktTglKrmKeCabang?.format("dd/MM/YYYY"))
                data.put("CBGterima", it.cabangTglDiterima?.format("dd/MM/YYYY"))
                data.put("CBGket", it.cabangKeterangan)

                reportData.add(data)
            }


            def reportDef = new JasperReportDef(name:'npb.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )

            reportDefs.add(reportDef)

        }


        def file = File.createTempFile("notaPesananBarang_",".pdf")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefs).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")

        response.outputStream << file.newInputStream()


    }
    def showFotoKabeng = {
        def npb = NotaPesananBarang.get(params.id)
        response.setContentType(npb.fotoImageMimeKabeng)
        response.setContentLength(npb.fotoKabeng.size())
        OutputStream out = response.getOutputStream();
        out.write(npb.fotoKabeng);
        out.close();
    }
    def showFotoHo = {
        def npb = NotaPesananBarang.get(params.id)
        response.setContentType(npb.fotoImageMimeHo)
        response.setContentLength(npb.fotoHo.size())
        OutputStream out = response.getOutputStream();
        out.write(npb.fotoHo);
        out.close();
    }

}
