package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class UnapprovedOrderPartsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = [
		'index',
		'list',
		'datatablesList'
	]

	static addPermissions = []

	static editPermissions = []

	static deletePermissions = []

	def unapprovedOrderPartsService

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {	
		[idTable: new Date().format("yyyyMMddhhmmss")] 
	}
	
	def datatablesList() {
        params.companyDealer = session?.userCompanyDealer
		render unapprovedOrderPartsService.datatablesList(params) as JSON
	}
	
	def show(Long id) {
		def result = unapprovedOrderPartsService.show(params)

		if(!result.error)
			return [ requestInstance: result.requestInstance, dateTimeFormat: result.dateTimeFormat ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

}
