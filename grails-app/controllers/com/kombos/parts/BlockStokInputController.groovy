package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class BlockStokInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def idRecept = params?.idRecep;
        [type : params.type,idTable: new Date().format("yyyyMMddhhmmss"),idRecept : idRecept]
    }

    def listEdit(){
        def idRecept = params?.idRecep;
        [type : params.type,idTable: new Date().format("yyyyMMddhhmmss"),idRecept: idRecept]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }

        def exist2 = []
        if(params."sCriteria_existEdit"){
            JSON.parse(params."sCriteria_existEdit").each{

                exist2 << it
            }
        }

           def c = PickingSlipDetail.createCriteria()
             def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                 eq("staDel", "0")
                 if (params."sCriteria_m111ID_modal") {
                     goods{
                         ilike("m111ID","%" + params.sCriteria_m111ID_modal+ "%" )
                     }
                 }

                 if (params."sCriteria_m111Nama_modal") {
                    goods{
                        ilike("m111Nama", "%" + params.sCriteria_m111Nama_modal + "%")
                    }
                 }
                 pickingSlip{
                     reception{
                         eq("id",params?.idRecept?.toLong())
                     }
                 }

                 if(params.type){
                     if(params.type == "1"){
                             if(exist.size()>0){
                                 not {
                                     or {
                                         exist.each {
                                             eq('goods',Goods.get(it))
                                         }
                                     }
                                 }
                             }
                     }else{
                         if(exist2.size()>0){
                             not {
                                 or {
                                     exist2.each {
                                         eq('goods',Goods.get(it))
                                     }
                                 }
                             }
                         }
                     }
                 }

                 switch (sortProperty) {
                     default:
                         order(sortProperty, sortDir)
                         break;
                 }
             }

             def rows = []
             int count = 1

             results.each {
                 rows << [

                         noUrut : count,

                         id: it.goods?.id,

                         m111ID_modal: it.goods?.m111ID,

                         m111Nama_modal: it.goods?.m111Nama,

                         qtyPicking : it.t142Qty1,

                         satuan : it.goods?.satuan?.toString()

                 ]

                 count++;
             }

       ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def show(Long id) {
        def pickingSlipDetailInstance = PickingSlipDetail.get(id)
        if (!pickingSlipDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pickingSlipDetail.label', default: 'Goods'), id])
            redirect(action: "list")
            return
        }

        [pickingSlipDetailInstance: pickingSlipDetailInstance]
    }

    def create(){
        render "Oke"
    }
}