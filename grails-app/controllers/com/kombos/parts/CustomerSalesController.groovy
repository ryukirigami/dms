package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class CustomerSalesController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def customerSalesService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render customerSalesService.datatablesList(params) as JSON
    }

    def create() {
        def result = customerSalesService.create(params)

        if (!result.error)
            return [customerSalesInstance: result.customerSalesInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer = session?.userCompanyDealer
        def result = customerSalesService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Customer Sales", ""])
            redirect(action: 'show', id: result.customerSalesInstance.id)
            return
        }

        render(view: 'create', model: [customerSalesInstance: result.customerSalesInstance])
    }

    def show(Long id) {
        def result = customerSalesService.show(params)

        if (!result.error)
            return [customerSalesInstance: result.customerSalesInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = customerSalesService.show(params)

        if (!result.error)
            return [customerSalesInstance: result.customerSalesInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = customerSalesService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Customer Sales", ""])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [customerSalesInstance: result.customerSalesInstance.attach()])
    }

    def delete() {
        def result = customerSalesService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Customer Sales", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(CustomerSales, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
