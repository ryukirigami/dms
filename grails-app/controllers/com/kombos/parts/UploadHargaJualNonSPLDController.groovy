package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadHargaJualNonSPLDController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
    }

    def upload() {
        def goods = null
        def requestBody = request.JSON
        HargaJualNonSPLDPerPart hargaJualInstance = null


        requestBody.each{
            hargaJualInstance = new HargaJualNonSPLDPerPart()

            goods = Goods.findByM111ID(it.kodeGoods)
            if(goods &&  (it.tanggalBerlaku && it.tanggalBerlaku!="") && (it.kodeGoods && it.kodeGoods!="") &&
                    (it.persenKenaikanHarga && it.persenKenaikanHarga!="") ){

                String persen = it.persenKenaikanHarga
                hargaJualInstance.goods = goods

                hargaJualInstance.t160PersenKenaikanHarga = Double.parseDouble(persen)
                DateFormat dateFormat = new SimpleDateFormat('yyyy-MM-dd')

                String tanggal = it.tanggalBerlaku
                hargaJualInstance.t160TMT = dateFormat.parse(it.tanggalBerlaku)

                hargaJualInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                hargaJualInstance.lastUpdProcess = "UPLOAD"
                hargaJualInstance.companyDealer = session.userCompanyDealer
                hargaJualInstance.dateCreated = datatablesUtilService?.syncTime()
                hargaJualInstance.lastUpdated = datatablesUtilService?.syncTime()
                hargaJualInstance.save(flush : true)

                def duplicate = HargaJualNonSPLDPerPart.findByGoodsAndCompanyDealerAndIdNotEqual(hargaJualInstance.goods, hargaJualInstance.companyDealer, hargaJualInstance.id)
                if(duplicate){
                    duplicate.delete()
                }


            }
        }

        flash.message = message(code: 'default.uploadHargaNonSPLDError.message', default: "Save Harga Non SPLD Done")
        render(view: "index", model: [operationInstance: hargaJualInstance])
    }

    def view() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd")
        def hargaJualNonSPLDInstance = new HargaJualNonSPLDPerPart(params)
        Map CONFIG_HARGA_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'tanggalBerlaku',
                        'B':'kodeGoods',
                        'C':'persenKenaikanHarga',

                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        int jmlhDataDuplicate = 0;
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index")
                return
            }

            Workbook workbook = null
            try{
                 workbook = WorkbookFactory.create(uploadExcel.inputStream)

            }catch(Exception ex){
                flash.message = "Illegal Content Type or Content Corrupted"
                uploadExcel = null
                render(view: "index")
                return
            }

         //   Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def hargaList = excelImportService.columns(workbook, CONFIG_HARGA_COLUMN_MAP)

            jsonData = hargaList as JSON
            Goods goods = null
            HargaJualNonSPLDPerPart duplicate = null
            String status = "0", style = ""
            def styleList = [:]

            hargaList?.each {

                if((it.tanggalBerlaku && it.tanggalBerlaku!="") && (it.kodeGoods && it.kodeGoods !="") && (it.persenKenaikanHarga && it.persenKenaikanHarga!="") ){


                    duplicate = HargaJualNonSPLDPerPart.findByGoodsAndCompanyDealer(Goods.findByM111ID(it.kodeGoods), session.userCompanyDealer)
                    if(duplicate){
                        jmlhDataDuplicate++;
                        status = "2";
                    }

                    goods = Goods.findByM111ID(it.kodeGoods)
                    Date date = null
                    try{
                 //       date = new Date().parse('yyyy-MM-dd', it.tanggalBerlaku)
                        String tanggal = it.tanggalBerlaku
                        date = dateFormat.parse(tanggal)
                    }catch(Exception ex){
                        println "Exc "+ex
                    }

                    if(!goods){
                        jmlhDataError++;
                        styleList.goods = "style='color:red;'"
                        status = "1"
                    }else if(!date){
                        jmlhDataError++;
                        styleList.tanggal = "style='color:red;'"
                        status = "1"
                    } else {
                        try{
                            String persenKenaikan = it.persenKenaikanHarga
                            double persen = Double.parseDouble(persenKenaikan)
                        }catch(Exception e){
                            jmlhDataError++;
                            styleList.persen = "style='color:red;'"
                            status = "1"
                        }

                    }





                } else {

                    jmlhDataError++;
                    status = "1";

                }

                    if(status.equals("1")){
                   //     style = "style='color:red;'"
                    }else if(status.equals("2")){
                        style="style='color:blue'"
                    } else {
                        style = ""
                    }

                htmlData+="<tr "+style+">\n" +
                        "                            <td "+styleList.tanggal+">\n" +
                        "                                "+it.tanggalBerlaku+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.goods+">\n" +
                        "                                "+it.kodeGoods+"\n" +
                        "                            </td>\n" +
                        "                            <td "+styleList.persen+">\n" +
                        "                                "+it.persenKenaikanHarga+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"

                status = "0"
            }
        }

        if(jmlhDataDuplicate>0){
            flash.message = message(code: 'default.uploadHargaNonSPLDError.message', default: "Read File Done : Terdapat "+jmlhDataDuplicate+" data yang duplicate, cek baris yang berwarna biru. Klik save untuk replace")
        }else if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadHargaNonSPLDError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadHargaNonSPLDError.message', default: "Read File Done")
        }

        render(view: "index", model: [hargaJualNonSPLDInstance : hargaJualNonSPLDInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }
}
