package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class RPPController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = RPP.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m161TglBerlaku") {
                ge("m161TglBerlaku", params."sCriteria_m161TglBerlaku")
                lt("m161TglBerlaku", params."sCriteria_m161TglBerlaku" + 1)
            }

            if (params."sCriteria_m161MasaPengajuanRPP") {
                eq("m161MasaPengajuanRPP",Integer.parseInt (params."sCriteria_m161MasaPengajuanRPP"))
            }

            if (params."sCriteria_m161MaxDapatRPP") {
                eq("m161MaxDapatRPP",Double.parseDouble(params."sCriteria_m161MaxDapatRPP"))
            }

            if (params."sCriteria_companyDealer") {
                eq("companyDealer", params."sCriteria_companyDealer")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m161TglBerlaku: it.m161TglBerlaku ? it.m161TglBerlaku.format(dateFormat) : "",

                    m161MasaPengajuanRPP: it.m161MasaPengajuanRPP,

                    m161MaxDapatRPP: it.m161MaxDapatRPP,

                    companyDealer: it.companyDealer,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [RPPInstance: new RPP(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def RPPInstance = new RPP(params)
        RPPInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        RPPInstance?.lastUpdProcess = "INSERT"

        def cek = RPP.createCriteria().list() {
            and{
                eq("m161TglBerlaku",RPPInstance.m161TglBerlaku)
                eq("m161MasaPengajuanRPP",RPPInstance.m161MasaPengajuanRPP)
                eq("m161MaxDapatRPP",RPPInstance.m161MaxDapatRPP)
            }
        }
        if(cek){
            flash.message = "Tanggal Berlaku sudah ada"
            render(view: "create", model: [RPPInstance: RPPInstance])
            return
        }
        if (!RPPInstance.save(flush: true)) {
            render(view: "create", model: [RPPInstance: RPPInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'RPP.label', default: 'RPP'), RPPInstance.id])
        redirect(action: "show", id: RPPInstance.id)
    }

    def show(Long id) {
        def RPPInstance = RPP.get(id)
        if (!RPPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'RPP.label', default: 'RPP'), id])
            redirect(action: "list")
            return
        }

        [RPPInstance: RPPInstance]
    }

    def edit(Long id) {
        def RPPInstance = RPP.get(id)
        if (!RPPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'RPP.label', default: 'RPP'), id])
            redirect(action: "list")
            return
        }

        [RPPInstance: RPPInstance]
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.m161MaxDapatRPP=params.m161MaxDapatRPP.replace(",","")
        def RPPInstance = RPP.get(id)
        if (!RPPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'RPP.label', default: 'RPP'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (RPPInstance.version > version) {

                RPPInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'RPP.label', default: 'RPP')] as Object[],
                        "Another user has updated this RPP while you were editing")
                render(view: "edit", model: [RPPInstance: RPPInstance])
                return
            }
        }

        def cek = RPP.createCriteria()
        def result = cek.list() {
            and{
                eq("m161TglBerlaku",params.m161TglBerlaku)
                eq("m161MasaPengajuanRPP",Integer.parseInt(params.m161MasaPengajuanRPP))
                eq("m161MaxDapatRPP",Double.parseDouble(params.m161MaxDapatRPP))
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [RPPInstance: RPPInstance])
            return
        }

        RPPInstance.properties = params
        RPPInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        RPPInstance?.lastUpdProcess = "UPDATE"

        if (!RPPInstance.save(flush: true)) {
            render(view: "edit", model: [RPPInstance: RPPInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'RPP.label', default: 'RPP'), RPPInstance.id])
        redirect(action: "show", id: RPPInstance.id)
    }

    def delete(Long id) {
        def RPPInstance = RPP.get(id)
        if (!RPPInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'RPP.label', default: 'RPP'), id])
            redirect(action: "list")
            return
        }

        try {
            RPPInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            RPPInstance?.lastUpdProcess = "DELETE"
            RPPInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'RPP.label', default: 'RPP'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'RPP.label', default: 'RPP'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(RPP, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(RPP, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
