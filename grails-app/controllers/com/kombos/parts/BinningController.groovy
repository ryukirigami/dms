package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class BinningController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
    def generateCodeService
    def binningService
    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = []

   	static editPermissions = []

   	static deletePermissions = []

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def sublist() {
        [nomorBinning: params.nomorBinning, tglBinning: params.tglBinning, petugasBinning: params.petugasBinning, isSelected: params.isSelected,
                idTableSub: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render binningService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
   		render binningService.datatablesSubList(params) as JSON
   	}

    def create() {
        def result = binningService.create(params)

        if (!result.error)
            return [binningInstance: result.binningInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {

        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = binningService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Binning", result.binningInstance.id])
            redirect(action: 'show', id: result.binningInstance.id)
            return
        }

        render(view: 'create', model: [binningInstance: result.binningInstance])
    }

    def show(Long id) {
        def result = binningService.show(params)

        if (!result.error)
            return [binningInstance: result.binningInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = binningService.show(params)

        if (!result.error)
            return [binningInstance: result.binningInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = binningService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Binning", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [binningInstance: result.binningInstance.attach()])
    }

    def massdeleteBinning() {
        def res = [:]
        try {
            binningService.massDeleteBinning(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
    def massdeleteBinningDetail() {
        def res = [:]
        try {
            binningService.massDeleteBinningDetail(params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    private ArrayList createReportDetail(List<BinningDetail> bndetList) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();

        int count = 0
        bndetList.each {

            def data = [nomorBinning: it.binning?.t168ID, tanggalBinning: it.binning?.t168TglJamBinning.format(dateFormat), tanggal: (new Date()).format(dateFormat)]
            data.put("noUrut", "" + count)
            data.put("kodeParts", it.goodsReceiveDetail?.goods?.m111ID)
            data.put("namaParts", it.goodsReceiveDetail?.goods?.m111Nama)
            data.put("nomorPO", it.goodsReceiveDetail?.poDetail?.po?.t164NoPO)
            data.put("tanggalPO", it.goodsReceiveDetail?.poDetail?.po?.t164TglPO?.format(dateFormat))
            data.put("qty", "" + it.goodsReceiveDetail?.t167Qty1Reff )
            data.put("satuan",it.goodsReceiveDetail?.goods?.satuan?.m118Satuan1)
            data.put("kodeLokasi", "-")

            reportData.add(data)
        }
        return reportData
    }

    def printBinningSlip(){
        def idBinnings
        def idBinningDetails

        if (params.idBinnings) idBinnings = JSON.parse(params.idBinnings)
        if (params.idBinningDetails) idBinningDetails = JSON.parse(params.idBinningDetails)

        List<JasperReportDef> reportDefList = []
        def reportData = new ArrayList();

        if (idBinnings)  {
            List<Long> idBns = new ArrayList<Long>();
            idBinnings.each {
                idBns.add(Long.parseLong(it))
            }
            List<Binning> bnList = Binning.withCriteria {
                inList('id', idBns)
            }
            List<BinningDetail> bndetList = BinningDetail.withCriteria {
                inList('binning', bnList)
            }
            reportData = createReportDetail(bndetList)
        } else {
            List<Long> ids = new ArrayList<Long>()
            idBinningDetails.each {
                ids.add(Long.parseLong(it))
            }

            List<BinningDetail> bndetList = BinningDetail.withCriteria {
                inList('id', ids)
            }
            reportData = createReportDetail(bndetList)
        }


        def reportDef = new JasperReportDef(name:'binningSlip.jasper',
                fileFormat:JasperExportFormat.PDF_FORMAT,
                reportData: reportData
        )

       reportDefList.add(reportDef)


        def file = File.createTempFile("binningSlip_",".pdf")
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")

        response.outputStream << file.newInputStream()

    }
}
