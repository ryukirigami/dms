package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class HargaJualNonSPLDController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    private org.codehaus.groovy.grails.web.util.StreamCharBuffer buffer

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = HargaJualNonSPLD.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_t159TMT") {
                ge("t159TMT", params."sCriteria_t159TMT")
                lt("t159TMT", params."sCriteria_t159TMT" + 1)
            }

            if (params."sCriteria_t159PersenKenaikanHarga") {
                eq("t159PersenKenaikanHarga", Double.parseDouble(params."sCriteria_t159PersenKenaikanHarga"))
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t159TMT: it.t159TMT ? it.t159TMT.format(dateFormat) : "",

                    t159PersenKenaikanHarga: it.t159PersenKenaikanHarga,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [hargaJualNonSPLDInstance: new HargaJualNonSPLD(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def hargaJualNonSPLDInstance = new HargaJualNonSPLD(params)

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date date = dateFormat.parse(params.t159TMT_dp)

        def duplicate = HargaJualNonSPLD.createCriteria().list {
            and{
                eq("t159TMT", dateFormat.parse(params.t159TMT_dp))
                //       eq("t159PersenKenaikanHarga", Double.parseDouble(params.t159PersenKenaikanHarga))
                eq("companyDealer", session.userCompanyDealer)
            }
        }

        if(duplicate){
            flash.message = "Harga Jual Non SPLD Sudah ada untuk tanggal tersebut"
            render(view: "create", model: [hargaJualNonSPLDInstance: hargaJualNonSPLDInstance])
            return
        }

        hargaJualNonSPLDInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        hargaJualNonSPLDInstance.setLastUpdProcess("INSERT")
        hargaJualNonSPLDInstance.companyDealer = CompanyDealer.findById(session.userCompanyDealerId)



        if (!hargaJualNonSPLDInstance.save(flush: true)) {
            render(view: "create", model: [hargaJualNonSPLDInstance: hargaJualNonSPLDInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD'), hargaJualNonSPLDInstance.id])
        redirect(action: "show", id: hargaJualNonSPLDInstance.id)
    }

    def show(Long id) {
        def hargaJualNonSPLDInstance = HargaJualNonSPLD.get(id)
        if (!hargaJualNonSPLDInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD'), id])
            redirect(action: "list")
            return
        }

        [hargaJualNonSPLDInstance: hargaJualNonSPLDInstance]
    }

    def edit(Long id) {
        def hargaJualNonSPLDInstance = HargaJualNonSPLD.get(id)
        if (!hargaJualNonSPLDInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD'), id])
            redirect(action: "list")
            return
        }

        [hargaJualNonSPLDInstance: hargaJualNonSPLDInstance]
    }

    def update(Long id, Long version) {
        def hargaJualNonSPLDInstance = HargaJualNonSPLD.get(id)
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (!hargaJualNonSPLDInstance) {
            buffer
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (hargaJualNonSPLDInstance.version > version) {

                hargaJualNonSPLDInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD')] as Object[],
                        "Another user has updated this HargaJualNonSPLD while you were editing")
                render(view: "edit", model: [hargaJualNonSPLDInstance: hargaJualNonSPLDInstance])
                return
            }
        }

        def duplicate = HargaJualNonSPLD.createCriteria().list {
            and{
                eq("t159TMT", dateFormat.parse(params.t159TMT_dp))
           //     eq("t159PersenKenaikanHarga", Double.parseDouble(params.t159PersenKenaikanHarga))
                eq("companyDealer", session.userCompanyDealer)
                notEqual("id", id)
            }
        }

        if(duplicate){
            flash.message = "Harga Jual Non SPLD Sudah ada untuk tanggal tersebut"
            render(view: "edit", model: [hargaJualNonSPLDInstance: hargaJualNonSPLDInstance])
            return
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        hargaJualNonSPLDInstance.properties = params
        hargaJualNonSPLDInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        hargaJualNonSPLDInstance.setLastUpdProcess("UPDATE")
        hargaJualNonSPLDInstance.companyDealer = CompanyDealer.findById(session.userCompanyDealerId)

        if (!hargaJualNonSPLDInstance.save(flush: true)) {
            render(view: "edit", model: [hargaJualNonSPLDInstance: hargaJualNonSPLDInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD'), hargaJualNonSPLDInstance.id])
        redirect(action: "show", id: hargaJualNonSPLDInstance.id)
    }

    def delete(Long id) {
        def hargaJualNonSPLDInstance = HargaJualNonSPLD.get(id)
        if (!hargaJualNonSPLDInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD'), id])
            redirect(action: "list")
            return
        }
        hargaJualNonSPLDInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        hargaJualNonSPLDInstance?.lastUpdProcess = "DELETE"

        try {
            hargaJualNonSPLDInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'hargaJualNonSPLD.label', default: 'HargaJualNonSPLD'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(HargaJualNonSPLD, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(HargaJualNonSPLD, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
