package com.kombos.parts





import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import java.text.SimpleDateFormat
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService

class GoodsHargaBeliController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def conversi = new Konversi()

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def dataTablesGoods() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]

        def x = 0


        session.exportParams = params


        def c = Goods.createCriteria()

        //mendapatkan value dari field cari
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if(params.sCriteria_koGo){
                ilike("m111ID", "%"+params.sCriteria_koGo+"%")
            }

            if(params.sCriteria_naGo){
                ilike("m111Nama", "%"+params.sCriteria_naGo+"%")
            }

            if(params.sCriteria_satGo){
                satuan{
                    ilike("m118Satuan1", "%"+params.sCriteria_satGo+"%")
                }
            }

            if(params.sCriteria_klsGo){
                ilike("m111Nama", "%"+params.sCriteria_klsGo+"%")
            }

        }

        def rows = []



        results.each {
            def klasifikasi = KlasifikasiGoods.findByGoods(it)?.franc?.m117NamaFranc
            rows << [

                    id: it.id,

                    kodeGoods : it.m111ID,

                    namaGoods : it.m111Nama,

                    satuan: it.satuan?.m118Satuan1,

                    klasifikasi: klasifikasi
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = GoodsHargaBeli.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
			companyDealer{
                eq("id",session.userCompanyDealer.id)
            }

            if(params."sCriteria_goods"){
                goods{
                    ilike("m111ID", "%" + (params."sCriteria_goods" as String) + "%")
                }
            }

            if(params."sCriteria_goods2"){
                goods{
                    ilike("m111Nama", "%" + (params."sCriteria_goods2" as String) + "%")
                }
            }

            if(params."sCriteria_t150TMT"){
                ge("t150TMT",params."sCriteria_t150TMT")
                lt("t150TMT",params."sCriteria_t150TMT" + 1)
            }

            if(params."sCriteria_t150Harga"){
                eq("t150Harga",params."sCriteria_t150Harga" as Double)
            }

            if(params."sCriteria_vendor"){
                vendor{
                    ilike("m121Nama","%" + (params."sCriteria_vendor" as String) + "%" )
                }
            }

            switch(sortProperty){
                case "goods1":
                    goods{
                        order("m111ID",sortDir)
                    }
                    break;
                case "goods2":
                    goods{
                        order("m111Nama",sortDir)
                    }
                    break;
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer?.m011NamaWorkshop,

                    kode: it.goods.m111ID,

                    t150TMT: it.t150TMT?it.t150TMT?.format(dateFormat):"",

                    t150Harga: it.t150Harga ? conversi.toRupiah(it.t150Harga) : 0,

                    goods: it.goods.m111Nama,

                    vendor: it?.vendor? it.vendor?.m121Nama:""

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [goodsHargaBeliInstance: new GoodsHargaBeli(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.t150Harga = params.t150Harga?.toString()?.replace(",","").toDouble()

        def goodsHargaBeliInstance = new GoodsHargaBeli(params)
        goodsHargaBeliInstance?.goods = Goods.findById(params.goodsId as Long)
        goodsHargaBeliInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        goodsHargaBeliInstance?.setLastUpdProcess("INSERT")
        goodsHargaBeliInstance?.setCompanyDealer(session.userCompanyDealer)
        goodsHargaBeliInstance?.setStaDel("0")
        def cek = GoodsHargaBeli.createCriteria().list {
            eq("staDel","0")
            goods{
                eq("id",params.goodsId as Long)
            }
            eq("companyDealer",session.userCompanyDealer)
        }
        if(cek){
            flash.message = "Data Sudah Ada"
            render(view: "create", model: [goodsHargaBeliInstance: goodsHargaBeliInstance])
            return
        }
        if (!goodsHargaBeliInstance.save(flush: true)) {
            render(view: "create", model: [goodsHargaBeliInstance: goodsHargaBeliInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'goodsHargaBeli.label', default: 'Harga Beli Goods'), ""])
        redirect(action: "show", id: goodsHargaBeliInstance.id)
    }

    def show(Long id) {
        def goodsHargaBeliInstance = GoodsHargaBeli.get(id)
        if (!goodsHargaBeliInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsHargaBeli.label', default: 'GoodsHargaBeli'), id])
            redirect(action: "list")
            return
        }

        [goodsHargaBeliInstance: goodsHargaBeliInstance]
    }

    def edit(Long id) {
        def goodsHargaBeliInstance = GoodsHargaBeli.get(id)
        if (!goodsHargaBeliInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsHargaBeli.label', default: 'GoodsHargaBeli'), id])
            redirect(action: "list")
            return
        }

        [goodsHargaBeliInstance: goodsHargaBeliInstance]
    }

    def update(Long id, Long version) {
        params?.t150Harga = params?.t150Harga?.toString()?.replace(",","").toDouble()
        def goodsHargaBeliInstance = GoodsHargaBeli?.get(id)
        if (!goodsHargaBeliInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'goodsHargaBeli.label', default: 'GoodsHargaBeli'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (goodsHargaBeliInstance.version > version) {

                goodsHargaBeliInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'goodsHargaBeli.label', default: 'GoodsHargaBeli')] as Object[],
                        "Another user has updated this GoodsHargaBeli while you were editing")
                render(view: "edit", model: [goodsHargaBeliInstance: goodsHargaBeliInstance])
                return
            }
        }
        def cek = GoodsHargaBeli.createCriteria().list {
            eq("staDel","0")
            goods{
                eq("id",params?.goodsId as Long)
            }

        }
//        if(cek){
//            for(find in cek){
//                if(find.id!=id){
//                    flash.message = "Data Sudah Ada"
//                    render(view: "edit", model: [goodsHargaBeliInstance: goodsHargaBeliInstance])
//                    return
//                }
//            }
//        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        goodsHargaBeliInstance?.properties = params
        goodsHargaBeliInstance?.setLastUpdProcess("UPDATE")
        goodsHargaBeliInstance?.setCompanyDealer(session.userCompanyDealer)
        goodsHargaBeliInstance?.goods = Goods.findById(params?.goodsId as Long) // as Long
        goodsHargaBeliInstance?.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        if (!goodsHargaBeliInstance.save(flush: true)) {
            render(view: "edit", model: [goodsHargaBeliInstance: goodsHargaBeliInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'goodsHargaBeli.label', default: 'Harga Beli Goods'), ""])
        redirect(action: "show", id: goodsHargaBeliInstance.id)
    }

    def delete() {
        def result = goodsHargaBeliService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["GoodsHargaBeli", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(GoodsHargaBeli, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
