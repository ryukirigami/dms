package com.kombos.parts

import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.NamaApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class CekKetersediaanPartsController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
	
	def cekKetersediaanPartsService

	def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_REQUEST_BOOKING_FEE)
        def approver = ""
        for(NamaApproval namaApproval : kegiatanApproval.namaApprovals){
            if(namaApproval.userProfile.companyDealer.id == session.userCompanyDealerId){
                approver += namaApproval.userProfile.fullname + "\r\n"
            }
        }
        [kegiatanApproval: kegiatanApproval, approver: approver]
    }
	
	def datatablesList() {
        params.companyDealer = session?.userCompanyDealer
		render cekKetersediaanPartsService.datatablesList(params) as JSON
	}

	def requestBookingFeeApproval() {
		def result = cekKetersediaanPartsService.requestBookingFeeApproval(params)

		if(!result.error) {
			render result as JSON
			return
		}

		render result as JSON
	}
	
	def orderParts(){
		def result = cekKetersediaanPartsService.orderParts(params)

		if(!result.error) {
			render result as JSON
			return
		}

		render result as JSON
	}
	
	def generateNomorDokumenRequestBookingFee() {
		def result = cekKetersediaanPartsService.generateNomorDokumenRequestBookingFee()
		render result as JSON
	}
    def deleteParts(){
        def result = [:]
        def req = 0
        def rd = new RequestDetail()
        def jsonArray = JSON.parse(params.requestIds)
        jsonArray.each {
            rd = RequestDetail.get(it)
            req  = rd.request.id
            result.sukses = "ok"
            rd.delete()
        }
        render result as JSON
    }
}
