package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.text.DateFormat
import java.text.SimpleDateFormat

class WarrantyClaimController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = WarrantyClaim.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
//eq ('staDel','0')
            if(params."sCriteria_kategori"){
                goods{
                    ilike("m111ID", "%" + (params."sCriteria_kategori" as String) + "%")
                }
            }

            if(params."sCriteria_goods"){
                goods{
                    ilike("m111Nama", "%" + (params."sCriteria_goods" as String) + "%")
                }
            }

            if (params."sCriteria_m164TglBerlaku") {
                ge("m164TglBerlaku", params."sCriteria_m164TglBerlaku")
                lt("m164TglBerlaku", params."sCriteria_m164TglBerlaku" + 1)
            }

//            if (params."sCriteria_goods") {
//
//                String kriteria = params.sCriteria_goods
//                if(kriteria.contains("|")){
//                    def split = kriteria.split("\\|")
//                    def kriteria1 = split[0]
//                    def kriteria2 = split[1]
//
//                    goods{
//                        or{
//                            ilike("m111Nama", "%"+kriteria2.trim()+"%")
//                            ilike("m111ID","%"+kriteria1.trim()+"%" )
//                        }
//                    }
//                }else{
//                    goods{
//                        or{
//                            ilike("m111Nama", "%"+params.sCriteria_goods+"%")
//                            ilike("m111ID","%"+params.sCriteria_goods+"%" )
//                        }
//                    }
//                }
//            }

            if (params."sCriteria_m164BulanWarranty") {
                eq("m164BulanWarranty",Integer.parseInt(params."sCriteria_m164BulanWarranty"))
            }

            if (params."sCriteria_m164KmWarranty") {
                eq("m164KmWarranty",Double.parseDouble(params."sCriteria_m164KmWarranty"))
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }



            switch (sortProperty) {
                case "goods1":
                    goods{
                        order("m111ID",sortDir)
                    }
                    break;
                case "goods2":
                    goods{
                        order("m111Nama",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    kode: it.goods?.m111ID,

                    goods: it.goods?.m111Nama,

                    m164TglBerlaku: it.m164TglBerlaku ? it.m164TglBerlaku.format(dateFormat) : "",

                    m164BulanWarranty: it.m164BulanWarranty,

                    m164KmWarranty: it.m164KmWarranty,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [warrantyClaimInstance: new WarrantyClaim(params)]
    }

    def save() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def warrantyClaimInstance = new WarrantyClaim(params)
        def cek = WarrantyClaim.createCriteria()
        def result = cek.list() {
            and{
                eq("goods",Goods.get(params.goods.id))
                eq("m164TglBerlaku", df.parse(params.m164TglBerlaku_dp))
                eq("m164BulanWarranty",Integer.parseInt(params.m164BulanWarranty))
                //eq("m053JobsId",operationInstance.m053JobsId?.trim(), [ignoreCase: true])
                eq("m164KmWarranty",Double.parseDouble(params.m164KmWarranty))
            }
        }

        if(!result){
            warrantyClaimInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            warrantyClaimInstance.setLastUpdProcess("INSERT")
            warrantyClaimInstance?.setLastUpdated(datatablesUtilService?.syncTime())
            warrantyClaimInstance?.setDateCreated(datatablesUtilService?.syncTime())
            if (!warrantyClaimInstance.save(flush: true)) {
                render(view: "create", model: [warrantyClaimInstance: warrantyClaimInstance])
                return
            }

        } else {
        flash.message = message(code: 'default.created.error.message', default: 'Data has been used.')
        render(view: "create", model: [warrantyClaimInstance: warrantyClaimInstance])
        return
    }



    flash.message = message(code: 'default.created.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), warrantyClaimInstance.id])
        redirect(action: "show", id: warrantyClaimInstance.id)
    }

    def show(Long id) {
        def warrantyClaimInstance = WarrantyClaim.get(id)
        if (!warrantyClaimInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), id])
            redirect(action: "list")
            return
        }

        [warrantyClaimInstance: warrantyClaimInstance]
    }

    def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def warrantyClaimInstance = WarrantyClaim.get(id)
        if (!warrantyClaimInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), id])
            redirect(action: "list")
            return
        }
        [warrantyClaimInstance: warrantyClaimInstance]
    }

    def update(Long id, Long version) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def warrantyClaimInstance = WarrantyClaim.get(id)
        if (!warrantyClaimInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (warrantyClaimInstance.version > version) {

                warrantyClaimInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'warrantyClaim.label', default: 'WarrantyClaim')] as Object[],
                        "Another user has updated this WarrantyClaim while you were editing")
                render(view: "edit", model: [warrantyClaimInstance: warrantyClaimInstance])
                return
            }
        }

        def cek = WarrantyClaim.createCriteria()
        def result = cek.list() {
            and{
                eq("goods",Goods.get(params.goods.id))
                eq("m164TglBerlaku", df.parse(params.m164TglBerlaku_dp))
                eq("m164BulanWarranty",Integer.parseInt(params.m164BulanWarranty))
                eq("m164KmWarranty",Double.parseDouble(params.m164KmWarranty))
            }
        }

        if(!result){

            warrantyClaimInstance.properties = params
            warrantyClaimInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            warrantyClaimInstance.setLastUpdProcess("UPDATE")
            warrantyClaimInstance?.setLastUpdated(datatablesUtilService?.syncTime())
            if (!warrantyClaimInstance.save(flush: true)) {
                render(view: "edit", model: [warrantyClaimInstance: warrantyClaimInstance])
                return
            }

        }else {
            flash.message = message(code: 'default.update.warrantyClaim.error.message', default: 'Duplicate Data Repair')
            render(view: "edit", model: [warrantyClaimInstance : warrantyClaimInstance])
            return
        }


        flash.message = message(code: 'default.updated.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), warrantyClaimInstance.id])
        redirect(action: "show", id: warrantyClaimInstance.id)
    }

    def delete(Long id) {
        def warrantyClaimInstance = WarrantyClaim.get(id)
        if (!warrantyClaimInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), id])
            redirect(action: "list")
            return
        }
        warrantyClaimInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        warrantyClaimInstance?.lastUpdProcess = "DELETE"


        try {
            warrantyClaimInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'warrantyClaim.label', default: 'WarrantyClaim'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(WarrantyClaim, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(WarrantyClaim, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
