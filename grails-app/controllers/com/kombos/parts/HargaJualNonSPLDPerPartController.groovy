package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class HargaJualNonSPLDPerPartController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = HargaJualNonSPLDPerPart.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_t160TMT") {
                ge("t160TMT", params."sCriteria_t160TMT")
                lt("t160TMT", params."sCriteria_t160TMT" + 1)
            }

            if (params."sCriteria_goods") {
                goods{
                    ilike("m111Nama","%"+params."sCriteria_goods"+"%" )
                }
              //  eq("goods",Goods.findAllByM111IDOrM111NamaIlike("%"+params."sCriteria_goods"+"%"))
            }

            if (params."sCriteria_kodeGoods") {

                goods{
                    ilike("m111ID","%"+params."sCriteria_kodeGoods"+"%" )
                }
                //  eq("goods",Goods.findAllByM111IDOrM111NamaIlike("%"+params."sCriteria_goods"+"%"))
            }

            if (params."sCriteria_t160PersenKenaikanHarga") {
                eq("t160PersenKenaikanHarga", Double.parseDouble(params."sCriteria_t160PersenKenaikanHarga"))
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                case "kodeGoods" :
                    goods{
                        order("m111ID", sortDir)
                    }
                    break;
                case "namaGoods" :
                    goods{
                        order("m111Nama", sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t160TMT: it.t160TMT ? it.t160TMT.format(dateFormat) : "",

                    kodeGoods : it.goods?.m111ID,

                    namaGoods: it.goods?.m111Nama,

                    t160PersenKenaikanHarga: it.t160PersenKenaikanHarga,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {

        def good = new Goods()

        if(params.idGood!=null){
            good = Goods.findById(Double.parseDouble(params.idGood))
        //    log.info("ini goodnya ada : "+ good.toString())

        }
        [hargaJualNonSPLDPerPartInstance: new HargaJualNonSPLDPerPart(params), good : good]

    }

    def save() {
        def hargaJualNonSPLDPerPartInstance = new HargaJualNonSPLDPerPart()

        hargaJualNonSPLDPerPartInstance.t160PersenKenaikanHarga = Double.parseDouble(params.t160PersenKenaikanHarga)
        hargaJualNonSPLDPerPartInstance.t160TMT = params.t160TMT
        hargaJualNonSPLDPerPartInstance?.goods = Goods.findById(params.goodsId as Long)
        hargaJualNonSPLDPerPartInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        hargaJualNonSPLDPerPartInstance.setLastUpdProcess("INSERT")
        hargaJualNonSPLDPerPartInstance.dateCreated = datatablesUtilService?.syncTime()
        hargaJualNonSPLDPerPartInstance.lastUpdated = datatablesUtilService?.syncTime()
        hargaJualNonSPLDPerPartInstance.companyDealer = CompanyDealer.findById(session.userCompanyDealerId)


        def duplicate = HargaJualNonSPLDPerPart.createCriteria().list {
            and{
                eq("t160TMT",hargaJualNonSPLDPerPartInstance.t160TMT)
                eq("goods",hargaJualNonSPLDPerPartInstance.goods)
                eq("companyDealer",hargaJualNonSPLDPerPartInstance.companyDealer)
            }
        }

        if(duplicate){
            flash.message = "Data Goods telah digunakan (duplicate)"
            render(view: "create", model: [hargaJualNonSPLDPerPartInstance: hargaJualNonSPLDPerPartInstance])
            return
        }

        if (!hargaJualNonSPLDPerPartInstance.save(flush: false)) {
            render(view: "create", model: [hargaJualNonSPLDPerPartInstance: hargaJualNonSPLDPerPartInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), hargaJualNonSPLDPerPartInstance.id])
        redirect(action: "show", id: hargaJualNonSPLDPerPartInstance.id)
    }

    def show(Long id) {
        def hargaJualNonSPLDPerPartInstance = HargaJualNonSPLDPerPart.get(id)
        if (!hargaJualNonSPLDPerPartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), id])
            redirect(action: "list")
            return
        }

        [hargaJualNonSPLDPerPartInstance: hargaJualNonSPLDPerPartInstance]
    }

    def edit() {

        def hargaJualNonSPLDPerPartInstance = HargaJualNonSPLDPerPart.get(Double.parseDouble(params.id))
        if (!hargaJualNonSPLDPerPartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), id])
            redirect(action: "list")
            return
        }

        def good = hargaJualNonSPLDPerPartInstance.goods

        if(params.idGood!=null){
            good = Goods.findById(Double.parseDouble(params.idGood))
            log.info("ini goodnya ada : "+ good.toString())

        }

        [hargaJualNonSPLDPerPartInstance: hargaJualNonSPLDPerPartInstance, good : good ]
    }

    def update(Long id, Long version) {
        def hargaJualNonSPLDPerPartInstance = HargaJualNonSPLDPerPart.findById(Double.parseDouble(params.idHargaJual))
        if (!hargaJualNonSPLDPerPartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (hargaJualNonSPLDPerPartInstance.version > version) {

                hargaJualNonSPLDPerPartInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart')] as Object[],
                        "Data telah digunakan duplicated")
                render(view: "edit", model: [hargaJualNonSPLDPerPartInstance: hargaJualNonSPLDPerPartInstance])
                return
            }
        }

        hargaJualNonSPLDPerPartInstance.t160PersenKenaikanHarga = Double.parseDouble(params.t160PersenKenaikanHarga)
        hargaJualNonSPLDPerPartInstance.t160TMT = params.t160TMT
        hargaJualNonSPLDPerPartInstance.goods = Goods.findByM111Nama(params.goods)
        hargaJualNonSPLDPerPartInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        hargaJualNonSPLDPerPartInstance.setLastUpdProcess("UPDATE")
        hargaJualNonSPLDPerPartInstance.setLastUpdated(datatablesUtilService?.syncTime())
        hargaJualNonSPLDPerPartInstance.companyDealer = CompanyDealer.findById(session.userCompanyDealerId)

        def duplicate = HargaJualNonSPLDPerPart.createCriteria().list {
            and{
                eq("t160TMT",hargaJualNonSPLDPerPartInstance.t160TMT)
                eq("goods",hargaJualNonSPLDPerPartInstance.goods)
                eq("companyDealer",hargaJualNonSPLDPerPartInstance.companyDealer)
                notEqual("id", id)
            }
        }

        if(duplicate){
            flash.message = "Data Goods telah digunakan (duplicate)"
            hargaJualNonSPLDPerPartInstance.discard()
            render(view: "edit", model: [hargaJualNonSPLDPerPartInstance: hargaJualNonSPLDPerPartInstance])
            return
        }

        if (!hargaJualNonSPLDPerPartInstance.save(flush: true)) {
            render(view: "edit", model: [hargaJualNonSPLDPerPartInstance: hargaJualNonSPLDPerPartInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), hargaJualNonSPLDPerPartInstance.id])
        redirect(action: "show", id: hargaJualNonSPLDPerPartInstance.id)
    }

    def delete(Long id) {
        def hargaJualNonSPLDPerPartInstance = HargaJualNonSPLDPerPart.get(id)
        if (!hargaJualNonSPLDPerPartInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), id])
            redirect(action: "list")
            return
        }

        try {
            hargaJualNonSPLDPerPartInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'hargaJualNonSPLDPerPart.label', default: 'HargaJualNonSPLDPerPart'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(HargaJualNonSPLDPerPart, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(HargaJualNonSPLDPerPart, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def listParts() {
        def res = [:]
        def opts = []
        def z = Goods.createCriteria()
        def results=z.list {
            or{
                ilike("m111Nama","%" + params.query + "%")
                ilike("m111ID","%" + params.query + "%")
            }
            eq('staDel', '0')
            setMaxResults(10)
        }
        results.each {
            opts<<it.m111ID.trim() +" || "+ it.m111Nama
        }
        res."options" = opts
        render res as JSON

    }

    def detailParts(){
        def result = [:]
        def data= params.noGoods as String
        if(data.contains("||")){
            def dataNogoods = data.split(" ")
            def goods = Goods.createCriteria().list {
                eq("staDel","0")
                ilike("m111ID","%"+dataNogoods[0].trim() + "%")
            }
            if(goods.size()>0){
                def goodsData = goods.last()
                result."hasil" = "ada"
                result."id" = goodsData?.id
                result."namaGoods" = goodsData?.m111Nama
                render result as JSON
            }else{
                result."hasil" = "tidakAda"
                render result as JSON
            }
        }else{
            result."hasil" = "tidakBisa"
            render result as JSON
        }
    }

}
