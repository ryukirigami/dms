package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat

class PartTipeOrderController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def cekWaktu(String jam){
        String jamTemp = jam;
        if(jamTemp.indexOf(':')<0){
            jamTemp+=':00'
        }else{
            if(jamTemp.indexOf(':')==0){
                String temp = jamTemp
                jamTemp = '-1'+temp
            }
            if(jamTemp.indexOf(':')==jamTemp.length()-1){
                jamTemp+='00'
            }
        }
        return jamTemp;
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PartTipeOrder.createCriteria()
        DateFormat df = new SimpleDateFormat("HH:mm")
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            vendor{
                eq("staDel","0")
            }
            if (params."sCriteria_m112TglBerlaku") {
                ge("m112TglBerlaku", params."sCriteria_m112TglBerlaku")
                lt("m112TglBerlaku", params."sCriteria_m112TglBerlaku" + 1)
            }

            if (params."sCriteria_vendor") {
                vendor{
                    ilike("m121Nama","%"+ params."sCriteria_vendor"+"%")
                }

            }

            if (params."sCriteria_m112TipeOrder") {
                ilike("m112TipeOrder", "%" + (params."sCriteria_m112TipeOrder" as String) + "%")
            }

            if(params."sCriteria_m112JamLamaDatang"){

                and{
                    le("m112JamLamaDatang1",Integer.parseInt(params."sCriteria_m112JamLamaDatang"))
                    ge("m112JamLamaDatang2",Integer.parseInt(params."sCriteria_m112JamLamaDatang"))
                }
            }

//            if (params."sCriteria_m112JamLamaDatang1") {
//                eq("m112JamLamaDatang1", params."sCriteria_m112JamLamaDatang1")
//            }
//
//            if (params."sCriteria_m112JamLamaDatang2") {
//                eq("m112JamLamaDatang2", params."sCriteria_m112JamLamaDatang2")
//            }

            if (params."sCriteria_m112CutoffTime1") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime1")
                Date date1 = df.parse(jam)
                eq("m112CutoffTime1",date1)
            }

            if (params."sCriteria_m112CutoffTime2") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime2")
                Date date2 = df.parse(jam)
                eq("m112CutoffTime2",date2)
            }

            if (params."sCriteria_m112CutoffTime3") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime3")
                Date date3 = df.parse(jam)
                eq("m112CutoffTime3",date3)
            }

            if (params."sCriteria_m112CutoffTime4") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime4")
                Date date4 = df.parse(jam)
                eq("m112CutoffTime4",date4)
            }

            if (params."sCriteria_m112CutoffTime5") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime5")
                Date date5 = df.parse(jam)
                eq("m112CutoffTime5",date5)
            }

            if (params."sCriteria_m112CutoffTime6") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime6")
                Date date6 = df.parse(jam)
                eq("m112CutoffTime6",date6)
            }

            if (params."sCriteria_m112CutoffTime7") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime7")
                Date date7 = df.parse(jam)
                eq("m112CutoffTime7",date7)
            }

            if (params."sCriteria_m112CutoffTime8") {
                String jam = cekWaktu(params."sCriteria_m112CutoffTime8")
                Date date8 = df.parse(jam)
                eq("m112CutoffTime8",date8)
            }

//            if (params."sCriteria_staDel") {
//                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
//            }
//
//            if (params."sCriteria_m112ID") {
//                ilike("m112ID", "%" + (params."sCriteria_m112ID" as String) + "%")
//            }
//
//            if (params."sCriteria_m112StaCutOffTime") {
//                eq("m112StaCutOffTime", params."sCriteria_m112StaCutOffTime")
//            }
//
//            if (params."sCriteria_createdBy") {
//                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
//            }
//
//            if (params."sCriteria_updatedBy") {
//                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
//            }
//
//            if (params."sCriteria_lastUpdProcess") {
//                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
//            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m112TglBerlaku: it.m112TglBerlaku?it.m112TglBerlaku.format(dateFormat):"",

                    vendor: it.vendor?.m121Nama,

                    m112TipeOrder: it.m112TipeOrder,

                    m112JamLamaDatang1: it.m112JamLamaDatang1 + " - " + it.m112JamLamaDatang2,

//                    m112JamLamaDatang2: it.m112JamLamaDatang2,

                    m112CutoffTime1: it.m112CutoffTime1 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime1?.getTime())),

                    m112CutoffTime2: it.m112CutoffTime2 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime2?.getTime())),

                    m112CutoffTime3: it.m112CutoffTime3 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime3?.getTime())),

                    m112CutoffTime4: it.m112CutoffTime4 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime4?.getTime())),

                    m112CutoffTime5: it.m112CutoffTime5 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime5?.getTime())),

                    m112CutoffTime6: it.m112CutoffTime6 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime6?.getTime())),

                    m112CutoffTime7: it.m112CutoffTime7 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime7?.getTime())),

                    m112CutoffTime8: it.m112CutoffTime8 == null ? "":new SimpleDateFormat("HH:mm").format(new Date(it.m112CutoffTime8?.getTime())),

//                    staDel: it.staDel,
//
//                    m112ID: it.m112ID,
//
//                    m112StaCutOffTime: it.m112StaCutOffTime,
//
//                    createdBy: it.createdBy,
//
//                    updatedBy: it.updatedBy,
//
//                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [partTipeOrderInstance: new PartTipeOrder(params)]
    }

    def save() {
        def partTipeOrderInstance = new PartTipeOrder()

        partTipeOrderInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        partTipeOrderInstance?.lastUpdProcess = "INSERT"
        partTipeOrderInstance?.setStaDel('0')
        partTipeOrderInstance?.dateCreated = datatablesUtilService?.syncTime()
        partTipeOrderInstance?.lastUpdated = datatablesUtilService?.syncTime()

        if(PartTipeOrder.findByVendorAndM112TglBerlakuAndM112TipeOrderAndM112JamLamaDatang1AndM112JamLamaDatang2(Vendor?.findById(Long.parseLong(params?.vendor?.id)), params?.m112TglBerlaku, params?.m112TipeOrder, Integer.parseInt(params?.m112JamLamaDatang1), Integer.parseInt(params?.m112JamLamaDatang2))==null){
            partTipeOrderInstance?.m112TglBerlaku = params?.m112TglBerlaku
            partTipeOrderInstance?.m112TipeOrder = params?.m112TipeOrder
            partTipeOrderInstance?.m112JamLamaDatang1 = Integer.parseInt(params?.m112JamLamaDatang1)
            partTipeOrderInstance?.m112JamLamaDatang2 = Integer.parseInt(params?.m112JamLamaDatang2)
            partTipeOrderInstance?.vendor = Vendor?.findById(Long.parseLong(params?.vendor?.id))
            if (params?.m112CutoffTime1_hour&&params?.m112CutoffTime1_minute){
                Time time1 = new Time(Integer?.parseInt(params?.m112CutoffTime1_hour),Integer?.parseInt(params?.m112CutoffTime1_minute),0)
                partTipeOrderInstance?.setM112CutoffTime1(time1)
            }
            if (params?.m112CutoffTime2_hour&&params?.m112CutoffTime2_minute){
                Time time2 = new Time(Integer?.parseInt(params?.m112CutoffTime2_hour),Integer?.parseInt(params?.m112CutoffTime2_minute),0)
                partTipeOrderInstance?.setM112CutoffTime2(time2)
            }
            if (params?.m112CutoffTime3_hour&&params?.m112CutoffTime3_minute){
                Time time3 = new Time(Integer?.parseInt(params?.m112CutoffTime3_hour),Integer?.parseInt(params?.m112CutoffTime3_minute),0)
                partTipeOrderInstance?.setM112CutoffTime3(time3)
            }
            if (params?.m112CutoffTime4_hour&&params?.m112CutoffTime4_minute){
                Time time4 = new Time(Integer?.parseInt(params?.m112CutoffTime4_hour),Integer?.parseInt(params?.m112CutoffTime4_minute),0)
                partTipeOrderInstance?.setM112CutoffTime4(time4)
            }
            if (params?.m112CutoffTime5_hour&&params?.m112CutoffTime5_minute){
                Time time5 = new Time(Integer?.parseInt(params?.m112CutoffTime5_hour),Integer?.parseInt(params?.m112CutoffTime5_minute),0)
                partTipeOrderInstance?.setM112CutoffTime5(time5)
            }
            if (params?.m112CutoffTime6_hour&&params?.m112CutoffTime6_minute){
                Time time6 = new Time(Integer?.parseInt(params?.m112CutoffTime6_hour),Integer?.parseInt(params?.m112CutoffTime6_minute),0)
                partTipeOrderInstance?.setM112CutoffTime6(time6)
            }
            if (params?.m112CutoffTime7_hour&&params?.m112CutoffTime7_minute){
                Time time7 = new Time(Integer?.parseInt(params?.m112CutoffTime7_hour),Integer?.parseInt(params?.m112CutoffTime7_minute),0)
                partTipeOrderInstance?.setM112CutoffTime7(time7)
            }
            if (params?.m112CutoffTime8_hour&&params?.m112CutoffTime8_minute){
                Time time8 = new Time(Integer?.parseInt(params?.m112CutoffTime8_hour),Integer?.parseInt(params?.m112CutoffTime8_minute),0)
                partTipeOrderInstance?.setM112CutoffTime8(time8)
            }

            if(partTipeOrderInstance.m112JamLamaDatang1>=partTipeOrderInstance.m112JamLamaDatang2){
                flash.message = "Lead Time Supply pertama harus lebih kecil dari yang kedua"
                render(view: "create", model: [partTipeOrderInstance: partTipeOrderInstance])
                return
            }

            if (!partTipeOrderInstance.save(flush: true)) {
                render(view: "create", model: [partTipeOrderInstance: partTipeOrderInstance])
                return
            }
        }else {
            flash.message = message(code: 'default.created.failed.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), Vendor?.findById(Long.parseLong(params?.vendor?.id)),params?.m112TglBerlaku,params?.m112TipeOrder,params?.m112JamLamaDatang1])
            render(view: "create", model: [partTipeOrderInstance: partTipeOrderInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), partTipeOrderInstance?.id])
        redirect(action: "show", id: partTipeOrderInstance?.id)

    }

    def show(Long id) {
        def partTipeOrderInstance = PartTipeOrder.get(id)
        if (!partTipeOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), id])
            redirect(action: "list")
            return
        }

        [partTipeOrderInstance: partTipeOrderInstance]
    }

    def edit(Long id) {
        def partTipeOrderInstance = PartTipeOrder.get(id)
        if (!partTipeOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), id])
            redirect(action: "list")
            return
        }

        [partTipeOrderInstance: partTipeOrderInstance]
    }

    def update(Long id, Long version) {
        def partTipeOrderInstance = PartTipeOrder.get(id)
        if (!partTipeOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (partTipeOrderInstance.version > version) {

                partTipeOrderInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'partTipeOrder.label', default: 'PartTipeOrder')] as Object[],
                        "Another user has updated this PartTipeOrder while you were editing")
                render(view: "edit", model: [partTipeOrderInstance: partTipeOrderInstance])
                return
            }
        }

//        partTipeOrderInstance.properties = params
        partTipeOrderInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        partTipeOrderInstance?.lastUpdProcess = "UPDATE"
        partTipeOrderInstance?.lastUpdated = datatablesUtilService?.syncTime()
        def partEdit = PartTipeOrder.findByVendorAndM112TglBerlakuAndM112TipeOrderAndM112JamLamaDatang1AndM112JamLamaDatang2(Vendor?.findById(Long.parseLong(params?.vendor?.id)), params?.m112TglBerlaku, params?.m112TipeOrder, Integer.parseInt(params?.m112JamLamaDatang1), Integer.parseInt(params?.m112JamLamaDatang2))
        if(!partEdit || partEdit.id==id ){
            partTipeOrderInstance?.m112TglBerlaku = params?.m112TglBerlaku
            partTipeOrderInstance?.m112TipeOrder = params?.m112TipeOrder
            partTipeOrderInstance?.m112JamLamaDatang1 = Integer.parseInt(params?.m112JamLamaDatang1)
            partTipeOrderInstance?.m112JamLamaDatang2 = Integer.parseInt(params?.m112JamLamaDatang2)
            partTipeOrderInstance?.vendor = Vendor?.findById(Long.parseLong(params?.vendor?.id))
                partTipeOrderInstance?.setM112CutoffTime1(null)
                partTipeOrderInstance?.setM112CutoffTime2(null)
                partTipeOrderInstance?.setM112CutoffTime3(null)
                partTipeOrderInstance?.setM112CutoffTime4(null)
                partTipeOrderInstance?.setM112CutoffTime5(null)
                partTipeOrderInstance?.setM112CutoffTime6(null)
                partTipeOrderInstance?.setM112CutoffTime7(null)
                partTipeOrderInstance?.setM112CutoffTime8(null)
            if (params?.m112CutoffTime1_hour&&params?.m112CutoffTime1_minute){
                Time time1 = new Time(Integer?.parseInt(params?.m112CutoffTime1_hour),Integer?.parseInt(params?.m112CutoffTime1_minute),0)
                partTipeOrderInstance?.setM112CutoffTime1(time1)
            }
            if (params?.m112CutoffTime2_hour&&params?.m112CutoffTime2_minute){
                Time time2 = new Time(Integer?.parseInt(params?.m112CutoffTime2_hour),Integer?.parseInt(params?.m112CutoffTime2_minute),0)
                partTipeOrderInstance?.setM112CutoffTime2(time2)
            }
            if (params?.m112CutoffTime3_hour&&params?.m112CutoffTime3_minute){
                Time time3 = new Time(Integer?.parseInt(params?.m112CutoffTime3_hour),Integer?.parseInt(params?.m112CutoffTime3_minute),0)
                partTipeOrderInstance?.setM112CutoffTime3(time3)
            }
            if (params?.m112CutoffTime4_hour&&params?.m112CutoffTime4_minute){
                Time time4 = new Time(Integer?.parseInt(params?.m112CutoffTime4_hour),Integer?.parseInt(params?.m112CutoffTime4_minute),0)
                partTipeOrderInstance?.setM112CutoffTime4(time4)
            }
            if (params?.m112CutoffTime5_hour&&params?.m112CutoffTime5_minute){
                Time time5 = new Time(Integer?.parseInt(params?.m112CutoffTime5_hour),Integer?.parseInt(params?.m112CutoffTime5_minute),0)
                partTipeOrderInstance?.setM112CutoffTime5(time5)
            }
            if (params?.m112CutoffTime6_hour&&params?.m112CutoffTime6_minute){
                Time time6 = new Time(Integer?.parseInt(params?.m112CutoffTime6_hour),Integer?.parseInt(params?.m112CutoffTime6_minute),0)
                partTipeOrderInstance?.setM112CutoffTime6(time6)
            }
            if (params?.m112CutoffTime7_hour&&params?.m112CutoffTime7_minute){
                Time time7 = new Time(Integer?.parseInt(params?.m112CutoffTime7_hour),Integer?.parseInt(params?.m112CutoffTime7_minute),0)
                partTipeOrderInstance?.setM112CutoffTime7(time7)
            }
            if (params?.m112CutoffTime8_hour&&params?.m112CutoffTime8_minute){
                Time time8 = new Time(Integer?.parseInt(params?.m112CutoffTime8_hour),Integer?.parseInt(params?.m112CutoffTime8_minute),0)
                partTipeOrderInstance?.setM112CutoffTime8(time8)
            }

            if(partTipeOrderInstance.m112JamLamaDatang1>=partTipeOrderInstance.m112JamLamaDatang2){
                flash.message = "Lead Time Supply pertama harus lebih kecil dari yang kedua"
                render(view: "edit", model: [partTipeOrderInstance: partTipeOrderInstance])
                return
            }

            if (!partTipeOrderInstance.save(flush: true)) {
                render(view: "edit", model: [partTipeOrderInstance: partTipeOrderInstance])
                return
            }
        }
//        }
        else {
            flash.message = message(code: 'default.created.failed.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), Vendor?.findById(Long.parseLong(params?.vendor?.id)),params?.m112TglBerlaku,params?.m112TipeOrder,params?.m112JamLamaDatang1])
            render(view: "edit", model: [partTipeOrderInstance: partTipeOrderInstance])
            return

        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), partTipeOrderInstance.id])
        redirect(action: "show", id: partTipeOrderInstance.id)
    }

    def delete(Long id) {
        def partTipeOrderInstance = PartTipeOrder.get(id)
        if (!partTipeOrderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), id])
            redirect(action: "list")
            return
        }

        try {
            partTipeOrderInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            partTipeOrderInstance?.lastUpdProcess = "DELETE"
            partTipeOrderInstance?.setStaDel('1')
            partTipeOrderInstance?.lastUpdated = datatablesUtilService?.syncTime()
            partTipeOrderInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'partTipeOrder.label', default: 'PartTipeOrder'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(PartTipeOrder, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PartTipeOrder, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
