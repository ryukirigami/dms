package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadETAController {


    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
    }

    def upload() {
        def goods = null
        def po = null
        def requestBody = request.JSON
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm")
        Date date;
        ETA etaInstance = null

        requestBody.each{
            etaInstance = new ETA()
            goods = Goods.findByM111ID(it.kodeParts)
            po = PO.findByT164NoPO(it.nomorPO)
            if(goods && it.eta && po && it.qty){

                etaInstance = ETA.findByPoAndGoodsAndCompanyDealer(po, goods,session?.userCompanyDealer)
                if(!etaInstance){
                    etaInstance = new ETA()
                    etaInstance.dateCreated = datatablesUtilService?.syncTime()
                    etaInstance.companyDealer = session?.userCompanyDealer
                }

                etaInstance.lastUpdated = datatablesUtilService?.syncTime()
                etaInstance.goods = goods
                etaInstance.po = po
                etaInstance.t165ETA = df.parse(it.eta)
                etaInstance.t165Qty1 = Double.parseDouble(it.qty)

                etaInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                etaInstance.lastUpdProcess = "Upload"
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                def cek = ETA.find(etaInstance)
                if(!cek){
                    etaInstance.save()
                }
            }
        }

        flash.message = message(code: 'default.uploadETAError.message', default: "Save Harga ETA Done")
        render(view: "index", model: [operationInstance: etaInstance])
    }

    def view() {
        def  etaInstance = new ETA(params)
        //handle upload file
        Map CONFIG_HARGA_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [                        //Col, Map-Key
                        'A':'nomorPO',
                        'B':'kodeParts',
                        'C':'qty',
                        'D':'eta'

                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        int jmlhDataDuplicate = 0;
        if(!uploadExcel?.empty){

            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [operationInstance: operationInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def etaList = excelImportService.columns(workbook, CONFIG_HARGA_COLUMN_MAP)

            jsonData = etaList as JSON
            PO po = null
            int qty = 0
            Goods goods

            String status = "0", style = ""
            etaList?.each {
                if((it.eta && it.eta!="") && (it.kodeParts && it.kodeParts !="") && (it.qty && it.qty!="") && (it.nomorPO && it.nomorPO!="") ){

                    try{
                        qty = Double.parseDouble(it.qty)
                    }catch(Exception ex){
                        qty = 0
                    }

                    po = PO.findByT164NoPO(it.nomorPO)

                    if(po){
                        println "po available"
                    }

                    goods = Goods.findByM111ID(it.kodeParts)
                    if(goods){
                        println "giids available"
                    }


                    def cekDup = ETA.findByPoAndGoods(po, goods)

                    if(po==null || goods==null || qty == 0 ){
                        jmlhDataError++;
                        status = "1";
                    }else if(cekDup){
                        jmlhDataDuplicate++
                        status = "2"
                    }



                } else {

                    jmlhDataError++;
                    status = "1";

                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else if(status.equals("2")) {
                    style = "style='color:blue;'"
                }else{
                    style = ""
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.nomorPO+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.kodeParts+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.qty+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.eta+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"

                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadETAError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else if(jmlhDataDuplicate > 0) {
            flash.message = message(code: 'default.uploadETAError.message', default: "Read File Done : Terdapat "+jmlhDataDuplicate+" data yang duplikat (Biru), save untuk mereplace")

        }else{
            flash.message = message(code: 'default.uploadETAError.message', default: "Read File Done")
        }

        render(view: "index", model: [etaInstance : etaInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }
}
