package com.kombos.parts

import com.kombos.administrasi.GeneralParameter
import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.utils.DatatablesUtilService
import com.kombos.customerprofile.HistoryCustomer
import com.kombos.generatecode.GenerateCodeService
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.DateFormat
import java.text.SimpleDateFormat

class SalesOrderController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService = new DatatablesUtilService()

    def conversi = new Konversi()

    def salesOrderService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable : new Date().format("ddMMyyyymmss")]
    }

    def addParts() {

    }

    def formAddParts() {
        def aksi = params.aksi
        def salesOrder = new SalesOrder()
        def ppn = 10
        def generalParam = GeneralParameter.findByCompanyDealerAndM000StaDel(session.userCompanyDealer,"0")
        if(generalParam?.m000Tax){
            ppn = generalParam?.m000Tax?.toInteger()
        }
        if(params.id){
            salesOrder = SalesOrder.get(params.id.toLong())
        }
        [aksi : aksi , salesOrderInstance: salesOrder , ppn : ppn]
    }

    def datatablesList() {
        params?.companyDealer = session?.userCompanyDealer
        session.exportParams = params

        render salesOrderService.datatablesList(params) as JSON
    }

    def showParts() {
        DeliveryOrder doNumber = null
        SalesOrder sorNumber = null
        def sorNumberDetail = null
        try {
            sorNumber =  SalesOrder.get(params.id as Long)
            sorNumberDetail = SalesOrderDetail.findAllBySorNumberAndStaDel(sorNumber,'0')
        }catch (Exception e){}


        [idTable : new Date().format("ddMMyyyymmss"), sorNumber: sorNumber,sorNumberDetail:sorNumberDetail]
    }

    def datatablesPartsList() {
        session.exportParams = params
        params?.companyDealer = session?.userCompanyDealer
        render salesOrderService.datatablesPartList(params) as JSON
    }
    def generateNomorSO(){
        def result = [:]
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = SalesOrder.createCriteria()
        def results = c.list {
            eq("companyDealer",session?.userCompanyDealer)
            ge("dateCreated",df.parse(currentDate))
            lt("dateCreated",df.parse(currentDate) + 1)
        }

        def m = results.size()?:0
        def reg = "R"+MappingCompanyRegion.findByCompanyDealer(session.userCompanyDealer).companyDealer.m011ID
        def data = reg +"." + new Date().format("yyyyMMdd").toString() + "." + String.format('SO%04d',m+1)
        result.value = data
        return result
    }
    def save() {
        def jsonArray = JSON.parse(params.ids)
        def salesOrder = new SalesOrder(params)
        salesOrder?.sorNumber = generateNomorSO().value
        salesOrder?.companyDealer = session.userCompanyDealer
        salesOrder?.salesType = Franc.get(params.salesTypes.toLong())
        salesOrder?.isApprove = 2
        salesOrder?.deliveryDate = new Date().parse("dd-MM-yyyy",params?.deliveryDates)
        salesOrder?.staDel = "0"
        salesOrder.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
        salesOrder.lastUpdProcess="INSERT"
        salesOrder?.creditDays = 0
        salesOrder?.tradingFor = params.tradingFor
        salesOrder?.dueDate = new Date() + 100
        salesOrder?.sorDate = new Date()
        salesOrder?.dateCreated = datatablesUtilService?.syncTime()
        salesOrder?.lastUpdated = datatablesUtilService?.syncTime()

        salesOrder?.save(flush: true)
        salesOrder?.errors?.each {println it}

        jsonArray.each {
            def soDetail = new SalesOrderDetail()
            soDetail?.sorNumber = salesOrder
            soDetail?.materialCode = Goods?.get(it as Long)
            soDetail?.quantity = params."qty_${it}".toDouble()
            soDetail?.unitPrice = params."harga_${it}".toDouble()
            def hargaBeli = 0
            try {
                hargaBeli  = GoodsHargaBeli.findByGoods(soDetail?.materialCode)?.t150Harga
            }catch (Exception e){}
            soDetail?.unitPriceBeli = hargaBeli
            soDetail?.discount = params."discount_${it}".toDouble()
            soDetail?.staDel = "0"
            soDetail.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            soDetail.lastUpdProcess="INSERT"
            soDetail.dateCreated=datatablesUtilService?.syncTime()
            soDetail.lastUpdated=datatablesUtilService?.syncTime()
            soDetail?.save(flush: true)
            soDetail?.errors?.each {println it}
        }

        def approval = new ApprovalT770(
                t770FK:salesOrder.id,
                companyDealer: salesOrder.companyDealer,
                createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                kegiatanApproval: KegiatanApproval.findByM770KegiatanApprovalAndStaDel(KegiatanApproval.PARTS_SALES,"0"),
                lastUpdProcess: "INSERT",
                staDel: '0',
                t770NoDokumen: salesOrder.sorNumber,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: "Butuh approval penjualan langsung",
                updatedBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                t770xNamaUser: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush: true)
        approval.errors.each {println it}

        def customer = HistoryCustomer.findByT182NamaDepan(salesOrder.customerName)
        if(!customer){
            def h = new HistoryCustomer(
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime(),
                    t182ID : new GenerateCodeService().codeGenerateSequence("T182_T102_ID",session.userCompanyDealer),
                    t182NamaDepan: salesOrder.customerName
            )
            h.save(flush: true)

        }
        render "OK"
    }

    def update() {
        def jsonArray = JSON.parse(params.ids)
        def jsonArrayDel = JSON.parse(params.idsDel)
        def sorTemp = SalesOrder.createCriteria().list {eq("staDel","0");eq("companyDealer",session?.userCompanyDealer)}
        def idLast = "SO001"
        if(sorTemp.size()>0){
            def temp = idLast.substring(idLast.indexOf("O")+1)
            def temp2 = temp.toInteger()+1
            if(temp2.toString().length()==1){
                idLast="SO00"+temp2
            }else if (temp2.toString().length()==2){
                idLast="SO0"+temp2
            }else{
                idLast="SO"+temp2
            }
        }
        def salesOrder = SalesOrder.get(params.idSO.toLong())
        params.lastUpdated = datatablesUtilService?.syncTime()
        salesOrder.properties = params
        salesOrder?.salesType = Franc.get(params.salesTypes.toLong())
        salesOrder?.deliveryDate = new Date().parse("dd-MM-yyyy",params?.deliveryDates)
        salesOrder.updatedBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
        salesOrder.lastUpdProcess="UPDATE"
        salesOrder?.save(flush: true)
        salesOrder?.errors?.each {println it}

        jsonArrayDel.each {
            def soDetail = SalesOrderDetail.findByMaterialCodeAndStaDel(Goods?.get(it.toLong()),"0")
            if(soDetail){
                soDetail.delete()
            }
        }

        jsonArray.each {
            def awal = 0
            def soDetail = new SalesOrderDetail()
            def fSoDetail = SalesOrderDetail.findByMaterialCodeAndStaDel(Goods?.get(it.toLong()),"0")
            if(fSoDetail){
                awal = fSoDetail.quantity
                soDetail = fSoDetail
            }else{
                soDetail.dateCreated = datatablesUtilService?.syncTime()
            }
            soDetail.lastUpdated = datatablesUtilService?.syncTime()
            soDetail?.sorNumber = salesOrder
            soDetail?.materialCode = Goods?.get(it.toLong())
            soDetail?.quantity = params."qty_${it}".toDouble()
            soDetail?.unitPrice = params."harga_${it}".toDouble()
            def hargaBeli = 0
            try {
                hargaBeli  = GoodsHargaBeli.findByGoods(soDetail?.materialCode)?.t150Harga
            }catch (Exception e){}
            soDetail?.unitPriceBeli = hargaBeli
            soDetail?.discount = params."discount_${it}".toDouble()
            soDetail?.staDel = "0"
            soDetail.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
            soDetail.lastUpdProcess="UPDATE"
            soDetail?.save(flush: true)
            soDetail?.errors?.each {println it}
        }

        render "OK"
    }

    def massdelete() {
        params.companyDealer = session?.userCompanyDealer
        def hasil = salesOrderService.massDelete(params)
        render "ok"
    }

    def listCustomer(){
        def res = [:]
        def opts = []
        def customers = CustomerSales.createCriteria().list {
            eq("staDel","0")
            eq("companyDealer",session?.userCompanyDealer)
            ilike("nama",params?.query+"%");
            maxResults(10);
        }
        customers.each {
            opts<<it.nama
        }
        res."options" = opts
        render res as JSON
    }

    def tableDetailData(){
        def result = []
        def hasil = SalesOrderDetail.createCriteria().list() {
            eq("staDel","0")
            sorNumber{
                eq("id",params.id.toLong())
            }
        }
        def total = 0
        hasil.each {
            def jumlah = it?.discount && it?.discount > 0 ? it.quantity * it.unitPrice - (it.discount/100 * it.quantity * it.unitPrice) : it.quantity * it.unitPrice
            total+=jumlah
            def stok = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it.materialCode,"0",session?.userCompanyDealer)
            result << [
                    id : it.materialCode.id,
                    m111IDAdd : it.materialCode.m111ID,
                    m111NamaAdd : it.materialCode.m111Nama,
                    stokAdd : stok ? stok?.t131Qty1Free : 0,
                    qtyAdd : it.quantity,
                    hargaAdd : conversi.toRupiah(it?.unitPrice) ? conversi.toRupiah(it?.unitPrice) : 0,
                    satuanAdd : it.materialCode.satuan.m118Satuan1,
                    discountAdd : it.discount,
                    jumlahAdd : jumlah ? conversi.toRupiah(jumlah) : 0,
                    total : total ? conversi.toRupiah(total) : 0
            ]
        }
        render result as JSON
    }
    def printSalesOrder(){
        def jsonArray = JSON.parse(params.idSalesOrder)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {

            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'salesOrder.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )

            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("salesOrder_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()

    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def so = SalesOrder.findById(id as Long)
        def c = SalesOrderDetail.createCriteria()
        def results = c.list {
            sorNumber{
                eq("id",id as Long)
            }
            eq("staDel","0")
        }

        results.sort {
            it.materialCode.m111ID
        }

        int count = 0
        def total = 0
        results.each {
            //
            def hargaTotal = it.unitPrice*it?.quantity
            def hargaDisc = it?.discount ? (hargaTotal * it.discount/100) : 0
            def subTotal = hargaTotal-hargaDisc
            def data = [:]
            count = count + 1
            data.put("sor",it.sorNumber?.sorNumber)
            data.put("tglPengiriman",it?.sorNumber?.deliveryDate?.format("dd/MM/YYYY"))
            //
            total += subTotal
            data.put("no",count)
            data.put("kodeParts",it.materialCode.m111ID)
            data.put("namaParts",it.materialCode.m111Nama)
            data.put("qty",it.quantity)
            data.put("disc",it.discount)
            data.put("satuan",it.materialCode.satuan.m118Satuan1)
            data.put("harga","Rp. " + conversi?.toRupiah(it.unitPrice))
            data.put("jumlah","Rp. " + conversi?.toRupiah(subTotal))
            data.put("total","Rp. " + conversi?.toRupiah(total))
            reportData.add(data)
        }

        return reportData

    }
}
