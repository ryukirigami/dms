package com.kombos.parts

import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.NamaApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.ApprovalT770
import com.kombos.maintable.HistoryApproval
import grails.converters.JSON

class PartsAdjustApproveController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']
    def index() {

        redirect(action: "list", params: params)
    }

    def popUp(){
            [status:params.status]
    }

    def sublist() {
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesSubList() {
        def results = PartsAdjDetail.findAllByPartsAdjustAndT146StaDel(PartsAdjust.get(params.id),"0")
        def rows = []

        results.each { partsAdjDetail ->
            def goods = partsAdjDetail?.goods
            rows << [
                    id: goods.id,
                    kodePA : partsAdjDetail.partsAdjust.t145ID,
                    pilih: partsAdjDetail != null,
                    kode: goods?.m111ID,
                    nama: goods?.m111Nama,
                    awal: partsAdjDetail == null ? 0 : partsAdjDetail?.t146JmlAwal1,
                    real: partsAdjDetail == null ? 0 : partsAdjDetail?.t146JmlAkhir1,
                    alasan: partsAdjDetail == null ? 0 : partsAdjDetail?.alasanAdjusment?.m165AlasanAdjusment,
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

        render ret as JSON
    }

    def list(Integer max) {
        if(!params.idGoods){
            params.idGoods = ['0'];
        }
        [idGoods : params.idGoods,idTable: new Date().format("yyyyMMddhhmmss")]
    }


    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PartsAdjust.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("t145StaDel","0")
            if(params."idGoods"){

                def list = []
                def listPartsAdjust = []
                list = JSON.parse(params.idGoods)
                list.each {
                    def partsAdjDetail = PartsAdjDetail.findByGoods(Goods.findByM111ID(it))
                    partsAdjDetail.each {
                        listPartsAdjust << it.partsAdjust
                    }
                }

                listPartsAdjust.each {
                    or{
                        eq("id", it.id)
                    }
                }
            }

            if (params."sCriteria_t145StatusApprove") {
                eq("t145StatusApprove", params."sCriteria_t145StatusApprove")
            }

            if (params."sCriteria_t145TglAdj" && params."sCriteria_t145TglAdjakhir") {
                ge("t145TglAdj", params."sCriteria_t145TglAdj")
                lt("t145TglAdj", params."sCriteria_t145TglAdjakhir" + 1)
            }
            if (sortProperty && !sortProperty?.toString()?.empty) {
                switch (sortProperty) {
                    default:
                        order(sortProperty, sortDir)
                        break;
                }
            }
        }

        def rows = []

        results.each {
            String statusApp = "-"
            if(it.t145StatusApprove=='1'){
                statusApp = "Approved"
            }else if(it.t145StatusApprove=='2'){
                statusApp = "UnApproved"
            }else{
                statusApp = "-"
            }
            rows << [

                    id: it.id,

                    t145ID: it.t145ID,
                    status: statusApp,

                    t145TglAdj: it.t145TglAdj ? it.t145TglAdj.format(dateFormat) : ""

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }
    def sendApprove(){
        def kegiatanApprove = KegiatanApproval.findByM770KegiatanApproval("Adjustment")
        if(!kegiatanApprove){
           def kegiatan = new KegiatanApproval(
               m770IdApproval: (KegiatanApproval.last().m770IdApproval.toInteger() + 1).toString(),
               m770KegiatanApproval: "Adjustment",
               m770NamaDomain: "partAdjustment",
               m770NamaTabel: "T145_PartsAdjust",
               m770NamaFk: "T145_ID",
               namaApprovals: NamaApproval.first(),
               m770NamaFieldDiupdate: "T145_StatusApprove",
               m770NamaFormDetail: "-",
               m770StaButuhNilai: 1,
               staDel: '0',
               afterApprovalService: 'Kegiatan AprroveService',
               createdBy: 'SYSTEM',
               lastUpdProcess: 'INSERT',
               dateCreated: datatablesUtilService?.syncTime(),
               lastUpdated: datatablesUtilService?.syncTime()
           ).save(flush: true)
        }
        def approve = new ApprovalT770(
                kegiatanApproval : KegiatanApproval.findByM770KegiatanApproval("Adjustment"),
                t770FK : 'T145_ID',
                companyDealer: session?.userCompanyDealer,
                t770NoDokumen : (ApprovalT770.last().t770NoDokumen.toInteger() + 1).toString(),
                t770TglJamSend : datatablesUtilService?.syncTime(),
                t770Status : StatusApproval.APPROVED,
                t770CurrentLevel : 1,
                t770Pesan :'-',
                staDel : '0',
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        ).save(flush: true)

        def ha = new HistoryApproval(
                approval : approve,
                status : StatusApproval.APPROVED,
                level : 1,
                tglJamApproved : new Date(),
                namaApproved : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                keterangan :params.pesan
        ).save(flush: true)

        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def pa = PartsAdjust.get(it)
            pa.t145StatusApprove = '1'
            pa.save(flush: true)
        }
        render "ok"
    }
    def sendUnApprove(){
        def kegiatanApprove = KegiatanApproval.findByM770KegiatanApproval("Adjustment")
        if(!kegiatanApprove){
            def kegiatan = new KegiatanApproval(
                    m770IdApproval: (KegiatanApproval.last().m770IdApproval.toInteger() + 1).toString(),
                    m770KegiatanApproval: "Adjustment",
                    m770NamaDomain: "partAdjustment",
                    m770NamaTabel: "T145_PartsAdjust",
                    m770NamaFk: "T145_ID",
                    namaApprovals: NamaApproval.first(),
                    m770NamaFieldDiupdate: "T145_StatusApprove",
                    m770NamaFormDetail: "-",
                    m770StaButuhNilai: 1,
                    staDel: '0',
                    afterApprovalService: 'Kegiatan AprroveService',
                    createdBy: 'SYSTEM',
                    lastUpdProcess: 'INSERT',
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
            ).save(flush: true)
        }
        def approve = new ApprovalT770(
                kegiatanApproval : KegiatanApproval.findByM770KegiatanApproval("Adjustment"),
                t770FK : 'T145_ID',
                t770NoDokumen : (ApprovalT770.last().t770NoDokumen.toInteger() + 1).toString(),
                t770TglJamSend : datatablesUtilService?.syncTime(),
                t770Status : StatusApproval.REJECTED,
                t770CurrentLevel : 1,
                t770Pesan :'-',
                companyDealer: session?.userCompanyDealer,
                staDel : '0',
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        ).save(flush: true)

        def ha = new HistoryApproval(
                approval : approve,
                status : StatusApproval.REJECTED,
                level : 1,
                tglJamApproved : new Date(),
                namaApproved : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                keterangan :params.pesan
        ).save(flush: true)

        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def pa = PartsAdjust.get(it)
            pa.t145StatusApprove = '2'
            pa.save(flush: true)
        }
        render "ok"
    }
}
