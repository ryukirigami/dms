package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class ReturnsController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def returnsService
    def vendor = null
    def noref = null
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {

        [idTable: new Date().format("yyyyMMddhhmmss") ]
    }

    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [vendor: params.vendor, t172ID : params.t172ID, t172TglJamReturn: params.t172TglJamReturn, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def subsublist() {
        [t167NoReff: params.t167NoReff, vendor: params.vendor, t172TglJamReturn: params.t172TglJamReturn, t172ID : params.t172ID, idTable: new Date().format("yyyyMMddhhmmss"), asiefData:vendor]
    }

    def datatablesList() {
        params.companyDealer = session?.userCompanyDealer
        render returnsService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
        render returnsService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {
        params.companyDealer = session?.userCompanyDealer
        render returnsService.datatablesSubSubList(params) as JSON
    }

    def create() {
        def result = returnsService.create(params)

        if (!result.error)
            return [returnsInstance: result.returnsInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = returnsService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Returns", result.returnsInstance.id])
            redirect(action: 'show', id: result.returnsInstance.id)
            return
        }

        render(view: 'create', model: [returnsInstance: result.returnsInstance])
    }

    def show(Long id) {
        def result = returnsService.show(params)

        if (!result.error)
            return [returnsInstance: result.returnsInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = returnsService.show(params)

         def reff = GoodsReceiveDetail?.findByInvoice(Returns.findById(id)?.invoice)?.t167Qty1Reff
         def rec = GoodsReceiveDetail?.findByInvoice(Returns.findById(id)?.invoice)?.t167Qty1Issued
        if (!result.error)
            return [returnsInstance: result.returnsInstance,idTable: new Date().format("yyyyMMddhhmmss"), reff : reff, rec : rec]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list', params: [ nama : "coba"])
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = returnsService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Returns", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [returnsInstance: result.returnsInstance.attach()])
    }

    def delete() {
        def result = returnsService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Returns", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        params.companyDealer = session?.userCompanyDealer
        def hasil = returnsService.massDelete(params)
        render "ok"
    }

    def printReturns(){
        def jsonArray = JSON.parse(params.idReturns)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {
            def reportData = calculateReportData(it)
            def reportDef = new JasperReportDef(name:'returns.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("returns_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def results = Returns.findAllByT172ID(id)
        results.sort {
            it.goods.m111ID
        }

        int count = 0
        double sumQty = 0
        results.each {
            def data = [:]
            sumQty = sumQty + it.t172Qty1Return

            count = count + 1

            data.put("nomorReturns",it.t172ID)
            data.put("tglReturns", it.t172TglJamReturn.format("dd MMMM yyyy HH:mm:ss"))
            data.put("vendor", it.vendor.m121Nama)
            data.put("alamatVendor",  it.vendor.m121Alamat)

            data.put("noUrut", count as String)
            data.put("kodeGoods",it.goods?.m111ID)
            data.put("namaGoods",it.goods?.m111Nama)
            data.put("nomorInvoice",it.invoice.po.t164NoPO)
            data.put("tglInvoice",it.invoice.po.t164TglPO.format(dateFormat))
            data.put("qtyReturns",it.t172Qty1Return as String)
            data.put("satuan",it.goods.satuan.m118Satuan1)

            data.put("sumQty",sumQty as String)

            reportData.add(data)
        }

        return reportData
    }
}
