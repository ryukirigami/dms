package com.kombos.parts

import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.NamaApproval
import com.kombos.customerprofile.FA
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
import org.hibernate.criterion.CriteriaSpecification

class InputOnHandAdjustmentController {

    static activiti = true

    // XXX activiti
    def taskService
    def activitiService
    def generateCodeService
    def datatablesUtilService
    def index() {
        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.PARTS_ONHANDADJUSTMENT)
        def approver = ""
        for(NamaApproval namaApproval : kegiatanApproval.namaApprovals){
            if(namaApproval.userProfile.companyDealer.id == session.userCompanyDealerId){
                approver += namaApproval.userProfile.fullname + "\r\n"
            }
        }
        [kegiatanApproval: kegiatanApproval, approver: approver,noDokumen:generateNomorDokumen().value]
    }

    def searchAndAddParts() {

    }

    def searchPartsDatatablesList() {
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }

        def ret

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0");
            if (params."sCriteria_kode") {
                ilike("m111ID", "%" + (params."sCriteria_kode" as String) + "%")
            }

            if (params."sCriteria_nama") {
                ilike("m111Nama", "%" + (params."sCriteria_nama" as String) + "%")
            }
            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
        }

        def rows = []

        results.each {
            def ps = PartsStok.findByGoodsAndStaDelAndCompanyDealer(it,'0',session?.userCompanyDealer)?.t131Qty1
            if(!ps){
                ps = 0
            }
            rows << [
                    id: it.id,
                    kode: it?.m111ID,
                    nama: it?.m111Nama,
                    satuan: it?.satuan?.m118Satuan1,
                    awalData : ps
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }


    def tambahParts() {
        def fa = FA.read(params.fa)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << it }
        session.selectedParts = oList
        render "ok"
    }

    def partsDatatablesList() {
        def results = session?.selectedParts ? session?.selectedParts : new ArrayList<>()
        def rows = []
        def partsAdjus = null
        if (session?.selectedPartsAdjust) {
            partsAdjus = session?.selectedPartsAdjust
        }

        results.each {
            def goods = Goods.read(it?.toString())
            PartsAdjDetail partsAdjDetail = partsAdjus == null ? null : PartsAdjDetail.findByGoodsAndPartsAdjust(goods, partsAdjus)
            rows << [
                    id: goods.id,
                    pilih: partsAdjDetail != null,
                    kode: goods?.m111ID,
                    nama: goods?.m111Nama,
                    awal: PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',session?.userCompanyDealer) == null ? 0 : PartsStok.findByGoodsAndStaDelAndCompanyDealer(goods,'0',session?.userCompanyDealer)?.t131Qty1,
                    satuan: goods?.satuan?.m118KodeSatuan,
                    real: partsAdjDetail == null ? 0 : partsAdjDetail?.t146JmlAkhir1,
                    alasan: partsAdjDetail == null ? 0 : partsAdjDetail?.alasanAdjusment?.id,
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords: results.size(), iTotalDisplayRecords: results.size(), aaData: rows]

        render ret as JSON
    }


    def doSave() {
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Goods.get(it?.toString()) }

        PartsAdjust partsAdjus = null
        if (session?.selectedPartsAdjust) {
//            partsAdjus = session?.selectedPartsAdjust
//            PartsAdjust.executeUpdate("delete PartsAdjDetail where goods not in :goods", [goods: oList])
        } else {
            partsAdjus = new PartsAdjust()
            partsAdjus.t145ID = generateCodeService.codeGenerateSequence("T145_ID",session.userCompanyDealer)
            partsAdjus.companyDealer = session?.userCompanyDealer
            partsAdjus.dateCreated = datatablesUtilService?.syncTime()
            partsAdjus.lastUpdated = datatablesUtilService?.syncTime()
            partsAdjus.save(flush: true)
        }

        oList.each {
            def cekAdjust = PartsAdjDetail.findByGoodsAndPartsAdjust(it,partsAdjus)

            PartsAdjDetail partsAdjDetail = PartsAdjDetail.findByGoodsAndPartsAdjust(it, partsAdjus) ?: new PartsAdjDetail()
            if(!cekAdjust){
                partsAdjDetail.dateCreated = datatablesUtilService?.syncTime()
            }
            partsAdjDetail.lastUpdated = datatablesUtilService?.syncTime()
            partsAdjDetail.alasanAdjusment = AlasanAdjusment.first()
            partsAdjDetail.goods = it
            partsAdjDetail.partsAdjust = partsAdjus
            partsAdjDetail.t146JmlAwal1 = request?.getParameter("awal_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.t146JmlAwal2 = request?.getParameter("awal_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.t146JmlAkhir1 = request?.getParameter("real_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.t146JmlAkhir2 = request?.getParameter("real_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.alasanAdjusment = AlasanAdjusment.read(request?.getParameter("alasan_" + it.id)?.toString())
            partsAdjDetail.save(flush: true)
        }


        session?.selectedPartsAdjust = partsAdjus

        render "ok"
    }


    def doSend() {
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Goods.get(it?.toString()) }

        PartsAdjust partsAdjus
        if (session?.selectedPartsAdjust) {
//            partsAdjus = session?.selectedPartsAdjust
//            PartsAdjust.executeUpdate("delete PartsAdjDetail where goods not in :goods", [goods: oList])
        } else {
            println "masuk Insert"
        }
            partsAdjus = new PartsAdjust()
            partsAdjus.t145ID = generateCodeService.codeGenerateSequence("T145_ID",session.userCompanyDealer)
            partsAdjus.companyDealer = session.userCompanyDealer
            partsAdjus.dateCreated = datatablesUtilService?.syncTime()
            partsAdjus.lastUpdated = datatablesUtilService?.syncTime()
            partsAdjus.save(flush: true)

        oList.each {
            def cekAdjust = PartsAdjDetail.findByGoodsAndPartsAdjust(it, partsAdjus)
            PartsAdjDetail partsAdjDetail = PartsAdjDetail.findByGoodsAndPartsAdjust(it, partsAdjus) ?: new PartsAdjDetail()
            if(!cekAdjust){
                partsAdjDetail.dateCreated = datatablesUtilService?.syncTime()
            }
            partsAdjDetail.lastUpdated = datatablesUtilService?.syncTime()
            partsAdjDetail.alasanAdjusment = AlasanAdjusment.first()
            partsAdjDetail.goods = it
            partsAdjDetail.partsAdjust = partsAdjus
            partsAdjDetail.t146JmlAwal1 = request?.getParameter("awal_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.t146JmlAwal2 = request?.getParameter("awal_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.t146JmlAkhir1 = request?.getParameter("real_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.t146JmlAkhir2 = request?.getParameter("real_" + it.id)?.toString()?.toDouble()
            partsAdjDetail.alasanAdjusment = AlasanAdjusment.read(request?.getParameter("alasan_" + it.id)?.toString())
            partsAdjDetail.save(flush: true)
        }


        session?.selectedPartsAdjust = partsAdjus
        def approval = new ApprovalT770(
                t770FK:partsAdjus.id as String,
                kegiatanApproval: KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.PARTS_ONHANDADJUSTMENT),
                t770NoDokumen: params.dokumen,
                t770TglJamSend: datatablesUtilService?.syncTime(),
                t770Pesan: params.Pesan,
                companyDealer: session?.userCompanyDealer,
                dateCreated: datatablesUtilService?.syncTime(),
                lastUpdated: datatablesUtilService?.syncTime()
        )
        approval.save(flush:true)
        approval.errors.each{ println it }

        session?.selectedPartsDisposal = null

        render "ok"
    }

    def getTableData(){
        def c = PartsAdjDetail.createCriteria()
        def results = c.list {
            eq("t146StaDel", "0")
            partsAdjust{
                eq("t145ID",params.id)
            }

        }

        def rows = []

        results.each {
            rows << [

                    id: it?.goods?.id,

                    kode: it?.goods?.m111ID,

                    nama: it?.goods?.m111Nama,

                    awal: it?.t146JmlAwal1,

                    real: it?.t146JmlAkhir1,

                    satuan: it?.goods?.satuan?.m118Satuan1,

                    alasan: it?.alasanAdjusment?.id

            ]
        }

        render rows as JSON

    }
    def generateNomorDokumen(){
        def result = [:]
        def c = ApprovalT770.createCriteria()
        def results = c.get () {
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            projections {
                max("id","id")
            }
        }

        def m = results.id?:0

        result.value = String.format('%014d',m+1)
        return result
    }
}
