package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class PartsTransferStokCekController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def partsTransferStokCekService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idTable: new Date().format("yyyyMMddhhmmss") ]
    }

    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [sCriteria_status:params.sCriteria_status, companyDealer: params.companyDealer, tglTransferStok1 : params.tglTransferStok1,tglTransferStok2: params.tglTransferStok2, nomorPO: params.nomorPO, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        session.exportParams = params

        params.userCompanyDealer = session.userCompanyDealer
        render partsTransferStokCekService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        params.companyDealer = session.userCompanyDealer
        render partsTransferStokCekService.datatablesSubList(params) as JSON
    }

    def massdelete() {
        def hasil = partsTransferStokCekService.massDelete(params)
        render "ok"

    }

    def printPartsTransferStok(){
        def jsonArray = JSON.parse(params.idPartsTransferStok)
        def returnsList = []

        List<JasperReportDef> reportDefList = []

        jsonArray.each {
            returnsList << it
        }

        returnsList.each {
            println it
            def reportData = calculateReportData(it)

            def reportDef = new JasperReportDef(name:'partsTransferStok.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: reportData

            )
            reportDefList.add(reportDef)
        }

        def file = File.createTempFile("partsTransferStok_",".pdf")
        file.deleteOnExit()

        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())

        response.setHeader("Content-Type", "application/pdf")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()


    }
    def calculateReportData(def id){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        def reportData = new ArrayList();
        def c = PartsTransferStok.createCriteria()
        def results = c.list {
            eq("nomorPO",id as String)
            eq("staDel","0")
            eq("cekBaca","1")
            eq("cekKetersediaan","1")
        }

        results.sort {
            it.goods.m111ID
        }

        int count = 0
        def total = 0

        results.each {
            def harga = GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session.userCompanyDealer) ? GoodsHargaBeli.findByGoodsAndStaDelAndCompanyDealer(it.goods,"0",session.userCompanyDealer)?.t150Harga : 0
            def data = [:]
            count = count + 1
            data.put("adrs",it.companyDealerTujuan?.m011Alamat)
            data.put("noWo",'Nomor NPB')
            data.put("cust",it.companyDealerTujuan?.m011NamaWorkshop)
            data.put("noSlip",it.noPengiriman)
            data.put("tanggal",it.tglTransferStok.format("dd/MM/YYYY"))
            data.put("jam",it.tglTransferStok.format("HH:mm"))
            //
            data.put("noUrut",count)
            data.put("kodeParts",it.goods.m111ID)
            data.put("namaParts",it.goods.m111Nama)
            data.put("qty",new Konversi().toRupiah(it.qtyParts))
            data.put("dsc","0")
            data.put("unitPrice",new Konversi().toRupiah(harga))
            data.put("extdAmmount",new Konversi().toRupiah(harga * it.qtyParts))

            total += (harga * it.qtyParts)
            data.put("total",new Konversi().toRupiah(total))

            reportData.add(data)

        }

        return reportData

    }
}
