package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class GroupDiskonController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = GroupDiskon.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {


            if (params."sCriteria_group") {
                eq("group", Group.findByM181NamaGroupIlike("%"+params."sCriteria_group"+"%"))
            }

            if (params."sCriteria_m172TglAwal") {
                ge("m172TglAwal", params."sCriteria_m172TglAwal")
                lt("m172TglAwal", params."sCriteria_m172TglAwal" + 1)
            }

            if (params."sCriteria_m172TglAkhir") {
                ge("m172TglAkhir", params."sCriteria_m172TglAkhir")
                lt("m172TglAkhir", params."sCriteria_m172TglAkhir" + 1)
            }

            if (params."sCriteria_m172PersenDiskon") {
                eq("m172PersenDiskon", Double.parseDouble(params."sCriteria_m172PersenDiskon"))
            }


                ilike("staDel", "0")


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    companyDealer: it.companyDealer,

                    group: it.group.m181NamaGroup,

                    m172TglAwal: it.m172TglAwal ? it.m172TglAwal.format(dateFormat) : "",

                    m172TglAkhir: it.m172TglAkhir ? it.m172TglAkhir.format(dateFormat) : "",

                    m172PersenDiskon: it.m172PersenDiskon,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [groupDiskonInstance: new GroupDiskon(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def groupDiskonInstance = new GroupDiskon(params)

        groupDiskonInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        groupDiskonInstance?.lastUpdProcess = "INSERT"
        groupDiskonInstance?.companyDealer = CompanyDealer.findById(session.userCompanyDealerId)
        groupDiskonInstance?.setStaDel('0')
        def tgl = GroupDiskon.findByM172TglAwalAndStaDel(groupDiskonInstance?.m172TglAwal,'0')
        def cek = GroupDiskon.createCriteria()
        def result = cek.list() {
            eq("group", Group.findById(params.group.id))
            ilike("staDel", "0")
        }
        def tanggalBeririsan = 0
        result.each {
            if(groupDiskonInstance?.m172TglAwal >= it?.m172TglAwal && groupDiskonInstance?.m172TglAwal <= it?.m172TglAkhir){
                tanggalBeririsan = 1
            }
            if(groupDiskonInstance?.m172TglAkhir >= it?.m172TglAwal && groupDiskonInstance?.m172TglAkhir <= it?.m172TglAkhir){
                tanggalBeririsan = 1
            }
            if(groupDiskonInstance?.m172TglAwal <= it?.m172TglAwal && groupDiskonInstance?.m172TglAkhir >= it?.m172TglAkhir){
                tanggalBeririsan = 1
            }
        }
        if(tanggalBeririsan == 1){
            flash.message = "* Tanggal Awal Beririsan dengan data tanggal group yang sama sebelumnya"
            render(view: "create", model: [groupDiskonInstance: groupDiskonInstance])
            return
        }
        if(groupDiskonInstance?.m172TglAwal > groupDiskonInstance?.m172TglAkhir){
            flash.message = "* Tanggal Akhir Harus Lebih Besar"
            render(view: "create", model: [groupDiskonInstance: groupDiskonInstance])
            return
        }

        if(tgl){
            flash.message = "* Data sudah terdaftar"
            render(view: "create", model: [groupDiskonInstance: groupDiskonInstance])
            return
        }
        if (!groupDiskonInstance.save(flush: true)) {
            render(view: "create", model: [groupDiskonInstance: groupDiskonInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), groupDiskonInstance.id])
        redirect(action: "show", id: groupDiskonInstance.id)
    }

    def show(Long id) {
        def groupDiskonInstance = GroupDiskon.get(id)
        if (!groupDiskonInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), id])
            redirect(action: "list")
            return
        }

        [groupDiskonInstance: groupDiskonInstance]
    }

    def edit(Long id) {
        def groupDiskonInstance = GroupDiskon.get(id)
        if (!groupDiskonInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), id])
            redirect(action: "list")
            return
        }

        [groupDiskonInstance: groupDiskonInstance]
    }

    def update(Long id, Long version) {
        def groupDiskonInstance = GroupDiskon.get(id)
        if (!groupDiskonInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (groupDiskonInstance.version > version) {

                groupDiskonInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'groupDiskon.label', default: 'GroupDiskon')] as Object[],
                        "Another user has updated this GroupDiskon while you were editing")
                render(view: "edit", model: [groupDiskonInstance: groupDiskonInstance])
                return
            }
        }
        def cek = GroupDiskon.findAllByGroupAndStaDel(Group.findById(params.group.id),'0')
        if(cek){
            for(find in cek) {
                if(find.id<id){
                    if(find?.m172TglAkhir.after(params.m172TglAwal) || find?.m172TglAkhir.compareTo(params.m172TglAwal)==0){
                        flash.message = "* Tanggal Awal Beririsan dengan data tanggal group yang sama sebelumnya"
                        render(view: "edit", model: [groupDiskonInstance: groupDiskonInstance])
                        return
                        break
                    }
                }
                if(find.id>id){
                    if(find?.m172TglAwal.before(params.m172TglAwal) || find?.m172TglAwal.compareTo(params.m172TglAwal)==0){
                        flash.message = "* Tanggal Awal Beririsan dengan data tanggal group yang sama sebelumnya"
                        render(view: "edit", model: [groupDiskonInstance: groupDiskonInstance])
                        return
                        break
                    }
                }
            }
        }

        if(params.m172TglAwal > params.m172TglAkhir){
            flash.message = "* Tanggal Akhir Harus Lebih Besar"
            render(view: "edit", model: [groupDiskonInstance: groupDiskonInstance])
            return
        }

        def tgl = GroupDiskon.findByM172TglAwalAndStaDel(params.m172TglAwal,'0')
        if(tgl && tgl?.id != id){
            flash.message = "* Data sudah terdaftar"
            render(view: "edit", model: [groupDiskonInstance: groupDiskonInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        groupDiskonInstance.properties = params
        groupDiskonInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        groupDiskonInstance?.lastUpdProcess = "UPDATE"

        if (!groupDiskonInstance.save(flush: true)) {
            render(view: "edit", model: [groupDiskonInstance: groupDiskonInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), groupDiskonInstance.id])
        redirect(action: "show", id: groupDiskonInstance.id)
    }

    def delete(Long id) {
        def groupDiskonInstance = GroupDiskon.get(id)
        if (!groupDiskonInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), id])
            redirect(action: "list")
            return
        }

        try {
            //groupDiskonInstance.delete(flush: true)
            groupDiskonInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            groupDiskonInstance?.lastUpdProcess = "DELETE"
            groupDiskonInstance?.setStaDel('1')
            groupDiskonInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'groupDiskon.label', default: 'GroupDiskon'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(GroupDiskon, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(GroupDiskon, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
