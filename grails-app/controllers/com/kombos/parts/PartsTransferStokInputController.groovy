package com.kombos.parts

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.MappingCompanyRegion
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.ApprovalT770
import grails.converters.JSON
import org.springframework.web.context.request.RequestContextHolder

import java.text.DateFormat
import java.text.SimpleDateFormat

class PartsTransferStokInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService
    def generateCodeService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }
    def generateNomorPengiriman(){
        def result = [:]
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def currentDate = new Date().format("dd-MM-yyyy")
        def c = PartsTransferStok.createCriteria()
        def results = c.list {
            ge("dateCreated",df.parse(currentDate))
            lt("dateCreated",df.parse(currentDate) + 1)
            eq("companyDealer",session.userCompanyDealer)
        }

        def m = results.size()?:0
        def reg = MappingCompanyRegion.findByCompanyDealer(session.userCompanyDealer).companyDealer?.m011ID
        def data = new Date().format("yyyyMMdd").toString() + "-" + reg +"-" + String.format('PTS%05d',m+1)
//        result.value = String.format('%014d',m+1)
        result.value = data
        return result
    }
    def partsAdd(){
        [idTable: new Date().format("yyyyMMddhhmmss")]
    }
    def dataTablesPartsAddList(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def exist = []
        if(params."sCriteria_exist"){
            JSON.parse(params."sCriteria_exist").each{
                exist << it
            }
        }
        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0");
            if (params."sCriteria_goods") {
                        ilike("m111ID","%" + params."sCriteria_goods" + "%")
            }
            if (params."sCriteria_goods2") {
                         ilike("m111Nama","%" + params."sCriteria_goods2" + "%")
             }

            if(exist.size()>0){
                not {
                    or {
                        exist.each {
                            eq('id', it as long)
                        }
                    }
                }
            }
            switch (sortProperty) {
                case "goods":
                             order("m111ID",sortDir)
                    break;
                case "goods2":
                             order("m111Nama",sortDir)
                     break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        def nos = 0
        results.each {
            def stokR =0
            try {
                stokR = PartsStok.findByGoodsAndCompanyDealerAndStaDel(it,session.userCompanyDealer,'0')?.t131Qty1
            }catch (Exception e){}

            nos = nos + 1
                 rows << [

                        id: it.id,

                        norut:nos,

                        goods: it.m111ID,

                        goods2: it.m111Nama,

                        stok: stokR ,

                        hargaBeli: GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it,session.userCompanyDealer,"0")?.t150Harga,

                ]

        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def list(Integer max) {
        String staUbah = "create"
        def cari = new PartsTransferStok()
        def u_tanggal = ""
        def u_companyDealerTujuan = ""
        if(params.noPartsTransferStok!=null){
            staUbah="ubah"
            cari = PartsTransferStok.findByNomorPO(params.noPartsTransferStok)
            u_tanggal = cari?.tglTransferStok
            u_companyDealerTujuan = cari?.companyDealerTujuan.m011NamaWorkshop?cari?.companyDealerTujuan.m011NamaWorkshop:""
        }
        [noPartsTransferStok: params.noPartsTransferStok, u_tanggal:u_tanggal, tanggal : new Date().format("yyyy-MM-dd"), aksi:staUbah, u_companyDealerTujuan  : u_companyDealerTujuan ]
    }
    def ubah(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
        def hasil = null
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each {
            def goods = Goods.get(it)
            if(goods){
                def cQty = (params."qty-${it}" as String).replace(',','.')
                PartsTransferStok noret = PartsTransferStok.findByNomorPOAndGoods(params.noPartsTransferStok,goods)
                def partsTransferStok2 = PartsTransferStok.get(noret.id as Long)
                partsTransferStok2.goods = goods
                partsTransferStok2.tglTransferStok = params.tanggal
                partsTransferStok2?.qtyParts = cQty as Double
                partsTransferStok2?.cekBaca = '0'
                partsTransferStok2?.lastUpdProcess = "UPDATE"
                partsTransferStok2?.lastUpdated =  datatablesUtilService?.syncTime()
                partsTransferStok2.save(flush: true)

            }
        }
        hasil = params.noPartsTransferStok
        render hasil
    }
    def generateNomorPO(){
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        def result = [:]
        DateFormat df = new SimpleDateFormat("yyyy")
        def currentDate = new Date().format("yyyy")
        def c = PO.createCriteria()
        def results = c.list {

        }

        def m = results.size()?:0
        def reg = MappingCompanyRegion.findByCompanyDealer(session.userCompanyDealer).companyDealer?.m011ID
        def data = reg +"." + new Date().format("yy").toString() + "." +  String.format('%05d',m+1)
//        result.value = String.format('%014d',m+1)
        result = data
        return result
    }
    def req(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")
        String tanggal =  params.tanggal
        def hasil = null
        def noPo = null
        def companyDealer =  CompanyDealer.findByM011NamaWorkshop(params.namaCompanyDealer)
        if(companyDealer){
            def jsonArray = JSON.parse(params.ids)
            if(params.noPartsTransferStokBefore==null || params.noPartsTransferStokBefore==""){
                noPo = generateNomorPO()
                def po = new PO(
                        t164TglPO:  datatablesUtilService?.syncTime(),
                        t164NoPO: noPo,
                        t164StaOpenCancelClose: POStatus.OPEN,
                        t164xNamaUser: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        t164xNamaDivisi: "Divisi",
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess: "INSERT",
                        dateCreated:  datatablesUtilService?.syncTime(),
                        lastUpdated:  datatablesUtilService?.syncTime()
                )
                po.save(flush:true)
                po.errors.each{
                    println it
                }
                def poDetail = new PODetail(
                        po: po,
                        dateCreated:  datatablesUtilService?.syncTime(),
                        lastUpdated:  datatablesUtilService?.syncTime(),
                        createdBy:  org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        lastUpdProcess:  org.apache.shiro.SecurityUtils.subject.principal.toString()
                )
                poDetail.save()
                poDetail.errors.each{
                    println it
                }
            }

            def idParts = ""
            int angka = 0
            jsonArray.each {
                angka++
                def goods = Goods.get(it)
                if(goods){
                    def cQty = (params."qty-${it}" as String).replace(',','.')
                    PartsTransferStok partsTransferStok = new PartsTransferStok()
                    if(params.noPartsTransferStokBefore==null || params.noPartsTransferStokBefore==""){
                        partsTransferStok?.nomorPO = noPo
                    }else{
                        partsTransferStok?.nomorPO = params.noPartsTransferStokBefore
                    }
                    partsTransferStok?.tglTransferStok = new Date()
                    partsTransferStok?.companyDealerTujuan = companyDealer
                    partsTransferStok?.companyDealer = session.userCompanyDealer
                    partsTransferStok?.goods = goods
                    partsTransferStok?.qtyParts = cQty as Double
                    partsTransferStok?.staDel = '0'
                    partsTransferStok?.staApprove = '2'
                    partsTransferStok?.staSudahInput = '0'
                    partsTransferStok?.cekBaca = '0'
                    partsTransferStok?.noPengiriman = generateNomorPengiriman().value
                    partsTransferStok?.dateCreated = datatablesUtilService?.syncTime()
                    partsTransferStok?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    partsTransferStok?.lastUpdated = datatablesUtilService?.syncTime()
                    partsTransferStok?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    partsTransferStok?.lastUpdProcess = "INSERT"

                    partsTransferStok.save(flush: true)
                }
            }
            hasil = noPo
            def approval = new ApprovalT770(
                    t770FK:noPo+"#"+idParts,
                    kegiatanApproval: KegiatanApproval.findByM770KegiatanApprovalAndStaDel(KegiatanApproval.PARTS_TRANSFER_STOCK,"0"),
                    t770NoDokumen: noPo,
                    t770TglJamSend: datatablesUtilService?.syncTime(),
                    t770Pesan: params.keterangan,
                    companyDealer: session?.userCompanyDealer,
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
            )
            approval.save(flush: true)
        }else {
            hasil = "fail"
        }
        render hasil
    }

    def getTableData(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
        def hasil = PartsTransferStok.createCriteria().list() {
            eq("nomorPO",params.id)
        }
        def result = []
        hasil.each {
            result << [

                    id: it.id,

                    kode: it.goods.m111ID,

                    nama: it.goods.m111Nama,

                    nomorPO : it.nomorPO,

                    tglTransferStok : it.tglTransferStok? it.tglTransferStok.format(dateFormat) : "",

                    Qty : it.qtyParts,

                    hargaBeli: GoodsHargaBeli.findByGoodsAndCompanyDealerAndStaDel(it.goods,session?.userCompanyDealer,"0") ,

            ]

        }
        render result as JSON
    }

    def kodeList() {
        def res = [:]

        def result = CompanyDealer.findAllWhere(staDel : '0')
        def opts = []
        result.each {
            opts << it.m011NamaWorkshop
        }

        res."options" = opts
        render res as JSON
    }
}
