package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.FA
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class InputBinningController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)
    def binningService
    def generateCodeService
    def datatablesUtilService

    def index() {
        def binning = Binning.read(params.id);
        def tglSkrg = datatablesUtilService.syncTime()
        def jamSkrg = datatablesUtilService.syncTime().getHours().toString()
        def mntSkrg = datatablesUtilService.syncTime().getMinutes().toString()
        String rcvHour = "00"
        String rcvMinuite = "00"
        List<String> hourList = new ArrayList<>()
        for (int i = 0; i < 24; i++) {
            if (i < 10) hourList.add("0" + i)
            else hourList.add("" + i)
        }
        List<String> minuiteList = new ArrayList<>()
        for (int i = 0; i < 60; i++) {
            if (i < 10) minuiteList.add("0" + i)
            else minuiteList.add("" + i)
        }

        if(params.id && binning){
            rcvHour = binning.t168TglJamBinning.getHours()
            rcvMinuite = binning.t168TglJamBinning.getMinutes()
        } else {
            session?.selectedBinning = null;
            binning = new Binning();
            //binning?.t168ID = generateCodeService.codeGenerateSequence("T168_ID",session.userCompanyDealer)
        }

        [binningInstance: binning, rcvHour: rcvHour,tglSkrg:tglSkrg,
                rcvMinuite: rcvMinuite, hourList: hourList, minuiteList: minuiteList,mntSkrg:mntSkrg,jamSkrg:jamSkrg]
    }

    def searchAndAddParts() {

    }

    def searchPartsDatatablesList() {
        render binningService.datatablesListInputAddParts(params) as JSON

    }

    def save() {
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each {
            if (params.binning_id) {
                oList << BinningDetail.get(it?.toString())
            } else {
                oList << GoodsReceiveDetail.get(it?.toString())
            }
        }

        DateFormat dfSimple = new SimpleDateFormat("dd-MM-yyyy")
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm")
        Date input_rcvDate = df.parse(params.input_rcvDate + ' ' + params.input_rcvHour + ':' + params.input_rcvMinuite)
        //qInvoice=&qInvoice_2=0&qInvoice_3=0&qInvoice_1=0
        //qReceive=&qReceive_2=0&qReceive_3=0&qReceive_1=0
        //qRusak=&qRusak_2=0&qRusak_3=0&qRusak_1=0
        //qSalah=&qSalah_2=0&qSalah_3=0&qSalah_1=0
        String qInvoice = params.qInvoice
        String qReceive = params.qReceive
        String qRusak = params.qRusak
        String qSalah = params.qSalah
        String[] qInvoices = qInvoice.split("&")
        String[] qReceives = qReceive.split("&")
        String[] qRusaks = qRusak.split("&")
        String[] qSalahs = qSalah.split("&")
        Binning binning
        if (params.binning_id) {
            binning = Binning.findById(params.binning_id)
            binning?.lastUpdProcess = "UPDATE"
        } else {
            binning = new Binning()
            binning?.t168ID = generateCodeService.codeGenerateSequence("T168_ID",session.userCompanyDealer)
            binning?.t168PetugasBinning = org.apache.shiro.SecurityUtils.subject.principal.toString()
            binning?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            binning?.lastUpdProcess = "INSERT"
            binning?.dateCreated = datatablesUtilService?.syncTime()
            binning?.companyDealer = session?.userCompanyDealer
        }
        binning?.lastUpdated = datatablesUtilService?.syncTime()
        binning?.t168TglJamBinning = input_rcvDate
        binning?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        binning?.staDel= '0'
        binning.save(flush: true)

        int i = 1;  //dimulai dari i = 1 , karena array ke-0=""
        oList.each {
            String[] parsed = qInvoices[i].split("=")
            String[] parsed2 = qReceives[i].split("=")
            String[] parsed3 = qRusaks[i].split("=")
            String[] parsed4 = qSalahs[i].split("=")

            def quantity1 = 0
            def quantity2 = 0
            def quantity3 = 0
            def  quantity4 = 0
            if (parsed[1] != null && !parsed[1].equals(""))
                quantity1 = Double.valueOf(parsed[1] as String)
            if (parsed2[1] != null && !parsed2[1].equals(""))
                quantity2 = Double.valueOf(parsed2[1] as String)
            if (parsed3[1] != null && !parsed3[1].equals(""))
                quantity3 = Double.valueOf(parsed3[1] as String)
            if (parsed4[1] != null && !parsed4[1].equals(""))
                quantity4 = Double.valueOf(parsed4[1] as String)

            BinningDetail binningDetail = new BinningDetail()
            GoodsReceiveDetail grd = null
            if (params.binning_id) {
                binningDetail = BinningDetail.findById("" + it.id)
                grd = binningDetail.goodsReceiveDetail
            } else {
                binningDetail.binning = binning
                binningDetail?.dateCreated = datatablesUtilService?.syncTime()
                grd = GoodsReceiveDetail.findById(Long.valueOf("" + it.id))
                binningDetail.goodsReceiveDetail = grd
                binningDetail?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            }

            binningDetail.t168Qty1Binning = quantity1
            binningDetail.t168Qty2Binning = quantity2
            binningDetail.t168xNamaUser = org.apache.shiro.SecurityUtils.subject.principal.toString()
            binningDetail.t168xNamaDivisi = org.apache.shiro.SecurityUtils.subject.principal.toString()

            binningDetail?.lastUpdated = datatablesUtilService?.syncTime()
            binningDetail?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            binningDetail?.lastUpdProcess = "INSERT"
            binningDetail?.staDel= '0'
            binningDetail.save(flush: true)

            //update qtyInvoice, qtyReceive, qtyRusak, qtySalah
            grd.t167Qty1Reff = quantity1
            grd.t167Qty2Reff = quantity1
            grd.t167Qty1Issued = quantity2
            grd.t167Qty2Issued = quantity2
            grd.t167Qty1Rusak = quantity3
            grd.t167Qty2Rusak = quantity3
            grd.t167Qty1Salah = quantity4
            grd.t167Qty2Salah = quantity4
            grd.staBinning = '1'
            grd.lastUpdated = datatablesUtilService.syncTime()
            grd.save(flush: true)
            i += 1
        }
        redirect(controller: 'binning', action:'list')
        //render "ok"
    }

    def tambahParts() {
        def fa = FA.read(params.fa)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << it }
        session.selectedParts = oList
        render "ok"
    }

    def addModal(Integer max) {
    }

    def inputBinningDatatablesList() {
        render binningService.inputBinningDatatablesList(params) as JSON
    }
    def addModalDatatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue
     		def exist = []
     		if(params."sCriteria_exist"){
     			JSON.parse(params."sCriteria_exist").each{
     				exist << it
     			}
     		}
             def ret
             def propertiesToRender = params.sColumns.split(",")
             def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
             def sortProperty = propertiesToRender[params.iSortCol_0 as int]
             def x = 0

             session.exportParams = params


            def c = GoodsReceiveDetail.createCriteria()
            def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
                goodsReceive{
                    eq("companyDealer",session?.userCompanyDealer)
                }
                isNull("staBinning")
                poDetail{
                    po{
                        eq("t164StaOpenCancelClose",POStatus.OPEN)
                    }
                }
                if(exist.size()>0){
                    not {
                        or {
                            exist.each {
                                eq('id', it as long)
                            }
                        }
                    }
                }
                if(params.sCriteria_kodeParts){
                    goods{
                        ilike("m111ID","%"+params.sCriteria_kodeParts+"%")
                    }
                }
                if(params.sCriteria_namaParts){
                    goods{
                        ilike("m111Nama","%"+params.sCriteria_namaParts+"%")
                    }
                }
                if(params.sCriteria_nomorPO){
                    poDetail{
                        po{
                            ilike("t164NoPO","%"+params.sCriteria_nomorPO+"%")
                        }
                    }
                }
                if(params.sCriteria_tglPO){
                    poDetail{
                        po{
                            ge("t164TglPO", params.sCriteria_tglPO)
                            lt("t164TglPO", params.sCriteria_tglPO + 1)
                        }
                    }
                }

                if(params.sCriteria_nomorInvoice){
                    invoice{
                        ilike("t166NoInv","%"+params.sCriteria_nomorInvoice+"%")
                    }
                }
                if(params.sCriteria_tglInvoice){
                    invoice{
                        ge("t166TglInv", params.sCriteria_tglInvoice)
                        lt("t166TglInv", params.sCriteria_tglInvoice + 1)
                    }
                }
            }


             def rows = []
             def nos = 0
        results.each {
                 nos = nos + 1
                 rows << [

                         id: it.id,

                         norut:nos,

                         kodePart: it.goods?.m111ID,

                         namaPart: it.goods?.m111Nama,

                         tglJamReceive: it.goodsReceive?.t167TglJamReceive.format(dateFormat),

                         nomorPO: it.poDetail?.po?.t164NoPO? it.poDetail?.po?.t164NoPO : it.updatedBy,

                         tglPO: it.poDetail?.po?.t164TglPO? it.poDetail?.po?.t164TglPO.format(dateFormat):(PO.findByT164NoPOLike("%"+it.updatedBy+"%")?.t164TglPO ? PO.findByT164NoPOLike("%"+it.updatedBy+"%")?.t164TglPO.format("dd/MM/yyyy"):""),

                         nomorInvoice: it.invoice?.t166NoInv,

                         tglInvoice: it.invoice?.t166TglInv?it.invoice?.t166TglInv.format(dateFormat):'-',

                         qtyInvoice : it.invoice?.t166Qty1,

                         qtyRece : it.t167Qty1Issued

                 ]
             }

             ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

             render ret as JSON
    }

}
