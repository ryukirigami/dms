package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class PartsReconsileController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = [
            'index',
            'list',
            'datatablesList'
    ]

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def partsReconsileService
    def vendor = null
    def noref = null
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {

        [idTable: new Date().format("yyyyMMddhhmmss") ]
    }

    def sublist() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
        [noPo: params.noPo,  idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def subsublist() {
        [noPo: params.noPo, noInvoice: params.noInvoice, idTable: new Date().format("yyyyMMddhhmmss")]
    }

    def datatablesList() {
        render partsReconsileService.datatablesList(params) as JSON
    }

    def datatablesSubList() {
        render partsReconsileService.datatablesSubList(params) as JSON
    }
    def datatablesSubSubList() {
        render partsReconsileService.datatablesSubSubList(params) as JSON
    }

}
