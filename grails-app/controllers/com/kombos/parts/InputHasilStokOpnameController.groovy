package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class InputHasilStokOpnameController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def inputHasilStokOpnameService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST",editQty: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.companyDealer = session?.userCompanyDealer
        def result=inputHasilStokOpnameService.list(params)
        [ part132ID : result.t132ID, part132Tanggal : result.t132Tanggal]
    }

    def datatablesList() {
        session.exportParams=params
        params.companyDealer = session?.userCompanyDealer
        render inputHasilStokOpnameService.datatablesList(params) as JSON
    }

    def editQty(){
        def result = inputHasilStokOpnameService.editQty(params);
        render result
    }

    def create() {
        def result = inputHasilStokOpnameService.create(params)

        if(!result.error)
            return [inputHasilStokOpnameInstance: result.inputHasilStokOpnameInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = inputHasilStokOpnameService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["StokOPName", result.inputHasilStokOpnameInstance.id])
            redirect(action:'show', id: result.inputHasilStokOpnameInstance.id)
            return
        }

        render(view:'create', model:[inputHasilStokOpnameInstance: result.inputHasilStokOpnameInstance])
    }

    def show(Long id) {
        def result = inputHasilStokOpnameService.show(params)

        if(!result.error)
            return [ inputHasilStokOpnameInstance: result.inputHasilStokOpnameInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = inputHasilStokOpnameService.show(params)

        if(!result.error)
            return [ inputHasilStokOpnameInstance: result.inputHasilStokOpnameInstance ]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = inputHasilStokOpnameService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["StokOPName", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[inputHasilStokOpnameInstance: result.inputHasilStokOpnameInstance.attach()])
    }

    def delete() {
        def result = inputHasilStokOpnameService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["StokOPName", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(StokOPName, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }


}
