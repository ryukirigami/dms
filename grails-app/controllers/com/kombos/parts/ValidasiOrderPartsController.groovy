package com.kombos.parts

import com.kombos.administrasi.KegiatanApproval
import com.kombos.administrasi.NamaApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class ValidasiOrderPartsController {
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	static viewPermissions = [
		'index',
		'list',
		'datatablesList'
	]

	static addPermissions = []

	static editPermissions = []

	static deletePermissions = []

	def validasiOrderPartsService

	def index() {
		redirect(action: "list", params: params)
	}
    def findData(){
        def results  = [:]
        def gooods = Goods.findById(params.id as Long)
        def klas = KlasifikasiGoods.findByGoods(gooods)
        results.kodePart = gooods.m111ID
        results.idGoods = gooods.id
        results.namaPart = gooods.m111Nama
        if(klas){
            results.group = klas.group.id
            results.franc = klas.franc.id
            results.scc = klas.scc.id
            results.location = klas.location.id
        }else{
            results.group = ""
            results.franc = ""
            results.scc = ""
            results.location = ""
        }
        render results as JSON
    }
    def saveKlasifikasiGoods(){
        def g = Goods.findById(params.idGoods as Long)
        def klas = KlasifikasiGoods.findByGoods(g)
        if(klas){
            klas = KlasifikasiGoods.get(klas?.id)
            klas?.companyDealer = session.userCompanyDealer
            klas?.goods = Goods.findById(params.idGoods as Long)
            klas?.group = Group.findById(params.group as Long)
            klas?.franc = Franc.findById(params.franc as Long)
            klas?.scc = SCC.findById(params.scc as Long)
            klas?.location = Location.findById(params.location as Long)
            klas?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            klas?.lastUpdProcess = 'INSERT'
            klas?.lastUpdated = datatablesUtilService?.syncTime()
            klas?.save(flush: true)
            klas.errors.each{
                println  "klasEdit == > " + it
            }
        }else{
            klas = new KlasifikasiGoods()
            klas?.companyDealer = session.userCompanyDealer
            klas?.goods = Goods.findById(params.idGoods as Long)
            klas?.group = Group.findById(params.group as Long)
            klas?.franc = Franc.findById(params.franc as Long)
            klas?.scc = SCC.findById(params.scc as Long)
            klas?.location = Location.findById(params.location as Long)
            klas?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            klas?.lastUpdProcess = 'INSERT'
            klas?.dateCreated = datatablesUtilService?.syncTime()
            klas?.lastUpdated = datatablesUtilService?.syncTime()
            klas?.save(flush: true)
            klas.errors.each{
                println  "klasSave == > " + it
            }
        }
        render "ok"
    }
	def list(Integer max) {
        KegiatanApproval kegiatanApproval = KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.APPROVAL_ORDER_PARTS)
        def approver = ""
        for(NamaApproval namaApproval : kegiatanApproval.namaApprovals){
            if(namaApproval.userProfile.companyDealer.id == session.userCompanyDealerId){
                approver += namaApproval.userProfile.fullname + "\r\n"
            }
        }
		[idTable: new Date().format("yyyyMMddhhmmss"), kegiatanApproval: kegiatanApproval, approver: approver]
	}
	
	
	def sublist() {
		String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue
		Date tglId = Date.parse(dateFormat,params.tanggalRequest)
		
		[tanggalRequest: params.tanggalRequest, idTanggal : tglId.format("yyyyMMdd"), idTable: new Date().format("yyyyMMddhhmmss")]
	}
	def subsublist() {
		[noReferensi: params.noReferensi, idTanggal: params.idTanggal, parentId: params.id, idTable: new Date().format("yyyyMMddhhmmss")]
	}

	def datatablesList() {
        params.companyDealer = session?.userCompanyDealer
		render validasiOrderPartsService.datatablesList(params) as JSON
	}
	
	def datatablesSubList() {
        params.companyDealer = session?.userCompanyDealer
        render validasiOrderPartsService.datatablesSubList(params) as JSON
	}
	def datatablesSubSubList() {
        params.companyDealer = session.userCompanyDealer
		render validasiOrderPartsService.datatablesSubSubList(params) as JSON
	}
	def saveHargaBeli() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def result = validasiOrderPartsService.saveHargaBeli(params)

        if(!result.error) {
            render result as JSON
            return
        }

        render result as JSON
	}
	
	def generateNomorDokumenApprovalOrderParts() {
		def result = validasiOrderPartsService.generateNomorDokumenApprovalOrderParts()
		render result as JSON
	}
	
	def requestApprovalOrderPartsApproval() {
        params.userCompanyDealer = session.userCompanyDealer
		def result = validasiOrderPartsService.requestApprovalOrderPartsApproval(params)

		if(!result.error) {
			render result as JSON
			return
		}

		render result as JSON
	}
	
	def requestDirectPO() {
        params.userCompanyDealer = session.userCompanyDealer
		def result = validasiOrderPartsService.requestDirectPO(params)

		if(!result.error) {
			render result as JSON
			return
		}

		render result as JSON
	}

    def deleteParts(){
        def dataReqDetails = JSON.parse(params.dataGoods)
        dataReqDetails.each {
            def reqD  = RequestDetail.get(it as Long)
            ValidasiOrder.findByRequestDetail(reqD).delete()
            reqD.delete()
        }
        render "ok"
    }
}
