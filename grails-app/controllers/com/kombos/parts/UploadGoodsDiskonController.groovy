package com.kombos.parts

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadGoodsDiskonController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def list(Integer max) {
    }
    def index() { }
    def view() {
        DateFormat df = new SimpleDateFormat("yyyy-M-dd")
        def goodsDiskonInstance = new GoodsDiskon(params)

        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'tglMulai',
                        'B':'tglSelesai',
                        'C':'namaGroup',
                        'D':'namaGoods',
                        'E':'diskon',

                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        def jsonData = ""
        int jmlhDataError = 0;
        String htmlData = "",cekData = ""
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream','text/plain'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [goodsDiskonInstance: goodsDiskonInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = jobList as JSON

            String status = "0", style = ""


            jobList?.each {
                def cekTgl = ""
                String mul = "",sel = ""
                def persen = null
                Date tglMulai= null, tglSelesai = null
                def groupIt = it
                try {
                    String data = it.diskon
                    persen = Double.parseDouble(data.toString())
                    if(persen>100){
                        cekTgl = "gagal"
                    }
                }catch (NumberFormatException ee){
                    cekTgl = "gagal"
                }
                try {
                    mul = it.tglMulai
                    tglMulai = new Date().parse('yyyy-MM-d', mul.toString())

                }catch (Exception e){
                    cekTgl = "gagal"
                }
                try {
                    sel = it.tglSelesai
                    tglSelesai = new Date().parse('yyyy-MM-d', sel.toString())
                }catch (Exception e){
                    cekTgl = "gagal"
                }
                if((it.tglMulai && it.tglMulai!="") && (it.tglSelesai && it.tglSelesai !="") && (it.namaGroup && it.namaGroup!= "") && (it.namaGoods && it.namaGoods!= "") && (it.diskon && it.diskon!= "")){
                    def cek = Group.createCriteria()
                    def groups = cek.list() {
                        and{
                            eq("m181NamaGroup",groupIt.namaGroup, [ignoreCase: true])
                            eq("staDel",'0')
                        }
                    }
                    def goods = Goods.findByM111IDIlikeAndStaDel(it.namaGoods,'0')

                    def tanggalBeririsan = 0
                    def cek2 = GoodsDiskon.createCriteria()
                    def result2 = cek2.list() {
                        group{
                            eq("m181NamaGroup",groupIt.namaGroup, [ignoreCase: true])
                        }
                        eq("goods", Goods.findByM111ID(groupIt.namaGoods))
                        ilike("staDel", "0")
                    }
                    result2.each {
                        if(tglMulai >= it?.m171TglAwal && tglMulai <= it?.m171TglAkhir){
                            tanggalBeririsan = 1
                        }
                        if(tglSelesai >= it?.m171TglAwal && tglSelesai <= it?.m171TglAkhir){
                            tanggalBeririsan = 1
                        }
                        if(tglMulai <= it?.m171TglAwal && tglSelesai >= it?.m171TglAkhir){
                            tanggalBeririsan = 1
                        }
                    }
                    if(tanggalBeririsan == 1){
                        jmlhDataError++
                        status = "2"
                    }
                    if(!groups || !goods){
                        jmlhDataError++;
                        status = "1";
                    }
                    if(tglMulai > tglSelesai){
                        jmlhDataError++
                        status = "1"
                    }
                    if(cekTgl == "gagal"){
                        jmlhDataError++
                        status = "1"
                    }
                } else {
                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                }else if(status.equals("2")){
                    style = "style='color:blue;'"
                } else {
                    style = ""
                }
                def isi1= ""
                if(tglMulai==null || tglMulai==""){
                    isi1 = it.tglMulai
                }else{
                    isi1 = tglMulai.format("dd/MM/YYYY")
                }
                def isi2= ""
                if(tglSelesai==null || tglSelesai==""){
                    isi2 = it.tglSelesai
                }else{
                    isi2 = tglSelesai.format("dd/MM/YYYY")
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+isi1+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+isi2+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.namaGroup+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.namaGoods+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.diskon+"\n" +
                        "                            </td>\n" +

                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadGroupDiskon.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah, baris warna biru data sudah ada")
        } else {
            flash.message = message(code: 'default.uploadGoodsDiskon.message', default: "Read File Done")
        }

        render(view: "index", model: [goodsDiskonInstance: goodsDiskonInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }
    def upload() {
        DateFormat df = new SimpleDateFormat("YYYY-MM-dd")

        def goodsDiskonInstance = null
        def goods = null
        def group = null
        def requestBody = request.JSON

        requestBody.each{
            goodsDiskonInstance = new GoodsDiskon()

            goods = Goods.findByM111IDIlikeAndStaDel(it.namaGoods,'0')
            group = Group.findByM181NamaGroupIlikeAndStaDel(it.namaGroup,'0')
            if(goods && group){
                String mul = it.tglMulai
                String sel = it.tglSelesai
                Date tglMulai = new Date().parse('yyyy-MM-d', mul.toString())

                    goodsDiskonInstance.m171TglAwal = new Date().parse('yyyy-MM-d',mul.toString())
                    goodsDiskonInstance.m171TglAkhir = new Date().parse('yyyy-MM-d', sel.toString())
                    goodsDiskonInstance.goods = goods
                    goodsDiskonInstance.group = group
                    def persen = (double) it.diskon
                    goodsDiskonInstance.m171PersenDiskon = persen

                    goodsDiskonInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                    goodsDiskonInstance.lastUpdProcess = "INSERT"
                    goodsDiskonInstance.setStaDel('0')
                    goodsDiskonInstance.dateCreated = datatablesUtilService?.syncTime()
                    goodsDiskonInstance.lastUpdated = datatablesUtilService?.syncTime()
                    try {
                        goodsDiskonInstance.save()
                    }catch (Exception e){
                        e.printStackTrace()
                    }

                flash.message = message(code: 'default.goodsDiskon.message', default: "Save Job Done")
            }else{
                flash.message = message(code: 'default.goodsDiskon.message', default: "gagal")
            }

        }

        render(view: "index", model: [goodsDiskonInstance: goodsDiskonInstance])

    }
}