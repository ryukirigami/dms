package com.kombos.customercomplaint

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.customerprofile.HistoryCustomerVehicle
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class ComplaintController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def complaintService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST", findNoPol: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

    def exportService
    def grailsApplication

	def list(Integer max) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy")
        if(!params.max) params.max = 10
        if(params?.format && params.format != "html"){
            response.contentType = grailsApplication.config.grails.mime.types[params.format]
            response.setHeader("Content-disposition", "attachment; filename=Complaint.${params.extension}")

            List fields = ["t921TglComplain", "t921NoReff","t921StaMediaKeluhan","t921DealerPembelian","t921DealerDiKomplain",
                    "t921AreaKeluhan","customerVehicle","historyCustomerVehicle","customer","historyCustomer","t921Km","t921StaPakaiKendaraan",
                    "t921Peran","t921NamaPelanggan","t921TelpRumah","t921TelpHp","t921Fax","t921Email","t921Alamat","t921ProfilPelanggan",
                    "t921TglKasusDiterima","t921TglLPKDikirim","t921DealerKomplain","t921StaKategoriKeluhan"]

            Map labels = ["t921TglComplain": "Tanggal Complain", "t921NoReff": "Nomor Referensi","t921StaMediaKeluhan":"Media Keluhan",
                    "t921DealerPembelian":"Main Dealer","t921DealerDiKomplain":"Dealer Yang Di Komplain","t921AreaKeluhan":"Area Keluhan",
                    "customerVehicle":"Customer Vehicle","historyCustomerVehicle":"History Customer Vehicle","customer":"Customer",
                    "historyCustomer":"History Customer","t921Km":"Odometer","t921StaPakaiKendaraan":"Status Pakai Kendaraan",
                    "t921Peran":"Peran Pelanggan","t921NamaPelanggan":"Nama Pelanggan","t921TelpRumah":"Telepon Rumah",
                    "t921TelpHp":"Telepon Selular","t921Fax":"Faximile","t921Email":"Email","t921Alamat":"Alamat Pelanggan",
                    "t921ProfilPelanggan":"Profil Pelanggan","t921TglKasusDiterima":"Tanggal Kasus Diterima dari TAM",
                    "t921TglLPKDikirim":"Tanggal LPK Dikirim ke TAM","t921DealerKomplain":"Dealer Tempat Mengkomplain",
                    "t921StaKategoriKeluhan":"Kategori Keluhan"]


            def formatTanggal = {
                domain, value -> return dateFormat.format(value)
            }
            Map formatters = [t921TglComplain: formatTanggal]
            exportService.export(params.format, response.outputStream, Complaint.list(params), fields, labels, formatters, null)
        }
        [ complaintInstanceList: Complaint.list(params)]
    }
	
	def datatablesList() {
		session.exportParams=params

		render complaintService.datatablesList(params) as JSON
	}

    def datatablesListUplad() {
        session.exportParams=params

        params.sessionId = session.id

        render complaintService.datatablesListUpload(params) as JSON
    }

    def findNoPol(){
        def result = complaintService.findNoPol(params);

        if(result!=null){
            render result as JSON
        }

//      flash.message = g.message(code: "complaint.noNoPol.message", default: "Tidak ada data")
//      render (view: 'create',model: [historyCVInstance: result,complaintInstance: complaintInstance])(view: 'create',model: [historyCVInstance: result,complaintInstance: complaintInstance])
//      return

    }

    def uploadPathComplainDoc(){
        def pathComplainDocInstance = new PathComplainDoc()
        pathComplainDocInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        pathComplainDocInstance?.lastUpdProcess = "INSERT"

        def sessionId

        def foto = request.getFile('t922Gambar')
        def notAllowContFoto = ['application/octet-stream']
        if(foto !=null && !notAllowContFoto.contains(foto.getContentType()) ){

            def okcontents = ['image/png', 'image/jpeg', 'image/gif']

            if (params.complaintId)  {
                pathComplainDocInstance?.complaint = Complaint.findById(new Long(params.complaintId))
                pathComplainDocInstance?.t922NamaFile = foto.getOriginalFilename()
                pathComplainDocInstance?.sessionIdCreation = ""
            }else{
                sessionId = session.id
                pathComplainDocInstance?.sessionIdCreation = sessionId
            }

            pathComplainDocInstance?.t922NamaFile = foto.getOriginalFilename()
            pathComplainDocInstance?.t922Gambar = foto.getBytes()
            pathComplainDocInstance?.t922Ket = params.t922Ket
            pathComplainDocInstance?.staData = "0"
            pathComplainDocInstance?.save(flush: true)
        }
        render "sukses"
    }

    def delPath(){
        String result=complaintService.delPath(params)
        render result
    }

    def tambahDealer(){
        String result=complaintService.tambahDealer(params)
        render result
    }

	def create() {
		def result = complaintService.create(params)
        def historyCVInstance = new HistoryCustomerVehicle(params);
        historyCVInstance.properties = params
        if(!result.error)
            return [complaintInstance: result.complaintInstance, historyCVInstance: historyCVInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.sessionId = session.id
	    def result = complaintService.save(params)

//        if(!result.error) {
//            flash.message = g.message(code: "default.created.message", args: ["Complaint", result.complaintInstance.id])
//            redirect(action:'show', id: result.complaintInstance.id)
//            return
//        }
//
//        render(view:'create', model:[complaintInstance: result.complaintInstance])

        def res = [:]
        if(result.errors){
            String message = ""
            result.errors.each{
                if(!it.toString().equals("grails.validation.ValidationErrors: 0 errors")){
                    if (it.toString().contains("t921NoReff")) {message = message + "Nomor Referensi is mandatory </br>"}
                    if (it.toString().contains("t921StaMediaKeluhan")) {message = message + "Media Keluhan is mandatory </br>"}
                    if (it.toString().contains("t921DealerDiKomplain")) {message = message + "Dealer yang Dikomplain is mandatory </br>"}
                    if (it.toString().contains("t921StaKategoriKeluhan")) {message = message + "Kategori Keluhan is mandatory </br>"}
                    if (it.toString().contains("t921AreaKeluhan")) {message = message + "Area Keluhan is mandatory </br>"}
                    if (it.toString().contains("t921NamaPelanggan")) {message = message + "Nama Pelanggan is mandatory </br>"}
                    if (it.toString().contains("t921Km")) {message = message + "Odometer is mandatory </br>"}
                    if (it.toString().contains("t921StaPakaiKendaraan")) {message = message + "Status Pakaian Kendaraan is mandatory </br>"}
                    if (it.toString().contains("t921Peran")) {message = message + "Peran Pelanggan is mandatory </br>"}
                    if (it.toString().contains("t921TelpRumah")) {message = message + "Telepon Rumah is mandatory </br>"}
                    if (it.toString().contains("t921TelpHp")) {message = message + "Telepon Selular is mandatory </br>"}
                    if (it.toString().contains("t921Fax")) {message = message + "Faximile is mandatory </br>"}
                    if (it.toString().contains("t921Email")) {message = message + "Email is mandatory </br>"}
                    if (it.toString().contains("t921Alamat")) {message = message + "Alamat is mandatory </br>"}
                    if (it.toString().contains("t921ProfilPelanggan")) {message = message + "Profil Pelanggan is mandatory </br>"}
                    if (it.toString().contains("t921TglKasusDiterima")) {message = message + "Tanggal Kasus Diterima dari TAM is mandatory </br>"}
                    if (it.toString().contains("t921TglLPKDikirim")) {message = message + "Tanggal LPK Dikirim ke TAM is mandatory </br>"}
                    if (it.toString().contains("t921DealerPembelian")) {message = message + "Dealer Pembelian is mandatory </br>"}
                    if (it.toString().contains("t921DealerKomplain")) {message = message + "Dealer Tempat Mengkomplain is mandatory </br>"}
                    if (it.toString().contains("t921Keluhan")) {message = message + "Keluhan is mandatory </br>"}
//                    if (it.toString().contains("t921Permintaan")) {message = message + "t921Permintaan is mandatory </br>"}
                    if (it.toString().contains("t921LatarBelakangTgl")) {message = message + "Tanggal Latar Belakang is mandatory </br>"}
//                    if (it.toString().contains("t921LatarBelakangPihakTerlibat")) {message = message + "t921LatarBelakangPihakTerlibat is mandatory </br>"}
//                    if (it.toString().contains("t921LatarBelakangFakta")) {message = message + "t921LatarBelakangFakta is mandatory </br>"}
//                    if (it.toString().contains("t921SebabPelanggan")) {message = message + "t921SebabPelanggan is mandatory </br>"}
//                    if (it.toString().contains("t921SebabDealer")) {message = message + "t921SebabDealer is mandatory </br>"}
                    if (it.toString().contains("t921StaPenanganan")) {message = message + "Status Penanganan is mandatory </br>"}
//                    if (it.toString().contains("t921StrategiPenyelesaian")) {message = message + "t921StrategiPenyelesaian is mandatory </br>"}
//                    if (it.toString().contains("t921TindakanTgl")) {message = message + "t921TindakanTgl is mandatory </br>"}
//                    if (it.toString().contains("t921TindakanPihakTerlibat")) {message = message + "t921TindakanPihakTerlibat is mandatory </br>"}
//                    if (it.toString().contains("t921TindakanFakta")) {message = message + "t921TindakanFakta is mandatory </br>"}
                    if (it.toString().contains("t921StaSolusi")) {message = message + "Solusi Penyelesaian is mandatory </br>"}
//                    if (it.toString().contains("t921StaKepuasanPelanggan")) {message = message + "t921StaKepuasanPelanggan is mandatory </br>"}
//                    if (it.toString().contains("t921AlasanKepuasan")) {message = message + "t921AlasanKepuasan is mandatory </br>"}
//                    if (it.toString().contains("t921TglPenyelesaianKeluhan")) {message = message + "t921TglPenyelesaianKeluhan is mandatory </br>"}
//                    if (it.toString().contains("t921StaLangkahPencegahan")) {message = message + "t921StaLangkahPencegahan is mandatory </br>"}
//                    if (it.toString().contains("t921TextLangkahPencegahan")) {message = message + "t921TextLangkahPencegahan is mandatory </br>"}
                }
            }
            res.message = message
        }
        render res as JSON
	}

	def show(Long id) {
		def result = complaintService.show(params)

		if(!result.error)
			return [ complaintInstance: result.complaintInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = complaintService.show(params)

		if(!result.error)
			return [ complaintInstance: result.complaintInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
		 def result = complaintService.update(params)
        params.lastUpdated = datatablesUtilService?.syncTime()

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Complaint", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[complaintInstance: result.complaintInstance.attach()])
	}

	def delete() {
		def result = complaintService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Complaint", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(Complaint, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def report = {
        def file = complaintService.createReport(Complaint.findAllById(new Long(params.idComplaint)))

        response.setHeader('Content-disposition', 'attachment;filename=Report.xls')
        response.setHeader('Content-length', "${file.size()}")

        OutputStream out = new BufferedOutputStream(response.outputStream)

        try {
            out.write(file.bytes)

        } finally {
            out.close()
            return false
        }
    }
}