package com.kombos.customercomplaint

import com.kombos.administrasi.DealerPenjual
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class PilihDealerController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = DealerPenjual.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params."sCriteria_m091NamaDealer"){
                ilike("m091NamaDealer","%" + (params."sCriteria_m091NamaDealer" as String) + "%")
            }

            if(params."sCriteria_m091AlamatDealer"){
                ilike("m091AlamatDealer","%" + (params."sCriteria_m091AlamatDealer" as String) + "%")
            }

            if(params."sCriteria_m091ID"){
                eq("m091ID",params."sCriteria_m091ID")
            }

            if(params."sCriteria_staDel"){
                ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m091NamaDealer: it.m091NamaDealer,

                    m091AlamatDealer: it.m091AlamatDealer,

                    m091ID: it.m091ID,

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def show(Long id) {
        def pilihDealerInstance = DealerPenjual.get(id)
        if (!pilihDealerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'pilihDealer.label', default: 'DealerPenjual'), id])
            redirect(action: "list")
            return
        }

        [pilihDealerInstance: pilihDealerInstance]
    }

}
