package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class JenisSurveyController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = JenisSurvey.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel", "0")
            if (params."sCriteria_m131ID") {
                ilike("m131ID", "%" + (params."sCriteria_m131ID" as String) + "%")
            }

            if (params."sCriteria_m131JenisSurvey") {
                ilike("m131JenisSurvey", "%" + (params."sCriteria_m131JenisSurvey" as String) + "%")
            }

            if (params."sCriteria_m131TipeSurvey") {
                eq("m131TipeSurvey",params."sCriteria_m131TipeSurvey")
            }

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m131ID: it.m131ID,

                    m131JenisSurvey: it.m131JenisSurvey,

                    m131TipeSurvey: it.m131TipeSurvey,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        def jenisSurveyInstance = new JenisSurvey(params)
        jenisSurveyInstance.setM131TipeSurvey("0")
        [jenisSurveyInstance: jenisSurveyInstance]

    }

    def save() {
        def jenisSurveyInstance = new JenisSurvey(params)
        jenisSurveyInstance.setCreatedBy (org.apache.shiro.SecurityUtils.subject.principal.toString())
        jenisSurveyInstance.setLastUpdProcess("INSERT")
        jenisSurveyInstance.setStaDel("0")
        jenisSurveyInstance?.setDateCreated(datatablesUtilService?.syncTime())
        jenisSurveyInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        def cek = JenisSurvey.createCriteria().list() {
            and{
                eq("m131JenisSurvey",jenisSurveyInstance.m131JenisSurvey?.trim(), [ignoreCase: true])
                eq("m131TipeSurvey",jenisSurveyInstance.m131TipeSurvey)
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [jenisSurveyInstance: jenisSurveyInstance])
            return
        }
        String sid = generateCodeService.codeGenerateSequence("M131_ID",null)
        jenisSurveyInstance?.m131ID = (sid.length() == 2) ? "00" + sid : (sid.length() == 3) ? "0" + sid : sid
        if (!jenisSurveyInstance.save(flush: true)) {
            render(view: "create", model: [jenisSurveyInstance: jenisSurveyInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), jenisSurveyInstance.id])
        redirect(action: "show", id: jenisSurveyInstance.id)
    }

    def show(Long id) {
        def jenisSurveyInstance = JenisSurvey.get(id)
        if (!jenisSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), id])
            redirect(action: "list")
            return
        }

        [jenisSurveyInstance: jenisSurveyInstance]
    }

    def edit(Long id) {
        def jenisSurveyInstance = JenisSurvey.get(id)
        if (!jenisSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), id])
            redirect(action: "list")
            return
        }

        [jenisSurveyInstance: jenisSurveyInstance]
    }

    def update(Long id, Long version) {
        def jenisSurveyInstance = JenisSurvey.get(id)
        if (!jenisSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (jenisSurveyInstance.version > version) {

                jenisSurveyInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'jenisSurvey.label', default: 'Jenis Survey')] as Object[],
                        "Another user has updated this JenisSurvey while you were editing")
                render(view: "edit", model: [jenisSurveyInstance: jenisSurveyInstance])
                return
            }
        }

        def cek = JenisSurvey.createCriteria()
        def result = cek.list() {
            and{
                eq("m131JenisSurvey",params.m131JenisSurvey?.trim(), [ignoreCase: true])
                eq("m131TipeSurvey",params.m131TipeSurvey)
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [jenisSurveyInstance: jenisSurveyInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        jenisSurveyInstance.properties = params
        jenisSurveyInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        jenisSurveyInstance.setLastUpdProcess("UPDATE")

        if (!jenisSurveyInstance.save(flush: true)) {
            render(view: "edit", model: [jenisSurveyInstance: jenisSurveyInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), jenisSurveyInstance.id])
        redirect(action: "show", id: jenisSurveyInstance.id)
    }

    def delete(Long id) {
        def jenisSurveyInstance = JenisSurvey.get(id)
        if (!jenisSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), id])
            redirect(action: "list")
            return
        }

        try {
            jenisSurveyInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            // yang wajib di set
            jenisSurveyInstance.setLastUpdProcess("DELETE")
// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
            jenisSurveyInstance.setStaDel("1") // untuk data awal di set 0 karena bukan data yang dihapus
            jenisSurveyInstance.save(flush: true)
            // 4 baris codingan di atas ini digunakan jika domain2 yang mempunyain field staDel
            // jika tidak ada stadel cukup dengan codingan 1 baris dibawah ini
            //jenisSurveyInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'jenisSurvey.label', default: 'Jenis Survey'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(JenisSurvey, params)
            // ada 2 function yang bisa digunakan
            // yaitu massDelete dan massDeleteStaDelNew
            // massDelete khusus untuk domain2 yang tidak ada field staDel
            // massDeleteStaDelNew khusus untuk domain2 yang ada field staDel
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(JenisSurvey, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
