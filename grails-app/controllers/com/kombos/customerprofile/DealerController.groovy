package com.kombos.customerprofile

import com.kombos.administrasi.DealerPenjual
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class DealerController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def c = DealerPenjual.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            if(params."sCriteria_m091NamaDealer"){
                ilike("m091NamaDealer","%" + (params."sCriteria_m091NamaDealer" as String) + "%")
            }

            if(params."sCriteria_m091AlamatDealer"){
                ilike("m091AlamatDealer","%" + (params."sCriteria_m091AlamatDealer" as String) + "%")
            }

            if(params."sCriteria_m091ID"){
                eq("m091ID",Integer.parseInt(params."sCriteria_m091ID"))
            }

            if(params."sCriteria_staDel"){
                ilike("staDel","%" + (params."sCriteria_staDel" as String) + "%")
            }


            switch(sortProperty){
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m091NamaDealer: it.m091NamaDealer,

                    m091AlamatDealer: it.m091AlamatDealer,

                    m091ID: it.m091ID,

                    staDel: it.staDel,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [dealerInstance: new DealerPenjual(params)]
    }

    def save() {
        def find = DealerPenjual.findAllByStaDel('0')
        String id=''
        if(find){
            int a=find.last().m091ID.toInteger()+1
            for(int b=a.toString().length();b<4;b++){
                id+='0'
            }
            id = id+a
        }else{
            id = '0001'
        }
        def dealerInstance = new DealerPenjual(params)
        dealerInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        dealerInstance?.lastUpdProcess = "INSERT"
        dealerInstance?.dateCreated = datatablesUtilService?.syncTime()
        dealerInstance?.lastUpdated = datatablesUtilService?.syncTime()
        dealerInstance?.setStaDel ('0')
        dealerInstance?.setM091ID(id)
        def cek = DealerPenjual.createCriteria().list {
            eq('staDel','0')
            eq('m091NamaDealer',params.m091NamaDealer.trim(),[ignoreCase : true])
            eq('m091AlamatDealer',params.m091AlamatDealer.trim(),[ignoreCase : true])
        }
        if(cek){
            flash.message="Data sudah ada"
            render(view: "create", model: [dealerInstance: dealerInstance])
            return
        }
        if (!dealerInstance.save(flush: true)) {
            render(view: "create", model: [dealerInstance: dealerInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'dealer.label', default: 'Dealer'), ''])
        redirect(action: "show", id: dealerInstance.id)
    }

    def show(Long id) {
        def dealerInstance = DealerPenjual.get(id)
        if (!dealerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealer.label', default: 'Dealer'), id])
            redirect(action: "list")
            return
        }

        [dealerInstance: dealerInstance]
    }

    def edit(Long id) {
        def dealerInstance = DealerPenjual.get(id)
        if (!dealerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealer.label', default: 'Dealer'), id])
            redirect(action: "list")
            return
        }

        [dealerInstance: dealerInstance]
    }

    def update(Long id, Long version) {
        def dealerInstance = DealerPenjual.get(id)
        if (!dealerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealer.label', default: 'Dealer'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (dealerInstance.version > version) {

                dealerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'dealer.label', default: 'DealerPenjual')] as Object[],
                        "Another user has updated this Dealer while you were editing")
                render(view: "edit", model: [dealerInstance: dealerInstance])
                return
            }
        }
        def cek = DealerPenjual.createCriteria().list {
            eq('staDel','0')
            eq('m091NamaDealer',params.m091NamaDealer.trim(),[ignoreCase : true])
            eq('m091AlamatDealer',params.m091AlamatDealer.trim(),[ignoreCase : true])
        }
        if(cek){
            for(find in cek){
                if(find.id!=id){
                    flash.message="Data sudah ada"
                    render(view: "edit", model: [dealerInstance: dealerInstance])
                    return
                }
            }
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        dealerInstance.properties = params
        dealerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        dealerInstance?.lastUpdProcess = "UPDATE"

        if (!dealerInstance.save(flush: true)) {
            render(view: "edit", model: [dealerInstance: dealerInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'dealer.label', default: 'Dealer'), ''])
        redirect(action: "show", id: dealerInstance.id)
    }

    def delete(Long id) {
        def dealerInstance = DealerPenjual.get(id)
        if (!dealerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dealer.label', default: 'Dealer'), id])
            redirect(action: "list")
            return
        }

        try {
            dealerInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            dealerInstance?.lastUpdProcess = "DELETE"
            dealerInstance?.setStaDel('1')
            dealerInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'dealer.label', default: 'Dealer'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'dealer.label', default: 'Dealer'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(DealerPenjual, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

    def updatefield(){
        def res = [:]
        try {
            datatablesUtilService.updateField(DealerPenjual, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message?:e.cause?.message
        }
        render "ok"
    }

}
