package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.Company
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SPKDetailController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = SPKDetail.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer);
            if (params."sCriteria_spk") {
                eq("spk", SPK.findByT191NoSPKIlike("%"+params."sCriteria_spk"+"%"))
            }

            if (params."sCriteria_customerVehicle") {
                eq("customerVehicle",CustomerVehicle.findByT103VinCodeIlike("%"+params."sCriteria_customerVehicle"+"%"))
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_company") {
                eq("company", Company.findByNamaPerusahaanIlike("%"+params."sCriteria_company"+"%"))
            }

            eq("staDel","0")
            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    spk: it.spk?.toString(),

                    customerVehicle: it.customerVehicle?.toString(),

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    company: it.company?.namaPerusahaan,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [SPKDetailInstance: new SPKDetail(params)]
    }

    def save() {
        def SPKDetailInstance = new SPKDetail()


        def customerVehicle = CustomerVehicle.findByT103VinCode(params.customerVehicle)
        SPKDetailInstance?.dateCreated = datatablesUtilService?.syncTime()
        SPKDetailInstance?.lastUpdated = datatablesUtilService?.syncTime()

        SPKDetailInstance.company = Company.get(params.company.id)
        SPKDetailInstance.spk = SPK.get(params.spk.id)
        SPKDetailInstance.customerVehicle = customerVehicle
        SPKDetailInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        SPKDetailInstance.setLastUpdProcess("INSERT")
        SPKDetailInstance?.staDel = '0'
        SPKDetailInstance?.companyDealer = session?.userCompanyDealer

        if (!SPKDetailInstance.save(flush: true)) {

            render(view: "create", model: [SPKDetailInstance: SPKDetailInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), SPKDetailInstance.id])
        render(view: "create", model: [SPKDetailInstance: SPKDetailInstance])
        //  redirect(action: "show", id: SPKDetailInstance.id)
    }

    def show(Long id) {
        def SPKDetailInstance = SPKDetail.get(id)

        if (!SPKDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), id])
            redirect(action: "list")
            return
        }

        [SPKDetailInstance: SPKDetailInstance]
    }

    def edit(Long id) {
        def SPKDetailInstance = SPKDetail.get(id)
        if (!SPKDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), id])
            redirect(action: "list")
            return
        }


        [SPKDetailInstance: SPKDetailInstance]
    }

    def update(Long id, Long version) {
        def SPKDetailInstance = SPKDetail.get(Long.parseLong(params.spkDetail.id))
        if (!SPKDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), id])
            redirect(action: "list")
            return
        }


        def customerVehicle = CustomerVehicle.findByT103VinCode(params.customerVehicle)

        SPKDetailInstance?.lastUpdated = datatablesUtilService?.syncTime()
        SPKDetailInstance.company = Company.get(params.company.id)
        SPKDetailInstance.spk = SPK.get(params.spk.id)
        SPKDetailInstance.customerVehicle = customerVehicle
        SPKDetailInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        SPKDetailInstance.setLastUpdProcess("Update")

        if (version != null) {
            if (SPKDetailInstance.version > version) {

                SPKDetailInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'SPKDetail.label', default: 'SPKDetail')] as Object[],
                        "Another user has updated this SPKDetail while you were editing")
                render(view: "edit", model: [SPKDetailInstance: SPKDetailInstance])
                return
            }
        }

        if (!SPKDetailInstance.save(flush: true)) {
            render(view: "edit", model: [SPKDetailInstance: SPKDetailInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), SPKDetailInstance.id])
        redirect(action: "show", id: SPKDetailInstance.id)
    }

    def delete(Long id) {
        def SPKDetailInstance = SPKDetail.get(id)
        if (!SPKDetailInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), id])
            redirect(action: "list")
            return
        }

        SPKDetailInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        SPKDetailInstance?.lastUpdProcess = "DELETE"
        SPKDetailInstance.staDel = '1'

        try {
            SPKDetailInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'SPKDetail.label', default: 'SPKDetail'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDel(SPKDetail, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(SPKDetail, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


    def getCustomerVehicle(){
        def res = [:]

        def result = CustomerVehicle.createCriteria().list {
            eq("staDel","0");
            ilike("t103VinCode",params?.query+"%");
            maxResults(10);
        }
        def opts = []
        result.each {
            opts << it.t103VinCode
        }

        res."options" = opts
        render res as JSON
    }

    def getNamaAlamatSTNK(){

        String customerVehicleID = params.customerVehicle
        def customerVehicle = CustomerVehicle.findByT103VinCodeAndStaDel(customerVehicleID?.trim(),"0")
        HistoryCustomerVehicle customerVehicleHistory = customerVehicle?.histories?.size()>0 ? customerVehicle?.histories?.last() : null

        def res = [namaSTNK : customerVehicleHistory? customerVehicleHistory.t183NamaSTNK : "", alamatSTNK : customerVehicleHistory? customerVehicleHistory.t183AlamatSTNK : ""]
        render res as JSON

    }

}
