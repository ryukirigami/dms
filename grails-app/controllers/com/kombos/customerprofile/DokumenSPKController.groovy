package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.commons.codec.binary.Base64
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.commons.CommonsMultipartFile

class DokumenSPKController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        SPKAsuransi spkAsuransi = SPKAsuransi.findById(new Long(params.spkAsuransiId))
        params.sCriteria_spkAsuransi = spkAsuransi

        def c = DokumenSPK.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_spkAsuransi") {
                eq("spkAsuransi", params."sCriteria_spkAsuransi")
            }

            if (params."sCriteria_kelengkapanDokumenAsuransi") {
                eq("kelengkapanDokumenAsuransi", params."sCriteria_kelengkapanDokumenAsuransi")
            }

            if (params."sCriteria_t195ID") {
                eq("t195ID", params."sCriteria_t195ID")
            }

            if (params."sCriteria_t195Keterangan") {
                ilike("t195Keterangan", "%" + (params."sCriteria_t195Keterangan" as String) + "%")
            }

            if (params."sCriteria_t195Foto") {
                ilike("t195Foto", "%" + (params."sCriteria_t195Foto" as String) + "%")
            }

            if (params."sCriteria_t195TglJamUpload") {
                ge("t195TglJamUpload", params."sCriteria_t195TglJamUpload")
                lt("t195TglJamUpload", params."sCriteria_t195TglJamUpload" + 1)
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    spkAsuransi: it.spkAsuransi.t193IDAsuransi,

                    kelengkapanDokumenAsuransi: it.kelengkapanDokumenAsuransi.m195NamaDokumenAsuransi,

                    t195ID: it.t195ID,

                    t195Keterangan: it.t195Keterangan,

                    t195Foto: it.t195Foto,

                    t195TglJamUpload: it.t195TglJamUpload ? it.t195TglJamUpload.format("HH:mm") : "",

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [dokumenSPKInstance: new DokumenSPK(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.staDel = '0'
        params.lastUpdProcess = "INSERT"
        params.spkAsuransi = SPKAsuransi.findById(new Long(params.spkAsuransiId))
        params.t195TglJamUpload = new Date()
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.companyDealer = session?.userCompanyDealer

        if(session.dokumenSPK){
            String dirImage = "C:/image/dokumenSPK"
            String newName = params.spkAsuransiId+"_"+params.kelengkapanDokumenAsuransi.id+"."+session.dokumenSPK.toString().tokenize(".").last()
            File file = new File(session.dokumenSPK)
            file.renameTo(new File(dirImage+"/"+newName))
            params.t195Foto = dirImage+"/"+newName
        }

        def dokumenSPKInstance = new DokumenSPK(params)
        if (!dokumenSPKInstance.save(flush: true)) {
            redirect(action: "edit", id: params.spkAsuransiId)
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), dokumenSPKInstance.id])
        redirect(action: "edit", id: params.spkAsuransiId)
    }

    def show(Long id) {
        def dokumenSPKInstance = DokumenSPK.get(id)
        if (!dokumenSPKInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), id])
            redirect(action: "list")
            return
        }

        [dokumenSPKInstance: dokumenSPKInstance]
    }

    def edit(Long id) {
//        def dokumenSPKInstance = DokumenSPK.get(id)
//        if (!dokumenSPKInstance) {
//            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), id])
//            redirect(action: "list")
//            return
//        }
//
//        [dokumenSPKInstance: dokumenSPKInstance]

        def SPKAsuransiInstance = SPKAsuransi.get(id)
        if (!SPKAsuransiInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), id])
            redirect(action: "list")
            return
        }

        [SPKAsuransiInstance: SPKAsuransiInstance,dokumenSPKInstance: new DokumenSPK(params)]
    }

    def update(Long id, Long version) {
        def dokumenSPKInstance = DokumenSPK.get(id)
        if (!dokumenSPKInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (dokumenSPKInstance.version > version) {

                dokumenSPKInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'dokumenSPK.label', default: 'DokumenSPK')] as Object[],
                        "Another user has updated this DokumenSPK while you were editing")
                render(view: "edit", model: [dokumenSPKInstance: dokumenSPKInstance])
                return
            }
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        dokumenSPKInstance.properties = params

        if (!dokumenSPKInstance.save(flush: true)) {
            render(view: "edit", model: [dokumenSPKInstance: dokumenSPKInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), dokumenSPKInstance.id])
        redirect(action: "show", id: dokumenSPKInstance.id)
    }

    def delete(Long id) {
        def dokumenSPKInstance = DokumenSPK.get(id)
        if (!dokumenSPKInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), id])
            redirect(action: "list")
            return
        }

        try {
            dokumenSPKInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'dokumenSPK.label', default: 'DokumenSPK'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(DokumenSPK, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(DokumenSPK, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def uploadImage() {
        if (params.myFile) {
            String fileName
            def inputStream
            def byteFile
            if (params.myFile instanceof CommonsMultipartFile) {
                fileName = params.myFile?.originalFilename
                inputStream = params.myFile.getInputStream()
            } else {
                fileName = params.myFile
                inputStream = request.getInputStream()
            }

//            fileName = org.apache.shiro.SecurityUtils.subject.principal.toString()+"_temp."+fileName.tokenize(".").last()
            fileName = fileName.replaceAll(" ", "_")

            File storedFile
            String dirImage = "C:/image/dokumenSPK"
            boolean fileSuccessfullyDeleted =  new File("${dirImage}/${fileName}").delete()

            try {
                storedFile = new File("${dirImage}/${fileName}")
                storedFile.append(inputStream)
            }catch(Exception e){
                try{
                    new File("${dirImage}").mkdirs()
                    storedFile = new File("${dirImage}/${fileName}")
                    storedFile.append(inputStream)
                }catch (Exception ex){
                    println(ex)
                    render "Directory ${dirImage} is write protected, please change the directory's permission"
                }
            }

            session.dokumenSPK = storedFile.path

            render '<img width="250" id="uploadedImage" name="uploadedImage" src="data:' + 'jpg' + ';base64,' + new String(new Base64().encode(storedFile.bytes), "UTF-8") + '" ' + ' />'
        } else {
            render "No Image"
        }
    }
}
