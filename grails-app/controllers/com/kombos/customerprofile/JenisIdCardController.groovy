package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class JenisIdCardController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

    def jenisIdCardService

    def generateCodeService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}
	
	def datatablesList() {
		render jenisIdCardService.datatablesList(params) as JSON
	}

	def create() {
		[jenisIdCardInstance: new JenisIdCard(params)]
	}

	def save() {
		def jenisIdCardInstance = new JenisIdCard(params)
        jenisIdCardInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        jenisIdCardInstance.setLastUpdProcess("INSERT")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
        jenisIdCardInstance.setStaDel("0") // untuk data awal di set 0 karena bukan data yang dihapus
        jenisIdCardInstance?.setDateCreated(datatablesUtilService?.syncTime())
        jenisIdCardInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        def cek = JenisIdCard.createCriteria().list() {
            and{
                eq("m060JenisIDCard",jenisIdCardInstance.m060JenisIDCard?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            jenisIdCardInstance.m060ID = null
            render(view: "create", model: [jenisIdCardInstance: jenisIdCardInstance])
            return
        }
        jenisIdCardInstance?.m060ID = generateCodeService.codeGenerateSequence("M060_ID",null)
        if (!jenisIdCardInstance.save(flush: true)) {
			render(view: "create", model: [jenisIdCardInstance: jenisIdCardInstance])
			return
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'jenisIdCard.label', default: 'Jenis Id Card'), jenisIdCardInstance.id])
		redirect(action: "show", id: jenisIdCardInstance.id)
	}

	def show(Long id) {
		def jenisIdCardInstance = JenisIdCard.get(id)
		if (!jenisIdCardInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisIdCard.label', default: 'Jenis Id Card'), id])
			redirect(action: "list")
			return
		}

		[jenisIdCardInstance: jenisIdCardInstance]
	}

	def edit(Long id) {
		def jenisIdCardInstance = JenisIdCard.get(id)
		if (!jenisIdCardInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisIdCard.label', default: 'Jenis Id Card'), id])
			redirect(action: "list")
			return
		}

		[jenisIdCardInstance: jenisIdCardInstance]
	}

	def update(Long id, Long version) {
		def jenisIdCardInstance = JenisIdCard.get(id)
//		if (!jenisIdCardInstance) {
//			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisIdCard.label', default: 'Jenis Id Card'), id])
//			redirect(action: "list")
//			return
//		}
        String original = jenisIdCardInstance.m060JenisIDCard
        jenisIdCardInstance.m060JenisIDCard = params.m060JenisIDCard
		if (version != null) {
			if (jenisIdCardInstance.version > version) {
				
				jenisIdCardInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
				[message(code: 'jenisIdCard.label', default: 'Jenis Id Card')] as Object[],
				"Another user has updated this JenisIdCard while you were editing")
				render(view: "edit", model: [jenisIdCardInstance: jenisIdCardInstance])
				return
			}
		}

        def cek = JenisIdCard.createCriteria()
        def result = cek.list() {
            and{
                eq("m060JenisIDCard",jenisIdCardInstance.m060JenisIDCard?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            boolean isExist = false;
            for(JenisIdCard jenisId : result) {
                if (jenisId.m060ID != jenisIdCardInstance.m060ID) {
                    isExist = true
                }
            }
            if (isExist) {
                flash.message = "Jenis Id Card " + jenisIdCardInstance.m060JenisIDCard + " Sudah Ada"
                jenisIdCardInstance.m060JenisIDCard = original
                render(view: "edit", model: [jenisIdCardInstance: jenisIdCardInstance])
                return
            }
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
		jenisIdCardInstance.properties = params
        jenisIdCardInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        jenisIdCardInstance.setLastUpdProcess("UPDATE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"

        if (!jenisIdCardInstance.save(flush: true)) {
			render(view: "edit", model: [jenisIdCardInstance: jenisIdCardInstance])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'jenisIdCard.label', default: 'Jenis Id Card'), jenisIdCardInstance.id])
		redirect(action: "show", id: jenisIdCardInstance.id)
	}

	def delete(Long id) {
		def jenisIdCardInstance = JenisIdCard.get(id)
		if (!jenisIdCardInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'jenisIdCard.label', default: 'Jenis Id Card'), id])
			redirect(action: "list")
			return
		}

		try {
            jenisIdCardInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
            jenisIdCardInstance.setLastUpdProcess("DELETE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
            jenisIdCardInstance.setStaDel("1") // untuk data awal di set 0 karena bukan data yang dihapus
            jenisIdCardInstance.save(flush: true)
            // 4 baris codingan di atas ini digunakan jika domain2 yang mempunyain field staDel
            // jika tidak ada stadel cukup dengan codingan 1 baris dibawah ini
            //jenisIdCardInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'jenisIdCard.label', default: 'Jenis Id Card'), id])
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'jenisIdCard.label', default: 'JenisIdCard'), id])
			redirect(action: "show", id: id)
		}
	}
	
	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDeleteStaDelNew(JenisIdCard, params)
            // ada 2 function yang bisa digunakan
            // yaitu massDelete dan massDeleteStaDelNew
            // massDelete khusus untuk domain2 yang tidak ada field staDel
            // massDeleteStaDelNew khusus untuk domain2 yang ada field staDel
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"		
	}
	
	def updatefield(){
		def res = [:]
		try {
			datatablesUtilService.updateField(JenisIdCard, params)
			res.message = "Update Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

}
