package com.kombos.customerprofile

import com.kombos.administrasi.Operation
import com.kombos.maintable.JobFA
import com.kombos.maintable.PartsFA
import com.kombos.parts.Goods
import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class PartsJobFieldActionController {
def datatablesUtilService
    def index() {

    }


    def partsDatatablesList() {

        def ret
        def c = PartsFA.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            fa{
                eq("id",params.fa.toLong())
            }
            eq("staDel","0")
        }

        def rows = []

        results.each {
            rows << [
                    id: it.goods.id,
                    deskripsi: it?.fa?.m185NamaFA,
                    nama: it?.goods?.m111Nama,
                    jumlah: it?.m187Qty1 ? it?.m187Qty1 : 0,
                    satuan: it?.goods?.satuan?.m118Satuan1
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def deleteParts() {
        def fa = FA.read(params.fa.toLong())
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Goods.get(it) }
        oList.each {
            def partfa = PartsFA.findByFaAndGoodsAndStaDel(fa, it,"0")
            if (partfa) {
                partfa.updatedBy = SecurityUtils.subject.principal.toString()
                partfa.lastUpdProcess = "DELETE"
                partfa.staDel = "1"
                partfa.save(flush: true)
                partfa.errors.each {println it}
            }
        }
        render "ok"
    }

    def tambahParts() {
        def fa = FA.get(params.fa.toLong())
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Goods.get(it) }
        oList.each {
            def partfa = new PartsFA()
            partfa.fa = fa
            partfa.goods = it
            partfa.staDel = "0"
            partfa.lastUpdProcess = "INSERT"
            partfa?.dateCreated = datatablesUtilService?.syncTime()
            partfa?.lastUpdated = datatablesUtilService?.syncTime()
            partfa.createdBy = SecurityUtils.subject.principal.toString()
            partfa.updatedBy = SecurityUtils.subject.principal.toString()
            partfa.save(flush: true)
            partfa.errors.each {println it}
        }
        render "ok"
    }

    def deleteJobs() {
        def fa = FA.get(params.fa.toLong())
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Operation.get(it) }
        oList.each {
            def jobfa = JobFA.findByFaAndOperationAndStaDel(fa, it,"0")
            if (jobfa) {
                jobfa.updatedBy = SecurityUtils.subject.principal.toString()
                jobfa.lastUpdProcess = "DELETE"
                jobfa.staDel = "1"
                jobfa.save(flush: true)
                jobfa.errors.each {println it}
            }
        }
        render "ok"
    }

    def tambahJobs() {
        def fa = FA.read(params.fa.toLong())
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << Operation.get(it) }
        oList.each {
            def jobfa = new JobFA()
            jobfa.fa= fa
            jobfa.operation= it
            jobfa.staDel= "0"
            jobfa.lastUpdProcess= "INSERT"
            jobfa?.dateCreated = datatablesUtilService?.syncTime()
            jobfa?.lastUpdated = datatablesUtilService?.syncTime()
            jobfa.createdBy= SecurityUtils.subject.principal.toString()
            jobfa.updatedBy= SecurityUtils.subject.principal.toString()
            jobfa.save(flush: true)
            jobfa.errors.each {println it}
        }
        render "ok"
    }

    def jobsDatatablesList() {

        def ret

        def c = JobFA.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            fa{
                eq("id",params.fa.toLong())
            }
            eq("staDel","0")
        }

        def rows = []

        results.each {
            rows << [
                    id: it.operation?.id,
                    deskripsi: it?.fa?.m185NamaFA,
                    nama: it?.operation?.m053NamaOperation
            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def searchPartsDatatablesList() {

        def ret

        def cek = PartsFA.createCriteria().list {
            eq("staDel","0")
            fa{
                eq("id",params.fa.toLong())
            }
        }
        def idFA = []
        cek.each {
            idFA<<it.goods.id
        }
        def c = Goods.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq('staDel',"0")
            if(idFA.size()>0){
                not {
                    'in'('id',idFA)
                }
            }
            if (params."sCriteria_kode") {
                ilike("m111ID", "%" + (params."sCriteria_kode" as String) + "%")
            }

            if (params."sCriteria_nama") {
                ilike("m111Nama", "%" + (params."sCriteria_nama" as String) + "%")
            }
            order("m111ID")
        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    kode: it?.m111ID,
                    nama: it?.m111Nama
            ]
        }


        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def searchJobsDatatablesList() {
        def ret
        def cek = JobFA.createCriteria().list {
            eq("staDel","0")
            fa{
                eq("id",params.fa.toLong())
            }
        }
        def idFA = []
        cek.each {
            idFA<<it.operation.id
        }
        def c = Operation.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq('staDel',"0")
            if(idFA.size()>0){
                not {
                    'in'('id',idFA)
                }
            }
            if (params."sCriteria_kode") {
                ilike("m053JobsId", "%" + (params."sCriteria_kode" as String) + "%")
            }

            if (params."sCriteria_nama") {
                ilike("m053NamaOperation", "%" + (params."sCriteria_nama" as String) + "%")
            }
            order("m053JobsId")
        }


        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    kode: it?.m053JobsId,
                    nama: it?.m053NamaOperation
            ]
        }


        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def doSave(){
        def jsonArray = JSON.parse(params.ids)
        def jsonJumlah = JSON.parse(params.jumlah)
        def fa = FA.get(params.fa.toLong())
        def oList = []
        jsonArray.each { oList << Goods.get(it) }
        int ind = 0;
        oList.each {
            def partsFA = PartsFA.findByFaAndGoodsAndStaDel(fa,it,"0");
            partsFA.m187Qty1 = jsonJumlah[ind].toString().toLong();
            partsFA.save(flush : true)
            ind++
        }
        render "ok"
    }

    def searchAndAddParts() {
        [fa: FA.read(params.fa.toLong())]
    }

    def searchAndAddJobs() {
        [fa: FA.read(params.fa.toLong())]
    }
}
