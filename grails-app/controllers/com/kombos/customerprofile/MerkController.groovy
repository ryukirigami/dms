package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class MerkController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Merk.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m064ID") {
                ilike("m064ID","%" + (params."sCriteria_m064ID" as String) + "%")
            }

            if (params."sCriteria_m064NamaMerk") {
                ilike("m064NamaMerk", "%" + (params."sCriteria_m064NamaMerk" as String) + "%")
            }

            ilike("staDel",'0')


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m064ID: it.m064ID,

                    m064NamaMerk: it.m064NamaMerk,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
    }

    def save() {
        def merkInstance = new Merk(params)
        merkInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        merkInstance?.lastUpdProcess = "INSERT"
        merkInstance?.setStaDel('0')
        merkInstance?.dateCreated = datatablesUtilService?.syncTime()
        merkInstance?.lastUpdated = datatablesUtilService?.syncTime()

        def cek = Merk.createCriteria().list() {
            and{
                eq("m064NamaMerk",merkInstance.m064NamaMerk?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [merkInstance: merkInstance])
            return
        }else{
            merkInstance?.m064ID = generateCodeService.codeGenerateSequence("M064_ID",null)
        }
        if (!merkInstance.save(flush: true)) {
            render(view: "create", model: [merkInstance: merkInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'merk.label', default: 'Merk'), merkInstance.m064ID])
        redirect(action: "show", id: merkInstance.id)
    }

    def show(Long id) {
        def merkInstance = Merk.get(id)
        if (!merkInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'merk.label', default: 'Merk'), id])
            redirect(action: "list")
            return
        }

        [merkInstance: merkInstance]
    }

    def edit(Long id) {
        def merkInstance = Merk.get(id)
        def idNew = merkInstance.m064ID
        if (!merkInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'merk.label', default: 'Merk'), id])
            redirect(action: "list")
            return
        }

        [merkInstance: merkInstance,idNew : idNew]
    }

    def update(Long id, Long version) {
        def merkInstance = Merk.get(id)
        if (!merkInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'merk.label', default: 'Merk'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (merkInstance.version > version) {

                merkInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'merk.label', default: 'Merk')] as Object[],
                        "Another user has updated this Merk while you were editing")
                render(view: "edit", model: [merkInstance: merkInstance])
                return
            }
        }

        def cek = Merk.createCriteria()
        def result = cek.list() {
            and{
                eq("m064NamaMerk",params.m064NamaMerk?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [merkInstance: merkInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        merkInstance.properties = params
        merkInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        merkInstance?.lastUpdProcess = "UPDATE"
        if (!merkInstance.save(flush: true)) {
            render(view: "edit", model: [merkInstance: merkInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'merk.label', default: 'Merk'), merkInstance.m064ID])
        redirect(action: "show", id: merkInstance.id)
    }

    def delete(Long id) {
        def merkInstance = Merk.get(id)
        if (!merkInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'merk.label', default: 'Merk'), id])
            redirect(action: "list")
            return
        }

        try {
            //merkInstance.delete(flush: true)
            merkInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            merkInstance?.lastUpdProcess = "DELETE"
            merkInstance?.setStaDel('1')
            merkInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'merk.label', default: 'Merk'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'merk.label', default: 'Merk'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Merk, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Merk, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
