package com.kombos.customerprofile

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.finance.MonthlyBalanceController
import com.kombos.maintable.*
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

class UploadDataSalesController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
    }

    def upload() {
        def historyCustomerInstance = null

        def vinCode = JSON.parse(params.vinCode)
        def company = JSON.parse(params.company)
        def gelarDepan = JSON.parse(params.gelarDepan)
        def namaDepan = JSON.parse(params.namaDepan)
        def namaBelakang = JSON.parse(params.namaBelakang)
        def gelarBelakang = JSON.parse(params.gelarBelakang)
        def jenisKelamin = JSON.parse(params.jenisKelamin)
        def tgglLahir = JSON.parse(params.tgglLahir)
        def noNpwp = JSON.parse(params.noNpwp)
        def noPajak = JSON.parse(params.noPajak)
//        def jenisIDCard = JSON.parse(params.jenisIDCard)
//        def nomorID = JSON.parse(params.nomorID)
//        def agama = JSON.parse(params.agama)
//        def staNikah = JSON.parse(params.staNikah)
        def noTelp = JSON.parse(params.noTelp)
        def noKantor = JSON.parse(params.noKantor)
        def noFax = JSON.parse(params.noFax)
        def noHP = JSON.parse(params.noHP)
        def email = JSON.parse(params.email)
        def alamat = JSON.parse(params.alamat)
//        def rt = JSON.parse(params.rt)
//        def rw = JSON.parse(params.rw)
//        def kelurahan = JSON.parse(params.kelurahan)
//        def kecamatan = JSON.parse(params.kecamatan)
//        def kabupaten = JSON.parse(params.kabupaten)
//        def provinsi = JSON.parse(params.provinsi)
//        def kodePos = JSON.parse(params.kodePos)
        def alamatNpwp = JSON.parse(params.alamatNpwp)
//        def rtNpwp = JSON.parse(params.rtNpwp)
//        def rwNpwp = JSON.parse(params.rwNpwp)
//        def kelurahanNpwp = JSON.parse(params.kelurahanNpwp)
//        def kecamatanNpwp = JSON.parse(params.kabupatenNpwp)
//        def kabupatenNpwp = JSON.parse(params.kabupatenNpwp)
//        def provinsiNpwp = JSON.parse(params.provinsiNpwp)
//        def kodePosNpwp = JSON.parse(params.kodePosNpwp)
        def namaStnk = JSON.parse(params.namaStnk)
        def alamatStnk = JSON.parse(params.alamatStnk)
        def tanggalStnk = JSON.parse(params.tanggalStnk)
        def nopolDepan = JSON.parse(params.nopolDepan)
        def nopolTengah = JSON.parse(params.nopolTengah)
        def nopolBelakang = JSON.parse(params.nopolBelakang)
        def kodeDealerPenjual = JSON.parse(params.kodeDealerPenjual)
        def kodeWarna = JSON.parse(params.kodeWarna)
        def fullModel = JSON.parse(params.fullModel)
        def tanggalDEC = JSON.parse(params.tanggalDEC)
        def tahunRakit = JSON.parse(params.tahunRakit)
        def bulanRakit = JSON.parse(params.bulanRakit)
        def noKunci = JSON.parse(params.noKunci)
//        def hobby = JSON.parse(params.hobby)
        def companyDealer = session.userCompanyDealer

        int ind = 0
        vinCode.each{
            def custVehicle = CustomerVehicle.findByT103VinCodeAndStaDel(it,"0");
            def cekCompany = Company.createCriteria().list {eq("staDel","0");eq("kodePerusahaan",company[ind],[ignoreCase: true])}
//            def jenisIC = JenisIdCard.createCriteria().list {eq("staDel","0");eq("m060ID",jenisIDCard[ind],[ignoreCase: true])}
//            def cAgama = Agama.createCriteria().list {eq("staDel","0");eq("m061ID",agama[ind],[ignoreCase: true])}
//            def cNikah = Nikah.createCriteria().list {eq("staDel","0");eq("m062ID",staNikah[ind],[ignoreCase: true])}
//            def cKelurahan = Kelurahan.createCriteria().list {eq("staDel","0");eq("m004ID",kelurahan[ind],[ignoreCase: true])}
//            def cKecamatan = Kecamatan.createCriteria().list {eq("staDel","0");eq("m003ID",kecamatan[ind],[ignoreCase: true])}
//            def cKabupaten = KabKota.createCriteria().list {eq("staDel","0");eq("m002ID",kabupaten[ind],[ignoreCase: true])}
//            def cProvinsi = Provinsi.createCriteria().list {eq("staDel","0");eq("m001ID",provinsi[ind],[ignoreCase: true])}
//            def cKelurahanNpwp = Kelurahan.createCriteria().list {eq("staDel","0");eq("m004ID",kelurahanNpwp[ind],[ignoreCase: true])}
//            def cKecamatanNpwp = Kecamatan.createCriteria().list {eq("staDel","0");eq("m003ID",kecamatanNpwp[ind],[ignoreCase: true])}
//            def cKabupatenNpwp = KabKota.createCriteria().list {eq("staDel","0");eq("m002ID",kabupatenNpwp[ind],[ignoreCase: true])}
//            def cProvinsiNpwp = Provinsi.createCriteria().list {eq("staDel","0");eq("m001ID",provinsiNpwp[ind],[ignoreCase: true])}
            def cNopolDepan = KodeKotaNoPol.createCriteria().list {eq("staDel","0");eq("m116ID",nopolDepan[ind],[ignoreCase: true])}
            def dealerPenjual = DealerPenjual.createCriteria().list {eq("staDel","0");eq("m091ID",kodeDealerPenjual[ind],[ignoreCase: true])}
            def warna = Warna.createCriteria().list {eq("staDel","0");eq("m092ID",kodeWarna[ind],[ignoreCase: true])}
            def cFullModel = FullModelCode.createCriteria().list {eq("staDel","0");eq("t110FullModelCode",fullModel[ind],[ignoreCase: true])}
//            def cHobby = Hobby.createCriteria().list {eq("staDel","0");if(hobby[ind].size()>0){inList("m063ID",hobby[ind],[ignoreCase: true])}}

            if(!custVehicle){
                custVehicle = new CustomerVehicle()
                custVehicle?.t103VinCode = it
                custVehicle?.companyDealer = companyDealer
                custVehicle?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
                custVehicle?.setLastUpdProcess("UPLOAD")
                custVehicle?.setStaDel("0")
                custVehicle?.save(flush: true)
                custVehicle?.errors?.each {println("Saving CV : "+it)}

                def histCV = new HistoryCustomerVehicle()
                histCV.companyDealer=companyDealer
                histCV.customerVehicle= custVehicle
                histCV.t183NamaSTNK= namaStnk[ind]
                histCV.t183AlamatSTNK= alamatStnk
                histCV.t183TglSTNK= new Date().parse("dd/MM/yyyy",tanggalStnk[ind])
                histCV.kodeKotaNoPol= cNopolDepan?.size()>0?cNopolDepan?.last():null
                histCV.t183NoPolTengah= nopolTengah[ind]
                histCV.t183NoPolBelakang= nopolBelakang[ind]
                histCV.dealerPenjual= dealerPenjual?.size()>0?dealerPenjual?.last():null
                histCV.warna= warna?.size()>0?warna?.last():null
                histCV.fullModelCode= cFullModel?.size()>0?cFullModel?.last():null
                histCV.t183TglDEC= new Date().parse("dd/MM/yyyy",tanggalDEC[ind])
                histCV.t183ThnBlnRakit= tahunRakit[ind].toString()+bulanRakit[ind].toString()
                histCV.t183NoKunci= noKunci[ind]
                histCV.t183TglEntry= datatablesUtilService?.syncTime() ? datatablesUtilService?.syncTime() : new Date()
                histCV.staDel= '0'
                histCV.createdBy= org.apache.shiro.SecurityUtils.subject.principal.toString()
                histCV.lastUpdProcess= "UPLOAD"
                histCV.save(flush: true)
                histCV?.errors?.each {println("Saving HCV : "+it)}

                def peran = PeranCustomer.get(4433 as long)

                def customer = new Customer(
                        t102ID : generateCodeService.codeGenerateSequence("T182_T102_ID",null),
                        companyDealer : companyDealer,
                        staDel: "0",
                        dateCreated: datatablesUtilService?.syncTime() ? datatablesUtilService?.syncTime() : new Date(),
                        lastUpdated: datatablesUtilService?.syncTime() ? datatablesUtilService?.syncTime() : new Date(),
                        peranCustomer: peran,
                        createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(), lastUpdProcess: "UPLOAD"
                ).save(flush:true)
                customer?.errors?.each {println("Saving C : "+it)}

                historyCustomerInstance = new HistoryCustomer();
                historyCustomerInstance.companyDealer = companyDealer
                historyCustomerInstance.customer = customer
                historyCustomerInstance.t182ID = customer?.id
                historyCustomerInstance.company=cekCompany?.size()>0?cekCompany?.last():null
                historyCustomerInstance.t182GelarD=gelarDepan[ind]
                historyCustomerInstance.t182NamaDepan=namaDepan[ind]
                historyCustomerInstance.t182NamaBelakang=namaBelakang[ind]
                historyCustomerInstance.t182GelarB=gelarBelakang[ind]
                historyCustomerInstance.t182JenisKelamin=jenisKelamin[ind].toString().equalsIgnoreCase("l")?"1":"2"
                historyCustomerInstance.t182TglLahir=new Date().parse("dd/MM/yyyy",tgglLahir[ind])
                historyCustomerInstance.jenisCustomer=cekCompany?.size()>0?"Corporate":"Personal"
                historyCustomerInstance.setPeranCustomer(peran)
                historyCustomerInstance.setT182StaTerimaMRS('1')
                historyCustomerInstance.t182NPWP=noNpwp[ind]
                historyCustomerInstance.t182NoFakturPajakStd=noPajak[ind]
//                historyCustomerInstance.jenisIdCard=jenisIC?.size()>0?jenisIC?.last():null
//                historyCustomerInstance.t182NomorIDCard=nomorID[ind]
//                historyCustomerInstance.agama=cAgama?.size()>0?cAgama.last():null
//                historyCustomerInstance.nikah=cNikah?.size()>0?cNikah?.last():null
                historyCustomerInstance.t182NoTelpRumah=noTelp[ind]
                historyCustomerInstance.t182NoTelpKantor=noKantor[ind]
                historyCustomerInstance.t182NoFax=noFax[ind]
                historyCustomerInstance.t182NoHp=noHP[ind]
                historyCustomerInstance.t182Email=email[ind]
                historyCustomerInstance.t182Alamat=alamat[ind]
//                historyCustomerInstance.t182RT=rt[ind]
//                historyCustomerInstance.t182RW=rw[ind]
//                historyCustomerInstance.kelurahan=cKelurahan?.size()>0?cKelurahan.last():null
//                historyCustomerInstance.kecamatan=cKecamatan?.size()>0?cKecamatan.last():null
//                historyCustomerInstance.kabKota=cKabupaten?.size()>0?cKabupaten.last():null
//                historyCustomerInstance.provinsi=cProvinsi?.size()>0?cProvinsi.last():null
//                historyCustomerInstance.t182KodePos=kodePos[ind]
                historyCustomerInstance.t182AlamatNPWP=alamatNpwp[ind]
//                historyCustomerInstance.t182RTNPWP=rtNpwp[ind]
//                historyCustomerInstance.t182RWNPWP=rwNpwp[ind]
//                historyCustomerInstance.kelurahan2=cKelurahanNpwp?.size()>0?cKelurahanNpwp.last():null
//                historyCustomerInstance.kecamatan2=cKecamatanNpwp?.size()>0?cKecamatanNpwp.last():null
//                historyCustomerInstance.kabKota2=cKabupatenNpwp?.size()>0?cKabupatenNpwp.last():null
//                historyCustomerInstance.provinsi2=cProvinsiNpwp?.size()>0?cProvinsiNpwp.last():null
//                historyCustomerInstance.t182KodePosNPWP=kodePosNpwp[ind]
//                historyCustomerInstance.hobbies=cHobby
                historyCustomerInstance.staDel="0"
                historyCustomerInstance.createdBy=org.apache.shiro.SecurityUtils.subject.principal.toString()
                historyCustomerInstance.lastUpdProcess="UPLOAD"
                historyCustomerInstance.dateCreated = datatablesUtilService?.syncTime()
                historyCustomerInstance.lastUpdated = datatablesUtilService?.syncTime()
                historyCustomerInstance.save(flush: true)
                historyCustomerInstance?.errors?.each {println("Saving HC : "+it)}
//                    }

                def mapping = new MappingCustVehicle()
                mapping?.companyDealer = companyDealer
                mapping?.customer = customer
                mapping?.customerVehicle = custVehicle
                mapping?.dateCreated = datatablesUtilService?.syncTime() ? datatablesUtilService?.syncTime() : new Date()
                mapping?.lastUpdated = datatablesUtilService?.syncTime() ? datatablesUtilService?.syncTime() : new Date()
                mapping?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mapping?.lastUpdProcess="UPLOAD"
                mapping?.save(flush: true)
                mapping?.errors?.each {println("Saving MAPPING : "+it)}

                int minSms = 8, minDm = 3, minTelp = 7
                def gp = GeneralParameter.findByCompanyDealerAndM000StaDel(session?.userCompanyDealer,"0")

                if(gp){
                    minSms = gp?.m000HMinusReminderSMS ? gp?.m000HMinusReminderSMS : minSms
                    minDm = gp?.m000HMinusTglReminderDM ? gp?.m000HMinusTglReminderDM?.toInteger() : minDm
                    minTelp = gp?.m000HMinusReminderSMS ? gp?.m000HMinusReminderSMS : minTelp
                }
                Date jatuhTempo = new Date().parse("dd/M/yyyy",histCV?.t183TglDEC?.format("dd/")+(histCV?.t183TglDEC?.format("M")?.toInteger()+1)+histCV?.t183TglDEC?.format("/yyyy"))
                def akhir = new MonthlyBalanceController().tanggalAkhir((histCV?.t183TglDEC?.format("M")?.toInteger()+1),(histCV?.t183TglDEC?.format("yyyy")?.toInteger()))
                if(akhir.toInteger()<histCV?.t183TglDEC?.format("dd")?.toInteger()){
                    jatuhTempo = new Date().parse("dd/M/yyyy",akhir+"/"+(histCV?.t183TglDEC?.format("M")?.toInteger()+1)+"/"+histCV?.t183TglDEC?.format("yyyy"))
                }
                def reminder = new Reminder()
                reminder.companyDealer = session.userCompanyDealer
                reminder.customerVehicle = custVehicle
                reminder.m201ID = JenisReminder.findByM201NamaJenisReminderIlike("%SBI%")
                reminder.m202ID = StatusReminder.findByM202NamaStatusReminderIlike("%early%")
                reminder.t201LastKM = 0
                reminder.t201LastServiceDate = histCV?.t183TglDEC
                reminder.t201TglJatuhTempo = jatuhTempo
                reminder.t201TglDM = jatuhTempo - minDm
                reminder.t201TglSMS = jatuhTempo - minSms
                reminder.t201TglEmail = jatuhTempo - minDm
                reminder.t201TglCall = jatuhTempo - minTelp
                reminder.staDel = "0"
                reminder.createdBy = "BATCH"
                reminder.lastUpdProcess = "INSERT"
                reminder.save(flush: true)
                reminder.errors.each {println it}
            }
            ind++
        }

        flash.message = message(code: 'default.uploadDataSales.message', default: "Save Data Sales Done")
        render(view: "index", model: [historyCustomerInstance: historyCustomerInstance])
    }

    def view() {
        def historyCustomerInstance = new HistoryCustomer(params)

        //handle upload file
        Map CONFIG_DATASALES_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'vinCode',
                        'B':'company',
                        'C':'gelarDepan',
                        'D':'namaDepan',
                        'E':'namaBelakang',
                        'F':'gelarBelakang',
                        'G':'jenisKelamin',
                        'H':'tgglLahir',
                        'I':'noNpwp',
                        'J':'noPajak',
//                        'K':'jenisIDCard',
//                        'L':'nomorID',
//                        'M':'agama',
//                        'N':'staNikah',
                        'K':'noTelp',
                        'L':'noKantor',
                        'M':'noFax',
                        'N':'noHP',
                        'O':'email',
                        'P':'alamat',
//                        'U':'rt',
//                        'V':'rw',
//                        'W':'kelurahan',
//                        'X':'kecamatan',
//                        'Y':'kabupaten',
//                        'Z':'provinsi',
//                        'AA':'kodePos',
                        'Q':'alamatNpwp',
//                        'AC':'rtNpwp',
//                        'AD':'rwNpwp',
//                        'AE':'kelurahanNpwp',
//                        'AF':'kecamatanNpwp',
//                        'AG':'kabupatenNpwp',
//                        'AH':'provinsiNpwp',
//                        'AI':'kodePosNpwp',
                        'R':'namaStnk',
                        'S':'alamatStnk',
                        'T':'tanggalStnk',
                        'U':'nopolDepan',
                        'V':'nopolTengah',
                        'W':'nopolBelakang',
                        'X':'kodeDealerPenjual',
                        'Y':'kodeWarna',
                        'Z':'fullModel',
                        'AA':'tanggalDEC',
                        'AB':'tahunRakit',
                        'AC':'bulanRakit',
                        'AD':'noKunci',
//                        'AW':'hobby',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [historyCustomerInstance: historyCustomerInstance])
                return
            }

            def arrVin = []
            def arrTemp = []

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_DATASALES_COLUMN_MAP)

            jsonData = jobList as JSON

            String status = "0", style = ""
            int ind = -1
            jobList?.each {
                ind++
                String strCompany = it?.company ? it?.company?.toString()?.trim():"", strVincode = it?.vinCode ? it.vinCode.toString().trim():"",
                    strGelarDepan = it?.gelarDepan ? it?.gelarDepan?.toString()?.trim() : "",strNamaDepan = it?.namaDepan ? it?.namaDepan?.toString()?.trim():"",
                    strNamaBelakang = it?.namaBelakang ? it?.namaBelakang?.toString()?.trim():"",strGelarBelakang = it?.gelarBelakang?it?.gelarBelakang?.toString()?.trim():"",
                    strJenisKelamin = it?.jenisKelamin?it?.jenisKelamin?.toString()?.trim():"",strTgglLahir = it?.tgglLahir?it?.tgglLahir?.toString()?.trim():"",
                    strNoNPWP = it?.noNpwp?it?.noNpwp?.toString()?.trim():"",strNoPajak = it?.noPajak?it?.noPajak?.toString()?.trim():"",
//                    strIDCard = it.jenisIDCard?it.jenisIDCard.toString().trim():"",strNoID = it.nomorID?it.nomorID.toString().trim():"",
//                    strAgama = it.agama?it.agama.toString().trim():"", strStaNikah = it.staNikah?it.staNikah.toString().trim():"",
                    strNoTelp = it.noTelp?it.noTelp.toString().trim():"",strNoKantor = it.noKantor?it.noKantor.toString().trim():"",
                    strNoFax = it.noFax?it.noFax.toString().trim():"", strNoHP = it.noHP?it.noHP.toString().trim():"",
                    strEmail = it.email?it.email.toString().trim():"",strAlamat = it.alamat?it.alamat.toString().trim():"",
//                    strRT = it.rt?it.rt.toString().trim():"", strRW = it.rw?it.rw.toString().trim():"",
//                    strKelurahan = it.kelurahan ? it.kelurahan.toString().trim().substring(0,it.kelurahan.toString().trim().indexOf(".")) : "", strKecamatan = it.kecamatan?it.kecamatan.toString().trim().substring(0,it.kecamatan.toString().trim().indexOf(".")):"",
//                    strKabupaten = it.kabupaten?it.kabupaten.toString().trim().substring(0,it.kabupaten.toString().trim().indexOf(".")):"", strProvinsi = it.provinsi?it.provinsi.toString().trim().substring(0,it.provinsi.toString().trim().indexOf(".")):"",
//                    strkodePos = it?.kodePos?it.kodePos.toString().trim().substring(0,it.kodePos.toString().trim().indexOf(".")):"",
                    strAlamatNpwp = it.alamatNpwp?it.alamatNpwp.toString().trim():"",
//                    strRTNpwp = it.rtNpwp?it.rtNpwp.toString().trim():"", strRWNpwp = it.rwNpwp?it.rwNpwp.toString().trim():"",
//                    strKelurahan2 = it.kelurahanNpwp?it.kelurahanNpwp.toString().trim().substring(0,it.kelurahanNpwp.toString().trim().indexOf(".")):"", strKecamatan2 = it.kecamatanNpwp?it.kecamatanNpwp.toString().trim().substring(0,it.kecamatanNpwp.toString().trim().indexOf(".")):"",
//                    strkodePosNpwp = it?.kodePosNpwp?it.kodePosNpwp.toString().trim().substring(0,it.kodePosNpwp.toString().trim().indexOf(".")):"",strKabupaten2 = it.kabupatenNpwp?it.kabupatenNpwp.toString().trim().substring(0,it.kabupatenNpwp.toString().trim().indexOf(".")):"",
//                    strProvinsi2 = it.provinsiNpwp?it.provinsiNpwp.toString().trim().substring(0,it.provinsiNpwp.toString().trim().indexOf(".")):"" ,
                    strNamaSTNK = it.namaStnk?it.namaStnk.toString().trim():"" ,
                    strAlamatSTNK = it.alamatStnk?it.alamatStnk.toString().trim():"" ,strTanggalSTNK = it.tanggalStnk?it.tanggalStnk.toString().trim():"" ,
                    strNopol = it.nopolDepan?it.nopolDepan.toString().trim():"",
                    strNopolTengah = it.nopolTengah?it.nopolTengah.toString().trim():"",
                    strNopolBelakang = it.nopolBelakang?it.nopolBelakang.toString().trim():"",  strDealer = it.kodeDealerPenjual?it.kodeDealerPenjual.toString().trim():"",
                    strWarna = it.kodeWarna?it.kodeWarna.toString().trim():"", strFullModel = it.fullModel?it.fullModel.toString().trim():"",
                    strTanggalDEC = it.tanggalDEC?it.tanggalDEC.toString().trim():"",
//                     strHobby = it.hobby?it.hobby.toString().trim():"",
                    strNoKunci = it.noKunci?it.noKunci.toString().trim():"",
                    strTahunRakit = it.tahunRakit ? (it.tahunRakit.toString().indexOf(".")>=0?it.tahunRakit.toString().substring(0,it.tahunRakit.toString().indexOf(".")).trim():it.tahunRakit.toString().trim()) :"",
                    strBlnRakit = it.bulanRakit ? (it.bulanRakit.toString().indexOf(".")>=0?it.bulanRakit.toString().substring(0,it.bulanRakit.toString().indexOf(".")).trim():it.bulanRakit.toString().trim()) :"";
                    if(strNopolTengah.concat(".")){
                        strNopolTengah = strNopolTengah.substring(0,strNopolTengah.length()-2)
                    }


                if((it.vinCode && it.vinCode!="") && (strNamaSTNK && strNamaSTNK!="")  && (strAlamatSTNK && strAlamatSTNK!="") && (strTanggalSTNK && strTanggalSTNK!="") &&
                    (it.tanggalDEC && it.tanggalDEC!="") && (strTahunRakit && strTahunRakit!="")&& (strBlnRakit && strBlnRakit!="")&& (strNoKunci && strNoKunci!="") &&
                    (strWarna && strWarna!="") && (strJenisKelamin && strJenisKelamin!="") && (strFullModel && strFullModel!="") &&
                    (it.nopolDepan && it.nopolDepan!="") && (it.kodeDealerPenjual && it.kodeDealerPenjual!="") &&
                    (it.fullModel && it.fullModel!="") && (it.namaDepan && it.namaDepan!="") && (strAlamatNpwp && strAlamatNpwp!="") &&
                    (it.alamat && it.alamat!="") && (it.noTelp && it.noTelp!="") && (it.noHP && it.noHP!="") && (strNopolTengah && strNopolTengah!="")
                    && (strNopolBelakang && strNopolBelakang!="")){
                    def companies = Company.createCriteria().list {eq("staDel","0");eq("kodePerusahaan",strCompany,[ignoreCase: true])}
//                    def jenisIC = JenisIdCard.createCriteria().list {eq("staDel","0");eq("m060ID",strIDCard,[ignoreCase: true])}
//                    def agamas = Agama.createCriteria().list {eq("staDel","0");eq("m061ID",strAgama,[ignoreCase: true])}
//                    def staNikahs = Nikah.createCriteria().list {eq("staDel","0");eq("m062ID",strStaNikah,[ignoreCase: true])}
//                    def kelurahans = Kelurahan.createCriteria().list {eq("staDel","0");eq("m004ID",strKelurahan?.trim(),[ignoreCase: true])}
//                    def kecamatans = Kecamatan.createCriteria().list {eq("staDel","0");eq("m003ID",strKecamatan,[ignoreCase: true])}
//                    def kabupatens = KabKota.createCriteria().list {eq("staDel","0");eq("m002ID",strKabupaten,[ignoreCase: true])}
//                    def provinsis = Provinsi.createCriteria().list {eq("staDel","0");eq("m001ID",strProvinsi,[ignoreCase: true])}
//                    def kelurahanNpwps = Kelurahan.createCriteria().list {eq("staDel","0");eq("m004ID",strKelurahan2,[ignoreCase: true])}
//                    def kecamatanNpwps = Kecamatan.createCriteria().list {eq("staDel","0");eq("m003ID",strKecamatan2,[ignoreCase: true])}
//                    def kabupatenNpwps = KabKota.createCriteria().list {eq("staDel","0");eq("m002ID",strKabupaten2,[ignoreCase: true])}
//                    def provinsiNpwps = Provinsi.createCriteria().list {eq("staDel","0");eq("m001ID",strProvinsi2,[ignoreCase: true])}
                    def nopolDepans = KodeKotaNoPol.createCriteria().list {eq("staDel","0");eq("m116ID",strNopol,[ignoreCase: true])}
                    def dealerPenjual = DealerPenjual.createCriteria().list {eq("staDel","0");eq("m091ID",strDealer,[ignoreCase: true])}
                    def warna = Warna.createCriteria().list {eq("staDel","0");eq("m092ID",strWarna,[ignoreCase: true])}
                    def fullModels = FullModelCode.createCriteria().list {eq("staDel","0");eq("t110FullModelCode",strFullModel,[ignoreCase: true])}
//                    def arrStrHobby = strHobby.split(",")
//                    def arrHobby = []
//                    arrStrHobby.each {
//                        def hobby = Hobby.createCriteria().list {eq("staDel","0");eq("m063ID",strHobby,[ignoreCase: true])}
//                        arrHobby << hobby.size()
//                    }
//                    def cek = Kelurahan.findByM004ID("1");
                    if(
                    (strNoTelp && !strNoTelp.isNumber()) || (strNoKunci && strNoKunci.length()>5 ) || (strTahunRakit && !strTahunRakit.isNumber())|| (strBlnRakit && !strBlnRakit.isNumber())
//                            || (strTahunRakit && strTahunRakit.length()>4)|| (strBlnRakit && strBlnRakit.length()>2)
//                            || (strCompany && strCompany!="" && companies.size()<1)
//                            || (strIDCard && strIDCard!="" && jenisIC.size()<1)
//                            || (strAgama && strAgama!="" && agamas.size()<1)
//                            || (strStaNikah && strStaNikah!="" && staNikahs.size()<1)
//                            || (strKelurahan && strKelurahan!="" && kelurahans.size()<1)
//                            || (strKecamatan && strKecamatan!="" && kecamatans.size()<1)
//                            ||(strKabupaten && strKabupaten!="" && kabupatens.size()<1)
//                            ||(strProvinsi && strProvinsi!="" && provinsis.size()<1)
//                            || (strKelurahan2 && strKelurahan2!="" && kelurahanNpwps.size()<1)
//                            ||(strKecamatan2 && strKecamatan2!="" && kecamatanNpwps.size()<1)
//                            ||(strKabupaten2 && strKabupaten2!="" && kabupatenNpwps.size()<1)
//                            || (strProvinsi2 && strProvinsi2!="" && provinsiNpwps.size()<1)
//                            ||(strNopol && strNopol!="" && nopolDepans.size()<1)
//                            ||(strDealer && strDealer!="" && dealerPenjual.size()<1)
//                            || (strWarna && strWarna!="" && warna.size()<1)
//                            ||(strFullModel && strFullModel!="" && fullModels.size()<1)
                    ){
                        jmlhDataError++;
                        status = "1";
                    }
//                    else{
//                        for(int a=0;a<arrStrHobby.size();a++){
//                            if(arrStrHobby[a] && arrStrHobby[a]!="" && arrHobby[a].toString().toInteger()<1){
//                                jmlhDataError++;
//                                status = "1";
//                                break;
//                            }
//                        }
//                    }
                } else {
                    jmlhDataError++;
                    status = "1";

                }

                arrVin << strVincode
                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                <input type='checkbox' class='pull-left row-select' name='cekSub' title='Select this'><input type='hidden' id='id"+ind+"' value='"+strVincode+"'/>&nbsp;&nbsp;"+strVincode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='company"+ind+"' value='"+strCompany+"'/> "+strCompany+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='gelarDepan"+ind+"' value='"+strGelarDepan+"'/> "+strGelarDepan+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='namaDepan"+ind+"' value='"+strNamaDepan+"' /> "+strNamaDepan+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='namaBelakang"+ind+"' value='"+strNamaBelakang+"'/>"+strNamaBelakang+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='gelarBelakang"+ind+"' value='"+strGelarBelakang+"'/> "+strGelarBelakang+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='jenisKelamin"+ind+"' value='"+strJenisKelamin+"'/>"+strJenisKelamin+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='tgglLahir"+ind+"' value='"+strTgglLahir+"'/> "+strTgglLahir+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='noNpwp"+ind+"' value='"+strNoNPWP+"'/> "+strNoNPWP+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='noPajak"+ind+"' value='"+strNoPajak+"'/> "+strNoPajak+"\n" +
                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='jenisIDCard"+ind+"' value='"+strIDCard+"'/> "+strIDCard+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='nomorID"+ind+"' value='"+strNoID+"' /> "+strNoID+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='agama"+ind+"' value='"+strAgama+"'/>"+strAgama+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='staNikah"+ind+"' value='"+strStaNikah+"'/> "+strStaNikah+"\n" +
//                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='noTelp"+ind+"' value='"+strNoTelp+"'/>"+strNoTelp+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='noKantor"+ind+"' value='"+strNoKantor+"'/> "+strNoKantor+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='noFax"+ind+"' value='"+strNoFax+"'/> "+strNoFax+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='noHP"+ind+"' value='"+strNoHP+"'/> "+strNoHP+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='email"+ind+"' value='"+strEmail+"'/> "+strEmail+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='alamat"+ind+"' value='"+strAlamat+"' /> "+strAlamat+"\n" +
                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='rt"+ind+"' value='"+strRT+"'/>"+strRT+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='rw"+ind+"' value='"+strRW+"'/> "+strRW+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kelurahan"+ind+"' value='"+strKelurahan+"'/>"+strKelurahan+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kecamatan"+ind+"' value='"+strKecamatan+"'/> "+strKecamatan+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kabupaten"+ind+"' value='"+strKabupaten+"'/> "+strKabupaten+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='provinsi"+ind+"' value='"+strProvinsi+"'/> "+strProvinsi+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kodePos"+ind+"' value='"+strkodePos+"'/> "+strkodePos+"\n" +
//                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='alamatNpwp"+ind+"' value='"+strAlamatNpwp+"' /> "+strAlamatNpwp+"\n" +
                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='rtNpwp"+ind+"' value='"+strRTNpwp+"'/>"+strRTNpwp+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='rwNpwp"+ind+"' value='"+strRWNpwp+"'/> "+strRWNpwp+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kelurahanNpwp"+ind+"' value='"+strKelurahan2+"'/>"+strKelurahan2+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kecamatanNpwp"+ind+"' value='"+strKecamatan2+"'/> "+strKecamatan2+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kabupatenNpwp"+ind+"' value='"+strKabupaten2+"'/> "+strKabupaten2+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='provinsiNpwp"+ind+"' value='"+strProvinsi2+"'/> "+strProvinsi2+"\n" +
//                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='kodePosNpwp"+ind+"' value='"+strkodePosNpwp+"'/> "+strkodePosNpwp+"\n" +
//                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='namaStnk"+ind+"' value='"+strNamaSTNK+"' /> "+strNamaSTNK+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='alamatStnk"+ind+"' value='"+strAlamatSTNK+"'/>"+strAlamatSTNK+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='tanggalStnk"+ind+"' value='"+strTanggalSTNK+"'/> "+strTanggalSTNK+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='nopolDepan"+ind+"' value='"+strNopol+"'/>"+strNopol+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='nopolTengah"+ind+"' value='"+strNopolTengah+"'/> "+strNopolTengah+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='nopolBelakang"+ind+"' value='"+strNopolBelakang+"'/> "+strNopolBelakang+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='kodeDealerPenjual"+ind+"' value='"+strDealer+"'/> "+strDealer+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='kodeWarna"+ind+"' value='"+strWarna+"'/> "+strWarna+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='fullModel"+ind+"' value='"+strFullModel+"' /> "+strFullModel+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='tanggalDEC"+ind+"' value='"+strTanggalDEC+"'/>"+strTanggalDEC+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='tahunRakit"+ind+"' value='"+strTahunRakit+"'/> "+strTahunRakit+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='bulanRakit"+ind+"' value='"+strBlnRakit+"'/>"+strBlnRakit+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='noKunci"+ind+"' value='"+strNoKunci+"'/> "+strNoKunci+"\n" +
                        "                            </td>\n" +
//                        "                            <td>\n" +
//                        "                                <input type='hidden' id='hobby"+ind+"' value='"+strHobby+"'/> "+strHobby+"\n" +
//                        "                            </td>\n" +
                        "                        </tr>"

                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadDataSalesError.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadDataSales.message', default: "Read File Done")
        }

        render(view: "index", model: [historyCustomerInstance: historyCustomerInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }

}
