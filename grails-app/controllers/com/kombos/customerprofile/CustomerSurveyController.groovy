package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class CustomerSurveyController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList', 'listDetail', 'datatablesListDetail']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = CustomerSurvey.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_t104Id") {
                eq("t104Id", new Integer(params."sCriteria_t104Id"))
            }

//            if (params."sCriteria_jenisSurvey") {
//                jenisSurvey{
//                    ilike("m131JenisSurvey","%"+ (params."sCriteria_jenisSurvey") + "%")
//                }
//            }


            if (params."sCriteria_t104TglAwal") {
                ge("t104TglAwal", params."sCriteria_t104TglAwal")
                lt("t104TglAwal", params."sCriteria_t104TglAwal" + 1)
            }

            if (params."sCriteria_t104TglAkhir") {
                ge("t104TglAkhir", params."sCriteria_t104TglAkhir")
                lt("t104TglAkhir", params."sCriteria_t104TglAkhir" + 1)
            }

            if (params."sCriteria_t104Text") {
                ilike("t104Text", "%" + (params."sCriteria_t104Text" as String) + "%")
            }

            if (params."sCriteria_t104xNamaUser") {
                ilike("t104xNamaUser", "%" + (params."sCriteria_t104xNamaUser" as String) + "%")
            }

            if (params."sCriteria_t104xNamaDivisi") {
                ilike("t104xNamaDivisi", "%" + (params."sCriteria_t104xNamaDivisi" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t104Id: it.t104Id,

//                    jenisSurvey: it.jenisSurvey?.m131JenisSurvey,

//                    customer: it.customer!=null?it.customer.t102ID:"",

//                    historyCustomer: it.historyCustomer,

                    t104TglAwal: it.t104TglAwal ? it.t104TglAwal.format(dateFormat) : "",

                    t104TglAkhir: it.t104TglAkhir ? it.t104TglAkhir.format(dateFormat) : "",

                    t104Text: it.t104Text,

                    t104xNamaUser: it.t104xNamaUser,

                    t104xNamaDivisi: it.t104xNamaDivisi,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

//                    customerVehicles: it.customerVehicles,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def datatablesListDetail(){
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params


        def c = HistoryCustomerVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")
            customerVehicle{
                order("t103VinCode")
            }
        }

        def rows = []
        def tampil = false
        results.each {

                if(it == it?.customerVehicle?.getCurrentCondition()){
                            rows << [

                                id: it?.customerVehicle?.id,

                                t103VinCode: it?.customerVehicle?.t103VinCode,

                                companyDealer: it.companyDealer==null?"":it.companyDealer.m011NamaWorkshop,

                                fullModelCode: it?.fullModelCode==null?"":it?.fullModelCode?.t110FullModelCode,

                                model: it?.fullModelCode==null?"":it?.fullModelCode?.baseModel?.m102NamaBaseModel,

                                tanggalDEC: it?.t183TglDEC==null?"":it?.t183TglDEC?.format("dd/MM/yyyy"),

                                nomorMesin: it?.t183NoMesin==null?"":it?.t183NoMesin,

                                warna: it?.warna==null?"":it.warna?.m092NamaWarna,

                                nomorKunci: it?.t183NoKunci==null?"":it?.t183NoKunci,

                                tanggalSTNK: it?.t183TglSTNK==null?"":it?.t183TglSTNK,

                                nomorPolisi: it?.fullNoPol==null?"":(it.fullNoPol),
                        ]
//                    }
              }
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [customerSurveyInstance: new CustomerSurvey(params)]
    }

    def save() {
        params.staDel = '0'
        params.lastUpdProcess = 'INSERT'
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        def customerSurveyInstance = new CustomerSurvey(params)
        if (!customerSurveyInstance.save(flush: true)) {
            render(view: "create", model: [customerSurveyInstance: customerSurveyInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), customerSurveyInstance.id])
        redirect(action: "show", id: customerSurveyInstance.id)
    }

    def show(Long id) {
        def customerSurveyInstance = CustomerSurvey.get(id)
        if (!customerSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), id])
            redirect(action: "list")
            return
        }

        [customerSurveyInstance: customerSurveyInstance]
    }

    def edit(Long id) {
        def customerSurveyInstance = CustomerSurvey.get(id)
        if (!customerSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), id])
            redirect(action: "list")
            return
        }

        [customerSurveyInstance: customerSurveyInstance]
    }

    def update(Long id, Long version) {
        def customerSurveyInstance = CustomerSurvey.get(id)
        if (!customerSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (customerSurveyInstance.version > version) {

                customerSurveyInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'customerSurvey.label', default: 'CustomerSurvey')] as Object[],
                        "Another user has updated this CustomerSurvey while you were editing")
                render(view: "edit", model: [customerSurveyInstance: customerSurveyInstance])
                return
            }
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        customerSurveyInstance.properties = params

        if (!customerSurveyInstance.save(flush: true)) {
            render(view: "edit", model: [customerSurveyInstance: customerSurveyInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), customerSurveyInstance.id])
        redirect(action: "show", id: customerSurveyInstance.id)
    }

    def delete(Long id) {
        def customerSurveyInstance = CustomerSurvey.get(id)
        if (!customerSurveyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), id])
            redirect(action: "list")
            return
        }

        try {
            customerSurveyInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'customerSurvey.label', default: 'CustomerSurvey'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(CustomerSurvey, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(CustomerSurvey, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def listDetail(){
    }

    def getCustomerSurveyInfo(){
        def res = [:]
        def dataId = params.customerSurvey
        def data = null
        if(dataId =="null" || dataId==null || dataId==""){

        }else{
            data = CustomerSurvey.findById(dataId as Long)
        }
        if(data){
            res.tglAwal = data.t104TglAwal.format("dd-MM-yyyy")
            res.tglAkhir = data.t104TglAkhir.format("dd-MM-yyyy")
        }else{
            res.tglAwal = ' '
            res.tglAkhir = ' '
        }

        render res as JSON
    }
    def setJDPower(){
        def res = [:]
        int suksesSave = 0, gagalSave = 0
        def jsonArray = JSON.parse(params.ids)
        jsonArray.each{
            def cs = CustomerSurvey.findById(params.id as Long)
            def cv = CustomerVehicle.findById(it as Long)
            def dataFound = CustomerSurveyDetail.findByCustomerSurveyAndCustomerVehicle(cs,cv)
            if(dataFound){
                gagalSave++
            }else{
                def csd = new CustomerSurveyDetail()
                csd.customerSurvey = cs
                csd.customerVehicle = cv
                csd.staDel = '0'
                csd.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                csd.lastUpdProcess = 'INSERT'
                csd.save(flush: true)
                suksesSave++
            }
        }

        res.suksesSave = suksesSave
        res.gagalSave = gagalSave
        render res as JSON
    }

}
