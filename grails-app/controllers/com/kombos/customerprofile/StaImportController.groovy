package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class StaImportController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = StaImport.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")

            if (params."sCriteria_m100ID") {
                ilike("m100ID", "%" + (params."sCriteria_m100ID" as String) + "%")
            }

            if (params."sCriteria_m100KodeStaImport") {
                ilike("m100KodeStaImport", "%" + (params."sCriteria_m100KodeStaImport" as String) + "%")
            }

            if (params."sCriteria_m100NamaStaImport") {
                ilike("m100NamaStaImport", "%" + (params."sCriteria_m100NamaStaImport" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m100ID: it.m100ID,

                    m100KodeStaImport: it.m100KodeStaImport,

                    m100NamaStaImport: it.m100NamaStaImport,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [staImportInstance: new StaImport(params)]
    }

    def save() {
        def staImportInstance = new StaImport(params)

        staImportInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        staImportInstance?.lastUpdProcess = "INSERT"
        staImportInstance?.dateCreated = datatablesUtilService?.syncTime()
        staImportInstance?.lastUpdated = datatablesUtilService?.syncTime()
        staImportInstance?.setStaDel('0')
        def cek1 = StaImport.createCriteria().list() {
            and{
                eq("m100KodeStaImport",staImportInstance.m100KodeStaImport?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek1){
            flash.message = "Kode Status Import sudah ada"
            render(view: "create", model: [staImportInstance: staImportInstance])
            return
        }
        def cek2 = StaImport.createCriteria().list() {
            and{
                eq("m100NamaStaImport",staImportInstance.m100NamaStaImport?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek2){
            flash.message = "Nama Status Import sudah ada"
            render(view: "create", model: [staImportInstance: staImportInstance])
            return
        }
        staImportInstance?.m100ID = generateCodeService.codeGenerateSequence("M100_ID",null)
        if (!staImportInstance.save(flush: true)) {
            render(view: "create", model: [staImportInstance: staImportInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'staImport.label', default: 'StaImport'), staImportInstance.id])
        redirect(action: "show", id: staImportInstance.id)
    }

    def show(Long id) {
        def staImportInstance = StaImport.get(id)
        if (!staImportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'staImport.label', default: 'StaImport'), id])
            redirect(action: "list")
            return
        }

        [staImportInstance: staImportInstance]
    }

    def edit(Long id) {
        def staImportInstance = StaImport.get(id)
        if (!staImportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'staImport.label', default: 'StaImport'), id])
            redirect(action: "list")
            return
        }

        [staImportInstance: staImportInstance]
    }

    def update(Long id, Long version) {
        def staImportInstance = StaImport.get(id)
        if (!staImportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'staImport.label', default: 'StaImport'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (staImportInstance.version > version) {

                staImportInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'staImport.label', default: 'StaImport')] as Object[],
                        "Another user has updated this StaImport while you were editing")
                render(view: "edit", model: [staImportInstance: staImportInstance])
                return
            }
        }

        def cek1 = StaImport.createCriteria()
        def result1 = cek1.list() {
            and{
                eq("m100KodeStaImport",params.m100KodeStaImport?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result1){
            for(find in result1){
                if(id!=find.id){
                    flash.message = "Kode Status Import sudah ada"
                    render(view: "edit", model: [staImportInstance: staImportInstance])
                    return
                }
            }
        }
        def cek2 = StaImport.createCriteria()
        def result2 = cek2.list() {
            and{
                eq("m100NamaStaImport",params.m100NamaStaImport?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result2){
            for(find in result2){
                if(id!=find.id){
                    flash.message = "Kode Status Import sudah ada"
                    render(view: "edit", model: [staImportInstance: staImportInstance])
                    return
                }
            }
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        staImportInstance.properties = params
        staImportInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        staImportInstance?.lastUpdProcess = "UPDATE"

        if (!staImportInstance.save(flush: true)) {
            render(view: "edit", model: [staImportInstance: staImportInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'staImport.label', default: 'StaImport'), staImportInstance.id])
        redirect(action: "show", id: staImportInstance.id)
    }

    def delete(Long id) {
        def staImportInstance = StaImport.get(id)
        if (!staImportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'staImport.label', default: 'StaImport'), id])
            redirect(action: "list")
            return
        }

        try {
            staImportInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            staImportInstance?.lastUpdProcess = "DELETE"
            staImportInstance?.setStaDel('1')
            staImportInstance.save(flush:true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'staImport.label', default: 'StaImport'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'staImport.label', default: 'StaImport'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(StaImport, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(StaImport, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
