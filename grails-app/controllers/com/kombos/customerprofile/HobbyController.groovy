package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class HobbyController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

   	def datatablesUtilService

    def hobbyService

    def generateCodeService

   	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

   	static viewPermissions = ['index', 'list', 'datatablesList']

   	static addPermissions = ['create', 'save']

   	static editPermissions = ['edit', 'update']

   	static deletePermissions = ['delete']

   	def index() {
   		redirect(action: "list", params: params)
   	}

   	def list(Integer max) {
   	}

   	def datatablesList() {
   		render hobbyService.datatablesList(params) as JSON
   	}

   	def create() {
   		[hobbyInstance: new Hobby(params)]
   	}

   	def save() {
   		def hobbyInstance = new Hobby(params)
        hobbyInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        hobbyInstance.setLastUpdProcess("INSERT")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
        hobbyInstance.setStaDel("0") // untuk data awal di set 0 karena bukan data yang dihapus
        hobbyInstance?.dateCreated = datatablesUtilService?.syncTime()
        hobbyInstance?.lastUpdated = datatablesUtilService?.syncTime()
        def cek = Hobby.createCriteria().list() {
            and{
                eq("m063NamaHobby",hobbyInstance.m063NamaHobby?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            hobbyInstance.m063ID = null
            render(view: "create", model: [hobbyInstance: hobbyInstance])
            return
        }
        hobbyInstance?.m063ID = generateCodeService.codeGenerateSequence("M063_ID",null)
        if (!hobbyInstance.save(flush: true)) {
   			render(view: "create", model: [hobbyInstance: hobbyInstance])
   			return
   		}

   		flash.message = message(code: 'default.created.message', args: [message(code: 'hobby.label', default: 'Hobby'), hobbyInstance.id])
   		redirect(action: "show", id: hobbyInstance.id)
   	}

   	def show(Long id) {
   		def hobbyInstance = Hobby.get(id)
   		if (!hobbyInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'hobby.label', default: 'Hobby'), id])
   			redirect(action: "list")
   			return
   		}

   		[hobbyInstance: hobbyInstance]
   	}

   	def edit(Long id) {
   		def hobbyInstance = Hobby.get(id)
   		if (!hobbyInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'hobby.label', default: 'Hobby'), id])
   			redirect(action: "list")
   			return
   		}

   		[hobbyInstance: hobbyInstance]
   	}

   	def update(Long id, Long version) {
   		def hobbyInstance = Hobby.get(id)
//   		if (!hobbyInstance) {
//   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'hobby.label', default: 'Hobby'), id])
//   			redirect(action: "list")
//   			return
//   		}
        String original = hobbyInstance.m063NamaHobby
        hobbyInstance.m063NamaHobby = params.m063NamaHobby
   		if (version != null) {
   			if (hobbyInstance.version > version) {

   				hobbyInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
   				[message(code: 'hobbby.label', default: 'Hobby')] as Object[],
   				"Another user has updated this Hobby while you were editing")
   				render(view: "edit", model: [hobbyInstance: hobbyInstance])
   				return
   			}
   		}
        def cek = Hobby.createCriteria()
        def result = cek.list() {
            and{
                eq("m063NamaHobby",hobbyInstance.m063NamaHobby?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            boolean isExist = false;
            for(Hobby hobby : result) {
                if (hobby.m063ID != hobbyInstance.m063ID) {
                    isExist = true
                }
            }
            if (isExist) {
                flash.message = "Hobby " + hobbyInstance.m063NamaHobby + " Sudah Ada"
                hobbyInstance.m063NamaHobby = original
                render(view: "edit", model: [hobbyInstance: hobbyInstance])
                return
            }
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
   	    hobbyInstance.properties = params
        hobbyInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
        hobbyInstance.setLastUpdProcess("UPDATE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"

           if (!hobbyInstance.save(flush: true)) {
   			render(view: "edit", model: [hobbyInstance: hobbyInstance])
   			return
   		}

   		flash.message = message(code: 'default.updated.message', args: [message(code: 'hobby.label', default: 'Hobby'), hobbyInstance.id])
   		redirect(action: "show", id: hobbyInstance.id)
   	}

   	def delete(Long id) {
   		def hobbyInstance = Hobby.get(id)
   		if (!hobbyInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'hobby.label', default: 'Hobby'), id])
   			redirect(action: "list")
   			return
   		}

   		try {
               hobbyInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString()) // yang wajib di set
               hobbyInstance.setLastUpdProcess("DELETE")// yang wajib di set, ada 3 option yaitu "INSERT", "UPDATE" , dan "DELETE"
               hobbyInstance.setStaDel("0") // untuk data awal di set 0 karena bukan data yang dihapus
               hobbyInstance.save(flush: true)
               flash.message = message(code: 'default.deleted.message', args: [message(code: 'hobby.label', default: 'Hobby'), id])
   			redirect(action: "list")
   		}
   		catch (DataIntegrityViolationException e) {
   			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'hobby.label', default: 'Hobby'), id])
   			redirect(action: "show", id: id)
   		}
   	}

   	def massdelete() {
   		def res = [:]
   		try {
   			datatablesUtilService.massDeleteStaDelNew(Hobby, params)
               // ada 2 function yang bisa digunakan
               // yaitu massDelete dan massDeleteStaDelNew
               // massDelete khusus untuk domain2 yang tidak ada field staDel
               // massDeleteStaDelNew khusus untuk domain2 yang ada field staDel
   			res.message = "Mass Delete Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}

   	def updatefield(){
   		def res = [:]
   		try {
   			datatablesUtilService.updateField(Hobbby, params)
   			res.message = "Update Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}
}