package com.kombos.customerprofile

import com.kombos.maintable.VehicleFA
import grails.converters.JSON
import org.apache.shiro.SecurityUtils

class AddDetailFieldActionController {
def datatablesUtilService

    def index() {}


    def doSave() {
        def fa = FA.read(params.fa)
        def jsonArray = JSON.parse(params.ids)
        def oList = []
        jsonArray.each { oList << HistoryCustomerVehicle.get(it?.toString()) }

        oList.each {
            def vehicleFa = VehicleFA.findByFaAndHistoryCustomerVehicleAndCustomerVehicle(fa, it , it.customerVehicle) ?:
                    new VehicleFA(
                            fa: fa,
                            customerVehicle: it?.customerVehicle,
                            historyCustomerVehicle: it,
                            lastUpdated: datatablesUtilService?.syncTime(), createdBy: SecurityUtils.subject.principal.toString(), updatedBy: SecurityUtils.subject.principal.toString(), lastUpdProcess: "INSERT",
                            dateCreated: datatablesUtilService?.syncTime()
                    )
            vehicleFa.companyDealer = it?.customerVehicle?.companyDealer
            vehicleFa.t185Dealer = it?.dealerPenjual?.m091NamaDealer
            vehicleFa.save(flush: true)
            println "vehicleFa.errors = $vehicleFa.errors"

        }
        render "ok"
    }

    def findList(){
        def html="<option value='"+0+"'> Pilih FA</option>"
        def c = FA.createCriteria().list {
            order("m185NamaFA");
        }
        c.each {
            html+="<option value=\'"+it.id+"\'> "+it.m185NamaFA+"</option>"
        }
        render html
    }

    def datatablesList() {
        String dateFormat = "dd/MM/yyyy"

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params


        def c = HistoryCustomerVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if (params."sCriteria_nopol") {
                ilike("fullNoPol","%"+params."sCriteria_nopol"+"%");
            }

            if (params."sCriteria_customerVehicle") {
                customerVehicle {
                    ilike("t103VinCode", "%" + (params."sCriteria_customerVehicle" as String) + "%")
                }
            }

            if (params."sCriteria_fullModelCode") {
                fullModelCode {
                    ilike("t110FullModelCode", "%" + (params."sCriteria_fullModelCode" as String) + "%")
                }
            }

            if (params."sCriteria_baseModel") {
                fullModelCode {
                    baseModel {
                        ilike("m102NamaBaseModel", "%" + (params."sCriteria_baseModel" as String) + "%")
                    }
                }
            }


            if (params."sCriteria_t183TglDEC") {
                ge("t183TglDEC", params."sCriteria_t183TglDEC")
                lt("t183TglDEC", params."sCriteria_t183TglDEC" + 1)
            }

            if (params."sCriteria_t183NoMesin") {
                ilike("t183NoMesin", "%" + (params."sCriteria_t183NoMesin" as String) + "%")
            }

            if (params."sCriteria_t183Warna") {
                warna {
                    ilike("m092NamaWarna", "%" + (params."sCriteria_t183Warna" as String) + "%")
                }
            }

            if (params."sCriteria_t183NoKunci") {
                ilike("t183NoKunci", "%" + (params."sCriteria_t183NoKunci" as String) + "%")
            }


            if (params."sCriteria_t183TglSTNK") {
                ge("t183TglSTNK", params."sCriteria_t183TglSTNK")
                lt("t183TglSTNK", params."sCriteria_t183TglSTNK" + 1)
            }

            if (params."sCriteria_t183ThnBlnRakit") {
                ilike("t183ThnBlnRakit", "%" + (params."sCriteria_t183ThnBlnRakit" as String) + "%")
            }

            eq("staDel", "0")
            switch(sortProperty){
                case "customerVehicle":
                    customerVehicle{
                        order("t103VinCode",sortDir)
                    }
                    break;
                case "fullModelCode":
                    fullModelCode{
                        order("t110FullModelCode",sortDir)
                    }
                    break;
                case "baseModel":
                    fullModelCode{
                        baseModel{
                            order("m102NamaBaseModel",sortDir)
                        }
                    }
                    break;
                case "warna":
                    warna{
                        order("m092NamaWarna",sortDir)
                    }
                    break;
                default:
                    order(sortProperty,sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
//            if(it == it?.customerVehicle?.getCurrentCondition()){
                rows << [

                        id: it.id,

                        nopol: it.fullNoPol,

                        customerVehicle: it.customerVehicle?.t103VinCode,

                        fullModelCode: it.fullModelCode?.t110FullModelCode,

                        baseModel: it.fullModelCode?.baseModel?.m102NamaBaseModel,

                        t183TglDEC: it.t183TglDEC ? it.t183TglDEC.format(dateFormat) : "",

                        t183NoMesin: it.t183NoMesin,

                        warna: it.warna?.m092NamaWarna,

                        t183NoKunci: it.t183NoKunci,

                        t183TglSTNK: it.t183TglSTNK ? it.t183TglSTNK.format(dateFormat) : "",

                        t183ThnBlnRakit: it.t183ThnBlnRakit
                ]
//            }
        }

        def ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }


    def vehicleListDatatablesList() {

        def c = HistoryCustomerVehicle.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

        }

        def rows = []

        results.each {
            rows << [
                    id: it.id,
                    nopol: it.kodeKotaNoPol?.m116ID + " " + it.t183NoPolTengah + " " + it.t183NoPolBelakang,
                    vinCode: it.customerVehicle?.t103VinCode,
                    fullModel: it.fullModelCode?.t110FullModelCode,
                    model: it.fullModelCode?.baseModel?.m102NamaBaseModel,
                    tanggalDEC: it.t183TglDEC ? it.t183TglDEC.format(dateFormat) : "",
                    nomorMesin: it.t183NoMesin,
                    warna: it.warna?.m092NamaWarna,
                    nomorKunci: it.t183NoKunci,
                    tanggalSTNK: it.t183TglSTNK ? it.t183TglSTNK.format(dateFormat) : "",
                    tahunDanBulanRakit: it.t183ThnBlnRakit
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

}
