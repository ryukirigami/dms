package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SPKAsuransiController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = SPKAsuransi.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("companyDealer",session?.userCompanyDealer);
            eq("staDel","0");
            if (params."sCriteria_t193IDAsuransi") {
                ilike("t193IDAsuransi", "%" + (params."sCriteria_t193IDAsuransi" as String) + "%")
            }

            if (params."sCriteria_vendorAsuransi") {
                vendorAsuransi{
                    ilike("m193Nama","%"+params."sCriteria_vendorAsuransi"+"%")
                }
            }

            if (params."sCriteria_customerVehicle") {
                customerVehicle{
                    ilike("t103VinCode","%"+params."sCriteria_customerVehicle"+"%")
                }
            }

            if (params."sCriteria_t193ID") {
                ilike("t193ID", "%" + (params."sCriteria_t193ID" as String) + "%")
            }

            if (params."sCriteria_t193StaNomorSPK") {
                ilike("t193StaNomorSPK", "%" + (params."sCriteria_t193StaNomorSPK" as String) + "%")
            }

            if (params."sCriteria_t193NomorSPK") {
                ilike("t193NomorSPK", "%" + (params."sCriteria_t193NomorSPK" as String) + "%")
            }

            if (params."sCriteria_t193TanggalSPK") {
                ge("t193TanggalSPK", params."sCriteria_t193TanggalSPK")
                lt("t193TanggalSPK", params."sCriteria_t193TanggalSPK" + 1)
            }

            if (params."sCriteria_t193NomorPolis") {
                ilike("t193NomorPolis", "%" + (params."sCriteria_t193NomorPolis" as String) + "%")
            }

            if (params."sCriteria_t193NamaPolis") {
                ilike("t193NamaPolis", "%" + (params."sCriteria_t193NamaPolis" as String) + "%")
            }

            if (params."sCriteria_t193AlamatPolis") {
                ilike("t193AlamatPolis", "%" + (params."sCriteria_t193AlamatPolis" as String) + "%")
            }

            if (params."sCriteria_t193TelpPolis") {
                ilike("t193TelpPolis", "%" + (params."sCriteria_t193TelpPolis" as String) + "%")
            }

            if (params."sCriteria_t193TglPolis") {
                ge("t193TglPolis", params."sCriteria_t193TglPolis")
                lt("t193TglPolis", params."sCriteria_t193TglPolis" + 1)
            }

            if (params."sCriteria_t193JmlPolis") {
                eq("t193JmlPolis", params."sCriteria_t193JmlPolis")
            }

            if (params."sCriteria_t193TglAwal") {
                ge("t193TglAwal", params."sCriteria_t193TglAwal")
                lt("t193TglAwal", params."sCriteria_t193TglAwal" + 1)
            }

            if (params."sCriteria_t193TglAkhir") {
                ge("t193TglAkhir", params."sCriteria_t193TglAkhir")
                lt("t193TglAkhir", params."sCriteria_t193TglAkhir" + 1)
            }

            if (params."sCriteria_t193StaUtamaTambahan") {
                ilike("t193StaUtamaTambahan", "%" + (params."sCriteria_t193StaUtamaTambahan" as String) + "%")
            }

            if (params."sCriteria_t193StaGantiTambahan") {
                ilike("t193StaGantiTambahan", "%" + (params."sCriteria_t193StaGantiTambahan" as String) + "%")
            }

            if (params."sCriteria_spkAsuransi") {
                eq("spkAsuransi", params."sCriteria_spkAsuransi")
            }

            if (params."sCriteria_t193StaJanjiKirimDok") {
                ilike("t193StaJanjiKirimDok", "%" + (params."sCriteria_t193StaJanjiKirimDok" as String) + "%")
            }

            if (params."sCriteria_t193TglJanjiKirimDok") {
                ge("t193TglJanjiKirimDok", params."sCriteria_t193TglJanjiKirimDok")
                lt("t193TglJanjiKirimDok", params."sCriteria_t193TglJanjiKirimDok" + 1)
            }

            if (params."sCriteria_t193StaKirimDok") {
                ilike("t193StaKirimDok", "%" + (params."sCriteria_t193StaKirimDok" as String) + "%")
            }

            if (params."sCriteria_t193TglKirimDok") {
                ge("t193TglKirimDok", params."sCriteria_t193TglKirimDok")
                lt("t193TglKirimDok", params."sCriteria_t193TglKirimDok" + 1)
            }

            if (params."sCriteria_t193MetodeKirim") {
                ilike("t193MetodeKirim", "%" + (params."sCriteria_t193MetodeKirim" as String) + "%")
            }

            if (params."sCriteria_t193StaReminderSurvey") {
                ilike("t193StaReminderSurvey", "%" + (params."sCriteria_t193StaReminderSurvey" as String) + "%")
            }

            if (params."sCriteria_t193TglReminderSurvey") {
                ge("t193TglReminderSurvey", params."sCriteria_t193TglReminderSurvey")
                lt("t193TglReminderSurvey", params."sCriteria_t193TglReminderSurvey" + 1)
            }

            if (params."sCriteria_t193StaActualSurvey") {
                ilike("t193StaActualSurvey", "%" + (params."sCriteria_t193StaActualSurvey" as String) + "%")
            }

            if (params."sCriteria_t193TglActualSurvey") {
                ge("t193TglActualSurvey", params."sCriteria_t193TglActualSurvey")
                lt("t193TglActualSurvey", params."sCriteria_t193TglActualSurvey" + 1)
            }

            if (params."sCriteria_t193Catatan") {
                ilike("t193Catatan", "%" + (params."sCriteria_t193Catatan" as String) + "%")
            }

            if (params."sCriteria_t193StaUtama") {
                ilike("t193StaUtama", "%" + (params."sCriteria_t193StaUtama" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t193IDAsuransi: it.t193IDAsuransi,

                    vendorAsuransi: it.vendorAsuransi.m193Nama,

                    customerVehicle: it.customerVehicle.t103VinCode,

                    t193ID: it.t193ID,

                    t193StaNomorSPK: it.t193StaNomorSPK,

                    t193NomorSPK: it.t193NomorSPK,

                    t193TanggalSPK: it.t193TanggalSPK ? it.t193TanggalSPK.format(dateFormat) : "",

                    t193NomorPolis: it.t193NomorPolis,

                    t193NamaPolis: it.t193NamaPolis,

                    t193AlamatPolis: it.t193AlamatPolis,

                    t193TelpPolis: it.t193TelpPolis,

                    t193TglPolis: it.t193TglPolis ? it.t193TglPolis.format(dateFormat) : "",

                    t193JmlPolis: it.t193JmlPolis,

                    t193TglAwal: it.t193TglAwal ? it.t193TglAwal.format(dateFormat) : "",

                    t193TglAkhir: it.t193TglAkhir ? it.t193TglAkhir.format(dateFormat) : "",

                    t193StaUtamaTambahan: it.t193StaUtamaTambahan,

                    t193StaGantiTambahan: it.t193StaGantiTambahan,

                    spkAsuransi: it.spkAsuransi,

                    t193StaJanjiKirimDok: it.t193StaJanjiKirimDok,

                    t193TglJanjiKirimDok: it.t193TglJanjiKirimDok ? it.t193TglJanjiKirimDok.format(dateFormat) : "",

                    t193StaKirimDok: it.t193StaKirimDok,

                    t193TglKirimDok: it.t193TglKirimDok ? it.t193TglKirimDok.format(dateFormat) : "",

                    t193MetodeKirim: it.t193MetodeKirim,

                    t193StaReminderSurvey: it.t193StaReminderSurvey,

                    t193TglReminderSurvey: it.t193TglReminderSurvey ? it.t193TglReminderSurvey.format(dateFormat) : "",

                    t193StaActualSurvey: it.t193StaActualSurvey,

                    t193TglActualSurvey: it.t193TglActualSurvey ? it.t193TglActualSurvey.format(dateFormat) : "",

                    t193Catatan: it.t193Catatan,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    t193StaUtama : it.t193StaUtama=='1'?"Utama":(it.t193StaUtama=='2'?"Tambahan":(it.t193StaUtama=='1'?"Pengganti":""))

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [SPKAsuransiInstance: new SPKAsuransi(params)]
    }

    def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.staDel = '0'
        params.lastUpdProcess = "INSERT"
        params.companyDealer = session?.userCompanyDealer
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.t193StaNomorSPK = params.t193StaNomorSPK=="on"?'1':'0'
        params.t193StaJanjiKirimDok = params.t193StaJanjiKirimDok=="on"?'1':'0'
        params.t193StaKirimDok = params.t193StaKirimDok=="on"?'1':'0'
        params.t193StaReminderSurvey = params.t193StaReminderSurvey=="on"?'1':'0'
        params.t193StaActualSurvey = params.t193StaActualSurvey=="on"?'1':'0'

        def SPKAsuransiInstance = new SPKAsuransi(params)
        def customerVehicle = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("t103VinCode",params.customerVehicles.trim(),[ignoreCase:true])
        }
        if(customerVehicle.size()<1){
            flash.message = "Vincode tidak ditemukan/salah"
            render(view: "create", model: [SPKAsuransiInstance: SPKAsuransiInstance,status:"create"])
            return
        }
        SPKAsuransiInstance?.customerVehicle = customerVehicle?.last()
        if (!SPKAsuransiInstance.save(flush: true)) {
            render(view: "create", model: [SPKAsuransiInstance: SPKAsuransiInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), SPKAsuransiInstance.id])
        redirect(action: "show", id: SPKAsuransiInstance.id)
    }

    def show(Long id) {
        def SPKAsuransiInstance = SPKAsuransi.get(id)
        if (!SPKAsuransiInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), id])
            redirect(action: "list")
            return
        }

        [SPKAsuransiInstance: SPKAsuransiInstance]
    }

    def edit(Long id) {
        def SPKAsuransiInstance = SPKAsuransi.get(id)
        if (!SPKAsuransiInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), id])
            redirect(action: "list")
            return
        }

        [SPKAsuransiInstance: SPKAsuransiInstance]
    }

    def update(Long id, Long version) {
        def SPKAsuransiInstance = SPKAsuransi.get(id)
        if (!SPKAsuransiInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (SPKAsuransiInstance.version > version) {

                SPKAsuransiInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi')] as Object[],
                        "Another user has updated this SPKAsuransi while you were editing")
                render(view: "edit", model: [SPKAsuransiInstance: SPKAsuransiInstance])
                return
            }
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        SPKAsuransiInstance.properties = params
        def customerVehicle = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("t103VinCode",params.customerVehicles.trim(),[ignoreCase:true])
        }
        if(customerVehicle.size()<1){
            flash.message = "Vincode tidak ditemukan/salah"
            render(view: "edit", model: [SPKAsuransiInstance: SPKAsuransiInstance,status:"edit"])
            return
        }
        SPKAsuransiInstance?.customerVehicle = customerVehicle?.last()

        if (!SPKAsuransiInstance.save(flush: true)) {
            render(view: "edit", model: [SPKAsuransiInstance: SPKAsuransiInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), SPKAsuransiInstance.id])
        redirect(action: "show", id: SPKAsuransiInstance.id)
    }

    def delete(Long id) {
        def SPKAsuransiInstance = SPKAsuransi.get(id)
        if (!SPKAsuransiInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), id])
            redirect(action: "list")
            return
        }

        try {
            SPKAsuransiInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'SPKAsuransi.label', default: 'SPKAsuransi'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(SPKAsuransi, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(SPKAsuransi, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
