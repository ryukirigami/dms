package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.Company
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class KomposisiKendaraanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = KomposisiKendaraan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq ("staDel","0")

            if (params."sCriteria_company") {
                eq("company",Company?.findByNamaPerusahaanIlike("%" + params."sCriteria_company" + "%"))
            }

            if (params."sCriteria_merk") {
                eq("merk",Merk?.findByM064NamaMerkIlike("%" + params."sCriteria_merk" + "%"))
            }

            if (params."sCriteria_model") {
                eq("model",Model?.findByM065NamaModelIlike ("%" + params."sCriteria_model" + "%"))
            }

            if (params."sCriteria_t105Id") {
                ilike("t105Id", "%" + (params."sCriteria_t105Id" as String) + "%")
            }

            if (params."sCriteria_t105TipeKepemilikan") {
                ilike("t105TipeKepemilikan", "%" + (params."sCriteria_t105TipeKepemilikan" as String) + "%")
            }

            if (params."sCriteria_t105Jumlah") {
                eq("t105Jumlah", params."sCriteria_t105Jumlah" as int)
            }

            if (params."sCriteria_t105Tahun") {
                ilike("t105Tahun", "%" + (params."sCriteria_t105Tahun" as String) + "%")
            }


                ilike("staDel", "0")

            if (params."sCriteria_customer") {
                eq("customer", params."sCriteria_customer")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    company: it.company?.namaPerusahaan,

                    merk: it.merk?.m064NamaMerk,

                    model: it.model?.m065NamaModel,

                    t105Id: it.t105Id,

                    t105TipeKepemilikan: it.t105TipeKepemilikan,

                    t105Jumlah: it.t105Jumlah,

                    t105Tahun: it.t105Tahun,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

                    customer: it.customer,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [komposisiKendaraanInstance: new KomposisiKendaraan(params)]
    }

    def save() {
        def komposisiKendaraanInstance = new KomposisiKendaraan(params)
        komposisiKendaraanInstance.company = Company.findByNamaPerusahaan(params.namaPerusahaan)
        komposisiKendaraanInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        komposisiKendaraanInstance?.lastUpdProcess = "INSERT"
        komposisiKendaraanInstance?.setStaDel('0')
        komposisiKendaraanInstance?.dateCreated = datatablesUtilService?.syncTime()
        komposisiKendaraanInstance?.lastUpdated = datatablesUtilService?.syncTime()
        def cek = KomposisiKendaraan.createCriteria().list() {
            and{
                eq("company",komposisiKendaraanInstance.company)
                eq("model",komposisiKendaraanInstance.model)
                eq("merk",komposisiKendaraanInstance.merk)
                eq("t105Jumlah",komposisiKendaraanInstance.t105Jumlah)
                eq("t105Tahun",komposisiKendaraanInstance.t105Tahun)
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [komposisiKendaraanInstance: komposisiKendaraanInstance])
            return
        }
        if (!komposisiKendaraanInstance.save(flush: true)) {
            render(view: "create", model: [komposisiKendaraanInstance: komposisiKendaraanInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'komposisiKendaraan.label', default: 'Komposisi Kendaraan'), komposisiKendaraanInstance.id])
        redirect(action: "show", id: komposisiKendaraanInstance.id)
    }

    def show(Long id) {
        def komposisiKendaraanInstance = KomposisiKendaraan.get(id)
        if (!komposisiKendaraanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'komposisiKendaraan.label', default: 'KomposisiKendaraan'), id])
            redirect(action: "list")
            return
        }

        [komposisiKendaraanInstance: komposisiKendaraanInstance]
    }

    def edit(Long id) {
        def komposisiKendaraanInstance = KomposisiKendaraan.get(id)
        if (!komposisiKendaraanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'komposisiKendaraan.label', default: 'KomposisiKendaraan'), id])
            redirect(action: "list")
            return
        }

        [komposisiKendaraanInstance: komposisiKendaraanInstance]
    }

    def update(Long id, Long version) {
        def komposisiKendaraanInstance = KomposisiKendaraan.get(id)
        if (!komposisiKendaraanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'komposisiKendaraan.label', default: 'KomposisiKendaraan'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (komposisiKendaraanInstance.version > version) {

                komposisiKendaraanInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'komposisiKendaraan.label', default: 'KomposisiKendaraan')] as Object[],
                        "Another user has updated this KomposisiKendaraan while you were editing")
                render(view: "edit", model: [komposisiKendaraanInstance: komposisiKendaraanInstance])
                return
            }
        }
        def cek = KomposisiKendaraan.createCriteria()
        def result = cek.list() {
            and{
                eq("company",Company.findByNamaPerusahaan(params.namaPerusahaan))
                eq("model",Model.findById(Long.parseLong(params.model?.id)))
                eq("merk",Merk.findById(Long.parseLong(params.merk?.id)))
                eq("t105Jumlah", Integer.parseInt(params.t105Jumlah))
                eq("t105Tahun", Integer.parseInt(params.t105Tahun))
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [komposisiKendaraanInstance: komposisiKendaraanInstance])
            return
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        komposisiKendaraanInstance.properties = params
        komposisiKendaraanInstance.company = Company.findByNamaPerusahaan(params.namaPerusahaan)
        komposisiKendaraanInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        komposisiKendaraanInstance?.lastUpdProcess = "UPDATE"

        if (!komposisiKendaraanInstance.save(flush: true)) {
            render(view: "edit", model: [komposisiKendaraanInstance: komposisiKendaraanInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'komposisiKendaraan.label', default: 'Komposisi Kendaraan'), komposisiKendaraanInstance.id])
        redirect(action: "show", id: komposisiKendaraanInstance.id)
    }

    def delete(Long id) {
        def komposisiKendaraanInstance = KomposisiKendaraan.get(id)
        if (!komposisiKendaraanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'komposisiKendaraan.label', default: 'KomposisiKendaraan'), id])
            redirect(action: "list")
            return
        }

        try {
            komposisiKendaraanInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            komposisiKendaraanInstance?.lastUpdProcess = "DELETE"
            komposisiKendaraanInstance?.setStaDel('1')
            komposisiKendaraanInstance.save(flush:true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'komposisiKendaraan.label', default: 'KomposisiKendaraan'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'komposisiKendaraan.label', default: 'KomposisiKendaraan'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(KomposisiKendaraan, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(KomposisiKendaraan, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def insertMerk(){
        def namaMerk = params.namaMerk
        def merk = new Merk()
        def cek = merk.createCriteria()
        def result = cek.list() {
            or{
                eq("m064NamaMerk",params.namaMerk?.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }
        def res = [:]

        if(result){
            println "ERROR"
            res.error = "duplicate"
            render res as JSON
        }else{
            merk.m064ID = generateCodeService.codeGenerateSequence("M064_ID",null)
            merk.m064NamaMerk = namaMerk
            merk?.dateCreated = datatablesUtilService?.syncTime()
            merk?.lastUpdated = datatablesUtilService?.syncTime()
            merk?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            merk?.lastUpdProcess = "INSERT"
            merk?.staDel = "0"

            merk.save(flush:true)

            def merkData = Merk.last();
            res.put("id", merkData.id)
            res.put("namaMerk", merkData.m064NamaMerk)
            render res as JSON

        }

    }

    def insertModel(){
        def namaModel = params.namaModel
        def idMerk = params.idMerk
        def merk = Merk.findById(Double.parseDouble(idMerk))
        def models = new Model()
        def cek = models.createCriteria()
        def result = cek.list() {
            or{
                eq("m065NamaModel",params.namaModel?.trim(), [ignoreCase: true])
            }
            eq("staDel",'0')
        }
        def res = [:]

        if(result){
            res.error = "duplicate"

            render res as JSON
        }else{
            def model = new Model()
            model.m065ID = generateCodeService.codeGenerateSequence("M065_ID",null)
            model.m065NamaModel = namaModel
            model.merk = merk
            model?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            model?.lastUpdProcess = "INSERT"
            model?.lastUpdated = datatablesUtilService?.syncTime()
            model?.dateCreated = datatablesUtilService?.syncTime()
            model?.staDel = "0"

            model?.save(flush:true)
            def modelData = Model.last();
            res.put("id",modelData.id)
            res.put("namaModel", model.m065NamaModel)
            render res as JSON
        }
    }

    def getModel (){
        def idMerk = params.idMerk
        def merk = Merk.get(idMerk)

        def modelList = Model.findAllByMerk(merk)

        def res = []
        modelList.each {
            res << [
                    id : it.id,
                    namaModel : it.m065NamaModel
            ]

        }
        render res as JSON
    }

    def dataPerusahaan() {
        def res = [:]
        def opts = []
        def result = Company.findAllWhere(staDel : '0')
        result.each {
            opts << it.namaPerusahaan
        }
        res."options" = opts
        render res as JSON

    }
}