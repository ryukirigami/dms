package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class WarnaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

//    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Warna.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("staDel","0")

            if (params."sCriteria_m092ID") {
                ilike("m092ID","%" + (params."sCriteria_m092ID" as String) + "%")
            }

            if (params."sCriteria_m092NamaWarna") {
                ilike("m092NamaWarna", "%" + (params."sCriteria_m092NamaWarna" as String) + "%")
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m092ID: it.m092ID,

                    m092NamaWarna: it.m092NamaWarna,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [warnaInstance: new Warna(params)]
    }

    def save() {
        def warnaInstance = new Warna(params)
        warnaInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        warnaInstance?.lastUpdProcess = "INSERT"
        warnaInstance?.dateCreated = datatablesUtilService?.syncTime()
        warnaInstance?.lastUpdated = datatablesUtilService?.syncTime()
        warnaInstance?.setStaDel('0')
        def cek = Warna.createCriteria().list() {
            and{
                eq("m092ID",warnaInstance.m092ID?.trim(), [ignoreCase: true])
                eq("m092NamaWarna",warnaInstance.m092NamaWarna?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [warnaInstance: warnaInstance])
            return
        }

//        warnaInstance?.m092ID = generateCodeService.codeGenerateSequence("M092_ID",null)
        if (!warnaInstance.save(flush: true)) {
            render(view: "create", model: [warnaInstance: warnaInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'warna.label', default: 'Warna'), warnaInstance.id])
        redirect(action: "show", id: warnaInstance.id)
    }

    def show(Long id) {
        def warnaInstance = Warna.get(id)
        if (!warnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warna.label', default: 'Warna'), id])
            redirect(action: "list")
            return
        }

        [warnaInstance: warnaInstance]
    }

    def edit(Long id) {
        def warnaInstance = Warna.get(id)
        def idNew = warnaInstance.m092ID
        if (!warnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warna.label', default: 'Warna'), id])
            redirect(action: "list")
            return
        }

        [warnaInstance: warnaInstance, idNew : idNew]
    }

    def update(Long id, Long version) {
        def warnaInstance = Warna.get(id)
        if (!warnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warna.label', default: 'Warna'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (warnaInstance.version > version) {

                warnaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'warna.label', default: 'Warna')] as Object[],
                        "Another user has updated this Warna while you were editing")
                render(view: "edit", model: [warnaInstance: warnaInstance])
                return
            }
        }

        def cek = Warna.createCriteria()
        def result = cek.list() {
            and{
                eq("m092ID",params.m092ID?.trim(), [ignoreCase: true])
                eq("m092NamaWarna",params.m092NamaWarna?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [warnaInstance: warnaInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        warnaInstance.properties = params
        warnaInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        warnaInstance?.lastUpdProcess = "UPDATE"

        if (!warnaInstance.save(flush: true)) {
            render(view: "edit", model: [warnaInstance: warnaInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'warna.label', default: 'Warna'), warnaInstance.id])
        redirect(action: "show", id: warnaInstance.id)
    }

    def delete(Long id) {
        def warnaInstance = Warna.get(id)
        if (!warnaInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'warna.label', default: 'Warna'), id])
            redirect(action: "list")
            return
        }

        try {
            warnaInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            warnaInstance?.lastUpdProcess = "DELETE"
            warnaInstance?.setStaDel('1')
            warnaInstance.save(flush:true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'warna.label', default: 'Warna'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'warna.label', default: 'Warna'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Warna, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Warna, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
