package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ModelController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = Model.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m065ID") {
                ilike("m065ID","%" + (params."sCriteria_m065ID" as String) + "%")
            }

            if (params."sCriteria_merk") {
                eq("merk",Merk.findByM064NamaMerkIlike("%" + (params."sCriteria_merk") + "%"))
            }

            if (params."sCriteria_m065NamaModel") {
                ilike("m065NamaModel", "%" + (params."sCriteria_m065NamaModel" as String) + "%")
            }

            ilike("staDel",'0')



            switch (sortProperty) {
                case "merk":
                    merk{
                        order("m064NamaMerk",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m065ID: it.m065ID,

                    merk: it.merk.m064NamaMerk,

                    m065NamaModel: it.m065NamaModel,

                    staDel: it.staDel,

                    createdBy: it.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it.lastUpdProcess,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {

    }

    def save() {
        def modelInstance = new Model(params)
        modelInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        modelInstance?.lastUpdProcess = "INSERT"
        modelInstance?.setStaDel('0')
        modelInstance?.dateCreated = datatablesUtilService?.syncTime()
        modelInstance?.lastUpdated = datatablesUtilService?.syncTime()
        def cek = Model.createCriteria().list() {
            and{
                eq("merk", modelInstance.merk)
                eq("m065NamaModel",modelInstance.m065NamaModel?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            render(view: "create", model: [modelInstance: modelInstance])
            return
        }else{
            modelInstance?.m065ID = generateCodeService.codeGenerateSequence("M065_ID",null)

        }
        if (!modelInstance.save(flush: true)) {
            render(view: "create", model: [modelInstance: modelInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'model.label', default: 'Model'), modelInstance.id])
        redirect(action: "show", id: modelInstance.id)
    }

    def show(Long id) {
        def modelInstance = Model.get(id)
        if (!modelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'model.label', default: 'Model'), id])
            redirect(action: "list")
            return
        }

        [modelInstance: modelInstance]
    }

    def edit(Long id) {
        def modelInstance = Model.get(id)
        def idNew = modelInstance.m065ID
        if (!modelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'model.label', default: 'Model'), id])
            redirect(action: "list")
            return
        }

        [modelInstance: modelInstance,idNew : idNew]
    }

    def update(Long id, Long version) {
        def modelInstance = Model.get(id)
        if (!modelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'model.label', default: 'Model'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (modelInstance.version > version) {

                modelInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'model.label', default: 'Model')] as Object[],
                        "Another user has updated this Model while you were editing")
                render(view: "edit", model: [modelInstance: modelInstance])
                return
            }
        }
        def cek = Model.createCriteria()
        def result = cek.list() {
            and{
                eq("merk",Merk.findById(Long.parseLong(params.merk.id)))
                eq("m065NamaModel",params.m065NamaModel?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            flash.message = "Data sudah ada"
            render(view: "edit", model: [modelInstance: modelInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        modelInstance.properties = params
        modelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        modelInstance?.lastUpdProcess = "UPDATE"
        if (!modelInstance.save(flush: true)) {
            render(view: "edit", model: [modelInstance: modelInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'model.label', default: 'Model'), modelInstance.id])
        redirect(action: "show", id: modelInstance.id)
    }

    def delete(Long id) {
        def modelInstance = Model.get(id)
        if (!modelInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'model.label', default: 'Model'), id])
            redirect(action: "list")
            return
        }

        try {
            modelInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            modelInstance?.lastUpdProcess = "DELETE"
            modelInstance?.setStaDel('1')
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'model.label', default: 'Model'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'model.label', default: 'Model'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDelNew(Model, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(Model, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
