package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.JobFA
import com.kombos.maintable.PartsFA
import com.kombos.maintable.VehicleFA
import com.kombos.production.FinalInspection
import grails.converters.JSON

class FAController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        def isFrom = "fa"
        if(params.isFrom){
            isFrom = params.isFrom
        }
        [isFrom : isFrom]
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = FA.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_m185ID") {
                eq("m185ID", params."sCriteria_m185ID")
            }

            if (params."sCriteria_m185TanggalFA") {
                ge("m185TanggalFA", params."sCriteria_m185TanggalFA")
                lt("m185TanggalFA", params."sCriteria_m185TanggalFA" + 1)
            }

            if (params."sCriteria_jenisFA") {
                eq("jenisFA", params."sCriteria_jenisFA")
            }

            if (params."sCriteria_m185NomorSurat") {
                ilike("m185NomorSurat", "%" + (params."sCriteria_m185NomorSurat" as String) + "%")
            }

            if (params."sCriteria_m185NamaFA") {
                ilike("m185NamaFA", "%" + (params."sCriteria_m185NamaFA" as String) + "%")
            }

            if (params."sCriteria_m185ThnBlnRakit1") {
                ilike("m185ThnBlnRakit1", "%" + (params."sCriteria_m185ThnBlnRakit1" as String) + "%")
            }

            if (params."sCriteria_m185ThnBlnRakit2") {
                ilike("m185ThnBlnRakit2", "%" + (params."sCriteria_m185ThnBlnRakit2" as String) + "%")
            }

            if (params."sCriteria_m185xNamaUser") {
                ilike("m185xNamaUser", "%" + (params."sCriteria_m185xNamaUser" as String) + "%")
            }

            if (params."sCriteria_m185xNamaDivisi") {
                ilike("m185xNamaDivisi", "%" + (params."sCriteria_m185xNamaDivisi" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }


            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    m185TanggalFA: it.m185TanggalFA ? it.m185TanggalFA.format(dateFormat) : "",

                    jenisFA: it.jenisFA,

                    m185NomorSurat: it.m185NomorSurat,

                    m185NamaFA: it.m185NamaFA,

                    m185ThnBlnRakit1: it.m185ThnBlnRakit1,

                    m185ThnBlnRakit2: it.m185ThnBlnRakit2

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [FAInstance: new FA(params)]
    }

    def save() {
        def FAInstance = new FA(params)
        FAInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        FAInstance.setLastUpdProcess("INSERT")
        FAInstance?.setDateCreated(datatablesUtilService?.syncTime())
        FAInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        if (params.m185ThnBlnRakit11){
            FAInstance.m185ThnBlnRakit1 += params.m185ThnBlnRakit11
        }
        if (params.m185ThnBlnRakit21){
            FAInstance.m185ThnBlnRakit2 += params.m185ThnBlnRakit21
        }
        def cek = FA.createCriteria().list {
            eq("m185NomorSurat",params.m185NomorSurat.trim(),[ignoreCase : true])
        }
        if(cek){
            flash.message = "Data dengan nomor surat tersebut sudah ada"
            render(view: "create", model: [FAInstance: FAInstance])
            return
        }
        if(params.m185ThnBlnRakit11.toLong()>12 || params.m185ThnBlnRakit21.toLong()>12){
            flash.message = "Nilai bulan rakit tidak boleh lebih dari 12"
            render(view: "create", model: [FAInstance: FAInstance])
            return
        }
        if((params.m185ThnBlnRakit1.toLong()==params.m185ThnBlnRakit2.toLong() && params.m185ThnBlnRakit11.toLong()>params.m185ThnBlnRakit21.toLong())
                || params.m185ThnBlnRakit1.toLong()>params.m185ThnBlnRakit2.toLong()){
            flash.message = "Tahun dan bulan rakit 2 harus setelah tahun bulan rakit 1"
            render(view: "create", model: [FAInstance: FAInstance])
            return
        }
        if (!FAInstance.save(flush: true)) {
            render(view: "create", model: [FAInstance: FAInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'FA.label', default: 'FA'), ""])
        redirect(action: "show", id: FAInstance.id)
    }

    def show(Long id) {
        def FAInstance = FA.get(id)
        if (!FAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FA.label', default: 'FA'), id])
            redirect(action: "list")
            return
        }

        [FAInstance: FAInstance]
    }

    def edit(Long id) {
        def FAInstance = FA.get(id)
        if (!FAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FA.label', default: 'FA'), id])
            redirect(action: "list")
            return
        }

        [FAInstance: FAInstance]
    }

    def update(Long id, Long version) {
        def FAInstance = FA.get(id)
        if (!FAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FA.label', default: 'FA'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (FAInstance.version > version) {

                FAInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'FA.label', default: 'FA')] as Object[],
                        "Another user has updated this FA while you were editing")
                render(view: "edit", model: [FAInstance: FAInstance])
                return
            }
        }

        def cek = FA.createCriteria().list {
            eq("m185NomorSurat",params.m185NomorSurat.trim(),[ignoreCase : true])
        }
        if(cek){
            for(find in cek){
                if(find.id!=id){
                    flash.message = "Data dengan nomor surat tersebut sudah ada"
                    render(view: "edit", model: [FAInstance: FAInstance])
                    return
                }
            }
        }
        if(params.m185ThnBlnRakit11.toLong()>12 || params.m185ThnBlnRakit21.toLong()>12){
            flash.message = "Nilai bulan rakit tidak boleh lebih dari 12"
            render(view: "edit", model: [FAInstance: FAInstance])
            return
        }
        if((params.m185ThnBlnRakit1.toLong()==params.m185ThnBlnRakit2.toLong() && params.m185ThnBlnRakit11.toLong()>params.m185ThnBlnRakit21.toLong())
            || params.m185ThnBlnRakit1.toLong()>params.m185ThnBlnRakit2.toLong()){
            flash.message = "Tahun dan bulan rakit 2 harus setelah tahun bulan rakit 1"
            render(view: "edit", model: [FAInstance: FAInstance])
            return
        }
        if(params.m185ThnBlnRakit11.toLong()>12 || params.m185ThnBlnRakit21.toLong()>12){
            flash.message = "Nilai bulan rakit tidak boleh lebih dari 12"
            render(view: "create", model: [FAInstance: FAInstance])
            return
        }
        params.lastUpdated = datatablesUtilService?.syncTime()
        FAInstance.properties = params
        FAInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        FAInstance.setLastUpdProcess("UPDATE")


        if (params.m185ThnBlnRakit11){
            FAInstance.m185ThnBlnRakit1 += params.m185ThnBlnRakit11
        }
        if (params.m185ThnBlnRakit21){
            FAInstance.m185ThnBlnRakit2 += params.m185ThnBlnRakit21
        }

        if (!FAInstance.save(flush: true)) {
            render(view: "edit", model: [FAInstance: FAInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'FA.label', default: 'FA'), ""])
        redirect(action: "show", id: FAInstance.id)
    }

    def delete(Long id) {
        def hasil = ""
        def FAInstance = FA.get(id)
        if (!FAInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'FA.label', default: 'FA'), id])
            redirect(action: "list")
            return
        }

        def cekInJob = JobFA.findByFaAndStaDel(FAInstance,"0")
        def cekInParts = PartsFA.findByFaAndStaDel(FAInstance,"0")
        def cekInVehicle = VehicleFA.findByFa(FAInstance)
        def cekInFI = FinalInspection.findByFa(FAInstance)
        if(cekInJob || cekInParts || cekInVehicle || cekInFI){
            hasil = "gagal"
        }else{
            FAInstance.delete(flush: true)
            hasil = "sukses"
        }

        render hasil
    }

    def massdelete() {
        def res = [:]
        def hasil = "sukses"
        try {
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            jsonArray.each { oList << FA.get(it)  }

            oList*.discard() //detach all the objects from session
            oList.each{
                def cekInJob = JobFA.findByFaAndStaDel(it,"0")
                def cekInParts = PartsFA.findByFaAndStaDel(it,"0")
                def cekInVehicle = VehicleFA.findByFa(it)
                def cekInFI = FinalInspection.findByFa(it)
                if(cekInJob || cekInParts || cekInVehicle || cekInFI){
                    hasil = "gagal"
                }else{
                    it.delete()
                }
            }
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render hasil
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(FA, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
