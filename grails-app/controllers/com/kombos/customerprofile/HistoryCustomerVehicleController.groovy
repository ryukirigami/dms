package com.kombos.customerprofile

import com.kombos.administrasi.FullModelCode
import com.kombos.administrasi.FullModelVinCode
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.Customer
import com.kombos.maintable.JawabanSurvey
import com.kombos.maintable.MappingCustVehicle
import com.kombos.reception.Reception
import com.kombos.woinformation.PartsRCP
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.context.request.RequestContextHolder

class HistoryCustomerVehicleController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

   	def datatablesUtilService

   	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

   	static viewPermissions = ['index', 'list', 'datatablesList']

   	static addPermissions = ['create', 'save']

   	static editPermissions = ['edit', 'update']

   	static deletePermissions = ['delete']

   	def index() {
   		redirect(action: "list", params: params)
   	}

   	def list(Integer max) {
   	}


   	def datatablesList() {

        def dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

   		session.exportParams=params

        def c = HistoryCustomerVehicle.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            eq("staDel", '0')

            if (params."sCriteria_nopol") {
                ilike("fullNoPol","%"+params."sCriteria_nopol"+"%");
            }

            if (params."sCriteria_customerVehicle") {
                customerVehicle {
                    ilike("t103VinCode", "%" + (params."sCriteria_customerVehicle" as String) + "%")
                }
            }

            if (params."sCriteria_fullModelCode") {
                fullModelCode {
                    ilike("t110FullModelCode", "%" + (params."sCriteria_fullModelCode" as String) + "%")
                }
            }

            if (params."sCriteria_baseModel") {
                fullModelCode {
                    baseModel {
                        ilike("m102NamaBaseModel", "%" + (params."sCriteria_baseModel" as String) + "%")
                    }
                }
            }

            if (params."sCriteria_t183TglDEC") {
                ge("t183TglDEC", params."sCriteria_t183TglDEC")
                lt("t183TglDEC", params."sCriteria_t183TglDEC" + 1)
            }

            if (params."sCriteria_t183NoMesin") {
                ilike("t183NoMesin", "%" + (params."sCriteria_t183NoMesin" as String) + "%")
            }

            if (params."sCriteria_t183Warna") {
                warna {
                    ilike("m092NamaWarna", "%" + (params."sCriteria_t183Warna" as String) + "%")
                }
            }

            if (params."sCriteria_t183NoKunci") {
                ilike("t183NoKunci", "%" + (params."sCriteria_t183NoKunci" as String) + "%")
            }

            if (params."sCriteria_t183TglSTNK") {
                ge("t183TglSTNK", params."sCriteria_t183TglSTNK")
                lt("t183TglSTNK", params."sCriteria_t183TglSTNK" + 1)
            }

            if (params."sCriteria_asuransi") {
                vendorAsuransi{
                    ilike("m193Nama", "%" + (params."sCriteria_asuransi" as String) + "%")
                }
            }

            if (params."sCriteria_leasing") {
                leasing{
                    ilike("m1000NamaLeasing", "%" + (params."sCriteria_leasing" as String) + "%")
                }
            }


           // order("fullNoPol","asc")
   		}

        def	rows = []

   		results.each {
            String npl = it?.kodeKotaNoPol?.m116ID+" "+it?.t183NoPolTengah+" "+it?.t183NoPolBelakang
   				rows << [

   					id: it.id,

                    nopol: npl,

                    customerVehicle: it.customerVehicle?.t103VinCode,

                    fullModelCode: it.fullModelCode?.t110FullModelCode,

                    baseModel: it.fullModelCode?.baseModel?.m102NamaBaseModel,

                    t183TglDEC: it.t183TglDEC ? it.t183TglDEC.format(dateFormat) : "",

                    t183NoMesin: it.t183NoMesin ? it.t183NoMesin:"-",

                    warna: it.warna?.m092NamaWarna,

                    t183NoKunci: it.t183NoKunci ? it.t183NoKunci:"-",

                    t183TglSTNK: it.t183TglSTNK ? it.t183TglSTNK.format(dateFormat) : "",

                    asuransi : it.vendorAsuransi?.m193Nama ? it.vendorAsuransi?.m193Nama : "-",

                    leasing : it.leasing?.m1000NamaLeasing ? it.leasing?.m1000NamaLeasing : "-"
   			]
   		}

        def ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

   		render ret as JSON
   	}

   	def create() {
        def historyCustomerVehicleInstance = new HistoryCustomerVehicle(params)
        historyCustomerVehicleInstance.setT183StaGantiPemilik("0")
        historyCustomerVehicleInstance.setT183StaTaxi("0")
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailCustomer'] = null
        [historyCustomerVehicleInstance: historyCustomerVehicleInstance]
   	}

   	def save() {
        def historyCustomerVehicleInstance = new HistoryCustomerVehicle(params)
//        historyCustomerVehicleInstance?.t183ThnBlnRakit = params.t183ThnBlnRakit_tahun + params.t183ThnBlnRakit_bulan
        historyCustomerVehicleInstance?.t183TglEntry = new Date()
        historyCustomerVehicleInstance?.t183StaProject = "1"
        historyCustomerVehicleInstance?.t183StaReminderSBI = "0"
        historyCustomerVehicleInstance?.t183StaReminderCustPasif = "0"
        historyCustomerVehicleInstance?.t183TglTransaksi= new Date()
        historyCustomerVehicleInstance?.staDel = "0"
        historyCustomerVehicleInstance?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        historyCustomerVehicleInstance?.lastUpdProcess = "INSERT"
        historyCustomerVehicleInstance?.dateCreated = datatablesUtilService?.syncTime()
        historyCustomerVehicleInstance?.lastUpdated = datatablesUtilService?.syncTime()
        def customerVehicle = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("t103VinCode",params.customerVehicles.trim(),[ignoreCase:true])
        }
        if(customerVehicle.size()<1){
            flash.message = "Vincode tidak ditemukan/salah"
            render(view: "create", model: [historyCustomerVehicleInstance: historyCustomerVehicleInstance,status:"create"])
            return
        }
        if(!params.fullModelCodeId){
            flash.message = "Fullmodelcode tidak ditemukan/salah"
            render(view: "create", model: [historyCustomerVehicleInstance: historyCustomerVehicleInstance,status:"create"])
            return
        }
        def fullModel = FullModelCode.get(params.fullModelCodeId.toLong())
        historyCustomerVehicleInstance?.customerVehicle = customerVehicle?.last()
        historyCustomerVehicleInstance?.fullModelCode = fullModel

   		if (!historyCustomerVehicleInstance.save(flush: true)) {
   			render(view: "create", model: [historyCustomerVehicleInstance: historyCustomerVehicleInstance,status:"create"])
   			return
   		}

   		flash.message = message(code: 'default.created.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), historyCustomerVehicleInstance.id])
        render """<script language="javascript" type="text/javascript">
                        expandTableLayout();
               </script> """
   	}

   	def show(Long id) {
   		def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(id)
   		if (!historyCustomerVehicleInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), id])
   			redirect(action: "list")
   			return
   		}

   		[historyCustomerVehicleInstance: historyCustomerVehicleInstance]
   	}

   	def edit(Long id) {
   		def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(id)
        def currentRequest = RequestContextHolder.requestAttributes
        currentRequest.session['detailCustomer'] = null
   		if (!historyCustomerVehicleInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), id])
   			redirect(action: "list")
   			return
   		}
        params.t183ThnBlnRakit_tahun = historyCustomerVehicleInstance.t183ThnBlnRakit ? historyCustomerVehicleInstance.t183ThnBlnRakit.substring(0,4) : ""
        params.t183ThnBlnRakit_bulan = historyCustomerVehicleInstance.t183ThnBlnRakit ? historyCustomerVehicleInstance.t183ThnBlnRakit.substring(4) : ""
   		[historyCustomerVehicleInstance: historyCustomerVehicleInstance, params: params]
   	}

   	def update(Long id, Long version) {
   		def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(id)

   		if (!historyCustomerVehicleInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), id])
            //redirect(action: "edit", id: historyCustomerVehicleInstance.id)
            render """<script language="javascript" type="text/javascript">
                                    expandTableLayout();
                           </script> """
   			return
   		}

   		if (version != null) {
   			if (historyCustomerVehicleInstance.version > version) {

   				historyCustomerVehicleInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
   				[message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle')] as Object[],
   				"Another user has updated this HistoryCustomerVehicle while you were editing")
   				render(view: "edit", model: [historyCustomerVehicleInstance: historyCustomerVehicleInstance,status:"update"])
   				return
   			}
   		}
        def customerVehicle = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("t103VinCode",params.customerVehicles.trim(),[ignoreCase:true])
        }
        if(customerVehicle.size()<1){
            render(view: "edit", model: [historyCustomerVehicleInstance: historyCustomerVehicleInstance,status:"update"])
            return
        }
        if(!params.fullModelCodeId){
            flash.message = "Fullmodelcode tidak ditemukan/salah"
            render(view: "edit", model: [historyCustomerVehicleInstance: historyCustomerVehicleInstance,status:"update"])
            return
        }
        def fullModel = FullModelCode.get(params.fullModelCodeId.toLong())
        params.lastUpdated = datatablesUtilService?.syncTime()
        historyCustomerVehicleInstance?.properties = params
        historyCustomerVehicleInstance?.customerVehicle = customerVehicle?.last()
        historyCustomerVehicleInstance?.fullModelCode = fullModel
//        historyCustomerVehicleInstance?.t183ThnBlnRakit = params.t183ThnBlnRakit_tahun + params.t183ThnBlnRakit_bulan
   		if (!historyCustomerVehicleInstance.save(flush: true)) {
   			render(view: "edit", model: [historyCustomerVehicleInstance: historyCustomerVehicleInstance,status:"update"])
   			return
   		}

        def currentRequest = RequestContextHolder.requestAttributes
        def rows = currentRequest.session['detailCustomer']
        rows.each{
            MappingCustVehicle mappingCustVehicle = MappingCustVehicle.findByCustomerAndCustomerVehicle(Customer.get(it.id),historyCustomerVehicleInstance?.customerVehicle)
            if(!mappingCustVehicle){
                mappingCustVehicle = new MappingCustVehicle()
                mappingCustVehicle.customerVehicle = historyCustomerVehicleInstance?.customerVehicle
                mappingCustVehicle.customer = Customer.get(it.id)
                mappingCustVehicle.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mappingCustVehicle?.dateCreated = datatablesUtilService?.syncTime()
                mappingCustVehicle.lastUpdProcess = 'INSERT'
                mappingCustVehicle?.lastUpdated = datatablesUtilService?.syncTime()
                mappingCustVehicle.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                mappingCustVehicle.save(flush: true)
            }
        }

        currentRequest.session['detailCustomer']=null

   		flash.message = message(code: 'default.updated.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), historyCustomerVehicleInstance.id])
        render """<script language="javascript" type="text/javascript">
                        expandTableLayout();
               </script> """
   	}

   	def delete(Long id) {
   		def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(id)
   		if (!historyCustomerVehicleInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), id])
   			redirect(action: "list")
   			return
   		}

   		try {
   			historyCustomerVehicleInstance.delete(flush: true)
   			flash.message = message(code: 'default.deleted.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), id])
   			redirect(action: "list")
   		}
   		catch (DataIntegrityViolationException e) {
   			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'historyCustomerVehicle.label', default: 'History Customer Vehicle'), id])
   			redirect(action: "show", id: id)
   		}
   	}

   	def massdelete() {
   		def res = [:]
   		try {
   			datatablesUtilService.massDeleteStaDelNew(HistoryCustomerVehicle, params)
   			res.message = "Mass Delete Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}

   	def updatefield(){
   		def res = [:]
   		try {
   			datatablesUtilService.updateField(HistoryCustomerVehicle, params)
   			res.message = "Update Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}

    def historyCustomerVehicleDatatablesList() {
        def propertiesToRender = params.sColumns.split(",")

        def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(params.historyCustomerVehicleId)
        def vinCode = historyCustomerVehicleInstance?.customerVehicle?.t103VinCode
        def results = new ArrayList<HistoryCustomerVehicle>()
        historyCustomerVehicleInstance?.customerVehicle?.histories?.each {
            def newVinCode = it?.customerVehicle?.t103VinCode
            if(newVinCode.equals(vinCode) && !it.equals(historyCustomerVehicleInstance)){
                results.add(it)
            }
        }

        session.exportParams=params

        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    nopol: it.fullNoPol,
                    customerVehicle: it.customerVehicle?.t103VinCode ?  it.customerVehicle?.t103VinCode:"-",
                    namaCustomer: it.t183NamaSTNK ? it.t183NamaSTNK:"-",
                    namaCompany: MappingCustVehicle.findByCustomerVehicle(it.customerVehicle)?.customer?.company?.namaPerusahaan ? MappingCustVehicle.findByCustomerVehicle(it.customerVehicle)?.customer?.company?.namaPerusahaan:"-"
            ]
        }
        def ret = [sEcho: params.sEcho, iTotalRecords:  results?.size(), iTotalDisplayRecords: results?.size(), aaData: rows]
        render ret as JSON
   	}

    def historyServiceDatatablesList() {
        def propertiesToRender = params.sColumns.split(",")

        def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(params.historyCustomerVehicleId)
        def vinCode = historyCustomerVehicleInstance?.customerVehicle?.t103VinCode
        def historyList = new ArrayList()
        historyCustomerVehicleInstance?.customerVehicle?.histories?.each {
            def newVinCode = it?.customerVehicle?.t103VinCode
            if(newVinCode.equals(vinCode) && !it.equals(historyCustomerVehicleInstance)){
                historyList.add(it)
            }
        }

        def results = Reception.findAllByHistoryCustomerVehicleInListAndStaDelAndStaSave(historyList,"0","0")

        session.exportParams=params

        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    tanggalService: it.t401TanggalWO,
                    nopol: it.historyCustomerVehicle?.kodeKotaNoPol?.m116ID + " " + it.historyCustomerVehicle?.t183NoPolTengah + " " + it.historyCustomerVehicle?.t183NoPolBelakang,
                    nomorWO: it.t401NoWO,
                    jenisService: it.operation?.m053NamaOperation,
                    serviceAdvisor: it.namaManPower?.t015NamaBoard,
                    teknisi: it.namaManPower?.t015NamaLengkap,
            ]
        }
        def ret = [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
        render ret as JSON
    }

    def serviceDetailDatatablesList() {
        def propertiesToRender = params.sColumns.split(",")

        def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(params.historyCustomerVehicleId)
        def vinCode = historyCustomerVehicleInstance?.customerVehicle?.t103VinCode
        def historyList = new ArrayList()
        historyCustomerVehicleInstance?.customerVehicle?.histories.each {
            def newVinCode = it?.customerVehicle?.t103VinCode
//            if (newVinCode.equals(vinCode) && !it.equals(historyCustomerVehicleInstance) && it.staDel.equals('0')) {
            if (newVinCode.equals(vinCode) && it.staDel.equals('0')) {
                historyList.add(it)
            }
        }

        def r = Reception.createCriteria()
        def reception = r.list {
            eq("staSave","0");
            eq("staDel","0")
            if(historyList.size()>0){
                inList("historyCustomerVehicle", historyList)
            }else{
                eq("staDel","5")
            }
            order("dateCreated", "desc")
        }


        def results
        if (reception.size() > 0) {
            results = PartsRCP.findAllByReception(reception.first())
        } else {
            results = PartsRCP.findAllByReceptionAndStaDel(null,'7')
        }

        session.exportParams=params

        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    nomorWO: it.reception?.t401NoWO,
                    nomorInvoice: it.jobInv?.invoice?.t701NoInv,
                    namaJob: it.operation ?.m053NamaOperation,
                    parts: it.goods?.m111Nama,
                    historyRequest: it.t403xKet,
            ]
        }
        def ret = [sEcho: params.sEcho, iTotalRecords:  results?.size(), iTotalDisplayRecords: results?.size(), aaData: rows]
        render ret as JSON
    }

    def historyCustomerDatatablesList() {

        def output
        if('-1'.equals(params.historyCustomerId)){
            def currentRequest = RequestContextHolder.requestAttributes
            def rows = currentRequest?.session?.detailCustomer
            def size = (rows?.size()>0)?rows?.size():0
            def data = (rows?.size()>0)?rows:[]
            output = [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: data]
        }else{
            def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(params.historyCustomerVehicleId)
            def customerVehicle = historyCustomerVehicleInstance?.customerVehicle
            def mapping = MappingCustVehicle.findAllByCustomerVehicle(customerVehicle)
            def rows = []
            mapping.each {
                def historyCustomer = HistoryCustomer.findByCustomerAndStaDel(it.customer,'0')
                if(historyCustomer) {
                    rows << [
                            id     : historyCustomerVehicleInstance?.id,
                            nopol: historyCustomerVehicleInstance?.fullNoPol,
                            namaCustomer  : historyCustomer?.fullNama,
                            peran  : historyCustomer?.peranCustomer?.m115NamaPeranCustomer ? historyCustomer?.peranCustomer?.m115NamaPeranCustomer:"-"
                    ]
                }
            }

            def currentRequest = RequestContextHolder.requestAttributes
            def sessRows = currentRequest?.session?.detailCustomer
            sessRows?.each{
                rows << it
            }
            def size = (rows?.size()>0)?rows?.size():0
            def data = (rows?.size()>0)?rows:[]
            output = [sEcho: params.sEcho, iTotalRecords: size, iTotalDisplayRecords: size, aaData: data]
        }
        render output as JSON
    }

    def tpssSurveyDatatablesList() {
        def propertiesToRender = params.sColumns.split(",")

        def historyCustomerVehicleInstance = HistoryCustomerVehicle.get(params.historyCustomerVehicleId)
        def results = JawabanSurvey.findAllByCustomerVehicle(historyCustomerVehicleInstance?.customerVehicle)

        session.exportParams=params

        def rows = []
        results.each {
            rows << [
                    id: it.id,
                    tanggal: it.customerSurvey?.t104TglAwal,
                    feedback: it.t108Jawaban,
            ]
        }
        def ret = [sEcho: params.sEcho, iTotalRecords:  results.size(), iTotalDisplayRecords: results.size(), aaData: rows]
        render ret as JSON
    }

    def fieldActionDatatablesList() {

    }

    def mrsDatatablesList() {

    }

    def mappingCustomer(){
//        render (view: 'mappingCustomer', params: params)
    }

    def mappingCustomerDataTables(){
        def dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams=params

        def historyCustomerVehicle =HistoryCustomerVehicle.get(params.historyCustomerVehicleId)
        def customerVehicle = historyCustomerVehicle?.customerVehicle
        def mapping = MappingCustVehicle.findAllByCustomerVehicle(customerVehicle)
        def rowss = []
        mapping.each {
            def historyCustomer = HistoryCustomer.findByCustomerAndStaDel(it.customer,'0')

            if(historyCustomer) {
                rowss << [
                        id     : historyCustomer?.id,
                        nopol: historyCustomerVehicle?.fullNoPol,
                        namaCustomer  : historyCustomer?.fullNama,
                        peran  : historyCustomer?.peranCustomer?.m115NamaPeranCustomer
                ]
            }
        }

        def currentRequest = RequestContextHolder.requestAttributes
        def sessRows = currentRequest?.session?.detailCustomer
        sessRows?.each{
            rowss << it
        }

        def inListRow = []
        rowss.each {
            inListRow<<it.id
        }

        def c = HistoryCustomer.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            if(inListRow?.size()>0) {
                not {'in'("id",inListRow)}
            }
            if (params."sCriteria_namaCustomer") {
                ilike("fullNama","%"+params."sCriteria_namaCustomer"+"%")
            }

            if (params."sCriteria_peranCustomer") {
                peranCustomer {
                    ilike("m115NamaPeranCustomer","%"+(params."sCriteria_peranCustomer" as String) + "%")
                }
            }
            if (params."sCriteria_alamatCustomer") {
                ilike("t182Alamat","%"+params."sCriteria_alamatCustomer"+"%")
            }

            order("fullNama");
            eq("staDel", "0")
        }

        def rows = []

        results.each {
            rows << [

                    id: it.customer?.id,

                    namaCustomer: it.fullNama,

                    peranCustomer: it?.peranCustomer?.m115NamaPeranCustomer,

                    alamatCustomer: it?.t182Alamat
            ]
        }

        def ret = [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def addNewCustomer() {

        def currentRequest = RequestContextHolder.requestAttributes
        def Customer customer = Customer.get(params.customerId)

        HistoryCustomer historyCustomer = HistoryCustomer.findByCustomerAndStaDel(customer,'0')
        def rows = currentRequest?.session?.detailCustomer
        if(rows){
            rows << [
                    id:customer?.id,
                    nopol: '',
                    namaCustomer: historyCustomer?.fullNama,
                    peran:historyCustomer?.peranCustomer?.m115NamaPeranCustomer,
                    alamatCustomer: historyCustomer?.t182Alamat

            ]
        }else{
            rows = []
            rows << [
                    id:customer?.id,
                    nopol: '',
                    namaCustomer: historyCustomer?.fullNama,
                    peran:historyCustomer?.peranCustomer?.m115NamaPeranCustomer,
                    alamatCustomer: historyCustomer?.t182Alamat
            ]
        }


        currentRequest.session['detailCustomer'] = rows
        render "ok"
    }

    def changeFullModel (){
        def idFullModel = params.idFullModel
        String baseModel = "", staImport = ""
        if(idFullModel){
            def fullModel = FullModelCode.get(idFullModel.toLong())
            baseModel = fullModel?.baseModel?.m102NamaBaseModel
            staImport = fullModel?.staImport?.m100NamaStaImport
        }
        def res = [baseModel : baseModel,staImport: staImport]
        render res as JSON
    }

    def getVincodes(){
        def res = [:]
        def opts = []
        def vincodes = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            ilike("t103VinCode","%"+params.word+"%")
            order("t103VinCode")
            maxResults(10);
        }
        vincodes.each {
            opts<<it.t103VinCode
        }
        res."options" = opts
        render res as JSON
    }

    def changeVincode(){
        String staAda = "",staImport = "",baseModel="",mssgVincode = "",staVinUsed = "0"
        def idUsed = null
        def vincode = params.vinCode ? params.vinCode.trim() : ""
        def custVehicle = CustomerVehicle.createCriteria().list {
            eq("staDel","0")
            eq("t103VinCode",vincode,[ignoreCase: true])
        }

        def vinCodeList = FullModelVinCode.createCriteria().list {
            eq("staDel","0")
            if(custVehicle.size()>0){
                customerVehicle{
                    eq("id",custVehicle?.last()?.id)
                }
            }else{
                eq("staDel","5");
            }
            maxResults(1);
        }
        def res = []
        if(vinCodeList.size()>0){
            staAda = "ya"
            def cariVehicle = HistoryCustomerVehicle.findByCustomerVehicleAndStaDel(vinCodeList?.first()?.customerVehicle,"0",[sort : 'dateCreated',order : 'desc'])
            if(cariVehicle){
                String tambahan = "";
                if(cariVehicle?.kodeKotaNoPol && cariVehicle?.t183NoPolTengah && cariVehicle?.t183NoPolBelakang){
                    tambahan = "dengan nomor polisi "+cariVehicle?.fullNoPol
                }
                mssgVincode = "Vincode sudah digunakan untuk kendaraan lain "+tambahan+". Proses save tidak bisa dilanjutkan";
                idUsed = cariVehicle?.id
                staVinUsed = "1"
            }
            vinCodeList.each {
                res << [
                        id : it.fullModelCode.id,
                        fullModelCode: it.fullModelCode.t110FullModelCode
                ]
                staImport = it?.fullModelCode?.staImport?.m100NamaStaImport
                baseModel = it?.fullModelCode?.baseModel?.m102NamaBaseModel
            }
        }else{
            staAda = "tidak"
            res << [
                    id : "",
                    fullModelCode: ""
            ]
        }
        def hasil = [res : res,ada : staAda,staImport : staImport,baseModel: baseModel,mssgVincode : mssgVincode,idUsed : idUsed,staVinUsed:staVinUsed]
        render hasil as JSON
    }
    def getFullModelCodes(){
        def res = [:]
        def opts = []
        def vincodes = FullModelCode.createCriteria().list {
            eq("staDel","0")
            ilike("t110FullModelCode","%"+params.word+"%")
            order("t110FullModelCode")
            maxResults(10);
        }
        vincodes.each {
            opts<<it.t110FullModelCode
        }
        res."options" = opts
        render res as JSON
    }

    def addVincode(){
        String hasil = "gagal";
        def updateData = false
        def cekVc = CustomerVehicle.createCriteria().list {
            eq("staDel","0");
            eq("t103VinCode",params?.vincode);
        }
        def vc = null
        if(cekVc.size()>0){
            vc = cekVc.last()
            updateData = true
        }else {
            vc = new CustomerVehicle(
                    t103VinCode : params?.vincode,
                    staDel: '0',
                    lastUpdProcess : "INSERT",
                    createdBy : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    dateCreated: datatablesUtilService?.syncTime(),
                    lastUpdated: datatablesUtilService?.syncTime()
            ).save(flush: true)
        }

        if(vc){
            def fm = FullModelCode.findByT110FullModelCodeAndStaDel(params.fullModelCode,"0")
            if(fm){
                def mp = new FullModelVinCode(
                        customerVehicle : vc,
                        t109WMI : params.wmi,
                        t109VDS : params.vds,
                        t109CekDigit : params.cekDigit,
                        t109VIS : params.vis,
                        t109ThnBlnPembuatan : params.bulan +""+ params.tahun,
                        fullModelCode : fm,
                        staDel: '0',
                        lastUpdProcess : "UPDATE",
                        createdBy : org.apache.shiro.SecurityUtils.subject.principal.toString(),
                        dateCreated : datatablesUtilService?.syncTime(),
                        lastUpdated : datatablesUtilService?.syncTime()

                )

                mp?.save(flush: true)
                mp?.errors?.each{
                    println "FullModelVinCode ==> " + it
                }
                hasil = "sukses";
                if(updateData){
                    hasil = "update";
                }
            }
        }

        render hasil
    }
}
