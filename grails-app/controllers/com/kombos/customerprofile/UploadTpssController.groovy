package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.Customer
import com.kombos.maintable.JawabanSurvey
import com.kombos.maintable.PertanyaanSurvey
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class UploadTpssController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def list(Integer max) {
    }
    def index() {

    }
    def view() {
        def customerSurveyInstance = new CustomerSurvey(params)

        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'kodeSurvey',
                        'B':'vinCode',
                        'C':'kodeCustomer',
                        'D':'tanggal1',
                        'E':'tanggal2',
                        'F':'tex',
                        'G':'kodePertanyaan',
                        'H':'jawaban',
                        'I':'nilai',
                        'J':'ket',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        def jsonData = ""
        int jmlhDataError = 0;
        String htmlData = ""
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream','text/plain'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                //println "asd"
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [customerSurveyInstance: customerSurveyInstance])
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def jobList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = jobList as JSON
            JenisSurvey jenisSurvey = null
            CustomerVehicle customerVehicle = null
            PertanyaanSurvey pertanyaanSurvey = null

            String status = "0", style = ""
            jobList?.each {
                if((it.kodeSurvey && it.kodeSurvey!="") && (it.vinCode && it.vinCode!="") && (it.kodeCustomer && it.kodeCustomer!="") && (it.tanggal1 && it.tanggal1!="") &&
                        (it.kodePertanyaan && it.kodePertanyaan!="") && (it.jawaban && it.jawaban!="") && (it.nilai && it.nilai!="")){
                    jenisSurvey = JenisSurvey.findByM131ID(it.kodeSurvey)
                    customerVehicle = CustomerVehicle.findByT103VinCode(it.vinCode)
                    int b = (int) it.kodePertanyaan
                    pertanyaanSurvey = PertanyaanSurvey.findByM132ID(b)

                    if(!jenisSurvey || !customerVehicle || !pertanyaanSurvey){
                        jmlhDataError++;
                        status = "1";
                    }
                } else {

                    jmlhDataError++;
                    status = "1";
                }

                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }

                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.kodeSurvey+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.vinCode+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.kodeCustomer+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.tanggal1+"\n" +
                        "                            </td >\n" +
                        "                            <td>\n" +
                        "                                "+it.tanggal2+"\n" +
                        "                            </td >\n" +
                        "                            <td>\n" +
                        "                                "+it.tex+"\n" +
                        "                            </td >\n" +
                        "                            <td>\n" +
                        "                                "+it.kodePertanyaan+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.jawaban+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.nilai+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.ket+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"
                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.uploadTpss.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.uploadTpss.message', default: "Read File Done")
        }

        render(view: "index", model: [customerSurveyInstance: customerSurveyInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
    }

    def upload() {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy")
        def customerSurveyInstance = null
        def jawabanSurveyInstance = null
        def requestBody = request.JSON
        JenisSurvey jenisSurvey = null
        CustomerVehicle customerVehicle = null
        Customer customer = null
        HistoryCustomer historyCustomer  = null
        PertanyaanSurvey pertanyaanSurvey = null
        //save
        def lastID = null
        def idNew = null
        def lastID2 = null
        def idNew2 = null
        requestBody.each{
            customerSurveyInstance = new CustomerSurvey()
            jawabanSurveyInstance = new JawabanSurvey()

            jenisSurvey = JenisSurvey.findByM131ID(it.kodeSurvey)
            customerVehicle = CustomerVehicle.findByT103VinCode(it.vinCode)
            pertanyaanSurvey = PertanyaanSurvey.findByM132ID(it.kodePertanyaan)
            //
            if(jenisSurvey && customerVehicle && pertanyaanSurvey){
                String mul = it.tanggal1
                String sel = it.tanggal2

                customerSurveyInstance.t104Id = (int)(customerSurveyInstance.last().id) + 1
                customerSurveyInstance.jenisSurvey = jenisSurvey
                customerSurveyInstance.t104TglAwal =  new Date().parse('yyyy-MM-d',mul.toString())
                customerSurveyInstance.t104TglAkhir =  new Date().parse('yyyy-MM-d',sel.toString())
                customerSurveyInstance.t104Text = it.tex
                customerSurveyInstance.setStaDel('0')
                customerSurveyInstance.setT104xNamaUser('user')
                customerSurveyInstance.setT104xNamaDivisi('divisi')
                customerSurveyInstance.dateCreated = datatablesUtilService?.syncTime()
                customerSurveyInstance.lastUpdated = datatablesUtilService?.syncTime()
                customerSurveyInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                customerSurveyInstance.lastUpdProcess = "INSERT"
                try {
                    def data = CustomerSurvey.findByT104Text(customerSurveyInstance.t104Text)
                    if(!data){
                        customerSurveyInstance.save(flush : true)
                    }
                }catch (Exception e){
                    e.printStackTrace()
                }

                //
                jawabanSurveyInstance.customerVehicle = customerVehicle
                jawabanSurveyInstance.customerSurvey = customerSurveyInstance
                jawabanSurveyInstance.t108ID = (int) (jawabanSurveyInstance.last().id) + 1
                jawabanSurveyInstance.jenisSurvey = jenisSurvey
                jawabanSurveyInstance.t108TglBerlakuPertanyaanSurvey = new Date()
                jawabanSurveyInstance.t108Pertanyaan = pertanyaanSurvey.m132Pertanyaan.toString()
                jawabanSurveyInstance.t108Nomor = ((int) (jawabanSurveyInstance.last().id) + 1).toString()
                jawabanSurveyInstance.pertanyaanSurvey = pertanyaanSurvey
                jawabanSurveyInstance.t108Jawaban = it.jawaban
                jawabanSurveyInstance.t108Nilai = it.nilai
                jawabanSurveyInstance.t108Keterangan = it.ket

                jawabanSurveyInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                jawabanSurveyInstance.lastUpdProcess = "INSERT"
                try {
                    jawabanSurveyInstance.save(flush : true)
                }catch (Exception e){
                    e.printStackTrace()
                }
                flash.message = message(code: 'default.uploadTpss.message', default: "Save Job Done")
            }else{
                flash.message = message(code: 'default.uploadTpss.message', default: "gagal")
            }

        }

        render(view: "index", model: [customerSurveyInstance: customerSurveyInstance])

    }
}
