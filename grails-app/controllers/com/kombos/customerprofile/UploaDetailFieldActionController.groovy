package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.VehicleFA
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.apache.shiro.SecurityUtils
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

class UploaDetailFieldActionController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def excelImportService

    def datatablesUtilService

    static allowedMethods = [save: "POST", upload: "POST", view: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        [idFA: '']
    }

    def upload() {
        def vehicleFAInstance = null
        def vinCode = JSON.parse(params.vincode)
        def ids = JSON.parse(params.ids)
        def mainDealer = JSON.parse(params.mainDealer)
        def dealer = JSON.parse(params.dealer)

        int ind = 0
        vinCode.each{
            vehicleFAInstance = new VehicleFA();
            if((it && it!="") && (ids[ind] && ids[ind].toString()!="") && (mainDealer[ind] && mainDealer[ind].toString()!="") &&
                    (dealer[ind] && dealer[ind].toString()!="") ){
                String sVinCode = it?.toString()?.replaceAll(" ","");
                vehicleFAInstance.customerVehicle = CustomerVehicle.findByT103VinCodeAndStaDel(it,"0")
                vehicleFAInstance.historyCustomerVehicle = CustomerVehicle.findByT103VinCodeAndStaDel(it?.toString()?.replaceAll(" ",""),"0").getCurrentCondition()
                vehicleFAInstance.fa = FA.get(ids[ind].toString().toLong())
                vehicleFAInstance.t185MainDealer = mainDealer[ind]
                vehicleFAInstance.t185Dealer = dealer[ind]
                vehicleFAInstance.companyDealer = session?.userCompanyDealer
                vehicleFAInstance.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                vehicleFAInstance.lastUpdProcess = "INSERT"
                vehicleFAInstance.dateCreated = datatablesUtilService?.syncTime()
                vehicleFAInstance.lastUpdated = datatablesUtilService?.syncTime()
                //cek apakah data sudah ada, jika tidak ada maka disimpan
                def temp = vehicleFAInstance
                def cari = VehicleFA.createCriteria().list {
                    eq('t185Dealer',dealer[ind],[ignoreCase: true])
                    eq('t185MainDealer',mainDealer[ind],[ignoreCase: true])
                    eq("fa",FA.get(ids[ind].toString().toLong()))
                    customerVehicle{
                        eq("staDel","0")
                        eq("t103VinCode",sVinCode,[ignoreCase: true])
                    }
                    eq('historyCustomerVehicle', CustomerVehicle.findByT103VinCodeAndStaDel(sVinCode,"0").getCurrentCondition())
                }
                if(!cari){
                    vehicleFAInstance.save()
                    vehicleFAInstance.errors.each {println it}
                }
            }
            ind++
        }

        flash.message = message(code: 'default.uploadDataSales.message', default: "Save Data Sales Done")
        render(view: "index", model: [vehicleFAInstance: vehicleFAInstance])
    }

    def view() {
        def idFA = "-"
        if(params.fa){
            idFA = params.fa
        }
        def vehicleFAInstance = new VehicleFA(params)

        //handle upload file
        Map CONFIG_DATA_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'customerVehicle',
                        'B':'t185MainDealer',
                        'C':'t185Dealer',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }
        String htmlData = ""
        def jsonData = ""
        int jmlhDataError = 0;
        if(!uploadExcel?.empty){
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "index", model: [vehicleFAInstance: vehicleFAInstance])
                return
            }

            def arrVin = []
            def arrTemp = []

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def dataList = excelImportService.columns(workbook, CONFIG_DATA_COLUMN_MAP)

            jsonData = dataList as JSON

            String status = "0", style = ""
            int ind = -1
            dataList?.each {
                ind++
                String customerVehicle = it?.customerVehicle?.toString()?.replaceAll(" ","");
                String t185MainDealer = it.t185MainDealer.toString()
                String t185Dealer = it.t185Dealer.toString()

                if((it.customerVehicle && it.customerVehicle!="") && (it.t185MainDealer && it.t185MainDealer!="") &&
                        (it.t185Dealer && it.t185Dealer!="")){
                    def cekCV = CustomerVehicle?.findByT103VinCodeAndStaDel(customerVehicle,"0")
                    if(!cekCV){
                        jmlhDataError++;
                        status = "1";
                    }
                } else {
                    jmlhDataError++;
                    status = "1";

                }

//                arrVin << t182ID
                if(status.equals("1")){
                    style = "style='color:red;'"
                } else {
                    style = ""
                }
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                <input type='checkbox' class='pull-left row-select' name='cekSub' title='Select this'><input type='hidden' id='id"+ind+"' value='"+idFA+"'/>&nbsp;&nbsp;<input type='text' readonly='' id='txt"+ind+"' value='"+idFA+"' />\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='customerVehicle"+ind+"' value='"+it.customerVehicle+"'/> "+it.customerVehicle+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='t185MainDealer"+ind+"' value='"+it.t185MainDealer+"'/> "+it.t185MainDealer+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                <input type='hidden' id='t185Dealer"+ind+"' value='"+it.t185Dealer+"' /> "+it.t185Dealer+"\n" +
                        "                            </td>\n" +
                        "                        </tr>"

                status = "0"
            }
        }
        if(jmlhDataError>0){
            flash.message = message(code: 'default.upload.error.message', default: "Read File Done : Terdapat "+jmlhDataError+" data yang tidak valid, cek baris yang berwarna merah")
        } else {
            flash.message = message(code: 'default.upload.done.message', default: "Read File Done")
        }

        render(view: "index", model: [vehicleFAInstance: vehicleFAInstance, htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError, idFA : params.fa])
    }

}
