package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException


class AgamaController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

   	def datatablesUtilService

    def agamaService

    def generateCodeService

   	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

   	static viewPermissions = ['index', 'list', 'datatablesList']

   	static addPermissions = ['create', 'save']

   	static editPermissions = ['edit', 'update']

   	static deletePermissions = ['delete']

   	def index() {
   		redirect(action: "list", params: params)
   	}

   	def list(Integer max) {
   	}

   	def datatablesList() {
   		render agamaService.datatablesList(params) as JSON
   	}

   	def create() {
   		[agamaInstance: new Agama(params)]
   	}

   	def save() {
       		def agamaInstance = new Agama(params)
            agamaInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            agamaInstance.setLastUpdProcess("INSERT")
            agamaInstance?.setDateCreated(datatablesUtilService?.syncTime())
            agamaInstance?.setLastUpdated(datatablesUtilService?.syncTime())
            agamaInstance.setStaDel("0")
        def cek = Agama.createCriteria().list() {
            and{
                eq("m061NamaAgama",agamaInstance.m061NamaAgama?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(cek){
            flash.message = "Data sudah ada"
            agamaInstance.m061ID = null
            render(view: "create", model: [agamaInstance: agamaInstance])
            return
        }
        agamaInstance?.m061ID = generateCodeService.codeGenerateSequence("M061_ID",null)
           if (!agamaInstance.save(flush: true)) {
   			render(view: "create", model: [agamaInstance: agamaInstance])
   			return
   		}

   		flash.message = message(code: 'default.created.message', args: [message(code: 'agama.label', default: 'Agama'), agamaInstance.id])
   		redirect(action: "show", id: agamaInstance.id)
   	}

   	def show(Long id) {
   		def agamaInstance = Agama.get(id)
   		if (!agamaInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'agama.label', default: 'Agama'), id])
   			redirect(action: "list")
   			return
   		}

   		[agamaInstance: agamaInstance]
   	}

   	def edit(Long id) {
   		def agamaInstance = Agama.get(id)
   		if (!agamaInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'agama.label', default: 'Agama'), id])
   			redirect(action: "list")
   			return
   		}

   		[agamaInstance: agamaInstance]
   	}

   	def update(Long id, Long version) {
   		def agamaInstance = Agama.get(id)
//   		if (!agamaInstance) {
//   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'agama.label', default: 'Agama'), id])
//   			redirect(action: "list")
//   			return
//   		}
        String original = agamaInstance.m061NamaAgama
        agamaInstance.m061NamaAgama = params.m061NamaAgama
   		if (version != null) {
   			if (agamaInstance.version > version) {

   				agamaInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
   				[message(code: 'agama.label', default: 'Agama')] as Object[],
   				"Another user has updated this Religion while you were editing")
   				render(view: "edit", model: [agamaInstance: agamaInstance])
   				return
   			}
   		}
        def cek = Agama.createCriteria()
        def result = cek.list() {
            and{
                eq("m061NamaAgama",agamaInstance.m061NamaAgama?.trim(), [ignoreCase: true])
                eq("staDel","0")
            }
        }
        if(result){
            boolean isExist = false;
            for(Agama ag : result) {
                if (ag.m061ID != agamaInstance.m061ID) {
                    isExist = true
                }
            }
            if (isExist) {
                flash.message = "Agama " + agamaInstance.m061NamaAgama + " Sudah Ada"
                agamaInstance.m061NamaAgama = original
                render(view: "edit", model: [agamaInstance: agamaInstance])
                return
            }
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
   		agamaInstance.properties = params
           agamaInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
           agamaInstance.setLastUpdProcess("UPDATE")

           if (!agamaInstance.save(flush: true)) {
   			render(view: "edit", model: [agamaInstance: agamaInstance])
   			return
   		}

   		flash.message = message(code: 'default.updated.message', args: [message(code: 'agama.label', default: 'Agama'), agamaInstance.id])
   		redirect(action: "show", id: agamaInstance.id)
   	}

   	def delete(Long id) {
   		def agamaInstance = Agama.get(id)
   		if (!agamaInstance) {
   			flash.message = message(code: 'default.not.found.message', args: [message(code: 'agama.label', default: 'Agama'), id])
   			redirect(action: "list")
   			return
   		}

   		try {
               agamaInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
               agamaInstance.setLastUpdProcess("DELETE")
               agamaInstance.setStaDel("1")
               agamaInstance.save(flush: true)
               flash.message = message(code: 'default.deleted.message', args: [message(code: 'agama.label', default: 'Agama'), id])
   			redirect(action: "list")
   		}
   		catch (DataIntegrityViolationException e) {
   			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'agama.label', default: 'Agama'), id])
   			redirect(action: "show", id: id)
   		}
   	}

   	def massdelete() {
   		def res = [:]
   		try {
   			datatablesUtilService.massDeleteStaDelNew(Agama, params)
   			res.message = "Mass Delete Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}

   	def updatefield(){
   		def res = [:]
   		try {
   			datatablesUtilService.updateField(Agama, params)
   			res.message = "Update Success"
   		} catch (e) {
   			log.error(e.message, e)
   			res.message = e.message?:e.cause?.message
   		}
   		render "ok"
   	}

}