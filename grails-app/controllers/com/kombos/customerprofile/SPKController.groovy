package com.kombos.customerprofile

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.Company
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class SPKController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def generateCodeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = SPK.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {

            if (params."sCriteria_t191NoSPK") {
                ilike("t191NoSPK", "%" + (params."sCriteria_t191NoSPK" as String) + "%")
            }

            if (params."sCriteria_t191TanggalSPK") {
                ge("t191TanggalSPK", params."sCriteria_t191TanggalSPK")
                lt("t191TanggalSPK", params."sCriteria_t191TanggalSPK" + 1)
            }

            if (params."sCriteria_t191TglAwal") {
                ge("t191TglAwal", params."sCriteria_t191TglAwal")
                lt("t191TglAwal", params."sCriteria_t191TglAwal" + 1)
            }

            if (params."sCriteria_t191TglAkhir") {
                ge("t191TglAkhir", params."sCriteria_t191TglAkhir")
                lt("t191TglAkhir", params."sCriteria_t191TglAkhir" + 1)
            }

            if (params."sCriteria_t191JmlSPK") {
                eq("t191JmlSPK", Double.parseDouble(params."sCriteria_t191JmlSPK"))
            }

            if (params."sCriteria_t191MaxHariPelunasan") {
                eq("t191MaxHariPelunasan", Integer.parseInt(params."sCriteria_t191MaxHariPelunasan"))
            }

            if (params."sCriteria_staDel") {
                ilike("staDel", "%" + (params."sCriteria_staDel" as String) + "%")
            }

            if (params."sCriteria_createdBy") {
                ilike("createdBy", "%" + (params."sCriteria_createdBy" as String) + "%")
            }

            if (params."sCriteria_updatedBy") {
                ilike("updatedBy", "%" + (params."sCriteria_updatedBy" as String) + "%")
            }

            if (params."sCriteria_lastUpdProcess") {
                ilike("lastUpdProcess", "%" + (params."sCriteria_lastUpdProcess" as String) + "%")
            }

            if (params."sCriteria_company") {
                eq("company",Company.findByNamaPerusahaanIlike("%"+params."sCriteria_company"+"%"))
            }

            eq("staDel", "0")
            eq("companyDealer", session?.userCompanyDealer)

            switch (sortProperty) {
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []

        results.each {
            rows << [

                    id: it.id,

                    t191NoSPK: it?.t191NoSPK,

                    t191TanggalSPK: it?.t191TanggalSPK ? it?.t191TanggalSPK.format(dateFormat) : "",

                    t191TglAwal: it?.t191TglAwal ? it?.t191TglAwal.format(dateFormat) : "",

                    t191TglAkhir: it?.t191TglAkhir ? it?.t191TglAkhir.format(dateFormat) : "",

                    t191JmlSPK: "Rp " + it?.t191JmlSPK,

                    t191MaxHariPelunasan: it?.t191MaxHariPelunasan,

                    staDel: it?.staDel,

                    createdBy: it?.createdBy,

                    updatedBy: it.updatedBy,

                    lastUpdProcess: it?.lastUpdProcess,

                    company: it?.company?.namaPerusahaan,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [SPKInstance: new SPK(params)]
    }

    def save() {
        def SPKInstance = new SPK(params)
        SPKInstance?.t191IDSPK = generateCodeService.codeGenerateSequence("T191_IDSPK",session.userCompanyDealer)
        SPKInstance.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        SPKInstance.setLastUpdProcess("INSERT")
        SPKInstance?.setDateCreated(datatablesUtilService?.syncTime())
        SPKInstance?.setLastUpdated(datatablesUtilService?.syncTime())

        SPKInstance?.staDel = '0'

        if (!SPKInstance.save(flush: true)) {
            render(view: "create", model: [SPKInstance: SPKInstance])
            return
        }
        //menghapus code pada tabel urutbelumterpakai
        //generateCodeService.hapusCodeBelumTerpakai("T191_IDSPK",session.userCompanyDealer,companyInstance?.kodePerusahaan)

        flash.message = message(code: 'default.created.message', args: [message(code: 'SPK.label', default: 'SPK'), SPKInstance.id])
        redirect(action: "show", id: SPKInstance.id)
    }

    def show(Long id) {
        def SPKInstance = SPK.get(id)
        if (!SPKInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPK.label', default: 'SPK'), id])
            redirect(action: "list")
            return
        }

        [SPKInstance: SPKInstance]
    }

    def edit(Long id) {
        def SPKInstance = SPK.get(id)
        if (!SPKInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPK.label', default: 'SPK'), id])
            redirect(action: "list")
            return
        }

        [SPKInstance: SPKInstance]
    }

    def update(Long id, Long version) {
        def SPKInstance = SPK.get(id)
        if (!SPKInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPK.label', default: 'SPK'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (SPKInstance.version > version) {

                SPKInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'SPK.label', default: 'SPK')] as Object[],
                        "Another user has updated this SPK while you were editing")
                render(view: "edit", model: [SPKInstance: SPKInstance])
                return
            }
        }

        params.lastUpdated = datatablesUtilService?.syncTime()
        SPKInstance.properties = params
        SPKInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        SPKInstance.setLastUpdProcess("UPDATE")

        if (!SPKInstance.save(flush: true)) {
            render(view: "edit", model: [SPKInstance: SPKInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'SPK.label', default: 'SPK'), SPKInstance.id])
        redirect(action: "show", id: SPKInstance.id)
    }

    def delete(Long id) {
        def SPKInstance = SPK.get(id)
        if (!SPKInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'SPK.label', default: 'SPK'), id])
            redirect(action: "list")
            return
        }

        SPKInstance?.updatedBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        SPKInstance?.lastUpdProcess = "DELETE"
        SPKInstance.staDel = '1'

        try {
            SPKInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'SPK.label', default: 'SPK'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'SPK.label', default: 'SPK'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDeleteStaDel(SPK, params)
           // log.info("print berhasil")
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
          //  log.error("print gagal")
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(SPK, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}
