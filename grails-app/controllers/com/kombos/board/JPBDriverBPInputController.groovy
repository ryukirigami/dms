package com.kombos.board

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class JPBDriverBPInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def JPBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        params.tglHeader = sdf.format(new Date())

        [params: params]
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session?.userCompanyDealer
        render JPBService.datatablesListDriver(params) as JSON
    }
}
