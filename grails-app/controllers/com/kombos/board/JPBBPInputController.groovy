package com.kombos.board

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.ManPowerStall
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class JPBBPInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def JPBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [idApp : params?.appointmentId,idRecept : params?.noWo, staApp: params.staApp]
    }

    def perDay(Integer max) {
        params.mulai = ""
        params.selesai = ""
        if(params?.idReception){
            params.mulai = JPB.findByReception(Reception.findByT401NoWO(params?.idReception))?.t451TglJamPlan
            params.selesai = JPB.findByReception(Reception.findByT401NoWO(params?.idReception))?.t451TglJamPlanFinished
        }
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        params.sCriteria_tglView = sdf.format(new Date())
        [params: params]
    }

    def saveAppointment(){
        String rencana = params.tglRencana + " " + params.jamRencana
        String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
        def app = Appointment.get(params.idApp as Long)
        app?.t301TglJamRencana = new Date().parse('d-MM-yyyy HH:mm',rencana.toString())
        app?.t301TglJamPenyerahann = new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
        app?.t301TglJamPenyerahanSchedule = new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
        def cekApp = Appointment.createCriteria().list {
                ge("t301TglJamRencana",app?.t301TglJamRencana)
                le("t301TglJamRencana",app?.t301TglJamRencana + 1)
                jpb{
                    order("id","asc")
                }
        }
        def cekData = 1
        cekApp.each {
            if(cekData==it.jpbId){
                cekData++
            }else{
                return
            }
        }

        app?.jpb = JPB.findById(cekData)
        app?.asb = ASB.findById(cekData)
        app?.setLastUpdated(datatablesUtilService?.syncTime())
        app?.save(flush: true)
        app.errors.each{
            println it
        }
        render "ok"
    }

    def perWeek(Integer max) {

        DateFormat sdf = new SimpleDateFormat("MMMM yyyy")
        DateFormat dayFormat = new SimpleDateFormat("EEE")
        DateFormat dd = new SimpleDateFormat("d")
//        Date theDay = new Date()
        Date theDay = datatablesUtilService?.syncTime()
        params.lblTglView = sdf.format(theDay)

        int lblSenin
        int lblSelasa
        int lblRabu
        int lblKamis
        int lblJumat
        int lblSabtu
        int lblMinggu
        String theDayName = dayFormat.format(theDay)
        int day = Integer.parseInt(dd.format(theDay))
        if (theDayName.equalsIgnoreCase("MON") ) {
            lblSenin = day
            lblSelasa = day + 1
            lblRabu = day + 2
            lblKamis = day + 3
            lblJumat = day + 4
            lblSabtu = day + 5
            lblMinggu = day + 6
        } else if (theDayName.equalsIgnoreCase("TUE") ) {
            lblSenin = day - 1
            lblSelasa = day
            lblRabu = day + 1
            lblKamis = day + 2
            lblJumat = day + 3
            lblSabtu = day + 4
            lblMinggu = day + 5
        } else if (theDayName.equalsIgnoreCase("WED") ) {
            lblSenin = day - 2
            lblSelasa = day - 1
            lblRabu = day
            lblKamis = day + 1
            lblJumat = day + 2
            lblSabtu = day + 3
            lblMinggu = day + 4
        } else if (theDayName.equalsIgnoreCase("THU") ) {
            lblSenin = day - 3
            lblSelasa = day - 2
            lblRabu = day - 1
            lblKamis = day
            lblJumat = day + 1
            lblSabtu = day + 2
            lblMinggu = day + 3
        } else if (theDayName.equalsIgnoreCase("FRI") ) {
            lblSenin = day - 4
            lblSelasa = day - 3
            lblRabu = day - 2
            lblKamis = day - 1
            lblJumat = day
            lblSabtu = day + 1
            lblMinggu = day + 2
        } else if (theDayName.equalsIgnoreCase("SAT") ) {
            lblSenin = day - 5
            lblSelasa = day - 4
            lblRabu = day - 3
            lblKamis = day - 2
            lblJumat = day - 1
            lblSabtu = day
            lblMinggu = day + 1
        } else if (theDayName.equalsIgnoreCase("SUN") ) {
            lblSenin = day - 6
            lblSelasa = day - 5
            lblRabu = day - 4
            lblKamis = day - 3
            lblJumat = day - 2
            lblSabtu = day - 1
            lblMinggu = day
        }

        params.lblSenin = lblSenin
        params.lblSelasa = lblSelasa
        params.lblRabu = lblRabu
        params.lblKamis = lblKamis
        params.lblJumat = lblJumat
        params.lblSabtu = lblSabtu
        params.lblMinggu = lblMinggu
        [params: params]
    }

    def perMonth(Integer max) {
        DateFormat dayFormat = new SimpleDateFormat("d")
        DateFormat monthFormat = new SimpleDateFormat("MMMM")
        DateFormat moFormat = new SimpleDateFormat("M")
        DateFormat yearFormat = new SimpleDateFormat("yyyy")

//        Date theDay = new Date()
        Date theDay = datatablesUtilService?.syncTime()
        String sdt = dayFormat.format(theDay)
        int dt = Integer.parseInt(sdt)
        String month = monthFormat.format(theDay)
        params.month1 = month
        String smo = moFormat.format(theDay)
        int mo = Integer.parseInt(smo)
        params.month2 = (mo == 1) ? "February" : (mo == 2) ? "March" : (mo == 3) ? "April" : (mo == 4) ? "May" : (mo == 5) ? "June" : (mo == 6) ? "July" :
                (mo == 7) ? "August" : (mo == 8) ? "September" : (mo == 9) ? "October" : (mo == 10) ? "November" : "December";
        String syear = yearFormat.format(theDay)
        int year = Integer.parseInt(syear)
        params.year = "" + year

        int endMonth = 30
        if (month.equalsIgnoreCase("January") || month.equalsIgnoreCase("March") || month.equalsIgnoreCase("May") ||
                month.equalsIgnoreCase("July") || month.equalsIgnoreCase("August") || month.equalsIgnoreCase("October") ||
                month.equalsIgnoreCase("December")) {
            endMonth = 31
        } else if (month.equalsIgnoreCase("April") || month.equalsIgnoreCase("June") || month.equalsIgnoreCase("September") ||
                month.equalsIgnoreCase("November") ) {
            endMonth = 30
        } else {
            if (year % 4 == 0) endMonth = 29
            else endMonth = 28
        }

        int i = 0;
        int j = 0;
        params.date1 = dt
        i += 1
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date2 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date3 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date4 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date5 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date6 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date7 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date8 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date9 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date10 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date11 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date12 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date13 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date14 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date15 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date16 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date17 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date18 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date19 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date20 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date21 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date22 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date23 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date24 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date25 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date26 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date27 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date28 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date29 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date30 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date31 = dt

        params.colspanMonth1 = i
        params.colspanMonth2 = j
        [params: params]
    }

    def datatablesList() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        params.dari = "BPInput"
        render JPBService.datatablesList(params, companyDealer) as JSON
    }

    def datatablesListWeek() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        params.dari = "BPInput"
        render JPBService.datatablesListWeek(params,companyDealer) as JSON
    }

    def datatablesListMonth() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        params.dari = "BPInput"
        render JPBService.datatablesListMonth(params,companyDealer) as JSON
    }

    def create() {
        def result = JPBService.create(params)

        if (!result.error)
            return [JPBInstance: result.JPBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = JPBService.save(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["JPB", result.JPBInstance.id])
            redirect(action: 'show', id: result.JPBInstance.id)
            return
        }

        render(view: 'create', model: [JPBInstance: result.JPBInstance])
    }

    def show(Long id) {
        def result = JPBService.show(params)

        if (!result.error)
            return [JPBInstance: result.JPBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = JPBService.show(params)

        if (!result.error)
            return [JPBInstance: result.JPBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = JPBService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["JPB", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [JPBInstance: result.JPBInstance.attach()])
    }

    def delete() {
        def result = JPBService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["JPB", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(JPB, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def saveJPBPerday(){
        if(params?.idApp){

        }

        if(params?.idReception){
            def reception = Reception.findByT401NoWOAndStaDel(params?.idReception,"0")
            params.dateCreated = datatablesUtilService?.syncTime()
            params.lastUpdated = datatablesUtilService?.syncTime()
            def mulai = params.tglSet + " " + params.jamDatang + ":" + params.menitDatang
            def selese = params.tglSet + " " + params.jamPenyerahan + ":" + params.menitPenyerahan

            String awal = params?.tgglAwal +" "+params?.jamAwal+":"+params?.menitAwal
            String akhir = params?.tgglAkhir +" "+params?.jamAkhir+":"+params?.menitAKhir
            def stall = ManPowerStall.get(params.stall)
            def namaManPower = stall?.namaManPower
                def mps = ManPowerStall.findByNamaManPowerAndStaDel(namaManPower,"0")
                def jpb = new JPB()
                jpb?.companyDealer= reception?.companyDealer
                jpb?.reception= reception
                jpb?.t451TglJamPlan= new Date().parse('dd/MM/yyyy H:m', mulai)
                jpb?.t451TglJamPlanFinished= new Date().parse('dd/MM/yyyy H:m', selese)
                jpb?.namaManPower= namaManPower
                jpb?.stall= mps?.stall ? mps?.stall : stall?.stall
                jpb?.t451StaTambahWaktu= '1'
                jpb?.t451staOkCancelReSchedule= '1'
                jpb?.t451AlasanReSchedule= '1'
                jpb?.staDel= '0'
                jpb?.lastUpdProcess= "INSERT"
                jpb?.createdBy= org.apache.shiro.SecurityUtils.subject.principal.toString()
                jpb?.dateCreated = datatablesUtilService?.syncTime()
                jpb?.lastUpdated = datatablesUtilService?.syncTime()
                jpb?.save(flush: true)
                jpb?.errors?.each {println it}

            reception.t401TglJamJanjiPenyerahan = new Date().parse('dd-MM-yyyy H:m', akhir)
            reception?.t401TglJamRencana = new Date().parse('dd-MM-yyyy H:m', awal)
            reception?.lastUpdated = datatablesUtilService?.syncTime()
            reception?.save(flush: true)


        render "ok"
        }
    }

    def deleteJPB(){
        def status = "ok"
        if(params?.noWO){
            def recept = Reception.findByT401NoWOIlike("%"+params?.noWO+"%");
            if(recept){
                def plot = new Date().parse("dd/MM/yyyy HH:mm", params.plotDays+" 00:00")
//                DateFormat df = new SimpleDateFormat("dd-MMM-yy")
//                def dateDelete = df.parse(params.plotDays)

                def jpbs = JPB.findAllByReceptionAndStaDelAndT451TglJamPlanGreaterThanEqualsAndT451TglJamPlanLessThan(recept , "0", plot, plot+1);
                jpbs.each {
                    it?.staDel = "1"
                    it?.save(flush: true);
                    if(it?.hasErrors()){
                        status = "gagal";
                        println it?.errors?.each {"JPB "+it}
                    }
                }
            }
        }
        render status
    }
}
