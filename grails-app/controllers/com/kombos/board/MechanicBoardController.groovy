package com.kombos.board

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KategoriJob
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class MechanicBoardController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def JPBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        DateFormat sdf = new SimpleDateFormat("EEEE, dd MMMM yyyy");
//        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        Date today = datatablesUtilService?.syncTime()
        params.tglHeader = sdf.format(today)
        def listKJob = []
        def listWarna = []
        KategoriJob.createCriteria().list {order("m055KategoriJob")}.each {
            listKJob << "V1"+it?.m055KategoriJob
            listKJob << "V"+it?.m055KategoriJob
            listWarna << it?.m055WarnaBord
            listWarna << it?.m055WarnaBord
        }
        [params: params,listKategori : listKJob, listWarna : listWarna]
    }

    def datatablesList() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer){
            companyDealer = CompanyDealer.get(params?.input_companyDealer?.toLong())
        }
        render JPBService.datatablesListMechanicalBoard(params, companyDealer) as JSON
    }

    def create() {
        def result = ASBService.create(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = ASBService.save(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["ASB", result.ASBInstance.id])
            redirect(action: 'show', id: result.ASBInstance.id)
            return
        }

        render(view: 'create', model: [ASBInstance: result.ASBInstance])
    }

    def show(Long id) {
        def result = ASBService.show(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = ASBService.show(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = ASBService.update(params)
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["ASB", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [ASBInstance: result.ASBInstance.attach()])
    }

    def delete() {
        def result = ASBService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["ASB", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(ASB, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}

