package com.kombos.board

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FlatRate
import com.kombos.administrasi.ManPowerStall
import com.kombos.administrasi.Operation
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.JobApp
import com.kombos.reception.Appointment
import grails.converters.JSON
import groovy.time.TimeCategory

import java.text.DateFormat
import java.text.SimpleDateFormat

class ASBController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def ASBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [params: params]
    }

    def perDay(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        Date today = datatablesUtilService?.syncTime()
        params.tglHeader = sdf.format(today)
        [params: params]
    }

    def saveAppointment(){
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        String rencana = params.tglRencana + " " + params.jamRencana
        String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
        def app = Appointment.get(params.idApp as Long)
        app?.t301TglJamRencana = new Date().parse('d-MM-yyyy HH:mm',rencana.toString())
        app?.t301TglJamPenyerahann = new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
        app?.t301TglJamPenyerahanSchedule = new Date().parse('d-MM-yyyy HH:mm',penyerahan.toString())
        def cekApp = Appointment.createCriteria().list {
            ge("t301TglJamRencana",app?.t301TglJamRencana)
            le("t301TglJamRencana",app?.t301TglJamRencana + 1)
            asb{
                order("id","asc")
            }
        }
        def cekData = 1
        cekApp.each {
            if(cekData==it?.asbId){
                cekData++
            }else{
                return
            }
        }

        app?.asb = ASB.findById(cekData)
        app?.jpb = JPB.findById(cekData)
        app?.save(flush: true)
        app.errors.each{
            println it
        }
        render "ok"
    }

    String fixTime(String time){
        String waktu = ""
        int jum = time.trim().length()
        if(jum<=2){
            if(jum==1){
                waktu = "0"+time.trim()+":00"
            }else{
                waktu = time.trim()+":00"
            }
        }else {
            if(jum==3){
                waktu = "0"+time.charAt(0)+":"+time.substring(1)
            }else{
                waktu = time.substring(0,2)+":"+time.substring(2)
            }
        }
        return waktu
    }

    def saveASB(){
        def app = Appointment.get(params.idApp as Long)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        Date dateAwal = null;
        Date dateAkhir = null;
        def asbTemp = null;
        if(params?.saveBy=="own"){
            dateAwal = new Date().parse("dd/MM/yyyy H:m",params?.tanggal+" "+params?.jamRencana)
            use(TimeCategory){
                dateAkhir = new Date().parse("dd/MM/yyyy H:m",params?.tanggal+" "+params?.jamPenyerahan) + 15.minutes
            }

            def mps = ManPowerStall.get(params?.stallid?.toLong())
            ASB asb = new ASB(
                    companyDealer: session?.userCompanyDealer,
                    dateCreated : datatablesUtilService?.syncTime(),
                    lastUpdated : datatablesUtilService?.syncTime(),
                    reception: app?.reception,
                    appointment: app,
                    t351TglJamApp: dateAwal,
                    t351TglJamFinished: dateAkhir,
                    namaManPower: mps?.namaManPower,
                    stall: mps?.stall,
                    t351StaOkCancelReSchedule: '1', t351StaBookingWeb: '1', t351OkBookingWeb: '1',
                    staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString()
            )

            asb.save(flush:true)
            asb.errors.each {
                println it
            }
            asbTemp = asb
        }else{
            def jsonTanggal = JSON.parse(params.tanggal)
            def jsonStart = JSON.parse(params.jamRencana)
            def jsonStop = JSON.parse(params.jamPenyerahan)
            def jsonStall = JSON.parse(params.stallid)
            for(int a=0;a<jsonTanggal.size();a++){
                String rencana =  jsonTanggal[a] + " " + fixTime(jsonStart[a].substring(3))
                String penyerahan =  jsonTanggal[a] + " " + fixTime(jsonStop[a].substring(3))
                FlatRate r = null;
                Operation o = null;
                def jobApps = JobApp.findAllByReception(app.reception)
                jobApps.each { JobApp job ->
                    if (job.operation && app.historyCustomerVehicle?.fullModelCode?.baseModel) {
                        r = FlatRate.findByOperation(job.operation)
                    }
                }

                ManPowerStall mps = ManPowerStall.get(jsonStall[a] as Long)
                use( TimeCategory ) {
                    ASB asb = new ASB(
                            companyDealer: session?.userCompanyDealer,
                            dateCreated : datatablesUtilService?.syncTime(),
                            lastUpdated : datatablesUtilService?.syncTime(),
                            reception: app?.reception,
                            appointment: app,
                            t351TglJamApp: new Date().parse('dd/MM/yyyy HH:mm',rencana.toString()),
                            t351TglJamFinished: new Date().parse('dd/MM/yyyy HH:mm',penyerahan.toString())+15.minutes,
                            operation:o,
                            namaManPower: mps?.namaManPower,
                            stall: mps?.stall,
                            flatRate: r,
                            t351StaOkCancelReSchedule: '1', t351StaBookingWeb: '1', t351OkBookingWeb: '1',
                            staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString()
                    )
                    asb.save(flush:true)
                    asb.errors.each {
                        println it
                    }
                    asbTemp = asb

                    if(a==0){
                        dateAwal = new Date().parse('dd/MM/yyyy HH:mm',rencana.toString())
                    }
                    if(a+1==jsonTanggal.size()){
                        dateAkhir = new Date().parse('dd/MM/yyyy HH:mm',penyerahan.toString()) + 15.minutes
                    }
                }
            }
        }

       app?.t301TglJamRencana = dateAwal
        use( TimeCategory ) {
            app?.t301TglJamPenyerahann = dateAkhir
            app?.t301TglJamPenyerahanSchedule = dateAkhir
        }
        app.asb = asbTemp
        app?.lastUpdated = datatablesUtilService?.syncTime()
        app.t301TglJamApp = dateAwal
        app?.save(flush: true)
        app.errors.each{
            println it
        }
        render "ok"
    }

    def dssAppointment(){
        CompanyDealer companyDealer = session?.userCompanyDealer
        render ASBService.dssAppointment(params, companyDealer) as JSON
    }

    def dssAppointment3(){
        CompanyDealer companyDealer = session?.userCompanyDealer
        render ASBService.dssAppointment3(params, companyDealer) as JSON
    }

    def perThreeDays(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        Date today = datatablesUtilService?.syncTime()
        params.tglHeader = sdf.format(today)
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.DATE, 1);
        Date tomorrow = c.getTime();
        c.add(Calendar.DATE, 1);
        Date dayAfterTomorrow = c.getTime();

        params.tglHeaderPlusOne = sdf.format(tomorrow)
        params.tglHeaderPlusTwo = sdf.format(dayAfterTomorrow)
        [params: params]
    }
    def datatablesList() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer){
            companyDealer = CompanyDealer.get(params?.input_companyDealer?.toLong())
        }
        render ASBService.datatablesList(params, companyDealer) as JSON
    }

    def datatablesListPlusZero() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        render ASBService.datatablesListPlusZero(params, companyDealer) as JSON
    }
    def datatablesListPlusOne() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        render ASBService.datatablesListPlusOne(params, companyDealer) as JSON
    }
    def datatablesListPlusTwo() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        render ASBService.datatablesListPlusTwo(params, companyDealer) as JSON
    }
    def create() {
        def result = ASBService.create(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = ASBService.save(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["ASB", result.ASBInstance.id])
            redirect(action: 'show', id: result.ASBInstance.id)
            return
        }

        render(view: 'create', model: [ASBInstance: result.ASBInstance])
    }

    def show(Long id) {
        def result = ASBService.show(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = ASBService.show(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = ASBService.update(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["ASB", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [ASBInstance: result.ASBInstance.attach()])
    }

    def delete() {
        def result = ASBService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["ASB", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(ASB, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

//    IAN
    def Reschedule(){
//        ====== untuk Menghapus =====
        def app = Appointment.get(params.idApp as Long)
        def jsonTanggal = JSON.parse(params.tanggal)
        def jsonStart = JSON.parse(params.jamRencana)
        def jsonStop = JSON.parse(params.jamPenyerahan)
        def jsonStall = JSON.parse(params.stallid)
        Date dateAwal = null;
        Date dateAkhir = null;
        def asbTemp = null;
        for(int a=0;a<jsonTanggal.size();a++){
            String rencana =  jsonTanggal[a] + " " + fixTime(jsonStart[a].substring(3))
            String penyerahan =  jsonTanggal[a] + " " + fixTime(jsonStop[a].substring(3))
            FlatRate r = null;
            Operation o = null;
            def jobApps = JobApp.findAllByReception(app.reception)
            jobApps.each { JobApp job ->
                if (job.operation && app.historyCustomerVehicle?.fullModelCode?.baseModel) {
                    r = FlatRate.findByOperation(job.operation)
                }
            }

            ManPowerStall mps = ManPowerStall.get(jsonStall[a] as Long)
            use( TimeCategory ) {
                ASB asb = new ASB(
                        companyDealer: session?.companyDealer,
                        reception: app?.reception,
                        appointment: app,
                        t351TglJamApp: new Date().parse('dd/MM/yyyy HH:mm',rencana.toString()),
                        t351TglJamFinished: new Date().parse('dd/MM/yyyy HH:mm',penyerahan.toString())+15.minutes,
                        operation:o,
                        namaManPower: mps?.namaManPower,
                        stall: mps?.stall,
                        flatRate: r,
                        t351StaOkCancelReSchedule: '1', t351StaBookingWeb: '1', t351OkBookingWeb: '1',
                        staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(), dateCreated: datatablesUtilService?.syncTime(), lastUpdated: datatablesUtilService?.syncTime()
                )
//                asb.save(flush:true)
                asb.delete(flush:true)
                asb.errors.each {
                    println it
                }
                asbTemp = asb

                if(a==0){
                    dateAwal = new Date().parse('dd/MM/yyyy HH:mm',rencana.toString())
                }
                if(a+1==jsonTanggal.size()){
                    dateAkhir = new Date().parse('dd/MM/yyyy HH:mm',penyerahan.toString()) + 15.minutes
                }
            }
        }
//        String rencana = params.tanggal + " " + params.jamRencana
//        String penyerahan = params.tanggal + " " + params.jamPenyerahan

        app?.t301TglJamRencana = dateAwal
        use( TimeCategory ) {
            app?.t301TglJamPenyerahann = dateAkhir
            app?.t301TglJamPenyerahanSchedule = dateAkhir
        }
        app.asb = asbTemp
        app.t301TglJamApp = dateAwal
        app.t301StaOkCancelReSchedule ="2"
        app?.lastUpdated = datatablesUtilService?.syncTime()
        app?.save(flush: true)
        app.errors.each{
            println it
        }
        render "ok"

    }
//    IAN
}
