package com.kombos.board

import com.kombos.customerprofile.CustomerVehicle
import com.kombos.customerprofile.HistoryCustomerVehicle
import com.kombos.maintable.AntriCuci
import com.kombos.maintable.InvoiceT701
import com.kombos.maintable.JOCTECO
import com.kombos.maintable.Settlement
import com.kombos.maintable.StatusActual
import com.kombos.parts.PickingSlip
import com.kombos.production.FinalInspection
import com.kombos.reception.CustomerIn
import com.kombos.reception.Reception
import com.kombos.woinformation.Actual
import grails.converters.JSON
import org.hibernate.Criteria
import org.hibernate.criterion.CriteriaSpecification

class VehicleTrackingStatusBoardController {

    def datatablesUtilService
    def index() {
    }

    def boardDataTable() {
        session.exportParams=params
        render boardList(params) as JSON
    }

    def boardList(params) {
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0
        def angka = params?.indMulai?.toInteger();
        def c = CustomerIn.createCriteria()
        def results = c.list (max: params.iDisplayLength as int, offset: angka) {
            eq("companyDealer",session?.userCompanyDealer)
            ge("dateCreated", new Date().clearTime())
            tujuanKedatangan{
                or{
                    ilike("m400Tujuan","%GR%")
                    ilike("m400Tujuan","%BP%")
                }
            }
            createAlias("reception", "reception", Criteria.LEFT_JOIN)
            or{
                isNull("reception");
                isNull("reception.isPending")
//                isNull("reception.t401StaInvoice")
            }
            order("t400NomorAntrian");
        }

        if(results?.size()<1){
            angka = 0
            results = CustomerIn.createCriteria().list (max: params.iDisplayLength as int, offset: angka) {
                eq("companyDealer",session?.userCompanyDealer)
                ge("dateCreated", new Date().clearTime())
                tujuanKedatangan{
                    or{
                        ilike("m400Tujuan","%GR%")
                        ilike("m400Tujuan","%BP%")
                    }
                }
                createAlias("reception", "reception", Criteria.LEFT_JOIN)
                or{
                    isNull("reception")
                    isNull("reception.isPending")
                }
                order("t400NomorAntrian");
            }
        }

        angka+=10

        def rows = []
        results.each {
            CustomerVehicle cv = it?.customerVehicle
            def hcv = HistoryCustomerVehicle.createCriteria().list {
                eq("customerVehicle", cv)
            }

            String np = null
            if (hcv != null && hcv.size() > 0) {
                np = hcv?.last()?.fullNoPol
            }

            if(!it?.customerVehicle){
                np = it?.t400noPol
            }

            String prosesService = "-"
            String finalInspection = "-"
            String receive = "-"
            String tungguAntrian = "-"
            String servicePlus = "-"
            String dokumen = "-"
            String callCustomer = "-"
            String janjiPenyerahan = "-"
            String tgguService = "-"
            String finish = "-"
            Reception r = it.reception
            if (r != null) {
                def tampil = true;
                def invoice = InvoiceT701.findByReceptionAndT701StaDel(r,'0')
                def settlement = Settlement.findByReceptionAndStaDel(r,'0')
                if(settlement){
                    finish = '0'
                }else{
                    if(invoice){
                       if((datatablesUtilService.syncTime().time - invoice?.dateCreated.time) > 1100000){
                            finish = '0'
                            try {
                                def not = Reception.get(r?.id)
                                not?.isPending = "1"
                                not?.save(flush: true)
                            }catch (Exception e){

                            }
                        }else if((datatablesUtilService.syncTime().time - invoice?.dateCreated.time) > 850000){
                            finish = '0'
                        }else{
                            callCustomer = '0'
                        }

                    }else{
                        def picking = PickingSlip.findByReceptionAndStaDel(r,'0')
                        if(picking || it.reception.t401StaInvoice){
                            dokumen = '0'
                        }else{
                            def cuci = AntriCuci.createCriteria().list {
                                eq("reception",r)
                            }
                            if(cuci.size()>0){
                                servicePlus = '0'
                            }else{
                                def finalIns = FinalInspection.findByReception(r)
                                def cariSelesiService = Actual.findByReceptionAndStatusActualAndStaDel(r,StatusActual.findByM452StatusActualIlike("%Clock Off%"),'0')
                                if(finalIns || cariSelesiService){
                                    finalInspection = '0'
                                }else{
                                    def cariMulaiService = Actual.findByReceptionAndStatusActualAndStaDel(r,StatusActual.findByM452StatusActualIlike("%Clock On%"),'0')
                                    if(cariMulaiService){
                                        prosesService = '0'
                                    }else{
                                        def cekjpb = JPB.findByReceptionAndStaDel(r,'0')
                                        if(cekjpb){
                                            tgguService = '0'
                                        }else{
                                            receive = '0'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                janjiPenyerahan = r?.t401TglJamJanjiPenyerahan ? r.t401TglJamJanjiPenyerahan.format("dd/MMMM HH:mm") : "";
            }else{
                if(it?.loket){
                    receive = "0";
                }else{
                    tungguAntrian = "0"
                }
            }
            def car = "avanza.png";
            try {
                def baseMod = it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.m102NamaBaseModel.toLowerCase()
                if(baseMod.contains("avanza")){
                    car = "avanza.png"
                }else if(baseMod.contains("dyna")){
                    car = "dyna.png"
                }else if(baseMod.contains("yaris")){
                    car = "yaris.png"
                }else if(baseMod.contains("veloz")){
                    car = "veloz.png"
                }else if(baseMod.contains("innova")){
                    car = "innova.png"
                }else if(baseMod.contains("agya")){
                    car = "agya.png"
                }else if(baseMod.contains("rush") || baseMod.contains("fortuner")){
                    car = "rush.png"
                }else if(baseMod.contains("hilux")){
                    car = "hilux.png"
                }else if(baseMod.contains("sienta")){
                    car = "sienta.png"
                }else if(baseMod.contains("camry") || baseMod.contains("corolla") || baseMod.contains("vios")
                        || baseMod.contains("altis") || baseMod.contains("limo")){
                    car = "camry.png"
                }else if(baseMod.contains("alphard")){
                    car = "alphard.png"
                }else if(baseMod.contains("calya")){
                    car = "calya.png"
                }else if(it?.reception?.historyCustomerVehicle?.fullModelCode?.baseModel?.kategoriKendaraan?.m101NamaKategori.toUpperCase().contains("OLD MODEL")){
                    car = "kijang.png"
                }
            }catch (Exception e){}

            rows <<
                    [
                            id: it.id,
                            car: car,
                            noPolisi: np,
                            tungguAntrian: tungguAntrian,
                            receive: receive,
                            tungguService: tgguService,
                            prosesService: prosesService,
                            finalInspection: finalInspection,
                            servicePlus: servicePlus,
                            dokumen: dokumen,
                            callCustomer: callCustomer,
                            finish: finish,
                            janjiPenyerahan: janjiPenyerahan
                    ]
        }

        [sEcho: params.sEcho, iTotalRecords:  results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows,indMulai : angka]
    }
}
