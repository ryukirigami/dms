package com.kombos.board

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.FlatRate
import com.kombos.administrasi.ManPowerStall
import com.kombos.administrasi.Operation
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.maintable.CustLamaWeb
import com.kombos.maintable.JobApp
import com.kombos.reception.Appointment
import com.kombos.reception.Reception
import com.kombos.woinformation.JobRCP
import grails.converters.JSON
import groovy.time.TimeCategory

import java.text.DateFormat
import java.text.SimpleDateFormat

class JPBGRInputController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def JPBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        String message = "",message2="",message3="";
        if (params.staReception == '1') {
            def jpb = JPB.findByReceptionAndStaDel(Reception.findByT401NoWO(params.noWo),"0")
            if (jpb) {
                params.appointmentId = Appointment.findByJpb(jpb)?.id
                if (!params.appointmentId) {
                    def appoi = Appointment.findByReception(Reception.findByT401NoWO(params.noWo))
                    params.appointmentId = appoi ? appoi.id : ""
                }
            }
        }
        def countJPB = 0;
        if(params?.noWo){
            def recept = Reception.findByT401NoWO(params.noWo)
            if(recept){
                countJPB = recept?.countJPB
                if(countJPB>0){
                    def jpb = JPB.findByReceptionAndStaDel(recept,"0");
                    if(jpb){
                        String namaTeknisi = jpb?.namaManPower?.t015NamaBoard
                        String jamMulai = jpb?.t451TglJamPlan?.format("HH:mm")
                        message = "Sudah ada data JPB untuk kendaraan tersebut"
                        message2 = "Nama Teknisi : "+namaTeknisi+" Jam Mulai : "+jamMulai+""
                        message3 = "Klik OK untuk melanjutkan proses, atau klik tombol clear JPB untuk menghapus data JPB sebelumnya"
                    }
                }
            }
        }
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        params.sCriteria_tglView = sdf.format(new Date());
        [params: params, countJPB : countJPB,pesan : message,pesan2 : message2, pesan3 : message3]
    }

    def dssAppointment() {
        CompanyDealer companyDealer = session?.userCompanyDealer
        render JPBService.dssAppointment(params, companyDealer) as JSON
    }

    def datatablesList() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        params.dari = "GRInput"
        render JPBService.datatablesList(params, companyDealer) as JSON
    }


    String fixTime(String time){
        String waktu = ""
        int jum = time.trim().length()
        if(jum<=2){
            if(jum==1){
                waktu = "0"+time.trim()+":00"
            }else{
                waktu = time.trim()+":00"
            }
        }else {
            if(jum==3){
                waktu = "0"+time.charAt(0)+":"+time.substring(1)
            }else{
                waktu = time.substring(0,2)+":"+time.substring(2)
            }
        }
        return waktu
    }

    def saveJPB() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        Date dateAwal = null;
        Date dateAkhir = null;
        def asbTemp = null;
        FlatRate r = null
        Operation o = null;
        CompanyDealer cd = session.userCompanyDealer;
        Reception rec = null
        Appointment app = null
        Date tempPenyerahan = new Date()
        if (params.noWo) {
            rec = Reception.findByT401NoWO(params.noWo)
            if (rec) {
                def jobRcp = JobRCP.findAllByReceptionAndStaDel(rec, "0")
                jobRcp.each { JobRCP job ->
                    if (job.operation && rec.historyCustomerVehicle?.fullModelCode?.baseModel) {
                        o = job.operation
                        r = FlatRate.findByOperationAndBaseModel(job.operation,rec.historyCustomerVehicle?.fullModelCode?.baseModel)
                    }
                }
            }
        } else {
            app = Appointment.get(params.idApp as Long)
            def jobApps = JobApp.findAllByReception(app.reception)
            jobApps.each { JobApp job ->
                if (job.operation && app.historyCustomerVehicle?.fullModelCode?.baseModel) {
                    r = FlatRate.findByOperationAndBaseModel(job.operation,app.historyCustomerVehicle?.fullModelCode?.baseModel)
                }
            }
            rec = app?.reception

        }
        if(params?.saveBy=="own"){
            dateAwal = new Date().parse("dd/MM/yyyy H:m",params?.tanggal+" "+params?.jamRencana)
            use(TimeCategory){
                dateAkhir = new Date().parse("dd/MM/yyyy H:m",params?.tanggal+" "+params?.jamPenyerahan) + 15.minutes
            }
            def dateAkhirRencana = new Date().parse("dd-MM-yyyy HH:mm",params.planDeliverDate+" "+params.planDeliverHour)
            if(dateAkhirRencana.format("ddMMyyyyhhmm")==new Date().clearTime().format("ddMMyyyyhhmm")){
                dateAkhirRencana = dateAkhir
            }
            def mps = ManPowerStall.get(params?.stallid?.toLong())
            JPB jpb = new JPB(
                    companyDealer: cd,
                    reception: rec,
                    t451TglJamPlan: dateAwal,
                    t451TglJamPlanFinished: dateAkhir,
                    namaManPower: mps?.namaManPower,
                    stall: mps?.stall,
                    flatRate: r,
                    t451StaTambahWaktu: '1',
                    t451staOkCancelReSchedule: '1',
                    t451AlasanReSchedule: '1',
                    staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(),
                    dateCreated: datatablesUtilService?.syncTime(), lastUpdated: datatablesUtilService?.syncTime()
            ).save(flush: true)
            rec?.t401TglJamRencana = dateAwal
            rec?.t401TglJamJanjiPenyerahan = dateAkhirRencana
        }else{
            def jsonTanggal = JSON.parse(params.tanggal)
            def jsonStart = JSON.parse(params.jamRencana)
            def jsonStop = JSON.parse(params.jamPenyerahan)
            def jsonStall = JSON.parse(params.stallid)
            for(int a=0;a<jsonTanggal.size();a++){
                String rencana =  jsonTanggal[a] + " " + fixTime(jsonStart[a].substring(3))
                String penyerahan =  jsonTanggal[a] + " " + fixTime(jsonStop[a].substring(3))

                ManPowerStall mps = ManPowerStall.get(jsonStall[a] as Long)

                use( TimeCategory ) {
                    JPB jpb = new JPB(
                            companyDealer: cd,
                            reception: rec,
                            t451TglJamPlan: new Date().parse('dd/MM/yyyy HH:mm', rencana.toString()),
                            t451TglJamPlanFinished: new Date().parse('dd/MM/yyyy HH:mm', penyerahan.toString()) + 15.minutes,
                            namaManPower: mps?.namaManPower,
                            stall: mps?.stall,
                            flatRate: r,
                            t451StaTambahWaktu: '1',
                            t451staOkCancelReSchedule: '1',
                            t451AlasanReSchedule: '1',
                            staDel: '0', lastUpdProcess: "INSERT", createdBy: org.apache.shiro.SecurityUtils.subject.principal.toString(), dateCreated: datatablesUtilService?.syncTime(), lastUpdated: datatablesUtilService?.syncTime()
                    )

                    asbTemp = jpb

                    if(a==0){
                        dateAwal = new Date().parse('dd/MM/yyyy HH:mm',rencana.toString())
                    }
                    if(a+1==jsonTanggal.size()){
                        dateAkhir = new Date().parse('dd/MM/yyyy HH:mm',penyerahan.toString()) + 15.minutes
                    }

                    if (rec) {
                        jpb.reception = rec
                        jpb.save(flush: true)
                        jpb.errors.each {
                            println it
                        }
                    } else if (app) {
                        jpb.addToAppointments(app)
                        jpb.save(flush: true)
                        jpb.errors.each {
                            println it
                        }
                        app.jpb = jpb
                        app?.save(flush: true)
                        app.errors.each {
                            println it
                        }

                        rec = app.reception
                    }
                }
            }

            if(params?.nowo){
                rec.t401TglJamRencana = new Date().parse("dd-MM-yyyy HH:mm",params.planStartDate+" "+params.planStartHour)
                rec.t401TglJamJanjiPenyerahan = new Date().parse("dd-MM-yyyy HH:mm",params.planDeliverDate+" "+params.planDeliverHour)
            }else{
                app?.t301TglJamRencana = new Date().parse("dd-MM-yyyy HH:mm",params.planStartDate+" "+params.planStartHour)
                app?.t301TglJamPenyerahann = new Date().parse("dd-MM-yyyy HH:mm",params.planDeliverDate+" "+params.planDeliverHour)
                if (rec) {
                    rec.t401TglJamRencana = new Date().parse("dd-MM-yyyy HH:mm",params.planStartDate+" "+params.planStartHour)
                    rec.t401TglJamJanjiPenyerahan = new Date().parse("dd-MM-yyyy HH:mm",params.planDeliverDate+" "+params.planDeliverHour)
                }
            }
        }

        if (rec) {
            rec?.save(flush: true)
            rec.errors.each {
                println it
            }
            def custLW = CustLamaWeb.findByReception(rec)
            if(custLW){
                custLW?.t188EstimasiWaktuService = dateAkhir
                custLW?.save(flush: true)
            }
        }
        render "ok"
    }

    def saveAppointment() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        String rencana = params.tglRencana + " " + params.jamRencana
        String penyerahan = params.tglPenyerahan + " " + params.jamPenyerahan
        def app = Appointment.get(params.idApp as Long)
        app?.t301TglJamRencana = new Date().parse('d-MM-yyyy HH:mm', rencana.toString())
        app?.t301TglJamPenyerahann = new Date().parse('d-MM-yyyy HH:mm', penyerahan.toString())
        app?.t301TglJamPenyerahanSchedule = new Date().parse('d-MM-yyyy HH:mm', penyerahan.toString())
        def cekApp = Appointment.createCriteria().list {
            ge("t301TglJamRencana", app?.t301TglJamRencana)
            le("t301TglJamRencana", app?.t301TglJamRencana + 1)
            asb {
                order("id", "asc")
            }
        }
        def cekData = 1
        cekApp.each {
            if (cekData == it.asbId) {
                cekData++
            } else {
                return
            }
        }

        app?.asb = ASB.findById(cekData)
        app?.jpb = JPB.findById(cekData)
        app?.save(flush: true)
        app.errors.each {
            println it
        }
        render "ok"
    }

    def create() {
        def result = JPBService.create(params)

        if (!result.error)
            return [JPBInstance: result.JPBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = JPBService.save(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["JPB", result.JPBInstance.id])
            redirect(action: 'show', id: result.JPBInstance.id)
            return
        }

        render(view: 'create', model: [JPBInstance: result.JPBInstance])
    }

    def show(Long id) {
        def result = JPBService.show(params)

        if (!result.error)
            return [JPBInstance: result.JPBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = JPBService.show(params)

        if (!result.error)
            return [JPBInstance: result.JPBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = JPBService.update(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["JPB", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [JPBInstance: result.JPBInstance.attach()])
    }

    def delete() {
        def result = JPBService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["JPB", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(JPB, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def deleteJPB(){
        def status = "ok"
        if(params?.noWO){
            def recept = Reception.findByT401NoWOIlike("%"+params?.noWO+"%");
            if(recept){
                def jpbs = JPB.findAllByReceptionAndStaDel(recept , "0");
                jpbs.each {
                    it?.staDel = "1"
                    it?.save(flush: true);
                    if(it?.hasErrors()){
                        status = "gagal";
                        println it?.errors?.each {"JPB "+it}
                    }
                }
            }
        }
        render status
    }
}