package com.kombos.board

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.reception.Appointment
import com.kombos.reception.CustomerIn
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class TodayAppointmentBoardController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def JPBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        params.tglHeader = sdf.format(new Date())

        [params: params]
    }

    def bp(Integer max) {
        // Dapetin tanggal hari ini per jam 00:00:00
        // Karena kalo new date aja bisa kena di waktu nya sehingga
        // Data ga muncul
        Calendar today = new GregorianCalendar()
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);



        def listCustomerNoShow = Appointment.createCriteria().list {
         ge("t301TglJamApp",today.getTime())

         //lt("t301TglJamApp",today.getTime() + 1)
            eq("t301StaOkCancelReSchedule","0")
        asb{
                operation{
                    kategoriJob{
                        eq("m055KategoriJob","BP")
                    }
                }
            }
        }




        def listCustomerShow = Appointment.createCriteria().list {
            ge("t301TglJamApp",today.getTime())
           // lt("t301TglJamApp",today.getTime() + 1)
            eq("t301staBookingWalkIn","0")
            asb{
                operation{
                    kategoriJob{
                        eq("m055KategoriJob","BP")
                    }
                }
            }
        }

        [params: params, customerNoShowBP: listCustomerNoShow.size(), customerShowBP: listCustomerShow.size()]
    }

    def gr(Integer max) {
        // Dapetin tanggal hari ini per jam 00:00:00
        // Karena kalo new date aja bisa kena di waktu nya sehingga
        // Data ga muncul
        Calendar today = new GregorianCalendar()
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        def waktuSekarang = df.format(new Date())
        Date dateAwal = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 00:00")
        Date dateAkhir = new Date().parse("dd/MM/yyyy HH:mm",waktuSekarang+" 24:00")

        def listNoShow = Appointment.createCriteria().list {
            ge("t301TglJamRencana",dateAwal)
            le("t301TglJamRencana",dateAkhir)
        }
        def jumNoShow = 0
        def jumShow = 0
        listNoShow.each {
            def temp = it?.historyCustomerVehicle?.customerVehicle
            def cekCustIn = CustomerIn.createCriteria().list {
                ge("dateCreated",dateAwal)
                le("dateCreated",dateAkhir)
                eq("customerVehicle",temp)
            }
            if(cekCustIn.size()<1){
                if(it?.t301TglJamRencana?.before(new Date())){
                    jumNoShow+=1
                }
            }else{
                jumShow+=1
            }
        }
        [params: params, customerNoShowGR: jumNoShow, customerShowGR: jumShow]
    }

    def datatablesListBp() {
        session.exportParams = params
//        params.companyDealer = session?.userCompanyDealer
        CompanyDealer companyDealer = session?.userCompanyDealer
        render JPBService.datatablesListTabBP(params, companyDealer) as JSON
    }

    def datatablesListGr() {
        session.exportParams = params
//        params.companyDealer = session?.userCompanyDealer
        CompanyDealer companyDealer = session?.userCompanyDealer
        render JPBService.datatablesListTab(params, companyDealer) as JSON
    }

    def vehicleTrackingStatusBoard() {
    }
}