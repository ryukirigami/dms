package com.kombos.board

import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.KategoriJob
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class ASBGRViewController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def ASBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        [params: params]
    }

    def perDay(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
//        Date today = new Date()
        Date today = datatablesUtilService?.syncTime()
        params.tglHeader = sdf.format(today)
        def listKJob = []
        def listWarna = []
        KategoriJob.createCriteria().list {order("m055KategoriJob")}.each {
            listKJob << "V1"+it?.m055KategoriJob
            listKJob << "V"+it?.m055KategoriJob
            listWarna << it?.m055WarnaBord
            listWarna << it?.m055WarnaBord
        }
        [params: params,listKategori : listKJob, listWarna : listWarna]
    }
    def perThreeDays(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
//        Date today = new Date()
        Date today = datatablesUtilService?.syncTime()
        params.tglHeader = sdf.format(today)
        params.tglHeader1 = sdf.format(today + 1)
        params.tglHeader2 = sdf.format(today + 2)
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        c.add(Calendar.DATE, 1);
        Date tomorrow = c.getTime();
        c.add(Calendar.DATE, 1);
        Date dayAfterTomorrow = c.getTime();

        params.tglHeaderPlusOne = sdf.format(tomorrow)
        params.tglHeaderPlusTwo = sdf.format(dayAfterTomorrow)
        def listKJob = []
        def listWarna = []
        KategoriJob.createCriteria().list {order("m055KategoriJob")}.each {
            listKJob << "V1"+it?.m055KategoriJob
            listKJob << "V"+it?.m055KategoriJob
            listWarna << it?.m055WarnaBord
            listWarna << it?.m055WarnaBord
        }
        [params: params,listKategori : listKJob, listWarna : listWarna]
    }

    def datatablesList() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer){
            companyDealer = CompanyDealer.get(params?.input_companyDealer?.toLong())
        }
        render ASBService.datatablesList(params, companyDealer) as JSON
//        render ASBService.datatablesList(params, companyDealer) as JSON
    }
    def datatablesListPlusZero() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer3Hari){
            companyDealer = CompanyDealer.get(params?.input_companyDealer3Hari?.toLong())
        }
        render ASBService.datatablesListPlusZero(params, companyDealer) as JSON
    }
    def datatablesListPlusOne() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer3Hari){
            companyDealer = CompanyDealer.get(params?.input_companyDealer3Hari?.toLong())
        }
        render ASBService.datatablesListPlusOne(params, companyDealer) as JSON
    }
    def datatablesListPlusTwo() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer3Hari){
            companyDealer = CompanyDealer.get(params?.input_companyDealer3Hari?.toLong())
        }
        render ASBService.datatablesListPlusTwo(params, companyDealer) as JSON
    }
}