package com.kombos.board

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class JPBBPViewController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def JPBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }
    def perDay(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        params.sCriteria_tglView = sdf.format(new Date())
        params.tglHeader = sdf.format(new Date())
        [params: params]
    }
    def perWeek(Integer max) {

        DateFormat sdf = new SimpleDateFormat("MMMM yyyy")
        DateFormat dayFormat = new SimpleDateFormat("EEE")
        DateFormat dd = new SimpleDateFormat("d")
        Date theDay = new Date()
        params.lblTglView = sdf.format(theDay)

        int lblSenin
        int lblSelasa
        int lblRabu
        int lblKamis
        int lblJumat
        int lblSabtu
        int lblMinggu
        String theDayName = dayFormat.format(theDay)
        int day = Integer.parseInt(dd.format(theDay))
        if (theDayName.equalsIgnoreCase("MON") ) {
            lblSenin = day
            lblSelasa = day + 1
            lblRabu = day + 2
            lblKamis = day + 3
            lblJumat = day + 4
            lblSabtu = day + 5
            lblMinggu = day + 6
        } else if (theDayName.equalsIgnoreCase("TUE") ) {
            lblSenin = day - 1
            lblSelasa = day
            lblRabu = day + 1
            lblKamis = day + 2
            lblJumat = day + 3
            lblSabtu = day + 4
            lblMinggu = day + 5
        } else if (theDayName.equalsIgnoreCase("WED") ) {
            lblSenin = day - 2
            lblSelasa = day - 1
            lblRabu = day
            lblKamis = day + 1
            lblJumat = day + 2
            lblSabtu = day + 3
            lblMinggu = day + 4
        } else if (theDayName.equalsIgnoreCase("THU") ) {
            lblSenin = day - 3
            lblSelasa = day - 2
            lblRabu = day - 1
            lblKamis = day
            lblJumat = day + 1
            lblSabtu = day + 2
            lblMinggu = day + 3
        } else if (theDayName.equalsIgnoreCase("FRI") ) {
            lblSenin = day - 4
            lblSelasa = day - 3
            lblRabu = day - 2
            lblKamis = day - 1
            lblJumat = day
            lblSabtu = day + 1
            lblMinggu = day + 2
        } else if (theDayName.equalsIgnoreCase("SAT") ) {
            lblSenin = day - 5
            lblSelasa = day - 4
            lblRabu = day - 3
            lblKamis = day - 2
            lblJumat = day - 1
            lblSabtu = day
            lblMinggu = day + 1
        } else if (theDayName.equalsIgnoreCase("SUN") ) {
            lblSenin = day - 6
            lblSelasa = day - 5
            lblRabu = day - 4
            lblKamis = day - 3
            lblJumat = day - 2
            lblSabtu = day - 1
            lblMinggu = day
        }

        params.lblSenin = lblSenin
        params.lblSelasa = lblSelasa
        params.lblRabu = lblRabu
        params.lblKamis = lblKamis
        params.lblJumat = lblJumat
        params.lblSabtu = lblSabtu
        params.lblMinggu = lblMinggu
        [params: params]
    }

    def perMonth(Integer max) {
        DateFormat dayFormat = new SimpleDateFormat("d")
        DateFormat monthFormat = new SimpleDateFormat("MMMM")
        DateFormat moFormat = new SimpleDateFormat("M")
        DateFormat yearFormat = new SimpleDateFormat("yyyy")

        Date theDay = new Date()
        String sdt = dayFormat.format(theDay)
        int dt = Integer.parseInt(sdt)
        String month = monthFormat.format(theDay)
        params.month1 = month
        String smo = moFormat.format(theDay)
        int mo = Integer.parseInt(smo)
        params.month2 = (mo == 1) ? "February" : (mo == 2) ? "March" : (mo == 3) ? "April" : (mo == 4) ? "May" : (mo == 5) ? "June" : (mo == 6) ? "July" :
                (mo == 7) ? "August" : (mo == 8) ? "September" : (mo == 9) ? "October" : (mo == 10) ? "November" : "December";
        String syear = yearFormat.format(theDay)
        int year = Integer.parseInt(syear)
        params.year = "" + year

        int endMonth = 30
        if (month.equalsIgnoreCase("January") || month.equalsIgnoreCase("March") || month.equalsIgnoreCase("May") ||
                month.equalsIgnoreCase("July") || month.equalsIgnoreCase("August") || month.equalsIgnoreCase("October") ||
                month.equalsIgnoreCase("December")) {
            endMonth = 31
        } else if (month.equalsIgnoreCase("April") || month.equalsIgnoreCase("June") || month.equalsIgnoreCase("September") ||
                month.equalsIgnoreCase("November") ) {
            endMonth = 30
        } else {
            if (year % 4 == 0) endMonth = 29
            else endMonth = 28
        }

        int i = 0;
        int j = 0;
        params.date1 = dt
        i += 1
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date2 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date3 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date4 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date5 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date6 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date7 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date8 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date9 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date10 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date11 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date12 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date13 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date14 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date15 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date16 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date17 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date18 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date19 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date20 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date21 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date22 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date23 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date24 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date25 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date26 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date27 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date28 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date29 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date30 = dt
        dt = (dt != endMonth + 1) ? dt + 1 : 1
        if (dt != 1 && j == 0) i += 1 else j += 1
        params.date31 = dt

        params.colspanMonth1 = i
        params.colspanMonth2 = j
        [params: params]
    }

    def datatablesList() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer){
            companyDealer = CompanyDealer.get(params?.input_companyDealer?.toLong())
        }
        params.dari = "mboard"
        render JPBService.datatablesList(params, companyDealer) as JSON
    }

    def datatablesListWeek() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer){
            companyDealer = CompanyDealer.get(params?.input_companyDealer?.toLong())
        }
        params.dari = "mboard"
        render JPBService.datatablesList(params, companyDealer) as JSON
    }

    def datatablesListMonth() {
        session.exportParams = params
        CompanyDealer companyDealer = session?.userCompanyDealer
        if(params.input_companyDealer){
            companyDealer = CompanyDealer.get(params?.input_companyDealer?.toLong())
        }
        params.dari = "mboard"
        render JPBService.datatablesList(params, companyDealer) as JSON
    }

    def create() {
        def result = JPBService.create(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        def result = ASBService.save(params)
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["JPB", result.ASBInstance.id])
            redirect(action: 'show', id: result.ASBInstance.id)
            return
        }
        render(view: 'create', model: [ASBInstance: result.ASBInstance])
    }

    def show(Long id) {
        def result = ASBService.show(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = ASBService.show(params)

        if (!result.error)
            return [ASBInstance: result.ASBInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        def result = ASBService.update(params)
        params.lastUpdated = datatablesUtilService?.syncTime()

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["JPB", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [ASBInstance: result.ASBInstance.attach()])
    }

    def delete() {
        def result = ASBService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["JPB", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }


    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(JPB, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def modalJPBB(){

    }
}
