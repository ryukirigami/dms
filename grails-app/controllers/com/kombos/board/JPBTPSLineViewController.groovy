package com.kombos.board

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

import java.text.DateFormat
import java.text.SimpleDateFormat

class JPBTPSLineViewController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def JPBService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        DateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
        params.tglHeader = sdf.format(new Date())

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);
        Date tomorrow = c.getTime();
        params.tglHeaderPlusOne = sdf.format(tomorrow)
        [params: params]
    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session.userCompanyDealer
        render JPBService.datatablesListADay(params) as JSON
    }

    def datatablesListNextDay() {
        session.exportParams = params
        params.companyDealer = session.userCompanyDealer
        render JPBService.datatablesListNextDay(params) as JSON
    }
}
