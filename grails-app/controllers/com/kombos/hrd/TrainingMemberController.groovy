package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class TrainingMemberController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def trainingMemberService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render trainingMemberService.datatablesList(params) as JSON
    }

    def create() {
        def result = trainingMemberService.create(params)

        if (!result.error)
            return [trainingMemberInstance: result.trainingMemberInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = trainingMemberService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["TrainingMember", result.trainingMemberInstance.id])
            redirect(action: 'show', id: result.trainingMemberInstance.id)
            return
        }

        render(view: 'create', model: [trainingMemberInstance: result.trainingMemberInstance])
    }

    def show(Long id) {
        def result = trainingMemberService.show(params)

        if (!result.error)
            return [trainingMemberInstance: result.trainingMemberInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = trainingMemberService.show(params)

        if (!result.error)
            return [trainingMemberInstance: result.trainingMemberInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = trainingMemberService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["TrainingMember", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [trainingMemberInstance: result.trainingMemberInstance.attach()])
    }

    def delete() {
        def result = trainingMemberService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["TrainingMember", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(TrainingMember, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

}
