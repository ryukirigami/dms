package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class TrainingInstructorController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def trainingInstructorService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def cekKaryawan(){
        def karyawan = Karyawan.findById(params.id as Long)
        def hk = HistoryKaryawan.findByKaryawanAndStaDel(karyawan,'0')
        def ck = CertificationKaryawan.findAllByKaryawan(karyawan,
                [max: 1, sort: "tglSertifikasi", order: "desc"])
        def data = [:]
        data.companyDealer = hk.companyDealerId
        data.jabatan = hk.jabatanAfterId
        data.sertifikasi = "notok"
        if(!hk.companyDealer){
            data.companyDealer = "notok"
        }
        if(!hk.jabatanAfter){
            data.jabatan = "notok"
        }
        if(ck){
            data.sertifikasi = ck?.first()?.id
        }

        render data as JSON
    }

    def datatablesList() {
        session.exportParams = params

        render trainingInstructorService.datatablesList(params) as JSON
    }

    def create() {
        def result = trainingInstructorService.create(params)

        if (!result.error)
            return [trainingInstructorInstance: result.trainingInstructorInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = trainingInstructorService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["TrainingInstructor", result.trainingInstructorInstance.id])
            redirect(action: 'show', id: result.trainingInstructorInstance.id)
            return
        }

        render(view: 'create', model: [trainingInstructorInstance: result.trainingInstructorInstance])
    }

    def show(Long id) {
        def result = trainingInstructorService.show(params)

        if (!result.error)
            return [trainingInstructorInstance: result.trainingInstructorInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = trainingInstructorService.show(params)

        if (!result.error)
            return [trainingInstructorInstance: result.trainingInstructorInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = trainingInstructorService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["TrainingInstructor", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [trainingInstructorInstance: result.trainingInstructorInstance.attach()])
    }

    def delete() {
        def result = trainingInstructorService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["TrainingInstructor", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(TrainingInstructor, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}
