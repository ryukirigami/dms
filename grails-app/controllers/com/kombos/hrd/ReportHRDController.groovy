package com.kombos.hrd

import com.kombos.administrasi.*
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import groovy.time.TimeCategory
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

import java.text.SimpleDateFormat

class ReportHRDController {
    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def jasperService

    def index() {
        [dealer : session.userCompanyDealer]
    }

    def previewData(){
        def formatFile = "";
        def formatBuntut = "";
        def fileFormat = ""
        if(params?.format=="x"){
            formatFile = ".xls"
            formatBuntut = "application/vnd.ms-excel";
            fileFormat = JasperExportFormat.XLS_FORMAT
        }else{
            formatFile = ".pdf";
            formatBuntut = "application/pdf";
            fileFormat = JasperExportFormat.PDF_FORMAT
        }
        List<JasperReportDef> reportDefList = []
        def file = null
        def reportData = calculateReportData(params)

        if(params.namaReport=="01"){
            def reportDef = new JasperReportDef(name:'biodataKaryawan.jasper',
                    fileFormat: fileFormat ,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Biodata_Karyawan",formatFile)

        }
        if(params.namaReport=="02"){
            def reportDef = new JasperReportDef(name:'KonditeKaryawan.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Kondite_Karyawan_",formatFile)
        }
        if(params.namaReport=="03"){
            def reportDef = new JasperReportDef(name:'Data_Karyawan.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Data_Karyawan",formatFile)
        }
        if(params.namaReport=="04"){
            def parameters = [
                    SUBREPORT_DIR : servletContext.getRealPath('/reports') + "/"
            ]
            def reportDef = new JasperReportDef(name:'KaryawanKeluarMasuk.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData,
                    parameters: parameters
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("DATA_KELUAR_MASUK_MUTASI_",formatFile)
        }
        if(params.namaReport=="05"){
            def reportDef = new JasperReportDef(name:'evaluasiKaryawan.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Evaluasi",formatFile)
        }
        if(params.namaReport=="06"){
            def reportDef = new JasperReportDef(name:'daftarHadirKaryawan.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Data_Hadir_Karyawan",formatFile)
        }
        if(params.namaReport=="11"){
            def reportDef = new JasperReportDef(name:'Facility.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Facility_",formatFile)
        }
        if(params.namaReport=="12"){
            def reportDef = new JasperReportDef(name:'RekapTrainingHistory.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Rekap_Training_History_",formatFile)
        }
        if(params.namaReport=="13"){
            def reportDef = new JasperReportDef(name:'Training_history_karyawan.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData)
            reportDefList.add(reportDef)
            file = File.createTempFile("Training_history_karyawan_",formatFile)
        }
        if(params.namaReport=="14"){
            def reportDef = new JasperReportDef(name:'RekapHistoriSertifikasi.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Rekap_History_Sertifikasi_",formatFile)
        }
        if(params.namaReport=="15"){
            def reportDef = new JasperReportDef(name:'SertifikasiHistory.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData)
            reportDefList.add(reportDef)
            file = File.createTempFile("History_Sertifikasi",formatFile)
        }
        if(params.namaReport=="16"){
            def reportDef = new JasperReportDef(name:'HistoryClassTrain.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("History_Class_Training_",formatFile)
        }
        if(params.namaReport=="17"){
            def reportDef = new JasperReportDef(name:'historySertifikasi.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("HistorySertifikasi",formatFile)
        }
        if(params.namaReport=="18"){
            def reportDef = new JasperReportDef(name:'trainingInstruction.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("trainingInstruction",formatFile)

        }
        if(params.namaReport=="19"){
            def reportDef = new JasperReportDef(name:'Detail_Instruktur.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Detail_Instruktur",formatFile)

        }
        if(params.namaReport=="20"){
            def reportDef = new JasperReportDef(name:'Detil_Sertifikasi.jasper',
                    fileFormat:fileFormat,
                    reportData: reportData
            )
            reportDefList.add(reportDef)
            file = File.createTempFile("Detil_Sertifikasi",formatFile)

        }

        file.deleteOnExit() //Aktifkan dulu
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", formatBuntut)
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }

    def calculateReportData(def params){
        String dateFormat = appSettingParamDateFormat.value?appSettingParamDateFormat.value:appSettingParamDateFormat.defaultValue

        def reportData = new ArrayList();
        def listBulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
        String tanggal = params.tanggal
        String tanggalEnd = params.tanggal2
        def strDate = new Date().parse("dd-MM-yyyy",tanggal.toString())
        def strEndDate = new Date().parse("dd-MM-yyyy",tanggalEnd.toString())

        SimpleDateFormat df2 = new SimpleDateFormat("dd-MM-yyyy")
        Calendar cal1 = Calendar.getInstance()

        if(params.namaReport=="01"){

            def c=Karyawan.createCriteria().list {
                eq("staDel","0")
//                eq("branch", CompanyDealer.findById(params.workshop as Long))
                eq("id", params.karyawan.id as Long)
            }
            c.each { kar ->
                def data = [:]
                def keluargaKaryawan = KeluargaKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar?.id)
                    }
                }
                def dataKeluarga = []
                keluargaKaryawan.each {
                    dataKeluarga << [
                            hubungan : it.hubungan.key,
                            namaKeluarga : it?.nama,
                            jk : it?.jenisKelamin,
                            ttl : it?.tempatLahir + ", " + (it?.tanggalLahirKel ? it?.tanggalLahirKel.format("dd/MM/yyyy"):""),
                            pendidikan : it?.pendidikan?.pendidikan,
                            namaPerusahaan : "-",
                            jabatan : it?.jabatan,
                    ]
                }
                if(keluargaKaryawan.size()==0){
                    dataKeluarga << [
                            hubungan : ""
                    ]
                }
                def riwayatPendidikan = PendidikanKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar?.id)
                    }
                    pendidikan{
                        order("id","asc")
                    }
                }
                def dataPendidikan = []
                riwayatPendidikan.each {
                    dataPendidikan << [
                            tingkatPendidikan : it?.pendidikan?.pendidikan,
                            namaSekolah : it?.namaSekolah,
                            kota : it?.kota?.m002NamaKabKota,
                            dari : it?.tahunMasuk,
                            sampai : it?.tahunLulus,
                            jurusan : it?.jurusan,
                            lulus : it?.statusLulus=="0"?"LULUS":"TDK LULUS",
                            ipk : it?.nemIpk,
                    ]
                }
                if(riwayatPendidikan.size()==0){
                    dataPendidikan << [
                            tingkatPendidikan : ""
                    ]
                }

                def riwayatTraining = TrainingMember.createCriteria().list {
                    karyawan{
                        eq("id",kar?.id)
                    }
                }
                def dataTraining = [], no = 0
                riwayatTraining.each {
                    no++
                    dataTraining << [
                            no : no,
                            namaTraining : it?.training?.namaTraining,
                            tempatTraining : it?.training?.trainingType?.tempatTraining,
                            kota : "",
                            tglMulai : it?.training?.dateBegin ? it?.training?.dateBegin.format("dd/MM/yyyy") : "",
                            tglSelesai : it?.training?.dateFinish ?  it?.training?.dateFinish.format("dd/MM/yyyy") : "",
                            statusLulus : it?.staGraduate=="0"?"LULUS":"TDK LULUS",
                            nilai : it?.point,
                            nilaiRata2 : it?.training?.pointAverage,
                            typeTraining : it?.training?.trainingType?.tipeTraining,
                            sertifikasi : it?.training?.certificationType?.tipeSertifikasi,
                    ]
                }
                if(riwayatTraining.size()==0){
                    dataTraining << [
                            no : ""
                    ]
                }

                def riwayatSertifikasi = CertificationKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar?.id)
                    }
                }
                def dataSertifikasi = [], noSertifikasi = 0
                riwayatSertifikasi.each {
                    noSertifikasi++
                    dataSertifikasi << [
                            no : noSertifikasi,
                            namaSertifikasi : it?.tipeSertifikasi?.namaSertifikasi,
                            tglMulai : it?.training?.dateBegin ? it?.training?.dateBegin.format("dd/MM/yyyy") : "",
                            tglSelesai : it?.training?.dateFinish ?  it?.training?.dateFinish.format("dd/MM/yyyy") : "",
                            tglLulus : it?.training?.dateFinish ?  it?.training?.dateFinish.format("dd/MM/yyyy") : "",
                            nilai : TrainingMember.findByKaryawanAndTraining(kar,it?.training)?.point,
                            nilaiRata2 : it?.training?.pointAverage,
                            statusLulus : TrainingMember.findByKaryawanAndTraining(kar,it?.training)?.staGraduate=="0"?"LULUS":"TDK LULUS",

                    ]
                }
                if(riwayatSertifikasi.size()==0){
                    dataSertifikasi << [
                            no : ""
                    ]
                }

                def riwayatPekerjaan = HistoryKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar?.id)
                    }
                }
                def dataPekerjaan = []
                riwayatPekerjaan.each {
                    dataPekerjaan << [
                            ket : it?.history?.history,
                            dari : it?.tanggal? it?.tanggal.format("dd/MM/yyyy") : "",
//                            sampai : it?.training?.dateFinish ?  it?.training?.dateFinish.format("dd/MM/yyyy") : "",
                            bagian : it?.divisi?.m012NamaDivisi,
                            jabatan : it?.jabatanBefore?.m014JabatanManPower,
//                            status : it?.training?.pointAverage,
                            alasan : it.reason,

                    ]
                }
                if(riwayatPekerjaan.size()==0){
                    dataPekerjaan << [
                            ket : ""
                    ]
                }

                def riwayatPenghargaan = HistoryRewardKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar?.id)
                    }
                }
                def dataPenghargaan = []
                riwayatPenghargaan.each {
                    dataPenghargaan << [
                            ket : it?.rewardKaryawan?.rewardKaryawan,
                            tgl : it?.tanggalReward ? it?.tanggalReward.format("dd/MM/yyyy") : "",
                            alasan : it?.keterangan

                    ]
                }
                if(riwayatPenghargaan.size()==0){
                    dataPenghargaan << [
                            ket : ""
                    ]
                }

                def riwayatPeringatan = HistoryWarningKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar?.id)
                    }
                }
                def dataPeringatan = []
                riwayatPeringatan.each {
                    dataPeringatan << [
                            ket : it?.warningKaryawan?.warningKaryawan,
                            tgl : it?.tanggalWarning ? it?.tanggalWarning.format("dd/MM/yyyy") : "",
                            alasan : it?.keterangan
                    ]
                }
                if(riwayatPeringatan.size()==0){
                    dataPeringatan << [
                            ket : ""
                    ]
                }
                data.put("companyDealer",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("nik",kar?.nomorPokokKaryawan)
                data.put("nama",kar?.nama)
                data.put("statusKaryawan",kar?.statusKaryawan?.statusKaryawan)
                data.put("tglDitetapkan",kar?.tanggalMasuk ? kar?.tanggalMasuk.format("dd/MM/yyyy") : "")
                data.put("jenisKelamin",kar?.jenisKelamin==0?"Laki - Laki":"Perempuan")
                data.put("ttl",kar?.kotaLahir?.m002NamaKabKota + ", "+ (kar?.tanggalLahir ? kar?.tanggalLahir.format("dd/MM/yyyy") : ""))
                data.put("tempatTinggal",kar?.alamat)
                data.put("telp",kar?.telepon)
                data.put("agama",kar?.agama?.m061NamaAgama)
                data.put("status",kar?.statusNikah?.m062StaNikah)
                data.put("golDar","")
                data.put("ktp_sim","")
                data.put("email",kar?.emailAddress)
//
                data.put("dataKeluarga",dataKeluarga)
                data.put("dataPendidikan",dataPendidikan)
                data.put("dataTraining",dataTraining)
                data.put("dataSertifikasi",dataSertifikasi)
                data.put("dataPekerjaan",dataPekerjaan)
                data.put("dataPeringatan",dataPeringatan)
                data.put("dataPenghargaan",dataPenghargaan)
                reportData.add(data)
            }
            if(c.size()==0){
                def data = [:]
                data.put("companyDealer",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                reportData.add(data)
            }
        }
        if(params.namaReport=="02"){
            def periode = params.tahunperiode;
            def divisi = Divisi.get(params.divisi.toLong())
            def company = CompanyDealer.get(params.workshop.toLong())
            def penilaianKaryawan = PenilaianKaryawan.createCriteria().list {
                eq("stadel","0")
                eq("companyDealer",company)
                eq("periode",periode,[ignoreCase : true])
                karyawan{
                    eq("branch",company)
                    eq("divisi",divisi)
                }
                projections {
                    groupProperty("karyawan")
                }
            }
            int a = 0
            if(penilaianKaryawan.size()<1){
                def data = [:]
                data.put("bagian",divisi?.m012NamaDivisi)
                data.put("kota",company?.kabKota ? company?.kabKota?.m002NamaKabKota+ " , " :"... , ")
                data.put("tanggal",new Date().format("dd-MM-yyyy"))
                data.put("no","")
                data.put("nik","")
                data.put("nama","")
                data.put("cabang","")
                data.put("jenisKelamin","")
                data.put("jabatan","")
                data.put("pendidikan","")
                data.put("teknis1","")
                data.put("teknis2","")
                data.put("teknis3","")
                data.put("logika1","")
                data.put("logika2","")
                data.put("logika3","")
                data.put("cepat1","")
                data.put("cepat2","")
                data.put("cepat3","")
                data.put("loyal1","")
                data.put("loyal2","")
                data.put("loyal3","")
                data.put("drive1","")
                data.put("drive2","")
                data.put("drive3","")
                data.put("disiplin1","")
                data.put("disiplin2","")
                data.put("disiplin3","")
                reportData.add(data)
            }
            penilaianKaryawan.each {
                a++
                def nilai1 = 0,nilai2 = 0,nilai3 = 0,nilai4 = 0,nilai5 = 0,nilai6 = 0,nilai7 = 0,nilai8 = 0,nilai9 = 0
                def nilai10 = 0,nilai11 = 0,nilai12 = 0,nilai13 = 0,nilai14 = 0,nilai15 = 0,nilai16 = 0,nilai17 = 0,nilai18 = 0
                Karyawan karyawan = it
                def results = PenilaianKaryawan.createCriteria().list {
                    eq("stadel","0")

                    eq("companyDealer",company)
                    eq("periode",periode)
                    eq("karyawan",karyawan)
                }
                int countKasie = 0
                int countKabag = 0
                int countKabeng = 0
                results.each {
                    if(it.penilai.jabatan.m014JabatanManPower.toUpperCase().contains("KASIE")){
                        countKasie++
                        nilai1+=it.teknis
                        nilai4+=it.logika
                        nilai7+=it.kecepatanKetelitian
                        nilai10+=it.loyalitas
                        nilai13+=it.drive
                        nilai16+=it.disiplin
                    }else if(it.penilai.jabatan.m014JabatanManPower.toUpperCase().contains("KABAG")){
                        countKabag++
                        nilai2+=it.teknis
                        nilai5+=it.logika
                        nilai8+=it.kecepatanKetelitian
                        nilai11+=it.loyalitas
                        nilai14+=it.drive
                        nilai17+=it.disiplin
                    }else{
                        countKabeng++
                        nilai3+=it.teknis
                        nilai6+=it.logika
                        nilai9+=it.kecepatanKetelitian
                        nilai12+=it.loyalitas
                        nilai15+=it.drive
                        nilai18+=it.disiplin
                    }
                }
                def data = [:]
                data.put("no",a)
                data.put("bagian",divisi?.m012NamaDivisi)
                data.put("nik",karyawan?.nomorPokokKaryawan)
                data.put("nama",karyawan.nama)
                data.put("cabang",karyawan.branch.m011NamaWorkshop)
                data.put("kota",company?.kabKota ? company?.kabKota?.m002NamaKabKota+ " , " :"... , ")
                data.put("tanggal",new Date().format("dd-MM-yyyy"))
                data.put("jenisKelamin",karyawan.jenisKelamin==0 ? "L" : "P")
                data.put("jabatan",karyawan.jabatan.m014JabatanManPower)
                data.put("pendidikan",karyawan.pendidikan?.last()?.pendidikan)
                data.put("teknis1",countKasie!=0 ? nilai1/countKasie : nilai1)
                data.put("teknis2",countKabag!=0 ? nilai2/countKabag : nilai2)
                data.put("teknis3",countKabeng!=0 ? nilai3/countKabeng : nilai3)
                data.put("logika1",countKasie!=0 ? nilai4/countKasie : nilai4)
                data.put("logika2",countKabag!=0 ? nilai5/countKabag : nilai5)
                data.put("logika3",countKabeng!=0 ? nilai6/countKabeng : nilai6)
                data.put("cepat1",countKasie!=0 ? nilai7/countKasie : nilai7)
                data.put("cepat2",countKabag!=0 ? nilai8/countKabag : nilai8)
                data.put("cepat3",countKabeng!=0 ? nilai9/countKabeng : nilai9)
                data.put("loyal1",countKasie!=0 ? nilai10/countKasie : nilai10)
                data.put("loyal2",countKabag!=0 ? nilai11/countKabag : nilai11)
                data.put("loyal3",countKabeng!=0 ? nilai12/countKabeng : nilai12)
                data.put("drive1",countKasie!=0 ? nilai13/countKasie : nilai13)
                data.put("drive2",countKabag!=0 ? nilai14/countKabag : nilai14)
                data.put("drive3",countKabeng!=0 ? nilai15/countKabeng : nilai15)
                data.put("disiplin1",countKasie!=0 ? nilai16/countKasie : nilai16)
                data.put("disiplin2",countKabag!=0 ? nilai17/countKabag : nilai17)
                data.put("disiplin3",countKabeng!=0 ? nilai18/countKabeng : nilai18)
                reportData.add(data)
            }
        }
        if(params.namaReport=="03"){

            def no=0
            def c=Karyawan.createCriteria().list {
                eq("staDel","0")
                eq("branch", CompanyDealer.findById(params.workshop as Long))

            }
            if(c.size()<1)
            {
                def data = [:]
                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("no","-")
                data.put("nik","-")
                data.put("nama","-")
                data.put("jabatan","-")
                data.put("lp","-")
                data.put("usia","-")
                data.put("stsrt","-")
                data.put("pendidikan","-")
                data.put("jurusan","-")
                data.put("ipk","-")
                data.put("sekolah","-")
                data.put("tgllahir","-")
                data.put("stskaryawan","-")
                data.put("tglmasuk","-")

                data.put("tglditetapkan","-")
                data.put("masakerja","-")
                data.put("mutasidari","-")
                data.put("notelep","-")
                data.put("notelep2","-")
                data.put("email","-")
                data.put("alamat","-")
                data.put("training","-")
                data.put("trainingsertifikasi","-")

                reportData.add(data)
                return reportData
            }
            c.each {
                def data = [:]
                no++
                data.put("workshop",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("no",no)
                data.put("nik",it?.nomorPokokKaryawan)
                data.put("nama",it?.nama)
                data.put("jabatan",it?.jabatan)
                data.put("lp",it?.jenisKelamin==0?"L":"P")
                data.put("usia","-")
                data.put("stsrt","-")
                data.put("pendidikan",it?.pendidikan)
                data.put("jurusan","-")
                data.put("ipk","-")
                data.put("sekolah","-")
                data.put("tgllahir",it?.tanggalLahir.format("dd-MM-yyyy"))
                data.put("stskaryawan",it?.statusKaryawan?.keterangan)
                data.put("tglmasuk",it?.tanggalMasuk?.format("dd-MM-yyyy") ? it?.tanggalMasuk?.format("dd-MM-yyyy") : "-")

                data.put("tglditetapkan","-")
                data.put("masakerja","-")
                data.put("mutasidari","-")
                data.put("notelep",it.telepon)
                data.put("notelep2","-")
                data.put("email",it?.emailAddress ? it?.emailAddress : "-")
                data.put("alamat",it?.alamat ? it?.alamat : "-")
                data.put("training",it?.trainingMember?.training?.namaTraining ? it?.trainingMember?.training?.namaTraining : "-")
                data.put("trainingsertifikasi",it?.sertifikasiKaryawan?.tipeSertifikasi? it?.sertifikasiKaryawan?.tipeSertifikasi : "-")

                reportData.add(data)
            }

        }
        if(params.namaReport=="04"){
            def awal = new Date().parse("dd-MM-yyyy",params.tanggal);
            def akhir = new Date().parse("dd-MM-yyyy",params.tanggal2);
            def company = CompanyDealer.get(params.workshop.toLong())
            def items1 = [],items2 = [],items3 = [],items4 = [],items5 = []
            def listOver = ["MASUK","MUTASI MASUK","KELUAR","MUTASI KELUAR","MUTASI INTERNAL"]
            for(int a=0;a<5;a++){
                def items = []
                int count = 0
                String judul1="", judul2="", judul3="", judul4="", judul5="", judul6="",judul7="",judulReport=""
                def cari = HistoryKaryawan.createCriteria().list {
                    eq("staDel","0")
                    ge("tanggal",awal)
                    lt("tanggal",(akhir+1))
                    eq("companyDealer",company)
                    history{
                        eq("keterangan",listOver[a],[ignoreCase: true])
                    }
                    order("tanggal")
                }
                if(a==2){
                    judul1="Tggl Keluar";judul2="Tggl Masuk";judul3="Masa Kerja";judul4="Tingkat Pendidikan";judul5="IPK / Raport";judul6="TRAINING";judul7="Alasan Keluar";judulReport="3.  DAFTAR KARYAWAN KELUAR";
                }else if(a==3){
                    judul1="Tggl Mutasi";judul2="Tggl Masuk";judul3="Cabang Tujuan";judul4="Jabatan Baru";judul5="TIngkat Pendidikan";judul6="IPK/Raport";judul7="KET. (Alasan Mutasi)";judulReport="4.  DAFTAR KARYAWAN MUTASI KELUAR";
                }
                if(cari.size()<1){
                   items << [
                       no : "",
                       nik : "",
                       nama : "",
                       jabatanAwal : "",
                       jabatan : "",
                       lp : "",
                       tggl : "",
                       tggl2 : "",
                       tgglMasuk : "",
                       tgglLahir : "",
                       tgglMutasi : "",
                       sts : "",
                       stskywn : "",
                       cabAsal : "",
                       jabatanCabAsal : "",
                       jurusan : "",
                       sekolah : "",
                       pengalaman : "",
                       jabatanBaru : "",
                       masaOrCabang : "",
                       pendidikan : "",
                       ipkrapot : "",
                       ket : "",
                       pendidikanOrJabatan : "",
                       ipkOrPendidikan : "",
                       trainingOrIPK : "",
                       alasanOrKet : "",
                       judulreport : judulReport,
                       judulTggl : judul1,
                       judulTggl2 : judul2,
                       judulMasaOrCabang : judul3,
                       judulPendidikanOrJabatan : judul4,
                       judulIpkOrPendidikan : judul5,
                       judulTrainingOrIPK : judul6,
                       judulAlasanOrKet : judul7
                   ]
                }
                cari.each {
                    String data2="", data3="", data4="", data5="", data6="",tgglMasuk = "",tgglMutasi = "",pendidikan="", jurusan = "", nemIpk = "",sekolah=""
                    def tempKaryawan = it.karyawan
                    def listPendidikan = PendidikanKaryawan.createCriteria().get {
                        eq("staDel","0")
                        eq("karyawan",tempKaryawan)
                        order("tahunLulus","desc")
                        maxResults(1);
                    }
                    if(a==2 || a==3){
                        def hist = History.findByKeteranganAndStaDel("MASUK","0")
                        def histKaryaman = HistoryKaryawan.findByCompanyDealerAndKaryawanAndHistoryAndStaDel(company,it.karyawan,hist,"0")
                        if(histKaryaman){
                            if(a==2){
                                use(TimeCategory){
                                    data2 = (new Date().year-histKaryaman?.tanggal?.year).toString()
                                }
                                if(listPendidikan){
                                    data3 = listPendidikan?.pendidikan?.pendidikan
                                    data4 = listPendidikan?.nemIpk?.toString()
                                }
                                def trainingMember = TrainingMember.createCriteria().list {
                                    eq("staDel","0")
                                    eq("karyawan",tempKaryawan)
                                }
                                if(trainingMember.size()>0){
                                    data5 = trainingMember?.training?.toString()
                                }
                            }else{
                                data2 = it.cabangAfter.m011NamaWorkshop
                                data3 = it.jabatanAfter.m014JabatanManPower
                                if(listPendidikan){
                                    data4 = listPendidikan?.pendidikan?.pendidikan
                                    data5 = listPendidikan?.nemIpk?.toString()
                                }
                            }
                            data6 = it.reason
                            tgglMasuk = histKaryaman.tanggal.format("dd/MM/yyyy")
                        }
                    }else{
                        tgglMasuk = it?.tanggal?.format("dd/MM/yyyy")
                    }
                    if(listPendidikan){
                        pendidikan = listPendidikan?.pendidikan?.pendidikan
                        jurusan = listPendidikan?.jurusan
                        nemIpk = listPendidikan?.nemIpk?.toString()
                        sekolah = listPendidikan?.namaSekolah
                    }
                    count++
                    items << [
                        no : count,
                        nik : it.karyawan.nomorPokokKaryawan,
                        nama : it.karyawan.nama,
                        jabatanAwal : it?.jabatanBefore ? it?.jabatanBefore?.m014JabatanManPower : "",
                        jabatan : it.karyawan.jabatan.m014JabatanManPower,
                        lp : it.karyawan.jenisKelamin==0 ? "L" : "P",
                        tggl : it.tanggal.format("dd/MM/yyyy"),
                        tggl2 : tgglMasuk,
                        tgglMasuk : tgglMasuk,
                        tgglLahir : it.karyawan.tanggalLahir ? it.karyawan.tanggalLahir.format("dd MMM yy") : "",
                        tgglMutasi : it.tanggal ? it.tanggal.format("dd/MM/yyyy") : "",
                        sts : it?.karyawan?.statusNikah ? it?.karyawan?.statusNikah?.m062StaNikah : "",
                        stskywn : it?.karyawan?.statusKaryawan ? it?.karyawan?.statusKaryawan?.statusKaryawan : "",
                        cabAsal : it.cabangBefore ? it.cabangBefore.m011NamaWorkshop : "",
                        jabatanCabAsal : it?.jabatanBefore ? it?.jabatanBefore?.m014JabatanManPower : "",
                        jurusan : jurusan,
                        sekolah : sekolah,
                        pengalaman : "",
                        jabatanBaru : it?.jabatanAfter ?it?.jabatanAfter?.m014JabatanManPower : "",
                        masaOrCabang : data2,
                        pendidikan : pendidikan,
                        ipkrapot : nemIpk,
                        ket : it.reason ? it.reason : "",
                        pendidikanOrJabatan : data3,
                        ipkOrPendidikan : data4,
                        trainingOrIPK : data5,
                        alasanOrKet : data6,
                        judulreport : judulReport,
                        judulTggl : judul1,
                        judulTggl2 : judul2,
                        judulMasaOrCabang : judul3,
                        judulPendidikanOrJabatan : judul4,
                        judulIpkOrPendidikan : judul5,
                        judulTrainingOrIPK : judul6,
                        judulAlasanOrKet : judul7
                    ]
                }
                if(a==0){
                    items1 = items
                }else if(a==1){
                    items2 = items
                }else if(a==2){
                    items3 = items
                }else if(a==3){
                    items4 = items
                }else if(a==4){
                    items5 = items
                }
            }
            def data = [:]
            data.put("cabang",company?.m011NamaWorkshop)
            data.put("tanggal",awal.format("dd MMMM yyyy") + " s.d. "+akhir.format("dd MMMM yyyy"))
            data.put("items1",items1)
            data.put("items2",items2)
            data.put("items3",items3)
            data.put("items4",items4)
            data.put("items5",items5)
            reportData.add(data)
        }
        if(params.namaReport=="05"){
            Calendar cal = Calendar.getInstance()
            cal.set(params.tahun as int,(params.bulan as int),1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            def sakit = 0, ijin = 0, alpa = 0, hadir
            def cuti, tl, tr, pendidikan = ""
            def teknis, logika, kecepatanKerja, ketelitianKerja, tanggungJawab, loyalitas, drive, jumlah, rataRata
            String bulanParam =  params.tahun + "-" + ((params.bulan  as int ) + 1) +"-1"
            Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
            String bulanParam2 =  params.tahun + "-" + ((params.bulan  as int ) + 1) + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
            Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
            def no=0
            def c=Karyawan.createCriteria().list {
                eq("staDel","0")
                eq("branch", CompanyDealer.findById(params.workshop as Long))
            }
            c.each { kar ->
                sakit = 0
                ijin = 0
                alpa = 0
                hadir = 0
                cuti = 0
                tl = 0
                tr =0
                teknis =0
                logika =0
                kecepatanKerja =0
                ketelitianKerja =0
                tanggungJawab =0
                loyalitas =0
                drive =0
                jumlah = 0
                rataRata = 0
                def absd = AbsensiKaryawanDetail.createCriteria().list {
                    absensiKaryawan{
                        karyawan{
                            eq("id",kar.id)
                        }
                        eq("bulanTahun",tgl.format("MMyyyy"))
                    }
                    eq("status","1")
                }
                absd.each {
                    sakit = it.absensiKaryawan.absent
                    alpa = it.absensiKaryawan.leave
                    hadir = it.absensiKaryawan.attend
                    ijin = it.absensiKaryawan.permit
                }
                def data = [:]
                def cutiS = CutiKaryawan.createCriteria().list {
                    eq("tahunCuti",params.tahun as int)
                    karyawan{
                        eq("id",kar.id)
                    }
                    ge("tanggalMulaiCuti",tgl)
                    lt("tanggalMulaiCuti",tgl2 + 1)
                }
                cutiS.each{
                    cuti += it.jumlahHariCuti
                }
                def tlsS = TugasLuarKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar.id)
                    }
                    ge("dateBegin",tgl)
                    lt("dateBegin",tgl2 + 1)
                }
                tlsS.each{
                    tl += (it.dateFinish - it.dateBegin)
                }
                def trS = TrainingMember.createCriteria().list {
                    karyawan{
                        eq("id",kar.id)
                    }
                    ge("tglGraduate",tgl)
                    lt("tglGraduate",tgl2 + 1)
                }
                trS.each{
                    tr ++
                }
                def pddk = PendidikanKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar.id)
                    }
                }
                pddk.each{
                    pendidikan += " " + it.jurusan
                }


                def penilaian = PenilaianKaryawan.createCriteria().list {
                    karyawan{
                        eq("id",kar.id)
                    }
                    eq("periode",tgl.format("yyyy"))
                }
                penilaian.each{
                    teknis += it.teknis
                    logika += it.logika
                    kecepatanKerja += 0
                    ketelitianKerja += it.kecepatanKetelitian
                    tanggungJawab += it.disiplin
                    loyalitas += it.loyalitas
                    drive += it.drive
                }
                jumlah = teknis + logika + kecepatanKerja + ketelitianKerja + tanggungJawab
                + loyalitas + drive
                no++

                data.put("companyDealer",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("no",no)
                data.put("periode","BULAN : " + (tgl.format("MMMM yyyy").toUpperCase()))
                data.put("namaKaryawan",kar.nama)
                data.put("jabatan",kar?.jabatan?.m014JabatanManPower)
                data.put("pendidikan",pendidikan)
                data.put("sakit",sakit)
                data.put("ijin",ijin)
                data.put("alpa",alpa)
                data.put("cuti",cuti)
                data.put("tl",tl)
                data.put("tr",tr)
                data.put("terlambat","")
                data.put("teknis",teknis)
                data.put("kecepatanKerja",kecepatanKerja)
                data.put("ketelitianKerja",ketelitianKerja)
                data.put("tanggungJawab",tanggungJawab)
                data.put("loyalitas",loyalitas)
                data.put("drive",drive)
                data.put("jumlah",jumlah)
                data.put("rataRata",jumlah / 7)
                data.put("targetJasa","")
                data.put("hasilBulanIni","")
                data.put("hasilSdBulanIni","")

                reportData.add(data)
            }
            if(c.size()==0){
                def data = [:]
                data.put("companyDealer",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("no",no)
                data.put("periode","BULAN : " + (tgl.format("MMMM yyyy").toUpperCase()))
                data.put("namaKaryawan","")
                data.put("jabatan","")
                data.put("pendidikan","")
                data.put("sakit","")
                data.put("ijin","")
                data.put("alpa","")
                data.put("cuti","")
                data.put("tl","")
                data.put("tr","")
                data.put("terlambat","")
                data.put("teknis","")
                data.put("kecepatanKerja","")
                data.put("ketelitianKerja","")
                data.put("tanggungJawab","")
                data.put("loyalitas","")
                data.put("drive","")
                data.put("jumlah","")
                data.put("rataRata","")
                data.put("targetJasa","")
                data.put("hasilBulanIni","")
                data.put("hasilSdBulanIni","")

                reportData.add(data)
            }
        }
        if(params.namaReport=="06"){
            Calendar cal = Calendar.getInstance()
            cal.set(params.tahun as int,(params.bulan as int),1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            def dataTanggal = []
            def sakit = 0, ijin = 0, alpa = 0, hadir
            def cuti, tl, tr

            String bulanParam =  params.tahun + "-" + ((params.bulan  as int ) + 1) +"-1"
            Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
            String bulanParam2 =  params.tahun + "-" + ((params.bulan  as int ) + 1) + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
            Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
            def no=0
            def c=Karyawan.createCriteria().list {
                eq("staDel","0")
                if(params.workshop!=""){
                    eq("branch", CompanyDealer.findById(params.workshop as Long))
                }
            }
            c.each { kar ->
                sakit = 0
                ijin = 0
                alpa = 0
                hadir = 0
                cuti = 0
                tl = 0
                tr =0
                for(int i=1;i<=31;i++){
                    dataTanggal[i] = ""
                }
                def absd = AbsensiKaryawanDetail.createCriteria().list {
                    absensiKaryawan{
                        karyawan{
                            eq("id",kar.id)
                        }
                        eq("bulanTahun",tgl.format("MMyyyy"))
                    }
//                    eq("staDel","0")
                    eq("status","1")
                }
                absd.each {
                    for(int j=1;j<=31;j++){
                        if(it?.tanggal?.date==j){
                            dataTanggal[j] = "V"
                        }
                    }
                    sakit += it?.absensiKaryawan?.leave
                    alpa += it?.absensiKaryawan?.absent
                    hadir += it?.absensiKaryawan?.attend
                    ijin += it?.absensiKaryawan?.permit
                }
                def data = [:]
                def cutiS = CutiKaryawan.createCriteria().list {
                    eq("tahunCuti",params.tahun as int)
                    ge("tanggalMulaiCuti",tgl)
                    lt("tanggalMulaiCuti",tgl2 + 1)
                }
                cutiS.each{
                    cuti += it.jumlahHariCuti
                }
                def tlsS = TugasLuarKaryawan.createCriteria().list {
                    ge("dateBegin",tgl)
                    lt("dateBegin",tgl2 + 1)
                }
                tlsS.each{
                    tl += (it.dateFinish - it.dateBegin)
                }

                def trS = TrainingMember.createCriteria().list {
                    ge("tglGraduate",tgl)
                    lt("tglGraduate",tgl2 + 1)
                }
                trS.each{
                    tr ++
                }
                no++
                data.put("companyDealer",session.userCompanyDealer)
                data.put("no",no)
                data.put("periode","DAFTAR HADIR BULAN : " + (tgl.format("MMMM yyyy").toUpperCase()))
                data.put("namaKaryawan",kar.nama)
                data.put("cabang",kar.branch?.m011NamaWorkshop)
                for(int i=1;i<=31;i++){
                    data.put("_"+i,dataTanggal[i])
                }
                data.put("sakit",sakit)
                data.put("ijin",ijin)
                data.put("alpa",alpa)
                data.put("hadir",hadir)
                data.put("cuti",cuti)
                data.put("tl",tl)
                data.put("tr",tr)
                data.put("terlambat","")
                data.put("perMenit","")

                reportData.add(data)
            }
            if(c.size()==0){
                def data = [:]
                data.put("companyDealer",session.userCompanyDealer)
                data.put("no",no)
                data.put("periode","DAFTAR HADIR BULAN : " + (tgl.format("MMMM yyyy").toUpperCase()))
                data.put("namaKaryawan","")
                data.put("sakit","")
                data.put("cabang","")
                data.put("ijin","")
                data.put("alpa","")
                data.put("hadir","")
                data.put("cuti","")
                data.put("tl","")
                data.put("tr","")
                data.put("terlambat","")
                data.put("perMenit","")
                reportData.add(data)
            }
        }
        if(params.namaReport=="11"){
            Calendar cal = Calendar.getInstance()
            cal.set(params.tahun as int,(params.bulan as int),1 )
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            def A, B, C, D, E, F, G, H, I, J, K, L, M, nabeng

            String bulanParam =  params.tahun + "-" + ((params.bulan  as int ) + 1) +"-1"
            Date tgl = new Date().parse('yyyy-MM-dd',bulanParam.toString())
            String bulanParam2 =  params.tahun + "-" + ((params.bulan  as int ) + 1) + "-" +cal.getActualMaximum(Calendar.DAY_OF_MONTH)
            Date tgl2 = new Date().parse('yyyy-MM-dd',bulanParam2.toString())
            def no=0
            def ta = 0, tb = 0, tc = 0, td = 0, te = 0, tf = 0, tg = 0, th = 0, ti = 0, tj = 0, tk = 0, tl = 0, tm = 0
            def nambeng = CompanyDealer.createCriteria().list {
                eq("staDel", "0")
            }

            nambeng.each {
                def data = [:]
                no++
                nabeng = it
                A = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("SB-SERVICE BERKALA"), SubJenisStall.findByM023NamaSubJenisStall("EM"), "0", tgl, tgl2)
                B = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("GR-GENERAL REPAIR"), SubJenisStall.findByM023NamaSubJenisStall("GR NON LIFT"), "0", tgl, tgl2)
                C = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("GR-GENERAL REPAIR"), SubJenisStall.findByM023NamaSubJenisStall("GR LIFT"), "0", tgl, tgl2)
                D = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("SB-SERVICE BERKALA"), SubJenisStall.findByM023NamaSubJenisStall("SBNP"), "0", tgl, tgl2)

                E = A.size() + B.size() + C.size() + D.size()

                F = Stall.findAllByCompanyDealerAndJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("THS"), "0", tgl, tgl2)
                G = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("LAIN-LAIN"), SubJenisStall.findByM023NamaSubJenisStall("SPOORING"), "0", tgl, tgl2)
                H = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("LAIN-LAIN"), SubJenisStall.findByM023NamaSubJenisStall("PDS"), "0", tgl, tgl2)
                I = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("LAIN-LAIN"), SubJenisStall.findByM023NamaSubJenisStall("DIO"), "0", tgl, tgl2)
                J = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("LAIN-LAIN"), SubJenisStall.findByM023NamaSubJenisStall("CUCI"), "0", tgl, tgl2)
                K = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("LAIN-LAIN"), SubJenisStall.findByM023NamaSubJenisStall("SERVICE PARKING"), "0", tgl, tgl2)

                def arr = ["SPOORING", "PDS", "DIO", "CUCI", "SERVICE PARKING"]

                L = Stall.findAllByCompanyDealerAndJenisStallAndSubJenisStallNotInListAndStaDelAndDateCreatedBetween(nabeng,
                        JenisStall.findByM021NamaJenisStall("LAIN-LAIN"),
                        SubJenisStall.findAllByM023NamaSubJenisStallInList(arr), "0", tgl, tgl2)
                M = F.size() + G.size() + H.size() + I.size() + J.size() + K.size()

                data.put("periode", tgl.format("MMMM yyyy").toUpperCase())
                data.put("no", no)
                data.put("namBeng", nabeng.m011NamaWorkshop)
                data.put("kolomA", A.size())
                data.put("kolomB", B.size())
                data.put("kolomC", C.size())
                data.put("kolomD", D.size())
                data.put("kolomE", E)
                data.put("kolomF", F.size())
                data.put("kolomG", G.size())
                data.put("kolomH", H.size())
                data.put("kolomI", I.size())
                data.put("kolomJ", J.size())
                data.put("kolomK", K.size())
                data.put("kolomL", L.size())
                data.put("kolomM", M)

                data.put("tKolomA", ta += A.size())
                data.put("tKolomB", tb += B.size())
                data.put("tKolomC", tc += C.size())
                data.put("tKolomD", td += D.size())
                data.put("tKolomE", te += E)
                data.put("tKolomF", tf += F.size())
                data.put("tKolomG", tg += G.size())
                data.put("tKolomH", th += H.size())
                data.put("tKolomI", ti += I.size())
                data.put("tKolomJ", tj += J.size())
                data.put("tKolomK", tk += K.size())
                data.put("tKolomL", tl += L.size())
                data.put("tKolomM", tm += M)

                reportData.add(data)

            }

        }
        if(params.namaReport=="12"){

            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")
            def company = CompanyDealer.get(params.workshop.toLong())
            def no=0

            def member = TrainingMember.createCriteria().list {
                training{
                    eq("staDel", "0")
                    eq("branch", company)
                    ge("dateBegin", strDate)
                    le("dateBegin", strEndDate)
                }
            }

            if(member.size() > 0){
                member.each {
                    def data = [:]
                    no++
                    data.put("cabang",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("noUrut",no)
                    data.put("namaKaryawan", it?.karyawan?.nama)
                    data.put("namaTraining",it?.training.namaTraining)
                    data.put("klsTraining",it?.training?.trainingType?.tipeTraining)
                    data.put("tgl1",it?.training?.dateBegin?.format("dd/MM/yyyy"))
                    data.put("tgl2",it?.training?.dateFinish?.format("dd/MM/yyyy"))
                    data.put("instruktur1",it?.training?.firstInstructor?.namaInstruktur ? it?.training?.firstInstructor?.namaInstruktur : "-")
                    data.put("instruktur2",it?.training?.secondInstructor?.namaInstruktur ? it?.training?.secondInstructor?.namaInstruktur : "-")
                    data.put("instruktur3",it?.training?.thirdInstructor?.namaInstruktur ? it?.training?.thirdInstructor?.namaInstruktur : "-")
                    data.put("nilai",it?.training?.pointMin)
                    data.put("rataNilai",it?.training?.pointAverage ? it?.training?.pointAverage : "-")
                    data.put("status",it?.staGraduate == "0" ? "Lulus" : "Tidak Lulus")

                    reportData.add(data)
                }
            }else{
                def data = [:]

                data.put("cabang",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("noUrut","-")
                data.put("namaKaryawan", "-")
                data.put("namaTraining", "-")
                data.put("klsTraining", "-")
                data.put("tgl1", "-")
                data.put("tgl2", "-")
                data.put("instruktur1", "-")
                data.put("instruktur2", "-")
                data.put("instruktur3", "-")
                data.put("nilai", "-")
                data.put("rataNilai", "-")
                data.put("status", "-")

                reportData.add(data)
            }


        }
        if(params.namaReport=="13"){
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")
            if(params.workshop!=""){
                def company = CompanyDealer.get(params.workshop.toLong())
            }
            def karyawan=Karyawan.get(params.karyawan.id)
            String staLulus =""
            def  history =TrainingMember.createCriteria().list {
                training{
                    eq("staDel","0")
                    if(params.workshop!=""){
                        eq("branch",CompanyDealer.get(params.workshop))
                    }
                    ge("dateBegin",strDate)
                    le("dateBegin",strEndDate)
                }
                eq("karyawan",karyawan)
            }

            if(history.size()<1)
            {
                def headers=[:]
                headers.put("periode",periode)
                headers.put("mulai","-")
                headers.put("selesai","-")
                headers.put("cabang",karyawan?.branch?.m011NamaWorkshop)
                headers.put("nama",karyawan?.nama)
                headers.put("jabatan",karyawan?.jabatan?.m014JabatanManPower)
                headers.put("namatraining","-")
                headers.put("kelastraining","-")
//                data.put("tanggal",it?.)
                headers.put("tanggalmulai","-")
                headers.put("tanggalselesai","-")
                headers.put("instruktur","-")
                headers.put("tempat","-")
                headers.put("nilai","-")
                headers.put("ratarata","-")
                headers.put("lulus","-")
                reportData.add(headers)
            }

            history.each {
                def data = [:]
                if(it.point<=it.training.pointMin){
                    staLulus = "Tidak"
                }else{
                    staLulus = "Lulus"
                }
                data.put("periode",periode)
                data.put("mulai",it?.training?.dateBegin.format("dd/MM/yyyy"))
                data.put("selesai",it?.training?.dateBegin.format("dd/MM/yyyy"))
                data.put("cabang",it?.training?.branch?.m011NamaWorkshop)
                data.put("nama",it?.karyawan?.nama)
                data.put("jabatan",it?.karyawan?.jabatan?.m014JabatanManPower)
                data.put("namatraining",it?.training?.namaTraining)
                data.put("kelastraining",it?.training?.trainingType?.tipeTraining)
//                data.put("tanggal",it?.)
                data.put("tanggalmulai",it?.training?.dateBegin.format("dd/MM/yyyy"))
                data.put("tanggalselesai",it?.training?.dateFinish.format("dd/MM/yyyy"))
                data.put("instruktur",it?.training?.firstInstructor?.namaInstruktur)
                data.put("tempat",it?.training?.trainingType?.tempatTraining ? it?.training?.trainingType?.tempatTraining : "-")
                data.put("nilai",it?.point)
                data.put("ratarata",it?.training.pointAverage)
                data.put("lulus",staLulus)
                reportData.add(data)

            }
        }
        if(params.namaReport=="14"){

            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")
            def company = CompanyDealer.get(params.workshop.toLong())
            def no=0

            def member = TrainingMember.createCriteria().list {
                training{
                    eq("staDel", "0")
                    eq("branch", company)
                    ge("dateBegin", strDate)
                    le("dateBegin", strEndDate)
                }
            }

            if(member.size() > 0){
                member.each {
                    no++
                    def data = [:]
                    data.put("cabang",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("noUrut",no)
                    data.put("namaKaryawan",it?.karyawan?.nama)
                    data.put("namaTraining",it?.training?.namaTraining)
                    data.put("tgl1",it?.training?.dateBegin?.format("dd/MM/yyyy"))
                    data.put("tgl2",it?.training?.dateFinish?.format("dd/MM/yyyy"))
                    data.put("nilai",it?.point ? it?.point : "-")
                    data.put("rataNilai",it?.training?.pointMin ? it?.training?.pointMin : "-")
                    data.put("status",it?.staGraduate == "0" ? "Lulus" : "Tidak Lulus")
                    data.put("tglLulus",it?.tglGraduate ? it?.tglGraduate?.format("dd/MM/yyyy") : "-")

                    reportData.add(data)
                }
            }else {
                def data = [:]
                data.put("cabang",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("noUrut", "-")
                data.put("namaKaryawan", "-")
                data.put("namaTraining", "-")
                data.put("tgl1", "-")
                data.put("tgl2", "-")
                data.put("nilai", "-")
                data.put("rataNilai", "-")
                data.put("status", "-")
                data.put("tglLulus", "-")

                reportData.add(data)
            }
        }
        if(params.namaReport=="15"){
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")
            def company = CompanyDealer.get(params.workshop.toLong())
            def karyawan = Karyawan.get(params.karyawan.id)
            def listCertification = CertificationKaryawan.createCriteria().list {
                eq("staDel","0")
                eq("karyawan",karyawan)
                ge("tglSertifikasi",strDate)
                lt("tglSertifikasi",strEndDate)
            }
            if(listCertification.size()<1){
                def headers = [:]
                headers.put("cabang",karyawan.branch.m011NamaWorkshop)
                headers.put("nama",karyawan?.nama)
                headers.put("jabatan",karyawan?.jabatan?.m014JabatanManPower)
                headers.put("periode",periode)
                headers.put("sertifikasi","-")
                headers.put("mulai","-")
                headers.put("selesai","-")
                headers.put("nilai","-")
                headers.put("minLulus","-")
                headers.put("staLulus","-")
                headers.put("tglLulus","-")
                reportData.add(headers);
            }
            listCertification.each {
                String staLulus = ""
                def data = [:]
                def training = it.training
                def members = TrainingMember.findByKaryawanAndTrainingAndStaDel(it.karyawan,training,"0")
                if(members){
                    if(members.point<=training.pointMin){
                        staLulus = "Tidak"
                    }else{
                        staLulus = "Ya"
                    }
                }
                data.put("sertifikasi",it.tipeSertifikasi.namaSertifikasi)
                data.put("mulai",training.dateBegin.format("dd/MM/yyyy"))
                data.put("selesai",training.dateBegin.format("dd/MM/yyyy"))
                data.put("nilai",members?.point?members?.point:"")
                data.put("minLulus",training.pointMin?training.pointMin:"")
                data.put("staLulus",staLulus)
                data.put("tglLulus",members?.tglGraduate?members?.tglGraduate.format("dd/MM/yyyy"):"")
                reportData.add(data)
            }

        }
        if(params.namaReport=="16"){

            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")
            def company = CompanyDealer.get(params.workshop.toLong())
            def no=0

            def cls = TrainingClassRoom.createCriteria().list {
                eq("staDel", "0")
                eq("branch", company)
                ge("dateBegin", strDate)
                le("dateBegin", strEndDate)
            }

            if(cls.size() > 0){
                cls.each {
                    no++
                    def data = [:]
                    data.put("cabang",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                    data.put("periode", periode)
                    data.put("noUrut",no)
                    data.put("namaTraining",it?.namaTraining)
                    data.put("rataNilai",it?.pointMin)
                    data.put("tgl1",it?.dateBegin?.format("dd/MM/yyyy"))
                    data.put("tgl2",it?.dateFinish?.format("dd/MM/yyyy"))
                    data.put("batch",it?.batch)
                    data.put("tempat",it?.trainingType?.tempatTraining ? it?.trainingType?.tempatTraining : "-")
                    data.put("instruktur",it?.firstInstructor?.namaInstruktur ? it?.firstInstructor?.namaInstruktur : "-")

                    reportData.add(data)
                }
            }else {
                def data = [:]
                data.put("cabang",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("periode", periode)
                data.put("noUrut", "-")
                data.put("namaTraining", "-")
                data.put("rataNilai", "-")
                data.put("tgl1", "-")
                data.put("tgl2", "-")
                data.put("batch", "-")
                data.put("tempat", "-")
                data.put("instruktur", "-")

                reportData.add(data)
            }
        }
        if(params.namaReport=="17"){
            Date tgl = new Date().parse('dd-MM-yyyy',params.tanggal)
            Date tglAkhir = new Date().parse('dd-MM-yyyy',params.tanggal2)
            def no=0
            def c= TrainingClassRoom.createCriteria().list {
                eq("staDel","0")
                eq("branch", CompanyDealer.findById(params.workshop as Long))
                ge("dateBegin",tgl)
                lt("dateFinish", tglAkhir + 1)
            }
            c.each { tc ->
                def tm = TrainingMember.createCriteria().list {
                    training{
                        eq("id", tc.id)
                    }
                }

                def data = [:]
                no++
                data.put("companyDealer",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("no",no)
                data.put("periode","Tanggal : " + (tgl.format("dd MMMM yyyy").toUpperCase()) + " - " + (tglAkhir.format("dd MMMM yyyy").toUpperCase()))
                data.put("namaSertifikasi",tc?.certificationType?.namaSertifikasi)
                data.put("standardLulus",tc?.pointMin)
                data.put("tglMulai",tc?.dateBegin ? tc?.dateBegin.format("dd/MM/yyyy") : "")
                data.put("tglAkhir",tc?.dateFinish ? tc?.dateFinish.format("dd/MM/yyyy") : "")
                data.put("batch",tc?.batch)
                data.put("tglPengumuman",tm.size() > 0 ? tm?.last()?.tglGraduate.format("dd/MM/yyyy") : "")
                reportData.add(data)
            }
            if(c.size()==0){
                def data = [:]
                data.put("companyDealer",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("no",no)
                data.put("periode","Tanggal Mulai : " + (tgl.format("dd MMMM yyyy").toUpperCase()) + " - Tanggal Akhir : " + (tglAkhir.format("dd MMMM yyyy").toUpperCase()))
                data.put("namaSertifikasi","")
                data.put("standardLulus","")
                data.put("tglMulai","")
                data.put("tglAkhir","")
                data.put("batch","")
                data.put("tglPengumuman","")

                reportData.add(data)
            }
        }
        if(params.namaReport=="18"){
            def no=0
            def trainingInstruction=TrainingInstructor.createCriteria().list {
                eq("staDel","0")
                eq("companyDealer", CompanyDealer.findById(params.workshop as Long))

            }

            trainingInstruction.each {

                def data = [:]
                no++
                data.put("no",no)
                data.put("nama",it.namaInstruktur ? it.namaInstruktur:"-")
                data.put("cabang",it.companyDealer?.m011NamaWorkshop ? it.companyDealer?.m011NamaWorkshop:"-")
                data.put("jabatan",it.manPower?.m014JabatanManPower ?it.manPower?.m014JabatanManPower:"-" )
                data.put("sertifikasiterakhir",it.certificationType?.namaSertifikasi ? it.certificationType?.namaSertifikasi:"-")
                data.put("tipeinstruktur",it.jenisInstruktur ? it.jenisInstruktur:"-")
                data.put("aktiftidakaktif",it.th020isAktif=="0"? "Aktif":"Tidak Aktif")
                data.put("kelastraining",it.trainingClassRoom?.namaTraining ? it.trainingClassRoom?.namaTraining:"-" )
                reportData.add(data)
            }
            return reportData
        }
        if(params.namaReport=="19"){
            def periode = strDate.format("d MMMM yyyy") + " - " + strEndDate.format("d MMMM yyyy")
            def company = CompanyDealer.get(params.workshop.toLong())


            def detil = TrainingClassRoom.createCriteria().list {
                eq("staDel", "0")
                eq("branch", company)
                ge("dateBegin", strDate)
                le("dateBegin", strEndDate)
            }

            detil.each {
                def data = [:]
                data.put("cabang",CompanyDealer.findById(params.workshop as Long).m011NamaWorkshop)
                data.put("namainstruktur",TrainingInstructor.findById(params.selectinstruktur as Long)?.namaInstruktur ? TrainingInstructor.findById(params.selectinstruktur as Long)?.namaInstruktur:"-" )
                data.put("periode", periode)
                data.put("kelastraining",it?.namaTraining ? it?.namaTraining:"-")
                data.put("nilairatarata", it?.pointAverage ? it?.pointAverage:"-" )
                data.put("nilaimengajar", it?.pointMax ? it?.pointMax:"-")
                data.put("tglawal", it?.dateBegin.format("d MMMM yyyy"))
                data.put("tglakhir", it?.dateFinish.format("d MMMM yyyy"))
                data.put("tempat", it?.trainingType?.tempatTraining ? it?.trainingType?.tempatTraining:"-" )

                reportData.add(data)
            }
        }
        if(params.namaReport=="20"){

            def company = CompanyDealer.get(params.workshop.toLong())
            def train = TrainingClassRoom.get(params.selecttraining.toLong())


            def sertifikat = TrainingMember.createCriteria().list {
                training{

                    eq("staDel", "0")
                    eq("branch", company)
                    ge("dateBegin", strDate)
                    le("dateBegin", strEndDate)
                    eq("id",train.id )
                }
            }



            sertifikat.each {
                def data = [:]
                data.put("namatraining",it?.training?.namaTraining ? it.training?.namaTraining:"-")
                data.put("nilairatarata",it?.training?.pointAverage ? it.training?.pointAverage:"-")
                data.put("instruktur",it?.training?.firstInstructor?.namaInstruktur +","? it?.training?.firstInstructor?.namaInstruktur +", ":"-" )
                data.put("instruktur2",it?.training?.secondInstructor?.namaInstruktur ? it?.training?.secondInstructor?.namaInstruktur :"-" +",")
                data.put("instruktur3",it?.training?.thirdInstructor?.namaInstruktur ? it?.training?.thirdInstructor?.namaInstruktur:"-")
                data.put("tglmulai",it?.training?.dateBegin.format("d MMMM yyyy"))
                data.put("tglakhir",it?.training?.dateFinish.format("d MMMM yyyy"))
                data.put("namapeserta",it?.karyawan?.nama ? it?.karyawan?.nama:"-")
                data.put("nilai",it?.point ? it?.point:"-" )
                data.put("lulustidak",it?.staGraduate == "0" ? "Lulus" : "Tidak Lulus" ? it?.staGraduate == "0" ? "Lulus" : "Tidak Lulus":"-")

                reportData.add(data)
            }
        }

        return reportData
    }
}
