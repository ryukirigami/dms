package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class CutiKaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def cutiKaryawanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.companyDealer=session.userCompanyDealer
		render cutiKaryawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = cutiKaryawanService.create(params)

        if(!result.error)
            return [cutiKaryawanInstance: result.cutiKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer=session.userCompanyDealer
		 def result = cutiKaryawanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["CutiKaryawan", result.cutiKaryawanInstance.id])
            redirect(action:'show', id: result.cutiKaryawanInstance.id)
            return
        }

        render(view:'create', model:[cutiKaryawanInstance: result.cutiKaryawanInstance])
	}

	def show(Long id) {
		def result = cutiKaryawanService.show(params)

		if(!result.error)
			return [ cutiKaryawanInstance: result.cutiKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
		def result = cutiKaryawanService.show(params)

		if(!result.error)
			return [ cutiKaryawanInstance: result.cutiKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
         params.dateCreated = datatablesUtilService?.syncTime()
         params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = cutiKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["CutiKaryawan", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[cutiKaryawanInstance: result.cutiKaryawanInstance.attach()])
	}

	def delete() {
		def result = cutiKaryawanService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["CutiKaryawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			/*datatablesUtilService.massDelete(CutiKaryawan, params)
			res.message = "Mass Delete Success"*/
           /* log.info(params.ids)
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            jsonArray.each { oList << domain.get(it)  }

            oList*.discard() //detach all the objects from session
            oList.each{ it.delete() }*/
            def jsonArray = JSON.parse(params.ids)
            def oList = []
            jsonArray.each {oList << CutiKaryawan.get(new Long(it))}
            oList.each {
                CutiKaryawan ck = it
                Karyawan k = ck.karyawan
                SisaCutiKaryawan sisa = SisaCutiKaryawan.findByKaryawanAndTahunCuti(k, ck.tahunCuti)
                sisa.sisaHariCuti = sisa.sisaHariCuti + ck.jumlahHariCuti
                it.delete(flush: true)
                res.message = "Mass Delete Success"
            }
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def sisaCutiTahunBerjalan() {
        def res = [:]
        res.sisaCutiTahunBerjalan = 12
        try {
            /*Karyawan karyawan = Karyawan.findById(new Long(params.id))*/
            //CutiKaryawan cutiKaryawan = CutiKaryawan.findByKaryawan(karyawan)
            /*SisaCutiKaryawan sisa = SisaCutiKaryawan.findByKaryawanAndTahunCuti(karyawan, params.tahun as int)*/


            def sisaCriteria = SisaCutiKaryawan.createCriteria().list {
               karyawan {
                   eq ("id", new Long(params.id))
               }
               eq("tahunCuti", Integer.parseInt(params.tahun))
            }

            if (sisaCriteria.size() > 0) {
                SisaCutiKaryawan s = sisaCriteria.last()
                print "===> " + s.sisaHariCuti
                res.sisaCutiTahunBerjalan = s.sisaHariCuti
            }
            /*int jumlahCuti = 0
            if (cutiKaryawan) {
                if (cutiKaryawan.jumlahHariCuti)
                    jumlahCuti = cutiKaryawan.jumlahHariCuti
            }*/
           /* SisaCutiKaryawan sisaCutiKaryawan = SisaCutiKaryawan.findById(new Long(params.id))
            if (sisaCutiKaryawan) {
                res.sisaCutiTahunBerjalan = sisaCutiKaryawan.sisaHariCuti
            }*/
            /*res.sisaCutiTahunBerjalan = 12 - jumlahCuti*/
        } catch (e) {
            res.error = e.message
            log.error(e.message, e)
        }

        render res as JSON
    }

}
