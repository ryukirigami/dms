package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class PendidikanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def pendidikanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render pendidikanService.datatablesList(params) as JSON
	}

	def create() {
		def result = pendidikanService.create(params)

        if(!result.error)
            return [pendidikanInstance: result.pendidikanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.staDel = '0'
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = pendidikanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Pendidikan", result.pendidikanInstance.id])
            redirect(action:'show', id: result.pendidikanInstance.id)
            return
        }

        render(view:'create', model:[pendidikanInstance: result.pendidikanInstance])
	}

	def show(Long id) {
		def result = pendidikanService.show(params)

		if(!result.error)
			return [ pendidikanInstance: result.pendidikanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def result = pendidikanService.show(params)

		if(!result.error)
			return [ pendidikanInstance: result.pendidikanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = pendidikanService.update(params)
        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Pendidikan", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[pendidikanInstance: result.pendidikanInstance.attach()])
	}

	def delete() {

        // delete staDel
        params.staDel = "1"

		def result = pendidikanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Pendidikan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
            if (params.ids && (params.type as Integer) == 1) {
                def ids = []
                def c = Pendidikan.createCriteria()
                def parsedIds = JSON.parse(params.ids)
                def results = c.list {
                    parsedIds.each {
                        c.notEqual("id", new Long(it))
                    }
                }

                results.each {
                    ids << String.valueOf(it.id)
                }

                params.ids = (ids as JSON).toString()
            }
			datatablesUtilService.massDeleteStaDel(Pendidikan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
}
