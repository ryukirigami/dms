package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class TerminalController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def terminalService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render terminalService.datatablesList(params) as JSON
	}

	def create() {
		def result = terminalService.create(params)

        if(!result.error)
            return [terminalInstance: result.terminalInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
		 def result = terminalService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Terminal", result.terminalInstance.id])
            redirect(action:'show', id: result.terminalInstance.id)
            return
        }

        render(view:'create', model:[terminalInstance: result.terminalInstance])
	}

	def show(Long id) {
		def result = terminalService.show(params)

		if(!result.error)
			return [ terminalInstance: result.terminalInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = terminalService.show(params)

		if(!result.error)
			return [ terminalInstance: result.terminalInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = terminalService.update(params)
        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Terminal", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[terminalInstance: result.terminalInstance.attach()])
	}

	def delete() {
		def result = terminalService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Terminal", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(Terminal, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	

}
