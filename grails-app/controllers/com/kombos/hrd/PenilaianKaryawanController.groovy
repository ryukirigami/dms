package com.kombos.hrd

import com.kombos.administrasi.CompanyDealer
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PenilaianKaryawanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    //def penilaianKaryawanService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
        params.companyDealer = session.userCompanyDealer
    }

    def list(Integer max) {
    }

    def datatablesList(params) {
        String dateFormat = appSettingParamDateFormat.value ? appSettingParamDateFormat.value : appSettingParamDateFormat.defaultValue

        def ret
        def propertiesToRender = params.sColumns.split(",")
        def sortDir = params.sSortDir_0?.equalsIgnoreCase('asc') ? 'asc' : 'desc'
        def sortProperty = propertiesToRender[params.iSortCol_0 as int]
        def x = 0

        session.exportParams = params

        def c = PenilaianKaryawan.createCriteria()
        def results = c.list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            eq("stadel","0")
            if(params?.ho){
                if(params?.cabang){
                    eq("companyDealer",CompanyDealer.get(params?.cabang?.toLong()))
                }
            }else{
                eq("companyDealer",session.userCompanyDealer)
            }
            if (params."sCriteria_karyawan") {
                karyawan {
                    ilike("nama", "%" + (params."sCriteria_karyawan" as String) + "%")
                }
            }
            if (params."sCriteria_periodeTahun") {
                ilike("periode", "%" + (params."sCriteria_periodeTahun" as String) + "%")
            }

            if(params."sCriteria_penilaiKaryawan"){
                ilike("penilai", "%" + (params."sCriteria_penilaiKaryawan" as String) + "%")
            }


            switch (sortProperty) {
                case "namaKaryawan":
                    karyawan{
                        order("nama",sortDir)
                    }
                    break;
                default:
                    order(sortProperty, sortDir)
                    break;
            }
        }

        def rows = []
        results.each {
            rows << [

                    id: it.id,

                    namaKaryawan: it?.karyawan?.nama,

                    periode: it?.periode,

                    penilai: it?.penilai?.nama,

                    jabatan: it?.jabatan?.m014JabatanManPower,

                    teknis: it?.teknis,

                    logika: it?.logika,

                    kecepatanKetelitian: it.kecepatanKetelitian,

                    loyalitas: it.loyalitas,

                    drive: it.drive,

                    disiplin: it.disiplin,

                    karyawan: it.karyawan?.nomorPokokKaryawan,

                    idPenilai : it.penilai,

            ]
        }

        ret = [sEcho: params.sEcho, iTotalRecords: results.totalCount, iTotalDisplayRecords: results.totalCount, aaData: rows]

        render ret as JSON
    }

    def create() {
        [penilaianKaryawanInstance: new PenilaianKaryawan(params)]
    }

    def save() {
        params.companyDealer = session.userCompanyDealer
        def penilaianKaryawanInstance = new PenilaianKaryawan(params)
        if(params.nmKaryawan ==""){
            flash.message = message(code: 'default.error.created.nmKaryawan.message', default: "Nama Karyawan tidak boleh kosong.")
            render(view: "create", model: [penilaianKaryawanInstance: penilaianKaryawanInstance])
            return
        }

        if(params.nmPenilai ==""){
            flash.message = message(code: 'default.error.created.nmPenilai.message', default: "Nama Penilai tidak boleh kosong.")
            render(view: "create", model: [penilaianKaryawanInstance: penilaianKaryawanInstance])
            return
        }

        penilaianKaryawanInstance?.setKaryawan(Karyawan.findByIdAndStaDel(params.karyawan.id, "0"))
        penilaianKaryawanInstance?.setPeriode(params.periode)
        penilaianKaryawanInstance?.setPenilai(Karyawan.findByIdAndStaDel(params.penilai.id, "0"))
        penilaianKaryawanInstance?.setJabatan(Karyawan.findByIdAndStaDel(params.penilai.id, "0").jabatan)
        penilaianKaryawanInstance?.setTeknis(Integer.parseInt(params.teknis))
        penilaianKaryawanInstance?.setLogika(Integer.parseInt(params.logika))
        penilaianKaryawanInstance?.setKecepatanKetelitian(Integer.parseInt(params.kecepatanKetelitian))
        penilaianKaryawanInstance?.setLoyalitas(Integer.parseInt(params.loyalitas))
        penilaianKaryawanInstance?.setDrive(Integer.parseInt(params.drive))
        penilaianKaryawanInstance?.setDisiplin((Integer.parseInt(params.disiplin)))
        penilaianKaryawanInstance?.setStadel("0")
        penilaianKaryawanInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        penilaianKaryawanInstance?.setLastUpdProcess("INSERT")
        penilaianKaryawanInstance?.setDateCreated(datatablesUtilService?.syncTime())
        penilaianKaryawanInstance?.setLastUpdated(datatablesUtilService?.syncTime())
        penilaianKaryawanInstance?.setCompanyDealer(Karyawan.findByIdAndStaDel(params.karyawan.id, "0") ? Karyawan.findByIdAndStaDel(params.karyawan.id, "0")?.branch : session?.userCompanyDealer)

        if (!penilaianKaryawanInstance.save(flush: true)) {
            render(view: "create", model: [penilaianKaryawanInstance: penilaianKaryawanInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'nmKaryawan.label', default: 'NamaKaryawan'), ''])
        redirect(action: "show", id: penilaianKaryawanInstance.id)
    }

    def show(Long id) {
        def penilaianKaryawanInstance = PenilaianKaryawan.get(id)
        if (!penilaianKaryawanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'nmKaryawan.label', default: 'NamaKaryawan'), id])
            redirect(action: "list")
            return
        }

        [penilaianKaryawanInstance: penilaianKaryawanInstance]
    }

    def edit(Long id) {
        def penilaianKaryawanInstance = PenilaianKaryawan.get(id)
        if (!penilaianKaryawanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'nmKaryawan.label', default: 'NamaKaryawan'), id])
            redirect(action: "list")
            return
        }

        [penilaianKaryawanInstance: penilaianKaryawanInstance]
    }

    def update(Long id, Long version) {
//        params.dateCreated = datatablesUtilService?.syncTime()
//        params.lastUpdated = datatablesUtilService?.syncTime()
        def penilaianKaryawanInstance = PenilaianKaryawan.get(id)
        if (!penilaianKaryawanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaKaryawan.label', default: 'PenilaianKaryawan'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (penilaianKaryawanInstance.version > version) {

                penilaianKaryawanInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'namaKaryawan.label', default: 'PenilaianKaryawan')] as Object[],
                        "Another user has updated this PenilaianKaryawan while you were editing")
                render(view: "edit", model: [penilaianKaryawanInstance: penilaianKaryawanInstance])
                return
            }
        }


        penilaianKaryawanInstance?.setKaryawan(Karyawan.findByIdAndStaDel(params.karyawan.id, "0"))
        penilaianKaryawanInstance?.setPeriode(params.periode)
        penilaianKaryawanInstance?.setPenilai(Karyawan.findByIdAndStaDel(params.penilai.id, "0"))
        penilaianKaryawanInstance?.setJabatan(Karyawan.findByIdAndStaDel(params.penilai.id, "0").jabatan)
        penilaianKaryawanInstance?.setTeknis(Integer.parseInt(params.teknis))
        penilaianKaryawanInstance?.setLogika(Integer.parseInt(params.logika))
        penilaianKaryawanInstance?.setKecepatanKetelitian(Integer.parseInt(params.kecepatanKetelitian))
        penilaianKaryawanInstance?.setLoyalitas(Integer.parseInt(params.loyalitas))
        penilaianKaryawanInstance?.setDrive(Integer.parseInt(params.drive))
        penilaianKaryawanInstance?.setDisiplin((Integer.parseInt(params.disiplin)))
        penilaianKaryawanInstance?.setStadel("0")
        penilaianKaryawanInstance?.setCreatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
        penilaianKaryawanInstance?.setLastUpdProcess("UPDATE")
        penilaianKaryawanInstance?.setDateCreated(datatablesUtilService?.syncTime())
        penilaianKaryawanInstance?.setLastUpdated(datatablesUtilService?.syncTime())

        if (!penilaianKaryawanInstance.save(flush: true)) {
            render(view: "edit", model: [penilaianKaryawanInstance: penilaianKaryawanInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'namaKaryawan.label', default: 'PenilaianKaryawan'), ''])
        redirect(action: "show", id: penilaianKaryawanInstance.id)
    }

    def delete() {
        def penilaianKaryawanInstance = PenilaianKaryawan.get(id)
        if (!penilaianKaryawanInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'namaKaryawan.label', default: 'PenilaianKaryawan'), id])
            redirect(action: "list")
            return
        }

        try {
            penilaianKaryawanInstance.setStadel("1")
            penilaianKaryawanInstance.setUpdatedBy(org.apache.shiro.SecurityUtils.subject.principal.toString())
            penilaianKaryawanInstance.setLastUpdProcess("DELETE")
            penilaianKaryawanInstance.save(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'namaKaryawan.label', default: 'PenilaianKaryawan'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'namaKaryawan.label', default: 'PenilaianKaryawan'), id])
            redirect(action: "show", id: id)
        }
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PenilaianKaryawan, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    def updatefield() {
        def res = [:]
        try {
            datatablesUtilService.updateField(PenilaianKaryawan, params)
            res.message = "Update Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }

    /*def getNamaKaryawan(){
        def res = [:]

        def result = Karyawan.findAllByStaDel('0')
        def opts = []
        result.each {
            opts << it.nama
        }

        res."options" = opts
        render res as JSON
    }

    def getIdKaryawan(){

        def karyawanID = params.nmKaryawan
        def nmKaryawan = Karyawan.findByNama(params.nmKaryawan)

        def res = [idKaryawan : nmKaryawan? nmKaryawan.nomorPokokKaryawan : ""]
        render res as JSON

    }

    def getNamaPenilai(){
        def res2 = [:]

        def result = Karyawan.findAllByStaDel('0')
        def opts = []
        result.each {
            opts << it.nama
        }

        res2."options" = opts
        render res2 as JSON
    }

    def getIdPenilaiJabatan(){

        def penilaiID = params.nmPenilai
        def nmPenilai = Karyawan.findByNama(params.nmPenilai)

        def res2 = [idPenilai : nmPenilai? nmPenilai.nomorPokokKaryawan : "", jabatanPen : nmPenilai? nmPenilai.jabatan.jabatan : ""]
        render res2 as JSON

    }*/
}
