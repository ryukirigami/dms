package com.kombos.hrd

import com.kombos.administrasi.KegiatanApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class HistoryWarningKaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService

	def historyWarningKaryawanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
        if(!KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.WARNING_KARYAWAN)){
            def kegiatanApprovalAdd = new KegiatanApproval()
            kegiatanApprovalAdd?.afterApprovalService = "historyWarningKaryawanService"
            kegiatanApprovalAdd?.createdBy= "SYSTEM"
            kegiatanApprovalAdd?.lastUpdProcess= "INSERT"
            kegiatanApprovalAdd?.dateCreated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.lastUpdated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.m770IdApproval= (KegiatanApproval.last().m770IdApproval.toInteger() + 1).toString()
            kegiatanApprovalAdd?.m770KegiatanApproval= KegiatanApproval.WARNING_KARYAWAN
            kegiatanApprovalAdd?.m770NamaDomain= "com.kombos.hrd.historyWarningKaryawan"
            kegiatanApprovalAdd?.m770NamaFieldDiupdate= "STA_APPROVAL"
            kegiatanApprovalAdd?.m770NamaFk = "STA_APPROVAL"
            kegiatanApprovalAdd?.m770NamaFormDetail = ""
            kegiatanApprovalAdd?.m770NamaTabel = "TH002_HISTORYWARNINGKARYAWAN"
            kegiatanApprovalAdd?.staDel= "0"
            kegiatanApprovalAdd?.m770StaButuhNilai = "0"
            kegiatanApprovalAdd?.save(flush: true)
            kegiatanApprovalAdd.errors.each{ println it }
        }
        params.companyDealer = session.userCompanyDealer
        redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
        params.companyDealer = session.userCompanyDealer
		session.exportParams=params

		render historyWarningKaryawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = historyWarningKaryawanService.create(params)

        if(!result.error)
            return [historyWarningKaryawanInstance: result.historyWarningKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.cd = session.userCompanyDealer
		 def result = historyWarningKaryawanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["HistoryWarningKaryawan", result.historyWarningKaryawanInstance.id])
            redirect(action:'show', id: result.historyWarningKaryawanInstance.id)
            return
        }

        render(view:'create', model:[historyWarningKaryawanInstance: result.historyWarningKaryawanInstance])
	}

	def show(Long id) {
		def result = historyWarningKaryawanService.show(params)

		if(!result.error)
			return [ historyWarningKaryawanInstance: result.historyWarningKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def result = historyWarningKaryawanService.show(params)

		if(!result.error)
			return [ historyWarningKaryawanInstance: result.historyWarningKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = historyWarningKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["HistoryWarningKaryawan", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[historyWarningKaryawanInstance: result.historyWarningKaryawanInstance.attach()])
	}

	def delete() {
		def result = historyWarningKaryawanService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["HistoryWarningKaryawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(HistoryWarningKaryawan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
}