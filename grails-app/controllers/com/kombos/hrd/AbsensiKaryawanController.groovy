package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import java.text.DateFormat
import java.text.SimpleDateFormat

class AbsensiKaryawanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def absensiKaryawanService

    def excelImportService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {

        redirect(action: "list", params: params)
       // Date skrg = new Date().parse("dd/MM/yyyy","01/"+new Date().format("MM")+"/"+new Date().format("yyyy"))
    }

    def list(Integer max) {
        params.companyDealer = session.userCompanyDealer
        def skrg = new Date().clearTime()
        def bsok = (new Date()+1).clearTime()
        String staAkhirBulan = "N"
        if(skrg.format("MM")!=bsok.format("MM")){
            staAkhirBulan = "Y"
        }
        [skrg : skrg,staAkhirBulan : staAkhirBulan, dataList : "testerDariList"]


    }

    def updateDetail(){
        def p = AbsensiKaryawanDetail.get(params.id)
        p.status = params.status
        p.keterangan = params.ket
        p.tanggal.hours = params.jam as int
        p.tanggal.minutes = params.menit as int
        p?.lastUpdated = datatablesUtilService?.syncTime()
        p.save(flush: true)
        render "ok"
    }

    def listDetail() {
        def absensi = AbsensiKaryawan.findById(params.id as Long)
        String data = absensi.bulanTahun
        def tahun = data.substring(2)
        def bulan = Date.parse("MM",data.substring(0,2)).format("MMMM")
        def bulanTahun = bulan +" "+ tahun
        [absensi : absensi,bulanTahun:bulanTahun]
    }
    def hapusDetail(){
        def absensiD = AbsensiKaryawanDetail.findById(params.id as Long)
        absensiD.staDel = "1"
        absensiD.save(flush : true)

        def absensiKaryawanInstance = AbsensiKaryawan.get(absensiD.absensiKaryawan.id)
        if(absensiD.status=="1"){
            absensiKaryawanInstance.attend = (AbsensiKaryawan.get(absensiD.absensiKaryawan.id).attend-1)
        }else if(absensiD.status=="5"){
            absensiKaryawanInstance.permit = (AbsensiKaryawan.get(absensiD.absensiKaryawan.id).permit-1)
        }else if(absensiD.status=="6"){
            absensiKaryawanInstance.absent = (AbsensiKaryawan.get(absensiD.absensiKaryawan.id).absent-1)
        }else if(absensiD.status=="7"){
            absensiKaryawanInstance.absent = (AbsensiKaryawan.get(absensiD.absensiKaryawan.id).leave-1)
        }
        render "ok"
    }
    def listUpload() {

    }

    def datatablesList() {
        session.exportParams = params
        params.companyDealer = session.userCompanyDealer
        render absensiKaryawanService.datatablesList(params) as JSON
    }

    def datatablesListDetail() {
        session.exportParams = params

        render absensiKaryawanService.datatablesListDetail(params) as JSON
    }

    def create() {
        def skrg = new Date().clearTime()
        def bsok = (new Date()+1).clearTime()
        String staAkhirBulan = "N"
        if(skrg.format("MM")!=bsok.format("MM")){
            staAkhirBulan = "Y"
        }
//        []

        def result = absensiKaryawanService.create(params)

        if (!result.error)
            return [absensiKaryawanInstance: result.absensiKaryawanInstance ,skrg : skrg,staAkhirBulan : staAkhirBulan, dataList : "testerDariList"]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')




    }

    def save() {
        params.companyDealer=session.userCompanyDealer
        def result = absensiKaryawanService.save(params)

        if (!result.error) {
            flash.message = result.error
            redirect(action: 'show', id: result.absensiKaryawanInstance.id)
            return
        }
        render(view: 'create', model: [absensiKaryawanInstance: result.absensiKaryawanInstance])
    }

    def show(Long id) {
        def result = absensiKaryawanService.show(params)

        if (!result.error)
            return [absensiKaryawanInstance: result.absensiKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def editForm() {
        def absD = []
        def absensi = AbsensiKaryawanDetail.get(params.id as Long)
        absD << [
                keterangan :absensi.keterangan,
                id :absensi.id,
                jam : absensi.tanggal.hours,
                tgl : absensi.tanggal,
                menit : absensi.tanggal.minutes,
                masuk : absensi.status,
        ]
        render absD as JSON
    }

    def update(Long id, Long version) {
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.lastUpdProcess = "UPDATE"
        def result = absensiKaryawanService.update(params)
        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["AbsensiKaryawan", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [absensiKaryawanInstance: result.absensiKaryawanInstance.attach()])
    }

    def delete() {
        def result = absensiKaryawanService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["AbsensiKaryawan", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def hasil=absensiKaryawanService.massDelete(params)
        render "ok"
    }

    def namaKaryawan() {
        def res = [:]
        def result = Karyawan.createCriteria().list {
            eq("staDel","0")
            if(!session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")){
                eq("branch",session.userCompanyDealer)
            }
        }
        def opts = []
        result.each {
            opts << it.nama
        }

        res."options" = opts
        render res as JSON
    }
    def viewUpload() {
        DateFormat df = new SimpleDateFormat("yyyy-M-dd HH:mm")

        //handle upload file
        Map CONFIG_JOB_COLUMN_MAP = [
                sheet:'Sheet1',
                startRow: 1,
                columnMap:  [
                        //Col, Map-Key
                        'A':'npk',
                        'B':'tanggal',
                        'C':'status',
                        'D':'terminal',
                ]
        ]

        CommonsMultipartFile uploadExcel = null
        if(request instanceof MultipartHttpServletRequest){
            MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
            uploadExcel = (CommonsMultipartFile)multipartHttpServletRequest.getFile("fileExcel");
        }

        def jsonData = ""
        int jmlhDataError = 0;
        String htmlData = "",cekData = ""
        if(!uploadExcel?.empty){
            //validate content type
            def okcontents = [
                    'application/excel','application/vnd.ms-excel','application/octet-stream','text/plain'
            ]
            if (!okcontents.contains(uploadExcel?.getContentType())) {
                flash.message = "Illegal Content Type"
                uploadExcel = null
                render(view: "listUpload")
                return
            }

            Workbook workbook = WorkbookFactory.create(uploadExcel.inputStream)
            //Iterate through bookList and create/persists your domain instances
            def absensiList = excelImportService.columns(workbook, CONFIG_JOB_COLUMN_MAP)

            jsonData = absensiList as JSON

            String status = "0", style = ""


            absensiList?.each {
                htmlData+="<tr "+style+">\n" +
                        "                            <td>\n" +
                        "                                "+it.npk+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.tanggal+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.status+"\n" +
                        "                            </td>\n" +
                        "                            <td>\n" +
                        "                                "+it.terminal+"\n" +
                        "                            </td>\n" +


                        "                        </tr>"
                status = "0"

            }
            if(jmlhDataError>0){
                flash.message = message(code: 'default.uploadGroupDiskon.message', default: "Read File Done : Terdapat data yang tidak valid, cek baris yang berwarna merah, baris warna biru data sudah ada")
            } else {
                flash.message = message(code: 'default.uploadGoodsDiskon.message', default: "Read File Done")
            }

            render(view: "listUpload", model: [htmlData:htmlData, jsonData:jsonData, jmlhDataError:jmlhDataError])
        }
    }
    def saveUpload() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd")

        def absensi = null
         def requestBody = request.JSON

        requestBody.each{
            def stts = ""
            if(it.status == "Masuk"){
                    stts = "1"
            }
            else if(it.status == "Pulang"){
                stts = "2"
            }
            else if(it.status == "Istirahat"){
                stts = "3"
            }
            else if(it.status == "Masuk Istirahat"){
                stts = "4"
            }

            else if(it.status == "Ijin"){
                stts = "5"
            }
            else if(it.status == "Alpha"){
                stts = "6"
            }
            else if(it.status == "Sakit"){
                stts = "7"
            }
            Date date = Date.parse("dd/MM/yyyy HH:mm:ss",it.tanggal as String)
            Date dateSrc = Date.parse("dd/MM/yyyy",date.format("dd/MM/yyyy").toString())
            def karyawan = Karyawan.findByNomorPokokKaryawan(it.npk)
            def terminal = Terminal.findByNamaTerminal(it.terminal)
            def bulanTahun = date.format("MMyyyy").toString()

            def data = AbsensiKaryawan.findByKaryawanAndBulanTahun(karyawan,bulanTahun)
            if(AbsensiKaryawan.findByKaryawanAndBulanTahun(karyawan,bulanTahun)){
                absensi = AbsensiKaryawan.get(data.id)
                if(!AbsensiKaryawanDetail.findByAbsensiKaryawanAndStatusAndStaDelAndTanggalGreaterThanEqualsAndTanggalLessThan(data,1,'0',dateSrc,dateSrc + 1)){
                    absensi.attend = (AbsensiKaryawan.get(data.id).attend + 1)
                    absensi.save(flush: true)
                }
            }else{
                absensi = new AbsensiKaryawan()
                absensi.karyawan = karyawan
                absensi.bulanTahun = bulanTahun
                absensi.attend = 1
                absensi.absent = 0
                absensi.leave = 0
                absensi.permit = 0
                absensi.overtime = 0
                absensi.companyDealer=session.userCompanyDealer
                absensi.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
                absensi.lastUpdProcess = "INSERT"
                absensi?.dateCreated = datatablesUtilService?.syncTime()
                absensi?.lastUpdated = datatablesUtilService?.syncTime()
                absensi.setStaDel('0')
                absensi.save(flush: true)
            }
            // success
            def akd = new AbsensiKaryawanDetail()
            akd?.absensiKaryawan = absensi
            akd?.tanggal = date
            akd?.status = stts
            akd?.terminal = terminal
            akd?.staDel = "0"
            akd?.dateCreated = datatablesUtilService?.syncTime()
            akd?.lastUpdated = datatablesUtilService?.syncTime()
            akd?.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
            akd?.lastUpdProcess = "INSERT"
            akd?.save(flush: true)
            akd.errors.each {
                println it
            }

                flash.message = message(code: 'default.goodsDiskon.message', default: "Save Done")
        }

        render "ok"

    }
}