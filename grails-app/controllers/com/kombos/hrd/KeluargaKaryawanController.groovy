package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class KeluargaKaryawanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def keluargaKaryawanService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render keluargaKaryawanService.datatablesList(params) as JSON
    }

    def create() {
        def result = keluargaKaryawanService.create(params)

        if (!result.error)
            return [keluargaKaryawanInstance: result.keluargaKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.lastUpdProcess = "INSERT"
        params.staDel = "0"
        params.createdBy = org.apache.shiro.SecurityUtils.subject.principal.toString()
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = keluargaKaryawanService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["KeluargaKaryawan", result.keluargaKaryawanInstance.id])
            redirect(action: 'show', id: result.keluargaKaryawanInstance.id)
            return
        }

        render(view: 'create', model: [keluargaKaryawanInstance: result.keluargaKaryawanInstance])
    }

    def show(Long id) {
        def result = keluargaKaryawanService.show(params)

        if (!result.error)
            return [keluargaKaryawanInstance: result.keluargaKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = keluargaKaryawanService.show(params)

        if (!result.error)
            return [keluargaKaryawanInstance: result.keluargaKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = keluargaKaryawanService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["KeluargaKaryawan", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [keluargaKaryawanInstance: result.keluargaKaryawanInstance.attach()])
    }

    def delete() {
        def result = keluargaKaryawanService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["KeluargaKaryawan", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(KeluargaKaryawan, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}

