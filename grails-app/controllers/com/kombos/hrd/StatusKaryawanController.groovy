package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class StatusKaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def statusKaryawanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render statusKaryawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = statusKaryawanService.create(params)

        if(!result.error)
            return [statusKaryawanInstance: result.statusKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = statusKaryawanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["StatusKaryawan", result.statusKaryawanInstance.id])
            redirect(action:'show', id: result.statusKaryawanInstance.id)
            return
        }

        render(view:'create', model:[statusKaryawanInstance: result.statusKaryawanInstance])
	}

	def show(Long id) {
		def result = statusKaryawanService.show(params)

		if(!result.error)
			return [ statusKaryawanInstance: result.statusKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def result = statusKaryawanService.show(params)

		if(!result.error)
			return [ statusKaryawanInstance: result.statusKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = statusKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["StatusKaryawan", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }
        render(view:'edit', model:[statusKaryawanInstance: result.statusKaryawanInstance.attach()])
	}

	def delete() {
        // delete staDel
        params.staDel = "1"

		def result = statusKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["StatusKaryawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
            if (params.ids && (params.type as Integer) == 1) {
                def ids = []
                def c = StatusKaryawan.createCriteria()
                def parsedIds = JSON.parse(params.ids)
                def results = c.list {
                    parsedIds.each {
                        c.notEqual("id", new Long(it))
                    }
                }

                results.each {
                    ids << String.valueOf(it.id)
                }

                params.ids = (ids as JSON).toString()
            }
			datatablesUtilService.massDelete(StatusKaryawan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	

}
