package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class KaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def karyawanService
    def jasperService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
        params.companyDealer = session.userCompanyDealer
		redirect(action: "list", params: params)
	}

	def list(Integer max) {

	}

	def datatablesList() {

		session.exportParams=params
        params.companyDealer = session.userCompanyDealer
		render karyawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = karyawanService.create(params)
        def data = [:]
        def role = ""
        data.dealer = session.userCompanyDealer
        data.dealerId = session.userCompanyDealerId
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        roles.each {
            if(it.authority == "HRD")
                data.role = 'HRD'
            else if(it.authority.contains('PERSONALIA'))
                data.role = 'HRD'
        }
        if(!result.error)
            return [karyawanInstance: result.karyawanInstance, data : data]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        def data = [:]
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        params.role = 'HRD'
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        roles.each {
            if(it.authority == "HO")
                params.role = 'HO'
        }
        params.cd = session.userCompanyDealer
        data.role = params.role
		def result = karyawanService.save(params)
        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["Karyawan", result.karyawanInstance.id])
            redirect(action:'show', id: result.karyawanInstance.id)
            return
        }
        else if(result.error.code == "dataGanda"){
            flash.message = "   * NPK Sudah Ada"
        }
        render(view:'create', model:[karyawanInstance: result.karyawanInstance,data: data])
	}

	def show(Long id) {
		def result = karyawanService.show(params)

		if(!result.error)
			return [ karyawanInstance: result.karyawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def data = [:]
        def role = ""
        data.dealer = session.userCompanyDealer
        data.dealerId = session.userCompanyDealerId
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        roles.each {
            if(it.authority == "HRD")
                data.role = 'HRD'
        }

        def result = karyawanService.show(params)

		if(!result.error)
            result.history = karyawanService.findHistoryKaryawan(result.karyawanInstance)
			return [ karyawanInstance: result.karyawanInstance, historyKaryawanInstance: result.history, data:data ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = karyawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["Karyawan", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[karyawanInstance: result.karyawanInstance.attach()])
	}

	def delete() {
		def result = karyawanService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["Karyawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			karyawanService.massDelete(params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def createHistory() {

    }


    def previewtraining(){

        List<JasperReportDef> reportDefList = []
        def file = null
        if(params.aksi=='historyTraining'){
            def Trainingreport = Trainingreport(params)
            def reportDef = new JasperReportDef(name:'Training_history_karyawan.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: Trainingreport)
            reportDefList.add(reportDef)
            file = File.createTempFile("Training_history_karyawan",".xls")
        }
        if(params.aksi=='historySertifikasi'){
            def dataReport = calculateDataReport(params)
            def reportDef = new JasperReportDef(name:'SertifikasiHistory.jasper',
                    fileFormat:JasperExportFormat.PDF_FORMAT,
                    reportData: dataReport)
            reportDefList.add(reportDef)
            file = File.createTempFile("History_Sertifikasi",".xls")
        }
        file.deleteOnExit()
        FileUtils.writeByteArrayToFile(file, jasperService.generateReport(reportDefList).toByteArray())
        response.setHeader("Content-Type", "application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment;filename=${file.name}")
        response.outputStream << file.newInputStream()
    }


    def Trainingreport(def params){
        def reportData = new ArrayList();
        String nik=params.nik
        def data=[:]

//        def krywn=Karyawan.findByIdAndStaDel(nik.toLong(),"0")
          def krywn=Karyawan.get(params.nik.toLong())

        def listCertification = CertificationKaryawan.createCriteria().list {
            eq("staDel","0")
            eq("karyawan",krywn)}

            listCertification.each {
            def training = it.training
            def members = TrainingMember.findByKaryawanAndTrainingAndStaDel(it.karyawan,training,"0")
            data.put("cabang",krywn.branch.m011NamaWorkshop)
            data.put("nama",krywn?.nama)
            data.put("jabatan",krywn?.jabatan?.m014JabatanManPower)
            data.put("namatraining",training.namaTraining)
            data.put("kelastraining",training.trainingType?.tipeTraining)
            data.put("tanggal",it.tglSertifikasi)
            data.put("tanggalmulai",training.dateBegin.format("dd/MM/yyyy"))
            data.put("tanggalselesai",training.dateFinish.format("dd/MM/yyyy"))
            data.put("instruktur",training.firstInstructor)
            data.put("tempat",training.trainingType?.tempatTraining)
            data.put("nilai",members?.point)
            data.put("lulus",members?.staGraduate)
        }
        reportData.add(data)
        return reportData
    }

    def calculateDataReport(def params){
        def reportData = new ArrayList();
        def karyawan = Karyawan.get(params.nik.toLong())
        def listCertification = CertificationKaryawan.createCriteria().list {
            eq("staDel","0")
            eq("karyawan",karyawan)
        }
        listCertification.each {
            def data = [:]
            def training = it.training
            def members = TrainingMember.findByKaryawanAndTrainingAndStaDel(it.karyawan,training,"0")
            data.put("cabang",karyawan.branch.m011NamaWorkshop)
            data.put("nama",karyawan?.nama)
            data.put("jabatan",karyawan?.jabatan?.m014JabatanManPower)
            data.put("sertifikasi",it.tipeSertifikasi)
            data.put("mulai",training.dateBegin.format("dd/MM/yyyy"))
            data.put("selesai",training.dateBegin.format("dd/MM/yyyy"))
            data.put("nilai",members?.point)
            data.put("minLulus",training.pointMin)
            data.put("staLulus","")
            data.put("tglLulus",members?.tglGraduate)
            reportData.add(data)
        }
        return reportData
    }
}
