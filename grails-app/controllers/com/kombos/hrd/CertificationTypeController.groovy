package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class CertificationTypeController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def certificationTypeService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render certificationTypeService.datatablesList(params) as JSON
    }

    def create() {
        def result = certificationTypeService.create(params)

        if (!result.error)
            return [certificationTypeInstance: result.certificationTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.lastUpdProcess = "INSERT"
        params.companyDealer = session.userCompanyDealer
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = certificationTypeService.save(params)
        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["CertificationType", result.certificationTypeInstance.id])
            redirect(action: 'show', id: result.certificationTypeInstance.id)
            return
        }
        render(view: 'create', model: [certificationTypeInstance: result.certificationTypeInstance])
    }

    def show(Long id) {
        def result = certificationTypeService.show(params)

        if (!result.error)
            return [certificationTypeInstance: result.certificationTypeInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = certificationTypeService.show(params)

        if (!result.error)
            return [certificationTypeInstance: result.certificationTypeInstance]
        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = certificationTypeService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["CertificationType", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [certificationTypeInstance: result.certificationTypeInstance.attach()])
    }

    def delete() {
        def result = certificationTypeService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["CertificationType", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(CertificationType, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }


}
