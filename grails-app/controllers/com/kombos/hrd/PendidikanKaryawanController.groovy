package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class PendidikanKaryawanController {

    def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

    def datatablesUtilService

    def pendidikanKaryawanService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static viewPermissions = ['index', 'list', 'datatablesList']

    static addPermissions = ['create', 'save']

    static editPermissions = ['edit', 'update']

    static deletePermissions = ['delete']

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
    }

    def datatablesList() {
        session.exportParams = params

        render pendidikanKaryawanService.datatablesList(params) as JSON
    }

    def create() {
        def result = pendidikanKaryawanService.create(params)

        if (!result.error)
            return [pendidikanKaryawanInstance: result.pendidikanKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = pendidikanKaryawanService.save(params)

        if (!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["PendidikanKaryawan", result.pendidikanKaryawanInstance.id])
            redirect(action: 'show', id: result.pendidikanKaryawanInstance.id)
            return
        }

        render(view: 'create', model: [pendidikanKaryawanInstance: result.pendidikanKaryawanInstance])
    }

    def show(Long id) {
        def result = pendidikanKaryawanService.show(params)

        if (!result.error)
            return [pendidikanKaryawanInstance: result.pendidikanKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def edit(Long id) {
        def result = pendidikanKaryawanService.show(params)

        if (!result.error)
            return [pendidikanKaryawanInstance: result.pendidikanKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
    }

    def update(Long id, Long version) {
        params.lastUpdProcess = "INSERT"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = pendidikanKaryawanService.update(params)

        if (!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["PendidikanKaryawan", params.id])
            redirect(action: 'show', id: params.id)
            return
        }

        if (result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action: 'list')
            return
        }

        render(view: 'edit', model: [pendidikanKaryawanInstance: result.pendidikanKaryawanInstance.attach()])
    }

    def delete() {
        def result = pendidikanKaryawanService.delete(params)

        if (!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["PendidikanKaryawan", params.id])
            redirect(action: 'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if (result.error.code == "default.not.found.message") {
            redirect(action: 'list')
            return
        }

        redirect(action: 'show', id: params.id)
    }

    def massdelete() {
        def res = [:]
        try {
            datatablesUtilService.massDelete(PendidikanKaryawan, params)
            res.message = "Mass Delete Success"
        } catch (e) {
            log.error(e.message, e)
            res.message = e.message ?: e.cause?.message
        }
        render "ok"
    }
}
