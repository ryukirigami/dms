package com.kombos.hrd

import com.kombos.administrasi.KegiatanApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class HistoryRewardKaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def historyRewardKaryawanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
        if(!KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.REWARD_KARYAWAN)){
            def kegiatanApprovalAdd = new KegiatanApproval()
            kegiatanApprovalAdd?.afterApprovalService = "historyRewardKaryawanService"
            kegiatanApprovalAdd?.createdBy= "SYSTEM"
            kegiatanApprovalAdd?.lastUpdProcess= "INSERT"
            kegiatanApprovalAdd?.dateCreated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.lastUpdated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.m770IdApproval= (KegiatanApproval.last().m770IdApproval.toInteger() + 1).toString()
            kegiatanApprovalAdd?.m770KegiatanApproval= KegiatanApproval.REWARD_KARYAWAN
            kegiatanApprovalAdd?.m770NamaDomain= "com.kombos.hrd.historyRewardKaryawan"
            kegiatanApprovalAdd?.m770NamaFieldDiupdate= "STA_APPROVAL"
            kegiatanApprovalAdd?.m770NamaFk = "STA_APPROVAL"
            kegiatanApprovalAdd?.m770NamaFormDetail = ""
            kegiatanApprovalAdd?.m770NamaTabel = "TH001_HISTORYREWARDKARYAWAN"
            kegiatanApprovalAdd?.staDel= "0"
            kegiatanApprovalAdd?.m770StaButuhNilai = "0"
            kegiatanApprovalAdd?.save(flush: true)
            kegiatanApprovalAdd.errors.each{ println it }
        }
        params.companyDealer = session.userCompanyDealer
        redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session.userCompanyDealer
		render historyRewardKaryawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = historyRewardKaryawanService.create(params)

        if(!result.error)
            return [historyRewardKaryawanInstance: result.historyRewardKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.cd = session.userCompanyDealer
		 def result = historyRewardKaryawanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["HistoryRewardKaryawan", result.historyRewardKaryawanInstance.id])
            redirect(action:'show', id: result.historyRewardKaryawanInstance.id, params: params)
            return
        }

        render(view:'create', model:[historyRewardKaryawanInstance: result.historyRewardKaryawanInstance])
	}

	def show(Long id, params) {
		def result = historyRewardKaryawanService.show(params)

		if(!result.error)
			return [ historyRewardKaryawanInstance: result.historyRewardKaryawanInstance, params: params]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def result = historyRewardKaryawanService.show(params)

		if(!result.error)
			return [ historyRewardKaryawanInstance: result.historyRewardKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = historyRewardKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["HistoryRewardKaryawan", params.id])
            redirect(action:'show', id: params.id, params: params)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[historyRewardKaryawanInstance: result.historyRewardKaryawanInstance.attach()])
	}

	def delete() {
		def result = historyRewardKaryawanService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["HistoryRewardKaryawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(HistoryRewardKaryawan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

}
