package com.kombos.hrd

import com.kombos.administrasi.KegiatanApproval
import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import com.kombos.baseapp.sec.shiro.User
import grails.converters.JSON

class HistoryKaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def historyKaryawanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
        if(!KegiatanApproval.findByM770KegiatanApproval(KegiatanApproval.HISTORY_KARYAWAN)){
            def kegiatanApprovalAdd = new KegiatanApproval()
            kegiatanApprovalAdd?.afterApprovalService = "historyKaryawanService"
            kegiatanApprovalAdd?.createdBy= "SYSTEM"
            kegiatanApprovalAdd?.lastUpdProcess= "INSERT"
            kegiatanApprovalAdd?.dateCreated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.lastUpdated    = datatablesUtilService?.syncTime()
            kegiatanApprovalAdd?.m770IdApproval= (KegiatanApproval.last().m770IdApproval.toInteger() + 1).toString()
            kegiatanApprovalAdd?.m770KegiatanApproval= KegiatanApproval.HISTORY_KARYAWAN
            kegiatanApprovalAdd?.m770NamaDomain= "com.kombos.hrd.historyKaryawan"
            kegiatanApprovalAdd?.m770NamaFieldDiupdate= "STA_APPROVAL"
            kegiatanApprovalAdd?.m770NamaFk = "STA_APPROVAL"
            kegiatanApprovalAdd?.m770NamaFormDetail = ""
            kegiatanApprovalAdd?.m770NamaTabel = "TH003_HISTORYKARYAWAN"
            kegiatanApprovalAdd?.staDel= "0"
            kegiatanApprovalAdd?.m770StaButuhNilai = "0"
            kegiatanApprovalAdd?.save(flush: true)
            kegiatanApprovalAdd.errors.each{ println it }
        }
        params.companyDealer = session.userCompanyDealer
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session.userCompanyDealer
		render historyKaryawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = historyKaryawanService.create(params)
        def data = [:]
        def role = ""
        data.dealer = session.userCompanyDealer
        data.dealerId = session.userCompanyDealerId
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        roles.each {
            if(it.authority == "HRD")
                data.role = 'HRD'
            else if(it.authority.contains('PERSONALIA'))
                data.role = 'HRD'
        }
        if(!result.error)
            return [historyKaryawanInstance: result.historyKaryawanInstance, data : data]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        String usernameShiro = org.apache.shiro.SecurityUtils.subject.principal
        def data = [:]
        User user = User.findByUsername(usernameShiro)
        def roles = user.roles
        params.role = 'HRD'
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
//        params.companyDealer = session.userCompanyDealer
        roles.each {
            if(it.authority == "HO")
                params.role = 'HO'
        }
        params.cd = session.userCompanyDealer
        data.role = params.role
		def result = historyKaryawanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["HistoryKaryawan", result.historyKaryawanInstance.id])
            redirect(action:'show', id: result.historyKaryawanInstance.id, params: params)
            return
        }

        render(view:'create', model:[historyKaryawanInstance: result.historyKaryawanInstance])
	}

	def show(Long id, params) {
		def result = historyKaryawanService.show(params)

		if(!result.error)
			return [ historyKaryawanInstance: result.historyKaryawanInstance, params: params]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def result = historyKaryawanService.show(params)

		if(!result.error)
			return [ historyKaryawanInstance: result.historyKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = historyKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["HistoryKaryawan", params.id])
            redirect(action:'show', id: params.id, params: params)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[historyKaryawanInstance: result.historyKaryawanInstance.attach()])
	}

	def delete() {
		def result = historyKaryawanService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["HistoryKaryawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(HistoryKaryawan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
}
