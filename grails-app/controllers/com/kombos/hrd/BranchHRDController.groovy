package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class BranchHRDController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def branchHRDService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render branchHRDService.datatablesList(params) as JSON
	}

	def create() {
		def result = branchHRDService.create(params)

        if(!result.error)
            return [branchHRDInstance: result.branchHRDInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
         params.lastUpdProcess = "INSERT"
         params.dateCreated = datatablesUtilService?.syncTime()
         params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = branchHRDService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["BranchHRD", result.branchHRDInstance.id])
            redirect(action:'show', id: result.branchHRDInstance.id)
            return
        }

        render(view:'create', model:[branchHRDInstance: result.branchHRDInstance])
	}

	def show(Long id) {
		def result = branchHRDService.show(params)

		if(!result.error)
			return [ branchHRDInstance: result.branchHRDInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
		def result = branchHRDService.show(params)

		if(!result.error)
			return [ branchHRDInstance: result.branchHRDInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}
	def update(Long id, Long version) {
         params.lastUpdProcess = "UPDATE"
         params.dateCreated = datatablesUtilService?.syncTime()
         params.lastUpdated = datatablesUtilService?.syncTime()
		 def result = branchHRDService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["BranchHRD", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[branchHRDInstance: result.branchHRDInstance.attach()])
	}

	def delete() {
		def result = branchHRDService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["BranchHRD", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(BranchHRD, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
}
