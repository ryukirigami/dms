package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class TrainingClassRoomController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def trainingClassRoomService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST", updatePesertaTraining: "POST", loadKaryawanPesertaTraining: "POST", inputHasilPeserta: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render trainingClassRoomService.datatablesList(params) as JSON
	}

	def create() {
		def result = trainingClassRoomService.create(params)

        if(!result.error)
            return [trainingClassRoomInstance: result.trainingClassRoomInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = trainingClassRoomService.save(params)
        if(!result.error) {
            TrainingClassRoom temp = TrainingClassRoom.get(result.trainingClassRoomInstance.id)
            def intstruk = TrainingInstructor.get(temp?.firstInstructor?.id)
            intstruk.trainingClassRoom = temp
            intstruk.save(flush: true)
            if(temp.secondInstructor){
                def new2 = TrainingInstructor.get(temp.secondInstructor?.id)
                new2.trainingClassRoom = temp
                new2.save(flush: true)
            }
            if(temp.thirdInstructor){
                def new3 = TrainingInstructor.get(temp.thirdInstructor?.id)
                new3.trainingClassRoom = temp
                new3.save(flush: true)
            }

            flash.message = g.message(code: "default.created.message", args: ["TrainingClassRoom", result.trainingClassRoomInstance.id])
            redirect(action:'show', id: result.trainingClassRoomInstance.id)
            return
        }

        render(view:'create', model:[trainingClassRoomInstance: result.trainingClassRoomInstance])
	}

	def show(Long id) {
		def result = trainingClassRoomService.show(params)

		if(!result.error)
			return [ trainingClassRoomInstance: result.trainingClassRoomInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def result = trainingClassRoomService.show(params)

		if(!result.error)
			return [ trainingClassRoomInstance: result.trainingClassRoomInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = trainingClassRoomService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["TrainingClassRoom", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[trainingClassRoomInstance: result.trainingClassRoomInstance.attach()])
	}

	def delete() {
		def result = trainingClassRoomService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["TrainingClassRoom", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(TrainingClassRoom, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

    def pesertaTraining() {

    }

    def pesertaTrainingTambahPeserta() {
        def retVal = [:]
        def result = TrainingClassRoom.findById(new Long(params.id as String))
        if (result) {
            retVal.namaTraining = result.namaTraining;
            retVal.id = result.id
            retVal.instruktur = "${result.firstInstructor?.namaInstruktur},${result.secondInstructor?.namaInstruktur},${result.thirdInstructor?.namaInstruktur}"
            retVal.tanggalTraining = "${result.dateBegin} - ${result.dateFinish}"
        } else {
            retVal.message = "Data tidak ditemukan"
        }

        render retVal as JSON
    }

    def loadKaryawanPesertaTraining() {
        def training = TrainingClassRoom.findById(Long.parseLong(params.id as String))
        def member = TrainingMember.findAllByTraining(training)
        def retVal = []
        member.each {
            retVal << [
                id: it.id,
                nomorPokokKaryawan: it.karyawan.nomorPokokKaryawan,
                namaKaryawan: it.karyawan.nama
            ]
        }
        [members: member, training: training]
    }

    /*
    ** Fungsi ini dipanggil ketika klik button
    ** simpan pada halaman peserta training - tambah peserta
     */
    def updatePesertaTraining() {
        def retVal = [:]
        def errMessage = []
        // jika ada existing data peserta yg dihapus
        if (params.deletedPeserta != -1) {
            String[] ids = (params.deletedPeserta as String).split(",")
            for (String id : ids) {
                Long lId = new Long(id.replace("[", "").replace("]", ""))
                def member = TrainingMember.findById(lId)
                if (member) {
                    member.delete(flush: true)
                }
            }
        }

        if (params.newPeserta) {
            def json = JSON.parse(params.newPeserta)

            json.each {j ->
                def karyawan = Karyawan.findById(new Long(j.karyawanId))
                def trainingClassRoom = TrainingClassRoom.findById(new Long(j.trainingId))
                def pesertaBaru = new TrainingMember()
                pesertaBaru.karyawan = karyawan
                pesertaBaru.training = trainingClassRoom
                pesertaBaru.dateCreated = datatablesUtilService.syncTime()
                pesertaBaru.lastUpdated = datatablesUtilService.syncTime()
                pesertaBaru.save(flush: true, failOnError: true)
                if (pesertaBaru.hasErrors()) {
                    log.error(pesertaBaru.errors)
                    errMessage.add(pesertaBaru.errors)
                }

            }
        }

        if (params.inputNilaiPeserta) {
            def json = JSON.parse(params.inputNilaiPeserta)

            json.each {j ->
                def peserta = TrainingMember.get(Long.parseLong(j.memberId as String))
                peserta.tglGraduate = Date.parse("dd/MM/yyyy", j.tglLulus)
                peserta.point =  Double.parseDouble(j.point)
                peserta.pointInstruktur =  Double.parseDouble(j.pointInstruktur)
                peserta.save()

                def trainClass = TrainingClassRoom.get(Long.parseLong(j.trainingId as String))
                trainClass.pointAverage =  Double.parseDouble(j.pointAvg)
                trainClass.pointMax =  Double.parseDouble(j.pointMax)
                trainClass.save()

                if (peserta.hasErrors()) {
                    log.error(peserta.errors)
                    errMessage.add(peserta.errors)
                }

            }
        }

        retVal.errors = errMessage
        render retVal as JSON
    }

    /*
    ** Fungsi ini dipanggil ketika
    ** user klik tombol '+' pada baris tabel
    ** peserta training
     */
    def getDetailPeserta() {
        def retVal = []
        if (params.id) {
            def karyawanList = TrainingClassRoom.findById(new Long(params.id))
            karyawanList.trainingMember.each {
                TrainingMember member = TrainingMember.findByKaryawan(it.karyawan)
                retVal << [
                        id: it.id,
                        nomorPokokKaryawan: it.karyawan?.nomorPokokKaryawan,
                        namaKaryawan: it.karyawan?.nama,
                        nilai: member?.point
                ]
            }
        }
        render retVal as JSON
    }

    def inputHasilPeserta() {
        def training = TrainingClassRoom.findById(Long.parseLong(params.id as String))
        def member = TrainingMember.findAllByTraining(training)
        def retVal = []
        member.each {
            retVal << [
                    id: it.id,
                    nomorPokokKaryawan: it.karyawan.nomorPokokKaryawan,
                    namaKaryawan: it.karyawan.nama
            ]
        }
        [members: member, training: training]
    }
}