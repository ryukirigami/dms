package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class TugasLuarKaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def tugasLuarKaryawanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
        params.companyDealer = session.userCompanyDealer
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params
        params.companyDealer = session.userCompanyDealer
		render tugasLuarKaryawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = tugasLuarKaryawanService.create(params)

        if(!result.error)
            return [tugasLuarKaryawanInstance: result.tugasLuarKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer=session.userCompanyDealer
        def result = tugasLuarKaryawanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["TugasLuarKaryawan", result.tugasLuarKaryawanInstance.id])
            redirect(action:'show', id: result.tugasLuarKaryawanInstance.id)
            return
        }

        render(view:'create', model:[tugasLuarKaryawanInstance: result.tugasLuarKaryawanInstance])
	}

	def show(Long id) {
		def result = tugasLuarKaryawanService.show(params)

		if(!result.error)
			return [ tugasLuarKaryawanInstance: result.tugasLuarKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        params.companyDealer=session.userCompanyDealer
        def result = tugasLuarKaryawanService.show(params)

		if(!result.error)
			return [ tugasLuarKaryawanInstance: result.tugasLuarKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = tugasLuarKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["TugasLuarKaryawan", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[tugasLuarKaryawanInstance: result.tugasLuarKaryawanInstance.attach()])
	}

	def delete() {
		def result = tugasLuarKaryawanService.delete(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["TugasLuarKaryawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
			datatablesUtilService.massDelete(TugasLuarKaryawan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}

}
