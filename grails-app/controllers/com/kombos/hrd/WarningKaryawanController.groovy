package com.kombos.hrd

import com.kombos.baseapp.AppSettingParam
import com.kombos.baseapp.AppSettingParamService
import grails.converters.JSON

class WarningKaryawanController {
	
	def appSettingParamDateFormat = AppSettingParam.findByCode(AppSettingParamService.DATE_FORMAT)

	def datatablesUtilService
	
	def warningKaryawanService

	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	static viewPermissions = ['index', 'list', 'datatablesList']
	
	static addPermissions = ['create', 'save']
	
	static editPermissions = ['edit', 'update']
	
	static deletePermissions = ['delete']
	
	def index() {
		redirect(action: "list", params: params)
        params.companyDealer = session.userCompanyDealer
	}

	def list(Integer max) {
	}

	def datatablesList() {
		session.exportParams=params

		render warningKaryawanService.datatablesList(params) as JSON
	}

	def create() {
		def result = warningKaryawanService.create(params)

        if(!result.error)
            return [warningKaryawanInstance: result.warningKaryawanInstance]

        flash.message = g.message(code: result.error.code, args: result.error.args)
        redirect(action: 'list')
	}

	def save() {
        params.lastUpdProcess = "INSERT"
        params.dateCreated = datatablesUtilService?.syncTime()
        params.lastUpdated = datatablesUtilService?.syncTime()
        params.companyDealer= session.userCompanyDealer
        def result = warningKaryawanService.save(params)

        if(!result.error) {
            flash.message = g.message(code: "default.created.message", args: ["WarningKaryawan", result.warningKaryawanInstance.id])
            redirect(action:'show', id: result.warningKaryawanInstance.id)
            return
        }

        render(view:'create', model:[warningKaryawanInstance: result.warningKaryawanInstance])
	}

	def show(Long id) {
		def result = warningKaryawanService.show(params)

		if(!result.error)
			return [ warningKaryawanInstance: result.warningKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def edit(Long id) {
        def result = warningKaryawanService.show(params)

		if(!result.error)
			return [ warningKaryawanInstance: result.warningKaryawanInstance ]

		flash.message = g.message(code: result.error.code, args: result.error.args)
		redirect(action: 'list')
	}

	def update(Long id, Long version) {
        params.lastUpdProcess = "UPDATE"
        params.lastUpdated = datatablesUtilService?.syncTime()
        def result = warningKaryawanService.update(params)
        if(!result.error) {
            flash.message = g.message(code: "default.updated.message", args: ["WarningKaryawan", params.id])
            redirect(action:'show', id: params.id)
            return
        }

        if(result.error.code == "default.not.found.message") {
            flash.message = g.message(code: result.error.code, args: result.error.args)
            redirect(action:'list')
            return
        }

        render(view:'edit', model:[warningKaryawanInstance: result.warningKaryawanInstance.attach()])
	}

	def delete() {
        // delete staDel
        params.staDel = "1"

		def result = warningKaryawanService.update(params)

        if(!result.error) {
            flash.message = g.message(code: "default.deleted.message", args: ["WarningKaryawan", params.id])
            redirect(action:'list')
            return
        }

        flash.message = g.message(code: result.error.code, args: result.error.args)

        if(result.error.code == "default.not.found.message") {
            redirect(action:'list')
            return
        }

        redirect(action:'show', id: params.id)
	}

	def massdelete() {
		def res = [:]
		try {
            if (params.ids && (params.type as Integer) == 1) {
                def ids = []
                def c = WarningKaryawan.createCriteria()
                def parsedIds = JSON.parse(params.ids)
                def results = c.list {
                    parsedIds.each {
                        c.notEqual("id", new Long(it))
                    }
                }

                results.each {
                    ids << String.valueOf(it.id)
                }

                params.ids = (ids as JSON).toString()
            }
			datatablesUtilService.massDeleteStaDel(WarningKaryawan, params)
			res.message = "Mass Delete Success"
		} catch (e) {
			log.error(e.message, e)
			res.message = e.message?:e.cause?.message
		}
		render "ok"
	}
	
	
}
