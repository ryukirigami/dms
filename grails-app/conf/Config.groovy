import org.grails.plugin.easygrid.Filter
import org.grails.plugin.easygrid.GridUtils
import org.grails.plugin.easygrid.grids.DataTablesGridService

import com.kombos.baseapp.CommonCodeDetail

// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.config.locations = [ "classpath:app-config.properties"]
grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [ html: ['text/html','application/xhtml+xml'],
                      xml: ['text/xml', 'application/xml'],
                      text: 'text/plain',
                      js: 'text/javascript',
                      rss: 'application/rss+xml',
                      atom: 'application/atom+xml',
                      css: 'text/css',
                      csv: 'text/csv',
                      all: '*/*',
                      json: ['application/json','text/json'],
                      form: 'application/x-www-form-urlencoded',
                      multipartForm: 'multipart/form-data',
                      excel: 'application/vnd.ms-excel',
                      ods: 'application/vnd.oasis.opendocument.spreadsheet',
                      pdf: 'application/pdf',
                      rtf: 'application/rtf'
                    ]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']


// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// enable query caching by default
grails.hibernate.cache.queries = true

appSettingParam.cache.size.kb = 100

// set per-environment serverURL stem for creating absolute links
environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.menu"
    }
}

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console
    // appender:
    //
    appenders {
        console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    }
	
    info   'grails.app'
	
	debug  'org.activiti'

    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
           'org.codehaus.groovy.grails.web.pages', //  GSP
           'org.codehaus.groovy.grails.web.sitemesh', //  layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping', // URL mapping
           'org.codehaus.groovy.grails.commons', // core / classloading
           'org.codehaus.groovy.grails.plugins', // plugins
           'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate',
		   'grails.app'		   
}

//activiti {
//    processEngineName = "activiti-engine-default"
//    databaseType = "h2"
//    deploymentName = appName
//    deploymentResources = ["file:./grails-app/conf/**/*.bpmn*.xml",
//            "file:./grails-app/conf/**/*.png",
//            "file:./src/taskforms/**/*.form"]
//    jobExecutorActivate = false
//    mailServerHost = "smtp.gmail.com"
//    mailServerPort = "465"
//    mailServerUsername = "pengirim.surel@gmail.com"
//    mailServerPassword = "kombos123"
//    mailServerDefaultFrom = "pengirim.surel@gmail.com"
//    history = "audit" // "none", "activity", "audit" or "full"
//    sessionUsernameKey = "username"
//    useFormKey = true
//}
//
//environments {
//    development {
//        activiti {
//            processEngineName = "activiti-engine-dev"
//            databaseSchemaUpdate = true // true, false or "create-drop"
//        }
//    }
//    test {
//        activiti {
//            processEngineName = "activiti-engine-test"
//            databaseSchemaUpdate = true
//            mailServerPort = "5025"
//        }
//    }
//    production {
//        activiti {
//            processEngineName = "activiti-engine-prod"
//            databaseSchemaUpdate = false
//            jobExecutorActivate = true
//        }
//    }
//}
// Added by Easygrid:


def stdDateFormat = 'MM/dd/yyyy'
easygrid {

	//default values added to each defined grid  ( if they are not already set )
	defaults {

		defaultMaxRows = 10 // the max no of rows displayed in the grid

		//used for automatically generating label messages from the column name
		//this will be transformed into a SimpleTemplateEngine instance ( '#' will be replaced with '$') and the binding variables will be: labelPrefix , column, gridConfig
		labelFormat = '#{labelPrefix}.#{column.name}.label'

		//called before inline editing : transforms the parameters into the actual object to be stored
		beforeSave = { params -> params }

		gridImpl = 'jqgrid' // the default grid implementation

		// these are properties used by the jqgrid template
		enableFilter = true
		addNavGrid = true

		fixedColumns = true
		noFixedColumns = 0

		//default export settings for various formats
		export {
			exportService = org.grails.plugin.easygrid.EasygridExportService

			//this section provides default values for the different export formats
			// for more options check out the export plugin

			// csv settings
			csv {
				separator = ','
				quoteCharacter = '"'
			}
			csv['header.enabled'] = true

			// excel settings
			excel['header.enabled'] = true
			//property that aggregates the widths defined per column
			excel['column.widths'] = { gridConfig ->
				def widths = []
				GridUtils.eachColumn(gridConfig, true) { column ->
					widths.add(column?.export?.width ?: 0.2)
				}
				widths
			}

			// pdf settings
			pdf['header.enabled'] = true
			pdf['column.widths'] = { gridConfig ->
				def widths = []
				GridUtils.eachColumn(gridConfig, true) { column ->
					widths.add(column?.export?.width ?: 0.2)
				}
				widths
			}
			pdf['border.color'] = java.awt.Color.BLACK
			pdf['pdf.orientation'] = 'landscape'

			// rtf settings
			rtf['header.enabled'] = true
			rtf {
			}

			// ods settings
			ods {
			}

			// xml settings
			xml['xml.root'] = { gridConfig ->
				//defaults to the export title
				gridConfig.export.export_title
			}
			xml {
			}
		}

		// jqgrid default properties
		// check the jqgrid documentation
		jqgrid {
			width = '"100%"'
			height = 250
			// number of rows to display by default
			rowNum = 20
		}

		dataTables {
			
		}

        dataTablesWithoutLinkId {

        }
		
		dataTablesNonEditable {
		
		}

        dataTablesWithChildren{

        }

        dataTablesGridWithChildrenShowId{

        }

        dataTablesGridShowId{

        }

        dataTablesShowChildrenForm{

        }

        dataTablesSetPeriodDate{

        }

        dataTablesParameterizedValue{

        }

		visualization {
			page = "'enable'"
			allowHtml = true
			alternatingRowStyle = true
//            showRowNumber = false
			pageSize = 10
		}

		// default security provider
		// spring security implementation
		// interprets the 'roles' property
		securityProvider = { grid, oper ->
			if (!grid.roles) {
				return true
			}
			def grantedRoles
			if (Map.isAssignableFrom(grid.roles.getClass())) {
				grantedRoles = grid.roles.findAll { op, role -> oper == op }.collect { op, role -> role }
			} else if (List.isAssignableFrom(grid.roles.getClass())) {
				grantedRoles = grid.roles
			} else {
				grantedRoles = [grid.roles]
			}
			SpringSecurityUtils.ifAllGranted(grantedRoles.inject('') { roles, role -> "${roles},${role}" })
		}

		//default autocomplete settings
		autocomplete {
			idProp = 'id'  // the name of the property of the id of the selected element (optionKey - in the replaced select tag)
			maxRows = 10 // the max no of elements to be displayed by the jquery autocomplete box
			template = '/templates/autocompleteRenderer' //the default autocomplete renderer
		}
	}

	// each grid has a "type" - which must be one of the datasources defined here
	dataSourceImplementations {
		//deprecated
		domain {
			// mandatory attribute: domainClass or initialCriteria
			dataSourceService = org.grails.plugin.easygrid.datasource.GormDatasourceService
			filters {
				//default search closures for different column types
				text = { Filter filter -> ilike(filter.column.name, "%${filter.paramValue}%") }
				number = { Filter filter -> eq(filter.column.name, filter.paramValue as int) }
				//todo
				date = { Filter filter -> eq(filter.column.name, filter.paramValue as Date) }

			}
		}

		// renamed for consistency - todo  -rename everywhere
		gorm {
			// mandatory attribute: domainClass or initialCriteria
			dataSourceService = org.grails.plugin.easygrid.datasource.GormDatasourceService
			filters {
				//default search closures
				text = { Filter filter -> ilike(filter.column.name, "%${filter.paramValue}%") }
				number = { Filter filter -> eq(filter.column.name, filter.paramValue as int) }
				//todo
				date = { Filter filter -> eq(filter.column.name, filter.paramValue as Date) }

			}
		}

		list {
			//mandatory attributes: context, attributeName
			dataSourceService = org.grails.plugin.easygrid.datasource.ListDatasourceService
			filters {
				//default search closures
				text = { Filter filter, row -> row[filter.column.name].contains filter.paramValue }
				number = { Filter filter, row -> row[filter.column.name] == filter.paramValue as int }
				//todo
				date = { Filter filter, row -> row[filter.column.name] == filter.paramValue as Date }

			}
		}

		custom {
			// mandatory attributes: 'dataProvider', 'dataCount'
			dataSourceService = org.grails.plugin.easygrid.datasource.CustomDatasourceService
		}
	}

	// these are the actual UI grid implementations
	// will be specified in the grid config using the 'gridImpl' property
	gridImplementations {

		//grails classic implementation - for demo purposes
		classic {
			gridRenderer = '/templates/easygrid/classicGridRenderer'
			gridImplService = org.grails.plugin.easygrid.grids.ClassicGridService
			inlineEdit = false
			formats = [
					(Date): { it.format(stdDateFormat) },
					(Boolean): { it ? "Yes" : "No" }
			]
		}

		//  jqgrid implementation
		jqgrid {
			gridRenderer = '/templates/easygrid/jqGridRenderer'          //  a gsp template that will be rendered
			gridImplService = org.grails.plugin.easygrid.grids.JqueryGridService  // the service class for this implementation
			inlineEdit = true    // specifies that this implementation allows inline Editing
			editRenderer = '/templates/easygrid/jqGridEditResponse'

			// there are 3 options to format the data
			// using the value closure in the column
			// using the named formatters ( defined below )
			// using the default type formats ( defined here ) - where you specify the type of data & the format closure
			formats = [
					(Date): { it.format(stdDateFormat) },
					(Calendar): { Calendar cal -> cal.format(stdDateFormat) },
					(Boolean): { it ? "Yes" : "No" }
			]
		}

		//  jquery datatables implementation
		dataTables {
			gridImplService = DataTablesGridService
			gridRenderer = '/templates/easygrid/dataTablesGridRenderer'
			inlineEdit = false
			formats = [
					(Date): { it.format(stdDateFormat) },
					(Boolean): { it ? "Yes" : "No" }
			]
		}
		
		dataTablesNonEditable {
			gridImplService = DataTablesGridService
			gridRenderer = '/templates/easygrid/dataTablesGridNonEditableRenderer'
			inlineEdit = false
			formats = [
					(Date): { it.format(stdDateFormat) },
					(Boolean): { it ? "Yes" : "No" }
			]
		}
		
		dataTablesLog {
			gridImplService = DataTablesGridService
			gridRenderer = '/templates/easygrid/dataTablesGridLogRenderer'
			inlineEdit = false
			formats = [
					(Date): { it.format(stdDateFormat) },
					(Boolean): { it ? "Yes" : "No" }
			]
		}

        dataTablesWithChildren{
            gridImplService=DataTablesGridService
            gridRenderer='/templates/easygrid/dataTablesWithChildren'
            inlineEdit=false
            formats=[
                    (Date): { it.format(stdDateFormat) },
                    (Boolean): { it ? "Yes" : "No" }
            ]
        }

        dataTablesGridWithChildrenShowId{
            gridImplService=DataTablesGridService
            gridRenderer='/templates/easygrid/dataTablesGridWithChildrenShowId'
            inlineEdit=false
            formats=[
                    (Date): { it.format(stdDateFormat) },
                    (Boolean): { it ? "Yes" : "No" }
            ]
        }


        dataTablesWithoutLinkId {
            gridImplService = DataTablesGridService
            gridRenderer = '/templates/easygrid/dataTablesGridRendererWithoutLinkId'
            inlineEdit = false
            formats = [
                    (Date): { it.format(stdDateFormat) },
                    (Boolean): { it ? "Yes" : "No" }
            ]
        }

        dataTablesGridShowId{
            gridImplService=DataTablesGridService
            gridRenderer='/templates/easygrid/dataTablesGridShowId'
            inlineEdit=false
            formats=[
                    (Date): { it.format(stdDateFormat) },
                    (Boolean): { it ? "Yes" : "No" }
            ]
        }

        dataTablesShowChildrenForm{
            gridImplService=DataTablesGridService
            gridRenderer='/templates/easygrid/dataTablesShowChildrenForm'
            inlineEdit=false
            formats=[
                    (Date): { it.format(stdDateFormat) },
                    (Boolean): { it ? "Yes" : "No" }
            ]
        }

        dataTablesSetPeriodDate{
            gridImplService=DataTablesGridService
            gridRenderer='/templates/easygrid/dataTablesSetPeriodDate'
            inlineEdit=false
            formats=[
                    (Date): { it.format(stdDateFormat) },
                    (Boolean): { it ? "Yes" : "No" }
            ]
        }

        dataTablesParameterizedValue{
            gridImplService=DataTablesGridService
            gridRenderer='/templates/easygrid/dataTablesParameterizedValue'
            inlineEdit=false
            formats=[
                    (Date): { it.format(stdDateFormat) },
                    (Boolean): { it ? "Yes" : "No" }
            ]
        }

		// google visualization implementation
		visualization {
			gridImplService = org.grails.plugin.easygrid.grids.VisualizationGridService
			gridRenderer = '/templates/easygrid/visualizationGridRenderer'
			inlineEdit = false
			formats = [
					(Date): { def cal = com.ibm.icu.util.Calendar.getInstance(); cal.setTime(it); cal.setTimeZone(com.ibm.icu.util.TimeZone.getTimeZone("GMT")); cal }, //wtf?
			]
		}

	}

	// section to define per column configurations
	columns {

		//default values for the columns
		defaults {
			enableFilter = true
			showInSelection = true
			sortable = true
			jqgrid {
				editable = true
			}
			classic {
				sortable = true
			}
			visualization {
				search = false
				searchType = 'text'
				valueType = com.google.visualization.datasource.datatable.value.ValueType.TEXT
			}
			dataTables {
				sWidth = "'100%'"
				sClass = "''"
			}
			export {
				width = 25
			}
		}

		// predefined column types  (set of configurations)
		// used to avoid code duplication
		types {
			id {
				property = 'id'
				enableFilter = false

				jqgrid {
					width = 40
					fixed = true
					search = false
					editable = false
//                formatter = 'editFormatter'
				}
				visualization {
					valueType = com.google.visualization.datasource.datatable.value.ValueType.NUMBER
				}
				export {
					width = 10
				}

			}

			actions {
				value = { '' }
				jqgrid {
					formatter = '"actions"'
					editable = false
					sortable = false
					resizable = false
					fixed = true
					width = 60
					search = false
					formatoptions = '{"keys":true}'
				}
				export {
					hidden = true
				}
			}

			version {
				property = 'version'
				jqgrid {
					hidden = true
				}
				export {
					hidden = true
				}
				visualization {
					valueType = com.google.visualization.datasource.datatable.value.ValueType.NUMBER
				}
			}
		}
	}

	// here we define different formatters
	// these are closures  which are called before the data is displayed to format the cell data
	// these are specified in the column section using : formatName
	formats {
		stdDateFormatter = {
			it.format(stdDateFormat)
		}
		visualizationDateFormatter = {
			def cal = com.ibm.icu.util.Calendar.getInstance(); cal.setTime(it); cal.setTimeZone(java.util.TimeZone.getTimeZone("GMT")); cal
		}
		stdBoolFormatter = {
			it ? "Yes" : "No"
		}

		nrToString = { nr ->
			if (nr / 1000000000 >= 1) {
				"${(nr / 1000000000) as int} billion"
			} else if (nr / 1000000 >= 1) {
				"${(nr / 1000000) as int} million"
			} else if (nr / 1000 >= 1) {
				"${(nr / 1000) as int}k"
			} else {
				"${nr}"
			}
		}

        bicodeToDesc={
            "${CommonCodeDetail.findByBicode(it)}"
        }

        linkFormat = {nr ->
            def quote = '"' + nr + '"'
            def call = "'alert(${quote});'"
            "<a href='#' onclick=${call}> Detail Message</a>";

        }
	}
}


activiti {
	processEngineName = "activiti-engine-default"
		  databaseType = "oracle"
		  databaseSchemaUpdate = false//"create-drop"
		  deploymentName = appName
		  deploymentResources = [
//			  					"file:./grails-app/conf/**/*.bpmn*.xml",p
//								 "file:./grails-app/conf/**/*.png",
//								 "file:./src/taskforms/**/*.form",
								 "classpath*:**/*.bpmn*.xml"
								 //"classpath*:**/*.png",
								 //"classpath*:**/*.form"
								 ]
		  jobExecutorActivate = false //true
		  mailServerHost = "mail.toyota-kombos.co.id"
		  mailServerPort = "465"
		  mailServerUsername = "cs@toyota-kombos.co.id"
		  mailServerPassword = "kombos12cs"
		  mailServerDefaultFrom = "cs@toyota-kombos.co.id"
		  history = "audit" // "none", "activity", "audit" or "full"
		  sessionUsernameKey = "activitiUsername"
		  useFormKey = true
}

securityConfig.userLookup.usernamePropertyName = 'username' //username property
securityConfig.userLookup.userDomainClassName = 'com.kombos.baseapp.sec.shiro.User' //domain classname without package
securityConfig.userLookup.authorityJoinClassName = 'UserRole'
securityConfig.userLookup.authority.className = 'Role' //domain classname without package

// set per-environment serverURL stem for creating absolute links
environments {
    development {
        grails.logging.jul.usebridge = true
        jasper.dir.reports = 'reports'
        activiti.databaseSchemaUpdate = true
        bootstrap.enable = false
//        activiti.databaseType = 'oracle'
//        activiti.databaseSchemaUpdate = false
    }
    test {
        grails.logging.jul.usebridge = false
        jasper.dir.reports = 'reports'
        activiti.databaseType = 'oracle'
        activiti.databaseSchemaUpdate = false
        bootstrap.enable = false
        // TODO: grails.serverURL = "http://www.changeme.menu"
    }
    production {
        grails.logging.jul.usebridge = false
        jasper.dir.reports = 'reports'
        activiti.databaseType = 'oracle'
        activiti.databaseSchemaUpdate = false
        bootstrap.enable = false
        // TODO: grails.serverURL = "http://www.changeme.menu"
    }
}




// Uncomment and edit the following lines to start using Grails encoding & escaping improvements

/* remove this line 
// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside null
                scriptlet = 'none' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }
}
remove this line */
