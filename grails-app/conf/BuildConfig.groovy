grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.server.port.http = "8881"
//grails.project.fork.run=true
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log 'warn' // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve

    repositories {
		inherits true // Whether to inherit repository definitions from plugins
        grailsPlugins()
        grailsHome()
        grailsCentral()
        mavenCentral()

        // uncomment these to enable remote dependency resolution from public Maven repositories
        //mavenCentral()
        //mavenLocal()
        //mavenRepo "http://snapshots.repository.codehaus.org"
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.menu/maven2/"
        mavenRepo "https://maven.alfresco.com/nexus/content/groups/public/"
        mavenRepo 'http://repository.codehaus.org/'
        mavenRepo 'https://repository.atlassian.com/maven2/'
        mavenRepo 'http://mirrors.ibiblio.org/maven2/'
        mavenRepo 'http://jasperreports.sourceforge.net/maven2'
        mavenRepo 'http://jaspersoft.artifactoryonline.com/jaspersoft/jr-ce-releases'
        mavenRepo 'http://smslib.org/maven2/v3/'
//        mavenRepo "http://repo.grails.org/grails/core"
//        mavenRepo "http://repo.grails.org/grails/plugins"

    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        // runtime 'mysql:mysql-connector-java:5.1.16'
		runtime 'vt-password:vt-password:3.1.1'
        runtime 'org.smslib:smslib:3.5.4'
//		runtime 'com.healthmarketscience.jackcess:jackcess:1.2.10'
        runtime 'net.sf.jasperreports:jasperreports-functions:5.5.0'
//		runtime ":commons-collections:3.2.1"
//		runtime ":commons-digester:2.1"
//		runtime ":commons-logging:1.1"
//		runtime ":jasperreports:5.6.0"
//		runtime ":jasperreports-chart-themes:5.6.0"
//		runtime ":jasperreports-core-renderer:5.6.0"
//		runtime ":jasperreports-extensions:3.5.3"
//		runtime ":jasperreports-fonts:5.6.0"
//		runtime ":jasperreports-functions:5.6.0"
//		runtime ":jasperreports-htmlcomponent:4.7.1"
//		runtime ":jasperreports-htmlcomponent:5.0.1"
//		runtime ":jasperreports-json:5.6.0"
//		runtime ":jasperreports-jtidy:r938"
//		runtime ":jcommon:1.0.15"
//		runtime ":jfreechart:1.0.12"
//		runtime ":joda-time:2.1"
    }

    plugins {
        runtime ":hibernate:$grailsVersion"
        //runtime ":jquery:1.7.1"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.2"
        //runtime ":jquery-ui:1.8.24"
        // Uncomment these (or add new ones) to enable additional resources capabilities
        //runtime ":zipped-resources:1.0"
        //runtime ":cached-resources:1.0"
        //runtime ":yui-minify-resources:0.1.4"

        build ":tomcat:$grailsVersion"

        compile ':cache:1.0.1'
        compile ':shiro:1.1.4', { 
				excludes "servlet-api" 
				}
        compile ":twitter-bootstrap:2.3.2"
        compile ":fixtures:1.2"
        compile ':easygrid:1.3.0',{
				exclude("jquery")
				}
        compile ":codenarc:0.21"
        compile ":activiti:5.9",{ 
				excludes 'spring-test'
				}  
        compile ":activiti-shiro:0.1.1"
        compile ":export:1.5"
        compile ":p6spy:0.5"
        compile ":excel-import:1.0.0"
//        compile ":csv:0.3.1" // plugin csv
        compile ":joda-time:1.4"
        compile ":jasper:1.7.0"

        //compile ':quartz:1.0'
        //compile ":quartz:1.0.1"
        //runtime ":yui-minify-resources:0.1.5"
    }
}

codenarc.reports = {

    // Each report definition is of the form:
    //    REPORT-NAME(REPORT-TYPE) {
    //        PROPERTY-NAME = PROPERTY-VALUE
    //        PROPERTY-NAME = PROPERTY-VALUE
    //    }

    MyXmlReport('xml') {                    // The report name "MyXmlReport" is user-defined; Report type is 'xml'
        outputFile = 'target/test-reports/CodeNarc-Report.xml'  // Set the 'outputFile' property of the (XML) Report
        title = 'Codenarc Result Report'             // Set the 'title' property of the (XML) Report
    }
    MyHtmlReport('html') {                  // Report type is 'html'
        outputFile = 'target/test-reports/CodeNarc-Report.html'
        title = 'Codenarc Result Report'
    }
}


