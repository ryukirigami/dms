dataSource {
    pooled = true
	//driverClassName = "oracle.jdbc.driver.OracleDriver"
	//driverClassName = "com.p6spy.engine.spy.P6SpyDriver" // use this driver to enable p6spy logging
	//driverClassName = "org.h2.Driver"
	//driverClassName = "com.p6spy.engine.spy.P6SpyDriver" // use this driver to enable p6spy logging
    //username = "sa" //"dmsuser" //"sa" //"sys as SYSDBA"
    //password = ""//"dmsuser123"//"DmsPasswd123"
	//dialect = org.hibernate.dialect.Oracle11gDialect
	//url = "jdbc:oracle:thin:@182.253.206.219:1521:dmsdb"
	//dialect = org.hibernate.dialect.H2Dialect
	//url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000;MODE=Oracle"
	//url = jdbc:h2:file:asdp;LOCK_TIMEOUT=60000;MVCC=true
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update"// one of 'create', 'create-drop', 'update', 'validate', ''
//            dbCreate = "none"// one of 'create', 'create-drop', 'update', 'validate', ''
//			driverClassName = "com.p6spy.engine.spy.P6SpyDriver" // use this driver to enable p6spy logging
//			dialect = org.hibernate.dialect.H2Dialect
//			url = "jdbc:h2:~/tmp/dms;MVCC=TRUE;AUTO_SERVER=TRUE;MODE=Oracle"
//			username = "sa" //"dmsuser" //"sa" //"sys as SYSDBA"
//			password = ""//"dmsuser123"//"DmsPasswd123"
//            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            pooled = true
            driverClassName = "oracle.jdbc.driver.OracleDriver"
            dialect = org.hibernate.dialect.Oracle11gDialect
            url = "jdbc:oracle:thin:@127.0.0.1:1521:XE"
//            url = "jdbc:oracle:thin:@182.253.206.218:1521:XE"
            username ="dms"
            password = "password"
        }
    }
    test {
        dataSource {
			dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
		    pooled = true
			driverClassName = "oracle.jdbc.driver.OracleDriver"
			dialect = org.hibernate.dialect.Oracle11gDialect
			url = "jdbc:oracle:thin:@127.0.0.1:1521:XE"
			username ="dms"
			password = "password"

        }
    }
    production {
        dataSource {
			dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
		    pooled = true
			driverClassName = "oracle.jdbc.driver.OracleDriver"
			dialect = org.hibernate.dialect.Oracle11gDialect
			url = "jdbc:oracle:thin:@13.76.198.94:1521:DMSDB"
//			url = "jdbc:oracle:thin:@127.0.0.1:1521:XE"
			username ="dmsnew"
			password = "password"

			properties {
			   initialSize = 10
			   maxActive = 100
			   minIdle = 10
			   maxIdle = 25
			   maxWait = 10000
			   //maxAge = 10 * 60000
			   timeBetweenEvictionRunsMillis = 5000
			   minEvictableIdleTimeMillis = 60000
			   validationQuery = "SELECT 1 FROM DUAL"
			   validationQueryTimeout = 3
			   //validationInterval = 15000
			   testOnBorrow = true
			   testWhileIdle = true
			   testOnReturn = false
			   //jdbcInterceptors = "ConnectionState;StatementCache(max=200)"
			   defaultTransactionIsolation = java.sql.Connection.TRANSACTION_READ_COMMITTED
			}

        }
    }
}
