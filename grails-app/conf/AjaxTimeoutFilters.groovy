import com.kombos.administrasi.CompanyDealer
import com.kombos.administrasi.GeneralParameter
import org.apache.shiro.SecurityUtils
// XXX activiti
//import org.grails.activiti.ActivitiConstants

import com.kombos.baseapp.UserActivity;
import com.kombos.baseapp.sec.shiro.User
import com.kombos.baseapp.utils.SecurityChecklistUtilService
import com.kombos.baseapp.menu.Menu;

import edu.vt.middleware.password.CharacterRule
import edu.vt.middleware.password.DigitCharacterRule
import edu.vt.middleware.password.LowercaseCharacterRule
import edu.vt.middleware.password.PasswordGenerator
import edu.vt.middleware.password.UppercaseCharacterRule


class AjaxTimeoutFilters {
	def approvalService
	//def activitiService
	def grailsApplication
	
    private static final String TIMEOUT_KEY = 'TIMEOUT_KEY'

    static uncheckActions = [
            [controller: 'Task', action: 'tasksCount'], [controller: 'Auth', action: 'checkSession']
    ]
	
	static genericControllerApproval = [
		[controller: 'FormulaManagement', action: 'save'], [controller: 'FormulaManagement', action: 'update'],[controller: 'FormulaManagement', action: 'delete'],
		[controller: 'Branch', action: 'save'], [controller: 'Branch', action: 'update'],[controller: 'Branch', action: 'delete'],
		[controller: 'GLBICodeMapping', action: 'save'], [controller: 'GLBICodeMapping', action: 'update'],[controller: 'GLBICodeMapping', action: 'delete'],
		[controller: 'ParameterizedValue', action: 'save'], [controller: 'ParameterizedValue', action: 'update'],[controller: 'ParameterizedValue', action: 'delete'],
		[controller: 'CommonCode', action: 'save'], [controller: 'CommonCode', action: 'update'],[controller: 'CommonCode', action: 'delete'],
		[controller: 'CommonCodeDetail', action: 'save'], [controller: 'CommonCodeDetail', action: 'update'],[controller: 'CommonCodeDetail', action: 'delete'],
		[controller: 'GroupLocalBranch', action: 'save'], [controller: 'GroupLocalBranch', action: 'update'],[controller: 'GroupLocalBranch', action: 'delete'],
		[controller: 'GroupLocalBranchDetail', action: 'update'],[controller: 'GroupLocalBranchDetail', action: 'delete'],
		[controller: 'GroupForm', action: 'save'], [controller: 'GroupForm', action: 'update'],[controller: 'GroupForm', action: 'delete'],
		[controller: 'GroupFormDetail', action: 'update'], [controller: 'GroupFormDetail', action: 'delete'],
		[controller: 'Role', action: 'save'], [controller: 'Role', action: 'update'], [controller: 'Role', action: 'delete'],
		[controller: 'user', action: 'update'],[controller: 'user', action: 'save'],[controller: 'user', action: 'delete'],
		[controller: 'RolePermissions', action: 'edit']
	]

    def filters = {
        all(controller:'*', action:'*') {
            before = {
				//log.info("before ajaxtimeout filter")
//                SecurityChecklistUtilService securityChecklistUtilService = new SecurityChecklistUtilService(SecurityChecklistUtilService.CODE_AUTOMATIC_LOG_OUT)
//
//                int sessionTimeout = securityChecklistUtilService.getAutomaticLogOut() * 60 * 1000

                CompanyDealer companyDealer = session.userCompanyDealer
                if(companyDealer){

                GeneralParameter generalParameterInstance = GeneralParameter.findByCompanyDealer(companyDealer)
                int h = generalParameterInstance ? generalParameterInstance?.m000WaktuSessionLog?.getHours() : 100;
                int m = generalParameterInstance ? generalParameterInstance?.m000WaktuSessionLog?.getMinutes() : 100;
                int sessionTimeout = ((h*3600) + (m*60)) * 1000

                def uncheck = uncheckActions.find {
                    (it.controller.toLowerCase() == controllerName.toLowerCase()) &&
                            ((it.action == '*') || (actionName && it.action.toLowerCase() == actionName.toLowerCase()))
                }
                
                if (uncheck) {
//                    println(actionName + " session timeout: " + sessionTimeout)
                    Long lastAccess = session[TIMEOUT_KEY]
                    if (lastAccess == null) {
  //                      println("lastAccess: null")
                        return false
                    }
      //              println("session time: " + (System.currentTimeMillis() - lastAccess))
                    if (System.currentTimeMillis() - lastAccess > sessionTimeout) {
    //                    println("invalidate")
                        session.invalidate()
                        // TODO - render response to trigger client redirect
                        return false
                    }
                }
                else {
                    //println("set session timeout key")
					//println("Session menu code: " + session.getAttribute("_menucode"))
					def menucode = params._menucode
					if(!menucode)
						menucode = session.getAttribute("_menucode")
					if(menucode  && SecurityUtils.subject.principal && actionName){
						Menu menu = Menu.findByMenuCode(menucode)
							
						User user = User.findByUsername(SecurityUtils.subject.principal)
						
						UserActivity ua = new UserActivity()
						ua.user = user
						ua.menu = menu
						ua.controller = controllerName
						ua.action = actionName
						ua.accessTime = new Date()
						
						ua.save(flush:true)
	
					}
//					def gca = false
//					
//					if(controllerName.toLowerCase() == 'groupformdetail'){
//						gca = genericControllerApproval.find {
//							(it.controller.toLowerCase() == controllerName.toLowerCase()) &&
//									((it.action == '*') || (actionName && it.action.toLowerCase() == actionName.toLowerCase()) &&  (approvalService.needApproval("groupForm", null))) 
//						}
//					} else if(controllerName.toLowerCase() == 'rolepermissions'){
//						gca = genericControllerApproval.find {
//							(it.controller.toLowerCase() == controllerName.toLowerCase()) &&
//									((it.action == '*') || (actionName && it.action.toLowerCase() == actionName.toLowerCase()) &&  (approvalService.needApproval("role", null))) 
//						}
//					} else if(controllerName.toLowerCase() == 'commoncodedetail'){
//						gca = genericControllerApproval.find {
//							(it.controller.toLowerCase() == controllerName.toLowerCase()) &&
//									((it.action == '*') || (actionName && it.action.toLowerCase() == actionName.toLowerCase()) &&  (approvalService.needApproval("CommonCode", null)))
//						}
//					} else if(controllerName.toLowerCase() == 'grouplocalbranchdetail'){
//						gca = genericControllerApproval.find {
//							(it.controller.toLowerCase() == controllerName.toLowerCase()) &&
//									((it.action == '*') || (actionName && it.action.toLowerCase() == actionName.toLowerCase()) &&  (approvalService.needApproval("groupLocalBranch", null)))
//						}
//					} else {
//						gca = genericControllerApproval.find {
//							(it.controller.toLowerCase() == controllerName.toLowerCase()) &&
//									((it.action == '*') || (actionName && it.action.toLowerCase() == actionName.toLowerCase()) &&  (approvalService.needApproval(it.controller, null)||approvalService.needApproval(controllerName, null)))
//						}
//					}
//					
//					if(gca) {
//						//String sessionUsernameKey = grailsApplication.config.activiti.sessionUsernameKey?:ActivitiConstants.DEFAULT_SESSION_USERNAME_KEY
//						//println("Harusnya di terusin ke approval generic")
//						//redirect(controller: 'GenericApproval', action:'requestApproval')
//						//params[sessionUsernameKey] = session[sessionUsernameKey]
//						params.originController = controllerName
//						params.originAction = actionName
//						params.controller = 'GenericApproval'
//						if(controllerName.toLowerCase() == 'user' && actionName.toLowerCase() == 'save'){
//							if(!params.passwordHash){
//								String passwordHash = this.generateNewPassword()
//								params.passwordHash = passwordHash
//							}
//						}
//						//log.info("RequestApproval params: " + params)
//						def module = controllerName.toUpperCase() + '_' + actionName.toUpperCase()
//						def approval = approvalService.createNewApproval(module,params)
//						params.approvalId = approval.id
//// XXX Activiti						
////						log.info ("activitiService startProcess")
////						ProcessInstance pi = activitiService.startProcess(params)
////						log.info ("activitiService ok")
//						
//						if(controllerName.toLowerCase() == 'user' && actionName.toLowerCase() == 'save'){
//							render("Your request will be submitted to request for approval, New user password: " + params.passwordHash)
//						} else {
//							render("Your request will be submitted to request for approval")
//						}
//						return false
//						
//					} else {
//						if(params.id && (actionName.toLowerCase() == 'edit' || actionName.toLowerCase() == 'delete')){
//							println("Params ID: " + params.id)
//							def pending = approvalService.pendingApproval(controllerName.toUpperCase() + '_', params.id.toString())
//							if(pending > 0){
//								render("This object has a pending approval")
//								return false
//							}
//						}
//					}
					
					
                    session[TIMEOUT_KEY] = System.currentTimeMillis()
                }


                true
                }
            }
			after = {
				//log.info("after ajaxtimeout filter")
			}
        }
    }
	
	def generateNewPassword(){
		// create a password generator
		PasswordGenerator generator = new PasswordGenerator()

		// create character rules to generate passwords with
		List<CharacterRule> rules = new ArrayList<CharacterRule>()
		rules.add(new DigitCharacterRule(1));
		rules.add(new UppercaseCharacterRule(1));
		rules.add(new LowercaseCharacterRule(1));

		return generator.generatePassword(8, rules)
	}
}
