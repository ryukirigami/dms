import org.apache.commons.dbcp.BasicDataSource
import org.apache.shiro.authc.credential.HashedCredentialsMatcher
import org.apache.shiro.crypto.hash.Sha512Hash

import com.kombos.utils.StringEncrypter

import org.springframework.jdbc.core.JdbcTemplate

// Place your Spring DSL code here
beans = {
//  jdbcTemplate(JdbcTemplate) {
//     dataSource = ref('dataSource')
//  }
  
//    credentialMatcher(Sha512CredentialsMatcher) {
//        storedCredentialsHexEncoded = true
//    }
	credentialMatcher(HashedCredentialsMatcher) {
//		storedCredentialsHexEncoded = false
//		hashSalted=true
//		hashIterations=1024
		hashAlgorithmName=Sha512Hash.ALGORITHM_NAME
	}

//    uncomment this to use default or fixed locale (will disable dynamic locale)
//    localeResolver (FixedLocaleResolver, new Locale("en", "US"))

//    uncomment this to set default (initial) locale
//    localeResolver(SessionLocaleResolver) {
//        Locale.setDefault(new Locale("id","ID"))
//    }
	
	
	
//	def ds = grailsApplication.config.dataSource
//	def eds = grailsApplication.config.encryptedConnectionString
//	
//	def dsurl = ds.url
//	def dsusername = ds.username
//	def dspassword = ds.password
//	
//	if(eds) {
//		StringEncrypter enc = StringEncrypter.getInstance();
//		enc.decrypt(eds).tokenize("|").each {
//			if(it.indexOf("url=")>=0) {
//				dsurl = it.substring(4)
//				ds.url = dsurl
//			} else if(it.indexOf("username=")>=0) {
//				dsusername = it.substring(9)
//			} else if(it.indexOf("password=")>=0) {
//				dspassword = it.substring(9)				
//			}
//		}	
//	}
//	
//	dataSource(BasicDataSource) {
//		driverClassName = ds.driverClassName
//		url = dsurl
//		password = dspassword
//		username = dsusername
//	}
	
}
