modules = {
    core {
        dependsOn 'bootstrap-css', 'bootstrap-js', 'jquery'//, 'modalBootstrap_patch'
        resource url:[dir: 'css', file: 'theme-switcher.css']
        resource url:[dir: 'css', file: 'go-to-top.css']
        resource url:[dir: 'css', file: 'font-awesome.min.css']//, disposition: 'head', attrs:[media:'screen, projection']
        resource url:[dir: 'css', file: 'font-awesome-ie7.min.css'], disposition: 'head', wrapper: { s -> "<!--[if lt IE 8]>$s<![endif]-->" }
        resource url:[dir: 'js', file: 'jqBootstrapValidation.js']//, disposition: 'head', attrs:[media:'screen, projection']
        resource url:[dir: 'js', file: 'json2.js']
        resource url:[dir: 'js', file: 'application.js']
        resource url:[dir: 'js', file: 'idle-timer.min.js']
        resource url:[dir: 'js', file: 'jquery-ui-1.10.4.custom.js']


    }

    modalBootstrap {
        resource url:[dir: 'js', file: 'bootstrap-modal.js']
        resource url:[dir: 'js', file: 'bootstrap-modalmanager.js']
    }
	
	modalBootstrap_patch {
		resource url:[dir: 'bootstrap-modal', file: 'bootstrap-modal.css']
        resource url:[dir: 'bootstrap-modal', file: 'bootstrap-modal.js']
        resource url:[dir: 'bootstrap-modal', file: 'bootstrap-modalmanager.js']
    }
	
    highslide {
        resource url:[dir: 'highslide', file: 'highslide-full.js']
        resource url:[dir: 'highslide', file: 'highslide.config.js']
        resource url:[dir: 'highslide', file: 'highslide.css']
        resource url:[dir: 'highslide', file: 'highslide-ie6.css'], disposition: 'head', wrapper: { s -> "<!--[if lt IE 7]>$s<![endif]-->" }
    }
    datatables {
        dependsOn 'jquery'
		resource url:[dir: 'datatables/js', file: 'jquery.dataTables.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'datatables/js', file: 'dataTables.bootstrap.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'datatables/js', file: 'jquery.dataTables.columnFilter.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'datatables/js', file: 'dataTables.fnReloadAjax.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'datatables/js', file: 'FixedColumns.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'datatables/js', file: 'bootbox.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'datatables/css', file: 'dt.bootstrap.css']//, disposition: 'head', attrs:[media:'screen, projection']
	 }
    baseapptheme {
		defaultBundle 'theme'
        resource url:[ dir: 'css/themes', file:'adminflare.css']//, attrs:[id:'baseapp-theme']
    }

    baseapplayout {
        resource url:[dir: 'js', file: 'baseapp-layout.js']
    }

    dataManagerPlugin{
        resource url:[dir:'js',file:'jquery.dataTables.editable.js']
    }

	baseapplist {
        resource url:[dir: 'js', file: 'baseapp-list.js']
    }
	
	baseappform {
		resource url:[dir: 'js', file: 'baseapp-form.js']
	}

    baseapplayoutcss {
        dependsOn 'baseapptheme'
		resource url:[ dir: 'css', file:'baseapp.css']
    }

    bootstrapeditable {
        resource url:[dir: 'bootstrap-editable/js', file: 'bootstrap-editable.js']//, disposition: 'head', attrs:[media:'screen, projection']
        resource url:[dir: 'bootstrap-editable/css', file: 'bootstrap-editable.css']//, disposition: 'head', attrs:[media:'screen, projection']
        resource url:[dir: 'css', file: 'custombootstrapeditable.css']//, disposition: 'head'
    }
	
//	bootstrapdatetimepicker {
//		resource url:[dir: 'datetimepicker/js', file: 'bootstrap-datetimepicker.min.js'], disposition: 'head', attrs:[media:'screen, projection']
//		resource url:[dir: 'datetimepicker/css', file: 'datetimepicker.css'], disposition: 'head', attrs:[media:'screen, projection']
//	}
	
	bootstrapdatepicker {
		resource url:[dir: 'datepicker/js', file: 'bootstrap-datepicker.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'datepicker/css', file: 'datepicker.css']//, disposition: 'head', attrs:[media:'screen, projection']
	}
	
	toastr {
		resource url:[dir: 'js', file: 'toastr.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'css', file: 'toastr.css']//, disposition: 'head', attrs:[media:'screen, projection']
		//resource url:[dir: 'css', file: 'ajaxfileupload.css'], disposition: 'head', attrs:[media:'screen, projection']
	}

    baseapp {
        dependsOn 'datatables', 'core', 'baseapplayout', 'bootstrapdatepicker', 'toastr', 'bootstrapselect', 'baseapplayoutcss'
        resource url:[dir: 'js', file: 'bootstrap-spinedit.js']//, disposition: 'head', attrs:[media:'screen, projection']
        resource url:[dir: 'css', file: 'bootstrap-spinedit.css']//, disposition: 'head', attrs:[media:'screen, projection']
    }
	
	bootstrapcheckboxtree {
		resource url:[dir: 'bootstrap-checkbox-tree/js', file: 'bootstrap-checkbox-tree.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'bootstrap-checkbox-tree/css', file: 'bootstrap-checkbox-tree.css']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'bootstrap-checkbox-tree/css', file: 'bootstrap-checkbox-tree-ie7.css'], disposition: 'head', attrs:[media:'screen, projection'], wrapper: { s -> "<!--[if lt IE 8]>$s<![endif]-->" }
	}	
	
	ajaxfileupload {
		resource url:[dir: 'js', file: 'ajaxfileupload.js']//, disposition: 'head', attrs:[media:'screen, projection']
		//resource url:[dir: 'css', file: 'ajaxfileupload.css'], disposition: 'head', attrs:[media:'screen, projection']
	}
	jquerynumeric {
		resource url:[dir: 'js', file: 'jquery.numeric.js']//, attrs:[media:'screen, projection']
	}
	jqueryvalidate {
		resource url:[dir: 'js', file: 'jquery.validate.min.js']//, attrs:[media:'screen, projection']
	}
    autoComplete {
        resource url:[dir: 'js', file: 'typeahead.js']//, attrs:[media:'screen, projection']
    }
    autoNumeric {
        resource url:[dir: 'js', file: 'autoNumeric.js']//, attrs:[media:'screen, projection']
    }

	jquerynumber {
		resource url:[dir: 'js', file: 'jquery.number.min.js']//, attrs:[media:'screen, projection']
	}
		
	bootstrapselect {
		resource url:[dir: 'bootstrap-select', file: 'bootstrap-select.min.js']//, disposition: 'head', attrs:[media:'screen, projection']
		resource url:[dir: 'bootstrap-select', file: 'bootstrap-select.min.css']//, disposition: 'head', attrs:[media:'screen, projection']
	}
	
	path {
		resource url:[dir: 'js', file: 'path.min.js']//, attrs:[media:'screen, projection']
	}
	
	jqueryhashchange {
		resource url:[dir: 'js', file: 'jquery.ba-hashchange.min.js']//, attrs:[media:'screen, projection']
	}
	
	base64 {
		resource url:[dir: 'js', file: 'base64.js']//, attrs:[media:'screen, projection']
	}
	login {
		defaultBundle 'login'
		resource url:[ dir: 'css', file:'login.css']//, attrs:[id:'style']
	}
	
	basement {
		
	}

    calendario{
        resource url:[dir: 'css', file: 'calendar.css']
        resource url:[dir: 'css', file: 'custom_2.css']
        resource url:[dir: 'js', file: 'jquery.calendario.js']
    }



    bootstraptooltip{
        resource url:[dir: 'js', file: 'bootstrap-tooltip.js']
    }

    jQueryContextMenu{
        resource url:[dir: 'js', file: 'jquery.ui.position.js']
        resource url:[dir: 'js', file: 'jquery.contextMenu.js']
        resource url:[dir: 'js', file: 'jquery.contextMenu.css']
    }

}