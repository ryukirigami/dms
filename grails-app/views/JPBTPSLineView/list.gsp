
<%@ page import="com.kombos.board.JPB" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="JPB" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<r:require modules="baseapplayout, baseapplist" />
		<g:javascript>
			var show;
			var edit;
			$(function(){ 

                $('#search_tglView').datepicker().on('changeDate', function(ev) {
               			var newDate = new Date(ev.date);
               			$('#search_tglView_day').val(newDate.getDate());
               			$('#search_tglView_month').val(newDate.getMonth()+1);
               			$('#search_tglView_year').val(newDate.getFullYear());
               			$(this).datepicker('hide');
               //			var oTable = $('#JPB_datatables').dataTable();
               //            oTable.fnReloadAjax();
               	});


});
</g:javascript>
	</head>
	<body>
    <div class="navbar box-header no-border">
   		<span class="pull-left">Job Progress Board (JPB) Body & Paint TPS Line</span>
   	</div>

	<div class="box">

        <table style="width: 100%;border: 0px;padding-left: 10px;padding-right: 10px;">
            <tr>
            <td style="width: 43%;vertical-align: top;">

                <div class="box">
                    <legend style="font-size: small">View JPB GR</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Tgl/Jam Janji Datang
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiDatang"/></td>
                            <td>
                                <div id="lbl_rcvDate" class="controls">
                                    <ba:datePicker id="input_rcvDate" name="input_rcvDate" precision="day" format="dd-MM-yyyy"  value=""  />
                                    <select id="input_rcvHour" name="input_rcvHour" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select> :
                                    <select id="input_rcvMinuite" name="input_rcvMinuite" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Status Mobil</td>
                            <td colspan="2">
                                <div class="controls">
                                    <g:radioGroup name="statusMobil" values="['W','L']" value="${JPBInstance?.t351StaOkCancelReSchedule}" labels="['Ditunggu','Ditinggal']">
                                        ${it.radio} <g:message code="${it.label}" />
                                    </g:radioGroup>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_penyerahan">
                                    Tgl/Jam Janji Penyerahan
                                </label>
                            </td>
                            <td><g:checkBox name="checkJanjiPenyerahan"/></td>
                            <td>
                                <div id="lbl_penyerahan" class="controls">
                                    <ba:datePicker id="input_penyerahan" name="input_penyerahan" precision="day" format="dd-MM-yyyy"  value=""  />
                                    <select id="input_penyerahanHour" name="input_penyerahanHour" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<24;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select> :
                                    <select id="input_penyerahanMinuite" name="input_penyerahanMinuite" style="width: 60px" required="">
                                        %{
                                            for (int i=0;i<60;i++){
                                                if(i<10){
                                                        out.println('<option value="'+i+'">0'+i+'</option>');
                                                } else {
                                                        out.println('<option value="'+i+'">'+i+'</option>');
                                                }
                                            }
                                        }%
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><button class="btn btn-primary" id="buttonSave" onclick="reloadReminderTable();">Search</button></td>
                        </tr>
                    </table>
                </div>

            </td>
            <td style="width: 23%;vertical-align: top;padding-left: 10px;">

            </td>

            <td style="width: 33%;vertical-align: top;padding-left: 10px;">
                <div class="box">
                    <legend style="font-size: small">View JPB</legend>
                    <table style="width: 100%;border: 0px">
                        <tr>
                            <td>
                                <label class="control-label" for="lbl_rcvDate">
                                    Nama Workshop
                                </label>
                            </td>
                            <td>
                                <div id="lbl_companyDealer" class="controls">
                                    %{--<g:select id="input_companyDealer" name="companyDealer.id" from="${com.kombos.administrasi.CompanyDealer.list()}" optionKey="id" required="" class="many-to-one"/>--}%
                                    <g:if test="${session?.userCompanyDealer?.m011NamaWorkshop?.toUpperCase()?.contains("HO")}">
                                        <g:select name="companyDealer.id" id="input_companyDealer" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:if>
                                    <g:else>
                                        <g:select name="companyDealer.id" id="input_companyDealer" readonly="" disabled="" from="${com.kombos.administrasi.CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop");}}" optionKey="id" optionValue="m011NamaWorkshop" value="${session?.userCompanyDealer?.id}" />
                                    </g:else>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label class="control-label" for="lbl_tglView">
                                    Tanggal
                                </label>
                            </td>
                            <td>
                                <div id="lbl_tglView" class="controls">
                                    <div id="filter_tglView" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                                         <input type="hidden" name="search_tglView" value="date.struct">
                                         <input type="hidden" name="search_tglView_day" id="search_tglView_day" value="">
                                         <input type="hidden" name="search_tglView_month" id="search_tglView_month" value="">
                                         <input type="hidden" name="search_tglView_year" id="search_tglView_year" value="">
                                         <input type="text" data-date-format="dd/mm/yyyy" name="search_tglView_dp" value="" id="search_tglView" class="search_init">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button class="btn btn-primary" id="buttonView" onclick="jpbView()">View</button>
                            </td>
                        </tr>

                    </table>
                </div>

            </td>

            </tr>
            <tr>
                <td colspan="3">
                    <div id="JPB-table">
                        <g:if test="${flash.message}">
                            <div class="message" role="status">
                                ${flash.message}
                            </div>
                        </g:if>

                        <g:render template="dataTables" />
                    </div>
                    <div id="day3">
                        <g:render template="dataTablesNextDay" />
                    </div>
                    </td>
                </tr>
        </table>

	</div>
</body>
</html>
