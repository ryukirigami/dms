
<%@ page import="com.kombos.customerprofile.HistoryCustomer" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main">
    <title><g:message code="uploadDataSales.label" default="Upload Data Sales"/></title>
    <r:require modules="baseapplayout" />
    <r:require modules="highslide" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js"></script>

    <g:javascript>
        <g:if test="${jmlhDataError && jmlhDataError>0}">
            $('#save').attr("disabled", true);
        </g:if>

        var saveForm;
        $(function(){

            function progress(e){
                if(e.lengthComputable){
                    //kalo mau pake progress bar
                    //$('progress').attr({value:e.loaded,max:e.total});
                }
            }

            saveForm = function() {
                var jum=0;
                var vinCode = []
                var company = []
                var gelarDepan = []
                var namaDepan = []
                var namaBelakang = []
                var gelarBelakang = []
                var jenisKelamin = []
                var tgglLahir = []
                var noNpwp = []
                var noPajak = []
                // var jenisIDCard = []
                // var nomorID = []
                // var agama = []
                // var staNikah = []
                var noTelp = []
                var noKantor = []
                var noFax = []
                var noHP = []
                var email = []
                var alamat = []
                // var rt = []
                // var rw = []
                // var kelurahan = []
                // var kecamatan = []
                // var kabupaten = []
                // var provinsi = []
                // var kodePos = []
                var alamatNpwp = []
                // var rtNpwp = []
                // var rwNpwp = []
                // var kelurahanNpwp = []
                // var kecamatanNpwp = []
                // var kabupatenNpwp = []
                // var provinsiNpwp = []
                // var kodePosNpwp = []
                var namaStnk = []
                var alamatStnk = []
                var tanggalStnk = []
                var nopolDepan = []
                var nopolTengah = []
                var nopolBelakang = []
                var kodeDealerPenjual = []
                var kodeWarna = []
                var fullModel = []
                var tanggalDEC = []
                var tahunRakit = []
                var bulanRakit = []
                var noKunci = []
                // var hobby = []
                var ind = 0
                $("#uploadDataSales_datatables tbody .row-select").each(function() {
                    if(this.checked){
                        jum++;
                        vinCode.push(document.getElementById("id"+ind).value)
                        company.push(document.getElementById("company"+ind).value)
                        gelarDepan.push(document.getElementById("gelarDepan"+ind).value)
                        namaDepan.push(document.getElementById("namaDepan"+ind).value)
                        namaBelakang.push(document.getElementById("namaBelakang"+ind).value)
                        gelarBelakang.push(document.getElementById("gelarBelakang"+ind).value)
                        jenisKelamin.push(document.getElementById("jenisKelamin"+ind).value)
                        tgglLahir.push(document.getElementById("tgglLahir"+ind).value)
                        noNpwp.push(document.getElementById("noNpwp"+ind).value)
                        noPajak.push(document.getElementById("noPajak"+ind).value)
                        // jenisIDCard.push(document.getElementById("jenisIDCard"+ind).value)
                        // nomorID.push(document.getElementById("nomorID"+ind).value)
                        // agama.push(document.getElementById("agama"+ind).value)
                        // staNikah.push(document.getElementById("staNikah"+ind).value)
                        noTelp.push(document.getElementById("noTelp"+ind).value)
                        noKantor.push(document.getElementById("noKantor"+ind).value)
                        noFax.push(document.getElementById("noFax"+ind).value)
                        noHP.push(document.getElementById("noHP"+ind).value)
                        email.push(document.getElementById("email"+ind).value)
                        alamat.push(document.getElementById("alamat"+ind).value)
                        // rt.push(document.getElementById("rt"+ind).value)
                        // rw.push(document.getElementById("rw"+ind).value)
                        // kelurahan.push(document.getElementById("kelurahan"+ind).value)
                        // kecamatan.push(document.getElementById("kecamatan"+ind).value)
                        // kabupaten.push(document.getElementById("kabupaten"+ind).value)
                        // provinsi.push(document.getElementById("provinsi"+ind).value)
                        // kodePos.push(document.getElementById("kodePos"+ind).value)
                        alamatNpwp.push(document.getElementById("alamatNpwp"+ind).value)
                        // rtNpwp.push(document.getElementById("rtNpwp"+ind).value)
                        // rwNpwp.push(document.getElementById("rwNpwp"+ind).value)
                        // kelurahanNpwp.push(document.getElementById("kelurahanNpwp"+ind).value)
                        // kecamatanNpwp.push(document.getElementById("kecamatanNpwp"+ind).value)
                        // kabupatenNpwp.push(document.getElementById("kabupatenNpwp"+ind).value)
                        // provinsiNpwp.push(document.getElementById("provinsiNpwp"+ind).value)
                        // kodePosNpwp.push(document.getElementById("kodePosNpwp"+ind).value)
                        namaStnk.push(document.getElementById("namaStnk"+ind).value)
                        alamatStnk.push(document.getElementById("alamatStnk"+ind).value)
                        tanggalStnk.push(document.getElementById("tanggalStnk"+ind).value)
                        nopolDepan.push(document.getElementById("nopolDepan"+ind).value)
                        nopolTengah.push(document.getElementById("nopolTengah"+ind).value)
                        nopolBelakang.push(document.getElementById("nopolBelakang"+ind).value)
                        kodeDealerPenjual.push(document.getElementById("kodeDealerPenjual"+ind).value)
                        kodeWarna.push(document.getElementById("kodeWarna"+ind).value)
                        fullModel.push(document.getElementById("fullModel"+ind).value)
                        tanggalDEC.push(document.getElementById("tanggalDEC"+ind).value)
                        tahunRakit.push(document.getElementById("tahunRakit"+ind).value)
                        bulanRakit.push(document.getElementById("bulanRakit"+ind).value)
                        noKunci.push(document.getElementById("noKunci"+ind).value)
                        // hobby.push(document.getElementById("hobby"+ind).value)
                    }
                    ind++
                });
                if(jum==0){
                    alert('Pilih data yang akan di simpan');
                    return
                }
                var cek = confirm('Anda yakin data akan disimpan?')
                if(cek){
                    var data = {};
                    data["vinCode"] = JSON.stringify(vinCode);
                    data["company"] = JSON.stringify(company);
                    data["gelarDepan"] = JSON.stringify(gelarDepan);
                    data["namaDepan"] = JSON.stringify(namaDepan);
                    data["namaBelakang"] = JSON.stringify(namaBelakang);
                    data["gelarBelakang"] = JSON.stringify(gelarBelakang);
                    data["jenisKelamin"] = JSON.stringify(jenisKelamin);
                    data["tgglLahir"] = JSON.stringify(tgglLahir);
                    data["noNpwp"] = JSON.stringify(noNpwp);
                    data["noPajak"] = JSON.stringify(noPajak);
                    // data["jenisIDCard"] = JSON.stringify(jenisIDCard);
                    // data["nomorID"] = JSON.stringify(nomorID);
                    // data["agama"] = JSON.stringify(agama);
                    // data["staNikah"] = JSON.stringify(staNikah);
                    data["noTelp"] = JSON.stringify(noTelp);
                    data["noKantor"] = JSON.stringify(noKantor);
                    data["noFax"] = JSON.stringify(noFax);
                    data["noHP"] = JSON.stringify(noHP);
                    data["email"] = JSON.stringify(email);
                    data["alamat"] = JSON.stringify(alamat);
                    // data["rt"] = JSON.stringify(rt);
                    // data["rw"] = JSON.stringify(rw);
                    // data["kelurahan"] = JSON.stringify(kelurahan);
                    // data["kecamatan"] = JSON.stringify(kecamatan);
                    // data["kabupaten"] = JSON.stringify(kabupaten);
                    // data["provinsi"] = JSON.stringify(provinsi);
                    // data["kodePos"] = JSON.stringify(kodePos);
                    data["alamatNpwp"] = JSON.stringify(alamatNpwp);
                    // data["rtNpwp"] = JSON.stringify(rtNpwp);
                    // data["rwNpwp"] = JSON.stringify(rwNpwp);
                    // data["kelurahanNpwp"] = JSON.stringify(kelurahanNpwp);
                    // data["kecamatanNpwp"] = JSON.stringify(kecamatanNpwp);
                    // data["kabupatenNpwp"] = JSON.stringify(kabupatenNpwp);
                    // data["provinsiNpwp"] = JSON.stringify(provinsiNpwp);
                    // data["kodePosNpwp"] = JSON.stringify(kodePosNpwp);
                    data["namaStnk"] = JSON.stringify(namaStnk);
                    data["alamatStnk"] = JSON.stringify(alamatStnk);
                    data["tanggalStnk"] = JSON.stringify(tanggalStnk);
                    data["nopolDepan"] = JSON.stringify(nopolDepan);
                    data["nopolTengah"] = JSON.stringify(nopolTengah);
                    data["nopolBelakang"] = JSON.stringify(nopolBelakang);
                    data["kodeDealerPenjual"] = JSON.stringify(kodeDealerPenjual);
                    data["kodeWarna"] = JSON.stringify(kodeWarna);
                    data["fullModel"] = JSON.stringify(fullModel);
                    data["tanggalDEC"] = JSON.stringify(tanggalDEC);
                    data["tahunRakit"] = JSON.stringify(tahunRakit);
                    data["bulanRakit"] = JSON.stringify(bulanRakit);
                    data["noKunci"] = JSON.stringify(noKunci);
                    // data["hobby"] = JSON.stringify(hobby);
                    $.ajax({
                        url:'${request.getContextPath()}/uploadDataSales/upload',
                        type: "POST",
                        data : data,
                        success : function(data){
                            $('#salesTable').empty();
                            $('#salesTable').append(data);
                            toastr.success("Sukses Upload");
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            alert('Internal Server Error');
                        }
                    });
                }
            }


});

function selectAll(){
if($('input[name=checkSelect]').is(':checked')){
    $(".row-select").attr("checked",true);
}else {
    $(".row-select").attr("checked",false);
}
}

$('#uploadDataSales_datatables tbody tr').on('click', function () {
var jum=0;
$("#uploadDataSales_datatables tbody .row-select").each(function() {
    if(this.checked){
        jum++;
    }
});
var rowCount = $('#uploadDataSales_datatables tbody tr').length;
if($(this).find('.row-select').is(":checked")){
    if(rowCount == jum){
        $('.select-all').attr('checked', true);
    }
} else {
    $('.select-all').attr('checked', false);
}
});

    </g:javascript>

</head>
<body>
<div id="salesTable">
    <div class="navbar box-header no-border">
        <span class="pull-left">
            <g:message code="uploadDataSales.label" default="Upload Data Sales"/>
        </span>
        <ul class="nav pull-right">
            <li></li>
            <li></li>
            <li class="separator"></li>
        </ul>
    </div>
    <div class="box">
        <div class="span12" id="historyCustomer-table">
            <fieldset>
                <form id="uploadDataSales-save" class="form-vertical" action="${request.getContextPath()}/uploadDataSales/save" method="post" onsubmit="submitForm();return false;">
                    <table>
                        <tr>
                            <td>
                                <fieldset class="form">
                                    <g:render template="form"/>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fieldset class="buttons controls">
                                    <g:submitButton class="btn btn-primary create" name="view" id="view" value="${message(code: 'default.button.view.label', default: 'Upload')}" />
                                    <g:field type="button" onclick="saveForm();" class="btn btn-primary create" name="save" id="save" value="${message(code: 'default.button.upload.label', default: 'Simpan')}"/>
                                    &nbsp;&nbsp;&nbsp;
                                    <g:if test="${flash.message}">
                                        ${flash.message}
                                    </g:if>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </form>
                <g:javascript>
                        var submitForm;
                        $(function(){

                            function progress(e){
                                if(e.lengthComputable){
                                    //kalo mau pake progress bar
                                    //$('progress').attr({value:e.loaded,max:e.total});
                                }
                            }

                            submitForm = function() {

                                var form = new FormData($('#uploadDataSales-save')[0]);

                                $.ajax({
                                    url:'${request.getContextPath()}/uploadDataSales/view',
                                    type: 'POST',
                                    xhr: function() {
                                        var myXhr = $.ajaxSettings.xhr();
                                        if(myXhr.upload){
                                            myXhr.upload.addEventListener('progress',progress, false);
                                        }
                                        return myXhr;
                                    },
                                    //add beforesend handler to validate or something
                                    //beforeSend: functionname,
                                    success: function (res) {
                                        $('#salesTable').empty();
                                        $('#salesTable').append(res);
                                        //reloadHistoryCustomerTable();
                                    },
                                    //add error handler for when a error occurs if you want!
                                    error: function (data, status, e){
                                        alert(e);
                                    },
                                    complete: function(xhr, status) {
                                    },
                                    data: form,
                                    cache: false,
                                    contentType: false,
                                    processData: false
                                });

                            }
                        });
                </g:javascript>
            </fieldset>
            <br>
            <div style="max-width: 100%; overflow: auto">
                <table id="uploadDataSales_datatables" cellpadding="0" cellspacing="0"
                       border="0"
                       class="display table table-striped table-bordered table-hover table-selectable"
                       width="100%">
                    <thead>
                    <tr>
                        <th>&nbsp;&nbsp;
                            <input type="checkbox" class="select-all" name="checkSelect" id="checkSelect" onclick="selectAll();"/>&nbsp;&nbsp;
                        VINCode
                        </th>
                        <th>
                            Kode Company
                        </th>
                        <th>
                            Gelar Depan
                        </th>
                        <th>
                            Nama Depan
                        </th>
                        <th>
                            Nama Belakang
                        </th>
                        <th>
                            Gelar Belakang
                        </th>
                        <th>
                            Jenis Kelamin
                        </th>
                        <th>
                            Tanggal Lahir
                        </th>
                        <th>
                            No. NPWP
                        </th>
                        <th>
                            No. Faktur Pajak
                        </th>
                        %{--<th>--}%
                            %{--Kode Jenis ID Card--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Nomor ID Card--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Agama--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Status Pernikahan--}%
                        %{--</th>--}%
                        <th>
                            No. Telp. Rumah
                        </th>
                        <th>
                            No. Telp. Kantor
                        </th>
                        <th>
                            No. Fax
                        </th>
                        <th>
                            No. HP
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Alamat
                        </th>
                        %{--<th>--}%
                            %{--RT--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--RW--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Kelurahan--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Kecamatan--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Kabupaten--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Provinsi--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Pos--}%
                        %{--</th>--}%
                        <th>
                            Alamat NPWP
                        </th>
                        %{--<th>--}%
                            %{--RT NPWP--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--RW NPWP--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Kelurahan NPWP--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Kecamatan NPWP--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Kabupaten NPWP--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Provinsi NPWP--}%
                        %{--</th>--}%
                        %{--<th>--}%
                            %{--Kode Pos NPWP--}%
                        %{--</th>--}%
                        <th>
                            Nama STNK
                        </th>
                        <th>
                            Alamat STNK
                        </th>
                        <th>
                            Tanggal STNK
                        </th>
                        <th>
                            Nopol Depan
                        </th>
                        <th>
                            Nopol Tengah
                        </th>
                        <th>
                            Nopol Belakang
                        </th>
                        <th>
                            Kode Dealer Penjual
                        </th>
                        <th>
                            Kode Warna
                        </th>
                        <th>
                            FullModel
                        </th>
                        <th>
                            Tanggal DEC
                        </th>
                        <th>
                            Tahun Rakit
                        </th>
                        <th>
                            Bulan Rakit
                        </th>
                        <th>
                            Nomor Kunci
                        </th>
                        %{--<th>--}%
                            %{--Kode Hobby--}%
                        %{--</th>--}%
                    </tr>
                    </thead>
                    <tbody>
                    <g:if test="${htmlData}">
                        ${htmlData}
                    </g:if>
                    <g:else>
                        <tr class="odd">
                            <td class="dataTables_empty" valign="top" colspan="49">No data available in table</td>
                        </tr>
                    </g:else>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
