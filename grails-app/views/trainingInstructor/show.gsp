

<%@ page import="com.kombos.hrd.TrainingInstructor" %>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<r:require modules="bootstrapeditable" />
<g:set var="entityName"
	value="${message(code: 'trainingInstructor.label', default: 'TrainingInstructor')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
<g:javascript disposition="head">
var deleteTrainingInstructor;

$(function(){ 
	deleteTrainingInstructor=function(id){
		$.ajax({type:'POST', url:'${request.contextPath}/trainingInstructor/delete/',
			data: { id: id },
   			success:function(data,textStatus){
   				reloadTrainingInstructorTable();
   				expandTableLayout('trainingInstructor');
   			},
   			error:function(XMLHttpRequest,textStatus,errorThrown){},
   			complete:function(XMLHttpRequest,textStatus){
   				$('#spinner').fadeOut();
   			}
   		});
	}
});

</g:javascript>
</head>
<body>
	<div id="show-trainingInstructor" role="main">
		<legend>
			<g:message code="default.show.label" args="[entityName]" />
		</legend>
		<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
		</g:if>
		<table id="trainingInstructor"
			class="table table-bordered table-hover">
			<tbody>

				
				<g:if test="${trainingInstructorInstance?.createdBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="createdBy-label" class="property-label"><g:message
					code="trainingInstructor.createdBy.label" default="Created By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="createdBy-label">
						%{--<ba:editableValue
								bean="${trainingInstructorInstance}" field="createdBy"
								url="${request.contextPath}/TrainingInstructor/updatefield" type="text"
								title="Enter createdBy" onsuccess="reloadTrainingInstructorTable();" />--}%
							
								<g:fieldValue bean="${trainingInstructorInstance}" field="createdBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingInstructorInstance?.dateCreated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="dateCreated-label" class="property-label"><g:message
					code="trainingInstructor.dateCreated.label" default="Date Created" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="dateCreated-label">
						%{--<ba:editableValue
								bean="${trainingInstructorInstance}" field="dateCreated"
								url="${request.contextPath}/TrainingInstructor/updatefield" type="text"
								title="Enter dateCreated" onsuccess="reloadTrainingInstructorTable();" />--}%
							
								<g:formatDate date="${trainingInstructorInstance?.dateCreated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingInstructorInstance?.karyawan}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="karyawan-label" class="property-label"><g:message
					code="trainingInstructor.karyawan.label" default="Karyawan" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="karyawan-label">
						%{--<ba:editableValue
								bean="${trainingInstructorInstance}" field="karyawan"
								url="${request.contextPath}/TrainingInstructor/updatefield" type="text"
								title="Enter karyawan" onsuccess="reloadTrainingInstructorTable();" />--}%
							
								<g:link controller="karyawan" action="show" id="${trainingInstructorInstance?.karyawan?.id}">${trainingInstructorInstance?.karyawan?.encodeAsHTML()}</g:link>
							
						</span></td>
					
				</tr>
				</g:if>
			
%{--KETEMU--}%
            <g:if test="${trainingInstructorInstance?.namaInstruktur}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="namaInstruktur-label" class="property-label"><g:message
                                code="trainingInstructor.namaInstruktur.label" default="Nama Instruktur" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="namaInstruktur-label">

                        <g:fieldValue bean="${trainingInstructorInstance}" field="namaInstruktur"/>

                    </span></td>

                </tr>
            </g:if>


            <g:if test="${trainingInstructorInstance?.jenisInstruktur}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="jenisInstruktur-label" class="property-label"><g:message
                                code="trainingInstructor.jenisInstruktur.label" default="Jenis Instruktur" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="jenisInstruktur-label">

                        <g:fieldValue bean="${trainingInstructorInstance}" field="jenisInstruktur"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${trainingInstructorInstance?.manPower}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="manPower-label" class="property-label"><g:message
                                code="trainingInstructor.manPower.label" default="Jabatan" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="manPower-label">

                        <g:fieldValue bean="${trainingInstructorInstance?.manPower}" field="m014JabatanManPower"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${trainingInstructorInstance?.certificationType}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="certificationType-label" class="property-label"><g:message
                                code="trainingInstructor.certificationType.label" default="Sertifikat Terakhir" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="certificationType-label">

                        <g:fieldValue bean="${trainingInstructorInstance?.certificationType}" field="namaSertifikasi"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${trainingInstructorInstance?.trainingClassRoom}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="trainingClassRoom-label" class="property-label"><g:message
                                code="trainingInstructor.trainingClassRoom.label" default="Kelas Training" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="trainingClassRoom-label">

                        <g:fieldValue bean="${trainingInstructorInstance?.trainingClassRoom}" field="namaTraining"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${trainingInstructorInstance?.companyDealer}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="companyDealer-label" class="property-label"><g:message
                                code="trainingInstructor.companyDealer.label" default="Company Dealer" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="companyDealer-label">

                        <g:fieldValue bean="${trainingInstructorInstance?.companyDealer}" field="m011NamaWorkshop"/>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${trainingInstructorInstance?.th020isTam}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="th020isTam-label" class="property-label"><g:message
                                code="trainingInstructor.th020isTam.label" default="isTam" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="th020isTam-label">
                        %{--<ba:editableValue
                                bean="${stallInstance}" field="m022StaTPSLine"
                                url="${request.contextPath}/Stall/updatefield" type="text"
                                title="Enter m022StaTPSLine" onsuccess="reloadStallTable();" />--}%

                        <g:if test="${trainingInstructorInstance?.th020isTam == '1'}">
                            <g:message code="trainingInstructor.th020isTam-label.tidak.label" default="Tidak" />
                        </g:if>
                        <g:else>
                            <g:message code="trainingInstructor.th020isTam-label.ya.label" default="Ya" />
                        </g:else>

                    </span></td>

                </tr>
            </g:if>

            <g:if test="${trainingInstructorInstance?.th020isAktif}">
                <tr>
                    <td class="span2" style="text-align: right;"><span
                            id="th020isAktif-label" class="property-label"><g:message
                                code="trainingInstructor.th020isAktif.label" default="isAktif" />:</span></td>

                    <td class="span3"><span class="property-value"
                                            aria-labelledby="th020isAktif-label">
                        <g:if test="${trainingInstructorInstance?.th020isAktif == '1'}">
                            <g:message code="trainingInstructor.th020isAktif-label.tidak.label" default="Tidak Aktif" />
                        </g:if>
                        <g:else>
                            <g:message code="trainingInstructor.th020isAktif-label.ya.label" default="Aktif" />
                        </g:else>

                    </span></td>

                </tr>
            </g:if>
            %{--KETEMU--}%

				<g:if test="${trainingInstructorInstance?.lastUpdProcess}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdProcess-label" class="property-label"><g:message
					code="trainingInstructor.lastUpdProcess.label" default="Last Upd Process" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdProcess-label">
						%{--<ba:editableValue
								bean="${trainingInstructorInstance}" field="lastUpdProcess"
								url="${request.contextPath}/TrainingInstructor/updatefield" type="text"
								title="Enter lastUpdProcess" onsuccess="reloadTrainingInstructorTable();" />--}%
							
								<g:fieldValue bean="${trainingInstructorInstance}" field="lastUpdProcess"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingInstructorInstance?.lastUpdated}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="lastUpdated-label" class="property-label"><g:message
					code="trainingInstructor.lastUpdated.label" default="Last Updated" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="lastUpdated-label">
						%{--<ba:editableValue
								bean="${trainingInstructorInstance}" field="lastUpdated"
								url="${request.contextPath}/TrainingInstructor/updatefield" type="text"
								title="Enter lastUpdated" onsuccess="reloadTrainingInstructorTable();" />--}%
							
								<g:formatDate date="${trainingInstructorInstance?.lastUpdated}" />
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingInstructorInstance?.staDel}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="staDel-label" class="property-label"><g:message
					code="trainingInstructor.staDel.label" default="Sta Del" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="staDel-label">
						%{--<ba:editableValue
								bean="${trainingInstructorInstance}" field="staDel"
								url="${request.contextPath}/TrainingInstructor/updatefield" type="text"
								title="Enter staDel" onsuccess="reloadTrainingInstructorTable();" />--}%
							
								<g:fieldValue bean="${trainingInstructorInstance}" field="staDel"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
				<g:if test="${trainingInstructorInstance?.updatedBy}">
				<tr>
				<td class="span2" style="text-align: right;"><span
					id="updatedBy-label" class="property-label"><g:message
					code="trainingInstructor.updatedBy.label" default="Updated By" />:</span></td>
					
						<td class="span3"><span class="property-value"
						aria-labelledby="updatedBy-label">
						%{--<ba:editableValue
								bean="${trainingInstructorInstance}" field="updatedBy"
								url="${request.contextPath}/TrainingInstructor/updatefield" type="text"
								title="Enter updatedBy" onsuccess="reloadTrainingInstructorTable();" />--}%
							
								<g:fieldValue bean="${trainingInstructorInstance}" field="updatedBy"/>
							
						</span></td>
					
				</tr>
				</g:if>
			
			</tbody>
		</table>
		<g:form class="form-horizontal">
			<fieldset class="buttons controls">
				<a class="btn cancel" href="javascript:void(0);"
					onclick="expandTableLayout('trainingInstructor');"><g:message
						code="default.button.cancel.label" default="Cancel" /></a>		
				<g:remoteLink class="btn btn-primary edit" action="edit"
					id="${trainingInstructorInstance?.id}"
					update="[success:'trainingInstructor-form',failure:'trainingInstructor-form']"
					on404="alert('not found');">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:remoteLink>
				<ba:confirm id="delete" class="btn cancel"
					message="${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}"
					onsuccess="deleteTrainingInstructor('${trainingInstructorInstance?.id}')" label="${message(code: 'default.button.delete.label', default: 'Delete')}"/>
			</fieldset>
		</g:form>
	</div>
</body>
</html>
