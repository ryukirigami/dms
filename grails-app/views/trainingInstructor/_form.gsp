<%@ page import="com.kombos.administrasi.CompanyDealer; com.kombos.hrd.TrainingClassRoom; com.kombos.administrasi.ManPower; com.kombos.hrd.TrainingInstructor" %>

<g:javascript>
    $(function() {
        $("#namaInstruktur").focus();
        $("#btnCariKaryawan").click(function() {
            oFormService.fnShowKaryawanDataTable();
        })
    })
</g:javascript>

<div class="control-group fieldcontain ${hasErrors(bean: trainingInstructorInstance, field: 'namaInstruktur', 'error')} ">
    <label class="control-label" for="instructorType">
        <g:message code="trainingInstructor.instructorType.label" default="Nama Instruktur"/>

    </label>

    <div class="controls">
        <g:textField name="namaInstruktur" value="${trainingInstructorInstance?.namaInstruktur}"/>
        <span class="help-block">
            <g:fieldError field="namaInstruktur" message="" bean="${trainingInstructorInstance}"/>
        </span>
    </div>
</div>


<div class="control-group fieldcontain ${hasErrors(bean: trainingInstructorInstance, field: 'karyawan', 'error')} ">
    <label class="control-label" for="karyawanMask">
        <g:message code="trainingInstructor.karyawan.label" default="Karyawan"/>
    </label>

    <div class="controls">
        <div class="input-append">
            <g:textField name="karyawan.name" id="karyawan_nama" value="${trainingInstructorInstance?.karyawan?.nama}"/>
            <span class="add-on">
                <a href="javascript:void(0);" id="btnCariKaryawan">
                    <i class="icon-search"/>
                </a>
            </span>
        </div>
        <span class="help-block">
            <g:fieldError field="karyawan" bean="${trainingInstructorInstance}"/>
        </span>
        <g:hiddenField name="karyawan.id" id="karyawan_id" value="${trainingInstructorInstance?.karyawan?.nama}"/>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: TrainingInstructor, field: 'jenisInstruktur', 'error')} required">
    <label class="control-label" for="jenisInstruktur">
        <g:message code="trainingInstructor.jenisInstruktur.label" default="Jenis Instruktur" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="jenisInstruktur" name="jenisInstruktur" noSelection="['':'Pilih Jenis Insruktur']" from="${["Adku","ICSK","IT","Mekanik BP","Mekanik GR","MRA","SA","TL"]}" required="" value="${trainingInstructorInstance?.jenisInstruktur}" class="many-to-one" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: TrainingInstructor, field: 'm014JabatanManPower', 'error')} required">
    <label class="control-label" for="jabatan">
        <g:message code="trainingInstructor.jabatan.label" default="Jabatan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="manPower" name="manPower.id" noSelection="['':'Pilih Jabatan']" from="${ManPower.createCriteria().list{eq("staDel","0");order("m014JabatanManPower")}}" optionKey="id" required="" optionValue="m014JabatanManPower"  value="${trainingInstructorInstance?.manPower?.id}" class="many-to-one" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: TrainingInstructor, field: 'namaSertifikasi', 'error')} required">
    <label class="control-label" for="sertifikatterakhir">
        <g:message code="trainingInstructor.jabatan.label" default="Sertifikat Terakhir" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="certificationType" name="certificationType.id" noSelection="['':'Pilih Sertifikat Terakhir']" from="${com.kombos.hrd.CertificationType.createCriteria().list {eq("staDel","0");order("keterangan")}}" optionKey="id" required="" optionValue="keterangan"  value="${trainingInstructorInstance?.certificationType?.id}" class="many-to-one" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: TrainingInstructor, field: 'companydealer', 'error')} required">
    <label class="control-label" for="companydealer">
        <g:message code="trainingInstructor.jabatan.label" default="Company Dealer" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:select id="companyDealer" name="companyDealer.id" noSelection="['':'Pilih Company Dealer']" from="${CompanyDealer.createCriteria().list {eq("staDel","0");order("m011NamaWorkshop")}}" optionKey="id" required="" optionValue="m011NamaWorkshop"  value="${trainingInstructorInstance?.companyDealer?.id}" class="many-to-one" />
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: TrainingInstructor, field: 'th020isTam', 'error')} required">
    <label class="control-label" for="th020isTam">
        <g:message code="trainingInstructor.th020isTam.label" default="TAM/Bukan" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:radioGroup name="th020isTam" values="['1','0']" labels="['Ya','Tidak']" value="${trainingInstructorInstance?.th020isTam}" required="">
            ${it.radio} ${it.label}
        </g:radioGroup>
    </div>
</div>

<div class="control-group fieldcontain ${hasErrors(bean: TrainingInstructor, field: 'th020isAktif', 'error')} required">
    <label class="control-label" for="th020isTam">
        <g:message code="trainingInstructor.th020isAktif.label" default="Aktif/Tidak" />
        <span class="required-indicator">*</span>
    </label>
    <div class="controls">
        <g:radioGroup name="th020isAktif" values="['1','0']" labels="['Ya','Tidak']" value="${trainingInstructorInstance?.th020isAktif}" required="">
            ${it.radio} ${it.label}
        </g:radioGroup>
    </div>
</div>


<!-- Karyawan Modal -->

<div id="karyawanModal" class="modal fade">
    <div class="modal-dialog" style="width: 1200px;">
        <div class="modal-content" style="width: 1200px;">
            <div class="modal-header">
                <a class="close" href="javascript:void(0);" data-dismiss="modal">×</a>
                <h4>Data Karyawan - List</h4>
            </div>
            <!-- dialog body -->
            <div class="modal-body" id="karyawanModal-body" style="max-height: 1200px;">
                <div class="box">
                    <div class="span12">
                        <fieldset>
                            <table>
                                <tr style="display:table-row;">
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <label class="control-label" for="sCriteria_nama">Nama Karyawan</label>
                                    </td>
                                    <td style="width: 130px; display:table-cell; padding:5px;">
                                        <input type="text" id="sCriteria_nama">
                                    </td>
                                    <td >
                                        <a id="btnSearchKaryawan" class="btn btn-primary" href="javascript:void(0);">
                                            Cari
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </div>
                </div>
                <g:render template="dataTablesKaryawan" />
            </div>

             <div class="modal-footer">
                 <a href="javascript:void(0);" class="btn cancel" onclick="oFormService.fnHideKaryawanDataTable();">
                     Tutup
                 </a>
                 <a href="javascript:void(0);" id="btnPilihKaryawan" class="btn btn-primary">
                     Pilih Karyawan
                 </a>
             </div>
        </div>
    </div>
</div>
