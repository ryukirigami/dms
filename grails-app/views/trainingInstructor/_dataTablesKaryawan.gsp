<%@ page import="com.kombos.hrd.Karyawan" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="karyawan_datatables" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover"
       width="100%">

</table>

<g:javascript>
var karyawanTable;
var reloadKaryawanTable;
$(function(){

    $("#btnPilihKaryawan").click(function() {
        $("#karyawan_datatables tbody").find("tr").each(function() {
            var radio = $(this).find("td:eq(0) input[type='radio']");
            if (radio.is(":checked")) {
                var id = radio.data("id");
                var nama = radio.data("nama");
                var npk = radio.data("npk");
                $("#karyawanMask").val(npk);
                $("#karyawan_id").val(id);
                $("#karyawan_nama").val(nama);
                $("#namaInstruktur").val(nama);
                $("#namaInstruktur").prop("readonly",true);
                $("#karyawan_nama").prop("readonly",true);
                $.ajax({
                    url:'${request.contextPath}/trainingInstructor/cekKaryawan',
                    type: "POST", // Always use POST when deleting data
                    data: { id: id },
                    success: function(data,item){
                            if(data.companyDealer!="notok"){
                                $("#companyDealer").val(data.companyDealer);
                            }
                            if(data.jabatan!="notok"){
                                $("#manPower").val(data.jabatan);
                            }
                            if(data.sertifikasi!="notok"){
                                $("#certificationType").val(data.sertifikasi);
                            }

                    },
                    complete: function(xhr, status) {
                    }
                });
                oFormService.fnHideKaryawanDataTable();

            }
        })
        reloadTrainingInstructorTable();
    });


	reloadKaryawanTable = function() {
		karyawanTable.fnDraw();
		}

    $("#btnSearchKaryawan").click(function() {
	    reloadKaryawanTable();
    });

	$('#search_tanggalLahir').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalLahir_day').val(newDate.getDate());
			$('#search_tanggalLahir_month').val(newDate.getMonth()+1);
			$('#search_tanggalLahir_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();
	});



	$('#search_tanggalMasuk').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_tanggalMasuk_day').val(newDate.getDate());
			$('#search_tanggalMasuk_month').val(newDate.getMonth()+1);
			$('#search_tanggalMasuk_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			karyawanTable.fnDraw();
	});

	karyawanTable = $('#karyawan_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : 4, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList", controller: "karyawan")}",
		"aoColumns": [

{
    "sTitle": "NPK",
	"sName": "nomorPokokKaryawan",
	"mDataProp": "nomorPokokKaryawan",
	"aTargets": [0],
	"mRender": function ( data, type, row ) {
		return "<input type='radio' data-id='"+row["id"]+"' data-nama='"+row["nama"]+"' data-npk='"+row["nomorPokokKaryawan"]+"' name='radio' class='pull-left row-select' style='position: relative; top: 3px;' aria-label='Row " + row["id"] + "' title='Select this'><input type='hidden' value='"+row["id"]+"'>&nbsp;&nbsp;"+data;
	},
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
	"sTitle": "Nama Karyawan",
	"sName": "nama",
	"mDataProp": "nama",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sTitle": "Tanggal Lahir",
	"sName": "tanggalLahir",
	"mDataProp": "tanggalLahir",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sTitle": "Alamat",
	"sName": "alamat",
	"mDataProp": "alamat",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

,

{
    "sTitle": "Pekerjaan / Departemen",
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

,

{
    "sTitle": "Status Karyawan",
	"sName": "statusKaryawan",
	"mDataProp": "statusKaryawan",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {

                        var namaKaryawan = $("#sCriteria_nama").val();

                        if (namaKaryawan) {
                            aoData.push({"name": "sCriteria_nama", "value": namaKaryawan});
                        }

                        %{--var alamat = $("#alamat").val();

                        if (alamat) {
                            aoData.push({"name": "sCriteria_alamat", "value": alamat});
                        }

                        var tanggalLahir = $("#tanggalLahir").val();

                        if (tanggalLahir) {
                            aoData.push({"name": "sCriteria_tanggalLahir", "value": tanggalLahir});
                        }

                        var statusKaryawan = $("#statusKaryawan").val();

                        if (statusKaryawan) {
                            aoData.push({"name": "sCriteria_statusKaryawan", "value": statusKaryawan});
                        }

                        var tanggalMasukFrom = $('#tanggalMasukFrom').val();
						var tanggalMasukFromDay = $('#tanggalMasukFrom_day').val();
						var tanggalMasukFromMonth = $('#tanggalMasukFrom_month').val();
						var tanggalMasukFromYear = $('#tanggalMasukFrom_year').val();

						if(tanggalMasukFrom){
							aoData.push(
									{"name": 'sCriteria_tanggalMasukFrom', "value": "date.struct"},
									{"name": 'sCriteria_tanggalMasukFrom_dp', "value": tanggalMasukFrom},
									{"name": 'sCriteria_tanggalMasukFrom_day', "value": tanggalMasukFromDay},
									{"name": 'sCriteria_tanggalMasukFrom_month', "value": tanggalMasukFromMonth},
									{"name": 'sCriteria_tanggalMasukFrom_year', "value": tanggalMasukFromYear}
							);
						}

                        var tanggalMasukTo = $('#tanggalMasukTo').val();
						var tanggalMasukToDay = $('#tanggalMasukTo_day').val();
						var tanggalMasukToMonth = $('#tanggalMasukTo_month').val();
						var tanggalMasukToYear = $('#tanggalMasukTo_year').val();

						if(tanggalMasukFrom){
							aoData.push(
									{"name": 'sCriteria_tanggalMasukTo', "value": "date.struct"},
									{"name": 'sCriteria_tanggalMasukTo_dp', "value": tanggalMasukTo},
									{"name": 'sCriteria_tanggalMasukTo_day', "value": tanggalMasukToDay},
									{"name": 'sCriteria_tanggalMasukTo_month', "value": tanggalMasukToMonth},
									{"name": 'sCriteria_tanggalMasukTo_year', "value": tanggalMasukToYear}
							);
						}

                        var available = $("input[type='radio']:checked").val();

                        if (available) {
                            aoData.push({name: "sCriteria_staAvailable", "value": available});
                        }--}%

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});

});
</g:javascript>



