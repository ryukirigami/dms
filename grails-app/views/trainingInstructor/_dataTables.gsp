
<%@ page import="com.kombos.hrd.TrainingInstructor" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="trainingInstructor_datatables" cellpadding="0" cellspacing="0"
	border="0"
	class="display table table-striped table-bordered table-hover"
	width="100%">
	<thead>
		<tr>

            <th style="border-bottom: none;padding: 5px;">
                <div>Nama Instruktur</div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="ID Karyawan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="Jenis Instruktur" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="Jabatan" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="Sertifikat Terakhir" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="Kelas Training Terakhir" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="Company Dealer" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="isTam" /></div>
            </th>

            <th style="border-bottom: none;padding: 5px;">
                <div><g:message code="trainingInstructor.karyawan.label" default="isAktif" /></div>
            </th>


        </tr>
		<tr>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_namaInstruktur" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_namaInstruktur" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_karyawan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_karyawan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_jenisinstruktur" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_jenisinstruktur" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_jabatan" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_jabatan" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_tipesertifikasi" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_tipesertifikasi" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_kelastraining" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_kelastraining" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_companydealer" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_companydealer" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_isTam" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_isTam" class="search_init" />
                </div>
            </th>

            <th style="border-top: none;padding: 5px;">
                <div id="filter_isAktif" style="padding-top: 0px;position:relative; margin-top: 0px;width: 135px;">
                    <input type="text" name="search_isAktif" class="search_init" />
                </div>
            </th>

        </tr>
	</thead>
</table>

<g:javascript>
var trainingInstructorTable;
var reloadTrainingInstructorTable;
$(function(){
	
	reloadTrainingInstructorTable = function() {
		trainingInstructorTable.fnDraw();
	}

	

$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) { 
			e.stopPropagation();
		 	trainingInstructorTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});

	trainingInstructorTable = $('#trainingInstructor_datatables').dataTable({
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [

{
	"sName": "namaInstruktur",
	"mDataProp": "namaInstruktur",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"200px",
	"bVisible": true,
	"mRender": function ( data, type, row ) {
		return '<input type="checkbox" class="pull-left row-select" aria-label="Row '+row['id']+'" title="Select this"><input type="hidden" value="'+row['id']+'">&nbsp;&nbsp;<a href="#" onclick="show('+row['id']+');">'+data+'</a><a class="pull-right cell-action" href="javascript:void(0);" onclick="edit('+row['id']+');">&nbsp;&nbsp;<i class="icon-pencil"></i>&nbsp;&nbsp;</a>';
	}
},

{
	"sName": "karyawan",
	"mDataProp": "karyawan",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},

{
	"sName": "jenisinstruktur",
	"mDataProp": "jenisinstruktur",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},

{
	"sName": "jabatan",
	"mDataProp": "jabatan",
	"aTargets": [4],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},

{
	"sName": "sertifikatterakhir",
	"mDataProp": "sertifikatterakhir",
	"aTargets": [5],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},

{
	"sName": "kelastraining",
	"mDataProp": "kelastraining",
	"aTargets": [6],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},

{
	"sName": "companydealer",
	"mDataProp": "companydealer",
	"aTargets": [7],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},

{
	"sName": "isTam",
	"mDataProp": "isTam",
	"aTargets": [8],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
},

{
	"sName": "isAktif",
	"mDataProp": "isAktif",
	"aTargets": [9],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"300px",
	"bVisible": true
}

],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
	
						var karyawan = $('#filter_karyawan input').val();
						if(karyawan){
							aoData.push(
									{"name": 'sCriteria_karyawan', "value": karyawan}
							);
						}

						var namaInstruktur = $("#filter_namaInstruktur input").val();
						if (namaInstruktur) {
						    aoData.push({
						        "name": "sCriteria_namaInstruktur",
						        "value": namaInstruktur
						    });
						}

						var jenisInstruktur = $("#filter_jenisinstruktur input").val();
						if(jenisInstruktur){
						    aoData.push({
                                "name" : "sCriteria_jenisInstruktur",
                                "value": jenisInstruktur
						    });
						}

						var jabatan = $("#filter_jabatan input").val();
                        if(jabatan){
                            aoData.push({
                                "name" : "sCriteria_jabatan",
                                "value" : jabatan
                            });
                        }

                        var tipeSertifikasi = $("#filter_tipesertifikasi").val();
                        if(tipeSertifikasi){
                            aoData.push({
                            "name" : "sCriteria_tipeSertifikasi",
                            "value" : tipeSertifikasi
                            });
                        }

                        var  kelasTraining = $("#filter_kelastraining").val();
                        if(kelasTraining){
                            aoData.push({
                                "name" : "sCriteria_kelastraining",
                                "value" : kelasTraining
                            });
                        }

                        var companydealer = $("#filter_companydealer").val();
                        if(companydealer){
                            aoData.push({
                                "name" : "sCriteria_companydealer",
                                "value" : companydealer
                            });
                        }

                        var isTam = $("#filter_isTam").val();
                        if(isTam){
                            aoData.push({
                                "name" : "sCriteria_isTam",
                                "value" : isTam
                            });
                        }

                        var isAktif = $("filter_isAktif").val();
                        if(isAktif){
                            aoData.push({
                                "name" : "sCriteria_isAktif",
                                "value" : isAktif
                            })
                        }

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}			
	});
});
</g:javascript>


			
