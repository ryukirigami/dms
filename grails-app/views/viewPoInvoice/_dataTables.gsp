
<%@ page import="com.kombos.reception.POSublet" %>

<r:require modules="baseapplayout" />
<g:render template="../menu/maxLineDisplay"/>

<table id="viewPoInvoice_datatables_${idTable}" cellpadding="0" cellspacing="0"
       border="0"
       class="display table table-striped table-bordered table-hover" style="table-layout: fixed; ">
    <thead>
    <tr>
        <th></th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>Nama Vendor</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>Nomor PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>Tanggal PO</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;">
            <div>Perihal</div>
        </th>
        <th style="border-bottom: none; padding: 5px; width: 200px;"/>
        <th style="border-bottom: none; padding: 5px; width: 200px;"/>
    </tr>
    </thead>
</table>

<g:javascript>
var viewPoInvoiceTable;
var reloadViewPoInvoiceTable;

function searchViewPoInvoice(){
    reloadViewPoInvoiceTable();
}

$(function(){

	reloadViewPoInvoiceTable = function() {
		viewPoInvoiceTable.fnDraw();
	}


	$('#search_t412TanggalPO').datepicker().on('changeDate', function(ev) {
			var newDate = new Date(ev.date);
			$('#search_t412TanggalPO_day').val(newDate.getDate());
			$('#search_t412TanggalPO_month').val(newDate.getMonth()+1);
			$('#search_t412TanggalPO_year').val(newDate.getFullYear());
			$(this).datepicker('hide');
			viewPoInvoiceTable.fnDraw();
	});




$("th div input").bind('keypress', function(e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 13) {
			e.stopPropagation();
		 	viewPoInvoiceTable.fnDraw();
		}
	});
	$("th div input").click(function (e) {
	 	e.stopPropagation();
	});


    var anOpen = [];
    $('#viewPoInvoice_datatables_${idTable} td.control').live('click',function () {
   		var nTr = this.parentNode;
  		var i = $.inArray( nTr, anOpen );
   		if ( i === -1 ) {
    		$('i', this).attr( 'class', "icon-minus" );
    		var oData = viewPoInvoiceTable.fnGetData(nTr);
    		$('#spinner').fadeIn(1);
	   		$.ajax({type:'POST',
	   			data : oData,
	   			url:'${request.contextPath}/viewPoInvoice/sublist',
	   			success:function(data,textStatus){
	   				var nDetailsRow = viewPoInvoiceTable.fnOpen(nTr,data,'details');
    				$('div.innerDetails', nDetailsRow).slideDown();
    				anOpen.push( nTr );
	   			},
	   			error:function(XMLHttpRequest,textStatus,errorThrown){},
	   			complete:function(XMLHttpRequest,textStatus){
	   				$('#spinner').fadeOut();
	   			}
	   		});

  		} else {
    		$('i', this).attr( 'class', 'icon-plus' );
    		$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
      			viewPoInvoiceTable.fnClose( nTr );
      			anOpen.splice( i, 1 );
    		});
  		}
	});

	viewPoInvoiceTable = $('#viewPoInvoice_datatables_${idTable}').dataTable({
		"sScrollX": "1200px",
		"bScrollCollapse": true,
		"bAutoWidth" : false,
		"bPaginate" : true,
		"sInfo" : "",
		"sInfoEmpty" : "",
		"sDom": "<'row-fluid'r>t<'row-fluid'<'span12'i>><'row-fluid'<'span12'p>>",
		bFilter: true,
		"bStateSave": false,
		'sPaginationType': 'bootstrap',
		"fnInitComplete": function () {
			this.fnAdjustColumnSizing(true);
		   },
		   "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
			return nRow;
		   },
		"bSort": true,
		"bProcessing": true,
		"bServerSide": true,
		"sServerMethod": "POST",
		"iDisplayLength" : maxLineDisplay, //maxLineDisplay is set in menu/maxLineDisplay.gsp
		"sAjaxSource": "${g.createLink(action: "datatablesList")}",
		"aoColumns": [
{
   "mDataProp": null,
   "sClass": "control center",
   "bSortable": false,
   "sWidth":"10px",
   "sDefaultContent": '<i class="icon-plus"></i>'
},
{
	"sName": "",
	"mDataProp": "namaVendor",
	"aTargets": [0],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"340px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "nomorPo",
	"aTargets": [1],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"170px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "tanggalPo",
	"aTargets": [2],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"170px",
	"bVisible": true
},
{
	"sName": "",
	"mDataProp": "perihal",
	"aTargets": [3],
	"bSearchable": true,
	"bSortable": true,
	"sWidth":"170px",
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"170px",
	"sDefaultContent": '',
	"bVisible": true
},
{
	"mDataProp": null,
	"bSortable": false,
	"sWidth":"170px",
	"sDefaultContent": '',
	"bVisible": true
}


],
        "fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
						var tanggalStart = $("#search_t412TanggalPO_start").val();
						var tanggalStartDay = $('#search_t412TanggalPO_start_day').val();
						var tanggalStartMonth = $('#search_t412TanggalPO_start_month').val();
						var tanggalStartYear = $('#search_t412TanggalPO_start_year').val();
                        if(tanggalStart){
							aoData.push(
									{"name": 'tanggalStart', "value": "date.struct"},
									{"name": 'tanggalStart_dp', "value": tanggalStart},
									{"name": 'tanggalStart_day', "value": tanggalStartDay},
									{"name": 'tanggalStart_month', "value": tanggalStartMonth},
									{"name": 'tanggalStart_year', "value": tanggalStartYear}
							);
						}

                        var tanggalEnd = $("#search_t412TanggalPO_start").val();
                        var tanggalEndDay = $('#search_t412TanggalPO_end_day').val();
						var tanggalEndMonth = $('#search_t412TanggalPO_end_month').val();
						var tanggalEndYear = $('#search_t412TanggalPO_end_year').val();
						if(tanggalEnd){
							aoData.push(
									{"name": 'tanggalEnd', "value": "date.struct"},
									{"name": 'tanggalEnd_dp', "value": tanggalEnd},
									{"name": 'tanggalEnd_day', "value": tanggalEndDay},
									{"name": 'tanggalEnd_month', "value": tanggalEndMonth},
									{"name": 'tanggalEnd_year', "value": tanggalEndYear}
							);
						}

                        var staSPLD1 = $('input[name=staSPLD1]').val();
                        if($('input[name=staSPLD1]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD1', "value": staSPLD1}
							);
						}

                        var staSPLD2 = $('input[name=staSPLD2]').val();
                        if($('input[name=staSPLD2]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD2', "value": staSPLD2}
							);
						}

						var staSPLD3 = $('input[name=staSPLD3]').val();
                        if($('input[name=staSPLD3]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD3', "value": staSPLD1}
							);
						}

                        var staSPLD4 = $('input[name=staSPLD4]').val();
                        if($('input[name=staSPLD4]').is(':checked')){
							aoData.push(
									{"name": 'staSPLD4', "value": staSPLD2}
							);
						}

						$.ajax({ "dataType": 'json',
							"type": "POST",
							"url": sSource,
							"data": aoData ,
							"success": function (json) {
								fnCallback(json);
							   },
							"complete": function () {
							   }
						});
		}
	});
});
</g:javascript>



