
<%@ page import="com.kombos.parts.Vendor; com.kombos.parts.Invoice" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title><g:message code="viewPoInvoice.label" default="View PO - Invoice" /></title>
		<r:require modules="baseapplayout" />
		<g:javascript disposition="head">
	var show;
	var loadForm;
	var shrinkTableLayout;
	var expandTableLayout;
	function selectAll(){
        if($('input[name=checkSelect]').is(':checked')){
            $(".row-select").attr("checked",true);
        }else {
            $(".row-select").attr("checked",false);
        }
    }

    function clearInput(){
        $("#search_t412TanggalPO_start").val("");
        $("#search_t412TanggalPO_start_day").val("");
        $("#search_t412TanggalPO_start_month").val("");
        $("#search_t412TanggalPO_start_year").val("");

        $("#search_t412TanggalPO_end").val("");
        $("#search_t412TanggalPO_end_day").val("");
        $("#search_t412TanggalPO_end_month").val("");
        $("#search_t412TanggalPO_end_year").val("");
        $('#staBelum').prop("checked",false);
        $('#staLengkap').prop("checked",false);
        searchViewPoInvoice();
    }

</g:javascript>
	</head>
	<body>
	<div class="navbar box-header no-border">
		<span class="pull-left"><g:message code="viewPoInvoice.label" default="View PO - Invoice" /></span>
		<ul class="nav pull-right">
		</ul>
	</div>
	<div class="box">
		<div class="span12" id="invoice-table">
			<g:if test="${flash.message}">
				<div class="message" role="status">
					${flash.message}
				</div>
			</g:if>
            <fieldset>
                <table style="padding-right: 10px;">
                    <tr>
                        <td colspan="2">
                            <legend style="font-size: 14px">
                                Filter PO
                            </legend>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="control-label" for="tanggal_invoice">
                                <g:message code="invoice.tanggal.label" default="Tanggal Invoice" />&nbsp;
                            </label>&nbsp;&nbsp;
                        </td>
                        <td>
                            <div id="tanggal_invoice" class="controls">
                                <ba:datePicker id="search_t412TanggalPO_start" name="search_t412TanggalPO_start" precision="day" format="dd/MM/yyyy"  value="" />
                                &nbsp;s.d.&nbsp;
                                <ba:datePicker id="search_t412TanggalPO_end" name="search_t412TanggalPO_end" precision="day" format="dd/MM/yyyy"  value="" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <legend style="font-size: 14px">
                                &nbsp;&nbsp;
                                Status PO
                            </legend>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;&nbsp;
                            <input type="checkbox" name="staBelum" id="staBelum" value="1">&nbsp;PO dengan invoice belum lengkap&nbsp;&nbsp;&nbsp;&nbsp;
                            <br/>
                            <br/>
                            &nbsp;&nbsp;
                            <input type="checkbox" name="staLengkap" id="staLengkap" value="0">&nbsp;PO dengan invoice sudah lengkap
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <div class="controls pull-right" style="right: 0">
                                <a class="btn btn-primary create" href="javascript:void(0);" onclick="searchViewPoInvoice();">
                                    <g:message code="default.search.label" default="Search"/>
                                </a>

                                <a class="btn cancel" href="javascript:void(0);" onclick="clearInput();">
                                    <g:message code="default.clear.label" default="Clear Search"/>
                                </a>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <br/>
            <legend style="font-size: 14px">
                View PO - Invoice
            </legend>
			<g:render template="dataTables" />
		</div>
		<div class="span7" id="invoice-form" style="display: none;"></div>
	</div>
</body>
</html>
